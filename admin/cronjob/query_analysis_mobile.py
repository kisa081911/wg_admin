#!/usr/bin/python
# -*- coding:utf-8 -*-
import string
import os
import MySQLdb
import sys
import time
from datetime import timedelta, date
import logging
import logging.handlers
import traceback
import threading

data = dict()

def daemonize():
        pid = os.fork()
        if pid != 0:
                os._exit(0)

        os.setsid()
        maxfd = os.sysconf("SC_OPEN_MAX")
        for fd in range(0, maxfd):
                try:
                        os.close(fd)
                except OSError:
                        pass

def prepare_log():
	log = logging.getLogger('/take5/QueryAnalysis/info_mobile.log')
	handler = logging.handlers.RotatingFileHandler('/take5/QueryAnalysis/info_mobile.log', 'a', 1024*1024*10, 2)
	fmt = logging.Formatter('%(asctime)s: %(message)s', "%Y-%m-%d %H:%M:%S")
	handler.setFormatter(fmt)
	log.addHandler(handler)
	log.setLevel(logging.DEBUG)
	return log

def GetServerName(ipaddress):
	if ipaddress == '10.3.0.12':
		return 'AnalysisDB'
	elif ipaddress == '10.3.0.150':
		return 'Mobilecommon1'
	elif ipaddress == '10.3.0.151':
		return 'Mobilecommon2'
	elif ipaddress == '10.3.0.16':
		return 'FriendDB'
	elif ipaddress == '10.3.0.13':
		return 'GameDB'
	elif ipaddress == '10.3.0.17':
		return 'LivestatsDB'
	elif ipaddress == '10.3.0.10':
		return 'MainDB'
	elif ipaddress == '10.3.0.11':
		return 'MainDB2'
	elif ipaddress == '10.3.0.200':
		return 'web01'
	elif ipaddress == '10.3.0.201':
		return 'web02'
	elif ipaddress == '10.3.0.203':
		return 'web03'
	elif ipaddress == '218.38.136.51':
		return 'Dev'
	else:
		return 'unknown'


def GetMaxlogidx():
	try:
		db = MySQLdb.connect('218.38.136.51', 'w_analysis', 'Troqkftjqj@0420', 'takefive_analysis')
		cursor = db.cursor()

		sql = "select ifnull(max(writedate), date_sub(now(), interval 1 hour)) from query_slow_log_mobile"
		cursor.execute(sql)

		result = cursor.fetchone()
		cursor.close()
		db.close()

		if result == None:
			return 0

		return result[0]

	except MySQLdb.MySQLError, message:
		log.debug(message)
		log.debug(sql)
		cursor.close()
		db.close()


def AnalysisQuery(writedate):
	try:
		db = MySQLdb.connect('218.38.136.51', 'takeadmin', 'xpdlzm5vkdlqm', 'takefive_analysis')
		cursor = db.cursor()

		sql = 'SELECT substring_index(contents, "Query", -1), writedate, ipaddress FROM server_log_mobile WHERE writedate > "%s" AND contents LIKE "%%Slow Query%%" ORDER BY writedate limit 1000'%writedate
		cursor.execute(sql)

		result = cursor.fetchall()

		stime = time.time()
		for row in result:

			try:
				query = row[0].split(') - ')[1]
				tokenlist = row[0].split(' ')
				querytime = tokenlist[0].replace('(', '')

				lowerquery = query.lower()
				if lowerquery.count('insert') >= 1:
					querytype = 'insert'
				elif lowerquery.count('update') >= 1:
					querytype = 'update'
				elif lowerquery.count('delete') >= 1:
					querytype = 'delete'
				else:
					querytype = 'select'

				sql = "insert into query_slow_log_mobile (ipaddress, servername, querytype, querytime, content, writedate) values('%s', '%s', '%s', %s, '%s', '%s')"%(row[2], GetServerName(row[2]), querytype, querytime, query, row[1])
				log.debug(sql)
				cursor.execute(sql)

				sql = "SELECT LAST_INSERT_ID()"
				cursor.execute(sql)
				queryidx = cursor.fetchone()[0]

				for token in tokenlist:
					if token.count('tbl') == 1 and token.count('.') == 0:
						table = token.split('(')[0]
						if table.find('tbl_user_online') >= 0:
							table = table[:15]
						if table.find('tbl_tournament') >= 0:
							table = table[:14]
						if table.find('tbl_user_friend') >= 0:
							table = table[:15]
						if table.find('tbl_user_inbox') > 0:
							table = table[:14]	
						if table.find('tbl_user_inbox_history') > 0:
							table = table[:22]
						if table.find('tbl_freecoin_log') > 0:
							table = table[:16]
						if table.find('tbl_bonus_log') > 0:
							table = table[:13]
						if table.find('tbl_game_log') >= 0:
							table = table[:12]
						sql = "insert into query_slow_table_mobile (queryidx, tablename) values(%d, '%s')"%(queryidx, table)
						log.debug(sql)
						cursor.execute(sql)
				

			except MySQLdb.MySQLError, message:
				log.debug(message)
				log.debug(sql)
		
			db.commit()

			writedate = row[1]

		time.sleep(0.1)

		etime = time.time()
		print etime - stime

		cursor.close()
		db.close()

		return writedate

	except Exception,e:
		log.debug(str(e))
		log.debug(e)
		log.debug(traceback.format_tb(sys.exc_info()[2]))
		cursor.close()
		db.close()


def SyncThread(writedate):
	writedate = GetMaxlogidx()

	while 1:
		try:
			if writedate == 0: 
				writedate = GetMaxlogidx()
				time.sleep(1)
				continue

			writedate = AnalysisQuery(writedate)

			print writedate
			time.sleep(10)

		except Exception,e:
			log.debug(str(e))
			log.debug(e)
			log.debug(traceback.format_tb(sys.exc_info()[2]))


daemonize()
log = prepare_log()

if __name__ == "__main__":

	"""
	if len(sys.argv) == 2:
		filepath = sys.argv[1]
	else:		
		filepath = mk_filepath()
	"""

	SyncThread(1)
	"""
	sync = threading.Thread(target=SyncThread, name=1)
	sync.start()
	sync.join()
	"""

#	for key in data:
#		log.debug("%d	%d"%(key, data[key]))

	sys.exit()
