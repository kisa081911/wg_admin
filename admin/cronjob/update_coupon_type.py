#!/usr/bin/env python

import string
import os
import MySQLdb
import sys
import time
from datetime import timedelta, date
import logging
import logging.handlers
import traceback
import threading

def daemonize():
        pid = os.fork()
        if pid != 0:
                os._exit(0)

        os.setsid()
        maxfd = os.sysconf("SC_OPEN_MAX")
        for fd in range(0, maxfd):
                try:
                        os.close(fd)
                except OSError:
                        pass

def prepare_log():
	log = logging.getLogger('/take5/cronjob/gamestat.log')
	handler = logging.handlers.RotatingFileHandler('/take5/cronjob/updatecoupon_type.log', 'a', 1024*1024*10, 5)
	fmt = logging.Formatter('%(asctime)s: %(message)s', "%Y-%m-%d %H:%M:%S")
	handler.setFormatter(fmt)
	log.addHandler(handler)
	log.setLevel(logging.DEBUG)
	return log


def InsertGameStats():
	try:
		db = MySQLdb.connect('218.38.136.51', 'w_main', 'Troqkftjqj@0420', 'takefive')
		cursor = db.cursor()

		#
		# tbl_product_order UPDATE
		#
	
		sql = "select count(*) as cnt from tbl_product_order WHERE couponidx = 0 AND coupon_type = 0;"
		
		cursor.execute(sql)
		result = cursor.fetchone()
		cnt = result[0]
		print sql
	
		while cnt > 0:
			sql = "UPDATE tbl_product_order SET coupon_type = -1 WHERE couponidx = 0 AND coupon_type = 0 LIMIT 10" 
			print sql
			cursor.execute(sql)
			db.commit()
			cnt -= 10
			time.sleep(1)
			
		#
		# tbl_product_order_mobile UPDATE
		#
	
		sql = "select count(*) as cnt from tbl_product_order_mobile WHERE couponidx = 0 AND coupon_type = 0;"
		
		cursor.execute(sql)
		result = cursor.fetchone()
		cnt = result[0]
		print sql
	
		while cnt > 0:
			sql = "UPDATE tbl_product_order_mobile SET coupon_type = -1 WHERE couponidx = 0 AND coupon_type = 0 LIMIT 10" 
			print sql
			cursor.execute(sql)
			db.commit()
			cnt -= 10
			time.sleep(1)

		cursor.close()
		db.close()

	except MySQLdb.MySQLError, message:
		log.debug(message)
		log.debug(sql)
		cursor.close()
		db.close()

def SyncThread():
	InsertGameStats()

log = prepare_log()

if __name__ == "__main__":

	SyncThread()

	sys.exit()

