#!/usr/bin/python
# -*- coding:utf-8 -*-
import MySQLdb
import sys
import time

def main():
        db = MySQLdb.connect('218.38.136.51', 'w_livestats', 'Troqkftjqj@0420', 'takefive_livestats')
        cursor = db.cursor()

	#
	# tbl_user_access_rawlog Delete
	#	
	for i in range(0, 10):
		sql = "SELECT COUNT(*) FROM tbl_user_access_rawlog_%d " % i
		sql += "WHERE TIMESTAMP < CAST(DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 48 HOUR), '%Y%m%d%H%i') AS UNSIGNED);"

		cursor.execute(sql)
		result = cursor.fetchone()
		cnt = result[0]
		print sql

		while cnt > 0:
			sql = "DELETE FROM tbl_user_access_rawlog_%d " % i
			sql += "WHERE TIMESTAMP < CAST(DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 48 HOUR), '%Y%m%d%H%i') AS UNSIGNED) LIMIT 5000;"
			print sql
			cursor.execute(sql)
			db.commit()
			cnt -= 5000
			time.sleep(1)

	#
	# tbl_user_inbox_collect Delete
	#
	for i in range(0, 20):
		sql = "SELECT COUNT(*) FROM tbl_user_inbox_collect_%d WHERE writedate < DATE_SUB(NOW(), INTERVAL 1 WEEK);" % i

		cursor.execute(sql)
		result = cursor.fetchone()
		cnt = result[0]
		print sql

		while cnt > 0:
			sql = "DELETE FROM tbl_user_inbox_collect_%d WHERE writedate < DATE_SUB(NOW(), INTERVAL 1 WEEK) LIMIT 5000;" % i
			print sql
			cursor.execute(sql)
			db.commit()
			cnt -= 5000
			time.sleep(1)
	
	#
	# tbl_user_retention_log Delete
	#	
	for i in range(0, 10):
		sql = "SELECT COUNT(*) FROM tbl_user_retention_log_%d WHERE today < DATE_SUB(NOW(), INTERVAL 95 DAY);" % i

		cursor.execute(sql)
        	result = cursor.fetchone()
        	cnt = result[0]
        	print sql

		while cnt > 0:
			sql = "DELETE FROM tbl_user_retention_log_%d WHERE today < DATE_SUB(NOW(), INTERVAL 95 DAY) LIMIT 5000;" % i
	                print sql
        	        cursor.execute(sql)
                	db.commit()
	                cnt -= 5000
	                time.sleep(1)
	                
	#
	# tbl_user_pay_retention_log Delete
	#	
	for i in range(0, 10):
		sql = "SELECT COUNT(*) FROM tbl_user_pay_retention_log_%d WHERE today < DATE_SUB(NOW(), INTERVAL 95 DAY);" % i

		cursor.execute(sql)
        	result = cursor.fetchone()
        	cnt = result[0]
        	print sql

		while cnt > 0:
			sql = "DELETE FROM tbl_user_pay_retention_log_%d WHERE today < DATE_SUB(NOW(), INTERVAL 95 DAY) LIMIT 5000;" % i
	                print sql
        	        cursor.execute(sql)
                	db.commit()
	                cnt -= 5000
	                time.sleep(1)
					
	#
	# tbl_user_30day_retention_log Delete
	#	
	for i in range(0, 10):
		sql = "SELECT COUNT(*) FROM tbl_user_30day_retention_log_%d WHERE today < DATE_SUB(NOW(), INTERVAL 95 DAY);" % i

		cursor.execute(sql)
        	result = cursor.fetchone()
        	cnt = result[0]
        	print sql

		while cnt > 0:
			sql = "DELETE FROM tbl_user_30day_retention_log_%d WHERE today < DATE_SUB(NOW(), INTERVAL 95 DAY) LIMIT 5000;" % i
	                print sql
        	        cursor.execute(sql)
                	db.commit()
	                cnt -= 5000
	                time.sleep(1)
	                
	#
	# tbl_user_login_log Delete
	#	
	for i in range(0, 10):
		sql = "SELECT COUNT(*) FROM tbl_user_login_log_%d WHERE today < DATE_SUB(NOW(), INTERVAL 95 DAY);" % i

		cursor.execute(sql)
        	result = cursor.fetchone()
        	cnt = result[0]
        	print sql

		while cnt > 0:
			sql = "DELETE FROM tbl_user_login_log_%d WHERE today < DATE_SUB(NOW(), INTERVAL 95 DAY) LIMIT 5000;" % i
	                print sql
        	        cursor.execute(sql)
                	db.commit()
	                cnt -= 5000
	                time.sleep(1)


	cursor.close()
	db.close()


if __name__ == '__main__':
        main()
        sys.exit()
