#!/usr/bin/env python

import string
import os
import MySQLdb
import sys
import time
from datetime import timedelta, date
import logging
import logging.handlers
import traceback
import threading

def daemonize():
        pid = os.fork()
        if pid != 0:
                os._exit(0)

        os.setsid()
        maxfd = os.sysconf("SC_OPEN_MAX")
        for fd in range(0, maxfd):
                try:
                        os.close(fd)
                except OSError:
                        pass

def prepare_log():
	log = logging.getLogger('/take5/cronjob/gamestat.log')
	handler = logging.handlers.RotatingFileHandler('/take5/cronjob/gamestat.log', 'a', 1024*1024*10, 5)
	fmt = logging.Formatter('%(asctime)s: %(message)s', "%Y-%m-%d %H:%M:%S")
	handler.setFormatter(fmt)
	log.addHandler(handler)
	log.setLevel(logging.DEBUG)
	return log


def InsertGameStats():
	try:
		db = MySQLdb.connect('218.38.136.51', 'w_main2', 'Troqkftjqj@0420', 'takefive2')
		cursor = db.cursor()

		stime = time.time()

		sql = "INSERT INTO tbl_game_cash_stats_daily SELECT CONCAT(DATE_FORMAT(writedate, '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(writedate, '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(writedate, '%i')/5)*5, ':00') unit, slottype, mode, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2  FROM tbl_game_cash_stats WHERE writedate >= ( SELECT IFNULL(DATE_ADD(MAX(writedate), INTERVAL 5 MINUTE), '2015-01-01 00:00:00') FROM tbl_game_cash_stats_daily WHERE is_v2 = 0) AND writedate < CONCAT(DATE_FORMAT(NOW(), '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5, ':00') AND is_v2  = 0 GROUP BY unit, slottype, mode, betlevel"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()
		
		sql = "INSERT INTO tbl_game_cash_stats_daily2(writedate, slottype, MODE, betlevel, moneyin, moneyout, unit_moneyin, unit_moneyout, treatamount, playcount, is_v2) SELECT DATE_FORMAT(writedate, '%Y-%m-%d') unit, slottype, MODE, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2 FROM tbl_game_cash_stats_daily WHERE writedate >= (SELECT IFNULL(DATE_SUB(MAX(writedate), INTERVAL 1 DAY), '2015-01-01') FROM tbl_game_cash_stats_daily2 WHERE is_v2 = 0) AND writedate < DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY) AND is_v2 = 0  GROUP BY unit, slottype, MODE, betlevel ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), moneyout = VALUES(moneyout), unit_moneyin = VALUES(unit_moneyin), unit_moneyout = VALUES(unit_moneyout), treatamount = VALUES(treatamount), playcount=VALUES(playcount);"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()
		
		sql = "INSERT INTO tbl_game_cash_stats_ios_daily SELECT CONCAT(DATE_FORMAT(writedate, '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(writedate, '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(writedate, '%i')/5)*5, ':00') unit, slottype, mode, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2  FROM tbl_game_cash_stats_ios WHERE writedate >= ( SELECT IFNULL(DATE_ADD(MAX(writedate), INTERVAL 5 MINUTE), '2015-01-01 00:00:00') FROM tbl_game_cash_stats_ios_daily WHERE is_v2 = 0) AND writedate < CONCAT(DATE_FORMAT(NOW(), '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5, ':00') AND is_v2  = 0 GROUP BY unit, slottype, mode, betlevel"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()
		
		sql = "INSERT INTO tbl_game_cash_stats_ios_daily2(writedate, slottype, MODE, betlevel, moneyin, moneyout, unit_moneyin, unit_moneyout, treatamount, playcount, is_v2) SELECT DATE_FORMAT(writedate, '%Y-%m-%d') unit, slottype, MODE, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2 FROM tbl_game_cash_stats_ios_daily WHERE writedate >= (SELECT IFNULL(DATE_SUB(MAX(writedate), INTERVAL 1 DAY), '2015-01-01') FROM tbl_game_cash_stats_ios_daily2 WHERE is_v2 = 0) AND writedate < DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY) AND is_v2 = 0  GROUP BY unit, slottype, MODE, betlevel ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), moneyout = VALUES(moneyout), unit_moneyin = VALUES(unit_moneyin), unit_moneyout = VALUES(unit_moneyout), treatamount = VALUES(treatamount), playcount=VALUES(playcount);"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()
		
		sql = "INSERT INTO tbl_game_cash_stats_android_daily SELECT CONCAT(DATE_FORMAT(writedate, '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(writedate, '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(writedate, '%i')/5)*5, ':00') unit, slottype, mode, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2  FROM tbl_game_cash_stats_android WHERE writedate >= ( SELECT IFNULL(DATE_ADD(MAX(writedate), INTERVAL 5 MINUTE), '2015-01-01 00:00:00') FROM tbl_game_cash_stats_android_daily WHERE is_v2 = 0) AND writedate < CONCAT(DATE_FORMAT(NOW(), '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5, ':00') AND is_v2  = 0 GROUP BY unit, slottype, mode, betlevel"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()
		
		sql = "INSERT INTO tbl_game_cash_stats_android_daily2(writedate, slottype, MODE, betlevel, moneyin, moneyout, unit_moneyin, unit_moneyout, treatamount, playcount, is_v2) SELECT DATE_FORMAT(writedate, '%Y-%m-%d') unit, slottype, MODE, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2 FROM tbl_game_cash_stats_android_daily WHERE writedate >= (SELECT IFNULL(DATE_SUB(MAX(writedate), INTERVAL 1 DAY), '2015-01-01') FROM tbl_game_cash_stats_android_daily2 WHERE is_v2 = 0) AND writedate < DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY) AND is_v2 = 0  GROUP BY unit, slottype, MODE, betlevel ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), moneyout = VALUES(moneyout), unit_moneyin = VALUES(unit_moneyin), unit_moneyout = VALUES(unit_moneyout), treatamount = VALUES(treatamount), playcount=VALUES(playcount);"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()
		
		sql = "INSERT INTO tbl_game_cash_stats_amazon_daily SELECT CONCAT(DATE_FORMAT(writedate, '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(writedate, '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(writedate, '%i')/5)*5, ':00') unit, slottype, mode, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2  FROM tbl_game_cash_stats_amazon WHERE writedate >= ( SELECT IFNULL(DATE_ADD(MAX(writedate), INTERVAL 5 MINUTE), '2015-01-01 00:00:00') FROM tbl_game_cash_stats_amazon_daily WHERE is_v2 = 0) AND writedate < CONCAT(DATE_FORMAT(NOW(), '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5, ':00') AND is_v2  = 0 GROUP BY unit, slottype, mode, betlevel"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()
		
		sql = "INSERT INTO tbl_game_cash_stats_amazon_daily2(writedate, slottype, MODE, betlevel, moneyin, moneyout, unit_moneyin, unit_moneyout, treatamount, playcount, is_v2) SELECT DATE_FORMAT(writedate, '%Y-%m-%d') unit, slottype, MODE, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2 FROM tbl_game_cash_stats_amazon_daily WHERE writedate >= (SELECT IFNULL(DATE_SUB(MAX(writedate), INTERVAL 1 DAY), '2015-01-01') FROM tbl_game_cash_stats_amazon_daily2 WHERE is_v2 = 0) AND writedate < DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY) AND is_v2 = 0  GROUP BY unit, slottype, MODE, betlevel ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), moneyout = VALUES(moneyout), unit_moneyin = VALUES(unit_moneyin), unit_moneyout = VALUES(unit_moneyout), treatamount = VALUES(treatamount), playcount=VALUES(playcount);"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()
		
		##V2
		sql = "INSERT INTO tbl_game_cash_stats_daily SELECT CONCAT(DATE_FORMAT(writedate, '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(writedate, '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(writedate, '%i')/5)*5, ':00') unit, slottype, mode, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2  FROM tbl_game_cash_stats WHERE writedate >= ( SELECT IFNULL(DATE_ADD(MAX(writedate), INTERVAL 5 MINUTE), '2015-01-01 00:00:00') FROM tbl_game_cash_stats_daily WHERE is_v2 = 1) AND writedate < CONCAT(DATE_FORMAT(NOW(), '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5, ':00') AND is_v2  = 1 GROUP BY unit, slottype, mode, betlevel"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()
		
		sql = "INSERT INTO tbl_game_cash_stats_daily2(writedate, slottype, MODE, betlevel, moneyin, moneyout, unit_moneyin, unit_moneyout, treatamount, playcount, is_v2) SELECT DATE_FORMAT(writedate, '%Y-%m-%d') unit, slottype, MODE, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2 FROM tbl_game_cash_stats_daily WHERE writedate >= (SELECT IFNULL(DATE_SUB(MAX(writedate), INTERVAL 1 DAY), '2015-01-01') FROM tbl_game_cash_stats_daily2 WHERE is_v2 = 1) AND writedate < DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY) AND is_v2 = 1  GROUP BY unit, slottype, MODE, betlevel ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), moneyout = VALUES(moneyout), unit_moneyin = VALUES(unit_moneyin), unit_moneyout = VALUES(unit_moneyout), treatamount = VALUES(treatamount), playcount=VALUES(playcount);"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()
		
		sql = "INSERT INTO tbl_game_cash_stats_ios_daily SELECT CONCAT(DATE_FORMAT(writedate, '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(writedate, '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(writedate, '%i')/5)*5, ':00') unit, slottype, mode, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2  FROM tbl_game_cash_stats_ios WHERE writedate >= ( SELECT IFNULL(DATE_ADD(MAX(writedate), INTERVAL 5 MINUTE), '2015-01-01 00:00:00') FROM tbl_game_cash_stats_ios_daily WHERE is_v2 = 1) AND writedate < CONCAT(DATE_FORMAT(NOW(), '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5, ':00') AND is_v2  = 1 GROUP BY unit, slottype, mode, betlevel"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()
		
		sql = "INSERT INTO tbl_game_cash_stats_ios_daily2(writedate, slottype, MODE, betlevel, moneyin, moneyout, unit_moneyin, unit_moneyout, treatamount, playcount, is_v2) SELECT DATE_FORMAT(writedate, '%Y-%m-%d') unit, slottype, MODE, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2 FROM tbl_game_cash_stats_ios_daily WHERE writedate >= (SELECT IFNULL(DATE_SUB(MAX(writedate), INTERVAL 1 DAY), '2015-01-01') FROM tbl_game_cash_stats_ios_daily2 WHERE is_v2 = 1) AND writedate < DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY) AND is_v2 = 1  GROUP BY unit, slottype, MODE, betlevel ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), moneyout = VALUES(moneyout), unit_moneyin = VALUES(unit_moneyin), unit_moneyout = VALUES(unit_moneyout), treatamount = VALUES(treatamount), playcount=VALUES(playcount);"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()
		
		sql = "INSERT INTO tbl_game_cash_stats_android_daily SELECT CONCAT(DATE_FORMAT(writedate, '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(writedate, '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(writedate, '%i')/5)*5, ':00') unit, slottype, mode, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2  FROM tbl_game_cash_stats_android WHERE writedate >= ( SELECT IFNULL(DATE_ADD(MAX(writedate), INTERVAL 5 MINUTE), '2015-01-01 00:00:00') FROM tbl_game_cash_stats_android_daily WHERE is_v2 = 1) AND writedate < CONCAT(DATE_FORMAT(NOW(), '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5, ':00') AND is_v2  = 1 GROUP BY unit, slottype, mode, betlevel"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()
		
		sql = "INSERT INTO tbl_game_cash_stats_android_daily2(writedate, slottype, MODE, betlevel, moneyin, moneyout, unit_moneyin, unit_moneyout, treatamount, playcount, is_v2) SELECT DATE_FORMAT(writedate, '%Y-%m-%d') unit, slottype, MODE, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2 FROM tbl_game_cash_stats_android_daily WHERE writedate >= (SELECT IFNULL(DATE_SUB(MAX(writedate), INTERVAL 1 DAY), '2015-01-01') FROM tbl_game_cash_stats_android_daily2 WHERE is_v2 = 1) AND writedate < DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY) AND is_v2 = 1  GROUP BY unit, slottype, MODE, betlevel ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), moneyout = VALUES(moneyout), unit_moneyin = VALUES(unit_moneyin), unit_moneyout = VALUES(unit_moneyout), treatamount = VALUES(treatamount), playcount=VALUES(playcount);"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()
		
		sql = "INSERT INTO tbl_game_cash_stats_amazon_daily SELECT CONCAT(DATE_FORMAT(writedate, '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(writedate, '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(writedate, '%i')/5)*5, ':00') unit, slottype, mode, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2  FROM tbl_game_cash_stats_amazon WHERE writedate >= ( SELECT IFNULL(DATE_ADD(MAX(writedate), INTERVAL 5 MINUTE), '2015-01-01 00:00:00') FROM tbl_game_cash_stats_amazon_daily WHERE is_v2 = 1) AND writedate < CONCAT(DATE_FORMAT(NOW(), '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5, ':00') AND is_v2  = 1 GROUP BY unit, slottype, mode, betlevel"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()
		
		sql = "INSERT INTO tbl_game_cash_stats_amazon_daily2(writedate, slottype, MODE, betlevel, moneyin, moneyout, unit_moneyin, unit_moneyout, treatamount, playcount, is_v2) SELECT DATE_FORMAT(writedate, '%Y-%m-%d') unit, slottype, MODE, betlevel, SUM(moneyin) moneyin, SUM(moneyout) moneyout, SUM(unit_moneyin) unit_moneyin, SUM(unit_moneyout) unit_moneyout, SUM(treatamount) treatamount, SUM(playcount) playcount, is_v2 FROM tbl_game_cash_stats_amazon_daily WHERE writedate >= (SELECT IFNULL(DATE_SUB(MAX(writedate), INTERVAL 1 DAY), '2015-01-01') FROM tbl_game_cash_stats_amazon_daily2 WHERE is_v2 = 1) AND writedate < DATE_ADD(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY) AND is_v2 = 1  GROUP BY unit, slottype, MODE, betlevel ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), moneyout = VALUES(moneyout), unit_moneyin = VALUES(unit_moneyin), unit_moneyout = VALUES(unit_moneyout), treatamount = VALUES(treatamount), playcount=VALUES(playcount);"
		log.debug(sql)
		cursor.execute(sql)
		db.commit()


		etime = time.time()
		print etime - stime

		log.debug(etime -stime)

		cursor.close()
		db.close()

	except MySQLdb.MySQLError, message:
		log.debug(message)
		log.debug(sql)
		cursor.close()
		db.close()

def SyncThread():
	InsertGameStats()

log = prepare_log()

if __name__ == "__main__":

	SyncThread()

	sys.exit()

