#!/usr/bin/env python

import string
import os
import MySQLdb
import sys
import time
from datetime import timedelta, date
import logging
import logging.handlers
import traceback
import threading

def daemonize():
        pid = os.fork()
        if pid != 0:
                os._exit(0)

        os.setsid()
        maxfd = os.sysconf("SC_OPEN_MAX")
        for fd in range(0, maxfd):
                try:
                        os.close(fd)
                except OSError:
                        pass

def prepare_log():
	log = logging.getLogger('/take5/cronjob/inbox.log')
	handler = logging.handlers.RotatingFileHandler('/take5/cronjob/inbox.log', 'a', 1024*1024*10, 5)
	fmt = logging.Formatter('%(asctime)s: %(message)s', "%Y-%m-%d %H:%M:%S")
	handler.setFormatter(fmt)
	log.addHandler(handler)
	log.setLevel(logging.DEBUG)
	return log


def DeleteInbox():
	try:
		db = MySQLdb.connect('218.38.136.51', 'w_inbox', 'Troqkftjqj@0420', 'takefive_inbox')
		cursor = db.cursor()
		
		stime = time.time()
		
		#
		# tbl_user_inbox Delete (Notification Gift [1001,1301,1401,1501,1601,1701,1801,1901])
		#
		for i in range(0, 20):
			sql = "SELECT COUNT(*) FROM tbl_user_inbox_%d WHERE writedate < DATE_SUB(NOW(), INTERVAL 48 HOUR) AND category IN (1001,1301,1401,1501,1601,1701,1801,1901);" % i
	
			cursor.execute(sql)
			result = cursor.fetchone()
			cnt = result[0]
			print sql
	
			while cnt > 0:
				sql = "DELETE FROM tbl_user_inbox_%d WHERE writedate < DATE_SUB(NOW(), INTERVAL 48 HOUR) AND category IN (1001,1301,1401,1501,1601,1701,1801,1901) LIMIT 5000;" % i
				print sql
				cursor.execute(sql)
				db.commit()
				cnt -= 5000
				
		time.sleep(1)
		
		#
		# tbl_user_inbox Delete
		#
		for i in range(0, 20):
			sql = "SELECT COUNT(*) FROM tbl_user_inbox_%d WHERE writedate < DATE_SUB(NOW(), INTERVAL 74 HOUR);" % i
	
			cursor.execute(sql)
			result = cursor.fetchone()
			cnt = result[0]
			print sql
	
			while cnt > 0:
				sql = "DELETE FROM tbl_user_inbox_%d WHERE writedate < DATE_SUB(NOW(), INTERVAL 74 HOUR) LIMIT 5000;" % i
				print sql
				cursor.execute(sql)
				db.commit()
				cnt -= 5000
				
		time.sleep(1)
		
		#
		# tbl_user_inbox_month Delete
		#
		sql = "SELECT COUNT(*) FROM tbl_user_inbox_month WHERE YEAR < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 3 MONTH), '%Y') OR (YEAR <= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 3 MONTH), '%Y') AND MONTH < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 3 MONTH), '%m'));"
	
		cursor.execute(sql)
		result = cursor.fetchone()
		cnt = result[0]
		print sql
	
		while cnt > 0:
			sql = "DELETE FROM tbl_user_inbox_month WHERE YEAR < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 3 MONTH), '%Y') OR (YEAR <= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 3 MONTH), '%Y') AND MONTH < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 3 MONTH), '%m')) LIMIT 5000;"
			print sql
			cursor.execute(sql)
			db.commit()
			cnt -= 5000
			time.sleep(1)
		
		etime = time.time()
		print etime - stime

		log.debug(etime -stime)
			
		cursor.close()
		db.close()
		

	except MySQLdb.MySQLError, message:
		log.debug(message)
		log.debug(sql)
		cursor.close()
		db.close()


def SyncThread():
	while 1:
		DeleteInbox()

		time.sleep(15)


daemonize()
log = prepare_log()

if __name__ == "__main__":

	SyncThread()
	"""
	sync = threading.Thread(target=SyncThread, name=1)
	sync.start()
	sync.join()
	"""

	sys.exit()

