<?  
    function game_insert_event()
    {
        global $db_main2;        
        
        $title = $_POST["title"];
        $category = $_POST["category"];
        $reward_type = $_POST["reward_type"];
        $reward_amount = $_POST["reward_amount"];
        $reward_amount_mobile = $_POST["reward_amount_mobile"];
        $total_coin = $_POST["total_coin"];
        $start_eventdate = $_POST["start_eventdate"];
        $end_eventdate = $_POST["end_eventdate"];
        $event_imagepath = $_POST["event_imagepath"];
        $eventcode = $_POST["eventcode"];
        $slotidx = $_POST["slotidx"];
        $slotbetcoin = $_POST["slotbetcoin"];
        $freespincnt = $_POST["freespincnt"];
        $reservation_date = $_POST["reservation_date"];
        $reservation_hour = $_POST["reservation_hour"];
        $reservation_minute = $_POST["reservation_minute"];
        $reservation_fanpage = $_POST["reservation_fanpage"];
        $reservation_imagepath = $_POST["reservation_imagepath"];
        $reservation_contents = $_POST["reservation_contents"];        
        $share_enabled = $_POST["share_enabled"];
        $featured_list = $_POST["featured_list"];
        $shorturl = $_POST["shorturl"];
        
        // Email Retention (category : 8,9,10)
        $agencyidx = $_POST["agencyidx"];
        $sendidx = $_POST["sendidx"];
        $promotion_text = encode_db_field($_POST["promotion_text"]);
        $contents_image_url = $_POST["contents_image_url"];
        $email_width = $_POST["email_width"];
        $target_type = $_POST["target_type"];
        $promotion_text =  str_replace('\\', '', $promotion_text);

        if ($title == "" || $category == "" || $start_eventdate == "" || $end_eventdate == "")
            return "필수항목이 빠졌습니다.";
        
        check_number($reward_type.$reward_amount);
        check_number($reward_type.$reward_amount_mobile);
        check_xss($title.$event_imagepath);
        
        if ($slotidx == "")
        	$slotidx = "0";
        	
       	if ($share_enabled == "")
       		$share_enabled = "0"; 
       	
       	$limit_total = 0;
       	
       	if($category == "4")
       		$limit_total = $total_coin;
       		
       	$addparam = "";       	
       	
       	if($slotidx != "" && $slotidx > 0 && $category != 6)
       		$addparam = "&slotidx=$slotidx";       	
       	
       	$url = FACEBOOK_CANVAS_URL."?eventcode=$eventcode".$addparam;
        
        $url .= "&dummy=".rand(0, 1000);

        //$shorturl= getShortURL($url);
        
        $sql = "SELECT shorturl FROM tbl_event WHERE shorturl = '$shorturl'";
        $duplicated_url = $db_main2->getvalue($sql);
        
        if($duplicated_url != "")
        {
        	return "중복된 Short URL 입니다.";
        }      
        
        $eventidx_sql = "SELECT MAX(eventidx)+1 AS new_eventidx ". 
							" FROM ".
							" ( ". 
							"	SELECT MAX(eventidx) AS eventidx FROM `tbl_ultra_freespin_event` ". 
							"	UNION ALL  ".
							"	SELECT MAX(eventidx) AS eventidx FROM `tbl_event` ".
							"	UNION ALL  ".
							"	SELECT MAX(eventidx) AS eventidx FROM `tbl_push_event` ".
							" ) t1";
        $new_eventidx = $db_main2->getvalue($eventidx_sql);
        
        $reservation_date = $reservation_date." ".$reservation_hour.":".$reservation_minute;
        
        $sql = "INSERT INTO tbl_event(eventidx, title,category,reward_type,reward_amount,reward_amount_mobile,limit_join,limit_total,start_eventdate,end_eventdate,event_imagepath,shorturl,eventcode,slotidx,reservation_date,reservation_fanpage,reservation_imagepath,reservation_contents,share_enabled,writedate)".
            " VALUES($new_eventidx,'$title','$category','$reward_type','$reward_amount','$reward_amount_mobile','$total_coin','$limit_total','$start_eventdate','$end_eventdate','$event_imagepath','$shorturl','$eventcode','$slotidx','$reservation_date','$reservation_fanpage','$reservation_imagepath','$reservation_contents','$share_enabled',NOW())";
        $db_main2->execute($sql);
        

        $eventidx = $new_eventidx;
        
        // Slot Free Spin
        if ($category == "6")
        {
        	$startdate = "$start_eventdate 00:00:00";
        	$enddate = "$end_eventdate 23:59:59";
        	 
       		$sql = "INSERT INTO tbl_ultra_freespin_event(eventidx, slot_type, totalbet, freespin_cnt, startdate, enddate) VALUES('$eventidx','$slotidx','$slotbetcoin','$freespincnt', '$startdate', '$enddate');";        
        	$db_main2->execute($sql);
        }
        
        // Email Retention
        if($category == "7" || $category == "8" || $category == "9")
        {
        	if($agencyidx == "1")
        		$agencyname = "SocketLabs";
        	else if($agencyidx == "2")
        		$agencyname = "SendPulse";
        	else if($agencyidx == "3")
        		$agencyname = "SES";
        
        	$sql = "INSERT INTO tbl_email_retention_setting(eventidx, agencyidx, agencyname, sendidx, eventcode, promotion_text, contents_image_url, email_width, shorturl, target_type) ".
        	   	   "VALUES($eventidx, $agencyidx, '$agencyname', $sendidx, '$eventcode', '$promotion_text', '$contents_image_url', $email_width, '$shorturl', $target_type);";
        	$db_main2->execute($sql);
        }
    }

    function game_update_event()
    {
        global $db_main2;        
        
        $eventidx = $_POST["eventidx"];
        $title = $_POST["title"];
        $category = $_POST["category"];
        $reward_type = $_POST["reward_type"];
        $reward_amount = $_POST["reward_amount"];
        $reward_amount_mobile = $_POST["reward_amount_mobile"];
        $total_coin = $_POST["total_coin"];
        $start_eventdate = $_POST["start_eventdate"];
        $end_eventdate = $_POST["end_eventdate"];
        $event_imagepath = $_POST["event_imagepath"];        
        $eventcode = $_POST["eventcode"];
        $slotidx = $_POST["slotidx"];
        $slotbetcoin = $_POST["slotbetcoin"];
        $freespincnt = $_POST["freespincnt"];
        $reservation_date = $_POST["reservation_date"];
        $reservation_hour = $_POST["reservation_hour"];
        $reservation_minute = $_POST["reservation_minute"];
        $reservation_fanpage = $_POST["reservation_fanpage"];        
        $reservation_imagepath = $_POST["reservation_imagepath"];
        $reservation_contents = $_POST["reservation_contents"];        
        $share_enabled = $_POST["share_enabled"]; 
        $shorturl = $_POST["shorturl"];
        
        // Email Retention (category : 8, 9)
        $agencyidx = $_POST["agencyidx"];
        $sendidx = $_POST["sendidx"];
        $promotion_text = encode_db_field($_POST["promotion_text"]);
        $contents_image_url = $_POST["contents_image_url"];
        $email_width = $_POST["email_width"];
        $target_type = $_POST["target_type"];
        $promotion_text =  str_replace('\\', '', $promotion_text);
        
        if ($eventidx == "" || $title == "" || $category == "" || $start_eventdate == "" || $end_eventdate == "")
            return "필수항목이 빠졌습니다.";
        
        check_number($reward_type.$reward_amount);
        check_number($reward_type.$reward_amount_mobile);
        check_xss($title.$event_imagepath);
        	
       	if ($share_enabled == "")
       		$share_enabled = "0";

    	$sql = "SELECT shorturl FROM tbl_event WHERE shorturl = '$shorturl'";
       	$duplicated_url = $db_main2->getvalue($sql);
       	
       	$sql = "SELECT shorturl FROM tbl_event WHERE eventidx = $eventidx AND shorturl = '$shorturl'";
       	$own_url = $db_main2->getvalue($sql);
       	
       	if($duplicated_url == $own_url)
       		$duplicated_url = "";
       	
       	if($duplicated_url != "")
       	{
       		return "중복된 Short URL 입니다.";
       	}
       	
       	$limit_total = 0;
       	
       	if($category == "4")
       		$limit_total = $total_coin;
        
        $reservation_date = $reservation_date." ".$reservation_hour.":".$reservation_minute;
        
        $sql = "UPDATE tbl_event SET title='$title',category='$category',reward_type='$reward_type',reward_amount='$reward_amount',reward_amount_mobile='$reward_amount_mobile',limit_join='$total_coin',limit_total='$limit_total',slotidx='$slotidx',shorturl='$shorturl',start_eventdate='$start_eventdate',end_eventdate='$end_eventdate', ".
        		"event_imagepath='$event_imagepath',reservation_date='$reservation_date',reservation_fanpage='$reservation_fanpage',reservation_imagepath='$reservation_imagepath',reservation_contents='$reservation_contents', ".
        		"share_enabled='$share_enabled' WHERE eventidx='$eventidx'";

        $db_main2->execute($sql);
        
        // Slot Free Spin
        if ($category == "6")
        {
        	$eventidx = $eventidx;
        
        	$startdate = "$start_eventdate 00:00:00";
        	$enddate = "$end_eventdate 23:59:59";
        	 
        	$sql = "INSERT INTO tbl_ultra_freespin_event(eventidx, slot_type, totalbet, freespin_cnt, startdate, enddate) ".
        			" VALUES('$eventidx','$slotidx','$slotbetcoin','$freespincnt', '$startdate', '$enddate') ".
        			" ON DUPLICATE KEY UPDATE slot_type= VALUES(slot_type),totalbet = VALUES(totalbet), freespin_cnt=VALUES(freespin_cnt), startdate=VALUES(startdate), enddate=VALUES(enddate);";
        	$db_main2->execute($sql);
        }
        
        // Email Retention
        if($category == "7" || $category == "8" || $category == "9")
        {
        	if($agencyidx == "1")
        		$agencyname = "SocketLabs";
        	else if($agencyidx == "2")
        		$agencyname = "SendPulse";
        	else if($agencyidx == "3")
        		$agencyname = "SES";
        
        	$sql = "UPDATE tbl_email_retention_setting SET eventidx = $eventidx, agencyidx = $agencyidx, agencyname = '$agencyname', sendidx = $sendidx, eventcode = '$eventcode', promotion_text = '$promotion_text', contents_image_url = '$contents_image_url', email_width = $email_width, shorturl = '$shorturl', target_type = $target_type WHERE eventidx = $eventidx;";
        	$db_main2->execute($sql);
        }
        
    }

    function game_update_shorturl()
    {
        global $db_main2;
        
        $eventidx = $_POST["eventidx"];
        
        if ($eventidx == "")
            return "필수항목이 빠졌습니다.";
        
        check_number($eventidx);
        
	    $data = $db_main2->getarray("SELECT eventcode,category,slotidx FROM tbl_event WHERE eventidx=$eventidx");

	    $eventcode = $data["eventcode"];
	    $category = $data["category"];
	    $slotidx = $data["slotidx"];
	    	    
        if ($eventcode == "")
            return "이벤트 정보를 찾을 수 없습니다.";
		
        $addparam = "";
        
        if($slotidx != "" && $slotidx > 0)
        	$addparam = "&slotidx=$slotidx";
        
        $url = FACEBOOK_CANVAS_URL."?eventcode=$eventcode".$addparam;
        
        $url .= "&dummy=".rand(0, 1000);
        
        $shorturl= getShortURL($url);
		
        $sql = "UPDATE tbl_event SET shorturl='$shorturl' WHERE eventidx=$eventidx";

        $db_main2->execute($sql);
    }
    
    function game_delete_event()
    {
        global $db_main2; 
        
        $eventidx = $_POST["eventidx"];
        $category = $_POST["category"];
        
        if ($eventidx == "")
            return "필수항목이 빠졌습니다.";
        
        check_number($eventidx);

        if($category == 6)
        {
        	$sql = "DELETE FROM tbl_event WHERE eventidx=$eventidx;\n".
            		"DELETE FROM tbl_event_result WHERE eventidx=$eventidx;\n".
        		"DELETE FROM tbl_ultra_freespin_event WHERE eventidx=$eventidx;\n";
        }
        else 
        {
        	$sql = "DELETE FROM tbl_event WHERE eventidx=$eventidx;\n".
        			"DELETE FROM tbl_event_result WHERE eventidx=$eventidx;\n";        
        }       
        
        $db_main2->execute($sql);
    } 
    
    function game_get_event_info()
    {
    	global $db_main2;
    
    	$category = $_POST["category"];
    
    	if ($category == "")
    		return "필수항목이 빠졌습니다.";
    
    	$sql = "SELECT '1' AS check_event,title,category,reward_type,reward_amount,event_imagepath,start_eventdate,end_eventdate,".
    			"reservation_date,reservation_fanpage,reservation_imagepath,reservation_contents FROM tbl_event WHERE category=$category ORDER BY writedate DESC LIMIT 1";

    	$event = $db_main2->getarray($sql);
    	
    	if ($event == null)
    		$event = array("check_event"=>"0");

    	return $event;
    }
    
    function game_send_comment_gift()
    {
    	global $db_main, $db_main2, $db_inbox, $db_analysis;
    	 
    	$articleid = $_POST["articleid"];
    	$userlist = $_POST["userlist"];
    	$commentlist = $_POST["commentlist"];
    	$reward_type = $_POST["reward_type"];
    	$reward_amount = $_POST["reward_amount"];
    	$memo = $_POST["memo"];
    	$message = $_POST["message"];
    	$admin_id = $_POST["adminid"];
    	 
    	if ($articleid == "" || $userlist == "" || $commentlist == "" || $reward_type == "" || $reward_amount == "" || $memo == "" || $message == "")
    		return "필수 항목이 빠졌습니다.";

    	check_number($coin);
    	 
    	$users = explode(",", $userlist);
    	$comments = explode(",", $commentlist);
    	 
    	for ($i=0; $i<sizeof($users); $i++)
    	{
    		$useridx = $users[$i];
    		$commentid = $comments[$i];
    	
    		$coin = 0;
    	
    		if($reward_type == "1")
    			$coin = $reward_amount;
    	
    		if ($coin < 0)
			{
				// 코인 줄일 때는 inbox로 안 주고 바로 차감
	            $sql = "INSERT INTO tbl_user_cache_update(useridx,coin,writedate) VALUES($useridx,'$coin',NOW());";
				$db_main2->execute($sql);
			}
			else
			{
				// 코인 발급 (inbox에 발급하는 것으로 변경)
	            $sql = "INSERT INTO tbl_user_inbox_".($useridx%20)."(useridx,category,coin,title,writedate) VALUES($useridx,101,'$coin','$message',NOW());";
	            $db_inbox->execute($sql);
			}
		
			// 관리자 발급 로그
			$sql = "INSERT INTO tbl_freecoin_admin_log(useridx, freecoin, reason, message, admin_id, writedate) ".
					"VALUES('$useridx', $coin, '$reason', '$message', '$admin_id', NOW())";
			$db_main->execute($sql);
			
			$sql = "UPDATE fanpage_article_comment SET gifttype=$reward_type, giftamount=giftamount+$coin WHERE articleid='$articleid' AND commentid='$commentid'";
			$db_analysis->execute($sql);
    	}
    }
    

    function game_change_ratio()
    {
    	global $db_main2, $db_analysis, $db_other, $login_adminid;
    		
    	$slottype = $_POST["slot"];
    	$current_ratio = $_POST["current_ratio"];
    	$update_ratio = $_POST["update_ratio"];
    	$category = $_POST["category"];
    	$ratio_type = $_POST["ratio_type"];

    	if($ratio_type == 1)
    	{
    		$total_ratio = $current_ratio + $update_ratio;
    		
    		$varidation_data = explode(".", $total_ratio);
    		$ratio_org = $varidation_data[0];
    		$variation_dev = $varidation_data[1];
    		
    		if($category == "" || $slottype == "" || $current_ratio == "" || $update_ratio == "" )
    			return "필수항목이 빠졌습니다.";
    		
    		if($category == "web")
    		{
	    		$db_table = "tbl_slot_profit_setting";
	    		$platform = 0;
    		}
    		else if($category == "ios")
    		{
    			$db_table = "tbl_slot_profit_setting_ios";
    			$platform = 1;
    		}
    		else if($category == "android")
    		{
    			$db_table = "tbl_slot_profit_setting_android";
    			$platform = 2;
    		}
    		else if($category == "amazon")
    		{
    			$db_table = "tbl_slot_profit_setting_amazon";
    			$platform = 3;
    		}
    		else if($category == "V20")
    		{
    			$db_table = "tbl_slot_profit_setting_v2";
    			$platform = 0;
    		}
    		
    		if($category == "all")
    		{
    			// iOS
    			if($variation_dev == "")
    			{
    				$sql = "UPDATE tbl_slot_profit_setting_ios SET ratio_org = $ratio_org, variation = 0, writedate = NOW() WHERE slottype = $slottype;";
    				$sql .= "UPDATE tbl_slot_profit_setting_android SET ratio_org = $ratio_org, variation = 0, writedate = NOW() WHERE slottype = $slottype;";
    				$sql .= "UPDATE tbl_slot_profit_setting_amazon SET ratio_org = $ratio_org, variation = 0, writedate = NOW() WHERE slottype = $slottype;";
    				$db_main2->execute($sql);
    			}
    			else
    			{
    				$sql = "UPDATE tbl_slot_profit_setting_ios SET ratio_org = $ratio_org, variation = 1, variation_dev = $variation_dev, writedate = NOW() WHERE slottype = $slottype;";
    				$sql .= "UPDATE tbl_slot_profit_setting_android SET ratio_org = $ratio_org, variation = 1, variation_dev = $variation_dev, writedate = NOW() WHERE slottype = $slottype;";
    				$sql .= "UPDATE tbl_slot_profit_setting_amazon SET ratio_org = $ratio_org, variation = 1, variation_dev = $variation_dev, writedate = NOW() WHERE slottype = $slottype;";
    				$db_main2->execute($sql);
    			}
    			 
    			$sql = "INSERT INTO tbl_slotratio_change_history(slottype, category, before_ratio, current_ratio, adminid, writedate) VALUES($slottype, 'ios', $current_ratio, $total_ratio, '$login_adminid', NOW());";    			
    			$sql .= "INSERT INTO tbl_slotratio_change_history(slottype, category, before_ratio, current_ratio, adminid, writedate) VALUES($slottype, 'android', $current_ratio, $total_ratio, '$login_adminid', NOW());";
    			$sql .= "INSERT INTO tbl_slotratio_change_history(slottype, category, before_ratio, current_ratio, adminid, writedate) VALUES($slottype, 'amazon', $current_ratio, $total_ratio, '$login_adminid', NOW());";
    			$db_analysis->execute($sql);
    			
    			$sql = "UPDATE tbl_slot_daily_check SET ratio_updatedate = NOW() WHERE slottype = $slottype AND platform = 1; ";
    			$sql .= "UPDATE tbl_slot_daily_check SET ratio_updatedate = NOW() WHERE slottype = $slottype AND platform = 2; ";
    			$sql .= "UPDATE tbl_slot_daily_check SET ratio_updatedate = NOW() WHERE slottype = $slottype AND platform = 3; ";
    			$db_other->execute($sql);
    		}
    		else if($category == "total")
    		{
    		    // iOS
    		    if($variation_dev == "")
    		    {
    		        $sql = "UPDATE tbl_slot_profit_setting SET ratio_org = $ratio_org, variation = 0, writedate = NOW() WHERE slottype = $slottype;";
    		        $sql .= "UPDATE tbl_slot_profit_setting_v2 SET ratio_org = $ratio_org, variation = 0, writedate = NOW() WHERE slottype = $slottype;";
    		        $sql .= "UPDATE tbl_slot_profit_setting_ios SET ratio_org = $ratio_org, variation = 0, writedate = NOW() WHERE slottype = $slottype;";
    		        $sql .= "UPDATE tbl_slot_profit_setting_android SET ratio_org = $ratio_org, variation = 0, writedate = NOW() WHERE slottype = $slottype;";
    		        $sql .= "UPDATE tbl_slot_profit_setting_amazon SET ratio_org = $ratio_org, variation = 0, writedate = NOW() WHERE slottype = $slottype;";
    		        $db_main2->execute($sql);
    		    }
    		    else
    		    {
    		        $sql = "UPDATE tbl_slot_profit_setting SET ratio_org = $ratio_org, variation = 1, variation_dev = $variation_dev, writedate = NOW() WHERE slottype = $slottype;";
    		        $sql .= "UPDATE tbl_slot_profit_setting_v2 SET ratio_org = $ratio_org, variation = 1, variation_dev = $variation_dev, writedate = NOW() WHERE slottype = $slottype;";
    		        $sql .= "UPDATE tbl_slot_profit_setting_ios SET ratio_org = $ratio_org, variation = 1, variation_dev = $variation_dev, writedate = NOW() WHERE slottype = $slottype;";
    		        $sql .= "UPDATE tbl_slot_profit_setting_android SET ratio_org = $ratio_org, variation = 1, variation_dev = $variation_dev, writedate = NOW() WHERE slottype = $slottype;";
    		        $sql .= "UPDATE tbl_slot_profit_setting_amazon SET ratio_org = $ratio_org, variation = 1, variation_dev = $variation_dev, writedate = NOW() WHERE slottype = $slottype;";
    		        $db_main2->execute($sql);
    		    }
    		    
    		    $sql = "INSERT INTO tbl_slotratio_change_history(slottype, category, before_ratio, current_ratio, adminid, writedate) VALUES($slottype, 'web', $current_ratio, $total_ratio, '$login_adminid', NOW());";
    		    $sql .= "INSERT INTO tbl_slotratio_change_history(slottype, category, before_ratio, current_ratio, adminid, writedate) VALUES($slottype, 'web2.0', $current_ratio, $total_ratio, '$login_adminid', NOW());";
    		    $sql .= "INSERT INTO tbl_slotratio_change_history(slottype, category, before_ratio, current_ratio, adminid, writedate) VALUES($slottype, 'ios', $current_ratio, $total_ratio, '$login_adminid', NOW());";
    		    $sql .= "INSERT INTO tbl_slotratio_change_history(slottype, category, before_ratio, current_ratio, adminid, writedate) VALUES($slottype, 'android', $current_ratio, $total_ratio, '$login_adminid', NOW());";
    		    $sql .= "INSERT INTO tbl_slotratio_change_history(slottype, category, before_ratio, current_ratio, adminid, writedate) VALUES($slottype, 'amazon', $current_ratio, $total_ratio, '$login_adminid', NOW());";
    		    $db_analysis->execute($sql);
    		    
    		    $sql = " UPDATE tbl_slot_daily_check SET ratio_updatedate = NOW() WHERE slottype = $slottype AND platform = 0;";
    		    $sql .= " UPDATE tbl_slot_daily_check SET ratio_updatedate = NOW() WHERE slottype = $slottype AND platform = 1;";
    		    $sql .= " UPDATE tbl_slot_daily_check SET ratio_updatedate = NOW() WHERE slottype = $slottype AND platform = 2;";
    		    $sql .= " UPDATE tbl_slot_daily_check SET ratio_updatedate = NOW() WHERE slottype = $slottype AND platform = 3;";
    		    $db_other->execute($sql);
    		}
    		else 
    		{    		
	    		if($variation_dev == "")
	    			$sql = "UPDATE ".$db_table." SET ratio_org = $ratio_org, variation = 0, writedate = NOW() WHERE slottype = $slottype";
	    		else
	    			$sql = "UPDATE ".$db_table." SET ratio_org = $ratio_org, variation = 1, variation_dev = $variation_dev, writedate = NOW() WHERE slottype = $slottype";
	    
	    		$db_main2->execute($sql);
	    		
	    		if($category == "web")
	    		{
	    		    if($variation_dev == "")
	    		        $sql = "UPDATE tbl_slot_profit_setting_v2 SET ratio_org = $ratio_org, variation = 0, writedate = NOW() WHERE slottype = $slottype";
    		        else
    		            $sql = "UPDATE tbl_slot_profit_setting_v2 SET ratio_org = $ratio_org, variation = 1, variation_dev = $variation_dev, writedate = NOW() WHERE slottype = $slottype";
    		            
	    		}
	    		$db_main2->execute($sql);
	    		    
	    		
	    		$sql = "INSERT INTO tbl_slotratio_change_history(slottype, category, before_ratio, current_ratio, adminid, writedate) VALUES($slottype, '$category', $current_ratio, $total_ratio, '$login_adminid', NOW())";
	    		$db_analysis->execute($sql);
	    		
	    		$sql = "UPDATE tbl_slot_daily_check SET ratio_updatedate = NOW() WHERE slottype = $slottype AND platform = $platform;";
	    		$db_other->execute($sql);
    		}    		
    	}
    	else if($ratio_type == 3)
    	{
    		$total_ratio = $current_ratio + $update_ratio;
    		
    		if($category == "" || $slottype == "" || $current_ratio == "" || $update_ratio == "" )
    			return "필수항목이 빠졌습니다.";
    		
    		if($category == "web")
    		{
    			$db_table = "tbl_slot_profit_setting";
    			$platform = 0;
    		}
    		else if($category == "ios")
    		{
    			$db_table = "tbl_slot_profit_setting_ios";
    			$platform = 1;
    		}
    		else if($category == "android")
    		{
    			$db_table = "tbl_slot_profit_setting_android";
    			$platform = 2;
    		}
    		else if($category == "amazon")
    		{
    			$db_table = "tbl_slot_profit_setting_amazon";
    			$platform = 3;
    		}
    		
   			$sql = "UPDATE ".$db_table." SET ratio3 = $total_ratio, writedate = NOW() WHERE slottype = $slottype";
    		
    		$db_main2->execute($sql);
    		
    		$sql = "INSERT INTO tbl_slotratio3_change_history(slottype, category, before_ratio, current_ratio, adminid, writedate) VALUES($slottype, '$category', $current_ratio, $total_ratio, '$login_adminid', NOW())";
    		$db_analysis->execute($sql);
    		
    		$sql = "UPDATE tbl_slot_daily_check SET ratio3_updatedate = NOW() WHERE slottype = $slottype AND platform = $platform;";
    		$db_other->execute($sql);
    	}
    }
    
    function game_get_select_slot()
    {
    	global $db_main2;
    
    	$slottype = $_POST["slottype"];
    	$category = $_POST["category"];
    	$ratio_type = $_POST["ratio_type"];
    	
    	if($category == "all")
    		$category = "ios";
    	 
    	if($category == "" || $slottype == "" || $ratio_type == "")
    		return "필수항목이 빠졌습니다.";
    	 
    	if($ratio_type == 1)
    	{
    	    if($category == "web" || $category == "total")
	    		$db_table = "tbl_slot_profit_setting";
	    	else if($category == "ios")
	    		$db_table = "tbl_slot_profit_setting_ios";
	    	else if($category == "android")
	    		$db_table = "tbl_slot_profit_setting_android";
	    	else if($category == "amazon")
	    		$db_table = "tbl_slot_profit_setting_amazon";
	    	else if($category == "V20")
	    	    $db_table = "tbl_slot_profit_setting_v2";
	    	 
	    	$sql = "SELECT slottype, ratio_org, variation, variation_dev, writedate FROM ".$db_table." WHERE slottype = $slottype";    	
	    
	    	$slot_ratio = $db_main2->getarray($sql);
    	}
    	else if($ratio_type == 3)
    	{
    		if($category == "web")
    			$db_table = "tbl_slot_profit_setting";
    		else if($category == "ios")
    			$db_table = "tbl_slot_profit_setting_ios";
    		else if($category == "android")
    			$db_table = "tbl_slot_profit_setting_android";
    		else if($category == "amazon")
    			$db_table = "tbl_slot_profit_setting_amazon";
    		 
    		$sql = "SELECT slottype, ratio3 AS ratio_org, variation, variation_dev, writedate FROM ".$db_table." WHERE slottype = $slottype AND special_value > 0;";
    		 
    		$slot_ratio = $db_main2->getarray($sql);
    	}
    	 
    	return $slot_ratio;
    }
    
    function game_get_ratio_history()
    {
    	global $db_analysis;
    	 
    	$slottype = $_POST["slottype"];
    	$category = $_POST["category"];
    	$ratio_type = $_POST["ratio_type"];
    	
    	if($category == "all")
    		$category = "ios";
    	
		if($category == "total")
		    $category = "web";
    	 
    	if($category == "" || $slottype == "" || $ratio_type == "")
    		return "필수항목이 빠졌습니다.";
    	 
    	if($ratio_type == 1)
    	{
    		$sql = "SELECT * FROM tbl_slotratio_change_history WHERE slottype = $slottype AND category = '$category' ORDER BY writedate DESC";
    	}
    	else if($ratio_type == 3)
    	{
    		$sql = "SELECT * FROM tbl_slotratio3_change_history WHERE slottype = $slottype AND category = '$category' ORDER BY writedate DESC";
    	}
    	 
    	return $db_analysis->gettotallist($sql);
    }

	function game_update_betrange()
	{
		global $db_game;

		$slotidx = $_POST["slotidx"];
		$roomtype = $_POST["roomtype"];
		$linecount = $_POST["linecount"];
		$totalbet = $_POST["totalbet"];

		if ($slotidx == "" || $roomtype == "" || $linecount == "" || $totalbet == "")
			return "필수항목이 빠졌습니다.";

		if ($roomtype != "1" && $roomtype != "2")
			return "올바르지 않은 roomtype 입니다.";

		$bet1 = intval($totalbet / $linecount);

		if ($roomtype == "1") {
			$sql = "UPDATE tbl_normal_bet_info ".
				"SET bet1 = $bet1, bet2 = $bet1 * 2, bet3 = $bet1 * 3, bet4 = $bet1 * 4, bet5 = $bet1 * 5, ".
				"bet6 = $bet1 * 6, bet7 = $bet1 * 7, bet8 = $bet1 * 8, bet9 = $bet1 * 9, bet10 = $bet1 * 10 ".
				"WHERE slotidx = $slotidx";
		} else {
			$sql = "UPDATE tbl_highroller_bet_info ".
				"SET bet1 = $bet1, bet2 = $bet1 * 2, bet3 = $bet1 * 5, bet4 = $bet1 * 10, bet5 = $bet1 * 20 ".
				"WHERE slotidx = $slotidx";
		}

		$db_game->execute($sql);
	}
	
	function game_get_fb_adset_info()
	{
		global $db_main2;
		 
		$campaign_id = $_POST["campaign_id"];
		 
		if($campaign_id == "" )
			return "필수항목이 빠졌습니다.";
	
		$sql = "SELECT adset_id, adset_name FROM `tbl_ad_stats` WHERE campaign_id = '$campaign_id' GROUP BY adset_id";		
	
		return $db_main2->gettotallist($sql);
	}
	
	function game_insert_and_push()
	{
		global $db_mobile;
	
		$reservation_date = $_POST["reservation_date"];
		$reservation_hour = $_POST["reservation_hour"];
		$reservation_minute = $_POST["reservation_minute"];
		$message = $_POST["message"];
		$title = $_POST["title"];
		$enabled = $_POST["reservation_enabled"];
		$eventcode = $_POST["eventcode"];
		$pushtype = $_POST["pushtype"];
		$os_type = $_POST["os_type"];
		$image_url = $_POST["image_url"];
		$image_url_short = $_POST["image_url_short"];
		$text_visible = $_POST["text_visible"];
		$image_url2 = ($_POST["image_url2"]=="")?$_POST["image_url"]:$_POST["image_url2"];
	
		$reservation_time = $reservation_date." ".$reservation_hour.":".$reservation_minute.":00";
	
		if ($reservation_date == "" || $message == "" || $enabled == "")
			return "필수항목이 빠졌습니다.";
	
		if($pushtype != null){
			if ($pushtype == 10 && eventcode == "")
				return "이벤트 코드를 입력해주세요.";
		}
		 
	
		$sql = "INSERT INTO tbl_message(title, message, os_type, sent, enabled, reservedate, push_code, push_type, image_url, image_url2, image_url_short, text_visible, is_fcm) VALUES ('$title','$message', $os_type, 0, $enabled, '$reservation_time','$eventcode',$pushtype, '$image_url', '$image_url2', '$image_url_short','$text_visible',1);";
	
		$db_mobile->execute($sql);
	}
	
	function game_update_and_push()
	{
		global $db_mobile;
	
		$mesgidx = $_POST["mesgidx"];
		$reservation_date = $_POST["reservation_date"];
		$reservation_hour = $_POST["reservation_hour"];
		$reservation_minute = $_POST["reservation_minute"];
		$message = $_POST["message"];
		$title = $_POST["title"];
		$enabled = $_POST["reservation_enabled"];
		$eventcode = $_POST["eventcode"];
		$pushtype = $_POST["pushtype"];
		$os_type = $_POST["os_type"];
		$image_url = $_POST["image_url"];
		$image_url_short = $_POST["image_url_short"];
		$text_visible = $_POST["text_visible"];
		$image_url2 = ($_POST["image_url2"]=="")?$_POST["image_url"]:$_POST["image_url2"];
	
		$reservation_time = $reservation_date." ".$reservation_hour.":".$reservation_minute.":00";
	
		if ($reservation_date == "" || $message == "" || $enabled == "" || $mesgidx == "")
			return "필수항목이 빠졌습니다.";
	
		if($pushtype != null){
			if ($pushtype == 10 && eventcode == "")
				return "이벤트 코드를 입력해주세요.";
		}
	
		$sql = "UPDATE tbl_message SET title = '$title', message = '$message', enabled = $enabled, reservedate = '$reservation_time', push_code = '$eventcode', push_type = $pushtype, os_type = $os_type, image_url = '$image_url' , image_url2 = '$image_url2', text_visible = '$text_visible', image_url_short = '$image_url_short'  WHERE mesgidx = $mesgidx";
	
		$db_mobile->execute($sql);
	}
	
	function game_delete_and_push()
	{
		global $db_mobile;
	
		$mesgidx = $_POST["mesgidx"];
	
		if($mesgidx == "")
			return "존재하지 않은 push 입니다";
	
		$sql = "DELETE FROM tbl_message WHERE mesgidx = $mesgidx";
	
		$db_mobile->execute($sql);
	}
	
	function game_insert_mobile_push_event()
	{
		global $db_main2;
		
		$os_type = $_POST["os_type"];
	
		$reservation_date = $_POST["reservation_date"];
		$reservation_hour = $_POST["reservation_hour"];
		$reservation_minute = $_POST["reservation_minute"];
		$enabled = $_POST["reservation_enabled"];
		$message = $_POST["message"];
		$title = $_POST["title"];
		$reward_type = $_POST["reward_type"];
		$reward_amount = ($_POST["reward_amount"]=="")? 0 : $_POST["reward_amount"];
		$limit_join = $_POST["limit_join"];		
		$push_code = $_POST["push_code"];
		$push_type = $_POST["push_type"];		
		$image_url = $_POST["image_url"];
		$image_url2 = ($_POST["image_url2"]=="")? $_POST["image_url"] : $_POST["image_url2"];
		
		$start_eventdate = $_POST["start_eventdate"];
		$end_eventdate = $_POST["end_eventdate"];
		$start_eventdate_hour = $_POST["start_eventdate_hour"];
		$start_eventdate_minute = $_POST["start_eventdate_minute"];
		$end_eventdate_hour = $_POST["end_eventdate_hour"];
		$end_eventdate_minute = $_POST["end_eventdate_minute"];
		
		$slotidx = $_POST["slotidx"];
		$slotbetcoin = $_POST["slotbetcoin"];
		$freespincnt = $_POST["freespincnt"];
		$image_url_short = $_POST["image_url_short"];
		$text_visible = $_POST["text_visible"];

		$reservation_time = $reservation_date." ".$reservation_hour.":".$reservation_minute.":00";
		
		$start_eventdate = $start_eventdate." ".$start_eventdate_hour.":".$start_eventdate_minute.":00";
		
		$end_eventdate = $end_eventdate." ".$end_eventdate_hour.":".$end_eventdate_minute.":00";

		if($reward_type == 3)
		{
			$end_eventdate = date("Y-m-d H:i:s", (strtotime("$reservation_time")+10800));
		}
		
	
		if ($reservation_date == "" || $message == "" || $enabled == "")
			return "필수항목이 빠졌습니다.";
	
		if ($push_code == "")
			return "이벤트 코드를 입력해주세요.";	

		if ($reward_type == "")
			$reward_type = 0;
		
		if ($limit_join == "")
			$limit_join = 0;
			
		$push_event_cnt = 0;
		$push_event_cnt = $db_main2->getvalue("SELECT COUNT(*) AS cnt FROM tbl_push_event WHERE DATE_FORMAT(reservedate,'%Y-%m-%d') = '$reservation_date' AND os_type = $os_type");
		
		if($push_event_cnt == 1)
		    $enabled = 2;
	    else if ($push_event_cnt == 2)
	        $enabled = 3;
	    
		$eventidx_sql = "SELECT MAX(eventidx)+1 AS new_eventidx ".
				" FROM ".
				" ( ".
				"	SELECT MAX(eventidx) AS eventidx FROM `tbl_ultra_freespin_event` ".
				"	UNION ALL  ".
				"	SELECT MAX(eventidx) AS eventidx FROM `tbl_event` ".
				"	UNION ALL  ".
				"	SELECT MAX(eventidx) AS eventidx FROM `tbl_push_event` ".
				" ) t1";
		$new_eventidx = $db_main2->getvalue($eventidx_sql);
			
		$sql = "INSERT INTO tbl_push_event(eventidx, os_type, title, message, reward_type, reward_amount, image_url, image_url2, image_url_short, text_visible, limit_join, push_type,  push_code, sent, enabled, start_eventdate, end_eventdate, reservedate, writedate) ".
				"VALUES ($new_eventidx, $os_type, '$title', '$message', $reward_type, $reward_amount, '$image_url', '$image_url2','$image_url_short', '$text_visible', $limit_join, $push_type, '$push_code',  0, $enabled, '$start_eventdate', '$end_eventdate', '$reservation_time',NOW())";	
		$db_main2->execute($sql);
		
		$eventidx = $new_eventidx;
		
		// Slot Free Spin
		if ($reward_type == "14")
		{
			$startdate = "$start_eventdate 00:00:00";
			$enddate = "$end_eventdate 23:59:59";
		
			$sql = "INSERT INTO tbl_ultra_freespin_event(eventidx, slot_type, totalbet, freespin_cnt, startdate, enddate, ispush) VALUES('$eventidx','$slotidx','$slotbetcoin','$freespincnt', '$startdate', '$enddate', 1);";
			$db_main2->execute($sql);
		}
		
	}
	
	function game_update_mobile_push_event()
	{
		global $db_main2;
	
		$eventidx = $_POST["eventidx"];
		$os_type = $_POST["os_type"];
		$reservation_date = $_POST["reservation_date"];
		$reservation_hour = $_POST["reservation_hour"];
		$reservation_minute = $_POST["reservation_minute"];
		$enabled = $_POST["reservation_enabled"];
		$message = $_POST["message"];
		$title = $_POST["title"];
		$reward_type = $_POST["reward_type"];
		$reward_amount = ($_POST["reward_amount"]=="")? 0 : $_POST["reward_amount"];
		$limit_join = $_POST["limit_join"];		
		$push_code = $_POST["push_code"];
		$push_type = $_POST["push_type"];		
		$image_url = $_POST["image_url"];
		$image_url2 = ($_POST["image_url2"]=="")? $_POST["image_url"] : $_POST["image_url2"];
		$start_eventdate = $_POST["start_eventdate"];
		$end_eventdate = $_POST["end_eventdate"];
		$start_eventdate_hour = $_POST["start_eventdate_hour"];
		$start_eventdate_minute = $_POST["start_eventdate_minute"];
		$end_eventdate_hour = $_POST["end_eventdate_hour"];
		$end_eventdate_minute = $_POST["end_eventdate_minute"];
		$image_url_short = $_POST["image_url_short"];
		$text_visible = $_POST["text_visible"];
		
		$slotidx = $_POST["slotidx"];
		$slotbetcoin = $_POST["slotbetcoin"];
		$freespincnt = $_POST["freespincnt"];

		$reservation_time = $reservation_date." ".$reservation_hour.":".$reservation_minute.":00";
		
		$start_eventdate = $start_eventdate." ".$start_eventdate_hour.":".$start_eventdate_minute.":00";
		
		$end_eventdate = $end_eventdate." ".$end_eventdate_hour.":".$end_eventdate_minute.":00";
		
		if ($reservation_date == "" || $message == "" || $enabled == "")
			return "필수항목이 빠졌습니다.";
	
		if ($push_code == "")
			return "이벤트 코드를 입력해주세요.";	
		
		if ($reward_type == "")
			$reward_type = 0;
		
		if ($limit_join == "")
			$limit_join = 0;
			
		$push_event_cnt = 0;
		$push_event_cnt = $db_main2->getvalue("SELECT COUNT(*) AS cnt FROM tbl_push_event WHERE DATE_FORMAT(reservedate,'%Y-%m-%d') = '$reservation_date' AND eventidx!=$eventidx");
			
		if($push_event_cnt == 1)
		    $enabled = 2;
	    else if ($push_event_cnt == 2)
	        $enabled = 3;

		$sql = "UPDATE tbl_push_event SET os_type = $os_type, title = '$title', message = '$message', reward_type = $reward_type, reward_amount = $reward_amount, limit_join = $limit_join, image_url = '$image_url', image_url2 = '$image_url2' , image_url_short = '$image_url_short' , text_visible = '$text_visible' ,   ".
			"push_type = $push_type, push_code = '$push_code', start_eventdate = '$start_eventdate', end_eventdate = '$end_eventdate', reservedate = '$reservation_time' WHERE eventidx = $eventidx";
		$db_main2->execute($sql);
		
		// Slot Free Spin
		if ($reward_type == "14")
		{
			$eventidx = $eventidx;
		
			$startdate = "$start_eventdate 00:00:00";
			$enddate = "$end_eventdate 23:59:59";
		
			$sql = "INSERT INTO tbl_ultra_freespin_event(eventidx, slot_type, totalbet, freespin_cnt, startdate, enddate, ispush) ".
					" VALUES('$eventidx','$slotidx','$slotbetcoin','$freespincnt', '$startdate', '$enddate',1) ".
					" ON DUPLICATE KEY UPDATE slot_type= VALUES(slot_type),totalbet = VALUES(totalbet), freespin_cnt=VALUES(freespin_cnt), startdate=VALUES(startdate), enddate=VALUES(enddate);";
			$db_main2->execute($sql);
		}
		
	}
	
	function game_delete_mobile_push_event()
	{
		global $db_main2;
	
		$eventidx = $_POST["eventidx"];
	
		if($eventidx == "")
			return "존재하지 않은  push Event 입니다";
		
		if ($eventidx == "")
			return "필수항목이 빠졌습니다.";
		
		check_number($eventidx);
	
		$reward_type = $db_main2->getvalue("SELECT reward_type FROM tbl_push_event WHERE eventidx = $eventidx");
		
		if($reward_type == 14)
		{
			$sql = "DELETE FROM tbl_push_event WHERE eventidx=$eventidx;\n".
					"DELETE FROM tbl_ultra_freespin_event WHERE eventidx=$eventidx;\n";
		}
		else
			$sql = "DELETE FROM tbl_push_event WHERE eventidx = $eventidx";
	
		$db_main2->execute($sql);
	}

	  function game_insert_cross_promotion()
    {
    	global $db_main2;
    	
    	$game_type = $_POST["game_type"];
    	$title = $_POST["title"];
    	$enable = $_POST["enable"];
    	$reward_amount = $_POST["reward_amount"];
    	$logo_path = $_POST["logo_path"];
		$title_path = $_POST["title_path"];
    	$popup_path = $_POST["popup_path"];
	
    	if ($game_type == "" || $title == "" || $enable == "" || $reward_amount == "" || $logo_path =="" || $title_path == ""|| $popup_path == "")
    		return "필수항목이 빠졌습니다.";
    
   		check_number($game_type);
   		check_number($enable);
   		check_number($reward_amount);
   		
   		check_xss($logo_path);
   		check_xss($title_path);
   		check_xss($popup_path);
   	
		$sql = "INSERT INTO tbl_cross_promotion_event(game_type,title,enable,reward_amount,logo_path,title_path,popup_path,writedate)".
				" VALUES($game_type,'$title',$enable,$reward_amount,'$logo_path','$title_path','$popup_path',now())";
   		$db_main2->execute($sql);
   
    
    }
    
    function game_update_cross_promotion()
    {
    	global $db_main2;
    
    	$eventidx = $_POST["eventidx"];
    	$game_type = $_POST["game_type"];
    	$title = $_POST["title"];
    	$enable = $_POST["enable"];
    	$reward_amount = $_POST["reward_amount"];
    	$logo_path = $_POST["logo_path"];
		$title_path = $_POST["title_path"];
    	$popup_path = $_POST["popup_path"];
	
    	if ($eventidx == "" || $game_type == "" || $title == "" || $enable == "" || $reward_amount == "" || $logo_path =="" || $title_path == ""|| $popup_path == "")
    		return "필수항목이 빠졌습니다.";
    
   		check_number($eventidx);
   		check_number($game_type);
   		check_number($enable);
   		check_number($reward_amount);
   		
   		check_xss($logo_path);
   		check_xss($title_path);
   		check_xss($popup_path);
   		
		$sql = "UPDATE tbl_cross_promotion_event SET game_type=$game_type, title='$title', enable=$enable, reward_amount=$reward_amount, logo_path='$logo_path',title_path='$title_path',popup_path='$popup_path' WHERE eventidx='$eventidx'";
    
    	$db_main2->execute($sql);
    
    }
    
    function game_delete_cross_promotion()
    {
    	global $db_main2;
    
    	$eventidx = $_POST["eventidx"];
    
    	if ($eventidx == "")
    		return "필수항목이 빠졌습니다.";
    
    		check_number($eventidx );
    
   			$sql = "DELETE FROM tbl_cross_promotion_event WHERE eventidx = $eventidx;";
 			$db_main2->execute($sql);
    }
    
    function game_fiesta_change_ratio()
    {
    	global $db_main2, $db_analysis, $db_other, $login_adminid;
    
    	$total_mode = $_POST["total_mode"];
    	$change_mode = $_POST["change_mode"];
    	$ratio_val = $_POST["ratio_val"];
    	$admin_id = $_POST["adminid"];
    	$slottype = $_POST["slottype"];
    	
    	if($total_mode == "" || $change_mode == "" || $ratio_val == "" )
    		return "필수항목이 빠졌습니다.";
    	
    	$tail = "";
    	$slottype_tail = "";
    	$update_sql = "";
    	
    	if($slottype!="")
    		$table = "tbl_jackpot_fiesta_slot_jackpot_ratio";
    	else 
    		$table = "tbl_slot_jackpot_ratio";
    	
    	if($slottype != "ALL" && $slottype != "")
    		$slottype_tail = "AND slottype = $slottype";
    				
	if($total_mode == 1)
			$tail ="WHERE betlevel >=0 AND betlevel <=9 $slottype_tail ";
    	else if($total_mode == 2)
    		$tail ="WHERE betlevel >=10 AND betlevel <=19 $slottype_tail ";
    	else 
    		$tail ="WHERE 1=1 $slottype_tail ";
    	
    	if($change_mode==0)
    		$update_sql = "UPDATE $table SET fiesta_jackpot_ratio = ROUND(fiesta_jackpot_ratio*1.$ratio_val) $tail ";
    	else if($change_mode==1)
    		$update_sql = "UPDATE $table SET fiesta_jackpot_ratio = ROUND(fiesta_jackpot_ratio*0.".(100-$ratio_val).") $tail ";
    	
   		$db_main2->execute($update_sql);
   		
    	$history_sql=" INSERT INTO fiesta_change_ratio_history(adminid, total_mode, change_mode, ratio_val, update_sql, slottype, writedate) ". 
    				 " VALUES('$admin_id','$total_mode','$change_mode','$ratio_val','$update_sql', '$slottype' ,now())";
    	$db_main2->execute($history_sql);
    }
    
    function game_insert_notice_popup()
    {
        global $db_main2;
        
        $message = $_POST["message"];
        $platform = $_POST["platform"];
        
        $startdate_day = $_POST["startdate_day"];
        $startdate_hour = $_POST["startdate_hour"];
        $startdate_min = $_POST["startdate_min"];
        $enddate_day = $_POST["enddate_day"];
        $enddate_hour = $_POST["enddate_hour"];
        $enddate_min = $_POST["enddate_min"];
        $enable = $_POST["enable"];
        
        $startdate = $startdate_day." ".$startdate_hour.":".$startdate_min.":00";
        $enddate = $enddate_day." ".$enddate_hour.":".$enddate_min.":00";
        
        if ($message == "" || $platform == "" || $startdate == "" || $enddate == "" || $enddate == "")
            return "필수항목이 빠졌습니다.";
        
        $sql = "INSERT INTO tbl_notice_popup_info(platform, enable, message, startdate, enddate)".
            " VALUES($platform, $enable,'$message','$startdate','$enddate')";
        $db_main2->execute($sql);
    }
    
    function game_update_notice_popup()
    {
        global $db_main2;
        
        $mesgidx = $_POST["mesgidx"];
        $message = $_POST["message"];
        $platform = $_POST["platform"];
        
        $startdate_day = $_POST["startdate_day"];
        $startdate_hour = $_POST["startdate_hour"];
        $startdate_min = $_POST["startdate_min"];
        $enddate_day = $_POST["enddate_day"];
        $enddate_hour = $_POST["enddate_hour"];
        $enddate_min = $_POST["enddate_min"];
        $enable = $_POST["enable"];
        
        $startdate = $startdate_day." ".$startdate_hour.":".$startdate_min.":00";
        $enddate = $enddate_day." ".$enddate_hour.":".$enddate_min.":00";
        
        
        
        if ($mesgidx == "" || $message == "" || $platform == "" || $startdate == "" || $enddate == "" || $enddate == "")
            return "필수항목이 빠졌습니다.";

            $sql = "UPDATE tbl_notice_popup_info SET message='$message',platform='$platform',startdate='$startdate',enddate='$enddate',enable='$enable' WHERE mesgidx='$mesgidx'";
        $db_main2->execute($sql);
    }
    
    function game_delete_notice_popup()
    {
        global $db_main2;
        
        $mesgidx = $_POST["mesgidx"];
        
        if ($mesgidx == "")
            return "필수항목이 빠졌습니다.";
            
        check_number($eventidx);
            
        $sql = "DELETE FROM tbl_notice_popup_info WHERE mesgidx=$mesgidx;";
        $db_main2->execute($sql);
    }
    
    function game_update_chat_block()
    {
        global $db_main2;
        
        $useridx = $_POST["useridx"];
        
        if ($useridx == "")
            return "필수항목이 빠졌습니다.";
            
        check_number($useridx);
            
        $sql = "UPDATE tbl_user_profile_block SET is_block = 0, report_count = 0 WHERE useridx = $useridx";
        $db_main2->execute($sql);
    }

    function game_insert_regular_push()
    {
        global $db_mobile;
        
        $group_no = $_POST["group_no"];
        $message = $_POST["message"];
        $title = $_POST["title"];
        $and_imgpath = $_POST["and_imgpath"];
        $ios_imgpath = $_POST["ios_imgpath"];
        $and_banner = $_POST["and_banner"];
        $text_visible = $_POST["text_visible"];
        
        if ($group_no == "" || $message == "" || $title == "" || $text_visible == "")
            return "필수항목이 빠졌습니다.";
            
            $sql = " INSERT INTO tbl_push_imgpath(group_no, message, title, and_imgpath, ios_imgpath, and_banner, text_visible) ". 
                   " VALUES ('$group_no','$title','$message', '$and_imgpath','$ios_imgpath','$and_banner','$text_visible');";
            $db_mobile->execute($sql);
    }
    
    function game_update_regular_push()
    {
        global $db_mobile;
        
        $group_no = $_POST["group_no"];
        $message = $_POST["message"];
        $title = $_POST["title"];
        $and_imgpath = $_POST["and_imgpath"];
        $ios_imgpath = $_POST["ios_imgpath"];
        $and_banner = $_POST["and_banner"];
        $text_visible = $_POST["text_visible"];
        
        $reservation_time = $reservation_date." ".$reservation_hour.":".$reservation_minute.":00";
        
        if ($group_no == "" || $message == "" || $title == "" || $text_visible == "")
            return "필수항목이 빠졌습니다.";
            
            $sql = "UPDATE tbl_push_imgpath SET title = '$title', message = '$message', and_imgpath = '$and_imgpath', ios_imgpath = '$ios_imgpath', and_banner = '$and_banner', text_visible = '$text_visible'  WHERE group_no = $group_no";
            
        $db_mobile->execute($sql);
    }
    
    function game_delete_regular_push()
    {
        global $db_mobile;
        
        $group_no = $_POST["group_no"];
        
        if($group_no == "")
            return "존재하지 않은 그룹 입니다";
            
            $sql = "DELETE FROM tbl_push_imgpath WHERE mesgidx = $mesgidx";
            
            $db_mobile->execute($sql);
    }
    
    function game_insert_notice_lobby()
    {
        global $db_main2;
        
        $message = $_POST["message"];
        $groups = $_POST["groups"];
        $enable = $_POST["enable"];
        
        if ($message == "" || $groups == "" )
            return "필수항목이 빠졌습니다.";
            
            $sql = "INSERT INTO tbl_notice_lobby_info(groups, enabled, message)".
                " VALUES($groups, $enable,'$message')";
            $db_main2->execute($sql);
    }
    
    function game_update_notice_lobby()
    {
        global $db_main2;
        
        $mesgidx = $_POST["mesgidx"];
        $message = $_POST["message"];
        $groups = $_POST["groups"];
        $enable = $_POST["enable"];
        
        if ($mesgidx == "" || $message == "" || $groups == "" )
            return "필수항목이 빠졌습니다.";
            
            $sql = "UPDATE tbl_notice_lobby_info SET message='$message',enabled='$enable',groups=$groups WHERE mesgidx='$mesgidx'";
            $db_main2->execute($sql);
    }
    
    function game_delete_notice_lobby()
    {
        global $db_main2;
        
        $mesgidx = $_POST["mesgidx"];
        
        if ($mesgidx == "")
            return "필수항목이 빠졌습니다.";
            
            check_number($eventidx);
            
            $sql = "DELETE FROM tbl_notice_lobby_info WHERE mesgidx=$mesgidx;";
            $db_main2->execute($sql);
    }
?>
