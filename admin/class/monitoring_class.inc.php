<?
	function monitoring_update_coin_increase_user_status()
    {
    	global $db_analysis;
    	 
    	$sql = "";
    	 
    	$logidx_list = explode(",", $_POST["logidx_list"]);
    	 
    	for($i=0; $i<sizeof($logidx_list); $i++)
    		$sql .= "UPDATE tbl_coin_increase_user_list SET status = '3' WHERE logidx = '".$logidx_list[$i]."';";
    
    	$db_analysis->execute($sql);
    }
	
	function monitoring_update_coin_increase_user()
	{
		global $db_analysis;
	
		$logidx = $_POST["logidx"];
		$status = $_POST["status"];
		$comment = $_POST["comment"];
		 
		$prev_comment = $db_analysis->getvalue("SELECT comment FROM tbl_coin_increase_user_list WHERE logidx = '$logidx'");
		 
		if($logidx == "" || $status == "")
			return "필수항목이 빠졌습니다.";
	
		if($prev_comment == $comment)
			$sql ="UPDATE tbl_coin_increase_user_list SET status = '$status', comment = '$comment' WHERE logidx = '$logidx'";
		else
			$sql ="UPDATE tbl_coin_increase_user_list SET status = '$status', comment = '$comment', commentdate = NOW() WHERE logidx = '$logidx'";
	
		$db_analysis->execute($sql);
	}
	
	function monitoring_update_coin_increase_user_new()
	{
		global $db_other;
	
		$logidx = $_POST["logidx"];
		$status = $_POST["status"];
		$comment = $_POST["comment"];
			
		$prev_comment = $db_other->getvalue("SELECT comment FROM tbl_coin_increase_user_list_new WHERE logidx = '$logidx'");
			
		if($logidx == "" || $status == "")
			return "필수항목이 빠졌습니다.";
	
		if($prev_comment == $comment)
			$sql ="UPDATE tbl_coin_increase_user_list_new SET status = '$status', comment = '$comment' WHERE logidx = '$logidx'";
		else
			$sql ="UPDATE tbl_coin_increase_user_list_new SET status = '$status', comment = '$comment', commentdate = NOW() WHERE logidx = '$logidx'";
	
		$db_other->execute($sql);
	}
	
	function monitoring_update_unknowncoin_user_status()
	{
		global $db_analysis;
		 
		$sql = "";
		$status = $_POST["status"];
		$logidx_list = explode(",", $_POST["logidx_list"]);
		 
		for($i=0; $i<sizeof($logidx_list); $i++)
		{
			if($status == 3)
				$sql .= "UPDATE tbl_unknowncoin_daily SET status = '$status' WHERE logidx = '".$logidx_list[$i]."';";
			else
			{
				$sql .= "INSERT INTO tbl_unknowncoin_check_daily(useridx, coin, unknown_coin, unknown_coin_log, unknown_coin_total, writedate, status, comment, commentdate) SELECT useridx, coin, unknown_coin, unknown_coin_log, unknown_coin_total, writedate, status, comment, commentdate FROM tbl_unknowncoin_daily WHERE STATUS = 1 AND logidx = '$logidx_list[$i]';";
				$sql .= "DELETE FROM tbl_unknowncoin_daily WHERE STATUS = 1 AND logidx = '$logidx_list[$i]';";
			}
		}
			 
		$db_analysis->execute($sql);
	}
	
	function monitoring_update_unknowncoin_user()
	{
		global $db_analysis;
	
		$logidx = $_POST["logidx"];
		$status = $_POST["status"];
		$comment = $_POST["comment"];
	
		$prev_comment = $db_analysis->getvalue("SELECT comment FROM tbl_unknowncoin_daily WHERE logidx = '$logidx'");
	
		if($logidx == "" || $status == "")
			return "필수항목이 빠졌습니다.";
	
		if($prev_comment == $comment)
			$sql ="UPDATE tbl_unknowncoin_daily SET status = '$status', comment = '$comment' WHERE logidx = '$logidx'";
		else
			$sql ="UPDATE tbl_unknowncoin_daily SET status = '$status', comment = '$comment', commentdate = NOW() WHERE logidx = '$logidx'";
	
		$db_analysis->execute($sql);
	}
	
	function monitoring_update_unknowncoin_user_new()
	{
		global $db_other;
	
		$logidx = $_POST["logidx"];
		$status = $_POST["status"];
		$comment = $_POST["comment"];
	
		$prev_comment = $db_other->getvalue("SELECT comment FROM tbl_unknowncoin_daily_new WHERE logidx = '$logidx'");
	
		if($logidx == "" || $status == "")
			return "필수항목이 빠졌습니다.";
	
		if($prev_comment == $comment)
			$sql ="UPDATE tbl_unknowncoin_daily_new SET status = '$status', comment = '$comment' WHERE logidx = '$logidx'";
		else
			$sql ="UPDATE tbl_unknowncoin_daily_new SET status = '$status', comment = '$comment', commentdate = NOW() WHERE logidx = '$logidx'";
	
		$db_other->execute($sql);
	}
	
	function monitoring_update_abuse_suspicion_user_status()
	{
		global $db_analysis;
	
		$sql = "";
		$status = $_POST["status"];
		$logidx_list = explode(",", $_POST["logidx_list"]);
	
		for($i=0; $i<sizeof($logidx_list); $i++)
		{
			if($status == 3)
				$sql .= "UPDATE tbl_abuse_suspicion_user_list SET status = '$status' WHERE logidx = '".$logidx_list[$i]."';";
			else
			{
				$sql .= "INSERT INTO tbl_abuse_suspicion_user_check_list(useridx, coin, os, createdate, writedate, status, comment, commentdate) SELECT useridx, coin, os, createdate, writedate, status, comment, commentdate FROM tbl_abuse_suspicion_user_list WHERE STATUS = 1 AND logidx = '$logidx_list[$i]';";
				$sql .= "DELETE FROM tbl_abuse_suspicion_user_list WHERE STATUS = 1 AND logidx = '$logidx_list[$i]';";
			}
		}
	
		$db_analysis->execute($sql);
	}
	
	function monitoring_insert_abuse_suspicion_user()
	{
		global $db_main, $db_analysis;
	
		$useridx = $_POST["useridx"];
		$status = $_POST["status"];
		$comment = $_POST["comment"];
	
		if($useridx == "" || $status == "")
			return "필수항목이 빠졌습니다.";
			
		check_number($useridx);
	
		$sql = "SELECT useridx FROM tbl_user WHERE useridx = $useridx";
	
		if($db_main->getvalue($sql) == "")
			return "존재하지 않는 유저입니다.";
			
		$sql = "SELECT coin, createdate FROM tbl_user WHERE useridx = $useridx";
		$userinfo = $db_main->gettotallist($sql);
			
		$coin = $userinfo[0]["coin"];
		$createdate = $userinfo[0]["createdate"];
			
		$sql = "SELECT adflag FROM tbl_user_ext WHERE useridx = $useridx";
		$adflag = $db_main->getvalue($sql);
			
		if ($adflag == "mobile")
			$adflag = 1;
		else if ($adflag == "mobile_ad")
			$adflag = 2;
		else if ($adflag == "mobile_kindle")
			$adflag = 3;
		else
			$adflag = 0;
	
		$sql = "INSERT INTO tbl_abuse_suspicion_user_list(useridx, coin, os, createdate, writedate, status, comment, commentdate) VALUES($useridx, $coin, $adflag, '$createdate', NOW(), $status, '$comment', NOW())";
		$db_analysis->execute($sql);
	}
	
	function monitoring_update_abuse_suspicion_user()
	{
		global $db_analysis;
	
		$logidx = $_POST["logidx"];
		$status = $_POST["status"];
		$comment = $_POST["comment"];
	
		$prev_comment = $db_analysis->getvalue("SELECT comment FROM tbl_abuse_suspicion_user_list WHERE logidx = '$logidx'");
	
		if($logidx == "" || $status == "")
			return "필수항목이 빠졌습니다.";
	
		if($prev_comment == $comment)
			$sql ="UPDATE tbl_abuse_suspicion_user_list SET status = '$status', comment = '$comment' WHERE logidx = '$logidx'";
		else
			$sql ="UPDATE tbl_abuse_suspicion_user_list SET status = '$status', comment = '$comment', commentdate = NOW() WHERE logidx = '$logidx'";
	
		$db_analysis->execute($sql);
	}
	
	function monitoring_update_abuse_user_debug()
	{
		global $db_main2;
		 
		$useridx = $_POST["useridx"];
		$comment = $_POST["comment"];
		$status = $_POST["status"];
		$platform = $_POST["platform"];		
		 
		if($useridx == "" || $status == "")
			return "필수항목이 빠졌습니다.";
		 
		if($platform == "")
			$platform = 1;
	
		if($status == 0)
		{
			if($platform == 0)
				$is_duplication = $db_main2->getvalue("SELECT COUNT(*) FROM tbl_user_debug WHERE useridx = $useridx");			
	
			if($is_duplication == 1)
				return "이미 등록된 유저입니다.";
		}
		 
		if($status == "1")
			$sql = "DELETE FROM tbl_user_debug WHERE useridx = $useridx";
		else
			$sql = "INSERT INTO tbl_user_debug(useridx, comment, user_type, writedate) VALUES($useridx, '$comment', 1, NOW())";
		 
		if($platform == 0)
			$db_main2->execute($sql);
	}

	function monitoring_update_gain_high_coin_user()
	{
		global $db_analysis, $db_main2;

		$today = $_POST["today"];
		$useridx = $_POST["useridx"];
		$slottype = $_POST["slottype"];
		$user_flag = $_POST["user_flag"];		
		$comment = $_POST["comment"];		
			
		if($useridx == "" || $user_flag == "")
			return "필수항목이 빠졌습니다.";
		
		$sql = "UPDATE tbl_slot_gain_high_coin_list SET user_flag = $user_flag, comment = '$comment', commentdate = NOW() WHERE useridx = $useridx AND today = '$today' AND slottype = $slottype;";
		$db_analysis->execute($sql);
		
		if($user_flag == '3')
		{
			$debug_sql =  "DELETE FROM tbl_user_debug WHERE useridx = $useridx AND user_type = 1";					
			$db_main2->execute($debug_sql);
		}
	}
	
	function monitoring_update_slot_high_coin_user_status()
	{
		global $db_analysis;
		
		$sql = "";
		
		$logidx_list = explode(",", $_POST["logidx_list"]);
		
		for($i=0; $i<sizeof($logidx_list); $i++)
			$sql .= "UPDATE tbl_slot_gain_high_coin_list SET user_flag = '4' WHERE logidx = '".$logidx_list[$i]."';";
		
			$db_analysis->execute($sql);
	}
	
	function monitoring_update_abuse_slot_user_new()
	{
		global $db_other;
		
		global $db_other;
		
		$logidx = $_POST["logidx"];
		$status = $_POST["status"];
		$comment = $_POST["comment"];
		
		$prev_comment = $db_other->getvalue("SELECT comment FROM tbl_abuse_slot_user_list_new WHERE logidx = '$logidx'");
		
		if($logidx == "" || $status == "")
			return "필수항목이 빠졌습니다.";
		
		if($prev_comment == $comment)
			$sql ="UPDATE tbl_abuse_slot_user_list_new SET status = '$status', comment = '$comment' WHERE logidx = '$logidx'";
		else
			$sql ="UPDATE tbl_abuse_slot_user_list_new SET status = '$status', comment = '$comment', commentdate = NOW() WHERE logidx = '$logidx'";
		
		$db_other->execute($sql);
	}
?>