<?
	function customer_save_qa()
	{
		global $db_analysis, $db_main, $db_inbox, $login_adminid;
	
		$qaidx = $_POST["qaidx"];
		$answer = $_POST["answer"];
		$memo = $_POST["memo"];
		$top_categoryidx = $_POST["top_categoryidx"];
		$categoryidx = $_POST["categoryidx"];
		$ispublic = $_POST["ispublic"];
		$ispending = $_POST["ispending"];
		$isspecial = $_POST["isspecial"];
		$isdelete = $_POST["isdelete"];
		$usereporting = $_POST["usereporting"];
		$reportingcategory = $_POST["reportingcategory"];
		$savetype = $_POST["savetype"];
		$status = $_POST["status"];
		$issupportnoti = $_POST["issupportnoti"];
		 
		$free_type = $_POST["free_type"];
		$free_amount = $_POST["free_amount"];
		$free_reason = $_POST["free_reason"];
		$free_message = $_POST["free_message"];
		$free_adminid = $_POST["free_adminid"];
	
		$level = $_POST["level"];
		$question = $_POST["question"];
		$useridx = $_POST["useridx"];
		$finalansweridx = $_POST["finalansweridx"];
	
		$answer=str_replace('"',"'",$answer);
		
		if ($qaidx == "" || ($savetype == "2" && $answer == "") || $ispublic == "" || $status == "")
			return "필수 항목이 빠졌습니다.";
	
		if ($ispending == "")
			$ispending = "0";
	
		if ($isspecial == "")
			$isspecial = "0";
	
		if ($isdelete == "")
			$isdelete = "0";
	
		if ($usereporting == "")
			$usereporting = "0";
	
		if ($free_amount == "")
			$free_amount = "0";
	
		if ($level == "")
			$level = "0";
	
		if ($finalansweridx == "")
			$finalansweridx = "0";
			
		if ($qaidx != "0")
		{
			$useridx = $db_analysis->getvalue("SELECT useridx FROM support_qa WHERE qaidx=$qaidx");
		}
	
		if ($useridx == "")
			return "존재하지 않는 QA 입니다.";
	
		if ($savetype == "1")
		{
			$sql = "UPDATE support_qa SET status=$status,responsedate=NOW(),ispublic=$ispublic,top_categoryidx=$top_categoryidx,categoryidx=$categoryidx,ispending=$ispending,isspecial=$isspecial,isdelete=$isdelete,usereporting=$usereporting,reportingcategory='$reportingcategory' WHERE qaidx=$qaidx;";
			$db_analysis->execute($sql);
		}
		else
		{
			if ($qaidx == "0")
			{
				$sql = "SELECT nickname,userid FROM tbl_user WHERE useridx=$useridx";
				$data = $db_main->getarray($sql);
	
				$facebookid = encode_db_field($data["userid"]);
				$name = encode_db_field($data["nickname"]);
	
				$sql = "INSERT INTO support_qa(top_categoryidx,categoryidx,useridx,facebookid,name,contents,writedate) VALUES($top_categoryidx,$categoryidx,$useridx,'$facebookid','$name','$question',NOW())";
				$db_analysis->execute($sql);
	
				$sql = "SELECT LAST_INSERT_ID()";
				$qaidx = $db_analysis->getvalue($sql);
			}
			else
			{
				$sql = "SELECT answeridx FROM support_qa_answer WHERE qaidx=$qaidx ORDER BY answeridx DESC LIMIT 1";
				$answeridx = $db_analysis->getvalue($sql);
	
				if ($answeridx != "" && (int)$answeridx > (int)$finalansweridx)
				{
					if ($finalansweridx == "0")
						return "이미 답변이 달린 QA 입니다.";
					else
						return "이미 후속 답변이 달린 QA 입니다.";
				}
			}
				
			$sql = "UPDATE support_qa SET status=1,responsedate=NOW(),ispublic=$ispublic,top_categoryidx=$top_categoryidx,categoryidx=$categoryidx,ispending=$ispending,isspecial=$isspecial,isdelete=$isdelete,usereporting=$usereporting,reportingcategory='$reportingcategory' WHERE qaidx=$qaidx;".
					"INSERT INTO support_qa_answer(qaidx,adminid,answer,memo,writedate,coin,coin_memo,coin_message,level) VALUES($qaidx,'$login_adminid','$answer','$memo',NOW(),'$free_amount','$free_reason','$free_message','$level')";
	
			$db_analysis->execute($sql);
			
			if($issupportnoti  > 0)
			{
				try
				{
					$sql = "SELECT userid FROM tbl_user WHERE useridx=$useridx";
					$facebookid = $db_main->getvalue($sql);
					
					$facebook = new Facebook(array(
							'appId'  => FACEBOOK_APP_ID,
							'secret' => FACEBOOK_SECRET_KEY,
							'cookie' => true,
					));
						
					$session = $facebook->getUser();
					
					if($issupportnoti == 1)
					   $template = " An answer to your recent inquiry has been posted. Click and Check it Now!";
					else if($issupportnoti == 2)
					   $template = " One Time Only Alert! You have been Selected to Receive our First-Ever Exclusive Coin Bonus!";
					write_log("useridx - ".$useridx);
					write_log("template - ".$template);
					write_log("href- "."?adflag=suppport_answer&qaidx=$qaidx");

					$args = array('template' => "$template",
							'href' => "?adflag=suppport_answer&qaidx=$qaidx");
					

					$info = $facebook->api("/$facebookid/notifications", "POST", $args);
				}
				catch (FacebookApiException $e)
				{
					if($e->getMessage() == "Unsupported operation" || $e->getMessage() == "(#200) Cannot send notifications to a user who has not installed the app")
					{
						write_log("Suppport Answer : ".$e->getMessage());
					}
					write_log($e->getMessage());
				}
			}
	
			if ($free_amount != "0")
			{
				$coin = 0;
				$free_msg = "";
	
				if($free_type == "1")
					$coin = $free_amount;

	
				if ($coin != "0")
				{
	            	// 코인 발급
            		$sql = "INSERT INTO tbl_freecoin_admin_log (useridx,freecoin,reason,message,writedate) VALUES('$useridx','$coin', '$free_reason', '$free_message', NOW())";
		        	$db_main->execute($sql);
				
					$sql = "INSERT INTO tbl_user_inbox_".($useridx%20)."(useridx,category,coin,title,writedate) VALUES($useridx,101,'$coin','$free_message',now());";
					$db_inbox->execute($sql);
				}
			}
		}
	}
		
	function customer_delete_qa()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
	
		if ($qaidx == "")
			return "필수 항목이 빠졌습니다.";
	
		$qaidx = $db_analysis->getvalue("SELECT qaidx FROM support_qa WHERE qaidx=$qaidx");
	
		if ($qaidx == "")
			return "존재하지 않는 QA 입니다.";
	
		$sql = "DELETE FROM support_qa WHERE qaidx=$qaidx;".
				"DELETE FROM support_qa_answer WHERE qaidx=$qaidx";
			
		$db_analysis->execute($sql);
	}
	
	function customer_answer_qa()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
	
		if ($qaidx == "")
			return "필수 항목이 빠졌습니다.";
	
		$sql = "UPDATE support_qa SET status=1,responsedate=NOW() WHERE qaidx IN ($qaidx)";
			
		$db_analysis->execute($sql);
	}
	
	function customer_update_answer()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
		$answeridx = $_POST["answeridx"];
		$level = $_POST["level"];
		$feedback = $_POST["feedback"];
	
		if ($qaidx == "" || $answeridx == "" || $level == "")
			return "필수 항목이 빠졌습니다.";
	
		$answeridx = $db_analysis->getvalue("SELECT answeridx FROM support_qa_answer WHERE answeridx=$answeridx");
	
		if ($answeridx == "")
			return "존재하지 않는 답변 입니다.";
	
		$sql = "UPDATE support_qa_answer SET level='$level',feedback='$feedback' WHERE answeridx=$answeridx";
			
		$db_analysis->execute($sql);
	}
	
	function customer_delete_answer()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
		$answeridx = $_POST["answeridx"];
	
		if ($qaidx == "" || $answeridx == "")
			return "필수 항목이 빠졌습니다.";
	
		$answeridx = $db_analysis->getvalue("SELECT answeridx FROM support_qa_answer WHERE answeridx=$answeridx");
	
		if ($answeridx == "")
			return "존재하지 않는 답변 입니다.";
	
		$sql = "DELETE FROM support_qa_answer WHERE answeridx=$answeridx;".
				"UPDATE support_qa SET status=0 WHERE qaidx=$qaidx AND NOT EXISTS (SELECT * FROM support_qa_answer WHERE qaidx=support_qa.qaidx)";
			
		$db_analysis->execute($sql);
	}
	
	function customer_save_cr()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
		$top_categoryidx = $_POST["top_categoryidx"];
		$categoryidx = $_POST["categoryidx"];
		$question = $_POST["question"];
		$writedate = $_POST["writedate"];
		$occurdate = $_POST["occurdate"];
		$useridx = $_POST["useridx"];
		$facebookid = $_POST["facebookid"];
		$name = $_POST["name"];
		 
		if($qaidx ==  "" || $top_categoryidx == "" || $categoryidx == "" || $question == "" || $writedate == "" || $occurdate == "" || $useridx == "" || $facebookid == "" || $name == "")
			return "필수 항목이 빠졌습니다.";
		 
		$sql = "SELECT reviewidx FROM claim_review WHERE qaidx = $qaidx";
		$old_reviewidx = $db_analysis->getvalue($sql);
		 
		if($old_reviewidx != "")
			return "이미 클레임 리뷰로 등록 된 문의입니다.";
		 
		$sql = "INSERT INTO claim_review(qaidx, top_categoryidx, categoryidx, useridx, facebookid, name, contents, occurdate, writedate) ".
				"VALUES($qaidx, $top_categoryidx, $categoryidx, $useridx, $facebookid, '$name', '$question', '$occurdate', '$writedate')";
		 
		$db_analysis->execute($sql);
		 
		$sql = "SELECT LAST_INSERT_ID()";
		$reviewidx = $db_analysis->getvalue($sql);
		 
		$sql = "UPDATE support_qa SET reviewidx = $reviewidx WHERE qaidx = $qaidx";
		$db_analysis->execute($sql);
	}
	
	function customer_save_qa_ios()
	{
		global $db_analysis, $db_main, $db_inbox, $login_adminid, $db_mobile;
	
		$qaidx = $_POST["qaidx"];
		$answer = $_POST["answer"];
		$memo = $_POST["memo"];
		$categoryidx = $_POST["categoryidx"];
		$ispublic = $_POST["ispublic"];
		$ispending = $_POST["ispending"];
		$isspecial = $_POST["isspecial"];
		$isdelete = $_POST["isdelete"];
		$usereporting = $_POST["usereporting"];
		$reportingcategory = $_POST["reportingcategory"];
		$savetype = $_POST["savetype"];
		$status = $_POST["status"];
		$level = $_POST["level"];
		$question = $_POST["question"];
		$useridx = $_POST["useridx"];
		$finalansweridx = $_POST["finalansweridx"];
		$issupportnoti = $_POST["issupportnoti"];
	
		$free_type = $_POST["free_type"];
		$free_amount = $_POST["free_amount"];
		$free_reason = $_POST["free_reason"];
		$free_message = $_POST["free_message"];
		$free_adminid = $_POST["free_adminid"];
		
		$answer=str_replace('"',"'",$answer);
			
		if ($qaidx == "" || ($savetype == "2" && $answer == "") || $ispublic == "" || $status == "")
			return "필수 항목이 빠졌습니다.";
	
		if ($ispending == "")
			$ispending = "0";
	
		if ($isspecial == "")
			$isspecial = "0";
	
		if ($isdelete == "")
			$isdelete = "0";
	
		if ($usereporting == "")
			$usereporting = "0";
	
		if ($free_amount == "")
			$free_amount = "0";
	
		if ($level == "")
			$level = "0";
	
		if ($finalansweridx == "")
			$finalansweridx = "0";
			
		if ($qaidx != "0")
		{
			$useridx = $db_analysis->getvalue("SELECT useridx FROM support_mobile_qa WHERE qaidx=$qaidx");
		}
	
		if ($useridx == "")
			return "존재하지 않는 QA 입니다.";
	
		if ($savetype == "1")
		{
			$sql = "UPDATE support_mobile_qa SET status=$status,responsedate=NOW(),ispublic=$ispublic,categoryidx=$categoryidx,ispending=$ispending,isspecial=$isspecial,isdelete=$isdelete,usereporting=$usereporting,reportingcategory='$reportingcategory' WHERE qaidx=$qaidx;";
			$db_analysis->execute($sql);
		}
		else
		{
			if ($qaidx == "0")
			{
				$sql = "SELECT nickname,userid FROM tbl_user WHERE useridx=$useridx";
				$data = $db_main->getarray($sql);
	
				$facebookid = encode_db_field($data["userid"]);
				$name = encode_db_field($data["nickname"]);
	
				$sql = "INSERT INTO support_mobile_qa(categoryidx,useridx,facebookid,name,contents,writedate) VALUES($categoryidx,$useridx,'$facebookid','$name','$question',NOW())";
				$db_analysis->execute($sql);
	
				$sql = "SELECT LAST_INSERT_ID()";
				$qaidx = $db_analysis->getvalue($sql);
			}
			else
			{
				$sql = "SELECT answeridx FROM support_mobile_qa_answer WHERE qaidx=$qaidx ORDER BY answeridx DESC LIMIT 1";
				$answeridx = $db_analysis->getvalue($sql);
	
				if ($answeridx != "" && (int)$answeridx > (int)$finalansweridx)
				{
					if ($finalansweridx == "0")
						return "이미 답변이 달린 QA 입니다.";
					else
						return "이미 후속 답변이 달린 QA 입니다.";
				}
			}
	
			$sql = "UPDATE support_mobile_qa SET status=1,responsedate=NOW(),ispublic=$ispublic,categoryidx=$categoryidx,ispending=$ispending,isspecial=$isspecial,isdelete=$isdelete,usereporting=$usereporting,reportingcategory='$reportingcategory' WHERE qaidx=$qaidx;".
					"INSERT INTO support_mobile_qa_answer(qaidx,adminid,answer,memo,writedate,coin,coin_memo,coin_message,level) VALUES($qaidx,'$login_adminid','$answer','$memo',NOW(),'$free_amount','$free_reason','$free_message','$level')";
	
			$db_analysis->execute($sql);
	
			if($issupportnoti > 0)
			{
				$sql="INSERT INTO tbl_user_support_qa_noti(useridx,qaidx,os_type,push_type,issend,senddate,writedate) VALUES($useridx,$qaidx,1,$issupportnoti,0,'0000-00-00 00:00:00',now())";
				$db_mobile->execute($sql);
			}
			
			if ($free_amount != "0")
			{
				$coin = 0;
				$free_msg = "";
	
				if($free_type == "1")
					$coin = $free_amount;
	
	
				if ($coin != "0")
				{
					// 코인 발급
					$sql = "INSERT INTO tbl_freecoin_admin_log (useridx,freecoin,reason,message,writedate) VALUES('$useridx','$coin', '$free_reason', '$free_message', NOW())";
					$db_main->execute($sql);
	
					$sql = "INSERT INTO tbl_user_inbox_".($useridx%20)."(useridx,category,coin,title,writedate) VALUES($useridx,101,'$coin','$free_message',NOW());";
					$db_inbox->execute($sql);
				}
			}
		}
	}
	
	function customer_save_qa_android()
	{
		global $db_analysis, $db_main, $db_inbox, $login_adminid, $db_mobile;
	
		$qaidx = $_POST["qaidx"];
		$answer = $_POST["answer"];
		$memo = $_POST["memo"];
		$categoryidx = $_POST["categoryidx"];
		$ispublic = $_POST["ispublic"];
		$ispending = $_POST["ispending"];
		$isspecial = $_POST["isspecial"];
		$isdelete = $_POST["isdelete"];
		$usereporting = $_POST["usereporting"];
		$reportingcategory = $_POST["reportingcategory"];
		$savetype = $_POST["savetype"];
		$status = $_POST["status"];
		$level = $_POST["level"];
		$question = $_POST["question"];
		$useridx = $_POST["useridx"];
		$finalansweridx = $_POST["finalansweridx"];
		$issupportnoti = $_POST["issupportnoti"];
		
		$free_type = $_POST["free_type"];
		$free_amount = $_POST["free_amount"];
		$free_reason = $_POST["free_reason"];
		$free_message = $_POST["free_message"];
		$free_adminid = $_POST["free_adminid"];
			
		$answer=str_replace('"',"'",$answer);
		
		if ($qaidx == "" || ($savetype == "2" && $answer == "") || $ispublic == "" || $status == "")
			return "필수 항목이 빠졌습니다.";
	
		if ($ispending == "")
			$ispending = "0";
	
		if ($isspecial == "")
			$isspecial = "0";
	
		if ($isdelete == "")
			$isdelete = "0";
	
		if ($usereporting == "")
			$usereporting = "0";
	
		if ($free_amount == "")
			$free_amount = "0";
	
		if ($level == "")
			$level = "0";
	
		if ($finalansweridx == "")
			$finalansweridx = "0";
			
		if ($qaidx != "0")
		{
			$useridx = $db_analysis->getvalue("SELECT useridx FROM support_android_qa WHERE qaidx=$qaidx");
		}
	
		if ($useridx == "")
			return "존재하지 않는 QA 입니다.";
	
		if ($savetype == "1")
		{
			$sql = "UPDATE support_android_qa SET status=$status,responsedate=NOW(),ispublic=$ispublic,categoryidx=$categoryidx,ispending=$ispending,isspecial=$isspecial,isdelete=$isdelete,usereporting=$usereporting,reportingcategory='$reportingcategory' WHERE qaidx=$qaidx;";
			$db_analysis->execute($sql);
		}
		else
		{
			if ($qaidx == "0")
			{
				$sql = "SELECT nickname,userid FROM tbl_user WHERE useridx=$useridx";
				$data = $db_main->getarray($sql);
	
				$facebookid = encode_db_field($data["userid"]);
				$name = encode_db_field($data["nickname"]);
	
				$sql = "INSERT INTO support_android_qa(categoryidx,useridx,facebookid,name,contents,writedate) VALUES($categoryidx,$useridx,'$facebookid','$name','$question',NOW())";
				$db_analysis->execute($sql);
	
				$sql = "SELECT LAST_INSERT_ID()";
				$qaidx = $db_analysis->getvalue($sql);
			}
			else
			{
				$sql = "SELECT answeridx FROM support_android_qa_answer WHERE qaidx=$qaidx ORDER BY answeridx DESC LIMIT 1";
				$answeridx = $db_analysis->getvalue($sql);
	
				if ($answeridx != "" && (int)$answeridx > (int)$finalansweridx)
				{
					if ($finalansweridx == "0")
						return "이미 답변이 달린 QA 입니다.";
					else
						return "이미 후속 답변이 달린 QA 입니다.";
				}
			}
	
			$sql = "UPDATE support_android_qa SET status=1,responsedate=NOW(),ispublic=$ispublic,categoryidx=$categoryidx,ispending=$ispending,isspecial=$isspecial,isdelete=$isdelete,usereporting=$usereporting,reportingcategory='$reportingcategory' WHERE qaidx=$qaidx;".
					"INSERT INTO support_android_qa_answer(qaidx,adminid,answer,memo,writedate,coin,coin_memo,coin_message,level) VALUES($qaidx,'$login_adminid','$answer','$memo',NOW(),'$free_amount','$free_reason','$free_message','$level')";
	
			$db_analysis->execute($sql);
	
			if($issupportnoti > 0)
			{
			    $sql="INSERT INTO tbl_user_support_qa_noti(useridx,qaidx,os_type,push_type,issend,senddate,writedate) VALUES($useridx,$qaidx,2,$issupportnoti,0,'0000-00-00 00:00:00',now())";
				$db_mobile->execute($sql);
			}
			
			if ($free_amount != "0")
			{
				$coin = 0;
				$free_msg = "";
	
				if($free_type == "1")
					$coin = $free_amount;
	
	
				if ($coin != "0")
				{
					// 코인 발급
					$sql = "INSERT INTO tbl_freecoin_admin_log (useridx,freecoin,reason,message,writedate) VALUES('$useridx','$coin', '$free_reason', '$free_message', NOW())";
					$db_main->execute($sql);
	
					$sql = "INSERT INTO tbl_user_inbox_".($useridx%20)."(useridx,category,coin,title,writedate) VALUES($useridx,101,'$coin','$free_message',NOW());";
					$db_inbox->execute($sql);
				}
			}
		}
	}
	
	function customer_save_qa_amazon()
	{
		global $db_analysis, $db_main, $db_inbox, $login_adminid, $db_mobile;
	
		$qaidx = $_POST["qaidx"];
		$answer = $_POST["answer"];
		$memo = $_POST["memo"];
		$categoryidx = $_POST["categoryidx"];
		$ispublic = $_POST["ispublic"];
		$ispending = $_POST["ispending"];
		$isspecial = $_POST["isspecial"];
		$isdelete = $_POST["isdelete"];
		$usereporting = $_POST["usereporting"];
		$reportingcategory = $_POST["reportingcategory"];
		$savetype = $_POST["savetype"];
		$status = $_POST["status"];
		$level = $_POST["level"];
		$question = $_POST["question"];
		$useridx = $_POST["useridx"];
		$finalansweridx = $_POST["finalansweridx"];
		$issupportnoti = $_POST["issupportnoti"];
		
		$free_type = $_POST["free_type"];
		$free_amount = $_POST["free_amount"];
		$free_reason = $_POST["free_reason"];
		$free_message = $_POST["free_message"];
		$free_adminid = $_POST["free_adminid"];
			
		$answer=str_replace('"',"'",$answer);
		
		if ($qaidx == "" || ($savetype == "2" && $answer == "") || $ispublic == "" || $status == "")
			return "필수 항목이 빠졌습니다.";
	
		if ($ispending == "")
			$ispending = "0";
	
		if ($isspecial == "")
			$isspecial = "0";
	
		if ($isdelete == "")
			$isdelete = "0";
	
		if ($usereporting == "")
			$usereporting = "0";
	
		if ($free_amount == "")
			$free_amount = "0";
	
		if ($level == "")
			$level = "0";
	
		if ($finalansweridx == "")
			$finalansweridx = "0";
			
		if ($qaidx != "0")
		{
			$useridx = $db_analysis->getvalue("SELECT useridx FROM support_amazon_qa WHERE qaidx=$qaidx");
		}
	
		if ($useridx == "")
			return "존재하지 않는 QA 입니다.";
	
		if ($savetype == "1")
		{
			$sql = "UPDATE support_amazon_qa SET status=$status,responsedate=NOW(),ispublic=$ispublic,categoryidx=$categoryidx,ispending=$ispending,isspecial=$isspecial,isdelete=$isdelete,usereporting=$usereporting,reportingcategory='$reportingcategory' WHERE qaidx=$qaidx;";
			$db_analysis->execute($sql);
		}
		else
		{
			if ($qaidx == "0")
			{
				$sql = "SELECT nickname,userid FROM tbl_user WHERE useridx=$useridx";
				$data = $db_main->getarray($sql);
	
				$facebookid = encode_db_field($data["userid"]);
				$name = encode_db_field($data["nickname"]);
	
				$sql = "INSERT INTO support_amazon_qa(categoryidx,useridx,facebookid,name,contents,writedate) VALUES($categoryidx,$useridx,'$facebookid','$name','$question',NOW())";
				$db_analysis->execute($sql);
	
				$sql = "SELECT LAST_INSERT_ID()";
				$qaidx = $db_analysis->getvalue($sql);
			}
			else
			{
				$sql = "SELECT answeridx FROM support_amazon_qa_answer WHERE qaidx=$qaidx ORDER BY answeridx DESC LIMIT 1";
				$answeridx = $db_analysis->getvalue($sql);
	
				if ($answeridx != "" && (int)$answeridx > (int)$finalansweridx)
				{
					if ($finalansweridx == "0")
						return "이미 답변이 달린 QA 입니다.";
					else
						return "이미 후속 답변이 달린 QA 입니다.";
				}
			}
	
			$sql = "UPDATE support_amazon_qa SET status=1,responsedate=NOW(),ispublic=$ispublic,categoryidx=$categoryidx,ispending=$ispending,isspecial=$isspecial,isdelete=$isdelete,usereporting=$usereporting,reportingcategory='$reportingcategory' WHERE qaidx=$qaidx;".
					"INSERT INTO support_amazon_qa_answer(qaidx,adminid,answer,memo,writedate,coin,coin_memo,coin_message,level) VALUES($qaidx,'$login_adminid','$answer','$memo',NOW(),'$free_amount','$free_reason','$free_message','$level')";
	
			$db_analysis->execute($sql);
	
			if($issupportnoti > 0)
			{
			    $sql="INSERT INTO tbl_user_support_qa_noti(useridx,qaidx,os_type,push_type,issend,senddate,writedate) VALUES($useridx,$qaidx,3,$issupportnoti,0,'0000-00-00 00:00:00',now())";
			    $db_mobile->execute($sql);
			}
			
			if ($free_amount != "0")
			{
				$coin = 0;
				$free_msg = "";
	
				if($free_type == "1")
					$coin = $free_amount;
	
	
				if ($coin != "0")
				{
					// 코인 발급
					$sql = "INSERT INTO tbl_freecoin_admin_log (useridx,freecoin,reason,message,writedate) VALUES('$useridx','$coin', '$free_reason', '$free_message', NOW())";
					$db_main->execute($sql);
	
					$sql = "INSERT INTO tbl_user_inbox_".($useridx%20)."(useridx,category,coin,title,writedate) VALUES($useridx,101,'$coin','$free_message',NOW());";
					$db_inbox->execute($sql);
				}
			}
		}
	}
	
	function customer_delete_qa_ios()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
	
		if ($qaidx == "")
			return "필수 항목이 빠졌습니다.";
	
		$qaidx = $db_analysis->getvalue("SELECT qaidx FROM support_mobile_qa WHERE qaidx=$qaidx");
	
		if ($qaidx == "")
			return "존재하지 않는 QA 입니다.";
	
		$sql = "DELETE FROM support_mobile_qa WHERE qaidx=$qaidx;".
				"DELETE FROM support_mobile_qa_answer WHERE qaidx=$qaidx";
			
		$db_analysis->execute($sql);
	}
	
	function customer_delete_qa_android()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
	
		if ($qaidx == "")
			return "필수 항목이 빠졌습니다.";
	
		$qaidx = $db_analysis->getvalue("SELECT qaidx FROM support_android_qa WHERE qaidx=$qaidx");
	
		if ($qaidx == "")
			return "존재하지 않는 QA 입니다.";
	
		$sql = "DELETE FROM support_android_qa WHERE qaidx=$qaidx;".
				"DELETE FROM support_android_qa_answer WHERE qaidx=$qaidx";
			
		$db_analysis->execute($sql);
	}
	
	function customer_delete_qa_amazon()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
	
		if ($qaidx == "")
			return "필수 항목이 빠졌습니다.";
	
		$qaidx = $db_analysis->getvalue("SELECT qaidx FROM support_amazon_qa WHERE qaidx=$qaidx");
	
		if ($qaidx == "")
			return "존재하지 않는 QA 입니다.";
	
		$sql = "DELETE FROM support_amazon_qa WHERE qaidx=$qaidx;".
				"DELETE FROM support_amazon_qa_answer WHERE qaidx=$qaidx";
			
		$db_analysis->execute($sql);
	}
	
	function customer_answer_qa_ios()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
	
		if ($qaidx == "")
			return "필수 항목이 빠졌습니다.";
	
		$sql = "UPDATE support_mobile_qa SET status=1,responsedate=NOW() WHERE qaidx IN ($qaidx)";
	
		$db_analysis->execute($sql);
	}
	
	function customer_answer_qa_android()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
	
		if ($qaidx == "")
			return "필수 항목이 빠졌습니다.";
	
		$sql = "UPDATE support_android_qa SET status=1,responsedate=NOW() WHERE qaidx IN ($qaidx)";
	
		$db_analysis->execute($sql);
	}
	
	function customer_answer_qa_amazon()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
	
		if ($qaidx == "")
			return "필수 항목이 빠졌습니다.";
	
		$sql = "UPDATE support_amazon_qa SET status=1,responsedate=NOW() WHERE qaidx IN ($qaidx)";
	
		$db_analysis->execute($sql);
	}
	
	function customer_delete_answer_ios()
	{
		global $db_analysis;
			
		$qaidx = $_POST["qaidx"];
		$answeridx = $_POST["answeridx"];
			
		if ($qaidx == "" || $answeridx == "")
			return "필수 항목이 빠졌습니다.";
			
		$answeridx = $db_analysis->getvalue("SELECT answeridx FROM support_mobile_qa_answer WHERE answeridx=$answeridx");
			
		if ($answeridx == "")
			return "존재하지 않는 답변 입니다.";
			
		$sql = "DELETE FROM support_mobile_qa_answer WHERE answeridx=$answeridx;".
				"UPDATE support_mobile_qa SET status=0 WHERE qaidx=$qaidx AND NOT EXISTS (SELECT * FROM support_mobile_qa_answer WHERE qaidx=support_mobile_qa.qaidx)";
	
		$db_analysis->execute($sql);
	}
	
	function customer_delete_answer_android()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
		$answeridx = $_POST["answeridx"];
	
		if ($qaidx == "" || $answeridx == "")
			return "필수 항목이 빠졌습니다.";
	
		$answeridx = $db_analysis->getvalue("SELECT answeridx FROM support_android_qa_answer WHERE answeridx=$answeridx");
	
		if ($answeridx == "")
			return "존재하지 않는 답변 입니다.";
	
		$sql = "DELETE FROM support_android_qa_answer WHERE answeridx=$answeridx;".
				"UPDATE support_android_qa SET status=0 WHERE qaidx=$qaidx AND NOT EXISTS (SELECT * FROM support_android_qa_answer WHERE qaidx=support_android_qa.qaidx)";
	
		$db_analysis->execute($sql);
	}
	
	function customer_delete_answer_amazon()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
		$answeridx = $_POST["answeridx"];
	
		if ($qaidx == "" || $answeridx == "")
			return "필수 항목이 빠졌습니다.";
	
		$answeridx = $db_analysis->getvalue("SELECT answeridx FROM support_amazon_qa_answer WHERE answeridx=$answeridx");
	
		if ($answeridx == "")
			return "존재하지 않는 답변 입니다.";
	
		$sql = "DELETE FROM support_amazon_qa_answer WHERE answeridx=$answeridx;".
				"UPDATE support_amazon_qa SET status=0 WHERE qaidx=$qaidx AND NOT EXISTS (SELECT * FROM support_amazon_qa_answer WHERE qaidx=support_amazon_qa.qaidx)";
	
		$db_analysis->execute($sql);
	}
	
	function customer_update_answer_ios()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
		$answeridx = $_POST["answeridx"];
		$level = $_POST["level"];
		$feedback = $_POST["feedback"];
	
		if ($qaidx == "" || $answeridx == "" || $level == "")
			return "필수 항목이 빠졌습니다.";
	
		$answeridx = $db_analysis->getvalue("SELECT answeridx FROM support_mobile_qa_answer WHERE answeridx=$answeridx");
	
		if ($answeridx == "")
			return "존재하지 않는 답변 입니다.";
	
		$sql = "UPDATE support_mobile_qa_answer SET level='$level',feedback='$feedback' WHERE answeridx=$answeridx";
	
		$db_analysis->execute($sql);
	}
	
	function customer_update_answer_android()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
		$answeridx = $_POST["answeridx"];
		$level = $_POST["level"];
		$feedback = $_POST["feedback"];
	
		if ($qaidx == "" || $answeridx == "" || $level == "")
			return "필수 항목이 빠졌습니다.";
	
		$answeridx = $db_analysis->getvalue("SELECT answeridx FROM support_android_qa_answer WHERE answeridx=$answeridx");
	
		if ($answeridx == "")
			return "존재하지 않는 답변 입니다.";
	
		$sql = "UPDATE support_android_qa_answer SET level='$level',feedback='$feedback' WHERE answeridx=$answeridx";
	
		$db_analysis->execute($sql);
	}
	
	function customer_update_answer_amazon()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
		$answeridx = $_POST["answeridx"];
		$level = $_POST["level"];
		$feedback = $_POST["feedback"];
	
		if ($qaidx == "" || $answeridx == "" || $level == "")
			return "필수 항목이 빠졌습니다.";
	
		$answeridx = $db_analysis->getvalue("SELECT answeridx FROM support_amazon_qa_answer WHERE answeridx=$answeridx");
	
		if ($answeridx == "")
			return "존재하지 않는 답변 입니다.";
	
		$sql = "UPDATE support_amazon_qa_answer SET level='$level',feedback='$feedback' WHERE answeridx=$answeridx";
	
		$db_analysis->execute($sql);
	}
	
	function customer_save_template()
	{
		global $db_analysis;
	
		$title = $_POST["title"];
		$top_categoryidx = $_POST["top_categoryidx"];
		$categoryidx = $_POST["categoryidx"];
		$contents = $_POST["contents"];
	
		if ($title == "" || $top_categoryidx == "" || $categoryidx == "" || $contents == "")
			return "필수 항목이 빠졌습니다.";
	
		$sql = "INSERT INTO support_qa_template(title,top_categoryidx,categoryidx,contents,writedate) VALUES ('$title',$top_categoryidx,$categoryidx,'$contents',NOW())";
		$db_analysis->execute($sql);
	}
	
	function customer_update_template()
	{
		global $db_analysis;
	
		$templateidx = $_POST["templateidx"];
		$title = $_POST["title"];
		$top_categoryidx = $_POST["top_categoryidx"];
		$categoryidx = $_POST["categoryidx"];
		$contents = $_POST["contents"];
	
		if ($templateidx == "" || $title == "" || $top_categoryidx == "" || $categoryidx == "" || $contents == "")
			return "필수 항목이 빠졌습니다.";
	
		$templateidx = $db_analysis->getvalue("SELECT templateidx FROM support_qa_template WHERE templateidx=$templateidx");
	
		if ($templateidx == "")
			return "존재하지 않는 QA 답변 템플릿입니다.";
	
		$sql = "UPDATE support_qa_template SET title='$title',contents='$contents',top_categoryidx=$top_categoryidx,categoryidx=$categoryidx WHERE templateidx=$templateidx";
	
		$db_analysis->execute($sql);
	}
	
	function customer_delete_template()
	{
		global $db_analysis;
	
		$templateidx = $_POST["templateidx"];
	
		if ($templateidx == "")
			return "필수 항목이 빠졌습니다.";
	
		$templateidx = $db_analysis->getvalue("SELECT templateidx FROM support_qa_template WHERE templateidx=$templateidx");
	
		if ($templateidx == "")
			return "존재하지 않는 QA 답변 템플릿입니다.";
	
		$sql = "DELETE FROM support_qa_template WHERE templateidx=$templateidx";
		$db_analysis->execute($sql);
	}
	
	function customer_save_faq()
	{
		global $db_analysis;
	
		$title = $_POST["title"];
		$top_categoryidx = $_POST["top_categoryidx"];
		$categoryidx = $_POST["categoryidx"];
		$contents = $_POST["contents"];
		$status = $_POST["status"];
	
		if ($title == "" || $top_categoryidx == "" || $categoryidx == "" || $contents == "" || $status == "")
			return "필수 항목이 빠졌습니다.";
	
		$faq_sort = $db_analysis->getvalue("SELECT IFNULL(MAX(faq_sort)+1,1) FROM support_faq");
	
		$sql = "INSERT INTO support_faq(title,top_categoryidx,categoryidx,contents,faq_sort,status,writedate) VALUES ('$title',$top_categoryidx,$categoryidx,'$contents','$faq_sort','$status',NOW())";
		$db_analysis->execute($sql);
	}
	
	function customer_update_faq()
	{
		global $db_analysis;
	
		$faqidx = $_POST["faqidx"];
		$title = $_POST["title"];
		$top_categoryidx = $_POST["top_categoryidx"];
		$categoryidx = $_POST["categoryidx"];
		$contents = $_POST["contents"];
		$status = $_POST["status"];
	
		if ($faqidx == "" || $title == "" || $top_categoryidx == "" || $categoryidx == "" || $contents == "")
			return "필수 항목이 빠졌습니다.";
	
		$faqidx = $db_analysis->getvalue("SELECT faqidx FROM support_faq WHERE faqidx=$faqidx");
	
		if ($faqidx == "")
			return "존재하지 않는 FAQ입니다.";
	
		$sql = "UPDATE support_faq SET title='$title',contents='$contents',status='$status',top_categoryidx=$top_categoryidx,categoryidx=$categoryidx WHERE faqidx=$faqidx";
		$db_analysis->execute($sql);
	}
	
	function customer_delete_faq()
	{
		global $db_analysis;
	
		$faqidx = $_POST["faqidx"];
	
		if ($faqidx == "")
			return "필수 항목이 빠졌습니다.";
	
		$faqidx = $db_analysis->getvalue("SELECT faqidx FROM support_faq WHERE faqidx=$faqidx");
	
		if ($faqidx == "")
			return "존재하지 않는 FAQ입니다.";
	
		$sql = "DELETE FROM support_faq WHERE faqidx=$faqidx";
		$db_analysis->execute($sql);
	}
	
	function customer_move_faq()
	{
		global $db_analysis;
	
		$faqidx = $_POST["faqidx"];
		$movetype = $_POST["movetype"];
	
		if ($faqidx == "" || $movetype == "")
			return "필수 항목이 빠졌습니다.";
	
		$faq_sort = $db_analysis->getvalue("SELECT faq_sort FROM support_faq WHERE faqidx=$faqidx");
	
		if ($faq_sort == "")
			return "존재하지 않는 FAQ입니다.";
	
		if ($movetype == "up")
		{
			$sql = "SELECT faqidx,faq_sort FROM support_faq WHERE faq_sort<$faq_sort ORDER BY faq_sort DESC LIMIT 1";
			$data = $db_analysis->getarray($sql);
	
			$prev_faqidx = $data["faqidx"];
			$prev_faq_sort = $data["faq_sort"];
	
			if ($prev_faqidx != "")
			{
				$sql = "UPDATE support_faq SET faq_sort=$prev_faq_sort WHERE faqidx=$faqidx;".
						"UPDATE support_faq SET faq_sort=$faq_sort WHERE faqidx=$prev_faqidx";
					
				$db_analysis->execute($sql);
			}
		}
		else
		{
			$sql = "SELECT faqidx,faq_sort FROM support_faq WHERE faq_sort>$faq_sort ORDER BY faq_sort ASC LIMIT 1";
			$data = $db_analysis->getarray($sql);
	
			$next_faqidx = $data["faqidx"];
			$next_faq_sort = $data["faq_sort"];
	
			if ($next_faqidx != "")
			{
				$sql = "UPDATE support_faq SET faq_sort=$next_faq_sort WHERE faqidx=$faqidx;".
						"UPDATE support_faq SET faq_sort=$faq_sort WHERE faqidx=$next_faqidx";
					
				$db_analysis->execute($sql);
			}
		}
	}
	
	function customer_save_mobile_ios_template()
	{
		global $db_analysis;
		 
		$title = $_POST["title"];
		$categoryidx = $_POST["categoryidx"];
		$contents = $_POST["contents"];
		 
		if ($title == "" || $categoryidx == "" || $contents == "")
			return "필수 항목이 빠졌습니다.";
		 
		$sql = "INSERT INTO support_mobile_qa_template(title,categoryidx,contents,writedate) VALUES ('$title',$categoryidx,'$contents',NOW())";
		$db_analysis->execute($sql);
	}
	
	function customer_save_mobile_android_template()
	{
		global $db_analysis;
	
		$title = $_POST["title"];
		$categoryidx = $_POST["categoryidx"];
		$contents = $_POST["contents"];
	
		if ($title == "" || $categoryidx == "" || $contents == "")
			return "필수 항목이 빠졌습니다.";
	
		$sql = "INSERT INTO support_android_qa_template(title,categoryidx,contents,writedate) VALUES ('$title',$categoryidx,'$contents',NOW())";
		$db_analysis->execute($sql);
	}
	
	function customer_save_mobile_amazon_template()
	{
		global $db_analysis;
	
		$title = $_POST["title"];
		$categoryidx = $_POST["categoryidx"];
		$contents = $_POST["contents"];
	
		if ($title == "" || $categoryidx == "" || $contents == "")
			return "필수 항목이 빠졌습니다.";
	
		$sql = "INSERT INTO support_amazon_qa_template(title,categoryidx,contents,writedate) VALUES ('$title',$categoryidx,'$contents',NOW())";
		$db_analysis->execute($sql);
	}
	
	function customer_update_mobile_ios_template()
	{
		global $db_analysis;
		 
		$templateidx = $_POST["templateidx"];
		$title = $_POST["title"];
		$categoryidx = $_POST["categoryidx"];
		$contents = $_POST["contents"];
		 
		if ($templateidx == "" || $title == "" || $categoryidx == "" || $contents == "")
			return "필수 항목이 빠졌습니다.";
		 
		$templateidx = $db_analysis->getvalue("SELECT templateidx FROM support_mobile_qa_template WHERE templateidx=$templateidx");
		 
		if ($templateidx == "")
			return "존재하지 않는 QA 답변 템플릿입니다.";
		 
		$sql = "UPDATE support_mobile_qa_template SET title='$title',contents='$contents',categoryidx=$categoryidx WHERE templateidx=$templateidx";
		 
		$db_analysis->execute($sql);
	}
	
	function customer_update_mobile_android_template()
	{
		global $db_analysis;
	
		$templateidx = $_POST["templateidx"];
		$title = $_POST["title"];
		$categoryidx = $_POST["categoryidx"];
		$contents = $_POST["contents"];
	
		if ($templateidx == "" || $title == "" || $categoryidx == "" || $contents == "")
			return "필수 항목이 빠졌습니다.";
	
		$templateidx = $db_analysis->getvalue("SELECT templateidx FROM support_android_qa_template WHERE templateidx=$templateidx");
	
		if ($templateidx == "")
			return "존재하지 않는 QA 답변 템플릿입니다.";
	
		$sql = "UPDATE support_android_qa_template SET title='$title',contents='$contents',categoryidx=$categoryidx WHERE templateidx=$templateidx";
	
		$db_analysis->execute($sql);
	}
	
	function customer_update_mobile_amazon_template()
	{
		global $db_analysis;
	
		$templateidx = $_POST["templateidx"];
		$title = $_POST["title"];
		$categoryidx = $_POST["categoryidx"];
		$contents = $_POST["contents"];
	
		if ($templateidx == "" || $title == "" || $categoryidx == "" || $contents == "")
			return "필수 항목이 빠졌습니다.";
	
		$templateidx = $db_analysis->getvalue("SELECT templateidx FROM support_amazon_qa_template WHERE templateidx=$templateidx");
	
		if ($templateidx == "")
			return "존재하지 않는 QA 답변 템플릿입니다.";
	
		$sql = "UPDATE support_amazon_qa_template SET title='$title',contents='$contents',categoryidx=$categoryidx WHERE templateidx=$templateidx";
	
		$db_analysis->execute($sql);
	}
	
	function customer_delete_mobile_ios_template()
	{
		global $db_analysis;
	
		$templateidx = $_POST["templateidx"];
		 
		if ($templateidx == "")
			return "필수 항목이 빠졌습니다.";
		 
		$templateidx = $db_analysis->getvalue("SELECT templateidx FROM support_mobile_qa_template WHERE templateidx=$templateidx");
	
		if ($templateidx == "")
			return "존재하지 않는 QA 답변 템플릿입니다.";
	
		$sql = "DELETE FROM support_mobile_qa_template WHERE templateidx=$templateidx";
	
		$db_analysis->execute($sql);
	}
	
	function customer_delete_mobile_android_template()
	{
		global $db_analysis;
	
		$templateidx = $_POST["templateidx"];
	
		if ($templateidx == "")
			return "필수 항목이 빠졌습니다.";
	
		$templateidx = $db_analysis->getvalue("SELECT templateidx FROM support_android_qa_template WHERE templateidx=$templateidx");
	
		if ($templateidx == "")
			return "존재하지 않는 QA 답변 템플릿입니다.";
	
		$sql = "DELETE FROM support_android_qa_template WHERE templateidx=$templateidx";
	
		$db_analysis->execute($sql);
	}
	
	function customer_delete_mobile_amazon_template()
	{
		global $db_analysis;
	
		$templateidx = $_POST["templateidx"];
	
		if ($templateidx == "")
			return "필수 항목이 빠졌습니다.";
	
		$templateidx = $db_analysis->getvalue("SELECT templateidx FROM support_amazon_qa_template WHERE templateidx=$templateidx");
	
		if ($templateidx == "")
			return "존재하지 않는 QA 답변 템플릿입니다.";
	
		$sql = "DELETE FROM support_amazon_qa_template WHERE templateidx=$templateidx";
	
		$db_analysis->execute($sql);
	}
	 
	function customer_move_mobile_ios_category()
	{
		global $db_analysis;
		 
		$categoryidx = $_POST["categoryidx"];
		$movetype = $_POST["movetype"];
		 
		if ($categoryidx == "" || $movetype == "")
			return "필수 항목이 빠졌습니다.";
		 
		$seqno = $db_analysis->getvalue("SELECT seqno FROM support_mobile_category WHERE categoryidx=$categoryidx");
		 
		if ($seqno == "")
			return "존재하지 않는 QA 분류입니다.";
		 
		if ($movetype == "up")
		{
			$sql = "SELECT categoryidx,seqno FROM support_mobile_category WHERE seqno<$seqno ORDER BY seqno DESC LIMIT 1";
			$data = $db_analysis->getarray($sql);
			 
			$prev_categoryidx = $data["categoryidx"];
			$prev_seqno = $data["seqno"];
			 
			if ($prev_categoryidx != "")
			{
				$sql = "UPDATE support_mobile_category SET seqno=$prev_seqno WHERE categoryidx=$categoryidx;".
						"UPDATE support_mobile_category SET seqno=$seqno WHERE categoryidx=$prev_categoryidx";
	
				$db_analysis->execute($sql);
			}
		}
		else
		{
			$sql = "SELECT categoryidx,seqno FROM support_mobile_category WHERE seqno>$seqno ORDER BY seqno ASC LIMIT 1";
			$data = $db_analysis->getarray($sql);
			 
			$next_categoryidx = $data["categoryidx"];
			$next_seqno = $data["seqno"];
			 
			if ($next_categoryidx != "")
			{
				$sql = "UPDATE support_mobile_category SET seqno=$next_seqno WHERE categoryidx=$categoryidx;".
						"UPDATE support_mobile_category SET seqno=$seqno WHERE categoryidx=$next_categoryidx";
	
				$db_analysis->execute($sql);
			}
		}
	}
	
	function customer_move_mobile_android_category()
	{
		global $db_analysis;
	
		$categoryidx = $_POST["categoryidx"];
		$movetype = $_POST["movetype"];
	
		if ($categoryidx == "" || $movetype == "")
			return "필수 항목이 빠졌습니다.";
	
		$seqno = $db_analysis->getvalue("SELECT seqno FROM support_android_category WHERE categoryidx=$categoryidx");
	
		if ($seqno == "")
			return "존재하지 않는 QA 분류입니다.";
	
		if ($movetype == "up")
		{
			$sql = "SELECT categoryidx,seqno FROM support_android_category WHERE seqno<$seqno ORDER BY seqno DESC LIMIT 1";
			$data = $db_analysis->getarray($sql);
			 
			$prev_categoryidx = $data["categoryidx"];
			$prev_seqno = $data["seqno"];
			 
			if ($prev_categoryidx != "")
			{
				$sql = "UPDATE support_android_category SET seqno=$prev_seqno WHERE categoryidx=$categoryidx;".
						"UPDATE support_android_category SET seqno=$seqno WHERE categoryidx=$prev_categoryidx";
	
				$db_analysis->execute($sql);
			}
		}
		else
		{
			$sql = "SELECT categoryidx,seqno FROM support_android_category WHERE seqno>$seqno ORDER BY seqno ASC LIMIT 1";
			$data = $db_analysis->getarray($sql);
			 
			$next_categoryidx = $data["categoryidx"];
			$next_seqno = $data["seqno"];
			 
			if ($next_categoryidx != "")
			{
				$sql = "UPDATE support_android_category SET seqno=$next_seqno WHERE categoryidx=$categoryidx;".
						"UPDATE support_android_category SET seqno=$seqno WHERE categoryidx=$next_categoryidx";
	
				$db_analysis->execute($sql);
			}
		}
	}
	
	function customer_move_mobile_amazon_category()
	{
		global $db_analysis;
	
		$categoryidx = $_POST["categoryidx"];
		$movetype = $_POST["movetype"];
	
		if ($categoryidx == "" || $movetype == "")
			return "필수 항목이 빠졌습니다.";
	
		$seqno = $db_analysis->getvalue("SELECT seqno FROM support_amazon_category WHERE categoryidx=$categoryidx");
	
		if ($seqno == "")
			return "존재하지 않는 QA 분류입니다.";
	
		if ($movetype == "up")
		{
			$sql = "SELECT categoryidx,seqno FROM support_amazon_category WHERE seqno<$seqno ORDER BY seqno DESC LIMIT 1";
			$data = $db_analysis->getarray($sql);
			 
			$prev_categoryidx = $data["categoryidx"];
			$prev_seqno = $data["seqno"];
			 
			if ($prev_categoryidx != "")
			{
				$sql = "UPDATE support_amazon_category SET seqno=$prev_seqno WHERE categoryidx=$categoryidx;".
						"UPDATE support_amazon_category SET seqno=$seqno WHERE categoryidx=$prev_categoryidx";
	
				$db_analysis->execute($sql);
			}
		}
		else
		{
			$sql = "SELECT categoryidx,seqno FROM support_amazon_category WHERE seqno>$seqno ORDER BY seqno ASC LIMIT 1";
			$data = $db_analysis->getarray($sql);
			 
			$next_categoryidx = $data["categoryidx"];
			$next_seqno = $data["seqno"];
			 
			if ($next_categoryidx != "")
			{
				$sql = "UPDATE support_amazon_category SET seqno=$next_seqno WHERE categoryidx=$categoryidx;".
						"UPDATE support_amazon_category SET seqno=$seqno WHERE categoryidx=$next_categoryidx";
	
				$db_analysis->execute($sql);
			}
		}
	}
	
	function customer_save_mobile_ios_category()
	{
		global $db_analysis;
		 
		$category = $_POST["category"];
		 
		if ($category == "")
			return "필수 항목이 빠졌습니다.";
		 
		$seqno = $db_analysis->getvalue("SELECT IFNULL(MAX(seqno)+1, 1) FROM support_mobile_category");
		 
		$sql = "INSERT INTO support_mobile_category(seqno,category) VALUES ($seqno,'$category')";
		$db_analysis->execute($sql);
	}
	
	function customer_save_mobile_android_category()
	{
		global $db_analysis;
	
		$category = $_POST["category"];
	
		if ($category == "")
			return "필수 항목이 빠졌습니다.";
	
		$seqno = $db_analysis->getvalue("SELECT IFNULL(MAX(seqno)+1, 1) FROM support_android_category");
	
		$sql = "INSERT INTO support_android_category(seqno,category) VALUES ($seqno,'$category')";
		$db_analysis->execute($sql);
	}
	
	function customer_save_mobile_amazon_category()
	{
		global $db_analysis;
	
		$category = $_POST["category"];
	
		if ($category == "")
			return "필수 항목이 빠졌습니다.";
	
		$seqno = $db_analysis->getvalue("SELECT IFNULL(MAX(seqno)+1, 1) FROM support_amazon_category");
	
		$sql = "INSERT INTO support_amazon_category(seqno,category) VALUES ($seqno,'$category')";
		$db_analysis->execute($sql);
	}
	
	function customer_update_mobile_ios_category()
	{
		global $db_analysis;
		 
		$categoryidx = $_POST["categoryidx"];
		$category = $_POST["category"];
		 
		if ($categoryidx == "" || $category == "")
			return "필수 항목이 빠졌습니다.";
		 
		$categoryidx = $db_analysis->getvalue("SELECT categoryidx FROM support_mobile_category WHERE categoryidx=$categoryidx");
		 
		if ($categoryidx == "")
			return "존재하지 않는 QA 분류입니다.";
		 
		$sql = "UPDATE support_mobile_category SET category='$category' WHERE categoryidx=$categoryidx";
		 
		$db_analysis->execute($sql);
	}
	
	function customer_update_mobile_android_category()
	{
		global $db_analysis;
	
		$categoryidx = $_POST["categoryidx"];
		$category = $_POST["category"];
	
		if ($categoryidx == "" || $category == "")
			return "필수 항목이 빠졌습니다.";
	
		$categoryidx = $db_analysis->getvalue("SELECT categoryidx FROM support_android_category WHERE categoryidx=$categoryidx");
	
		if ($categoryidx == "")
			return "존재하지 않는 QA 분류입니다.";
	
		$sql = "UPDATE support_android_category SET category='$category' WHERE categoryidx=$categoryidx";
	
		$db_analysis->execute($sql);
	}
	
	function customer_update_mobile_amazon_category()
	{
		global $db_analysis;
	
		$categoryidx = $_POST["categoryidx"];
		$category = $_POST["category"];
	
		if ($categoryidx == "" || $category == "")
			return "필수 항목이 빠졌습니다.";
	
		$categoryidx = $db_analysis->getvalue("SELECT categoryidx FROM support_amazon_category WHERE categoryidx=$categoryidx");
	
		if ($categoryidx == "")
			return "존재하지 않는 QA 분류입니다.";
	
		$sql = "UPDATE support_amazon_category SET category='$category' WHERE categoryidx=$categoryidx";
	
		$db_analysis->execute($sql);
	}
	
	function customer_delete_mobile_ios_category()
	{
		global $db_analysis;
		 
		$categoryidx = $_POST["categoryidx"];
		 
		if ($categoryidx == "")
			return "필수 항목이 빠졌습니다.";
		 
		$categoryidx = $db_analysis->getvalue("SELECT categoryidx FROM support_mobile_category WHERE categoryidx=$categoryidx");
		 
		if ($categoryidx == "")
			return "존재하지 않는 QA 분류입니다.";
		 
		$sql = "DELETE FROM support_mobile_category WHERE categoryidx=$categoryidx";
		$db_analysis->execute($sql);
	}
	
	function customer_delete_mobile_android_category()
	{
		global $db_analysis;
	
		$categoryidx = $_POST["categoryidx"];
	
		if ($categoryidx == "")
			return "필수 항목이 빠졌습니다.";
	
		$categoryidx = $db_analysis->getvalue("SELECT categoryidx FROM support_android_category WHERE categoryidx=$categoryidx");
	
		if ($categoryidx == "")
			return "존재하지 않는 QA 분류입니다.";
	
		$sql = "DELETE FROM support_android_category WHERE categoryidx=$categoryidx";
		$db_analysis->execute($sql);
	}
	
	function customer_delete_mobile_amazon_category()
	{
		global $db_analysis;
	
		$categoryidx = $_POST["categoryidx"];
	
		if ($categoryidx == "")
			return "필수 항목이 빠졌습니다.";
	
		$categoryidx = $db_analysis->getvalue("SELECT categoryidx FROM support_amazon_category WHERE categoryidx=$categoryidx");
	
		if ($categoryidx == "")
			return "존재하지 않는 QA 분류입니다.";
	
		$sql = "DELETE FROM support_amazon_category WHERE categoryidx=$categoryidx";
		$db_analysis->execute($sql);
	}
	
	function customer_update_mobileqa()
	{
		global $db_analysis;
	
		$qaidx = $_POST["qaidx"];
	
		if ($qaidx == "")
			return "필수 항목이 빠졌습니다.";
	
		$sql = "UPDATE support_mobile_qa2 SET status=1,responsedate=NOW() WHERE qaidx = '$qaidx'";
	
		$db_analysis->execute($sql);
	
		return $qaidx;
	}
	
	function customer_get_bug_issue_title()
	{
		global $db_analysis;
		 
		$type = $_POST["type"];
		 
		$sql = "SELECT * FROM support_bug_issue_title WHERE type = '$type'";
		$bug_issue_title_list = $db_analysis->gettotallist($sql);
		 
		return $bug_issue_title_list;
	}
	
	function customer_insert_bug_issue_title()
	{
		global $db_analysis;
		 
		$type = $_POST["type"];
		$title = $_POST["title"];
		 
		if($type ==  "" || $title == "")
			return "필수 항목이 빠졌습니다.";
		 
		$sql = "INSERT INTO support_bug_issue_title (type, title) VALUES($type, '$title')";
		write_log($sql);
		$db_analysis->execute($sql);
	}
	
	function customer_insert_bug_issue_report()
	{
		global $db_main,$db_analysis;
		 
		$type = $_POST["type"];
		$title = $_POST["title"];
		$qaidx = $_POST["qaidx"];
		$useridx= $_POST["useridx"];
		$platform = $_POST["platform"];
		 
		if($type == "" || $title ==  "" || $qaidx == "" || $useridx == "" || $platform == "")
			return "필수 항목이 빠졌습니다.";
		 
		$sql = "SELECT userid,nickname,coin FROM tbl_user WHERE useridx=$useridx";
		$data = $db_main->getarray($sql);
		 
		$facebookid = $data["userid"];
		$username = $data["nickname"];
		$coin = $data["coin"];
		 
		$sql = "SELECT SUM(money) AS money
				FROM
				(
					SELECT IFNULL(SUM(facebookcredit / 10),0) AS money FROM tbl_product_order WHERE STATUS=1 AND useridx=$useridx
					UNION ALL
					SELECT IFNULL(SUM(money),0) AS money FROM tbl_product_order_mobile WHERE STATUS=1 AND useridx=$useridx
				) t1";
		$money = $db_main->getvalue($sql);
		 
		$sql = "SELECT vip_level FROM tbl_user_detail WHERE useridx=$useridx";
		$vip_level = $db_main->getvalue($sql);
		 
		 
		if($platform == 0)
			$table_name = "support_qa";
		else if($platform == 1)
			$table_name = "support_mobile_qa";
		else if($platform == 2)
   			$table_name = "support_android_qa";
	   	else if($platform == 3)
   			$table_name = "support_amazon_qa";
	
	   	if($platform == 0)
   		{
			$sql = "SELECT contents FROM $table_name WHERE qaidx=$qaidx";
			$contents = $db_analysis->getvalue($sql);
			$mtype = "";
	   	}
		else
		{
			$sql = "SELECT contents, mtype FROM $table_name WHERE qaidx=$qaidx";
			$qa_arr = $db_analysis->getarray($sql);

			$contents = $qa_arr["contents"];
			$mtype = $qa_arr["mtype"];
		}
	   								 
		$contents = encode_db_field($contents);
   						 
		$sql = "INSERT INTO support_bug_issue_report(type, title, platform, useridx, facebookid, username, coin, money, member_level, mtype, contents, writedate)".
   				"VALUES($type, '$title', $platform, $useridx, '$facebookid', '$username', $coin, $money, $vip_level, '$mtype', '$contents', NOW())";
   		$db_analysis->execute($sql);
	}
	
	function customer_delete_bug_issue_report()
	{
		global $db_analysis;
		 
		$issueidx_list = $_POST["issueidx_list"];
		
   		if($issueidx_list ==  "")
	   			return "필수 항목이 빠졌습니다.";
	   			 
   		$sql = "DELETE FROM support_bug_issue_report WHERE issueidx IN ($issueidx_list)";
	   		$db_analysis->execute($sql);
	}
?>