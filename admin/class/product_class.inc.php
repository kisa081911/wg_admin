<?	
	function product_get_useridx($db_main, $facebookid, $userkey)
	{
		$info = product_get_userinfo($db_main, $facebookid, $userkey);
	
		return $info["useridx"];
	}

	function product_get_userinfo($db_main, $facebookid, $userkey)
	{
		$userinfo = $_POST["userinfo"];
		$userinfo_check = $_POST["userinfo_check"];
	
		if ($userinfo != "" && $userinfo_check != "" && md5($userinfo) == $userinfo_check)
		{
			$infos = explode("|||", base64_decode($userinfo));
				
			if ($infos[0] == $facebookid && $infos[1] == $userkey && $infos[2] != "" && $infos[3] != "")
			{
				$useridx = $infos[2];
				$nickname = $infos[3];
	
				return array("useridx" => $useridx, "nickname" => $nickname);
			}
		}
	
		$sql = "SELECT useridx,nickname FROM tbl_user WHERE userid='$facebookid' AND userkey='$userkey'";
		$data = $db_main->getarray($sql);

		$useridx = $data["useridx"];
		$nickname = $data["nickname"];
			
		return array("useridx" => $useridx, "nickname" => $nickname);
	}

	function product_get_product_info()
	{
		global $db_main, $db_main2;
		
		$facebookid = $_POST["facebookid"];
		$userkey = $_POST["userkey"];
		$productkey = $_POST["productkey"];
		
		if ($facebookid == "" || $productkey == "")
			return "parameter missing.";
		
		$useridx = product_get_useridx($db_main, $facebookid, $userkey);
		
		if ($useridx == "")
			return "user information not exists.";
		
		$sql = "SELECT productname,description,package,amount,facebookcredit,sendgift1,sendgift2,sendgift3 FROM tbl_product WHERE productkey='$productkey' AND status=1";
		
		$result = $db_main->getarray($sql);
		
		return $result;
	}
	
	function product_save_product()
	{
		global $db_main;
		
		$productname = $_POST["productname"];
		$imageurl = $_POST["imageurl"];
		$description = $_POST["description"];
		$category = $_POST["category"];
		$amount = $_POST["amount"];
		$facebookcredit = $_POST["facebookcredit"];
		$vip_point = $_POST["vip_point"];
		$special_more = $_POST["special_more"];
		$special_discount = $_POST["special_discount"];
		$status = $_POST["status"];
		$product_type = $_POST["product_type"];
		
		if($category != 3)
		{
			if ($category == "" || $productname == "" || $description == "" || $imageurl == "" || $amount == "" || $facebookcredit == "" || $vip_point == "" ||  $status == "" || $product_type == "")
			{
				return "필수 항목이 빠졌습니다";
			}
		}
		else 
		{
			if ($category == "" || $productname == "" || $description == "" || $imageurl == "" || $facebookcredit == "" ||  $status == "" || $product_type == "")
			{
				return "필수 항목이 빠졌습니다";
			}
			
			$amount = 0;
			$vip_point = 0;
		}
		$facebookcredit = $facebookcredit*10;

		check_number($category);
		check_number($amount);
		check_number($facebookcredit);
		check_number($vip_point);
		check_number($status);				
		
		//productkey 무작위 생성
		if($special_discount > 0 )
		{
			$productkey = sprintf ("%02d",$category)."_".sprintf ("%03d",$facebookcredit)."_$special_discount".$special_discountsubstr(uniqid(), -5);
		}
		else if($special_more > 0 )
		{
			$productkey = sprintf ("%02d",$category)."_".sprintf ("%03d",$facebookcredit)."_$special_more".substr(uniqid(), -5);
		}
		else
		{
			$productkey = sprintf ("%02d",$category)."_".sprintf ("%03d",$facebookcredit)."_".substr(uniqid(), -7);
		}

		
		$sql = "INSERT INTO tbl_product(productkey,category,productname,description,imageurl,amount,facebookcredit,vip_point,special_more,special_discount,status,product_type,writedate) ".
				"VALUES('$productkey',$category,'$productname','$description','$imageurl','$amount','$facebookcredit','$vip_point', $special_more, $special_discount,'$status',$product_type,NOW())";
// 		$db_main->execute($sql);
		
	}
	
	function product_update_product()
	{
		global $db_main;
		
		$productidx = $_POST["productidx"];
		$category = $_POST["category"];
		$productname = $_POST["productname"];
		$description = $_POST["description"];
		$imageurl = $_POST["imageurl"];
		$amount = $_POST["amount"];
		$facebookcredit = $_POST["facebookcredit"];
		$vip_point = $_POST["vip_point"];
		$special_more = $_POST["special_more"];
		$special_discount = $_POST["special_discount"];
		$status = $_POST["status"];
		$product_type = $_POST["product_type"];
		
		if ($productidx == "" || $category == "" || $productname == "" || $description == "" || $imageurl == "" || $amount == "" || $facebookcredit == "" || $vip_point == "" || $product_type == "")
			return "필수 항목이 빠졌습니다";
		
		check_number($productidx);
		check_number($category);
		check_number($amount);
		check_number($facebookcredit);
		check_number($vip_point);
		check_number($status);
		
		//존재하는 상품인지 확인
		$productidx = $db_main->getvalue("SELECT productidx FROM tbl_product WHERE productidx=$productidx");
		
		if ($productidx == "")
			return "잘못된 접근입니다.";
		
		//상품 정보 업데이트
		$sql = "UPDATE tbl_product SET category=$category,productname='$productname',description='$description',imageurl='$imageurl',amount=$amount,facebookcredit=$facebookcredit,vip_point=$vip_point,special_more=$special_more,special_discount=$special_discount,status=$status,product_type=$product_type ".
				"WHERE productidx=$productidx";
		
		$db_main->execute($sql);
	}
	
	function product_delete_product()
	{
		global $db_main;
		
		$productidx = $_POST["productidx"];
		
		if ($productidx == "")
			return "필수 항목이 빠졌습니다.";
		
		check_number($productidx);
		
		//존재하는 상품인지 확인
		$productidx = $db_main->getvalue("SELECT productidx FROM tbl_product WHERE productidx=$productidx");
		
		if ($productidx == "")
			return "잘못된 접근입니다.";
		
		//상품 삭제
		$sql = "UPDATE tbl_product SET status=3 WHERE productidx=$productidx";
		
		$db_main->execute($sql);
	}
	
	function product_dispute_settle()
	{
		global $db_main;
		
		$facebook = new Facebook(array(
				'appId'  => FACEBOOK_APP_ID,
				'secret' => FACEBOOK_SECRET_KEY,
				'cookie' => true,
		));
	
		$orderidx = $_POST["orderidx"];
		$orderno = $_POST["orderno"];
		$clearreason = $_POST["clearreason"];
		$settlereason = $_POST["settlereason"];
	
		if ($orderidx == "" || $orderno == "" || $clearreason == "")
			return "필수 항목이 빠졌습니다.";
	
		check_number($orderidx);
	
		try
		{
			$result = $facebook->api("/$orderno/dispute", 'POST', array("reason"=>"$settlereason"));
		}
		catch (FacebookApiException $e)
		{
			write_log($e->getMessage());
			return $e->getMessage();
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
			return $e->getMessage();
		}
	
		$sql = "UPDATE tbl_product_order_dispute SET status='$settlereason', iscomplete=3, cleardate=NOW(), clearreason='$clearreason' WHERE orderno='$orderno'";
		$db_main->execute($sql);
	}
	
	function product_dispute_refund()
	{
		global $db_main;
		
		$facebook = new Facebook(array(
				'appId'  => FACEBOOK_APP_ID,
				'secret' => FACEBOOK_SECRET_KEY,
				'cookie' => true,
		));
	
		$orderidx = $_POST["orderidx"];
		$orderno = $_POST["orderno"];
		$clearreason = $_POST["clearreason"];

		if ($orderidx == "" || $orderno == "" || $clearreason == "")
			return "필수 항목이 빠졌습니다.";
	
		check_number($orderidx);
	
		try
		{
			$order_info = $facebook->api("/$orderno?fields=refundable_amount");

			if(sizeof($order_info["refundable_amount"]) > 0)
			{
				$refund_info = $order_info["refundable_amount"];
					
				$refund_currency = $refund_info["currency"];
				$refund_amount = $refund_info["amount"];
			}
		}
		catch (FacebookApiException $e)
		{
			write_log($e->getMessage());
			return $e->getMessage();
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
			return $e->getMessage();
		}
	
		try
		{
			$result = $facebook->api("/$orderno/refunds", 'POST', array("currency"=>$refund_currency, "amount"=>$refund_amount, "reason"=>"CUSTOMER_SERVICE"));
		}
		catch (FacebookApiException $e)
		{
			write_log($e->getMessage());
			return $e->getMessage();
			
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
			return $e->getMessage();
		}
		
		$sql = "UPDATE tbl_product_order_dispute SET status='Admin Refund', iscomplete=1, cleardate=NOW(), clearreason='$clearreason' WHERE orderno='$orderno'";
		$db_main->execute($sql);
	}
	
	function product_save_product_mobile()
	{
		global $db_main;
	
		$os_type = $_POST["os_type"];
		$productname = $_POST["productname"];
		$description = $_POST["description"];
		$imageurl = $_POST["imageurl"];
		$amount = $_POST["amount"];
		$money = $_POST["money"];
		$facebookcredit = $_POST["facebookcredit"];	
		$category = $_POST["category"];
		$status = $_POST["status"];
		$productkey = $_POST["productkey"];
		$vip_point  = $_POST["vip_point"];
		$special_more  = $_POST["special_more"];
		$special_dicount  = $_POST["special_dicount"];
	
		if ($os_type == "" || $productname == "" || $description == "" || $amount == "" || $facebookcredit == "" || $category == "" || $status == "" || $productkey == "" || $vip_point == "" || $special_more == "" || $special_dicount == "")
			return "필수 항목이 빠졌습니다";
	
		check_number($category);
		check_number($amount);
		check_number($package);
		check_number($status);
		check_number($premiumday);
		check_number($sendgift1);
		check_number($sendgift2);
		check_number($sendgift3);
	
		$sql = "INSERT INTO tbl_product_mobile(productkey,os_type,productkey,category,productname,description,imageurl,amount,facebookcredit,money,vip_point,special_more,special_discount,status,writedate) 
				VALUES('$productkey',$os_type,'$productkey','$category',$productname','$description','$imageurl','$amount','$facebookcredit','$money','$vip_point','$special_more','$special_dicount','$status',NOW())";
	
		$db_main->execute($sql);
	}
	
	function product_update_product_mobile()
	{
		global $db_main;
			
		$productidx = $_POST["productidx"];
		$os_type = $_POST["os_type"];
		$productname = $_POST["productname"];
		$description = $_POST["description"];
		$imageurl = $_POST["imageurl"];
		$amount = $_POST["amount"];
		$money = $_POST["money"];
		$facebookcredit = $_POST["facebookcredit"];
		$category = $_POST["category"];
		$status = $_POST["status"];
		$productkey = $_POST["productkey"];
		$vip_point  = $_POST["vip_point"];
		$special_more  = $_POST["special_more"];
		$special_dicount  = $_POST["special_dicount"];
	
		if ($productidx == "" || $os_type == "" || $productname == "" || $description == "" || $amount == "" || $facebookcredit == "" || $category == "" || $status == "" || $productkey == "" || $vip_point == "" || $special_more == "" || $special_dicount == "")
			return "필수 항목이 빠졌습니다";
	
		check_number($productidx);
		check_number($amount);
		check_number($category);
		check_number($status);
		check_number($vip_point);
		check_number($special_more);		
		check_number($special_dicount);		
	
		//존재하는 상품인지 확인
		$productidx = $db_main->getvalue("SELECT productidx FROM tbl_product_mobile WHERE productidx=$productidx AND os_type = $os_type");
	
		if ($productidx == "")
			return "잘못된 접근입니다.";
	
		//상품 정보 업데이트
		$sql = "UPDATE tbl_product_mobile SET productkey='$productkey',category='$category',productname='$productname',description='$description',imageurl='$imageurl',amount='$amount',facebookcredit='$facebookcredit',money='$money',vip_point='$vip_point',special_more='$special_more',special_discount='$special_discount',status='$status' WHERE productidx='$productidx'";
	
		$db_main->execute($sql);
	}
	
	function product_delete_product_mobile()
	{
		global $db_main;
	
		$productidx = $_POST["productidx"];
	
		if ($productidx == "")
			return "필수 항목이 빠졌습니다.";
	
		check_number($productidx);
	
		//존재하는 상품인지 확인
		$productidx = $db_main->getvalue("SELECT productidx FROM tbl_product_mobile WHERE productidx=$productidx");
	
		if ($productidx == "")
			return "잘못된 접근입니다.";
	
		//상품 삭제
		$sql = "UPDATE tbl_product_mobile SET status=3 WHERE productidx=$productidx";
	
		$db_main->execute($sql);
	}
?>