 <?
	function user_save_free_resource()
	{
		global $db_main, $db_main2, $db_inbox;
	
		$useridx = $_POST["useridx"];		
		$free_amount = $_POST["free_amount"];
		$reason = $_POST["reason"];
		$message = $_POST["message"];
		$admin_id = $_POST["admin_id"];
		$category = $_POST["category"];
		$facebookid = $_POST["facebookid"];		
		$honorlevel = $_POST["honorlevel"];
	
		if ($useridx == "" || $free_amount == "" || $reason == "" || $message == "" || $admin_id == "" 
				|| $category == "" || $facebookid == "" || $honorlevel == "")
			return "필수 항목이 빠졌습니다.";
	
		check_number($useridx);	
	
		if($category == "coin")
		{
			if ($free_amount < 0)
			{
				// 코인 줄일 때는 inbox로 안 주고 바로 차감
	            $sql = "INSERT INTO tbl_user_cache_update(useridx,coin,writedate) VALUES($useridx,'$free_amount',NOW());\n";
				$db_main2->execute($sql);
			}
			else
			{
				// 코인 발급 (inbox에 발급하는 것으로 변경)
	            $sql = "INSERT INTO tbl_user_inbox_".($useridx%20)."(useridx,category,coin,title,writedate) VALUES($useridx,101,'$free_amount','$message',NOW());\n";
	            $db_inbox->execute($sql);
			}
		
			// 관리자 발급 로그
			$sql = "INSERT INTO tbl_freecoin_admin_log(useridx, freecoin, reason, message, admin_id, writedate) ".
					"VALUES('$useridx', $free_amount, '$reason', '$message', '$admin_id', NOW())";
			$db_main->execute($sql);
		}
		else if($category == "ty")
		{
			$sql = "SELECT nickname FROM tbl_user WHERE useridx = $useridx";
			$nickname = $db_main->getvalue($sql); 
			
			$nickname = addslashes($nickname);
			
			$sql = "UPDATE tbl_user_detail SET ty_point = ty_point + $free_amount WHERE useridx = $useridx;";
			$db_main->execute($sql);
			
			$sql = "INSERT INTO tbl_user_weekly_point (useridx, userid, nickname, weekly, ty_point, honor_level) ".
					"VALUES ($useridx, '$facebookid', '$nickname', YEARWEEK(NOW()), $free_amount, $honorlevel) ".
					"ON DUPLICATE KEY UPDATE ty_point = ty_point + $free_amount, honor_level = $honorlevel;";
			$db_main2->execute($sql);
			
			// 관리자 발급 로그
			$sql = "INSERT INTO tbl_freety_admin_log(useridx, freety, reason, message, admin_id, writedate) ".
					"VALUES('$useridx', $free_amount, '$reason', '$message', '$admin_id', NOW())";
			$db_main->execute($sql);
		}
	}
	
	function user_save_block_resource()
	{
		global $db_main, $db_main2;
	
		$useridx = $_POST["useridx"];		
		$admin_id = $_POST["admin_id"];
	
		if ($useridx == "" || $admin_id == "")
			return "필수 항목이 빠졌습니다.";
	
		check_number($useridx);	

		$coin = $db_main->getvalue("SELECT coin FROM tbl_user WHERE useridx = $useridx;");
		
		if ($coin > 0)
		{
			// 코인 줄일 때는 inbox로 안 주고 바로 차감
            		$sql = "INSERT INTO tbl_user_cache_update(useridx,coin,writedate) VALUES($useridx,'-$coin',NOW());\n";
			$db_main2->execute($sql);
			
			// 관리자 발급 로그
			$sql = "INSERT INTO tbl_freecoin_admin_log(useridx, freecoin, reason, message, admin_id, writedate) ".
					"VALUES('$useridx', -$coin, '어뷰저 코인 차감', '어뷰저 코인 차감', '$admin_id', NOW())";
			$db_main->execute($sql);
		}
	}
	
	function user_update_user_block()
	{
		global $db_main, $db_analysis, $db_friend;
	
		$useridx = $_POST["useridx"];
		$isblock = $_POST["isblock"];
		$reason = $_POST["reason"];
		$admin_id = $_POST["admin_id"];
	
		if ($useridx == "" || $isblock == "")
			return "필수 항목이 빠졌습니다.";
	
		$useridx = $db_main->getvalue("SELECT useridx FROM tbl_user_ext WHERE useridx='$useridx'");
	
		if ($useridx == "")
			return "존재하지 않는 사용자입니다.";
	
		$sql = "UPDATE tbl_user_ext SET isblock=$isblock WHERE useridx=$useridx;";
	
		if ($isblock == '1')
		{
			$sql .= "INSERT INTO tbl_user_block(useridx,blockcount,reason,blockdate) VALUES($useridx,999,'$reason',NOW()) ON DUPLICATE KEY UPDATE blockcount=999;";
			$online_sql = "DELETE FROM tbl_user_online_".($useridx%10)." WHERE useridx=$useridx;";
			$online_sql .= "DELETE FROM tbl_user_online_sync2 WHERE useridx=$useridx;";
			$history_sql = "INSERT INTO tbl_user_block_history(useridx, reason, status, adminid, writedate) VALUES($useridx, '$reason', 1, '$admin_id', NOW());";
		}
		else
		{
			$sql .= "DELETE FROM tbl_user_block WHERE useridx=$useridx";
			$history_sql = "INSERT INTO tbl_user_block_history(useridx, reason, status, adminid, writedate) VALUES($useridx, 'Remove Block', 0, '$admin_id', NOW());";
		}
	
		$db_main->execute($sql);
		$db_friend->execute($online_sql);
		$db_analysis->execute($history_sql);
	}
	
	function user_get_freecoinlog_list()
	{
		global $db_main2;
	
		$page = $_POST["page"];
		$listcount = "10";
		$useridx = $_POST["useridx"];
	
		if ($page == "" || $listcount == "" || $useridx == "")
			return "필수항목이 빠졌습니다.";
	
// 		$sql = "SELECT category, (SELECT name FROM `tbl_freecoin_type` WHERE type = t1.type) AS name, amount, writedate FROM tbl_freecoin_log_".($useridx % 10)." t1 WHERE useridx = $useridx ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
		$sql = "SELECT category, IFNULL((SELECT name FROM `tbl_total_freecoin_type` WHERE TYPE = t1.type AND inbox_type = t1.inbox_type),'inbox') AS name, amount, inbox_type,writedate  FROM tbl_user_freecoin_log_".($useridx % 10)." t1 WHERE useridx = $useridx  ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
			
		return $db_main2->gettotallist($sql);
	}
	
	function user_get_inbox_list()
	{
		global $db_livestats;
		
		$page = $_POST["page"];
		$listcount = "10";
		$useridx = $_POST["useridx"];
		
		if ($page == "" || $listcount == "" || $useridx == "")
			return "필수항목이 빠졌습니다.";
		
		$sql = "SELECT category AS inbox_type, type AS os_type, coin AS amount, writedate FROM tbl_user_inbox_collect_".($useridx % 20)." t1 WHERE useridx = $useridx AND category != 301 ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
			
		return $db_livestats->gettotallist($sql);
	}
	
	function user_get_actionlog_list()
	{
		global $db_analysis;
	
		$page = $_POST["page"];
		$listcount = "10";
		$os_type = $_POST["os_type"];
		$useridx = $_POST["useridx"];
		$userid = $_POST["userid"];
		
		$search_date = $_POST["search_date"];
		$search_hour = ($_POST["search_hour"]< 10 && $_POST["search_hour"] != "" )?"0".$_POST["search_hour"]:$_POST["search_hour"];
		$search_minute = ($_POST["search_minute"]< 10 && $_POST["search_minute"] != "")?"0".$_POST["search_minute"]:$_POST["search_minute"];
		$search_version = $_POST["search_version"];
	
		if ($os_type == "" || $page == "" || $listcount == "" || $userid == "" || $useridx == "")
			return "필수항목이 빠졌습니다.";
	
		if($search_date != '')
		{
		    if($search_hour == "")
		    {
    		    $search_datetime = $search_date;
    		    $tail = "AND DATE_FORMAT(writedate,'%Y-%m-%d') = '$search_datetime'";
		    }
    		else if($search_hour != "" && $search_minute == "")
    		{
    		    $search_datetime = $search_date." ".$search_hour;
    		    $tail = "AND DATE_FORMAT(writedate,'%Y-%m-%d %H') = '$search_datetime'";
    		}
    		else
    		{
    		    $search_datetime = $search_date." ".$search_hour.":".$search_minute;
    		    $tail = "AND DATE_FORMAT(writedate,'%Y-%m-%d %H:%i') = '$search_datetime'";
    		}		    
		}
		
		if($search_version != "")
		{
			if($search_version == 2)
				$tail .= " AND is_v2 = 1";
			else
				$tail .= " AND is_v2 = 0";
			
		}
			
		if($os_type == "web")
			$sql = "SELECT actionidx,ipaddress,facebookid,category,category_info,action,action_info,staytime,isplaynow,coin,is_v2,writedate FROM user_action_log_".($userid%10)." WHERE facebookid='$userid' $tail ORDER BY actionidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
		else if($os_type == "ios")
			$sql = "SELECT actionidx,ipaddress,useridx,category,category_info,action,action_info,staytime,isplaynow,coin,writedate FROM user_action_log_ios_".($useridx%10)." WHERE useridx='$useridx' $tail ORDER BY actionidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
		else if($os_type == "android")
			$sql = "SELECT actionidx,ipaddress,useridx,category,category_info,action,action_info,staytime,isplaynow,coin,writedate FROM user_action_log_android_".($useridx%10)." WHERE useridx='$useridx' $tail ORDER BY actionidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
		else if($os_type == "amazon")
			$sql = "SELECT actionidx,ipaddress,useridx,category,category_info,action,action_info,staytime,isplaynow,coin,writedate FROM user_action_log_amazon WHERE useridx='$useridx' $tail ORDER BY actionidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
		$date_list = $db_analysis->gettotallist($sql);
	
	
        return  $date_list;
	}
	
	function user_get_actionlog_totalcount()
	{
	    global $db_analysis;
	    
	    $os_type = $_POST["os_type"];
	    $useridx = $_POST["useridx"];
	    $userid = $_POST["userid"];
	    
	    $search_date = $_POST["search_date"];
	    $search_hour = ($_POST["search_hour"]< 10 && $_POST["search_hour"] != "" )?"0".$_POST["search_hour"]:$_POST["search_hour"];
	    $search_minute = ($_POST["search_minute"]< 10 && $_POST["search_minute"] != "")?"0".$_POST["search_minute"]:$_POST["search_minute"];
	    
	    if ($os_type == "" ||   $userid == "" || $useridx == "")
	        return "필수항목이 빠졌습니다.";
	        
        if($search_date != '')
        {
            if($search_hour == "")
            {
                $search_datetime = $search_date;
                $tail = "AND DATE_FORMAT(writedate,'%Y-%m-%d') = '$search_datetime'";
            }
            else if($search_hour != "" && $search_minute == "")
            {
                $search_datetime = $search_date." ".$search_hour;
                $tail = "AND DATE_FORMAT(writedate,'%Y-%m-%d %H') = '$search_datetime'";
            }
            else
            {
                $search_datetime = $search_date." ".$search_hour.":".$search_minute;
                $tail = "AND DATE_FORMAT(writedate,'%Y-%m-%d %H:%i') = '$search_datetime'";
            }
            
        }

        if($os_type == "web")
            $sql = "SELECT COUNT(*) FROM user_action_log_".($userid%10)." WHERE facebookid='$userid' $tail ORDER BY actionidx";
        else if($os_type == "ios")
            $sql = "SELECT COUNT(*) FROM user_action_log_ios_".($useridx%10)." WHERE useridx='$useridx' $tail ORDER BY actionidx";
        else if($os_type == "android")
            $sql = "SELECT COUNT(*) FROM user_action_log_android_".($useridx%10)." WHERE useridx='$useridx' $tail ORDER BY actionidx";
        else if($os_type == "amazon")
            $sql = "SELECT COUNT(*) FROM user_action_log_amazon WHERE useridx='$useridx' ORDER BY actionidx";
            
        $total_count = $db_analysis->getvalue($sql);
                                        
        return array("totalcount"=>$total_count);
	}
	
	function user_get_friend_list()
    {
    	global $db_friend;

    	$useridx = $_POST["useridx"];
    	$client_accesstoken = $_POST["client_accesstoken"];
    	
    	$sql = "SELECT B.facebookid, B.useridx, B.nickname, CONCAT('http://graph.facebook.com/', B.facebookid, '/picture?type=square&access_token=$client_accesstoken') AS photourl FROM tbl_user_friend_".($useridx % 20)." A JOIN tbl_user_gamedata B ON A.friendidx=B.useridx WHERE A.useridx=$useridx";
    	
    	return $db_friend->gettotallist($sql);
    }
    
    function user_update_friend()
    {
    	global $db_main, $db_friend;
    	
    	$facebook = new Facebook(array(
    			'appId'  => FACEBOOK_APP_ID,
    			'secret' => FACEBOOK_SECRET_KEY,
    			'cookie' => true,
    	));
    
    	$useridx = $_POST["useridx"];
    
    	if ($useridx == "")
    		return "필수 항목이 빠졌습니다.";
    
    	$sql = "SELECT userid FROM tbl_user WHERE useridx=$useridx";
    	$userid = $db_main->getvalue($sql);
    
    	$_SESSION['facebook_session'] = "";
    	$facebook->setAppAccessTokenUse(true);
    
    	$friends = $facebook->api("/$userid/friends?fields=id&limit=5000");
    	$friendlist = "";
    
    	if(sizeof($friends['data']) > 0)
    	{
    		$sql = "";
    		$insertcount = 0;
    		$insert_str = "";
    		$default_insert_str = "";
    			
    		// 기존 친구 목록 모두 삭제
    		$sql = "DELETE FROM tbl_user_friend_".($useridx % 20)." WHERE useridx=$useridx;";

    		for($i=0; $i<20; $i++)
    		{
    			$sql .= "DELETE FROM tbl_user_friend_".$i." WHERE friendidx=$useridx;";
    		}

    				
   			foreach ($friends['data'] as $friend)
			{
				$u_friendid = $friend['id'];

				if($insert_str == "")
					$insert_str = "$u_friendid";
				else
					$insert_str .= ",$u_friendid";
							
				if ($insertcount == 0)
				{
					$insertcount++;
				}
				else if ($insertcount < 100)
				{
					$insertcount++;
				}
				else
				{
					$insertcount = 0;
				}
			}
						
			$default_insert_str = $insert_str;
						
// 			if($insert_str == "")
// 				$insert_str = HELPER_FB_ID;
// 			else
// 				$insert_str .= ",".HELPER_FB_ID;
						
			if($insert_str != "")
			{
				$sql = "INSERT INTO tbl_user_friend_".($useridx % 20)."(useridx,friendidx,friendid) ".
						"SELECT $useridx AS useridx, useridx AS friendidx, facebookid ".
						"FROM tbl_user_gamedata t1 ".
						"WHERE facebookid IN ($insert_str) AND useridx IS NOT NULL ".
						"ON DUPLICATE KEY UPDATE friendid = t1.facebookid;";
			}
						
			if($default_insert_str != "")
			{
				// 게임을 하는 친구의 친구 목록에 나를 추가
				for($i=0; $i < 20; $i++)
				{
					$sql .= "INSERT INTO tbl_user_friend_$i(useridx, friendidx, friendid) ".
							"SELECT useridx, $useridx AS friendidx, $userid AS friendid ".
							"FROM tbl_user_gamedata ".
							"WHERE facebookid IN ($default_insert_str) ".
							"	AND useridx IS NOT NULL AND useridx % 20 = $i ".
							"ON DUPLICATE KEY UPDATE friendid=VALUES(friendid);";
				}
			}
				
			if($sql != "")
				$db_friend->execute($sql);
			
			$sql = "UPDATE tbl_user_ext SET frienddate=NOW() WHERE useridx=$useridx;";
			$db_main->execute($sql);
			
			$sql = "SELECT COUNT(*) FROM tbl_user_friend_".($useridx%20)." WHERE useridx=$useridx";
			$friend_count = $db_friend->getvalue($sql);
			
			$sql = "UPDATE tbl_user_detail SET friend_count=$friend_count WHERE useridx=$useridx";
			$db_main->execute($sql);
			
			$sql = "UPDATE tbl_user_detail SET friend_count=$friend_count WHERE useridx=$useridx";
			$db_friend->execute($sql);
    	}
    }
    
    function user_get_user_event_list()
    {
    	global $db_main2;
    
    	$page = $_POST["page"];
    	$listcount = "10";
    	$useridx = $_POST["useridx"];
    
    	if ($page == "" || $listcount == "" || $useridx == "")
    		return "필수항목이 빠졌습니다.";
    
    	$sql = "SELECT eventidx, (SELECT category FROM tbl_event WHERE eventidx=tbl_event_result.eventidx) AS category, (SELECT title FROM tbl_event WHERE eventidx=tbl_event_result.eventidx) AS title, reward_type, reward_amount, ".
    			"writedate FROM tbl_event_result WHERE useridx=$useridx ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    	return $db_main2->gettotallist($sql);
    }
    
    function user_get_jackpot_list()
    {
    	global $db_main;
    
    	$page = $_POST["page"];
    	$listcount = "10";
    	$useridx = $_POST["useridx"];
    
    	if ($page == "" || $listcount == "" || $useridx == "")
    		return "필수항목이 빠졌습니다.";
    
    	$sql = "SELECT devicetype, slottype, amount, fiestaidx, (SELECT owner FROM `tbl_jackpot_hall_member` WHERE jackpothallidx = t1.jackpothallidx AND useridx = t1.useridx) AS jackpot_owner, ".
				"(SELECT COUNT(*) FROM tbl_jackpot_hall_member WHERE jackpothallidx = t1.jackpothallidx) AS jackpot_member_count, ".
				"(SELECT jackpotamount FROM tbl_jackpot_hall WHERE jackpothallidx = t1.jackpothallidx) AS total_jackpot_amount, ".
				"writedate ".
				"FROM tbl_jackpot_log t1 ".
				"WHERE useridx=$useridx ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    	return $db_main->gettotallist($sql);
    }
    
    function user_get_bigwin_list()
    {
    	global $db_main2;
    
    	$page = $_POST["page"];
    	$listcount = "10";
    	$useridx = $_POST["useridx"];
    
    	if ($page == "" || $listcount == "" || $useridx == "")
    		return "필수항목이 빠졌습니다.";
    
    	$sql = "SELECT slottype,objectidx,amount,grade,writedate FROM tbl_bigwin_log WHERE useridx=$useridx ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    	return $db_main2->gettotallist($sql);
    }
    
    function user_get_user_unkown_coin_list()
    {
    	global $db_main;
    	
    	$page = $_POST["page"];
    	$listcount = "10";
    	$useridx = $_POST["useridx"];
    	
    	if ($page == "" || $listcount == "" || $useridx == "")
    		return "필수항목이 빠졌습니다.";
    	
    	$sql = "SELECT coin_change,category,writedate FROM tbl_unknown_coin WHERE useridx=$useridx ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    	
    	return $db_main->gettotallist($sql);
    }
    
    function user_get_free_resource_list()
    {
    	global $db_main;
    	
    	$page = $_POST["page"];
    	$listcount = "10";
    	$useridx = $_POST["useridx"];
    	
    	if ($page == "" || $listcount == "" || $useridx == "")
    		return "필수항목이 빠졌습니다.";
    	
    	$sql = "SELECT freecoin, reason, message, writedate FROM tbl_freecoin_admin_log WHERE useridx=$useridx ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    	
    	return $db_main->gettotallist($sql);
    }
    
    function user_update_daily_inflowrout_cost()
    {
    	global $db_analysis;
    
    	$type = $_POST["type"];
    	$writedate = $_POST["writedate"];
    	$cost_value = $_POST["cost_value"];
    
    	for($i=0; $i<sizeof($cost_value); $i++)
    	{
	    	$day = $i + 1;
	    	$day = str_pad( $day, 2, 0, STR_PAD_LEFT );
	    	$today = $writedate.$day;
	    		
	    	$sql = "INSERT INTO `tbl_user_inflowroute_cost` (`today`, `type`, `cost`) VALUES('$today', $type, $cost_value[$i]) ON DUPLICATE KEY UPDATE cost = VALUES(cost);";
	    	$db_analysis->execute($sql);
    	}
    }
    
    function user_get_slotdetail_list()
    {
    	global $db_other;
    	 
    	$page = $_POST["page"];
    	$listcount = "10";
    	$os_type = $_POST["os_type"];
    	$useridx = $_POST["useridx"];
    	 
    	if ($page == "" || $listcount == "" || $os_type == "" || $useridx == "")
    		return "필수항목이 빠졌습니다.";
    	 
    	if($os_type == "web")
    	{
    		$sql = "SELECT slottype, moneyin, moneyout, ROUND(moneyout/moneyin*100, 2) AS slot_winrate, writedate FROM tbl_user_gamelog WHERE useridx=$useridx AND MODE NOT IN (4, 5, 9, 26, 29) AND slottype != 0 ".
    				"AND DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 7 DAY), '%Y-%m-%d') <= writedate AND writedate <= NOW() ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    	}
    	else if($os_type == "ios")
    	{
    		$sql = "SELECT slottype, moneyin, moneyout, ROUND(moneyout/moneyin*100, 2) AS slot_winrate, writedate FROM tbl_user_gamelog_ios WHERE useridx=$useridx AND MODE NOT IN (4, 5, 9, 26, 29) AND slottype != 0 ".
    				"AND DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 7 DAY), '%Y-%m-%d') <= writedate AND writedate <= NOW() ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    	}
    	else if($os_type == "android")
    	{
    		$sql = "SELECT slottype, moneyin, moneyout, ROUND(moneyout/moneyin*100, 2) AS slot_winrate, writedate FROM tbl_user_gamelog_android WHERE useridx=$useridx AND MODE NOT IN (4, 5, 9, 26, 29) AND slottype != 0 ".
    				"AND DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 7 DAY), '%Y-%m-%d') <= writedate AND writedate <= NOW() ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    	}    	
    	else if($os_type == "amazon")
    	{
    		$sql = "SELECT slottype, moneyin, moneyout, ROUND(moneyout/moneyin*100, 2) AS slot_winrate, writedate FROM tbl_user_gamelog_amazon WHERE useridx=$useridx AND MODE NOT IN (4, 5, 9, 26, 29) AND slottype != 0 ".
    				"AND DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 7 DAY), '%Y-%m-%d') <= writedate AND writedate <= NOW() ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    	}
    	
    	return $db_other->gettotallist($sql);
    }
    
    function user_get_uuid_list()
    {
    	global $db_mobile,$db_main;
    
    	$page = $_POST["page"];
    	$listcount = "10";
    	$uuid = $_POST["uuid"];
    
    	if ($page == "" || $listcount == "" || $uuid == "")
    		return "필수항목이 빠졌습니다.";
    
    	$sql = "SELECT useridx FROM `tbl_mobile` t1 JOIN `tbl_user_mobile_connection_log` t2 ON t1.device_id = t2.device_id ".
    			"WHERE uuid = '$uuid' ORDER BY logindate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    	$uuid_data = $db_mobile->gettotallist($sql);
    
    	$user_list = "";
    
    	for($i=0; $i<sizeof($uuid_data); $i++)
    	{
	    	if($user_list == "")
	    		$user_list = $uuid_data[$i]["useridx"];
	    	else
	    		$user_list .= ", ".$uuid_data[$i]["useridx"];
    	}
    
        $sql = "SELECT t1.useridx, t1.userid, t1.nickname, t1.coin, t1.level, t2.logindate, t1.createdate FROM tbl_user t1 LEFT JOIN tbl_user_ext t2 ".
				"ON t1.useridx = t2.useridx WHERE t1.useridx in ($user_list) ORDER BY t1.createdate DESC";    
    	return $db_main->gettotallist($sql);
    }
	
	function user_update_agency_spend()
    {
    	global $db_main2;
    	
    	$basic_date = $_POST["basic_date"];
    	$platform = $_POST["platform"];
    	$agency_index = $_POST["agency_index"];
    	$spend_value = $_POST["spend_value"];
	$spend_minus_value = $_POST["spend_minus_value"];
	
	if($basic_date == "" || $platform == "" || $agency_index == "" || $spend_value == "")
	    		return "필수항목이 빠졌습니다.";
    	
    	for($i=0; $i<sizeof($spend_value); $i++)
    	{
    		$day = $i + 1;
    		$day = str_pad($day, 2, 0, STR_PAD_LEFT);
    		$today = $basic_date.'-'.$day;
    		
    		$spend_origin = $spend_value[$i];
    		$spend_minus = $spend_minus_value[$i];
    		$spend = $spend_origin - $spend_minus;
    		
    		$sql = " INSERT INTO tbl_agency_spend_daily(today, platform, agencyidx, spend, spend_origin, spend_minus) ".
        		   " VALUES('$today', $platform, $agency_index, $spend, $spend_origin, $spend_minus) ".
        		   " ON DUPLICATE KEY UPDATE spend = VALUES(spend), spend_origin = VALUES(spend_origin), spend_minus = VALUES(spend_minus);";
    		$db_main2->execute($sql);
    	}
    }
	function user_get_agency_spend()
    {
    	global $db_main2;
    	
    	$basic_date = $_POST["basic_date"];
    	$platform = $_POST["platform"];
    	$agency_index = $_POST["agency_index"];
    	
    	// 해당 년월 데이터가 없으면 default value DB 생성
    	$basic_date_tmp = explode("-", $basic_date);
    	$basic_year = $basic_date_tmp[0];
    	$basic_month = $basic_date_tmp[1];
    	 
    	$last_day = date("t", mktime(0, 0, 1, $basic_month, 1, $basic_year));
    	
    	if($agency_index == "8")
    		$agency_name = "Facebook Ads";
    	else if($agency_index == "2")
    		$agency_name = "vungle_int";
    	else if($agency_index == "10")
    		$agency_name = "258angel_int";
    	else if($agency_index == "11")
    		$agency_name = "amazon";
    	else if($agency_index == "12")
    		$agency_name = "cheetahmobile_int";
    	else if($agency_index == "13")
    		$agency_name = "mobvista_int";
    	else if($agency_index == "14")
    		$agency_name = "yeahmobi_int";
    	else if($agency_index == "15")
    		$agency_name = "socialclicks";
    	else if($agency_index == "16")
    		$agency_name = "adaction_int";
    	else if($agency_index == "17")
    		$agency_name = "Twitter Ads";
    	else if($agency_index == "18")
    		$agency_name = "taptica_int";
    	else if($agency_index == "19")
    		$agency_name = "glispa_int";
    	else if($agency_index == "22")
    		$agency_name = "googleadwords_int";
    	else if($agency_index == "23")
    		$agency_name = "moloco_int";
    	else if($agency_index == "24")
    		$agency_name = "heyzap_int";
    	else if($agency_index == "26")
    		$agency_name = "crossinstall_int";
    	else if($agency_index == "29")
    		$agency_name = "iconpeak_int";
    	else if($agency_index == "30")
    		$agency_name = "dqna_int";
    	else if($agency_index == "31")
    		$agency_name = "Apple Search Ads";
		else if($agency_index == "32")
    		$agency_name = "amazon";
		else if($agency_index == "36")
    		$agency_name = "liftoff_int";
		else if($agency_index == "37")
    		$agency_name = "m_ddi_int";
		else if($agency_index == "38")
    		$agency_name = "m_duc_int";
    	
    	$sql = "SELECT * FROM tbl_agency_spend_daily WHERE agencyidx = $agency_index AND SUBSTR(today, 1, 7) = '$basic_date' and platform = $platform";
    		
    	if($db_main2->getvalue($sql) == "")
    	{
    		for($j=0; $j<$last_day; $j++)
    		{
    			if($j < 10)
    				$basic_day = '0'.$j+1;
    			else
    				$basic_day = $j+1;
    			
    			if($platform == 1 || $platform == 2)
    			{
	    			$sql = "INSERT INTO tbl_agency_spend_daily VALUES('$basic_year-$basic_month-$basic_day', '1', '$agency_index', '$agency_name', '0','0','0');";
	    			$sql .= "INSERT INTO tbl_agency_spend_daily VALUES('$basic_year-$basic_month-$basic_day', '2', '$agency_index', '$agency_name', '0','0','0');";
    			}
    			else if($platform == 3)
    				$sql = "INSERT INTO tbl_agency_spend_daily VALUES('$basic_year-$basic_month-$basic_day', '3', '$agency_index', '$agency_name', '0','0','0');";
    			
    			$db_main2->execute($sql);
    		}
    	}
    	
    	$sql = "SELECT * FROM tbl_agency_spend_daily WHERE SUBSTR(today, 1, 7) = '$basic_date' AND platform = $platform AND agencyidx = $agency_index ORDER BY today ASC";
    	return $db_main2->gettotallist($sql);
    }
    function user_get_actionlog_blackbox()
    {
    	global $db_main, $db_analysis;
       	 
    	$redmine = $_POST["redmine"];
    	$useridx = $_POST["useridx"];
    	$os = $_POST["os"];
    	//$writedate_date = $_POST["writedate_date"];
    	//$writedate_hour = $_POST["writedate_hour"];
    	//$writedate_min = $_POST["writedate_min"];
    
    	if($redmine == "" || $useridx == "" || $os == "" )
    		return "필수 항목이 빠졌습니다.";
    
    		//$writedate = $writedate_date." ".$writedate_hour.":".$writedate_min.":00";
    
    		$sql = "SELECT userid FROM tbl_user WHERE useridx = $useridx";
    
    		$facebookid = $db_main->getvalue($sql);
    
    		if($os == 0)
    			$db_table = "user_action_log_".($facebookid%10);
    		else if($os == 1)
    			$db_table = "user_action_log_ios_".($useridx%10);
    		else if($os == 2)
    			$db_table = "user_action_log_android_".($useridx%10);
    		else if($os == 3)
    			$db_table = "user_action_log_amazon";
    
    		if($os == 0)
	    		$sql = "SELECT $redmine AS redmine_number, ipaddress, facebookid, category, category_info, action, action_info, staytime, isplaynow, coin, $os AS os, writedate FROM $db_table WHERE facebookid = '$facebookid'";
    		else
    			$sql = "SELECT $redmine AS redmine_number, ipaddress, useridx, category, category_info, action, action_info, staytime, isplaynow, coin, $os AS os, writedate FROM $db_table WHERE useridx = '$useridx'";
    		return $db_analysis->gettotallist($sql);
    						 
    }
    
    function user_insert_actionlog_blackbox()
    {
    	global $db_main, $db_analysis, $db_other;
    	 
    	$redmine = $_POST["redmine"];
    	$useridx = $_POST["useridx"];
    	$os = $_POST["os"];
    	//$writedate_date = $_POST["writedate_date"];
    	//$writedate_hour = $_POST["writedate_hour"];
    	//$writedate_min = $_POST["writedate_min"];
    	 
    	if($redmine == "" || $useridx == "" || $os == "")
    		return "필수 항목이 빠졌습니다.";
    		 
    		//$writedate = $writedate_date." ".$writedate_hour.":".$writedate_min.":00";
    		 
    		$sql = "SELECT userid FROM tbl_user WHERE useridx = $useridx";
    		$facebookid = $db_main->getvalue($sql);
    		 
    		if($os == 0)
    		{
    			$action_table = "user_action_log_".($facebookid%10);
    			$game_table = "tbl_user_gamelog";
    			$client_table = "client_communication_log";
    		}
    		else if($os == 1)
    		{
    			$action_table = "user_action_log_ios_".($useridx%10);
    			$game_table = "tbl_user_gamelog_ios";
    			$client_table = "client_communication_log_ios";
    		}
    		else if($os == 2)
    		{
    			$action_table = "user_action_log_android_".($useridx%10);
    			$game_table = "tbl_user_gamelog_android";
    			$client_table = "client_communication_log_android";
    		}
    		else if($os == 3)
    		{
    			$action_table = "user_action_log_amazon";
    			$game_table = "tbl_user_gamelog_amazon";
    			$client_table = "client_communication_log_amazon";
    		}
    		
    		if($os == 0)
    		{
	    		$action_sql = "INSERT INTO user_action_log_blackbox(redmine_number, useridx, facebookid, ipaddress, category, category_info, action, action_info, staytime, isplaynow, coin, os, writedate) ".
	    				"(SELECT $redmine AS redmine_number, $useridx AS useridx, facebookid, ipaddress, category, category_info, action, action_info, staytime, isplaynow, coin, $os AS os, writedate FROM $action_table WHERE facebookid = '$facebookid')";
    		}
    		else 
    		{
    			$action_sql = "INSERT INTO user_action_log_blackbox(redmine_number, useridx, facebookid, ipaddress, category, category_info, action, action_info, staytime, isplaynow, coin, os, writedate) ".
    					"(SELECT $redmine AS redmine_number, $useridx AS useridx, $facebookid as facebookid, ipaddress, category, category_info, action, action_info, staytime, isplaynow, coin, $os AS os, writedate FROM $action_table WHERE useridx = '$useridx')";
    		}
    
    		$db_analysis->execute($action_sql);
    		 
    		$game_sql = "INSERT INTO tbl_user_gamelog_blackbox(redmine_number, useridx, subtype, money_in, money_out, os, writedate) ".
    				"(SELECT $redmine AS redmine_number, useridx, slottype, moneyin, moneyout, $os AS os, writedate FROM $game_table WHERE useridx = $useridx) ";
    		$db_other->execute($game_sql);
    		 
    		if($os == 0)
    		{
    			$client_sql = "INSERT INTO client_communication_log_blackbox(redmine_number, useridx, ipaddress, facebookid, cmd, errcode, latency, os, writedate) ".
    					"(SELECT $redmine AS redmine_number, $useridx AS useridx, ipaddress, facebookid, cmd, errcode, latency, $os AS os, writedate FROM $client_table WHERE facebookid = '$facebookid')";
    		}
    		else
    		{
    			$client_sql = "INSERT INTO client_communication_log_blackbox(redmine_number, useridx, ipaddress, facebookid, cmd, errcode, latency, os, writedate) ".
    				"(SELECT $redmine AS redmine_number, $useridx AS useridx, ipaddress, $facebookid as facebookid, cmd, errcode, latency, $os AS os, writedate FROM $client_table WHERE useridx = '$useridx')";
    		}
    		$db_analysis->execute($client_sql);
    }
    
    function user_get_blackbox_action_list()
    {
    	global $db_analysis, $totalcount1;
    
    	$page = $_POST["page"];
    	$redmine = $_POST["redmine"];
    	$useridx = $_POST["useridx"];
    	$os = $_POST["os"];
    	$listcount = "12";
    	 
    	if ($page == "" || $listcount == "" || $redmine == "" || $useridx == "" || $os == "")
    		return "필수항목이 빠졌습니다.";
    		 
    		$tail = "WHERE redmine_number=$redmine AND useridx = $useridx AND os = $os";
    		 
    		$totalcount1 = $db_analysis->getvalue("SELECT COUNT(*) FROM user_action_log_blackbox $tail");
    		 
    		$sql = "SELECT actionidx,ipaddress,facebookid,category,category_info,action,action_info,staytime,isplaynow,coin,writedate FROM user_action_log_blackbox $tail ORDER BY actionidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    		return $db_analysis->gettotallist($sql);
    }
    
    function user_get_blackbox_game_list()
    {
    	global $db_other, $totalcount1;
    
    	$page = $_POST["page"];
    	$redmine = $_POST["redmine"];
    	$useridx = $_POST["useridx"];
    	$os = $_POST["os"];
    	$listcount = "12";
    
    	if ($page == "" || $listcount == "" || $redmine == "" || $useridx == "" || $os == "")
    		return "필수항목이 빠졌습니다.";
    
    		$tail = "WHERE redmine_number=$redmine AND useridx = $useridx AND os = $os";
    
    		$totalcount1 = $db_other->getvalue("SELECT COUNT(*) FROM tbl_user_gamelog_blackbox $tail");
    
    		$sql = "SELECT logidx, useridx, subtype, money_in, money_out, os, writedate FROM tbl_user_gamelog_blackbox $tail ORDER BY logidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    		return $db_other->gettotallist($sql);
    }
    
    function user_get_blackbox_client_list()
    {
    	global $db_analysis, $totalcount1;
    
    	$page = $_POST["page"];
    	$redmine = $_POST["redmine"];
    	$useridx = $_POST["useridx"];
    	$os = $_POST["os"];
    	$listcount = "12";
    	if ($page == "" || $listcount == "" || $redmine == "" || $useridx == "" || $os == "")
    		return "필수항목이 빠졌습니다.";
    
    		$tail = "WHERE redmine_number=$redmine AND useridx = $useridx AND os = $os";
    
    		$totalcount1 = $db_analysis->getvalue("SELECT COUNT(*) FROM client_communication_log_blackbox $tail");
    
    		$sql = "SELECT logidx, useridx, ipaddress, facebookid, cmd, errcode, latency, os, writedate FROM client_communication_log_blackbox $tail ORDER BY logidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    		return $db_analysis->gettotallist($sql);
    }
    
    function user_delete_blackbox()
    {
    	global $db_analysis, $db_other;
    	 
    	$redmine = $_POST["redmine"];
    	 
    	if ($redmine == "")
    		return "필수항목이 빠졌습니다.";
    		 
    		$sql = "DELETE FROM user_action_log_blackbox WHERE redmine_number = $redmine";
    		$db_analysis->execute($sql);
    		 
    		$sql = "DELETE FROM client_communication_log_blackbox WHERE redmine_number = $redmine";
    		$db_analysis->execute($sql);
    		 
    		$sql = "DELETE FROM tbl_user_gamelog_blackbox WHERE redmine_number = $redmine";
    		$db_other->execute($sql);
    }
    
    function user_update_user_uuid_block()
    {
    	global $db_main, $db_analysis, $db_friend, $db_mobile;
    
    	$useridx = $_POST["useridx"];
    	$isblock = $_POST["isblock"];
    	$reason = $_POST["reason"];
    	$admin_id = $_POST["admin_id"];
    
    	if ($useridx == "" || $isblock == "")
    		return "필수 항목이 빠졌습니다.";
    
    	$useridx = $db_main->getvalue("SELECT useridx FROM tbl_user_ext WHERE useridx='$useridx'");
    
    	if ($useridx == "")
    		return "존재하지 않는 사용자입니다.";
    
    	$sql = "SELECT UUID FROM `tbl_mobile` t1 JOIN `tbl_user_mobile_connection_log` t2 ON t1.device_id = t2.device_id WHERE useridx = $useridx and uuid !='' ";
    	$uuid_list = $db_mobile->gettotallist($sql);
    
    	for($u=0; $u<sizeof($uuid_list);$u++)
    	{
    		$uuid= $uuid_list[$u]['UUID'];
    		
    		$sql = " SELECT DISTINCT useridx FROM `tbl_mobile` t1 JOIN `tbl_user_mobile_connection_log` t2 ON t1.device_id = t2.device_id ". 
							" WHERE UUID = '$uuid'";
    		$useridx_list = $db_mobile->gettotallist($sql);
    		
    		$sql= "";
    		$online_sql = "";
    		$history_sql = "";
    		for($h=0; $h<sizeof($useridx_list);$h++)
    		{
    			$block_useridx = $useridx_list[$h]['useridx'];
    			
		    	if ($isblock == '1')
		    	{
		    		$sql .= " UPDATE tbl_user_ext SET isblock=$isblock WHERE useridx=$block_useridx; ";
		    		$sql .= " INSERT INTO tbl_user_block(useridx,blockcount,reason,blockdate) VALUES($block_useridx,999,'$reason',NOW()) ON DUPLICATE KEY UPDATE blockcount=999; ";
					$online_sql .= " DELETE FROM tbl_user_online_".($block_useridx%10)." WHERE useridx=$block_useridx; ";
		    		$online_sql .= " DELETE FROM tbl_user_online_sync2 WHERE useridx=$block_useridx; ";
		    		$history_sql .= " INSERT INTO tbl_user_block_history(useridx, reason, status, adminid, writedate) VALUES($block_useridx, '$reason', 1, '$admin_id', NOW()); ";
		    	}
		    	else
		    	{
		    		$sql .= " UPDATE tbl_user_ext SET isblock=$isblock WHERE useridx=$block_useridx; ";
		    		$sql .= " DELETE FROM tbl_user_block WHERE useridx=$block_useridx; ";
		    		$history_sql .= " INSERT INTO tbl_user_block_history(useridx, reason, status, adminid, writedate) VALUES($block_useridx, 'Remove Block', 0, '$admin_id', NOW()); ";
		    	}
			
    		}
    		
    		if ($isblock == '1')
    			$uuid_sql = " INSERT INTO tbl_block_device(uuid) VALUES('$uuid'); ";
    		else 
    			$uuid_sql = " DELETE FROM tbl_block_device WHERE uuid='$uuid' ";
    		
    		$db_mobile->execute($uuid_sql);
    		
    		if ($sql != "")
    		$db_main->execute($sql);
    		
    		if ($online_sql != "")
    		$db_friend->execute($online_sql);
    		
    		if ($history_sql != "")
    		$db_analysis->execute($history_sql);
    		
    		
    	}
    }
    
 function user_send_a2u()
    {
        global $db_main, $db_slave, $db_main2, $facebook;
        
        $useridx = $_POST["useridx"];
        $fb_id = $_POST["fb_id"];
        $fb_a2u_msg = $_POST["fb_a2u_msg"];
        $fb_a2u_txt = $_POST["fb_a2u_txt"];
        $send_type = $_POST["send_type"];
        $emoji_code = $_POST["emoji_code"];
        $bundle_flag = $_POST["bundle_flag"];
        $sql_money = $_POST["sql_money"];
        $sql_leavedays = $_POST["sql_leavedays"];
        $sql_contactdays = $_POST["sql_contactdays"];
        $reservation_date = $_POST["reservation_date"];
        $reservation_hour = $_POST["reservation_hour"];
        $reservation_minute = $_POST["reservation_minute"];
        
        $reserve_date = "";
        
        if($reservation_date != "")
        {
            if(strlen($reservation_hour) == 1)
            {
                $reservation_hour = "0".$reservation_hour;
            }
            
            if(strlen($reservation_minute) == 1)
            {
                $reservation_minute = "0".$reservation_minute;
            }
            
            $reserve_date = $reservation_date." ".$reservation_hour.":".$reservation_minute.":00";
        }
        
        if($send_type == 0) // 예약 발송
        {
            if($bundle_flag == "0") //일괄발송이 아닐때
            {
                // status = 0인 데이터가 있는 지 확인
                $sql = "SELECT COUNT(*) FROM tbl_vip_leave_a2u WHERE useridx=$useridx AND status = 0";
                $is_already = $db_main->getvalue($sql);
                
                if($is_already == 0)
                {
                    $sql = "SELECT logindate FROM tbl_vip_leave_user WHERE useridx=$useridx";
                    $logindate = $db_main2->getvalue($sql);
                    
                    $sql = "INSERT INTO `tbl_vip_leave_a2u`(useridx, fb_id, a2u_msg, status, last_logindate,reserve_date, writedate) ".
                        "VALUES($useridx, $fb_id, '$fb_a2u_txt', 0, '$logindate', '$reserve_date', NOW())";
                    $db_main->execute($sql);
                }
                else
                {
                    return "이미 발송 대기 중인 노티가 있습니다.";
                }
            }
            else if($bundle_flag == "1") //일괄 발송일때
            {
                if($reserve_date != "")
                {
                    $sql = 	"INSERT INTO tbl_vip_leave_control(control_type, message, status, reserve_date, image_url, imageurl_short, money, leavedays, contactdays, writedate) ".
                        "VALUES(0, '$fb_a2u_txt', 0, '$reserve_date', '', '', '$sql_money', '$sql_leavedays', '$sql_contactdays', now())";
                    $db_main2->execute($sql);
                }
                else
                {
                    $fb_a2u_txt_temp = "";
                    $a2u_txt_nickname = "";
                    $a2u_txt_coin = "";
                    
                    $bundle_sql = 	"SELECT useridx, fb_id, nickname, total_money, email,  coin, isblock, leavedays, logindate, createdate, welcome_coin, last_a2u_send_date, last_email_send_date, last_push_send_date, last_fb_msg_send_date, IF(contact_1 >= contact_2, contact_1, contact_2) AS max_contact ".
                        "FROM ( ".
                        "   SELECT useridx, fb_id, nickname, total_money, email, coin, isblock, leavedays, logindate, createdate, welcome_coin, last_a2u_send_date, last_email_send_date, last_push_send_date, last_fb_msg_send_date, IF(last_a2u_send_date >= last_email_send_date, last_a2u_send_date, last_email_send_date) AS contact_1, IF(last_push_send_date >= last_fb_msg_send_date, last_push_send_date, last_fb_msg_send_date) AS contact_2 ".
                        "   FROM tbl_vip_leave_user ".
                        "   WHERE 1=1 AND status = 0 $sql_money $sql_leavedays AND fb_id > 0 AND is_app_delete = 0 ".
                        ") t1 ".
                        "WHERE 1=1 $sql_contactdays ";
                    $user_list = $db_main2->gettotallist($bundle_sql);
                    
                    for($i=0;$i<sizeof($user_list);$i++)
                    {
                        $bundle_useridx = $user_list[$i]["useridx"];
                        $bundle_fb_id = $user_list[$i]["fb_id"];
                        $max_contact = $user_list[$i]["max_contact"];
                        
                        // status = 0인 데이터가 있는 지 확인
                        //$sql = "SELECT COUNT(*) FROM tbl_vip_leave_a2u WHERE useridx=$bundle_useridx AND status = 0";
                        //$is_already = $db_slave->getvalue($sql);
                        
                        //if($is_already == 0)
                        //{
                        //마지막 컨택일이 7일이상
                        $sql = "SELECT COUNT(*) FROM tbl_vip_leave_user WHERE DATE_SUB(NOW(), INTERVAL 7 DAY) >= '$max_contact' AND useridx = $bundle_useridx ";
                        $is_contact_7day = $db_main2_slave->getvalue($sql);
                        
                        if($is_contact_7day > 0)
                        {
                            $sql = "SELECT logindate FROM tbl_vip_leave_user WHERE useridx=$bundle_useridx";
                            $logindate = $db_main2->getvalue($sql);
                            
                            if($reservation_date != "")
                            {
                                if(strlen($reservation_hour) == 1)
                                {
                                    $reservation_hour = "0".$reservation_hour;
                                }
                                
                                if(strlen($reservation_minute) == 1)
                                {
                                    $reservation_minute = "0".$reservation_minute;
                                }
                                
                                $reserve_date = $reservation_date." ".$reservation_hour.":".$reservation_minute.":00";
                            }
                            
                            $a2u_txt_nickname = str_replace('#nickname#',$user_list[$i]["nickname"],$fb_a2u_txt);
                            
                            $a2u_txt_coin = str_replace('#welcome_coin#',"$".$user_list[$i]["welcome_coin"],$a2u_txt_nickname);
                            
                            $fb_a2u_txt_temp = $a2u_txt_coin;
                            
                            $fb_a2u_txt_temp = encode_db_field($fb_a2u_txt_temp);
                            
                            if($user_list[$i]["welcome_coin"] > 0)
                            {
                                $sql = "INSERT INTO `tbl_vip_leave_a2u`(useridx, fb_id, a2u_msg, status, last_logindate, reserve_date, writedate) ".
                                    "VALUES($bundle_useridx, $bundle_fb_id, '$fb_a2u_txt_temp', 0, '$logindate', '$reserve_date', NOW())";
                                $db_main->execute($sql);
                            }
                        }
                        //}
                    }
                }
            }
            
            //return "개발 중";
        }
        else if($send_type == 1) // 즉시발송
        {
            try
            {
                $reward = 0;
                
                // noti_group = 15
                $sql = "INSERT INTO tbl_notification_group_log_new(useridx, platform, group_no, type, bonus_coin, senddate) VALUES($useridx, 0, 15, 1, $reward, NOW());";
                $db_main->execute($sql);
                
                $sql = "SELECT LAST_INSERT_ID()";
                $notiidx = $db_main->getvalue($sql);
                
                $session = $facebook->getUser();
                
                $args = array('template' => "$fb_a2u_msg",
                    'href' => "?adflag=game_noti_click_vp&notiidx=$notiidx",
                    'ref' => "group_base_group_vp");
                
                $info = $facebook->api("/$fb_id/notifications", "POST", $args);
                
                // 전송 후 tbl_vip_leave_user 노티 발송 시간 업데이트
                $sql = "UPDATE tbl_vip_leave_user SET last_a2u_send_date=NOW() WHERE useridx=$useridx;";
                $db_main2->execute($sql);
            }
            catch (FacebookApiException $e)
            {
                //return $e->getMessage();
                
                write_log($e->getMessage());
                
                if($e->getMessage() == "Unsupported operation" || $e->getMessage() == "(#200) Cannot send notifications to a user who has not installed the app")
                {
                    $sql = "DELETE FROM tbl_notification_group_log_new WHERE notiidx = $notiidx;";
                    $sql .= "INSERT INTO tbl_user_app_delete(useridx, status, writedate) VALUES($useridx, 1, NOW()) ON DUPLICATE KEY UPDATE status=VALUES(status), writedate=VALUES(writedate);";
                    $db_main->execute($sql);
                }
                else
                {
                    $str_message = explode(".", $e->getMessage());
                    $not_allowed_message = explode(")", $e->getMessage());
                    
                    if($str_message[0] == "Unsupported post request" || $not_allowed_message[0] == "(#100")
                    {
                        $sql = "DELETE FROM tbl_notification_group_log_new WHERE notiidx = $notiidx;";
                        $sql .= "INSERT INTO tbl_user_app_delete(useridx, status, writedate) VALUES($useridx, 1, NOW()) ON DUPLICATE KEY UPDATE status=VALUES(status), writedate=VALUES(writedate);";
                        $db_main->execute($sql);
                    }
                    else
                    {
                        write_log($e->getMessage());
                    }
                }
                
                return $e->getMessage();
            }
            catch (Exception $e)
            {
                write_log($e->getMessage());
                
                return "A2U 발송 실패";
            }
        }
        
    }
    
    function user_send_push()
    {
        global $db_main, $db_main2, $db_mobile, $facebook;
        
        $useridx = $_POST["useridx"];
        $fb_push_msg = $_POST["fb_push_msg"];
        $fb_push_txt = $_POST["fb_push_txt"];
        $send_type = $_POST["send_type"];
        $bundle_flag = $_POST["bundle_flag"];
        $sql_money = $_POST["sql_money"];
        $sql_leavedays = $_POST["sql_leavedays"];
        $sql_contactdays = $_POST["sql_contactdays"];
        $reservation_date = $_POST["reservation_date"];
        $reservation_hour = $_POST["reservation_hour"];
        $reservation_minute = $_POST["reservation_minute"];
        $amount = $_POST["amount"];
        
        $today = date("Ymd");
        $reserve_date = "";
        $user_platform = 0;
        
        $eventcode = "vip_".$today;
        
        $eventcode = $eventcode."_".$db_mobile->getvalue("SELECT count(*) FROM tbl_individual_info WHERE push_code like '%$eventcode%'");
        
        if($reservation_date != "")
        {
            if(strlen($reservation_hour) == 1)
            {
                $reservation_hour = "0".$reservation_hour;
            }
            
            if(strlen($reservation_minute) == 1)
            {
                $reservation_minute = "0".$reservation_minute;
            }
            
            $reserve_date = $reservation_date." ".$reservation_hour.":".$reservation_minute.":00";
        }
        else
        {
            $reserve_date = "";
        }
        
        if($send_type == 0) // 예약 발송
        {
            if($bundle_flag == "0")
            {
                // status = 0인 데이터가 있는 지 확인
                $sql = "SELECT COUNT(*) FROM tbl_individual_push WHERE useridx=$useridx AND status = 0 AND eventcode = '$eventcode'";
                $is_already = $db_mobile->getvalue($sql);
                
                if($is_already == 0)
                {
                    $sql = "SELECT os_type FROM `tbl_user_mobile_connection_log` WHERE useridx = $useridx ORDER BY logindate DESC LIMIT 1";
                    $user_platform = $db_mobile->getvalue($sql);
                    
                    $sql = "SELECT isblock FROM tbl_vip_leave_user WHERE useridx = $useridx ";
                    $user_isblock = $db_main2->getvalue($sql);
                    
                    if(($user_platform != 0 || $user_platform != "") && $user_isblock == 0)
                    {
                        $sql = "INSERT INTO `tbl_individual_push`(push_code, push_type, useridx, message, status, category, reservedate, amount) ".
                            "VALUES('$eventcode', 15, $useridx, '$fb_push_txt', 0, $user_platform, '$reserve_date','$amount')";
                        $db_mobile->execute($sql);
                    }
                }
                else
                {
                    return "이미 발송 대기 중인 Push가 있습니다.";
                }
            }
            else if($bundle_flag == "1")
            {
                $bundle_sql = 	"SELECT useridx, fb_id, nickname, total_money, email,  coin, isblock, leavedays, logindate, createdate, welcome_coin, last_a2u_send_date, last_email_send_date, last_push_send_date, last_fb_msg_send_date, IF(contact_1 >= contact_2, contact_1, contact_2) AS max_contact ".
                    "FROM ( ".
                    "   SELECT useridx, fb_id, nickname, total_money, email, coin, isblock, leavedays, logindate, createdate, welcome_coin, last_a2u_send_date, last_email_send_date, last_push_send_date, last_fb_msg_send_date, IF(last_a2u_send_date >= last_email_send_date, last_a2u_send_date, last_email_send_date) AS contact_1, IF(last_push_send_date >= last_fb_msg_send_date, last_push_send_date, last_fb_msg_send_date) AS contact_2 ".
                    "   FROM tbl_vip_leave_user ".
                    "   WHERE 1=1 AND status = 0 $sql_money $sql_leavedays AND is_push_disable = 0 ".
                    ") t1 ".
                    "WHERE 1=1 $sql_contactdays ";
                $user_list = $db_main2->gettotallist($bundle_sql);
                
                $fb_a2u_txt_temp = "";
                $a2u_txt_nickname = "";
                $a2u_txt_coin = "";
                
                for($i=0;$i<sizeof($user_list);$i++)
                {
                    $bundle_useridx = $user_list[$i]["useridx"];
                    $max_contact = $user_list[$i]["max_contact"];
                    
                    // status = 0인 데이터가 있는 지 확인
                    $sql = "SELECT COUNT(*) FROM tbl_individual_push WHERE useridx=$bundle_useridx AND status = 0 AND push_code = '$eventcode'";
                    $is_already = $db_mobile->getvalue($sql);
                    
                    if($is_already == 0)
                    {
                        //마지막 컨택일이 7일이상
                        $sql = "SELECT COUNT(*) FROM tbl_vip_leave_user WHERE DATE_SUB(NOW(), INTERVAL 7 DAY) >= '$max_contact' AND useridx = $bundle_useridx ";
                        $is_contact_7day = $db_main2->getvalue($sql);
                        
                        if($is_contact_7day > 0)
                        {
                            $push_txt_nickname = str_replace('#nickname#',$user_list[$i]["nickname"],$fb_push_txt);
                            
                            $push_txt_coin = str_replace('#welcome_coin#',"$".$user_list[$i]["welcome_coin"],$push_txt_nickname);
                            
                            $fb_push_txt_temp = $push_txt_coin;
                            
                            $fb_push_txt_temp = encode_db_field($fb_push_txt_temp);
                            
                            $sql = "SELECT os_type FROM `tbl_user_mobile_connection_log` WHERE useridx = $bundle_useridx ORDER BY logindate DESC LIMIT 1";
                            $user_platform = $db_mobile->getvalue($sql);
                            
                            $sql = "SELECT isblock FROM tbl_vip_leave_user WHERE useridx = $bundle_useridx ";
                            $user_isblock = $db_main2->getvalue($sql);
                            
                            if(($user_platform != 0 || $user_platform != "") && $user_isblock == 0)
                            {
                                if($user_list[$i]["welcome_coin"] > 0)
                                {
                                    
                                    $sql = "INSERT INTO `tbl_individual_push`(push_code, push_type, useridx, message, status, category, reservedate, amount) ".
                                        "VALUES('$eventcode', 15, $bundle_useridx, '$fb_push_txt_temp',  0, $user_platform, '$reserve_date','$amount')";
                                    $db_mobile->execute($sql);
                                }
                            }
                        }
                    }
                }
                
                if(sizeof($user_list) > 0)
                {
                    $sql = "INSERT INTO `tbl_individual_info`(push_code, push_type, amount, writedate) ".
                        "VALUES('$eventcode', 15, '$amount', now())";
                    $db_mobile->execute($sql);
                }
                
            }
            
            //return "개발 중";
        }
        else if($send_type == 1) // 즉시발송
        {
            $user_platform = 0;
            
            // status = 0인 데이터가 있는 지 확인
            $sql = "SELECT COUNT(*) FROM tbl_individual_push WHERE useridx=$useridx AND status = 0 AND eventcode = '$eventcode' ";
            $is_already = $db_mobile->getvalue($sql);
            
            if($is_already == 0)
            {
                $sql = "SELECT os_type FROM `tbl_user_mobile_connection_log` WHERE useridx = $useridx ORDER BY logindate DESC LIMIT 1";
                $user_platform = $db_mobile->getvalue($sql);
                
                if(($user_platform != 0 || $user_platform != "") && $user_isblock == 0)
                {
                    $sql = "INSERT INTO `tbl_individual_push`(eventcode, useridx, message, status, category, reservedate) ".
                        "VALUES('$eventcode',$useridx, '$fb_push_msg', 0, $user_platform, now())";
                    $db_mobile->execute($sql);
                }
            }
            else
            {
                return "이미 발송 대기 중인 Push가 있습니다.";
            }
        }
        
    }
    
    function user_get_share_list()
    {
        global $db_main2;
        
        $page = $_POST["page"];
        $listcount = "10";
        $useridx = $_POST["useridx"];
        
        if ($page == "" || $listcount == "" || $useridx == "")
            return "필수항목이 빠졌습니다.";
            
        $sql = "SELECT sharecode, sharetype, totalamount, shareamount, status, COUNT(t2.shareidx) AS collect_cnt, t1.writedate
    		    FROM tbl_share_chips t1 LEFT JOIN tbl_share_chips_collect t2 ON t1.shareidx = t2.shareidx
    		    WHERE t1.useridx=$useridx
        		GROUP BY sharecode
        		ORDER BY t1.writedate DESC
                LIMIT ".(($page-1) * $listcount).", ".$listcount;
        return $db_main2->gettotallist($sql);
    }
    
?>
