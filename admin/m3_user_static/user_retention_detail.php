<?
    $top_menu = "user_static";
    $sub_menu = "user_retention_detail";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $startdate = $_GET["startdate"];
    $enddate = $_GET["enddate"];
    
    $search_platform = ($_GET["search_platform"] == "") ? 0 : $_GET["search_platform"];
    $search_data = ($_GET["search_data"] == "") ? 0 : $_GET["search_data"];
    $search_view = ($_GET["search_view"] == "") ? 0 : $_GET["search_view"];
    
    //오늘 날짜 정보
    $today = get_past_date(date("Y-m-d"),1,"d");
    $before_day = "2015-11-19";
    $startdate = ($startdate == "") ? $before_day : $startdate;
    $enddate = ($enddate == "") ? $today : $enddate;
    
    $pagename = "user_retention_detail.php";
    
    $db_main2 = new CDatabase_Main2();
    $db_other = new CDatabase_Other();
    
    $std_count_sql = "reg_count";
    
    if($search_data == 1)
    	$std_count_sql = "day_0";
    else if($search_data == 2)
    	$std_count_sql = "day_1";
    else if($search_data == 3)
    	$std_count_sql = "loyal_count";
    
    if($search_view == 0)
    {
    	$groupby_sql = "DATE_FORMAT(reg_date, '%Y-%m-%d')";
    
    	$pre_sql = "SELECT DATE_FORMAT(reg_date, '%Y-%m') AS reg_date, SUM(reg_count) AS reg_count, SUM(loyal_count) AS loyal_count,  ".
    			"	SUM(day_0) AS day_0, SUM(CASE WHEN day_0 > 0 THEN $std_count_sql ELSE 0 END) AS day_0_reg_count, ".
    			"	SUM(day_1) AS day_1, SUM(CASE WHEN day_1 > 0 THEN $std_count_sql ELSE 0 END) AS day_1_reg_count, ".
    			"	SUM(day_3) AS day_3, SUM(CASE WHEN day_3 > 0 THEN $std_count_sql ELSE 0 END) AS day_3_reg_count, ".
    			"	SUM(day_7) AS day_7, SUM(CASE WHEN day_7 > 0 THEN $std_count_sql ELSE 0 END) AS day_7_reg_count, ".
    			"	SUM(day_14) AS day_14, SUM(CASE WHEN day_14 > 0 THEN $std_count_sql ELSE 0 END) AS day_14_reg_count, ".
    			"	SUM(day_28) AS day_28, SUM(CASE WHEN day_28 > 0 THEN $std_count_sql ELSE 0 END) AS day_28_reg_count, ".
    			"	SUM(day_60) AS day_60, SUM(CASE WHEN day_60 > 0 THEN $std_count_sql ELSE 0 END) AS day_60_reg_count, ".
    			"	SUM(day_90) AS day_90, SUM(CASE WHEN day_90 > 0 THEN $std_count_sql ELSE 0 END) AS day_90_reg_count ".
    			"FROM ( ";
    
    	$suf_sql = ") t1 ".
    			"GROUP BY DATE_FORMAT(reg_date, '%Y-%m')";
    }
    else
    {
    	$groupby_sql = "DATE_FORMAT(reg_date, '%Y-%m-%d')";
    
    	$pre_sql = "SELECT reg_date, reg_count, loyal_count, ".
    			"	day_0, (CASE WHEN day_0 > 0 THEN $std_count_sql ELSE 0 END) AS day_0_reg_count, ".
    			"	day_1, (CASE WHEN day_1 > 0 THEN $std_count_sql ELSE 0 END) AS day_1_reg_count, ".
    			"	day_3, (CASE WHEN day_3 > 0 THEN $std_count_sql ELSE 0 END) AS day_3_reg_count, ".
    			"	day_7, (CASE WHEN day_7 > 0 THEN $std_count_sql ELSE 0 END) AS day_7_reg_count, ".
    			"	day_14, (CASE WHEN day_14 > 0 THEN $std_count_sql ELSE 0 END) AS day_14_reg_count, ".
    			"	day_28, (CASE WHEN day_28 > 0 THEN $std_count_sql ELSE 0 END) AS day_28_reg_count, ".
    			"	day_60, (CASE WHEN day_60 > 0 THEN $std_count_sql ELSE 0 END) AS day_60_reg_count, ".
    			"	day_90, (CASE WHEN day_90 > 0 THEN $std_count_sql ELSE 0 END) AS day_90_reg_count ".
    			"FROM (";
    	$suf_sql = ") t1";
    }
    

    $retention_list = array(array());
    
    if($search_platform == 0)
    {
    	//Web
    	$sql = $pre_sql."SELECT $groupby_sql AS reg_date, SUM(reg_count) AS reg_count, SUM(loyal_count) AS loyal_count, ".
    			"	SUM(day_0) AS day_0, SUM(day_1) AS day_1, SUM(day_3) AS day_3, SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
    			"FROM tbl_user_retention_daily ".
    			"WHERE '2015-11-19' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate' AND platform = 0 ".
    			"GROUP BY $groupby_sql".$suf_sql;	

    	$web_list = array(array("name" => "Web", "data" => $db_other->gettotallist($sql)));
    	
    	$retention_list = array_merge($retention_list, $web_list);
    	
    	//Android
    	$sql = $pre_sql."SELECT $groupby_sql AS reg_date, SUM(reg_count) AS reg_count, SUM(loyal_count) AS loyal_count, ".
    			"	SUM(day_0) AS day_0, SUM(day_1) AS day_1, SUM(day_3) AS day_3, SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
    			"FROM tbl_user_retention_daily ".
    			"WHERE '2015-11-19' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate' AND platform = 2 ".
    			"GROUP BY $groupby_sql".$suf_sql;
    	$and_list = array(array("name" => "Android", "data" => $db_other->gettotallist($sql)));

    	$retention_list = array_merge($retention_list, $and_list);
    	
    	//IOS
    	$sql = $pre_sql."SELECT $groupby_sql AS reg_date, SUM(reg_count) AS reg_count, SUM(loyal_count) AS loyal_count, ".
    			"	SUM(day_0) AS day_0, SUM(day_1) AS day_1, SUM(day_3) AS day_3, SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
    			"FROM tbl_user_retention_daily ".
    			"WHERE '2015-11-19' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate' AND platform = 1 ".
    			"GROUP BY $groupby_sql".$suf_sql;
    	$ios_list = array(array("name" => "IOS", "data" => $db_other->gettotallist($sql)));
    	
    	$retention_list = array_merge($retention_list, $ios_list);
    	
    	//Amazon
    	$sql = $pre_sql."SELECT $groupby_sql AS reg_date, SUM(reg_count) AS reg_count, SUM(loyal_count) AS loyal_count, ".
    			"	SUM(day_0) AS day_0, SUM(day_1) AS day_1, SUM(day_3) AS day_3, SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
    			"FROM tbl_user_retention_daily ".
    			"WHERE '2015-11-19' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate' AND platform = 3 ".
    			"GROUP BY $groupby_sql".$suf_sql;
    	$amazon_list = array(array("name" => "Amazon", "data" => $db_other->gettotallist($sql)));
    	
    	$retention_list = array_merge($retention_list, $amazon_list);
    }
    else
    {
    	$sql = "SELECT adflag FROM tbl_adflag_flag WHERE category != 100";
		$adflag_list = $db_main2->gettotallist($sql);	
		
		for($i = 0; $i < sizeof($adflag_list); $i++)
		{
			$adflag_info = $adflag_list[$i]["adflag"];			
			$adflag_info_name = $adflag_info;
			
			if($adflag_info == "viral")
				$adflag_info = "";
			
			$sql = $pre_sql."SELECT $groupby_sql AS reg_date, SUM(reg_count) AS reg_count, SUM(loyal_count) AS loyal_count, ".
					"	SUM(day_0) AS day_0, SUM(day_1) AS day_1, SUM(day_3) AS day_3, SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
					"FROM tbl_user_retention_daily ".
					"WHERE '2015-11-19' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate' AND adflag = '$adflag_info' ".
					"GROUP BY $groupby_sql".$suf_sql;			
			$adflag_result = array(array("name" => "$adflag_info_name", "data" => $db_other->gettotallist($sql)));			
			
			$retention_list = array_merge($retention_list, $adflag_result);
		} 
    }
    
    $db_main2->end();
    $db_other->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#startdate").datepicker({ });
	});
	
	$(function() {
		$("#enddate").datepicker({ });
	});
	
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.startdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    } 
	
	    if (search_form.enddate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    } 
	
	    search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 사용자 유지현황(세부)</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<span class="search_lbl">구분&nbsp;&nbsp;&nbsp;</span>
				<select name="search_platform" id="search_platform">
					<option value="0" <?= ($search_platform == "0") ? "selected" : "" ?>>플랫폼별</option>
					<option value="1" <?= ($search_platform == "1") ? "selected" : "" ?>>adflag별</option>
				</select>
				<span class="search_lbl ml20">Data&nbsp;&nbsp;&nbsp;</span>
				<select name="search_data" id="search_data">
					<option value="0" <?= ($search_data == "0") ? "selected" : "" ?>>비율(Installs)</option>
					<option value="1" <?= ($search_data == "1") ? "selected" : "" ?>>비율(D+0)</option>
					<option value="2" <?= ($search_data == "2") ? "selected" : "" ?>>비율(D+1)</option>
					<option value="3" <?= ($search_data == "3") ? "selected" : "" ?>>비율(Loyal Users)</option>
					<option value="4" <?= ($search_data == "4") ? "selected" : "" ?>>숫자</option>
				</select>
				<span class="search_lbl ml20">기간&nbsp;&nbsp;&nbsp;</span>
				<select name="search_view" id="search_view">
					<option value="0" <?= ($search_view == "0") ? "selected" : "" ?>>월</option>
					<option value="1" <?= ($search_view == "1") ? "selected" : "" ?>>일</option>
				</select>
				<span class="search_lbl ml20">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" /> ~
				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $startdate ?> ~ <?= $enddate ?></span> 통계입니다
	</div>
	
<?
	for($i=1; $i<sizeof($retention_list); $i++)
	{
		if($i > 0)
		{
?>
			<br/><br/>
<?
		}
?>
		<div class="search_result"><?= $retention_list[$i]["name"]; ?></div>
		<table class="tbl_list_basic1">
		<colgroup>
			<col width="70">
			<col width="70">
			<col width="70">
<?
	if($search_data != 2)
	{
?>
	        <col width="70">
<?
	}
?>
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
		</colgroup>
        <thead>
         	<th>가입월</th>
			<th>가입자수</th>
			<th>Loyal Users</th>
<?
	if($search_data != 2)
	{
?>
	        <th>D+0</th>
<?
	}
?>
			
			<th>D+1</th>
			<th>D+3</th>
			<th>D+7</th>
			<th>D+14</th>
			<th>D+28</th>
			<th>D+60</th>
			<th>D+90</th>
        </thead>
        <tbody>
<?
		$retention_detail = $retention_list[$i]["data"];
		
		$sum_reg_count = 0;
		$sum_loyal_count = 0;
		$sum_day_0 = 0;
		$sum_day_0_reg_count = 0;
		$sum_day_1 = 0;
		$sum_day_1_reg_count = 0;
		$day_1_row_count = 0;
		$sum_day_3 = 0;
		$sum_day_3_reg_count = 0;
		$day_3_row_count = 0;
		$sum_day_7 = 0;
		$sum_day_7_reg_count = 0;
		$day_7_row_count = 0;
		$sum_day_14 = 0;
		$sum_day_14_reg_count = 0;
		$day_14_row_count = 0;
		$sum_day_28 = 0;
		$sum_day_28_reg_count = 0;
		$day_28_row_count = 0;
		$sum_day_60 = 0;
		$sum_day_60_reg_count = 0;
		$day_60_row_count = 0;
		$sum_day_90 = 0;
		$sum_day_90_reg_count = 0;
		$day_90_row_count = 0;

		for($j=0; $j<sizeof($retention_detail); $j++)
		{
			$reg_date = $retention_detail[$j]["reg_date"];
			$reg_count = $retention_detail[$j]["reg_count"];
			$loyal_count = $retention_detail[$j]["loyal_count"];
			$day_0 = $retention_detail[$j]["day_0"];
			$day_1 = $retention_detail[$j]["day_1"];
			$day_3 = $retention_detail[$j]["day_3"];
			$day_7 = $retention_detail[$j]["day_7"];
			$day_14 = $retention_detail[$j]["day_14"];
			$day_28 = $retention_detail[$j]["day_28"];
			$day_60 = $retention_detail[$j]["day_60"];
			$day_90 = $retention_detail[$j]["day_90"];
			
			$day_0_reg_count = $retention_detail[$j]["day_0_reg_count"];
			$day_1_reg_count = $retention_detail[$j]["day_1_reg_count"];
			$day_3_reg_count = $retention_detail[$j]["day_3_reg_count"];
			$day_7_reg_count = $retention_detail[$j]["day_7_reg_count"];
			$day_14_reg_count = $retention_detail[$j]["day_14_reg_count"];
			$day_28_reg_count = $retention_detail[$j]["day_28_reg_count"];
			$day_60_reg_count = $retention_detail[$j]["day_60_reg_count"];
			$day_90_reg_count = $retention_detail[$j]["day_90_reg_count"];
			
			$sum_reg_count += $reg_count;
			$sum_loyal_count += $loyal_count;
			$sum_day_0 += $day_0;
			$sum_day_1 += $day_1;
			$sum_day_3 += $day_3;
			$sum_day_7 += $day_7;
			$sum_day_14 += $day_14;
			$sum_day_28 += $day_28;
			$sum_day_60 += $day_60;
			$sum_day_90 += $day_90;
			
			$sum_day_0_reg_count += $day_0_reg_count;
			$sum_day_1_reg_count += $day_1_reg_count;
			$sum_day_3_reg_count += $day_3_reg_count;
			$sum_day_7_reg_count += $day_7_reg_count;
			$sum_day_14_reg_count += $day_14_reg_count;
			$sum_day_28_reg_count += $day_28_reg_count;
			$sum_day_60_reg_count += $day_60_reg_count;
			$sum_day_90_reg_count += $day_90_reg_count;
			
			if($day_1 > 0)
				$day_1_row_count++;
			
			if($day_3 > 0)
				$day_3_row_count++;
			
			if($day_7 > 0)
				$day_7_row_count++;
			
			if($day_14 > 0)
				$day_14_row_count++;
			
			if($day_28 > 0)
				$day_28_row_count++;
			
			if($day_60 > 0)
				$day_60_row_count++;
			
			if($day_90 > 0)
				$day_90_row_count++;
			
			$add_per = "";
			$add_point = 0;
			
			if($search_data == 0)
			{
				$add_per = "%";
				$add_point = 1;
				
				$loyal_count = ($reg_count == 0) ? 0 : round($loyal_count/$reg_count*100, $add_point);
				
				if($search_view == 0)
				{
					$day_0 = ($day_0_reg_count == 0) ? 0 : round($day_0/$day_0_reg_count*100, $add_point);
					$day_1 = ($day_1_reg_count == 0) ? 0 : round($day_1/$day_1_reg_count*100, $add_point);
					$day_3 = ($day_3_reg_count == 0) ? 0 : round($day_3/$day_3_reg_count*100, $add_point);
					$day_7 = ($day_7_reg_count == 0) ? 0 : round($day_7/$day_7_reg_count*100, $add_point);
					$day_14 = ($day_14_reg_count == 0) ? 0 : round($day_14/$day_14_reg_count*100, $add_point);
					$day_28 = ($day_28_reg_count == 0) ? 0 : round($day_28/$day_28_reg_count*100, $add_point);
					$day_60 = ($day_60_reg_count == 0) ? 0 : round($day_60/$day_60_reg_count*100, $add_point);
					$day_90 = ($day_90_reg_count == 0) ? 0 : round($day_90/$day_90_reg_count*100, $add_point);
				}
				else
				{
					$day_0 = ($day_0_reg_count == 0) ? 0 : round($day_0/$day_0_reg_count*100, $add_point);
					$day_1 = ($day_1_reg_count == 0) ? 0 : round($day_1/$day_1_reg_count*100, $add_point);
					$day_3 = ($day_3_reg_count == 0) ? 0 : round($day_3/$day_3_reg_count*100, $add_point);
					$day_7 = ($day_7_reg_count == 0) ? 0 : round($day_7/$day_7_reg_count*100, $add_point);
					$day_14 = ($day_14_reg_count == 0) ? 0 : round($day_14/$day_14_reg_count*100, $add_point);
					$day_28 = ($day_28_reg_count == 0) ? 0 : round($day_28/$day_28_reg_count*100, $add_point);
					$day_60 = ($day_60_reg_count == 0) ? 0 : round($day_60/$day_60_reg_count*100, $add_point);
					$day_90 = ($day_90_reg_count == 0) ? 0 : round($day_90/$day_90_reg_count*100, $add_point);
				}
			}
			else if($search_data == 1)
			{
				$add_per = "%";
				$add_point = 1;
				
				$loyal_count = ($reg_count == 0) ? 0 : round($loyal_count/$reg_count*100, $add_point);
				
				if($search_view == 0)
				{
					$day_0 = ($day_0_reg_count == 0) ? 0 : round($day_0/$day_0_reg_count*100, $add_point);
					$day_1 = ($day_1_reg_count == 0) ? 0 : round($day_1/$day_1_reg_count*100, $add_point);
					$day_3 = ($day_3_reg_count == 0) ? 0 : round($day_3/$day_3_reg_count*100, $add_point);
					$day_7 = ($day_7_reg_count == 0) ? 0 : round($day_7/$day_7_reg_count*100, $add_point);
					$day_14 = ($day_14_reg_count == 0) ? 0 : round($day_14/$day_14_reg_count*100, $add_point);
					$day_28 = ($day_28_reg_count == 0) ? 0 : round($day_28/$day_28_reg_count*100, $add_point);
					$day_60 = ($day_60_reg_count == 0) ? 0 : round($day_60/$day_60_reg_count*100, $add_point);
					$day_90 = ($day_90_reg_count == 0) ? 0 : round($day_90/$day_90_reg_count*100, $add_point);
				}
				else
				{
					$std_division = $day_0;
					
					$day_0 = ($std_division == 0) ? 0 : round($day_0/$std_division*100, $add_point);
					$day_1 = ($std_division == 0) ? 0 : round($day_1/$std_division*100, $add_point);
					$day_3 = ($std_division == 0) ? 0 : round($day_3/$std_division*100, $add_point);
					$day_7 = ($std_division == 0) ? 0 : round($day_7/$std_division*100, $add_point);
					$day_14 = ($std_division == 0) ? 0 : round($day_14/$std_division*100, $add_point);
					$day_28 = ($std_division == 0) ? 0 : round($day_28/$std_division*100, $add_point);
					$day_60 = ($std_division == 0) ? 0 : round($day_60/$std_division*100, $add_point);
					$day_90 = ($std_division == 0) ? 0 : round($day_90/$std_division*100, $add_point);
				}
			}
			else if($search_data == 2)
			{
				$add_per = "%";
				$add_point = 1;
			
				$loyal_count = ($reg_count == 0) ? 0 : round($loyal_count/$reg_count*100, $add_point);
				
				if($search_view == 0)
				{
					$day_0 = ($day_0_reg_count == 0) ? 0 : round($day_0/$day_0_reg_count*100, $add_point);
					$day_1 = ($day_1_reg_count == 0) ? 0 : round($day_1/$day_1_reg_count*100, $add_point);
					$day_3 = ($day_3_reg_count == 0) ? 0 : round($day_3/$day_3_reg_count*100, $add_point);
					$day_7 = ($day_7_reg_count == 0) ? 0 : round($day_7/$day_7_reg_count*100, $add_point);
					$day_14 = ($day_14_reg_count == 0) ? 0 : round($day_14/$day_14_reg_count*100, $add_point);
					$day_28 = ($day_28_reg_count == 0) ? 0 : round($day_28/$day_28_reg_count*100, $add_point);
					$day_60 = ($day_60_reg_count == 0) ? 0 : round($day_60/$day_60_reg_count*100, $add_point);
					$day_90 = ($day_90_reg_count == 0) ? 0 : round($day_90/$day_90_reg_count*100, $add_point);
				}
				else
				{
					$std_division = $day_1;
						
					$day_0 = ($std_division == 0) ? 0 : round($day_0/$std_division*100, $add_point);
					$day_1 = ($std_division == 0) ? 0 : round($day_1/$std_division*100, $add_point);
					$day_3 = ($std_division == 0) ? 0 : round($day_3/$std_division*100, $add_point);
					$day_7 = ($std_division == 0) ? 0 : round($day_7/$std_division*100, $add_point);
					$day_14 = ($std_division == 0) ? 0 : round($day_14/$std_division*100, $add_point);
					$day_28 = ($std_division == 0) ? 0 : round($day_28/$std_division*100, $add_point);
					$day_60 = ($std_division == 0) ? 0 : round($day_60/$std_division*100, $add_point);
					$day_90 = ($std_division == 0) ? 0 : round($day_90/$std_division*100, $add_point);
				}
			}
			else if($search_data == 3)
			{
				$add_per = "%";
				$add_point = 1;
					
				if($search_view == 0)
				{
					$day_0 = ($day_0_reg_count == 0) ? 0 : round($day_0/$day_0_reg_count*100, $add_point);
					$day_1 = ($day_1_reg_count == 0) ? 0 : round($day_1/$day_1_reg_count*100, $add_point);
					$day_3 = ($day_3_reg_count == 0) ? 0 : round($day_3/$day_3_reg_count*100, $add_point);
					$day_7 = ($day_7_reg_count == 0) ? 0 : round($day_7/$day_7_reg_count*100, $add_point);
					$day_14 = ($day_14_reg_count == 0) ? 0 : round($day_14/$day_14_reg_count*100, $add_point);
					$day_28 = ($day_28_reg_count == 0) ? 0 : round($day_28/$day_28_reg_count*100, $add_point);
					$day_60 = ($day_60_reg_count == 0) ? 0 : round($day_60/$day_60_reg_count*100, $add_point);
					$day_90 = ($day_90_reg_count == 0) ? 0 : round($day_90/$day_90_reg_count*100, $add_point);
				}
				else
				{
					$std_division = $loyal_count;
				
					$day_0 = ($std_division == 0) ? 0 : round($day_0/$std_division*100, $add_point);
					$day_1 = ($std_division == 0) ? 0 : round($day_1/$std_division*100, $add_point);
					$day_3 = ($std_division == 0) ? 0 : round($day_3/$std_division*100, $add_point);
					$day_7 = ($std_division == 0) ? 0 : round($day_7/$std_division*100, $add_point);
					$day_14 = ($std_division == 0) ? 0 : round($day_14/$std_division*100, $add_point);
					$day_28 = ($std_division == 0) ? 0 : round($day_28/$std_division*100, $add_point);
					$day_60 = ($std_division == 0) ? 0 : round($day_60/$std_division*100, $add_point);
					$day_90 = ($std_division == 0) ? 0 : round($day_90/$std_division*100, $add_point);
				}
				
				$loyal_count = ($std_division == 0) ? 0 : round($loyal_count/$std_division*100, $add_point);
			}
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
	            <td class="tdc"><?= $reg_date ?></td>
	            <td class="tdc"><?= number_format($reg_count) ?></td>
	            <td class="tdc"><?= number_format($loyal_count, $add_point)." $add_per" ?></td>
<?
	if($search_data != 2)
	{
?>
	            <td class="tdc"><?= number_format($day_0, $add_point)." $add_per" ?></td>
<?
	}
?>
	            <td class="tdc"><?= number_format($day_1, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_3, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_7, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_14, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_28, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_60, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_90, $add_point)." $add_per" ?></td>
	        </tr>
<?
	}
	
    if(sizeof($retention_detail) == 0)
    {
?>
   			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   				<td class="tdc" colspan="<?= ($search_data == 2) ? 10 : 11?>">검색 결과가 없습니다.</td>
   			</tr>
<?
   	}
   	else
   	{
   		if($search_data == 0)
   		{
   			$sum_loyal_count = round($sum_loyal_count/$sum_reg_count*100, $add_point);
   			$sum_day_0 = ($sum_day_0_reg_count == 0) ? 0 : round($sum_day_0/$sum_day_0_reg_count*100, $add_point);
   			$sum_day_1 = ($sum_day_1_reg_count == 0) ? 0 : round($sum_day_1/$sum_day_1_reg_count*100, $add_point);
   			$sum_day_3 = ($sum_day_3_reg_count == 0) ? 0 : round($sum_day_3/$sum_day_3_reg_count*100, $add_point);
   			$sum_day_7 = ($sum_day_7_reg_count == 0) ? 0 : round($sum_day_7/$sum_day_7_reg_count*100, $add_point);
   			$sum_day_14 = ($sum_day_14_reg_count == 0) ? 0 : round($sum_day_14/$sum_day_14_reg_count*100, $add_point);
   			$sum_day_28 = ($sum_day_28_reg_count == 0) ? 0 : round($sum_day_28/$sum_day_28_reg_count*100, $add_point);
   			$sum_day_60 = ($sum_day_60_reg_count == 0) ? 0 : round($sum_day_60/$sum_day_60_reg_count*100, $add_point);
   			$sum_day_90 = ($sum_day_90_reg_count == 0) ? 0 : round($sum_day_90/$sum_day_90_reg_count*100, $add_point);
   		}
   		else if($search_data == 1)
   		{
   			$sum_loyal_count = round($sum_loyal_count/$sum_reg_count*100, $add_point);
   			$sum_day_0 = ($sum_day_0_reg_count == 0) ? 0 : round($sum_day_0/$sum_day_0_reg_count*100, $add_point);
   			$sum_day_1 = ($sum_day_1_reg_count == 0) ? 0 : round($sum_day_1/$sum_day_1_reg_count*100, $add_point);
   			$sum_day_3 = ($sum_day_3_reg_count == 0) ? 0 : round($sum_day_3/$sum_day_3_reg_count*100, $add_point);
   			$sum_day_7 = ($sum_day_7_reg_count == 0) ? 0 : round($sum_day_7/$sum_day_7_reg_count*100, $add_point);
   			$sum_day_14 = ($sum_day_14_reg_count == 0) ? 0 : round($sum_day_14/$sum_day_14_reg_count*100, $add_point);
   			$sum_day_28 = ($sum_day_28_reg_count == 0) ? 0 : round($sum_day_28/$sum_day_28_reg_count*100, $add_point);
   			$sum_day_60 = ($sum_day_60_reg_count == 0) ? 0 : round($sum_day_60/$sum_day_60_reg_count*100, $add_point);
   			$sum_day_90 = ($sum_day_90_reg_count == 0) ? 0 : round($sum_day_90/$sum_day_90_reg_count*100, $add_point);
   		}
   		else if($search_data == 2)
   		{
   			$sum_loyal_count = round($sum_loyal_count/$sum_reg_count*100, $add_point);
   			$sum_day_0 = ($sum_day_0_reg_count == 0) ? 0 : round($sum_day_0/$sum_day_0_reg_count*100, $add_point);
   			$sum_day_1 = ($sum_day_1_reg_count == 0) ? 0 : round($sum_day_1/$sum_day_1_reg_count*100, $add_point);
   			$sum_day_3 = ($sum_day_3_reg_count == 0) ? 0 : round($sum_day_3/$sum_day_3_reg_count*100, $add_point);
   			$sum_day_7 = ($sum_day_7_reg_count == 0) ? 0 : round($sum_day_7/$sum_day_7_reg_count*100, $add_point);
   			$sum_day_14 = ($sum_day_14_reg_count == 0) ? 0 : round($sum_day_14/$sum_day_14_reg_count*100, $add_point);
   			$sum_day_28 = ($sum_day_28_reg_count == 0) ? 0 : round($sum_day_28/$sum_day_28_reg_count*100, $add_point);
   			$sum_day_60 = ($sum_day_60_reg_count == 0) ? 0 : round($sum_day_60/$sum_day_60_reg_count*100, $add_point);
   			$sum_day_90 = ($sum_day_90_reg_count == 0) ? 0 : round($sum_day_90/$sum_day_90_reg_count*100, $add_point);
   		}
   		else if($search_data == 3)
   		{
   			$sum_loyal_count = round($sum_loyal_count/$sum_reg_count*100, $add_point);
   			$sum_day_0 = ($sum_day_0_reg_count == 0) ? 0 : round($sum_day_0/$sum_day_0_reg_count*100, $add_point);
   			$sum_day_1 = ($sum_day_1_reg_count == 0) ? 0 : round($sum_day_1/$sum_day_1_reg_count*100, $add_point);
   			$sum_day_3 = ($sum_day_3_reg_count == 0) ? 0 : round($sum_day_3/$sum_day_3_reg_count*100, $add_point);
   			$sum_day_7 = ($sum_day_7_reg_count == 0) ? 0 : round($sum_day_7/$sum_day_7_reg_count*100, $add_point);
   			$sum_day_14 = ($sum_day_14_reg_count == 0) ? 0 : round($sum_day_14/$sum_day_14_reg_count*100, $add_point);
   			$sum_day_28 = ($sum_day_28_reg_count == 0) ? 0 : round($sum_day_28/$sum_day_28_reg_count*100, $add_point);
   			$sum_day_60 = ($sum_day_60_reg_count == 0) ? 0 : round($sum_day_60/$sum_day_60_reg_count*100, $add_point);
   			$sum_day_90 = ($sum_day_90_reg_count == 0) ? 0 : round($sum_day_90/$sum_day_90_reg_count*100, $add_point);
   		}
   		else 
   		{
   			$sum_loyal_count = (sizeof($retention_detail) == 0) ? 0 : round($sum_loyal_count/sizeof($retention_detail));
   			$sum_day_0 =(sizeof($retention_detail) == 0) ? 0 :  round($sum_day_0/sizeof($retention_detail));
   			$sum_day_1 = ($day_1_row_count == 0) ? 0 : round($sum_day_1/$day_1_row_count);
   			$sum_day_3 = ($day_3_row_count == 0) ? 0 : round($sum_day_3/$day_3_row_count);
   			$sum_day_7 = ($day_7_row_count == 0) ? 0 : round($sum_day_7/$day_7_row_count);
   			$sum_day_14 = ($day_14_row_count == 0) ? 0 : round($sum_day_14/$day_14_row_count);
   			$sum_day_28 = ($day_28_row_count == 0) ? 0 : round($sum_day_28/$day_28_row_count);
   			$sum_day_60 = ($day_60_row_count == 0) ? 0 : round($sum_day_60/$day_60_row_count);
   			$sum_day_90 = ($day_90_row_count == 0) ? 0 : round($sum_day_90/$day_90_row_count);
   			
   		}
   		
   		$sum_reg_count = round($sum_reg_count/sizeof($retention_detail));
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point_title">평균</td>
	        	<td class="tdc point"><?= number_format($sum_reg_count) ?></td>
	            <td class="tdc point"><?= number_format($sum_loyal_count, $add_point)." $add_per" ?></td>
<?
	if($search_data != 2)
	{
?>
            	<td class="tdc point"><?= number_format($sum_day_0, $add_point)." $add_per" ?></td>
<?
	}
?>
	            <td class="tdc point"><?= number_format($sum_day_1, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_3, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_7, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_14, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_28, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_60, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_90, $add_point)." $add_per" ?></td>
	        </tr>
<?
   	}
?>
        </tbody>
	</table>
<?
	}
?>
</div>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>