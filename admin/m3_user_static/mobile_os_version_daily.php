<?
	$top_menu = "user_static";
	$sub_menu = "mobile_os_version_daily";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$term = ($_GET["term"] == "") ? "1" : $_GET["term"];
	
	if ($term != "1" && $term != "2" && $term != "3")
		error_back("잘못된 접근입니다.");
	
	if($_GET["search_date"] == "")
		$search_date = date("Y-m-d",strtotime("-1 days"));
	else
		$search_date = $_GET["search_date"];
	
	$db_mobile = new CDatabase_Mobile();
	
	$sql = "SELECT SUM(count) FROM `mobile_os_version_daily` WHERE today = '$search_date' AND os_type = $term";
	$ios_total = $db_mobile->getvalue($sql);
	
	$sql = "SELECT * FROM `mobile_os_version_daily` WHERE today = '$search_date' AND os_type = $term ORDER BY os_version DESC";	
	$ios_verion = $db_mobile->gettotallist($sql);
	
	$db_mobile->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#search_date").datepicker({ });
	});

	function search()
	{
		var search_form = document.search_form;
	    
		if (search_form.search_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.search_date.focus();
	    	return;
		} 
	
		search_form.submit();
	}

function change_term(term)
	{
		var search_form = document.search_form;
		
		var ios = document.getElementById("term_ios");
		var android = document.getElementById("term_android");
		var amazon = document.getElementById("term_amazon");
		
		document.search_form.term.value = term;

		if (term == "1")
		{
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "2")
		{
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (term == "3")
		{
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}

		search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 모바일 OS 버전 통계 </div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<input type="hidden" name="term" id="term" value="<?= $term ?>" />
			<div class="search_box">				
				<input type="button" class="<?= ($term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_term('1')" />
				<input type="button" class="<?= ($term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_term('2')"    />
				<input type="button" class="<?= ($term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_term('3')"    />
				
				<span class="search_lbl">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="search_date" name="search_date" style="width:75px" readonly="readonly" value="<?= $search_date ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/>
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->

	<div class="search_result">
		<span><?= $search_date ?></span> 현황입니다(최근 하루이내 로그인)
	</div>
	
	<div style=" float:left;">
	<div class="h2_title pt20">
<?
	if($term == 1)
	{
?>
	iOS
<?
	}
	else if($term == 2)
	{
?>
	android
<?
	}
	else if($term == 3)
	{
?>
	amazon
<?
	}
?>
	
	</div>
	<table class="tbl_list_basic1" style="width: 350px;">
		<colgroup>
			<col width="100">
			<col width="150">
			<col width="100">
		</colgroup>
        <thead>
        	<tr>
        		<th>OS버전</th>
            	<th>사용자수</th>
             	<th>비율</th>
         	</tr>
        </thead>
        <tbody>
<?
    for($i=0; $i<sizeof($ios_verion); $i++)
    {
    	$version = $ios_verion[$i]["os_version"];
    	$count = $ios_verion[$i]["count"];
    	$per = ($count/$ios_total) * 100;
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="tdc"><?= $version ?></td>
            <td class="tdr"><?= number_format($count) ?></td>
            <td class="tdr"><?= round($per, 2) ?>%</td>
        </tr>
<?
    }
?>
		

<?    
    if(sizeof($ios_verion) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="3">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
   	else
   	{
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="tdc point">합계</td>
            <td class="tdr point"><?= number_format($ios_total) ?></td>
            <td class="tdr point">100%</td>
        </tr>
<?
   	} 
?>
        </tbody>
	</table>
	</div>
	
        </tbody>
	</table>
	</div>
	
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>