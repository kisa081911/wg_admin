<?
    $top_menu = "user_static";
    $sub_menu = "guest_login_stat_daily";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $today = date("Y-m-d");
    
    $os_type = ($_GET["os_type"] == "") ? "4" :$_GET["os_type"];
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    
    if($search_start_createdate == "")
        $search_start_createdate = date("Y-m-d", strtotime("-3 day"));
        
    if($search_end_createdate == "")
        $search_end_createdate = $today;
        
    $tail = "";
        
    $db_main2 = new CDatabase_Main2();
    
    $sql = " SELECT * FROM tbl_guestuser_stat_daily WHERE today BETWEEN '$search_start_createdate' AND '$search_end_createdate' ORDER BY today desc, platform asc";
    $guestuser_data = $db_main2->gettotallist($sql);
    
    $db_main2->end();
    
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#start_createdate").datepicker({ });
	});
	
	$(function() {
		$("#end_createdate").datepicker({ });
	});
	
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.startdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    } 
	
	    if (search_form.enddate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    } 
	
	    search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap" style="height:76px;">
		<div class="title"><?= $top_menu_txt ?> &gt; 게스트 로그인 통계</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<span class="search_lbl ml20">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" /> ~
				<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	<div class="search_result"><?= $os_txt?></div>
			<div class="search_result">
        		<span><?= $search_start_createdate ?> ~ <?= $search_end_createdate ?></span> 통계입니다
        	</div>
    		<table class="tbl_list_basic1">
    		<colgroup>
    			<col width="70">
    			<col width="70">
    			<col width="90">
                <col width="90">
                <col width="90">
                <col width="90">
                <col width="90">
                <col width="90">
                <col width="90">
                <col width="90">
    		</colgroup>
            <thead>
            <tr>
             	<th rowspan="2">날짜</th>
    			<th rowspan="2">플랫폼</th>
    			<th colspan="5">게스트 유지 일수</th>
    			<th colspan="5">게스트 결제 인원</th>
    		</tr>
            <tr>
            	<th>전체</th>
            	<th>D+7</th>
            	<th>D+14</th>
            	<th>D+28</th>
            	<th>D+60이상</th>
            	<th>전체</th>
            	<th>D+7</th>
            	<th>D+14</th>
            	<th>D+28</th>
            	<th>D+60이상</th>
        	</tr>
            </thead>
            <tbody>
	
<?
    $amazing_data_stat = array();
    
    for($i=0; $i<sizeof($guestuser_data); $i++)
    {
        $today = $guestuser_data[$i]["today"];
        $platform = $guestuser_data[$i]["platform"];
        $totalcount = $guestuser_data[$i]["totalcount"];
        $d_7 = $guestuser_data[$i]["d_7"];
        $d_14 = $guestuser_data[$i]["d_14"];
        $d_28 = $guestuser_data[$i]["d_28"];
        $d_60 = $guestuser_data[$i]["d_60"];
        
        $pay_totalcount = $guestuser_data[$i]["pay_totalcount"];
        $pay_d_7 = $guestuser_data[$i]["pay_d_7"];
        $pay_d_14 = $guestuser_data[$i]["pay_d_14"];
        $pay_d_28 = $guestuser_data[$i]["pay_d_28"];
        $pay_d_60 = $guestuser_data[$i]["pay_d_60"];
        
        $platform_str="";
        
        if($platform == 1)
        {
            $platform_str="IOS";
        }
        else if($platform == 2)
        {
            $platform_str="Android";
        }
        else if($platform == 3)
        {
            $platform_str="Amazon";
        }
        $sum_totalcount += $totalcount;
        $sum_d_7 += $d_7;
        $sum_d_14 += $d_14;
        $sum_d_28 += $d_28;
        $sum_d_60 += $d_60;
        
        $sum_pay_totalcount += $pay_totalcount;
        $sum_pay_d_7 += $pay_d_7;
        $sum_pay_d_14 += $pay_d_14;
        $sum_pay_d_28 += $pay_d_28;
        $sum_pay_d_60 += $pay_d_60;
       
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
            if($i == 0 || $today != $guestuser_data[$i-1]["today"]){
?>
            <td class="tdc point" rowspan="4"><?= $today ?></td>
<?
            }
?>		
            <td class="tdc"><?= $platform_str?></td>
            <td class="tdc"><?= number_format($totalcount) ?></td>
            <td class="tdc"><?= number_format($d_7) ?></td>
            <td class="tdc"><?= number_format($d_14) ?></td>
            <td class="tdc"><?= number_format($d_28) ?></td>
            <td class="tdc"><?= number_format($d_60) ?></td>
            <td class="tdc"><?= number_format($pay_totalcount) ?></td>
            <td class="tdc"><?= number_format($pay_d_7) ?></td>
            <td class="tdc"><?= number_format($pay_d_14) ?></td>
            <td class="tdc"><?= number_format($pay_d_28) ?></td>
            <td class="tdc"><?= number_format($pay_d_60) ?></td>
        </tr>
<?
            if($today != $guestuser_data[$i+1]["today"])
            {
?>

            <td class="tdc">합계</td>
            <td class="tdc"><?= number_format($sum_totalcount) ?></td>
            <td class="tdc"><?= number_format($sum_d_7) ?></td>
            <td class="tdc"><?= number_format($sum_d_14) ?></td>
            <td class="tdc"><?= number_format($sum_d_28) ?></td>
            <td class="tdc"><?= number_format($sum_d_60) ?></td>
            <td class="tdc"><?= number_format($sum_pay_totalcount) ?></td>
            <td class="tdc"><?= number_format($sum_pay_d_7) ?></td>
            <td class="tdc"><?= number_format($sum_pay_d_14) ?></td>
            <td class="tdc"><?= number_format($sum_pay_d_28) ?></td>
            <td class="tdc"><?= number_format($sum_pay_d_60) ?></td>
        </tr>
<?      
            $sum_totalcount = 0;
            $sum_d_7 = 0;
            $sum_d_14 = 0;
            $sum_d_28 = 0;
            $sum_d_60 = 0;
    
            $sum_pay_totalcount = 0;
            $sum_pay_d_7 = 0;
            $sum_pay_d_14 = 0;
            $sum_pay_d_28 = 0;
            $sum_pay_d_60 = 0;
            }
    }
    
    if(sizeof($guestuser_data) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="5">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
            </tbody>
    	</table>
</div>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>