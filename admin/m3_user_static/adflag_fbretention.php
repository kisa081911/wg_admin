<?
    $top_menu = "user_static";
    $sub_menu = "adflag_fbretention";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    $search_payday = $_GET["payday"];
	$isearch = $_GET["issearch"];
	$category = ($_GET["search_tab"] == "") ? "1" : $_GET["search_tab"];
		
	if ($isearch == "")
	{
		$search_end_createdate  = date("Y-m-d", time());
		$search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 9);
	}
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});

	function tab_change(tab)
	{
		window.location.href = "adflag_fbretention.php?category="+tab + "&start_createdate=<?= $search_start_createdate ?>&end_createdate=<?= $search_end_createdate ?>&payday=<?= $search_payday ?>&issearch=1";
	}

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	
	    search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="adflag_fbretention.php">
		<input type=hidden name=issearch value="1">
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; Retention 복귀 분석</div>
                <div class="search_box">
                	<select name="search_tab" id="search_tab">
                		<option value="1" <?= ($category=="1") ? "selected" : "" ?>>Retention1</option>
                		<option value="2" <?= ($category=="2") ? "selected" : "" ?>>Retention2</option>
                		<option value="3" <?= ($category=="3") ? "selected" : "" ?>>Retention3</option>
                		<option value="4" <?= ($category=="4") ? "selected" : "" ?>>Retention4</option>
                		<option value="5" <?= ($category=="5") ? "selected" : "" ?>>Retention5</option>	
                		<option value="10" <?= ($category=="10") ? "selected" : "" ?>>A2U Group1(어제 가입한 전체 유저한테 발송) </option>
                		<option value="11" <?= ($category=="11") ? "selected" : "" ?>>A2U Group2(튜토리얼 완료,27시간 이내 로그인,daylogin=1)</option>
                		<option value="12" <?= ($category=="12") ? "selected" : "" ?>>A2U Group3(2주 이탈자 ,50만 코인 미만,최종 로그인 시간 기준)</option>			
					</select>
					<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
                    <input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
                    &nbsp;&nbsp;&nbsp;&nbsp;결제액 기준 : 복귀 후 <input type="text" class="search_text" id="payday" name="payday" style="width:60px" value="<?= $search_payday ?>" onkeypress="search_press(event); return checknum();" />일 이내 결제
                     <input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
                </div>
            </div>
            <!-- //title_warp -->
            
            <div class="search_result">
                <span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
            </div>
            
            <div id="tab_content_1">
            	<table class="tbl_list_basic1">
		            <colgroup>
		                <col width="">
		                <col width="">
		                <col width="">
		                <col width="">
		                <col width="">
		                <col width="">
		                <col width=""> 
		            </colgroup>
            		<thead>
			            <tr>
			                <th>유입일</th>
			                <th class="tdr">복귀회원수</th>
			                <th class="tdr">평균복귀일</th>
			                <th class="tdr">Engage회원수</th>
			                <th class="tdr">결제회원수</th>
			                <th class="tdr">총결제 금액</th>
			                <th class="tdr">평균결제 금액</th>
			            </tr>
            		</thead>
            		<tbody>
<?
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();

    $now = time();
    
    if ($search_end_createdate != "")
    {
    	$now = strtotime($search_end_createdate);
    }
    else
    {
    	$search_end_createdate = date('Y-m-d');
    }
    
    if ($search_start_createdate == "")
    {
    	$search_start_createdate = '2015-10-20';
    }
   		
	$sum_totalcount = array();
	$sum_leavedays = array();
	$sum_each_leavedays = array();
	$sum_paycount = array();
	$sum_totalcredit = array();
	$sum_engagecount = array();
	
	$list_count = 0;
	
	$now2 = $now;
	$now -= 9 * 60 * 60;

    while (true)
    {
    	$today = date("Y-m-d H:i:s", $now);
    	$tomorrow = date("Y-m-d H:i:s", $now + 24 * 60 * 60);

		$today2 = date("Y-m-d", $now2);
    	
    	if ($tomorrow < $search_start_createdate)
    		break;

		if ($today2 < $search_start_createdate)
		{
    		$totalcount = $sum_totalcount[$fbsource];
    		$leavedays = $sum_leavedays[$fbsource];
    		$each_leavedays = $sum_each_leavedays[$fbsource];
			$paycount = $sum_paycount[$fbsource];
			$totalcredit = $sum_totalcredit[$fbsource];
			$engagecount = $sum_engagecount[$fbsource];
			$leavedays = "";
			$each_leavedays = "";
		}
		else
		{
			$sql = "SELECT COUNT(*) AS totalcount,IFNULL(AVG(leavedays),0) AS leavedays,IFNULL(SUM(leavedays),0) AS each_leavedays FROM tbl_user_fbretention WHERE writedate>='$today' AND writedate<'$tomorrow' AND category='$category'";
			$data = $db_main2->getarray($sql);
			
			$totalcount = $data["totalcount"];
			$leavedays = $data["leavedays"];
			$each_leavedays = $data["each_leavedays"];
			
			if ($category == "1")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='fbretention' AND retentiondate='$today2'");
			else if ($category == "2")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='fbretention2' AND retentiondate='$today2'");
			else if ($category == "3")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='fbretention3' AND retentiondate='$today2'");
			else if ($category == "4")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='fbretention4' AND retentiondate='$today2'");
			else if ($category == "10")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='gamenotifygroup1' AND retentiondate='$today2'");
			else if ($category == "11")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='gamenotifygroup2' AND retentiondate='$today2'");
			else if ($category == "12")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='gamenotifygroup3' AND retentiondate='$today2'");
			
			if ($engagecount == "")
				$engagecount = "0";
			
			$sql = "SELECT useridx FROM tbl_user_fbretention A WHERE writedate>='$today' AND writedate<'$tomorrow' AND category='$category' AND NOT EXISTS (SELECT * FROM tbl_user_fbretention WHERE useridx=A.useridx AND writedate<A.writedate AND writedate>DATE_ADD(A.writedate, INTERVAL -30 DAY))";
			$list = $db_main2->gettotallist($sql);
			
			$userlist = "0";
	
	    	for ($i=0; $i<sizeof($list); $i++)
	    	{
				$useridx = $list[$i]["useridx"];
				
				$userlist .= ",$useridx";
			}
			
			$where_add = "";
			
			if ($search_payday != "")
			{
				$where_add = " AND writedate<=DATE_ADD('$today', INTERVAL $search_payday DAY)";
			}
			
			$sql = "SELECT COUNT(DISTINCT useridx) AS paycount,IFNULL(SUM(facebookcredit),0) AS totalcredit ". 
					"FROM ( ".
					"	SELECT useridx, facebookcredit ".
					"	FROM tbl_product_order WHERE useridx IN ($userlist) AND status=1 AND writedate>='$today' $where_add ".
					") t1";
			
			$data = $db_main->getarray($sql);
			
			$paycount = $data["paycount"];
			$totalcredit = $data["totalcredit"];
					
			if ($sum_totalcount[$fbsource] == "")
				$sum_totalcount[$fbsource] = 0;
    			
			if ($sum_leavedays[$fbsource] == "")
				$sum_leavedays[$fbsource] = 0;
			
			if ($sum_each_leavedays[$fbsource] == "")
				$sum_each_leavedays[$fbsource] = 0;
			
			if ($sum_paycount[$fbsource] == "")
				$sum_paycount[$fbsource] = 0;
    			
			if ($sum_totalcredit[$fbsource] == "")
				$sum_totalcredit[$fbsource] = 0;
								
    		$sum_totalcount[$fbsource] += $totalcount;
    		$sum_leavedays[$fbsource] += $leavedays;
    		$sum_each_leavedays[$fbsource] += $each_leavedays;
			$sum_paycount[$fbsource] += $paycount;
			$sum_totalcredit[$fbsource] += $totalcredit;
			$sum_engagecount[$fbsource] += $engagecount;
		}
		
		if($totalcredit != "0")
			$totalcredit = (string)$totalcredit*0.1;
   		
		if ($totalcount == 0)
		{
			$leaveratio = 0;
			$payratio = 0;
			$averagecredit = 0;
			$engageratio = 0;
		}
		else
		{
			$leaveratio = ($list_count == 0) ? 0 : round($sum_leavedays[$fbsource] / $list_count);
			$payratio = ($totalcount == 0) ? 0 : round($paycount * 10000 / $totalcount) / 100;
			$averagecredit = ($totalcount == 0) ? 0 : round($totalcredit * 100 / $totalcount) / 100;
			$engageratio = ($totalcount == 0) ? 0 : round($engagecount * 10000 / $totalcount) / 100;
		}
		
?>
					<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
	                    <td class="tdc point_title" valign="center"><?= $today2 ?></td>
	                    <td class="tdr point"><?= number_format($totalcount) ?></td>
	                    <td class="tdr point"><?= ($leavedays == "") ? number_format($leaveratio) : number_format($leavedays) ?></td>
	                    <td class="tdr point"><?= number_format($engagecount) ?> (<?= $engageratio ?> %)</td>
	                    <td class="tdr point"><?= number_format($paycount) ?> (<?= $payratio ?> %)</td>
	                    <td class="tdr point">$ <?= number_format($totalcredit) ?></td>
	                    <td class="tdr point">$ <?= $averagecredit ?></td>
	                </tr>
<?
    	$now -= 60 * 60 * 24;
		$now2 -= 60 * 60 * 24;
    	
    	$list_count++;
    }

?>
					<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
	                    <td class="tdc point_title" valign="center"><b>Total</b></td>
	                    <td class="tdr point"><?= number_format($sum_totalcount[$fbsource]) ?></td>
	                    <td class="tdr point"><?= ($sum_totalcount[$fbsource] == 0) ? 0 :  number_format(round($sum_each_leavedays[$fbsource] / $sum_totalcount[$fbsource])) ?></td>
	                    <td class="tdr point"><?= number_format($sum_engagecount[$fbsource]) ?> (<?= ($sum_totalcount[$fbsource] == 0) ? 0 : round($sum_engagecount[$fbsource] / $sum_totalcount[$fbsource] * 100, 2) ?> %)</td>
	                    <td class="tdr point"><?= number_format($sum_paycount[$fbsource]) ?> (<?= ($sum_totalcount[$fbsource] == 0) ? 0: round($sum_paycount[$fbsource] * 10000 / $sum_totalcount[$fbsource]) / 100 ?> %)</td>
	                    <td class="tdr point">$ <?= number_format($sum_totalcredit[$fbsource]*0.1) ?></td>
	                    <td class="tdr point">$ <?= ($sum_totalcount[$fbsource] == 0) ? 0 : round($sum_totalcredit[$fbsource] * 0.1 * 1000 / $sum_totalcount[$fbsource]) / 1000 ?></td>
	                </tr>
<?
	$db_main->end();
	$db_main2->end();
?>    
            	</tbody>
			</table>
		</div>   
	</form>
</div>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
