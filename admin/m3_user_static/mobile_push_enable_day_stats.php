<?
	$top_menu = "user_static";
	$sub_menu = "mobile_push_enable_day_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
	$pagename = "mobile_push_enable_day_stats.php";
	$type = ($_GET["search_tab"] == "") ? "1" : $_GET["search_tab"];
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today,7,"d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db_analysis = new CDatabase_Analysis();
	$db_other = new CDatabase_Other();
	
	if($type == 1 || $type == 2)
	{
		$sql = "SELECT * FROM user_push_enabled_stat WHERE TYPE = $type AND today BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' ORDER BY today DESC, os ASC";
		$user_push_enabled_list = $db_analysis->gettotallist($sql);
		
		$sql = "SELECT COUNT(today) as rowcount FROM user_push_enabled_stat WHERE TYPE = $type AND today BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY today ORDER BY today DESC";
		$today_rowspan_list = $db_analysis->gettotallist($sql);
	}
	else if($type == 3)
	{
		$sql = "SELECT * FROM tbl_push_enable_stat WHERE today BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' ORDER BY today ASC, platform ASC";
		$push_enable_list = $db_other->gettotallist($sql);
		
		$sql = "SELECT COUNT(today) as rowcount FROM tbl_push_enable_stat WHERE today BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY today ORDER BY today ASC";
		$today_rowspan_list = $db_other->gettotallist($sql);
	}
	else if($type == 4)
	{
		$sql = "SELECT * FROM tbl_push_reward_cancel_stat WHERE today BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' ORDER BY today DESC, platform ASC";
		$push_reward_cancel_list = $db_other->gettotallist($sql);
		
		$sql ="SELECT COUNT(today) as rowcount FROM tbl_push_reward_cancel_stat WHERE today BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY today ORDER BY today DESC";
		$today_rowspan_list = $db_other->gettotallist($sql);
	}
	
	$db_analysis->end();
	$db_other->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
        function search_press(e)
        {
            if (((e.which) ? e.which : e.keyCode) == 13)
            {
                search();
            }
        }
        	
        function search()
        {
            var search_form = document.search_form;
        	
            search_form.submit();
        }
        
        $(function() {
            $("#startdate").datepicker({ });
        });
        	
        $(function() {
            $("#enddate").datepicker({ });
       });
</script>
    
    <!-- CONTENTS WRAP -->
<div class="contents_wrap">
   	<!-- title_warp -->
   	<div class="title_wrap">
   		<div class="title"><?= $top_menu_txt ?> &gt; Mobile Push Enable 일별 통계 </div>
   		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
   			<div class="search_box">
   				<select name="search_tab" id="search_tab">
   					<option value="1" <?= ($type == "1") ? "selected" : "" ?>>DAU Enabled 현황</option>
   					<option value="2" <?= ($type == "2") ? "selected" : "" ?>>2주 활동자의 enable 현황 (Daily)</option>
   					<option value="3" <?= ($type == "3") ? "selected" : "" ?>>신규 사용자 Enable 비중</option>
   					<!-- <option value="4" <?= ($type == "4") ? "selected" : "" ?>>Push 설정 리워드 현황</option>  -->
   				</select>
   				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
   				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
   				<input type="button" class="btn_search" value="검색" onclick="search()" />
   			</div>
   		</form>
   	</div>
   	<!-- //title_warp -->
    	
   	<div class="search_result">
   		<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다
   	</div>
<?
	if($type == 1)
	{
?>
   	<div class="h2_title">[DAU Enabled 현황]</div>
   	
<?
	}
	else if($type == 2) 
	{
?>	
   	<div class="h2_title">[2주 활동자의 Enable 현황 (Daily)]</div>
<?
	}
	else if($type == 3)
	{
?>
	<div class="h2_title">[신규 사용자 Enable 비중]</div>
<?
	}
	else if($type == 4)
	{
?>
	<div class="h2_title">[Push 설정 리워드 현황]</div>
<?
	}
?>

	<div id="tab_content_1">
    	<table class="tbl_list_basic1">
        <colgroup>
<?
	if($type == 1 || $type == 2)
	{
?>
        	<col width="180">
            <col width="">
            <col width="250">
            <col width="250">
            <col width="250">
<?
	}
	else if($type == 3)
	{
?>
			<col width="80">
			<col width="70">
			<col width="120">
<?
		for($i=0; $i<15; $i++)
		{
?>
			<col width="55">
<?
		}
	}
	else if($type == 4)
	{
?>
			<col width="200">
			<col width="100">
			<col width="260">
			<col width="260">
			<col width="260">
<?
	}
?>
        </colgroup>
        <thead>
	        <tr>
<?
	if($type == 1 || $type == 2)
	{
?>
	            <th class="tdc">날짜</th>
	            <th class="tdc">플랫폼</th>
	            <th class="tdr">활동 회원수</th>
	            <th class="tdr">Enable 회원수</th>
	            <th class="tdr">비중</th>
<?
	}
	else if($type == 3)
	{
?>
				<th class="tdc">날짜</th>
	            <th class="tdc">플랫폼</th>
	            <th class="tdc">신규 회원수</th>
<?
		for($i=0; $i<15; $i++)
		{
?>
				<th class="tdc">D+<?= $i ?></th>
<?
		}
	}
	else if($type == 4)
	{
?>
				<th class="tdc">날짜</th>
				<th class="tdc">플랫폼</th>
				<th class="tdr">사용자수</th>
				<th class="tdr">D+1 해지 사용자수</th>
				<th class="tdr">D+7 해지 사용자수</th>
<?
	}
?>
	        </tr>
        </thead>
        <tbody>
<?
	$rowspan_index = 0;
	$tmp_today = $today;
	
	if($type == 1 || $type == 2)
	{
		for($i=0; $i<sizeof($user_push_enabled_list); $i++)
		{
			$today = $user_push_enabled_list[$i]["today"];
			$os = $user_push_enabled_list[$i]["os"];
			$usercount = $user_push_enabled_list[$i]["usercount"];
			$push_enabled_count = $user_push_enabled_list[$i]["push_enabled_count"];
			$specific = ($push_enabled_count / $usercount) * 100;
			
			if($os == "1")
				$os = "iOS";
			else if($os == "2")
				$os = "Android";
			else if($os == "3")
				$os = "Amazon";
		
?>
        	<tr>
<?
			if($tmp_today != $today || $i == 0)
			{
?>
        		<td class="tdc point" rowspan="<?= $today_rowspan_list[$rowspan_index++]["rowcount"] ?>"><?= $today ?></td>
<?
			}
?>
        		<td class="tdc"><?= $os ?></td>
        		<td class="tdr"><?= number_format($usercount) ?></td>
        		<td class="tdr"><?= number_format($push_enabled_count) ?></td>
        		<td class="tdr"><?= number_format($specific, 2) ?>%</td>
        	</tr>
<?
			$tmp_today = $today;
		}
	}
	else if($type == 3)
	{
		for($i=0; $i<sizeof($push_enable_list); $i++)
		{
			$today = $push_enable_list[$i]["today"];
			$platform = $push_enable_list[$i]["platform"];
			$join_count = $push_enable_list[$i]["join_count"];
			$day_0 = ($push_enable_list[$i]["day_0"] / $join_count) * 100;
			$day_1 = ($push_enable_list[$i]["day_1"] / $join_count) * 100; 
			$day_2 = ($push_enable_list[$i]["day_2"] / $join_count) * 100; 
			$day_3 = ($push_enable_list[$i]["day_3"] / $join_count) * 100; 
			$day_4 = ($push_enable_list[$i]["day_4"] / $join_count) * 100; 
			$day_5 = ($push_enable_list[$i]["day_5"] / $join_count) * 100; 
			$day_6 = ($push_enable_list[$i]["day_6"] / $join_count) * 100; 
			$day_7 = ($push_enable_list[$i]["day_7"] / $join_count) * 100; 
			$day_8 = ($push_enable_list[$i]["day_8"] / $join_count) * 100; 
			$day_9 = ($push_enable_list[$i]["day_9"] / $join_count) * 100; 
			$day_10 = ($push_enable_list[$i]["day_10"] / $join_count) * 100; 
			$day_11 = ($push_enable_list[$i]["day_11"] / $join_count) * 100; 
			$day_12 = ($push_enable_list[$i]["day_12"] / $join_count) * 100; 
			$day_13 = ($push_enable_list[$i]["day_13"] / $join_count) * 100; 
			$day_14 = ($push_enable_list[$i]["day_14"] / $join_count) * 100;
			
			if($platform == "1")
				$platform = "iOS";
			else if($platform == "2")
				$platform = "Android";
			else if($platform == "3")
				$platform = "Amazon";
?>
			<tr>
<?
			if($tmp_today != $today || $i == 0)
			{
?>
        		<td class="tdc point" rowspan="<?= $today_rowspan_list[$rowspan_index++]["rowcount"] ?>"><?= $today ?></td>
<?
			}
?>
				<td class="tdc"><?= $platform ?></td>
        		<td class="tdr"><?= number_format($join_count) ?></td>
        		<td class="tdr"><?= number_format($day_0)?>%</td>
        		<td class="tdr"><?= number_format($day_1)?>%</td>
        		<td class="tdr"><?= number_format($day_2)?>%</td>
        		<td class="tdr"><?= number_format($day_3)?>%</td>
        		<td class="tdr"><?= number_format($day_4)?>%</td>
        		<td class="tdr"><?= number_format($day_5)?>%</td>
        		<td class="tdr"><?= number_format($day_6)?>%</td>
        		<td class="tdr"><?= number_format($day_7)?>%</td>
        		<td class="tdr"><?= number_format($day_8)?>%</td>
        		<td class="tdr"><?= number_format($day_9)?>%</td>
        		<td class="tdr"><?= number_format($day_10)?>%</td>
        		<td class="tdr"><?= number_format($day_11)?>%</td>
        		<td class="tdr"><?= number_format($day_12)?>%</td>
        		<td class="tdr"><?= number_format($day_13)?>%</td>
        		<td class="tdr"><?= number_format($day_14)?>%</td>
			</tr>
<?
			$tmp_today = $today;
		}
	}
	else if($type == 4)
	{
		for($i=0; $i<sizeof($push_reward_cancel_list); $i++)
		{
			$today = $push_reward_cancel_list[$i]["today"];
			$platform = $push_reward_cancel_list[$i]["platform"];
			$user_count = $push_reward_cancel_list[$i]["user_count"];
			$d1_cancel = $push_reward_cancel_list[$i]["d1_cancel"];
			$d7_cancel = $push_reward_cancel_list[$i]["d7_cancel"];
			
			if($platform == "1")
				$platform = "iOS";
			else if($platform == "2")
				$platform = "Android";
			else if($platform == "3")
				$platform = "Amazon";
?>
			<tr>
<?
			if($tmp_today != $today || $i == 0)
			{
?>
        		<td class="tdc point" rowspan="<?= $today_rowspan_list[$rowspan_index++]["rowcount"] ?>"><?= $today ?></td>
<?
			}
?>
        		<td class="tdc"><?= $platform ?></td>
        		<td class="tdr"><?= number_format($user_count) ?></td>
        		<td class="tdr"><?= number_format($d1_cancel) ?></td>
        		<td class="tdr"><?= number_format($d7_cancel) ?></td>
        	</tr>
<?
			$tmp_today = $today;
		}
	}
?>
        </tbody>
        </table>
	</div>
</div>
<!--  //CONTENTS WRAP -->
            
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");    
?>
