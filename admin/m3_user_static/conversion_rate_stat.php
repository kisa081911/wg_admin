<?
	$top_menu = "user_static";
	$sub_menu = "conversion_rate_stat";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	function dates_weekofyear($year, $week)
	{
		for($i=1; $i<=7; $i++)
		{
			$first_week_day = mktime(0, 0, 0, 1, $i, $year);
	
			if(date('W', $first_week_day) == 1)
			{
				$first_monday = $i;
				$first_sunday = $i + 6;
				break;
			}
		}
	
		$week_day[0] = date('Y-m-d', mktime(0, 0, 0, 1, ($first_monday) + 7 * ($week - 1), $year));
		$week_day[1] = date('Y-m-d', mktime(0, 0, 0, 1, ($first_sunday) + 7 * ($week - 1), $year));
	
		return $week_day;
	}
	
	$db_other = new CDatabase_Other();
	
	if($_GET["start_date"] == "")
	{
		$start_date = date("Y-m-d");
		
		$sql = "SELECT DATE_FORMAT('$start_date','%Y'),WEEK('$start_date')";
		$start_week_list = $db_other->getarray($sql);
		$start_week = $start_week_list[1];
		$start_year = $start_week_list[0];
	}
	else
	{
		$search_date = $_GET["start_date"];
		
		$start_date = $search_date;
		
		$sql = "SELECT DATE_FORMAT('$start_date','%Y'),WEEK('$start_date')";
		$start_week_list = $db_other->getarray($sql);
		$start_week = $start_week_list[1];
		$start_year = $start_week_list[0];
	}
	
	$sql = "SELECT week, max(join_count) AS join_count, SUM(conversion_count) AS conversion_count
			FROM tbl_user_conversion_daily
			WHERE week <= $start_week
			AND year = $start_year
			GROUP BY week
			ORDER BY week DESC";
	$conversion_list = $db_other->gettotallist($sql);
	
//  	$sql = "SELECT week, COUNT(regweek) AS regweek_cnt, SUM(conversion_count) AS conversion_count
// 			FROM tbl_user_conversion_daily 
// 			WHERE regweek >= $start_week
// 			GROUP BY week ORDER BY week DESC ";
//  	$total_conversion_list = $db_other->gettotallist($sql);
 	
 	$db_other->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript">
	$(function() {
    	$("#start_date").datepicker({ });
	});

	function search()
    {
        var search_form = document.search_form;
            
        if (search_form.start_date.value == "")
        {
            alert("기준일을 입력하세요.");
            search_form.start_date.focus();
            return;
        } 

        search_form.submit();
    }
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 모바일 전환율 현황 </div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<span class="search_lbl">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="start_date" name="start_date" style="width:70px" readonly="readonly" value="<?= $start_date ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/>
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->

	<div class="search_result">
		<span><?= $start_date ?></span> 통계입니다
	</div>
	
	<div class="search_result">Web 가입자 -> Mobile 연동 현황 </div>
	
	<table class="tbl_list_basic1" style="width:900px;margin-left: auto; margin-right: auto;">
		<colgroup>
			<col>
			<col>
			<col>
            <col>
		</colgroup>
        <thead>
         	<tr>
         		<th>가입 Week</th>
         		<th class="tdr">가입자수</th>
         		<th class="tdr">모바일연동수</th>
               	<th class="tdr">모바일연동비율</th>
         	</tr>
        </thead>
        <tbody>
<?
    for($i=0; $i<sizeof($conversion_list); $i++)
    {
    	$week = str_replace('2017','', $conversion_list[$i]["week"]);
    	$join_count = $conversion_list[$i]["join_count"];
    	$conversion_count = $conversion_list[$i]["conversion_count"];
    	$conversion_rate = ($join_count == 0) ? 0 : round(($conversion_count / $join_count) * 100, 2);
    	
     	$weekcnt = $total_conversion_list[$rowcnt]["regweek_cnt"];
     	
     	$week_arr = dates_weekofyear($start_year, $week);
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
			<td class="tdc point" rowspan="<?= $weekcnt?>"><?= $week?>주차 <text style="font-size:12px">( <?= $week_arr[0] ?> ~ <?= $week_arr[1] ?> )</text></td>
            <td class="tdr" rowspan="<?= $weekcnt?>"><?= number_format($join_count) ?></td>
            <td class="tdr"><?= number_format($conversion_count) ?></td>
            <td class="tdr"><?= $conversion_rate ?>%</td>
        </tr>
<?
    }
    
    if(sizeof($conversion_list) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="18">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>
	
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>