<?
	$top_menu = "user_static";
	$sub_menu = "mobile_version_daily";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	if($_GET["search_date"] == "")
		$search_date = date("Y-m-d",strtotime("-1 days"));
	else
		$search_date = $_GET["search_date"];
	
	$db_mobile = new CDatabase_Mobile();
	
	$sql = "SELECT SUM(count) FROM `mobile_version_daily` WHERE today = '$search_date' AND os_type = 1";
	$ios_total = $db_mobile->getvalue($sql);
	
	$sql = "SELECT * FROM `mobile_version_daily` WHERE today = '$search_date' AND os_type = 1 ORDER BY SUBSTRING(VERSION, 1, 2) DESC, CAST(SUBSTRING(VERSION, 3, 2) AS UNSIGNED) DESC, CAST(SUBSTRING(VERSION, 6, 1) AS UNSIGNED) DESC";	
	$ios_verion = $db_mobile->gettotallist($sql);
	
	$sql = "SELECT SUM(count) FROM `mobile_version_daily` WHERE today = '$search_date' AND os_type = 2";
	$and_total = $db_mobile->getvalue($sql);
	
	$sql = "SELECT * FROM `mobile_version_daily` WHERE today = '$search_date' AND os_type = 2 ORDER BY SUBSTRING(VERSION, 1, 2) DESC, CAST(SUBSTRING(VERSION, 3, 2) AS UNSIGNED) DESC, CAST(SUBSTRING(VERSION, 6, 1) AS UNSIGNED) DESC";
	$and_verion = $db_mobile->gettotallist($sql);
	
	$sql = "SELECT SUM(count) FROM `mobile_version_daily` WHERE today = '$search_date' AND os_type = 3";
	$ama_total = $db_mobile->getvalue($sql);
	
	$sql = "SELECT * FROM `mobile_version_daily` WHERE today = '$search_date' AND os_type = 3 ORDER BY SUBSTRING(VERSION, 1, 2) DESC, CAST(SUBSTRING(VERSION, 3, 2) AS UNSIGNED) DESC, CAST(SUBSTRING(VERSION, 6, 1) AS UNSIGNED) DESC";
	$ama_verion = $db_mobile->gettotallist($sql);
	
	$db_mobile->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript">
	$(function() {
		$("#search_date").datepicker({ });
	});

	function search()
	{
		var search_form = document.search_form;
	    
		if (search_form.search_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.search_date.focus();
	    	return;
		} 
	
		search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 모바일 버전 통계 </div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">				
				<span class="search_lbl">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="search_date" name="search_date" style="width:75px" readonly="readonly" value="<?= $search_date ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/>
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->

	<div class="search_result">
		<span><?= $search_date ?></span> 현황입니다(최근 하루이내 로그인)
	</div>
	
	<div style=" float:left;">
	<div class="h2_title pt20">iOS</div>
	<table class="tbl_list_basic1" style="width: 350px;">
		<colgroup>
			<col width="100">
			<col width="150">
			<col width="100">
		</colgroup>
        <thead>
        	<tr>
        		<th>버전</th>
            	<th>사용자수</th>
             	<th>비율</th>
         	</tr>
        </thead>
        <tbody>
<?
    for($i=0; $i<sizeof($ios_verion); $i++)
    {
    	$version = $ios_verion[$i]["version"];
    	$count = $ios_verion[$i]["count"];
    	$per = ($count/$ios_total) * 100;
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="tdc"><?= $version ?></td>
            <td class="tdr"><?= number_format($count) ?></td>
            <td class="tdr"><?= round($per, 2) ?>%</td>
        </tr>
<?
    }
?>
		

<?    
    if(sizeof($ios_verion) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="3">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
   	else
   	{
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="tdc point">합계</td>
            <td class="tdr point"><?= number_format($ios_total) ?></td>
            <td class="tdr point">100%</td>
        </tr>
<?
   	} 
?>
        </tbody>
	</table>
	</div>
	
	<div style="float:left; margin-left:15px;">
	<div class="h2_title pt20">Android</div>
	<table class="tbl_list_basic1" style="width: 350px;">
		<colgroup>
			<col width="100">
			<col width="150">
			<col width="100">
		</colgroup>
        <thead>
        	<tr>
        		<th>버전</th>
            	<th>사용자수</th>
             	<th>비율</th>
         	</tr>
        </thead>
        <tbody>
<?
    for($i=0; $i<sizeof($and_verion); $i++)
    {
    	$version = $and_verion[$i]["version"];
    	$count = $and_verion[$i]["count"];
    	$per = ($count/$and_total) * 100;
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="tdc"><?= $version ?></td>
            <td class="tdr"><?= number_format($count) ?></td>
            <td class="tdr"><?= round($per, 2) ?>%</td>
        </tr>
<?
    }
?>
		

<?    
    if(sizeof($and_verion) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="3">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
   	else
   	{
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="tdc point">합계</td>
            <td class="tdr point"><?= number_format($and_total) ?></td>
            <td class="tdr point">100%</td>
        </tr>
<?
   	} 
?>
        </tbody>
	</table>
	</div>
	
	<div style="float:left; margin-left:15px;">
	<div class="h2_title pt20">Amazon</div>
	<table class="tbl_list_basic1" style="width: 350px;">
		<colgroup>
			<col width="100">
			<col width="150">
			<col width="100">
		</colgroup>
        <thead>
        	<tr>
        		<th>버전</th>
            	<th>사용자수</th>
             	<th>비율</th>
         	</tr>
        </thead>
        <tbody>
<?
    for($i=0; $i<sizeof($ama_verion); $i++)
    {
    	$version = $ama_verion[$i]["version"];
    	$count = $ama_verion[$i]["count"];
    	$per = ($count/$ama_total) * 100;
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="tdc"><?= $version ?></td>
            <td class="tdr"><?= number_format($count) ?></td>
            <td class="tdr"><?= round($per, 2) ?>%</td>
        </tr>
<?
    }
?>
		

<?    
    if(sizeof($ama_verion) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="3">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
   	else
   	{
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="tdc point">합계</td>
            <td class="tdr point"><?= number_format($ama_total) ?></td>
            <td class="tdr point">100%</td>
        </tr>
<?
   	} 
?>
        </tbody>
	</table>
	</div>
	
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>