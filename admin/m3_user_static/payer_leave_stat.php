<?
	include("../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_otherdb = new CDatabase_Other();
	$db_slave_main = new CDatabase_Slave_Main();
	
	$db_main->execute("SET wait_timeout=7200");
	$db_main2->execute("SET wait_timeout=7200");
	$db_otherdb->execute("SET wait_timeout=7200");
	$db_slave_main->execute("SET wait_timeout=7200");
	
	$today = date("Y-m-d", time() - 60 * 60);
    		
    $web_leaveday_7day_count = 0;
    $web_leaveday_14day_count = 0;
    $web_leaveday_28day_count = 0;
    		
    $ios_leaveday_7day_count = 0;
    $ios_leaveday_14day_count = 0;
    $ios_leaveday_28day_count = 0;
    		
    $and_leaveday_7day_count = 0;
    $and_leaveday_14day_count = 0;
    $and_leaveday_28day_count = 0;
    		
    $ama_leaveday_7day_count = 0;
    $ama_leaveday_14day_count = 0;
    $ama_leaveday_28day_count = 0;
    		
    // 이탈
    $sql = "SELECT IF(adflag LIKE 'mobile%', adflag, 'web') AS adflag, flag, COUNT(*) AS total ".
			"FROM ( ".
			"	SELECT useridx, logindate, DATEDIFF(NOW(), logindate) AS flag, adflag ".
			"	FROM tbl_user_ext ". 
			"	WHERE useridx > 20000 AND ( logindate > DATE_SUB(NOW(), INTERVAL 8 DAY) AND logindate < DATE_SUB(NOW(), INTERVAL 6 DAY) ". 
			"	|| logindate > DATE_SUB(NOW(), INTERVAL 15 DAY) AND logindate < DATE_SUB(NOW(), INTERVAL 13 DAY) || logindate > DATE_SUB(NOW(), INTERVAL 29 DAY) AND logindate < DATE_SUB(NOW(), INTERVAL 27 DAY)) ".
			") t1 JOIN tbl_user_membership t2 ON t1.useridx = t2.useridx ". 
			"WHERE t2.member_point > 0 AND flag IN (7, 14, 28) ".
			"GROUP BY IF(adflag LIKE 'mobile%', adflag, 'web'), flag ";
    		 
    $leave_list = $db_main->gettotallist($sql);
    		 
    for($i=0; $i<sizeof($leave_list); $i++)
    {
    	$adflag = $leave_list[$i]["adflag"];
    	$flag = $leave_list[$i]["flag"];
    	$total = $leave_list[$i]["total"];
    	
    	if($adflag == "web")
    	{
    		if($flag == 7)
    			$web_leaveday_7day_count = $total;
    		else if($flag == 14)
    			$web_leaveday_14day_count = $total;
    		else if($flag == 28)
    			$web_leaveday_28day_count = $total;
    	}
    	else if($adflag == "mobile")
    	{
    		if($flag == 7)
    			$ios_leaveday_7day_count = $total;
    		else if($flag == 14)
    			$ios_leaveday_14day_count = $total;
    		else if($flag == 28)
    			$ios_leaveday_28day_count = $total;
    	}
    	else if($adflag == "mobile_ad")
    	{
    		if($flag == 7)
    			$and_leaveday_7day_count = $total;
    		else if($flag == 14)
    			$and_leaveday_14day_count = $total;
    		else if($flag == 28)
    			$and_leaveday_28day_count = $total;
    	}
    	else if($adflag == "mobile_kindle")
    	{
    		if($flag == 7)
    			$ama_leaveday_7day_count = $total;
    		else if($flag == 14)
    			$ama_leaveday_14day_count = $total;
    		else if($flag == 28)
    			$ama_leaveday_28day_count = $total;
    	}
    }
    		
    $web_retention_7day_count = 0;
    $web_retention_14day_count = 0;
    $web_retention_28day_count = 0;
    
    $ios_retention_7day_count = 0;
    $ios_retention_14day_count = 0;
    $ios_retention_28day_count = 0;
    		
    $and_retention_7day_count = 0;
    $and_retention_14day_count = 0;
    $and_retention_28day_count = 0;
    		
    $ama_retention_7day_count = 0;
    $ama_retention_14day_count = 0;
    $ama_retention_28day_count = 0;    
	
    // 7일이상 복귀
    $sql = "SELECT useridx ".
			"FROM `tbl_user_retention_log` ". 
			"WHERE useridx > 20000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND is_payer = 1 AND leavedays >= 7 ";
    		
    $retention_list = $db_main2->gettotallist($sql);
    
    $userlist = "";
    		 
    for($i=0; $i<sizeof($retention_list); $i++)
    {
    	$useridx = $retention_list[$i]["useridx"];
    	
    	if($userlist == "")
    		$userlist = $useridx;
    	else
    		$userlist .= ",".$useridx;
    }
    
	$sql = "SELECT IF(adflag LIKE 'mobile%', adflag, 'web') AS adflag, COUNT(*) AS total FROM tbl_user_ext WHERE useridx IN ($userlist) GROUP BY IF(adflag LIKE 'mobile%', adflag, 'web')";
    $retention_week_list = $db_slave_main->gettotallist($sql);
    
    for($i=0; $i<sizeof($retention_week_list); $i++)
    {
    	$adflag = $retention_week_list[$i]["adflag"];
    	$total = $retention_week_list[$i]["total"];
    	
    	if($adflag == "web")
    		$web_retention_7day_count = $total;
    	else if($adflag == "mobile")
    		$ios_retention_7day_count = $total;
    	else if($adflag == "mobile_ad")
    		$and_retention_7day_count = $total;
    	else if($adflag == "mobile_kindle")
    		$ama_retention_7day_count = $total;
    }
    		
    $sql = "SELECT useridx ".
    		"FROM `tbl_user_retention_mobile_log` ".
    		"WHERE useridx > 20000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND is_payer = 1 AND leavedays >= 7 ";
    	
    $retention_mobile_list = $db_main2->gettotallist($sql);
    
    $userlist = "";
     
    for($i=0; $i<sizeof($retention_mobile_list); $i++)
    {
    	$useridx = $retention_mobile_list[$i]["useridx"];
    	 
    	if($userlist == "")
    		$userlist = $useridx;
    	else
    		$userlist .= ",".$useridx;
    }
    
    $sql = "SELECT IF(adflag LIKE 'mobile%', adflag, 'web') AS adflag, COUNT(*) AS total FROM tbl_user_ext WHERE useridx IN ($userlist) GROUP BY IF(adflag LIKE 'mobile%', adflag, 'web')";
    $mobile_retention_week_list = $db_slave_main->gettotallist($sql);    
    
    for($i=0; $i<sizeof($mobile_retention_week_list); $i++)
    {
    	$adflag = $mobile_retention_week_list[$i]["adflag"];
    	$total = $mobile_retention_week_list[$i]["total"];
    	 
    	if($adflag == "web")
    		$web_retention_7day_count += $total;
    	else if($adflag == "mobile")
    		$ios_retention_7day_count += $total;
    	else if($adflag == "mobile_ad")
    		$and_retention_7day_count += $total;
    	else if($adflag == "mobile_kindle")
    		$ama_retention_7day_count += $total;
    }
    
    // 14일이상 복귀
    $sql = "SELECT useridx ".
    		"FROM `tbl_user_retention_log` ".
    		"WHERE useridx > 20000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND is_payer = 1 AND leavedays >= 14 ";
    
    $retention_list = $db_main2->gettotallist($sql);
    
    $userlist = "";
     
    for($i=0; $i<sizeof($retention_list); $i++)
    {
    	$useridx = $retention_list[$i]["useridx"];
    	 
    	if($userlist == "")
    		$userlist = $useridx;
    	else
    		$userlist .= ",".$useridx;
    }
    
    $sql = "SELECT IF(adflag LIKE 'mobile%', adflag, 'web') AS adflag, COUNT(*) AS total FROM tbl_user_ext WHERE useridx IN ($userlist) GROUP BY IF(adflag LIKE 'mobile%', adflag, 'web')";
    $retention_2week_list = $db_slave_main->gettotallist($sql);
    
    for($i=0; $i<sizeof($retention_2week_list); $i++)
    {
    	$adflag = $retention_2week_list[$i]["adflag"];
    	$total = $retention_2week_list[$i]["total"];
    	 
    	if($adflag == "web")
    		$web_retention_14day_count = $total;
    	else if($adflag == "mobile")
    		$ios_retention_14day_count = $total;
    	else if($adflag == "mobile_ad")
    		$and_retention_14day_count = $total;
    	else if($adflag == "mobile_kindle")
    		$ama_retention_14day_count = $total;
    }
    
    $sql = "SELECT useridx ".
    		"FROM `tbl_user_retention_mobile_log` ".
    		"WHERE useridx > 20000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND is_payer = 1 AND leavedays >= 14 ";
     
    $retention_mobile_list = $db_main2->gettotallist($sql);
    
    $userlist = "";
     
    for($i=0; $i<sizeof($retention_mobile_list); $i++)
    {
    	$useridx = $retention_mobile_list[$i]["useridx"];
    
    	if($userlist == "")
    		$userlist = $useridx;
    	else
    		$userlist .= ",".$useridx;
    }
    
    $sql = "SELECT IF(adflag LIKE 'mobile%', adflag, 'web') AS adflag, COUNT(*) AS total FROM tbl_user_ext WHERE useridx IN ($userlist) GROUP BY IF(adflag LIKE 'mobile%', adflag, 'web')";
    $mobile_retention_2week_list = $db_slave_main->gettotallist($sql);
    
    for($i=0; $i<sizeof($mobile_retention_2week_list); $i++)
    {
    	$adflag = $mobile_retention_2week_list[$i]["adflag"];
    	$total = $mobile_retention_2week_list[$i]["total"];
    
    	if($adflag == "web")
    		$web_retention_14day_count += $total;
    	else if($adflag == "mobile")
    		$ios_retention_14day_count += $total;
    	else if($adflag == "mobile_ad")
    		$and_retention_14day_count += $total;
    	else if($adflag == "mobile_kindle")
    		$ama_retention_14day_count += $total;
    }
    
    // 28일이상 복귀
    $sql = "SELECT useridx ".
    		"FROM `tbl_user_retention_log` ".
    		"WHERE useridx > 20000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND is_payer = 1 AND leavedays >= 28 ";
    
    $retention_list = $db_main2->gettotallist($sql);
    
    $userlist = "";
     
    for($i=0; $i<sizeof($retention_list); $i++)
    {
    	$useridx = $retention_list[$i]["useridx"];
    
    	if($userlist == "")
    		$userlist = $useridx;
    	else
    		$userlist .= ",".$useridx;
    }
    
    $sql = "SELECT IF(adflag LIKE 'mobile%', adflag, 'web') AS adflag, COUNT(*) AS total FROM tbl_user_ext WHERE useridx IN ($userlist) GROUP BY IF(adflag LIKE 'mobile%', adflag, 'web')";
    $retention_4week_list = $db_slave_main->gettotallist($sql);
    
    for($i=0; $i<sizeof($retention_4week_list); $i++)
    {
    	$adflag = $retention_4week_list[$i]["adflag"];
    	$total = $retention_4week_list[$i]["total"];
    
    	if($adflag == "web")
    		$web_retention_28day_count = $total;
    	else if($adflag == "mobile")
    		$ios_retention_28day_count = $total;
    	else if($adflag == "mobile_ad")
    		$and_retention_28day_count = $total;
    	else if($adflag == "mobile_kindle")
    		$ama_retention_28day_count = $total;
    }
    
    $sql = "SELECT useridx ".
    		"FROM `tbl_user_retention_mobile_log` ".
    		"WHERE useridx > 20000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND is_payer = 1 AND leavedays >= 28 ";
     
    $retention_mobile_list = $db_main2->gettotallist($sql);
    
    $userlist = "";
     
    for($i=0; $i<sizeof($retention_mobile_list); $i++)
    {
    	$useridx = $retention_mobile_list[$i]["useridx"];
    
    	if($userlist == "")
    		$userlist = $useridx;
    	else
    		$userlist .= ",".$useridx;
    }
    
    $sql = "SELECT IF(adflag LIKE 'mobile%', adflag, 'web') AS adflag, COUNT(*) AS total FROM tbl_user_ext WHERE useridx IN ($userlist) GROUP BY IF(adflag LIKE 'mobile%', adflag, 'web')";
    $mobile_retention_4week_list = $db_slave_main->gettotallist($sql);
    
    for($i=0; $i<sizeof($mobile_retention_4week_list); $i++)
    {
    	$adflag = $mobile_retention_4week_list[$i]["adflag"];
    	$total = $mobile_retention_4week_list[$i]["total"];
    
    	if($adflag == "web")
    		$web_retention_28day_count += $total;
    	else if($adflag == "mobile")
    		$ios_retention_28day_count += $total;
    	else if($adflag == "mobile_ad")
    		$and_retention_28day_count += $total;
    	else if($adflag == "mobile_kindle")
    		$ama_retention_28day_count += $total;
    }
    		
    $web_firstorder_count = 0;
    $ios_firstorder_count = 0;
    $and_firstorder_count = 0;
    $ama_firstorder_count = 0;
    		
    // 첫결제자 분포
    $sql = "SELECT IF(adflag LIKE 'mobile%', adflag, 'web') AS adflag, COUNT(*) AS total ".
    		"FROM tbl_user_first_order ".
    		"WHERE purchase_date = '$today' ".
    		"GROUP BY IF(adflag LIKE 'mobile%', adflag, 'web') ";
    		
    $firstorder_list = $db_main2->gettotallist($sql);
    	
    for($i=0; $i<sizeof($firstorder_list); $i++)
    {
    	$adflag = $firstorder_list[$i]["adflag"];
    	$total = $firstorder_list[$i]["total"];
    	
    	if($adflag == "web")
			$web_firstorder_count = $total;
		else if($adflag == "mobile")
			$ios_firstorder_count = $total;
		else if($adflag == "mobile_ad")
			$and_firstorder_count = $total;
		else if($adflag == "mobile_kindle")
			$ama_firstorder_count = $total;
    }
    		 
    $sql = "INSERT INTO tbl_payer_login_stat VALUES('$today', 0, $web_firstorder_count, $web_leaveday_7day_count, $web_leaveday_14day_count, $web_leaveday_28day_count, $web_retention_7day_count, $web_retention_14day_count, $web_retention_28day_count), ".
      		"('$today', 1, $ios_firstorder_count, $ios_leaveday_7day_count, $ios_leaveday_14day_count, $ios_leaveday_28day_count, $ios_retention_7day_count, $ios_retention_14day_count, $ios_retention_28day_count), ".
      		"('$today', 2, $and_firstorder_count, $and_leaveday_7day_count, $and_leaveday_14day_count, $and_leaveday_28day_count, $and_retention_7day_count, $and_retention_14day_count, $and_retention_28day_count), ".
      		"('$today', 3, $ama_firstorder_count, $ama_leaveday_7day_count, $ama_leaveday_14day_count, $ama_leaveday_28day_count, $ama_retention_7day_count, $ama_retention_14day_count, $ama_retention_28day_count) ";
    $db_otherdb->execute($sql);
    		
	$db_main->end();
	$db_main2->end();
	$db_otherdb->end();
	$db_slave_main->end();
?>
