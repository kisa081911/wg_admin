<?
	$top_menu = "user_static";
	$sub_menu = "user_vip_level_stats";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["start_createdate"];
	$pagename = "user_vip_level_stats.php";
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today,15,"d");
	$search_start_createdate = ($search_start_createdate == "") ? $before_day : $search_start_createdate;
	$search_end_createdate = ($enddate == "") ? $today : $search_end_createdate;

	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT * ".
			"FROM ".
			"( ".
				"SELECT today, type, vip_level_0, vip_level_1, vip_level_2, vip_level_3, vip_level_4, vip_level_5,  vip_level_6, vip_level_7, vip_level_8, vip_level_9, vip_level_10 ".
				"FROM tbl_vip_level_stat_log WHERE TYPE = 0 AND today between '$search_start_createdate' AND '$search_end_createdate' ". 
				"UNION ALL ". 
				"SELECT today, type, vip_level_0, vip_level_1, vip_level_2, vip_level_3, vip_level_4, vip_level_5,  vip_level_6, vip_level_7, vip_level_8, vip_level_9, vip_level_10 ".
				"FROM tbl_vip_level_stat_log WHERE TYPE = 1 AND today between '$search_start_createdate' AND '$search_end_createdate' ". 
			") t1 ORDER BY today DESC, type ASC;";
	$vip_level_user_info = $db_analysis->gettotallist($sql);	
	
	$sql = "SELECT today, vip_level_0, vip_level_1, vip_level_2, vip_level_3, vip_level_4, vip_level_5,  vip_level_6, vip_level_7, vip_level_8, vip_level_9, vip_level_10 ".
			"FROM tbl_vip_level_stat_log WHERE TYPE = 0 AND today between '$search_start_createdate' AND '$search_end_createdate' ORDER BY today ASC;";
	$vip_level_user_graph_info = $db_analysis->gettotallist($sql);
	
	$vip_level_user_list = array();
	$vip_level_0_list = array();
	$vip_level_1_list = array();
	$vip_level_2_list = array();
	$vip_level_3_list = array();
	$vip_level_4_list = array();
	$vip_level_5_list = array();
	$vip_level_6_list = array();
	$vip_level_7_list = array();
	$vip_level_8_list = array();
	$vip_level_9_list = array();
	$vip_level_10_list = array();
	
	$vip_level_user_list_pointer = sizeof($vip_level_user_graph_info);
	
	$date_pointer = $search_end_createdate;
	
	$loop_count = get_diff_date($search_end_createdate, $search_start_createdate, "d") + 1;
	
	for ($i=0; $i<$loop_count; $i++)
	{
		if ($vip_level_user_list_pointer > 0)
			$today = $vip_level_user_graph_info[$vip_level_user_list_pointer-1]["today"];
	
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$vip_level_user = $vip_level_user_graph_info[$vip_level_user_list_pointer-1];

			$vip_level_user_list[$i] = $date_pointer;
			$vip_level_0_list[$i] = $vip_level_user["vip_level_0"];
			$vip_level_1_list[$i] = $vip_level_user["vip_level_1"];
			$vip_level_2_list[$i] = $vip_level_user["vip_level_2"];
			$vip_level_3_list[$i] = $vip_level_user["vip_level_3"];
			$vip_level_4_list[$i] = $vip_level_user["vip_level_4"];
			$vip_level_5_list[$i] = $vip_level_user["vip_level_5"];
			$vip_level_6_list[$i] = $vip_level_user["vip_level_6"];
			$vip_level_7_list[$i] = $vip_level_user["vip_level_7"];
			$vip_level_8_list[$i] = $vip_level_user["vip_level_8"];
			$vip_level_9_list[$i] = $vip_level_user["vip_level_9"];
			$vip_level_10_list[$i] = $vip_level_user["vip_level_10"];
			
	
			$vip_level_user_list_pointer--;
		}
		else
		{
			$vip_level_user_list[$i] = $date_pointer;
			$vip_level_0_list[$i] = 0;
			$vip_level_1_list[$i] = 0;
			$vip_level_2_list[$i] = 0;
			$vip_level_3_list[$i] = 0;
			$vip_level_4_list[$i] = 0;
			$vip_level_5_list[$i] = 0;
			$vip_level_6_list[$i] = 0;
			$vip_level_7_list[$i] = 0;
			$vip_level_8_list[$i] = 0;
			$vip_level_9_list[$i] = 0;
			$vip_level_10_list[$i] = 0;
		}
	
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
?>

<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});

	google.load("visualization", "1", {packages:["corechart"]});

	function drawChart() 
	{
		var data0 = new google.visualization.DataTable();
		
        data0.addColumn('string', '날짜');
        data0.addColumn('number', 'Level 0');
        data0.addColumn('number', 'Level 1');
        data0.addColumn('number', 'Level 2');
        data0.addColumn('number', 'Level 3');
        data0.addColumn('number', 'Level 4');
        data0.addColumn('number', 'Level 5');
        data0.addColumn('number', 'Level 6');
        data0.addColumn('number', 'Level 7');
        data0.addColumn('number', 'Level 8');
        data0.addColumn('number', 'Level 9');
        data0.addColumn('number', 'Level 10');        
        data0.addRows([
<?
	   	for ($i=sizeof($vip_level_user_list); $i>0; $i--)
	    {
	    	$_date = $vip_level_user_list[$i-1];
	        $_level_0 = $vip_level_0_list[$i-1];
	        $_level_1 = $vip_level_1_list[$i-1];
	        $_level_2 = $vip_level_2_list[$i-1];
	        $_level_3 = $vip_level_3_list[$i-1];
	        $_level_4 = $vip_level_4_list[$i-1];
	        $_level_5 = $vip_level_5_list[$i-1];
	        $_level_6 = $vip_level_6_list[$i-1];
	        $_level_7 = $vip_level_7_list[$i-1];
	        $_level_8 = $vip_level_8_list[$i-1];
	        $_level_9 = $vip_level_9_list[$i-1];
	        $_level_10 = $vip_level_10_list[$i-1];       
	        
	        echo("['".$_date."',".$_level_0.",".$_level_1.",".$_level_2.",".$_level_3.",".$_level_4.",".$_level_5.",".$_level_6.",".$_level_7.",".$_level_8.",".$_level_9.",".$_level_10."]");
			
	        if ($i > 1)
	            echo(",");
	    }
?>
        ]);


		var options = {                                                   
	            width:1050,                         
	            height:200,
	            axisTitlesPosition:'in',
	            curveType:'none',
	            focusTarget:'category',
	            interpolateNulls:'true',
	            legend:'top',
	            fontSize : 12,
	            chartArea:{left:80,top:40,width:1020,height:130}
	    };
	
		var chart = new google.visualization.LineChart(document.getElementById('chart_data0'));
	    chart.draw(data0, options);

	}

    google.setOnLoadCallback(drawChart);
    
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="user_vip_level_stats.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; VIP 인원 통계</div>
		<div class="search_box">
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">VIP 인원 통계 </span>
	<div id="tab_content_1">
		<table class="tbl_list_basic1"  style="width:1100px">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">				
			</colgroup>
			<thead>
				<tr>
	        		<th>일   자</th>
	        		<th>기   준</th>
	            	<th>VIP 0</th>
	            	<th>VIP 1</th>
	            	<th>VIP 2</th>
	            	<th>VIP 3</th>
	            	<th>VIP 4</th>
	            	<th>VIP 5</th>
	            	<th>VIP 6</th>
	            	<th>VIP 7</th>
	            	<th>VIP 8</th>
	            	<th>VIP 9</th>
	            	<th>VIP 10</th>
	            	<th>합계</th>            	
         		</tr>
			</thead>
			<tbody>
<?		
    for($i=0; $i<sizeof($vip_level_user_info); $i++)
    {
    		$today = $vip_level_user_info[$i]["today"];
    		$type = $vip_level_user_info[$i]["type"];
    		$vip_level_0 = $vip_level_user_info[$i]["vip_level_0"];
    		$vip_level_1 = $vip_level_user_info[$i]["vip_level_1"];
    		$vip_level_2 = $vip_level_user_info[$i]["vip_level_2"];
    		$vip_level_3 = $vip_level_user_info[$i]["vip_level_3"];
    		$vip_level_4 = $vip_level_user_info[$i]["vip_level_4"];
    		$vip_level_5 = $vip_level_user_info[$i]["vip_level_5"];
    		$vip_level_6 = $vip_level_user_info[$i]["vip_level_6"];
    		$vip_level_7 = $vip_level_user_info[$i]["vip_level_7"];
    		$vip_level_8 = $vip_level_user_info[$i]["vip_level_8"];
    		$vip_level_9 = $vip_level_user_info[$i]["vip_level_9"];
    		$vip_level_10 = $vip_level_user_info[$i]["vip_level_10"];

    		$sum_vip_level = $vip_level_0 + $vip_level_1 + $vip_level_2 + $vip_level_3 + $vip_level_4 + $vip_level_5 + $vip_level_6 + $vip_level_7 + $vip_level_8 + $vip_level_9 + $vip_level_10; 
    		
    		if($today != $vip_level_user_info[$i-1]["today"])
    		{
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
					<td class="tdc point" rowspan="2"><?= $today?></td>
<?
    		}
					if($type == 0)
					{
?>
						<td class="tdc point">인원</td>
						<td class="tdc point"><?=number_format($vip_level_0)?></td>
						<td class="tdc point"><?=number_format($vip_level_1)?></td>
						<td class="tdc point"><?=number_format($vip_level_2)?></td>
						<td class="tdc point"><?=number_format($vip_level_3)?></td>
						<td class="tdc point"><?=number_format($vip_level_4)?></td>
						<td class="tdc point"><?=number_format($vip_level_5)?></td>
						<td class="tdc point"><?=number_format($vip_level_6)?></td>
						<td class="tdc point"><?=number_format($vip_level_7)?></td>
						<td class="tdc point"><?=number_format($vip_level_8)?></td>
						<td class="tdc point"><?=number_format($vip_level_9)?></td>
						<td class="tdc point"><?=number_format($vip_level_10)?></td>
						<td class="tdc point"><?=number_format($sum_vip_level)?></td>	
<?
						$sum_vip_level = 0;
					}
					
					if($type == 1)
					{	
?>
						<td class="tdc point">2주 이탈자</td>
						<td class="tdc point"><?=number_format($vip_level_0)?></td>
						<td class="tdc point"><?=number_format($vip_level_1)?></td>
						<td class="tdc point"><?=number_format($vip_level_2)?></td>
						<td class="tdc point"><?=number_format($vip_level_3)?></td>
						<td class="tdc point"><?=number_format($vip_level_4)?></td>
						<td class="tdc point"><?=number_format($vip_level_5)?></td>
						<td class="tdc point"><?=number_format($vip_level_6)?></td>
						<td class="tdc point"><?=number_format($vip_level_7)?></td>
						<td class="tdc point"><?=number_format($vip_level_8)?></td>
						<td class="tdc point"><?=number_format($vip_level_9)?></td>
						<td class="tdc point"><?=number_format($vip_level_10)?></td>
						<td class="tdc point"><?=number_format($sum_vip_level)?></td>						
<?
						$sum_vip_level = 0;
					}
					
			if($today != $vip_level_user_info[$i-1]["today"])
			{
?>
				</tr>
<?
			}
    }
?>
			</tbody>
		</table>
     </div>
      
	  <br><br><br><br>
	 <div id="tab_content_1">
     	<div class="h2_title">[Graph]</div>
	 	<div id="chart_data0" style="height:230px; min-width: 500px"></div>
	 </div>       
     </form>	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_analysis->end();
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>