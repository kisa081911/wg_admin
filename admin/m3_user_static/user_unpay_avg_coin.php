<?
	$top_menu = "user_static";
	$sub_menu = "user_unpay_avg_coin";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$startdate = ($_GET["startdate"] == "") ? date("Y-m-d", strtotime("-3 month")) : $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d", time()-24*60*60) : $_GET["enddate"];
	$search_day = ($_GET["search_day"] == "") ? "1,3,7,14,28,60,90" : $_GET["search_day"];
    $term = "1440";
	$pagename = "user_unpay_avg_coin.php";
	
	$db_analysis = new CDatabase_Analysis();
	
	if ($startdate == "")
		$startdate = "2015-12-21";	

	$sql = "SELECT today, day_after_install, all_avg_coin, avg_coin, ios_avg_coin, and_avg_coin, ama_avg_coin FROM tbl_user_unpay_retention_log WHERE day_after_install IN ($search_day) AND today BETWEEN '$startdate' AND '$enddate';";
	$user_unpay_data = $db_analysis->gettotallist($sql);

	$sql = "SELECT day_after_install FROM tbl_user_unpay_retention_log WHERE day_after_install IN ($search_day) AND today BETWEEN '$startdate' AND '$enddate' GROUP BY day_after_install;";
	$user_unpay_count = $db_analysis->gettotallist($sql);
	
	$db_analysis->end();

?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    
    function drawChart() 
    {
        var data1 = new google.visualization.DataTable();
        var data2 = new google.visualization.DataTable();
        var data3 = new google.visualization.DataTable();
        var data4 = new google.visualization.DataTable();
        var data5 = new google.visualization.DataTable();
        
        data1.addColumn('string', '시간');
<?
		for($i=0; $i < sizeof($user_unpay_count); $i++)
		{
?>
        	data1.addColumn('number', '<?=$user_unpay_count[$i]["day_after_install"]?>');
<?
		}
		
		$start = round(mktime(0, 0, 0, date("m",strtotime($startdate)), date("d", strtotime($startdate)), date("Y", strtotime($startdate)))/($term*60));
		$end = round(mktime(0, 0, 0, date("m",strtotime($enddate)), date("d", strtotime($enddate)), date("Y", strtotime($enddate)))/($term*60)) + 1;
?>
        data2.addColumn('string', '시간');
<?
		for($i=0; $i < sizeof($user_unpay_count); $i++)
		{
?>
        	data2.addColumn('number', '<?=$user_unpay_count[$i]["day_after_install"]?>');
<?
		}
		
?>
        data3.addColumn('string', '시간');
<?
		for($i=0; $i < sizeof($user_unpay_count); $i++)
		{
?>
        	data3.addColumn('number', '<?=$user_unpay_count[$i]["day_after_install"]?>');
<?
		}
		
?>
        data4.addColumn('string', '시간');
<?
		for($i=0; $i < sizeof($user_unpay_count); $i++)
		{
?>
        	data4.addColumn('number', '<?=$user_unpay_count[$i]["day_after_install"]?>');
<?
		}
?>
        data5.addColumn('string', '시간');
<?
		for($i=0; $i < sizeof($user_unpay_count); $i++)
		{
?>
        	data5.addColumn('number', '<?=$user_unpay_count[$i]["day_after_install"]?>');
<?
		}
?>
        
        data1.addRows([
<?
	    for ($i=$start; $i<$end; $i=$i+1)
	    {
	    	$temp_time = explode(":",date("H:i:s", $i*($term*60)));
	    	$temp_minute = $temp_time[0]*60 + $temp_time[1];
	    
	    	$list_date = date("Y-m-d", $i*($term*60));
	    
	    	echo("['$list_date'");
	    
	    	$print = false;
	    	for ($j=0; $j<sizeof($user_unpay_data); $j++)
	    	{
	    		$temp_date = date("Y-m-d", $i*($term*60));
	    
	    		if ($user_unpay_data[$j]["today"] == $temp_date)
	    		{
	    			echo(",{v:".$user_unpay_data[$j]["all_avg_coin"].",f:'".make_price_format($user_unpay_data[$j]["all_avg_coin"])."'}");
	    			$print = true;
	            }
	    	}
	    
	    		if (!$print)
	    		{
	    			for($k=0; $k<sizeof($user_unpay_count); $k++)
	    			{
	    				echo(",{v:0,f:'0'}");
	    			}
	    		}
	    
	    		if ($i+1 >= $end)
	    			echo("]");
	    		else
	    			echo("],");
	    }
?>
		]);
		
        data2.addRows([
<?
	    for ($i=$start; $i<$end; $i=$i+1)
	    {
	    	$temp_time = explode(":",date("H:i:s", $i*($term*60)));
	    	$temp_minute = $temp_time[0]*60 + $temp_time[1];
	    
	    	$list_date = date("Y-m-d", $i*($term*60));
	    
	    	echo("['$list_date'");
	    
	    	$print = false;
	    	for ($j=0; $j<sizeof($user_unpay_data); $j++)
	    	{
	    		$temp_date = date("Y-m-d", $i*($term*60));
	    
	    		if ($user_unpay_data[$j]["today"] == $temp_date)
	    		{
	    			echo(",{v:".$user_unpay_data[$j]["avg_coin"].",f:'".make_price_format($user_unpay_data[$j]["avg_coin"])."'}");
	    			$print = true;
	            }
	    	}
	    
	    		if (!$print)
	    		{
	    			for($k=0; $k<sizeof($user_unpay_count); $k++)
	    			{
	    				echo(",{v:0,f:'0'}");
	    			}
	    		}
	    
	    		if ($i+1 >= $end)
	    			echo("]");
	    		else
	    			echo("],");
	    }
?>
		]);
		
        data3.addRows([
<?
	    for ($i=$start; $i<$end; $i=$i+1)
	    {
	    	$temp_time = explode(":",date("H:i:s", $i*($term*60)));
	    	$temp_minute = $temp_time[0]*60 + $temp_time[1];
	    
	    	$list_date = date("Y-m-d", $i*($term*60));
	    
	    	echo("['$list_date'");
	    
	    	$print = false;
	    	for ($j=0; $j<sizeof($user_unpay_data); $j++)
	    	{
	    		$temp_date = date("Y-m-d", $i*($term*60));
	    
	    		if ($user_unpay_data[$j]["today"] == $temp_date)
	    		{
	    			echo(",{v:".$user_unpay_data[$j]["ios_avg_coin"].",f:'".make_price_format($user_unpay_data[$j]["ios_avg_coin"])."'}");
	    			$print = true;
	            }
	    	}
	    
	    		if (!$print)
	    		{
	    			for($k=0; $k<sizeof($user_unpay_count); $k++)
	    			{
	    				echo(",{v:0,f:'0'}");
	    			}
	    		}
	    
	    		if ($i+1 >= $end)
	    			echo("]");
	    		else
	    			echo("],");
	    }
?>
		]);
		
        data4.addRows([
<?
	    for ($i=$start; $i<$end; $i=$i+1)
	    {
	    	$temp_time = explode(":",date("H:i:s", $i*($term*60)));
	    	$temp_minute = $temp_time[0]*60 + $temp_time[1];
	    
	    	$list_date = date("Y-m-d", $i*($term*60));
	    
	    	echo("['$list_date'");
	    
	    	$print = false;
	    	for ($j=0; $j<sizeof($user_unpay_data); $j++)
	    	{
	    		$temp_date = date("Y-m-d", $i*($term*60));
	    
	    		if ($user_unpay_data[$j]["today"] == $temp_date)
	    		{
	    			echo(",{v:".$user_unpay_data[$j]["and_avg_coin"].",f:'".make_price_format($user_unpay_data[$j]["and_avg_coin"])."'}");
	    			$print = true;
	            }
	    	}
	    
	    		if (!$print)
	    		{
	    			for($k=0; $k<sizeof($user_unpay_count); $k++)
	    			{
	    				echo(",{v:0,f:'0'}");
	    			}
	    		}
	    
	    		if ($i+1 >= $end)
	    			echo("]");
	    		else
	    			echo("],");
	    }
?>
		]);
		
        data5.addRows([
<?
	    for ($i=$start; $i<$end; $i=$i+1)
	    {
	    	$temp_time = explode(":",date("H:i:s", $i*($term*60)));
	    	$temp_minute = $temp_time[0]*60 + $temp_time[1];
	    
	    	$list_date = date("Y-m-d", $i*($term*60));
	    
	    	echo("['$list_date'");
	    
	    	$print = false;
	    	for ($j=0; $j<sizeof($user_unpay_data); $j++)
	    	{
	    		$temp_date = date("Y-m-d", $i*($term*60));
	    
	    		if ($user_unpay_data[$j]["today"] == $temp_date)
	    		{
	    			echo(",{v:".$user_unpay_data[$j]["ama_avg_coin"].",f:'".make_price_format($user_unpay_data[$j]["ama_avg_coin"])."'}");
	    			$print = true;
	            }
	    	}
	    
	    		if (!$print)
	    		{
	    			for($k=0; $k<sizeof($user_unpay_count); $k++)
	    			{
	    				echo(",{v:0,f:'0'}");
	    			}
	    		}
	    
	    		if ($i+1 >= $end)
	    			echo("]");
	    		else
	    			echo("],");
	    }
?>
		]);

        var options = {          
                axisTitlesPosition:'in',  
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize:12,
                chartArea:{left:100,top:40,width:1000,height:130}
		};
                    
		var chart1 = new google.visualization.LineChart(document.getElementById('chart_div1'));
		chart1.draw(data1, options);
		var chart2 = new google.visualization.LineChart(document.getElementById('chart_div2'));
		chart2.draw(data2, options);
		var chart3 = new google.visualization.LineChart(document.getElementById('chart_div3'));
		chart3.draw(data3, options);
		var chart4 = new google.visualization.LineChart(document.getElementById('chart_div4'));
		chart4.draw(data4, options);
		var chart5 = new google.visualization.LineChart(document.getElementById('chart_div5'));
		chart5.draw(data5, options);
                      
    }
            
	google.setOnLoadCallback(drawChart);

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	
	    search_form.submit();
	}
	
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 미결제자 평균 보유 코인(1일~90일)</div>
		<div class="clear"></div>	
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				검색 일 : 
				<input type="text" class="search_text" id="search_day" name="search_day" style="width:350px" value="<?= $search_day ?>"/>
			</div>
			<div class="clear"></div>					
			<div class="search_box">						
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />				
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다
	</div>
	
	<div class="h2_title">[전체]</div>
	<div id="chart_div1" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title">[Web]</div>
	<div id="chart_div2" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title">[iOS]</div>
	<div id="chart_div3" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title">[Android]</div>
	<div id="chart_div4" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title">[Amazon]</div>
	<div id="chart_div5" style="height:230px; min-width: 500px"></div>
	
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>