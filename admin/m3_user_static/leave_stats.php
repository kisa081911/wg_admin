<?
    $top_menu = "user_static";
    $sub_menu = "leave_stats";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    $search_start_orderdate = $_GET["start_orderdate"];
    $search_end_orderdate = $_GET["end_orderdate"];
	$isearch = $_GET["issearch"];
	$category = ($_GET["category"] == "") ? "1":$_GET["category"];
		
	if ($isearch == "")
	{
		$search_end_createdate  = date("Y-m-d", time() - 60 * 60);
		$search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 14);
	}
	
	$now = time();
	
	if ($search_end_createdate != "")
	{
		$now = strtotime($search_end_createdate);
	}
	else
	{
		$search_end_createdate = date('Y-m-d');
	}
	
    $db_livestats = new CDatabase_Livestats();
    
    $orderby="";
    
    if($category == "2")
    {
    	$orderby="order by today desc";
    }
    
    $sql =	" SELECT today, leave_normal, leave_continuous_play, leave_first_order, leave_continuous_order, leave_vip, return_normal, return_continuous_play, return_first_order, return_continuous_order, return_vip ".
			" FROM tbl_user_leave_daily ".
			" WHERE '$search_start_createdate' <= today ".
			" AND today <= '$search_end_createdate' ". $orderby;
    
    $loyal_leave_list = $db_livestats->gettotallist($sql);
    
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
	<?
			if ($category == "1")
			{
				$leave_chaet_string="";
				$return_chaet_string="";
				
				foreach ($loyal_leave_list as $loyal_leave_listobj ){
					$today=$loyal_leave_listobj["today"];
					 
					$leave_normal=$loyal_leave_listobj["leave_normal"];
					$leave_continus_play=$loyal_leave_listobj["leave_continuous_play"];
					$leave_first_order=$loyal_leave_listobj["leave_first_order"];
					$leave_continus_order=$loyal_leave_listobj["leave_continuous_order"];
					$leave_vip=$loyal_leave_listobj["leave_vip"];
				
					$return_normal=$loyal_leave_listobj["return_normal"];
					$return_continus_play=$loyal_leave_listobj["return_continuous_play"];
					$return_first_order=$loyal_leave_listobj["return_first_order"];
					$return_continus_order=$loyal_leave_listobj["return_continuous_order"];
					$return_vip=$loyal_leave_listobj["return_vip"];
					 
				
					 
					$leave_chaet_string .= "['".$today."'";
					$leave_chaet_string .=	",{v:".$leave_normal.",f:'".number_format($leave_normal)."'}".
											",{v:".$leave_continus_play.",f:'".number_format($leave_continus_play)."'}".
											",{v:".$leave_first_order.",f:'".number_format($leave_first_order)."'}".
											",{v:".$leave_continus_order.",f:'".number_format($leave_continus_order)."'}".
											",{v:".$leave_vip.",f:'".number_format($leave_vip)."'}],";
				
					$return_chaet_string .="['".$today."'";
					$return_chaet_string .=	",{v:".$return_normal.",f:'".number_format($return_normal)."'}".
											",{v:".$return_continus_play.",f:'".number_format($return_continus_play)."'}".
											",{v:".$return_first_order.",f:'".number_format($return_first_order)."'}".
											",{v:".$return_continus_order.",f:'".number_format($return_continus_order)."'}".
											",{v:".$return_vip.",f:'".number_format($return_vip)."'}],";
				}
		?>
			google.load("visualization", "1", {packages:["corechart"]});

			 function drawChart() 
			    {
			        var data1 = new google.visualization.DataTable();
			        data1.addColumn('string', '날짜');
			        data1.addColumn('number', '일반');
			        data1.addColumn('number', '지속적 플레이(7일이상 로그인)');
			        data1.addColumn('number', '결제(5회 미만)');
			        data1.addColumn('number', '지속적 결제(5번이상 결제)');
			        data1.addColumn('number', 'VIP유저($99이상 결제)');
			        data1.addRows([<?=$leave_chaet_string?>]);
			        
			        var data2 = new google.visualization.DataTable();
			        data2.addColumn('string', '날짜');
			        data2.addColumn('number', '일반');
			        data2.addColumn('number', '지속적 플레이(7일이상 로그인)');
			        data2.addColumn('number', '결제(5회 미만)');
			        data2.addColumn('number', '지속적 결제(5번이상 결제)');
			        data2.addColumn('number', 'VIP유저($99이상 결제)');
					data2.addRows([<?=$return_chaet_string?>]);
			        
			        var options1 = {
			                width:1050,                       
			                height:480,
			                axisTitlesPosition:'in',
			                curveType:'none',
			                focusTarget:'category',
			                interpolateNulls:'true',
			                legend:'top',
			                fontSize : 12,
			                chartArea:{left:60,top:40,width:1040,height:300}
			            };
			            
			        var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
			        chart.draw(data1, options1);        
			        
			        chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
			        chart.draw(data2, options1);        
			    }
			        
				google.setOnLoadCallback(drawChart);	
		<?
			}
		?>

	function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;

        search_form.submit();
    }
	
	function tab_change(tab)
	{
		window.location.href = "leave_stats.php?category="+tab + "&start_createdate=<?= $search_start_createdate ?>&end_createdate=<?= $search_end_createdate ?>&payday=<?= $search_payday ?>&issearch=1";
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	 <form name="search_form" id="search_form"  method="get" action="leave_stats.php">
		<input type=hidden name=issearch value="1"/>
		<input type=hidden name=category value="<?= $category ?>"/>
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt;기존유저 이탈</div>
			<div class="search_box">
				<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
			
			<div class="clear"></div>
		</div>
		<!-- //title_warp -->
            
		<div class="search_result">
			<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
		</div>
            
		<ul class="tab">
			<li id="tab_1" class="<?= ($category == "1") ? "select" : "" ?>" onclick="tab_change('1')">그래프</li>
			<li id="tab_2" class="<?= ($category == "2") ? "select" : "" ?>" onclick="tab_change('2')">상세보기</li>
		</ul>
	 </form>
	 
<?
	if ($category == "1")
	{
?>
		<br/><br/>
		<div class="h2_title">[2주 이탈]</div>
		<div id="chart_div1" style="height:480px; min-width: 500px"></div>
		<br/>
		<div class="h2_title">[이탈 복귀]</div>
		<div id="chart_div2" style="height:480px; min-width: 500px"></div>
<?

	}
	else if ($category == "2")
	{
?>
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="200">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width=""> 
			</colgroup>
			<thead>
	            <tr>
	                <th rowspan="2">날짜</th>
	                <th class="tdc" colspan="5">2주 이탈</th>
	                <!-- <th class="tdc" colspan="5">이탈복귀</th> -->
	            </tr>
	            <tr>
	                <th class="tdc">일반</th>
	                <th class="tdc">지속적플레이</th>
	                <th class="tdc">결제(5회 미만)</th>
	                <th class="tdc">지속적 결제</th>
	                <th class="tdc">vip유저</th>
	                <th class="tdc">일반</th>
	                <th class="tdc">지속적플레이</th>
	                <th class="tdc">첫결제</th>
	                <th class="tdc">지속적 결제</th>
	                <th class="tdc">vip유저</th>
	            </tr>
			</thead>
			<tbody>
<?
		foreach ($loyal_leave_list as $loyal_leave_listobj)
		{
			$today = $loyal_leave_listobj["today"];
			$leave_normal = $loyal_leave_listobj["leave_normal"];
			$leave_continus_play = $loyal_leave_listobj["leave_continuous_play"];
			$leave_first_order = $loyal_leave_listobj["leave_first_order"];
			$leave_continus_order = $loyal_leave_listobj["leave_continuous_order"];
			$leave_vip = $loyal_leave_listobj["leave_vip"];
			$return_normal = $loyal_leave_listobj["return_normal"];
			$return_continus_play = $loyal_leave_listobj["return_continuous_play"];
			$return_first_order = $loyal_leave_listobj["return_first_order"];
			$return_continus_order = $loyal_leave_listobj["return_continuous_order"];
			$return_vip = $loyal_leave_listobj["return_vip"];
				
			$total_leave_normal +=$leave_normal;
			$total_leave_continus_play +=$leave_continus_play;
			$total_leave_first_order +=$leave_first_order;
			$total_leave_continus_order +=$leave_continus_order;
			$total_leave_vip +=$leave_vip;
			$total_return_normal +=$return_normal;
			$total_return_continus_play +=$return_continus_play;
			$total_return_first_order+=$return_first_order;
			$total_return_continus_order+=$return_continus_order;
			$total_return_vip+=$return_vip;
			
?>			
	        	<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
					<td class="tdc point_title"><?=$today?></td>
					<td class="tdc point"><?=number_format($leave_normal)?></td>
					<td class="tdc point"><?=number_format($leave_continus_play)?></td>
					<td class="tdc point"><?=number_format($leave_first_order)?></td>
					<td class="tdc point"><?=number_format($leave_continus_order)?></td>
					<td class="tdc point"><?=number_format($leave_vip)?></td>
					<td class="tdr point"><?=number_format($return_normal)?></td>
					<td class="tdr point"><?=number_format($return_continus_play)?></td>
					<td class="tdr point"><?=number_format($return_first_order)?></td>
					<td class="tdr point"><?=number_format($return_continus_order)?></td>
					<td class="tdr point"><?=number_format($return_vip)?></td>
				</tr>
<?
		}
		if(sizeof($loyal_leave_list) == 0)
		{
?>
		   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
		   			<td class="tdc" colspan="11">검색 결과가 없습니다.</td>
		   		</tr>
<?
		}
		else 
		{
?>		
			<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
					<td class="tdc point_title">합계</td>
					<td class="tdc point"><?=number_format($total_leave_normal)?></td>
					<td class="tdc point"><?=number_format($total_leave_continus_play)?></td>
					<td class="tdc point"><?=number_format($total_leave_first_order)?></td>
					<td class="tdc point"><?=number_format($total_leave_continus_order)?></td>
					<td class="tdc point"><?=number_format($total_leave_vip)?></td>
					<td class="tdr point"><?=number_format($total_return_normal)?></td>
					<td class="tdr point"><?=number_format($total_return_continus_play)?></td>
					<td class="tdr point"><?=number_format($total_return_first_order)?></td>
					<td class="tdr point"><?=number_format($total_return_continus_order)?></td>
					<td class="tdr point"><?=number_format($total_return_vip)?></td>
				</tr>
<?
		}
?>						
			</tbody>
		</table>
	</div>
<?
	}
?>
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_livestats->end();

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>