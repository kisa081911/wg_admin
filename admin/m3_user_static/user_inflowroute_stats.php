<?
	$top_menu = "user_static";
	$sub_menu = "user_maintain_new";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$db_analysis = new CDatabase_Analysis();
	
	if($_GET["start_date"] == "")
	{
		$sql = "SELECT MAX(today) FROM tbl_user_inflowroute_daily";
		$start_date = $db_analysis->getvalue($sql);
		
		$search_date = substr($start_date, 0, 4)."-".substr($start_date, 4, 2)."-".substr($start_date, 6, 2);
	}
	else
	{
		$search_date = $_GET["start_date"];
		$start_date = str_replace("-","",$search_date);
	}
	
	$sql = "SELECT * FROM tbl_user_inflowroute_daily WHERE today = '$start_date' AND type = 0 ORDER BY today DESC, regmonth ASC";
	$web_list = $db_analysis->gettotallist($sql);
	
 	$sql = "SELECT today, COUNT(regmonth) AS regmonth, SUM(joincount) AS joincount, SUM(activecount) AS activecount, SUM(webcredit) AS webcredit, ". 
       		"SUM(mobilecredit) AS mobilecredit, SUM(webcredit2week) AS webcredit2week, SUM(mobilecredit2week) AS mobilecredit2week ".
			"FROM tbl_user_inflowroute_daily ".
			"WHERE today = '$start_date' AND type = 0 ".
			"GROUP BY today ORDER BY today DESC, regmonth ASC";
 	$web_sumlist = $db_analysis->gettotallist($sql);
 	
 	$sql = "SELECT * FROM tbl_user_inflowroute_daily WHERE today = '$start_date' AND type = 1 ORDER BY today DESC, regmonth ASC";
 	$ios_list = $db_analysis->gettotallist($sql);
 	
 	$sql = "SELECT today, COUNT(regmonth) AS regmonth, SUM(joincount) AS joincount, SUM(activecount) AS activecount, SUM(webcredit) AS webcredit, ".
 			"SUM(mobilecredit) AS mobilecredit, SUM(webcredit2week) AS webcredit2week, SUM(mobilecredit2week) AS mobilecredit2week ".
 			"FROM tbl_user_inflowroute_daily ".
 			"WHERE today = '$start_date' AND type = 1 ".
 			"GROUP BY today ORDER BY today DESC, regmonth ASC";
 	$ios_sumlist = $db_analysis->gettotallist($sql);
 	
 	$sql = "SELECT * FROM tbl_user_inflowroute_daily WHERE today = '$start_date' AND type = 2 ORDER BY today DESC, regmonth ASC";
 	$android_list = $db_analysis->gettotallist($sql);
 	
 	$sql = "SELECT today, COUNT(regmonth) AS regmonth, SUM(joincount) AS joincount, SUM(activecount) AS activecount, SUM(webcredit) AS webcredit, ".
 			"SUM(mobilecredit) AS mobilecredit, SUM(webcredit2week) AS webcredit2week, SUM(mobilecredit2week) AS mobilecredit2week ".
 			"FROM tbl_user_inflowroute_daily ".
 			"WHERE today = '$start_date' AND type = 2 ".
 			"GROUP BY today ORDER BY today DESC, regmonth ASC";
 	$android_sumlist = $db_analysis->gettotallist($sql);

 	$sql = "SELECT * FROM tbl_user_inflowroute_daily WHERE today = '$start_date' AND type = 3 ORDER BY today DESC, regmonth ASC";
 	$amazon_list = $db_analysis->gettotallist($sql);
 	
 	$sql = "SELECT today, COUNT(regmonth) AS regmonth, SUM(joincount) AS joincount, SUM(activecount) AS activecount, SUM(webcredit) AS webcredit, ".
 			"SUM(mobilecredit) AS mobilecredit, SUM(webcredit2week) AS webcredit2week, SUM(mobilecredit2week) AS mobilecredit2week ".
 			"FROM tbl_user_inflowroute_daily ".
 			"WHERE today = '$start_date' AND type = 3 ".
 			"GROUP BY today ORDER BY today DESC, regmonth ASC";
 	$amazon_sumlist = $db_analysis->gettotallist($sql);
 	
 	$cost_list = $db_analysis->gettotallist("SELECT regmonth,cost,os_type FROM tbl_marketing_month_cost");
 	
 	$month_cost_web = array();
 	$month_cost_ios = array();
 	$month_cost_android = array(); 	
 	$month_cost_amazon = array();
 	
 	for($i=0;$i<sizeof($cost_list);$i++)
 	{
 	    $regmonth = $cost_list[$i]["regmonth"];
 	    $os_type = $cost_list[$i]["os_type"];
 	    $cost = $cost_list[$i]["cost"];
 	    
 	    if($os_type == 0)
 	    {
 	        $month_cost_web[$regmonth] = $cost;
 	    }
 	    else if($os_type == 1)
 	    {
 	        $month_cost_ios[$regmonth] = $cost;
 	    }
 	    else if($os_type == 2)
 	    {
 	        $month_cost_android[$regmonth] = $cost;
 	    }
 	    else if($os_type == 3)
 	    {
 	        $month_cost_amazon[$regmonth] = $cost;
 	    }
 	}
 	
 	$db_analysis->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
    	$("#start_date").datepicker({ });
	});

	function search()
    {
        var search_form = document.search_form;
            
        if (search_form.start_date.value == "")
        {
            alert("기준일을 입력하세요.");
            search_form.start_date.focus();
            return;
        } 

        search_form.submit();
    }
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 사용자 유지현황(전체) </div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<span class="search_lbl">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="start_date" name="start_date" style="width:65px" readonly="readonly" value="<?= $search_date ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/>
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->

	<div class="search_result">
		<span><?= $search_date ?></span> 통계입니다
	</div>
	
	<div class="search_result">Web</div>
	
	<table class="tbl_list_basic1" style="width:1300px">
		<colgroup>
			<col width="50">
			<col width="70">
			<col width="70">
            <col width="80">
            <col width="70">
            <col width="70">
            <col width="100">
            <col width="100">
            <col width="80">
            <col width="100">
            <col width="70">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="70">
            <col width="70">
            <col width="70">   
		</colgroup>
        <thead>
        	<tr>
        		<th rowspan="2">일자</th>
            	<th colspan="5">일반현황</th>
            	<th colspan="5">누적</th>
            	<th colspan="5">최근 2주</th>
            	<th colspan="2"></th>
         	</tr>
         	<tr>
         		<th>가입월</th>
             	<th>가입자수</th>
               	<th>2주유지수</th>
               	<th>유지비율</th>
               	<th>이탈율</th>
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>비용</th>
               	<th>ROI</th>
         	</tr>
        </thead>
        <tbody>
<?
	$rowcnt = 0;
	$j = 1;
	$summonth_cost = 0;
		
    for($i=0; $i<sizeof($web_list); $i++)
    {
    	$today = $web_list[$i]["today"];
    	$regmonth = $web_list[$i]["regmonth"];
    	$joincount = $web_list[$i]["joincount"];
    	$activecount = $web_list[$i]["activecount"];
    	$activerate = ($joincount == 0) ? 0 : round(($activecount / $joincount) * 100, 2);
    	$webcredit = $web_list[$i]["webcredit"];
    	$mobilecredit = $web_list[$i]["mobilecredit"];
    	$totalcredit = $webcredit + $mobilecredit;
    	$mobilerate = ($totalcredit == 0) ? 0 : round(($mobilecredit / $totalcredit) * 100, 2);
    	$avgcredit = ($joincount == 0) ? 0 : round(($totalcredit / $joincount), 2);
    	$webcredit2week = $web_list[$i]["webcredit2week"];
    	$mobilecredit2week = $web_list[$i]["mobilecredit2week"];
    	$totalcredit2week = $webcredit2week + $mobilecredit2week;
    	$mobile2weekrate = ($totalcredit2week == 0) ? 0 : round(($mobilecredit2week / $totalcredit2week) * 100, 2);
    	$avgcredit2week = ($activecount == 0) ? 0 : round(($totalcredit2week / $activecount), 2);
    	$month_cost = $month_cost_web[$regmonth];
    	$summonth_cost += $month_cost;
    	$roi = ($month_cost == 0) ? 0 : round($totalcredit / $month_cost * 100, 2);
    	
     	$monthcnt = $web_sumlist[$rowcnt]["regmonth"];
     	$sumjoincount = $web_sumlist[$rowcnt]["joincount"];
     	$sumactivecount = $web_sumlist[$rowcnt]["activecount"];
     	$sumactiverate = ($sumjoincount == 0) ? 0 : round(($sumactivecount / $sumjoincount) * 100, 2);
     	$sumwebcredit = $web_sumlist[$rowcnt]["webcredit"];
     	$summobilecredit = $web_sumlist[$rowcnt]["mobilecredit"];
     	$sumtotalcredit = $sumwebcredit + $summobilecredit;
     	$summobilerate = ($sumtotalcredit == 0) ? 0 : round(($summobilecredit / $sumtotalcredit) * 100, 2);
     	$sumavgcredit = ($sumjoincount == 0) ? 0 : round(($sumtotalcredit / $sumjoincount), 2);
     	$sumwebcredit2week = $web_sumlist[$rowcnt]["webcredit2week"];
     	$summobilecredit2week = $web_sumlist[$rowcnt]["mobilecredit2week"];
     	$sumtotalcredit2week = $sumwebcredit2week + $summobilecredit2week;
     	$summobile2weekrate = ($sumtotalcredit2week == 0) ? 0 : round(($summobilecredit2week / $sumtotalcredit2week) * 100, 2);
     	$sumavgcredit2week = ($sumactivecount == 0) ? 0 : round(($sumtotalcredit2week / $sumactivecount), 2);
     	$sumroi = ($summonth_cost == 0) ? 0 : round($sumtotalcredit / $summonth_cost * 100, 2);
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
			if($j == 1)
			{
?>
				<td class="tdr point" rowspan="<?= $monthcnt?>"><?= $today?></td>
<?
			}
?>
            <td class="tdr"><?= $regmonth ?></td>
            <td class="tdr"><?= number_format($joincount) ?></td>
            <td class="tdr"><?= number_format($activecount) ?></td>
            <td class="tdr"><?= $activerate ?>%</td>
            <td class="tdr"><?= round(100 - $activerate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($webcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($mobilecredit, 2) ?></td>
            <td class="tdr"><?= number_format($mobilerate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($totalcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($avgcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($webcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($mobilecredit2week, 2) ?></td>
            <td class="tdr"><?= number_format($mobile2weekrate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($totalcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($avgcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($month_cost) ?></td>
            <td class="tdr"><?= number_format($roi, 2) ?></td>
        </tr>
<?
		if($j == $monthcnt)
		{
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point_title" colspan="2">합계</td>
            	<td class="tdr point"><?= number_format($sumjoincount) ?></td>
            	<td class="tdr point"><?= number_format($sumactivecount) ?></td>
            	<td class="tdr point"><?= $sumactiverate ?>%</td>
            	<td class="tdr point"><?= round(100 - $sumactiverate, 2) ?>%</td>
            	<td class="tdr point">$<?= number_format($sumwebcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobilerate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumwebcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit2week, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobile2weekrate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summonth_cost) ?></td>
	            <td class="tdr point"><?= number_format($sumroi, 2) ?></td>
			</tr>
<?
			$j = 1;
			$rowcnt++;
    	}
    	else
    	{
    		$j++;	
    	}
    }
    
    if(sizeof($web_list) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="18">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>
	
	<br/><br/>
		
	<div class="search_result">IOS</div>
	
	<table class="tbl_list_basic1" style="width:1300px">
		<colgroup>
			<col width="50">
			<col width="70">
			<col width="70">
            <col width="80">
            <col width="70">
            <col width="70">
            <col width="100">
            <col width="100">
            <col width="80">
            <col width="100">
            <col width="70">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="70">
            <col width="70">
            <col width="70">   
		</colgroup>
        <thead>
        	<tr>
        		<th rowspan="2">일자</th>
            	<th colspan="5">일반현황</th>
            	<th colspan="5">누적</th>
            	<th colspan="5">최근 2주</th>
            	<th colspan="2"></th>
         	</tr>
         	<tr>
         		<th>가입월</th>
             	<th>가입자수</th>
               	<th>2주유지수</th>
               	<th>유지비율</th>
               	<th>이탈율</th>
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>비용</th>
               	<th>ROI</th>
         	</tr>
        </thead>
        <tbody>
<?
	$rowcnt = 0;
	$j = 1;
	$summonth_cost = 0;
		
    for($i=0; $i<sizeof($ios_list); $i++)
    {
    	$today = $ios_list[$i]["today"];
    	$regmonth = $ios_list[$i]["regmonth"];
    	$joincount = $ios_list[$i]["joincount"];
    	$activecount = $ios_list[$i]["activecount"];
    	$activerate = ($joincount == 0) ? 0 : round(($activecount / $joincount) * 100, 2);
    	$webcredit = $ios_list[$i]["webcredit"];
    	$mobilecredit = $ios_list[$i]["mobilecredit"];
    	$totalcredit = $webcredit + $mobilecredit;
    	$mobilerate = ($totalcredit == 0) ? 0 : round(($mobilecredit / $totalcredit) * 100, 2);
    	$avgcredit = ($joincount == 0) ? 0 : round(($totalcredit / $joincount), 2);
    	$webcredit2week = $ios_list[$i]["webcredit2week"];
    	$mobilecredit2week = $ios_list[$i]["mobilecredit2week"];
    	$totalcredit2week = $webcredit2week + $mobilecredit2week;
    	$mobile2weekrate = ($totalcredit2week == 0) ? 0 : round(($mobilecredit2week / $totalcredit2week) * 100, 2);
    	$avgcredit2week = ($activecount == 0) ? 0 : round(($totalcredit2week / $activecount), 2);
    	$month_cost = $month_cost_ios[$regmonth];
    	$summonth_cost += $month_cost;
    	$roi = ($month_cost == 0) ? 0 : round($totalcredit / $month_cost * 100, 2);
    	
     	$monthcnt = $ios_sumlist[$rowcnt]["regmonth"];
     	$sumjoincount = $ios_sumlist[$rowcnt]["joincount"];
     	$sumactivecount = $ios_sumlist[$rowcnt]["activecount"];
     	$sumactiverate = ($sumjoincount == 0) ? 0 : round(($sumactivecount / $sumjoincount) * 100, 2);
     	$sumwebcredit = $ios_sumlist[$rowcnt]["webcredit"];
     	$summobilecredit = $ios_sumlist[$rowcnt]["mobilecredit"];
     	$sumtotalcredit = $sumwebcredit + $summobilecredit;
     	$summobilerate = ($sumtotalcredit == 0) ? 0 : round(($summobilecredit / $sumtotalcredit) * 100, 2);
     	$sumavgcredit = ($sumjoincount == 0) ? 0 : round(($sumtotalcredit / $sumjoincount), 2);
     	$sumwebcredit2week = $ios_sumlist[$rowcnt]["webcredit2week"];
     	$summobilecredit2week = $ios_sumlist[$rowcnt]["mobilecredit2week"];
     	$sumtotalcredit2week = $sumwebcredit2week + $summobilecredit2week;
     	$summobile2weekrate = ($sumtotalcredit2week == 0) ? 0 : round(($summobilecredit2week / $sumtotalcredit2week) * 100, 2);
     	$sumavgcredit2week = ($sumactivecount == 0) ? 0 : round(($sumtotalcredit2week / $sumactivecount), 2);
     	$sumroi = ($summonth_cost == 0) ? 0 : round($sumtotalcredit / $summonth_cost * 100, 2);
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
			if($j == 1)
			{
?>
				<td class="tdr point" rowspan="<?= $monthcnt?>"><?= $today?></td>
<?
			}
?>
            <td class="tdr"><?= $regmonth ?></td>
            <td class="tdr"><?= number_format($joincount) ?></td>
            <td class="tdr"><?= number_format($activecount) ?></td>
            <td class="tdr"><?= $activerate ?>%</td>
            <td class="tdr"><?= round(100 - $activerate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($webcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($mobilecredit, 2) ?></td>
            <td class="tdr"><?= number_format($mobilerate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($totalcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($avgcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($webcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($mobilecredit2week, 2) ?></td>
            <td class="tdr"><?= number_format($mobile2weekrate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($totalcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($avgcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($month_cost) ?></td>
            <td class="tdr"><?= number_format($roi, 2) ?></td>
        </tr>
<?
		if($j == $monthcnt)
		{
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point_title" colspan="2">합계</td>
            	<td class="tdr point"><?= number_format($sumjoincount) ?></td>
            	<td class="tdr point"><?= number_format($sumactivecount) ?></td>
            	<td class="tdr point"><?= $sumactiverate ?>%</td>
            	<td class="tdr point"><?= round(100 - $sumactiverate, 2) ?>%</td>
            	<td class="tdr point">$<?= number_format($sumwebcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobilerate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumwebcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit2week, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobile2weekrate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summonth_cost) ?></td>
            	<td class="tdr point"><?= number_format($sumroi, 2) ?></td>
			</tr>
<?
			$j = 1;
			$rowcnt++;
    	}
    	else
    	{
    		$j++;	
    	}
    }
    
    if(sizeof($ios_list) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="18">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>
	
	<br/><br/>
		
	<div class="search_result">Android</div>
	
	<table class="tbl_list_basic1" style="width:1300px">
		<colgroup>
			<col width="50">
			<col width="70">
			<col width="70">
            <col width="80">
            <col width="70">
            <col width="70">
            <col width="100">
            <col width="100">
            <col width="80">
            <col width="100">
            <col width="70">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="70">
            <col width="70">
            <col width="70">   
		</colgroup>
        <thead>
        	<tr>
        		<th rowspan="2">일자</th>
            	<th colspan="5">일반현황</th>
            	<th colspan="5">누적</th>
            	<th colspan="5">최근 2주</th>
            	<th colspan="2"></th>
         	</tr>
         	<tr>
         		<th>가입월</th>
             	<th>가입자수</th>
               	<th>2주유지수</th>
               	<th>유지비율</th>
               	<th>이탈율</th>
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>비용</th>
               	<th>ROI</th>
         	</tr>
        </thead>
        <tbody>
<?
	$rowcnt = 0;
	$j = 1;
	$summonth_cost = 0;
		
    for($i=0; $i<sizeof($android_list); $i++)
    {
    	$today = $android_list[$i]["today"];
    	$regmonth = $android_list[$i]["regmonth"];
    	$joincount = $android_list[$i]["joincount"];
    	$activecount = $android_list[$i]["activecount"];
    	$activerate = ($joincount == 0) ? 0 : round(($activecount / $joincount) * 100, 2);
    	$webcredit = $android_list[$i]["webcredit"];
    	$mobilecredit = $android_list[$i]["mobilecredit"];
    	$totalcredit = $webcredit + $mobilecredit;
    	$mobilerate = ($totalcredit == 0) ? 0 : round(($mobilecredit / $totalcredit) * 100, 2);
    	$avgcredit = ($joincount == 0) ? 0 : round(($totalcredit / $joincount), 2);
    	$webcredit2week = $android_list[$i]["webcredit2week"];
    	$mobilecredit2week = $android_list[$i]["mobilecredit2week"];
    	$totalcredit2week = $webcredit2week + $mobilecredit2week;
    	$mobile2weekrate = ($totalcredit2week == 0) ? 0 : round(($mobilecredit2week / $totalcredit2week) * 100, 2);
    	$avgcredit2week = ($activecount == 0) ? 0 : round(($totalcredit2week / $activecount), 2);
    	$month_cost = $month_cost_android[$regmonth];
    	$summonth_cost += $month_cost;
    	$roi = ($month_cost == 0) ? 0 : round($totalcredit / $month_cost * 100, 2);
    	
     	$monthcnt = $android_sumlist[$rowcnt]["regmonth"];
     	$sumjoincount = $android_sumlist[$rowcnt]["joincount"];
     	$sumactivecount = $android_sumlist[$rowcnt]["activecount"];
     	$sumactiverate = ($sumjoincount == 0) ? 0 : round(($sumactivecount / $sumjoincount) * 100, 2);
     	$sumwebcredit = $android_sumlist[$rowcnt]["webcredit"];
     	$summobilecredit = $android_sumlist[$rowcnt]["mobilecredit"];
     	$sumtotalcredit = $sumwebcredit + $summobilecredit;
     	$summobilerate = ($sumtotalcredit == 0) ? 0 : round(($summobilecredit / $sumtotalcredit) * 100, 2);
     	$sumavgcredit = ($sumjoincount == 0) ? 0 : round(($sumtotalcredit / $sumjoincount), 2);
     	$sumwebcredit2week = $android_sumlist[$rowcnt]["webcredit2week"];
     	$summobilecredit2week = $android_sumlist[$rowcnt]["mobilecredit2week"];
     	$sumtotalcredit2week = $sumwebcredit2week + $summobilecredit2week;
     	$summobile2weekrate = ($sumtotalcredit2week == 0) ? 0 : round(($summobilecredit2week / $sumtotalcredit2week) * 100, 2);
     	$sumavgcredit2week = ($sumactivecount == 0) ? 0 : round(($sumtotalcredit2week / $sumactivecount), 2);
     	$sumroi = ($summonth_cost == 0) ? 0 : round($sumtotalcredit / $summonth_cost * 100, 2);
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
			if($j == 1)
			{
?>
				<td class="tdr point" rowspan="<?= $monthcnt?>"><?= $today?></td>
<?
			}
?>
            <td class="tdr"><?= $regmonth ?></td>
            <td class="tdr"><?= number_format($joincount) ?></td>
            <td class="tdr"><?= number_format($activecount) ?></td>
            <td class="tdr"><?= $activerate ?>%</td>
            <td class="tdr"><?= round(100 - $activerate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($webcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($mobilecredit, 2) ?></td>
            <td class="tdr"><?= number_format($mobilerate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($totalcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($avgcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($webcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($mobilecredit2week, 2) ?></td>
            <td class="tdr"><?= number_format($mobile2weekrate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($totalcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($avgcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($month_cost) ?></td>
            <td class="tdr"><?= number_format($roi, 2) ?></td>
        </tr>
<?
		if($j == $monthcnt)
		{
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point_title" colspan="2">합계</td>
            	<td class="tdr point"><?= number_format($sumjoincount) ?></td>
            	<td class="tdr point"><?= number_format($sumactivecount) ?></td>
            	<td class="tdr point"><?= $sumactiverate ?>%</td>
            	<td class="tdr point"><?= round(100 - $sumactiverate, 2) ?>%</td>
            	<td class="tdr point">$<?= number_format($sumwebcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobilerate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumwebcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit2week, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobile2weekrate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summonth_cost) ?></td>
	            <td class="tdr point"><?= number_format($sumroi, 2) ?></td>
			</tr>
<?
			$j = 1;
			$rowcnt++;
    	}
    	else
    	{
    		$j++;	
    	}
    }
    
    if(sizeof($android_list) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="18">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>
	
	<br/><br/>
		
	<div class="search_result">Amazon</div>
	
	<table class="tbl_list_basic1" style="width:1300px">
		<colgroup>
			<col width="50">
			<col width="70">
			<col width="70">
            <col width="80">
            <col width="70">
            <col width="70">
            <col width="100">
            <col width="100">
            <col width="80">
            <col width="100">
            <col width="70">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="70">
            <col width="70">
            <col width="70">   
		</colgroup>
        <thead>
        	<tr>
        		<th rowspan="2">일자</th>
            	<th colspan="5">일반현황</th>
            	<th colspan="5">누적</th>
            	<th colspan="5">최근 2주</th>
            	<th colspan="2"></th>
         	</tr>
         	<tr>
         		<th>가입월</th>
             	<th>가입자수</th>
               	<th>2주유지수</th>
               	<th>유지비율</th>
               	<th>이탈율</th>
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>비용</th>
               	<th>ROI</th>
         	</tr>
        </thead>
        <tbody>
<?
	$rowcnt = 0;
	$j = 1;
	$summonth_cost = 0;
		
    for($i=0; $i<sizeof($amazon_list); $i++)
    {
    	$today = $amazon_list[$i]["today"];
    	$regmonth = $amazon_list[$i]["regmonth"];
    	$joincount = $amazon_list[$i]["joincount"];
    	$activecount = $amazon_list[$i]["activecount"];
    	$activerate = ($joincount == 0) ? 0 : round(($activecount / $joincount) * 100, 2);
    	$webcredit = $amazon_list[$i]["webcredit"];
    	$mobilecredit = $amazon_list[$i]["mobilecredit"];
    	$totalcredit = $webcredit + $mobilecredit;
    	$mobilerate = ($totalcredit == 0) ? 0 : round(($mobilecredit / $totalcredit) * 100, 2);
    	$avgcredit = ($joincount == 0) ? 0 : round(($totalcredit / $joincount), 2);
    	$webcredit2week = $amazon_list[$i]["webcredit2week"];
    	$mobilecredit2week = $amazon_list[$i]["mobilecredit2week"];
    	$totalcredit2week = $webcredit2week + $mobilecredit2week;
    	$mobile2weekrate = ($totalcredit2week == 0) ? 0 : round(($mobilecredit2week / $totalcredit2week) * 100, 2);
    	$avgcredit2week = ($activecount == 0) ? 0 : round(($totalcredit2week / $activecount), 2);
    	$month_cost = $month_cost_amazon[$regmonth];
    	$summonth_cost += $month_cost;
    	$roi = ($month_cost == 0) ? 0 : round($totalcredit / $month_cost * 100, 2);
    	
     	$monthcnt = $amazon_sumlist[$rowcnt]["regmonth"];
     	$sumjoincount = $amazon_sumlist[$rowcnt]["joincount"];
     	$sumactivecount = $amazon_sumlist[$rowcnt]["activecount"];
     	$sumactiverate = ($sumjoincount == 0) ? 0 : round(($sumactivecount / $sumjoincount) * 100, 2);
     	$sumwebcredit = $amazon_sumlist[$rowcnt]["webcredit"];
     	$summobilecredit = $amazon_sumlist[$rowcnt]["mobilecredit"];
     	$sumtotalcredit = $sumwebcredit + $summobilecredit;
     	$summobilerate = ($sumtotalcredit == 0) ? 0 : round(($summobilecredit / $sumtotalcredit) * 100, 2);
     	$sumavgcredit = ($sumjoincount == 0) ? 0 : round(($sumtotalcredit / $sumjoincount), 2);
     	$sumwebcredit2week = $amazon_sumlist[$rowcnt]["webcredit2week"];
     	$summobilecredit2week = $amazon_sumlist[$rowcnt]["mobilecredit2week"];
     	$sumtotalcredit2week = $sumwebcredit2week + $summobilecredit2week;
     	$summobile2weekrate = ($sumtotalcredit2week == 0) ? 0 : round(($summobilecredit2week / $sumtotalcredit2week) * 100, 2);
     	$sumavgcredit2week = ($sumactivecount == 0) ? 0 : round(($sumtotalcredit2week / $sumactivecount), 2);
     	$sumroi = ($summonth_cost == 0) ? 0 : round($sumtotalcredit / $summonth_cost * 100, 2);
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
			if($j == 1)
			{
?>
				<td class="tdr point" rowspan="<?= $monthcnt?>"><?= $today?></td>
<?
			}
?>
            <td class="tdr"><?= $regmonth ?></td>
            <td class="tdr"><?= number_format($joincount) ?></td>
            <td class="tdr"><?= number_format($activecount) ?></td>
            <td class="tdr"><?= $activerate ?>%</td>
            <td class="tdr"><?= round(100 - $activerate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($webcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($mobilecredit, 2) ?></td>
            <td class="tdr"><?= number_format($mobilerate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($totalcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($avgcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($webcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($mobilecredit2week, 2) ?></td>
            <td class="tdr"><?= number_format($mobile2weekrate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($totalcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($avgcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($month_cost) ?></td>
            <td class="tdr"><?= number_format($roi, 2) ?></td>
        </tr>
<?
		if($j == $monthcnt)
		{
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point_title" colspan="2">합계</td>
            	<td class="tdr point"><?= number_format($sumjoincount) ?></td>
            	<td class="tdr point"><?= number_format($sumactivecount) ?></td>
            	<td class="tdr point"><?= $sumactiverate ?>%</td>
            	<td class="tdr point"><?= round(100 - $sumactiverate, 2) ?>%</td>
            	<td class="tdr point">$<?= number_format($sumwebcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobilerate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumwebcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit2week, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobile2weekrate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summonth_cost) ?></td>
	            <td class="tdr point"><?= number_format($sumroi, 2) ?></td>
			</tr>
<?
			$j = 1;
			$rowcnt++;
    	}
    	else
    	{
    		$j++;	
    	}
    }
    
    if(sizeof($amazon_list) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="18">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>
	
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>