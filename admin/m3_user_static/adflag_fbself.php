<?
    $top_menu = "user_static";
    $sub_menu = "adflag_fbself";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    $search_start_orderdate = $_GET["start_orderdate"];
    $search_end_orderdate = $_GET["end_orderdate"];
    $search_payday = $_GET["payday"];
	$isearch = $_GET["issearch"];
	$select_time = $_GET["select_time"];
	$category = ($_GET["category"] == "") ? "1":$_GET["category"];
	
	if($search_payday != "" && ($search_start_orderdate != "" || $search_end_orderdate != ""))
		error_back("검색값이 잘못되었습니다.");
	
	if ($isearch == "")
	{
		$search_end_createdate  = date("Y-m-d", time() - 60 * 60);
		$search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 6);
	}
	
	if ($select_time == "")
	{
		$select_time = "marketing";
	}
	
	function get_stat($summarylist, $fbsource, $today, $property)
	{
		$stat = 0;
		
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if ($summarylist[$i]["day"] == $today && $summarylist[$i]["fbsource"] == $fbsource)
			{
				$stat += $summarylist[$i][$property];
			}
		}	
		
		return $stat;
	}
	
	$now = time();
	
	if ($search_end_createdate != "")
	{
		$now = strtotime($search_end_createdate);
	}
	else
	{
		$search_end_createdate = date('Y-m-d');
	}
	
	$db_main = new CDatabase_Main();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});

	$(function() {
	    $("#start_orderdate").datepicker({ });
	});

	$(function() {
	    $("#end_orderdate").datepicker({ });
	});

	function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;

        search_form.submit();
    }
	
	function tab_change(tab)
	{
		window.location.href = "adflag_fbself.php?category="+tab + "&start_createdate=<?= $search_start_createdate ?>&end_createdate=<?= $search_end_createdate ?>&payday=<?= $search_payday ?>&issearch=1";
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	 <form name="search_form" id="search_form"  method="get" action="adflag_fbself.php">
		<input type=hidden name=issearch value="1"/>
		<input type=hidden name=category value="<?= $category ?>"/>
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 자체마케팅 유입 분석</div>
			<div class="search_box">
                	시간 기준 : 				
					<input type="radio" value="marketing" name="select_time" id="select_time" <?= ($select_time == "" || $select_time == "marketing") ? "checked=\"true\"" : ""?>/> 마케팅 
					<input type="radio" value="server" name="select_time" id="select_time" <?= ($select_time == "server") ? "checked=\"true\"" : ""?>/> 서버
					&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
                    &nbsp;&nbsp;&nbsp;&nbsp;결제액 기준 : 가입 후 <input type="text" class="search_text" id="payday" name="payday" style="width:60px" value="<?= $search_payday ?>" onkeypress="search_press(event); return checknum();" />일 이내 결제
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
			
			<div class="clear"></div>
                
			<div class="search_box">
				<input type="text" class="search_text" id="start_orderdate" name="start_orderdate" value="<?= $search_start_orderdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_orderdate" name="end_orderdate" value="<?= $search_end_orderdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
				&nbsp;&nbsp;&nbsp;&nbsp;기간내 결제
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
		</div>
		<!-- //title_warp -->
            
		<div class="search_result">
			<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
		</div>
            
		<ul class="tab">
			<li id="tab_1" class="<?= ($category == "1") ? "select" : "" ?>" onclick="tab_change('1')">성과 요약</li>
			<li id="tab_2" class="<?= ($category == "2") ? "select" : "" ?>" onclick="tab_change('2')">일별 추이</li>
		</ul>
	 </form>
	 
<?
	if ($category == "1")
	{
?>
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
			</colgroup>
            <thead>
            	<tr>
	                <th>캠페인종류</th>
                	<th class="tdr">회원가입수</th>
                	<th class="tdr">미게임회원수</th>
                	<th class="tdr">미게임율</th>
                	<th class="tdr">결제회원수</th>
                	<th class="tdr">PUR</th>
	               	<th class="tdr">총결제횟수</th>
                	<th class="tdr">총결제<br>credit</th>
                	<th class="tdr">평균결제<br>credit</th>
                	<th class="tdr">ARPPU</th>
            	</tr>
            </thead>
            <tbody>
<?
		$sql = "SELECT * FROM (SELECT fbsource,COUNT(*) AS totalcount, ".
				"ABS(IFNULL(SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
				",0)) AS unplaycount,";
		
		if ($search_payday != "")
		{
			$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0)) AS paycount,".
					"(IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) + IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0)) AS totalpaycount,".
					"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0))AS totalcredit ";
		}
		else if(($search_start_orderdate != "" && $search_end_orderdate != ""))
		{
			$search_start_orderdate_minute = $search_start_orderdate." 00:00:00";
			$search_end_orderdate_minute = $search_end_orderdate." 23:59:59";
		
			$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute') THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute') THEN 1 ELSE 0 END),0)) AS paycount,".
					"(IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0) + IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0)) AS totalpaycount,".
					"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0)) AS totalcredit ";
		}
		else
		{
			$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0)) AS paycount,".
					"(IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) + IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1)),0)) AS totalpaycount,".
					"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1)),0)) AS totalcredit ";
		}
		
		if($select_time == "marketing")
		{
			$sdate = date('Y-m-d H:i:s', strtotime($search_start_createdate) - 60 * 60 * 9); //(마케팅)-UTC
			$tail =  "createdate >='$sdate' AND createdate <='$search_end_createdate 14:59:59'";
		}
		else if($select_time == "server")
		{
			$sdate = date('Y-m-d H:i:s', strtotime($search_start_createdate)); // 서버 시간
			$tail =  "createdate >='$sdate' AND createdate <='$search_end_createdate 23:59:59'";
		}
		
		$sql .= " FROM tbl_user_ext WHERE $tail AND adflag LIKE 'fbself%' GROUP BY fbsource) AS TEMP ORDER BY totalcount DESC";
		$summarylist = $db_main->gettotallist($sql);

		for ($i=0; $i<sizeof($summarylist); $i++)   		
    	{
	  		$fbsource = $summarylist[$i]["fbsource"];
	  		$totalcount = $summarylist[$i]["totalcount"];
			$unplaycount = $summarylist[$i]["unplaycount"];
			$paycount = $summarylist[$i]["paycount"];
			$totalpaycount = $summarylist[$i]["totalpaycount"];
	   		$totalcredit = $summarylist[$i]["totalcredit"];

			if ($totalcount == 0)
			{
				$unplayratio = 0;
				$payratio = 0;
				$averagecredit = 0;
			}
			else
			{
				$unplayratio = round($unplaycount * 10000 / $totalcount) / 100;
				$payratio = round($paycount * 10000 / $totalcount) / 100;
				$averagecredit = round($totalcredit * 1000 / $totalcount) / 1000;
			}
?>
				<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
                    <td class="point"><?= $fbsource ?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>
                    <td class="tdr point"><?= number_format($unplaycount) ?></td>
                    <td class="tdr point"><?= $unplayratio ?> %</td>
                    <td class="tdr point"><?= number_format($paycount) ?></td>
                    <td class="tdr point"><?= $payratio ?> %</td>
                    <td class="tdr point"><?= number_format($totalpaycount) ?></td>
                    <td class="tdr point">$ <?= number_format($totalcredit) ?></td>
                    <td class="tdr point">$ <?= number_format($averagecredit, 2) ?></td>
                    <td class="tdr point"><?= ($paycount == 0) ? "-" : number_format($totalcredit / $paycount, 1) ?></td>
                </tr>
<?
    	}
?>
			</tbody>
		</table>
	</div>
<?
	}
	else if ($category == "2")
	{
?>
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">  
                <col width="">  
                <col width="">  
                <col width="">
			</colgroup>
			<thead>
	            <tr>
	                <th>유입일</th>
	                <th class="tdr">캠페인종류</th>
	                <th class="tdr">회원가입수</th>
	                <th class="tdr">미게임회원수</th>
	                <th class="tdr">미게임율</th>
	                <th class="tdr">결제회원수</th>
	                <th class="tdr">PUR</th>
	                <th class="tdr">총결제횟수</th>
	                <th class="tdr">총결제 credit<br>(평균)</th>
	                <th class="tdr">ARPPU</th>
	            </tr>
			</thead>
			<tbody>
<?
		$sql = "SELECT fbsource,DATE_FORMAT(createdate,'%Y-%m-%d') AS day,COUNT(*) AS totalcount, ".
				"ABS(IFNULL(SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
				",0)) AS unplaycount,";
			
		if ($search_payday != "")
		{
			$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0)) AS paycount,".
					"(IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) + IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0)) AS totalpaycount,".
					"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0))AS totalcredit ";
		}
		else if(($search_start_orderdate != "" && $search_end_orderdate != ""))
		{
			$search_start_orderdate_minute = $search_start_orderdate." 00:00:00";
			$search_end_orderdate_minute = $search_end_orderdate." 23:59:59";

			$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute') THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute') THEN 1 ELSE 0 END),0)) AS paycount,".
					"(IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0) + IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0)) AS totalpaycount,".
					"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0)) AS totalcredit ";
		}
		else
		{
			$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0)) AS paycount,".
					"(IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) + IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1)),0)) AS totalpaycount,".
					"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1)),0)) AS totalcredit ";
		}
		
		if($select_time == "marketing")
		{
			$sdate = date('Y-m-d H:i:s', strtotime($search_start_createdate) - 60 * 60 * 9); //(마케팅)-UTC
			$tail =  "createdate >='$sdate' AND createdate <='$search_end_createdate 14:59:59'";
		}
		else if($select_time == "server")
		{
			$sdate = date('Y-m-d H:i:s', strtotime($search_start_createdate)); // 서버 시간
			$tail =  "createdate >='$sdate' AND createdate <='$search_end_createdate 23:59:59'";
		}
			
		$sql .= " FROM tbl_user_ext WHERE $tail AND adflag LIKE 'fbself%' GROUP BY fbsource,DATE_FORMAT(createdate,'%Y-%m-%d')";
		$summarylist = $db_main->gettotallist($sql);
		
		$sum_totalcount = array();
		$sum_unplaycount = array();
		$sum_paycount = array();
		$sum_totalpaycount = array();
		$sum_totalcredit = array();
		
		$fbsource_list = array();
		$fbsource_info = "";
		
	   	for ($i=0; $i<sizeof($summarylist); $i++)
	   	{
	   		$fbsource = $summarylist[$i]["fbsource"];
	   		
	   		if (strpos($fbsource_info, "[$fbsource]") === false)
	   		{
				$fbsource_info .= "[$fbsource]";
	   			$fbsource_list[sizeof($fbsource_list)] = $fbsource;	
	   		}
	   	}
	   		
	    while (true)
	    {
	    	$today = date("Y-m-d", $now);
	    	$tomorrow = date("Y-m-d", $now + 24 * 60 * 60);
	    	
	    	if ($tomorrow < $search_start_createdate)
	    		break;
	
	    	for ($i=0; $i<sizeof($fbsource_list); $i++)
	    	{
	    		$fbsource = $fbsource_list[$i];
	
	    		if ($today < $search_start_createdate)
	    		{
		    		$totalcount = $sum_totalcount[$fbsource];
	    			$unplaycount = $sum_unplaycount[$fbsource];
	   				$paycount = $sum_paycount[$fbsource];
	   				$totalpaycount = $sum_totalpaycount[$fbsource];
	   				$totalcredit = $sum_totalcredit[$fbsource];
	    		}
	    		else
	    		{
					$totalcount = get_stat($summarylist, $fbsource, $today, "totalcount");
					$unplaycount = get_stat($summarylist, $fbsource, $today, "unplaycount");
					$paycount = get_stat($summarylist, $fbsource, $today, "paycount");
					$totalpaycount = get_stat($summarylist, $fbsource, $today, "totalpaycount");
					$totalcredit = get_stat($summarylist, $fbsource, $today, "totalcredit");
		    			
					if ($sum_totalcount[$fbsource] == "")
						$sum_totalcount[$fbsource] = 0;
		    			
					if ($sum_unplaycount[$fbsource] == "")
						$sum_unplaycount[$fbsource] = 0;
		    			
					if ($sum_paycount[$fbsource] == "")
						$sum_paycount[$fbsource] = 0;
					if ($sum_totalpaycount[$fbsource] == "")
						$sum_totalpaycount[$fbsource] = 0;
					if ($sum_totalcredit[$fbsource] == "")
						$sum_totalcredit[$fbsource] = 0;
										
		    		$sum_totalcount[$fbsource] += $totalcount;
		    		$sum_unplaycount[$fbsource] += $unplaycount;
					$sum_paycount[$fbsource] += $paycount;
					$sum_totalpaycount[$fbsource] += $totalpaycount;
					$sum_totalcredit[$fbsource] += $totalcredit;
				}
	       		
	    		if ($totalcount == 0)
	    		{
	    			$unplayratio = 0;
	    			$payratio = 0;
	    			$averagecredit = 0;
	    		}
	    		else
	    		{
	    			$unplayratio = round($unplaycount * 10000 / $totalcount) / 100;
	    			$payratio = round($paycount * 10000 / $totalcount) / 100;
	    			$averagecredit = round($totalcredit * 1000 / $totalcount) / 1000;
	    		}
?>
	        	<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if ($i == 0)
				{
?>                        
					<td class="tdc point_title" rowspan="<?= sizeof($fbsource_list) ?>" valign="center"><?= ($today < $search_start_createdate) ? "<b>Total</b>" : $today ?></td>
<?
				}
?>                            
					<td class="tdr point"><?= $fbsource ?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>
                    <td class="tdr point"><?= number_format($unplaycount) ?></td>
                    <td class="tdr point"><?= $unplayratio ?> %</td>
                    <td class="tdr point"><?= number_format($paycount) ?></td>
                    <td class="tdr point"><?= $payratio ?> %</td>
                    <td class="tdr point"><?= number_format($totalpaycount) ?></td>
                    <td class="tdr point">$ <?= number_format($totalcredit) ?> ($ <?= number_format($averagecredit, 2) ?>)</td>
                    <td class="tdr point"><?= ($paycount == 0) ? "-" : number_format($totalcredit / $paycount, 1) ?></td>
				</tr>
<?
			}
		
			$now -= 60 * 60 * 24;
		}
?>    
			</tbody>
		</table>
	</div>
<?
	}
?>
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main->end();

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>