<?  
    include($_SERVER["DOCUMENT_ROOT"]."/common/common_include.inc.php");
    
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    $search_start_orderdate = $_GET["start_orderdate"];
    $search_end_orderdate = $_GET["end_orderdate"];
    $search_os = $_GET["search_os"];
    $search_payday = $_GET["payday"];
	$issearch = $_GET["issearch"];
	$select_time = $_GET["select_time"];
	
	if ($issearch == "")
	{
		$search_end_createdate  = date("Y-m-d", time() - 60 * 60);
		$search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 6);
		$search_tail = "platform != 0";
	}
	
	$filename_str ="";
	
	if ($select_time == "")
		$select_time = "marketing";
	
	if($search_os == "1")
	{
		$search_tail = "platform = 1";
		$filename_str = "_ios";
	}
	else if($search_os == "2")
	{
		$search_tail = "platform = 2";
		$filename_str = "_android";
	}
	else if($search_os == "3")
	{
		$search_tail = "platform = 3";
		$filename_str = "_amazon";		
	}
	else
	{
		$search_tail = "platform != 0";
		$filename_str = "_total";
	}
	
	function get_adflag_list($summarylist)
	{
		$row_array = array();
	
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if(!in_array($summarylist[$i]["adflag"], $row_array))
				array_push($row_array, $summarylist[$i]["adflag"]);
		}
	
		return $row_array;
	}
		
	function get_today_row_array($summarylist, $today)
	{
		$row_array = array();
	
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if ($summarylist[$i]["day"] == $today)
			{
				array_push($row_array, $summarylist[$i]["adflag"]);
			}
		}
	
		return $row_array;
	}
	
	function get_stat($summarylist, $adflag, $today, $property)
	{
		$stat = 0;
		
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if ($summarylist[$i]["day"] == $today && $summarylist[$i]["adflag"] == $adflag)
			{
				$stat += $summarylist[$i][$property];
			}
		}	
		
		return $stat;
	}
	
	function get_total_stat($summarylist, $adflag, $property)
	{
		$stat = 0;
	
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if ($summarylist[$i]["adflag"] == $adflag)
			{
				$stat += $summarylist[$i][$property];
			}
		}
	
		return $stat;
	}
	
	function get_total_spend($spendlist, $adflag)
	{
		$stat = 0;
		
		for ($i=0; $i<sizeof($spendlist); $i++)
		{
			if ($spendlist[$i]["agencyname"] == $adflag)
			{
				$stat += $spendlist[$i]["spend"];
			}
		}
	
		return $stat;
	}
?>
<?
	header("Pragma: public");
	header("Expires: 0");
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=adflag_mobile_detail".$filename_str."_"."$search_start_createdate"._."$search_end_createdate.xls");
	header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
	header("Content-Description: PHP5 Generated Data");
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<div id="tab_content_1">
		<table class="tbl_list_basic1" border="1">
            <colgroup>
                <col width="90">
                <col width="120">
                <col width="100">
                <col width="100">
                <col width="100">
                <col width="100">
                <col width="100">
                <col width="100">
                <col width="100">
                <col width="100">
                <col width="100">
                <col width="100">
            </colgroup>
            <thead>
            <tr>
                <th>유입일</th>
                <th class="tdr">유입경로</th>
                <th class="tdr">회원가입수</th>
                <th class="tdr">미게임회원수</th>                
                <th class="tdr">미게임률</th>                
                <th class="tdr">결제회원수</th>
                <th class="tdr">PUR</th>
                <th class="tdr">총결제 금액</th>
                <th class="tdr">ARPU</th>
                <th class="tdr">ARPPU</th>
                <th class="tdr">비용</th>
                <th class="tdr">ROI</th>
                <th class="tdr">CPI</th>
            </tr>
            </thead>
            <tbody>
<?
    $db_main = new CDatabase_Main();

    $now = time();
    
    if ($search_end_createdate != "")
    {
    	$now = strtotime($search_end_createdate);
    }
    else
    {
    	$search_end_createdate = date('Y-m-d');
    }
    
    if ($search_start_createdate == "")
    {
    	$search_start_createdate = '2012-09-03';
    }
    
	$sql = "SELECT adflag, ";
	
	if($select_time == "marketing")
		$sql .= "DATE_FORMAT(date_add(createdate, INTERVAL 9 HOUR),'%Y-%m-%d') AS day, ";
	else if($select_time == "server")
		$sql .= "DATE_FORMAT(createdate,'%Y-%m-%d') AS day, ";
	
	$sql .=	"COUNT(*) AS totalcount,".
			"ABS(IFNULL(SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			",0)) AS unplaycount,";
	
	if ($search_payday != "")
	{
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0)) AS paycount,".
				"(IFNULL(SUM((SELECT IFNULL(ROUND(SUM(facebookcredit)/10,2),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) + IFNULL(SUM((SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0))AS totalcredit ";
	}
	else if(($search_start_orderdate != "" && $search_end_orderdate != ""))
	{
		$search_start_orderdate_minute = $search_start_orderdate." 00:00:00";
		$search_end_orderdate_minute = $search_end_orderdate." 23:59:59";
	
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute') THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute') THEN 1 ELSE 0 END),0)) AS paycount,".
				"(IFNULL(SUM((SELECT IFNULL(ROUND(SUM(facebookcredit)/10,2),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0) + IFNULL(SUM((SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0)) AS totalcredit ";
	}
	else
	{
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0)) AS paycount,".
				"(IFNULL(SUM((SELECT IFNULL(ROUND(SUM(facebookcredit)/10,2),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) + IFNULL(SUM((SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1)),0)) AS totalcredit ";
	}
	
	if($select_time == "marketing")
	{
		$sdate = date('Y-m-d H:i:s', strtotime($search_start_createdate) - 60 * 60 * 9); //(마케팅)-UTC
		$tail =  " AND adflag NOT LIKE 'retention%' AND createdate >='$sdate' AND createdate <='$search_end_createdate 14:59:59' GROUP BY adflag,DATE_FORMAT(date_add(createdate, interval 9 hour),'%Y-%m-%d') ORDER BY day DESC, adflag ASC";
	}
	else if($select_time == "server")
	{
		$sdate = date('Y-m-d H:i:s', strtotime($search_start_createdate)); // 서버 시간
		$tail =  " AND adflag NOT LIKE 'retention%' AND createdate >='$sdate' AND createdate <='$search_end_createdate 23:59:59' GROUP BY adflag,DATE_FORMAT(createdate,'%Y-%m-%d') ORDER BY day DESC, adflag ASC";
	}
		
	$sql .= " FROM tbl_user_ext WHERE $search_tail $tail ";
	$summarylist = $db_main->gettotallist($sql);
	
	for($i=0; $i<sizeof($summarylist); $i++)
	{
		$today = $summarylist[$i]["day"];
		$adflag = $summarylist[$i]["adflag"];
		
		if($adflag == "")
			$adflag = "viral";
	}
	
	$currenttoday = "";
	
	$db_main2 = new CDatabase_Main2();
	
	$tail = "";
	
	if($search_os == "1")
		$tail .= " AND platform = 1 ";
	else if($search_os == "2")
		$tail .= " AND platform = 2 ";
	else if($search_os == "3")
		$tail .= " AND platform = 3 ";
	
	$sql = "SELECT today, agencyname, spend FROM tbl_agency_spend_daily WHERE today >= '$sdate' AND today <= '$search_end_createdate' $tail ORDER BY today DESC, agencyidx ASC";
	$spendlist = $db_main2->gettotallist($sql);
	
	for($i=0; $i<sizeof($summarylist); $i++)
	{
		$today = $summarylist[$i]["day"];
		
		if($currenttoday != $today)
		{
			$currenttoday = $today;
			
			$viral_list = get_today_row_array($summarylist, $today);
			
			for($j=0; $j<sizeof($viral_list); $j++)
			{
				$adflag = $viral_list[$j];
				
				$totalcount = get_stat($summarylist, $adflag, $today, "totalcount");
				$unplaycount = get_stat($summarylist, $adflag, $today, "unplaycount");				
				$paycount = get_stat($summarylist, $adflag, $today, "paycount");
				$totalcredit = get_stat($summarylist, $adflag, $today, "totalcredit");
				
				$unplayratio = ($totalcount > 0) ? round($unplaycount * 100 / $totalcount, 2) : 0;
				$payratio = ($totalcount > 0) ? round($paycount * 100 / $totalcount, 2) : 0;
				$averagecredit = ($totalcount > 0) ? round($totalcredit / $totalcount, 2) : 0;
				
				$spend = 0;
				
				for($k=0; $k<sizeof($spendlist); $k++)
				{
					if(($spendlist[$k]["agencyname"] == $adflag) && ($spendlist[$k]["today"] == $currenttoday))
						$spend += $spendlist[$k]["spend"];
				}
?>
				<tr>
<?
				if($j == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($viral_list)+1?>" valign="center"><?= $today ?></td>
<?
				} 
?>
					<td class="tdr point"><?= ($adflag == "") ? "viral" : $adflag ?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>
                    <td class="tdr point"><?= number_format($unplaycount) ?></td>                    
                    <td class="tdr point"><?= $unplayratio ?> %</td>                    
                    <td class="tdr point"><?= number_format($paycount) ?></td>
                    <td class="tdr point"><?= $payratio ?> %</td>
                    <td class="tdr point">$<?= number_format($totalcredit) ?></td>
                    <td class="tdr point">$<?= $averagecredit ?></td>
                    <td class="tdr point"><?= ($paycount == "0") ? "-" : number_format($totalcredit / $paycount, 1) ?></td>
                    <td class="tdr point"><?= ($spend == "0") ? "-" : "$".number_format($spend) ?></td>
                    <td class="tdr point"><?= ($spend == "0") ? "-" : number_format(($totalcredit / $spend) * 100, 1)."%" ?></td>
                    <td class="tdr point"><?= ($totalcount == "0") ? "-" : "$".number_format(($spend / $totalcount), 1) ?></td>
				</tr>
<?
				$total_totalcount +=$totalcount;
				$total_unplaycount +=$unplaycount;
				$total_unplayratio = ($total_totalcount > 0) ? round($total_unplaycount * 100 / $total_totalcount, 2) : 0;
				$total_paycount+=$paycount;
				$total_totalcredit+=$totalcredit;
				$total_spend += $spend;
				
				$total_payratio = ($total_totalcount > 0) ? round($total_paycount * 100 / $total_totalcount, 2) : 0;
				$total_averagecredit = ($total_totalcount > 0) ? round($total_totalcredit / $total_totalcount, 2) : 0;
				
				if($j+1 == sizeof($viral_list))
				{
?>		
				<tr>
					<td class="tdr point">Total</td>
                    <td class="tdr point"><?= number_format($total_totalcount) ?></td>
                    <td class="tdr point"><?= number_format($total_unplaycount) ?></td>                    
                    <td class="tdr point"><?= $total_unplayratio ?> %</td>                    
                    <td class="tdr point"><?= number_format($total_paycount) ?></td>
                    <td class="tdr point"><?= $total_payratio ?> %</td>
                    <td class="tdr point">$<?= number_format($total_totalcredit) ?></td>
                    <td class="tdr point">$<?= $total_averagecredit ?></td>
                    <td class="tdr point"><?= ($total_paycount == "0") ? "-" : number_format($total_totalcredit / $total_paycount, 1) ?></td>
                    <td class="tdr point"><?= ($total_spend == "0") ? "-" : "$".number_format($total_spend) ?></td>
                    <td class="tdr point"><?= ($total_spend == "0") ? "-" : number_format(($total_totalcredit / $total_spend) * 100, 1)."%" ?></td>
                    <td class="tdr point"><?= ($total_totalcount == "0") ? "-" : "$".number_format(($total_spend / $total_totalcount), 1) ?></td>
				</tr>	
<?				
				}

			}
			
			$total_totalcount =0;
			$total_unplaycount =0;
			$total_unplayratio = 0;
			$total_paycount =0;
			$total_totalcredit =0;
			$total_spend =0;
			
			$total_payratio = 0;
			$total_averagecredit = 0;
			
		}
	}
	
	if(sizeof($summarylist) > 0)
	{
		$bigger_viral_list = get_adflag_list($summarylist);
		usort($bigger_viral_list, 'strcasecmp');
		
		for($j=0; $j<sizeof($bigger_viral_list); $j++)
		{
			$adflag = $bigger_viral_list[$j];
			
			$sum_totalcount = get_total_stat($summarylist, $adflag, "totalcount");
			$sum_unplaycount = get_total_stat($summarylist, $adflag, "unplaycount");			
			$sum_paycount = get_total_stat($summarylist, $adflag, "paycount");
			$sum_totalcredit = get_total_stat($summarylist, $adflag, "totalcredit");
				
			$sum_unplayratio = ($sum_totalcount > 0) ? round($sum_unplaycount * 100 / $sum_totalcount, 2) : 0;
			$sum_payratio = ($sum_totalcount > 0) ? round($sum_paycount * 100 / $sum_totalcount, 2) : 0;
			$sum_averagecredit = ($sum_totalcount > 0) ? round($sum_totalcredit / $sum_totalcount, 2) : 0;
			
			$sum_spend = get_total_spend($spendlist, $adflag);
?>
				<tr>
<?
				if($j == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($bigger_viral_list)?>" valign="center">Total</td>
<?
				}
?>
					<td class="tdr point"><?= ($adflag == "") ? "viral" : $adflag ?></td>
                    <td class="tdr point"><?= number_format($sum_totalcount) ?></td>
                    <td class="tdr point"><?= number_format($sum_unplaycount) ?></td>                    
                    <td class="tdr point"><?= $sum_unplayratio ?> %</td>                    
                    <td class="tdr point"><?= number_format($sum_paycount) ?></td>
                    <td class="tdr point"><?= $sum_payratio ?> %</td>
                    <td class="tdr point">$<?= number_format($sum_totalcredit) ?></td>
                    <td class="tdr point">$<?= $sum_averagecredit ?></td>
                    <td class="tdr point"><?= ($sum_paycount == "0") ? "-" : number_format($sum_totalcredit / $sum_paycount, 1) ?></td>
                    <td class="tdr point"><?= ($sum_spend == "0") ? "-" : "$".number_format($sum_spend) ?></td>
                    <td class="tdr point"><?= ($sum_spend == "0") ? "-" : number_format(($sum_totalcredit / $sum_spend) * 100, 1)."%" ?></td>
                    <td class="tdr point"><?= ($sum_totalcount == "0") ? "-" : "$".number_format(($sum_spend / $sum_totalcount), 1) ?></td>
				</tr>
<?
		}
	}
?>    
			</tbody>
		</table>
	</div>
	
<?
	$db_main->end();
	$db_main2->end();
	
?>