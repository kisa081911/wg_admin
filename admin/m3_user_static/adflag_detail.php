<?
    $top_menu = "user_static";
    $sub_menu = "adflag_detail";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    $search_start_orderdate = $_GET["start_orderdate"];
    $search_end_orderdate = $_GET["end_orderdate"];
    $search_payday = $_GET["payday"];
	$isearch = $_GET["issearch"];
	$select_time = $_GET["select_time"];
	
	if($search_payday != "" && ($search_start_orderdate != "" || $search_end_orderdate != ""))
		error_back("검색값이 잘못되었습니다.");
		
	if ($isearch == "")
	{
		$search_end_createdate  = date("Y-m-d", time() - 60 * 60);
		$search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 6);
	}
	
	if ($select_time == "")
	{
		$select_time = "marketing";
	}
	
	function get_stat($summarylist, $adflag_list, $adflag, $today, $property)
	{
		$stat = 0;
		
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if ($summarylist[$i]["day"] == $today)
			{
				$dummy = $summarylist[$i]["adflag"];
			
				if(in_array($dummy, $adflag_list) == false)
					$dummy = "viral";
				
				if($dummy == $adflag)
					$stat += $summarylist[$i][$property];	
			}
		}	
		
		return $stat;
	}
	
	function get_total_stat($summarylist, $adflag_list, $adflag, $property)
	{
		$stat = 0;
	
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			$dummy = $summarylist[$i]["adflag"];
			
			if(in_array($dummy, $adflag_list) == false)
				$dummy = "viral";
			
			if($dummy == $adflag)
				$stat += $summarylist[$i][$property];
		}
		
		return $stat;
	}
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
$(function() {
    $("#start_createdate").datepicker({ });
});

$(function() {
    $("#end_createdate").datepicker({ });
});

$(function() {
    $("#start_orderdate").datepicker({ });
});

$(function() {
    $("#end_orderdate").datepicker({ });
});
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">  
	<form name="search_form" id="search_form"  method="get" action="adflag_detail.php">
		<input type=hidden name=issearch value="1" />
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 유입경로별 유입 현황(Web)</div>
			<div class="search_box">
				시간 기준 :
				<!-- input type="radio" value="marketing" name="select_time" id="select_time" <?= ($select_time == "marketing") ? "checked=\"true\"" : ""?>/> 마케팅
				<input type="radio" value="server" name="select_time" id="select_time" <?= ($select_time == "server") ? "checked=\"true\"" : ""?>/ --> 서버 				 
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
				&nbsp;&nbsp;&nbsp;&nbsp;결제액 기준 : 가입 후 <input type="text" class="search_text" id="payday" name="payday" style="width:60px" value="<?= $search_payday ?>" onkeypress="search_press(event); return checknum();" />일 이내 결제
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
                
                <div class="clear"></div>
                
                <div class="search_box">
                    <input type="text" class="search_text" id="start_orderdate" name="start_orderdate" value="<?= $search_start_orderdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
                    <input type="text" class="search_text" id="end_orderdate" name="end_orderdate" value="<?= $search_end_orderdate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
                    &nbsp;&nbsp;&nbsp;&nbsp;기간내 결제
                     <input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
                </div>
            </div>
            <!-- //title_warp -->
            
            <div class="search_result">
                <span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
            </div>
            
            <div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
                <col width=""> 
                <col width="">
            </colgroup>
            <thead>
            <tr>
                <th>유입일</th>
                <th class="tdr">유입경로</th>
                <th class="tdr">회원가입수</th>
                <th class="tdr">미게임회원수</th>
                <th class="tdr">2주이탈복귀</th>
                <th class="tdr">결제회원수</th>
                <th class="tdr">총결제횟수</th>
                <th class="tdr">총결제 credit<br>(평균)</th>
            </tr>
            </thead>
            <tbody>
<?
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();


    $now = time();
    
    if ($search_end_createdate != "")
    {
    	$now = strtotime($search_end_createdate);
    }
    else
    {
    	$search_end_createdate = date('Y-m-d');
    }
    
    if ($search_start_createdate == "")
    {
    	$search_start_createdate = '2012-09-03';
    }
    
	$sql = "SELECT adflag, ";
	
	if($select_time == "marketing")
		$sql .= "DATE_FORMAT(date_add(createdate, interval 9 hour),'%Y-%m-%d') AS day, ";				 
	else if($select_time == "server")
		$sql .= "DATE_FORMAT(createdate,'%Y-%m-%d') AS day, ";
		
	$sql .=	"COUNT(*) AS totalcount, ".
			"ABS(IFNULL(SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			",0)) AS unplaycount,";
		
	if ($search_payday != "")
	{
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0)) AS paycount,".
				"(IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) + IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0)) AS totalpaycount,".
				"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0))AS totalcredit ";
	}
	else if(($search_start_orderdate != "" && $search_end_orderdate != ""))
	{
		$search_start_orderdate_minute = $search_start_orderdate." 00:00:00";
		$search_end_orderdate_minute = $search_end_orderdate." 23:59:59";
		
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute') THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute') THEN 1 ELSE 0 END),0)) AS paycount,".
				"(IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0) + IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0)) AS totalpaycount,".
				"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0)) AS totalcredit ";
	}	
	else
	{
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0)) AS paycount,".
				"(IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) + IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1)),0)) AS totalpaycount,".
				"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1)),0)) AS totalcredit ";
	}
	
	if($select_time == "marketing")
	{
		$sdate = date('Y-m-d H:i:s', strtotime($search_start_createdate) - 60 * 60 * 9); //(마케팅)-UTC
		$tail =  "createdate >='$sdate' AND createdate <='$search_end_createdate 14:59:59' AND platform = 0 GROUP BY adflag,DATE_FORMAT(date_add(createdate, interval 9 hour),'%Y-%m-%d')";
	}
	else if($select_time == "server")
	{
		$sdate = date('Y-m-d H:i:s', strtotime($search_start_createdate)); // 서버 시간
		$tail =  "createdate >='$sdate' AND createdate <='$search_end_createdate 23:59:59' AND platform = 0 GROUP BY adflag,DATE_FORMAT(createdate,'%Y-%m-%d')";
	}	
	
	$sql .= " FROM tbl_user_ext WHERE ".$tail." ORDER BY day DESC, adflag ASC";
	$summarylist = $db_main->gettotallist($sql);
	
	for($i=0; $i<sizeof($summarylist); $i++)
	{
		$today = $summarylist[$i]["day"];
		$adflag = $summarylist[$i]["adflag"];
		
		$summarylist[$i]["returncount"] = $db_main2->getvalue("SELECT IFNULL(SUM(usercount),0) FROM tbl_user_retention WHERE adflag = '$adflag' AND retentiondate='$today'");
	}
	
	$sql = "SELECT IF(adflag = '', 'viral', adflag) AS adflag FROM tbl_user_ext ".
	   	"WHERE adflag NOT LIKE ('cr_%') AND  adflag NOT LIKE ('share_%') AND adflag NOT LIKE ('og_%') AND adflag NOT LIKE ('game_noti_%')   AND adflag != 'email_retention'     ".
	   	"AND '$sdate' <= createdate AND createdate <= '$search_end_createdate' AND platform = 0  GROUP BY adflag";
	$marketing_list = $db_main->gettotallist($sql);
	
	$adflag_list = array();
	
	for($i=0; $i<sizeof($marketing_list); $i++)
		array_push($adflag_list, $marketing_list[$i]["adflag"]);
	
	$currenttoday = "";
		
	for($i=0; $i<sizeof($summarylist); $i++)
	{
		$today = $summarylist[$i]["day"];
		
		if($currenttoday != $today)
		{
			$currenttoday = $today;
			
			for($j=0; $j<sizeof($marketing_list); $j++)
			{
				$adflag_info = $marketing_list[$j]["adflag"];
				
				$totalcount = get_stat($summarylist, $adflag_list, $adflag_info, $today, "totalcount");
				$unplaycount = get_stat($summarylist, $adflag_list, $adflag_info, $today, "unplaycount");
				$returncount = get_stat($summarylist, $adflag_list, $adflag_info, $today, "returncount");
				$paycount = get_stat($summarylist, $adflag_list, $adflag_info, $today, "paycount");
				$totalpaycount = get_stat($summarylist, $adflag_list, $adflag_info, $today, "totalpaycount");
				$totalcredit = get_stat($summarylist, $adflag_list, $adflag_info, $today, "totalcredit");
				
				$totalcredit = round($totalcredit/10, 1);
								
				$unplayratio = ($totalcount > 0) ? round($unplaycount * 10000 / $totalcount) / 100 : 0;
				$payratio = ($totalcount > 0) ? round($paycount * 10000 / $totalcount) / 100 : 0;
				$averagecredit = ($totalcount > 0) ? round($totalcredit * 1000 / $totalcount) / 1000 : 0;

?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($j == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($marketing_list)?>" valign="center"><?= $today ?></td>
<?
				} 
?>
					<td class="tdr point"><?= $adflag_info ?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>
                    <td class="tdr point"><?= number_format($unplaycount) ?> (<?= $unplayratio ?> %)</td>
                    <td class="tdr point"><?= number_format($returncount) ?></td>
                    <td class="tdr point"><?= number_format($paycount) ?> (<?= $payratio ?> %)</td>
                    <td class="tdr point"><?= number_format($totalpaycount) ?></td>
                    <td class="tdr point">$<?= number_format($totalcredit) ?> ($<?= $averagecredit ?>)</td>
				</tr>
<?
			}
		}
	}
	
	if(sizeof($summarylist) > 0)
	{
		for($j=0; $j<sizeof($marketing_list); $j++)
		{
			$adflag_info = $marketing_list[$j]["adflag"];
			
			$sum_totalcount = get_total_stat($summarylist, $adflag_list, $adflag_info, "totalcount");
			$sum_unplaycount = get_total_stat($summarylist, $adflag_list, $adflag_info, "unplaycount");
			$sum_returncount = get_total_stat($summarylist, $adflag_list, $adflag_info, "returncount");
			$sum_paycount = get_total_stat($summarylist, $adflag_list, $adflag_info, "paycount");
			$sum_totalpaycount = get_total_stat($summarylist, $adflag_list, $adflag_info, "totalpaycount");
			$sum_totalcredit = get_total_stat($summarylist, $adflag_list, $adflag_info, "totalcredit");
			
			$sum_totalcredit = round($sum_totalcredit/10, 1);
							
$sum_unplayratio = ($sum_totalcount > 0) ? round($sum_unplaycount * 10000 / $sum_totalcount) / 100 : 0;
			$sum_payratio = ($sum_totalcount > 0) ? round($sum_paycount * 10000 / $sum_totalcount) / 100 : 0;
			$sum_averagecredit = ($sum_totalcount > 0) ? round($sum_totalcredit * 1000 / $sum_totalcount) / 1000 : 0;

?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($j == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($marketing_list)?>" valign="center">Total</td>
<?
				}
?>
					<td class="tdr point"><?= $adflag_info ?></td>
                    <td class="tdr point"><?= number_format($sum_totalcount) ?></td>
                    <td class="tdr point"><?= number_format($sum_unplaycount) ?> (<?= $sum_unplayratio ?> %)</td>
                    <td class="tdr point"><?= number_format($sum_returncount) ?></td>
                    <td class="tdr point"><?= number_format($sum_paycount) ?> (<?= $sum_payratio ?> %)</td>
                    <td class="tdr point"><?= number_format($sum_totalpaycount) ?></td>
                    <td class="tdr point">$<?= number_format($sum_totalcredit) ?> ($<?= $sum_averagecredit ?>)</td>
			</tr>
<?
		}
	}

	$db_main->end();
	$db_main2->end();
?>    
            </tbody>
            </table>
        </div>
        
        </form>
</div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
