<?
    $top_menu = "user_static";
    $sub_menu = "join_leave_stats";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    $search_start_orderdate = $_GET["start_orderdate"];
    $search_end_orderdate = $_GET["end_orderdate"];
	$isearch = $_GET["issearch"];
	$category = ($_GET["category"] == "") ? "1":$_GET["category"];
		
	if ($isearch == "")
	{
		$search_end_createdate  = date("Y-m-d", time() - 60 * 60);
		$search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 14);
	}
	
	$now = time();
	
	if ($search_end_createdate != "")
	{
		$now = strtotime($search_end_createdate);
	}
	else
	{
		$search_end_createdate = date('Y-m-d');
	}

	$db_other = new CDatabase_Other();
    
    $orderby="";
    
    if($category == "2")
    {
    	$orderby="order by reg_date desc";
    }
    
	$sql =  " SELECT reg_date, reg_count, install_count, play_count ".
			" FROM ".   
			" ( ".
			"	SELECT  DATE_FORMAT(reg_date, '%Y-%m-%d') AS reg_date, ".
			"			SUM(reg_count) AS reg_count, ".
			" 			SUM(install_count) AS install_count, ".
			" 			SUM(play_count) AS play_count ".
        	"	 FROM   tbl_user_detail_retention_daily ".
        	" 	 WHERE  '$search_start_createdate' <= reg_date AND reg_date <= '$search_end_createdate' ".
        	"    GROUP  BY Date_format(reg_date, '%Y-%m-%d') ".
        	" ) t1 ".$orderby;
		
    $join_leave_list = $db_other->gettotallist($sql);
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
<?
	if ($category == "1")
	{
?>
	google.load("visualization", "1", {packages:["corechart"]});

	 function drawChart() 
	    {
	        var data1 = new google.visualization.DataTable();
	        data1.addColumn('string', '날짜');
	        data1.addColumn('number', '신규 유입');
	        data1.addColumn('number', '미 인스톨');
	        data1.addColumn('number', '미 게임');
	        data1.addRows([
<?
			foreach ($join_leave_list as $join_leave_listobj){
				$reg_date=$join_leave_listobj["reg_date"];
				$reg_count=$join_leave_listobj["reg_count"];
				$install_count=$join_leave_listobj["install_count"];
				$play_count=$join_leave_listobj["play_count"];
				
				$noplay_count = ($reg_count == 0) ? 0 : ($play_count == 0) ? $reg_count : round($reg_count-$play_count, $add_point);
				$noinstall_count = ($reg_count == 0) ? 0 : ($install_count == 0) ? $reg_count : round($reg_count-$install_count, $add_point);
				
				echo("['".$reg_date."'");
				echo(",{v:".$reg_count.",f:'".number_format($reg_count)."'},{v:".$noinstall_count.",f:'".number_format($noinstall_count)."'},{v:".$noplay_count.",f:'".number_format($noplay_count)."'}],");
				
			}
?>]);
	        
	        var options1 = {
	                width:1050,                       
	                height:480,
	                axisTitlesPosition:'in',
	                curveType:'none',
	                focusTarget:'category',
	                interpolateNulls:'true',
	                legend:'top',
	                fontSize : 12,
	                chartArea:{left:60,top:40,width:1040,height:300}
	            };
	            
	        var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
	        chart.draw(data1, options1);        
	    }
	        
		google.setOnLoadCallback(drawChart);	
<?
	}
?>

	function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;

        search_form.submit();
    }
	
	function tab_change(tab)
	{
		window.location.href = "join_leave_stats.php?category="+tab + "&start_createdate=<?= $search_start_createdate ?>&end_createdate=<?= $search_end_createdate ?>&issearch=1";
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	 <form name="search_form" id="search_form"  method="get" action="join_leave_stats.php">
		<input type=hidden name=issearch value="1"/>
		<input type=hidden name=category value="<?= $category ?>"/>
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 신규유입 이탈</div>
			<div class="search_box">
				<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
			
			<div class="clear"></div>
		</div>
		<!-- //title_warp -->
            
		<div class="search_result">
			<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
		</div>
            
		<ul class="tab">
			<li id="tab_1" class="<?= ($category == "1") ? "select" : "" ?>" onclick="tab_change('1')">그래프</li>
			<li id="tab_2" class="<?= ($category == "2") ? "select" : "" ?>" onclick="tab_change('2')">상세보기</li>
		</ul>
	 </form>
	 
<?
	if ($category == "1")
	{
?>
		<br/><br/>
		<!-- 		<div class="h2_title">[신규유입 이탈 - 표]</div> -->
		<div id="chart_div1" style="height:480px; min-width: 500px"></div>
<?
	}
	else if ($category == "2")
	{
?>
		<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
                <col width="90">
                <col width="150">
				<col width="*">
                <col width="70"> 
			</colgroup>
            <thead>
            	<tr>
	                <th class="tdc" rowspan=2>날짜</th>
	                <th class="tdc" rowspan=2>신규유입자수</th>
	                <th class="tdc" colspan="3">이탈</th>
            	</tr>
            	<tr>
	                <th class="tdc" style="width: 150px;">미인스톨</th>
	                <th class="tdc" style="width: 150px;">미게임</th>
            	</tr>
            </thead>
            <tbody>
<?
			foreach ($join_leave_list as $join_leave_list_obj)
			{
				$reg_date = $join_leave_list_obj["reg_date"];
				$reg_count = $join_leave_list_obj["reg_count"];
				$install_count = $join_leave_list_obj["install_count"];
				$play_count = $join_leave_list_obj["play_count"];
				
				$noplay_count = ($reg_count == 0) ? 0 : ($play_count == 0) ? $reg_count : round($reg_count-$play_count, $add_point);
				$noinstall_count = ($reg_count == 0) ? 0 : ($install_count == 0) ? $reg_count : round($reg_count-$install_count, $add_point);
				
				$install_rate = ($reg_count == 0) ? 0 : round(100-($install_count/$reg_count*100), $add_point);
				$play_rate = ($reg_count == 0) ? 0 : round(100-($play_count/$reg_count*100), $add_point);
				
				$total_noplay_count+=$noplay_count;
				$total_noinstall_count+=$noinstall_count;
				$total_install_count +=$install_count;
				$total_reg_count +=$reg_count;
				$total_play_count+=$play_count;
				
?>            
				<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
                    <td class="tdc point_title"><?=$reg_date?></td>
                    <td class="tdc point"><?=number_format($reg_count)?></td>
                    <td class="tdc point"><?=number_format($noinstall_count)."(".number_format($install_rate)." %)"?></td>
                    <td class="tdc point"><?=number_format($noplay_count)."(".number_format($play_rate)." %)"?></td>
                </tr>

<?
			}
			
			if(sizeof($join_leave_list) == 0)
			{
?>
				   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				   			<td class="tdc" colspan="9">검색 결과가 없습니다.</td>
				   		</tr>
<?
			}
			else 
			{
				$total_install_rate = ($total_reg_count == 0) ? 0 : round(100-($total_install_count/$total_reg_count*100), $add_point);
				$total_play_rate = ($total_reg_count == 0) ? 0 : round(100-($total_play_count/$total_reg_count*100), $add_point);
?>                
				<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
                    <td class="tdc point_title">합계</td>
                    <td class="tdc point"><?=number_format($total_reg_count)?></td>
                    <td class="tdc point"><?=number_format($total_noinstall_count)."(".number_format($total_install_rate)." %)"?></td>
                    <td class="tdc point"><?=number_format($total_noplay_count)."(".number_format($total_play_rate)." %)"?></td>
                </tr>
<?
			}
	}
?>
			</tbody>
		</table>
	</div>
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_other->end();	

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>