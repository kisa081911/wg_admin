<?
    $top_menu = "user_static";
    $sub_menu = "adflag_fbretention_retention";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    $search_start_orderdate = $_GET["start_orderdate"];
    $search_end_orderdate = $_GET["end_orderdate"];
    $search_payday = $_GET["payday"];
    $isearch = $_GET["issearch"];
    $category = ($_GET["search_tab"] == "") ? "0" : $_GET["search_tab"];
    
    if($search_payday != "" && ($search_start_orderdate != "" || $search_end_orderdate != ""))
    	error_back("검색값이 잘못되었습니다.");
    
    if ($isearch == "")
    {
    	$search_end_createdate  = date("Y-m-d", time());
    	$search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 9);
    }
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});

	$(function() {
	    $("#start_orderdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_orderdate").datepicker({ });
	});
	
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	
	    search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="adflag_fbretention_retention.php">
		<input type=hidden name=issearch value="1">
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; Retention 복귀 분석</div>
                <div class="search_box">
                	<select name="search_tab" id="search_tab">
                		<option value="0" <?= ($category=="0") ? "selected" : "" ?>>전체</option>
                		<option value="1" <?= ($category=="1") ? "selected" : "" ?>>Retention1</option>
                		<option value="2" <?= ($category=="2") ? "selected" : "" ?>>Retention2</option>
                		<option value="3" <?= ($category=="3") ? "selected" : "" ?>>Retention3</option>
                		<option value="4" <?= ($category=="4") ? "selected" : "" ?>>Retention4</option>	
                		<option value="5" <?= ($category=="5") ? "selected" : "" ?>>Retention5</option>
                		<option value="20" <?= ($category=="20") ? "selected" : "" ?>>Retention6</option>	
                		<option value="21" <?= ($category=="21") ? "selected" : "" ?>>Retention7</option>	
                		<option value="22" <?= ($category=="22") ? "selected" : "" ?>>Retention8</option>
                		<option value="23" <?= ($category=="23") ? "selected" : "" ?>>Retention9</option>
                		<option value="24" <?= ($category=="24") ? "selected" : "" ?>>Retention10</option>
                		<option value="25" <?= ($category=="25") ? "selected" : "" ?>>Retention11</option>
                		<option value="26" <?= ($category=="26") ? "selected" : "" ?>>Retention12</option>
                		<option value="27" <?= ($category=="27") ? "selected" : "" ?>>Retention13</option>
                		<option value="28" <?= ($category=="28") ? "selected" : "" ?>>Retention14</option>
                		<option value="29" <?= ($category=="29") ? "selected" : "" ?>>Retention15</option>
                		<option value="30" <?= ($category=="30") ? "selected" : "" ?>>Retention16</option>
                		<option value="31" <?= ($category=="31") ? "selected" : "" ?>>Retention17</option>
                		<option value="32" <?= ($category=="32") ? "selected" : "" ?>>Retention18</option>
                		<option value="33" <?= ($category=="33") ? "selected" : "" ?>>Retention19</option>
                		<option value="34" <?= ($category=="34") ? "selected" : "" ?>>Retention20</option>
                		<option value="35" <?= ($category=="35") ? "selected" : "" ?>>Retention21</option>
                		<option value="36" <?= ($category=="36") ? "selected" : "" ?>>Retention22</option>
                		<option value="36" <?= ($category=="37") ? "selected" : "" ?>>Retention23</option>
                		<option value="38" <?= ($category=="38") ? "selected" : "" ?>>Retention24</option>
                		<option value="39" <?= ($category=="39") ? "selected" : "" ?>>Retention25</option>
                		<option value="40" <?= ($category=="40") ? "selected" : "" ?>>Retention26</option>
                		<option value="41" <?= ($category=="41") ? "selected" : "" ?>>Retention27</option>
                		<option value="42" <?= ($category=="42") ? "selected" : "" ?>>Retention28</option>
                		<option value="43" <?= ($category=="43") ? "selected" : "" ?>>Retention29</option>
                		<option value="44" <?= ($category=="44") ? "selected" : "" ?>>Retention30</option>
                		<option value="45" <?= ($category=="45") ? "selected" : "" ?>>Retention31</option>
                		<option value="46" <?= ($category=="46") ? "selected" : "" ?>>Retention32</option>
                		<option value="47" <?= ($category=="47") ? "selected" : "" ?>>Retention33</option>
                		<option value="48" <?= ($category=="48") ? "selected" : "" ?>>Retention34</option>
                		<option value="49" <?= ($category=="49") ? "selected" : "" ?>>Retention35</option>
                		<option value="50" <?= ($category=="50") ? "selected" : "" ?>>Retention36</option>
                		<option value="51" <?= ($category=="51") ? "selected" : "" ?>>Retention37</option>
                		<option value="52" <?= ($category=="52") ? "selected" : "" ?>>Retention38</option>
                		<option value="53" <?= ($category=="53") ? "selected" : "" ?>>Retention39</option>
                		<option value="54" <?= ($category=="54") ? "selected" : "" ?>>Retention40</option>
                		<option value="55" <?= ($category=="55") ? "selected" : "" ?>>Retention41</option>
                		<option value="56" <?= ($category=="56") ? "selected" : "" ?>>Retention42</option>
                		<option value="57" <?= ($category=="57") ? "selected" : "" ?>>Retention43</option>
                		<option value="58" <?= ($category=="58") ? "selected" : "" ?>>Retention44</option>
                		<option value="59" <?= ($category=="59") ? "selected" : "" ?>>Retention45</option>
                		<option value="60" <?= ($category=="60") ? "selected" : "" ?>>Retention46</option>
                		<option value="61" <?= ($category=="61") ? "selected" : "" ?>>Retention47</option>
                		<option value="62" <?= ($category=="62") ? "selected" : "" ?>>Retention48</option>
                		<option value="63" <?= ($category=="63") ? "selected" : "" ?>>Retention49</option>
                		<option value="64" <?= ($category=="64") ? "selected" : "" ?>>Retention50</option>
                		<option value="65" <?= ($category=="65") ? "selected" : "" ?>>Retention51</option>
                		<option value="66" <?= ($category=="66") ? "selected" : "" ?>>Retention52</option>
                		<option value="67" <?= ($category=="67") ? "selected" : "" ?>>Retention53</option>
                		<option value="68" <?= ($category=="68") ? "selected" : "" ?>>Retention54</option>
                		<option value="69" <?= ($category=="69") ? "selected" : "" ?>>Retention55</option>
                		<option value="70" <?= ($category=="70") ? "selected" : "" ?>>Retention56</option>
                		<option value="71" <?= ($category=="71") ? "selected" : "" ?>>Retention57</option>
                		<option value="72" <?= ($category=="72") ? "selected" : "" ?>>Retention58</option>
                		<option value="73" <?= ($category=="73") ? "selected" : "" ?>>Retention59</option>
                		<option value="74" <?= ($category=="74") ? "selected" : "" ?>>Retention60</option>
                		<option value="75" <?= ($category=="75") ? "selected" : "" ?>>Retention61</option>
                		<option value="76" <?= ($category=="76") ? "selected" : "" ?>>Retention62</option>
                		<option value="77" <?= ($category=="77") ? "selected" : "" ?>>Retention63</option>
                		<option value="78" <?= ($category=="78") ? "selected" : "" ?>>Retention64</option>
                		<option value="79" <?= ($category=="79") ? "selected" : "" ?>>Retention65</option>
                		<option value="80" <?= ($category=="80") ? "selected" : "" ?>>Retention66</option>
                		<option value="81" <?= ($category=="81") ? "selected" : "" ?>>Retention67</option>
                		<option value="82" <?= ($category=="82") ? "selected" : "" ?>>Retention68</option>
					</select>&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
                    <input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
                    &nbsp;&nbsp;&nbsp;&nbsp;결제액 기준 : 복귀 후 <input type="text" class="search_text" id="payday" name="payday" style="width:60px" value="<?= $search_payday ?>" onkeypress="search_press(event); return checknum();" />일 이내 결제
                     <input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
                </div>
            </div>
            <!-- //title_warp -->
            <div class="clear"></div>
            <!-- //title_warp -->
            <div class="search_box">
      				<input type="text" class="search_text" id="start_orderdate" name="start_orderdate" value="<?= $search_start_orderdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
      				<input type="text" class="search_text" id="end_orderdate" name="end_orderdate" value="<?= $search_end_orderdate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
      				&nbsp;&nbsp;&nbsp;&nbsp;기간내 결제
      				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
      			</div>
      			<div class="clear"></div>
            <div class="search_result">
                <span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
            </div>
            
            <div id="tab_content_1">
            	<table class="tbl_list_basic1">
		            <colgroup>
		                <col width="">
		                <col width="">
		                <col width="">
		                <col width="">
		                <col width="">
		                <col width="">
		                <col width=""> 
		                <col width=""> 
		            </colgroup>
            		<thead>
			            <tr>
			                <th>유입일</th>
			                <th class="tdr">복귀회원수</th>
			                <th class="tdr">평균복귀일</th>
			                <th class="tdr">Engage회원수</th>
			                <th class="tdr">결제회원수</th>
			                <th class="tdr">총결제 금액</th>
			                <th class="tdr">총결제 횟수</th>
			                <th class="tdr">평균결제 금액</th>
			            </tr>
            		</thead>
            		<tbody>
<?
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();

    $now = time();
    
    if ($search_end_createdate != "")
    {
    	$now = strtotime($search_end_createdate);
    }
    else
    {
    	$search_end_createdate = date('Y-m-d');
    }
    
    if ($search_start_createdate == "")
    {
    	$search_start_createdate = '2015-10-20';
    }
   		
	$sum_totalcount = array();
	$sum_leavedays = array();
	$sum_each_leavedays = array();
	$sum_paycount = array();
	$sum_totalcredit = array();
	$sum_totalcredit_count = array();
	$sum_engagecount = array();
	
	$list_count = 0;
	
	$now2 = $now;
	$now -= 9 * 60 * 60;

    while (true)
    {
    	$today = date("Y-m-d H:i:s", $now);
    	$tomorrow = date("Y-m-d H:i:s", $now + 24 * 60 * 60);

		$today2 = date("Y-m-d", $now2);
    	
    	if ($tomorrow < $search_start_createdate)
    		break;

		if ($today2 < $search_start_createdate)
		{
    		$totalcount = $sum_totalcount[$fbsource];
    		$leavedays = $sum_leavedays[$fbsource];
    		$each_leavedays = $sum_each_leavedays[$fbsource];
			$paycount = $sum_paycount[$fbsource];
			$totalcredit = $sum_totalcredit[$fbsource];
			$totalcredit_count = $sum_totalcredit_count[$fbsource];
			$engagecount = $sum_engagecount[$fbsource];
			$leavedays = "";
			$each_leavedays = "";
		}
		else
		{
			if($category == "0")
				$sub_category = " AND category IN (1, 2, 3, 4, 5, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82) ";
			else
				$sub_category = " AND category='$category' ";
			
			$sql = "SELECT COUNT(*) AS totalcount,IFNULL(AVG(leavedays),0) AS leavedays,IFNULL(SUM(leavedays),0) AS each_leavedays FROM tbl_user_fbretention WHERE writedate>='$today' AND writedate<'$tomorrow' $sub_category";
			$data = $db_main2->getarray($sql);
			
			$totalcount = $data["totalcount"];
			$leavedays = $data["leavedays"];
			$each_leavedays = $data["each_leavedays"];
			
			
			if($category == "0")
				$engagecount = $db_main2->getvalue("SELECT SUM(usercount_engage)+SUM(1weekcount_engage)+SUM(2daycount_engage) FROM tbl_user_retention WHERE adflag IN ('retention', 'retention2', 'retention3', 'retention4', 'retention5', 'retention6', 'retention7', 'retention8', 'retention9', 'retention10', 'retention11', 'retention12', 'retention13', 'retention14', 'retention15', 'retention16', 'retention17', 'retention18', 'retention19', 'retention20', 'retention21', 'retention22', 'retention23', 'retention24', 'retention25', 'retention26', 'retention27', 'retention28', 'retention29', 'retention30', 'retention31', 'retention32', 'retention33', 'retention34', 'retention35', 'retention36', 'retention37', 'retention38', 'retention39', 'retention40', 'retention41', 'retention42', 'retention43', 'retention44', 'retention45', 'retention46', 'retention47', 'retention48', 'retention49', 'retention50', 'retention51', 'retention52', 'retention53', 'retention54', 'retention55', 'retention56', 'retention57', 'retention58', 'retention59', 'retention60', 'retention61', 'retention62', 'retention63', 'retention64', 'retention65', 'retention66', 'retention67', 'retention68') AND retentiondate='$today2'");
			else if ($category == "1")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention' AND retentiondate='$today2'");
			else if ($category == "2")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention2' AND retentiondate='$today2'");
			else if ($category == "3")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention3' AND retentiondate='$today2'");
			else if ($category == "4")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention4' AND retentiondate='$today2'");
			else if ($category == "5")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention5' AND retentiondate='$today2'");
			else if ($category == "20")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention6' AND retentiondate='$today2'");
			else if ($category == "21")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention7' AND retentiondate='$today2'");
			else if ($category == "22")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention8' AND retentiondate='$today2'");
			else if ($category == "23")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention9' AND retentiondate='$today2'");
			else if ($category == "24")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention10' AND retentiondate='$today2'");
			else if ($category == "25")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention11' AND retentiondate='$today2'");
			else if ($category == "26")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention12' AND retentiondate='$today2'");
			else if ($category == "27")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention13' AND retentiondate='$today2'");
			else if ($category == "28")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention14' AND retentiondate='$today2'");
			else if ($category == "29")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention15' AND retentiondate='$today2'");
			else if ($category == "30")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention16' AND retentiondate='$today2'");
			else if ($category == "31")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention17' AND retentiondate='$today2'");
			else if ($category == "32")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention18' AND retentiondate='$today2'");
			else if ($category == "33")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention19' AND retentiondate='$today2'");
			else if ($category == "34")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention20' AND retentiondate='$today2'");
			else if ($category == "35")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention21' AND retentiondate='$today2'");
			else if ($category == "36")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention22' AND retentiondate='$today2'");
			else if ($category == "37")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention23' AND retentiondate='$today2'");
			else if ($category == "38")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention24' AND retentiondate='$today2'");
			else if ($category == "39")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention25' AND retentiondate='$today2'");
			else if ($category == "40")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention26' AND retentiondate='$today2'");
			else if ($category == "41")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention27' AND retentiondate='$today2'");
			else if ($category == "42")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention28' AND retentiondate='$today2'");
			else if ($category == "43")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention29' AND retentiondate='$today2'");
			else if ($category == "44")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention30' AND retentiondate='$today2'");
			else if ($category == "45")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention31' AND retentiondate='$today2'");
			else if ($category == "46")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention32' AND retentiondate='$today2'");
			else if ($category == "47")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention33' AND retentiondate='$today2'");
			else if ($category == "48")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention34' AND retentiondate='$today2'");
			else if ($category == "49")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention35' AND retentiondate='$today2'");
			else if ($category == "50")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention36' AND retentiondate='$today2'");
			else if ($category == "51")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention37' AND retentiondate='$today2'");
			else if ($category == "52")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention38' AND retentiondate='$today2'");
			else if ($category == "53")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention39' AND retentiondate='$today2'");
			else if ($category == "54")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention40' AND retentiondate='$today2'");
			else if ($category == "55")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention41' AND retentiondate='$today2'");
			else if ($category == "56")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention42' AND retentiondate='$today2'");
			else if ($category == "57")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention43' AND retentiondate='$today2'");
			else if ($category == "58")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention44' AND retentiondate='$today2'");
			else if ($category == "59")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention45' AND retentiondate='$today2'");
			else if ($category == "60")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention46' AND retentiondate='$today2'");
			else if ($category == "61")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention47' AND retentiondate='$today2'");
			else if ($category == "62")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention48' AND retentiondate='$today2'");
			else if ($category == "63")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention49' AND retentiondate='$today2'");
			else if ($category == "64")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention50' AND retentiondate='$today2'");
			else if ($category == "65")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention51' AND retentiondate='$today2'");
			else if ($category == "66")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention52' AND retentiondate='$today2'");
			else if ($category == "67")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention53' AND retentiondate='$today2'");
			else if ($category == "68")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention54' AND retentiondate='$today2'");
			else if ($category == "69")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention55' AND retentiondate='$today2'");
			else if ($category == "70")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention56' AND retentiondate='$today2'");
			else if ($category == "71")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention57' AND retentiondate='$today2'");
			else if ($category == "72")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention58' AND retentiondate='$today2'");
			else if ($category == "73")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention59' AND retentiondate='$today2'");
			else if ($category == "74")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention60' AND retentiondate='$today2'");
			else if ($category == "75")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention61' AND retentiondate='$today2'");
			else if ($category == "76")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention62' AND retentiondate='$today2'");
			else if ($category == "77")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention63' AND retentiondate='$today2'");
			else if ($category == "78")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention64' AND retentiondate='$today2'");
			else if ($category == "79")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention65' AND retentiondate='$today2'");
			else if ($category == "80")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention66' AND retentiondate='$today2'");
			else if ($category == "81")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention67' AND retentiondate='$today2'");
			else if ($category == "82")
				$engagecount = $db_main2->getvalue("SELECT usercount_engage+1weekcount_engage+2daycount_engage FROM tbl_user_retention WHERE adflag='retention68' AND retentiondate='$today2'");
			
			if ($engagecount == "")
				$engagecount = "0";
			
			$sql = "SELECT useridx FROM tbl_user_fbretention A WHERE writedate>='$today' AND writedate < '$tomorrow' $sub_category AND NOT EXISTS (SELECT * FROM tbl_user_fbretention WHERE useridx=A.useridx AND writedate<A.writedate AND writedate>DATE_ADD(A.writedate, INTERVAL -30 DAY))";
			$list = $db_main2->gettotallist($sql);
			
			$userlist = "0";
	
	    	for ($i=0; $i<sizeof($list); $i++)
	    	{
				$useridx = $list[$i]["useridx"];
				
				$userlist .= ",$useridx";
			}
			
			$where_add = "";
			
			if ($search_payday != "")
			{
				$where_add = " AND writedate<=DATE_ADD('$today', INTERVAL $search_payday DAY)";
			}
			else if(($search_start_orderdate != "" && $search_end_orderdate != ""))
			{
				$where_add = " AND writedate BETWEEN '$search_start_orderdate 00:00:00' AND '$search_end_orderdate 23:59:59'";
			}
			
			$sql = "SELECT COUNT(DISTINCT useridx) AS paycount, IFNULL(SUM(facebookcredit),0) AS totalcredit, COUNT(useridx) AS totalcredit_count ". 
					"FROM ( ".
					"	SELECT useridx, facebookcredit ".
					"	FROM tbl_product_order WHERE useridx IN ($userlist) AND status=1 AND writedate>='$today' $where_add ".
					") t1";
			
			$data = $db_main->getarray($sql);
			
			$paycount = $data["paycount"];
			$totalcredit = $data["totalcredit"];
			$totalcredit_count = $data["totalcredit_count"];
					
			if ($sum_totalcount[$fbsource] == "")
				$sum_totalcount[$fbsource] = 0;
    			
			if ($sum_leavedays[$fbsource] == "")
				$sum_leavedays[$fbsource] = 0;
			
			if ($sum_each_leavedays[$fbsource] == "")
				$sum_each_leavedays[$fbsource] = 0;
			
			if ($sum_paycount[$fbsource] == "")
				$sum_paycount[$fbsource] = 0;
    			
			if ($sum_totalcredit[$fbsource] == "")
				$sum_totalcredit[$fbsource] = 0;
			
			if ($sum_totalcredit_count[$fbsource] == "")
				$sum_totalcredit_count[$fbsource] = 0;
								
    		$sum_totalcount[$fbsource] += $totalcount;
    		$sum_leavedays[$fbsource] += $leavedays;
    		$sum_each_leavedays[$fbsource] += $each_leavedays;
			$sum_paycount[$fbsource] += $paycount;
			$sum_totalcredit[$fbsource] += $totalcredit;
			$sum_totalcredit_count[$fbsource] += $totalcredit_count;
			$sum_engagecount[$fbsource] += $engagecount;
		}
		
		if($totalcredit != "0")
			$totalcredit = (string)$totalcredit*0.1;
   		
		if ($totalcount == 0)
		{
			$leaveratio = 0;
			$payratio = 0;
			$averagecredit = 0;
			$engageratio = 0;
		}
		else
		{
			$leaveratio = ($list_count == 0) ? 0 : round($sum_leavedays[$fbsource] / $list_count);
			$payratio = ($totalcount == 0) ? 0 : round($paycount * 10000 / $totalcount) / 100;
			$averagecredit = ($totalcount == 0) ? 0 : round($totalcredit * 100 / $totalcount) / 100;
			$engageratio = ($totalcount == 0) ? 0 : round($engagecount * 10000 / $totalcount) / 100;
		}
		
?>
					<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
	                    <td class="tdc point_title" valign="center"><?= $today2 ?></td>
	                    <td class="tdr point"><?= number_format($totalcount) ?></td>
	                    <td class="tdr point"><?= ($leavedays == "") ? number_format($leaveratio) : number_format($leavedays) ?></td>
	                    <td class="tdr point"><?= number_format($engagecount) ?> (<?= $engageratio ?> %)</td>
	                    <td class="tdr point"><?= number_format($paycount) ?> (<?= $payratio ?> %)</td>
	                    <td class="tdr point">$ <?= number_format($totalcredit) ?></td>
	                    <td class="tdr point"><?= number_format($totalcredit_count) ?></td>
	                    <td class="tdr point">$ <?= $averagecredit ?></td>
	                </tr>
<?
    	$now -= 60 * 60 * 24;
		$now2 -= 60 * 60 * 24;
    	
    	$list_count++;
    }

?>
					<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
	                    <td class="tdc point_title" valign="center"><b>Total</b></td>
	                    <td class="tdr point"><?= number_format($sum_totalcount[$fbsource]) ?></td>
	                    <td class="tdr point"><?= ($sum_totalcount[$fbsource] == 0) ? 0 :  number_format(round($sum_each_leavedays[$fbsource] / $sum_totalcount[$fbsource])) ?></td>
	                    <td class="tdr point"><?= number_format($sum_engagecount[$fbsource]) ?> (<?= ($sum_totalcount[$fbsource] == 0) ? 0 : round($sum_engagecount[$fbsource] / $sum_totalcount[$fbsource] * 100, 2) ?> %)</td>
	                    <td class="tdr point"><?= number_format($sum_paycount[$fbsource]) ?> (<?= ($sum_totalcount[$fbsource] == 0) ? 0: round($sum_paycount[$fbsource] * 10000 / $sum_totalcount[$fbsource]) / 100 ?> %)</td>
	                    <td class="tdr point">$ <?= number_format($sum_totalcredit[$fbsource]*0.1) ?></td>
	                    <td class="tdr point"><?= number_format($sum_totalcredit_count[$fbsource]) ?></td>
	                    <td class="tdr point">$ <?= ($sum_totalcount[$fbsource] == 0) ? 0 : round($sum_totalcredit[$fbsource] * 0.1 * 1000 / $sum_totalcount[$fbsource]) / 1000 ?></td>
	                </tr>
<?
	$db_main->end();
	$db_main2->end();
?>    
            	</tbody>
			</table>
		</div>   
	</form>
</div>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
