
<?
    $top_menu = "user_static";
    $sub_menu = "bgu_featuring_stat";
    
    $menu_idx = "300";
    $sub_menu_idx = "33";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $db_main2 = new CDatabase_Main2();
    $db_main = new CDatabase_Main();
    
    $sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, DATE_FORMAT(writedate, '%H') AS `hour`, DATE_FORMAT(writedate, '%Y-%m-%d %H') AS today_hour, COUNT(IF(isnew = 1, 1, NULL)) AS new_user_cnt, COUNT(IF(isnew = 0, 1, NULL)) AS retention_user_cnt, SUM(leavedays) AS leavedays, COUNT(useridx) AS user_cnt  
			FROM `tbl_user_featured` 
    	 	where writedate >= '2017-05-25 07:00:00'
    		AND writedate <= '2017-05-31 07:00:00'
			GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), DATE_FORMAT(writedate, '%H')
    		ORDER BY today_hour DESC "; 
    $bgu_featuring_list = $db_main2->gettotallist($sql);
	
	$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, DATE_FORMAT(writedate, '%H') AS `hour`, DATE_FORMAT(writedate, '%Y-%m-%d %H') AS today_hour, COUNT(IF(isnew = 1, 1, NULL)) AS new_user_cnt, COUNT(IF(isnew = 0, 1, NULL)) AS retention_user_cnt, SUM(leavedays) AS leavedays, COUNT(useridx) AS user_cnt  
			FROM `tbl_user_featured` 
    	 	where writedate >= '2017-12-14 07:00:00'
			GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), DATE_FORMAT(writedate, '%H')
    		ORDER BY today_hour DESC "; 
    $new_bgu_featuring_list = $db_main2->gettotallist($sql);
    
    $sql = " SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS DAY,COUNT(useridx) AS COUNT FROM tbl_user_ext WHERE createdate >= '2017-05-25 07:00:00' AND createdate < '2017-05-31 07:00:00' ".
		   " AND fbsource IN('bgl_featured','appcenter','appcenter_featured','canvas_recommended','sidebar_recommended','gameroom') ".
// 		   " AND fbsource LIKE '%bgl_featured%' ".
		   " GROUP BY DATE_FORMAT(createdate, '%Y-%m-%d')";
    $bgu_featuring_history = $db_main->gettotallist($sql);
    
    $sql="SELECT DATE_FORMAT(createdate, '%Y-%m-%d')AS today ,DATE_FORMAT(createdate, '%H')AS hour, COUNT(useridx) AS count FROM tbl_user_ext 
			where createdate >= '2017-12-14 07:00:00'
			AND fbsource IN('bgl_featured','appcenter','appcenter_featured','canvas_recommended','sidebar_recommended','gameroom')
			GROUP BY DATE_FORMAT(createdate, '%Y-%m-%d'), DATE_FORMAT(createdate, '%H')
			ORDER BY today DESC, `hour` ASC";
    $bgu_featuring_joinaddlist = $db_main->gettotallist($sql);
     
    $rowspan_list = array();
    $rowspan_index = 0;
    $rowspan_cnt = 0;
    
    $today_tmp = $new_bgu_featuring_list[0]["today"];
    
    for($i=0; $i<sizeof($new_bgu_featuring_list); $i++)
    {
    	if($today_tmp != $new_bgu_featuring_list[$i]["today"] || $i == sizeof($new_bgu_featuring_list) - 1)
    	{
    		if($i == sizeof($new_bgu_featuring_list) - 1)
    			$rowspan_cnt++;
    		
    		$rowspan_list[$rowspan_index++] = $rowspan_cnt;
    		$rowspan_cnt = 0;
    	}
    	
    	$today_tmp = $new_bgu_featuring_list[$i]["today"];
    	$rowspan_cnt++;
    }
	
	
	$beforehour_list = array();
	$beforenewusercount_list = array();
	$beforeretentionusercount_list = array();
	$nowhour_list = array();
	$nownewusercount_list = array();
	$nowretentionusercount_list = array();
	
	for($i=0;$i<sizeof($bgu_featuring_list);$i++)
	{
		$beforehour_list[$i] = $bgu_featuring_list[$i]["today_hour"];
		$beforenewusercount_list[$i] = $bgu_featuring_list[$i]["new_user_cnt"];
		$beforeretentionusercount_list[$i] = $bgu_featuring_list[$i]["retention_user_cnt"];
	}
	
	for($i=0;$i<sizeof($new_bgu_featuring_list);$i++)
	{
		$nowhour_list[$i] = $new_bgu_featuring_list[$i]["today_hour"];
		$nownewusercount_list[$i] = $new_bgu_featuring_list[$i]["new_user_cnt"];
		$nowretentionusercount_list[$i] = $new_bgu_featuring_list[$i]["retention_user_cnt"];
	}
	
	$db_main2->end();
    $db_main->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});

	function drawChart() 
	{
		var dataBeforuser = new google.visualization.DataTable();
        
		dataBeforuser.addColumn('string', '시간');
		dataBeforuser.addColumn('number', '신규 유저수');
		dataBeforuser.addColumn('number', '복귀 유저수');
		dataBeforuser.addRows([
<?
		
		for ($i=sizeof($beforehour_list); $i>0; $i--)
	    {
	    	$hour = $beforehour_list[$i-1];
	    	$beforenewusercount = $beforenewusercount_list[$i-1];
	    	$beforeretentionusercount = $beforeretentionusercount_list[$i-1];
	    	
	        echo("['".$hour."'");
	        
	        if ($beforenewusercount != "")
	            echo(",{v:".$beforenewusercount.",f:'".make_price_format($beforenewusercount)."'}");
	        else
	            echo(",0");
	        
	        if ($beforeretentionusercount != "")
	            echo(",{v:".$beforeretentionusercount.",f:'".make_price_format($beforeretentionusercount)."'}]");
	        else
	            echo(",0]");
	        
	        if ($i > 1)
				echo(",");
	    }
?>      
        ]);
		
		var dataNowuser = new google.visualization.DataTable();
        
		dataNowuser.addColumn('string', '시간');
		dataNowuser.addColumn('number', '신규 유저수');
		dataNowuser.addColumn('number', '복귀 유저수');
		dataNowuser.addRows([
<?
		
		for ($i=sizeof($nowhour_list); $i>0; $i--)
	    {
	    	$hour = $nowhour_list[$i-1];
	    	$nownewusercount = $nownewusercount_list[$i-1];
	    	$nowretentionusercount = $nowretentionusercount_list[$i-1];
	    	
	        echo("['".$hour."'");
	        
	        if ($nownewusercount != "")
	            echo(",{v:".$nownewusercount.",f:'".make_price_format($nownewusercount)."'}");
	        else
	            echo(",0");
	        
	        if ($nowretentionusercount != "")
	            echo(",{v:".$nowretentionusercount.",f:'".make_price_format($nowretentionusercount)."'}]");
	        else
	            echo(",0]");
	        
	        if ($i > 1)
				echo(",");
	    }
?>      
        ]);
		
		
		var options_user = {
                title:'',                                                      
                width:1050,                         
                height:200,
                axisTitlesPosition:'in',
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize : 12,
                colors: ['#0000CD', '#008000', '#D2691E', '#C0C0C0', '#FFD700'],
                chartArea:{left:80,top:40,width:1020,height:130}
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_divBeforeUser'));
        chart.draw(dataBeforuser, options_user); 

		chart = new google.visualization.LineChart(document.getElementById('chart_divNowUser'));
        chart.draw(dataNowuser, options_user);
	}
	
	google.setOnLoadCallback(drawChart);
</script>
	<!-- CONTENTS WRAP -->
	<div class="contents_wrap">
		<!-- title_warp -->
	    <div class="title_wrap">
	    	<div class="title"><?= $top_menu_txt ?> &gt; BGU 피쳐링 현황</div>
	    </div>
	    <!-- //title_warp -->
	    		<div class="search_result">이전 데이터 (일자 별 유입 수) fbsource - [bgl_featured, appcenter, appcenter_featured, canvas_recommended, sidebar_recommended, gameroom]</div>
		
	    <div id="tab_content_1">
		    <table class="tbl_list_basic1" width="200">
			    <tbody>
			    <tr class="" onmouseover="className='tr_over'" onmouseout="className=''" style="border-top:1px double;">
						<td class="tdc point_title">날짜</td>
<?
	
	for($i=0; $i<sizeof($bgu_featuring_history); $i++)
	{
		$today = $bgu_featuring_history[$i]["DAY"];
?>
					
						<td class="tdc point"><?= $today ?></td>
<?
	}
?>
						<td class="tdc point_title" valign="center" style="border-right: 1px dotted #dbdbdb;"><b>Total</b></td>
					</tr>
					<tr class="" onmouseover="className='tr_over'" onmouseout="className=''" style="border-top:1px double;">
						<td class="tdc point_title" >유입 수</td>
<?
	$history_total_user_cnt = 0;
	
	for($i=0; $i<sizeof($bgu_featuring_history); $i++)
	{
		$user_cnt = $bgu_featuring_history[$i]["COUNT"];
	    
		$history_total_user_cnt += $user_cnt;
?>
					
						<td class="tdc point" ><?= number_format($user_cnt) ?>&nbsp;</td>
<?
	}
?>
						<td class="tdc point" colspan="<?=sizeof($bgu_featuring_history)?>"><?= number_format($history_total_user_cnt) ?>&nbsp;</td>
					</tr>
			    </tbody>
	        </table>
		</div>    
	    </br>
	    </br>
	    <div class="clear"></div>
		
		<div class="h2_title">[BGL 회원수 - 이전 데이터]</div>
		<div id="chart_divBeforeUser" style="height:230px; min-width: 500px"></div>
		
		<div class="clear"></div>
		
		<div class="h2_title">[BGL 회원수 - 2017.12.14 ~ 2017.12.25]</div>
		<div id="chart_divNowUser" style="height:230px; min-width: 500px"></div>
		
		<div class="clear"></div>
	     
	    <div id="tab_content_1">
		    <table class="tbl_list_basic1">
			    <colgroup>
			        <col width="">
			        <col width="60">
			        <col width="">
			        <col width="">
			        <col width="">
			        <col width="">
			    </colgroup>
			    <thead>
				    <tr>
				        <th>날짜</th>
				        <th style="border-right: 1px dotted #dbdbdb;">시간</th>
				        <th class="tdr" style="border-right: 1px dotted #dbdbdb;">신규 유저 수</th>
				        <th class="tdr" style="border-right: 1px dotted #dbdbdb;">복귀 유저 수</th>
				        <th class="tdr" style="border-right: 1px dotted #dbdbdb;">평균 복귀일</th>
				        <th class="tdr" style="border-right: 1px dotted #dbdbdb;">총 유저 수</th>
				    </tr>
			    </thead>
			    <tbody>
<?
	$total_new_user_cnt = 0;
	$total_retention_user_cnt = 0;
	$total_user_cnt = 0;
	$total_leavedays = 0;
	
	$day_new_user_cnt = 0;
	$day_retention_user_cnt = 0;
	$day_user_cnt = 0;
	$day_leavedays = 0;
	
	$rowspan_index = 0;
	$today_tmp = $new_bgu_featuring_list[0]["today"];
	
	for($i=0; $i<sizeof($new_bgu_featuring_list); $i++)
	{
		$today = $new_bgu_featuring_list[$i]["today"];
		$hour = $new_bgu_featuring_list[$i]["hour"];
		$new_user_cnt = $new_bgu_featuring_list[$i]["new_user_cnt"];
		$retention_user_cnt = $new_bgu_featuring_list[$i]["retention_user_cnt"];
		$user_cnt = $new_bgu_featuring_list[$i]["user_cnt"];
		$leavedays = $new_bgu_featuring_list[$i]["leavedays"];
		$new_user_cnt2 = $bgu_featuring_joinaddlist[$i]["count"];
		
		$day_new_user_cnt += $new_user_cnt;
		//$day_new_user_cnt += $new_user_cnt2;
		$day_retention_user_cnt += $retention_user_cnt;
		$day_leavedays += $leavedays;
		$day_user_cnt += $user_cnt;
		
		$total_new_user_cnt += $new_user_cnt;
		//$total_new_user_cnt += $new_user_cnt2;
		$total_retention_user_cnt += $retention_user_cnt;
		$total_leavedays += $leavedays;
		$total_user_cnt += $user_cnt;
		
		if($today_tmp != $new_bgu_featuring_list[$i]["today"] || $i == 0)
		{
?>
					<tr class="" onmouseover="className='tr_over'" onmouseout="className=''" style="border-top:1px double;">
						<td class="tdc point" rowspan="<?= $rowspan_list[$rowspan_index++] ?>"><?= $today ?></td>
<?
		}
		else
		{
?>
					<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
		}
?>
						<td class="tdc point" style="border-right: 1px dotted #dbdbdb;"><?= $hour ?> 시</td>
						<td class="tdr" style="border-right: 1px dotted #dbdbdb;"><?= number_format(($new_user_cnt))?>&nbsp;</td>
						<td class="tdr" style="border-right: 1px dotted #dbdbdb;"><?= number_format($retention_user_cnt) ?>&nbsp;</td>
						<td class="tdr" style="border-right: 1px dotted #dbdbdb;"><?= ($retention_user_cnt == 0) ? 0 : number_format($leavedays / $retention_user_cnt, 2) ?>&nbsp;</td>
						<td class="tdr" style="border-right: 1px dotted #dbdbdb;"><?= number_format($user_cnt) ?>&nbsp;</td>						
					</tr>
<?
		$today_tmp = $new_bgu_featuring_list[$i]["today"];
		
		if($today_tmp != $new_bgu_featuring_list[$i+1]["today"])
		{
?>
					<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
						<td class="tdc point" colspan="2" style="border-right: 1px dotted #dbdbdb;">Total</td>
						<td class="tdr point" style="border-right: 1px dotted #dbdbdb;"><?= number_format($day_new_user_cnt) ?>&nbsp;</td>
						<td class="tdr point" style="border-right: 1px dotted #dbdbdb;"><?= number_format($day_retention_user_cnt) ?>&nbsp;</td>
						<td class="tdr point" style="border-right: 1px dotted #dbdbdb;"><?= ($day_retention_user_cnt == 0) ? 0 : number_format($day_leavedays / $day_retention_user_cnt, 2) ?>&nbsp;</td>
						<td class="tdr point" style="border-right: 1px dotted #dbdbdb;"><?= number_format($day_user_cnt) ?>&nbsp;</td>
					</tr>
<?
			$day_new_user_cnt = 0;
			$day_retention_user_cnt = 0;
			$day_leavedays = 0;
			$day_user_cnt = 0;
		}
	}
?>
					<tr class="" onmouseover="className='tr_over'" onmouseout="className=''" style="border-top:1px double;">
						<td class="tdc point_title" valign="center" colspan="2" style="border-right: 1px dotted #dbdbdb;"><b>Total</b></td>
						<td class="tdr point" style="border-right: 1px dotted #dbdbdb;"><?= number_format($total_new_user_cnt) ?>&nbsp;</td>
						<td class="tdr point" style="border-right: 1px dotted #dbdbdb;"><?= number_format($total_retention_user_cnt) ?>&nbsp;</td>
						<td class="tdr point" style="border-right: 1px dotted #dbdbdb;"><?= ($total_retention_user_cnt == 0) ? 0 : number_format($total_leavedays / $total_retention_user_cnt, 2) ?>&nbsp;</td>
						<td class="tdr point" style="border-right: 1px dotted #dbdbdb;"><?= number_format($total_user_cnt) ?>&nbsp;</td>
					</tr>
			    </tbody>
	        </table>
		</div>    
	</div>
</div>
<!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>