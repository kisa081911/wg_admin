<?
    $top_menu = "user_static";
    $sub_menu = "adflag_detail_analysis";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $search_adflag = $_GET["adflag"];
    $search_countrycode = $_GET["countrycode"];
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    $search_start_orderdate = $_GET["start_orderdate"];
    $search_end_orderdate = $_GET["end_orderdate"];
    $search_payday = $_GET["payday"];
	$isearch = $_GET["issearch"];
	$select_time = $_GET["select_time"];
	
	if($search_payday != "" && ($search_start_orderdate != "" || $search_end_orderdate != ""))
		error_back("검색값이 잘못되었습니다.");
		
	if ($isearch == "")
	{
		$search_end_createdate  = date("Y-m-d", time() - 60 * 60);
		$search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 6);
	}
	
	if ($select_time == "")
	{
		$select_time = "marketing";
	}
	
	function get_stat($summarylist, $sex, $country, $today, $property)
	{
		$stat = 0;
		
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if ($summarylist[$i]["day"] == $today || $today == "")
			{
				if ($summarylist[$i]["sex"] == $sex || $sex == "")
				{
					if ($summarylist[$i]["country"] == $country || $country == "" || $country == "unknown" && $summarylist[$i]["country"] == "")
					{
						$stat += $summarylist[$i][$property];
					}
				}
			}
		}	
		
		return $stat;
	}
	
	function get_stat2($summarylist, $sex, $startage, $endage, $property)
	{
		$stat = 0;
		
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if ($summarylist[$i]["sex"] == $sex || $sex == "")
			{
				$stat += $summarylist[$i][$property];
			}
		}	
		
		return $stat;
	}	
	
	$now = time();
	
	if ($search_end_createdate != "")
	{
		$now = strtotime($search_end_createdate);
	}
	else
	{
		$search_end_createdate = date('Y-m-d');
	}
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
    $db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT codename,fullname FROM country_code ORDER BY fullname";
	$country_list = $db_analysis->gettotallist($sql);
	
	$sql = "SELECT adflag FROM tbl_adflag_flag";
	$adflag_list = $db_main2->gettotallist($sql);
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});

	$(function() {
	    $("#start_orderdate").datepicker({ });
	});

	$(function() {
	    $("#end_orderdate").datepicker({ });
	});

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	
	    search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="adflag_detail_analysis.php">
		<input type=hidden name=issearch value="1"/>
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 유입경로 유입 상세분석</div>
			<div class="search_box">
				시간 기준 :
				<input type="radio" value="marketing" name="select_time" id="select_time" <?= ($select_time == "marketing") ? "checked=\"true\"" : ""?>/> 마케팅
				<input type="radio" value="server" name="select_time" id="select_time" <?= ($select_time == "server") ? "checked=\"true\"" : ""?>/> 서버
			</div>
			<div class="clear"></div>	
			<div class="search_box">
				<select id="adflag" name="adflag">
					<option value="">유입경로 선택</option>
<?
				for($i = 0; $i < sizeof($adflag_list); $i++)
				{
						$adflag_info = $adflag_list[$i]["adflag"];
				
?>
					<option value="<?=$adflag_info?>" <?= ($search_adflag == "$adflag_info") ? "selected" : "" ?>><?=$adflag_info?></option>					
<?
				}
?>
					<!--<option value="fbself6" <?= ($search_adflag == "fbself6") ? "selected" : "" ?>>fbself6</option>
					<option value="maudau" <?= ($search_adflag == "maudau") ? "selected" : "" ?>>maudau</option>
					<option value="socialclicks" <?= ($search_adflag == "socialclicks") ? "selected" : "" ?>>socialclicks</option>					
					<option value="wbingo_bottom" <?= ($search_adflag == "wbingo_bottom") ? "selected" : "" ?>>wbingo_bottom</option>
					<option value="wbingo_popup" <?= ($search_adflag == "wbingo_popup") ? "selected" : "" ?>>wbingo_popup</option>					
					<option value="wcasino_popup_new" <?= ($search_adflag == "wcasino_popup_new") ? "selected" : "" ?>>wcasino_popup_new</option>
					<option value="cr_dub_noreward" <?= ($search_adflag == "cr_dub_noreward") ? "selected" : "" ?>>cr_dub_noreward</option>
					<option value="cr_dub_noreward_pay" <?= ($search_adflag == "cr_dub_noreward_pay") ? "selected" : "" ?>>cr_dub_noreward_pay</option>
					<option value="cr_dub_reward" <?= ($search_adflag == "cr_dub_reward") ? "selected" : "" ?>>cr_dub_reward</option>
					<option value="cr_dub_reward_pay" <?= ($search_adflag == "cr_dub_reward_pay") ? "selected" : "" ?>>cr_dub_reward_pay</option>
					<option value="cr_duc_noreward" <?= ($search_adflag == "cr_duc_noreward") ? "selected" : "" ?>>cr_duc_noreward</option>
					<option value="cr_duc_noreward_pay" <?= ($search_adflag == "cr_duc_noreward_pay") ? "selected" : "" ?>>cr_duc_noreward_pay</option>
					<option value="cr_duc_reward" <?= ($search_adflag == "cr_duc_reward") ? "selected" : "" ?>>cr_duc_reward</option>
					<option value="cr_duc_reward_pay" <?= ($search_adflag == "cr_duc_reward_pay") ? "selected" : "" ?>>cr_duc_reward_pay</option>
					<option value="socialclicks" <?= ($search_adflag == "socialclicks") ? "selected" : "" ?>>socialclicks</option>
					<option value="wcasino_popup" <?= ($search_adflag == "wcasino_popup") ? "selected" : "" ?>>wcasino_popup</option>                		
					<option value="wcasino_bottom" <?= ($search_adflag == "wcasino_bottom") ? "selected" : "" ?>>wcasino_bottom</option>                		
					<option value="wcasino_ad" <?= ($search_adflag == "wcasino_ad") ? "selected" : "" ?>>wcasino_ad</option>
					<option value="wsoli_top" <?= ($search_adflag == "wsoli_top") ? "selected" : "" ?>>wsoli_top</option>
					<option value="invite_event" <?= ($search_adflag == "invite_event") ? "selected" : "" ?>>invite_event</option>
					<option value="user_share" <?= ($search_adflag == "user_share") ? "selected" : "" ?>>user_share</option>
					<option value="event" <?= ($search_adflag == "event") ? "selected" : "" ?>>event</option>
					<option value="fanpage" <?= ($search_adflag == "fanpage") ? "selected" : "" ?>>fanpage</option>
					<option value="share_friend_lv" <?= ($search_adflag == "share_friend_lv") ? "selected" : "" ?>>share_friend_lv</option>
					<option value="share_bingo" <?= ($search_adflag == "share_bingo") ? "selected" : "" ?>>share_bingo</option>
					<option value="howtoplay" <?= ($search_adflag == "howtoplay") ? "selected" : "" ?>>howtoplay</option>
					<option value="all" <?= ($search_adflag == "all") ? "selected" : "" ?>>유입경로전체</option>   -->               		
				</select>
				<select id="countrycode" name="countrycode">
					<option value="">국가 선택</option>
<?
	for ($i=0; $i<sizeof($country_list); $i++)
	{
		$codename = $country_list[$i]["codename"];
		$fullname = $country_list[$i]["fullname"];
?>		
					<option value="<?= $codename ?>" <?= ($search_countrycode == $codename) ? "selected" : "" ?>><?= $fullname ?></option>
<?
	}
?>
				</select>
				<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
					&nbsp;&nbsp;&nbsp;&nbsp;결제액 기준 : 가입 후 <input type="text" class="search_text" id="payday" name="payday" style="width:60px" value="<?= $search_payday ?>" onkeypress="search_press(event); return checknum();" />일 이내 결제
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
			
			<div class="clear"></div>
                
			<div class="search_box">
				<input type="text" class="search_text" id="start_orderdate" name="start_orderdate" value="<?= $search_start_orderdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_orderdate" name="end_orderdate" value="<?= $search_end_orderdate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
				&nbsp;&nbsp;&nbsp;&nbsp;기간내 결제
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
			
		</div>
		<!-- //title_warp -->
	</form>
<?
	if ($search_adflag != "")
	{
?>		
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	<div class="search_result">
		1. 일자별 유입 현황
	</div>
	
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
                <col width="">
                <col width=""> 
                <col width=""> 
            </colgroup>
            <thead>
	            <tr>
	                <th>유입일</th>
	                <th class="tdr">회원가입수</th>
	                <th class="tdr">미게임회원수</th>
	                <th class="tdr">2주이탈복귀</th>
	                <th class="tdr">결제회원수</th>
	                <th class="tdr">총결제 credit<br>(평균)</th>
	            </tr>
			</thead>
			<tbody>
<?
	if ($search_adflag == "viral")
		$adflag = "";
	else if ($search_adflag == "maudau")
		$adflag = "maudau%";
	else if ($search_adflag == "all")
		$adflag = "%";
	else
		$adflag = $search_adflag;
		
	$sql = "SELECT sex,country, ";
	
	if($select_time == "marketing")
		$sql .= "DATE_FORMAT(date_add(tbl_user_ext.createdate, interval 9 hour),'%Y-%m-%d') AS day, ";				 
	else if($select_time == "server")
		$sql .= "DATE_FORMAT(tbl_user_ext.createdate,'%Y-%m-%d') AS day, ";	
	
	$sql .= "COUNT(*) AS totalcount,".
			"ABS(IFNULL(SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			",0)) AS unplaycount,";
		
	if ($search_payday != "")
	{
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0)) AS paycount,".
				"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0))AS totalcredit ";
	}
	else if(($search_start_orderdate != "" && $search_end_orderdate != ""))
	{
		$search_start_orderdate_minute = $search_start_orderdate." 00:00:00";
		$search_end_orderdate_minute = $search_end_orderdate." 23:59:59";	
		
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute') THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute') THEN 1 ELSE 0 END),0)) AS paycount,".
				"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0)) AS totalcredit ";
				
	}
	else
	{
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0)) AS paycount,".
				"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1)),0)) AS totalcredit ";
	}
		
	if($select_time == "marketing")
	{
		$sdate = date('Y-m-d H:i:s', strtotime($search_start_createdate) - 60 * 60 * 9); //(마케팅)-UTC
		$tail = "tbl_user_ext.createdate>='$sdate' AND tbl_user_ext.createdate<='$search_end_createdate 14:59:59'";
	}
	else if($select_time == "server")
	{
		$sdate = date('Y-m-d H:i:s', strtotime($search_start_createdate)); // 서버 시간
		$tail = "tbl_user_ext.createdate>='$sdate' AND tbl_user_ext.createdate<='$search_end_createdate 23:59:59'";
	}
	
	$sql .= " FROM tbl_user_ext JOIN tbl_user ON tbl_user_ext.useridx=tbl_user.useridx WHERE ".$tail." AND adflag LIKE '$adflag' ";
	
	if ($search_countrycode != "")
		$sql .= " AND country='$search_countrycode' ";
	
	if($select_time == "marketing")
	{
		$group_date = "DATE_FORMAT(date_add(tbl_user_ext.createdate, INTERVAL 9 HOUR),'%Y-%m-%d')"; //(마케팅)-UTC
	
	}
	else if($select_time == "server")
	{
		$group_date = "DATE_FORMAT(tbl_user_ext.createdate, '%Y-%m-%d')"; // 서버 시간
	}
	
	$sql .= " GROUP BY ".$group_date.",sex,country";
	$summarylist = $db_main->gettotallist($sql);
	
    while (true)
    {
    	$today = date("Y-m-d", $now);
    	$tomorrow = date("Y-m-d", $now + 24 * 60 * 60);
    	
    	if ($today < $search_start_createdate)
    		break;
    		
		$returncount = $db_main2->getvalue("SELECT IFNULL(SUM(usercount),0) FROM tbl_user_retention WHERE adflag LIKE '$adflag' AND retentiondate='$today'");
		$totalcount = get_stat($summarylist, "", "", $today, "totalcount");
		$unplaycount = get_stat($summarylist, "", "", $today, "unplaycount");
		$paycount = get_stat($summarylist, "", "", $today, "paycount");
		$totalcredit = get_stat($summarylist, "", "", $today, "totalcredit");
   		
		if ($totalcount == 0)
		{
			$unplayratio = 0;
			$payratio = 0;
			$averagecredit = 0;
		}
		else
		{
			$unplayratio = round($unplaycount * 10000 / $totalcount) / 100;
			$payratio = round($paycount * 10000 / $totalcount) / 100;
			$averagecredit = round($totalcredit * 1000 / $totalcount) / 1000;
		}
?>
				<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
					<td class="tdc point_title" valign="center"><?= $today ?></td>
					<td class="tdr point"><?= number_format($totalcount) ?></td>
					<td class="tdr point"><?= number_format($unplaycount) ?> (<?= $unplayratio ?> %)</td>
					<td class="tdr point"><?= number_format($returncount) ?></td>
					<td class="tdr point"><?= number_format($paycount) ?> (<?= $payratio ?> %)</td>
					<td class="tdr point"><?= number_format($totalcredit) ?> (<?= $averagecredit ?>)</td>
				</tr>
<?
    	$now -= 60 * 60 * 24;
    }
?>    
			</tbody>
		</table>
	</div>
	<br><br>
	<div class="search_result">
		2. 국가별, 성별 유입 현황 (Top 100 국가)
	</div>
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
            </colgroup>
            <thead>
	            <tr>
	                <th>국가</th>
	                <th>성별</th>
	                <th class="tdr">회원가입수</th>
	                <th class="tdr">미게임회원수</th>
	                <th class="tdr">결제회원수</th>
	                <th class="tdr">총결제 credit<br>(평균)</th>
	            </tr>
            </thead>
            <tbody>
<?
	if ($search_countrycode != "")
		$list = $db_main->gettotallist("SELECT country FROM tbl_user_ext WHERE adflag LIKE '$adflag' AND country='$search_countrycode' AND createdate>='$search_start_createdate' AND createdate<='$search_end_createdate 23:59:59' GROUP BY country ORDER BY COUNT(*) DESC LIMIT 100");
	else
		$list = $db_main->gettotallist("SELECT country FROM tbl_user_ext WHERE adflag LIKE '$adflag' AND createdate>='$search_start_createdate' AND createdate<='$search_end_createdate 23:59:59' GROUP BY country ORDER BY COUNT(*) DESC LIMIT 100");
	
    for ($i=0; $i<sizeof($list); $i++)
    {
    	$country = $list[$i]["country"];
		
		$fullname = $db_analysis->getvalue("SELECT fullname FROM country_code WHERE codename='$country'");
		
		if ($fullname == "")
			$fullname = $country;
		
		if ($country == "")
			$country = "unknown";
			
		for ($j=0; $j<2; $j++)
		{
			$totalcount = get_stat($summarylist, $j+1, $country, "", "totalcount");
			$unplaycount = get_stat($summarylist, $j+1, $country, "", "unplaycount");
			$paycount = get_stat($summarylist, $j+1, $country, "", "paycount");
			$totalcredit = get_stat($summarylist, $j+1, $country, "", "totalcredit");
			
			if ($totalcount == 0)
			{
				$unplayratio = 0;
				$payratio = 0;
				$averagecredit = 0;
			}
			else
			{
				$unplayratio = round($unplaycount * 10000 / $totalcount) / 100;
				$payratio = round($paycount * 10000 / $totalcount) / 100;
				$averagecredit = round($totalcredit * 1000 / $totalcount) / 1000;
			}
			
?>
                <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
			if ($j == 0)
			{
?>
                    <td class="tdc point_title" valign="center" rowspan=2><?= ($fullname == "") ? "unknown" : $fullname ?></td>
<?				
			}
?>
                    <td class="tdc point_title" valign="center"><?= ($j == 0) ? "Male" : "Female" ?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>
                    <td class="tdr point"><?= number_format($unplaycount) ?> (<?= $unplayratio ?> %)</td>
                    <td class="tdr point"><?= number_format($paycount) ?> (<?= $payratio ?> %)</td>
                    <td class="tdr point"><?= number_format($totalcredit) ?> (<?= $averagecredit ?>)</td>
            	</tr>
<?
		}
    }
?>    
            </tbody>
		</table>
	</div>   
	<br><br>
	<div class="search_result">
		3. 성별 유입 현황
	</div>
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
            </colgroup>
            <thead>
	            <tr>
	                <th>성별</th>
	                <th class="tdr">회원가입수</th>
	                <th class="tdr">미게임회원수</th>
	                <th class="tdr">결제회원수</th>
	                <th class="tdr">총결제 credit<br>(평균)</th>
	            </tr>
			</thead>
			<tbody>
<?
    for ($i=0; $i<2; $i++)
    {
    	$sex = $i+1;
		
		$totalcount = get_stat($summarylist, $sex, "", "", "totalcount");
		$unplaycount = get_stat($summarylist, $sex, "", "", "unplaycount");
		$paycount = get_stat($summarylist, $sex, "", "", "paycount");
		$totalcredit = get_stat($summarylist, $sex, "", "", "totalcredit");
			
		if ($totalcount == 0)
		{
			$unplayratio = 0;
			$payratio = 0;
			$averagecredit = 0;
		}
		else
		{
			$unplayratio = round($unplaycount * 10000 / $totalcount) / 100;
			$payratio = round($paycount * 10000 / $totalcount) / 100;
			$averagecredit = round($totalcredit * 1000 / $totalcount) / 1000;
		}
?>
				<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
					<td class="tdc point_title" valign="center"><?= ($sex == 1) ? "Male" : "Female" ?></td>
					<td class="tdr point"><?= number_format($totalcount) ?></td>
					<td class="tdr point"><?= number_format($unplaycount) ?> (<?= $unplayratio ?> %)</td>
					<td class="tdr point"><?= number_format($paycount) ?> (<?= $payratio ?> %)</td>
					<td class="tdr point"><?= number_format($totalcredit) ?> (<?= $averagecredit ?>)</td>
				</tr>
<?
    }
?>    
			</tbody>
		</table>
	</div> 
<?
	}
	else
	{
?>   
	<div class="search_result">
                유입경로를 선택해주세요.
	</div>
<?
	}
?> 
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main->end();
	$db_main2->end();
	$db_analysis->end();

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>