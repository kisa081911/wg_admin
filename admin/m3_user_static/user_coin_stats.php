<?
    $top_menu = "user_static";
    $sub_menu = "user_coin_stats";

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $startdate = $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d", time()) : $_GET["enddate"];
    $term = "1440";
    $pagename = "user_coin_stats.php";
    
    if ($term != "1440")
        error_back("잘못된 접근입니다.");
    
    check_xss($startdate.$enddate);
    
    // 최근 30일
    if ($startdate == "")
        $startdate = date("Y-m-d", mktime(0,0,0,date("m", strtotime($enddate)),(date("d", strtotime($enddate))-30),date("Y",strtotime($enddate))));
        
    $tail = " WHERE today >= '$startdate 00:00:00' AND today <= '$enddate 23:59:59' "; 
    $group_by = " GROUP BY today"; 
    
    $db_analysis = new CDatabase_Analysis();
    $db_other = new CDatabase_Other();

    $sql = "SELECT today,".
        "SUM(IF(type=1 AND subtype=1,coin,0)) AS alluser_total_coin,".
        "SUM(IF(type=1 AND subtype=2,coin,0)) AS alluser_avg_coin, ".
        "SUM(IF(type=2 AND subtype=1,coin,0)) AS orderuser_total_coin, ".
        "SUM(IF(type=2 AND subtype=2,coin,0)) AS orderuser_avg_coin,".
        "SUM(IF(type=3 AND subtype=1,coin,0)) AS activeuser_total_coin, ".
        "SUM(IF(type=3 AND subtype=2,coin,0)) AS activeuser_avg_coin, ".
        "SUM(IF(type=4 AND subtype=1,coin,0)) AS activeuser_notorder_total_coin, ".
        "SUM(IF(type=4 AND subtype=2,coin,0)) AS activeuser_notorder_avg_coin, ".
        "SUM(IF(type=5 AND subtype=1,coin,0)) AS orderuser_30_total_coin, ".
        "SUM(IF(type=5 AND subtype=2,coin,0)) AS orderuser_30_avg_coin ".
        "FROM user_coin_log $tail $group_by";
        
    $loglist = $db_analysis->gettotallist($sql);
    
    
    #Take5
    # 웹 활동유저 전체 보유 코인 평균
    $sql = "SELECT today, ROUND(AVG(currentcoin)) AS avg_coin ".
    		"FROM `tbl_user_playstat_gather_daily`	".
    		"WHERE statcode = 100 AND today >= '$startdate' AND today <= '$enddate'	".
    		"GROUP BY today	";
    $web_play_user_coin = $db_other->gettotallist($sql);
    
    $list_pointer = sizeof($web_play_user_coin);
    
    $date_pointer = $enddate;
    
    for ($i=0; $i<get_diff_date($enddate, $startdate, "d"); $i++)
    {
    	if ($list_pointer > 0)
    		$today = $web_play_user_coin[$list_pointer-1]["today"];
    
    	if (get_diff_date($date_pointer, $today, "d") == 0)
    	{
    		$web_play_user_log = $web_play_user_coin[$list_pointer-1];
    		
    		$web_play_user_date_list[$i] = $date_pointer;    		
    		$web_play_user_avg_coin[$i] = $web_play_user_log["avg_coin"];
    
    		$list_pointer--;
    	}
    	else
    	{
    		$web_play_user_date_list[$i] = $date_pointer;
    		$web_play_user_avg_coin[$i] = 0;    
    	}
    
    	$date_pointer = get_past_date($date_pointer, 1, "d");
    }
    
    # 웹 VIP 결제자 보유 코인 평균    
    $sql = "SELECT today, ROUND(AVG(currentcoin)) AS avg_coin ".
			"FROM `tbl_user_playstat_gather_daily`	".
			"WHERE statcode IN (106, 107, 108, 109, 110, 111) AND today >= '$startdate' AND today <= '$enddate' ".
			"GROUP BY today ";
    $web_vip_user_coin = $db_other->gettotallist($sql);

    $list_pointer = sizeof($web_vip_user_coin);
    
    $date_pointer = $enddate;
    
    for ($i=0; $i<get_diff_date($enddate, $startdate, "d"); $i++)
    {
    	if ($list_pointer > 0)
    		$today = $web_vip_user_coin[$list_pointer-1]["today"];
    
    	if (get_diff_date($date_pointer, $today, "d") == 0)
    	{
	    	$web_vip_user_log = $web_vip_user_coin[$list_pointer-1];
	    
	    	$web_vip_user_date_list[$i] = $date_pointer;
	    	$web_vip_user_avg_coin[$i] = $web_vip_user_log["avg_coin"];
	    
	    	$list_pointer--;
    	}
    	else
    	{
        	$web_vip_user_date_list[$i] = $date_pointer;
        	$web_vip_user_avg_coin[$i] = 0;
        }
    
        $date_pointer = get_past_date($date_pointer, 1, "d");
    }
    
    # Ios 활동유저 전체 보유 코인 평균
    $sql = "SELECT today, ROUND(AVG(currentcoin)) AS avg_coin ".
    		"FROM `tbl_user_playstat_gather_daily_ios`	".
    		"WHERE statcode = 100 AND today >= '$startdate' AND today <= '$enddate'	".
    		"GROUP BY today	";
    $ios_play_user_coin = $db_other->gettotallist($sql);
    
    $list_pointer = sizeof($ios_play_user_coin);
    
    $date_pointer = $enddate;
    
    for ($i=0; $i<get_diff_date($enddate, $startdate, "d"); $i++)
    {
    	if ($list_pointer > 0)
    		$today = $ios_play_user_coin[$list_pointer-1]["today"];
    
    	if (get_diff_date($date_pointer, $today, "d") == 0)
    	{
    		$ios_play_user_log = $ios_play_user_coin[$list_pointer-1];
    
    		$ios_play_user_date_list[$i] = $date_pointer;
    		$ios_play_user_avg_coin[$i] = $ios_play_user_log["avg_coin"];
    
    		$list_pointer--;
    	}
    	else
    	{
        	$ios_play_user_date_list[$i] = $date_pointer;
        	$ios_play_user_avg_coin[$i] = 0;
        }
    
        		$date_pointer = get_past_date($date_pointer, 1, "d");
    }
    
    # ios VIP 결제자 보유 코인 평균
    $sql = "SELECT today, ROUND(AVG(currentcoin)) AS avg_coin ".
   			"FROM `tbl_user_playstat_gather_daily_ios`	".
			"WHERE statcode IN (106, 107, 108, 109, 110, 111) AND today >= '$startdate' AND today <= '$enddate' ".
    		"GROUP BY today ";
	$ios_vip_user_coin = $db_other->gettotallist($sql);
    
    $list_pointer = sizeof($ios_vip_user_coin);
    
    $date_pointer = $enddate;
    
    for ($i=0; $i<get_diff_date($enddate, $startdate, "d"); $i++)
    {
    	if ($list_pointer > 0)
    		$today = $ios_vip_user_coin[$list_pointer-1]["today"];
    
    	if (get_diff_date($date_pointer, $today, "d") == 0)
    	{
    		$ios_vip_user_log = $ios_vip_user_coin[$list_pointer-1];
    			 
    		$ios_vip_user_date_list[$i] = $date_pointer;
    		$ios_vip_user_avg_coin[$i] = $ios_vip_user_log["avg_coin"];
    				 
    		$list_pointer--;
		}
		else
		{
			$ios_vip_user_date_list[$i] = $date_pointer;
			$ios_vip_user_avg_coin[$i] = 0;
		}
    
		$date_pointer = get_past_date($date_pointer, 1, "d");
    }
    
    # Android 활동유저 전체 보유 코인 평균
    $sql = "SELECT today, ROUND(AVG(currentcoin)) AS avg_coin ".
    		"FROM `tbl_user_playstat_gather_daily_android`	".
    		"WHERE statcode = 100 AND today >= '$startdate' AND today <= '$enddate'	".
    		"GROUP BY today	";
    $android_play_user_coin = $db_other->gettotallist($sql);
    
    $list_pointer = sizeof($android_play_user_coin);
    
    $date_pointer = $enddate;
    
    for ($i=0; $i<get_diff_date($enddate, $startdate, "d"); $i++)
    {
    	if ($list_pointer > 0)
    		$today = $android_play_user_coin[$list_pointer-1]["today"];
    
    	if (get_diff_date($date_pointer, $today, "d") == 0)
    	{
    		$android_play_user_log = $android_play_user_coin[$list_pointer-1];
    
    		$android_play_user_date_list[$i] = $date_pointer;
    		$android_play_user_avg_coin[$i] = $android_play_user_log["avg_coin"];
    
    		$list_pointer--;
    	}
    	else
    	{
    		$android_play_user_date_list[$i] = $date_pointer;
    		$android_play_user_avg_coin[$i] = 0;
    	}
    
    		$date_pointer = get_past_date($date_pointer, 1, "d");
    }
    
    # Android VIP 결제자 보유 코인 평균
	$sql = "SELECT today, ROUND(AVG(currentcoin)) AS avg_coin ".
    		"FROM `tbl_user_playstat_gather_daily_android`	".
			"WHERE statcode IN (106, 107, 108, 109, 110, 111) AND today >= '$startdate' AND today <= '$enddate' ".
    		"GROUP BY today ";
    $android_vip_user_coin = $db_other->gettotallist($sql);
    
    $list_pointer = sizeof($android_vip_user_coin);
    
    $date_pointer = $enddate;
    
    for ($i=0; $i<get_diff_date($enddate, $startdate, "d"); $i++)
    {
    	if ($list_pointer > 0)
    		$today = $android_vip_user_coin[$list_pointer-1]["today"];
    
    	if (get_diff_date($date_pointer, $today, "d") == 0)
    	{
    		$android_vip_user_log = $android_vip_user_coin[$list_pointer-1];
    
    		$android_vip_user_date_list[$i] = $date_pointer;
    		$android_vip_user_avg_coin[$i] = $android_vip_user_log["avg_coin"];
    						
    		$list_pointer--;
    	}
    	else
    	{
    		$android_vip_user_date_list[$i] = $date_pointer;
    		$android_vip_user_avg_coin[$i] = 0;
    	}
    
    	$date_pointer = get_past_date($date_pointer, 1, "d");
    }
    
    # amazon 활동유저 전체 보유 코인 평균
    $sql = "SELECT today, ROUND(AVG(currentcoin)) AS avg_coin ".
    		"FROM `tbl_user_playstat_gather_daily_amazon`	".
    		"WHERE statcode = 100 AND today >= '$startdate' AND today <= '$enddate'	".
    		"GROUP BY today	";
    $amazon_play_user_coin = $db_other->gettotallist($sql);
    
    $list_pointer = sizeof($amazon_play_user_coin);
    
    $date_pointer = $enddate;
    
    for ($i=0; $i<get_diff_date($enddate, $startdate, "d"); $i++)
    {
    	if ($list_pointer > 0)
    		$today = $amazon_play_user_coin[$list_pointer-1]["today"];
    
    	if (get_diff_date($date_pointer, $today, "d") == 0)
    	{
    		$amazon_play_user_log = $amazon_play_user_coin[$list_pointer-1];
    
    		$amazon_play_user_date_list[$i] = $date_pointer;
    		$amazon_play_user_avg_coin[$i] = $amazon_play_user_log["avg_coin"];
    
    		$list_pointer--;
    	}
    	else
    	{
    		$amazon_play_user_date_list[$i] = $date_pointer;
    		$amazon_play_user_avg_coin[$i] = 0;
    	}
    
    		$date_pointer = get_past_date($date_pointer, 1, "d");
    }
    
    # amazon VIP 결제자 보유 코인 평균
    $sql = "SELECT today, ROUND(AVG(currentcoin)) AS avg_coin ".
    		"FROM `tbl_user_playstat_gather_daily_amazon`	".
			"WHERE statcode IN (106, 107, 108, 109, 110, 111) AND today >= '$startdate' AND today <= '$enddate' ".
    		"GROUP BY today ";
    $amazon_vip_user_coin = $db_other->gettotallist($sql);
    
    $list_pointer = sizeof($amazon_vip_user_coin);
    
    $date_pointer = $enddate;
    
    for ($i=0; $i<get_diff_date($enddate, $startdate, "d"); $i++)
    {
    	if ($list_pointer > 0)
    		$today = $amazon_vip_user_coin[$list_pointer-1]["today"];
    
    	if (get_diff_date($date_pointer, $today, "d") == 0)
    	{
    		$amazon_vip_user_log = $amazon_vip_user_coin[$list_pointer-1];
    
    		$amazon_vip_user_date_list[$i] = $date_pointer;
    		$amazon_vip_user_avg_coin[$i] = $amazon_vip_user_log["avg_coin"];
    
    		$list_pointer--;
    	}
    	else
    	{
    		$amazon_vip_user_date_list[$i] = $date_pointer;
    		$amazon_vip_user_avg_coin[$i] = 0;
    	}
    
    	$date_pointer = get_past_date($date_pointer, 1, "d");
    }
    
    $db_other->end();
    $db_analysis->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    
    function drawChart() 
    {

		var play_user_data = new google.visualization.DataTable();        
		play_user_data.addColumn('string', '시간');
		play_user_data.addColumn('number', '코인');
		play_user_data.addRows([
		                		
<?
	        for ($i=sizeof($web_play_user_date_list); $i>0; $i--)
	        {
	        	$date = $web_play_user_date_list[$i-1];
	        	$coin = $web_play_user_avg_coin[$i-1];
	        		        	
	            echo("['".$date."'");
	            
	            if ($coin != "")
	                echo(",{v:".$coin.",f:'".make_price_format($coin)."'}]");
	            else
	                echo(",0]");
	            
	            if ($i > 1)
	    			echo(",");
	        }
?>     
        ]);

		var vip_user_data = new google.visualization.DataTable();        
		vip_user_data.addColumn('string', '시간');
		vip_user_data.addColumn('number', '코인');
		vip_user_data.addRows([
		                		
<?
	        for ($i=sizeof($web_vip_user_date_list); $i>0; $i--)
	        {
	        	$date = $web_vip_user_date_list[$i-1];
	        	$coin = $web_vip_user_avg_coin[$i-1];
	
	        	
	            echo("['".$date."'");
	            
	            if ($coin != "")
	                echo(",{v:".$coin.",f:'".make_price_format($coin)."'}]");
	            else
	                echo(",0]");
	            
	            if ($i > 1)
	    			echo(",");
	        }
?>     
        ]);	

		var ios_play_user_data = new google.visualization.DataTable();        
		ios_play_user_data.addColumn('string', '시간');
		ios_play_user_data.addColumn('number', '코인');
		ios_play_user_data.addRows([
		                		
<?
	        for ($i=sizeof($ios_play_user_date_list); $i>0; $i--)
	        {
	        	$date = $ios_play_user_date_list[$i-1];
	        	$coin = $ios_play_user_avg_coin[$i-1];
	        		        	
	            echo("['".$date."'");
	            
	            if ($coin != "")
	                echo(",{v:".$coin.",f:'".make_price_format($coin)."'}]");
	            else
	                echo(",0]");
	            
	            if ($i > 1)
	    			echo(",");
	        }
?>     
        ]);

		var ios_vip_user_data = new google.visualization.DataTable();        
		ios_vip_user_data.addColumn('string', '시간');
		ios_vip_user_data.addColumn('number', '코인');
		ios_vip_user_data.addRows([
		                		
<?
	        for ($i=sizeof($ios_vip_user_date_list); $i>0; $i--)
	        {
	        	$date = $ios_vip_user_date_list[$i-1];
	        	$coin = $ios_vip_user_avg_coin[$i-1];
	
	        	
	            echo("['".$date."'");
	            
	            if ($coin != "")
	                echo(",{v:".$coin.",f:'".make_price_format($coin)."'}]");
	            else
	                echo(",0]");
	            
	            if ($i > 1)
	    			echo(",");
	        }
?>     
        ]);

		var android_play_user_data = new google.visualization.DataTable();        
		android_play_user_data.addColumn('string', '시간');
		android_play_user_data.addColumn('number', '코인');
		android_play_user_data.addRows([
		                		
<?
	        for ($i=sizeof($android_play_user_date_list); $i>0; $i--)
	        {
	        	$date = $android_play_user_date_list[$i-1];
	        	$coin = $android_play_user_avg_coin[$i-1];
	        		        	
	            echo("['".$date."'");
	            
	            if ($coin != "")
	                echo(",{v:".$coin.",f:'".make_price_format($coin)."'}]");
	            else
	                echo(",0]");
	            
	            if ($i > 1)
	    			echo(",");
	        }
?>     
        ]);

		var android_vip_user_data = new google.visualization.DataTable();        
		android_vip_user_data.addColumn('string', '시간');
		android_vip_user_data.addColumn('number', '코인');
		android_vip_user_data.addRows([
		                		
<?
	        for ($i=sizeof($android_vip_user_date_list); $i>0; $i--)
	        {
	        	$date = $android_vip_user_date_list[$i-1];
	        	$coin = $android_vip_user_avg_coin[$i-1];
	
	        	
	            echo("['".$date."'");
	            
	            if ($coin != "")
	                echo(",{v:".$coin.",f:'".make_price_format($coin)."'}]");
	            else
	                echo(",0]");
	            
	            if ($i > 1)
	    			echo(",");
	        }
?>     
        ]);	

		var amazon_play_user_data = new google.visualization.DataTable();        
		amazon_play_user_data.addColumn('string', '시간');
		amazon_play_user_data.addColumn('number', '코인');
		amazon_play_user_data.addRows([
		                		
<?
	        for ($i=sizeof($amazon_play_user_date_list); $i>0; $i--)
	        {
	        	$date = $amazon_play_user_date_list[$i-1];
	        	$coin = $amazon_play_user_avg_coin[$i-1];
	        		        	
	            echo("['".$date."'");
	            
	            if ($coin != "")
	                echo(",{v:".$coin.",f:'".make_price_format($coin)."'}]");
	            else
	                echo(",0]");
	            
	            if ($i > 1)
	    			echo(",");
	        }
?>     
        ]);

		var amazon_vip_user_data = new google.visualization.DataTable();        
		amazon_vip_user_data.addColumn('string', '시간');
		amazon_vip_user_data.addColumn('number', '코인');
		amazon_vip_user_data.addRows([
		                		
<?
	        for ($i=sizeof($amazon_vip_user_date_list); $i>0; $i--)
	        {
	        	$date = $amazon_vip_user_date_list[$i-1];
	        	$coin = $amazon_vip_user_avg_coin[$i-1];
	
	        	
	            echo("['".$date."'");
	            
	            if ($coin != "")
	                echo(",{v:".$coin.",f:'".make_price_format($coin)."'}]");
	            else
	                echo(",0]");
	            
	            if ($i > 1)
	    			echo(",");
	        }
?>     
        ]);	
        
        var data1 = new google.visualization.DataTable();
        
        data1.addColumn('string', '시간');
        data1.addColumn('number', '코인');
        data1.addRows([
<?
    $start = round(mktime(0, 0, 0, date("m",strtotime($startdate)), date("d", strtotime($startdate)), date("Y", strtotime($startdate)))/($term*60));
    $end = round(mktime(0, 0, 0, date("m",strtotime($enddate)), date("d", strtotime($enddate)), date("Y", strtotime($enddate)))/($term*60)) + 1;
    
    for ($i=$start; $i<$end; $i=$i+1)
    {      
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        echo("['$list_date'");  
        
        $print = false;
        for ($j=0; $j<sizeof($loglist); $j++)
        {
            $temp_date = date("Y-m-d", $i*($term*60));  
            
            if ($loglist[$j]["today"] == $temp_date)
            {
                echo(",{v:".$loglist[$j]["alluser_total_coin"].",f:'".make_price_format($loglist[$j]["alluser_total_coin"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",{v:0,f:'0'}");
        
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");               
    }    
?>          
        ]);
        
        var data2 = new google.visualization.DataTable();
        
        data2.addColumn('string', '시간');
        data2.addColumn('number', '코인');
        data2.addRows([
<?
    for ($i=$start; $i<$end; $i=$i+1)
    {      
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        echo("['$list_date'");  
        
        $print = false;
        for ($j=0; $j<sizeof($loglist); $j++)
        {
            $temp_date = date("Y-m-d", $i*($term*60));  
            
            if ($loglist[$j]["today"] == $temp_date)
            {
                echo(",{v:".$loglist[$j]["alluser_avg_coin"].",f:'".make_price_format($loglist[$j]["alluser_avg_coin"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",{v:0,f:'0'}");
        
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");               
    }    
?>          
        ]);
        
        var data3 = new google.visualization.DataTable();
        
        data3.addColumn('string', '시간');
        data3.addColumn('number', '코인');
        data3.addColumn('number', '총 보유코인의 30%이상 소지 유저 제외 코인');
        data3.addRows([
<?
    for ($i=$start; $i<$end; $i=$i+1)
    {      
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        echo("['$list_date'");  
        
        $print = false;
        for ($j=0; $j<sizeof($loglist); $j++)
        {
            $temp_date = date("Y-m-d", $i*($term*60));  
            
            if ($loglist[$j]["today"] == $temp_date)
            {
                echo(",{v:".$loglist[$j]["orderuser_total_coin"].",f:'".make_price_format($loglist[$j]["orderuser_total_coin"])."'}");
                echo(",{v:".$loglist[$j]["orderuser_30_total_coin"].",f:'".make_price_format($loglist[$j]["orderuser_30_total_coin"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",{v:0,f:'0'},{v:0,f:'0'}");
        
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");               
    }    
?>          
        ]);
        
        var data4 = new google.visualization.DataTable();
        
        data4.addColumn('string', '시간');
        data4.addColumn('number', '코인');
        data4.addColumn('number', '총 보유코인의 30%이상 소지 유저 제외 코인');
        data4.addRows([
<?
    for ($i=$start; $i<$end; $i=$i+1)
    {      
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        echo("['$list_date'");  
        
        $print = false;
        for ($j=0; $j<sizeof($loglist); $j++)
        {
            $temp_date = date("Y-m-d", $i*($term*60));  
            
            if ($loglist[$j]["today"] == $temp_date)
            {
                echo(",{v:".$loglist[$j]["orderuser_avg_coin"].",f:'".make_price_format($loglist[$j]["orderuser_avg_coin"])."'}");
                echo(",{v:".$loglist[$j]["orderuser_30_avg_coin"].",f:'".make_price_format($loglist[$j]["orderuser_30_avg_coin"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",{v:0,f:'0'},{v:0,f:'0'}");
        
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");               
    }    
?>          
        ]);
        
        var data5 = new google.visualization.DataTable();
        
        data5.addColumn('string', '시간');
        data5.addColumn('number', '코인');
        data5.addRows([
<?
    for ($i=$start; $i<$end; $i=$i+1)
    {      
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        echo("['$list_date'");  
        
        $print = false;
        for ($j=0; $j<sizeof($loglist); $j++)
        {
            $temp_date = date("Y-m-d", $i*($term*60));  
            
            if ($loglist[$j]["today"] == $temp_date)
            {
                echo(",{v:".$loglist[$j]["activeuser_total_coin"].",f:'".make_price_format($loglist[$j]["activeuser_total_coin"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",{v:0,f:'0'}");
        
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");               
    }    
?>          
        ]);
        
        var data6 = new google.visualization.DataTable();
        
        data6.addColumn('string', '시간');
        data6.addColumn('number', '코인');
        data6.addRows([
<?
    for ($i=$start; $i<$end; $i=$i+1)
    {      
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        echo("['$list_date'");  
        
        $print = false;
        for ($j=0; $j<sizeof($loglist); $j++)
        {
            $temp_date = date("Y-m-d", $i*($term*60));  
            
            if ($loglist[$j]["today"] == $temp_date)
            {
                echo(",{v:".$loglist[$j]["activeuser_avg_coin"].",f:'".make_price_format($loglist[$j]["activeuser_avg_coin"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",{v:0,f:'0'}");
        
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");               
    }    
?>          
        ]);
        
        var data7 = new google.visualization.DataTable();
        
        data7.addColumn('string', '시간');
        data7.addColumn('number', '코인');
        data7.addRows([
<?
    for ($i=$start; $i<$end; $i=$i+1)
    {      
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        echo("['$list_date'");  
        
        $print = false;
        for ($j=0; $j<sizeof($loglist); $j++)
        {
            $temp_date = date("Y-m-d", $i*($term*60));  
            
            if ($loglist[$j]["today"] == $temp_date)
            {
                echo(",{v:".$loglist[$j]["activeuser_notorder_total_coin"].",f:'".make_price_format($loglist[$j]["activeuser_notorder_total_coin"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",{v:0,f:'0'}");
        
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");               
    }    
?>          
        ]);
        
        var data8 = new google.visualization.DataTable();
        
        data8.addColumn('string', '시간');
        data8.addColumn('number', '코인');
        data8.addRows([
<?
    for ($i=$start; $i<$end; $i=$i+1)
    {      
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        echo("['$list_date'");  
        
        $print = false;
        for ($j=0; $j<sizeof($loglist); $j++)
        {
            $temp_date = date("Y-m-d", $i*($term*60));  
            
            if ($loglist[$j]["today"] == $temp_date)
            {
                echo(",{v:".$loglist[$j]["activeuser_notorder_avg_coin"].",f:'".make_price_format($loglist[$j]["activeuser_notorder_avg_coin"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",{v:0,f:'0'}");
        
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");               
    }    
?>          
        ]);
    
        var options = {          
            axisTitlesPosition:'in',  
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize:12,
            chartArea:{left:100,top:40,width:1000,height:130}
        };
                
        var chart = new google.visualization.LineChart(document.getElementById('chart_play_user'));
        chart.draw(play_user_data, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_vip_user'));
        chart.draw(vip_user_data, options); 

        var chart = new google.visualization.LineChart(document.getElementById('chart_ios_play_user'));
        chart.draw(ios_play_user_data, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_ios_vip_user'));
        chart.draw(ios_vip_user_data, options);  

        var chart = new google.visualization.LineChart(document.getElementById('chart_android_play_user'));
        chart.draw(android_play_user_data, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_android_vip_user'));
        chart.draw(android_vip_user_data, options);  

        var chart = new google.visualization.LineChart(document.getElementById('chart_amazon_play_user'));
        chart.draw(amazon_play_user_data, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_amazon_vip_user'));
        chart.draw(amazon_vip_user_data, options);          

        chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
        chart.draw(data1, options); 
        
        chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
        chart.draw(data2, options); 
        
        chart = new google.visualization.LineChart(document.getElementById('chart_div3'));
        chart.draw(data3, options);  
        
        chart = new google.visualization.LineChart(document.getElementById('chart_div4'));
        chart.draw(data4, options);     
        
        chart = new google.visualization.LineChart(document.getElementById('chart_div5'));
        chart.draw(data5, options);   
        
        chart = new google.visualization.LineChart(document.getElementById('chart_div6'));
        chart.draw(data6, options);  
        
        chart = new google.visualization.LineChart(document.getElementById('chart_div7'));
        chart.draw(data7, options);     
        
        chart = new google.visualization.LineChart(document.getElementById('chart_div8'));
        chart.draw(data8, options);               
    }
        
    google.setOnLoadCallback(drawChart);    

    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }

    function search()
    {
        var search_form = document.search_form;

        search_form.submit();
    }
    
    $(function() {
        $("#startdate").datepicker({ });
    });
    
    $(function() {
        $("#enddate").datepicker({ });
    });
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 보유 코인 통계</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->

	<div class="search_result">
		<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다
	</div>
	
	
	
	<div class="h2_title">
	Web 활동 유저 평균 코인 <font size="1">(만단위)</font>
	</div>          
	<div id="chart_play_user" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title  mt30">
	Web vip 유저 평균 코인<font size="1">(만단위)</font>
	</div>          
	<div id="chart_vip_user" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title">
	Ios 활동 유저 평균 코인 <font size="1">(만단위)</font>
	</div>          
	<div id="chart_ios_play_user" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title  mt30">
	Ios vip 유저 평균 코인<font size="1">(만단위)</font>
	</div>          
	<div id="chart_ios_vip_user" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title">
	Android 활동 유저 평균 코인 <font size="1">(만단위)</font>
	</div>          
	<div id="chart_android_play_user" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title  mt30">
	Android vip 유저 평균 코인<font size="1">(만단위)</font>
	</div>          
	<div id="chart_android_vip_user" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title">
	Amazon 활동 유저 평균 코인 <font size="1">(만단위)</font>
	</div>          
	<div id="chart_amazon_play_user" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title  mt30">
	Amazon vip 유저 평균 코인<font size="1">(만단위)</font>
	</div>          
	<div id="chart_amazon_vip_user" style="height:230px; min-width: 500px"></div>
            
	<div class="h2_title  mt30">
	사용자 코인 합계
	</div>          
	<div id="chart_div1" style="height:230px; min-width: 500px"></div>
            
	<div class="h2_title mt30">
	사용자 코인 평균
	</div>          
	<div id="chart_div2" style="height:230px; min-width: 500px"></div>
            
	<div class="h2_title mt30">
	7일 이내 구매자 코인 합계
	</div>          
	<div id="chart_div3" style="height:230px; min-width: 500px"></div>
            
	<div class="h2_title mt30">
	7일 이내 구매자 코인 평균
	</div>          
	<div id="chart_div4" style="height:230px; min-width: 500px"></div>
            
	<div class="h2_title mt30">
	7일 이내 활동사용자 코인 합계 
	</div>          
	<div id="chart_div5" style="height:230px; min-width: 500px"></div>
            
	<div class="h2_title mt30">
	7일 이내 활동사용자 코인 평균
	</div>          
	<div id="chart_div6" style="height:230px; min-width: 500px"></div>
            
	<div class="h2_title mt30">
	7일 이내 활동사용자 &amp; 7일 이내 비구매자 코인 합계 
	</div>          
	<div id="chart_div7" style="height:230px; min-width: 500px"></div>
            
	<div class="h2_title mt30">
	7일 이내 활동사용자 &amp; 7일 이내 비구매자 코인 평균
	</div>          
	<div id="chart_div8" style="height:230px; min-width: 500px"></div>
            
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>