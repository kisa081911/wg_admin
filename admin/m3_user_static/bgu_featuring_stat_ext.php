<?
    $top_menu = "user_static";
    $sub_menu = "bgu_featuring_stat_ext";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $pagename = "bgu_featuring_stat_ext.php";
    
    $startdate = $_GET["startdate"];
    $enddate = $_GET["enddate"];
    
    check_xss($startdate.$enddate);
    
    $startdate = ($startdate == "") ? get_past_date(date("Y-m-d"),7,"d") : $startdate;
    $enddate = ($enddate == "") ? date("Y-m-d") : $enddate;
    
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	
	    search_form.submit();
	}
	
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});
</script>

		<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
                <!-- title_warp -->
                <div class="title_wrap">
                    <div class="title"><?= $top_menu_txt ?> &gt; BGU 피쳐링 상세 현황</div>
                    
                    <form name="search_form" id="search_form"  method="get" action="freecoin_stat_web.php">
	                	<div class="search_box">
		                    <input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
		                    <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
							<input type="button" class="btn_search" value="검색" onclick="search()" />
                		</div>
                	</form>
                </div>
                <!-- //title_warp -->
				<table class="tbl_list_basic1">
	                <colgroup>
						<col width="">
						<col width="">
						<col width="">
						<col width="">
						<col width="">
						<col width="">
						<col width="">
						<col width="">
						<col width="170">
						<col width="">
					</colgroup>
	                
	                <thead>
		                <tr>
		                	<th class="tdc" rowspan="2">날짜</th>
		                	<th class="tdc" colspan="3" style="border-right: 1px dotted #dbdbdb;">신규</th>
		                	<th class="tdc" colspan="4" style="border-right: 1px dotted #dbdbdb;">2주이상 이탈 복귀자</th>
		                </tr>
		                <tr>
		                	<th class="tdr">가입자 수</th>
		                	<th class="tdr">결제자 수</th>
		                	<th class="tdr" style="border-right: 1px dotted #dbdbdb;">결제 금액</th>
		                	
		                	<th class="tdr">복귀자 수</th>
		                	<th class="tdr">평균 복귀일</th>
		                	<th class="tdr">결제 경험자 수</th>
		                	<th class="tdr" style="border-right: 1px dotted #dbdbdb;">복귀 후 결제금액</th>
		                </tr>
	                </thead>
	                
	                <tbody>
<?
	$total_join_cnt = 0;
	$total_join_payer_cnt = 0;
	$total_join_payer_money = 0;

	$total_leave_2week_cnt = 0;
	$total_avg_2week_leavedays = 0;
	$total_leave_2week_payer_cnt = 0;
	$total_leave_2week_payer_money = 0;
	
	$today = $startdate;
	while ($today != $enddate)
	{
		$today = date("Y-m-d", strtotime($today."1 day"));
		
		$db_main = new CDatabase_Slave_Main();
		$db_main2 = new CDatabase_Main2();
		
		$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today,
		COUNT(*) AS leave_2week_cnt, SUM(leavedays) AS avg_2week_leavedays, COUNT(IF(is_payer = 1, 1,NULL)) AS payer_cnt
		FROM tbl_user_retention_log
		WHERE useridx > 20000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (fbsource = 'bgl_featured' OR fbsource = 'appcenter_featured')
		GROUP BY today
		ORDER BY today ASC";
		$leave_2week_stats = $db_main2->getarray($sql);
		
		$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, COUNT(IF(isnew = 1, 1, NULL)) AS new_user_cnt
		FROM tbl_user_featured
		WHERE useridx > 20000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'
		AND writedate > '2017-05-25 00:00:00'
		GROUP BY today
		ORDER BY today ASC";
		$join_stats = $db_main2->getarray($sql);
		
		
		$leave_2week_cnt = $leave_2week_stats["leave_2week_cnt"];
		$avg_2week_leavedays = $leave_2week_stats["avg_2week_leavedays"];
		$leave_2week_money = $leave_2week_stats["leave_2week_money"];
		$leave_2week_payer_cnt = $leave_2week_stats["payer_cnt"];
		$join_cnt = $join_stats["new_user_cnt"];
		
		// 신규
		$sql = "select useridx
		FROM tbl_user_featured
		WHERE useridx > 20000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'
		AND writedate > '2017-05-25 00:00:00'";
		$join_user_list = $db_main2->gettotallist($sql);
		
		$join_userlist = "";
		$join_payer_cnt;
		for($j=0; $j<sizeof($join_user_list); $j++)
		{
			if($j == sizeof($join_user_list) - 1)
				$join_userlist .= $join_user_list[$j]["useridx"];
				else
					$join_userlist .= $join_user_list[$j]["useridx"].",";
		
		}
		
		if($join_userlist != "")
		{
			$sql = "SELECT IFNULL(SUM(money), 0) AS money
			FROM
			(
			SELECT SUM(facebookcredit / 10) AS money FROM tbl_product_order WHERE useridx > 20000 AND STATUS = 1 AND writedate >= '$today 00:00:00' AND useridx IN ($join_userlist)
			UNION ALL
			SELECT SUM(money) AS money FROM tbl_product_order_mobile WHERE useridx > 20000 AND STATUS = 1 AND writedate >= '$today 00:00:00' AND useridx IN ($join_userlist)
			) t1";
			$join_payer_money = $db_main->getvalue($sql);
			
			$sql = "SELECT IFNULL(COUNT(DISTINCT useridx), 0) AS cnt
			FROM
			(
			SELECT useridx FROM tbl_product_order WHERE useridx > 20000 AND STATUS = 1 AND writedate >= '$today 00:00:00' AND useridx IN ($join_userlist)
			UNION ALL
			SELECT useridx FROM tbl_product_order_mobile WHERE useridx > 20000 AND STATUS = 1 AND writedate >= '$today 00:00:00' AND useridx IN ($join_userlist)
			) t1";
			$join_payer_cnt = $db_main->getvalue($sql);
		}
		else
			$join_payer_money = 0;
		
			// 2주 이상 이탈자 결제액
			$sql = "SELECT useridx
			FROM tbl_user_retention_log
			WHERE useridx > 20000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (fbsource = 'bgl_featured' OR fbsource = 'appcenter_featured')";
			$leave_2week_user_list = $db_main2->gettotallist($sql);
		
			$leave_2week_userlist = "";
		
			for($j=0; $j<sizeof($leave_2week_user_list); $j++)
			{
				if($j == sizeof($leave_2week_user_list) - 1)
					$leave_2week_userlist .= $leave_2week_user_list[$j]["useridx"];
					else
						$leave_2week_userlist .= $leave_2week_user_list[$j]["useridx"].",";
		
			}
		
			if($leave_2week_userlist != "")
			{
				$sql = "SELECT IFNULL(SUM(money), 0) AS money
				FROM
				(
				SELECT SUM(facebookcredit / 10) AS money FROM tbl_product_order WHERE useridx > 20000 AND STATUS = 1 AND writedate >= '$today 00:00:00' AND useridx IN ($leave_2week_userlist)
				UNION ALL
				SELECT SUM(money) AS money FROM tbl_product_order_mobile WHERE useridx > 20000 AND STATUS = 1 AND writedate >= '$today 00:00:00' AND useridx IN ($leave_2week_userlist)
				) t1";
				$leave_2week_payer_money = $db_main->getvalue($sql);
			}
			else
				$leave_2week_payer_money = 0;
		
				$total_join_cnt += $join_cnt;
				$total_join_payer_cnt += $join_payer_cnt;
				$total_join_payer_money += $join_payer_money;
					
				$total_leave_2week_cnt += $leave_2week_cnt;
				$total_avg_2week_leavedays += $avg_2week_leavedays;
				$total_leave_2week_payer_cnt += $leave_2week_payer_cnt;
				$total_leave_2week_payer_money += $leave_2week_payer_money;
	
?>
						<tr onmouseover="className='tr_over'" onmouseout="className=''">
							<td class="tdc point"><?= $today ?></td>
							<td class="tdr"><?= number_format($join_cnt) ?></td>
							<td class="tdr"><?= number_format($join_payer_cnt) ?></td>
							<td class="tdr" style="border-right: 1px dotted #dbdbdb;">$ <?= number_format($join_payer_money, 2) ?></td>
							
							<td class="tdr"><?= number_format($leave_2week_cnt) ?></td>
							<td class="tdr"><?= ($leave_2week_cnt == 0) ? 0 : number_format($avg_2week_leavedays / $leave_2week_cnt, 2) ?></td>
							<td class="tdr"><?= number_format($leave_2week_payer_cnt) ?></td>
							<td class="tdr" style="border-right: 1px dotted #dbdbdb;">$ <?= number_format($leave_2week_payer_money, 2) ?></td>
						</tr>
<?
	}
?>

						<tr>
							<td class="tdc point_title">Total</td>
							<td class="tdr"><?= number_format($total_join_cnt) ?></td>
							<td class="tdr"><?= number_format($total_join_payer_cnt) ?></td>
							<td class="tdr" style="border-right: 1px dotted #dbdbdb;">$ <?= number_format($total_join_payer_money, 2) ?></td>
							
							<td class="tdr"><?= number_format($total_leave_2week_cnt) ?></td>
							<td class="tdr"><?= ($total_leave_2week_cnt == 0) ? 0 : number_format($total_avg_2week_leavedays / $total_leave_2week_cnt, 2) ?></td>
							<td class="tdr"><?= number_format($total_leave_2week_payer_cnt) ?></td>
							<td class="tdr" style="border-right: 1px dotted #dbdbdb;">$ <?= number_format($total_leave_2week_payer_money, 2) ?></td>
						</tr>
	                </tbody>
                </table>
            </form>
            
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
	$db_main->end();
	$db_main2->end();

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>