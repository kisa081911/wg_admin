<?
    $top_menu = "user_static";
    $sub_menu = "user_retention_detail_daily";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $startdate = $_GET["startdate"];
    $enddate = $_GET["enddate"];
    
    $search_adflag = ($_GET["search_adflag"] == "") ? 0 : $_GET["search_adflag"];
    $search_data = ($_GET["search_data"] == "") ? 0 : $_GET["search_data"];
    $search_view = ($_GET["search_view"] == "") ? 0 : $_GET["search_view"];
    $os_type = ($_GET["os_type"] == "") ? 0 : $_GET["os_type"];
    
    //오늘 날짜 정보
    $today = get_past_date(date("Y-m-d"),1,"d");
    $before_day = "2016-01-06";
    $startdate = ($startdate == "") ? $before_day : $startdate;
    $enddate = ($enddate == "") ? $today : $enddate;
    
    $pagename = "user_retention_detail_daily.php";
    
    $db_other = new CDatabase_Other();
    
    $sql = "SELECT REPLACE( adflag, '_viral' , '' ) AS adflag FROM tbl_user_detail_retention_daily WHERE '$startdate' <= reg_date AND reg_date <= '$enddate' AND adflag NOT IN ('wcasino_popup -', 'wcasino_popuphttps://www.facebook.com') GROUP BY REPLACE( adflag, '_viral' , '' )";
    $adflag_list = $db_other->gettotallist($sql);
    
    if($search_view == 0)
    {
    	$stamp_sql = "DATE_FORMAT(reg_date, '%Y-%m')";
    	$regdate_sql = "DATE_FORMAT(reg_date, '%Y-%m')";    	
    	$reg_count_sql = "SUM(IF(reg_date='$enddate',0,reg_count))";
    }
    else
    {
    	
    	$stamp_sql = "DATE_FORMAT(reg_date, '%Y-%m-%d')";
    	$regdate_sql = "DATE_FORMAT(reg_date, '%Y-%m-%d')";
    	$reg_count_sql = "SUM(reg_count)";  	
    }
    
    if($search_adflag == "0")
    {
    	if($search_view == 0)
    	{
    		if($os_type == 0)
    		{
	    		$sql = "SELECT DATE_FORMAT(reg_date, '%Y-%m-%d') AS stamp, DATE_FORMAT(reg_date, '%Y-%m') AS reg_date,  DATEDIFF('$today', reg_date) AS std_day, ".
	    				"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count,  SUM(loyal_count) AS loyal_count, SUM(day_1) AS day_1 ".
	    				"FROM tbl_user_detail_retention_daily ".
	    				"WHERE '2016-01-06' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate' AND platform = 0 ".
	    				"GROUP BY stamp ".
	    				"ORDER BY stamp ASC, reg_date ASC";
	    		$day_reg_count_list = array(array("name" => "Web", "data" => $db_other->gettotallist($sql)));
    		}
    		else if($os_type == 2)
    		{	
	    		$sql = "SELECT DATE_FORMAT(reg_date, '%Y-%m-%d') AS stamp, DATE_FORMAT(reg_date, '%Y-%m') AS reg_date,  DATEDIFF('$today', reg_date) AS std_day, ".
	    				"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count,  SUM(loyal_count) AS loyal_count, SUM(day_1) AS day_1 ".
	    				"FROM tbl_user_detail_retention_daily ".
	    				"WHERE '2016-11-16' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate'  AND platform = 2 ".
	    				"GROUP BY stamp ".
	    				"ORDER BY stamp ASC, reg_date ASC";
	    		$day_reg_count_list = array(array("name" => "Android", "data" => $db_other->gettotallist($sql)));
    		}
    		else if($os_type == 1)
    		{
	     		$sql = "SELECT DATE_FORMAT(reg_date, '%Y-%m-%d') AS stamp, DATE_FORMAT(reg_date, '%Y-%m') AS reg_date,  DATEDIFF('$today', reg_date) AS std_day, ".
	     				"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count,  SUM(loyal_count) AS loyal_count, SUM(day_1) AS day_1 ".
	     				"FROM tbl_user_detail_retention_daily ".
	     				"WHERE '2016-11-16' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate' AND platform = 1 ".
	     				"GROUP BY stamp ".
	     				"ORDER BY stamp ASC, reg_date ASC";
	     		$day_reg_count_list = array(array("name" => "IOS", "data" => $db_other->gettotallist($sql)));
    		}
    		else if($os_type == 3)
    		{
	     		$sql = "SELECT DATE_FORMAT(reg_date, '%Y-%m-%d') AS stamp, DATE_FORMAT(reg_date, '%Y-%m') AS reg_date,  DATEDIFF('$today', reg_date) AS std_day, ".
	     				"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count,  SUM(loyal_count) AS loyal_count, SUM(day_1) AS day_1 ".
	     				"FROM tbl_user_detail_retention_daily ".
	     				"WHERE '2016-11-16' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate'  AND platform = 3 ".
	     				"GROUP BY stamp ".
	     				"ORDER BY stamp ASC, reg_date ASC";
	     		$day_reg_count_list = array(array("name" => "Amazon", "data" => $db_other->gettotallist($sql)));
    		}
    	}
    	
    	if($os_type == 0)
    	{
    		$platform_name = "WEB";
    		//Web
    		$sql = "SELECT $stamp_sql AS stamp, $regdate_sql AS reg_date, DATEDIFF('$today', reg_date) AS std_day, ".
    				"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count, SUM(loyal_count) AS loyal_count, ".
    				"	SUM(install_count) AS install_count, SUM(play_count) AS play_count, SUM(tutorial_count) AS tutorial_count, ".
    				"	SUM(day_1) AS day_1, SUM(day_2) AS day_2,	".
    				"	SUM(day_3) AS day_3, SUM(day_4) AS day_4, SUM(day_5) AS day_5, SUM(day_6) AS day_6,	".
    				"	SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
    				"FROM tbl_user_detail_retention_daily ".
    				"WHERE '2016-01-06' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate'  AND platform = 0 ".
    				"GROUP BY stamp ".
    				"ORDER BY stamp ASC, reg_date ASC";
    		$retention_list = array(array("name" => "Web", "data" => $db_other->gettotallist($sql)));
    	}
    	else if($os_type == 1)
    	{
    		$platform_name = "IOS";
    		//IOS
    		$sql = "SELECT $stamp_sql AS stamp, $regdate_sql AS reg_date, DATEDIFF('$today', reg_date) AS std_day, ".
    				"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count, SUM(loyal_count) AS loyal_count, ".
    				"	SUM(install_count) AS install_count, SUM(play_count) AS play_count, SUM(tutorial_count) AS tutorial_count, ".
    				"	SUM(day_1) AS day_1, SUM(day_2) AS day_2,	".
    				"	SUM(day_3) AS day_3, SUM(day_4) AS day_4, SUM(day_5) AS day_5, SUM(day_6) AS day_6,	".
    				"	SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
    				"FROM tbl_user_detail_retention_daily ".
    				"WHERE '2016-01-06' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate' AND platform = 1 ".
    				"GROUP BY stamp ".
    				"ORDER BY stamp ASC, reg_date ASC";
    		$retention_list = array(array("name" => "IOS", "data" => $db_other->gettotallist($sql)));
    		
    	}
    	else if($os_type == 2)
    	{
    		$platform_name = "Android";
    		//Android
    		$sql = "SELECT $stamp_sql AS stamp, $regdate_sql AS reg_date, DATEDIFF('$today', reg_date) AS std_day, ".
    				"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count, SUM(loyal_count) AS loyal_count, ".
    				"	SUM(install_count) AS install_count, SUM(play_count) AS play_count, SUM(tutorial_count) AS tutorial_count, ".
    				"	SUM(day_1) AS day_1, SUM(day_2) AS day_2,	".
    				"	SUM(day_3) AS day_3, SUM(day_4) AS day_4, SUM(day_5) AS day_5, SUM(day_6) AS day_6,	".
    				"	SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
    				"FROM tbl_user_detail_retention_daily ".
    				"WHERE '2016-11-16' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate' AND platform = 2 ".
    				"GROUP BY stamp ".
    				"ORDER BY stamp ASC, reg_date ASC";
    		$retention_list = array(array("name" => "Android", "data" => $db_other->gettotallist($sql)));
    	}
    	else if($os_type == 3)
    	{
    		$platform_name = "Amazon";
    		//amazon
    		$sql = "SELECT $stamp_sql AS stamp, $regdate_sql AS reg_date, DATEDIFF('$today', reg_date) AS std_day, ".
    				"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count, SUM(loyal_count) AS loyal_count, ".
    				"	SUM(install_count) AS install_count, SUM(play_count) AS play_count, SUM(tutorial_count) AS tutorial_count, ".
    				"	SUM(day_1) AS day_1, SUM(day_2) AS day_2,	".
    				"	SUM(day_3) AS day_3, SUM(day_4) AS day_4, SUM(day_5) AS day_5, SUM(day_6) AS day_6,	".
    				"	SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
    				"FROM tbl_user_detail_retention_daily ".
    				"WHERE '2016-01-06' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate'  AND platform = 3 ".
    				"GROUP BY stamp ".
    				"ORDER BY stamp ASC, reg_date ASC";
    		$retention_list = array(array("name" => "Amazon", "data" => $db_other->gettotallist($sql)));
    	}
    }
    else
    {
    	if($os_type == 0)
    		$platform_name = "$search_adflag - Web";
    	if($os_type == 1)
    		$platform_name = "$search_adflag - IOS";
    	else if($os_type == 2)
    		$platform_name = "$search_adflag - Android";
    	else if($os_type == 3)
    		$platform_name = "$search_adflag - Amazon";
    				
    	if($search_view == 0)
    	{
//     		$sql = "SELECT DATE_FORMAT(reg_date, '%Y-%m-%d') AS stamp, DATE_FORMAT(reg_date, '%Y-%m') AS reg_date,  DATEDIFF('$today', reg_date) AS std_day, ".
//     				"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count,  SUM(loyal_count) AS loyal_count, SUM(day_1) AS day_1 ".
//     				"FROM tbl_user_detail_retention_daily ".
//     				"WHERE '$before_day' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate' ".
//     				"AND adflag = '".($search_adflag == "viral" ? "" : $search_adflag)."' ".
//     				"GROUP BY stamp ".
//     				"ORDER BY stamp ASC, reg_date ASC";
//     		$day_reg_count_list =  array(array("name" => "$search_adflag", "data" => $db_other->gettotallist($sql)));
    		
    		if($os_type == 0)
    		{
	    		$sql = "SELECT DATE_FORMAT(reg_date, '%Y-%m-%d') AS stamp, DATE_FORMAT(reg_date, '%Y-%m') AS reg_date,  DATEDIFF('$today', reg_date) AS std_day, ".
	    				"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count,  SUM(loyal_count) AS loyal_count, SUM(day_1) AS day_1 ".
	    				"FROM tbl_user_detail_retention_daily ".
	    				"WHERE '$before_day' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate' AND platform = 0 ".
	    				"AND adflag = '".($search_adflag == "viral" ? "" : $search_adflag)."' ".
	    				"GROUP BY stamp ".
	    				"ORDER BY stamp ASC, reg_date ASC";
	    		$day_reg_count_list = array(array("name" => "Web", "data" => $db_other->gettotallist($sql)));
    		}
    		
    		else if($os_type == 2)
    		{
	    		$sql = "SELECT DATE_FORMAT(reg_date, '%Y-%m-%d') AS stamp, DATE_FORMAT(reg_date, '%Y-%m') AS reg_date,  DATEDIFF('$today', reg_date) AS std_day, ".
	    				"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count,  SUM(loyal_count) AS loyal_count, SUM(day_1) AS day_1 ".
	    				"FROM tbl_user_detail_retention_daily ".
	    				"WHERE '$before_day' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate'  AND platform = 2 ".
	    				"AND adflag = '".($search_adflag == "viral" ? "" : $search_adflag)."' ".
	    				"GROUP BY stamp ".
	    				"ORDER BY stamp ASC, reg_date ASC";
	    		$day_reg_count_list = array(array("name" => "Android", "data" => $db_other->gettotallist($sql)));
    		}
    		else if($os_type == 1)
    		{
	    		$sql = "SELECT DATE_FORMAT(reg_date, '%Y-%m-%d') AS stamp, DATE_FORMAT(reg_date, '%Y-%m') AS reg_date,  DATEDIFF('$today', reg_date) AS std_day, ".
	    				"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count,  SUM(loyal_count) AS loyal_count, SUM(day_1) AS day_1 ".
	    				"FROM tbl_user_detail_retention_daily ".
	    				"WHERE '$before_day' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate' AND platform = 1 ".
	    				"AND adflag = '".($search_adflag == "viral" ? "" : $search_adflag)."' ".
	    				"GROUP BY stamp ".
	    				"ORDER BY stamp ASC, reg_date ASC";
	    		$day_reg_count_list = array(array("name" => "IOS", "data" => $db_other->gettotallist($sql)));
    		}
    		else if($os_type == 3)
    		{
	    		$sql = "SELECT DATE_FORMAT(reg_date, '%Y-%m-%d') AS stamp, DATE_FORMAT(reg_date, '%Y-%m') AS reg_date,  DATEDIFF('$today', reg_date) AS std_day, ".
	    				"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count,  SUM(loyal_count) AS loyal_count, SUM(day_1) AS day_1 ".
	    				"FROM tbl_user_detail_retention_daily ".
	    				"WHERE '$before_day' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate'  AND platform = 3 ".
	    				"AND adflag = '".($search_adflag == "viral" ? "" : $search_adflag)."' ".
	    				"GROUP BY stamp ".
	    				"ORDER BY stamp ASC, reg_date ASC";
	    		$day_reg_count_list = array(array("name" => "Amazon", "data" => $db_other->gettotallist($sql)));
	    	}
    	}
    	
    	if($os_type == 0)
    	{
    		$platform_name = "WEB";
    		//Web
	    	$sql = "SELECT $stamp_sql AS stamp, $regdate_sql AS reg_date, DATEDIFF('$today', reg_date) AS std_day, ".
	    			"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count, SUM(loyal_count) AS loyal_count, ".
	    			"	SUM(install_count) AS install_count, SUM(play_count) AS play_count, SUM(tutorial_count) AS tutorial_count, ".
	    			"	SUM(day_1) AS day_1, SUM(day_2) AS day_2,	".
	    			"	SUM(day_3) AS day_3, SUM(day_4) AS day_4, SUM(day_5) AS day_5, SUM(day_6) AS day_6,	".
	    			"	SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
	    			"FROM tbl_user_detail_retention_daily ".
	    			"WHERE '2016-01-06' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate'  AND platform = 0 ".
	    			"AND adflag = '".($search_adflag == "viral" ? "" : $search_adflag)."' ".
	    			"GROUP BY stamp ".
	    			"ORDER BY stamp ASC, reg_date ASC";
	    	$retention_list = array(array("name" => "Web", "data" => $db_other->gettotallist($sql)));
    	}
    	else if($os_type == 1)
    	{
    		$platform_name = "IOS";
    		//IOS
	    	$sql = "SELECT $stamp_sql AS stamp, $regdate_sql AS reg_date, DATEDIFF('$today', reg_date) AS std_day, ".
	    			"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count, SUM(loyal_count) AS loyal_count, ".
	    			"	SUM(install_count) AS install_count, SUM(play_count) AS play_count, SUM(tutorial_count) AS tutorial_count, ".
	    			"	SUM(day_1) AS day_1, SUM(day_2) AS day_2,	".
	    			"	SUM(day_3) AS day_3, SUM(day_4) AS day_4, SUM(day_5) AS day_5, SUM(day_6) AS day_6,	".
	    			"	SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
	    			"FROM tbl_user_detail_retention_daily ".
	    			"WHERE '2016-01-06' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate' AND platform = 1 ".
	    			"AND adflag = '".($search_adflag == "viral" ? "" : $search_adflag)."' ".
	    			"GROUP BY stamp ".
	    			"ORDER BY stamp ASC, reg_date ASC";
	    	$retention_list = array(array("name" => "IOS", "data" => $db_other->gettotallist($sql)));
    	
    	}
    	else if($os_type == 2)
    	{
    		$platform_name = "Android";
    		//Android
	    	$sql = "SELECT $stamp_sql AS stamp, $regdate_sql AS reg_date, DATEDIFF('$today', reg_date) AS std_day, ".
	    			"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count, SUM(loyal_count) AS loyal_count, ".
	    			"	SUM(install_count) AS install_count, SUM(play_count) AS play_count, SUM(tutorial_count) AS tutorial_count, ".
	    			"	SUM(day_1) AS day_1, SUM(day_2) AS day_2,	".
	    			"	SUM(day_3) AS day_3, SUM(day_4) AS day_4, SUM(day_5) AS day_5, SUM(day_6) AS day_6,	".
	    			"	SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
	    			"FROM tbl_user_detail_retention_daily ".
	    			"WHERE '2016-11-16' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate' AND platform = 2 ".
	    			"AND adflag = '".($search_adflag == "viral" ? "" : $search_adflag)."' ".
	    			"GROUP BY stamp ".
	    			"ORDER BY stamp ASC, reg_date ASC";
	    	$retention_list = array(array("name" => "Android", "data" => $db_other->gettotallist($sql)));
    	}
    	else if($os_type == 3)
    	{
    		$platform_name = "Amazon";
    		//amazon
	    	$sql = "SELECT $stamp_sql AS stamp, $regdate_sql AS reg_date, DATEDIFF('$today', reg_date) AS std_day, ".
	    			"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count, SUM(loyal_count) AS loyal_count, ".
	    			"	SUM(install_count) AS install_count, SUM(play_count) AS play_count, SUM(tutorial_count) AS tutorial_count, ".
	    			"	SUM(day_1) AS day_1, SUM(day_2) AS day_2,	".
	    			"	SUM(day_3) AS day_3, SUM(day_4) AS day_4, SUM(day_5) AS day_5, SUM(day_6) AS day_6,	".
	    			"	SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
	    			"FROM tbl_user_detail_retention_daily ".
	    			"WHERE '2016-01-06' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate'  AND platform = 3 ".
	    			"AND adflag = '".($search_adflag == "viral" ? "" : $search_adflag)."' ".
	    			"GROUP BY stamp ".
	    			"ORDER BY stamp ASC, reg_date ASC";
	    	$retention_list = array(array("name" => "Amazon", "data" => $db_other->gettotallist($sql)));
    	}
//     	$sql = "SELECT $stamp_sql AS stamp, $regdate_sql AS reg_date, DATEDIFF('$today', reg_date) AS std_day, ".
//     			"	SUM(reg_count) AS std_data, $reg_count_sql AS reg_count, SUM(loyal_count) AS loyal_count, ".
//     			"	SUM(install_count) AS install_count, SUM(play_count) AS play_count, SUM(tutorial_count) AS tutorial_count, ".
//     			"	SUM(day_1) AS day_1, SUM(day_2) AS day_2,	".
//     			"	SUM(day_3) AS day_3, SUM(day_4) AS day_4, SUM(day_5) AS day_5, SUM(day_6) AS day_6,	".
//     			"	SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
//     			"FROM tbl_user_detail_retention_daily ".
//     			"WHERE '2016-01-06' <= reg_date AND '$startdate' <= reg_date AND reg_date <= '$enddate' ".
//     			"AND adflag = '".($search_adflag == "viral" ? "" : $search_adflag)."' ".
//     			"GROUP BY stamp ".
//     			"ORDER BY stamp ASC, reg_date ASC";
    	//$retention_list = array(array("name" => "$search_adflag", "data" => $db_other->gettotallist($sql)));
    }
    
    $db_other->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#startdate").datepicker({ });
	});
	
	$(function() {
		$("#enddate").datepicker({ });
	});
	
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.startdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    } 
	
	    if (search_form.enddate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    } 
	
	    search_form.submit();
	}

	function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap" style="height: 88px;">
		<div class="title"><?= $top_menu_txt ?> &gt; Retention</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box" style=" margin-top: -27px;">
			
				<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />
			<div style="margin-left:739px;">
        		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
					<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
					<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
					<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
					<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
				</span>
			</div>
				</br>
				<span class="search_lbl ml20">구분&nbsp;</span>
				<select name="search_adflag" id="search_adflag">
					<option value="0" <?= ($search_adflag == "0") ? "selected" : "" ?>>전체</option>
<?
				for($i=0; $i<sizeof($adflag_list); $i++)
				{
					$adflag = $adflag_list[$i]["adflag"];
					
					$adflag = ($adflag == "") ? "viral" : $adflag;
?>
					<option value="<?= $adflag ?>" <?= ("$search_adflag" == "$adflag") ? "selected" : "" ?>><?= $adflag ?></option>
<?
				}
?>
				</select>
				<span class="search_lbl ml20">Data&nbsp;&nbsp;&nbsp;</span>
				<select name="search_data" id="search_data">
					<option value="0" <?= ($search_data == "0") ? "selected" : "" ?>>비율(Installs)</option>
					<option value="2" <?= ($search_data == "2") ? "selected" : "" ?>>비율(D+1)</option>
					<option value="3" <?= ($search_data == "3") ? "selected" : "" ?>>비율(Loyal Users)</option>
					<option value="4" <?= ($search_data == "4") ? "selected" : "" ?>>숫자</option>
				</select>
				<span class="search_lbl ml20">기간&nbsp;&nbsp;&nbsp;</span>
				<select name="search_view" id="search_view">
					<option value="0" <?= ($search_view == "0") ? "selected" : "" ?>>월</option>
					<option value="1" <?= ($search_view == "1") ? "selected" : "" ?>>일</option>
				</select>
				<span class="search_lbl ml20">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" /> ~
				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->


	
<?
	for($i=0; $i<sizeof($retention_list); $i++)
	{
		if($i > 0)
		{
?>
		<br/><br/>
<?
		}
?>
		<div class="search_result"><?=$platform_name?></div>
		<div class="search_result">
			<span><?= $startdate ?> ~ <?= $enddate ?></span> 통계입니다
		</div>
		<table class="tbl_list_basic1">
		<colgroup>
			<col width="80">
			<col width="70">
			<col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
			<col width="70">
            <col width="70">
            <col width="70">
            <col width="65">
            <col width="65">
            <col width="65">
		</colgroup>
        <thead>
         	<th>가입월</th>
			<th>가입자수</th>
			<th>Loyal Users</th>
			<th>미설치</th>
			<th>미게임</th>
			<th>튜토리얼</th>
			<th>D+1</th>
			<th>D+2</th>
			<th>D+3</th>
			<th>D+4</th>
			<th>D+5</th>
			<th>D+6</th>
			<th>D+7</th>
			<th>D+14</th>
			<th>D+28</th>
			<th>D+60</th>
			<th>D+90</th>
        </thead>
        <tbody>
<?
		$retention_detail = $retention_list[$i]["data"];
		$day_reg_count_detail = $day_reg_count_list[$i]["data"];
		
		$sum_reg_count = 0;
		$sum_loyal_count = 0;
		$sum_install_count = 0;
		$sum_play_count = 0;
		$sum_tutorial_count = 0;
		$sum_day_1 = 0;
		$sum_day_1_reg_count = 0;
		$day_1_row_count = 0;
		$sum_day_2 = 0;
		$sum_day_2_reg_count = 0;
		$day_2_row_count = 0;
		$sum_day_3 = 0;
		$sum_day_3_reg_count = 0;
		$day_3_row_count = 0;
		
		$sum_day_4 = 0;
		$sum_day_4_reg_count = 0;
		$day_4_row_count = 0;
		$sum_day_5 = 0;
		$sum_day_5_reg_count = 0;
		$day_5_row_count = 0;
		$sum_day_6 = 0;
		$sum_day_6_reg_count = 0;
		$day_6_row_count = 0;
		
		$sum_day_7 = 0;
		$sum_day_7_reg_count = 0;
		$day_7_row_count = 0;
		$sum_day_14 = 0;
		$sum_day_14_reg_count = 0;
		$day_14_row_count = 0;
		$sum_day_28 = 0;
		$sum_day_28_reg_count = 0;
		$day_28_row_count = 0;
		$sum_day_60 = 0;
		$sum_day_60_reg_count = 0;
		$day_60_row_count = 0;
		$sum_day_90 = 0;
		$sum_day_90_reg_count = 0;
		$day_90_row_count = 0;
		
		$day_90_reg_count = array();
		$day_60_reg_count = array();
		$day_28_reg_count = array();
		$day_14_reg_count = array();
		$day_7_reg_count = array();
		$day_6_reg_count = array();
		$day_5_reg_count = array();
		$day_4_reg_count = array();
		$day_3_reg_count = array();
		$day_2_reg_count = array();
		$day_1_reg_count = array();
		
		$sum_row_day_90_reg_count = 0;
		$sum_row_day_60_reg_count = 0;
		$sum_row_day_28_reg_count = 0;
		$sum_row_day_14_reg_count = 0;
		$sum_row_day_7_reg_count = 0;
		$sum_row_day_6_reg_count = 0;
		$sum_row_day_5_reg_count = 0;
		$sum_row_day_4_reg_count = 0;
		$sum_row_day_3_reg_count = 0;
		$sum_row_day_2_reg_count = 0;
		$sum_row_day_1_reg_count = 0;
		
		if($search_view == 0)
		{
			for($k = 0; $k < sizeof($day_reg_count_detail); $k++)
			{
				$reg_date = $day_reg_count_detail[$k]["reg_date"];
				$std_day = $day_reg_count_detail[$k]["std_day"];
				$reg_count = $day_reg_count_detail[$k]["reg_count"];
				$day_1 = $day_reg_count_detail[$k]["day_1"];
				$loyal_count = $day_reg_count_detail[$k]["loyal_count"];

				if($search_data == 0 || $search_data == 4)
					$std_count_sql = $reg_count;
				else if($search_data == 2)
					$std_count_sql = $day_1;
				else if($search_data == 3)
					$std_count_sql = $loyal_count;
		
				if($std_day >= 90)
					$day_90_reg_count[$reg_date] += $std_count_sql;
		
				if($std_day >= 60)
					$day_60_reg_count[$reg_date] += $std_count_sql;
		
				if($std_day >= 28)
					$day_28_reg_count[$reg_date] += $std_count_sql;
		
				if($std_day >= 14)
					$day_14_reg_count[$reg_date] += $std_count_sql;
		
				if($std_day >= 7)
					$day_7_reg_count[$reg_date] += $std_count_sql;
		
				if($std_day >= 6)
					$day_6_reg_count[$reg_date] += $std_count_sql;

				if($std_day >= 5)
					$day_5_reg_count[$reg_date] += $std_count_sql;

				if($std_day >= 4)
					$day_4_reg_count[$reg_date] += $std_count_sql;

				if($std_day >= 3)
					$day_3_reg_count[$reg_date] += $std_count_sql;

				if($std_day >= 2)
					$day_2_reg_count[$reg_date] += $std_count_sql;

				if($std_day >= 1)
					$day_1_reg_count[$reg_date] += $std_count_sql;


				if($std_day >= 90)
					$sum_row_day_90_reg_count += $std_count_sql;

				if($std_day >= 60)
					$sum_row_day_60_reg_count += $std_count_sql;

				if($std_day >= 28)
					$sum_row_day_28_reg_count += $std_count_sql;

				if($std_day >= 14)
					$sum_row_day_14_reg_count += $std_count_sql;

				if($std_day >= 7)
					$sum_row_day_7_reg_count += $std_count_sql;

				if($std_day >= 6)
					$sum_row_day_6_reg_count += $std_count_sql;

				if($std_day >= 5)
					$sum_row_day_5_reg_count += $std_count_sql;

				if($std_day >= 4)
					$sum_row_day_4_reg_count += $std_count_sql;

				if($std_day >= 3)
					$sum_row_day_3_reg_count += $std_count_sql;

				if($std_day >= 2)
					$sum_row_day_2_reg_count += $std_count_sql;

				if($std_day >= 1)
					$sum_row_day_1_reg_count += $std_count_sql;
			}
		}
		
		

		for($j=0; $j<sizeof($retention_detail); $j++)
		{
			$reg_date = $retention_detail[$j]["reg_date"];
			$std_day = $retention_detail[$j]["std_day"];
			$std_data = $retention_detail[$j]["std_data"];
			$reg_count = $retention_detail[$j]["reg_count"];
			$loyal_count = $retention_detail[$j]["loyal_count"];
			$install_count = $retention_detail[$j]["install_count"];;
			$play_count = $retention_detail[$j]["play_count"];;
			$tutorial_count = $retention_detail[$j]["tutorial_count"];;
			$day_1 = $retention_detail[$j]["day_1"];
			$day_2 = $retention_detail[$j]["day_2"];
			$day_3 = $retention_detail[$j]["day_3"];
			$day_4 = $retention_detail[$j]["day_4"];
			$day_5 = $retention_detail[$j]["day_5"];
			$day_6 = $retention_detail[$j]["day_6"];
			$day_7 = $retention_detail[$j]["day_7"];
			$day_14 = $retention_detail[$j]["day_14"];
			$day_28 = $retention_detail[$j]["day_28"];
			$day_60 = $retention_detail[$j]["day_60"];
			$day_90 = $retention_detail[$j]["day_90"];
			
			$sum_reg_count += $std_data;
			$sum_loyal_count += $loyal_count;
			$sum_install_count += $install_count;
			$sum_play_count += $play_count;
			$sum_tutorial_count += $tutorial_count;
			$sum_day_1 += $day_1;
			$sum_day_2 += $day_2;
			$sum_day_3 += $day_3;
			$sum_day_4 += $day_4;
			$sum_day_5 += $day_5;
			$sum_day_6 += $day_6;
			$sum_day_7 += $day_7;
			$sum_day_14 += $day_14;
			$sum_day_28 += $day_28;
			$sum_day_60 += $day_60;
			$sum_day_90 += $day_90;
			
			
			if($search_data == 0 || $search_data == 4)				
				$std_count_sql = $reg_count;				
			else if($search_data == 2)
				$std_count_sql = $day_1;
			else if($search_data == 3)
				$std_count_sql = $loyal_count;
			
			if($search_view == 0)
			{
				if($std_day >= 90)
					$sum_day_90_reg_count = $sum_row_day_90_reg_count;
					
				if($std_day >= 60)
					$sum_day_60_reg_count = $sum_row_day_60_reg_count;
					
				if($std_day >= 28)
					$sum_day_28_reg_count = $sum_row_day_28_reg_count;
					
				if($std_day >= 14)
					$sum_day_14_reg_count = $sum_row_day_14_reg_count;
					
				if($std_day >= 7)
					$sum_day_7_reg_count = $sum_row_day_7_reg_count;
					
				if($std_day >= 6)
					$sum_day_6_reg_count = $sum_row_day_6_reg_count;
					
				if($std_day >= 5)
					$sum_day_5_reg_count = $sum_row_day_5_reg_count;
					
				if($std_day >= 4)
					$sum_day_4_reg_count = $sum_row_day_4_reg_count;
					
				if($std_day >= 3)
					$sum_day_3_reg_count = $sum_row_day_3_reg_count;
					
				if($std_day >= 2)
					$sum_day_2_reg_count = $sum_row_day_2_reg_count;
					
				if($std_day >= 1)
					$sum_day_1_reg_count = $sum_row_day_1_reg_count;
			
			
				$sum_day_0_reg_count += $std_data;
			}
			else
			{
				if($std_day >= 90)
					$sum_day_90_reg_count += $std_count_sql;
					
				if($std_day >= 60)
					$sum_day_60_reg_count += $std_count_sql;
					
				if($std_day >= 28)
					$sum_day_28_reg_count += $std_count_sql;
					
				if($std_day >= 14)
					$sum_day_14_reg_count += $std_count_sql;
					
				if($std_day >= 7)
					$sum_day_7_reg_count += $std_count_sql;
					
				if($std_day >= 6)
					$sum_day_6_reg_count += $std_count_sql;
					
				if($std_day >= 5)
					$sum_day_5_reg_count += $std_count_sql;
					
				if($std_day >= 4)
					$sum_day_4_reg_count += $std_count_sql;
					
				if($std_day >= 3)
					$sum_day_3_reg_count += $std_count_sql;
					
				if($std_day >= 2)
					$sum_day_2_reg_count += $std_count_sql;
					
				if($std_day >= 1)
					$sum_day_1_reg_count += $std_count_sql;
				
				$sum_day_0_reg_count += $std_data;
			}
			
			if($day_1 > 0)
				$day_1_row_count++;
				
			if($day_2 > 0)
				$day_2_row_count++;
				
			if($day_3 > 0)
				$day_3_row_count++;
				
			if($day_4 > 0)
				$day_4_row_count++;
				
			if($day_5 > 0)
				$day_5_row_count++;
			
			if($day_6 > 0)
				$day_6_row_count++;
				
			if($day_7 > 0)
				$day_7_row_count++;
				
			if($day_14 > 0)
				$day_14_row_count++;
				
			if($day_28 > 0)
				$day_28_row_count++;
				
			if($day_60 > 0)
				$day_60_row_count++;
				
			if($day_90 > 0)
				$day_90_row_count++;
			
			$add_per = "";
			$add_point = 0;
			
			if($search_data == 0)
			{
				$add_per = "%";
				$add_point = 1;
				
				$loyal_count = ($std_data == 0) ? 0 : round($loyal_count/$std_data*100, $add_point);
				$install_count = ($std_data == 0) ? 0 : round(100-($install_count/$std_data*100), $add_point);
				$play_count = ($std_data == 0) ? 0 : round(100-($play_count/$std_data*100), $add_point);
				$tutorial_count = ($std_data == 0) ? 0 : round($tutorial_count/$std_data*100, $add_point);
				
				if($search_view == 0)
				{

					$day_1 = ($day_1_reg_count[$reg_date] == 0) ? 0 : round($day_1/$day_1_reg_count[$reg_date]*100, $add_point);
					$day_2 = ($day_2_reg_count[$reg_date] == 0) ? 0 : round($day_2/$day_2_reg_count[$reg_date]*100, $add_point);
					$day_3 = ($day_3_reg_count[$reg_date] == 0) ? 0 : round($day_3/$day_3_reg_count[$reg_date]*100, $add_point);
					$day_4 = ($day_4_reg_count[$reg_date] == 0) ? 0 : round($day_4/$day_4_reg_count[$reg_date]*100, $add_point);
					$day_5 = ($day_5_reg_count[$reg_date] == 0) ? 0 : round($day_5/$day_5_reg_count[$reg_date]*100, $add_point);
					$day_6 = ($day_6_reg_count[$reg_date] == 0) ? 0 : round($day_6/$day_6_reg_count[$reg_date]*100, $add_point);
					$day_7 = ($day_7_reg_count[$reg_date] == 0) ? 0 : round($day_7/$day_7_reg_count[$reg_date]*100, $add_point);
					$day_14 = ($day_14_reg_count[$reg_date] == 0) ? 0 : round($day_14/$day_14_reg_count[$reg_date]*100, $add_point);
					$day_28 = ($day_28_reg_count[$reg_date] == 0) ? 0 : round($day_28/$day_28_reg_count[$reg_date]*100, $add_point);
					$day_60 = ($day_60_reg_count[$reg_date] == 0) ? 0 : round($day_60/$day_60_reg_count[$reg_date]*100, $add_point);
					$day_90 = ($day_90_reg_count[$reg_date] == 0) ? 0 : round($day_90/$day_90_reg_count[$reg_date]*100, $add_point);
				}
				else
				{
					$day_1 = ($reg_count == 0) ? 0 : round($day_1/$reg_count*100, $add_point);
					$day_2 = ($reg_count == 0) ? 0 : round($day_2/$reg_count*100, $add_point);
					$day_3 = ($reg_count == 0) ? 0 : round($day_3/$reg_count*100, $add_point);
					$day_4 = ($reg_count == 0) ? 0 : round($day_4/$reg_count*100, $add_point);
					$day_5 = ($reg_count == 0) ? 0 : round($day_5/$reg_count*100, $add_point);
					$day_6 = ($reg_count == 0) ? 0 : round($day_6/$reg_count*100, $add_point);
					$day_7 = ($reg_count == 0) ? 0 : round($day_7/$reg_count*100, $add_point);
					$day_14 = ($reg_count == 0) ? 0 : round($day_14/$reg_count*100, $add_point);
					$day_28 = ($reg_count == 0) ? 0 : round($day_28/$reg_count*100, $add_point);
					$day_60 = ($reg_count == 0) ? 0 : round($day_60/$reg_count*100, $add_point);
					$day_90 = ($reg_count == 0) ? 0 : round($day_90/$reg_count*100, $add_point);
				}
			}
			else if($search_data == 2)
			{
				$add_per = "%";
				$add_point = 1;
			
				$loyal_count = ($std_data == 0) ? 0 : round($loyal_count/$std_data*100, $add_point);
				$install_count = ($std_data == 0) ? 0 : round(100-($install_count/$std_data*100), $add_point);
				$play_count = ($std_data == 0) ? 0 : round(100-($play_count/$std_data*100), $add_point);
				$tutorial_count = ($std_data == 0) ? 0 : round($tutorial_count/$std_data*100, $add_point);
				
				$std_division = $day_1;
				
				if($search_view == 0)
				{
					$day_1 = ($day_1_reg_count[$reg_date] == 0) ? 0 : round($day_1/$day_1_reg_count[$reg_date]*100, $add_point);
					$day_2 = ($day_2_reg_count[$reg_date] == 0) ? 0 : round($day_2/$day_2_reg_count[$reg_date]*100, $add_point);
					$day_3 = ($day_3_reg_count[$reg_date] == 0) ? 0 : round($day_3/$day_3_reg_count[$reg_date]*100, $add_point);
					$day_4 = ($day_4_reg_count[$reg_date] == 0) ? 0 : round($day_4/$day_4_reg_count[$reg_date]*100, $add_point);
					$day_5 = ($day_5_reg_count[$reg_date] == 0) ? 0 : round($day_5/$day_5_reg_count[$reg_date]*100, $add_point);
					$day_6 = ($day_6_reg_count[$reg_date] == 0) ? 0 : round($day_6/$day_6_reg_count[$reg_date]*100, $add_point);
					$day_7 = ($day_7_reg_count[$reg_date] == 0) ? 0 : round($day_7/$day_7_reg_count[$reg_date]*100, $add_point);
					$day_14 = ($day_14_reg_count[$reg_date] == 0) ? 0 : round($day_14/$day_14_reg_count[$reg_date]*100, $add_point);
					$day_28 = ($day_28_reg_count[$reg_date] == 0) ? 0 : round($day_28/$day_28_reg_count[$reg_date]*100, $add_point);
					$day_60 = ($day_60_reg_count[$reg_date] == 0) ? 0 : round($day_60/$day_60_reg_count[$reg_date]*100, $add_point);
					$day_90 = ($day_90_reg_count[$reg_date] == 0) ? 0 : round($day_90/$day_90_reg_count[$reg_date]*100, $add_point);
				}
				else
				{						
					$day_1 = ($std_division == 0) ? 0 : round($day_1/$std_division*100, $add_point);
					$day_2 = ($std_division == 0) ? 0 : round($day_2/$std_division*100, $add_point);
					$day_3 = ($std_division == 0) ? 0 : round($day_3/$std_division*100, $add_point);
					$day_4 = ($std_division == 0) ? 0 : round($day_4/$std_division*100, $add_point);
					$day_5 = ($std_division == 0) ? 0 : round($day_5/$std_division*100, $add_point);
					$day_6 = ($std_division == 0) ? 0 : round($day_6/$std_division*100, $add_point);
					$day_7 = ($std_division == 0) ? 0 : round($day_7/$std_division*100, $add_point);
					$day_14 = ($std_division == 0) ? 0 : round($day_14/$std_division*100, $add_point);
					$day_28 = ($std_division == 0) ? 0 : round($day_28/$std_division*100, $add_point);
					$day_60 = ($std_division == 0) ? 0 : round($day_60/$std_division*100, $add_point);
					$day_90 = ($std_division == 0) ? 0 : round($day_90/$std_division*100, $add_point);
				}
			}
			else if($search_data == 3)
			{
				$add_per = "%";
				$add_point = 1;								
				
				$std_division = $loyal_count;
									
				if($search_view == 0)
				{					
					$day_1 = ($day_1_reg_count[$reg_date] == 0) ? 0 : round($day_1/$day_1_reg_count[$reg_date]*100, $add_point);
					$day_2 = ($day_2_reg_count[$reg_date] == 0) ? 0 : round($day_2/$day_2_reg_count[$reg_date]*100, $add_point);
					$day_3 = ($day_3_reg_count[$reg_date] == 0) ? 0 : round($day_3/$day_3_reg_count[$reg_date]*100, $add_point);
					$day_4 = ($day_4_reg_count[$reg_date] == 0) ? 0 : round($day_4/$day_4_reg_count[$reg_date]*100, $add_point);
					$day_5 = ($day_5_reg_count[$reg_date] == 0) ? 0 : round($day_5/$day_5_reg_count[$reg_date]*100, $add_point);
					$day_6 = ($day_6_reg_count[$reg_date] == 0) ? 0 : round($day_6/$day_6_reg_count[$reg_date]*100, $add_point);
					$day_7 = ($day_7_reg_count[$reg_date] == 0) ? 0 : round($day_7/$day_7_reg_count[$reg_date]*100, $add_point);
					$day_14 = ($day_14_reg_count[$reg_date] == 0) ? 0 : round($day_14/$day_14_reg_count[$reg_date]*100, $add_point);
					$day_28 = ($day_28_reg_count[$reg_date] == 0) ? 0 : round($day_28/$day_28_reg_count[$reg_date]*100, $add_point);
					$day_60 = ($day_60_reg_count[$reg_date] == 0) ? 0 : round($day_60/$day_60_reg_count[$reg_date]*100, $add_point);
					$day_90 = ($day_90_reg_count[$reg_date] == 0) ? 0 : round($day_90/$day_90_reg_count[$reg_date]*100, $add_point);
				}
				else
				{				
					$day_1 = ($std_division == 0) ? 0 : round($day_1/$std_division*100, $add_point);
					$day_2 = ($std_division == 0) ? 0 : round($day_2/$std_division*100, $add_point);
					$day_3 = ($std_division == 0) ? 0 : round($day_3/$std_division*100, $add_point);
					$day_4 = ($std_division == 0) ? 0 : round($day_4/$std_division*100, $add_point);
					$day_5 = ($std_division == 0) ? 0 : round($day_5/$std_division*100, $add_point);
					$day_6 = ($std_division == 0) ? 0 : round($day_6/$std_division*100, $add_point);
					$day_7 = ($std_division == 0) ? 0 : round($day_7/$std_division*100, $add_point);
					$day_14 = ($std_division == 0) ? 0 : round($day_14/$std_division*100, $add_point);
					$day_28 = ($std_division == 0) ? 0 : round($day_28/$std_division*100, $add_point);
					$day_60 = ($std_division == 0) ? 0 : round($day_60/$std_division*100, $add_point);
					$day_90 = ($std_division == 0) ? 0 : round($day_90/$std_division*100, $add_point);
				}
				
				$loyal_count = ($std_data == 0) ? 0 : round($loyal_count/$std_data*100, $add_point);
				$install_count = ($std_data == 0) ? 0 : round(100-($install_count/$std_data*100), $add_point);
				$play_count = ($std_data == 0) ? 0 : round(100-($play_count/$std_data*100), $add_point);
				$tutorial_count = ($std_data == 0) ? 0 : round($tutorial_count/$std_data*100, $add_point);
			}
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
	            <td class="tdc"><?= $reg_date ?></td>
	            <td class="tdc"><?= number_format($std_data) ?></td>
	            <td class="tdc"><?= number_format($loyal_count, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($install_count, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($play_count, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($tutorial_count, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_1, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_2, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_3, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_4, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_5, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_6, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_7, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_14, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_28, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_60, $add_point)." $add_per" ?></td>
	            <td class="tdc"><?= number_format($day_90, $add_point)." $add_per" ?></td>
	        </tr>
<?
	}
	
    if(sizeof($retention_detail) == 0)
    {
?>
   			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   				<td class="tdc" colspan="<?= ($search_data == 2) ? 16 : 17?>">검색 결과가 없습니다.</td>
   			</tr>
<?
   	}
   	else
   	{
   		if($search_data == 0)
   		{
   			$sum_loyal_count = round($sum_loyal_count/$sum_reg_count*100, $add_point);
   			$sum_install_count = round(100-($sum_install_count/$sum_reg_count*100), $add_point);
   			$sum_play_count = round(100-($sum_play_count/$sum_reg_count*100), $add_point);
   			$sum_tutorial_count = round($sum_tutorial_count/$sum_reg_count*100, $add_point);
   			$sum_day_1 = ($sum_day_1_reg_count == 0) ? 0 : round($sum_day_1/$sum_day_1_reg_count*100, $add_point);
   			$sum_day_2 = ($sum_day_2_reg_count == 0) ? 0 : round($sum_day_2/$sum_day_2_reg_count*100, $add_point);
   			$sum_day_3 = ($sum_day_3_reg_count == 0) ? 0 : round($sum_day_3/$sum_day_3_reg_count*100, $add_point);
   			$sum_day_4 = ($sum_day_4_reg_count == 0) ? 0 : round($sum_day_4/$sum_day_4_reg_count*100, $add_point);
   			$sum_day_5 = ($sum_day_5_reg_count == 0) ? 0 : round($sum_day_5/$sum_day_5_reg_count*100, $add_point);
   			$sum_day_6 = ($sum_day_6_reg_count == 0) ? 0 : round($sum_day_6/$sum_day_6_reg_count*100, $add_point);
   			$sum_day_7 = ($sum_day_7_reg_count == 0) ? 0 : round($sum_day_7/$sum_day_7_reg_count*100, $add_point);
   			$sum_day_14 = ($sum_day_14_reg_count == 0) ? 0 : round($sum_day_14/$sum_day_14_reg_count*100, $add_point);
   			$sum_day_28 = ($sum_day_28_reg_count == 0) ? 0 : round($sum_day_28/$sum_day_28_reg_count*100, $add_point);
   			$sum_day_60 = ($sum_day_60_reg_count == 0) ? 0 : round($sum_day_60/$sum_day_60_reg_count*100, $add_point);
   			$sum_day_90 = ($sum_day_90_reg_count == 0) ? 0 : round($sum_day_90/$sum_day_90_reg_count*100, $add_point);
   		}
   		else if($search_data == 2)
   		{
   			$sum_loyal_count = round($sum_loyal_count/$sum_reg_count*100, $add_point);
   			$sum_install_count = round(100-($sum_install_count/$sum_reg_count*100), $add_point);
   			$sum_play_count = round(100-($sum_play_count/$sum_reg_count*100), $add_point);
   			$sum_tutorial_count = round($sum_tutorial_count/$sum_reg_count*100, $add_point);
   			$sum_day_1 = ($sum_day_1_reg_count == 0) ? 0 : round($sum_day_1/$sum_day_1_reg_count*100, $add_point);
   			$sum_day_2 = ($sum_day_2_reg_count == 0) ? 0 : round($sum_day_2/$sum_day_2_reg_count*100, $add_point);
   			$sum_day_3 = ($sum_day_3_reg_count == 0) ? 0 : round($sum_day_3/$sum_day_3_reg_count*100, $add_point);
   			$sum_day_4 = ($sum_day_4_reg_count == 0) ? 0 : round($sum_day_4/$sum_day_4_reg_count*100, $add_point);
   			$sum_day_5 = ($sum_day_5_reg_count == 0) ? 0 : round($sum_day_5/$sum_day_5_reg_count*100, $add_point);
   			$sum_day_6 = ($sum_day_6_reg_count == 0) ? 0 : round($sum_day_6/$sum_day_6_reg_count*100, $add_point);
   			$sum_day_7 = ($sum_day_7_reg_count == 0) ? 0 : round($sum_day_7/$sum_day_7_reg_count*100, $add_point);
   			$sum_day_14 = ($sum_day_14_reg_count == 0) ? 0 : round($sum_day_14/$sum_day_14_reg_count*100, $add_point);
   			$sum_day_28 = ($sum_day_28_reg_count == 0) ? 0 : round($sum_day_28/$sum_day_28_reg_count*100, $add_point);
   			$sum_day_60 = ($sum_day_60_reg_count == 0) ? 0 : round($sum_day_60/$sum_day_60_reg_count*100, $add_point);
   			$sum_day_90 = ($sum_day_90_reg_count == 0) ? 0 : round($sum_day_90/$sum_day_90_reg_count*100, $add_point);
   		}
   		else if($search_data == 3)
   		{
   			$sum_loyal_count = round($sum_loyal_count/$sum_reg_count*100, $add_point);
   			$sum_install_count = round(100-($sum_install_count/$sum_reg_count*100), $add_point);
   			$sum_play_count = round(100-($sum_play_count/$sum_reg_count*100), $add_point);
   			$sum_tutorial_count = round($sum_tutorial_count/$sum_reg_count*100, $add_point);
   			$sum_day_1 = ($sum_day_1_reg_count == 0) ? 0 : round($sum_day_1/$sum_day_1_reg_count*100, $add_point);
   			$sum_day_2 = ($sum_day_2_reg_count == 0) ? 0 : round($sum_day_2/$sum_day_2_reg_count*100, $add_point);
   			$sum_day_3 = ($sum_day_3_reg_count == 0) ? 0 : round($sum_day_3/$sum_day_3_reg_count*100, $add_point);
   			$sum_day_4 = ($sum_day_4_reg_count == 0) ? 0 : round($sum_day_4/$sum_day_4_reg_count*100, $add_point);
   			$sum_day_5 = ($sum_day_5_reg_count == 0) ? 0 : round($sum_day_5/$sum_day_5_reg_count*100, $add_point);
   			$sum_day_6 = ($sum_day_6_reg_count == 0) ? 0 : round($sum_day_6/$sum_day_6_reg_count*100, $add_point);
   			$sum_day_7 = ($sum_day_7_reg_count == 0) ? 0 : round($sum_day_7/$sum_day_7_reg_count*100, $add_point);
   			$sum_day_14 = ($sum_day_14_reg_count == 0) ? 0 : round($sum_day_14/$sum_day_14_reg_count*100, $add_point);
   			$sum_day_28 = ($sum_day_28_reg_count == 0) ? 0 : round($sum_day_28/$sum_day_28_reg_count*100, $add_point);
   			$sum_day_60 = ($sum_day_60_reg_count == 0) ? 0 : round($sum_day_60/$sum_day_60_reg_count*100, $add_point);
   			$sum_day_90 = ($sum_day_90_reg_count == 0) ? 0 : round($sum_day_90/$sum_day_90_reg_count*100, $add_point);
   		}
   		else 
   		{
   			$sum_loyal_count = (sizeof($retention_detail) == 0) ? 0 : round($sum_loyal_count/sizeof($retention_detail));
   			$sum_install_count = (sizeof($retention_detail) == 0) ? 0 : round($sum_install_count/sizeof($retention_detail));
   			$sum_play_count = (sizeof($retention_detail) == 0) ? 0 : round($sum_play_count/sizeof($retention_detail));
   			$sum_tutorial_count = (sizeof($retention_detail) == 0) ? 0 : round($sum_tutorial_count/sizeof($retention_detail));
   			$sum_day_1 = ($day_1_row_count == 0) ? 0 : round($sum_day_1/$day_1_row_count);
   			$sum_day_2 = ($day_2_row_count == 0) ? 0 : round($sum_day_2/$day_2_row_count);
   			$sum_day_3 = ($day_3_row_count == 0) ? 0 : round($sum_day_3/$day_3_row_count);
   			$sum_day_4 = ($day_4_row_count == 0) ? 0 : round($sum_day_4/$day_4_row_count);
   			$sum_day_5 = ($day_5_row_count == 0) ? 0 : round($sum_day_5/$day_5_row_count);
   			$sum_day_6 = ($day_6_row_count == 0) ? 0 : round($sum_day_6/$day_6_row_count);
   			$sum_day_7 = ($day_7_row_count == 0) ? 0 : round($sum_day_7/$day_7_row_count);
   			$sum_day_14 = ($day_14_row_count == 0) ? 0 : round($sum_day_14/$day_14_row_count);
   			$sum_day_28 = ($day_28_row_count == 0) ? 0 : round($sum_day_28/$day_28_row_count);
   			$sum_day_60 = ($day_60_row_count == 0) ? 0 : round($sum_day_60/$day_60_row_count);
   			$sum_day_90 = ($day_90_row_count == 0) ? 0 : round($sum_day_90/$day_90_row_count);
   			
   		}
   		
   		$sum_reg_count = round($sum_reg_count/sizeof($retention_detail));
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point_title">평균</td>
	        	<td class="tdc point"><?= number_format($sum_reg_count) ?></td>
	            <td class="tdc point"><?= number_format($sum_loyal_count, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_install_count, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_play_count, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_tutorial_count, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_1, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_2, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_3, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_4, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_5, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_6, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_7, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_14, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_28, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_60, $add_point)." $add_per" ?></td>
	            <td class="tdc point"><?= number_format($sum_day_90, $add_point)." $add_per" ?></td>
	        </tr>
<?
   	}
?>
        </tbody>
	</table>
<?
	}
?>
</div>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>