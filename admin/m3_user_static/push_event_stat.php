<?
	$top_menu = "user_static";
    $sub_menu = "push_event_stat";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	$os_type = ($_GET["os_type"] =="") ? 0 : $_GET["os_type"];
	
	if($search_start_createdate == "")
		$search_start_createdate = date("Y-m-d", strtotime("-4 day"));
	
	if($search_end_createdate == "")
		$search_end_createdate = $today;
	
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "ods-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}
	
	$sql = "SELECT push_code, DATE_FORMAT(start_eventdate,'%Y-%m-%d') AS start_eventdate, os_type, reward_type FROM `tbl_push_event` ". 
			" WHERE start_eventdate >= '$search_start_createdate 00:00:00' ".
			" AND start_eventdate <= '$search_end_createdate 23:59:59' ".
			" ORDER BY start_eventdate DESC";
	$event_list = $db_main2->gettotallist($sql);
?>

<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">

	$(function() {
	    $("#start_createdate").datepicker({ });
	});

	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="push_event_stat.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Push Event 통계</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				Platform : 
				<select name="os_type" id="os_type">
					<option value="0" <?= ($os_type == "0") ? "selected" : "" ?>>ALL</option>
					<option value="1" <?= ($os_type == "1") ? "selected" : "" ?>>IOS</option>
					<option value="2" <?= ($os_type == "2") ? "selected" : "" ?>>Android</option>
					<option value="3" <?= ($os_type == "3") ? "selected" : "" ?>>Amazon</option>
				</select>
				<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
				<input type="button" class="btn_search" value="검색" onclick="search_form.submit()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">Reward Type</th>
					<th class="tdc">Platform</th>
					<th class="tdc">보낸 수</th>
					<th class="tdc">받은 수</th>
					<th class="tdc">보상 지급 금액 / 수</th>
					<th class="tdc">미 보상 유저 수</th>
					<th class="tdc">1주 이상 이탈 결제자</th>
					<th class="tdc">1주 이상 이탈 비결제자 </th>
				</tr>
			</thead>
			<tbody>
<?
			$tmp_row_count = 1;
			for($i=0; $i<sizeof($event_list); $i++)
			{
				$push_code = $event_list[$i]["push_code"];
				$today = $event_list[$i]["start_eventdate"];
				
				$reward_type = $event_list[$i]["reward_type"];
				
				if($os_type == 0)
				{
					$sql = "SELECT SUM(send_count) AS send_count, SUM(reserve_count) AS reserve_count, SUM(reward_amount)AS reward_amount, SUM(amount_push_count) AS amount_push_count, SUM(nonreward_count) AS nonreward_count ".
							", SUM(isnew_count) AS isnew_count, SUM(isreturn_count) AS isreturn_count, SUM(nopay_count) AS nopay_count, SUM(pay_count) AS pay_count".
							" FROM `tbl_push_event_stat_daily2` ". 
							" WHERE SUBSTRING(push_code,5) =  '$push_code' ";
				} 
				else 
				{
					$sql = "SELECT SUM(send_count) AS send_count, SUM(reserve_count) AS reserve_count, SUM(reward_amount)AS reward_amount, SUM(amount_push_count) AS amount_push_count, SUM(nonreward_count) AS nonreward_count ".
							", SUM(isnew_count) AS isnew_count, SUM(isreturn_count) AS isreturn_count, SUM(nopay_count) AS nopay_count, SUM(pay_count) AS pay_count".
							" FROM `tbl_push_event_stat_daily2` ".
							" WHERE SUBSTRING(push_code,5) =  '$push_code' AND os_type =  $os_type";
				}
				$event_data =  $db_analysis -> getarray($sql);
				
				if(sizeof($event_data) > 0)
				{
					$send_count = $event_data[0];
					$reserve_count = $event_data[1];
					$reward_amount = $event_data[2];
					$amount_push_count = $event_data[3];
					$nonreward_count = $event_data[4];
					$isnew_count = $event_data[5];
					$isreturn_count = $event_data[6];
					$nopay_count = $event_data[7];
					$pay_count = $event_data[8];
					
					$os_type_str ="";
					$reward_type_str="";
					
					if($os_type == 1)
						$os_type_str ="IOS";
					else if($os_type == 2)
						$os_type_str ="Android";
					else if($os_type == 3)
						$os_type_str ="Amazon";
					else 
						$os_type_str ="All";
					
					if($reward_type == 1)
						$reward_type_str ="Lucky Wheel";
					else if($reward_type == 2)
						$reward_type_str ="Free Coin";
					else if($reward_type == 3)
						$reward_type_str ="Give A Way";
					else if($reward_type == 10)
						$reward_type_str ="Scratch Card";
					else if($reward_type == 11)
						$reward_type_str ="Slot of Fortune";
					else if($reward_type == 12)
						$reward_type_str ="Golden Egg";
					else if($reward_type == 14)
						$reward_type_str ="Free Spins	";
?>				
				<tr>
					<td class="tdc point"><?= $today ?></td>
					<td class="tdc"><?= $reward_type_str?></td>
					<td class="tdc"><?= $os_type_str?></td>
					<td class="tdc"><?= number_format($send_count) ?></td>
					<td class="tdc"><?= number_format($reserve_count) ?></td>
					<td class="tdc"><?= number_format($reward_amount)." / ".number_format($amount_push_count) ?></td>
					<td class="tdc"><?= number_format($nonreward_count) ?></td>
					<td class="tdc"><?= number_format($pay_count) ?></td>
					<td class="tdc"><?= number_format($nopay_count) ?></td>
				</tr>
<?
				}
			}
?>
			</tbody>
		</table>
	</div>
</form>
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main2->end();
	$db_analysis->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
