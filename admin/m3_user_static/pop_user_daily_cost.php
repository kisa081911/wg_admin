<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	$type = $_POST["type"];
	$year = $_POST["year"];
	$month = $_POST["month"];
	
	$dbaccesstype = $_SESSION["dbaccesstype"];
	
	if($year == "")
		$year = date("Y");
	
	if($month == "")
		$month = date("m");	
	
	$month = str_pad( $month, 2, 0, STR_PAD_LEFT );
	
	$searchdate = $year.$month;

	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT * FROM tbl_user_inflowroute_cost WHERE type=$type AND SUBSTR(today, 1, 6) = '$searchdate'";	
	$cost_list = $db_analysis->gettotallist($sql);
	
	$db_analysis->end();	
?>
<script type="text/javascript">
function update_cost()
{
	var input_form = document.input_form;
	var cost_length = '<?=sizeof($cost_list)?>';
	var cost_value = new Array(cost_length);

	for(i = 0; i < cost_length; i++)
	{
		cost_value[i] = input_form.elements['cost[]'][i].value;
	}		
	
	var param = {};
	param.type = <?=$type?>;
	param.writedate = '<?=$searchdate?>';
    param.cost_value = cost_value;    
    WG_ajax_execute("user/update_daily_inflowrout_cost", param, update_daily_inflowrout_cost_callback, true);
}

function update_daily_inflowrout_cost_callback(result, reason)
{
	 var input_form = document.input_form; 
	 
    if (!result)
    {
        alert("오류 발생 - " + reason);
    }
    else
    {
        window.location.reload(false);
    }
}
</script>
	<div id="layer_wrap">  
    	<div class="layer_header" >
        	<div class="layer_title">COST</div>
        	<img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    	</div>
    	
    	<div class="layer_contents_wrap" style="width:250px;height:900px;">
    		<form name="input_form" id="input_form" method="get" onsubmit="return false;">  
    	
<?
	$count = 1;
	for($i=0; $i<sizeof($cost_list); $i++)
	{
		$today = $cost_list[$i]["today"];
		$cost = $cost_list[$i]["cost"];
?>
              	  <th><?= $today ?></th>
                  <td><input type="text" name="cost[]" class="view_tbl_text" style="width:100px;" value="<?= ($cost == "") ? 0 : $cost ?>"/></td>

	  			  </br> 
<?
	}
	
	if($dbaccesstype == "1" || $dbaccesstype == "3")
	{
?>
				<div style="text-align:right"><button onclick="update_cost();">수정</button></div>
<?
	}
?>
				</form> 
		</div>	
	</div> 