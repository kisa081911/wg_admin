<?
	$top_menu = "user_static";
	$sub_menu = "user_switch_daily_stat";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["start_createdate"];
	$pagename = "user_switch_daily_stat.php";
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today,15,"d");
	$search_start_createdate = ($search_start_createdate == "") ? $before_day : $search_start_createdate;
	$search_end_createdate = ($enddate == "") ? $today : $search_end_createdate;

	$db_main2 = new CDatabase_Main2();
	
	
	$sql = "SELECT * FROM tbl_user_switch_daily WHERE today between  '$search_start_createdate' AND '$search_end_createdate' ". 
			"ORDER BY today DESC;";
	$user_switch_list = $db_main2 ->gettotallist($sql);	
?>

<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
    
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="user_vip_level_stats.php">
	<div class="title_wrap">
	<div class="title"><?= $top_menu_txt ?> &gt; 2.0 전환율 통계</div>
		<div class="search_box">
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	<div id="tab_content_1">
		<table class="tbl_list_basic1"  style="width:1100px">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
			</colgroup>
			<thead>
				<tr>
	        		<th>날짜</th>
	        		<th>A</br> 활동 사용자(웹)</th>
	        		<th>B</br> 2.0 대상 사용자</th>
	        		<th>C</br> 2.0로비 변경 사용자</br>(DAILY로 한번이라도 로비를 본 사용자)</th>
	        		<th>D</br> 2.0 변경 사용자</br>(로그인 시 2.0으로 접속하게 될 사용자)</th>
	            	<th>비율(D/A)</th>
         		</tr>
			</thead>
			<tbody>
<?		
    for($i=0; $i<sizeof($user_switch_list); $i++)
    {
        $today = $user_switch_list[$i]["today"];
        $switch_change_cnt = $user_switch_list[$i]["switch_change_cnt"];
        $switchuser_cnt = $user_switch_list[$i]["switchuser_cnt"];
        $facebookactivecount = $user_switch_list[$i]["facebookactivecount"];
        $switch_targetuser_cnt = $user_switch_list[$i]["switch_targetuser_cnt"];
        $rate = round($switchuser_cnt/$facebookactivecount*100,4);
?>
    			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
    					<td class="tdc point"><?= $today?></td>
    					<td class="tdc point"><?=number_format($facebookactivecount)?></td>
    					<td class="tdc point"><?=number_format($switch_targetuser_cnt)?></td>
    					<td class="tdc point"><?=number_format($switch_change_cnt)?></td>
    					<td class="tdc point"><?=number_format($switchuser_cnt)?></td>
    					<td class="tdc point"><?=number_format($rate,2)?>%</td>
    			</tr>
<?
    }
?>
			</tbody>
		</table>
     </div>
     </form>	
</div>
<!--  //CONTENTS WRAP -->
<?
$db_main2 ->end();
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>