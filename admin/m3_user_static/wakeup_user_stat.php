<?
	$top_menu = "user_static";
	$sub_menu = "wakeup_user_stat";
	
	$check = ($_GET["check"] == "") ? "0" : $_GET["check"];
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$page = ($_GET["page"] == "") ? "1" : $_GET["page"];
	
	$search_send_status = $_GET["send_status"];
	$receiver_idx = $_GET["receiver_idx"];
	$receiver_id = $_GET["receiver_id"];
	$search_update_sd = $_GET["search_update_sd"];
	$search_update_ed = $_GET["search_update_ed"];
	$search_nickname = $_GET["nickname"];
	$search_wakeupidx = ($_GET["wakeupidx"] == "") ? "-1" : $_GET["wakeupidx"];
	
	$listcount = "10";
	$pagename = "wakeup_user_stat.php";
	
	$pagefield = " send_status=$search_send_status&receiver_idx=$receiver_idx&receiver_id=$receiver_id&wakeupidx=$wakeupidx& ".
				 " search_update_sd=$search_update_sd&search_update_ed=$search_update_ed&wakeupidx=$search_wakeupidx";
	
	$tail = "WHERE STATUS=1 ";
	$jointail = "";
	
	if($search_send_status == "-1" || $search_send_status == "")
		$tail .= " AND 1=1 ";
	else if($search_send_status == 0)
		$tail .= " AND send_status = 0 ";
	else if($search_send_status == 1)
		$tail .= " AND send_status = 1 ";
	
	$order_by = " GROUP BY receiver_idx ";
	
	if($receiver_idx != "")
		$tail .= "AND receiver_idx = $receiver_idx ";
	if($search_update_sd != "")
		$tail .= "AND updatedate >= '$search_update_sd 00:00:00' ";
	if($search_update_ed != "")
		$tail .= "AND updatedate <= '$search_update_ed 23:59:59' ";
		
	if($search_nickname != "")
		$jointail = "AND t2.nickname like '%$search_nickname%'";
	
	if($search_wakeupidx != "-1")
		$tail .= "AND wakeupidx =$search_wakeupidx";
		
	$db_friend = new CDatabase_Friend();
	
	$sql = "SELECT count(*) FROM ( ". 
			 " 						SELECT receiver_idx FROM tbl_wakeup_request t1 JOIN `tbl_user_gamedata` t2 ON t2.useridx = t1.receiver_idx $jointail $tail $order_by ) t1";
	$totalcount = $db_friend->getvalue($sql);
	
	$sql = "SELECT MAX(wakeupidx) FROM tbl_wakeup_request ";
	$maxwakeupidx = $db_friend->getvalue($sql);
	
	$sql = " SELECT receiver_idx, wakeupidx, receiver_id, send_status, STATUS, updatedate, t2.nickname ". 
			 " FROM tbl_wakeup_request t1 JOIN `tbl_user_gamedata` t2 ON t2.useridx = t1.receiver_idx $jointail $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;
	$wakeupuser_list = $db_friend->gettotallist($sql);
	
	if ($totalcount < ($page-1) * $listcount && $page != 1)
		$page = floor(($totalcount + $listcount - 1) / $listcount);
		
	$db_friend->end();
?>

<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script>
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}

	function search()
	{
	    var search_form = document.search_form;
	
	    search_form.submit();
	}

	$(function() {
	    $("#search_update_sd").datepicker({ });
	});
	
	$(function() {
	    $("#search_update_ed").datepicker({ });
	});

	function selectAll(source) 
	{
		var holder = document.getElementById( "table_form" );
		var checkboxes = holder.getElementsByClassName( "chkbox" );
		
		for(var i=0; i<checkboxes.length; i++)
		{
			checkboxes[i].checked = source.checked;
		}	
	}

	var bindCheckboxes = ( 
			function() {
				var _last_selected = null;
				
				return function() {
					var holder = document.getElementById( "table_form" );

					if ( holder === null ) 
						return;
					
					var checkboxes = holder.getElementsByClassName( "chkbox" );
					var index = 0;
					
					for ( var i = 0; i < checkboxes.length; ++i ) 
					{
						if ( "checkbox" !== checkboxes[ i ].getAttribute( "type" ) ) 
							continue;
						
						( function( ix ) {
							checkboxes[ ix ].addEventListener( "click", 
									function( event ) {
										var checked = this.checked;
										
										if ( event.shiftKey && ix != _last_selected ) 
										{
											var start = Math.min( ix, _last_selected ), stop = Math.max( ix, _last_selected );
											
											for ( var j = start; j <= stop; ++j ) 
											{
												checkboxes[ j ].checked = checked;
											}

											_last_selected = null;
										} 
										else 
										{
											_last_selected = ix;
										}
									}, false );
		      			})( index );
		      			
		      			index++;
		    		}
					
		  		}
			}
	)();
	document.addEventListener( "DOMContentLoaded", function() { bindCheckboxes( "table_form" );}, false );

	function update_coin_increase_user_status() 
	{
		var holder = document.getElementById( "table_form" );
		var checkboxes = holder.getElementsByClassName( "chkbox" );
		var logidx_list = "";
		
		for(var i=0; i<checkboxes.length; i++)
		{
			if(checkboxes[i].checked == true)
			{
				if(logidx_list == "")
					logidx_list = checkboxes[i].value;
				else
					logidx_list += "," + checkboxes[i].value;
			}
		}

		var param = {};
		param.logidx_list = logidx_list;        
        
		WG_ajax_execute("monitoring/update_coin_increase_user_status", param, update_coin_increase_user_status_callback);
	}
	
	function update_coin_increase_user_status_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else        
        {
            alert("상태를 변경 했습니다.");
            window.location.href = window.location.href;
        }
    }

	function update_abuse_user_debug(useridx, status) 
	{
		var msg = "";
		var comment = "";
		
		if(status == 0)
			msg = "디버그 등록 하시겠습니까?";
		else
			msg = "디버그 해제 하시겠습니까?";
		
		if(confirm(msg) == 0)
			return;

		if(status == 0)
			comment = prompt("Comment를 입력하세요.");


		if(comment == null)
			return;

		var param = {};
		param.useridx = useridx;
		param.status = status;

		if(status == 0)
			param.comment = comment;			
		
		WG_ajax_execute("monitoring/update_abuse_user_debug", param, update_abuse_user_debug_callback);
	}

	function update_abuse_user_debug_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else        
        {
            window.location.href = window.location.href;
        }
    }
</script>
		
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Wakeup 복귀 유저 통계 <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
	</div>
	<!-- //title_warp -->
	
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?=$pagename ?>">
		<div class="detail_search_wrap">
			<input type="hidden" name="check" id="check" value=<?= $check ?> />
			<span class="search_lbl">유입경로</span>
			<select name="send_status" id="send_status">
				<option value="-1" <?= ($search_send_status == "-1" || $search_send_status == "") ? "selected" : "" ?>>전체</option>
				<option value="0" <?= ($search_send_status == "0") ? "selected" : "" ?>>Organic</option>
				<option value="1" <?= ($search_send_status == "1" ) ? "selected" : "" ?>>Wakeup Request</option>
			</select>
			
			&nbsp;
			
			<span class="search_lbl">회차</span>
			<select name="wakeupidx" id="wakeupidx">
				<option value="-1" <?= ($search_wakeupidx == "-1" || $search_send_status == "") ? "selected" : "" ?>>전체</option>
<?
	for($p=1; $p<=$maxwakeupidx; $p++)
	{
?>
				<option value="<?=$p?>" <?= ($search_wakeupidx == "0") ? "selected" : "" ?>><?=$p?></option>
<?	
	}
?>		
			</select>
			
			&nbsp;
			
			<span class="search_lbl">useridx</span>
			<input type="text" class="search_text" id="receiver_idx" name="receiver_idx" style="width:120px" value="<?= encode_html_attribute($search_useridx) ?>" onkeypress="search_press(event)" />&nbsp;
			<span class="search_lbl">이름</span>
			<input type="text" class="search_text" id="nickname" name="nickname" style="width:120px" value="<?= encode_html_attribute($nickname) ?>" onkeypress="search_press(event)" />&nbsp;
			<span class="search_lbl">복귀일</span>
			<input type="text" class="search_text" id="search_update_sd" name="search_update_sd" style="width:65px" value="<?= $search_update_sd ?>" 
				onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
            <input type="text" class="search_text" id="search_update_ed" name="search_update_ed" style="width:65px" value="<?= $search_update_ed ?>" 
            	onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />&nbsp;
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>
    
    <form id="table_form">
		<table name="tbl" class="tbl_list_basic1">
	        <colgroup>
	            <col width="50">
	            <col width="65">
	            <col width="">
	            <col width="">
	            <col width="">
	        </colgroup>
	        
	        <thead>
	      	    <tr>
	                <th>번호</th>
	                <th>회차</th>
	                <th>useridx</th>
	                <th>facebookid</th>
	                <th>이름</th>
	                <th>종류</th>
	                <th>복귀일</th>
	           	</tr>
			</thead> 
			
			<tbody>
<?
		$db_main = new CDatabase_Main();
	
    	for($i=0; $i<sizeof($wakeupuser_list); $i++)
    	{
	     	$receiver_idx = $wakeupuser_list[$i]["receiver_idx"];
	     	$receiver_id = $wakeupuser_list[$i]["receiver_id"];
	      	$send_status = $wakeupuser_list[$i]["send_status"];
	      	$updatedate = $wakeupuser_list[$i]["updatedate"];
	      	$nickname = $wakeupuser_list[$i]["nickname"];
	      	$wakeupidx = $wakeupuser_list[$i]["wakeupidx"];
	      	$send_status_str="";
	      	
	      	if($send_status == "0")
	      		$send_status_str = "Organic";
	      	else if($send_status == "1")
	      		$send_status_str = "Wakeup Request";
	    	
	      	$sql = "SELECT userid,nickname,(SELECT COUNT(*) FROM tbl_user_online_sync2 WHERE useridx=tbl_user.useridx) AS online_statu FROM tbl_user WHERE useridx='$receiver_idx'";
	      	$userinfo = $db_main->getarray($sql);
	      		 
	      	$photourl = get_fb_pictureURL($receiver_id,$client_accesstoken);
?>
				<tr>
					<td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
		            <td class="tdc"><?= $wakeupidx ?></td>
		            <td class="tdc"><?= $receiver_idx ?></td>
		            <td class="tdc"><?= $receiver_id ?></td>
					<td class="point_title"><a href="javascript:view_user_dtl(<?= $receiver_idx ?>,'')"><span style="float:left;padding-top:8px;padding-right:2px;"><img src="/images/icon/<?= ($userinfo["online_status"] == "1") ? "status_1.png" : "status_3.png" ?>" align="absmiddle" /></span><span style="float:left;"><img src="<?= $photourl ?>" height="25" width="25" align="absmiddle" hspace="3"></span><?= $nickname ?>&nbsp;&nbsp;</a><img src="/images/icon/facebook.png" height="20" width="20" align="absmiddle" style="cursor:pointer" onclick="event.cancelBubble=true;window.open('http://www.facebook.com/profile.php?id=<?= $userinfo["userid"] ?>')" /></td>
		            <td class="tdc"><?= $send_status_str ?></td>
		            <td class="tdc"><?= $updatedate ?></td>
<?
		}
		$db_main->end();
?>
		        </tr>
			</tbody>
		</table>
	</form>
	
	<br>
<?
	    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>