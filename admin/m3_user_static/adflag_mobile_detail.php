<?
    $top_menu = "user_static";
    $sub_menu = "adflag_mobile_detail";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    $dbaccesstype = $_SESSION["dbaccesstype"];
    
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    $search_start_orderdate = $_GET["start_orderdate"];
    $search_end_orderdate = $_GET["end_orderdate"];
    $search_os = $_GET["search_os"];
    $search_rejoin = $_GET["search_rejoin"];
    $search_payday = $_GET["payday"];
	$issearch = $_GET["issearch"];
	$select_time = $_GET["select_time"];
	
	if($search_payday != "" && ($search_start_orderdate != "" || $search_end_orderdate != ""))
		error_back("검색값이 잘못되었습니다.");
		
	if ($issearch == "")
	{
		$search_end_createdate  = date("Y-m-d", time() - 60 * 60);
		$search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 6);
		$search_tail = "adflag Like 'mobile%'";
	}
	
	if ($select_time == "")
		$select_time = "marketing";
	
	if($search_os == "1")
		$search_tail = "platform = 1";
	else if($search_os == "2")
		$search_tail = "platform = 2";
	else if($search_os == "3")
		$search_tail = "platform = 3";
	else
		$search_tail = "platform IN (1, 2, 3)";
	
	if($search_rejoin == "")
		$search_rejoin = "0";
	
	function get_adflag_list($summarylist)
	{
		$row_array = array();
		
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if(!in_array($summarylist[$i]["adflag"], $row_array))
				array_push($row_array, $summarylist[$i]["adflag"]);
		}
		
		return $row_array;
	}
		
	function get_today_row_array($summarylist, $today)
	{
		$row_array = array();
	
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if ($summarylist[$i]["day"] == $today)
			{
				array_push($row_array, $summarylist[$i]["adflag"]);
			}
		}
	
		return $row_array;
	}
	
	function get_stat($summarylist, $adflag, $today, $property)
	{
		$stat = 0;
		
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if ($summarylist[$i]["day"] == $today && $summarylist[$i]["adflag"] == $adflag)
			{
				$stat += $summarylist[$i][$property];
			}
		}	
		
		return $stat;
	}
	
	function get_total_stat($summarylist, $adflag, $property)
	{
		$stat = 0;
	
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if ($summarylist[$i]["adflag"] == $adflag)
			{
				$stat += $summarylist[$i][$property];
			}
		}
	
		return $stat;
	}
	
	function get_total_spend($spendlist, $adflag)
	{
		$stat = 0;
		
		for ($i=0; $i<sizeof($spendlist); $i++)
		{
			if ($spendlist[$i]["agencyname"] == $adflag)
			{
				$stat += $spendlist[$i]["spend"];
			}
		}
	
		return $stat;
	}
	

?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#start_orderdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_orderdate").datepicker({ });
	});
	
	function pop_marketing_cost_input_list()
	{
		open_layer_popup("pop_marketing_cost_input.php?basic_date=<?= substr($search_start_createdate, 0, 7) ?>", 230, 620);
	}
</script>

        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
        <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="adflag_mobile_detail.php">
        	<!-- title_wrap -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; Viral 유입 상세 분석(Mobile)</div>
			</div>
			
			<div class="detail_search_wrap" style="height:60px;">
				<div class="floatr">
					<input type=hidden name=issearch value="1">
					<span class="search_lbl">시간 기준 :</span>
                    <input type="radio" value="marketing" name="select_time" id="select_time" <?= ($select_time == "" || $select_time == "marketing") ? "checked=\"true\"" : ""?>/> 마케팅 
					<input type="radio" value="server" name="select_time" id="select_time" <?= ($select_time == "server") ? "checked=\"true\"" : ""?>/> 서버
					&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="search_lbl">OS :</span>
					<select name="search_os" id="search_os">
                		<option value="0" <?= ($search_os=="0") ? "selected" : "" ?>>전체</option>
						<option value="1" <?= ($search_os=="1") ? "selected" : "" ?>>IOS</option>
						<option value="2" <?= ($search_os=="2") ? "selected" : "" ?>>Android</option>
						<option value="3" <?= ($search_os=="3") ? "selected" : "" ?>>Amazon</option>						
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
                    <input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
					<span class="search_lbl">결제액 기준 : 가입 후 </span><input type="text" class="search_text" id="payday" name="payday" style="width:60px" value="<?= $search_payday ?>" onkeypress="search_press(event); return checknum();" />일 이내 결제
					<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
				</div>
					
				<div class="clear" style="height:9px;"></div>
				
				<div class="floatr">
					<span class="search_lbl">재설치자 결제 포함여부 :</span>
					<select name="search_rejoin" id="search_rejoin">
                		<option value="0" <?= ($search_rejoin=="0") ? "selected" : "" ?>>미포함</option>
						<option value="1" <?= ($search_rejoin=="1") ? "selected" : "" ?>>포함</option>				
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" class="search_text" id="start_orderdate" name="start_orderdate" value="<?= $search_start_orderdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
                    <input type="text" class="search_text" id="end_orderdate" name="end_orderdate" value="<?= $search_end_orderdate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <span class="search_lbl">기간내 결제</span>
					<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
				</div>
			</div>
            
            <div class="search_result">
                <span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
                <input type="button" class="btn_03" value="Excel 다운로드" 
                onclick="window.location.href='adflag_mobile_detail_excel.php?start_createdate=<?= $search_start_createdate ?>&end_createdate=<?= $search_end_createdate ?>&start_orderdate=<?= $search_start_orderdate ?>&end_orderdate=<?= $search_end_orderdate ?>&search_os=<?= $search_os ?>&payday=<?= $search_payday ?>&issearch=<?= $issearch ?>&select_time=<?= $select_time ?>&search_rejoin=<?= $search_rejoin?>'">
<?
	if($login_dbaccesstype == "1" || $dbaccesstype == "1" || $login_dbaccesstype == "3" || $dbaccesstype == "3" || $adminid ="kisa0819")
	{
?>
                <input type="button" class="btn_03" value="비용 수기 입력" onclick="pop_marketing_cost_input_list()"/>
<?
	}
?>
            </div>
            
            <div id="tab_content_1" style="width:1200px">
            <table class="tbl_list_basic1" >
            <colgroup>
                <col width="90">
                <col width="120">
                <col width="100">
                <col width="90">
                <col width="90">
                <col width="100">
                <col width="130">
                <col width="130">
                <col width="130">
                <col width="50">
                <col width="50">
                <col width="50">
                <col width="50">
            </colgroup>
            <thead>
            <tr>
                <th>유입일</th>
                <th class="tdr">유입경로</th>
                <th class="tdr">회원가입수<br>(appsflyer)</th>
                <th class="tdr">회원가입수<br>(내부DB)</th>
                <th class="tdr">미게임회원수</th>                
                <th class="tdr">결제회원수</th>
                <th class="tdr">총결제 금액<br>(평균)</th>
                <th class="tdr">ARPPU</th>
                <th class="tdr">비용</th>
                <th class="tdr">ROI</th>
                <th class="tdr">CPI</th>
            </tr>
            </thead>
            <tbody>
<?
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();

    $now = time();
    
    if ($search_end_createdate != "")
    {
    	$now = strtotime($search_end_createdate);
    }
    else
    {
    	$search_end_createdate = date('Y-m-d');
    }
    
    if ($search_start_createdate == "")
    {
    	$search_start_createdate = '2016-11-03';
    }
    
	$sql = "SELECT adflag, ";
	
	if($select_time == "marketing")
		$sql .= "DATE_FORMAT(date_add(createdate, INTERVAL 9 HOUR),'%Y-%m-%d') AS day, ";
	else if($select_time == "server")
		$sql .= "DATE_FORMAT(createdate,'%Y-%m-%d') AS day, ";
	
	$sql .=	"COUNT(*) AS totalcount,".
			"ABS(IFNULL(SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			",0)) AS unplaycount,";
	
	if ($search_payday != "")
	{
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0)) AS payer_count,".
				"(IFNULL(SUM((SELECT IFNULL(ROUND(SUM(facebookcredit)/10,2),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) + IFNULL(SUM((SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0)) AS totalcredit ";
	}
	else if(($search_start_orderdate != "" && $search_end_orderdate != ""))
	{
		$search_start_orderdate_minute = $search_start_orderdate." 00:00:00";
		$search_end_orderdate_minute = $search_end_orderdate." 23:59:59";
	
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute') THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute') THEN 1 ELSE 0 END),0)) AS payer_count,".
				"(IFNULL(SUM((SELECT IFNULL(ROUND(SUM(facebookcredit)/10,2),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0) + IFNULL(SUM((SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0)) AS totalcredit ";
	}
	else
	{
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0)) AS payer_count,".
				"(IFNULL(SUM((SELECT IFNULL(ROUND(SUM(facebookcredit)/10,2),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) + IFNULL(SUM((SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1)),0)) AS totalcredit ";
	}
	
	if($select_time == "marketing")
	{
		$sdate = date('Y-m-d H:i:s', strtotime($search_start_createdate) - 60 * 60 * 9); //(마케팅)-UTC
		$tail =  " AND adflag NOT LIKE 'retention%' AND createdate >='$sdate' AND createdate <='$search_end_createdate 14:59:59' GROUP BY adflag,DATE_FORMAT(date_add(createdate, interval 9 hour),'%Y-%m-%d') ORDER BY day DESC, adflag ASC";

	}
	else if($select_time == "server")
	{
		$sdate = date('Y-m-d H:i:s', strtotime($search_start_createdate)); // 서버 시간
		$tail =  " AND adflag NOT LIKE 'retention%' AND createdate >='$sdate' AND createdate <='$search_end_createdate 23:59:59' GROUP BY adflag,DATE_FORMAT(createdate,'%Y-%m-%d') ORDER BY day DESC, adflag ASC";
	}
		
	$sql .= " FROM tbl_user_ext WHERE $search_tail $tail ";
	write_log($sql);
	$summarylist = $db_main->gettotallist($sql);
	
  	for($i=0; $i<sizeof($summarylist); $i++)
	{
		$today = $summarylist[$i]["day"];
		$adflag = $summarylist[$i]["adflag"];
		
		if($adflag == "")
			$adflag = "viral";
	}
	
	$currenttoday = "";
	
	$tail = "";
	$sum_appsflyer = 0;
	
	if($search_os == "1")
		$tail .= " AND platform = 1 ";
	else if($search_os == "2")
		$tail .= " AND platform = 2 ";
	else if($search_os == "3")
		$tail .= " AND platform = 3 ";
	
	$sql = "SELECT today, agencyname, spend FROM tbl_agency_spend_daily WHERE today >= '$sdate' AND today <= '$search_end_createdate' $tail ORDER BY today DESC, agencyidx ASC";
	$spendlist = $db_main2->gettotallist($sql);
	
	for($i=0; $i<sizeof($summarylist); $i++)
	{
		$today = $summarylist[$i]["day"];
		
		if($currenttoday != $today)
		{
			$currenttoday = $today;
			
			$viral_list = get_today_row_array($summarylist, $today);
			
			for($j=0; $j<sizeof($viral_list); $j++)
			{
				$adflag = $viral_list[$j];
					
				$totalcount = get_stat($summarylist, $adflag, $today, "totalcount");
				$unplaycount = get_stat($summarylist, $adflag, $today, "unplaycount");				
				$payer_count = get_stat($summarylist, $adflag, $today, "payer_count");
				$totalcredit = get_stat($summarylist, $adflag, $today, "totalcredit");
				
				$unplayratio = ($totalcount > 0) ? round($unplaycount * 100 / $totalcount, 2) : 0;
												
				$spend = 0;
				$nojoincount = 0;
				$rejoincount = 0;

				for($k=0; $k<sizeof($spendlist); $k++)
				{
					if(($spendlist[$k]["agencyname"] == $adflag) && ($spendlist[$k]["today"] == $currenttoday))
						$spend += $spendlist[$k]["spend"];
				}
				
				$total_money[$adflag] += $totalcredit;
				$total_payer_count[$adflag] += $payer_count;
				
				$payratio = ($totalcount > 0) ? round($payer_count * 100 / $totalcount, 2) : 0;
				$averagecredit = ($totalcount > 0) ? round($totalcredit / $totalcount, 2) : 0;

				$totalappsflyer = $totalcount;
				$sum_nojoin[$adflag] += $nojoincount;
				$sum_rejoin[$adflag] += $rejoincount;
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($j == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($viral_list)?>" valign="center"><?= $today ?></td>
<?
				} 
?>
					<td class="tdr point"><?= ($adflag == "") ? "viral" : $adflag ?></td>
                    <td class="tdr point"><?= number_format($totalappsflyer) ?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>
                    <td class="tdr point"><?= number_format($unplaycount) ?> (<?= $unplayratio ?> %)</td>                    
                    <td class="tdr point"><?= number_format($payer_count) ?> (<?= $payratio ?> %)</td>
                    <td class="tdr point">$<?= number_format($totalcredit) ?> ($<?= $averagecredit ?>)</td>
                    <td class="tdr point"><?= ($payer_count == "0") ? "-" : number_format($totalcredit / $payer_count, 1) ?></td>
                    <td class="tdr point"><?= ($spend == "0") ? "-" : "$".number_format($spend) ?></td>
                    <td class="tdr point"><?= ($spend == "0") ? "-" : number_format(($totalcredit / $spend) * 100, 1)."%" ?></td>
                    <td class="tdr point"><?= ($totalcount == "0") ? "-" : "$".number_format(($spend / $totalcount), 1) ?></td>
				</tr>
<?
			}
		}
	}
	
	if(sizeof($summarylist) > 0)
	{
		$bigger_viral_list = get_adflag_list($summarylist);
		usort($bigger_viral_list, 'strcasecmp');
		
		for($j=0; $j<sizeof($bigger_viral_list); $j++)
		{
			$adflag = $bigger_viral_list[$j];
			
			$sum_totalcount = get_total_stat($summarylist, $adflag, "totalcount");
			$sum_unplaycount = get_total_stat($summarylist, $adflag, "unplaycount");			
			$sum_payer_count = $total_payer_count[$adflag];
			$sum_totalcredit = $total_money[$adflag];
				
			$sum_unplayratio = ($sum_totalcount > 0) ? round($sum_unplaycount * 100 / $sum_totalcount, 2) : 0;
			$sum_payratio = ($sum_totalcount > 0) ? round($sum_payer_count * 100 / $sum_totalcount, 2) : 0;
			$sum_averagecredit = ($sum_totalcount > 0) ? round($sum_totalcredit / $sum_totalcount, 2) : 0;
			
			$sum_spend = get_total_spend($spendlist, $adflag);
			
			$sum_appsflyer = $sum_totalcount + $sum_nojoin[$adflag] + $sum_rejoin[$adflag];
			$sum_nojoin_rate = ($sum_appsflyer == 0)? "-" : round($sum_nojoin[$adflag]/$sum_appsflyer*100, 1);
			$sum_rejoin_rate = ($sum_appsflyer == 0)? "-" : round($sum_rejoin[$adflag]/$sum_appsflyer*100, 1);
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($j == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($bigger_viral_list)?>" valign="center">Total</td>
<?
				}
?>
					<td class="tdr point"><?= ($adflag == "") ? "viral" : $adflag ?></td>
					<td class="tdr point"><?= number_format($sum_appsflyer) ?></td>
					<td class="tdr point"><?= number_format($sum_nojoin[$adflag]) ?>(<?= $sum_nojoin_rate?>%)</td>
					<td class="tdr point"><?= number_format($sum_rejoin[$adflag]) ?>(<?= $sum_rejoin_rate?>%)</td>
		          	<td class="tdr point"><?= number_format($sum_totalcount) ?></td>
		          	<td class="tdr point"><?= number_format($sum_unplaycount) ?> (<?= $sum_unplayratio ?> %)</td>                    
		          	<td class="tdr point"><?= number_format($sum_payer_count) ?> (<?= $sum_payratio ?> %)</td>
		          	<td class="tdr point">$<?= number_format($sum_totalcredit) ?> ($<?= $sum_averagecredit ?>)</td>
		          	<td class="tdr point"><?= ($sum_payer_count == "0") ? "-" : number_format($sum_totalcredit / $sum_payer_count, 1) ?></td>
		          	<td class="tdr point"><?= ($sum_spend == "0") ? "-" : "$".number_format($sum_spend) ?></td>
		          	<td class="tdr point"><?= ($sum_spend == "0") ? "-" : number_format(($sum_totalcredit / $sum_spend) * 100, 1)."%" ?></td>
		          	<td class="tdr point"><?= ($sum_totalcount == "0") ? "-" : "$".number_format(($sum_spend / $sum_totalcount), 1) ?></td>
			</tr>
<?
		}
	}
?>    
            </tbody>
            </table>
        	</div>
		</form>
	</div>
    <!-- //MAIN WRAP -->
<?
	$db_main->end();
	$db_main2->end();
	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
