<?
	$top_menu = "user_static";
	$sub_menu = "user_honor_level_stats";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["start_createdate"];
	$pagename = "user_vip_level_stats.php";
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today,15,"d");
	$search_start_createdate = ($search_start_createdate == "") ? $before_day : $search_start_createdate;
	$search_end_createdate = ($enddate == "") ? $today : $search_end_createdate;

	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT * ".
			"FROM ".
			"( ".
				"SELECT today, type, honor_level_0, honor_level_1, honor_level_2, honor_level_3, honor_level_4, honor_level_5,  honor_level_6, honor_level_7, honor_level_8, honor_level_9, honor_level_10 ".
				"FROM tbl_honor_level_stat_log WHERE TYPE = 0 AND today between '$search_start_createdate' AND '$search_end_createdate' ". 
				"UNION ALL ". 
				"SELECT today, type, honor_level_0, honor_level_1, honor_level_2, honor_level_3, honor_level_4, honor_level_5,  honor_level_6, honor_level_7, honor_level_8, honor_level_9, honor_level_10 ".
				"FROM tbl_honor_level_stat_log WHERE TYPE = 1 AND today between '$search_start_createdate' AND '$search_end_createdate' ". 
			") t1 ORDER BY today DESC, type ASC;";
	$honor_level_user_info = $db_analysis->gettotallist($sql);	
	
	$sql = "SELECT today, honor_level_0, honor_level_1, honor_level_2, honor_level_3, honor_level_4, honor_level_5,  honor_level_6, honor_level_7, honor_level_8, honor_level_9, honor_level_10 ".
			"FROM tbl_honor_level_stat_log WHERE TYPE = 0 AND today between '$search_start_createdate' AND '$search_end_createdate' ORDER BY today ASC;";
	$honor_level_user_graph_info = $db_analysis->gettotallist($sql);
	
	$honor_level_user_list = array();
	$honor_level_0_list = array();
	$honor_level_1_list = array();
	$honor_level_2_list = array();
	$honor_level_3_list = array();
	$honor_level_4_list = array();
	$honor_level_5_list = array();
	$honor_level_6_list = array();
	$honor_level_7_list = array();
	$honor_level_8_list = array();
	$honor_level_9_list = array();
	$honor_level_10_list = array();
	
	$honor_level_user_list_pointer = sizeof($honor_level_user_graph_info);
	
	$date_pointer = $search_end_createdate;
	
	$loop_count = get_diff_date($search_end_createdate, $search_start_createdate, "d") + 1;
	
	for ($i=0; $i<$loop_count; $i++)
	{
		if ($honor_level_user_list_pointer > 0)
			$today = $honor_level_user_graph_info[$honor_level_user_list_pointer-1]["today"];
	
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$honor_level_user = $honor_level_user_graph_info[$honor_level_user_list_pointer-1];

			$honor_level_user_list[$i] = $date_pointer;
			$honor_level_0_list[$i] = $honor_level_user["honor_level_0"];
			$honor_level_1_list[$i] = $honor_level_user["honor_level_1"];
			$honor_level_2_list[$i] = $honor_level_user["honor_level_2"];
			$honor_level_3_list[$i] = $honor_level_user["honor_level_3"];
			$honor_level_4_list[$i] = $honor_level_user["honor_level_4"];
			$honor_level_5_list[$i] = $honor_level_user["honor_level_5"];
			$honor_level_6_list[$i] = $honor_level_user["honor_level_6"];
			$honor_level_7_list[$i] = $honor_level_user["honor_level_7"];
			$honor_level_8_list[$i] = $honor_level_user["honor_level_8"];
			$honor_level_9_list[$i] = $honor_level_user["honor_level_9"];
			$honor_level_10_list[$i] = $honor_level_user["honor_level_10"];
			
	
			$honor_level_user_list_pointer--;
		}
		else
		{
			$honor_level_user_list[$i] = $date_pointer;
			$honor_level_0_list[$i] = 0;
			$honor_level_1_list[$i] = 0;
			$honor_level_2_list[$i] = 0;
			$honor_level_3_list[$i] = 0;
			$honor_level_4_list[$i] = 0;
			$honor_level_5_list[$i] = 0;
			$honor_level_6_list[$i] = 0;
			$honor_level_7_list[$i] = 0;
			$honor_level_8_list[$i] = 0;
			$honor_level_9_list[$i] = 0;
			$honor_level_10_list[$i] = 0;
		}
	
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
?>

<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});

	google.load("visualization", "1", {packages:["corechart"]});

	function drawChart() 
	{
		var data0 = new google.visualization.DataTable();
		
        data0.addColumn('string', '날짜');
        data0.addColumn('number', 'Level 0');
        data0.addColumn('number', 'Level 10');
        data0.addColumn('number', 'Level 20');
        data0.addColumn('number', 'Level 30');
        data0.addColumn('number', 'Level 40');
        data0.addColumn('number', 'Level 50');
        data0.addColumn('number', 'Level 60');
        data0.addColumn('number', 'Level 70');
        data0.addColumn('number', 'Level 80');
        data0.addColumn('number', 'Level 90');
        data0.addColumn('number', 'Level 100');        
        data0.addRows([
<?
	   	for ($i=sizeof($honor_level_user_list); $i>0; $i--)
	    {
	    	$_date = $honor_level_user_list[$i-1];
	        $_level_0 = $honor_level_0_list[$i-1];
	        $_level_1 = $honor_level_1_list[$i-1];
	        $_level_2 = $honor_level_2_list[$i-1];
	        $_level_3 = $honor_level_3_list[$i-1];
	        $_level_4 = $honor_level_4_list[$i-1];
	        $_level_5 = $honor_level_5_list[$i-1];
	        $_level_6 = $honor_level_6_list[$i-1];
	        $_level_7 = $honor_level_7_list[$i-1];
	        $_level_8 = $honor_level_8_list[$i-1];
	        $_level_9 = $honor_level_9_list[$i-1];
	        $_level_10 = $honor_level_10_list[$i-1];       
	        
	        echo("['".$_date."',".$_level_0.",".$_level_1.",".$_level_2.",".$_level_3.",".$_level_4.",".$_level_5.",".$_level_6.",".$_level_7.",".$_level_8.",".$_level_9.",".$_level_10."]");
			
	        if ($i > 1)
	            echo(",");
	    }
?>
        ]);


		var options = {                                                   
	            width:1050,                         
	            height:200,
	            axisTitlesPosition:'in',
	            curveType:'none',
	            focusTarget:'category',
	            interpolateNulls:'true',
	            legend:'top',
	            fontSize : 12,
	            chartArea:{left:80,top:40,width:1020,height:130}
	    };
	
		var chart = new google.visualization.LineChart(document.getElementById('chart_data0'));
	    chart.draw(data0, options);

	}

    google.setOnLoadCallback(drawChart);
    
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="user_honor_level_stats.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; honor 인원 통계</div>
		<div class="search_box">
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">honor 인원 통계 </span>
	<div id="tab_content_1">
		<table class="tbl_list_basic1"  style="width:1100px">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">				
			</colgroup>
			<thead>
				<tr>
	        		<th>일   자</th>
	        		<th>기   준</th>
	            	<th>Fame 0~9</th>
	            	<th>Fame 10~19</th>
	            	<th>Fame 20~29</th>
	            	<th>Fame 30~39</th>
	            	<th>Fame 40~49</th>
	            	<th>Fame 50~59</th>
	            	<th>Fame 60~69</th>
	            	<th>Fame 70~79</th>
	            	<th>Fame 80~89</th>
	            	<th>Fame 90~99</th>
	            	<th>Fame 100</th>
	            	<th>합계</th>            	
         		</tr>
			</thead>
			<tbody>
<?		
    for($i=0; $i<sizeof($honor_level_user_info); $i++)
    {
    		$today = $honor_level_user_info[$i]["today"];
    		$type = $honor_level_user_info[$i]["type"];
    		$honor_level_0 = $honor_level_user_info[$i]["honor_level_0"];
    		$honor_level_1 = $honor_level_user_info[$i]["honor_level_1"];
    		$honor_level_2 = $honor_level_user_info[$i]["honor_level_2"];
    		$honor_level_3 = $honor_level_user_info[$i]["honor_level_3"];
    		$honor_level_4 = $honor_level_user_info[$i]["honor_level_4"];
    		$honor_level_5 = $honor_level_user_info[$i]["honor_level_5"];
    		$honor_level_6 = $honor_level_user_info[$i]["honor_level_6"];
    		$honor_level_7 = $honor_level_user_info[$i]["honor_level_7"];
    		$honor_level_8 = $honor_level_user_info[$i]["honor_level_8"];
    		$honor_level_9 = $honor_level_user_info[$i]["honor_level_9"];
    		$honor_level_10 = $honor_level_user_info[$i]["honor_level_10"];

    		$sum_honor_level = $honor_level_0 + $honor_level_1 + $honor_level_2 + $honor_level_3 + $honor_level_4 + $honor_level_5 + $honor_level_6 + $honor_level_7 + $honor_level_8 + $honor_level_9 + $honor_level_10; 
    		
    		if($today != $honor_level_user_info[$i-1]["today"])
    		{
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
					<td class="tdc point" rowspan="2"><?= $today?></td>
<?
    		}
					if($type == 0)
					{
?>
						<td class="tdc point">인원</td>
						<td class="tdc point"><?=number_format($honor_level_0)?></td>
						<td class="tdc point"><?=number_format($honor_level_1)?></td>
						<td class="tdc point"><?=number_format($honor_level_2)?></td>
						<td class="tdc point"><?=number_format($honor_level_3)?></td>
						<td class="tdc point"><?=number_format($honor_level_4)?></td>
						<td class="tdc point"><?=number_format($honor_level_5)?></td>
						<td class="tdc point"><?=number_format($honor_level_6)?></td>
						<td class="tdc point"><?=number_format($honor_level_7)?></td>
						<td class="tdc point"><?=number_format($honor_level_8)?></td>
						<td class="tdc point"><?=number_format($honor_level_9)?></td>
						<td class="tdc point"><?=number_format($honor_level_10)?></td>
						<td class="tdc point"><?=number_format($sum_honor_level)?></td>	
<?
						$sum_honor_level = 0;
					}
					
					if($type == 1)
					{	
?>
						<td class="tdc point">2주 이탈자</td>
						<td class="tdc point"><?=number_format($honor_level_0)?></td>
						<td class="tdc point"><?=number_format($honor_level_1)?></td>
						<td class="tdc point"><?=number_format($honor_level_2)?></td>
						<td class="tdc point"><?=number_format($honor_level_3)?></td>
						<td class="tdc point"><?=number_format($honor_level_4)?></td>
						<td class="tdc point"><?=number_format($honor_level_5)?></td>
						<td class="tdc point"><?=number_format($honor_level_6)?></td>
						<td class="tdc point"><?=number_format($honor_level_7)?></td>
						<td class="tdc point"><?=number_format($honor_level_8)?></td>
						<td class="tdc point"><?=number_format($honor_level_9)?></td>
						<td class="tdc point"><?=number_format($honor_level_10)?></td>
						<td class="tdc point"><?=number_format($sum_honor_level)?></td>						
<?
						$sum_honor_level = 0;
					}
					
			if($today != $honor_level_user_info[$i-1]["today"])
			{
?>
				</tr>
<?
			}
    }
?>
			</tbody>
		</table>
     </div>
      
	  <br><br><br><br>
	 <div id="tab_content_1">
     	<div class="h2_title">[Graph]</div>
	 	<div id="chart_data0" style="height:230px; min-width: 500px"></div>
	 </div>       
     </form>	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_analysis->end();
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>