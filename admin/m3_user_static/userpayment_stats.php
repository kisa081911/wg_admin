<?
	$top_menu = "user_static";
	$sub_menu = "userpayment_stats";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_type = ($_GET["type"] == "") ? "1" : $_GET["type"];
	$search_subtype = ($_GET["subtype"]="")? 100 : $_GET["subtype"];
	$search_subtype2 = ($_GET["subtype2"]="")? 200 : $_GET["subtype2"];
	$search_subtype3 = ($_GET["subtype3"]="")? 300 :$_GET["subtype3"];
	$search_os = ($_GET["os"] == "") ? "0" : $_GET["os"];
	
	if($_GET["start_date"] == "")
		$search_sdate = date("Y-m-d",strtotime("-7 days"));
	else
		$search_sdate = $_GET["start_date"];
	
	if($_GET["end_date"] == "")
		$search_edate = date("Y-m-d");
	else
		$search_edate = $_GET["end_date"];
	
	$db_table = "tbl_user_playstat_gather_daily";
	
	if($search_os == "1")
		$db_table = "tbl_user_playstat_gather_daily_ios";
	else if($search_os == "2")
		$db_table = "tbl_user_playstat_gather_daily_android";
	else if($search_os == "3")
		$db_table = "tbl_user_playstat_gather_daily_amazon";
	else if($search_os == "4")
		$db_table = "tbl_user_playstat_gather_daily_web";
	
	$db_other = new CDatabase_Other();
	
	$start_date = str_replace("-","",$search_date);
	
	if($search_subtype != "" || $search_subtype2 != "" || $search_subtype3 != "")
	{
		if($search_type == "1")
			$statcode = $search_subtype;
		else if($search_type == "2")
			$statcode = $search_subtype2;
		else if($search_type == "3")
			$statcode = $search_subtype3;
		
		$tail = " AND statcode = $statcode";
	}
	
	$sql = "SELECT * FROM $db_table WHERE '$search_sdate' <= today AND today <= '$search_edate' AND LEFT(statcode, 1) = $search_type $tail ORDER BY today DESC, statcode ASC";
	
	$daylist = $db_other->gettotallist($sql);
	
	$db_other->end();
?>
<link type="text/css" href="/js/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>  
<script type="text/javascript">
	$(function() {
		$("#start_date").datepicker({ });
		$("#end_date").datepicker({ });
	});

	function search()
	{
		var search_form = document.search_form;
	    
		if (search_form.start_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.start_date.focus();
	    	return;
		} 
	
		if (search_form.end_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.end_date.focus();
	    	return;
		} 
	
		search_form.submit();
	}

	function change_type()
    {
        var type = document.getElementById("type");
        var subtype = document.getElementById("subtype");
        var subtype2 = document.getElementById("subtype2");
        var subtype3 = document.getElementById("subtype3");

        if (type.value == "1")
        {
            subtype.style.display = "";
            subtype2.style.display = "none";
            subtype3.style.display = "none";
            
            subtype2.value = "";
            subtype3.value = "";
        }
        else if(type.value == "2")
        {
        	subtype.style.display = "none";
            subtype2.style.display = "";
            subtype3.style.display = "none";

            subtype.value = "";
            subtype3.value = "";
        }
        else if(type.value == "3")
        {
        	subtype.style.display = "none";
        	subtype2.style.display = "none";
            subtype3.style.display = "";

            subtype.value = "";
            subtype2.value = "";
        }
    }
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 사용자 유형별 통계 </div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<select name="os" id="os">
					<option value="0" <?= ($search_os=="0") ? "selected" : "" ?>>Facebook</option>
					<option value="1" <?= ($search_os=="1") ? "selected" : "" ?>>iOS</option>
					<option value="2" <?= ($search_os=="2") ? "selected" : "" ?>>Android</option>
					<option value="3" <?= ($search_os=="3") ? "selected" : "" ?>>Amazon</option>
                </select>&nbsp;
                
                <select name="type" id="type" onchange="change_type()">
					<option value="1" <?= ($search_type=="1") ? "selected" : "" ?>>전체</option>
					<option value="2" <?= ($search_type=="2") ? "selected" : "" ?>>결제자</option>
					<option value="3" <?= ($search_type=="3") ? "selected" : "" ?>>비결제자</option>
                </select>&nbsp;
                
                 <select name="subtype" id="subtype" style="<?= ($search_type == 1) ? "" : "display:none"?>";>
                 	<option value="" <?= ($search_subtype=="") ? "selected" : "" ?>>선택하세요</option>
					<option value="100" <?= ($search_subtype=="100") ? "selected" : "" ?>>전체 평균</option>
					<option value="101" <?= ($search_subtype=="101") ? "selected" : "" ?>>VIP lv0 평균</option>
					<option value="102" <?= ($search_subtype=="102") ? "selected" : "" ?>>VIP lv1 평균</option>
					<option value="103" <?= ($search_subtype=="103") ? "selected" : "" ?>>VIP lv2 평균</option>
					<option value="104" <?= ($search_subtype=="104") ? "selected" : "" ?>>VIP lv3 평균</option>
					<option value="105" <?= ($search_subtype=="105") ? "selected" : "" ?>>VIP lv4 평균</option>
					<option value="106" <?= ($search_subtype=="106") ? "selected" : "" ?>>VIP lv5 평균</option>
					<option value="107" <?= ($search_subtype=="107") ? "selected" : "" ?>>VIP lv6 평균</option>
					<option value="108" <?= ($search_subtype=="108") ? "selected" : "" ?>>VIP lv7 평균</option>
					<option value="109" <?= ($search_subtype=="109") ? "selected" : "" ?>>VIP lv8 평균</option>
					<option value="110" <?= ($search_subtype=="110") ? "selected" : "" ?>>VIP lv9 평균</option>
					<option value="111" <?= ($search_subtype=="111") ? "selected" : "" ?>>VIP lv10 평균</option>
					<option value="197" <?= ($search_subtype=="197") ? "selected" : "" ?>>무 결제자</option>
					<option value="198" <?= ($search_subtype=="198") ? "selected" : "" ?>>$39 이상 결제자</option>
					<option value="199" <?= ($search_subtype=="199") ? "selected" : "" ?>>$499 이상 결제자</option>
                </select>&nbsp;
                <select name="subtype2" id="subtype2" style="<?= ($search_type == 2) ? "" : "display:none"?>">
                	<option value="" <?= ($search_subtype2=="") ? "selected" : "" ?>>선택하세요</option>
					<option value="200" <?= ($search_subtype2=="200") ? "selected" : "" ?>>전체 평균</option>
					<option value="201" <?= ($search_subtype2=="201") ? "selected" : "" ?>>VIP lv0 평균</option>
					<option value="202" <?= ($search_subtype2=="202") ? "selected" : "" ?>>VIP lv1 평균</option>
					<option value="203" <?= ($search_subtype2=="203") ? "selected" : "" ?>>VIP lv2 평균</option>
					<option value="204" <?= ($search_subtype2=="204") ? "selected" : "" ?>>VIP lv3 평균</option>
					<option value="205" <?= ($search_subtype2=="205") ? "selected" : "" ?>>VIP lv4 평균</option>
					<option value="206" <?= ($search_subtype2=="206") ? "selected" : "" ?>>VIP lv5 평균</option>
					<option value="207" <?= ($search_subtype2=="207") ? "selected" : "" ?>>VIP lv6 평균</option>
					<option value="208" <?= ($search_subtype2=="208") ? "selected" : "" ?>>VIP lv7 평균</option>
					<option value="209" <?= ($search_subtype2=="209") ? "selected" : "" ?>>VIP lv8 평균</option>
					<option value="210" <?= ($search_subtype2=="210") ? "selected" : "" ?>>VIP lv9 평균</option>
					<option value="211" <?= ($search_subtype2=="211") ? "selected" : "" ?>>VIP lv10 평균</option>
					<option value="298" <?= ($search_subtype2=="298") ? "selected" : "" ?>>$39 이상 결제자</option>
					<option value="299" <?= ($search_subtype2=="299") ? "selected" : "" ?>>$499 이상 결제자</option>
                </select>&nbsp;
                <select name="subtype3" id="subtype3" style="<?= ($search_type == 3) ? "" : "display:none"?>">
                	<option value="300" <?= ($search_subtype3=="300") ? "selected" : "" ?>>전체 평균</option>
					<option value="301" <?= ($search_subtype3=="301") ? "selected" : "" ?>>VIP lv0 평균</option>
					<option value="302" <?= ($search_subtype3=="302") ? "selected" : "" ?>>VIP lv1 평균</option>
					<option value="303" <?= ($search_subtype3=="303") ? "selected" : "" ?>>VIP lv2 평균</option>
					<option value="304" <?= ($search_subtype3=="304") ? "selected" : "" ?>>VIP lv3 평균</option>
					<option value="305" <?= ($search_subtype3=="305") ? "selected" : "" ?>>VIP lv4 평균</option>
					<option value="306" <?= ($search_subtype3=="306") ? "selected" : "" ?>>VIP lv5 평균</option>
					<option value="307" <?= ($search_subtype3=="307") ? "selected" : "" ?>>VIP lv6 평균</option>
					<option value="308" <?= ($search_subtype3=="308") ? "selected" : "" ?>>VIP lv7 평균</option>
					<option value="309" <?= ($search_subtype3=="309") ? "selected" : "" ?>>VIP lv8 평균</option>
					<option value="310" <?= ($search_subtype3=="310") ? "selected" : "" ?>>VIP lv9 평균</option>
					<option value="311" <?= ($search_subtype3=="311") ? "selected" : "" ?>>VIP lv10 평균</option>
					<option value="398" <?= ($search_subtype3=="398") ? "selected" : "" ?>>$39 이상 결제자</option>
					<option value="399" <?= ($search_subtype3=="399") ? "selected" : "" ?>>$499 이상 결제자</option>
                </select>&nbsp;
				<span class="search_lbl">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="start_date" name="start_date" style="width:70px" readonly="readonly" value="<?= $search_sdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/> ~
				<input type="text" class="search_text" id="end_date" name="end_date" style="width:70px" readonly="readonly" value="<?= $search_edate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/>
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->

	<div class="search_result">
		<span><?= $search_sdate ?></span> ~ <span><?= $search_edate ?></span> 현황입니다
	</div>
	
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="">
			<col width="">
			<col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
		</colgroup>
        <thead>
        	<tr>
        		<th>일자</th>
        		<th>유형</th>
            	<th>회원수</th>
             	<th>현재코인</th>               	
               	<th>플레이코인(만)</th>
               	<th>플레이시간</th>
               	<th>승률</th>
               	<th>로스트코인(만)</th>
               	<th>결제금액</th>
               	<th>구매코인(만)</th>
               	<th>days after install</th>
               	<th>days afer purchase</th>
         	</tr>
        </thead>
        <tbody>
<?
    for($i=0; $i<sizeof($daylist); $i++)
    {
    	$today = $daylist[$i]["today"];
    	$statcode = $daylist[$i]["statcode"];
    	$usercnt = $daylist[$i]["usercount"];
    	$currentcoin = $daylist[$i]["currentcoin"];
    	$playamount = $daylist[$i]["moneyin"];
    	$playtime = $daylist[$i]["playtime"];
    	$winrate = $daylist[$i]["winrate"];
    	$purchasecoin = $daylist[$i]["purchase_coin"];
    	$lostcoin = $daylist[$i]["lostcoin"];
    	$purchasecredit = $daylist[$i]["purchase_usd"];
    	$purchase_passed = $daylist[$i]["purchase_passed"];
    	$install_passed = $daylist[$i]["install_passed"];
    	
    	$stat_type = $statcode[0];
	$stat_subtype = substr($statcode, 1, 2);
    	
    	if($stat_type == 1)
    		$user_type = "";
    	else if($stat_type == 2)
    		$user_type = "결제자";
    	else if($stat_type == 3)
    		$user_type = "비결제자";
    	
    	
	    	switch($stat_subtype)
	    	{
	    		case "00":
	    			$statcode = "전체 평균";
	    			break;
	    		case "01":
	    			$statcode = "VIP lv0 평균";
	    			break;
	    		case "02":
	    			$statcode = "VIP lv1 평균";
	    			break;
	    		case "03":
	    			$statcode = "VIP lv2 평균";
	    			break;
	    		case "04":
	    			$statcode = "VIP lv3 평균";
	    			break;
	    		case "05":
	    			$statcode = "VIP lv4 평균";
	    			break;
	    		case "06":
	    			$statcode = "VIP lv5 평균";
	    			break;
	    		case "07":
	    			$statcode = "VIP lv6 평균";
	    			break;
	    		case "08":
	    			$statcode = "VIP lv7 평균";
	    			break;
	    		case "09":
	    			$statcode = "VIP lv8 평균";
	    			break;
	    		case "10":
	    			$statcode = "VIP lv9 평균";
	    			break;
	    		case "11":
	    			$statcode = "VIP lv10 평균";
	    			break;
	    		case "95":
	    			$statcode = "프리미엄 평균";
	    			break;
	    		case "96":
	    			$statcode = "미 프리미엄 평균";
	    			break;
	    		case "97":
	    			$statcode = "비 결제자";
	    			break;
	    		case "98":
	    			$statcode = "$39 이상 결제자";
	    			break;
	    		case "99":
	    			$statcode = "$499 이상 결제자";
	    			break;
	    	}
    	
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
			<td class="tdc point"><?= $today?></td>
			<td class="tdc point"><?= $user_type." ".$statcode?></td>
            <td class="tdr"><?= number_format($usercnt) ?></td>
            <td class="tdr"><?= number_format($currentcoin) ?></td>
            <td class="tdr"><?= number_format($playamount) ?></td>
            <td class="tdc"><?= number_format($playtime) ?></td>
            <td class="tdc"><?= round($winrate, 2) ?>%</td>
            <td class="tdr"><?= number_format($lostcoin) ?></td>
            <td class="tdr">$<?= number_format($purchasecredit) ?></td>
            <td class="tdr"><?= number_format($purchasecoin) ?></td>
            <td class="tdr"><?= number_format($install_passed) ?></td>
            <td class="tdr"><?= number_format($purchase_passed) ?></td>
        </tr>
<?
    }
    
    if(sizeof($daylist) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="12">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>
	
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>