<?
    $top_menu = "user_static";
    $sub_menu = "pay_user_stats";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $os_term = ($_GET["os_term"] == "") ? "4" : $_GET["os_term"];
    $viewmode = ($_GET["viewmode"] == "") ? "0" : $_GET["viewmode"];
    
    if ($os_term != "0" && $os_term != "1" && $os_term != "2" && $os_term != "3" && $os_term != "4")
    	error_back("잘못된 접근입니다.");
    
    $startdate = $_GET["startdate"];
    $enddate = $_GET["enddate"];
        
    $today = date("Y-m-d", strtotime("-1 day"));
    $before_day = get_past_date($today,7,"d");
    
    $startdate = ($startdate == "") ? $before_day : $startdate;
    $enddate = ($enddate == "") ? $today : $enddate;
    	
    $db_otherdb = new CDatabase_Other();
    
    $table = "tbl_payer_login_stat";
    
    $orderby_tail="ASC";
    
    if($viewmode == 1)
    	$orderby_tail="DESC";
    
    if($os_term == "4")
    {
    	$tail = "";
    	$os_txt = "Total";
    }
    else
    {
    	if($os_term == "0")
    	{
    		$tail = "AND platform = 0";
    		$os_txt = "Web";
    	}
    	else if($os_term == "1")
    	{
			$tail = "AND platform = 1";
			$os_txt = "iOS";
    	}
    	else if($os_term == "2")
    	{
			$tail = "AND platform = 2";
			$os_txt = "Android";
    	}
    	else if($os_term == "3")
    	{
			$tail = "AND platform = 3";
			$os_txt = "Amazon";
    	}
    }
    
    if($os_term == 4)
    {
		$sql = "SELECT today, SUM(firstbuy_cnt) AS firstbuy_cnt, SUM(leaveday_7) AS leaveday_7, SUM(leaveday_14) AS leaveday_14, SUM(leaveday_28) AS leaveday_28, ".
				"SUM(retention_7) AS retention_7, SUM(retention_14) AS retention_14, SUM(retention_28) AS retention_28 ".  
				"FROM `tbl_payer_login_stat` ". 
				"WHERE today BETWEEN '$startdate' AND '$enddate' ". 
				"GROUP BY today ". 
				"ORDER BY today $orderby_tail ";
    }
    else
    	$sql = "SELECT * FROM `tbl_payer_login_stat` WHERE today BETWEEN '$startdate' AND '$enddate' $tail ORDER BY today $orderby_tail";
    
    $data_list = $db_otherdb->gettotallist($sql);
    
    $date_list = array();
    $firstbuy_list = array();
    $leave_week_list = array();
    $leave_2week_list = array();
    $leave_4week_list = array();
    $retention_week_list = array();
    $retention_2week_list = array();
    $retention_4week_list = array();
    
    $list_pointer = sizeof($data_list);
    
    $date_pointer = $enddate;
     
    for ($i=0; $i<get_diff_date($enddate, $startdate, "d"); $i++)
    {
    	if ($list_pointer > 0)
    		$today = $data_list[$list_pointer-1]["today"];
    
    		if (get_diff_date($date_pointer, $today, "d") == 0)
    		{
    			$payer_log = $data_list[$list_pointer-1];
    				
    			$date_list[$i] = $date_pointer;
    			$firstbuy_list[$i] = $payer_log["firstbuy_cnt"];
    			$leave_week_list[$i] = $payer_log["leaveday_7"];
    			$leave_2week_list[$i] = $payer_log["leaveday_14"];
    			$leave_4week_list[$i] = $payer_log["leaveday_28"];
    			$retention_week_list[$i] = $payer_log["retention_7"];
    			$retention_2week_list[$i] = $payer_log["retention_14"];
    			$retention_4week_list[$i] = $payer_log["retention_28"];
    				
    			$list_pointer--;
    		}
    		else
    		{
    			$date_list[$i] = $date_pointer;
    			$firstbuy_list[$i] = 0;
    			$leave_week_list[$i] = 0;
    			$leave_2week_list[$i] = 0;
    			$leave_4week_list[$i] = 0;
    			$retention_week_list[$i] = 0;
    			$retention_2week_list[$i] = 0;
    			$retention_4week_list[$i] = 0;
    		}
    
    		$date_pointer = get_past_date($date_pointer, 1, "d");
    }
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});

	function drawChart() 
	{
	    var data1 = new google.visualization.DataTable();
	    
	    data1.addColumn('string', '날짜');
	    data1.addColumn('number', '첫결제자');
	    data1.addRows([
<?
	for ($i=sizeof($date_list); $i>0; $i--)
	{
		$date = $date_list[$i-1];
		$fisrtbuy_count = $firstbuy_list[$i-1];
		
		echo("['".$date."'");
		
        if ($fisrtbuy_count != "")
            echo(",{v:".$fisrtbuy_count.",f:'".number_format($fisrtbuy_count)."'}]");
        else
            echo(",0]");
        
        if ($i > 1)
			echo(",");
	}
?>     
	    ]);
	
		var data2 = new google.visualization.DataTable();
	    
	    data2.addColumn('string', '날짜');
	    data2.addColumn('number', '7일 이탈자');
	    data2.addColumn('number', '7일 복귀자');
	    data2.addRows([
<?
	for ($i=sizeof($date_list); $i>0; $i--)
	{
		$date = $date_list[$i-1];
		$leave_count = $leave_week_list[$i-1];
		$retention_count = $retention_week_list[$i-1];
		$fisrtbuy_count = $firstbuy_list[$i-1];
		
		echo("['".$date."'");
		
        if ($leave_count != "")
            echo(",{v:".$leave_count.",f:'".number_format($leave_count)."'}");
        else
            echo(",0");
        
		if ($retention_count != "")
			echo(",{v:".$retention_count.",f:'".number_format($retention_count)."'}]");
		else
			echo(",0]");
        
        if ($i > 1)
			echo(",");
	}
?>     
	    ]);
	
		var data3 = new google.visualization.DataTable();
	    
	    data3.addColumn('string', '날짜');
	    data3.addColumn('number', '14일 이탈자');
	    data3.addColumn('number', '14일 복귀자');
	    data3.addRows([
<?
	for ($i=sizeof($date_list); $i>0; $i--)
	{
		$date = $date_list[$i-1];
		$leave_count = $leave_2week_list[$i-1];
		$retention_count = $retention_2week_list[$i-1];
		
		echo("['".$date."'");
		
        if ($leave_count != "")
            echo(",{v:".$leave_count.",f:'".number_format($leave_count)."'}");
        else
            echo(",0");
        
		if ($retention_count != "")
			echo(",{v:".$retention_count.",f:'".number_format($retention_count)."'}]");
		else
			echo(",0]");
        
        if ($i > 1)
			echo(",");
	}
?>     
	    ]);
	
		var data4 = new google.visualization.DataTable();
	    
	    data4.addColumn('string', '날짜');
	    data4.addColumn('number', '28일 이탈자');
	    data4.addColumn('number', '28일 복귀자');
	    data4.addRows([
<?
	for ($i=sizeof($date_list); $i>0; $i--)
	{
		$date = $date_list[$i-1];
		$leave_count = $leave_4week_list[$i-1];
		$retention_count = $retention_4week_list[$i-1];
		
		echo("['".$date."'");
		
        if ($leave_count != "")
            echo(",{v:".$leave_count.",f:'".number_format($leave_count)."'}");
        else
            echo(",0");
        
		if ($retention_count != "")
			echo(",{v:".$retention_count.",f:'".number_format($retention_count)."'}]");
		else
			echo(",0]");
        
        if ($i > 1)
			echo(",");
	}
?>     
	    ]);

		var data5 = new google.visualization.DataTable();
	    
	    data5.addColumn('string', '날짜');
	    data5.addColumn('number', '7일 복귀자+첫결제자-7일 이탈자');
	    data5.addRows([
<?
	for ($i=sizeof($date_list); $i>0; $i--)
	{
		$date = $date_list[$i-1];
		$leave_count = $leave_week_list[$i-1];
		$retention_count = $retention_week_list[$i-1];
		$firstbuy_count = $firstbuy_list[$i-1];
		
		$complex_count = $retention_count + $firstbuy_count - $leave_count;
		
		echo("['".$date."'");
        
		if ($complex_count != "")
			echo(",{v:".$complex_count.",f:'".number_format($complex_count)."'}]");
		else
			echo(",0]");
        
        if ($i > 1)
			echo(",");
	}
?>     
	    ]);
	        
	    var options = {
	            title:'',                                                      
	            width:1050,                         
	            height:200,
	            axisTitlesPosition:'in',
	            curveType:'none',
	            focusTarget:'category',
	            interpolateNulls:'true',
	            legend:'top',
	            fontSize : 12,
	            chartArea:{left:80,top:40,width:1020,height:130}
	    };
	
	    var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
	    chart.draw(data1, options);
	
	    chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
	    chart.draw(data2, options);
	    
	    chart = new google.visualization.LineChart(document.getElementById('chart_div3'));
	    chart.draw(data3, options);
	
	    chart = new google.visualization.LineChart(document.getElementById('chart_div4'));
	    chart.draw(data4, options);

	    chart = new google.visualization.LineChart(document.getElementById('chart_div5'));
	    chart.draw(data5, options);
	}
	
	google.setOnLoadCallback(drawChart);

	function change_os_term(term)
	{
		var search_form = document.search_form;

		var total = document.getElementById("term_total");
		var web = document.getElementById("term_web");
		var ios = document.getElementById("term_ios");
		var android = document.getElementById("term_android");
		var amazon = document.getElementById("term_amazon");
		
		document.search_form.os_term.value = term;
	
		if (term == "0")
		{
			total.className="btn_schedule";
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "1")
		{
			total.className="btn_schedule";
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "2")
		{
			total.className="btn_schedule";
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (term == "3")
		{
			total.className="btn_schedule";
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
		else if (term == "4")
		{
			total.className="btn_schedule_select";
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
	
		search_form.submit();
	}

	function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }

    function search()
    {
        var search_form = document.search_form;

        search_form.submit();
    }
    
    $(function() {
        $("#startdate").datepicker({ });
    });
    
    $(function() {
        $("#enddate").datepicker({ });
    });
</script>

        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        		
        		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
		        	<input type="button" class="<?= ($os_term == "4") ? "btn_schedule_select" : "btn_schedule" ?>" value="Total" id="term_total" onclick="change_os_term('4')"    />
					<input type="button" class="<?= ($os_term == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="term_web" onclick="change_os_term('0')"    />
					<input type="button" class="<?= ($os_term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_os_term('1')" />
					<input type="button" class="<?= ($os_term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_os_term('2')"    />
					<input type="button" class="<?= ($os_term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_os_term('3')"    />
				</span>
        		
	            <!-- title_warp -->
	            <div class="title_wrap">
	                <div class="title"><?= $top_menu_txt ?> &gt; 결제 이탈/복귀 현황 (<?= $os_txt ?>)</div>
	                
	                <form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
        			<input type="hidden" name="os_term" id="os_term" value="<?= $os_term ?>" />
					
					<div class="search_box">
						<span class="search_lbl ml20">View Mode&nbsp;&nbsp;&nbsp;</span>
						<select name="viewmode" id="viewmode">
							<option value="0" <?= ($viewmode == "0") ? "selected" : "" ?>>그래프</option>
							<option value="1" <?= ($viewmode == "1") ? "selected" : "" ?>>리스트</option>
						</select>
						&nbsp;&nbsp;&nbsp;
	                    <input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
	                    <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
	                     <input type="button" class="btn_search" value="검색" onclick="search()" />
	                </div>
	                
	                </form>
	            </div>
	            <!-- //title_warp -->
            
            <div class="search_result">
            	<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다
            </div>
<?
	if($viewmode == 0)
	{
?>
            <div class="h2_title">[첫결제자]</div>
	    	<div id="chart_div1" style="height:230px; min-width: 500px"></div>
	    	
	    	<div class="h2_title">[7일 복귀자+첫결제자-7일 이탈자]</div>
	    	<div id="chart_div5" style="height:230px; min-width: 500px"></div>
            
            <div class="h2_title">[7일 복귀/이탈자]</div>
	    	<div id="chart_div2" style="height:230px; min-width: 500px"></div>
	    	
	    	<div class="h2_title">[14일 복귀/이탈자]</div>
	    	<div id="chart_div3" style="height:230px; min-width: 500px"></div>
            
            <div class="h2_title">[28일 복귀/이탈자]</div>
	    	<div id="chart_div4" style="height:230px; min-width: 500px"></div>
<?		
	}
	else 
	{
?>
			<table class="tbl_list_basic1" style="width:100%">
				<colgroup>
					<col width="">
					<col width="">
					<col width="">
					<col width="">
		            <col width="">
		            <col width="">
				</colgroup>
		        <thead>
		        	<tr>
		        		<th rowspan="2" style="border-right: 1px solid #dbdbdb;border-left: 1px solid #dbdbdb;">일자</th>
		            	<th rowspan="2" style="border-right: 1px solid #dbdbdb;">첫결제자</th>
		            	<th rowspan="2" style="border-right: 1px solid #dbdbdb;">7일 복귀자+첫결제자-7일 이탈자</th>
		            	<th colspan="2" style="border-right: 1px solid #dbdbdb;">7일 복귀/이탈자</th>	
		            	<th colspan="2" style="border-right: 1px solid #dbdbdb;">14일 복귀/이탈자</th>
		            	<th colspan="2" style="border-right: 1px solid #dbdbdb;">28일 복귀/이탈자</th>
		         	</tr>
		         	<tr>
		               	<th>7일이탈자</th>
		               	<th style="border-right: 1px solid #dbdbdb;">7일복귀자</th>
		               	<th>14일이탈자</th>
		               	<th style="border-right: 1px solid #dbdbdb;">14일복귀자</th>
		               	<th>28일이탈자</th>
		               	<th style="border-right: 1px solid #dbdbdb;">28일복귀자</th>
		         	</tr>
		        </thead>
		        <tbody>
		<?
			for($k=0;$k<sizeof($data_list);$k++)
			{
				$today = $data_list[$k]["today"];
				$firstbuy_cnt = $data_list[$k]["firstbuy_cnt"];
				$leaveday_7 = $data_list[$k]["leaveday_7"];
				$leaveday_14 = $data_list[$k]["leaveday_14"];
				$leaveday_28 = $data_list[$k]["leaveday_28"];
				$retention_7 = $data_list[$k]["retention_7"];
				$retention_14 = $data_list[$k]["retention_14"];
				$retention_28 = $data_list[$k]["retention_28"];
				$complex_count = $retention_7 + $firstbuy_cnt - $leaveday_7;
		?>
					<tr>
						<td class="tdc"><?=$today?></td>
						<td class="tdc"><?=number_format($firstbuy_cnt)?></td>
		               	<td class="tdc"><?=number_format($complex_count)?></td>
		               	<td class="tdc"><?=number_format($leaveday_7)?></td>
		               	<td class="tdc"><?=number_format($retention_7)?></td>
		               	<td class="tdc"><?=number_format($leaveday_14)?></td>
		               	<td class="tdc"><?=number_format($retention_14)?></td>
		               	<td class="tdc"><?=number_format($leaveday_28)?></td>
						<td class="tdc"><?=number_format($retention_28)?></td>
					</tr>
		<?		
			}
		?>
					
		        </tbody>
		    </table>
<?
	}
?>
        
    	</div>
    	<!-- //CONTENTS WRAP -->
<?
	$db_otherdb->end();
	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>