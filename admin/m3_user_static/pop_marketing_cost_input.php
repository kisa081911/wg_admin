<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");

	$dbaccesstype = $_SESSION["dbaccesstype"];
	
	$is_retention = $_GET["is_retention"];
	$basic_date = $_GET["basic_date"];
	
	if(basic_date == "")
		$basic_date = date("Y").'-'.date("m");
	
	$basic_date = explode("-", $basic_date);
	
	$basic_year = $basic_date[0];
	$basic_month = $basic_date[1];
	
	$spend_value = explode(",", $_POST["spend_value"]);
	$platform = $_GET["platform"];
	$agency_index = $_GET["agency_index"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>비용입력</title>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script>
<script type="text/javascript">
	function change_platform(platform)
	{
		var agency_title = document.getElementById("agency_title");
		var agency_index_1 = document.getElementById("agency_index_1");
		var agency_index_2 = document.getElementById("agency_index_2");
		
		
		if(platform == 1 || platform == 2)
		{
			agency_title.style.display = "";
			agency_index_1.style.display = "";
			agency_index_2.style.display = "none";
		}
		else if (platform == 3)
		{
			agency_title.style.display = "";
			agency_index_1.style.display = "none";
			agency_index_2.style.display = "";
		}
		else
		{
			agency_title.style.display = "none";
			agency_index_1.style.display = "none";
			agency_index_2.style.display = "none";
		}
	}

	function change_agency()
	{
		var spend_length = submit_form.elements['spend[]'].length;
		var spend_value = new Array(spend_length);
		
 		for(i = 0; i < spend_length; i++)
 		{
 			spend_value[i] = submit_form.elements['spend[]'][i].disabled = true;
 		}

 		document.getElementById("update_btn").disabled = true;
	}

	function get_agency_spend()
	{
		var input_form = document.input_form;

		var basic_year = input_form.basic_year.value;
		var basic_month = input_form.basic_month.value;
		var platform = input_form.platform.value;
		var agency_index = 0;
		var ttm_input = document.getElementById("ttm_input");
		
		if(platform == 1 || platform == 2)
		{
			agency_index = input_form.agency_index_1.value;
			
			if(agency_index == 41)
			{
				ttm_input.style.display = "";
			}
		}
		else if(platform == 3)
			agency_index = input_form.agency_index_2.value;
		
		if(basic_year == "")
		{
			alert("기준년을 입력하세요.");
			input_form.basic_year.focus();
			return;
		}
		if(basic_month == "")
		{
			alert("기준월을 입력하세요.");
			input_form.basic_month.focus();
			return;
		}

		if(platform == "")
		{
			alert("platform을 선택하세요.");
			input_form.platform.focus();
			return;
		}
		if(agency_index == "")
		{
			alert("agency를 선택하세요.");
			return;
		}

		if(!basic_month.match(/0.*/))
		{
			if(basic_month < 10)
			{
				basic_month = "0" + basic_month;
			}
		}
		
		var param = {};
		param.basic_date =  basic_year + "-" + basic_month);
		param.platform = platform;
		param.agency_index = agency_index;
		param.is_retention = <?= $is_retention ?>;

		SendListRequest("user/get_agency_spend", param, get_agency_spend_callback);

		param = new HashMap();
		param.put("platform", platform);
		
		SendQueryRequest("user/get_ttm_value", param, get_ttm_value_callback);
	}

	function get_agency_spend_callback(result, reason, totalcount, list)
	{
		if (!result)
		{
			alert("오류 발생 - " + reason);
		}
		else
		{
			var tbody = document.getElementById("agency_contents");

			while (tbody.childNodes.length > 0)
                tbody.removeChild(tbody.childNodes[0]);

            for(var i=0; i<list.length; i++)
            {
                var tr = document.createElement("tr");
                var td = document.createElement("td");
                var th = document.createElement("th");
                
                var inputtxt = document.createElement('INPUT');
                inputtxt.name = 'spend[]';
                inputtxt.id = 'spend[]';
                inputtxt.type = 'text';
                inputtxt.value = list[i].get("spend");
                inputtxt.size = '8';
                inputtxt.style.textAlign = 'right';
                
                td.className = "tdc";
                td.innerText = list[i].get("today");
                tr.appendChild(td);
                
				td = document.createElement("td");
				td.className = "tdr";
				td.appendChild(inputtxt);
				tr.appendChild(td);
				
                tbody.appendChild(tr);
            }

            var spend_length = submit_form.elements['spend[]'].length;
    		var spend_value = new Array(spend_length);
    		
     		for(i = 0; i < spend_length; i++)
     		{
     			spend_value[i] = submit_form.elements['spend[]'][i].disabled = false;
     		}

     		document.getElementById("update_btn").disabled = false;
		}
	}

	function get_ttm_value_callback(result, reason, map)
    {
    	if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
        	var ttm = map.get("ttm_value");
    		document.getElementById("ttm_value").value = ttm;
        }
    }

	function update_agency_spend()
	{
		var input_form = document.input_form;
		var submit_form = document.submit_form;

		var platform = input_form.platform.value;
		var agency_index = 0;

		if(platform == 1 || platform == 2)
			agency_index = input_form.agency_index_1.value;
		else if(platform == 3)
			agency_index = input_form.agency_index_2.value;
		
		var spend_length = submit_form.elements['spend[]'].length;
		var spend_value = new Array(spend_length);

 		for(i = 0; i < spend_length; i++)
 		{
 	 		if(submit_form.elements['spend[]'][i].value == "")
 	 		{
 	 	 		alert("비용을 입력해주세요.");
 	 	 		submit_form.elements['spend[]'][i].focus();
 	 	 		return;
 	 		}
 			spend_value[i] = submit_form.elements['spend[]'][i].value;
 		}

 		if(confirm("수정하시겠습니까?") == 0)
			return;
 		
 	    var param = new HashMap();
	    param.put("basic_date", input_form.basic_year.value + "-" + input_form.basic_month.value);
  	    param.put("platform", platform);
  	    param.put("agency_index", agency_index);
  	  	param.put("spend_value", spend_value);
  		param.put("is_retention", <?= $is_retention ?>);
 	    
 	    SendExecuteRequest("user/update_agency_spend", param, update_agency_spend_callback);
	}

	function update_agency_spend_callback(result, reason)
	{
	    if (!result)
	    {
	        alert("오류 발생 - " + reason);
	    }
	    else
	    {
		    alert("수정 완료되었습니다.");
		    get_agency_spend();
	    }
	}

	function layer_close()
	{
	    window.parent.close_layer_popup("<?= $_GET["layer_no"] ?>");
	}

	function update_ttm_value()
	{
		var ttm_value = document.getElementById("ttm_value").value;

		var param = new HashMap();
  	    param.put("ttm_value", ttm_value);
 	    
 	    SendExecuteRequest("user/update_ttm_value", param, update_ttm_value_callback);
	}

	function update_ttm_value_callback(result, reason)
	{
		 if (!result)
	    {
	        alert("오류 발생 - " + reason);
	    }
	    else
	    {
		    alert("수정 완료되었습니다.");
		    get_agency_spend();
	    }
	}
</script>
</head>

<body class="layer_body" style="width:230px" onload="window.focus()" onkeyup="if (getEventKeycode(event) == 27) { layer_close(); }">
	<div id="layer_wrap" style="width:230px;height:450px;">
		<br/>
		<form name="input_form" id="input_form" method="get" onsubmit="return false;">
		    platform
		    <select name="platform" id="platform" onchange="change_platform(this.value);">
		    	<option value="">선택하세요</option>
		        <option value="1">iOS</option>
		        <option value="2">Android</option>
		        <option value="3">Amazon</option>
		    </select>
		    <br/>
		    <text name="agency_title" id="agency_title" style="display:none">agency</text>
		    <select name="agency_index_1" id="agency_index_1" style="display:none" onchange="change_agency()">
		    	<option value="">선택하세요</option>
<?
	if($is_retention == 0)
	{
?>
		        <option value="10">258angel_int</option>
		        <option value="12">cheetahmobile_int</option>
		        <option value="13">mobvista_int</option>
		        <option value="14">yeahmobi_int</option>
		        <option value="15">socialclicks</option>
		        <option value="16">adaction_int</option>
		        <option value="17">Twitter Ads</option>
		        <option value="18">taptica_int</option>
		        <option value="19">glispa_int</option>
		        <option value="22">googleadwords_int</option>
		        <option value="23">moloco_int</option>
		        <option value="24">heyzap_int</option>
		        <option value="26">crossinstall_int</option>
		        <option value="29">iconpeak_int</option>
		        <option value="30">dqna_int</option>
		        <option value="31">Apple Search Ads</option>
		        <option value="32">discovry_int</option>
		        <option value="33">loopme_int</option>
		        <option value="34">startapp_int</option>
		        <option value="36">swaymobile_int</option>
		        <option value="39">liftoff_int</option>
		        <option value="40">youappi_int</option>
		        <option value="41">nend_int</option>
		        <option value="42">performancerevenues_int</option>
		        <option value="43">adaction2_int</option>
		        <option value="44">liniad_int</option>
		        <option value="45">crobo_int</option>
		        <option value="46">feedmob_int</option>
		        <option value="49">maio_int</option>
		        <option value="50">manage_int</option>
		        <option value="51">avazu_int</option>
		        <option value="53">meettab_int</option>
		        <option value="54">mobair_int</option>
		        <option value="55">mobrain_int</option>
		        <option value="56">raftika_int</option>
		        <option value="57">koneo_int</option>
		        <option value="58">Persona.ly_int</option>
		        <option value="59">motive_int</option>
		        <option value="60">bigabid_int</option>
		        <option value="61">seccosquared_int</option>
<?
	}
	else if($is_retention == 1)
	{
?>
				<option value="1">remerge_int</option>
				<option value="2">dataliftretargeting_int</option>
<?
	}
?>
		    </select>
		    <select name="agency_index_2" id="agency_index_2" style="display:none">
		    	<option value="">선택하세요</option>
		        <option value="11">amazon</option>
		    </select>
		    <br/>
		    <input type="text" class="search_text" id="basic_year" name="basic_year" value="<?= $basic_year ?>" maxlength="10" style="width:30px" />년
		    <input type="text" class="search_text" id="basic_month" name="basic_month" value="<?= $basic_month ?>" maxlength="10" style="width:30px" />월
		    <input type="button" class="btn_02" onclick="get_agency_spend()" value="조회"/>
	    </form>
	    <div class="layer_header" >
	    	<div class="layer_title" id="agency_name">
	    		<div id="ttm_input" name="ttm_input" style="display:none;">
		    		<text>TTM : </text>
		    		<input type="text" id="ttm_value" name="ttm_value" value="0" maxlength="8" style="width:50px"/>
		    		<input type="button" class="btn_02" style="height:20px;" onclick="update_ttm_value()" value="변경"/>
		    	</div>
	    	</div>
	    	
	    	<img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="layer_close()" />	
	    </div>
    	
    	<div class="layer_contents_wrap" style="width:200px;height:450px; overflow-y:auto;">
    		<form name="submit_form" id="submit_form" method="get" onsubmit="return false;">
				<table>
					<thead>
						<tr>
							<th>날짜</th>
							<th>비용</th>
						</tr>
					</thead>
					<tbody id="agency_contents"></tbody>
				</table>
    		</form>
    	</div>
<?
	if($dbaccesstype == "1" || $dbaccesstype == "3")
	{   
?>
		<div style="text-align:right"><input id="update_btn" type="button" class="btn_02" onclick="update_agency_spend();" value="수정"></div>
<?
	}
?>
    </div>
</body>