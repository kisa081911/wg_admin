<?
	$top_menu = "user_static";
	$sub_menu = "user_inflowroute_stats_daily";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$db_analysis = new CDatabase_Analysis();
	
	if($_GET["start_date"] == "")
	{
		$sql = "SELECT MAX(today) FROM tbl_user_inflowroute_daily";
		$start_date = $db_analysis->getvalue($sql);
		
		$search_date = substr($start_date, 0, 4)."-".substr($start_date, 4, 2)."-".substr($start_date, 6, 2);		
	}
	else
	{
		$search_date = $_GET["start_date"];		
		$start_date = str_replace("-","",$search_date);
	}	
		
	$day_month = array(4, 6, 9, 11);	
	
	$sql = "SELECT today, regmonth, SUBSTR(today, 7, 2) AS regday, SUBSTR(regmonth, 6, 2) AS month FROM tbl_user_inflowroute_daily WHERE today = '$start_date' AND regmonth >= '2015-11' AND type = 0 ORDER BY today DESC, regmonth ASC";
	$web_list = $db_analysis->gettotallist($sql);
	
 	$sql = "SELECT today, COUNT(regmonth) AS regmonth ".
			"FROM tbl_user_inflowroute_daily ".
			"WHERE today = '$start_date' AND regmonth >= '2015-11' AND type = 0 ".
			"GROUP BY today ORDER BY today DESC, regmonth ASC";
 	$web_sumlist = $db_analysis->gettotallist($sql);
 	
 	$sql = "SELECT today, regmonth, SUBSTR(today, 7, 2) AS regday, SUBSTR(regmonth, 6, 2) AS month FROM tbl_user_inflowroute_daily WHERE today = '$start_date' AND regmonth >= '2014-11' AND type = 2 ORDER BY today DESC, regmonth ASC";
 	$android_list = $db_analysis->gettotallist($sql);
 	
 	$sql = "SELECT today, COUNT(regmonth) AS regmonth ".
 			"FROM tbl_user_inflowroute_daily ".
 			"WHERE today = '$start_date' AND regmonth >= '2015-11' AND type = 2 ".
 			"GROUP BY today ORDER BY today DESC, regmonth ASC";
 	$android_sumlist = $db_analysis->gettotallist($sql);
 	
 	$sql = "SELECT today, regmonth, SUBSTR(today, 7, 2) AS regday, SUBSTR(regmonth, 6, 2) AS month FROM tbl_user_inflowroute_daily WHERE today = '$start_date' AND regmonth >= '2014-11' AND type = 1 ORDER BY today DESC, regmonth ASC";
 	$ios_list = $db_analysis->gettotallist($sql);
 	
 	$sql = "SELECT today, COUNT(regmonth) AS regmonth ".
 			"FROM tbl_user_inflowroute_daily ".
 			"WHERE today = '$start_date' AND regmonth >= '2015-11' AND type = 1 ".
 			"GROUP BY today ORDER BY today DESC, regmonth ASC";
 	$ios_sumlist = $db_analysis->gettotallist($sql);
 	
 	$sql = "SELECT today, regmonth, SUBSTR(today, 7, 2) AS regday, SUBSTR(regmonth, 6, 2) AS month FROM tbl_user_inflowroute_daily WHERE today = '$start_date' AND regmonth >= '2014-11' AND type = 3 ORDER BY today DESC, regmonth ASC";
 	$amazon_list = $db_analysis->gettotallist($sql);
 	
 	$sql = "SELECT today, COUNT(regmonth) AS regmonth ".
 			"FROM tbl_user_inflowroute_daily ".
 			"WHERE today = '$start_date' AND regmonth >= '2015-11' AND type = 3 ".
 			"GROUP BY today ORDER BY today DESC, regmonth ASC";
 	$amazon_sumlist = $db_analysis->gettotallist($sql);
 	
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
    	$("#start_date").datepicker({ });
	});

	function search()
    {
        var search_form = document.search_form;
            
        if (search_form.start_date.value == "")
        {
            alert("기준일을 입력하세요.");
            search_form.start_date.focus();
            return;
        } 

        search_form.submit();
    }

	function pop_user_daily_cost_list(type)
	{		
		try
		{
			var request = $.ajax({
	    		url: "pop_user_daily_cost.php",
				type: "POST",
				data: {type:type,year:<?=substr($start_date,0,4)?>,month:<?=substr($start_date,4,2)?>},
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 사용자 유지현황(일간) </div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<span class="search_lbl">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="start_date" name="start_date" style="width:65px" readonly="readonly" value="<?= $search_date ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/>
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->

	<div class="search_result">
		<span><?= $search_date ?></span> 통계입니다.		
	</div>
	
	<div class="search_result">Web&nbsp;&nbsp;<a href="javascript:pop_user_daily_cost_list(0);">+cost</a></div>
	<table class="tbl_list_basic1" style="width:1300px">
		<colgroup>
			<col width="50">
			<col width="100">			
            <col width="80">            
            <col width="100">
            <col width="100">
            <col width="80">
            <col width="100">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="70">
            <col width="70">
            <col width="70">
		</colgroup>
        <thead>
        	<tr>
        		<th rowspan="2">일자</th>
            	<th colspan="2">일반현황</th>
            	<th colspan="7">누적</th>
            	<th colspan="7">최근 2주</th>
         	</tr>
         	<tr>
         		<th>가입월일</th>
             	<th>가입자수</th>               	
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>비용</th>
               	<th>ROI</th>
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>비용</th>
               	<th>ROI</th>
         	</tr>
        </thead>
        <tbody>
<?
	$rowcnt = 0;
	$j = 1;
	$summonth_daily_cost = 0;
	$summonth_daily_2week_cost = 0;
		
    for($i=0; $i<sizeof($web_list); $i++)
    {
    	$today = $web_list[$i]["today"];
    	$regmonth = $web_list[$i]["regmonth"];
    	$regday = $web_list[$i]["regday"];
    	$month = $web_list[$i]["month"];
    	
    	if($month == 2 && ($regday == 31 || $regday == 30 || $regday == 29))
    	{
    		$regday = 28;
    	}
    	else if(in_array($month, $day_month) && $regday == 31)
    	{
    		$regday = 30;
    	}
    	
    	$regdate = str_replace("-", "", $regmonth).$regday;
    	$first_regdate = str_replace("-", "", $regmonth)."01";
    	$search_2week_date = date("Ymd", strtotime($regmonth."-".$regday) - 86400*14);    	
   		
   		$sql = "SELECT t1.today as today, t1.type as type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week, IFNULL(cost, 0) AS cost, IFNULL(cost_2week, 0) AS cost_2week ".
   				"FROM (SELECT * FROM tbl_user_inflowroute_daily WHERE today = '$regdate' AND regmonth = '".$regmonth."' AND TYPE = 0 ORDER BY today DESC, regmonth ASC) t1 ".
   				"LEFT JOIN ".   				
   				"(SELECT today, (SELECT IFNULL(SUM(cost), 0) FROM `tbl_user_inflowroute_cost` WHERE type = 0 AND '$first_regdate' <= today  AND today <= '$regdate') AS cost, ".
   				"(SELECT IFNULL(SUM(cost), 0) FROM `tbl_user_inflowroute_cost` WHERE type = 0 AND '$search_2week_date' <= today  AND today <= '$regdate') AS cost_2week ".
   				"FROM `tbl_user_inflowroute_cost` WHERE type = 0 AND today = '$regdate') t2 ON t1.today=t2.today;";
   		
    	$web_list_detail = $db_analysis->getarray($sql);    	
    	
    	$regmonth = $web_list_detail["regmonth"];
    	$joincount = $web_list_detail["joincount"];
    	$activecount = $web_list_detail["activecount"];
    	$webcredit = $web_list_detail["webcredit"];
    	$mobilecredit = $web_list_detail["mobilecredit"];
    	$totalcredit = $webcredit + $mobilecredit;
    	$mobilerate = ($totalcredit == 0) ? 0 : round(($mobilecredit / $totalcredit) * 100, 2);
    	$avgcredit = ($joincount == 0) ? 0 : round(($totalcredit / $joincount), 2);
    	$webcredit2week = $web_list_detail["webcredit2week"];
    	$mobilecredit2week = $web_list_detail["mobilecredit2week"];
    	$daily_cost = $web_list_detail["cost"];
    	$daily_2week_cost = $web_list_detail["cost_2week"];
    	$totalcredit2week = $webcredit2week + $mobilecredit2week;
    	$mobile2weekrate = ($totalcredit2week == 0) ? 0 : round(($mobilecredit2week / $totalcredit2week) * 100, 2);
    	$avgcredit2week = ($activecount == 0) ? 0 : round(($totalcredit2week / $activecount), 2);
    	
    	$summonth_daily_cost += $daily_cost;
    	$summonth_daily_2week_cost += $daily_2week_cost;
    	$daily_roi = ($daily_cost == 0) ? 0 : round($totalcredit / $daily_cost * 100, 2);
    	$daily_2week_roi = ($daily_2week_cost == 0) ? 0 : round($totalcredit2week / $daily_2week_cost * 100, 2);

     	$monthcnt = $web_sumlist[$rowcnt]["regmonth"];
     	
     	$sumjoincount += $joincount;
     	$sumactivecount += $activecount;
     	$sumwebcredit += $webcredit;
     	$summobilecredit += $mobilecredit;
     	$sumwebcredit2week += $webcredit2week;
     	$summobilecredit2week += $mobilecredit2week;

?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
			if($j == 1)
			{
?>
				<td class="tdr point" rowspan="<?= $monthcnt?>"><?= $today?></td>
<?
			}
			
			if($regmonth != "")
			{
?>
	            <td class="tdc"><?= $regmonth."-".$regday ?></td>
	            <td class="tdr"><?= number_format($joincount) ?></td>            
	            <td class="tdr">$<?= number_format($webcredit, 2) ?></td>
	            <td class="tdr">$<?= number_format($mobilecredit, 2) ?></td>
	            <td class="tdr"><?= number_format($mobilerate, 2) ?>%</td>
	            <td class="tdr">$<?= number_format($totalcredit, 2) ?></td>
	            <td class="tdr">$<?= number_format($avgcredit, 2) ?></td>
	            <td class="tdr">$<?= number_format($daily_cost) ?></td>
	            <td class="tdr"><?= number_format($daily_roi, 2) ?>%</td>
	            <td class="tdr">$<?= number_format($webcredit2week, 2) ?></td>
	            <td class="tdr">$<?= number_format($mobilecredit2week, 2) ?></td>
	            <td class="tdr"><?= number_format($mobile2weekrate, 2) ?>%</td>
	            <td class="tdr">$<?= number_format($totalcredit2week, 2) ?></td>
	            <td class="tdr">$<?= number_format($avgcredit2week, 2) ?></td>
	            <td class="tdr">$<?= number_format($daily_2week_cost) ?></td>
	            <td class="tdr"><?= number_format($daily_2week_roi, 2)?>%</td>
<?
			}
?>
        </tr>
<?
		if($j == $monthcnt)
		{
			$sumtotalcredit = $sumwebcredit + $summobilecredit;
			$summobilerate = ($sumtotalcredit == 0) ? 0 : round(($summobilecredit / $sumtotalcredit) * 100, 2);
			$sumavgcredit = ($sumjoincount == 0) ? 0 : round(($sumtotalcredit / $sumjoincount), 2);
			$sumtotalcredit2week = $sumwebcredit2week + $summobilecredit2week;
			$summobile2weekrate = ($sumtotalcredit2week == 0) ? 0 : round(($summobilecredit2week / $sumtotalcredit2week) * 100, 2);
			$sumavgcredit2week = ($sumactivecount == 0) ? 0 : round(($sumtotalcredit2week / $sumactivecount), 2);
			$sumdailyroi = ($summonth_daily_cost == 0) ? 0 : round($sumtotalcredit / $summonth_daily_cost * 100, 2);
			$sumdaily2weekroi = ($summonth_daily_2week_cost == 0) ? 0 : round($sumtotalcredit2week / $summonth_daily_2week_cost * 100, 2);
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point_title" colspan="2">합계</td>
            	<td class="tdr point"><?= number_format($sumjoincount) ?></td>            	
            	<td class="tdr point">$<?= number_format($sumwebcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobilerate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summonth_daily_cost) ?></td>
           		<td class="tdr point"><?= number_format($sumdailyroi, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumwebcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit2week, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobile2weekrate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summonth_daily_2week_cost) ?></td>
           		<td class="tdr point"><?= number_format($sumdaily2weekroi, 2) ?>%</td>
			</tr>
<?
			$j = 1;
			$rowcnt++;
    	}
    	else
    	{
    		$j++;	
    	}
    }
    
    if(sizeof($web_list) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="16">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>
	
	<br/><br/>
	<div class="search_result">IOS&nbsp;&nbsp;<a href="javascript:pop_user_daily_cost_list(1);">+cost</a></div>
	
	<table class="tbl_list_basic1" style="width:1300px">
		<colgroup>
			<col width="50">
			<col width="100">			
            <col width="80">            
            <col width="100">
            <col width="100">
            <col width="80">
            <col width="100">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="70">
            <col width="70">
            <col width="70">
		</colgroup>
        <thead>
        	<tr>
        		<th rowspan="2">일자</th>
            	<th colspan="2">일반현황</th>
            	<th colspan="7">누적</th>
            	<th colspan="7">최근 2주</th>
         	</tr>
         	<tr>
         		<th>가입월일</th>
             	<th>가입자수</th>               	
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>비용</th>
               	<th>ROI</th>
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>비용</th>
               	<th>ROI</th>
         	</tr>
        </thead>
        <tbody>
<?
	$rowcnt = 0;
	$j = 1;
	$summonth_cost = 0;
	$summonth_cost = 0;
	$sumjoincount = 0;
	$sumactivecount = 0;
	$sumwebcredit = 0;
	$summobilecredit = 0;
	$sumwebcredit2week = 0;
	$summobilecredit2week = 0;
	$summonth_daily_cost = 0;
	$summonth_daily_2week_cost = 0;
		
    for($i=0; $i<sizeof($ios_list); $i++)
    {
    	$today = $ios_list[$i]["today"];
    	$regmonth = $ios_list[$i]["regmonth"];
    	$regday = $ios_list[$i]["regday"];
    	$month = $ios_list[$i]["month"];
    	 
    	if($month == 2 && ($regday == 31 || $regday == 30 || $regday == 29))
    	{
    		$regday = 28;
    	}
    	else if(in_array($month, $day_month) && $regday == 31)
    	{
    		$regday = 30;
    	}
    	
    	$regdate = str_replace("-", "", $regmonth).$regday;
    	$first_regdate = str_replace("-", "", $regmonth)."01";
    	$search_2week_date = date("Ymd", strtotime($regmonth."-".$regday) - 86400*14);
    	
    	$sql = "SELECT t1.today as today, t1.type as type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week, IFNULL(cost, 0) AS cost, IFNULL(cost_2week, 0) AS cost_2week ".
    			"FROM (SELECT * FROM tbl_user_inflowroute_daily WHERE today = '$regdate' AND regmonth = '".$regmonth."' AND TYPE = 1 ORDER BY today DESC, regmonth ASC) t1 ".
    			"LEFT JOIN ".
    			"(SELECT today, (SELECT IFNULL(SUM(cost), 0) FROM `tbl_user_inflowroute_cost` WHERE type = 1 AND '$first_regdate' <= today  AND today <= '$regdate') AS cost, ".
    			"(SELECT IFNULL(SUM(cost), 0) FROM `tbl_user_inflowroute_cost` WHERE type = 1 AND '$search_2week_date' <= today  AND today <= '$regdate') AS cost_2week ".
    			"FROM `tbl_user_inflowroute_cost` WHERE type = 1 AND today = '$regdate') t2 ON t1.today=t2.today;";
    					 
    	$ios_list_detail = $db_analysis->getarray($sql);
    	
    	$regmonth = $ios_list_detail["regmonth"];
    	$joincount = $ios_list_detail["joincount"];
    	$activecount = $ios_list_detail["activecount"];
    	$webcredit = $ios_list_detail["webcredit"];
    	$mobilecredit = $ios_list_detail["mobilecredit"];
    	$totalcredit = $webcredit + $mobilecredit;
    	$mobilerate = ($totalcredit == 0) ? 0 : round(($mobilecredit / $totalcredit) * 100, 2);
    	$avgcredit = ($joincount == 0) ? 0 : round(($totalcredit / $joincount), 2);
    	$webcredit2week = $ios_list_detail["webcredit2week"];
    	$mobilecredit2week = $ios_list_detail["mobilecredit2week"];
    	$daily_cost = $ios_list_detail["cost"];
    	$daily_2week_cost = $ios_list_detail["cost_2week"];
    	$totalcredit2week = $webcredit2week + $mobilecredit2week;
    	$mobile2weekrate = ($totalcredit2week == 0) ? 0 : round(($mobilecredit2week / $totalcredit2week) * 100, 2);
    	$avgcredit2week = ($activecount == 0) ? 0 : round(($totalcredit2week / $activecount), 2);
    	
    	$summonth_daily_cost += $daily_cost;
    	$summonth_daily_2week_cost += $daily_2week_cost;
    	$daily_roi = ($daily_cost == 0) ? 0 : round($totalcredit / $daily_cost * 100, 2);
    	$daily_2week_roi = ($daily_2week_cost == 0) ? 0 : round($totalcredit2week / $daily_2week_cost * 100, 2);
    	
     	$monthcnt = $ios_sumlist[$rowcnt]["regmonth"];
     	
     	$sumjoincount += $joincount;
     	$sumactivecount += $activecount;
     	$sumwebcredit += $webcredit;
     	$summobilecredit += $mobilecredit;
     	$sumwebcredit2week += $webcredit2week;
     	$summobilecredit2week += $mobilecredit2week; 
     	
     	if($regmonth == "")
     		$regmonth = "2015-08";
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
			if($j == 1)
			{
?>
				<td class="tdr point" rowspan="<?= $monthcnt?>"><?= $today?></td>
<?
			}
?>
            <td class="tdc"><?= $regmonth."-".$regday ?></td>
            <td class="tdr"><?= number_format($joincount) ?></td>            
            <td class="tdr">$<?= number_format($webcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($mobilecredit, 2) ?></td>
            <td class="tdr"><?= number_format($mobilerate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($totalcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($avgcredit, 2) ?></td>
             <td class="tdr">$<?= number_format($daily_cost) ?></td>
            <td class="tdr"><?= number_format($daily_roi, 2) ?>%</td>
            <td class="tdr">$<?= number_format($webcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($mobilecredit2week, 2) ?></td>
            <td class="tdr"><?= number_format($mobile2weekrate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($totalcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($avgcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($daily_2week_cost) ?></td>
            <td class="tdr"><?= number_format($daily_2week_roi, 2)?>%</td>
        </tr>
<?
		if($j == $monthcnt)
		{
			$sumtotalcredit = $sumwebcredit + $summobilecredit;
			$summobilerate = ($sumtotalcredit == 0) ? 0 : round(($summobilecredit / $sumtotalcredit) * 100, 2);
			$sumavgcredit = ($sumjoincount == 0) ? 0 : round(($sumtotalcredit / $sumjoincount), 2);
			$sumtotalcredit2week = $sumwebcredit2week + $summobilecredit2week;
			$summobile2weekrate = ($sumtotalcredit2week == 0) ? 0 : round(($summobilecredit2week / $sumtotalcredit2week) * 100, 2);
			$sumavgcredit2week = ($sumactivecount == 0) ? 0 : round(($sumtotalcredit2week / $sumactivecount), 2);
			$sumdailyroi = ($summonth_daily_cost == 0) ? 0 : round($sumtotalcredit / $summonth_daily_cost * 100, 2);
			$sumdaily2weekroi = ($summonth_daily_2week_cost == 0) ? 0 : round($sumtotalcredit2week / $summonth_daily_2week_cost * 100, 2);
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point_title" colspan="2">합계</td>
            	<td class="tdr point"><?= number_format($sumjoincount) ?></td>            	
            	<td class="tdr point">$<?= number_format($sumwebcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobilerate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summonth_daily_cost) ?></td>
           		<td class="tdr point"><?= number_format($sumdailyroi, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumwebcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit2week, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobile2weekrate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summonth_daily_2week_cost) ?></td>
           		<td class="tdr point"><?= number_format($sumdaily2weekroi, 2) ?>%</td>
			</tr>
<?
			$j = 1;
			$rowcnt++;
    	}
    	else
    	{
    		$j++;	
    	}
    }
    
    if(sizeof($ios_list) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="16">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>
	</br></br>
		
	<div class="search_result">Android&nbsp;&nbsp;<a href="javascript:pop_user_daily_cost_list(2);">+cost</a></div>
	
	<table class="tbl_list_basic1" style="width:1300px">
		<colgroup>
			<col width="50">
			<col width="100">			
            <col width="80">            
            <col width="100">
            <col width="100">
            <col width="80">
            <col width="100">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="70">
            <col width="70">
            <col width="70">
		</colgroup>
        <thead>
        	<tr>
        		<th rowspan="2">일자</th>
            	<th colspan="2">일반현황</th>
            	<th colspan="7">누적</th>
            	<th colspan="7">최근 2주</th>
         	</tr>
         	<tr>
         		<th>가입월일</th>
             	<th>가입자수</th>               	
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>비용</th>
               	<th>ROI</th>
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>비용</th>
               	<th>ROI</th>
         	</tr>
        </thead>
        <tbody>
<?
	$rowcnt = 0;
	$j = 1;
	$summonth_cost = 0;
	$summonth_cost = 0;
	$sumjoincount = 0;
	$sumactivecount = 0;
	$sumwebcredit = 0;
	$summobilecredit = 0;
	$sumwebcredit2week = 0;
	$summobilecredit2week = 0;
	$summonth_daily_cost = 0;
	$summonth_daily_2week_cost = 0;
		
    for($i=0; $i<sizeof($android_list); $i++)
    {
    	$today = $android_list[$i]["today"];
    	$regmonth = $android_list[$i]["regmonth"];
    	$regday = $android_list[$i]["regday"];
    	$month = $android_list[$i]["month"];
    	 
    	if($month == 2 && ($regday == 31 || $regday == 30 || $regday == 29))
    	{
    		$regday = 28;
    	}
    	else if(in_array($month, $day_month) && $regday == 31)
    	{
    		$regday = 30;
    	}
    	
    	$regdate = str_replace("-", "", $regmonth).$regday;
    	$first_regdate = str_replace("-", "", $regmonth)."01";
    	$search_2week_date = date("Ymd", strtotime($regmonth."-".$regday) - 86400*14);
    	
    	$sql = "SELECT t1.today as today, t1.type as type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week, IFNULL(cost, 0) AS cost, IFNULL(cost_2week, 0) AS cost_2week ".
    			"FROM (SELECT * FROM tbl_user_inflowroute_daily WHERE today = '$regdate' AND regmonth = '".$regmonth."' AND TYPE = 2 ORDER BY today DESC, regmonth ASC) t1 ".
    			"LEFT JOIN ".
    			"(SELECT today, (SELECT IFNULL(SUM(cost), 0) FROM `tbl_user_inflowroute_cost` WHERE type = 2 AND '$first_regdate' <= today  AND today <= '$regdate') AS cost, ".
    			"(SELECT IFNULL(SUM(cost), 0) FROM `tbl_user_inflowroute_cost` WHERE type = 2 AND '$search_2week_date' <= today  AND today <= '$regdate') AS cost_2week ".
    			"FROM `tbl_user_inflowroute_cost` WHERE type = 2 AND today = '$regdate') t2 ON t1.today=t2.today;";
    					 
    	$android_list_detail = $db_analysis->getarray($sql);
    	
    	$regmonth = $android_list_detail["regmonth"];
    	$joincount = $android_list_detail["joincount"];
    	$activecount = $android_list_detail["activecount"];
    	$webcredit = $android_list_detail["webcredit"];
    	$mobilecredit = $android_list_detail["mobilecredit"];
    	$totalcredit = $webcredit + $mobilecredit;
    	$mobilerate = ($totalcredit == 0) ? 0 : round(($mobilecredit / $totalcredit) * 100, 2);
    	$avgcredit = ($joincount == 0) ? 0 : round(($totalcredit / $joincount), 2);
    	$webcredit2week = $android_list_detail["webcredit2week"];
    	$mobilecredit2week = $android_list_detail["mobilecredit2week"];
    	$daily_cost = $android_list_detail["cost"];
    	$daily_2week_cost = $android_list_detail["cost_2week"];
    	$totalcredit2week = $webcredit2week + $mobilecredit2week;
    	$mobile2weekrate = ($totalcredit2week == 0) ? 0 : round(($mobilecredit2week / $totalcredit2week) * 100, 2);
    	$avgcredit2week = ($activecount == 0) ? 0 : round(($totalcredit2week / $activecount), 2);
    	
    	$summonth_daily_cost += $daily_cost;
    	$summonth_daily_2week_cost += $daily_2week_cost;
    	$daily_roi = ($daily_cost == 0) ? 0 : round($totalcredit / $daily_cost * 100, 2);
    	$daily_2week_roi = ($daily_2week_cost == 0) ? 0 : round($totalcredit2week / $daily_2week_cost * 100, 2);
    	
     	$monthcnt = $android_sumlist[$rowcnt]["regmonth"];
     	
     	$sumjoincount += $joincount;
     	$sumactivecount += $activecount;
     	$sumwebcredit += $webcredit;
     	$summobilecredit += $mobilecredit;
     	$sumwebcredit2week += $webcredit2week;
     	$summobilecredit2week += $mobilecredit2week; 
     	
     	if($regmonth == "")
     		$regmonth = "2015-08";
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
			if($j == 1)
			{
?>
				<td class="tdr point" rowspan="<?= $monthcnt?>"><?= $today?></td>
<?
			}
?>
            <td class="tdc"><?= $regmonth."-".$regday ?></td>
            <td class="tdr"><?= number_format($joincount) ?></td>            
            <td class="tdr">$<?= number_format($webcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($mobilecredit, 2) ?></td>
            <td class="tdr"><?= number_format($mobilerate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($totalcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($avgcredit, 2) ?></td>
             <td class="tdr">$<?= number_format($daily_cost) ?></td>
            <td class="tdr"><?= number_format($daily_roi, 2) ?>%</td>
            <td class="tdr">$<?= number_format($webcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($mobilecredit2week, 2) ?></td>
            <td class="tdr"><?= number_format($mobile2weekrate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($totalcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($avgcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($daily_2week_cost) ?></td>
            <td class="tdr"><?= number_format($daily_2week_roi, 2)?>%</td>
        </tr>
<?
		if($j == $monthcnt)
		{
			$sumtotalcredit = $sumwebcredit + $summobilecredit;
			$summobilerate = ($sumtotalcredit == 0) ? 0 : round(($summobilecredit / $sumtotalcredit) * 100, 2);
			$sumavgcredit = ($sumjoincount == 0) ? 0 : round(($sumtotalcredit / $sumjoincount), 2);
			$sumtotalcredit2week = $sumwebcredit2week + $summobilecredit2week;
			$summobile2weekrate = ($sumtotalcredit2week == 0) ? 0 : round(($summobilecredit2week / $sumtotalcredit2week) * 100, 2);
			$sumavgcredit2week = ($sumactivecount == 0) ? 0 : round(($sumtotalcredit2week / $sumactivecount), 2);
			$sumdailyroi = ($summonth_daily_cost == 0) ? 0 : round($sumtotalcredit / $summonth_daily_cost * 100, 2);
			$sumdaily2weekroi = ($summonth_daily_2week_cost == 0) ? 0 : round($sumtotalcredit2week / $summonth_daily_2week_cost * 100, 2);
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point_title" colspan="2">합계</td>
            	<td class="tdr point"><?= number_format($sumjoincount) ?></td>            	
            	<td class="tdr point">$<?= number_format($sumwebcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobilerate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summonth_daily_cost) ?></td>
           		<td class="tdr point"><?= number_format($sumdailyroi, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumwebcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit2week, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobile2weekrate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summonth_daily_2week_cost) ?></td>
           		<td class="tdr point"><?= number_format($sumdaily2weekroi, 2) ?>%</td>
			</tr>
<?
			$j = 1;
			$rowcnt++;
    	}
    	else
    	{
    		$j++;	
    	}
    }
    
    if(sizeof($android_list) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="16">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>	
	<br/><br/>
	
	<div class="search_result">Amazon&nbsp;&nbsp;<a href="javascript:pop_user_daily_cost_list(3);">+cost</a></div>
	
	<table class="tbl_list_basic1" style="width:1300px">
		<colgroup>
			<col width="50">
			<col width="100">			
            <col width="80">            
            <col width="100">
            <col width="100">
            <col width="80">
            <col width="100">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="80">
            <col width="70">
            <col width="70">
            <col width="70">
		</colgroup>
        <thead>
        	<tr>
        		<th rowspan="2">일자</th>
            	<th colspan="2">일반현황</th>
            	<th colspan="7">누적</th>
            	<th colspan="7">최근 2주</th>
         	</tr>
         	<tr>
         		<th>가입월일</th>
             	<th>가입자수</th>               	
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>비용</th>
               	<th>ROI</th>
               	<th>웹결제</th>
               	<th>모바일결제</th>
               	<th>모바일비중</th>
               	<th>결제합계</th>
               	<th>평균결제</th>
               	<th>비용</th>
               	<th>ROI</th>
         	</tr>
        </thead>
        <tbody>
<?
	$rowcnt = 0;
	$j = 1;
	$summonth_cost = 0;
	$summonth_cost = 0;
	$sumjoincount = 0;
	$sumactivecount = 0;
	$sumwebcredit = 0;
	$summobilecredit = 0;
	$sumwebcredit2week = 0;
	$summobilecredit2week = 0;
	$summonth_daily_cost = 0;
	$summonth_daily_2week_cost = 0;
		
    for($i=0; $i<sizeof($amazon_list); $i++)
    {
    	$today = $amazon_list[$i]["today"];
    	$regmonth = $amazon_list[$i]["regmonth"];
    	$regday = $amazon_list[$i]["regday"];
    	$month = $amazon_list[$i]["month"];
    	 
    	if($month == 2 && ($regday == 31 || $regday == 30 || $regday == 29))
    	{
    		$regday = 28;
    	}
    	else if(in_array($month, $day_month) && $regday == 31)
    	{
    		$regday = 30;
    	}
    	
    	$regdate = str_replace("-", "", $regmonth).$regday;
    	$first_regdate = str_replace("-", "", $regmonth)."01";
    	$search_2week_date = date("Ymd", strtotime($regmonth."-".$regday) - 86400*14);
    	
    	$sql = "SELECT t1.today as today, t1.type as type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week, IFNULL(cost, 0) AS cost, IFNULL(cost_2week, 0) AS cost_2week ".
    			"FROM (SELECT * FROM tbl_user_inflowroute_daily WHERE today = '$regdate' AND regmonth = '".$regmonth."' AND TYPE = 3 ORDER BY today DESC, regmonth ASC) t1 ".
    			"LEFT JOIN ".
    			"(SELECT today, (SELECT IFNULL(SUM(cost), 0) FROM `tbl_user_inflowroute_cost` WHERE type = 3 AND '$first_regdate' <= today  AND today <= '$regdate') AS cost, ".
    			"(SELECT IFNULL(SUM(cost), 0) FROM `tbl_user_inflowroute_cost` WHERE type = 3 AND '$search_2week_date' <= today  AND today <= '$regdate') AS cost_2week ".
    			"FROM `tbl_user_inflowroute_cost` WHERE type = 3 AND today = '$regdate') t2 ON t1.today=t2.today;";
  					 
    	$amazon_list_detail = $db_analysis->getarray($sql);
    	
    	$regmonth = $amazon_list_detail["regmonth"];
    	$joincount = $amazon_list_detail["joincount"];
    	$activecount = $amazon_list_detail["activecount"];
    	$webcredit = $amazon_list_detail["webcredit"];
    	$mobilecredit = $amazon_list_detail["mobilecredit"];
    	$totalcredit = $webcredit + $mobilecredit;
    	$mobilerate = ($totalcredit == 0) ? 0 : round(($mobilecredit / $totalcredit) * 100, 2);
    	$avgcredit = ($joincount == 0) ? 0 : round(($totalcredit / $joincount), 2);
    	$webcredit2week = $amazon_list_detail["webcredit2week"];
    	$mobilecredit2week = $amazon_list_detail["mobilecredit2week"];
    	$daily_cost = $amazon_list_detail["cost"];
    	$daily_2week_cost = $amazon_list_detail["cost_2week"];
    	$totalcredit2week = $webcredit2week + $mobilecredit2week;
    	$mobile2weekrate = ($totalcredit2week == 0) ? 0 : round(($mobilecredit2week / $totalcredit2week) * 100, 2);
    	$avgcredit2week = ($activecount == 0) ? 0 : round(($totalcredit2week / $activecount), 2);
    	
    	$summonth_daily_cost += $daily_cost;
    	$summonth_daily_2week_cost += $daily_2week_cost;
    	$daily_roi = ($daily_cost == 0) ? 0 : round($totalcredit / $daily_cost * 100, 2);
    	$daily_2week_roi = ($daily_2week_cost == 0) ? 0 : round($totalcredit2week / $daily_2week_cost * 100, 2);
    	
     	$monthcnt = $amazon_sumlist[$rowcnt]["regmonth"];
     	
     	$sumjoincount += $joincount;
     	$sumactivecount += $activecount;
     	$sumwebcredit += $webcredit;
     	$summobilecredit += $mobilecredit;
     	$sumwebcredit2week += $webcredit2week;
     	$summobilecredit2week += $mobilecredit2week; 
     	
     	if($regmonth == "")
     		$regmonth = "2015-08";
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
			if($j == 1)
			{
?>
				<td class="tdr point" rowspan="<?= $monthcnt?>"><?= $today?></td>
<?
			}
?>
            <td class="tdc"><?= $regmonth."-".$regday ?></td>
            <td class="tdr"><?= number_format($joincount) ?></td>            
            <td class="tdr">$<?= number_format($webcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($mobilecredit, 2) ?></td>
            <td class="tdr"><?= number_format($mobilerate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($totalcredit, 2) ?></td>
            <td class="tdr">$<?= number_format($avgcredit, 2) ?></td>
             <td class="tdr">$<?= number_format($daily_cost) ?></td>
            <td class="tdr"><?= number_format($daily_roi, 2) ?>%</td>
            <td class="tdr">$<?= number_format($webcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($mobilecredit2week, 2) ?></td>
            <td class="tdr"><?= number_format($mobile2weekrate, 2) ?>%</td>
            <td class="tdr">$<?= number_format($totalcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($avgcredit2week, 2) ?></td>
            <td class="tdr">$<?= number_format($daily_2week_cost) ?></td>
            <td class="tdr"><?= number_format($daily_2week_roi, 2)?>%</td>
        </tr>
<?
		if($j == $monthcnt)
		{
			$sumtotalcredit = $sumwebcredit + $summobilecredit;
			$summobilerate = ($sumtotalcredit == 0) ? 0 : round(($summobilecredit / $sumtotalcredit) * 100, 2);
			$sumavgcredit = ($sumjoincount == 0) ? 0 : round(($sumtotalcredit / $sumjoincount), 2);
			$sumtotalcredit2week = $sumwebcredit2week + $summobilecredit2week;
			$summobile2weekrate = ($sumtotalcredit2week == 0) ? 0 : round(($summobilecredit2week / $sumtotalcredit2week) * 100, 2);
			$sumavgcredit2week = ($sumactivecount == 0) ? 0 : round(($sumtotalcredit2week / $sumactivecount), 2);
			$sumdailyroi = ($summonth_daily_cost == 0) ? 0 : round($sumtotalcredit / $summonth_daily_cost * 100, 2);
			$sumdaily2weekroi = ($summonth_daily_2week_cost == 0) ? 0 : round($sumtotalcredit2week / $summonth_daily_2week_cost * 100, 2);
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point_title" colspan="2">합계</td>
            	<td class="tdr point"><?= number_format($sumjoincount) ?></td>            	
            	<td class="tdr point">$<?= number_format($sumwebcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobilerate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summonth_daily_cost) ?></td>
           		<td class="tdr point"><?= number_format($sumdailyroi, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumwebcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summobilecredit2week, 2) ?></td>
	            <td class="tdr point"><?= number_format($summobile2weekrate, 2) ?>%</td>
	            <td class="tdr point">$<?= number_format($sumtotalcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($sumavgcredit2week, 2) ?></td>
	            <td class="tdr point">$<?= number_format($summonth_daily_2week_cost) ?></td>
           		<td class="tdr point"><?= number_format($sumdaily2weekroi, 2) ?>%</td>
			</tr>
<?
			$j = 1;
			$rowcnt++;
    	}
    	else
    	{
    		$j++;	
    	}
    }
    
    if(sizeof($amazon_list) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="16">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>
	
	<div class="clear"></div>
</div>
<?
	$db_analysis->end();

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
