<?
	$top_menu = "user_static";
	$sub_menu = "user_maintain";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	if($_GET["start_date"] == "")
		$search_date = date("Y-m-d");
	else
		$search_date = $_GET["start_date"];
	
	$db_analysis = new CDatabase_Analysis();
	
	$start_date = str_replace("-","",$search_date);
	
	$sql = "SELECT * FROM `tbl_user_maintainstat_daily` WHERE today <= '$start_date' AND today > date_format(DATE_ADD('$start_date',INTERVAL -8 DAY), '%Y%m%d') ORDER BY today DESC, regmonth ASC";
	
	$daylist = $db_analysis->gettotallist($sql);
	
	$sql = "SELECT today, COUNT(regmonth) AS regmonth, SUM(regusercnt) AS regusercnt, SUM(2weekleavecnt) AS 2weekleavecnt, SUM(nogameusercnt) AS nogameusercnt".
			", SUM(ordersum) AS ordersum, SUM(2weekorder) AS 2weekorder FROM `tbl_user_maintainstat_daily` ".
			"WHERE today <= '$start_date' AND today > date_format(DATE_ADD('$start_date', INTERVAL -8 DAY) ,'%Y%m%d') ".
			"GROUP BY today ORDER BY today DESC, regmonth ASC";
	
	$sumlist = $db_analysis->gettotallist($sql);
	
	$db_analysis->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
    	$("#start_date").datepicker({ });
	});

	function search()
    {
        var search_form = document.search_form;
            
        if (search_form.start_date.value == "")
        {
            alert("기준일을 입력하세요.");
            search_form.start_date.focus();
            return;
        } 

        search_form.submit();
    }
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 사용자 유지현황(Web) </div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<span class="search_lbl">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="start_date" name="start_date" style="width:65px" readonly="readonly" value="<?= $search_date ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/>
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->

	<div class="search_result">
		<span><?= $search_date ?></span> ~ <span><?= date("Y-m-d", strtotime($search_date." -7 day")) ?></span> 현황입니다
	</div>
	
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="50">
			<col width="50">
			<col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="100">
            <col width="80">
            <col width="110">
            <col width="100">               
		</colgroup>
        <thead>
        	<tr>
        		<th>일자</th>
            	<th>가입월</th>
             	<th>회원수</th>
               	<th>2주 이탈수</th>
               	<th>2주 이탈율</th>
               	<th>미게임수</th>
               	<th>미게임비율</th>
               	<th>누적결제</th>
               	<th>누적 평균결제액</th>
               	<th>최근2주 결제</th>
               	<th>최근2주 평균결제액</th>
               	<th class="tdr">최근2주 결제비중</th>
         	</tr>
        </thead>
        <tbody>
<?
	$rowcnt = 0;
	$j = 1;
		
    for($i=0; $i<sizeof($daylist); $i++)
    {
    	$today = $daylist[$i]["today"];
    	$regmonth = $daylist[$i]["regmonth"];
    	$regusercnt = $daylist[$i]["regusercnt"];
    	$twoweekleavecnt = $daylist[$i]["2weekleavecnt"];
    	$nogameusercnt = $daylist[$i]["nogameusercnt"];
    	$ordersum = $daylist[$i]["ordersum"];
    	$twoweekorder = $daylist[$i]["2weekorder"];
    	
    	$monthcnt = $sumlist[$rowcnt]["regmonth"];
    	$sumregusercnt = $sumlist[$rowcnt]["regusercnt"];
    	$sum2weekleavecnt = $sumlist[$rowcnt]["2weekleavecnt"];
    	$sumnogameusercnt = $sumlist[$rowcnt]["nogameusercnt"];
    	$sumordersum = $sumlist[$rowcnt]["ordersum"];
    	$sum2weekorder = $sumlist[$rowcnt]["2weekorder"];
    	
    	$avrsum2weekorder = 0;
    	
    	if($sum2weekorder == 0)
    	{
    		$avrsum2weekorder = 0;
    	}
    	else
    	{
    		$avrsum2weekorder = round(($sum2weekorder / $sum2weekorder) * 100, 2);
    	}
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
			if($j == 1)
			{
?>
				<td class="tdr point" rowspan="<?= $monthcnt?>"><?= $today?></td>
<?
			}
?>
            <td class="tdr"><?= $regmonth ?></td>
            <td class="tdr"><?= number_format($regusercnt) ?></td>
            <td class="tdr"><?= number_format($twoweekleavecnt) ?></td>
            <td class="tdr"><?= round(($twoweekleavecnt / $regusercnt) * 100, 2) ?>%</td>
            <td class="tdr"><?= number_format($nogameusercnt) ?></td>
            <td class="tdr"><?= round(($nogameusercnt / $regusercnt) * 100, 2) ?>%</td>
            <td class="tdr"><?= make_price_format($ordersum) ?></td>
            <td class="tdr"><?= round(($ordersum / $regusercnt), 3) ?></td>
            <td class="tdr"><?= make_price_format($twoweekorder) ?></td>
            <td class="tdr"><?= round(($twoweekorder / ($regusercnt-$twoweekleavecnt)), 3) ?></td>
            <td class="tdr"><?= round(($twoweekorder / $sum2weekorder) * 100, 2) ?>%</td>
        </tr>
<?
		if($j == $monthcnt)
		{
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point_title" colspan="2">합계</td>
            	<td class="tdr point"><?= number_format($sumregusercnt) ?></td>
            	<td class="tdr point"><?= number_format($sum2weekleavecnt) ?></td>
            	<td class="tdr point"><?= round(($sum2weekleavecnt / $sumregusercnt) * 100, 2) ?>%</td>
            	<td class="tdr point"><?= number_format($sumnogameusercnt) ?></td>
            	<td class="tdr point"><?= round(($sumnogameusercnt / $sumregusercnt) * 100, 2) ?>%</td>
            	<td class="tdr point"><?= make_price_format($sumordersum) ?></td>
            	<td class="tdr point"><?= round(($sumordersum / $sumregusercnt), 3) ?></td>
            	<td class="tdr point"><?= make_price_format($sum2weekorder) ?></td>
            	<td class="tdr point"><?= round(($sum2weekorder / ($sumregusercnt-$sum2weekleavecnt)), 3) ?></td>
            	<td class="tdr point"><?= $avrsum2weekorder ?>%</td>
			</tr>
<?
			$j = 1;
			$rowcnt++;
    	}
    	else
    	{
    		$j++;	
    	}
    }
    
    if(sizeof($daylist) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="12">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>
	
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>