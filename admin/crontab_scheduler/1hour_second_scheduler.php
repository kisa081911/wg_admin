<?
	include("../common/common_include.inc.php");
	include("../common/dbconnect/db_util_redshift.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_slave_main2 = new CDatabase_Slave_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_friend = new CDatabase_Friend();
	$db_game = new CDatabase_Game();
	$db_other = new CDatabase_Other();
	$db_livestats = new CDatabase_Livestats();
	$db_slave_livestats = new CDatabase_Slave_Livestats();
	$db_mobile = new CDatabase_Mobile();
	$db_inbox = new CDatabase_Inbox();
	$db_slave_main = new CDatabase_Slave_Main();
	
	$db_main->execute("SET wait_timeout=7200");
	$db_main2->execute("SET wait_timeout=7200");
	$db_slave_main2->execute("SET wait_timeout=7200");
	$db_analysis->execute("SET wait_timeout=7200");
	$db_friend->execute("SET wait_timeout=7200");
	$db_game->execute("SET wait_timeout=7200");
	$db_other->execute("SET wait_timeout=7200");
	$db_livestats->execute("SET wait_timeout=7200");
	$db_slave_livestats->execute("SET wait_timeout=7200");
	$db_mobile->execute("SET wait_timeout=7200");
	$db_inbox->execute("SET wait_timeout=7200");
	$db_slave_main->execute("SET wait_timeout=7200");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$port = "";
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}
	
	try
	{
 		$sql = "INSERT INTO tbl_scheduler_log(today, type, status, writedate) VALUES('$today', 302, 0, NOW());";
 		$db_analysis->execute($sql);
 		$sql = "SELECT LAST_INSERT_ID();";
 		$logidx = $db_analysis->getvalue($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());		
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/1hour_second_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/1hour_second_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("1hour_second_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	if (date("H") == "00")
	{
		$today = date("Y-m-d", time() - 60 * 60);
		$tomorrow = date("Y-m-d", time() + 60 * 60);
	}
	else
	{
		$today = date("Y-m-d");
		$tomorrow = date("Y-m-d", time() + 24 * 60 * 60);
	}	
	
	// TY Send 통계
	try
	{
		$category = 0;
		$ty_send_cnt = 0;
		$ty_user_cnt = 0;
	
		$ty_reward_cnt = 0;
		$ty_reward_user_cnt = 0;
		$ty_reward_amount = 0;
	
		for($i=0; $i<10; $i++)
		{
			$sql = "SELECT COUNT(*) AS send_total, COUNT(DISTINCT sender_useridx) AS send_user_cnt ".
					"FROM tbl_user_ty_history_$i ".
					"WHERE writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
			$ty_info = $db_friend->getarray($sql);
				
			$sql = "SELECT IFNULL(SUM(amount),0) AS total_amount, COUNT(logidx) AS total_cnt, COUNT(DISTINCT useridx) AS total_user_cnt ".
					"FROM tbl_user_freecoin_log_$i ".
					"WHERE type = 2 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
			$reward_info = $db_slave_main2->getarray($sql);
	
			$ty_reward_cnt += $reward_info["total_cnt"];
			$ty_reward_user_cnt += $reward_info["total_user_cnt"];
			$ty_reward_amount += $reward_info["total_amount"];
	
				
			$ty_send_cnt += $ty_info["send_total"];
			$ty_user_cnt += $ty_info["send_user_cnt"];
		}
	
		$sql = "INSERT INTO tbl_ty_stat(today, category, send_cnt, user_cnt, reward_cnt, reward_user_cnt, reward_amount) ".
				"VALUES('$today', $category, $ty_send_cnt, $ty_user_cnt, $ty_reward_cnt, $ty_reward_user_cnt, $ty_reward_amount) ".
				"ON DUPLICATE KEY UPDATE send_cnt=VALUES(send_cnt), user_cnt=VALUES(user_cnt), reward_cnt=VALUES(reward_cnt), reward_user_cnt=VALUES(reward_user_cnt), reward_amount=VALUES(reward_amount)";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	// Treat Ticket Click 통계
	try
	{
		for($n=0;$n<10;$n++)
		{
			$sql = "SELECT sharetype, os_type, COUNT(sharetype) AS cnt_treat, COUNT(DISTINCT useridx) AS cnt_treat_user, IFNULL(SUM(shareamount), 0) AS sum_treat_amount FROM tbl_slot_event_$n ".
					"WHERE eventtype IN (1,2,3,4,5) AND roomidx < 1000000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' GROUP BY sharetype, os_type;";
			$treat_ticket_list = $db_game->gettotallist($sql);
				
			for($i = 0; $i < sizeof($treat_ticket_list); $i++)
			{
				$treat_type = $treat_ticket_list[$i]["sharetype"];
				$os_type = $treat_ticket_list[$i]["os_type"];
				$cnt_treat = $treat_ticket_list[$i]["cnt_treat"];
				$cnt_treat_user = $treat_ticket_list[$i]["cnt_treat_user"];
				$sum_treat_amount = $treat_ticket_list[$i]["sum_treat_amount"];
	
				$sql = "INSERT INTO tbl_treat_ticket_stat(today, os_type, treat_type, treat_count, treat_user_count, treat_amount) ".
						"VALUES('$today', $os_type, $treat_type, $cnt_treat, $cnt_treat_user, $sum_treat_amount) ON DUPLICATE KEY UPDATE treat_count=VALUES(treat_count), treat_user_count = VALUES(treat_user_count), treat_amount=VALUES(treat_amount)";
				$db_analysis->execute($sql);
			}
				
			$sql = "SELECT sharetype, os_type, COUNT(sharetype) AS cnt_treat, COUNT(DISTINCT useridx) AS cnt_treat_user, IFNULL(SUM(shareamount), 0) AS sum_treat_amount FROM tbl_slot_event_$n ".
					"WHERE eventtype IN (1,2,3,4,5) AND roomidx >= 1000000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' GROUP BY sharetype, os_type;";
			$treat_ticket_list = $db_game->gettotallist($sql);
				
			for($i = 0; $i < sizeof($treat_ticket_list); $i++)
			{
				$treat_type = $treat_ticket_list[$i]["sharetype"];
				$os_type = $treat_ticket_list[$i]["os_type"];
				$cnt_treat = $treat_ticket_list[$i]["cnt_treat"];
				$cnt_treat_user = $treat_ticket_list[$i]["cnt_treat_user"];
				$sum_treat_amount = $treat_ticket_list[$i]["sum_treat_amount"];
	
				$sql = "INSERT INTO tbl_treat_ticket_platinum_stat(today, os_type, treat_type, treat_count, treat_user_count, treat_amount) ".
						"VALUES('$today', $os_type, $treat_type, $cnt_treat, $cnt_treat_user, $sum_treat_amount) ON DUPLICATE KEY UPDATE treat_count=VALUES(treat_count), treat_user_count = VALUES(treat_user_count), treat_amount=VALUES(treat_amount)";
				$db_analysis->execute($sql);
			}
		}
	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	// Treat Ticket 금액 통계
	try
	{	//web
		$sql = "SELECT IFNULL(SUM(moneyin),0) FROM tbl_game_cash_stats_daily2 WHERE betlevel BETWEEN 0 AND 9 AND writedate = '$today';";
		$daily_normal_loyalty_amount = $db_slave_main2->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(moneyin),0) FROM tbl_game_cash_stats_daily2 WHERE betlevel BETWEEN 10 AND 19 AND writedate = '$today';";
		$daily_platinum_loyalty_amount = $db_slave_main2->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(loyaltypot),0) FROM tbl_user_treat t1 join tbl_user_ext t2 on t1.useridx = t2.useridx WHERE t2.platform = 0 AND t1.useridx > $std_useridx;";
		$loyalty_keep_amount = $db_main->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(treatamount),0) FROM tbl_treat_ticket t1 join tbl_user_ext t2 on t1.useridx = t2.useridx WHERE t2.platform = 0 AND t1.useridx > $std_useridx;";
		$treat_keep_amount = $db_main->getvalue($sql);
		
		$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
				"FROM tbl_game_cash_stats_daily2 ".
				"WHERE betlevel BETWEEN 0 AND 9 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
		$daily_treat_amount = $db_slave_main2->getvalue($treat_sql);
		
		$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
				"FROM tbl_game_cash_stats_daily2 ".
				"WHERE betlevel BETWEEN 10 AND 19 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
		$daily_platinum_treat_amount = $db_slave_main2->getvalue($treat_sql);
		
		$exchange_treat_user_cnt = 0;
		$exchange_treat_reward_amount = 0;
		
		for($i=0; $i<10; $i++)
		{
			$sql = "SELECT SUM(amount) AS total_amount, COUNT(DISTINCT useridx) AS total_user_cnt ".
					"FROM tbl_user_freecoin_log_$i ".
					"WHERE type IN (5, 17) AND category = 0 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
		
			$treat_reward_info = $db_slave_main2->getarray($sql);
				
			$exchange_treat_user_cnt += $treat_reward_info["total_user_cnt"];
			$exchange_treat_reward_amount += $treat_reward_info["total_amount"];
		}
			
		$sql = "INSERT INTO tbl_treat_ticket_use_stat(today, os_type, daily_loyalty_amount, daily_platinum_loyalty_amount, daily_treat_amount, daily_platinum_treat_amount, loyalty_keep_amount, treat_keep_amount, exchange_treat_count, exchange_treat_amount) ".
				"VALUES('$today', 0, '$daily_normal_loyalty_amount', '$daily_platinum_loyalty_amount', '$daily_treat_amount', '$daily_platinum_treat_amount', '$loyalty_keep_amount', '$treat_keep_amount', '$exchange_treat_user_cnt', '$exchange_treat_reward_amount') ".
				"ON DUPLICATE KEY UPDATE daily_loyalty_amount=VALUES(daily_loyalty_amount), daily_platinum_loyalty_amount=VALUES(daily_platinum_loyalty_amount), daily_treat_amount=VALUES(daily_treat_amount), daily_platinum_treat_amount=VALUES(daily_platinum_treat_amount), loyalty_keep_amount=VALUES(loyalty_keep_amount), treat_keep_amount=VALUES(treat_keep_amount), exchange_treat_count=VALUES(exchange_treat_count), exchange_treat_amount=VALUES(exchange_treat_amount);";
		$db_analysis->execute($sql);
		
		//IOS
		$sql = "SELECT IFNULL(SUM(moneyin),0) FROM tbl_game_cash_stats_ios_daily2 WHERE betlevel BETWEEN 0 AND 9 AND writedate = '$today';";
		$daily_normal_loyalty_amount = $db_slave_main2->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(moneyin),0) FROM tbl_game_cash_stats_ios_daily2 WHERE betlevel BETWEEN 10 AND 19 AND writedate = '$today';";
		$daily_platinum_loyalty_amount = $db_slave_main2->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(loyaltypot),0) FROM tbl_user_treat t1 join tbl_user_ext t2 on t1.useridx = t2.useridx WHERE t2.platform = 1 AND t1.useridx > $std_useridx;";
		$loyalty_keep_amount = $db_main->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(treatamount),0) FROM tbl_treat_ticket t1 join tbl_user_ext t2 on t1.useridx = t2.useridx WHERE t2.platform = 1 AND t1.useridx > $std_useridx;";
		$treat_keep_amount = $db_main->getvalue($sql);
		
		$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
				"FROM tbl_game_cash_stats_ios_daily2 ".
				"WHERE betlevel BETWEEN 0 AND 9 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
		$daily_treat_amount = $db_slave_main2->getvalue($treat_sql);
		
		$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
				"FROM tbl_game_cash_stats_ios_daily2 ".
				"WHERE betlevel BETWEEN 10 AND 19 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
		$daily_platinum_treat_amount = $db_slave_main2->getvalue($treat_sql);
		
		$exchange_treat_user_cnt = 0;
		$exchange_treat_reward_amount = 0;
		
		for($i=0; $i<10; $i++)
		{
			$sql = "SELECT SUM(amount) AS total_amount, COUNT(DISTINCT useridx) AS total_user_cnt ".
					"FROM tbl_user_freecoin_log_$i ".
					"WHERE type IN (5, 17) AND category = 1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
			$treat_reward_info = $db_slave_main2->getarray($sql);
		
			$exchange_treat_user_cnt += $treat_reward_info["total_user_cnt"];
			$exchange_treat_reward_amount += $treat_reward_info["total_amount"];
		}
			
		$sql = "INSERT INTO tbl_treat_ticket_use_stat(today, os_type, daily_loyalty_amount, daily_platinum_loyalty_amount, daily_treat_amount, daily_platinum_treat_amount, loyalty_keep_amount, treat_keep_amount, exchange_treat_count, exchange_treat_amount) ".
				"VALUES('$today', 1, '$daily_normal_loyalty_amount', '$daily_platinum_loyalty_amount', '$daily_treat_amount', '$daily_platinum_treat_amount', '$loyalty_keep_amount', '$treat_keep_amount', '$exchange_treat_user_cnt', '$exchange_treat_reward_amount') ".
				"ON DUPLICATE KEY UPDATE daily_loyalty_amount=VALUES(daily_loyalty_amount), daily_platinum_loyalty_amount=VALUES(daily_platinum_loyalty_amount), daily_treat_amount=VALUES(daily_treat_amount), daily_platinum_treat_amount=VALUES(daily_platinum_treat_amount), loyalty_keep_amount=VALUES(loyalty_keep_amount), treat_keep_amount=VALUES(treat_keep_amount), exchange_treat_count=VALUES(exchange_treat_count), exchange_treat_amount=VALUES(exchange_treat_amount);";
		$db_analysis->execute($sql);
		
		//Android
		$sql = "SELECT IFNULL(SUM(moneyin),0) FROM tbl_game_cash_stats_android_daily2 WHERE betlevel BETWEEN 0 AND 9 AND writedate = '$today';";
		$daily_normal_loyalty_amount = $db_slave_main2->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(moneyin),0) FROM tbl_game_cash_stats_android_daily2 WHERE betlevel BETWEEN 10 AND 19 AND writedate = '$today';";
		$daily_platinum_loyalty_amount = $db_slave_main2->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(loyaltypot),0) FROM tbl_user_treat t1 join tbl_user_ext t2 on t1.useridx = t2.useridx WHERE t2.platform = 2 AND t1.useridx > $std_useridx;";
		$loyalty_keep_amount = $db_main->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(treatamount),0) FROM tbl_treat_ticket t1 join tbl_user_ext t2 on t1.useridx = t2.useridx WHERE t2.platform = 2 AND t1.useridx > $std_useridx;";
		$treat_keep_amount = $db_main->getvalue($sql);
		
		$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
				"FROM tbl_game_cash_stats_android_daily2 ".
				"WHERE betlevel BETWEEN 0 AND 9 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
		$daily_treat_amount = $db_slave_main2->getvalue($treat_sql);
		
		$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
				"FROM tbl_game_cash_stats_android_daily2 ".
				"WHERE betlevel BETWEEN 10 AND 19 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
		$daily_platinum_treat_amount = $db_slave_main2->getvalue($treat_sql);
		
		$exchange_treat_user_cnt = 0;
		$exchange_treat_reward_amount = 0;
		
		for($i=0; $i<10; $i++)
		{
			$sql = "SELECT SUM(amount) AS total_amount, COUNT(DISTINCT useridx) AS total_user_cnt ".
					"FROM tbl_user_freecoin_log_$i ".
					"WHERE type IN (5, 17) AND category = 2 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
			$treat_reward_info = $db_slave_main2->getarray($sql);
		
			$exchange_treat_user_cnt += $treat_reward_info["total_user_cnt"];
			$exchange_treat_reward_amount += $treat_reward_info["total_amount"];
		}
		
		$sql = "INSERT INTO tbl_treat_ticket_use_stat(today, os_type, daily_loyalty_amount, daily_platinum_loyalty_amount, daily_treat_amount, daily_platinum_treat_amount, loyalty_keep_amount, treat_keep_amount, exchange_treat_count, exchange_treat_amount) ".
				"VALUES('$today', 2, '$daily_normal_loyalty_amount', '$daily_platinum_loyalty_amount', '$daily_treat_amount', '$daily_platinum_treat_amount', '$loyalty_keep_amount', '$treat_keep_amount', '$exchange_treat_user_cnt', '$exchange_treat_reward_amount') ".
				"ON DUPLICATE KEY UPDATE daily_loyalty_amount=VALUES(daily_loyalty_amount), daily_platinum_loyalty_amount=VALUES(daily_platinum_loyalty_amount), daily_treat_amount=VALUES(daily_treat_amount), daily_platinum_treat_amount=VALUES(daily_platinum_treat_amount), loyalty_keep_amount=VALUES(loyalty_keep_amount), treat_keep_amount=VALUES(treat_keep_amount), exchange_treat_count=VALUES(exchange_treat_count), exchange_treat_amount=VALUES(exchange_treat_amount);";
		$db_analysis->execute($sql);
		
		//Amazon
		$sql = "SELECT IFNULL(SUM(moneyin),0) FROM tbl_game_cash_stats_amazon_daily2 WHERE betlevel BETWEEN 0 AND 9 AND writedate = '$today';";
		$daily_normal_loyalty_amount = $db_slave_main2->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(moneyin),0) FROM tbl_game_cash_stats_amazon_daily2 WHERE betlevel BETWEEN 10 AND 19 AND writedate = '$today';";
		$daily_platinum_loyalty_amount = $db_slave_main2->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(loyaltypot),0) FROM tbl_user_treat t1 join tbl_user_ext t2 on t1.useridx = t2.useridx WHERE t2.platform = 3 AND t1.useridx > $std_useridx;";
		$loyalty_keep_amount = $db_main->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(treatamount),0) FROM tbl_treat_ticket t1 join tbl_user_ext t2 on t1.useridx = t2.useridx WHERE t2.platform = 3 AND t1.useridx > $std_useridx;";
		$treat_keep_amount = $db_main->getvalue($sql);
		
		$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
				"FROM tbl_game_cash_stats_amazon_daily2 ".
				"WHERE betlevel BETWEEN 0 AND 9 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
		$daily_treat_amount = $db_slave_main2->getvalue($treat_sql);
		
		$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
				"FROM tbl_game_cash_stats_amazon_daily2 ".
				"WHERE betlevel BETWEEN 10 AND 19 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
		$daily_platinum_treat_amount = $db_slave_main2->getvalue($treat_sql);
		
		$exchange_treat_user_cnt = 0;
		$exchange_treat_reward_amount = 0;
		
		for($i=0; $i<10; $i++)
		{
			$sql = "SELECT SUM(amount) AS total_amount, COUNT(DISTINCT useridx) AS total_user_cnt ".
					"FROM tbl_user_freecoin_log_$i ".
					"WHERE type IN (5, 17) AND category = 3 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
			$treat_reward_info = $db_slave_main2->getarray($sql);
		
			$exchange_treat_user_cnt += $treat_reward_info["total_user_cnt"];
			$exchange_treat_reward_amount += $treat_reward_info["total_amount"];
		}
		
		$sql = "INSERT INTO tbl_treat_ticket_use_stat(today, os_type, daily_loyalty_amount, daily_platinum_loyalty_amount, daily_treat_amount, daily_platinum_treat_amount, loyalty_keep_amount, treat_keep_amount, exchange_treat_count, exchange_treat_amount) ".
				"VALUES('$today', 3, '$daily_normal_loyalty_amount', '$daily_platinum_loyalty_amount', '$daily_treat_amount', '$daily_platinum_treat_amount', '$loyalty_keep_amount', '$treat_keep_amount', '$exchange_treat_user_cnt', '$exchange_treat_reward_amount') ".
				"ON DUPLICATE KEY UPDATE daily_loyalty_amount=VALUES(daily_loyalty_amount), daily_platinum_loyalty_amount=VALUES(daily_platinum_loyalty_amount), daily_treat_amount=VALUES(daily_treat_amount), daily_platinum_treat_amount=VALUES(daily_platinum_treat_amount), loyalty_keep_amount=VALUES(loyalty_keep_amount), treat_keep_amount=VALUES(treat_keep_amount), exchange_treat_count=VALUES(exchange_treat_count), exchange_treat_amount=VALUES(exchange_treat_amount);";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		// Insert tbl_honor_level_stat_log
		for($i = 0; $i < 10; $i++)
		{
			if($i == 0)
				$sub_sql = "AND honor_level BETWEEN 3 AND 9";
			else if($i == 1)
				$sub_sql = "AND honor_level BETWEEN 10 AND 19";
			else if($i == 2)
				$sub_sql = "AND honor_level BETWEEN 20 AND 29";
			else if($i == 3)
				$sub_sql = "AND honor_level BETWEEN 30 AND 39";
			else if($i == 4)
				$sub_sql = "AND honor_level BETWEEN 40 AND 49";
			else if($i == 5)
				$sub_sql = "AND honor_level BETWEEN 50 AND 59";
			else if($i == 6)
				$sub_sql = "AND honor_level BETWEEN 60 AND 69";
			else if($i == 7)
				$sub_sql = "AND honor_level BETWEEN 70 AND 79";
			else if($i == 8)
				$sub_sql = "AND honor_level BETWEEN 80 AND 89";
			else if($i == 9)
				$sub_sql = "AND honor_level BETWEEN 90 AND 99";
			else if($i == 10)
				$sub_sql = "AND honor_level BETWEEN 90 AND 100";
	
			$sql = "SELECT $i AS honor_level, COUNT(honor_level) AS cnt_honor_user, IFNULL(SUM(IF(logindate < DATE_SUB(NOW(), INTERVAL 2 WEEK), 1, 0)), 0) AS cnt_honor_2week  FROM tbl_user WHERE useridx  > $std_useridx $sub_sql";
			$honor_level_info = $db_main->getarray($sql);
	
			$honor_level = $honor_level_info["honor_level"];
			$cnt_honor_user = $honor_level_info["cnt_honor_user"];
			$cnt_honor_2week = $honor_level_info["cnt_honor_2week"];
	
			$sql = "INSERT INTO tbl_honor_level_stat_log(today, type, honor_level_".$i.") VALUES('$today',0,$cnt_honor_user) ".
					"ON DUPLICATE KEY UPDATE honor_level_".$i." = VALUES(honor_level_".$i.")";
			$db_analysis->execute($sql);
	
			$sql = "INSERT INTO tbl_honor_level_stat_log(today, type, honor_level_".$i.") VALUES('$today',1,$cnt_honor_2week) ".
					"ON DUPLICATE KEY UPDATE honor_level_".$i." = VALUES(honor_level_".$i.")";
			$db_analysis->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		//Insert tbl_fameball_daily
		$sql = "SELECT slottype, os_type, SUM(fameball) AS sum_fameball FROM tbl_bigwin_log ".
				"WHERE useridx > $std_useridx AND objectidx NOT IN (0) AND objectidx < 1000000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' GROUP BY slottype, os_type";
		$user_fameball_info = $db_slave_main2->gettotallist($sql);
	
		for($i=0; $i<sizeof($user_fameball_info); $i++)
		{
			$slottype = $user_fameball_info[$i]["slottype"];
			$os_type = $user_fameball_info[$i]["os_type"];
			$sum_fameball = $user_fameball_info[$i]["sum_fameball"];
				
			$sql = "INSERT INTO tbl_fameball_daily(today, bettype, os_type, slottype, fameball) ".
					"VALUES('$today', 0, '$os_type', '$slottype','$sum_fameball') ON DUPLICATE KEY UPDATE ".
					"fameball=VALUES(fameball);";
			$db_analysis->execute($sql);
		}
	
		$sql = "SELECT slottype, SUM(fameball) AS sum_fameball, os_type FROM tbl_bigwin_log ".
				"WHERE useridx > $std_useridx AND objectidx >= 1000000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' GROUP BY slottype, os_type";
		$user_fameball_info = $db_slave_main2->gettotallist($sql);
	
		for($i=0; $i<sizeof($user_fameball_info); $i++)
		{
			$slottype = $user_fameball_info[$i]["slottype"];
			$os_type = $user_fameball_info[$i]["os_type"];
			$sum_fameball = $user_fameball_info[$i]["sum_fameball"];
	
			$sql = "INSERT INTO tbl_fameball_daily(today, bettype, os_type, slottype, fameball) ".
					"VALUES('$today', 1, '$os_type', '$slottype','$sum_fameball') ON DUPLICATE KEY UPDATE ".
					"fameball=VALUES(fameball);";
			$db_analysis->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	

	// insert table : tbl_game_cash_stats_daily2 (usercount)
	try
	{
		if (date("H") == "00")
		{
			$today = date("Y-m-d", time() - 60 * 60);
			$tomorrow = date("Y-m-d");
		}
		else
		{
			$today = date("Y-m-d");
			$tomorrow = date("Y-m-d",strtotime("+1 days"));
		}
	
		//Web
		$sql = "select  date_format(writedate, '%Y-%m-%d') as writedate, slottype, mode, betlevel, count(distinct useridx) as cnt_user, is_v2 from tbl_user_gamelog ".
				"where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 AND useridx > 20000  group by date_format(writedate, '%Y-%m-%d'), slottype, mode, betlevel, is_v2;";
		$usercount_data = $db_other->gettotallist($sql);
			
		$insert_sql = "";
			
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$slottype = $usercount_data[$i]["slottype"];
			$mode = $usercount_data[$i]["mode"];
			$betlevel = $usercount_data[$i]["betlevel"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
			$is_v2 = $usercount_data[$i]["is_v2"];
	
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_cash_stats_daily2(writedate, slottype, mode, betlevel, usercount,is_v2) VALUES('$writedate', $slottype, $mode, $betlevel, $cnt_user, $is_v2)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $slottype, $mode, $betlevel, $cnt_user, $is_v2)";
			}
		}
			
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_main2->execute($insert_sql);
		}
	
		//IOS
		$sql = "select  date_format(writedate, '%Y-%m-%d') as writedate, slottype, mode, betlevel, count(distinct useridx) as cnt_user from tbl_user_gamelog_ios ".
				"where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 AND useridx > 20000  group by date_format(writedate, '%Y-%m-%d'), slottype, mode, betlevel;";
		$usercount_data = $db_other->gettotallist($sql);
			
		$insert_sql = "";
			
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$slottype = $usercount_data[$i]["slottype"];
			$mode = $usercount_data[$i]["mode"];
			$betlevel = $usercount_data[$i]["betlevel"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
	
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_cash_stats_ios_daily2(writedate, slottype, mode, betlevel, usercount) VALUES('$writedate', $slottype, $mode, $betlevel, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $slottype, $mode, $betlevel, $cnt_user)";
			}
		}
			
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_main2->execute($insert_sql);
		}
	
		//Android
		$sql = "select  date_format(writedate, '%Y-%m-%d') as writedate, slottype, mode, betlevel, count(distinct useridx) as cnt_user from tbl_user_gamelog_android ".
				"where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' AND slottype > 0 AND useridx > 20000 group by date_format(writedate, '%Y-%m-%d'), slottype, mode, betlevel;";
		$usercount_data = $db_other->gettotallist($sql);
			
		$insert_sql = "";
			
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$slottype = $usercount_data[$i]["slottype"];
			$mode = $usercount_data[$i]["mode"];
			$betlevel = $usercount_data[$i]["betlevel"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
	
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_cash_stats_android_daily2(writedate, slottype, mode, betlevel, usercount) VALUES('$writedate', $slottype, $mode, $betlevel, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $slottype, $mode, $betlevel, $cnt_user)";
			}
		}
			
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_main2->execute($insert_sql);
		}
	
		//amazon
		$sql = "select  date_format(writedate, '%Y-%m-%d') as writedate, slottype, mode, betlevel, count(distinct useridx) as cnt_user from tbl_user_gamelog_amazon ".
				"where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 AND useridx > 20000  group by date_format(writedate, '%Y-%m-%d'), slottype, mode, betlevel;";
		$usercount_data = $db_other->gettotallist($sql);
			
		$insert_sql = "";
			
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$slottype = $usercount_data[$i]["slottype"];
			$mode = $usercount_data[$i]["mode"];
			$betlevel = $usercount_data[$i]["betlevel"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
	
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_cash_stats_amazon_daily2(writedate, slottype, mode, betlevel, usercount) VALUES('$writedate', $slottype, $mode, $betlevel, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $slottype, $mode, $betlevel, $cnt_user)";
			}
		}
			
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_main2->execute($insert_sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	try
	{
		//Web
		$sql = "SELECT date_format(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 0 AS cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user ".
				"FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) GROUP BY date_format(writedate, '%Y-%m-%d')	".
				"union all	".
				"SELECT  date_format(writedate, '%Y-%m-%d') AS writedate, 2 as cnt_type, slottype as cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user	".
				"FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) GROUP BY date_format(writedate, '%Y-%m-%d'), slottype	".
				"union all	".
				"select  date_format(writedate, '%Y-%m-%d') as writedate, 3 AS cnt_type, mode as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 group by date_format(writedate, '%Y-%m-%d'), mode	".
				"union all	".
				"select date_format(writedate, '%Y-%m-%d') as writedate, 4 AS cnt_type, betlevel as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) group by date_format(writedate, '%Y-%m-%d'), betlevel";
		$usercount_data = $db_other->gettotallist($sql);
	
		$insert_sql = "";
	
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 0;
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$cnt_type_value = $usercount_data[$i]["cnt_type_value"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
				
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_distinct_usercnt(writedate, os_type, cnt_type, cnt_type_value, usercount) VALUES('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
		}
	
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
	
		//IOS
		$sql = "SELECT date_format(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 0 AS cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user ".
				"FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) GROUP BY date_format(writedate, '%Y-%m-%d')	".
				"union all	".
				"SELECT  date_format(writedate, '%Y-%m-%d') AS writedate, 2 as cnt_type, slottype as cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user	".
				"FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) GROUP BY date_format(writedate, '%Y-%m-%d'), slottype	".
				"union all	".
				"select  date_format(writedate, '%Y-%m-%d') as writedate, 3 AS cnt_type, mode as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog_ios where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 group by date_format(writedate, '%Y-%m-%d'), mode	".
				"union all	".
				"select date_format(writedate, '%Y-%m-%d') as writedate, 4 AS cnt_type, betlevel as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog_ios where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) group by date_format(writedate, '%Y-%m-%d'), betlevel";
		$usercount_data = $db_other->gettotallist($sql);
	
		$insert_sql = "";
	
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 1;
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$cnt_type_value = $usercount_data[$i]["cnt_type_value"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
	
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_distinct_usercnt(writedate, os_type, cnt_type, cnt_type_value, usercount) VALUES('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
		}
	
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
	
		//android
		$sql = "SELECT date_format(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 0 AS cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user ".
				"FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) GROUP BY date_format(writedate, '%Y-%m-%d')	".
				"union all	".
				"SELECT  date_format(writedate, '%Y-%m-%d') AS writedate, 2 as cnt_type, slottype as cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user	".
				"FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) GROUP BY date_format(writedate, '%Y-%m-%d'), slottype	".
				"union all	".
				"select  date_format(writedate, '%Y-%m-%d') as writedate, 3 AS cnt_type, mode as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog_android where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 group by date_format(writedate, '%Y-%m-%d'), mode	".
				"union all	".
				"select date_format(writedate, '%Y-%m-%d') as writedate, 4 AS cnt_type, betlevel as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog_android where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) group by date_format(writedate, '%Y-%m-%d'), betlevel";
		$usercount_data = $db_other->gettotallist($sql);
	
		$insert_sql = "";
	
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 2;
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$cnt_type_value = $usercount_data[$i]["cnt_type_value"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
	
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_distinct_usercnt(writedate, os_type, cnt_type, cnt_type_value, usercount) VALUES('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
		}
	
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
	
		//amazon
		$sql = "SELECT date_format(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 0 AS cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user ".
				"FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) GROUP BY date_format(writedate, '%Y-%m-%d')	".
				"union all	".
				"SELECT  date_format(writedate, '%Y-%m-%d') AS writedate, 2 as cnt_type, slottype as cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user	".
				"FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) GROUP BY date_format(writedate, '%Y-%m-%d'), slottype	".
				"union all	".
				"select  date_format(writedate, '%Y-%m-%d') as writedate, 3 AS cnt_type, mode as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog_amazon where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 group by date_format(writedate, '%Y-%m-%d'), mode	".
				"union all	".
				"select date_format(writedate, '%Y-%m-%d') as writedate, 4 AS cnt_type, betlevel as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog_amazon where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) group by date_format(writedate, '%Y-%m-%d'), betlevel";
		$usercount_data = $db_other->gettotallist($sql);
	
		$insert_sql = "";
	
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 3;
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$cnt_type_value = $usercount_data[$i]["cnt_type_value"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
	
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_distinct_usercnt(writedate, os_type, cnt_type, cnt_type_value, usercount) VALUES('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
		}
	
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	try
	{
		$sql = "SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 1 AS betmode, 0 AS slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 2 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 3 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 4 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 1 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 2 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 3 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 4 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 1 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 2 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 3 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 110 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 4 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 10 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE;";
		$usercount_data = $db_other->gettotallist($sql);
	
		$insert_sql = "";
	
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 0;
			$betmode = $usercount_data[$i]["betmode"];
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$betmode = $usercount_data[$i]["betmode"];
			$slottype = $usercount_data[$i]["slottype"];
			$mode = $usercount_data[$i]["mode"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
	
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_betmode_distinct_usercnt(writedate, os_type, betmode, cnt_type, slottype, mode, usercount) VALUES('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
		}
	
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
	
		$sql = "SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 1 AS betmode, 0 AS slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 2 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 3 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 4 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 1 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 2 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 3 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 4 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 1 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 2 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 3 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 110 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 4 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 10 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE;";
		$usercount_data = $db_other->gettotallist($sql);
	
		$insert_sql = "";
	
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 1;
			$betmode = $usercount_data[$i]["betmode"];
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$betmode = $usercount_data[$i]["betmode"];
			$slottype = $usercount_data[$i]["slottype"];
			$mode = $usercount_data[$i]["mode"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
	
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_betmode_distinct_usercnt(writedate, os_type, betmode, cnt_type, slottype, mode, usercount) VALUES('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
		}
	
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
	
		$sql = "SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 1 AS betmode, 0 AS slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 2 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 3 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 4 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 1 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 2 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 3 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 4 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 1 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 2 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 3 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 110 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 4 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 10 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE;";
		$usercount_data = $db_other->gettotallist($sql);
	
		$insert_sql = "";
	
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 2;
			$betmode = $usercount_data[$i]["betmode"];
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$betmode = $usercount_data[$i]["betmode"];
			$slottype = $usercount_data[$i]["slottype"];
			$mode = $usercount_data[$i]["mode"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
	
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_betmode_distinct_usercnt(writedate, os_type, betmode, cnt_type, slottype, mode, usercount) VALUES('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
		}
	
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
	
		$sql = "SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 1 AS betmode, 0 AS slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 2 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 3 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 4 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 1 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 2 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 3 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 4 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 1 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 2 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 3 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 110 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 4 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 10 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE;";
		$usercount_data = $db_other->gettotallist($sql);
	
		$insert_sql = "";
	
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 3;
			$betmode = $usercount_data[$i]["betmode"];
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$betmode = $usercount_data[$i]["betmode"];
			$slottype = $usercount_data[$i]["slottype"];
			$mode = $usercount_data[$i]["mode"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
	
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_betmode_distinct_usercnt(writedate, os_type, betmode, cnt_type, slottype, mode, usercount) VALUES('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
		}
	
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	// V2 유저 수
	try
	{
		//Web
		$sql = "SELECT date_format(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 0 AS cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user ".
				"FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND is_v2 = 1 GROUP BY date_format(writedate, '%Y-%m-%d')	".
				"union all	".
				"SELECT  date_format(writedate, '%Y-%m-%d') AS writedate, 2 as cnt_type, slottype as cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user	".
				"FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND is_v2 = 1 GROUP BY date_format(writedate, '%Y-%m-%d'), slottype	".
				"union all	".
				"select  date_format(writedate, '%Y-%m-%d') as writedate, 3 AS cnt_type, mode as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 AND is_v2 = 1 group by date_format(writedate, '%Y-%m-%d'), mode	".
				"union all	".
				"select date_format(writedate, '%Y-%m-%d') as writedate, 4 AS cnt_type, betlevel as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND is_v2 = 1 group by date_format(writedate, '%Y-%m-%d'), betlevel";
		$usercount_data = $db_other->gettotallist($sql);
		 
		$insert_sql = "";
		 
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 0;
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$cnt_type_value = $usercount_data[$i]["cnt_type_value"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
			 
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_distinct_usercntV2(writedate, os_type, cnt_type, cnt_type_value, usercount) VALUES('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
		}
		 
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
		 
		//IOS
		$sql = "SELECT date_format(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 0 AS cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user ".
				"FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND is_v2 = 1 GROUP BY date_format(writedate, '%Y-%m-%d')	".
				"union all	".
				"SELECT  date_format(writedate, '%Y-%m-%d') AS writedate, 2 as cnt_type, slottype as cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user	".
				"FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND is_v2 = 1 GROUP BY date_format(writedate, '%Y-%m-%d'), slottype	".
				"union all	".
				"select  date_format(writedate, '%Y-%m-%d') as writedate, 3 AS cnt_type, mode as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog_ios where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 AND is_v2 = 1 group by date_format(writedate, '%Y-%m-%d'), mode	".
				"union all	".
				"select date_format(writedate, '%Y-%m-%d') as writedate, 4 AS cnt_type, betlevel as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog_ios where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND is_v2 = 1 group by date_format(writedate, '%Y-%m-%d'), betlevel";
		$usercount_data = $db_other->gettotallist($sql);
		 
		$insert_sql = "";
		 
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 1;
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$cnt_type_value = $usercount_data[$i]["cnt_type_value"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
			 
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_distinct_usercntV2(writedate, os_type, cnt_type, cnt_type_value, usercount) VALUES('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
		}
		 
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
		 
		//android
		$sql = "SELECT date_format(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 0 AS cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user ".
				"FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND is_v2 = 1 GROUP BY date_format(writedate, '%Y-%m-%d')	".
				"union all	".
				"SELECT  date_format(writedate, '%Y-%m-%d') AS writedate, 2 as cnt_type, slottype as cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user	".
				"FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND is_v2 = 1 GROUP BY date_format(writedate, '%Y-%m-%d'), slottype	".
				"union all	".
				"select  date_format(writedate, '%Y-%m-%d') as writedate, 3 AS cnt_type, mode as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog_android where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 AND is_v2 = 1 group by date_format(writedate, '%Y-%m-%d'), mode	".
				"union all	".
				"select date_format(writedate, '%Y-%m-%d') as writedate, 4 AS cnt_type, betlevel as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog_android where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND is_v2 = 1 group by date_format(writedate, '%Y-%m-%d'), betlevel";
		$usercount_data = $db_other->gettotallist($sql);
		 
		$insert_sql = "";
		 
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 2;
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$cnt_type_value = $usercount_data[$i]["cnt_type_value"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
			 
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_distinct_usercntV2(writedate, os_type, cnt_type, cnt_type_value, usercount) VALUES('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
		}
		 
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
		 
		//amazon
		$sql = "SELECT date_format(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 0 AS cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user ".
				"FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND is_v2 = 1 GROUP BY date_format(writedate, '%Y-%m-%d')	".
				"union all	".
				"SELECT  date_format(writedate, '%Y-%m-%d') AS writedate, 2 as cnt_type, slottype as cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user	".
				"FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND is_v2 = 1 GROUP BY date_format(writedate, '%Y-%m-%d'), slottype	".
				"union all	".
				"select  date_format(writedate, '%Y-%m-%d') as writedate, 3 AS cnt_type, mode as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog_amazon where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 AND is_v2 = 1 group by date_format(writedate, '%Y-%m-%d'), mode	".
				"union all	".
				"select date_format(writedate, '%Y-%m-%d') as writedate, 4 AS cnt_type, betlevel as cnt_type_value, count(distinct useridx) as cnt_user	".
				"from tbl_user_gamelog_amazon where writedate >= '$today 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND is_v2 = 1 group by date_format(writedate, '%Y-%m-%d'), betlevel";
		$usercount_data = $db_other->gettotallist($sql);
		 
		$insert_sql = "";
		 
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 3;
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$cnt_type_value = $usercount_data[$i]["cnt_type_value"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
			 
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_distinct_usercntV2(writedate, os_type, cnt_type, cnt_type_value, usercount) VALUES('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $cnt_type, $cnt_type_value, $cnt_user)";
			}
		}
		 
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	try
	{		 
		$sql = "SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 1 AS betmode, 0 AS slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 2 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 3 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 4 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10  AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 1 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 2 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 3 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 4 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 1 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 0 AND 9 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 2 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 10 AND 19 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 3 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 110 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 4 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 10 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE;";
		$usercount_data = $db_other->gettotallist($sql);
		 
		$insert_sql = "";
		 
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 0;
			$betmode = $usercount_data[$i]["betmode"];
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$betmode = $usercount_data[$i]["betmode"];
			$slottype = $usercount_data[$i]["slottype"];
			$mode = $usercount_data[$i]["mode"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
			 
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_betmode_distinct_usercntV2(writedate, os_type, betmode, cnt_type, slottype, mode, usercount) VALUES('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
		}
		 
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
		 
		$sql = "SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 1 AS betmode, 0 AS slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 2 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 3 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 4 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 1 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 2 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 3 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 4 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 1 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 0 AND 9 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 2 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 10 AND 19 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 3 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 110 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 4 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_ios WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 10 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE;";
		$usercount_data = $db_other->gettotallist($sql);
		 
		$insert_sql = "";
		 
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 1;
			$betmode = $usercount_data[$i]["betmode"];
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$betmode = $usercount_data[$i]["betmode"];
			$slottype = $usercount_data[$i]["slottype"];
			$mode = $usercount_data[$i]["mode"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
			 
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_betmode_distinct_usercntV2(writedate, os_type, betmode, cnt_type, slottype, mode, usercount) VALUES('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
		}
		 
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
		 
		$sql = "SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 1 AS betmode, 0 AS slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 2 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 3 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 4 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 1 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 2 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 3 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 4 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 1 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 0 AND 9 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 2 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 10 AND 19 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 3 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 110 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 4 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_android WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 10 AND is_v2 = 1 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE;";
		$usercount_data = $db_other->gettotallist($sql);
		 
		$insert_sql = "";
		 
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 2;
			$betmode = $usercount_data[$i]["betmode"];
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$betmode = $usercount_data[$i]["betmode"];
			$slottype = $usercount_data[$i]["slottype"];
			$mode = $usercount_data[$i]["mode"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
			 
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_betmode_distinct_usercntV2(writedate, os_type, betmode, cnt_type, slottype, mode, usercount) VALUES('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
		}
		 
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
		 
		$sql = "SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 1 AS betmode, 0 AS slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 2 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 3 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 4 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 1 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 2 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 3 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 110 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 4 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND MODE NOT IN (4, 5, 9, 26, 29, 31) AND betlevel >= 10 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 1 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 2 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 3 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 110 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE
		UNION ALL
		SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 4 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user
		FROM tbl_user_gamelog_amazon WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel >= 10 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE;";
		$usercount_data = $db_other->gettotallist($sql);
		 
		$insert_sql = "";
		 
		for($i = 0; $i < sizeof($usercount_data); $i++)
		{
			$writedate = $usercount_data[$i]["writedate"];
			$os_type = 3;
			$betmode = $usercount_data[$i]["betmode"];
			$cnt_type = $usercount_data[$i]["cnt_type"];
			$betmode = $usercount_data[$i]["betmode"];
			$slottype = $usercount_data[$i]["slottype"];
			$mode = $usercount_data[$i]["mode"];
			$cnt_user = $usercount_data[$i]["cnt_user"];
			 
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_game_betmode_distinct_usercnt(writedate, os_type, betmode, cnt_type, slottype, mode, usercount) VALUES('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
			else
			{
				$insert_sql .= ",('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
			}
		}
		 
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
			$db_other->execute($insert_sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	// 2.0 전환율
	try
	{
		//2.0 로비 변경 사용자
		$sql = "SELECT COUNT(DISTINCT useridx)AS  cnt FROM `tbl_user_switch_log` WHERE is_v2 = 1  AND  logindate <= '$today 23:59:59' AND logindate >= '$today 00:00:00' AND useridx > 20000";
		$switch_change_cnt = $db_slave_livestats->getvalue($sql);
		 
		//2.0 로비 사용자
		$sql = "SELECT COUNT(DISTINCT useridx)AS  cnt FROM `tbl_user_switch` WHERE is_v2 = 1  AND  logindate <= '$today 23:59:59' AND logindate >= '$today 00:00:00' AND useridx > 20000";
		$switchuser_cnt = $db_main->getvalue($sql);
		 
		//2.0 대상자
		$sql = "SELECT COUNT(DISTINCT useridx)AS  cnt FROM `tbl_switch_target_user_loginlog` WHERE  today  = '$today' AND useridx > 20000";
		$switch_targetuser_cnt = $db_slave_livestats->getvalue($sql);
		 
		//웹 사용자
		$sql = "SELECT todayfacebookactivecount FROM user_join_log WHERE today = '$today'";
		$facebookactivecount = $db_analysis->getvalue($sql);
		 
		$insert_sql = " INSERT INTO tbl_user_switch_daily(today,switch_change_cnt,switchuser_cnt,facebookactivecount, switch_targetuser_cnt) VALUES('$today', '$switch_change_cnt','$switchuser_cnt','$facebookactivecount','$switch_targetuser_cnt') ".
				" ON DUPLICATE KEY UPDATE switch_change_cnt=VALUES(switch_change_cnt), switchuser_cnt=VALUES(switchuser_cnt),facebookactivecount=VALUES(facebookactivecount),switch_targetuser_cnt=VALUES(switch_targetuser_cnt);";
		 
		if($insert_sql != "")
			$db_main2->execute($insert_sql);
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	try
	{
		// 최근 24시간 동안 가입한 사용자의 미게임률을 판단
		$sql = "SELECT COUNT(*) AS join_today FROM tbl_user WHERE createdate > DATE_SUB(NOW(), INTERVAL 1 DAY)";
		$day_total = $db_main->getvalue($sql);
	
		$sql = "SELECT SUM(join_nogame_today) AS join_nogame_today
				FROM (
					SELECT
						useridx, (CASE WHEN (IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END),0)
						+ IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END),0)
						+ IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END),0)
						+ IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END),0)
						) < 1 THEN 1 ELSE 0 END) AS join_nogame_today
					FROM tbl_user
					WHERE createdate > DATE_SUB(NOW(), INTERVAL 1 DAY)
					GROUP BY useridx
				) t1";
		$day_noplay = $db_main->getvalue($sql);
	
		if ($day_total == 0)
			$rate = 0;
		else
			$rate = ($day_total - $day_noplay) * 100 / $day_total;
	
		$sql = "DELETE FROM activeuser_stat WHERE writedate=LEFT(NOW(),10)";
		$db_analysis->execute($sql);
	
		$sql = "INSERT INTO activeuser_stat(activeuser_rate,writedate) VALUES('$rate',LEFT(NOW(),10))";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	
	try
	{
		$sql = " UPDATE tbl_coupon_issue_log t1, (SELECT couponidx,STATUS,usedate FROM tbl_coupon WHERE STATUS = 1 AND usedate >= '$today 00:00:00' AND usedate <= '$today 23:59:59' ) t2  ".
				" SET t1.status = t2.status, t1.usedate = t2.usedate ".
				" WHERE t1.couponidx = t2.couponidx";
		$db_main2->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	try
	{
		$sql = "TRUNCATE TABLE tbl_user_honor_rank;";
		$db_main2->execute($sql);
		 
		//block_user
		$sql = "SELECT useridx FROM tbl_user_block ORDER BY blockdate DESC LIMIT 20";
		$block_user_list = $db_slave_main->gettotallist($sql);
		 
		$block_user_sql = "";
	
		for($i = 0; $i < sizeof($block_user_list); $i++)
		{
			$block_useridx = $block_user_list[$i]["useridx"];
	
			if($i == 0)
				$block_user_sql = $block_useridx;
			else
				$block_user_sql .= ",".$block_useridx;
		}
		 
		//daily honor rank
		$sql = "SELECT useridx FROM tbl_user WHERE useridx NOT IN ( $block_user_sql ) ORDER BY honor_point desc LIMIT 10";
		$daily_honor_rank_list = $db_slave_main->gettotallist($sql);
		 
		$daily_honor_rank_sql = "";
		 
		for($i = 0; $i < sizeof($daily_honor_rank_list); $i++)
		{
			$daily_honor_rank_useridx = $daily_honor_rank_list[$i]["useridx"];
	
			$rankidx = $i + 1;
			 
			if($i == 0)
				$daily_honor_rank_sql = "INSERT INTO tbl_user_honor_rank(rankidx, useridx, rank_type, writedate) VALUES($rankidx, $daily_honor_rank_useridx, 0, NOW())";
			else
				$daily_honor_rank_sql .= ",($rankidx, $daily_honor_rank_useridx, 0, NOW())";
		}
		 
		if($daily_honor_rank_sql != "")
		{
			$daily_honor_rank_sql .= " ON DUPLICATE KEY UPDATE writedate=VALUES(writedate)";
			$db_main2->execute($daily_honor_rank_sql);
		}
		 
		//weekly honor rank
		$sql = "SELECT useridx FROM tbl_user_weekly_point WHERE useridx NOT IN ( $block_user_sql ) AND weekly = yearweek(now()) ORDER BY honor_point DESC LIMIT 10";
		$weekly_honor_rank_list = $db_slave_main2->gettotallist($sql);
	
		$weekly_honor_rank_sql = "";
	
		for($i = 0; $i < sizeof($weekly_honor_rank_list); $i++)
		{
			$weekly_honor_rank_useridx = $weekly_honor_rank_list[$i]["useridx"];
			 
			$rankidx = $i + 1;
			 
			if($i == 0)
				$weekly_honor_rank_sql = "INSERT INTO tbl_user_honor_rank(rankidx, useridx, rank_type, writedate) VALUES($rankidx, $weekly_honor_rank_useridx, 1, NOW())";
			else
				$weekly_honor_rank_sql .= ",($rankidx, $weekly_honor_rank_useridx, 1, NOW())";
		}
	
		if($weekly_honor_rank_sql != "")
		{
			$weekly_honor_rank_sql .= " ON DUPLICATE KEY UPDATE writedate=VALUES(writedate)";
			$db_main2->execute($weekly_honor_rank_sql);
		}
		 
		//monthly honor rank
		$sql = "SELECT useridx FROM tbl_user_monthly_point WHERE useridx NOT IN ( $block_user_sql ) AND monthly = EXTRACT(YEAR_MONTH FROM NOW()) ORDER BY honor_point DESC LIMIT 10";
		$monthly_honor_rank_list = $db_slave_main2->gettotallist($sql);
	
		$monthly_honor_rank_sql = "";
	
		for($i = 0; $i < sizeof($monthly_honor_rank_list); $i++)
		{
			$monthly_honor_rank_useridx = $monthly_honor_rank_list[$i]["useridx"];
			 
			$rankidx = $i + 1;
			 
			if($i == 0)
				$monthly_honor_rank_sql = "INSERT INTO tbl_user_honor_rank(rankidx, useridx, rank_type, writedate) VALUES($rankidx, $monthly_honor_rank_useridx, 2, NOW())";
			else
				$monthly_honor_rank_sql .= ",($rankidx, $monthly_honor_rank_useridx, 2, NOW())";
		}
	
		if($monthly_honor_rank_sql != "")
		{
			$monthly_honor_rank_sql .= " ON DUPLICATE KEY UPDATE writedate=VALUES(writedate)";
			$db_main2->execute($monthly_honor_rank_sql);
		}
		 
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	try 
	{
		$sql = "SELECT MAX(eventidx) FROM tbl_push_event WHERE end_eventdate <= DATE_SUB(NOW(), INTERVAL 1 MONTH);";
		$max_eventidx = $db_slave_main2->getvalue($sql);
		
		$sql = "SELECT COUNT(*) FROM tbl_push_event_user WHERE eventcode < '$max_eventidx'";
		$push_event_user_cnt = $db_slave_main2->getvalue($sql);
		
		if($push_event_user_cnt > 0)
		{
			$sql = "DELETE FROM tbl_push_event_user WHERE eventcode < '$max_eventidx'";
			$db_main2->execute($sql);
		}	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	try
	{
		$sql = "UPDATE tbl_scheduler_log SET status = $issuccess, writedate = NOW() WHERE logidx = $logidx;";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();
	$db_slave_main2->end();
	$db_analysis->end();
	$db_friend->end();
	$db_game->end();
	$db_other->end();
	$db_livestats->end();
	$db_slave_livestats->end();
	$db_mobile->end();
	$db_inbox->end();
	$db_slave_main->end();
?>
