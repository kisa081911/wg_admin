<?
	include("../common/common_include.inc.php");
	include("../common/dbconnect/db_util_redshift.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_slave_main2 = new CDatabase_Slave_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_friend = new CDatabase_Friend();
	$db_game = new CDatabase_Game();
	$db_other = new CDatabase_Other();
	$db_livestats = new CDatabase_Livestats();
	$db_slave_livestats = new CDatabase_Slave_Livestats();
	$db_mobile = new CDatabase_Mobile();
	$db_inbox = new CDatabase_Inbox();
	$db_slave_main = new CDatabase_Slave_Main();
	
	$db_main->execute("SET wait_timeout=7200");
	$db_main2->execute("SET wait_timeout=7200");
	$db_slave_main2->execute("SET wait_timeout=7200");
	$db_analysis->execute("SET wait_timeout=7200");
	$db_friend->execute("SET wait_timeout=7200");
	$db_game->execute("SET wait_timeout=7200");
	$db_other->execute("SET wait_timeout=7200");
	$db_livestats->execute("SET wait_timeout=7200");
	$db_slave_livestats->execute("SET wait_timeout=7200");
	$db_mobile->execute("SET wait_timeout=7200");
	$db_inbox->execute("SET wait_timeout=7200");
	$db_slave_main->execute("SET wait_timeout=7200");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$port = "";
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}
	
	try
	{
 		$sql = "INSERT INTO tbl_scheduler_log(today, type, status, writedate) VALUES('$today', 301, 0, NOW());";
 		$db_analysis->execute($sql);
 		$sql = "SELECT LAST_INSERT_ID();";
 		$logidx = $db_analysis->getvalue($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());		
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/1hour_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/1hour_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("1minute_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	$facebook = new Facebook(array(
			'appId'  => FACEBOOK_APP_ID,
			'secret' => FACEBOOK_SECRET_KEY,
			'cookie' => true,
	));
	
	try
	{
		//Insert user_join_log, user_revisit_log
		if (date("H") == "00")
		{
			$today = date("Y-m-d", time() - 60 * 60);
			$tomorrow = date("Y-m-d", time() + 60 * 60);
		}
		else
		{
			$today = date("Y-m-d");
			$tomorrow = date("Y-m-d", time() + 24 * 60 * 60);
		}
		
		// 총 회원수, 일일 회원가입수, 일일 Active 회원 수, 결제율
		$sql = "SELECT COUNT(*) FROM tbl_user WHERE useridx>$std_useridx";
		$totalcount = $db_main->getvalue($sql);
		
		// vip_level 3이상 회원수(vip 유저)
		$sql = "SELECT COUNT(*) FROM tbl_user_detail WHERE useridx > $std_useridx AND vip_level >= 3";
		$todayvipcount = $db_main->getvalue($sql);
		
		// 프리미엄 회원수($39 이상 구매)
		$sql = "SELECT DISTINCT COUNT(useridx) FROM tbl_user_detail WHERE useridx > $std_useridx AND ispremium = 1";
		$todaypremiumcount = $db_main->getvalue($sql);
		
		//일일 회원 가임자 수
		$sql = "SELECT COUNT(*) FROM tbl_user_ext WHERE createdate >= '$today' AND createdate < '$tomorrow'";
		$todayjoincount = $db_main->getvalue($sql);	

		//일일 IOS 가입자 수
		$sql = "SELECT COUNT(*) FROM tbl_user_ext WHERE platform = 1 AND createdate >= '$today' AND createdate < '$tomorrow'";
		$todaymobilejoincount = $db_main->getvalue($sql);

		//일일  Android 가입자 수
		$sql = "SELECT COUNT(*) FROM tbl_user_ext WHERE platform = 2 AND createdate >= '$today' AND createdate < '$tomorrow'";
		$todayandroidjoincount = $db_main->getvalue($sql);
		
		//일일  Amazon 가입자 수
		$sql = "SELECT COUNT(*) FROM tbl_user_ext WHERE platform = 3 AND createdate >= '$today' AND createdate < '$tomorrow'";
		$todayamazonjoincount = $db_main->getvalue($sql);
				
		$todayactivecount = 0;
		$active_facebook_cnt = 0;
		$active_ios_cnt = 0;
		$active_android_cnt = 0;
		$active_amazon_cnt = 0;
		
		//일일 플랫폼 별 활동 유저수
		for($i=0; $i<10; $i++)
		{
			$sql = "SELECT SUM(CASE WHEN platform = 0 THEN 1 ELSE 0 END) AS facebook, SUM(CASE WHEN platform = 1 THEN 1 ELSE 0 END) AS ios, SUM(CASE WHEN platform = 2 THEN 1 ELSE 0 END) AS android, SUM(CASE WHEN platform = 3 THEN 1 ELSE 0 END) AS amazon ".
					"FROM tbl_user_login_log_$i ".
					"WHERE today = '$today'";		
			$active_info = $db_slave_livestats->getarray($sql);

			$active_facebook_cnt += $active_info["facebook"];
			$active_ios_cnt += $active_info["ios"];
			$active_android_cnt += $active_info["android"];
			$active_amazon_cnt += $active_info["amazon"];
		}

		//일일 활동 유저(중복제거)
		for($i=0; $i<10; $i++)
		{
			$sql = "SELECT COUNT(DISTINCT useridx) AS active_count ".
			"FROM tbl_user_login_log_$i ".
			"WHERE today = '$today'";
			
			$active_count = $db_slave_livestats->getvalue($sql);
			
			$todayactivecount += $active_count;
		}		
		
		//일일 결제 유저 수(중복제거)
		$sql = "SELECT COUNT(DISTINCT useridx) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order WHERE useridx > $std_useridx AND STATUS = 1 AND writedate>='$today 00:00:00' AND writedate<'$tomorrow 00:00:00' ".
				"	UNION ALL ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND STATUS = 1 AND writedate>='$today 00:00:00' AND writedate<'$tomorrow 00:00:00' ".
				"	UNION ALL ".
				"	SELECT useridx FROM tbl_product_order_earn WHERE useridx > $std_useridx AND STATUS = 1 AND writedate>='$today 00:00:00' AND writedate<'$tomorrow 00:00:00' ".
				") t1";
		$todaypaycount = $db_main->getvalue($sql);
		
		//일일 결제 유저 수(중복제거)
		$sql = "SELECT COUNT(DISTINCT useridx) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order WHERE useridx > $std_useridx AND STATUS = 1 AND writedate>='$today 00:00:00' AND writedate<'$tomorrow 00:00:00' ".
				"	UNION ALL ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND STATUS = 1 AND writedate>='$today 00:00:00' AND writedate<'$tomorrow 00:00:00' ".
				") t1";
		$todaypackagecount = $db_main->getvalue($sql);
		
		//결제 유저 중 활동 유저
		$sql = "SELECT COUNT(*) FROM tbl_user_ext WHERE useridx > $std_useridx AND logindate>='$today' AND logindate<'$tomorrow' ".
				"AND (EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1) ".
				"OR EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1) ".
				"OR EXISTS (SELECT * FROM tbl_product_order_earn WHERE useridx=tbl_user_ext.useridx AND status=1))";		
		$haspaycount = $db_main->getvalue($sql);		
		
		//결제 유저 중 활동 유저
		$sql = "SELECT COUNT(*) FROM tbl_user_ext WHERE useridx > $std_useridx AND logindate>='$today' AND logindate<'$tomorrow' ".
				"AND (EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1) ".
				"OR EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1))";				
		$haspackagecount = $db_main->getvalue($sql);

		//결제 유저 중 vip_level 3이상이고 로그인한 유저 수
		$sql = "SELECT COUNT(*) FROM tbl_user_ext WHERE logindate>='$today' AND logindate<'$tomorrow' ". 
				"AND EXISTS (SELECT * FROM tbl_user_detail WHERE useridx = tbl_user_ext.useridx AND vip_level >= 3)	".
				"AND (EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND STATUS=1)	". 
				"OR EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND STATUS=1));";
		$hasvipcount = $db_main->getvalue($sql);
		
		//가입 2주 경과
		$sql = "SELECT COUNT(*) FROM tbl_user_ext WHERE useridx > $std_useridx AND logindate>='$today' AND logindate<'$tomorrow' AND createdate<=DATE_SUB(logindate, INTERVAL 2 WEEK)";
		$week2count = $db_main->getvalue($sql);

		//가입 1달 경과
		$sql = "SELECT COUNT(*) FROM tbl_user_ext WHERE useridx > $std_useridx AND logindate>='$today' AND logindate<'$tomorrow' AND createdate<=DATE_SUB(logindate, INTERVAL 1 MONTH)";
		$month1count = $db_main->getvalue($sql);
		
		//가입 3달 경과
		$sql = "SELECT COUNT(*) FROM tbl_user_ext WHERE useridx > $std_useridx AND logindate>='$today' AND logindate<'$tomorrow' AND createdate<=DATE_SUB(logindate, INTERVAL 3 MONTH)";
		$month3count = $db_main->getvalue($sql);
		
		//가입 6달 경과
		$sql = "SELECT COUNT(*) FROM tbl_user_ext WHERE useridx > $std_useridx AND logindate>='$today' AND logindate<'$tomorrow' AND createdate<=DATE_SUB(logindate, INTERVAL 6 MONTH)";
		$month6count = $db_main->getvalue($sql);
		
		//가입 9달 경과
		$sql = "SELECT COUNT(*) FROM tbl_user_ext WHERE useridx > $std_useridx AND logindate>='$today' AND logindate<'$tomorrow' AND createdate<=DATE_SUB(logindate, INTERVAL 9 MONTH)";
		$month9count = $db_main->getvalue($sql);
		
		// 앱 사용자, 메인 팽페이지, freechips 팽페이지
		try
		{
			$app = $facebook->api("/".FACEBOOK_APP_ID."?fields=daily_active_users,daily_active_users_rank,monthly_active_users,monthly_active_users_rank,weekly_active_users");
			$daily_active_users = $app["daily_active_users"];
			$daily_active_users_rank = $app["daily_active_users_rank"];
			$weekly_active_users = $app["weekly_active_users"];
			$monthly_active_users = $app["monthly_active_users"];
			$monthly_active_users_rank = $app["monthly_active_users_rank"];
				
			//$fan = $facebook->api("/".FAN_PAGE_ID."?fields=talking_about_count,fan_count");
			$fan_likes = 0;
			$fan_talking = 0;
		}
		catch (FacebookApiException $e)
		{
			$params = array('cmd'=>'facebook_error_log', 'ipaddress'=>'1 hour scheduler', 'facebookid'=>'', 'data_cmd'=>303, 'errcode'=>$e->getMessage());
			log_node_async("log", $params);
		}
		
		$sql = "DELETE FROM user_join_log WHERE today='$today';".
				"INSERT INTO user_join_log(today,totalcount,todayvipcount,todaypremiumcount,todayjoincount,todayactivecount,todayfacebookactivecount,todayiosactivecount,todayandroidactivecount,todayamazonactivecount,todaypaycount,todaypackagecount,haspaycount,haspackagecount,hasvipcount,week2count,month1count,month3count,month6count,month9count,daily_active_users,weekly_active_users,monthly_active_users,fan_likes,fan_talking,dau_rank,mau_rank) ".
				"VALUES('$today','$totalcount','$todayvipcount','$todaypremiumcount','$todayjoincount','$todayactivecount','$active_facebook_cnt','$active_ios_cnt', '$active_android_cnt', '$active_amazon_cnt','$todaypaycount','$todaypackagecount','$haspaycount','$haspackagecount','$hasvipcount','$week2count','$month1count','$month3count','$month6count','$month9count','$daily_active_users','$weekly_active_users','$monthly_active_users', '$fan_likes', '$fan_talking', $daily_active_users_rank, $monthly_active_users_rank)";		
		$db_analysis->execute($sql);
		
		$total_revisitcount = $todayactivecount - $todayjoincount;
		$mobile_revisitcount = $active_ios_cnt - $todaymobilejoincount;
		$android_revisitcount = $active_android_cnt - $todayandroidjoincount;
		$amazon_revisitcount = $active_amazon_cnt - $todayamazonjoincount;
		 
		$sql = "DELETE FROM user_revisit_log WHERE today='$today';".
				"INSERT INTO user_revisit_log VALUES('$today', '$todayactivecount', '$active_ios_cnt', '$active_android_cnt', '$active_amazon_cnt', '$total_revisitcount', '$mobile_revisitcount', '$android_revisitcount', '$amazon_revisitcount')";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		//잭팟 일일 합계
		//일반
		$sql = "SELECT devicetype, slottype, SUM(amount) AS amount ".
				"FROM tbl_jackpot_log ".
				"WHERE objectidx NOT IN (0) AND objectidx < 1000000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' ".
				"GROUP BY devicetype, slottype";
		
		$jackpot_list = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($jackpot_list); $i++)
		{
			$sql = "INSERT INTO tbl_jackpot_stat_daily VALUES('$today', '0', ".$jackpot_list[$i]["devicetype"].", ".$jackpot_list[$i]["slottype"].", ".$jackpot_list[$i]["amount"].") ON DUPLICATE KEY UPDATE ".
					"amount=".$jackpot_list[$i]["amount"];
					
			$db_analysis->execute($sql);
		}
		
		//하이롤러
		$sql = "SELECT devicetype, slottype, SUM(amount) AS amount ".
				"FROM tbl_jackpot_log ".
				"WHERE objectidx >= 1000000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' ".
				"GROUP BY devicetype, slottype ";
		
		$jackpot_list = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($jackpot_list); $i++)
		{
			$sql = "INSERT INTO tbl_jackpot_stat_daily VALUES('$today', '1', ".$jackpot_list[$i]["devicetype"].", ".$jackpot_list[$i]["slottype"].", ".$jackpot_list[$i]["amount"].") ON DUPLICATE KEY UPDATE ".
					"amount=".$jackpot_list[$i]["amount"];
			
				$db_analysis->execute($sql);
		}	
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}	

	try 
	{
		// user_coin_log 남기기	
		if (date("H") == "00")
		{
			$today = date("Y-m-d", time() - 60 * 60);
		}
		else
		{
			$today = date("Y-m-d");
		}
		
		$str_useridx = 20000;
		
		if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
		{
			$port = ":8081";
			$str_useridx = 10000;
		}
		
		$tail = " WHERE useridx > $str_useridx";
		$order_tail = " AND EXISTS (SELECT useridx FROM tbl_product_order WHERE useridx=tbl_user.useridx AND status=1 AND writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY) GROUP BY useridx HAVING SUM(facebookcredit)/10 >= 39)";
		$active_tail = " AND logindate >= DATE_SUB(NOW(), INTERVAL 7 DAY)";
		$not_order_tail = " AND NOT EXISTS (SELECT useridx FROM tbl_product_order WHERE useridx=tbl_user.useridx AND status=1 AND writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY) GROUP BY useridx HAVING SUM(facebookcredit)/10 < 39)";
		
		// 모든 사용자 코인 합계, 평균
		$sql = "SELECT IFNULL(ROUND(SUM(coin)),0) AS total_coin,IFNULL(ROUND(AVG(coin)),0) AS avg_coin FROM tbl_user $tail";
		$data = $db_main->getarray($sql);
		
		$alluser_total_coin = $data["total_coin"];
		$alluser_avg_coin = $data["avg_coin"];
		
		// 7일 이내, $39이상 구매자 코인 합계, 평균
		$sql = "SELECT IFNULL(ROUND(SUM(coin)), 0) AS total_coin, IFNULL(ROUND(AVG(coin)), 0) AS avg_coin, COUNT(t2.useridx) AS total_user, IFNULL(ROUND(SUM(coin)*0.3), 0) AS coin_30_percent ".
				"FROM ( ".
				"	SELECT useridx ".
				"	FROM ( ".
				"		SELECT useridx, facebookcredit ".
				"		FROM tbl_product_order ".
				"		WHERE useridx > $str_useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY) ".
				"		UNION ALL ".
				"		SELECT useridx, facebookcredit ".
				"		FROM tbl_product_order_mobile ".
				"		WHERE useridx > $str_useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY) ".
				") t1 ".
				"GROUP BY useridx ".
				"HAVING SUM(facebookcredit) >= 390 ".
				") t2 LEFT JOIN tbl_user t3 ON t2.useridx = t3.useridx";
		$data = $db_main->getarray($sql);
		
		$orderuser_total_coin = $data["total_coin"];
		$orderuser_avg_coin = $data["avg_coin"];
		$orderuser_user_count = $data["total_user"];
		$orderuser_coin_30_percent = $data["coin_30_percent"];
		
		// 7일 이내, $39이상 구매자 총 보유코인의 30% 이상 소지 유저 제외 코인 합계, 평균
		$sql = "SELECT IFNULL(ROUND(SUM(coin)), 0) AS total_coin, IFNULL(ROUND(AVG(coin)), 0) AS avg_coin ".
				"FROM ".
				"(	".
				"	SELECT t2.useridx, coin	".
				"	FROM (	". 
				"		SELECT useridx	". 
				"		FROM (	". 
				"			SELECT useridx, facebookcredit	". 
				"			FROM tbl_product_order 	".
				"			WHERE useridx > $str_useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY)	".
				"			UNION ALL ".
				"			SELECT useridx, facebookcredit  ".
				"			FROM tbl_product_order_mobile ".
				"			WHERE useridx > $str_useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY) ".
				"		) t1	". 
				"		GROUP BY useridx	". 
				"		HAVING SUM(facebookcredit) >= 390	". 
				"	) t2 LEFT JOIN tbl_user t3 ON t2.useridx = t3.useridx HAVING coin < $orderuser_coin_30_percent	". 
				") t4";
		$data_30_percent = $db_main->getarray($sql);
		
		$orderuser_30_percent_total_coin = $data_30_percent["total_coin"];
		$orderuser_30_percent_avg_coin = $data_30_percent["avg_coin"];

		// 7일 이내 활동 사용자 코인 합계, 평균
		$sql = "SELECT IFNULL(ROUND(SUM(coin)),0) AS total_coin,IFNULL(ROUND(AVG(coin)),0) AS avg_coin FROM tbl_user $tail $active_tail";
		$data = $db_main->getarray($sql);
		
		$activeuser_total_coin = $data["total_coin"];
		$activeuser_avg_coin = $data["avg_coin"];
		
		// 7일 이내 활동 사용자 & 7일내  $39미만 구매자 코인 합계, 평균
		$sql = "SELECT IFNULL(ROUND(SUM(coin)), 0) AS total_coin, COUNT(useridx) AS total_user ".
				"FROM tbl_user ".
				"WHERE useridx > $str_useridx AND logindate >= DATE_SUB(NOW(), INTERVAL 7 DAY) ";
		$data = $db_main->getarray($sql);
		
		$activeuser_notorder_total_coin = $data["total_coin"] - $orderuser_total_coin;
		
		$activeuser_notorder_count = $data["total_user"] - $orderuser_user_count;
		
		$activeuser_notorder_avg_coin = round($activeuser_notorder_count==0?0:($data["total_coin"] - $orderuser_total_coin)/$activeuser_notorder_count);
		
		$sql = "DELETE FROM user_coin_log WHERE today='$today';";
		$sql .= "INSERT INTO user_coin_log(today,type,subtype,coin) VALUES('$today','1','1',$alluser_total_coin);";
		$sql .= "INSERT INTO user_coin_log(today,type,subtype,coin) VALUES('$today','1','2',$alluser_avg_coin);";
		
		$sql .= "INSERT INTO user_coin_log(today,type,subtype,coin) VALUES('$today','2','1',$orderuser_total_coin);";
		$sql .= "INSERT INTO user_coin_log(today,type,subtype,coin) VALUES('$today','2','2',$orderuser_avg_coin);";
		
		$sql .= "INSERT INTO user_coin_log(today,type,subtype,coin) VALUES('$today','3','1',$activeuser_total_coin);";
		$sql .= "INSERT INTO user_coin_log(today,type,subtype,coin) VALUES('$today','3','2',$activeuser_avg_coin);";
		
		$sql .= "INSERT INTO user_coin_log(today,type,subtype,coin) VALUES('$today','4','1',$activeuser_notorder_total_coin);";
		$sql .= "INSERT INTO user_coin_log(today,type,subtype,coin) VALUES('$today','4','2',$activeuser_notorder_avg_coin);";
		
		$sql .= "INSERT INTO user_coin_log(today,type,subtype,coin) VALUES('$today','5','1',$orderuser_30_percent_total_coin);";
		$sql .= "INSERT INTO user_coin_log(today,type,subtype,coin) VALUES('$today','5','2',$orderuser_30_percent_avg_coin);";
		
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		// 이벤트 효과 분석 (신규가입, 2주 이탈 복귀)
		$sql = "SELECT eventidx,useridx,writedate,(SELECT eventcode FROM tbl_event WHERE eventidx=tbl_event_result.eventidx) AS eventcode FROM tbl_event_result WHERE isnew=-1 ORDER BY eventidx DESC LIMIT 50000";
		$list = $db_slave_main2->gettotallist($sql);
	
		for ($i=0; $i<sizeof($list); $i++)
		{
			$eventidx = (int)($list[$i]["eventidx"]);
			$eventcode = $list[$i]["eventcode"];
			$useridx = $list[$i]["useridx"];
			$writedate = substr($list[$i]["writedate"], 0, 10);
			$isnew = "0";
			$isreturn = "0";
				
			$sql = "SELECT isnew, isreturn FROM tbl_event_user WHERE eventcode='$eventcode' AND useridx=$useridx";
			$data = $db_main2->getarray($sql);
	
			if ($data["isnew"] != "")
			{
				$isnew = $data["isnew"];
				$isreturn = $data["isreturn"];
			}
				
			$sql = "UPDATE tbl_event_result SET isnew=$isnew, isreturn=$isreturn WHERE eventidx=$eventidx AND useridx=$useridx";
			$db_main2->execute($sql);
		}
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		// 이벤트 효과 분석 (구매)
		$sql = "SELECT eventidx,useridx,writedate FROM tbl_event_result WHERE ordercredit=-1 AND writedate<DATE_SUB(NOW(), INTERVAL 24 HOUR) ORDER BY eventidx DESC LIMIT 50000";
		$list = $db_slave_main2->gettotallist($sql);
	
		for ($i=0; $i<sizeof($list); $i++)
		{
			$eventidx = $list[$i]["eventidx"];
			$useridx = $list[$i]["useridx"];
			$writedate = $list[$i]["writedate"];
			$ordercredit = "0";
	
			$sql = "SELECT ".
					"(SELECT IFNULL(SUM(facebookcredit)/10,0) FROM tbl_product_order WHERE useridx='$useridx' AND status=1 AND writedate BETWEEN '$writedate' AND DATE_ADD('$writedate', INTERVAL 24 HOUR))".
					"	+ ".
					"(SELECT IFNULL(SUM(money),0) AS facebookcredit FROM tbl_product_order_mobile WHERE useridx='$useridx' AND STATUS=1 AND writedate BETWEEN '$writedate' AND DATE_ADD('$writedate', INTERVAL 24 HOUR))";
			$ordercredit = $db_main->getvalue($sql);
	
			$sql = "UPDATE tbl_event_result SET ordercredit='$ordercredit' WHERE eventidx=$eventidx AND useridx=$useridx";
			$db_main2->execute($sql);
		}
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
		
	try 
	{
		// push 이벤트 효과 분석 (신규가입, 1주 이탈 복귀)
		$sql = "SELECT eventidx,useridx,writedate FROM tbl_push_event_result WHERE isnew=-1 ORDER BY eventidx DESC LIMIT 50000";
		$list = $db_slave_main2->gettotallist($sql);
	
		for ($i=0; $i<sizeof($list); $i++)
		{
			$eventidx = (int)($list[$i]["eventidx"]);
			$useridx = $list[$i]["useridx"];
			$writedate = substr($list[$i]["writedate"], 0, 10);
			$isnew = "0";
			$isreturn = "0";
				
			$sql = "SELECT isnew, isreturn,isleave_2weeks FROM tbl_push_event_user WHERE eventcode='$eventidx' AND useridx=$useridx";
			$data = $db_main2->getarray($sql);
	
			if ($data["isnew"] != "")
			{
				$isnew = $data["isnew"];
				$isreturn = $data["isreturn"];
				$isleave_2weeks = $data["isleave_2weeks"];
			}
		
			if($isleave_2weeks == "")
				$isleave_2weeks = 0;
				
			$sql = "UPDATE tbl_push_event_result SET isnew=$isnew, isreturn=$isreturn, isleave_2weeks=$isleave_2weeks WHERE eventidx=$eventidx AND useridx=$useridx";
			$db_main2->execute($sql);
		}
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	
	try
	{
		// push 이벤트 효과 분석 (구매)
		$sql = "SELECT eventidx,useridx,writedate FROM tbl_push_event_result WHERE ordercredit=-1 AND writedate<DATE_SUB(NOW(), INTERVAL 24 HOUR) ORDER BY eventidx DESC LIMIT 50000";
		$list = $db_slave_main2->gettotallist($sql);
	
		for ($i=0; $i<sizeof($list); $i++)
		{
			$eventidx = $list[$i]["eventidx"];
			$useridx = $list[$i]["useridx"];
			$writedate = $list[$i]["writedate"];
			$ordercredit = "0";
	
			$sql = "SELECT ".
					"(SELECT IFNULL(SUM(facebookcredit)/10,0) FROM tbl_product_order WHERE useridx='$useridx' AND status=1 AND writedate BETWEEN '$writedate' AND DATE_ADD('$writedate', INTERVAL 24 HOUR))".
					"	+ ".
					"(SELECT IFNULL(SUM(money),0) AS facebookcredit FROM tbl_product_order_mobile WHERE useridx='$useridx' AND STATUS=1 AND writedate BETWEEN '$writedate' AND DATE_ADD('$writedate', INTERVAL 24 HOUR))";
			$ordercredit = $db_main->getvalue($sql);
	
			$sql = "UPDATE tbl_push_event_result SET ordercredit='$ordercredit' WHERE eventidx=$eventidx AND useridx=$useridx";
			$db_main2->execute($sql);
		}
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try 
	{
		if (date("H") == "00")
		{
			$today = date("Y-m-d", time() - 60 * 60);
			$tomorrow = date("Y-m-d", time() + 60 * 60);
		}
		else
		{
			$today = date("Y-m-d");
			$tomorrow = date("Y-m-d", time() + 24 * 60 * 60);
		}
		
		// Vip Level 통계	
		for($i = 0; $i < 11; $i++)
		{
			if ($i == 0)
				$tail_str = " AND EXISTS (SELECT * FROM tbl_product_order WHERE useridx = t1.useridx AND STATUS = 1)";
						
			$sql = "SELECT $i AS vip_level, COUNT(vip_level) AS cnt_vip_user, IFNULL(SUM(IF(logindate < DATE_SUB(NOW(), INTERVAL 2 WEEK), 1, 0)), 0) AS cnt_vip_2week ".
					"FROM tbl_user t1 LEFT JOIN tbl_user_detail t2 ".
					"ON t1.useridx = t2.useridx ".
					"WHERE t1.useridx > $std_useridx  AND vip_level = $i $tail_str";
			$vip_level_info = $db_main->getarray($sql);
			
			$vip_level = $vip_level_info["vip_level"];
			$cnt_vip_user = $vip_level_info["cnt_vip_user"];
			$cnt_vip_2week = $vip_level_info["cnt_vip_2week"];
			
			$sql = "INSERT INTO tbl_vip_level_stat_log(today, type, vip_level_".$i.") VALUES('$today',0,$cnt_vip_user) ".
					"ON DUPLICATE KEY UPDATE vip_level_".$i." = VALUES(vip_level_".$i.")";
			$db_analysis->execute($sql);
								
			$sql = "INSERT INTO tbl_vip_level_stat_log(today, type, vip_level_".$i.") VALUES('$today',1,$cnt_vip_2week) ".
					"ON DUPLICATE KEY UPDATE vip_level_".$i." = VALUES(vip_level_".$i.")";
			$db_analysis->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		//Insert tbl_user_unpay_retention_log
		if (date("H") == "00")
		{
			$today = date("Y-m-d", time() - 60 * 60);
			$tomorrow = date("Y-m-d", time() + 60 * 60);
			
			// 전체
			$sql = "SELECT DATEDIFF(NOW(), t1.createdate) AS day_after_install, ceil(AVG(coin)) AS avg_coin ".
					"FROM tbl_user t1 LEFT JOIN tbl_user_ext t2 ON t1.useridx=t2.useridx ".
					"WHERE t1.useridx > $std_useridx AND t1.createdate > DATE_SUB(NOW(), INTERVAL 90 DAY) ".
					"AND t2.logindate > date_sub(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK) ".
					"AND (NOT EXISTS (SELECT * FROM tbl_product_order WHERE useridx = t1.useridx) ".
					"OR NOT EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx = t1.useridx)) ".
					"AND t1.createdate < '$tomorrow 00:00:00' ".
					"GROUP BY day_after_install ORDER BY day_after_install";
			$user_unpay_retention_list = $db_main->gettotallist($sql);
				
			for($i=0; $i<sizeof($user_unpay_retention_list); $i++)
			{
				$day_after_install = $user_unpay_retention_list[$i]["day_after_install"];
				$avg_coin = $user_unpay_retention_list[$i]["avg_coin"];
			
				$sql = "INSERT INTO tbl_user_unpay_retention_log(today, day_after_install, all_avg_coin) ".
						"VALUES('$today', '$day_after_install','$avg_coin') ON DUPLICATE KEY UPDATE ".
						"all_avg_coin=VALUES(all_avg_coin);";				
				$db_analysis->execute($sql);
			}
	
			// Web
			$sql = "SELECT DATEDIFF(NOW(), t1.createdate) AS day_after_install, ceil(AVG(coin)) AS avg_coin ".
					"FROM tbl_user t1 LEFT JOIN tbl_user_ext t2 ON t1.useridx=t2.useridx ".
					"WHERE t1.useridx > $std_useridx AND t1.createdate > DATE_SUB(NOW(), INTERVAL 90 DAY) ".
					"AND t2.logindate > date_sub(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK) ".
					"AND NOT EXISTS (SELECT * FROM tbl_product_order WHERE useridx = t1.useridx) ".
					"AND t1.createdate < '$tomorrow 00:00:00' ".
					"AND platform = 0 ".
					"GROUP BY day_after_install ORDER BY day_after_install";
			$user_unpay_retention_list = $db_main->gettotallist($sql);
	
			for($i=0; $i<sizeof($user_unpay_retention_list); $i++)
			{
				$day_after_install = $user_unpay_retention_list[$i]["day_after_install"];
				$avg_coin = $user_unpay_retention_list[$i]["avg_coin"];
	
				$sql = "INSERT INTO tbl_user_unpay_retention_log(today, day_after_install, avg_coin) ".
						"VALUES('$today', '$day_after_install','$avg_coin') ON DUPLICATE KEY UPDATE ".
						"avg_coin=VALUES(avg_coin);";
				$db_analysis->execute($sql);
			}
			
			// iOS
			$sql = "SELECT DATEDIFF(NOW(), t1.createdate) AS day_after_install, ceil(AVG(coin)) AS avg_coin ".
					"FROM tbl_user t1 LEFT JOIN tbl_user_ext t2 ON t1.useridx=t2.useridx ".
					"WHERE t1.useridx > $std_useridx AND t1.createdate > DATE_SUB(NOW(), INTERVAL 90 DAY) ".
					"AND t2.logindate > date_sub(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK) ".
					"AND (NOT EXISTS (SELECT * FROM tbl_product_order WHERE useridx = t1.useridx) ".
					"OR NOT EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx = t1.useridx)) ".
					"AND t1.createdate < '$tomorrow 00:00:00' ".
					"AND platform = 1 ".
					"GROUP BY day_after_install ORDER BY day_after_install";
			$user_unpay_retention_list = $db_main->gettotallist($sql);
				
			for($i=0; $i<sizeof($user_unpay_retention_list); $i++)
			{
				$day_after_install = $user_unpay_retention_list[$i]["day_after_install"];
				$avg_coin = $user_unpay_retention_list[$i]["avg_coin"];
			
				$sql = "INSERT INTO tbl_user_unpay_retention_log(today, day_after_install, ios_avg_coin) ".
						"VALUES('$today', '$day_after_install','$avg_coin') ON DUPLICATE KEY UPDATE ".
						"ios_avg_coin=VALUES(ios_avg_coin);";
				$db_analysis->execute($sql);
			}
								
			// Android
			$sql = "SELECT DATEDIFF(NOW(), t1.createdate) AS day_after_install, ceil(AVG(coin)) AS avg_coin ".
					"FROM tbl_user t1 LEFT JOIN tbl_user_ext t2 ON t1.useridx=t2.useridx ".
					"WHERE t1.useridx > $std_useridx AND t1.createdate > DATE_SUB(NOW(), INTERVAL 90 DAY) ".
					"AND t2.logindate > date_sub(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK) ".
					"AND (NOT EXISTS (SELECT * FROM tbl_product_order WHERE useridx = t1.useridx) ".
					"OR NOT EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx = t1.useridx)) ".
					"AND t1.createdate < '$tomorrow 00:00:00' ".
					"AND platform = 2 ".
					"GROUP BY day_after_install ORDER BY day_after_install";
			$user_unpay_retention_list = $db_main->gettotallist($sql);
							
			for($i=0; $i<sizeof($user_unpay_retention_list); $i++)
			{
				$day_after_install = $user_unpay_retention_list[$i]["day_after_install"];
				$avg_coin = $user_unpay_retention_list[$i]["avg_coin"];
			
				$sql = "INSERT INTO tbl_user_unpay_retention_log(today, day_after_install, and_avg_coin) ".
						"VALUES('$today', '$day_after_install','$avg_coin') ON DUPLICATE KEY UPDATE ".
						"and_avg_coin=VALUES(and_avg_coin);";
				$db_analysis->execute($sql);
			}
							
			// Amazon
			$sql = "SELECT DATEDIFF(NOW(), t1.createdate) AS day_after_install, ceil(AVG(coin)) AS avg_coin ".
					"FROM tbl_user t1 LEFT JOIN tbl_user_ext t2 ON t1.useridx=t2.useridx ".
					"WHERE t1.useridx > $std_useridx AND t1.createdate > DATE_SUB(NOW(), INTERVAL 90 DAY) ".
					"AND t2.logindate > date_sub(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK) ".
					"AND (NOT EXISTS (SELECT * FROM tbl_product_order WHERE useridx = t1.useridx) ".
					"OR NOT EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx = t1.useridx)) ".
					"AND t1.createdate < '$tomorrow 00:00:00' ".
					"AND platform = 3 ".
					"GROUP BY day_after_install ORDER BY day_after_install";
			$user_unpay_retention_list = $db_main->gettotallist($sql);
							
			for($i=0; $i<sizeof($user_unpay_retention_list); $i++)
			{
				$day_after_install = $user_unpay_retention_list[$i]["day_after_install"];
				$avg_coin = $user_unpay_retention_list[$i]["avg_coin"];

				$sql = "INSERT INTO tbl_user_unpay_retention_log(today, day_after_install, ama_avg_coin) ".
						"VALUES('$today', '$day_after_install','$avg_coin') ON DUPLICATE KEY UPDATE ".
						"ama_avg_coin=VALUES(ama_avg_coin);";
				$db_analysis->execute($sql);
			}
			
			$sql = "SELECT '$today' AS today, platform, vip_level, IF(lastbuydate > DATE_SUB(NOW(), INTERVAL 4 WEEK), 1, 0) AS isrecentbuy, ".
					"	COUNT(couponidx) AS total_coupon, COUNT(DISTINCT t1.useridx) AS total_usercount, SUM(coupon_more) AS total_more, SUM(DATEDIFF(expiredate, NOW())) AS total_expireday ".
					"FROM ( ".
					"	SELECT couponidx, useridx, coupon_more, status, expiredate, writedate ".
					"	FROM tbl_coupon ".
					"	WHERE expiredate >= '$today 00:00:00' AND status = 0 AND category = 0 ".
					") t1 JOIN tbl_coupon_issue t2 ON t1.useridx = t2.useridx ".
					"GROUP BY platform, vip_level, isrecentbuy";
			 
			$stat_list = $db_slave_main2->gettotallist($sql);
			 
			for($i=0; $i<sizeof($stat_list); $i++)
			{
				$today = $stat_list[$i]["today"];
				$platform = $stat_list[$i]["platform"];
				$member_level = $stat_list[$i]["vip_level"];
				$isrecentbuy = $stat_list[$i]["isrecentbuy"];
				$total_coupon = $stat_list[$i]["total_coupon"];
				$total_usercount = $stat_list[$i]["total_usercount"];
				$total_more = $stat_list[$i]["total_more"];
				$total_expireday = $stat_list[$i]["total_expireday"];
				
				$sql = "INSERT INTO tbl_coupon_stat_daily(today, platform, member_level, isrecentbuy, total_coupon, total_usercount, total_more, total_expireday) ".
	    				"VALUES('$today', $platform, $member_level, $isrecentbuy, $total_coupon, $total_usercount, $total_more, $total_expireday) ".
				    				"ON DUPLICATE KEY UPDATE total_coupon=VALUES(total_coupon), total_usercount=VALUES(total_usercount), total_more=VALUES(total_more), total_expireday=VALUES(total_expireday)";
	    		$db_other->execute($sql);
    		}
   
    		//사용한 일반 쿠폰(category = 0)
    		$sql = "SELECT '$today' AS today, platform, vip_level, IF(lastbuydate > DATE_SUB(NOW(), INTERVAL 4 WEEK), 1, 0) AS isrecentbuy, ".
			    	"	COUNT(couponidx) AS use_coupon, COUNT(DISTINCT t1.useridx) AS use_usercount, SUM(coupon_more) AS use_more ".
	    			"FROM ( ".
	    			"	SELECT couponidx, useridx, coupon_more, status, expiredate, writedate ".
	    			"	FROM tbl_coupon ".
	    			"	WHERE expiredate >= '$today 00:00:00' AND status = 1 AND '$today 00:00:00' <= usedate AND usedate < '$tomorrow 00:00:00' AND category = 0  ".
	    			") t1 JOIN tbl_coupon_issue t2 ON t1.useridx = t2.useridx ".
	    			"GROUP BY platform, vip_level, isrecentbuy";
   
    		$use_stat_list = $db_slave_main2->gettotallist($sql);
   
			for($i=0; $i<sizeof($use_stat_list); $i++)
			{
				$today = $use_stat_list[$i]["today"];
				$platform = $use_stat_list[$i]["platform"];
				$member_level = $use_stat_list[$i]["vip_level"];
				$isrecentbuy = $use_stat_list[$i]["isrecentbuy"];
				$use_coupon = $use_stat_list[$i]["use_coupon"];
				$use_usercount = $use_stat_list[$i]["use_usercount"];
				$use_more = $use_stat_list[$i]["use_more"];
			    					 
				$sql = "INSERT INTO tbl_coupon_stat_daily(today, platform, member_level, isrecentbuy, use_coupon, use_usercount, use_more) ".
    					"VALUES('$today', $platform, $member_level, $isrecentbuy, $use_coupon, $use_usercount, $use_more) ".
			    		"ON DUPLICATE KEY UPDATE use_coupon=VALUES(use_coupon), use_usercount=VALUES(use_usercount), use_more=VALUES(use_more)";
    			$db_other->execute($sql);
			}
			    	
			//발급된 일반 쿠폰(category = 0)
			$sql = "SELECT '$today' AS today, platform, vip_level, isrecentbuy, ".
					"	COUNT(couponidx) AS issue_coupon, COUNT(DISTINCT t1.useridx) AS issue_usercount, SUM(coupon_more) AS issue_more ".
    				"FROM ( ".
    				"	SELECT useridx, platform, vip_level, IF(lastbuydate > DATE_SUB(NOW(), INTERVAL 4 WEEK), 1, 0) AS isrecentbuy ".
    				"	FROM tbl_coupon_issue ".
			    	"	WHERE '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' ".
			    	") t1 JOIN tbl_coupon t2 ON t1.useridx = t2.useridx AND '$today 00:00:00' <= t2.writedate AND t2.writedate < '$tomorrow 00:00:00' AND category = 0 ".
    				"GROUP BY platform, vip_level, isrecentbuy";
   
    		$issue_stat_list = $db_slave_main2->gettotallist($sql);
			    	 
			for($i=0; $i<sizeof($issue_stat_list); $i++)
			{
			   	$today = $issue_stat_list[$i]["today"];
			   	$platform = $issue_stat_list[$i]["platform"];
			   	$member_level = $issue_stat_list[$i]["vip_level"];
			   	$isrecentbuy = $issue_stat_list[$i]["isrecentbuy"];
			   	$issue_coupon = $issue_stat_list[$i]["issue_coupon"];
			   	$issue_usercount = $issue_stat_list[$i]["issue_usercount"];
			   	$issue_more = $issue_stat_list[$i]["issue_more"];
			    	 
			   	$sql = "INSERT INTO tbl_coupon_stat_daily(today, platform, member_level, isrecentbuy, issue_coupon, issue_usercount, issue_more) ".
    					"VALUES('$today', $platform, $member_level, $isrecentbuy, $issue_coupon, $issue_usercount, $issue_more) ".
			    		"ON DUPLICATE KEY UPDATE issue_coupon=VALUES(issue_coupon), issue_usercount=VALUES(issue_usercount), issue_more=VALUES(issue_more)";
    			$db_other->execute($sql);
			}
			    	 
			//발급된 일반 쿠폰 중 사용가능한 쿠폰(category = 0)
			$sql = "SELECT '$today' AS today, platform, vip_level, IF(lastbuydate > DATE_SUB(NOW(), INTERVAL 4 WEEK), 1, 0) AS isrecentbuy, ".
			 		"	COUNT(couponidx) AS expire_coupon, COUNT(DISTINCT t1.useridx) AS expire_usercount, SUM(coupon_more) AS expire_more ".
    				"FROM ( ".
    				"	SELECT couponidx, useridx, coupon_more, STATUS, expiredate, writedate ".
    				"	FROM tbl_coupon ".
			    	"	WHERE status = 0 AND '$today 00:00:00' <= expiredate AND expiredate < '$tomorrow 00:00:00' AND category = 0  ".
			    	") t1 JOIN tbl_coupon_issue t2 ON t1.useridx = t2.useridx ".
    				"GROUP BY platform, vip_level, isrecentbuy";
   
    		$expire_stat_list = $db_slave_main2->gettotallist($sql);
			    	 
			for($i=0; $i<sizeof($expire_stat_list); $i++)
			{
				$today = $expire_stat_list[$i]["today"];
				$platform = $expire_stat_list[$i]["platform"];
				$member_level = $expire_stat_list[$i]["vip_level"];
				$isrecentbuy = $expire_stat_list[$i]["isrecentbuy"];
				$expire_coupon = $expire_stat_list[$i]["expire_coupon"];
				$expire_usercount = $expire_stat_list[$i]["expire_usercount"];
				$expire_more = $expire_stat_list[$i]["expire_more"];
			    	 
				$sql = "INSERT INTO tbl_coupon_stat_daily(today, platform, member_level, isrecentbuy, expire_coupon, expire_usercount, expire_more) ".
    					"VALUES('$today', $platform, $member_level, $isrecentbuy, $expire_coupon, $expire_usercount, $expire_more) ".
			    		"ON DUPLICATE KEY UPDATE expire_coupon=VALUES(expire_coupon), expire_usercount=VALUES(expire_usercount), expire_more=VALUES(expire_more)";
    			$db_other->execute($sql);
    		}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		if (date("H") == "00")
		{
			// tbl_user_slot_share_info 삭제
			$sql = "TRUNCATE TABLE `tbl_user_slot_share_info`;";
			$db_main2->execute($sql);
		}
	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}

	try
	{
		if (date("H") == "00")
		{
			$today = date("Y-m-d", time() - 60 * 60);
			$tomorrow = date("Y-m-d", time() + 60 * 60);
		}
		else
		{
			$today = date("Y-m-d");
			$tomorrow = date("Y-m-d", time() + 24 * 60 * 60);
		}
		
		$start_date = date('Y-m', strtotime($today))."-01";
		$end_date = date('Y-m', strtotime($today.' + 1 month'))."-01";
		
		//month 기준 결제 통계
		//web
		$sql = "SELECT DATE_FORMAT(createdate, '%Y-%m') AS join_date, ROUND(SUM(facebookcredit)/10, 1) AS total_credit, COUNT(*) AS pay_count, COUNT(DISTINCT t1.useridx) AS user_count ".
				"FROM ( ".
				"	SELECT useridx, facebookcredit, writedate ".
				"	FROM tbl_product_order ".
				"	WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND '$start_date 00:00:00' <= writedate AND writedate < '$end_date 00:00:00' ".
				") t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx	".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		$list = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($list); $i++)
		{
			$join_date = $list[$i]["join_date"];
			$pay_date = date('Y-m', strtotime($start_date));
			$total_credit = $list[$i]["total_credit"];
			$pay_count = $list[$i]["pay_count"];
			$user_count = $list[$i]["user_count"];
		
			$sql = "INSERT INTO tbl_user_month_purchase_stats(join_date, pay_date, total_credit, pay_count, user_count) VALUES('$join_date', '$pay_date', '$total_credit', '$pay_count', '$user_count') ".
					"ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), pay_count = VALUES(pay_count), user_count = VALUES(user_count);";
			$db_other->execute($sql);
		}
		
		//Ios
		$sql = "SELECT DATE_FORMAT(createdate, '%Y-%m') AS join_date, ROUND(SUM(money), 2) AS total_credit, COUNT(*) AS pay_count, COUNT(DISTINCT t1.useridx) AS user_count ".
				"FROM ( ".
				"	SELECT useridx, money, writedate ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND os_type=1 AND '$start_date 00:00:00' <= writedate AND writedate < '$end_date 00:00:00' ".
				") t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx	".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		$list = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($list); $i++)
		{
			$join_date = $list[$i]["join_date"];
			$pay_date = date('Y-m', strtotime($start_date));
			$total_credit = $list[$i]["total_credit"];
			$pay_count = $list[$i]["pay_count"];
			$user_count = $list[$i]["user_count"];
		
			$sql = "INSERT INTO tbl_user_month_purchase_stats_ios(join_date, pay_date, total_credit, pay_count, user_count) VALUES('$join_date', '$pay_date', '$total_credit', '$pay_count', '$user_count') ".
					"ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), pay_count = VALUES(pay_count), user_count = VALUES(user_count);";
			$db_other->execute($sql);
		}
			
		//Android
		$sql = "SELECT DATE_FORMAT(createdate, '%Y-%m') AS join_date, ROUND(SUM(money), 2) AS total_credit, COUNT(*) AS pay_count, COUNT(DISTINCT t1.useridx) AS user_count ".
				"FROM ( ".
				"	SELECT useridx, money, writedate ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND os_type=2 AND '$start_date 00:00:00' <= writedate AND writedate < '$end_date 00:00:00' ".
				") t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx	".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		$list = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($list); $i++)
		{
			$join_date = $list[$i]["join_date"];
			$pay_date = date('Y-m', strtotime($start_date));
			$total_credit = $list[$i]["total_credit"];
			$pay_count = $list[$i]["pay_count"];
			$user_count = $list[$i]["user_count"];
		
			$sql = "INSERT INTO tbl_user_month_purchase_stats_android(join_date, pay_date, total_credit, pay_count, user_count) VALUES('$join_date', '$pay_date', '$total_credit', '$pay_count', '$user_count') ".
					"ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), pay_count = VALUES(pay_count), user_count = VALUES(user_count);";
			$db_other->execute($sql);
		}
		
		//Amazon
		$sql = "SELECT DATE_FORMAT(createdate, '%Y-%m') AS join_date, ROUND(SUM(money), 2) AS total_credit, COUNT(*) AS pay_count, COUNT(DISTINCT t1.useridx) AS user_count ".
				"FROM ( ".
				"	SELECT useridx, money, writedate ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND os_type=3 AND '$start_date 00:00:00' <= writedate AND writedate < '$end_date 00:00:00' ".
				") t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx	".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		$list = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($list); $i++)
		{
			$join_date = $list[$i]["join_date"];
			$pay_date = date('Y-m', strtotime($start_date));
			$total_credit = $list[$i]["total_credit"];
			$pay_count = $list[$i]["pay_count"];
			$user_count = $list[$i]["user_count"];
		
			$sql = "INSERT INTO tbl_user_month_purchase_stats_amazon(join_date, pay_date, total_credit, pay_count, user_count) VALUES('$join_date', '$pay_date', '$total_credit', '$pay_count', '$user_count') ".
					"ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), pay_count = VALUES(pay_count), user_count = VALUES(user_count);";
			$db_other->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	// 일별 구매 상품 통계 - Web
	try
	{
		// 카테고리별
		$sql = "SELECT category, SUM(total_count) AS total_count, SUM(total_facebookcredit) AS total_facebookcredit, SUM(total_coin) AS total_coin ".
				"FROM ".
				"( ".				
				"	SELECT IFNULL(IF(couponidx > 0, 100, category), -1) AS category, COUNT(orderidx) AS total_count, SUM(t1.facebookcredit) AS total_facebookcredit, IFNULL(SUM(coin), 0) AS total_coin ".
				"	FROM ".
				"	( ".
				"		SELECT productidx, orderidx, facebookcredit, coin, couponidx ".
				"		FROM tbl_product_order ".
				"		WHERE useridx > $std_useridx AND status IN (1, 3) AND writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' ". 
				"	) t1 LEFT JOIN tbl_product t2 ON t1.productidx = t2.productidx ".
				"	GROUP BY category, couponidx ".
				") t3 GROUP BY category";
	
		$category_list = $db_main->gettotallist($sql);

		for($i=0; $i<sizeof($category_list); $i++)
		{
			$order_category = $category_list[$i]["category"];
			$order_count = $category_list[$i]["total_count"];
			$order_facebookcredit = $category_list[$i]["total_facebookcredit"];
			$order_coin = $category_list[$i]["total_coin"];
					
			$sql = "INSERT INTO tbl_product_stat_daily(writedate, type, category, productidx, coupon_type, total_count, use_coupon, total_facebookcredit, total_coin) ".
					"VALUES('$today', 0, $order_category, 0, 0, $order_count, 0, $order_facebookcredit, $order_coin) ".
					"ON DUPLICATE KEY UPDATE total_count = VALUES(total_count), total_facebookcredit = VALUES(total_facebookcredit), total_coin = VALUES(total_coin)";
							
				$db_analysis->execute($sql);
		}

		// 상품별
		$sql = "SELECT t1.productidx, COUNT(orderidx) AS total_count, SUM(coupon) AS use_coupon, SUM(t1.facebookcredit) AS total_facebookcredit, IFNULL(SUM(coin), 0) AS total_coin ".
				"FROM ( ".
				"	SELECT productidx, orderidx, facebookcredit, coin, IF(couponidx > 0, 1, 0) AS coupon ".
				"	FROM tbl_product_order ".
				"	WHERE useridx > $std_useridx AND status IN (1, 3) AND writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' ".
				") t1 LEFT JOIN tbl_product t2 ON t1.productidx = t2.productidx ".
				"GROUP BY productidx, coupon";
		
		$product_list = $db_main->gettotallist($sql);

		for($i=0; $i<sizeof($product_list); $i++)
		{
			$order_product = $product_list[$i]["productidx"];
			$order_count = $product_list[$i]["total_count"];
			$order_facebookcredit = $product_list[$i]["total_facebookcredit"];
			$order_use_coupon = $product_list[$i]["use_coupon"];
			$order_coin = $product_list[$i]["total_coin"];
			
			if($order_use_coupon > 0)
				$coupon_type = 1;
			else
				$coupon_type = 0;
		
			$sql = "INSERT INTO tbl_product_stat_daily(writedate, type, category, productidx, coupon_type, total_count, use_coupon, total_facebookcredit, total_coin) ".
					"VALUES('$today', 1, 0, $order_product, $coupon_type, $order_count, $order_use_coupon, $order_facebookcredit, $order_coin) ".
					"ON DUPLICATE KEY UPDATE total_count = VALUES(total_count), use_coupon = VALUES(use_coupon), total_facebookcredit = VALUES(total_facebookcredit), total_coin = VALUES(total_coin)";
								
			$db_analysis->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	// 일별 구매 상품 통계 - Mobile
	try
	{
		// 카테고리별
		$sql = "SELECT category, SUM(total_count) AS total_count, ROUND(IFNULL(SUM(total_money), 0), 2) AS total_money, SUM(total_coin) AS total_coin ".
				"FROM ".
				"( ".
				"	SELECT IFNULL(IF(couponidx > 0, 100, category), -1) AS category, COUNT(orderidx) AS total_count, SUM(t1.money*10) AS total_money, IFNULL(SUM(coin), 0) AS total_coin ".
				"	FROM ".
				"	( ".
				"		SELECT productidx, orderidx, money, coin, couponidx ".
				"		FROM tbl_product_order_mobile ".
				"		WHERE useridx > $std_useridx AND status = 1 AND writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' ".
				"	) t1 LEFT JOIN tbl_product_mobile t2 ON t1.productidx = t2.productidx ".
				"	GROUP BY category, couponidx ".
				") t3 GROUP BY category";
	
		$category_list = $db_main->gettotallist($sql);
	
		for($i=0; $i<sizeof($category_list); $i++)
		{
			$order_category = $category_list[$i]["category"];
			$order_count = $category_list[$i]["total_count"];
			$order_money = $category_list[$i]["total_money"];
			$order_coin = $category_list[$i]["total_coin"];
					
			$sql = "INSERT INTO tbl_product_stat_mobile_daily(writedate, type, category, productidx, coupon_type, total_count, use_coupon, total_money, total_coin) ".
					"VALUES('$today', 0, $order_category, 0, 0, $order_count, 0, '$order_money', $order_coin) ".
					"ON DUPLICATE KEY UPDATE total_count = VALUES(total_count), total_money = VALUES(total_money), total_coin = VALUES(total_coin)";
				
			$db_analysis->execute($sql);
		}
	
		// 상품별
		$sql = "SELECT t1.productidx, COUNT(orderidx) AS total_count, SUM(coupon) AS use_coupon, SUM(t1.money*10) AS total_money, IFNULL(SUM(coin), 0) AS total_coin ".
				"FROM ( ".
				"	SELECT productidx, orderidx, money, coin, IF(couponidx > 0, 1, 0) AS coupon ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE useridx > $std_useridx AND status =  1 AND writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' ".
				") t1 LEFT JOIN tbl_product_mobile t2 ON t1.productidx = t2.productidx ".
				"GROUP BY productidx, coupon";
	
		$product_list = $db_main->gettotallist($sql);
	
		for($i=0; $i<sizeof($product_list); $i++)
		{
			$order_product = $product_list[$i]["productidx"];
			$order_count = $product_list[$i]["total_count"];
			$order_money = $product_list[$i]["total_money"];
			$order_use_coupon = $product_list[$i]["use_coupon"];
			$order_coin = $product_list[$i]["total_coin"];
		
			if($order_use_coupon > 0)
				$coupon_type = 1;
			else
				$coupon_type = 0;

			$sql = "INSERT INTO tbl_product_stat_mobile_daily(writedate, type, category, productidx, coupon_type, total_count, use_coupon, total_money, total_coin) ".
					"VALUES('$today', 1, 0, $order_product, $coupon_type, $order_count, $order_use_coupon, $order_money, $order_coin) ".
					"ON DUPLICATE KEY UPDATE total_count = VALUES(total_count), use_coupon = VALUES(use_coupon), total_money = VALUES(total_money), total_coin = VALUES(total_coin)";

			$db_analysis->execute($sql);
		}
		
		// 상품 타입별
		$SQL = " SELECT * ".
	   	    " FROM ( ".
	   	    "        SELECT t1.os_type AS os_type, t1.productidx, t1.product_type, FORMAT(t2.money, 2) AS productname, imageurl, paycount, t1.money, t1.coin ".
	   	    "        FROM ( ".
	   	    "                SELECT os_type, productidx, COUNT(productidx) AS paycount, ROUND(IFNULL(SUM(money),0)) AS money, IF(special_discount <> 0, 1, 0) AS is_discount,product_type, SUM(coin) AS coin ,coupon_type ".
	   	    "                FROM tbl_product_order_mobile ".
	   	    "                WHERE STATUS=1 AND useridx>$std_useridx AND writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' ".
	   	    "                GROUP BY productidx,product_type, is_discount, os_type ".
	   	    "        ) t1 LEFT JOIN tbl_product_mobile t2 ON t1.productidx = t2.productidx ".
	   	    "        WHERE paycount > 0 ".
	   	    " ) t1 ".
	   	    " ORDER BY money DESC";
	    
		    $product_list = $db_main->gettotallist($SQL);
	    
		    FOR($i=0; $i<sizeof($product_list); $i++)
		    {
		        $os_type = $product_list[$i]["os_type"];
		        $product_type = $product_list[$i]["product_type"];
		        $productname = $product_list[$i]["productname"];
			$productidx = $product_list[$i]["productidx"];
		        $imageurl = $product_list[$i]["imageurl"];
		        $paycount = $product_list[$i]["paycount"];
		        $money = $product_list[$i]["money"];
		        $coin = $product_list[$i]["coin"];
	        
		        $SQL = "INSERT INTO tbl_product_stat_mobile_daily_new(writedate, os_type, productidx, product_type, productname, imgurl, pay_count, money, coin) ".
		   	        "VALUES('$today', $os_type, $productidx, $product_type, '$productname', '$imageurl', $paycount, $money, $coin) ".
		   	        "ON DUPLICATE KEY UPDATE pay_count = VALUES(pay_count), money = VALUES(money), coin = VALUES(coin)";
	        
		        $db_analysis->EXECUTE($SQL);
		    }
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		$sql = "SELECT serverip, servername, MAX(diskuse) AS diskuse, MAX(cpu) AS cpu, MIN(ROUND((memfree/memtotal*10000)/100,2)) AS mem_rate, MAX(established) AS conn, MAX(timewait) AS timewait, MAX(closewait) AS closewait, MAX(sync_recv) AS sync_recv ".
				"FROM tbl_server_status_log  ".
				"WHERE writedate >= '$today 00:00:00' AND writedate < '$tomorrow 00:00:00' GROUP BY serverip;";
	
		$server_status_list = $db_analysis->gettotallist($sql);
	
		for($i=0; $i<sizeof($server_status_list); $i++)
		{
			$serverip = $server_status_list[$i]["serverip"];
			$servername = $server_status_list[$i]["servername"];
			$diskuse = $server_status_list[$i]["diskuse"];
			$cpu = $server_status_list[$i]["cpu"];
			$mem_rate = $server_status_list[$i]["mem_rate"];
			$established = $server_status_list[$i]["conn"];
			$timewait = $server_status_list[$i]["timewait"];
			$closewait = $server_status_list[$i]["closewait"];
			$sync_recv = $server_status_list[$i]["sync_recv"];
					
			$sql = "INSERT INTO tbl_server_status_log_daily(writedate, serverip, servername, diskuse, cpu, mem_rate, established, timewait, closewait, sync_recv) ".
					"VALUES('$today', '$serverip','$servername','$diskuse','$cpu','$mem_rate','$established','$timewait','$closewait','$sync_recv') ON DUPLICATE KEY UPDATE ".
					"diskuse=VALUES(diskuse), cpu=VALUES(cpu), mem_rate=VALUES(mem_rate), established=VALUES(established), timewait=VALUES(timewait), closewait=VALUES(closewait), sync_recv=VALUES(sync_recv);";
					
			$db_analysis->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		$online_today = date("Y-m-d", time() - 60 * 60);
		
		//일반
		$sql = "SELECT gameslottype, 0 AS mode, SUM(totalcount) AS sum_totalcount ".
				"FROM ".
				"(	".
				"	SELECT gameslottype, objectidx, SUM(totalcount) AS totalcount ".
				"	FROM user_online_game_log ".
				"	WHERE objectidx < 1000000 AND writedate BETWEEN '$online_today 00:00:00' AND '$online_today 23:59:59' ".
				"	GROUP BY gameslottype, objectidx ".
				") t1 GROUP BY gameslottype";
		$web_normal_data_list = $db_analysis->gettotallist($sql);
	
		for ($i=0; $i<sizeof($web_normal_data_list); $i++)
		{
			$gameslottype = $web_normal_data_list[$i]["gameslottype"];
			$mode = $web_normal_data_list[$i]["mode"];
			$sum_totalcount = $web_normal_data_list[$i]["sum_totalcount"];
	
			$sql = "INSERT INTO user_online_game_log_daily(today, mode, gameslottype, totalcount) VALUES ('$online_today', $mode, $gameslottype, $sum_totalcount)  ON DUPLICATE KEY UPDATE totalcount=VALUES(totalcount);";
			$db_analysis->execute($sql);
		}
	
		//하이롤러
		$sql = "SELECT gameslottype, 1 AS mode, SUM(totalcount) AS sum_totalcount ".
				"FROM ".
				"(	".
				"	SELECT gameslottype, objectidx, SUM(totalcount) AS totalcount ".
				"	FROM user_online_game_log ".
				"	WHERE objectidx >= 1000000 AND writedate BETWEEN '$online_today 00:00:00' AND '$online_today 23:59:59' ".
				"	GROUP BY gameslottype, objectidx ".
				") t1 GROUP BY gameslottype";
		$web_highroller_data_list = $db_analysis->gettotallist($sql);
	
		for ($i=0; $i<sizeof($web_highroller_data_list); $i++)
		{
			$gameslottype = $web_highroller_data_list[$i]["gameslottype"];
			$mode = $web_highroller_data_list[$i]["mode"];
			$sum_totalcount = $web_highroller_data_list[$i]["sum_totalcount"];
				
			$sql = "INSERT INTO user_online_game_log_daily(today, mode, gameslottype, totalcount) VALUES ('$online_today', $mode, $gameslottype, $sum_totalcount)  ON DUPLICATE KEY UPDATE totalcount=VALUES(totalcount);";
			$db_analysis->execute($sql);
		}
		
		//mobile
		$sql = "SELECT gameslottype, 0 AS mode, SUM(totalcount) AS sum_totalcount, os_type ".
				"FROM ".
				"(	".
				"	SELECT gameslottype, objectidx, SUM(totalcount) AS totalcount, os_type ".
				"	FROM user_online_game_mobile_log ".
				"	WHERE objectidx < 1000000 AND writedate BETWEEN '$online_today 00:00:00' AND '$online_today 23:59:59' ".
				"	GROUP BY gameslottype, objectidx, os_type ".
				") t1 GROUP BY gameslottype, os_type";
		$mobile_normal_data_list = $db_analysis->gettotallist($sql);
		
		for ($i=0; $i<sizeof($mobile_normal_data_list); $i++)
		{
			$gameslottype = $mobile_normal_data_list[$i]["gameslottype"];
			$mode = $mobile_normal_data_list[$i]["mode"];
			$sum_totalcount = $mobile_normal_data_list[$i]["sum_totalcount"];
			$os_type = $mobile_normal_data_list[$i]["os_type"];
		
			$sql = "INSERT INTO user_online_game_mobile_log_daily(today, mode, os_type, gameslottype, totalcount) VALUES ('$online_today', $mode, $os_type,  $gameslottype, $sum_totalcount)  ".
					"ON DUPLICATE KEY UPDATE totalcount=VALUES(totalcount);";
			$db_analysis->execute($sql);
		}
		
		$sql = "SELECT gameslottype, 1 AS mode, SUM(totalcount) AS sum_totalcount, os_type ".
				"FROM ".
				"(	".
				"	SELECT gameslottype, objectidx, SUM(totalcount) AS totalcount, os_type ".
				"	FROM user_online_game_mobile_log ".
				"	WHERE objectidx >= 1000000 AND writedate BETWEEN '$online_today 00:00:00' AND '$online_today 23:59:59' ".
				"	GROUP BY gameslottype, objectidx, os_type ".
				") t1 GROUP BY gameslottype, os_type";
		$mobile_highroller_data_list = $db_analysis->gettotallist($sql);
		
		for ($i=0; $i<sizeof($mobile_highroller_data_list); $i++)
		{
			$gameslottype = $mobile_highroller_data_list[$i]["gameslottype"];
			$mode = $mobile_highroller_data_list[$i]["mode"];
			$sum_totalcount = $mobile_highroller_data_list[$i]["sum_totalcount"];
			$os_type = $mobile_highroller_data_list[$i]["os_type"];
		
			$sql = "INSERT INTO user_online_game_mobile_log_daily(today, mode, os_type, gameslottype, totalcount) VALUES ('$online_today',  $mode, $os_type,  $gameslottype, $sum_totalcount)  ".
					"ON DUPLICATE KEY UPDATE totalcount=VALUES(totalcount);";
			$db_analysis->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		if (date("H") == "00")
		{
			$retention_today = date("Y-m-d", time() - 60 * 60);
				
			for($i = 0; $i < 10; $i++)
			{
				$sql = "SELECT today, useridx FROM tbl_user_30day_retention_log_$i WHERE today = '$retention_today'";
				$pay_retention_stat_list = $db_slave_livestats->gettotallist($sql);
				
				for($j=0; $j<sizeof($pay_retention_stat_list); $j++)
				{
					$today = $pay_retention_stat_list[$j]["today"];
					$useridx = $pay_retention_stat_list[$j]["useridx"];
					
					$pay_sql = " SELECT SUM(sum_credit) ". 
                               " FROM ( ".
                               " 	SELECT IFNULL(SUM(facebookcredit/10), 0) AS sum_credit FROM tbl_product_order ".
                               " 	WHERE STATUS = 1 AND useridx = $useridx AND '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59' ".
                               " 	UNION ALL ".
                               " 	SELECT IFNULL(SUM(facebookcredit/10), 0) AS sum_credit FROM tbl_product_order_mobile ".
                               " 	WHERE STATUS = 1 AND useridx = $useridx AND '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59' ".
                               " ) t1";
					$sum_credit = $db_main->getvalue($pay_sql);
					
					$update_retention_sql = "UPDATE tbl_user_30day_retention_log_$i SET money = '$sum_credit' WHERE useridx = $useridx AND today = '$today'";
					$db_livestats->execute($update_retention_sql);
				}
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";		
	}
		
	try 
	{
		if (date("H") == "00")
		{
			
			$std_28day_today = date("Y-m-d", time() - 60 * 60);
			$std_28day_tomorrow = date("Y-m-d", time());

			$sql = "SELECT ".
					"	COUNT(DISTINCT t1.useridx) AS cnt_user, IFNULL(SUM(facebookcredit)/10,0) AS sum_money, COUNT(DISTINCT t2.useridx) AS cnt_pay ".
					"	FROM ". 
					"	( 	".
					"		SELECT useridx ". 
					"		FROM tbl_user ". 
					"		WHERE useridx > $std_useridx AND createdate < DATE_SUB('$std_28day_tomorrow 00:00:00', INTERVAL 28 DAY) ". 
					"		AND '$std_28day_today 00:00:00' <= logindate AND logindate < '$std_28day_tomorrow 00:00:00' ".
					"	) t1 ". 
					"	LEFT JOIN ".
					"	( ".
					"		SELECT useridx, facebookcredit ". 
					"		FROM tbl_product_order ". 
					"		WHERE STATUS IN (1, 3) AND '$std_28day_today 00:00:00' <= writedate AND writedate < '$std_28day_tomorrow 00:00:00' ".
					"		UNION ALL ".
					"		SELECT useridx, facebookcredit ".
					"		FROM tbl_product_order_mobile ".
					"		WHERE STATUS = 1 AND '$std_28day_today 00:00:00' <= writedate AND writedate < '$std_28day_tomorrow 00:00:00' ".
					"	) t2 ON t1.useridx = t2.useridx";	
			$std_28day_data = $db_main->getarray($sql);
			
			$std_28day_activecount = $std_28day_data['cnt_user'];
			$std_28day_money = $std_28day_data['sum_money'];
			$std_28day_purchasecount = $std_28day_data['cnt_pay'];
			
			$insert_sql = "INSERT INTO tbl_user_std_28day_daily(today, activecount, money, purchasecount) VALUES('$std_28day_today', '$std_28day_activecount', '$std_28day_money', '$std_28day_purchasecount');";
			$db_analysis->execute($insert_sql);						
		}		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		if (date("H") == "00")
		{
			$std_addme_today = date("Y-m-d", time() - 60 * 60);
			
			$sql = "SELECT COUNT(*) FROM `tbl_user_detail` WHERE useridx > $std_useridx AND addme = 1;";
			$cnt_addme = $db_main->getvalue($sql);
			
			$insert_sql = "INSERT INTO tbl_cnt_addme(today, user_cnt) VALUES('$std_addme_today', '$cnt_addme');";
			$db_analysis->execute($insert_sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try 
	{
		for($i = 0; $i < 10; $i++)
		{
			$delete_reqeust_sql .= "DELETE FROM tbl_user_friend_request_$i WHERE writedate < DATE_SUB(NOW(), INTERVAL 7 DAY);";
		}
		
		if($delete_reqeust_sql != "")
			$db_main2->execute($delete_reqeust_sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		if (date("H") == "00")
		{
			$std_today = date("Y-m-d", time() - 60 * 60);
			
			$sql = "SELECT today,useridx,slottype,sum_moneyin,sum_moneyout,sub_money,winrate ".
					"FROM ".
					"( ".
					"	SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, useridx, slottype, SUM(moneyin) AS sum_moneyin, SUM(moneyout) AS sum_moneyout, ".
					"	SUM(moneyout) - SUM(moneyin) AS sub_money, SUM(moneyout)/SUM(moneyin) AS winrate FROM tbl_user_gamelog ".
					"	WHERE useridx > $std_useridx AND slottype != 0 AND mode IN (0,31) AND  DATE_FORMAT(writedate, '%Y-%m-%d') = '$std_today' ".
					"	GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), useridx, slottype  HAVING sub_money > 0 ORDER BY sub_money DESC, slottype ASC ".
					") t1 GROUP BY today, slottype";
			$user_list = $db_other->gettotallist($sql);
		
			for($i = 0; $i < sizeof($user_list); $i++)
			{
				$today = $user_list[$i]["today"];
				$useridx = $user_list[$i]["useridx"];
				$slottype = $user_list[$i]["slottype"];
				$sum_moneyin = $user_list[$i]["sum_moneyin"];
				$sum_moneyout = $user_list[$i]["sum_moneyout"];
				$sub_money = $user_list[$i]["sub_money"];
				$winrate = $user_list[$i]["winrate"];
								
				$insert_sql = "INSERT INTO tbl_slot_gain_high_coin_list(today, useridx, slottype, sum_moneyin, sum_moneyout, sub_money, winrate) ".
							"VALUES ('$today', $useridx, $slottype, $sum_moneyin, $sum_moneyout, $sub_money, '$winrate') ".
							"ON DUPLICATE KEY UPDATE sum_moneyin=VALUES(sum_moneyin), sum_moneyout=VALUES(sum_moneyout), sub_money=VALUES(sub_money), winrate=VALUES(winrate);";
				$db_analysis->execute($insert_sql);
			}
		}
	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	// 결제유저수 및 결제경험자 로그인 데이터
	try
	{
		if (date("H") == "00")
			$today = date("Y-m-d", time() - 60 * 60);
		else
			$today = date("Y-m-d");
		
		$payer_web_logincount = 0;
		$payer_ios_logincount = 0;
		$payer_android_logincount = 0;
		$payer_amazon_logincount = 0;
		
		for($i=0; $i<10; $i++)
		{
			$sql = "SELECT SUM(CASE WHEN platform = 0 THEN 1 ELSE 0 END) AS facebook, SUM(CASE WHEN platform = 1 THEN 1 ELSE 0 END) AS ios, SUM(CASE WHEN platform = 2 THEN 1 ELSE 0 END) AS android, SUM(CASE WHEN platform = 3 THEN 1 ELSE 0 END) AS amazon ".
					"FROM tbl_user_login_log_$i ".
					"WHERE today = '$today' AND useridx > 20000 AND payer = 1 ";
			
			$payer_active_info = $db_slave_livestats->getarray($sql);
			
			$payer_web_logincount += $payer_active_info["facebook"];
			$payer_ios_logincount += $payer_active_info["ios"];
			$payer_android_logincount += $payer_active_info["android"];
			$payer_amazon_logincount += $payer_active_info["amazon"];
		}
		
		if($payer_ios_logincount == "")
			$payer_ios_logincount = 0;
		
		if($payer_android_logincount == "")
			$payer_android_logincount = 0;
		
		if($payer_amazon_logincount == "")
			$payer_amazon_logincount = 0;		
	
		// 결제 유저수
		$sql = "SELECT COUNT(DISTINCT useridx)
				FROM tbl_product_order
				WHERE useridx > 20000 AND STATUS = 1 AND writedate >= '$today 00:00:00'";	
		$payer_web_usercount = $db_main->getvalue($sql);
		
		$sql = "SELECT os_type, COUNT(DISTINCT useridx) AS total_payer_count
				FROM tbl_product_order_mobile
				WHERE useridx > 20000 AND STATUS = 1 AND writedate >= '$today 00:00:00' GROUP BY os_type";
		$today_payer_list = $db_main->gettotallist($sql);
	
		for($i=0; $i<sizeof($today_payer_list); $i++)
		{
			$platform = $today_payer_list[$i]["os_type"];
			$pay_user_count = $today_payer_list[$i]["total_payer_count"];
			
			if($platform == 1)
			{
				$payer_ios_usercount =  $pay_user_count;
			}
			else if($platform == 2)
			{
				$payer_android_usercount =  $pay_user_count;
			}
			else if($platform == 3)
			{
				$payer_amazon_usercount =  $pay_user_count;
			}
		}
		
		if($payer_web_usercount == "")
			$payer_web_usercount = 0;
		
		if($payer_ios_usercount == "")
			$payer_ios_usercount = 0;
		
		if($payer_android_usercount == "")
			$payer_android_usercount = 0;
		
		if($payer_amazon_usercount == "")
			$payer_amazon_usercount = 0;
	
		$sql = "DELETE FROM tbl_user_payer_stat_daily WHERE today = '$today';";
		$sql .= "INSERT INTO tbl_user_payer_stat_daily VALUES('$today', $payer_web_logincount, $payer_ios_logincount, $payer_android_logincount, $payer_amazon_logincount, $payer_web_usercount, $payer_ios_usercount, $payer_android_usercount, $payer_amazon_usercount);";
		$db_other->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	// 게스트 유저 ARPU 데이터
	
	try
	{
		if (date("H") == "00")
		{
			$db_redshift = new CDatabase_Redshift();
			$today = date("Y-m-d");		
			$yesterday =  date("Y-m-d", strtotime($today."-1 day"));

			
			$sql = " select is_guest, platform, count(useridx) as logincount ".
					" from t5_user ".
					" where not exists (select * from t5_user_playstat_daily where useridx=t5_user.useridx) and logindate >= '$yesterday 00:00:00' and logindate < '$today 00:00:00' and platform > 0 ".
					" group by is_guest, platform ".
					" order by is_guest asc, platform asc" ;
			$mobile_logincount_list = $db_redshift->gettotallist($sql);
	
			$ios_logincount = ($mobile_logincount_list[0]['logincount']=="")? 0 : $mobile_logincount_list[0]['logincount'];
			$and_logincount = ($mobile_logincount_list[1]['logincount']=="")? 0 : $mobile_logincount_list[1]['logincount'];
			$ama_logincount = ($mobile_logincount_list[2]['logincount']=="")? 0 : $mobile_logincount_list[2]['logincount'];
	
			$guest_ios_logincount = ($mobile_logincount_list[3]['logincount']=="")? 0 : $mobile_logincount_list[3]['logincount'];
			$guest_and_logincount = ($mobile_logincount_list[4]['logincount']=="")? 0 : $mobile_logincount_list[4]['logincount'];
			$guest_ama_logincount = ($mobile_logincount_list[5]['logincount']=="")? 0 : $mobile_logincount_list[5]['logincount'];
				
				
			$sql = "select is_guest, platform, sum(money) as money, count(distinct t1.useridx) as payer_count ".
					" from ( ".
					"  select is_guest, platform, useridx ".
					"  from t5_user ".
					"  where not exists (select * from t5_user_playstat_daily where useridx=t5_user.useridx) and logindate >= '$yesterday 00:00:00' and platform > 0 ".
					" ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
					" where status = 1 and t1.useridx > 20000 and writedate >= '$yesterday 00:00:00' and writedate < '$today 00:00:00' ".
					" group by is_guest, platform ".
					" order by is_guest asc, platform asc; ";
	
			$mobile_money_list = $db_redshift->gettotallist($sql);
	
			$ios_money = ($mobile_money_list[0]['money']=="")? 0 : $mobile_money_list[0]['money'];
			$and_money = ($mobile_money_list[1]['money']=="")? 0 : $mobile_money_list[1]['money'];
			$ama_money = ($mobile_money_list[2]['money']=="")? 0 : $mobile_money_list[2]['money'];
	
			$ios_payercount = ($mobile_money_list[0]['payer_count']=="")? 0 : $mobile_money_list[0]['payer_count'];
			$and_payercount = ($mobile_money_list[1]['payer_count']=="")? 0 : $mobile_money_list[1]['payer_count'];
			$ama_payercount = ($mobile_money_list[2]['payer_count']=="")? 0 : $mobile_money_list[2]['payer_count'];
	
			$guest_ios_money = ($mobile_money_list[3]['money']=="")? 0 : $mobile_money_list[3]['money'];
			$guest_and_money = ($mobile_money_list[4]['money']=="")? 0 : $mobile_money_list[4]['money'];
			$guest_ama_money = ($mobile_money_list[5]['money']=="")? 0 : $mobile_money_list[5]['money'];
	
			$guest_ios_payercount = ($mobile_money_list[3]['payer_count']=="")? 0 : $mobile_money_list[3]['payer_count'];
			$guest_and_payercount = ($mobile_money_list[4]['payer_count']=="")? 0 : $mobile_money_list[4]['payer_count'];
			$guest_ama_payercount = ($mobile_money_list[5]['payer_count']=="")? 0 : $mobile_money_list[5]['payer_count'];
	
			$sql = "INSERT INTO tbl_user_guest_stat_daily VALUES('$yesterday', $guest_ios_logincount, $guest_and_logincount, $guest_ama_logincount, $guest_ios_payercount, $guest_and_payercount, $guest_ama_payercount, $guest_ios_money, $guest_and_money, $guest_ama_money, $ios_logincount, $and_logincount, $ama_logincount, $ios_payercount, $and_payercount, $ama_payercount, $ios_money, $and_money, $ama_money)ON DUPLICATE KEY UPDATE guest_ios_logincount = VALUES(guest_ios_logincount),guest_and_logincount= VALUES(guest_and_logincount),guest_ama_logincount= VALUES(guest_ama_logincount),guest_ios_payercount= VALUES(guest_ios_payercount),guest_and_payercount= VALUES(guest_and_payercount),guest_ama_payercount= VALUES(guest_ama_payercount),guest_ios_money= VALUES(guest_ios_money),guest_and_money= VALUES(guest_and_money),guest_ama_money= VALUES(guest_ama_money),ios_logincount= VALUES(ios_logincount),and_logincount= VALUES(and_logincount),ama_logincount= VALUES(ama_logincount),ios_payercount= VALUES(ios_payercount),and_payercount= VALUES(and_payercount),ama_payercount= VALUES(ama_payercount),ios_money= VALUES(ios_money),and_money= VALUES(and_money), ama_money= VALUES(ama_money);";
			$db_other->execute($sql);
		
			$db_redshift->end();
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	// Jacpot 누적 금액
	try
	{
		if (date("H") == "00")
		{
			$today = date("Y-m-d", strtotime("-1 days"));
			
			$sql = "SELECT 0 AS bettype, 0 AS ismultijackpot, slottype, SUM(jackpot) AS jackpot FROM `tbl_slot_object` WHERE ismultijackpot = 0 GROUP BY slottype ".
					"UNION ALL ".
					"SELECT 0 AS bettype, 1 AS ismultijackpot, t1.slottype, SUM(jackpot1) AS jackpot FROM `tbl_slot_object` t1 JOIN `tbl_multi_jackpot` t2 ON t1.objectidx = t2.objectidx WHERE ismultijackpot = 1 GROUP BY t1.slottype	".
					"UNION ALL	".
					"SELECT 1 AS bettype, 0 AS ismultijackpot, slottype, SUM(jackpot) AS jackpot FROM `tbl_jackpot_high` WHERE multi_jackpot = 0 GROUP BY slottype	".
					"UNION ALL	".
					"SELECT 1 AS bettype, 1 AS ismultijackpot, slottype, SUM(jackpot1) AS jackpot FROM `tbl_jackpot_high` WHERE multi_jackpot = 1 GROUP BY slottype";		
			$jackpot_save_list = $db_game->gettotallist($sql);
			
			for($i = 0; $i < sizeof($jackpot_save_list); $i++)
			{
				$bettype = $jackpot_save_list[$i]["bettype"];
				$ismultijackpot = $jackpot_save_list[$i]["ismultijackpot"];
				$slottype = $jackpot_save_list[$i]["slottype"];
				$jackpot = $jackpot_save_list[$i]["jackpot"];
			
				$sql = "INSERT INTO tbl_jackpot_save_stat_daily(today, bettype, ismultijackpot, slottype, amount) VALUES('$today', $bettype, $ismultijackpot, $slottype, $jackpot) ON DUPLICATE KEY UPDATE amount = VALUES(amount);";
				$db_analysis->execute($sql);
			}
		}
	}
	catch(Exception $e)
	{
		$issuccess = 0;
		write_log($e->getMessage());
	}
	
	// freecoin 맞추기
	try
	{
		if (date("H") == "00")
		{
			$freecoin_today = date("Y-m-d", strtotime("-1 days"));
			$freecoin_tomorrow = date("Y-m-d");
				
			$sql = "INSERT INTO tbl_freecoin_daily(today, category, type, freecount, freeamount) ".
					"SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, category, type, SUM(freecount) AS total_cnt, SUM(freeamount) AS total_amount ".
					"FROM tbl_user_freecoin_stat ".
					"WHERE '$freecoin_today 00:00:00' <= writedate AND writedate < '$freecoin_tomorrow 23:59:59' ".
					"GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), category, type ".
					"ON DUPLICATE KEY UPDATE freecount = VALUES(freecount), freeamount = VALUES(freeamount)";
			$db_analysis->execute($sql);
		}
	}
	catch(Exception $e)
	{
		$issuccess = 0;
		write_log($e->getMessage());
	}
	
	try
	{
		$sql = "SELECT useridx, COUNT(orderidx) AS order_cnt ".
				"FROM tbl_product_order_mobile ".
				"WHERE useridx > 20000 AND writedate > DATE_SUB(NOW(), INTERVAL 1 DAY) AND STATUS = 3 AND NOT EXISTS (SELECT * FROM tbl_user_block WHERE useridx = tbl_product_order_mobile.useridx AND blockcount > 10) ".
				"GROUP BY useridx ".
				"HAVING order_cnt >= 5";
	
		$abuse_list = $db_main->gettotallist($sql);
	
		for($i=0; $i<sizeof($abuse_list); $i++)
		{
			$abuse_useridx = $abuse_list[$i]["useridx"];
			$reason = "모바일 결제 어뷰즈";

			// 차단
			$sql = "UPDATE tbl_user_ext SET isblock=1 WHERE useridx=$abuse_useridx;";
			$sql .= "INSERT INTO tbl_user_block(useridx,blockcount,reason,blockdate) VALUES($abuse_useridx,999,'$reason',NOW()) ON DUPLICATE KEY UPDATE blockcount=999;";
			$db_main->execute($sql);
					
			$sql = "DELETE FROM tbl_user_online_".($abuse_useridx%10)." WHERE useridx=$abuse_useridx;";
			$sql .= "DELETE FROM tbl_user_online_sync2 WHERE useridx=$abuse_useridx;";
			$db_friend->execute($sql);
					
			$history_sql = "INSERT INTO tbl_user_block_history(useridx, reason, status, adminid, writedate) VALUES($abuse_useridx, '$reason', 1, 'Scheduler', NOW());";
			$db_analysis->execute($history_sql);
	
			// 코인 몰수
			$sql = "SELECT coin FROM tbl_user WHERE useridx = $abuse_useridx";
			$abuse_coin = $db_main->getvalue($sql);
					
			$abuse_coin = $abuse_coin*-1;
					
			$sql = "INSERT INTO tbl_user_cache_update(useridx,coin,writedate) VALUES($abuse_useridx,'$abuse_coin',NOW());";
			$db_main2->execute($sql);
					
			// 코인 몰수 로그
			$sql = "INSERT INTO tbl_freecoin_admin_log (useridx,freecoin,reason,message,writedate) VALUES('$abuse_useridx','$abuse_coin', '$reason', '', NOW())";
			$db_main->execute($sql);
					
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		$sql = "SELECT useridx ".
				"FROM ( ".
				"	SELECT useridx, COUNT(*) AS total_count, SUM(IF(STATUS=3,1,0)) AS abuse_count FROM tbl_product_order_mobile WHERE writedate > '2015-01-01 00:00:00' AND useridx > 20000 GROUP BY useridx HAVING total_count >= 20 ".
				") t1 ".
				"WHERE NOT EXISTS (SELECT useridx FROM tbl_user_block WHERE useridx = t1.useridx AND blockcount > 10) AND abuse_count/total_count > 0.5";
	
		$abuse_list = $db_main->gettotallist($sql);
	
		for($i=0; $i<sizeof($abuse_list); $i++)
		{
			$abuse_useridx = $abuse_list[$i]["useridx"];
			$reason = "모바일 결제 어뷰즈(누적)";
			
			// 차단
			$sql = "UPDATE tbl_user_ext SET isblock=1 WHERE useridx=$abuse_useridx;";
			$sql .= "INSERT INTO tbl_user_block(useridx,blockcount,reason,blockdate) VALUES($abuse_useridx,999,'$reason',NOW()) ON DUPLICATE KEY UPDATE blockcount=999;";
			$db_main->execute($sql);
				
			$sql = "DELETE FROM tbl_user_online_".($abuse_useridx%20)." WHERE useridx=$abuse_useridx;";
			$sql .= "DELETE FROM tbl_user_online_sync2 WHERE useridx=$abuse_useridx;";
			$db_friend->execute($sql);
				
			$history_sql = "INSERT INTO tbl_user_block_history(useridx, reason, status, adminid, writedate) VALUES($abuse_useridx, '$reason', 1, 'Scheduler', NOW());";
			$db_analysis->execute($history_sql);
		
			// 코인 몰수
			$sql = "SELECT coin FROM tbl_user WHERE useridx = $abuse_useridx";
			$abuse_coin = $db_main->getvalue($sql);
		
			$abuse_coin = $abuse_coin*-1;
		
			$sql = "INSERT INTO tbl_user_cache_update(useridx,coin,writedate) VALUES($abuse_useridx,'$abuse_coin',NOW());";
			$db_main2->execute($sql);
		
			// 코인 몰수 로그
			$sql = "INSERT INTO tbl_freecoin_admin_log (useridx,freecoin,reason,message,writedate) VALUES('$abuse_useridx','$abuse_coin', '$reason', '', NOW())";
			$db_main->execute($sql);
			
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		$today = date("Y-m-d");
		
		// 사용자 국가별 통계
		$sql = "SELECT A.country, totalusercnt, IFNULL(usercnt, 0) AS usercnt, IFNULL(purchase, 0) AS purchase ".
				"FROM ( ".
				"	SELECT country, COUNT(useridx) as totalusercnt ".
				"	FROM tbl_user_ext ".
				"	WHERE '$today 00:00:00' < logindate AND logindate < '$today 23:59:59' ".
				"		AND country in('us', 'au', 'ca', 'de', 'gb', 'fr', 'nl', 'no', 'nz', 'tw') ".
				"	GROUP BY country ".
				") A LEFT JOIN ( ".
				"	SELECT country, COUNT(DISTINCT t1.useridx) AS usercnt, SUM(facebookcredit) as purchase ".
				"	FROM tbl_product_order AS t1 LEFT JOIN tbl_user_ext AS t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND status=1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' ".
				"		AND country in('us', 'au', 'ca', 'de', 'gb', 'fr', 'nl', 'no', 'nz', 'tw') ".
				"	GROUP BY country ".
				") B ON A.country = B.country ".
				"ORDER BY purchase DESC";
	
		$nationlist = $db_main->gettotallist($sql);
	
		$sql = "SELECT COUNT(DISTINCT useridx) AS purchasercnt, IFNULL(SUM(facebookcredit), 0) AS totalpurchase, (SELECT COUNT(*) FROM tbl_user_ext WHERE '$today 00:00:00' < logindate AND logindate < '$today 23:59:59' ) AS totalusercnt ".
				"FROM tbl_product_order ".
				"WHERE useridx > 20000 AND status=1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
	
		$totaldata = $db_main->getarray($sql);
	
		$totalpurchasercnt = ($totaldata["purchasercnt"] =="")? 0 :$totaldata["purchasercnt"];
		$totalpurchase = ($totaldata["totalpurchase"] =="")? 0 :$totaldata["totalpurchase"];
		$totalusercnt = ($totaldata["totalusercnt"] =="")? 0 :$totaldata["totalusercnt"];
	
		for($i=0; $i<sizeof($nationlist);$i++)
		{
			$nation = $nationlist[$i]["country"];
			$usercnt = ($nationlist[$i]["totalusercnt"]=="")? 0 : $nationlist[$i]["totalusercnt"];
			$purchasercnt = $nationlist[$i]["usercnt"];
			$purchase = $nationlist[$i]["purchase"];
	
			$sql = "INSERT INTO tbl_user_nationstat_daily(today, nation, totalusercnt, usercnt, totalpurchasercnt, purchasercnt, totalpurchase, purchase) ".
					"VALUES ('$today', '$nation', $totalusercnt, $usercnt, $totalpurchasercnt, $purchasercnt, $totalpurchase, $purchase) ON DUPLICATE KEY UPDATE ".
					"totalusercnt=$totalusercnt, usercnt=$usercnt, totalpurchasercnt=$totalpurchasercnt, purchasercnt=$purchasercnt, totalpurchase=$totalpurchase, purchase=$purchase";
	
			$db_other->execute($sql);
		}
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	//Push Enable 통계
	try
	{
		if(date("H") == "00")
		{
			$mobile_push_enable_today = date("Y-m-d", strtotime("-1 days"));
	
			$sql = "SELECT os_type, SUM(IF(push_enabled=1, 1, 0)) AS push_enabled_count, COUNT(*) AS usercount	 
					FROM	
					(	
						SELECT t1.os_type, push_enabled, t1.device_id	
						FROM tbl_mobile t1 
						JOIN `tbl_user_mobile_connection_log` t2 
						ON t1.device_id = t2.device_id 
						WHERE  t2.logindate > DATE_SUB(NOW(), INTERVAL 1 DAY) GROUP BY t1.device_id
					) t3 GROUP BY os_type";
			$push_enabled_list = $db_mobile->gettotallist($sql);
	
			$insert_sql = "";
	
			for($i=0; $i<sizeof($push_enabled_list); $i++)
			{
				$os_type = $push_enabled_list[$i]["os_type"];
				$push_enabled_count = $push_enabled_list[$i]["push_enabled_count"];
				$usercount = $push_enabled_list[$i]["usercount"];
					
				if($insert_sql == "")
					$insert_sql = "INSERT INTO user_push_enabled_stat VALUES('$mobile_push_enable_today', 1, $os_type, $push_enabled_count, $usercount);";
				else
					$insert_sql .= "INSERT INTO user_push_enabled_stat VALUES('$mobile_push_enable_today', 1, $os_type, $push_enabled_count, $usercount);";
			}
	
			if($insert_sql != "")
				$db_analysis->execute($insert_sql);
	
			$sql = "SELECT os_type, SUM(IF(push_enabled=1, 1, 0)) AS push_enabled_count, COUNT(*) AS usercount	 
					FROM	
					(	
						SELECT t1.os_type, push_enabled, t1.device_id	
						FROM tbl_mobile t1 
						JOIN `tbl_user_mobile_connection_log` t2 
						ON t1.device_id = t2.device_id 
						WHERE  t2.logindate > DATE_SUB(NOW(), INTERVAL 2 WEEK) GROUP BY t1.device_id
					) t3 GROUP BY os_type";
			$active_push_enabled_list = $db_mobile->gettotallist($sql);
	
			$insert_sql = "";
	
			for($i=0; $i<sizeof($active_push_enabled_list); $i++)
			{
				$os_type = $active_push_enabled_list[$i]["os_type"];
				$push_enabled_count = $active_push_enabled_list[$i]["push_enabled_count"];
				$usercount = $active_push_enabled_list[$i]["usercount"];
	
				if($insert_sql == "")
					$insert_sql = "INSERT INTO user_push_enabled_stat VALUES('$mobile_push_enable_today', 2, $os_type, $push_enabled_count, $usercount);";
				else
					$insert_sql .= "INSERT INTO user_push_enabled_stat VALUES('$mobile_push_enable_today', 2, $os_type, $push_enabled_count, $usercount);";
			}
	
			if($insert_sql != "")
				$db_analysis->execute($insert_sql);	
		
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		if (date("H") == "00")
			$today = date("Y-m-d", time() - 60 * 60);
		else
			$today = date("Y-m-d");
		
		//ios
		$sql = "SELECT t1.useridx, writedate, createdate ".
				"FROM tbl_user_auth_change_log t1 JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx	".
				"WHERE t1.useridx > 20000 AND change_type = 0 AND platform = 1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
		$user_createdate_list = $db_main->gettotallist($sql);
			
		$usercount = 0;
		$elapsed_days = 0;
		
		for($i=0; $i<sizeof($user_createdate_list); $i++)
		{
			$create_date = date_create($user_createdate_list[$i]["createdate"]);
			$connect_date = date_create($user_createdate_list[$i]["writedate"]);
		
			$interval = $create_date->diff($connect_date);
			$elapsed_days += $interval->format("%r%a");
		
			$usercount++;
		}
			
		$sql = "INSERT INTO tbl_facebook_connect_stat_daily (today, platform, usercount, elapsed_days) VALUES ('$today', 1, $usercount, $elapsed_days) ".
				"ON DUPLICATE KEY UPDATE usercount=VALUES(usercount), elapsed_days=VALUES(elapsed_days)";
		$db_other->execute($sql);
			
		//android
		$sql = "SELECT t1.useridx, writedate, createdate ".
				"FROM tbl_user_auth_change_log t1 JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx	".
				"WHERE t1.useridx > 20000 AND change_type = 0 AND platform = 2 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
		$user_createdate_list = $db_main->gettotallist($sql);
		
		$usercount = 0;
		$elapsed_days = 0;
								
		for($i=0; $i<sizeof($user_createdate_list); $i++)
		{
			$create_date = date_create($user_createdate_list[$i]["createdate"]);
			$connect_date = date_create($user_createdate_list[$i]["writedate"]);
				
			$interval = $create_date->diff($connect_date);
			$elapsed_days += $interval->format("%r%a");
				
			$usercount++;
		}
								
		$sql = "INSERT INTO tbl_facebook_connect_stat_daily (today, platform, usercount, elapsed_days) VALUES ('$today', 2, $usercount, $elapsed_days) ".
				"ON DUPLICATE KEY UPDATE usercount=VALUES(usercount), elapsed_days=VALUES(elapsed_days)";
		$db_other->execute($sql);
		
						
		//amazon
		$sql = "SELECT t1.useridx, writedate, createdate ".
				"FROM tbl_user_auth_change_log t1 JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx	".
				"WHERE t1.useridx > 20000 AND change_type = 0 AND platform = 3 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
		$user_createdate_list = $db_main->gettotallist($sql);
		
		$usercount = 0;
		$elapsed_days = 0;
						
		for($i=0; $i<sizeof($user_createdate_list); $i++)
		{
			$create_date = date_create($user_createdate_list[$i]["createdate"]);
			$connect_date = date_create($user_createdate_list[$i]["writedate"]);
							
			$interval = $create_date->diff($connect_date);
			$elapsed_days += $interval->format("%r%a");
									
			$usercount++;
		}
						
		$sql = "INSERT INTO tbl_facebook_connect_stat_daily (today, platform, usercount, elapsed_days) VALUES ('$today', 3, $usercount, $elapsed_days) ".
				"ON DUPLICATE KEY UPDATE usercount=VALUES(usercount), elapsed_days=VALUES(elapsed_days)";
		$db_other->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
// 	try
// 	{
// 		if (date("H") == "00")
// 			$today = date("Y-m-d", time() - 60 * 60);
// 		else
// 			$today = date("Y-m-d");
	
// 		$sql = "SELECT 7 as group_no, COUNT(*) as cnt FROM tbl_coupon_noti_log WHERE status = 1 AND senddate>='$today 00:00:00' AND senddate<='$today 23:59:59';";
// 		$coupon_a2u_info =  $db_main2->getarray($sql);
	
// 		$coupon_a2u_group_no = $coupon_a2u_info["group_no"];
// 		$coupon_a2u_cnt = $coupon_a2u_info["cnt"];
	
// 		$sql = "INSERT INTO tbl_a2u_sendcount (today, group_no, cnt) VALUES ('$today', $coupon_a2u_group_no, $coupon_a2u_cnt) ".
// 				"ON DUPLICATE KEY UPDATE cnt=VALUES(cnt)";
// 		$db_analysis->execute($sql);
	
// 		$sql = "SELECT 13 AS group_no, COUNT(*) as cnt FROM tbl_user_firstbuy_gift_log WHERE inboxidx > 0 AND senddate>='$today 00:00:00' AND senddate<='$today 23:59:59';";
// 		$firstbuy_a2u_info =  $db_main2->getarray($sql);
	
// 		$firstbuy_a2u_group_no = $firstbuy_a2u_info["group_no"];
// 		$firstbuy_a2u_cnt = $firstbuy_a2u_info["cnt"];
	
// 		$sql = "INSERT INTO tbl_a2u_sendcount (today, group_no, cnt) VALUES ('$today', $firstbuy_a2u_group_no, $firstbuy_a2u_cnt) ".
// 				"ON DUPLICATE KEY UPDATE cnt=VALUES(cnt)";
// 		$db_analysis->execute($sql);
	
// 		$sql = "SELECT 14 AS group_no, COUNT(*) as cnt FROM tbl_user_newuser_gift_log WHERE inboxidx > 0 AND senddate>='$today 00:00:00' AND senddate<='$today 23:59:59';";
// 		$newuser_a2u_info =  $db_main2->getarray($sql);
	
// 		$newuser_a2u_group_no = $newuser_a2u_info["group_no"];
// 		$newuser_a2u_cnt = $newuser_a2u_info["cnt"];
	
// 		$sql = "INSERT INTO tbl_a2u_sendcount (today, group_no, cnt) VALUES ('$today', $newuser_a2u_group_no, $newuser_a2u_cnt) ".
// 				"ON DUPLICATE KEY UPDATE cnt=VALUES(cnt)";
// 		$db_analysis->execute($sql);
	
// 		$sql = "SELECT 15 as group_no, COUNT(*) as cnt FROM tbl_notification_group_log WHERE group_no = 6 AND inboxidx > 0 AND senddate>='$today 00:00:00' AND senddate<='$today 23:59:59';";
// 		$nopay_3day_a2u_info =  $db_main->getarray($sql);
	
// 		$nopay_3day_a2u_group_no = $nopay_3day_a2u_info["group_no"];
// 		$nopay_3day_a2u_cnt = $nopay_3day_a2u_info["cnt"];
	
// 		$sql = "INSERT INTO tbl_a2u_sendcount (today, group_no, cnt) VALUES ('$today', $nopay_3day_a2u_group_no, $nopay_3day_a2u_cnt) ".
// 				"ON DUPLICATE KEY UPDATE cnt=VALUES(cnt)";
// 		$db_analysis->execute($sql);
	
// 		$sql = "SELECT 16 as group_no, COUNT(*) as cnt FROM tbl_notification_group_log WHERE group_no = 7 AND inboxidx > 0 AND senddate>='$today 00:00:00' AND senddate<='$today 23:59:59';";
// 		$vip_a2u_info =  $db_main->getarray($sql);
	
// 		$vip_a2u_group_no = $vip_a2u_info["group_no"];
// 		$vip_a2u_cnt = $vip_a2u_info["cnt"];
	
// 		$sql = "INSERT INTO tbl_a2u_sendcount (today, group_no, cnt) VALUES ('$today', $vip_a2u_group_no, $vip_a2u_cnt) ".
// 				"ON DUPLICATE KEY UPDATE cnt=VALUES(cnt)";
// 		$db_analysis->execute($sql);
	
// 		$sql = "SELECT 17 as group_no, COUNT(*) as cnt FROM tbl_notification_group_log WHERE group_no = 8 AND inboxidx > 0 AND senddate>='$today 00:00:00' AND senddate<='$today 23:59:59';";
// 		$normal_a2u_info =  $db_main->getarray($sql);
	
// 		$normal_a2u_group_no = $normal_a2u_info["group_no"];
// 		$normal_a2u_cnt = $normal_a2u_info["cnt"];
	
// 		$sql = "INSERT INTO tbl_a2u_sendcount (today, group_no, cnt) VALUES ('$today', $normal_a2u_group_no, $normal_a2u_cnt) ".
// 				"ON DUPLICATE KEY UPDATE cnt=VALUES(cnt)";
// 		$db_analysis->execute($sql);
	
// 	}
// 	catch(Exception $e)
// 	{
// 		write_log($e->getMessage());
// 	}
	
	try
	{
		if (date("H") == "00")
		{
			$yesterday = date("Y-m-d",strtotime("-1 days"));			
			
			for($i=0; $i<10; $i++)
			{
				$sql = "SELECT SUM(CASE WHEN platform = 1 THEN 1 ELSE 0 END) AS ios, SUM(CASE WHEN platform = 2 THEN 1 ELSE 0 END) AS android, SUM(CASE WHEN platform = 3 THEN 1 ELSE 0 END) AS amazon ".
						"FROM tbl_user_login_log_$i ".
						"WHERE today = '$yesterday' AND platform IN (1, 2, 3) AND is_guest = 1";
				$guest_logincount_list = $db_slave_livestats->getarray($sql);
			
				$guest_ios_logincount += $guest_logincount_list["ios"];
				$guest_android_logincount += $guest_logincount_list["android"];
				$guest_amazon_logincount += $guest_logincount_list["amazon"];
			}
			
			if($guest_ios_logincount == "")
				$guest_ios_logincount = 0;
				
			if($guest_android_logincount == "")
				$guest_android_logincount = 0;
			
			if($guest_amazon_logincount == "")
				$guest_amazon_logincount = 0;			

			for($i=0; $i<10; $i++)
			{
				$sql = "SELECT SUM(CASE WHEN platform = 1 THEN 1 ELSE 0 END) AS ios, SUM(CASE WHEN platform = 2 THEN 1 ELSE 0 END) AS android, SUM(CASE WHEN platform = 3 THEN 1 ELSE 0 END) AS amazon ".
						"FROM tbl_user_login_log_$i ".
						"WHERE today = '$yesterday' AND platform IN (1, 2, 3) AND is_guest = 0";
						$mobile_logincount_list = $db_slave_livestats->getarray($sql);
							
						$mobile_ios_logincount += $mobile_logincount_list["ios"];
						$mobile_android_logincount += $mobile_logincount_list["android"];
						$mobile_amazon_logincount += $mobile_logincount_list["amazon"];
			}
				
			if($mobile_ios_logincount == "")
				$mobile_ios_logincount = 0;
			
			if($mobile_android_logincount == "")
				$mobile_android_logincount = 0;
					
			if($mobile_amazon_logincount == "")
				$mobile_amazon_logincount = 0;
		
				
			$sql = "SELECT os_type, COUNT(DISTINCT(useridx)) AS payer_count, SUM(money) AS money FROM tbl_product_order_mobile WHERE useridx > 20000 AND STATUS = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
					"AND EXISTS(SELECT useridx FROM tbl_user_ext WHERE isguest = 1 AND useridx = tbl_product_order_mobile.useridx) ".
					"GROUP BY os_type ORDER BY os_type ASC";
	
			$guest_money_list = $db_main->gettotallist($sql);
			
			for($i = 0; $i < sizeof($guest_money_list); $i++)
			{
				$platform = $guest_money_list[$i]["os_type"];
				$guest_payer_count = $guest_money_list[$i]["payer_count"];
				$guest_money = $guest_money_list[$i]["money"];
		
				$guest_mobile_payercount[$platform] = $guest_payer_count;
				$guest_mobile_money[$platform] = $guest_money;
			}
	
			if($guest_mobile_payercount[1] == "")
				$guest_mobile_payercount[1] = 0;
				
			if($guest_mobile_payercount[2] == "")
				$guest_mobile_payercount[2] = 0;
			
			if($guest_mobile_payercount[3] == "")
				$guest_mobile_payercount[3] = 0;

			if($guest_mobile_money[1] == "")
				$guest_mobile_money[1] = 0;
			
			if($guest_mobile_money[2] == "")
				$guest_mobile_money[2] = 0;
				
			if($guest_mobile_money[3] == "")
				$guest_mobile_money[3] = 0;

				
			$sql = "SELECT os_type, COUNT(DISTINCT(useridx)) AS payer_count, SUM(money) AS money FROM tbl_product_order_mobile WHERE useridx > 20000 AND STATUS = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
					"AND NOT EXISTS(SELECT useridx FROM tbl_user_ext WHERE isguest = 1 AND useridx = tbl_product_order_mobile.useridx) ".
					"GROUP BY os_type ORDER BY os_type ASC";
				
			$money_list = $db_main->gettotallist($sql);
			
			for($i = 0; $i < sizeof($money_list); $i++)
			{
				$platform = $money_list[$i]["os_type"];
				$payer_count = $money_list[$i]["payer_count"];
				$money = $money_list[$i]["money"];
			
				$mobile_payercount[$platform] = $payer_count;
				$mobile_money[$platform] = $money;
			}
				
			if($mobile_payercount[1] == "")
				$mobile_payercount[1] = 0;
				
			if($mobile_payercount[2] == "")
				$mobile_payercount[2] = 0;
			
			if($mobile_payercount[3] == "")
				$mobile_payercount[3] = 0;

			if($mobile_money[1] == "")
				$mobile_money[1] = 0;
			
			if($mobile_money[2] == "")
				$mobile_money[2] = 0;
				
			if($mobile_money[3] == "")
				$mobile_money[3] = 0;

				
			$sql = "INSERT INTO tbl_user_guest_stat_daily VALUES('$yesterday', $guest_ios_logincount, $guest_android_logincount, $mobile_amazon_logincount, $guest_mobile_payercount[1], $guest_mobile_payercount[2], $guest_mobile_payercount[3], $guest_mobile_money[1], $guest_mobile_money[2], $guest_mobile_money[3], ".
					"$mobile_ios_logincount, $mobile_android_logincount, $mobile_amazon_logincount, $mobile_payercount[1], $mobile_payercount[2], $mobile_payercount[3], $mobile_money[1], $mobile_money[2], $mobile_money[3])ON DUPLICATE KEY UPDATE guest_ios_logincount = VALUES(guest_ios_logincount),guest_and_logincount= VALUES(guest_and_logincount),guest_ama_logincount= VALUES(guest_ama_logincount),guest_ios_payercount= VALUES(guest_ios_payercount),guest_and_payercount= VALUES(guest_and_payercount),guest_ama_payercount= VALUES(guest_ama_payercount),guest_ios_money= VALUES(guest_ios_money),guest_and_money= VALUES(guest_and_money),guest_ama_money= VALUES(guest_ama_money),ios_logincount= VALUES(ios_logincount),and_logincount= VALUES(and_logincount),ama_logincount= VALUES(ama_logincount),ios_payercount= VALUES(ios_payercount),and_payercount= VALUES(and_payercount),ama_payercount= VALUES(ama_payercount),ios_money= VALUES(ios_money),and_money= VALUES(and_money), ama_money= VALUES(ama_money);";
			$db_other->execute($sql);
		}
	}
	catch(Exception $e)
	{
		$issuccess = 0;
		write_log($e->getMessage());
	}
		
	try
	{
		$today = date("Y-m-d");
		$yesterday = date("Y-m-d",strtotime("-1 days"));
		
		// web 최근 30일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE platform = 0 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE 'fbself%')";
		$join_count_30day = $db_slave_main->getvalue($sql);
		
		$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
				"FROM tbl_ad_stats ".
				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 30 DAY), '%Y-%m-%d') < today ";
		$spend_30day = $db_slave_main2->getvalue($sql);
		
		// 최근 30일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE platform = 0 AND (adflag LIKE 'fbself%' OR adflag) AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate";
		$nogame_30day = $db_slave_main->getvalue($sql);
		
		// 최근 30일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 0 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE 'fbself%') ".
				") t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"UNION ALL ".
				"SELECT t1.useridx, money ".
				"FROM ( ".
				"	SELECT useridx ".
				"	FROM tbl_user_ext ".
				"	WHERE platform = 0 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE 'fbself%') ".
				") t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_30day = $db_slave_main->getarray($sql);
		
		$pay_30day = $pay_info_30day["money"];
		$pay_user_cnt_30day = $pay_info_30day["user_cnt"];
		
		// 최근 14일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE platform = 0 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE 'fbself%')";
		$join_count_14day = $db_slave_main->getvalue($sql);
		
		$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
				"FROM tbl_ad_stats ".
				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 14 DAY), '%Y-%m-%d') < today ";
		$spend_14day = $db_slave_main2->getvalue($sql);
		
		// 최근 14일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE platform = 0 AND (adflag LIKE 'fbself%') AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate";
		$nogame_14day = $db_slave_main->getvalue($sql);
		
		// 최근 14일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 0 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE 'fbself%') ".
				") t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"UNION ALL ".
				"SELECT t1.useridx, money ".
				"FROM ( ".
				"	SELECT useridx ".
				"	FROM tbl_user_ext ".
				"	WHERE platform = 0 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE 'fbself%') ".
				") t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_14day = $db_slave_main->getarray($sql);

		$pay_14day = $pay_info_14day["money"];
		$pay_user_cnt_14day = $pay_info_14day["user_cnt"];
		
		// 최근 7일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE platform = 0 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE 'fbself%')";
		$join_count_7day = $db_slave_main->getvalue($sql);
		
		$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
				"FROM tbl_ad_stats ".
				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7 DAY), '%Y-%m-%d') < today ";
		$spend_7day = $db_slave_main2->getvalue($sql);
		
		// 최근 14일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE platform = 0 AND (adflag LIKE 'fbself%') AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate";
		$nogame_7day = $db_slave_main->getvalue($sql);
		
		// 최근 7일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 0 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE 'fbself%') ".
				") t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"UNION ALL ".
				"SELECT t1.useridx, money ".
				"FROM ( ".
				"	SELECT useridx ".
				"	FROM tbl_user_ext ".
				"	WHERE platform = 0 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE 'fbself%') ".
				") t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_7day = $db_slave_main->getarray($sql);

		$pay_7day = $pay_info_7day["money"];
		$pay_user_cnt_7day = $pay_info_7day["user_cnt"];
		
		// 어제 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE platform = 0 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE 'fbself%')";
		$join_count_yesterday = $db_slave_main->getvalue($sql);
		
		// 어제 미게임수
		$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
				"FROM tbl_ad_stats ".
				"WHERE today BETWEEN '$yesterday' AND '$yesterday' ";
		$spend_yesterday = $db_slave_main2->getvalue($sql);
		
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE platform = 0 AND (adflag LIKE 'fbself%') AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$nogame_yesterday = $db_slave_main->getvalue($sql);
		
		// 어제 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 0 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE 'fbself%') ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 0 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE 'fbself%') ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_yesterday = $db_slave_main->getarray($sql);
		
		$pay_yesterday = $pay_info_yesterday["money"];
		$pay_user_cnt_yesterday = $pay_info_yesterday["user_cnt"];
		
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 1, '$join_count_30day', 'Join(30day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 2, '$spend_30day', 'Spend(30day)', now()) ON DUPLICATE KEY UPDATE value='$spend_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 3, '$nogame_30day', 'No Game(30day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 4, '$pay_30day', 'Pay(30day)', now()) ON DUPLICATE KEY UPDATE value='$pay_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 5, '$pay_user_cnt_30day', 'Pay User Count(30day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 6, '$join_count_14day', 'Join(14day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 7, '$spend_14day', 'Spend(14day)', now()) ON DUPLICATE KEY UPDATE value='$spend_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 8, '$nogame_14day', 'No Game(14day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 9, '$pay_14day', 'Pay(14day)', now()) ON DUPLICATE KEY UPDATE value='$pay_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 10, '$pay_user_cnt_14day', 'Pay User Count(14day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 11, '$join_count_7day', 'Join(7day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 12, '$spend_7day', 'Spend(7day)', now()) ON DUPLICATE KEY UPDATE value='$spend_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 13, '$nogame_7day', 'No Game(7day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 14, '$pay_7day', 'Pay(7day)', now()) ON DUPLICATE KEY UPDATE value='$pay_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 15, '$pay_user_cnt_7day', 'Pay User Count(7day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 16, '$join_count_yesterday', 'Join(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$join_count_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 17, '$spend_yesterday', 'Spend(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$spend_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 18, '$nogame_yesterday', 'No Game(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$nogame_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 19, '$pay_yesterday', 'Pay(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$pay_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 20, '$pay_user_cnt_yesterday', 'Pay User Count(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		$sql = "INSERT INTO tbl_marketing_dash_daily(today, platform, market_type, category, reg_count, spend, nogame_count, pay_user_count, pay_amount) ".
				"VALUES('$today', 0, 0, 1, $join_count_30day, $spend_30day, $nogame_30day, $pay_user_cnt_30day, $pay_30day), ".
				"	('$today', 0, 0, 2, $join_count_14day, $spend_14day, $nogame_14day, $pay_user_cnt_14day, $pay_14day), ".
				"	('$today', 0, 0, 3, $join_count_7day, $spend_7day, $nogame_7day, $pay_user_cnt_7day, $pay_7day), ".
				"	('$today', 0, 0, 4, $join_count_yesterday, $spend_yesterday, $nogame_yesterday, $pay_user_cnt_yesterday, $pay_yesterday)".
				"ON DUPLICATE KEY UPDATE reg_count=VALUES(reg_count), spend=VALUES(spend), nogame_count=VALUES(nogame_count), pay_user_count=VALUES(pay_user_count), pay_amount=VALUES(pay_amount)";
		$db_other->execute($sql);
		
		// ios - fbself 최근 30일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 1";
		$join_count_30day = $db_slave_main->getvalue($sql);
		
// 		$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
// 				"FROM tbl_ad_stats_mobile ".
// 				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 30 DAY), '%Y-%m-%d') < today AND platform = 1 ";
		$sql = " SELECT IFNULL(SUM(spend), 0) ". 
		  		" FROM tbl_agency_spend_daily ".
                " WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 30 DAY), '%Y-%m-%d') < today AND platform = 1 AND (agencyname LIKE 'Face%' OR agencyname LIKE 'm_%') ";
		$spend_30day = $db_slave_main2->getvalue($sql);
		
		// 최근 30일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 1 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate";
		$nogame_30day = $db_slave_main->getvalue($sql);
		
		// 최근 30일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_30day = $db_slave_main->getarray($sql);

		$pay_30day = $pay_info_30day["money"];
		$pay_user_cnt_30day = $pay_info_30day["user_cnt"];
		
		// 최근 14일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 1";
		$join_count_14day = $db_slave_main->getvalue($sql);
		
		// 최근 14일 비용
// 		$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
// 				"FROM tbl_ad_stats_mobile ".
// 				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 14 DAY), '%Y-%m-%d') < today AND platform = 1 ";
		$sql = " SELECT IFNULL(SUM(spend), 0) ".
		  		" FROM tbl_agency_spend_daily ".
		  		" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 14 DAY), '%Y-%m-%d') < today AND platform = 1 AND (agencyname LIKE 'Face%' OR agencyname LIKE 'm_%') ";
		$spend_14day = $db_slave_main2->getvalue($sql);
		
		// 최근 14일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 1 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate";
		$nogame_14day = $db_slave_main->getvalue($sql);
		
		// 최근 14일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_14day = $db_slave_main->getarray($sql);
				
		$pay_14day = $pay_info_14day["money"];
		$pay_user_cnt_14day = $pay_info_14day["user_cnt"];
		
		// 최근 7일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 1";
		$join_count_7day = $db_slave_main->getvalue($sql);
		
		// 최근 7일 비용
// 		$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
// 				"FROM tbl_ad_stats_mobile ".
// 				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7 DAY), '%Y-%m-%d') < today AND platform = 1 ";
		$sql = " SELECT IFNULL(SUM(spend), 0) ".
		  		" FROM tbl_agency_spend_daily ".
		  		" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7 DAY), '%Y-%m-%d') < today AND platform = 1 AND (agencyname LIKE 'Face%' OR agencyname LIKE 'm_%') ";
		$spend_7day = $db_slave_main2->getvalue($sql);
		
		// 최근 7일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate";
		$nogame_7day = $db_slave_main->getvalue($sql);
		
		// 최근 7일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_7day = $db_slave_main->getarray($sql);
		
		$pay_7day = $pay_info_7day["money"];
		$pay_user_cnt_7day = $pay_info_7day["user_cnt"];
		
		// 어제 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 1";
		$join_count_yesterday = $db_slave_main->getvalue($sql);
		
		// 어제 비용
// 		$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
// 				"FROM tbl_ad_stats_mobile ".
// 				"WHERE today BETWEEN '$yesterday' AND '$yesterday' AND platform = 1 ";
		$sql = " SELECT IFNULL(SUM(spend), 0) ".
		  		" FROM tbl_agency_spend_daily ".
		  		" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') < today AND platform = 1 AND (agencyname LIKE 'Face%' OR agencyname LIKE 'm_%') ";
		$spend_yesterday = $db_slave_main2->getvalue($sql);
		
		// 어제 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 1 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$nogame_yesterday = $db_slave_main->getvalue($sql);
		
		// 어제 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_yesterday = $db_slave_main->getarray($sql);
				
		$pay_yesterday = $pay_info_yesterday["money"];
		$pay_user_cnt_yesterday = $pay_info_yesterday["user_cnt"];
		
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 101, '$join_count_30day', 'Join(30day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 102, '$spend_30day', 'Spend(30day)', now()) ON DUPLICATE KEY UPDATE value='$spend_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 103, '$nogame_30day', 'No Game(30day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 104, '$pay_30day', 'Pay(30day)', now()) ON DUPLICATE KEY UPDATE value='$pay_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 105, '$pay_user_cnt_30day', 'Pay User Count(30day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 106, '$join_count_14day', 'Join(14day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 107, '$spend_14day', 'Spend(14day)', now()) ON DUPLICATE KEY UPDATE value='$spend_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 108, '$nogame_14day', 'No Game(14day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 109, '$pay_14day', 'Pay(14day)', now()) ON DUPLICATE KEY UPDATE value='$pay_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 110, '$pay_user_cnt_14day', 'Pay User Count(14day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 111, '$join_count_7day', 'Join(7day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 112, '$spend_7day', 'Spend(7day)', now()) ON DUPLICATE KEY UPDATE value='$spend_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 113, '$nogame_7day', 'No Game(7day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 114, '$pay_7day', 'Pay(7day)', now()) ON DUPLICATE KEY UPDATE value='$pay_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 115, '$pay_user_cnt_7day', 'Pay User Count(7day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 116, '$join_count_yesterday', 'Join(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$join_count_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 117, '$spend_yesterday', 'Spend(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$spend_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 118, '$nogame_yesterday', 'No Game(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$nogame_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 119, '$pay_yesterday', 'Pay(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$pay_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 120, '$pay_user_cnt_yesterday', 'Pay User Count(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		$sql = "INSERT INTO tbl_marketing_dash_daily(today, platform, market_type, category, reg_count, spend, nogame_count, pay_user_count, pay_amount) ".
				"VALUES('$today', 1, 0, 1, $join_count_30day, $spend_30day, $nogame_30day, $pay_user_cnt_30day, $pay_30day), ".
				"	('$today', 1, 0, 2, $join_count_14day, $spend_14day, $nogame_14day, $pay_user_cnt_14day, $pay_14day), ".
				"	('$today', 1, 0, 3, $join_count_7day, $spend_7day, $nogame_7day, $pay_user_cnt_7day, $pay_7day), ".
				"	('$today', 1, 0, 4, $join_count_yesterday, $spend_yesterday, $nogame_yesterday, $pay_user_cnt_yesterday, $pay_yesterday)".
				"ON DUPLICATE KEY UPDATE reg_count=VALUES(reg_count), spend=VALUES(spend), nogame_count=VALUES(nogame_count), pay_user_count=VALUES(pay_user_count), pay_amount=VALUES(pay_amount)";
		$db_other->execute($sql);
		
		// ios - agency 최근 30일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND platform = 1";
		$join_count_30day = $db_slave_main->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(spend), 0) ".
	    		"FROM tbl_agency_spend_daily ".
				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 30 DAY), '%Y-%m-%d') < today AND platform = 1 AND (agencyname NOT LIKE 'Face%' AND agencyname NOT LIKE 'm_d%') ";
		$spend_30day = $db_slave_main2->getvalue($sql);
		
		// 최근 30일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND platform = 1 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate";
		$nogame_30day = $db_slave_main->getvalue($sql);
		
		// 최근 30일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_30day = $db_slave_main->getarray($sql);
				
		$pay_30day = $pay_info_30day["money"];
		$pay_user_cnt_30day = $pay_info_30day["user_cnt"];
		
		// 최근 14일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND platform = 1";
		$join_count_14day = $db_slave_main->getvalue($sql);
		
		// 최근 14일 비용
		$sql = "SELECT IFNULL(SUM(spend), 0) ".
	    		"FROM tbl_agency_spend_daily ".
				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 14 DAY), '%Y-%m-%d') < today AND platform = 1  AND (agencyname NOT LIKE 'Face%' AND agencyname NOT LIKE 'm_d%')";
		$spend_14day = $db_slave_main2->getvalue($sql);
		
		// 최근 14일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND platform = 1 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate";
		$nogame_14day = $db_slave_main->getvalue($sql);
		
		// 최근 14일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_14day = $db_slave_main->getarray($sql);
				
		$pay_14day = $pay_info_14day["money"];
		$pay_user_cnt_14day = $pay_info_14day["user_cnt"];
		
		// 최근 7일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND platform = 1";
		$join_count_7day = $db_slave_main->getvalue($sql);
		
		// 최근 7일 비용
		$sql = "SELECT IFNULL(SUM(spend), 0) ".
	    		"FROM tbl_agency_spend_daily ".
				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7 DAY), '%Y-%m-%d') < today AND platform = 1 AND (agencyname NOT LIKE 'Face%' AND agencyname NOT LIKE 'm_d%')";
		$spend_7day = $db_slave_main2->getvalue($sql);
		
		// 최근 7일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND platform = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate";
		$nogame_7day = $db_slave_main->getvalue($sql);
		
		// 최근 7일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_7day = $db_slave_main->getarray($sql);
		
		$pay_7day = $pay_info_7day["money"];
		$pay_user_cnt_7day = $pay_info_7day["user_cnt"];
		
		// 어제 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND platform = 1";
		$join_count_yesterday = $db_slave_main->getvalue($sql);
		
		// 어제 비용
		$sql = "SELECT IFNULL(SUM(spend), 0) ".
	    		"FROM tbl_agency_spend_daily ".
				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') < today AND platform = 1 AND (agencyname NOT LIKE 'Face%' AND agencyname NOT LIKE 'm_d%')";
		$spend_yesterday = $db_slave_main2->getvalue($sql);
		
		// 어제 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND platform = 1 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$nogame_yesterday = $db_slave_main->getvalue($sql);
		
		// 어제 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 1 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_yesterday = $db_slave_main->getarray($sql);
		
		$pay_yesterday = $pay_info_yesterday["money"];
		$pay_user_cnt_yesterday = $pay_info_yesterday["user_cnt"];
		
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 401, '$join_count_30day', 'Join(30day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 402, '$spend_30day', 'Spend(30day)', now()) ON DUPLICATE KEY UPDATE value='$spend_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 403, '$nogame_30day', 'No Game(30day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 404, '$pay_30day', 'Pay(30day)', now()) ON DUPLICATE KEY UPDATE value='$pay_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 405, '$pay_user_cnt_30day', 'Pay User Count(30day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 406, '$join_count_14day', 'Join(14day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 407, '$spend_14day', 'Spend(14day)', now()) ON DUPLICATE KEY UPDATE value='$spend_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 408, '$nogame_14day', 'No Game(14day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 409, '$pay_14day', 'Pay(14day)', now()) ON DUPLICATE KEY UPDATE value='$pay_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 410, '$pay_user_cnt_14day', 'Pay User Count(14day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 411, '$join_count_7day', 'Join(7day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 412, '$spend_7day', 'Spend(7day)', now()) ON DUPLICATE KEY UPDATE value='$spend_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 413, '$nogame_7day', 'No Game(7day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 414, '$pay_7day', 'Pay(7day)', now()) ON DUPLICATE KEY UPDATE value='$pay_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 415, '$pay_user_cnt_7day', 'Pay User Count(7day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 416, '$join_count_yesterday', 'Join(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$join_count_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 417, '$spend_yesterday', 'Spend(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$spend_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 418, '$nogame_yesterday', 'No Game(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$nogame_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 419, '$pay_yesterday', 'Pay(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$pay_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 420, '$pay_user_cnt_yesterday', 'Pay User Count(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		$sql = "INSERT INTO tbl_marketing_dash_daily(today, platform, market_type, category, reg_count, spend, nogame_count, pay_user_count, pay_amount) ".
				"VALUES('$today', 1, 1, 1, $join_count_30day, $spend_30day, $nogame_30day, $pay_user_cnt_30day, $pay_30day), ".
				"	('$today', 1, 1, 2, $join_count_14day, $spend_14day, $nogame_14day, $pay_user_cnt_14day, $pay_14day), ".
				"	('$today', 1, 1, 3, $join_count_7day, $spend_7day, $nogame_7day, $pay_user_cnt_7day, $pay_7day), ".
				"	('$today', 1, 1, 4, $join_count_yesterday, $spend_yesterday, $nogame_yesterday, $pay_user_cnt_yesterday, $pay_yesterday)".
				"ON DUPLICATE KEY UPDATE reg_count=VALUES(reg_count), spend=VALUES(spend), nogame_count=VALUES(nogame_count), pay_user_count=VALUES(pay_user_count), pay_amount=VALUES(pay_amount)";
		$db_other->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		$today = date("Y-m-d");
		$yesterday = date("Y-m-d",strtotime("-1 days"));
		
		// android - fbself 최근 30일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 2";
		$join_count_30day = $db_slave_main->getvalue($sql);
		
// 		$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
// 				"FROM tbl_ad_stats_mobile ".
// 				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 30 DAY), '%Y-%m-%d') < today AND platform = 2 ";
		$sql = " SELECT IFNULL(SUM(spend), 0) ".
		  		" FROM tbl_agency_spend_daily ".
		  		" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 30 DAY), '%Y-%m-%d') < today AND platform = 2 AND (agencyname LIKE 'Face%' OR agencyname LIKE 'm_%') ";
		$spend_30day = $db_slave_main2->getvalue($sql);
		
		// 최근 30일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 2 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate";
		$nogame_30day = $db_slave_main->getvalue($sql);
		
		// 최근 30일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_30day = $db_slave_main->getarray($sql);
		
		$pay_30day = $pay_info_30day["money"];
		$pay_user_cnt_30day = $pay_info_30day["user_cnt"];
		
		// 최근 14일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 2";
		$join_count_14day = $db_slave_main->getvalue($sql);
		
		// 최근 14일 비용
// 		$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
// 				"FROM tbl_ad_stats_mobile ".
// 				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 14 DAY), '%Y-%m-%d') < today AND platform = 2 ";
		$sql = " SELECT IFNULL(SUM(spend), 0) ".
		  		" FROM tbl_agency_spend_daily ".
		  		" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 14 DAY), '%Y-%m-%d') < today AND platform = 2 AND (agencyname LIKE 'Face%' OR agencyname LIKE 'm_%') ";
		$spend_14day = $db_slave_main2->getvalue($sql);
		
		// 최근 14일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 2 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate";
		$nogame_14day = $db_slave_main->getvalue($sql);
		
		// 최근 14일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_14day = $db_slave_main->getarray($sql);
		
		$pay_14day = $pay_info_14day["money"];
		$pay_user_cnt_14day = $pay_info_14day["user_cnt"];
		
		// 최근 7일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 2";
		$join_count_7day = $db_slave_main->getvalue($sql);
		
		// 최근 7일 비용
// 		$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
// 				"FROM tbl_ad_stats_mobile ".
// 				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7 DAY), '%Y-%m-%d') < today AND platform = 2 ";
		$sql = " SELECT IFNULL(SUM(spend), 0) ".
		  		" FROM tbl_agency_spend_daily ".
		  		" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7 DAY), '%Y-%m-%d') < today AND platform = 2 AND (agencyname LIKE 'Face%' OR agencyname LIKE 'm_%') ";
		$spend_7day = $db_slave_main2->getvalue($sql);
		
		// 최근 7일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate";
		$nogame_7day = $db_slave_main->getvalue($sql);
		
		// 최근 7일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_7day = $db_slave_main->getarray($sql);
				
		$pay_7day = $pay_info_7day["money"];
		$pay_user_cnt_7day = $pay_info_7day["user_cnt"];
		
		// 어제 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 2";
		$join_count_yesterday = $db_slave_main->getvalue($sql);
		
		// 어제 비용
// 		$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
// 				"FROM tbl_ad_stats_mobile ".
// 				"WHERE today BETWEEN '$yesterday' AND '$yesterday' AND platform = 2 ";
		$sql = " SELECT IFNULL(SUM(spend), 0) ".
		  		" FROM tbl_agency_spend_daily ".
		  		" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') < today AND platform = 2 AND (agencyname LIKE 'Face%' OR agencyname LIKE 'm_%') ";
		$spend_yesterday = $db_slave_main2->getvalue($sql);
		
		// 어제 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 2 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$nogame_yesterday = $db_slave_main->getvalue($sql);
		
		// 어제 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_yesterday = $db_slave_main->getarray($sql);
				
		$pay_yesterday = $pay_info_yesterday["money"];
		$pay_user_cnt_yesterday = $pay_info_yesterday["user_cnt"];
		
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 201, '$join_count_30day', 'Join(30day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 202, '$spend_30day', 'Spend(30day)', now()) ON DUPLICATE KEY UPDATE value='$spend_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 203, '$nogame_30day', 'No Game(30day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 204, '$pay_30day', 'Pay(30day)', now()) ON DUPLICATE KEY UPDATE value='$pay_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 205, '$pay_user_cnt_30day', 'Pay User Count(30day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 206, '$join_count_14day', 'Join(14day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 207, '$spend_14day', 'Spend(14day)', now()) ON DUPLICATE KEY UPDATE value='$spend_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 208, '$nogame_14day', 'No Game(14day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 209, '$pay_14day', 'Pay(14day)', now()) ON DUPLICATE KEY UPDATE value='$pay_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 210, '$pay_user_cnt_14day', 'Pay User Count(14day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 211, '$join_count_7day', 'Join(7day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 212, '$spend_7day', 'Spend(7day)', now()) ON DUPLICATE KEY UPDATE value='$spend_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 213, '$nogame_7day', 'No Game(7day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 214, '$pay_7day', 'Pay(7day)', now()) ON DUPLICATE KEY UPDATE value='$pay_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 215, '$pay_user_cnt_7day', 'Pay User Count(7day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 216, '$join_count_yesterday', 'Join(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$join_count_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 217, '$spend_yesterday', 'Spend(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$spend_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 218, '$nogame_yesterday', 'No Game(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$nogame_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 219, '$pay_yesterday', 'Pay(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$pay_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 220, '$pay_user_cnt_yesterday', 'Pay User Count(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		$sql = "INSERT INTO tbl_marketing_dash_daily(today, platform, market_type, category, reg_count, spend, nogame_count, pay_user_count, pay_amount) ".
				"VALUES('$today', 2, 0, 1, $join_count_30day, $spend_30day, $nogame_30day, $pay_user_cnt_30day, $pay_30day), ".
				"	('$today', 2, 0, 2, $join_count_14day, $spend_14day, $nogame_14day, $pay_user_cnt_14day, $pay_14day), ".
				"	('$today', 2, 0, 3, $join_count_7day, $spend_7day, $nogame_7day, $pay_user_cnt_7day, $pay_7day), ".
				"	('$today', 2, 0, 4, $join_count_yesterday, $spend_yesterday, $nogame_yesterday, $pay_user_cnt_yesterday, $pay_yesterday)".
				"ON DUPLICATE KEY UPDATE reg_count=VALUES(reg_count), spend=VALUES(spend), nogame_count=VALUES(nogame_count), pay_user_count=VALUES(pay_user_count), pay_amount=VALUES(pay_amount)";
		$db_other->execute($sql);
		
		// android - agency 최근 30일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND platform = 2";
		$join_count_30day = $db_slave_main->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(spend), 0) ".
	    		"FROM tbl_agency_spend_daily ".
				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 30 DAY), '%Y-%m-%d') < today AND platform = 2 AND (agencyname NOT LIKE 'Face%' AND agencyname NOT LIKE '%m_%')";
		$spend_30day = $db_slave_main2->getvalue($sql);
		
		// 최근 30일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND platform = 2 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate";
		$nogame_30day = $db_slave_main->getvalue($sql);
		
		// 최근 30일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_30day = $db_slave_main->getarray($sql);
		
		$pay_30day = $pay_info_30day["money"];
		$pay_user_cnt_30day = $pay_info_30day["user_cnt"];
		
		// 최근 14일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND platform = 2";
		$join_count_14day = $db_slave_main->getvalue($sql);
		
		// 최근 14일 비용
		$sql = "SELECT IFNULL(SUM(spend), 0) ".
	    		"FROM tbl_agency_spend_daily ".
				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 14 DAY), '%Y-%m-%d') < today AND platform = 2 AND (agencyname NOT LIKE 'Face%' AND agencyname NOT LIKE '%m_%')";
		$spend_14day = $db_slave_main2->getvalue($sql);
		
		// 최근 14일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND platform = 2 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate";
		$nogame_14day = $db_slave_main->getvalue($sql);
		
		// 최근 14일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_14day = $db_slave_main->getarray($sql);
		
		$pay_14day = $pay_info_14day["money"];
		$pay_user_cnt_14day = $pay_info_14day["user_cnt"];
		
		// 최근 7일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND platform = 2";
		$join_count_7day = $db_slave_main->getvalue($sql);
		
		// 최근 7일 비용
		$sql = "SELECT IFNULL(SUM(spend), 0) ".
	    		"FROM tbl_agency_spend_daily ".
				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7 DAY), '%Y-%m-%d') < today AND platform = 2 AND (agencyname NOT LIKE 'Face%' AND agencyname NOT LIKE '%m_%')";
		$spend_7day = $db_slave_main2->getvalue($sql);
		
		// 최근 7일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND platform = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate";
		$nogame_7day = $db_slave_main->getvalue($sql);
		
		// 최근 7일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_7day = $db_slave_main->getarray($sql);
		
		$pay_7day = $pay_info_7day["money"];
		$pay_user_cnt_7day = $pay_info_7day["user_cnt"];
		
		// 어제 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND platform = 2";
		$join_count_yesterday = $db_slave_main->getvalue($sql);
		
		// 어제 비용
		$sql = "SELECT IFNULL(SUM(spend), 0) ".
	    		"FROM tbl_agency_spend_daily ".
				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') < today AND platform = 2 AND (agencyname NOT LIKE 'Face%' AND agencyname NOT LIKE '%m_%')";
		$spend_yesterday = $db_slave_main2->getvalue($sql);
		
		// 어제 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE '%_int' OR adflag LIKE '%_int_viral')AND adflag NOT LIKE 'm_d%' AND platform = 2 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$nogame_yesterday = $db_slave_main->getvalue($sql);
		
		// 어제 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 2 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_yesterday = $db_slave_main->getarray($sql);
		
		$pay_yesterday = $pay_info_yesterday["money"];
		$pay_user_cnt_yesterday = $pay_info_yesterday["user_cnt"];
		
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 501, '$join_count_30day', 'Join(30day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 502, '$spend_30day', 'Spend(30day)', now()) ON DUPLICATE KEY UPDATE value='$spend_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 503, '$nogame_30day', 'No Game(30day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 504, '$pay_30day', 'Pay(30day)', now()) ON DUPLICATE KEY UPDATE value='$pay_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 505, '$pay_user_cnt_30day', 'Pay User Count(30day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 506, '$join_count_14day', 'Join(14day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 507, '$spend_14day', 'Spend(14day)', now()) ON DUPLICATE KEY UPDATE value='$spend_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 508, '$nogame_14day', 'No Game(14day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 509, '$pay_14day', 'Pay(14day)', now()) ON DUPLICATE KEY UPDATE value='$pay_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 510, '$pay_user_cnt_14day', 'Pay User Count(14day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 511, '$join_count_7day', 'Join(7day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 512, '$spend_7day', 'Spend(7day)', now()) ON DUPLICATE KEY UPDATE value='$spend_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 513, '$nogame_7day', 'No Game(7day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 514, '$pay_7day', 'Pay(7day)', now()) ON DUPLICATE KEY UPDATE value='$pay_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 515, '$pay_user_cnt_7day', 'Pay User Count(7day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 516, '$join_count_yesterday', 'Join(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$join_count_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 517, '$spend_yesterday', 'Spend(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$spend_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 518, '$nogame_yesterday', 'No Game(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$nogame_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 519, '$pay_yesterday', 'Pay(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$pay_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 520, '$pay_user_cnt_yesterday', 'Pay User Count(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		$sql = "INSERT INTO tbl_marketing_dash_daily(today, platform, market_type, category, reg_count, spend, nogame_count, pay_user_count, pay_amount) ".
				"VALUES('$today', 2, 1, 1, $join_count_30day, $spend_30day, $nogame_30day, $pay_user_cnt_30day, $pay_30day), ".
				"	('$today', 2, 1, 2, $join_count_14day, $spend_14day, $nogame_14day, $pay_user_cnt_14day, $pay_14day), ".
				"	('$today', 2, 1, 3, $join_count_7day, $spend_7day, $nogame_7day, $pay_user_cnt_7day, $pay_7day), ".
				"	('$today', 2, 1, 4, $join_count_yesterday, $spend_yesterday, $nogame_yesterday, $pay_user_cnt_yesterday, $pay_yesterday)".
				"ON DUPLICATE KEY UPDATE reg_count=VALUES(reg_count), spend=VALUES(spend), nogame_count=VALUES(nogame_count), pay_user_count=VALUES(pay_user_count), pay_amount=VALUES(pay_amount)";
		$db_other->execute($sql);
		
		// amazon - agency 최근 30일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') AND platform = 3";
		$join_count_30day = $db_slave_main->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(spend), 0) ".
				"FROM tbl_agency_spend_daily ".
				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 30 DAY), '%Y-%m-%d') < today AND platform = 3";
		$spend_30day = $db_slave_main2->getvalue($sql);
		
		// 최근 30일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') AND platform = 3 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate";
		$nogame_30day = $db_slave_main->getvalue($sql);
		
		// 최근 30일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 3 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 3 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_30day = $db_slave_main->getarray($sql);
		
		$pay_30day = $pay_info_30day["money"];
		$pay_user_cnt_30day = $pay_info_30day["user_cnt"];
		
		// 최근 14일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') AND platform = 3";
		$join_count_14day = $db_slave_main->getvalue($sql);
		
		// 최근 14일 비용
		$sql = "SELECT IFNULL(SUM(spend), 0) ".
				"FROM tbl_agency_spend_daily ".
				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 14 DAY), '%Y-%m-%d') < today AND platform = 3";
		$spend_14day = $db_slave_main2->getvalue($sql);
		
		// 최근 14일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') AND platform = 3 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate";
		$nogame_14day = $db_slave_main->getvalue($sql);
		
		// 최근 14일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 3 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 3 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_14day = $db_slave_main->getarray($sql);
				
		$pay_14day = $pay_info_14day["money"];
		$pay_user_cnt_14day = $pay_info_14day["user_cnt"];
		
		// 최근 7일 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') AND platform = 3";
		$join_count_7day = $db_slave_main->getvalue($sql);
		
		// 최근 7일 비용
		$sql = "SELECT IFNULL(SUM(spend), 0) ".
				"FROM tbl_agency_spend_daily ".
				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7 DAY), '%Y-%m-%d') < today AND platform = 3";
		$spend_7day = $db_slave_main2->getvalue($sql);
		
		// 최근 7일 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') AND platform = 3 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate";
		$nogame_7day = $db_slave_main->getvalue($sql);
		
		// 최근 7일 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 3 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 3 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_7day = $db_slave_main->getarray($sql);
		
		$pay_7day = $pay_info_7day["money"];
		$pay_user_cnt_7day = $pay_info_7day["user_cnt"];
		
		// 어제 가입자 수
		$sql = "SELECT COUNT(useridx) ".
				"FROM tbl_user_ext ".
				"WHERE createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') AND platform = 3";
		$join_count_yesterday = $db_slave_main->getvalue($sql);
		
		// 어제 비용
		$sql = "SELECT IFNULL(SUM(spend), 0) ".
				"FROM tbl_agency_spend_daily ".
				"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') < today AND platform = 3";
		$spend_yesterday = $db_slave_main2->getvalue($sql);
		
		// 어제 미게임수
		$sql = "SELECT IFNULL( ".
				"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS join_nogame_today ".
				"FROM tbl_user_ext ".
				"WHERE (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') AND platform = 3 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$nogame_yesterday = $db_slave_main->getvalue($sql);
		
		// 어제 매출
		$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 3 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') ".
				"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS IN (1, 3) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money ".
				"	FROM ( ".
				"		SELECT useridx ".
				"		FROM tbl_user_ext ".
				"		WHERE platform = 3 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') ".
				"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
				") total";
		$pay_info_yesterday = $db_slave_main->getarray($sql);
		
		$pay_yesterday = $pay_info_yesterday["money"];
		$pay_user_cnt_yesterday = $pay_info_yesterday["user_cnt"];
		
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 301, '$join_count_30day', 'Join(30day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 302, '$spend_30day', 'Spend(30day)', now()) ON DUPLICATE KEY UPDATE value='$spend_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 303, '$nogame_30day', 'No Game(30day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 304, '$pay_30day', 'Pay(30day)', now()) ON DUPLICATE KEY UPDATE value='$pay_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 305, '$pay_user_cnt_30day', 'Pay User Count(30day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_30day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 306, '$join_count_14day', 'Join(14day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 307, '$spend_14day', 'Spend(14day)', now()) ON DUPLICATE KEY UPDATE value='$spend_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 308, '$nogame_14day', 'No Game(14day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 309, '$pay_14day', 'Pay(14day)', now()) ON DUPLICATE KEY UPDATE value='$pay_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 310, '$pay_user_cnt_14day', 'Pay User Count(14day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_14day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 311, '$join_count_7day', 'Join(7day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 312, '$spend_7day', 'Spend(7day)', now()) ON DUPLICATE KEY UPDATE value='$spend_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 313, '$nogame_7day', 'No Game(7day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 314, '$pay_7day', 'Pay(7day)', now()) ON DUPLICATE KEY UPDATE value='$pay_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 315, '$pay_user_cnt_7day', 'Pay User Count(7day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 316, '$join_count_yesterday', 'Join(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$join_count_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 317, '$spend_yesterday', 'Spend(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$spend_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 318, '$nogame_yesterday', 'No Game(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$nogame_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 319, '$pay_yesterday', 'Pay(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$pay_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 320, '$pay_user_cnt_yesterday', 'Pay User Count(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		$sql = "INSERT INTO tbl_marketing_dash_daily(today, platform, market_type, category, reg_count, spend, nogame_count, pay_user_count, pay_amount) ".
				"VALUES('$today', 3, 1, 1, $join_count_30day, $spend_30day, $nogame_30day, $pay_user_cnt_30day, $pay_30day), ".
				"	('$today', 3, 1, 2, $join_count_14day, $spend_14day, $nogame_14day, $pay_user_cnt_14day, $pay_14day), ".
				"	('$today', 3, 1, 3, $join_count_7day, $spend_7day, $nogame_7day, $pay_user_cnt_7day, $pay_7day), ".
				"	('$today', 3, 1, 4, $join_count_yesterday, $spend_yesterday, $nogame_yesterday, $pay_user_cnt_yesterday, $pay_yesterday)".
				"ON DUPLICATE KEY UPDATE reg_count=VALUES(reg_count), spend=VALUES(spend), nogame_count=VALUES(nogame_count), pay_user_count=VALUES(pay_user_count), pay_amount=VALUES(pay_amount)";
		$db_other->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
		
	try
	{
		// 차단된 사용자 자동으로 풀어주는 루틴 (5회 이상 차단 시 48시간 차단, 10회 이상시는 영구차단이라 풀어주지 않음)
		$sql = "UPDATE tbl_user_ext SET isblock=0 WHERE isblock=1 AND NOT EXISTS (SELECT * FROM tbl_user_block WHERE useridx=tbl_user_ext.useridx AND (blockcount>9 OR blockcount>4 AND blockdate>DATE_SUB(NOW(), INTERVAL 47 HOUR) OR blockcount<5 AND blockdate>DATE_SUB(NOW(), INTERVAL 2 HOUR)))";
		$db_main->execute($sql);
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		if (date("H") == "00")
			$today = date("Y-m-d", time() - 60 * 60);		
		else
			$today = date("Y-m-d");
		
		//loyaltypot, treatamount 비결제자 비중
		$sql = "SELECT DATE_FORMAT(logindate, '%Y-%m-%d') AS today, 1 AS typeid, COUNT(tu.useridx) AS total_cnt, SUM(IF(loyal_treat < 0, 1, 0)) AS more_treat ".
				" , SUM(IF(loyal_treat < 0, loyal, 0)) AS more_treat_total_loyal ".
				" , SUM(IF(loyal_treat < 0, treat, 0)) AS more_treat_total_treat ".
				" , SUM(IF(loyal_treat > 0, loyal, 0)) AS more_loyal_total_loyal ".
				" , SUM(IF(loyal_treat > 0, treat, 0)) AS more_loyal_total_treat ".
				"FROM (	".
				"	SELECT tt.useridx, createdate, logindate, IFNULL(loyaltypot, 0) AS loyal, IFNULL(total_amount, 0) AS treat, IFNULL(loyaltypot-total_amount, 0) AS loyal_treat	".
				"	FROM (	".
				"		SELECT t1.useridx, createdate, logindate, loyaltypot	".
				"		FROM tbl_user_ext t1 LEFT JOIN tbl_user_treat t2 ON t1.useridx = t2.useridx	".
				"		WHERE logindate >= '$today 00:00:00' AND logindate <= '$today 23:59:59'	". 
				"	) tt JOIN (	".
				"	SELECT recv_useridx, SUM(treatamount) AS total_amount	".
				"	FROM `tbl_treat_ticket`	".
				"	GROUP BY recv_useridx	".
				"	) ttt ON tt.useridx = ttt.recv_useridx	".
				") tu LEFT JOIN `tbl_user_detail` AS td ON tu.useridx = td.useridx WHERE vip_point = 0 ".
				"GROUP BY DATE_FORMAT(logindate, '%Y-%m-%d');";	
		$nopayer_list = $db_main->getarray($sql);
		
		$nopayer_today = $nopayer_list["today"];
		$nopayer_typeid = 1;
		$nopayer_total_user = $nopayer_list["total_cnt"];
		$more_treat_total_loyal = $nopayer_list["more_treat_total_loyal"];
		$more_treat_total_treat = $nopayer_list["more_treat_total_treat"];
		$more_loyal_total_loyal = $nopayer_list["more_loyal_total_loyal"];
		$more_loyal_total_treat = $nopayer_list["more_loyal_total_treat"];
		$total_have_loyal = $nopayer_list["total_have_loyal"];
		$total_have_treat = $nopayer_list["total_have_treat"];
		$nopayer_more_treat = $nopayer_list["more_treat"];
		$nopayer_more_loyal_treat = $nopayer_total_user-$nopayer_more_treat;
		
		$sql = "INSERT INTO tbl_weight_loyaltypot_treat(today, typeid, total_user, more_loyal_treat, more_treat,more_treat_total_loyal,more_treat_total_treat,more_loyal_total_loyal,more_loyal_total_treat) ". 
				" VALUES('$nopayer_today', 1, $nopayer_total_user, $nopayer_more_treat, $nopayer_more_loyal_treat,$more_treat_total_loyal,$more_treat_total_treat,$more_loyal_total_loyal,$more_loyal_total_treat) ".
				" ON DUPLICATE KEY UPDATE total_user = VALUES(total_user), more_loyal_treat = VALUES(more_loyal_treat), more_treat = VALUES(more_treat) ".
				" ,more_treat_total_loyal= VALUES(more_treat_total_loyal),more_treat_total_treat= VALUES(more_treat_total_treat),more_loyal_total_loyal= VALUES(more_loyal_total_loyal),more_loyal_total_treat= VALUES(more_loyal_total_treat)";
		$db_analysis->execute($sql);		
	
		
		$sql = "SELECT DATE_FORMAT(logindate, '%Y-%m-%d') AS today, 2 AS typeid, COUNT(tu.useridx) AS total_cnt, SUM(IF(loyal_treat < 0, 1, 0)) AS more_treat".
				" , SUM(IF(loyal_treat < 0, loyal, 0)) AS more_treat_total_loyal ".
				" , SUM(IF(loyal_treat < 0, treat, 0)) AS more_treat_total_treat ".
				" , SUM(IF(loyal_treat > 0, loyal, 0)) AS more_loyal_total_loyal ".
				" , SUM(IF(loyal_treat > 0, treat, 0)) AS more_loyal_total_treat ".
				"FROM (	".
				"	SELECT tt.useridx, createdate, logindate, IFNULL(loyaltypot, 0) AS loyal, IFNULL(total_amount, 0) AS treat, IFNULL(loyaltypot-total_amount, 0) AS loyal_treat	".
				"	FROM (	".
				"		SELECT t1.useridx, createdate, logindate, loyaltypot	".
				"		FROM tbl_user_ext t1 LEFT JOIN tbl_user_treat t2 ON t1.useridx = t2.useridx	".
				"		WHERE logindate >= '$today 00:00:00' AND logindate <= '$today 23:59:59'	". 
				"	) tt JOIN (	".
				"	SELECT recv_useridx, SUM(treatamount) AS total_amount	".
				"	FROM `tbl_treat_ticket`	".
				"	GROUP BY recv_useridx	".
				"	) ttt ON tt.useridx = ttt.recv_useridx	".
				") tu LEFT JOIN `tbl_user_detail` AS td ON tu.useridx = td.useridx WHERE vip_point > 0 ".
				"GROUP BY DATE_FORMAT(logindate, '%Y-%m-%d');";	
		
		$payer_list = $db_main->getarray($sql);
		
		$payer_today = $payer_list["today"];
		$payer_typeid = 2;
		$payer_total_user = $payer_list["total_cnt"];
		$more_treat_total_loyal = $payer_list["more_treat_total_loyal"];
		$more_treat_total_treat = $payer_list["more_treat_total_treat"];
		$more_loyal_total_loyal = $payer_list["more_loyal_total_loyal"];
		$more_loyal_total_treat = $payer_list["more_loyal_total_treat"];
		$payer_more_treat = $payer_list["more_treat"];
		$payer_more_loyal_treat = $payer_total_user-$payer_more_treat;
		
		$sql = "INSERT INTO tbl_weight_loyaltypot_treat(today, typeid, total_user, more_loyal_treat, more_treat,more_treat_total_loyal,more_treat_total_treat,more_loyal_total_loyal,more_loyal_total_treat) ". 
				" VALUES('$payer_today', 2, $payer_total_user, $payer_more_loyal_treat, $payer_more_treat,$more_treat_total_loyal,$more_treat_total_treat,$more_loyal_total_loyal,$more_loyal_total_treat) ".
				" ON DUPLICATE KEY UPDATE total_user = VALUES(total_user), more_loyal_treat = VALUES(more_loyal_treat), more_treat = VALUES(more_treat) ".
				" ,more_treat_total_loyal= VALUES(more_treat_total_loyal),more_treat_total_treat= VALUES(more_treat_total_treat),more_loyal_total_loyal= VALUES(more_loyal_total_loyal),more_loyal_total_treat= VALUES(more_loyal_total_treat)";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	try
	{
		// 코인 정합성 목록_new
		$org_today = date('Y-m-d');
	
		$str_useridx = 20000;
	
		for ($i=0; $i<2; $i++)
		{
			$today = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-$i,date("Y", time())));
			$yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-($i+1),date("Y", time())));
	
			$sql = "SELECT IFNULL(SUM(freeamount),0) FROM tbl_user_freecoin_stat WHERE type NOT IN (12) AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
			$sum_free_coin = $db_analysis->getvalue($sql);
				
			$sql = "SELECT (SELECT IFNULL(SUM(coin),0) FROM tbl_user WHERE useridx>$str_useridx) AS total_coin,".
					"(SELECT IFNULL(SUM(amount),0) FROM tbl_jackpot_log WHERE useridx>$str_useridx AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59') AS jackpot_coin,".
					"((SELECT IFNULL(SUM(coin),0) FROM tbl_product_order WHERE status=1 AND useridx>$str_useridx AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59') + ".
					"(SELECT IFNULL(SUM(coin),0) FROM tbl_product_order_mobile WHERE status=1 AND useridx>$str_useridx AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59') + ".
					"(SELECT IFNULL(SUM(coin),0) FROM tbl_product_order_earn WHERE status=1 AND useridx>$str_useridx AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59')) AS order_coin, ".
					"(SELECT IFNULL(COUNT(useridx)*1000000,0) FROM tbl_user WHERE useridx>$str_useridx AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59') AS join_coin_log ";
			$coin_info = $db_main->getarray($sql);
				
			$sql = "SELECT os_type, IFNULL(SUM(coin_change),0) AS unknown_coin ".
					"FROM tbl_unknown_coin ".
					"WHERE useridx>$str_useridx AND category IN (0, 1, 3, 11, 13, 22) AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND os_type IN (0,1,2,3) ".
					"GROUP BY os_type";
				
			$unknown_list = $db_main->gettotallist($sql);
				
			for($j=0; $j<sizeof($unknown_list); $j++)
			{
				$type = $unknown_list[$j]["os_type"];
					
				if($type == "0")
					$web_unknown = $unknown_list[$j]["unknown_coin"];
				else if($type == "1")
					$ios_unknown = $unknown_list[$j]["unknown_coin"];
				else if($type == "2")
					$and_unknown = $unknown_list[$j]["unknown_coin"];
				else if($type == "3")
					$ama_unknown = $unknown_list[$j]["unknown_coin"];
			}
				
			if($web_unknown == "")
				$web_unknown = 0;
					
			if($ios_unknown == "")
				$ios_unknown = 0;
	
			if($and_unknown == "")
				$and_unknown = 0;
							
			if($ama_unknown == "")
				$ama_unknown = 0;
								
			$sql = "SELECT os_type, IFNULL(SUM(coin_change),0) AS unknown_coin_log ".
					"FROM tbl_unknown_coin ".
					"WHERE useridx>$str_useridx AND category IN (2, 4, 12, 14, 15, 24, 25) AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND os_type IN (0,1,2,3) ".
					"GROUP BY os_type";
								
			$unknown_log_list = $db_main->gettotallist($sql);
								
			for($k=0; $k<sizeof($unknown_log_list); $k++)
			{
				$type = $unknown_log_list[$k]["os_type"];
					
				if($type == "0")
					$web_unknown_log = $unknown_log_list[$k]["unknown_coin_log"];
				else if($type == "1")
					$ios_unknown_log = $unknown_log_list[$k]["unknown_coin_log"];
				else if($type == "2")
					$and_unknown_log = $unknown_log_list[$k]["unknown_coin_log"];
				else if($type == "3")
					$ama_unknown_log = $unknown_log_list[$k]["unknown_coin_log"];
			}
	
			if($web_unknown_log == "")
				$web_unknown_log = 0;
					
			if($ios_unknown_log == "")
				$ios_unknown_log = 0;
			
			if($and_unknown_log == "")
				$and_unknown_log = 0;
							
			if($ama_unknown_log == "")
				$ama_unknown_log = 0;
					
			$total_coin = $coin_info["total_coin"];
			$order_coin = $coin_info["order_coin"];
				
			$jackpot_coin = $coin_info["jackpot_coin"];
			$join_coin_log = $coin_info["join_coin_log"];

				
			$game_inout = $db_slave_main2->getvalue("SELECT IFNULL(SUM(moneyout-moneyin-treatamount),0) FROM tbl_game_cash_stats WHERE  mode != 26 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
			$ios_game_inout = $db_slave_main2->getvalue("SELECT IFNULL(SUM(moneyout-moneyin-treatamount),0) FROM tbl_game_cash_stats_ios WHERE  mode != 26 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
			$android_game_inout = $db_slave_main2->getvalue("SELECT IFNULL(SUM(moneyout-moneyin-treatamount),0) FROM tbl_game_cash_stats_android WHERE  mode != 26 AND  writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
			$amazon_game_inout = $db_slave_main2->getvalue("SELECT IFNULL(SUM(moneyout-moneyin-treatamount),0) FROM tbl_game_cash_stats_amazon WHERE  mode != 26 AND  writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
				
			$bankrup_vip = $db_slave_main2->getvalue("SELECT IFNULL(SUM(collect_amount),0) FROM tbl_bankrupt_vip_collect_log WHERE  writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");

			if ($org_today != $today)
				$total_coin = $db_analysis->getvalue("SELECT coin FROM user_coin_log WHERE today='$today' AND type=1 AND subtype=1");

				$yesterday_total_coin = $db_analysis->getvalue("SELECT coin FROM user_coin_log WHERE today='$yesterday' AND type=1 AND subtype=1");
				$sum_order_coin = $order_coin;
				$game_profit = $game_inout + $ios_game_inout + $android_game_inout + $amazon_game_inout + $jackpot_coin;
				$sum_coin = $sum_free_coin + $sum_order_coin + $game_profit + $web_unknown + $ios_unknown + $and_unknown + $ama_unknown + $join_coin_log + $sub_admin_free_coin + $bankrup_vip;
				$change_coin = $total_coin - $yesterday_total_coin;

				$sql = "DELETE FROM user_unknown_coin_new WHERE writedate='$today'; ";
				$sql .= "INSERT INTO user_unknown_coin_new(writedate,free_coin,order_coin,game_profit,join_coin,unknown_coin,ios_unknown_coin,and_unknown_coin,ama_unknown_coin,unknown_coin_log,ios_unknown_coin_log,and_unknown_coin_log,ama_unknown_coin_log,sum_coin,change_coin) ".
						" VALUES('$today',$sum_free_coin,$sum_order_coin,$game_profit,$join_coin_log,$web_unknown,$ios_unknown,$and_unknown,$ama_unknown,$web_unknown_log,$ios_unknown_log,$and_unknown_log,$ama_unknown_log,$sum_coin,$change_coin)";
				$db_analysis->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
	    if(date("H") == "07")
	    {
	        // 기간제 오퍼
	        $org_today = date('Y-m-d');
	        $yesterday =  date("Y-m-d", strtotime($org_today."-1 day"));
	        
	        // 대장자 인원 & 구매인원 & 비구매인원
	        $sql = " SELECT type_detail, COUNT(useridx) AS usercount, SUM(IF(is_buy = 1,1,0)) AS buy_count ".
	   	        " FROM `tbl_subscribe_user_info` ".
	   	        " WHERE type_detail > 0 ".
	   	        " AND target_credit != 0 ".
	   	        " AND  writedate <= '$org_today 07:00:00' AND writedate >= '$yesterday 07:00:00' ".
	   	        " GROUP BY  type_detail";
	        $subscribe_user_stat = $db_main2-> gettotallist($sql);
	        
	        for ($i=0; $i<sizeof($subscribe_user_stat); $i++)
	        {
	            $type_detail = $subscribe_user_stat[$i]['type_detail'];
	            $buy_count = $subscribe_user_stat[$i]['buy_count'];
	            $usercount = $subscribe_user_stat[$i]['usercount'];
	            
	            $sql = "insert into `tbl_subscribe_stat_daily` (`today`, `type`, `usercount`, `buycount`) values('$yesterday','$type_detail','$usercount','$buy_count') ON DUPLICATE KEY UPDATE usercount=VALUES(usercount) , buycount=VALUES(buycount);";
	            $db_main2->execute($sql);
	        }
	        
	        // 보너스 대장자 인원
	        $sql = " SELECT t2.type_detail ,COUNT(t1.useridx) as usercount FROM tbl_subscribe_user_bonus_info t1 ".
	   	        " JOIN tbl_subscribe_user_info t2 ".
	   	        " ON t1.useridx = t2.useridx ".
	   	        " WHERE purchasedate <= NOW() AND NOW() <= expiredate ".
	   	        " GROUP BY t2.type_detail";
	        $subscribe_bonus_user_stat = $db_main2-> gettotallist($sql);
	        
	        for ($i=0; $i<sizeof($subscribe_bonus_user_stat); $i++)
	        {
	            $type_detail = $subscribe_bonus_user_stat[$i]['type_detail'];
	            $usercount = $subscribe_bonus_user_stat[$i]['usercount'];
	            
	            $sql = "insert into `tbl_subscribe_stat_daily` (`today`, `type`, `bonus_usercount`) values('$yesterday','$type_detail','$usercount') ON DUPLICATE KEY UPDATE bonus_usercount=VALUES(bonus_usercount)";
	            $db_main2->execute($sql);
	        }
	        
	        // 보너스 획득 인원
	        $sql = " SELECT  t2.type_detail ,COUNT(t1.useridx) as usercount FROM `tbl_subscribe_user_bonus_log` t1 ".
	   	        " JOIN tbl_subscribe_user_info t2 ".
	   	        " ON t1.useridx = t2.useridx ".
	   	        " WHERE t1.writedate <= '$org_today 07:00:00' AND t1.writedate >= '$yesterday 07:00:00' ".
	   	        " GROUP BY t2.type_detail";
	        $subscribe_getbonus_user_stat = $db_main2-> gettotallist($sql);
	        
	        for ($i=0; $i<sizeof($subscribe_getbonus_user_stat); $i++)
	        {
	            $type_detail = $subscribe_getbonus_user_stat[$i]['type_detail'];
	            $usercount = $subscribe_getbonus_user_stat[$i]['usercount'];
	            
	            $sql = "insert into `tbl_subscribe_stat_daily` (`today`, `type`, `bonus_getusercount`) values('$yesterday','$type_detail','$usercount') ON DUPLICATE KEY UPDATE bonus_getusercount=VALUES(bonus_getusercount)";
	            $db_main2->execute($sql);
	        }
	    }
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	try
	{
	    if (date("H") == "00")
	        $today = date("Y-m-d", time() - 60 * 60);
	    else
	       $today = date("Y-m-d");
	            
        $sql = "SELECT SUM(IF(STATUS = 1,1,0)) AS use_count, SUM(IF(STATUS = 0,1,0)) AS nouse_count FROM tbl_coupon WHERE coupon_more = 100 AND DATE_FORMAT(writedate,'%Y-%m-%d') = '$today';";
        
        $coupon_list = $db_main2->getarray($sql);
        
        $use_count = ($coupon_list["use_count"]=="")?0:$coupon_list["use_count"];
        $nouse_count = ($coupon_list["nouse_count"]=="")?0:$coupon_list["nouse_count"];
        
        $sql = "INSERT INTO `tbl_100coupon_daily`(today, use_count, nouse_count) VALUES('$today',$use_count,$nouse_count) ".
            " ON DUPLICATE KEY UPDATE use_count = VALUES(use_count), nouse_count = VALUES(nouse_count)";
        $db_main2->execute($sql);
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = 0;
	}
	
	try
	{
	    if (date("H") == "01")
	    {
	        write_log("tbl_total_user_freecoin_daily_s");
	        $yesterday = date("Y-m-d", strtotime($today . "-1 day"));
	        $today = date("Y-m-d");
	        
	        $sql = "SELECT category,MAX(freeamount) as maxfreeamount ,COUNT(DISTINCT useridx) AS usercount  FROM (
                            	SELECT useridx, category,IFNULL(SUM(amount), 0) AS freeamount,COUNT(DISTINCT useridx) AS usercount
                            	FROM
                            	(
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_0` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_1` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_2` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_3` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_4` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_5` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_6` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_7` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_8` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_9` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            	) t1 GROUP BY useridx,category
                             ) t2 GROUP BY category ";
	        $total_user_freecoin_category = $db_main2->gettotallist($sql);
	        
	        for ($i = 0; $i < sizeof($total_user_freecoin_category); $i ++) {
	            $category = ($total_user_freecoin_category[$i]["category"] == "") ? 0 : $total_user_freecoin_category[$i]["category"];
	            $usercount = ($total_user_freecoin_category[$i]["usercount"] == "") ? 0 : $total_user_freecoin_category[$i]["usercount"];
	            $maxfreeamount = ($total_user_freecoin_category[$i]["maxfreeamount"] == "") ? 0 : $total_user_freecoin_category[$i]["maxfreeamount"];
	            
	            $sql = "INSERT INTO `tbl_total_user_freecoin_daily`(today, category, usercount, maxfreeamount) VALUES('$yesterday','$category',$usercount,$maxfreeamount) " . " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount), maxfreeamount = VALUES(maxfreeamount)";
	            $db_analysis->execute($sql);
	        }
	        
	        $sql = "SELECT MAX(freeamount) as maxfreeamount,COUNT(DISTINCT useridx) AS usercount  FROM (
                            	SELECT useridx, IFNULL(SUM(amount), 0) AS freeamount,COUNT(DISTINCT useridx) AS usercount
                            	FROM
                            	(
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_0` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_1` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_2` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_3` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_4` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_5` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_6` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_7` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_8` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            		UNION ALL
                            		SELECT writedate, category, TYPE, useridx, amount
                            		FROM `tbl_user_freecoin_log_9` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'
                            	) t1 GROUP BY useridx
                             ) t2";
	        $total_user_freecoin = $db_main2->getarray($sql);
	        
	        $usercount = ($total_user_freecoin["usercount"] == "") ? 0 : $total_user_freecoin["usercount"];
	        $maxfreeamount = ($total_user_freecoin["maxfreeamount"] == "") ? 0 : $total_user_freecoin["maxfreeamount"];
	        
	        $sql = "INSERT INTO `tbl_total_user_freecoin_daily`(today, category, usercount, maxfreeamount) VALUES('$yesterday',4,$usercount,$maxfreeamount) " . " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount), maxfreeamount = VALUES(maxfreeamount)";
	        $db_analysis->execute($sql);
	        
            write_log("tbl_total_user_freecoin_daily_e");
	      }
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = 0;
	}
	
	//어메이징딜 통계
	try
	{
	    if (date("H") == "01")
	    {
	        write_log("tbl_retention_offer_daily_s");
	        $yesterday = date("Y-m-d", strtotime($today . "-1 day"));
	        
	        $sql = "SELECT DATE_FORMAT(DATE_SUB(duedate ,INTERVAL 1 DAY),'%Y-%m-%d') as today ".
	   	        " ,os_type ".
	   	        " , TYPE ".
	   	        " , COUNT(useridx) AS total_usercount ".
	   	        " , SUM(IF(facebookcredit = 50,1,0)) AS group1_5  ".
	   	        " , SUM(IF(TYPE = 1 AND(facebookcredit = 190 OR facebookcredit = 200) ,1,0)) AS group1_19 ".
	   	        " , SUM(IF(TYPE = 2 AND(facebookcredit = 190 OR facebookcredit = 200) ,1,0)) AS group2_19 ".
	   	        " , SUM(IF(facebookcredit = 990 OR facebookcredit = 1000 ,1,0)) AS group2_99 ".
	   	        " FROM `tbl_user_retention_offer` ".
	   	        " WHERE DATE_SUB(duedate ,INTERVAL 1 DAY) >= '$yesterday 00:00:00' AND DATE_SUB(duedate ,INTERVAL 1 DAY) <= '$yesterday 23:59:59' ".
	   	        " GROUP BY os_type, TYPE";
	        $retention_offer_list = $db_main2->gettotallist($sql);
	        
	        for ($i = 0; $i < sizeof($retention_offer_list); $i ++) {
	            $today = $retention_offer_list[$i]["today"];
	            $os_type = $retention_offer_list[$i]["os_type"];
	            $type = $retention_offer_list[$i]["TYPE"];
	            $total_usercount = ($retention_offer_list[$i]["total_usercount"] == "") ? 0 : $retention_offer_list[$i]["total_usercount"];
	            $group1_5 = ($retention_offer_list[$i]["group1_5"] == "") ? 0 : $retention_offer_list[$i]["group1_5"];
	            $group1_19 = ($retention_offer_list[$i]["group1_19"] == "") ? 0 : $retention_offer_list[$i]["group1_19"];
	            $group2_19 = ($retention_offer_list[$i]["group2_19"] == "") ? 0 : $retention_offer_list[$i]["group2_19"];
	            $group2_99 = ($retention_offer_list[$i]["group2_99"] == "") ? 0 : $retention_offer_list[$i]["group2_99"];
	            
	            $sql = " insert into `tbl_retention_offer_daily` (`today`, `type`, `os_type`, `target_usercount`, `offer_pay_count_group1_5`, `offer_pay_count_group1_19`, `offer_pay_count_group2_19`, `offer_pay_count_group2_99`, `non_offer_pay_count`, `non_offer_usercount`, `non_offer_user_paymoney`) ".
	   	            " values('$today','$type','$os_type','$total_usercount','$group1_5','$group1_19','$group2_19','$group2_99','0','0','0') ".
	   	            " ON DUPLICATE KEY UPDATE offer_pay_count_group1_5 = VALUES(offer_pay_count_group1_5), offer_pay_count_group1_19 = VALUES(offer_pay_count_group1_19), offer_pay_count_group2_19 = VALUES(offer_pay_count_group2_19), offer_pay_count_group2_99 = VALUES(offer_pay_count_group2_99);";
	            $db_main2->execute($sql);
	        }
	        if(sizeof($retention_offer_list) > 0)
	        {
	            for($i = 0; $i < 4 ;$i ++)
	            {
	                $sql = "SELECT COUNT(*)
                            FROM `tbl_user_retention_offer`
                            WHERE duedate >= '$yesterday 00:00:00' AND duedate <= '$yesterday 23:59:59'
                            AND buydate != '0000-00-00 00:00:00'
                            AND facebookcredit = 0 AND os_type = $i";
	                $non_offer_pay_count = $db_main2->getvalue($sql);
	                
	                $sql = "UPDATE tbl_retention_offer_daily SET non_offer_pay_count = $non_offer_pay_count  WHERE today='$yesterday' AND os_type = $i;";
	                $db_main2->execute($sql);
	            }
	            
	            $sql = "SELECT os_type, COUNT(DISTINCT useridx) AS non_offer_usercount , SUM(facebookcredit/10) AS non_offer_user_paymoney  
                        FROM tbl_product_order_mobile
                        WHERE product_type = 10 AND writedate >= '$yesterday 00:00:00' AND writedate <= '$yesterday 23:59:59' GROUP BY os_type
                        UNION ALL
                        SELECT 0, COUNT(DISTINCT useridx) AS non_offer_usercount , SUM(facebookcredit/10) AS non_offer_user_paymoney 
                        FROM tbl_product_order WHERE product_type = 10  AND writedate >= '$yesterday 00:00:00' AND writedate <= '$yesterday 23:59:59' ";
	            $non_offer_list = $db_main->gettotallist($sql);
	            for($i=0;$i<sizeof($non_offer_list);$i++)
	            {
	                
	                $non_offer_usercount = ($non_offer_list[$i]["non_offer_usercount"] == "")?0:$non_offer_list[$i]["non_offer_usercount"];
	                $non_offer_user_paymoney = ($non_offer_list[$i]["non_offer_user_paymoney"] == "")?0:$non_offer_list[$i]["non_offer_user_paymoney"];
	                $os_type = $non_offer_list[$i]["os_type"];
    	            
    	            $sql = "UPDATE tbl_retention_offer_daily SET non_offer_usercount = $non_offer_usercount, non_offer_user_paymoney = $non_offer_user_paymoney  WHERE today='$yesterday' AND os_type = $os_type;";
    	            $db_main2->execute($sql);
	            }
	            
	        }
	        write_log("tbl_retention_offer_daily_e");
	      }
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = 0;
	}
	
	//게스트 유저 로그인 통계
	try
	{
	    $today = date("Y-m-d");
	    $sql = " SELECT DATE_FORMAT(t1.logindate,'%Y-%m-%d') AS today ".
	   	    " , t1.platform ".
	   	    " , COUNT(DISTINCT t1.useridx) AS totalcount ".
	   	    " , SUM(IF(DATEDIFF(t1.logindate,t1.createdate) < 7 ,1,0)) AS d ".
	   	    " , SUM(IF(DATEDIFF(t1.logindate,t1.createdate) >= 7 AND DATEDIFF(t1.logindate,t1.createdate) < 14 ,1,0)) AS d_7 ".
	   	    " , SUM(IF(DATEDIFF(t1.logindate,t1.createdate) >= 14 AND DATEDIFF(t1.logindate,t1.createdate) < 28 ,1,0)) AS d_14 ".
	   	    " , SUM(IF(DATEDIFF(t1.logindate,t1.createdate) >= 28 AND DATEDIFF(t1.logindate,t1.createdate) < 60 ,1,0)) AS d_28 ".
	   	    " , SUM(IF(DATEDIFF(t1.logindate,t1.createdate) >= 60 ,1,0)) AS d_60 ".
	   	    " , SUM(IF(t2.vip_point > 0 ,1,0)) AS pay_totalcount ".
	   	    " , SUM(IF(DATEDIFF(t1.logindate,t1.createdate) < 7 AND t2.vip_point > 0 ,1,0)) AS pay_d ".
	   	    " , SUM(IF(DATEDIFF(t1.logindate,t1.createdate) >= 7 AND DATEDIFF(t1.logindate,t1.createdate) < 14  AND t2.vip_point > 0,1,0)) AS pay_d_7 ".
	   	    " , SUM(IF(DATEDIFF(t1.logindate,t1.createdate) >= 14 AND DATEDIFF(t1.logindate,t1.createdate) < 28 AND t2.vip_point > 0,1,0)) AS pay_d_14 ".
	   	    " , SUM(IF(DATEDIFF(t1.logindate,t1.createdate) >= 28 AND DATEDIFF(t1.logindate,t1.createdate) < 60 AND t2.vip_point > 0,1,0)) AS pay_d_28 ".
	   	    " , SUM(IF(DATEDIFF(t1.logindate,t1.createdate) >= 60 AND t2.vip_point > 0 ,1,0)) AS pay_d_60".
	   	    " FROM tbl_user_ext t1 ".
	   	    " JOIN  tbl_user_detail t2 ".
	   	    " ON t1.useridx = t2.useridx ".
	   	    " WHERE isguest = 1 ".
	   	    " AND t1.logindate >= '$today 00:00:00' ".
	   	    " AND t1.logindate <= '$today 23:59:59'".
	   	    " GROUP BY t1.platform";
	    
	    $product_list = $db_main->gettotallist($sql);
	    
	    for($i=0; $i<sizeof($product_list); $i++)
	    {
	        $today = $product_list[$i]["today"];
	        $platform = $product_list[$i]["platform"];
	        $totalcount = $product_list[$i]["totalcount"];
	        $d = $product_list[$i]["d"];
	        $d_7 = $product_list[$i]["d_7"];
	        $d_14 = $product_list[$i]["d_14"];
	        $d_28 = $product_list[$i]["d_28"];
	        $d_60 = $product_list[$i]["d_60"];
	        $pay_totalcount = $product_list[$i]["pay_totalcount"];
	        $pay_d = $product_list[$i]["pay_d"];
	        $pay_d_7 = $product_list[$i]["pay_d_7"];
	        $pay_d_14 = $product_list[$i]["pay_d_14"];
	        $pay_d_28 = $product_list[$i]["pay_d_28"];
	        $pay_d_60 = $product_list[$i]["pay_d_60"];
	        
	        $sql =  " insert into `tbl_guestuser_stat_daily` (`today`, `platform`, `totalcount`, `d`, `d_7`, `d_14`, `d_28`, `d_60`, `pay_totalcount`,`pay_d`, `pay_d_7`, `pay_d_14`, `pay_d_28`, `pay_d_60`) ".
	   	        " values('$today','$platform','$totalcount','$d','$d_7','$d_14','$d_28','$d_60','$pay_totalcount','$pay_d','$pay_d_7','$pay_d_14','$pay_d_28','$pay_d_60')".
	   	        " ON DUPLICATE KEY UPDATE d = VALUES(d), d_7 = VALUES(d_7), d_14 = VALUES(d_14), d_28 = VALUES(d_28), d_60 = VALUES(d_60), pay_totalcount=VALUES(pay_totalcount),pay_d = VALUES(pay_d), pay_d_7 = VALUES(pay_d_7), pay_d_14 = VALUES(pay_d_14), pay_d_28 = VALUES(pay_d_28), pay_d_60 = VALUES(pay_d_60)";
	        
	        $db_main2->execute($sql);
	    }
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = 0;
	}
	
	try
	{
	    $today = date("Y-m-d");
	    
	    $sql = "SELECT COUNT(*) FROM `tbl_user_ext` WHERE platform = 1 AND isguest = 1 AND createdate >= '$today 00:00:00' AND createdate <= '$today 23:59:59'";
	    $todaymobileguestjoincount = $db_main->getvalue($sql);
	    
	    $sql = "SELECT COUNT(*) FROM `tbl_user_ext` WHERE platform = 2 AND isguest = 1 AND createdate >= '$today 00:00:00' AND createdate <= '$today 23:59:59'";
	    $todayandroidguestjoincount = $db_main->getvalue($sql);
	    
	    $sql = "SELECT COUNT(*) FROM `tbl_user_ext` WHERE platform = 3 AND isguest = 1 AND createdate >= '$today 00:00:00' AND createdate <= '$today 23:59:59'";
	    $todayamazonguestjoincount = $db_main->getvalue($sql);
	    
	    $sql = "SELECT COUNT(*) FROM `tbl_user_ext` WHERE platform = 1 AND isguest = 1 AND logindate >= '$today 00:00:00' AND logindate <= '$today 23:59:59'";
	    $todaymobileguestactivecount = $db_main->getvalue($sql);
	    
	    $sql = "SELECT COUNT(*) FROM `tbl_user_ext` WHERE platform = 2 AND isguest = 1 AND logindate >= '$today 00:00:00' AND logindate <= '$today 23:59:59'";
	    $todayandroidguestactivecount = $db_main->getvalue($sql);
	    
	    $sql = "SELECT COUNT(*) FROM `tbl_user_ext` WHERE platform = 3 AND isguest = 1 AND logindate >= '$today 00:00:00' AND logindate <= '$today 23:59:59'";
	    $todayamazonguestactivecount = $db_main->getvalue($sql);
	    
	    $sql = "DELETE FROM user_mobile_guest_join_log WHERE today='$today';".
	   	    "INSERT INTO user_mobile_guest_join_log(today, todaymobilejoincount, todayandroidjoincount, todayamazonjoincount, todaymobileactivecount, todayandroidactivecount, todayamazonactivecount) ".
	   	    "VALUES('$today','$todaymobileguestjoincount','$todayandroidguestjoincount','$todayamazonguestjoincount','$todaymobileguestactivecount','$todayandroidguestactivecount', '$todayamazonguestactivecount')";
	    
	    $db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	//A2U 노티 통계 new
	try
	{
	    if (date("H") == "00")
            $today = date("Y-m-d", time() - 60 * 60);
        else
            $today = date("Y-m-d");
            
        $sql = "SELECT 14 as group_no, COUNT(*) as cnt FROM tbl_coupon_noti_log WHERE status = 1 AND senddate>='$today 00:00:00' AND senddate<='$today 23:59:59';";
        $coupon_a2u_info =  $db_main2->getarray($sql);
        
        $coupon_a2u_group_no = $coupon_a2u_info["group_no"];
        $coupon_a2u_cnt = $coupon_a2u_info["cnt"];
        
        $sql = "INSERT INTO tbl_a2u_sendcount (today, group_no, cnt) VALUES ('$today', $coupon_a2u_group_no, $coupon_a2u_cnt) ".
            "ON DUPLICATE KEY UPDATE cnt=VALUES(cnt)";
        $db_analysis->execute($sql);
        
        $sql = "SELECT 13 AS group_no, COUNT(*) as cnt FROM tbl_user_firstbuy_gift_log WHERE inboxidx > 0 AND senddate>='$today 00:00:00' AND senddate<='$today 23:59:59';";
        $firstbuy_a2u_info =  $db_main2->getarray($sql);
        
        $firstbuy_a2u_group_no = $firstbuy_a2u_info["group_no"];
        $firstbuy_a2u_cnt = $firstbuy_a2u_info["cnt"];
        
        $sql = "INSERT INTO tbl_a2u_sendcount (today, group_no, cnt) VALUES ('$today', $firstbuy_a2u_group_no, $firstbuy_a2u_cnt) ".
            "ON DUPLICATE KEY UPDATE cnt=VALUES(cnt)";
        $db_analysis->execute($sql);
        
        $sql = "SELECT 1 AS group_no, COUNT(*) as cnt FROM tbl_user_newuser_gift_log WHERE senddate>='$today 00:00:00' AND senddate<='$today 23:59:59';";
        $group1_a2u_info =  $db_main2->getarray($sql);
        
        $group1_a2u_group_no = $group1_a2u_info["group_no"];
        $group1_a2u_cnt = $group1_a2u_info["cnt"];
        
        $sql = "INSERT INTO tbl_a2u_sendcount (today, group_no, cnt) VALUES ('$today', $group1_a2u_group_no, $group1_a2u_cnt) ".
            "ON DUPLICATE KEY UPDATE cnt=VALUES(cnt)";
        $db_analysis->execute($sql);
        
        $sql = "SELECT 85 AS group_no, COUNT(*) as cnt FROM tbl_notification_group_log_new WHERE  senddate>='$today 00:00:00' AND senddate<='$today 23:59:59' AND group_no = 13 ;";
        $group13_a2u_info =  $db_main->getarray($sql);
        
        $group13_a2u_group_no = $group13_a2u_info["group_no"];
        $group13_a2u_cnt = $group13_a2u_info["cnt"];
        
        $sql = "INSERT INTO tbl_a2u_sendcount (today, group_no, cnt) VALUES ('$today', $group13_a2u_group_no, $group13_a2u_cnt) ".
            "ON DUPLICATE KEY UPDATE cnt=VALUES(cnt)";
        $db_analysis->execute($sql);
        
        $sql = "SELECT group_no, COUNT(*) AS cnt FROM tbl_notification_group_log_new WHERE senddate>='$today 00:00:00' AND senddate<='$today  23:59:59' AND group_no != 1 AND group_no != 13 GROUP BY group_no;";
        $noti_list = $db_main->gettotallist($sql);
        
        for($i=0; $i<sizeof($noti_list); $i++)
        {
            $group_no = $noti_list[$i]["group_no"];
            $cnt = $noti_list[$i]["cnt"];
            
            $sql = "INSERT INTO tbl_a2u_sendcount (today, group_no, cnt) VALUES ('$today', $group_no, $cnt) ".
                "ON DUPLICATE KEY UPDATE cnt=VALUES(cnt)";
            $db_analysis->execute($sql);
            
        }
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	}
	
	try
	{
		$sql = "UPDATE tbl_scheduler_log SET status = $issuccess, writedate = NOW() WHERE logidx = $logidx;";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();
	$db_slave_main2->end();
	$db_analysis->end();
	$db_friend->end();
	$db_game->end();
	$db_other->end();
	$db_livestats->end();
	$db_slave_livestats->end();
	$db_mobile->end();
	$db_inbox->end();
	$db_slave_main->end();
?>
