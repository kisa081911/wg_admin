<?
	include("../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_other = new CDatabase_Other();
	$db_mobile = new CDatabase_Mobile();
	$db_livestats = new CDatabase_Livestats();
	
	$db_main->execute("SET wait_timeout=72000");
	$db_analysis->execute("SET wait_timeout=72000");
	$db_main2->execute("SET wait_timeout=72000");
	$db_other->execute("SET wait_timeout=72000");
	$db_mobile->execute("SET wait_timeout=72000");
	$db_livestats->execute("SET wait_timeout=72000");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$port = "";
	
	$str_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}

	try
	{
		$sql = "INSERT INTO tbl_scheduler_log(today, type, status, writedate) VALUES('$today', 401, 0, NOW());";
		$db_analysis->execute($sql);
		$sql = "SELECT LAST_INSERT_ID();";
		$logidx = $db_analysis->getvalue($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/daily_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/daily_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("daily_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	try
	{
		// 사용자 유지 현황 - 월별 신규회원수 조회
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, date_format(createdate, '%Y%m') AS regmonth, count(useridx) AS regusercnt FROM tbl_user_ext WHERE useridx > $str_useridx GROUP BY date_format(createdate, '%y-%m')";
		$reguser_list = $db_main->gettotallist($sql);
	
		for ($i=0; $i<sizeof($reguser_list); $i++)
		{
			$userstat_today = $reguser_list[$i]["today"];
			$regmonth = $reguser_list[$i]["regmonth"];
			$regusercnt = $reguser_list[$i]["regusercnt"];
							
			$sql = "INSERT INTO tbl_user_maintainstat_daily(today, regmonth, regusercnt) VALUES('$userstat_today', '$regmonth', $regusercnt) ON DUPLICATE KEY UPDATE regusercnt=$regusercnt";
			$db_analysis->execute($sql);
		}
	
		// 사용자 유지 현황 - 2주 이탈자 수
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, date_format(createdate, '%Y%m') AS regmonth, count(useridx) AS 2weekleavecnt FROM tbl_user_ext WHERE useridx > $str_useridx AND logindate < date_sub(now(), interval 2 week) GROUP BY date_format(createdate, '%y-%m')";
		$twoweekleave_list = $db_main->gettotallist($sql);
	
		for ($i=0; $i<sizeof($twoweekleave_list); $i++)
		{
			$userstat_today = $twoweekleave_list[$i]["today"];
			$regmonth = $twoweekleave_list[$i]["regmonth"];
			$twoweekleavecnt = $twoweekleave_list[$i]["2weekleavecnt"];
			
			$sql = "INSERT INTO tbl_user_maintainstat_daily(today, regmonth, 2weekleavecnt) VALUES('$userstat_today', '$regmonth', $twoweekleavecnt) ON DUPLICATE KEY UPDATE 2weekleavecnt=$twoweekleavecnt";
			$db_analysis->execute($sql);
		}
	
		// 사용자 유지 현황 - 미게임 인원수
		$sql = "SELECT DATE_FORMAT(NOW(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y%m') AS regmonth, ".				
				"	IFNULL(SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"	- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0) AS nogameusercnt ".
				"FROM tbl_user_ext ". 
				"WHERE useridx > $str_useridx GROUP BY DATE_FORMAT(createdate, '%y-%m')";

		$nogameuser_list = $db_main->gettotallist($sql);
	
		for ($i=0; $i<sizeof($nogameuser_list); $i++)
		{
			$userstat_today = $nogameuser_list[$i]["today"];
			$regmonth = $nogameuser_list[$i]["regmonth"];
			$nogameusercnt = $nogameuser_list[$i]["nogameusercnt"];
	
			$sql = "INSERT INTO tbl_user_maintainstat_daily(today, regmonth, nogameusercnt) VALUES('$userstat_today', '$regmonth', $nogameusercnt) ON DUPLICATE KEY UPDATE nogameusercnt=$nogameusercnt";
			$db_analysis->execute($sql);
		}
	
		// 사용자 유지 현황 - 누적결제
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, date_format(createdate, '%Y%m') as regmonth, round(sum(facebookcredit)/10) AS ordersum FROM tbl_product_order a JOIN tbl_user_ext b ON a.useridx = b.useridx where status = 1 AND a.useridx > $str_useridx GROUP BY date_format(createdate, '%y-%m')";
		$ordersum_list = $db_main->gettotallist($sql);
	
		for ($i=0; $i<sizeof($ordersum_list); $i++)
		{
			$userstat_today = $ordersum_list[$i]["today"];
			$regmonth = $ordersum_list[$i]["regmonth"];
			$ordersum = $ordersum_list[$i]["ordersum"];
	
			$sql = "INSERT INTO tbl_user_maintainstat_daily(today, regmonth, ordersum) VALUES('$userstat_today', '$regmonth', $ordersum) ON DUPLICATE KEY UPDATE ordersum=$ordersum";
			$db_analysis->execute($sql);
		}
	
		// 사용자 유지 현황 - 최근2주결제
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, date_format(createdate, '%Y%m') AS regmonth, round(sum(facebookcredit)/10) AS 2weekorder FROM tbl_product_order a JOIN tbl_user_ext b ON a.useridx = b.useridx WHERE status = 1 AND a.useridx > $str_useridx AND writedate >= date_sub(now(), interval 14 day) GROUP BY date_format(createdate, '%y-%m')";
		$twoweekorder_list = $db_main->gettotallist($sql);
	
		for ($i=0; $i<sizeof($twoweekorder_list); $i++)
		{
			$userstat_today = $twoweekorder_list[$i]["today"];
			$regmonth = $twoweekorder_list[$i]["regmonth"];
			$twoweekorder = $twoweekorder_list[$i]["2weekorder"];
	
			$sql = "INSERT INTO tbl_user_maintainstat_daily(today, regmonth, 2weekorder) VALUES('$userstat_today', '$regmonth', $twoweekorder) ON DUPLICATE KEY UPDATE 2weekorder=$twoweekorder";
			$db_analysis->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// 사용자 유지 현황 - 월별 신규회원수 조회(웹)
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS joincount ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"	AND platform NOT IN (1, 2, 3) ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		$reguser_list = $db_main->gettotallist($sql);
	
		for ($i=0; $i<sizeof($reguser_list); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 0;
			$regmonth = $reguser_list[$i]["regmonth"];
			$joincnt = $reguser_list[$i]["joincount"];
							
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount) VALUES('$today', $type, '$regmonth', $joincnt) ON DUPLICATE KEY UPDATE joincount=$joincnt";
			$db_analysis->execute($sql);
		}
	
		// 웹 활동유저수
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS activecnt ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"	AND platform NOT IN (1, 2, 3) ".
				"	AND logindate > date_sub(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK) ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
	
		$webactivelist = $db_main->gettotallist($sql);
	
		for($i=0; $i<sizeof($webactivelist); $i++)
		{
			$today = $webactivelist[$i]["today"];
			$type = 0;
			$regmonth = $webactivelist[$i]["regmonth"];
			$activecnt = $webactivelist[$i]["activecnt"];
	
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, $activecnt, 0, 0, 0, 0) ON DUPLICATE KEY UPDATE activecount=$activecnt";
	
			$db_analysis->execute($sql);
		}
	
		// 웹 가입자 누적 웹 결제
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"		AND platform NOT IN (1, 2, 3) ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
	
		$webtotalcreditlist = $db_main->gettotallist($sql);
	
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $webtotalcreditlist[$i]["today"];
			$type = 0;
			$regmonth = $webtotalcreditlist[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];
	
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, $webcredit, 0, 0, 0) ON DUPLICATE KEY UPDATE webcredit=$webcredit";
	
			$db_analysis->execute($sql);
		}
	
		// 웹 가입자 누적 모바일 결제
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"		AND platform NOT IN (1, 2, 3) ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
		

	
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 0;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
	
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, $mobilecredit, 0, 0) ON DUPLICATE KEY UPDATE mobilecredit=$mobilecredit";
	
			$db_analysis->execute($sql);
		}

		// 웹 가입자 최근2주 웹 결제
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"		AND platform NOT IN (1, 2, 3) ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
	
		$webtotalcreditlist = $db_main->gettotallist($sql);
	
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $webtotalcreditlist[$i]["today"];
			$type = 0;
			$regmonth = $webtotalcreditlist[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];
	
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, $webcredit, 0) ON DUPLICATE KEY UPDATE webcredit2week=$webcredit";
	
			$db_analysis->execute($sql);
		}

		// 웹 가입자 최근2주 모바일 결제
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"		AND platform NOT IN (1, 2, 3) ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
	
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
	
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 0;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
	
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, 0, $mobilecredit) ON DUPLICATE KEY UPDATE mobilecredit2week=$mobilecredit";
	
			$db_analysis->execute($sql);
		}
		
		// 사용자 유지 현황 - 월별 신규회원수 조회(Ios)
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS joincount ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"	AND platform = 1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		$reguser_list = $db_main->gettotallist($sql);
		
		for ($i=0; $i<sizeof($reguser_list); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 1;
			$regmonth = $reguser_list[$i]["regmonth"];
			$joincnt = $reguser_list[$i]["joincount"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount) VALUES('$today', $type, '$regmonth', $joincnt) ON DUPLICATE KEY UPDATE joincount=$joincnt";
			$db_analysis->execute($sql);
		}
		
		// Ios 활동유저수
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS activecnt ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"	AND platform = 1 ".
				"	AND logindate > date_sub(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK) ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webactivelist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webactivelist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 1;
			$regmonth = $reguser_list[$i]["regmonth"];
			$activecnt = $webactivelist[$i]["activecnt"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, $activecnt, 0, 0, 0, 0) ON DUPLICATE KEY UPDATE activecount=$activecnt";
		
			$db_analysis->execute($sql);
		}
		
		// Ios 가입자 누적 웹 결제
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"	AND platform = 1 ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webtotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 1;
			$regmonth = $reguser_list[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, $webcredit, 0, 0, 0) ON DUPLICATE KEY UPDATE webcredit=$webcredit";
		
			$db_analysis->execute($sql);
		}
		
		// Ios 가입자 누적 모바일 결제
		
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"		AND platform = 1 ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 1;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
			
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, $mobilecredit, 0, 0) ON DUPLICATE KEY UPDATE mobilecredit=$mobilecredit";
		
			$db_analysis->execute($sql);
		}
		
		// Ios 가입자 최근2주 웹 결제
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"		AND platform = 1 ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webtotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 1;
			$regmonth = $reguser_list[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];

			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, $webcredit, 0) ON DUPLICATE KEY UPDATE webcredit2week=$webcredit";

			$db_analysis->execute($sql);
		}
		
		// Ios 가입자 최근2주 모바일 결제
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"		AND platform = 1 ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 1;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, 0, $mobilecredit) ON DUPLICATE KEY UPDATE mobilecredit2week=$mobilecredit";

			$db_analysis->execute($sql);
		}

		// 사용자 유지 현황 - 월별 신규회원수 조회(Android)
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS joincount ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"	AND platform = 2 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		$reguser_list = $db_main->gettotallist($sql);
		
		for ($i=0; $i<sizeof($reguser_list); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 2;
			$regmonth = $reguser_list[$i]["regmonth"];
			$joincnt = $reguser_list[$i]["joincount"];
								
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount) VALUES('$today', $type, '$regmonth', $joincnt) ON DUPLICATE KEY UPDATE joincount=$joincnt";
			$db_analysis->execute($sql);
		}
		
		// Android 활동유저수
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS activecnt ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"	AND platform = 2 ".
				"	AND logindate > date_sub(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK) ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webactivelist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webactivelist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 2;
			$regmonth = $reguser_list[$i]["regmonth"];
			$activecnt = $webactivelist[$i]["activecnt"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, $activecnt, 0, 0, 0, 0) ON DUPLICATE KEY UPDATE activecount=$activecnt";
		
			$db_analysis->execute($sql);
		}
		
		// Android 가입자 누적 웹 결제
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"	AND  platform = 2 ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webtotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 2;
			$regmonth = $reguser_list[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, $webcredit, 0, 0, 0) ON DUPLICATE KEY UPDATE webcredit=$webcredit";
		
			$db_analysis->execute($sql);
		}
		
		// Android 가입자 누적 모바일 결제
		
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
						"	WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"		AND  platform = 2 ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 2;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, $mobilecredit, 0, 0) ON DUPLICATE KEY UPDATE mobilecredit=$mobilecredit";
		
			$db_analysis->execute($sql);
		}
		
		// Android 가입자 최근2주 웹 결제
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"		AND  platform = 2 ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webtotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 2;
			$regmonth = $reguser_list[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, $webcredit, 0) ON DUPLICATE KEY UPDATE webcredit2week=$webcredit";
		
			$db_analysis->execute($sql);
		}
		
		// Android 가입자 최근2주 모바일 결제
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"		AND  platform = 2 ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 2;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, 0, $mobilecredit) ON DUPLICATE KEY UPDATE mobilecredit2week=$mobilecredit";
		
			$db_analysis->execute($sql);
		}
		
		// 사용자 유지 현황 - 월별 신규회원수 조회(Amazon)
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS joincount ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"	AND platform = 3 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		$reguser_list = $db_main->gettotallist($sql);
		
		for ($i=0; $i<sizeof($reguser_list); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 3;
			$regmonth = $reguser_list[$i]["regmonth"];
			$joincnt = $reguser_list[$i]["joincount"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount) VALUES('$today', $type, '$regmonth', $joincnt) ON DUPLICATE KEY UPDATE joincount=$joincnt";
			$db_analysis->execute($sql);
		}
		
		// Amazon 활동유저수
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS activecnt ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"	AND platform = 3 ".
				"	AND logindate > date_sub(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK) ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webactivelist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webactivelist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 3;
			$regmonth = $reguser_list[$i]["regmonth"];
			$activecnt = $webactivelist[$i]["activecnt"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, $activecnt, 0, 0, 0, 0) ON DUPLICATE KEY UPDATE activecount=$activecnt";
		
			$db_analysis->execute($sql);
		}
		
		// Amazon 가입자 누적 웹 결제
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"	AND platform = 3 ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webtotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 3;
			$regmonth = $reguser_list[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, $webcredit, 0, 0, 0) ON DUPLICATE KEY UPDATE webcredit=$webcredit";
		
			$db_analysis->execute($sql);
		}
		
		// Amazon 가입자 누적 모바일 결제
		
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"		AND platform = 3 ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 3;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, $mobilecredit, 0, 0) ON DUPLICATE KEY UPDATE mobilecredit=$mobilecredit";
		
			$db_analysis->execute($sql);
		}
		
		// Amazon 가입자 최근2주 웹 결제
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"		AND platform = 3 ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webtotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 3;
			$regmonth = $reguser_list[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, $webcredit, 0) ON DUPLICATE KEY UPDATE webcredit2week=$webcredit";
		
			$db_analysis->execute($sql);
		}
		
		// Amazon 가입자 최근2주 모바일 결제
		$sql = "SELECT date_format(now(), '%Y%m%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE useridx > $str_useridx AND '2015-10-01 00:00:00' < createdate ".
				"		AND platform = 3 ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 3;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, 0, $mobilecredit) ON DUPLICATE KEY UPDATE mobilecredit2week=$mobilecredit";
		
			$db_analysis->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}

// 	try 
// 	{
// 		//가입 일주일내 비결제자 1억 코인 이상 유저		
// 		$sql = "SELECT t1.useridx AS useridx, t1.coin AS coin, t1.createdate AS createdate, t2.adflag AS adflag ".
// 				"FROM	".
// 				"(	".	 
// 				"	SELECT useridx, coin, createdate	". 
// 				"	FROM `tbl_user`	". 
// 				"	WHERE createdate > DATE_SUB(NOW(), INTERVAL 7 DAY) AND coin > 100000000 AND useridx > $str_useridx	".	
// 				") t1 JOIN tbl_user_ext t2	". 
// 				"ON t1.useridx = t2.useridx	".  
// 				"WHERE NOT EXISTS (SELECT * FROM `tbl_product_order` WHERE useridx = t1.useridx AND STATUS = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 7 DAY))";
// 		$user_data = $db_main->gettotallist($sql);
		
// 		$insert_query = "";
// 		$insert_query_list = "";
		
// 		for($i=0; $i<sizeof($user_data); $i++)
// 		{
// 			$useridx = $user_data[$i]['useridx'];
// 			$coin = $user_data[$i]['coin'];
// 			$adflag = $user_data[$i]['adflag'];
// 			$createdate = $user_data[$i]['createdate'];
									
// 			$current_time = date_create(date("Y-m-d H:i:s"));
// 			$createdate_time = date_create($createdate);
									
// 			$diff = date_diff($createdate_time, $current_time);
									
// 			if($coin > 500000000 && ($diff->days > 3))
// 			{
// 				if($insert_query_list == "")
// 					$insert_query_list = "INSERT INTO tbl_coin_increase_user_list(useridx, coin, adflag, createdate, writedate, status, comment) VALUES($useridx, $coin, '$adflag', '$createdate', NOW(), 0, '')";
// 				else
// 					$insert_query_list .= ", ($useridx, $coin, '$adflag', '$createdate', NOW(), 0, '')";
// 			}
		
// 			if($insert_query == "")
// 				$insert_query = "INSERT INTO tbl_coin_increase_user(useridx, coin, adflag, createdate, writedate) VALUES($useridx, $coin, '$adflag', '$createdate', NOW())";
// 			else
// 				$insert_query .= ", ($useridx, $coin, '$adflag', '$createdate', NOW())";
// 		}
		
// 		if($insert_query != "")
// 			$db_analysis->execute($insert_query);
		
// 		if($insert_query_list != "")
// 			$db_analysis->execute($insert_query_list);
		
// 		$sql = "INSERT INTO tbl_coin_increase_user_check_list(useridx, coin, adflag, createdate, writedate, status, comment, commentdate) ".
// 				"(SELECT useridx, coin, adflag, createdate, writedate, status, comment, commentdate FROM tbl_coin_increase_user_list WHERE STATUS = 1)";
// 		$db_analysis->execute($sql);
		
// 		$sql = "DELETE FROM tbl_coin_increase_user_list WHERE STATUS = 1";
// 		$db_analysis->execute($sql);
		
// 	}
// 	catch(Exception $e)
// 	{
// 		write_log($e->getMessage());
// 		$issuccess = "0";
// 	}	
	
// 	try
// 	{
// 		//Unknown Coin 하루 1억이상
		
// 		$sql = "SELECT t1.useridx AS useridx, coin, SUM(coin_change) AS total_unknowncoin, createdate ".
// 				"FROM tbl_unknown_coin t1 INNER JOIN tbl_user t2 ON t1.useridx = t2.useridx	".
// 				"WHERE writedate > DATE_SUB(NOW(), INTERVAL 1 DAY) AND category IN (1, 3, 11, 13) AND t1.useridx > $str_useridx	".
// 				"GROUP BY t1.useridx HAVING total_unknowncoin > 100000000;";
		
// 		$unknown_data = $db_main->gettotallist($sql);
// 		$insert_query = "";
		
// 		for($i=0; $i<sizeof($unknown_data); $i++)
// 		{
// 			$useridx = $unknown_data[$i]['useridx'];
// 			$coin = $unknown_data[$i]['coin'];
// 			$total_unknowncoin = $unknown_data[$i]['total_unknowncoin'];
// 			$createdate = $unknown_data[$i]['createdate'];
									
// 			$sql = "SELECT IFNULL(SUM(coin_change), 0) AS unknown_log FROM tbl_unknown_coin ".
// 					"WHERE writedate > DATE_SUB(NOW(), INTERVAL 1 DAY) AND category IN (2, 4, 12, 14) AND useridx = $useridx";
		
// 			$unknowncoin_log = $db_main->getvalue($sql);
		
// 			$unknowncoin = $total_unknowncoin - $unknowncoin_log;
		
// 			if($insert_query == "")
// 				$insert_query = "INSERT INTO tbl_unknowncoin_daily(useridx, coin, unknown_coin, unknown_coin_log, unknown_coin_total, createdate, writedate) VALUES($useridx, $coin, $unknowncoin, $unknowncoin_log, $total_unknowncoin, '$createdate', NOW())";
// 			else
// 				$insert_query .= ", ($useridx, $coin, $unknowncoin, $unknowncoin_log, $total_unknowncoin, '$createdate', NOW())";
// 		}
		
// 		if($insert_query != "")
// 			$db_analysis->execute($insert_query);
		
// 	}
// 	catch(Exception $e)
// 	{
// 		write_log($e->getMessage());
// 		$issuccess = "0";
// 	}
	
	// 사용자 유지현황(광고)
	try
	{
		// 사용자 유지 현황 - 월별 신규회원수 조회(웹)
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS joincount ".
				"FROM tbl_user_ext ".
				"WHERE '2016-02-01 00:00:00' <= createdate ".
				"	AND (adflag LIKE 'fbself%' OR adflag = 'socialclicks' OR adflag = 'nanigans') ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		$reguser_list = $db_main->gettotallist($sql);
	
		for ($i=0; $i<sizeof($reguser_list); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 0;
			$regmonth = $reguser_list[$i]["regmonth"];
			$joincnt = $reguser_list[$i]["joincount"];
	
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount) VALUES('$today', $type, '$regmonth', $joincnt) ON DUPLICATE KEY UPDATE joincount=$joincnt";
			$db_analysis->execute($sql);
		}
	
		// 웹 활동유저수
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS activecnt ".
				"FROM tbl_user_ext ".
				"WHERE '2016-02-01 00:00:00' <= createdate ".
				"	AND (adflag LIKE 'fbself%' OR adflag = 'socialclicks' OR adflag = 'nanigans') ".
				"	AND logindate > date_sub(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK) ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
	
		$webactivelist = $db_main->gettotallist($sql);
	
		for($i=0; $i<sizeof($webactivelist); $i++)
		{
			$today = $webactivelist[$i]["today"];
			$type = 0;
			$regmonth = $webactivelist[$i]["regmonth"];
			$activecnt = $webactivelist[$i]["activecnt"];
	
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, $activecnt, 0, 0, 0, 0) ON DUPLICATE KEY UPDATE activecount=$activecnt";	
			$db_analysis->execute($sql);
		}
	
		// 웹 가입자 누적 웹 결제
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2016-02-01 00:00:00' <= createdate ".
				"		AND (adflag LIKE 'fbself%' OR adflag = 'socialclicks' OR adflag = 'nanigans') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
	
		$webtotalcreditlist = $db_main->gettotallist($sql);
	
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $webtotalcreditlist[$i]["today"];
			$type = 0;
			$regmonth = $webtotalcreditlist[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];
	
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, $webcredit, 0, 0, 0) ON DUPLICATE KEY UPDATE webcredit=$webcredit";	
			$db_analysis->execute($sql);
		}
	
		// 웹 가입자 누적 모바일 결제
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, SUM(totalcredit) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2015-01-01 00:00:00' <= createdate ".
				"		AND (adflag LIKE 'fbself%' OR adflag = 'socialclicks' OR adflag = 'nanigans') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
	
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
	
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 0;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
	
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, $mobilecredit, 0, 0) ON DUPLICATE KEY UPDATE mobilecredit=$mobilecredit";
	
			$db_analysis->execute($sql);
		}
	
		// 웹 가입자 최근2주 웹 결제
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2016-02-01 00:00:00' <= createdate ".
				"		AND (adflag LIKE 'fbself%' OR adflag = 'socialclicks' OR adflag = 'nanigans') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
	
		$webtotalcreditlist = $db_main->gettotallist($sql);
	
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $webtotalcreditlist[$i]["today"];
			$type = 0;
			$regmonth = $webtotalcreditlist[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];
	
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, $webcredit, 0) ON DUPLICATE KEY UPDATE webcredit2week=$webcredit";
	
			$db_analysis->execute($sql);
		}
	
		// 웹 가입자 최근2주 모바일 결제
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, SUM(totalcredit) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2016-02-01 00:00:00' <= createdate ".
				"		AND (adflag LIKE 'fbself%' OR adflag = 'socialclicks') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
	
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
	
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 0;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
	
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, 0, $mobilecredit) ON DUPLICATE KEY UPDATE mobilecredit2week=$mobilecredit";
	
			$db_analysis->execute($sql);
		}
		
		// 사용자 유지 현황 - 월별 신규회원수 조회(Ios)
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS joincount ".
				"FROM tbl_user_ext ".
				"WHERE '2016-12-06 00:00:00' <= createdate ".
				"	AND platform = 1 AND adflag NOT LIKE 'share%' ".
				"	AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		$reguser_list = $db_main->gettotallist($sql);
		
		for ($i=0; $i<sizeof($reguser_list); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 1;
			$regmonth = $reguser_list[$i]["regmonth"];
			$joincnt = $reguser_list[$i]["joincount"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount) VALUES('$today', $type, '$regmonth', $joincnt) ON DUPLICATE KEY UPDATE joincount=$joincnt";
			$db_analysis->execute($sql);
		}
		
		// Ios 활동유저수
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS activecnt ".
				"FROM tbl_user_ext ".
				"WHERE '2016-12-06 00:00:00' <= createdate ".
				"	AND platform = 1 AND adflag NOT LIKE 'share%' ".
				"	AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				"	AND logindate > date_sub(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK) ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webactivelist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webactivelist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 1;
			$regmonth = $reguser_list[$i]["regmonth"];
			$activecnt = $webactivelist[$i]["activecnt"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, $activecnt, 0, 0, 0, 0) ON DUPLICATE KEY UPDATE activecount=$activecnt";
		
			$db_analysis->execute($sql);
		}
		
		// Ios 가입자 누적 웹 결제
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2016-12-06 00:00:00' <= createdate ".
				"		AND platform = 1 AND adflag NOT LIKE 'share%' ".
				"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webtotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 1;
			$regmonth = $reguser_list[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, $webcredit, 0, 0, 0) ON DUPLICATE KEY UPDATE webcredit=$webcredit";
		
			$db_analysis->execute($sql);
		}
		
		// Ios 가입자 누적 모바일 결제
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2016-12-06 00:00:00' <= createdate ".
				"		AND platform = 1 AND adflag NOT LIKE 'share%' ".
				"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 1;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, $mobilecredit, 0, 0) ON DUPLICATE KEY UPDATE mobilecredit=$mobilecredit";
		
			$db_analysis->execute($sql);
		}
		
		// Ios 가입자 최근2주 웹 결제
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2016-12-06 00:00:00' <= createdate ".
				"		AND platform = 1 AND adflag NOT LIKE 'share%' ".
				"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webtotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 1;
			$regmonth = $reguser_list[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, $webcredit, 0) ON DUPLICATE KEY UPDATE webcredit2week=$webcredit";
		
			$db_analysis->execute($sql);
		}
		
		// Ios 가입자 최근2주 모바일 결제
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2016-12-06 00:00:00' <= createdate ".
				"		AND platform = 1 AND adflag NOT LIKE 'share%' ".
				"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 1;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, 0, $mobilecredit) ON DUPLICATE KEY UPDATE mobilecredit2week=$mobilecredit";
		
			$db_analysis->execute($sql);
		}
		
		// 사용자 유지 현황 - 월별 신규회원수 조회(Android)
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS joincount ".
				"FROM tbl_user_ext ".
				"WHERE '2016-11-15 00:00:00' <= createdate ".
				"	AND platform = 2 AND adflag NOT LIKE 'share%' ".
				"	AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		$reguser_list = $db_main->gettotallist($sql);
		
		for ($i=0; $i<sizeof($reguser_list); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 2;
			$regmonth = $reguser_list[$i]["regmonth"];
			$joincnt = $reguser_list[$i]["joincount"];
								
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount) VALUES('$today', $type, '$regmonth', $joincnt) ON DUPLICATE KEY UPDATE joincount=$joincnt";
			$db_analysis->execute($sql);
		}
		
		// Android 활동유저수
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS activecnt ".
				"FROM tbl_user_ext ".
				"WHERE '2016-11-15 00:00:00' <= createdate ".
				"	AND platform = 2 AND adflag NOT LIKE 'share%' ".
				"	AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				"	AND logindate > date_sub(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK) ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webactivelist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webactivelist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 2;
			$regmonth = $reguser_list[$i]["regmonth"];
			$activecnt = $webactivelist[$i]["activecnt"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, $activecnt, 0, 0, 0, 0) ON DUPLICATE KEY UPDATE activecount=$activecnt";
		
			$db_analysis->execute($sql);
		}
		
		// Android 가입자 누적 웹 결제
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2016-11-15 00:00:00' <= createdate ".
				"		AND platform = 2 AND adflag NOT LIKE 'share%' ".
				"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webtotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 2;
			$regmonth = $reguser_list[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, $webcredit, 0, 0, 0) ON DUPLICATE KEY UPDATE webcredit=$webcredit";
		
			$db_analysis->execute($sql);
		}
		
		// Android 가입자 누적 모바일 결제		
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2016-11-15 00:00:00' <= createdate ".
				"		AND platform = 2 AND adflag NOT LIKE 'share%' ".
				"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 2;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, $mobilecredit, 0, 0) ON DUPLICATE KEY UPDATE mobilecredit=$mobilecredit";
		
			$db_analysis->execute($sql);
		}
		
		// Android 가입자 최근2주 웹 결제
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2016-11-15 00:00:00' <= createdate ".
				"		AND platform = 2 AND adflag NOT LIKE 'share%' ".
				"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webtotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 2;
			$regmonth = $reguser_list[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, $webcredit, 0) ON DUPLICATE KEY UPDATE webcredit2week=$webcredit";
		
			$db_analysis->execute($sql);
		}
		
		// Android 가입자 최근2주 모바일 결제
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2016-11-15 00:00:00' <= createdate ".
				"		AND platform = 2 AND adflag NOT LIKE 'share%' ".
				"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 2;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, 0, $mobilecredit) ON DUPLICATE KEY UPDATE mobilecredit2week=$mobilecredit";
		
			$db_analysis->execute($sql);
		}
		
		// 사용자 유지 현황 - 월별 신규회원수 조회(Amazon)
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS joincount ".
				"FROM tbl_user_ext ".
				"WHERE '2016-12-06 00:00:00' <= createdate ".
				"	AND platform = 3 AND adflag NOT LIKE 'share%' ".
				"	AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		$reguser_list = $db_main->gettotallist($sql);
		
		for ($i=0; $i<sizeof($reguser_list); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 3;
			$regmonth = $reguser_list[$i]["regmonth"];
			$joincnt = $reguser_list[$i]["joincount"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount) VALUES('$today', $type, '$regmonth', $joincnt) ON DUPLICATE KEY UPDATE joincount=$joincnt";
			$db_analysis->execute($sql);
		}
		
		// Amazon 활동유저수
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS activecnt ".
				"FROM tbl_user_ext ".
				"WHERE '2016-12-06 00:00:00' <= createdate ".
				"	AND platform = 3 AND adflag NOT LIKE 'share%' ".
				"	AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				"	AND logindate > date_sub(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK) ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webactivelist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webactivelist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 3;
			$regmonth = $reguser_list[$i]["regmonth"];
			$activecnt = $webactivelist[$i]["activecnt"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, $activecnt, 0, 0, 0, 0) ON DUPLICATE KEY UPDATE activecount=$activecnt";
		
			$db_analysis->execute($sql);
		}
		
		// Amazon 가입자 누적 웹 결제
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2016-12-06 00:00:00' <= createdate ".
				"		AND platform = 3 AND adflag NOT LIKE 'share%' ".
				"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webtotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 3;
			$regmonth = $reguser_list[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, $webcredit, 0, 0, 0) ON DUPLICATE KEY UPDATE webcredit=$webcredit";
		
			$db_analysis->execute($sql);
		}
		
		// Amazon 가입자 누적 모바일 결제
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2016-12-06 00:00:00' <= createdate ".
				"		AND platform = 3 AND adflag NOT LIKE 'share%' ".
				"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 3;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, $mobilecredit, 0, 0) ON DUPLICATE KEY UPDATE mobilecredit=$mobilecredit";
		
			$db_analysis->execute($sql);
		}
		
		// Amazon 가입자 최근2주 웹 결제
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2016-12-06 00:00:00' <= createdate ".
				"		AND platform = 3 AND adflag NOT LIKE 'share%' ".
				"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$webtotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($webtotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 3;
			$regmonth = $reguser_list[$i]["regmonth"];
			$webcredit = $webtotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, $webcredit, 0) ON DUPLICATE KEY UPDATE webcredit2week=$webcredit";
		
			$db_analysis->execute($sql);
		}
		
		// Amazon 가입자 최근2주 모바일 결제
		$sql = "SELECT date_format(now(), '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
				"FROM ( ".
				"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 2 WEEK)), 0 ) AS totalcredit ".
				"	FROM tbl_user_ext ".
				"	WHERE '2016-12-06 00:00:00' <= createdate ".
				"		AND platform = 3 AND adflag NOT LIKE 'share%' ".
				"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
				") t1 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
		
		$mobiletotalcreditlist = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
		{
			$today = $reguser_list[$i]["today"];
			$type = 3;
			$regmonth = $reguser_list[$i]["regmonth"];
			$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
		
			$sql = "INSERT INTO tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
					"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, 0, $mobilecredit) ON DUPLICATE KEY UPDATE mobilecredit2week=$mobilecredit";
		
			$db_analysis->execute($sql);
		}
	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	// Mobile OS Version
	try
	{
		$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	
		$sql = "SELECT os_type, os_version, count(useridx) as count
				FROM 
				(
					SELECT useridx, t1.os_type, os_version, logindate	
					FROM tbl_mobile t1 
					JOIN `tbl_user_mobile_connection_log` t2 
					ON t1.device_id = t2.device_id 
					WHERE  t2.logindate >= '$yesterday 00:00:00' AND t2.logindate <= '$yesterday 23:59:59'
				)t5 GROUP BY os_type, os_version ";
		$mobile_data = $db_mobile->gettotallist($sql);
	
		$insert_sql = "";
	
		for($i=0; $i<sizeof($mobile_data); $i++)
		{
			$os_version = $mobile_data[$i]["os_version"];
			$os_type = $mobile_data[$i]["os_type"];
			$count = $mobile_data[$i]["count"];
	
			if($insert_sql == "")
				$insert_sql = "INSERT INTO mobile_os_version_daily VALUES('$yesterday', '$os_version', $os_type,$count);";
				else
					$insert_sql .= "INSERT INTO mobile_os_version_daily VALUES('$yesterday', '$os_version', $os_type,$count);";
		}
	
		if($insert_sql != "")
			$db_mobile->execute($insert_sql);
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	// isPremium update
	try
	{
			
		$sql= "SELECT useridx FROM tbl_user_premium_booster WHERE end_date < NOW();";
		$useridx_list_arr = $db_main2->gettotallist($sql);
	
		for($i=0; $i<sizeof($useridx_list_arr); $i++)
		{
		    $useridx = $useridx_list_arr[$i]["useridx"];
	
			if($useridx_list == "")
			    $useridx_list .= "$useridx";
			else
			    $useridx_list .= ",$useridx";
		}
	
		$sql = "UPDATE tbl_user_detail SET ispremium = 0 WHERE useridx IN($useridx_list)";
		$db_main->execute($sql);
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	// Mobile Version
	try
	{
		$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	
		$sql = "SELECT os_type, app_version, count(useridx) as count
				FROM 
				(
					SELECT useridx, t1.os_type, app_version, logindate	
					FROM tbl_mobile t1 
					JOIN `tbl_user_mobile_connection_log` t2 
					ON t1.device_id = t2.device_id 
					WHERE  t2.logindate >= '$yesterday 00:00:00' AND t2.logindate <= '$yesterday 23:59:59'
				)t5 GROUP BY os_type, app_version ";
		$mobile_data = $db_mobile->gettotallist($sql);
	
		$insert_sql = "";
	
		for($i=0; $i<sizeof($mobile_data); $i++)
		{
			$app_version = $mobile_data[$i]["app_version"];
			$os_type = $mobile_data[$i]["os_type"];
			$count = $mobile_data[$i]["count"];
	
			if($insert_sql == "")
				$insert_sql = "INSERT INTO mobile_version_daily VALUES('$yesterday', '$app_version', $os_type,$count);";
				else
					$insert_sql .= "INSERT INTO mobile_version_daily VALUES('$yesterday', '$app_version', $os_type,$count);";
		}
	
		if($insert_sql != "")
			$db_mobile->execute($insert_sql);
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	

	
	try
	{
		$sql = "UPDATE tbl_scheduler_log SET status = $issuccess, writedate = NOW() WHERE logidx = $logidx;";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();
	$db_analysis->end();
	$db_other->end();
	$db_livestats->end();
	$db_mobile->end();
?>