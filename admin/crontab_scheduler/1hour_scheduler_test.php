<?
	include("../common/common_include.inc.php");
	include("../common/dbconnect/db_util_redshift.inc.php");
	include("../common/dbconnect/db_util_slave_main_backup.inc.php");
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_friend = new CDatabase_Friend();
	$db_game = new CDatabase_Game();
	$db_other = new CDatabase_Other();
	$db_livestats = new CDatabase_Livestats();
	$db_mobile = new CDatabase_Mobile();
	$db_inbox = new CDatabase_Inbox();
	$db_slave_main = new CDatabase_Slave_Main();
	$db_slave_main_backup = new CDatabase_Slave_Main_Bak();
	$db_redshift = new CDatabase_Redshift();
	
	$db_main->execute("SET wait_timeout=7200");
	$db_main2->execute("SET wait_timeout=7200");
	$db_analysis->execute("SET wait_timeout=7200");
	$db_friend->execute("SET wait_timeout=7200");
	$db_game->execute("SET wait_timeout=7200");
	$db_other->execute("SET wait_timeout=7200");
	$db_livestats->execute("SET wait_timeout=7200");
	$db_mobile->execute("SET wait_timeout=7200");
	$db_inbox->execute("SET wait_timeout=7200");
	$db_slave_main_backup->execute("SET wait_timeout=7200");
	$db_slave_main->execute("SET wait_timeout=7200");
	
	function utf8_bytes($cp)
	{
	    if ($cp > 0x10000){
	        # 4 bytes
	        return	chr(0xF0 | (($cp & 0x1C0000) >> 18)).
	        chr(0x80 | (($cp & 0x3F000) >> 12)).
	        chr(0x80 | (($cp & 0xFC0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else if ($cp > 0x800){
	        # 3 bytes
	        return	chr(0xE0 | (($cp & 0xF000) >> 12)).
	        chr(0x80 | (($cp & 0xFC0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else if ($cp > 0x80){
	        # 2 bytes
	        return	chr(0xC0 | (($cp & 0x7C0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else{
	        # 1 byte
	        return chr($cp);
	    }
	}

	try
	{
	    //hot slot
		$sql=" SELECT slottype ".
	         " FROM `tbl_slot_ranking_info`  ".
	         " WHERE slottype NOT IN (25, 26, 27, 29, 30, 90, 91, 92, 93, 94, 102, 103, 104, 109, 110, 111) ".
             " AND slottype NOT IN(SELECT slottype FROM `tbl_slot_list` WHERE open_date_mobile > DATE_SUB(NOW(),INTERVAL 2 WEEK))".
	         " AND platform = 2 ORDER BY ranking DESC LIMIT 3,3;";
		$mobile_hotslot_list = $db_main2 -> gettotallist($sql);
		$hotslot_str = "";
		for($i=0; $i<sizeof($mobile_hotslot_list); $i++)
		{
		    $slottype = $mobile_hotslot_list[$i]["slottype"];
		    
		    if($hotslot_str == "")
		        $hotslot_str.=$slottype;
		    else
		        $hotslot_str.=",".$slottype;
		}
		
		$sql="UPDATE tbl_new_hot_slot_mobile SET hotslot = '$hotslot_str'";

	    echo ($sql);
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	
	$db_main->end();
	$db_main2->end();
	$db_analysis->end();
	$db_friend->end();
	$db_game->end();
	$db_other->end();
	$db_livestats->end();
	$db_mobile->end();
	$db_inbox->end();
	$db_redshift->end();
	$db_slave_main_backup->end();	
	$db_slave_main->end();	
?>
