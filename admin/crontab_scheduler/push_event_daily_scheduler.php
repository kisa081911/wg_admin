<?
	include("../common/common_include.inc.php");

	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	
	$db_analysis->execute("SET wait_timeout=72000");
	$db_main2->execute("SET wait_timeout=72000");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$port = "";
	
	$str_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}

	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/push_event_daily_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/push_event_daily_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("push_event_daily_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	
	try
	{
		//Unknown Coin 하루 1억이상
		
		$sql = " SELECT t1.eventidx ".
						" , SUM(IF(t1.reward_amount > 0,1,0)) AS amount_push_count ".
						" , SUM(t1.reward_amount) AS reward_amount ".
						" , SUM(IF(t1.reward_type = 3 AND t1.reward_amount = 0, 1, 0)) AS nonreward_count ". 
						" , t3.send_count AS send_count ".
						" , COUNT(t1.useridx) AS reserve_count ".
				 " FROM tbl_push_event_result t1 ".
				 " JOIN tbl_push_event t2 ".
				 " ON t1.eventidx = t2.eventidx ".
				 " JOIN tbl_push_event_stat t3 ".
				 " ON t2.push_code= t3.push_code ".
				 " GROUP BY  eventidx"; 
		
		$push_event_list = $db_main2->gettotallist($sql);
		$insert_query ="";
		
		for($i=0; $i<sizeof($push_event_list); $i++)
		{
			$eventidx = $push_event_list[$i]['eventidx'];
			$send_count = $push_event_list[$i]['send_count'];
			$reserve_count = $push_event_list[$i]['reserve_count'];
			$reward_amount = $push_event_list[$i]['reward_amount'];
			$amount_push_count = $push_event_list[$i]['amount_push_count'];
			$nonreward_count = $push_event_list[$i]['nonreward_count'];
									
		
			if($insert_query == "")
				$insert_query = "INSERT INTO tbl_push_event_stat_daily(eventidx, send_count, reserve_count, reward_amount, amount_push_count, nonreward_count) ". 
									" VALUES($eventidx,  $send_count, $reserve_count, $reward_amount, $amount_push_count, $nonreward_count)";
			else
				$insert_query .= ", ($eventidx, $send_count, $reserve_count, $reward_amount, $amount_push_count, $nonreward_count)";
		}
		
		if($insert_query != "")
		{
			$insert_query .= "ON DUPLICATE KEY UPDATE send_count = VALUES(send_count), reserve_count = VALUES(reserve_count), reward_amount = VALUES(reward_amount), amount_push_count = VALUES(amount_push_count), nonreward_count = VALUES(nonreward_count);";
			$db_analysis->execute($insert_query);
		}
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	$db_main2->end();
	$db_analysis->end();

	?>