<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/dbconnect/db_util_redshift.inc.php");
    include("../common/common_include.inc.php");
    
    $slave_main = new CDatabase_Slave_Main();
    $db_analysis = new CDatabase_Analysis();
    $db_redshift = new CDatabase_Redshift();
    $db_other = new CDatabase_Other();
    $db_main2 = new CDatabase_Slave_Main2();
		
	$slave_main->execute("SET wait_timeout=3600");
    $db_analysis->execute("SET wait_timeout=3600");
    $db_other->execute("SET wait_timeout=3600");
    $db_main2->execute("SET wait_timeout=3600");
	
	ini_set("memory_limit", "-1");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/redshift_dashboard_scheduler.php") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/redshift_dashboard_scheduler.php") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("Take5 redshift_dashboard_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}

	try
	{
		$standard_date = date("Y-m-d H:i").":00";
		$standard_shortdate = date("Y-m-d");
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		// Dash Board 결제
		// 전체
		$today = date("Y-m-d");
		$yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));

		// 최근 28일 결제 금액
		$sql = "SELECT SUM(total_money) AS total_money ".
				"FROM ( ".
				"	SELECT ROUND(IFNULL(SUM(facebookcredit/10),0), 2) AS total_money ".
				"	FROM tbl_product_order ".
				"	WHERE STATUS = 1 AND useridx > 20000 AND DATE_SUB('$today', INTERVAL 28 DAY) < writedate AND writedate < '$yesterday 00:00:00' ".
				"   UNION ALL ".
				"	SELECT ROUND(IFNULL(SUM(money),0), 2) AS total_money ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE STATUS = 1 AND useridx > 20000 AND DATE_SUB('$today', INTERVAL 28 DAY) < writedate AND writedate < '$yesterday 00:00:00' ".
				") t1";
		$mysql_order_28day = $slave_main->getvalue($sql);
		
		// 최근 28일 결제 금액(redshift)
		$sql = "SELECT SUM(total_money) AS total_money ".
		  		"FROM ( ".
		  		"	SELECT ROUND(NVL(SUM(facebookcredit/10::real),0), 2) AS total_money ".
		  		"	FROM t5_product_order ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND DATEADD(DAY,(-28),'$today') < writedate AND writedate < '$yesterday 00:00:00' ".
		  		"   UNION ALL ".
		  		"	SELECT ROUND(NVL(SUM(money::real),0), 2) AS total_money ".
		  		"	FROM t5_product_order_mobile ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND DATEADD(DAY,(-28),'$today') < writedate AND writedate < '$yesterday 00:00:00' ".
		  		") t1";
		$redshift_order_28day = $db_redshift->getvalue($sql);
		
		// 최근 28일 결제 취소 금액
		$sql = "SELECT SUM(cancel_money) AS cancel_money ".
		  		"FROM ( ".
		  		"	SELECT ROUND(IFNULL(SUM(facebookcredit/10),0), 2) AS cancel_money ".
		  		"	FROM tbl_product_order ".
		  		"	WHERE STATUS = 2 AND useridx > 20000 AND DATE_SUB('$today', INTERVAL 28 DAY) < canceldate AND canceldate < '$yesterday 00:00:00' AND canceldate IS NOT NULL ".
		  		"   UNION ALL ".
		  		"	SELECT ROUND(IFNULL(SUM(money),0), 2) AS cancel_money ".
		  		"	FROM tbl_product_order_mobile ".
		  		"	WHERE STATUS = 2 AND useridx > 20000 AND DATE_SUB('$today', INTERVAL 28 DAY) < canceldate AND canceldate < '$yesterday 00:00:00' AND canceldate IS NOT NULL ".
		  		") t1";
		$mysql_order_cancel_28day = $slave_main->getvalue($sql);
		
		// 최근 28일 결제 취소 금액(redshift)
		$sql = "SELECT SUM(cancel_money) AS cancel_money ".
		  		"FROM ( ".
		  		"	SELECT ROUND(NVL(SUM(facebookcredit/10::real),0), 2) AS cancel_money ".
		  		"	FROM t5_product_order ".
		  		"	WHERE STATUS = 2 AND useridx > 20000 AND DATEADD(DAY,(-28),'$today') < canceldate AND canceldate < '$yesterday 00:00:00' AND canceldate IS NOT NULL ".
		  		"   UNION ALL ".
		  		"	SELECT ROUND(NVL(SUM(money::real),0), 2) AS cancel_money ".
		  		"	FROM t5_product_order_mobile ".
		  		"	WHERE STATUS = 2 AND useridx > 20000 AND DATEADD(DAY,(-28),'$today') < canceldate AND canceldate < '$yesterday 00:00:00' AND canceldate IS NOT NULL ".
		  		") t1";
		$redshift_order_cancel_28day = $db_redshift->getvalue($sql);
		
		// 최근 28일 결제 건수
		$sql = "SELECT SUM(total_count) AS total_count ".
				"FROM ( ".
				"	SELECT COUNT(*) AS total_count ".
				"	FROM tbl_product_order ".
				"	WHERE STATUS = 1 AND useridx > 20000 AND DATE_SUB('$today', INTERVAL 28 DAY) < writedate AND writedate < '$yesterday 00:00:00' ".
				"   UNION ALL ".
				"	SELECT COUNT(*) AS total_count ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE STATUS = 1 AND useridx > 20000 AND DATE_SUB('$today', INTERVAL 28 DAY) < writedate AND writedate < '$yesterday 00:00:00' ".
				") t1";
		$mysql_count_28day = $slave_main->getvalue($sql);
		
		// 최근 28일 결제 건수(redshift)
		$sql = "SELECT SUM(total_count) AS total_count ".
		  		"FROM ( ".
		  		"	SELECT COUNT(*) AS total_count ".
		  		"	FROM t5_product_order ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND DATEADD(DAY,(-28),'$today') < writedate AND writedate < '$yesterday 00:00:00' ".
		  		"   UNION ALL ".
		  		"	SELECT COUNT(*) AS total_count ".
		  		"	FROM t5_product_order_mobile ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND DATEADD(DAY,(-28),'$today') < writedate AND writedate < '$yesterday 00:00:00' ".
		  		") t1";
		$redshift_count_28day = $db_redshift->getvalue($sql);
		
		// 최근 7일 결제 금액
		$sql = "SELECT SUM(total_money) AS total_money ".
		  		"FROM ( ".
		  		"	SELECT ROUND(IFNULL(SUM(facebookcredit/10),0), 2) AS total_money ".
		  		"	FROM tbl_product_order ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND DATE_SUB('$today', INTERVAL 7 DAY) < writedate AND writedate < '$yesterday 00:00:00' ".
		  		"   UNION ALL ".
		  		"	SELECT ROUND(IFNULL(SUM(money),0), 2) AS total_money ".
		  		"	FROM tbl_product_order_mobile ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND DATE_SUB('$today', INTERVAL 7 DAY) < writedate AND writedate < '$yesterday 00:00:00' ".
		  		") t1";
		$mysql_order_7day = $slave_main->getvalue($sql);
		
		// 최근 7일 결제 금액(redshift)
		$sql = "SELECT SUM(total_money) AS total_money ".
		  		"FROM ( ".
		  		"	SELECT ROUND(NVL(SUM(facebookcredit/10::real),0), 2) AS total_money ".
		  		"	FROM t5_product_order ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND DATEADD(DAY,(-7),'$today') < writedate AND writedate < '$yesterday 00:00:00' ".
		  		"   UNION ALL ".
		  		"	SELECT ROUND(NVL(SUM(money::real),0), 2) AS total_money ".
		  		"	FROM t5_product_order_mobile ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND DATEADD(DAY,(-7),'$today') < writedate AND writedate < '$yesterday 00:00:00' ".
		  		") t1";
		$redshift_order_7day = $db_redshift->getvalue($sql);
		
		// 최근 7일 결제 취소 금액
		$sql = "SELECT SUM(cancel_money) AS cancel_money ".
		  		"FROM ( ".
		  		"	SELECT ROUND(IFNULL(SUM(facebookcredit/10),0), 2) AS cancel_money ".
		  		"	FROM tbl_product_order ".
		  		"	WHERE STATUS = 2 AND useridx > 20000 AND DATE_SUB('$today', INTERVAL 7 DAY) < canceldate AND canceldate < '$yesterday 00:00:00' AND canceldate IS NOT NULL ".
		  		"   UNION ALL ".
		  		"	SELECT ROUND(IFNULL(SUM(money),0), 2) AS cancel_money ".
		  		"	FROM tbl_product_order_mobile ".
		  		"	WHERE STATUS = 2 AND useridx > 20000 AND DATE_SUB('$today', INTERVAL 7 DAY) < canceldate AND canceldate < '$yesterday 00:00:00' AND canceldate IS NOT NULL ".
		  		") t1";
		$mysql_order_cancel_7day = $slave_main->getvalue($sql);
		
		// 최근 7일 결제 취소 금액(redshift)
		$sql = "SELECT SUM(cancel_money) AS cancel_money ".
		  		"FROM ( ".
		  		"	SELECT ROUND(NVL(SUM(facebookcredit/10::real),0), 2) AS cancel_money ".
		  		"	FROM t5_product_order ".
		  		"	WHERE STATUS = 2 AND useridx > 20000 AND DATEADD(DAY,(-7),'$today') < canceldate AND canceldate < '$yesterday 00:00:00' AND canceldate IS NOT NULL ".
		  		"   UNION ALL ".
		  		"	SELECT ROUND(NVL(SUM(money::real),0), 2) AS cancel_money ".
		  		"	FROM t5_product_order_mobile ".
		  		"	WHERE STATUS = 2 AND useridx > 20000 AND DATEADD(DAY,(-7),'$today') < canceldate AND canceldate < '$yesterday 00:00:00' AND canceldate IS NOT NULL ".
		  		") t1";
		$redshift_order_cancel_7day = $db_redshift->getvalue($sql);
		
		// 최근 7일 결제 건수
		$sql = "SELECT SUM(total_count) AS total_count ".
		  		"FROM ( ".
		  		"	SELECT COUNT(*) AS total_count ".
		  		"	FROM tbl_product_order ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND DATE_SUB('$today', INTERVAL 7 DAY) < writedate AND writedate < '$yesterday 00:00:00' ".
		  		"   UNION ALL ".
		  		"	SELECT COUNT(*) AS total_count ".
		  		"	FROM tbl_product_order_mobile ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND DATE_SUB('$today', INTERVAL 7 DAY) < writedate AND writedate < '$yesterday 00:00:00' ".
		  		") t1";
		$mysql_count_7day = $slave_main->getvalue($sql);
		
		// 최근 7일 결제 건수(redshift)
		$sql = "SELECT SUM(total_count) AS total_count ".
		  		"FROM ( ".
		  		"	SELECT COUNT(*) AS total_count ".
		  		"	FROM t5_product_order ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND DATEADD(DAY,(-7),'$today') < writedate AND writedate < '$yesterday 00:00:00' ".
		  		"   UNION ALL ".
		  		"	SELECT COUNT(*) AS total_count ".
		  		"	FROM t5_product_order_mobile ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND DATEADD(DAY,(-7),'$today') < writedate AND writedate < '$yesterday 00:00:00' ".
		  		") t1";
		$redshift_count_7day = $db_redshift->getvalue($sql);
		
		// 어제 결제 금액
		$sql = "SELECT SUM(total_money) AS total_money ".
				"FROM ( ".
				"	SELECT ROUND(IFNULL(SUM(facebookcredit/10),0), 2) AS total_money ".
				"	FROM tbl_product_order ".
				"	WHERE STATUS = 1 AND useridx > 20000 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
				"   UNION ALL ".
				"	SELECT ROUND(IFNULL(SUM(money),0), 2) AS total_money ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE STATUS = 1 AND useridx > 20000 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
				") t1";
		$mysql_order_yesterday = $slave_main->getvalue($sql);
		
		//어제 결제 금액(redshift)
		$sql = "SELECT SUM(total_money) AS total_money ".
		  		"FROM ( ".
		  		"	SELECT ROUND(NVL(SUM(facebookcredit/10::real),0), 2) AS total_money ".
		  		"	FROM t5_product_order ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		"   UNION ALL ".
		  		"	SELECT ROUND(NVL(SUM(money::real),0), 2) AS total_money ".
		  		"	FROM t5_product_order_mobile ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		") t1";
		$redshift_order_yesterday = $db_redshift->getvalue($sql);
		
		// 어제 결제 취소 금액
		$sql = "SELECT SUM(cancel_money) AS cancel_money ".
		  		"FROM ( ".
		  		"	SELECT ROUND(IFNULL(SUM(facebookcredit/10),0), 2) AS cancel_money ".
		  		"	FROM tbl_product_order ".
		  		"	WHERE STATUS = 2 AND useridx > 20000 AND canceldate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND canceldate IS NOT NULL ".
		  		"   UNION ALL ".
		  		"	SELECT ROUND(IFNULL(SUM(money),0), 2) AS cancel_money ".
		  		"	FROM tbl_product_order_mobile ".
		  		"	WHERE STATUS = 2 AND useridx > 20000 AND canceldate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND canceldate IS NOT NULL ".
		  		") t1";
		$mysql_order_cancel_yesterday = $slave_main->getvalue($sql);
		
		// 어제 결제 취소 금액(redshift)
		$sql = "SELECT SUM(cancel_money) AS cancel_money ".
		  		"FROM ( ".
		  		"	SELECT ROUND(NVL(SUM(facebookcredit/10::real),0), 2) AS cancel_money ".
		  		"	FROM t5_product_order ".
		  		"	WHERE STATUS = 2 AND useridx > 20000 AND canceldate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND canceldate IS NOT NULL ".
		  		"   UNION ALL ".
		  		"	SELECT ROUND(NVL(SUM(money::real),0), 2) AS cancel_money ".
		  		"	FROM t5_product_order_mobile ".
		  		"	WHERE STATUS = 2 AND useridx > 20000 AND canceldate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND canceldate IS NOT NULL ".
		  		") t1";
		$redshift_order_cancel_yesterday = $db_redshift->getvalue($sql);
		
		// 어제 결제 건수
		$sql = "SELECT SUM(total_count) AS total_count ".
				"FROM ( ".
				"	SELECT COUNT(*) AS total_count ".
				"	FROM tbl_product_order ".
				"	WHERE STATUS = 1 AND useridx > 20000 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
				"   UNION ALL ".
				"	SELECT COUNT(*) AS total_count ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE STATUS = 1 AND useridx > 20000 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
				") t1";
		$mysql_count_yesterday = $slave_main->getvalue($sql);
		
		// 어제 결제 건수(redshift)
		$sql = "SELECT SUM(total_count) AS total_count ".
		  		"FROM ( ".
		  		"	SELECT COUNT(*) AS total_count ".
		  		"	FROM t5_product_order ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		"   UNION ALL ".
		  		"	SELECT COUNT(*) AS total_count ".
		  		"	FROM t5_product_order_mobile ".
		  		"	WHERE STATUS = 1 AND useridx > 20000 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		") t1";
		$redshift_count_yesterday = $db_redshift->getvalue($sql);
		
		$sql = "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(100, 1, 1, '$mysql_order_28day', '$redshift_order_28day', 'Pay(order_28day)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_order_28day', redshift_value='$redshift_order_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(100, 1, 2, '$mysql_order_cancel_28day', '$redshift_order_cancel_28day', 'Pay(order_cancel_28day)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_order_cancel_28day', redshift_value='$redshift_order_cancel_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(100, 1, 3, '$mysql_count_28day', '$redshift_count_28day', 'Pay(count_28day)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_count_28day', redshift_value='$redshift_count_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(100, 1, 4, '$mysql_order_7day', '$redshift_order_7day', 'Pay(order_7day)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_order_7day', redshift_value='$redshift_order_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(100, 1, 5, '$mysql_order_cancel_7day', '$redshift_order_cancel_7day', 'Pay(order_cancel_7day)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_order_cancel_7day', redshift_value='$redshift_order_cancel_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(100, 1, 6, '$mysql_count_7day', '$redshift_count_7day', 'Pay(count_7day)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_count_7day', redshift_value='$redshift_count_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(100, 1, 7, '$mysql_order_yesterday', '$redshift_order_yesterday', 'Pay(order_yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_order_yesterday', redshift_value='$redshift_order_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(100, 1, 8, '$mysql_order_cancel_yesterday', '$redshift_order_cancel_yesterday', 'Pay(order_cancel_yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_order_cancel_yesterday', redshift_value='$redshift_order_cancel_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(100, 1, 9, '$mysql_count_yesterday', '$redshift_count_yesterday', 'Pay(count_yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_count_yesterday', redshift_value='$redshift_count_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		
		// 회원가입 전체
		// 가입 인원수
		//최근 28일 
		$sql = "SELECT COUNT(*) AS join_28day FROM tbl_user t1 JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx WHERE t1.useridx > 20000 AND t1.createdate >= DATE_SUB('$today', INTERVAL 28 DAY) AND t1.createdate < '$today 00:00:00'";
		$mysql_join_28days = $slave_main->getvalue($sql);
		
		//최근 28일(redshift)
		$sql = "SELECT COUNT(*) AS join_28day FROM t5_user WHERE createdate >= DATEADD(DAY,(-28),'$today') AND createdate < '$today 00:00:00' ";
		$redshift_join_28days = $db_redshift->getvalue($sql);

		//최근 7일
		$sql = "SELECT COUNT(*) AS join_7day FROM tbl_user t1 JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx WHERE t1.useridx > 20000 AND t1.createdate >= DATE_SUB('$today', INTERVAL 7 DAY) AND t1.createdate < '$today 00:00:00' ";
		$mysql_join_7days = $slave_main->getvalue($sql);
		
		//최근 7일(redshift)
		$sql = "SELECT COUNT(*) AS join_7day FROM t5_user WHERE createdate >= DATEADD(DAY,(-7),'$today') AND createdate < '$today 00:00:00' ";
		$redshift_join_7days = $db_redshift->getvalue($sql);
		
		//어제
		$sql = "SELECT COUNT(*) AS join_yesterday FROM tbl_user t1 JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx WHERE t1.useridx > 20000 AND t1.createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$mysql_join_yesterday = $slave_main->getvalue($sql);
		
		//어제(redshift)
		$sql = "SELECT COUNT(*) AS join_yesterday FROM t5_user WHERE createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$redshift_join_yesterday = $db_redshift->getvalue($sql);

		$sql = "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(100, 4, 1, '$mysql_join_28days', '$redshift_join_28days', 'Join(28days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_join_28days', redshift_value='$redshift_join_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(100, 4, 2, '$mysql_join_7days', '$redshift_join_7days', 'Join(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_join_7days', redshift_value='$redshift_join_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(100, 4, 3, '$mysql_join_yesterday', '$redshift_join_yesterday', 'Join(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_join_yesterday', redshift_value='$redshift_join_yesterday', writedate=now();";
		$db_analysis->execute($sql);

	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
	    //최근 7일(tbl_user_gamelog)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(moneyin) AS sum_in_7day, SUM(moneyout) AS sum_out_7day FROM tbl_user_gamelog WHERE writedate >= DATE_SUB('$today', INTERVAL 7 DAY) AND writedate < '$yesterday 00:00:00'";
	    $mysql_gamelog_7days_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_cnt_7days = $mysql_gamelog_7days_data["count_7day"];
	    $mysql_gamelog_moneyin_7days = $mysql_gamelog_7days_data["sum_in_7day"];
	    $mysql_gamelog_moneyout_7days = $mysql_gamelog_7days_data["sum_out_7day"];
	    
	    //최근 7일(redshift)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(money_in) AS sum_in_7day, SUM(money_out) AS sum_out_7day FROM t5_user_gamelog WHERE writedate >= DATEADD(DAY,(-7),'$today') AND writedate < '$yesterday 00:00:00'";
	    $redshift_gamelog_7days_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_cnt_7days = $redshift_gamelog_7days_data["count_7day"];
	    $redshift_gamelog_moneyin_7days = $redshift_gamelog_7days_data["sum_in_7day"];
	    $redshift_gamelog_moneyout_7days = $redshift_gamelog_7days_data["sum_out_7day"];
	    
	    //어제
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(moneyin) AS sum_in_yesterday, SUM(moneyout) AS sum_out_yesterday FROM tbl_user_gamelog WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $mysql_gamelog_yesterday_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_cnt_yesterday = $mysql_gamelog_yesterday_data["count_yesterday"];
	    $mysql_gamelog_moneyin_yesterday = $mysql_gamelog_yesterday_data["sum_in_yesterday"];
	    $mysql_gamelog_moneyout_yesterday = $mysql_gamelog_yesterday_data["sum_out_yesterday"];
	    
	    //어제(redshift)
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(money_in) AS sum_in_yesterday, SUM(money_out) AS sum_out_yesterday FROM t5_user_gamelog WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $redshift_gamelog_yesterday_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_cnt_yesterday = $redshift_gamelog_yesterday_data["count_yesterday"];
	    $redshift_gamelog_moneyin_yesterday = $redshift_gamelog_yesterday_data["sum_in_yesterday"];
	    $redshift_gamelog_moneyout_yesterday = $redshift_gamelog_yesterday_data["sum_out_yesterday"];
	    
	    $sql = "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(0, 2, 1, '$mysql_gamelog_cnt_7days', '$redshift_gamelog_cnt_7days', 'RowCount(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_cnt_7days', redshift_value='$redshift_gamelog_cnt_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(0, 2, 2, '$mysql_gamelog_moneyin_7days', '$redshift_gamelog_moneyin_7days', 'Moneyin(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_moneyin_7days', redshift_value='$redshift_gamelog_moneyin_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(0, 2, 3, '$mysql_gamelog_moneyout_7days', '$redshift_gamelog_moneyout_7days', 'Moneyout(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_moneyout_7days', redshift_value='$redshift_gamelog_moneyout_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(0, 2, 4, '$mysql_gamelog_cnt_yesterday', '$redshift_gamelog_cnt_yesterday', 'RowCount(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_cnt_yesterday', redshift_value='$redshift_gamelog_cnt_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(0, 2, 5, '$mysql_gamelog_moneyin_yesterday', '$redshift_gamelog_moneyin_yesterday', 'Moneyin(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_moneyin_yesterday', redshift_value='$redshift_gamelog_moneyin_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(0, 2, 6, '$mysql_gamelog_moneyout_yesterday', '$redshift_gamelog_moneyout_yesterday', 'Moneyout(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_moneyout_yesterday', redshift_value='$redshift_gamelog_moneyout_yesterday', writedate=now();";
	    	    
	    
	    $db_analysis->execute($sql);
	    
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	//IOS
	try
	{
	    //IOS
	    //최근 7일(tbl_user_gamelog_ios)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(moneyin) AS sum_in_7day, SUM(moneyout) AS sum_out_7day FROM tbl_user_gamelog_ios WHERE writedate >= DATE_SUB('$today', INTERVAL 7 DAY) AND writedate < '$yesterday 00:00:00'";
	    $mysql_gamelog_ios_7days_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_ios_cnt_7days = $mysql_gamelog_ios_7days_data["count_7day"];
	    $mysql_gamelog_ios_moneyin_7days = $mysql_gamelog_ios_7days_data["sum_in_7day"];
	    $mysql_gamelog_ios_moneyout_7days = $mysql_gamelog_ios_7days_data["sum_out_7day"];
	    
	    //최근 6일(redshift)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(money_in) AS sum_in_7day, SUM(money_out) AS sum_out_7day FROM t5_user_gamelog_ios WHERE writedate >= DATEADD(DAY,(-7),'$today') AND writedate < '$yesterday 00:00:00'";
	    $redshift_gamelog_ios_7days_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_ios_cnt_7days = $redshift_gamelog_ios_7days_data["count_7day"];
	    $redshift_gamelog_ios_moneyin_7days = $redshift_gamelog_ios_7days_data["sum_in_7day"];
	    $redshift_gamelog_ios_moneyout_7days = $redshift_gamelog_ios_7days_data["sum_out_7day"];
	    
	    //어제
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(moneyin) AS sum_in_yesterday, SUM(moneyout) AS sum_out_yesterday FROM tbl_user_gamelog_ios WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $mysql_gamelog_ios_yesterday_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_ios_cnt_yesterday = $mysql_gamelog_ios_yesterday_data["count_yesterday"];
	    $mysql_gamelog_ios_moneyin_yesterday = $mysql_gamelog_ios_yesterday_data["sum_in_yesterday"];
	    $mysql_gamelog_ios_moneyout_yesterday = $mysql_gamelog_ios_yesterday_data["sum_out_yesterday"];
	    
	    //어제(redshift)
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(money_in) AS sum_in_yesterday, SUM(money_out) AS sum_out_yesterday FROM t5_user_gamelog_ios WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $redshift_gamelog_ios_yesterday_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_ios_cnt_yesterday = $redshift_gamelog_ios_yesterday_data["count_yesterday"];
	    $redshift_gamelog_ios_moneyin_yesterday = $redshift_gamelog_ios_yesterday_data["sum_in_yesterday"];
	    $redshift_gamelog_ios_moneyout_yesterday = $redshift_gamelog_ios_yesterday_data["sum_out_yesterday"];
	    
	    $sql = "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(1, 2, 1, '$mysql_gamelog_ios_cnt_7days', '$redshift_gamelog_ios_cnt_7days', 'RowCount(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ios_cnt_7days', redshift_value='$redshift_gamelog_ios_cnt_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(1, 2, 2, '$mysql_gamelog_ios_moneyin_7days', '$redshift_gamelog_ios_moneyin_7days', 'Moneyin(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ios_moneyin_7days', redshift_value='$redshift_gamelog_ios_moneyin_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(1, 2, 3, '$mysql_gamelog_ios_moneyout_7days', '$redshift_gamelog_ios_moneyout_7days', 'Moneyout(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ios_moneyout_7days', redshift_value='$redshift_gamelog_ios_moneyout_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(1, 2, 4, '$mysql_gamelog_ios_cnt_yesterday', '$redshift_gamelog_ios_cnt_yesterday', 'RowCount(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ios_cnt_yesterday', redshift_value='$redshift_gamelog_ios_cnt_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(1, 2, 5, '$mysql_gamelog_ios_moneyin_yesterday', '$redshift_gamelog_ios_moneyin_yesterday', 'Moneyin(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ios_moneyin_yesterday', redshift_value='$redshift_gamelog_ios_moneyin_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(1, 2, 6, '$mysql_gamelog_ios_moneyout_yesterday', '$redshift_gamelog_ios_moneyout_yesterday', 'Moneyout(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ios_moneyout_yesterday', redshift_value='$redshift_gamelog_ios_moneyout_yesterday', writedate=now();";
	    
	    $db_analysis->execute($sql);
	    
	}
	catch (Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	//Android
	try
	{
	    //최근 7일(tbl_user_gamelog_android)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(moneyin) AS sum_in_7day, SUM(moneyout) AS sum_out_7day FROM tbl_user_gamelog_android WHERE writedate >= DATE_SUB('$today', INTERVAL 7 DAY) AND writedate < '$yesterday 00:00:00'";
	    $mysql_gamelog_and_7days_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_and_cnt_7days = $mysql_gamelog_and_7days_data["count_7day"];
	    $mysql_gamelog_and_moneyin_7days = $mysql_gamelog_and_7days_data["sum_in_7day"];
	    $mysql_gamelog_and_moneyout_7days = $mysql_gamelog_and_7days_data["sum_out_7day"];
	    
	    //최근 6일(redshift)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(money_in) AS sum_in_7day, SUM(money_out) AS sum_out_7day FROM t5_user_gamelog_android WHERE writedate >= DATEADD(DAY,(-7),'$today') AND writedate < '$yesterday 00:00:00'";
	    $redshift_gamelog_and_7days_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_and_cnt_7days = $redshift_gamelog_and_7days_data["count_7day"];
	    $redshift_gamelog_and_moneyin_7days = $redshift_gamelog_and_7days_data["sum_in_7day"];
	    $redshift_gamelog_and_moneyout_7days = $redshift_gamelog_and_7days_data["sum_out_7day"];
	    
	    //어제
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(moneyin) AS sum_in_yesterday, SUM(moneyout) AS sum_out_yesterday FROM tbl_user_gamelog_android WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $mysql_gamelog_and_yesterday_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_and_cnt_yesterday = $mysql_gamelog_and_yesterday_data["count_yesterday"];
	    $mysql_gamelog_and_moneyin_yesterday = $mysql_gamelog_and_yesterday_data["sum_in_yesterday"];
	    $mysql_gamelog_and_moneyout_yesterday = $mysql_gamelog_and_yesterday_data["sum_out_yesterday"];
	    
	    //어제(redshift)
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(money_in) AS sum_in_yesterday, SUM(money_out) AS sum_out_yesterday FROM t5_user_gamelog_android WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $redshift_gamelog_and_yesterday_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_and_cnt_yesterday = $redshift_gamelog_and_yesterday_data["count_yesterday"];
	    $redshift_gamelog_and_moneyin_yesterday = $redshift_gamelog_and_yesterday_data["sum_in_yesterday"];
	    $redshift_gamelog_and_moneyout_yesterday = $redshift_gamelog_and_yesterday_data["sum_out_yesterday"];
	    
	    $sql = "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(2, 2, 1, '$mysql_gamelog_and_cnt_7days', '$redshift_gamelog_and_cnt_7days', 'RowCount(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_and_cnt_7days', redshift_value='$redshift_gamelog_and_cnt_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(2, 2, 2, '$mysql_gamelog_and_moneyin_7days', '$redshift_gamelog_and_moneyin_7days', 'Moneyin(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_and_moneyin_7days', redshift_value='$redshift_gamelog_and_moneyin_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(2, 2, 3, '$mysql_gamelog_and_moneyout_7days', '$redshift_gamelog_and_moneyout_7days', 'Moneyout(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_and_moneyout_7days', redshift_value='$redshift_gamelog_and_moneyout_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(2, 2, 4, '$mysql_gamelog_and_cnt_yesterday', '$redshift_gamelog_and_cnt_yesterday', 'RowCount(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_and_cnt_yesterday', redshift_value='$redshift_gamelog_and_cnt_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(2, 2, 5, '$mysql_gamelog_and_moneyin_yesterday', '$redshift_gamelog_and_moneyin_yesterday', 'Moneyin(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_and_moneyin_yesterday', redshift_value='$redshift_gamelog_and_moneyin_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(2, 2, 6, '$mysql_gamelog_and_moneyout_yesterday', '$redshift_gamelog_and_moneyout_yesterday', 'Moneyout(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_and_moneyout_yesterday', redshift_value='$redshift_gamelog_and_moneyout_yesterday', writedate=now();";
	    
	    $db_analysis->execute($sql);
	    
	}
	catch (Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	//Amazon
	try
	{
	    //최근 7일(tbl_user_gamelog_amazon)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(moneyin) AS sum_in_7day, SUM(moneyout) AS sum_out_7day FROM tbl_user_gamelog_amazon WHERE writedate >= DATE_SUB('$today', INTERVAL 7 DAY) AND writedate < '$yesterday 00:00:00'";
	    $mysql_gamelog_ama_7days_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_ama_cnt_7days = $mysql_gamelog_ama_7days_data["count_7day"];
	    $mysql_gamelog_ama_moneyin_7days = $mysql_gamelog_ama_7days_data["sum_in_7day"];
	    $mysql_gamelog_ama_moneyout_7days = $mysql_gamelog_ama_7days_data["sum_out_7day"];
	    
	    //최근 6일(redshift)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(money_in) AS sum_in_7day, SUM(money_out) AS sum_out_7day FROM t5_user_gamelog_amazon WHERE writedate >= DATEADD(DAY,(-7),'$today') AND writedate < '$yesterday 00:00:00'";
	    $redshift_gamelog_ama_7days_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_ama_cnt_7days = $redshift_gamelog_ama_7days_data["count_7day"];
	    $redshift_gamelog_ama_moneyin_7days = $redshift_gamelog_ama_7days_data["sum_in_7day"];
	    $redshift_gamelog_ama_moneyout_7days = $redshift_gamelog_ama_7days_data["sum_out_7day"];
	    
	    //어제
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(moneyin) AS sum_in_yesterday, SUM(moneyout) AS sum_out_yesterday FROM tbl_user_gamelog_amazon WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $mysql_gamelog_ama_yesterday_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_ama_cnt_yesterday = $mysql_gamelog_ama_yesterday_data["count_yesterday"];
	    $mysql_gamelog_ama_moneyin_yesterday = $mysql_gamelog_ama_yesterday_data["sum_in_yesterday"];
	    $mysql_gamelog_ama_moneyout_yesterday = $mysql_gamelog_ama_yesterday_data["sum_out_yesterday"];
	    
	    //어제(redshift)
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(money_in) AS sum_in_yesterday, SUM(money_out) AS sum_out_yesterday FROM t5_user_gamelog_amazon WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $redshift_gamelog_ama_yesterday_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_ama_cnt_yesterday = $redshift_gamelog_ama_yesterday_data["count_yesterday"];
	    $redshift_gamelog_ama_moneyin_yesterday = $redshift_gamelog_ama_yesterday_data["sum_in_yesterday"];
	    $redshift_gamelog_ama_moneyout_yesterday = $redshift_gamelog_ama_yesterday_data["sum_out_yesterday"];
	    
	    $sql = "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(3, 2, 1, '$mysql_gamelog_ama_cnt_7days', '$redshift_gamelog_ama_cnt_7days', 'RowCount(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ama_cnt_7days', redshift_value='$redshift_gamelog_ama_cnt_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(3, 2, 2, '$mysql_gamelog_ama_moneyin_7days', '$redshift_gamelog_ama_moneyin_7days', 'Moneyin(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ama_moneyin_7days', redshift_value='$redshift_gamelog_ama_moneyin_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(3, 2, 3, '$mysql_gamelog_ama_moneyout_7days', '$redshift_gamelog_ama_moneyout_7days', 'Moneyout(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ama_moneyout_7days', redshift_value='$redshift_gamelog_ama_moneyout_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(3, 2, 4, '$mysql_gamelog_ama_cnt_yesterday', '$redshift_gamelog_ama_cnt_yesterday', 'RowCount(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ama_cnt_yesterday', redshift_value='$redshift_gamelog_ama_cnt_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(3, 2, 5, '$mysql_gamelog_ama_moneyin_yesterday', '$redshift_gamelog_ama_moneyin_yesterday', 'Moneyin(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ama_moneyin_yesterday', redshift_value='$redshift_gamelog_ama_moneyin_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(3, 2, 6, '$mysql_gamelog_ama_moneyout_yesterday', '$redshift_gamelog_ama_moneyout_yesterday', 'Moneyout(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ama_moneyout_yesterday', redshift_value='$redshift_gamelog_ama_moneyout_yesterday', writedate=now();";
	    
	    $db_analysis->execute($sql);
	    
	}
	catch (Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	try
	{
	    $freecoin_total_count_27days = 0;
	    $freecoin_total_count_7days = 0;
	    $freecoin_total_count_yesterday = 0;
	    
	    //최근 27일
	    for($i=0;$i<10;$i++)
	    {
	        $sql = "SELECT COUNT(*) AS count_27day FROM tbl_user_freecoin_log_$i WHERE writedate >= DATE_SUB('$today', INTERVAL 27 DAY) AND writedate < '$yesterday 00:00:00' AND useridx > 20000 ";
	        $mysql_freecoin_cnt_27days = $db_main2->getvalue($sql);
	        
	        $freecoin_total_count_27days += $mysql_freecoin_cnt_27days;
	    }
	    
	    //최근 28일(redshift)
	    $sql = "SELECT COUNT(*) AS count_27day FROM t5_user_freecoin_log WHERE writedate >= DATEADD(DAY,(-27),'$today') AND writedate < '$yesterday 00:00:00' AND useridx > 20000 ";
	    $redshift_freecoin_cnt_27days = $db_redshift->getvalue($sql);
	    
	    //최근 7일
	    for($i=0;$i<10;$i++)
	    {
	        $sql = "SELECT COUNT(*) AS count_7day FROM tbl_user_freecoin_log_$i WHERE writedate >= DATE_SUB('$today', INTERVAL 7 DAY) AND writedate < '$yesterday 00:00:00' AND useridx > 20000 ";
	        $mysql_freecoin_cnt_7days = $db_main2->getvalue($sql);
	        
	        $freecoin_total_count_7days += $mysql_freecoin_cnt_7days;
	    }
	    
	    //최근 7일(redshift)
	    $sql = "SELECT COUNT(*) AS count_7day FROM t5_user_freecoin_log WHERE writedate >= DATEADD(DAY,(-7),'$today') AND writedate < '$yesterday 00:00:00' AND useridx > 20000 ";
	    $redshift_freecoin_cnt_7days = $db_redshift->getvalue($sql);
		
		//어제
	    for($i=0;$i<10;$i++)
	    {
	        $sql = "SELECT COUNT(*) AS count_yesterday FROM tbl_user_freecoin_log_$i WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND useridx > 20000 ";
	        $mysql_freecoin_cnt_yesterday = $db_main2->getvalue($sql);
	        
	        $freecoin_total_count_yesterday += $mysql_freecoin_cnt_yesterday;
	    }
	    
	    //어제(redshift)
	    $sql = "SELECT COUNT(*) AS count_yesterday FROM t5_user_freecoin_log WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND useridx > 20000";
	    $redshift_freecoin_cnt_yesterday = $db_redshift->getvalue($sql);
		
	    $sql = "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(100, 3, 1, '$freecoin_total_count_27days', '$redshift_freecoin_cnt_27days', 'RowCount(27days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$freecoin_total_count_27days', redshift_value='$redshift_freecoin_cnt_27days', writedate=now();";
		$sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(100, 3, 2, '$freecoin_total_count_7days', '$redshift_freecoin_cnt_7days', 'RowCount(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$freecoin_total_count_7days', redshift_value='$redshift_freecoin_cnt_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(100, 3, 3, '$freecoin_total_count_yesterday', '$redshift_freecoin_cnt_yesterday', 'RowCount(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$freecoin_total_count_yesterday', redshift_value='$redshift_freecoin_cnt_yesterday', writedate=now();";
	    $db_analysis->execute($sql);
	    
	}
	catch (Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	try
	{
	    //최근 28일
	    $sql = "SELECT COUNT(*) AS count_28day FROM tbl_user_retention_log WHERE writedate >= DATE_SUB('$today', INTERVAL 28 DAY) AND writedate < '$yesterday 00:00:00'";
	    $mysql_retention_cnt_28days = $db_main2->getvalue($sql);
	    
	    //최근 28일(redshift)
	    $sql = "SELECT COUNT(*) AS count_28day FROM t5_user_retention_log WHERE writedate >= DATEADD(DAY,(-28),'$today') AND writedate < '$yesterday 00:00:00'";
	    $redshift_retention_cnt_28days = $db_redshift->getvalue($sql);
	    
	    //최근 7일
	    $sql = "SELECT COUNT(*) AS count_7day FROM tbl_user_retention_log WHERE writedate >= DATE_SUB('$today', INTERVAL 7 DAY) AND writedate < '$yesterday 00:00:00'";
	    $mysql_retention_cnt_7days = $db_main2->getvalue($sql);
	    
	    //최근 7일(redshift)
	    $sql = "SELECT COUNT(*) AS count_7day FROM t5_user_retention_log WHERE writedate >= DATEADD(DAY,(-7),'$today') AND writedate < '$yesterday 00:00:00'";
	    $redshift_retention_cnt_7days = $db_redshift->getvalue($sql);
	    
	    //어제
	    $sql = "SELECT COUNT(*) AS count_yesterday FROM tbl_user_retention_log WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $mysql_retention_cnt_yesterday = $db_main2->getvalue($sql);
	    
	    //어제(redshift)
	    $sql = "SELECT COUNT(*) AS count_yesterday FROM t5_user_retention_log WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $redshift_retention_cnt_yesterday = $db_redshift->getvalue($sql);
	    
	    $sql = "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(0, 5, 1, '$mysql_retention_cnt_28days', '$redshift_retention_cnt_28days', 'RowCount(28days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_retention_cnt_28days', redshift_value='$redshift_retention_cnt_28days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(0, 5, 2, '$mysql_retention_cnt_7days', '$redshift_retention_cnt_7days', 'RowCount(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_retention_cnt_7days', redshift_value='$redshift_retention_cnt_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(0, 5, 3, '$mysql_retention_cnt_yesterday', '$redshift_retention_cnt_yesterday', 'RowCount(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_retention_cnt_yesterday', redshift_value='$redshift_retention_cnt_yesterday', writedate=now();";
	    $db_analysis->execute($sql);
	    
	}
	catch (Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	//tbl_user_retention_mobile_log
	try
	{
	    //최근 28일
	    $sql = "SELECT COUNT(*) AS count_28day FROM tbl_user_retention_mobile_log WHERE writedate >= DATE_SUB('$today', INTERVAL 28 DAY) AND writedate < '$yesterday 00:00:00'";
	    $mysql_retention_cnt_28days = $db_main2->getvalue($sql);
	    
	    //최근 28일(redshift)
	    $sql = "SELECT COUNT(*) AS count_28day FROM t5_user_retention_mobile_log WHERE writedate >= DATEADD(DAY,(-28),'$today') AND writedate < '$yesterday 00:00:00'";
	    $redshift_retention_cnt_28days = $db_redshift->getvalue($sql);
	    
	    //최근 7일
	    $sql = "SELECT COUNT(*) AS count_7day FROM tbl_user_retention_mobile_log WHERE writedate >= DATE_SUB('$today', INTERVAL 7 DAY) AND writedate < '$yesterday 00:00:00'";
	    $mysql_retention_cnt_7days = $db_main2->getvalue($sql);
	    
	    //최근 7일(redshift)
	    $sql = "SELECT COUNT(*) AS count_7day FROM t5_user_retention_mobile_log WHERE writedate >= DATEADD(DAY,(-7),'$today') AND writedate < '$yesterday 00:00:00'";
	    $redshift_retention_cnt_7days = $db_redshift->getvalue($sql);
	    
	    //어제
	    $sql = "SELECT COUNT(*) AS count_yesterday FROM tbl_user_retention_mobile_log WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $mysql_retention_cnt_yesterday = $db_main2->getvalue($sql);
	    
	    //어제(redshift)
	    $sql = "SELECT COUNT(*) AS count_yesterday FROM t5_user_retention_mobile_log WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $redshift_retention_cnt_yesterday = $db_redshift->getvalue($sql);
	    
	    $sql = "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(1, 5, 1, '$mysql_retention_cnt_28days', '$redshift_retention_cnt_28days', 'RowCount(28days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_retention_cnt_28days', redshift_value='$redshift_retention_cnt_28days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(1, 5, 2, '$mysql_retention_cnt_7days', '$redshift_retention_cnt_7days', 'RowCount(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_retention_cnt_7days', redshift_value='$redshift_retention_cnt_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(1, 5, 3, '$mysql_retention_cnt_yesterday', '$redshift_retention_cnt_yesterday', 'RowCount(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_retention_cnt_yesterday', redshift_value='$redshift_retention_cnt_yesterday', writedate=now();";
	    $db_analysis->execute($sql);
	    
	}
	catch (Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	try
	{
	    //최근 6일(tbl_user_gamelog_3h)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(moneyin) AS sum_in_7day, SUM(moneyout) AS sum_out_7day FROM tbl_user_gamelog_3h WHERE writedate >= DATE_SUB('$today', INTERVAL 7 DAY) AND writedate < '$yesterday 00:00:00'";
	    $mysql_gamelog_3h_7days_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_3h_cnt_7days = $mysql_gamelog_3h_7days_data["count_7day"];
	    $mysql_gamelog_3h_moneyin_7days = $mysql_gamelog_3h_7days_data["sum_in_7day"];
	    $mysql_gamelog_3h_moneyout_7days = $mysql_gamelog_3h_7days_data["sum_out_7day"];
	    
	    //최근 6일(redshift)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(money_in) AS sum_in_7day, SUM(money_out) AS sum_out_7day FROM t5_user_gamelog_3h WHERE writedate >= DATEADD(DAY,(-7),'$today') AND writedate < '$yesterday 00:00:00'";
	    $redshift_gamelog_3h_7days_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_3h_cnt_7days = $redshift_gamelog_3h_7days_data["count_7day"];
	    $redshift_gamelog_3h_moneyin_7days = $redshift_gamelog_3h_7days_data["sum_in_7day"];
	    $redshift_gamelog_3h_moneyout_7days = $redshift_gamelog_3h_7days_data["sum_out_7day"];
	    
	    //어제
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(moneyin) AS sum_in_yesterday, SUM(moneyout) AS sum_out_yesterday FROM tbl_user_gamelog_3h WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $mysql_gamelog_3h_yesterday_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_3h_cnt_yesterday = $mysql_gamelog_3h_yesterday_data["count_yesterday"];
	    $mysql_gamelog_3h_moneyin_yesterday = $mysql_gamelog_3h_yesterday_data["sum_in_yesterday"];
	    $mysql_gamelog_3h_moneyout_yesterday = $mysql_gamelog_3h_yesterday_data["sum_out_yesterday"];
	    
	    //어제(redshift)
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(money_in) AS sum_in_yesterday, SUM(money_out) AS sum_out_yesterday FROM t5_user_gamelog_3h WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $redshift_gamelog_3h_yesterday_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_3h_cnt_yesterday = $redshift_gamelog_3h_yesterday_data["count_yesterday"];
	    $redshift_gamelog_3h_moneyin_yesterday = $redshift_gamelog_3h_yesterday_data["sum_in_yesterday"];
	    $redshift_gamelog_3h_moneyout_yesterday = $redshift_gamelog_3h_yesterday_data["sum_out_yesterday"];
	    
	    $sql = "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(0, 6, 1, '$mysql_gamelog_3h_cnt_7days', '$redshift_gamelog_3h_cnt_7days', 'RowCount(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_3h_cnt_7days', redshift_value='$redshift_gamelog_3h_cnt_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(0, 6, 2, '$mysql_gamelog_3h_moneyin_7days', '$redshift_gamelog_3h_moneyin_7days', 'Moneyin(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_3h_moneyin_7days', redshift_value='$redshift_gamelog_3h_moneyin_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(0, 6, 3, '$mysql_gamelog_3h_moneyout_7days', '$redshift_gamelog_3h_moneyout_7days', 'Moneyout(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_3h_moneyout_7days', redshift_value='$redshift_gamelog_3h_moneyout_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(0, 6, 4, '$mysql_gamelog_3h_cnt_yesterday', '$redshift_gamelog_3h_cnt_yesterday', 'RowCount(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_3h_cnt_yesterday', redshift_value='$redshift_gamelog_3h_cnt_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(0, 6, 5, '$mysql_gamelog_3h_moneyin_yesterday', '$redshift_gamelog_3h_moneyin_yesterday', 'Moneyin(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_3h_moneyin_yesterday', redshift_value='$redshift_gamelog_3h_moneyin_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(0, 6, 6, '$mysql_gamelog_3h_moneyout_yesterday', '$redshift_gamelog_3h_moneyout_yesterday', 'Moneyout(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_3h_moneyout_yesterday', redshift_value='$redshift_gamelog_3h_moneyout_yesterday', writedate=now();";
	    
	    
	    $db_analysis->execute($sql);
	    
	}
	catch (Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	//IOS
	try
	{
	    //IOS
	    //최근 7일(tbl_user_gamelog_mobile_3h)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(moneyin) AS sum_in_7day, SUM(moneyout) AS sum_out_7day FROM tbl_user_gamelog_ios_3h WHERE writedate >= DATE_SUB('$today', INTERVAL 7 DAY) AND writedate < '$yesterday 00:00:00'";
	    $mysql_gamelog_ios_3h_7days_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_ios_3h_cnt_7days = $mysql_gamelog_ios_3h_7days_data["count_7day"];
	    $mysql_gamelog_ios_3h_moneyin_7days = $mysql_gamelog_ios_3h_7days_data["sum_in_7day"];
	    $mysql_gamelog_ios_3h_moneyout_7days = $mysql_gamelog_ios_3h_7days_data["sum_out_7day"];
	    
	    //최근 7일(redshift)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(money_in) AS sum_in_7day, SUM(money_out) AS sum_out_7day FROM t5_user_gamelog_ios_3h WHERE writedate >= DATEADD(DAY,(-7),'$today') AND writedate < '$yesterday 00:00:00'";
	    $redshift_gamelog_ios_3h_7days_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_ios_3h_cnt_7days = $redshift_gamelog_ios_3h_7days_data["count_7day"];
	    $redshift_gamelog_ios_3h_moneyin_7days = $redshift_gamelog_ios_3h_7days_data["sum_in_7day"];
	    $redshift_gamelog_ios_3h_moneyout_7days = $redshift_gamelog_ios_3h_7days_data["sum_out_7day"];
	    
	    //어제
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(moneyin) AS sum_in_yesterday, SUM(moneyout) AS sum_out_yesterday FROM tbl_user_gamelog_ios_3h WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $mysql_gamelog_ios_3h_yesterday_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_ios_3h_cnt_yesterday = $mysql_gamelog_ios_3h_yesterday_data["count_yesterday"];
	    $mysql_gamelog_ios_3h_moneyin_yesterday = $mysql_gamelog_ios_3h_yesterday_data["sum_in_yesterday"];
	    $mysql_gamelog_ios_3h_moneyout_yesterday = $mysql_gamelog_ios_3h_yesterday_data["sum_out_yesterday"];
	    
	    //어제(redshift)
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(money_in) AS sum_in_yesterday, SUM(money_out) AS sum_out_yesterday FROM t5_user_gamelog_ios_3h WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $redshift_gamelog_ios_3h_yesterday_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_ios_3h_cnt_yesterday = $redshift_gamelog_ios_3h_yesterday_data["count_yesterday"];
	    $redshift_gamelog_ios_3h_moneyin_yesterday = $redshift_gamelog_ios_3h_yesterday_data["sum_in_yesterday"];
	    $redshift_gamelog_ios_3h_moneyout_yesterday = $redshift_gamelog_ios_3h_yesterday_data["sum_out_yesterday"];
	    
	    $sql = "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(1, 6, 1, '$mysql_gamelog_ios_3h_cnt_7days', '$redshift_gamelog_ios_3h_cnt_7days', 'RowCount(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ios_3h_cnt_7days', redshift_value='$redshift_gamelog_ios_3h_cnt_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(1, 6, 2, '$mysql_gamelog_ios_3h_moneyin_7days', '$redshift_gamelog_ios_3h_moneyin_7days', 'Moneyin(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ios_3h_moneyin_7days', redshift_value='$redshift_gamelog_ios_3h_moneyin_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(1, 6, 3, '$mysql_gamelog_ios_3h_moneyout_7days', '$redshift_gamelog_ios_3h_moneyout_7days', 'Moneyout(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ios_3h_moneyout_7days', redshift_value='$redshift_gamelog_ios_3h_moneyout_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(1, 6, 4, '$mysql_gamelog_ios_3h_cnt_yesterday', '$redshift_gamelog_ios_3h_cnt_yesterday', 'RowCount(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ios_3h_cnt_yesterday', redshift_value='$redshift_gamelog_ios_3h_cnt_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(1, 6, 5, '$mysql_gamelog_ios_3h_moneyin_yesterday', '$redshift_gamelog_ios_3h_moneyin_yesterday', 'Moneyin(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ios_3h_moneyin_yesterday', redshift_value='$redshift_gamelog_ios_3h_moneyin_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(1, 6, 6, '$mysql_gamelog_ios_3h_moneyout_yesterday', '$redshift_gamelog_ios_3h_moneyout_yesterday', 'Moneyout(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ios_3h_moneyout_yesterday', redshift_value='$redshift_gamelog_ios_3h_moneyout_yesterday', writedate=now();";
	    
	    $db_analysis->execute($sql);
	    
	}
	catch (Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	//Android
	try
	{
	    //최근 7일(tbl_user_gamelog_android_3h)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(moneyin) AS sum_in_7day, SUM(moneyout) AS sum_out_7day FROM tbl_user_gamelog_android_3h WHERE writedate >= DATE_SUB('$today', INTERVAL 7 DAY) AND writedate < '$yesterday 00:00:00'";
	    $mysql_gamelog_and_3h_7days_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_and_3h_cnt_7days = $mysql_gamelog_and_3h_7days_data["count_7day"];
	    $mysql_gamelog_and_3h_moneyin_7days = $mysql_gamelog_and_3h_7days_data["sum_in_7day"];
	    $mysql_gamelog_and_3h_moneyout_7days = $mysql_gamelog_and_3h_7days_data["sum_out_7day"];
	    
	    //최근 7일(redshift)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(money_in) AS sum_in_7day, SUM(money_out) AS sum_out_7day FROM t5_user_gamelog_android_3h WHERE writedate >= DATEADD(DAY,(-7),'$today') AND writedate < '$yesterday 00:00:00'";
	    $redshift_gamelog_and_3h_7days_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_and_3h_cnt_7days = $redshift_gamelog_and_3h_7days_data["count_7day"];
	    $redshift_gamelog_and_3h_moneyin_7days = $redshift_gamelog_and_3h_7days_data["sum_in_7day"];
	    $redshift_gamelog_and_3h_moneyout_7days = $redshift_gamelog_and_3h_7days_data["sum_out_7day"];
	    
	    //어제
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(moneyin) AS sum_in_yesterday, SUM(moneyout) AS sum_out_yesterday FROM tbl_user_gamelog_android_3h WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $mysql_gamelog_and_3h_yesterday_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_and_3h_cnt_yesterday = $mysql_gamelog_and_3h_yesterday_data["count_yesterday"];
	    $mysql_gamelog_and_3h_moneyin_yesterday = $mysql_gamelog_and_3h_yesterday_data["sum_in_yesterday"];
	    $mysql_gamelog_and_3h_moneyout_yesterday = $mysql_gamelog_and_3h_yesterday_data["sum_out_yesterday"];
	    
	    //어제(redshift)
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(money_in) AS sum_in_yesterday, SUM(money_out) AS sum_out_yesterday FROM t5_user_gamelog_android_3h WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $redshift_gamelog_and_3h_yesterday_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_and_3h_cnt_yesterday = $redshift_gamelog_and_3h_yesterday_data["count_yesterday"];
	    $redshift_gamelog_and_3h_moneyin_yesterday = $redshift_gamelog_and_3h_yesterday_data["sum_in_yesterday"];
	    $redshift_gamelog_and_3h_moneyout_yesterday = $redshift_gamelog_and_3h_yesterday_data["sum_out_yesterday"];
	    
	    $sql = "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(2, 6, 1, '$mysql_gamelog_and_3h_cnt_7days', '$redshift_gamelog_and_3h_cnt_7days', 'RowCount(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_and_3h_cnt_7days', redshift_value='$redshift_gamelog_and_3h_cnt_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(2, 6, 2, '$mysql_gamelog_and_3h_moneyin_7days', '$redshift_gamelog_and_3h_moneyin_7days', 'Moneyin(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_and_3h_moneyin_7days', redshift_value='$redshift_gamelog_and_3h_moneyin_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(2, 6, 3, '$mysql_gamelog_and_3h_moneyout_7days', '$redshift_gamelog_and_3h_moneyout_7days', 'Moneyout(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_and_3h_moneyout_7days', redshift_value='$redshift_gamelog_and_3h_moneyout_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(2, 6, 4, '$mysql_gamelog_and_3h_cnt_yesterday', '$redshift_gamelog_and_3h_cnt_yesterday', 'RowCount(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_and_3h_cnt_yesterday', redshift_value='$redshift_gamelog_and_3h_cnt_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(2, 6, 5, '$mysql_gamelog_and_3h_moneyin_yesterday', '$redshift_gamelog_and_3h_moneyin_yesterday', 'Moneyin(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_and_3h_moneyin_yesterday', redshift_value='$redshift_gamelog_and_3h_moneyin_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(2, 6, 6, '$mysql_gamelog_and_3h_moneyout_yesterday', '$redshift_gamelog_and_3h_moneyout_yesterday', 'Moneyout(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_and_3h_moneyout_yesterday', redshift_value='$redshift_gamelog_and_3h_moneyout_yesterday', writedate=now();";
	    
	    $db_analysis->execute($sql);
	    
	}
	catch (Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	//Amazon
	try
	{
	    //최근 7일(tbl_user_gamelog_amazon_3h)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(moneyin) AS sum_in_7day, SUM(moneyout) AS sum_out_7day FROM tbl_user_gamelog_amazon_3h WHERE writedate >= DATE_SUB('$today', INTERVAL 7 DAY) AND writedate < '$yesterday 00:00:00'";
	    $mysql_gamelog_ama_3h_7days_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_ama_3h_cnt_7days = $mysql_gamelog_ama_3h_7days_data["count_7day"];
	    $mysql_gamelog_ama_3h_moneyin_7days = $mysql_gamelog_ama_3h_7days_data["sum_in_7day"];
	    $mysql_gamelog_ama_3h_moneyout_7days = $mysql_gamelog_ama_3h_7days_data["sum_out_7day"];
	    
	    //최근 7일(redshift)
	    $sql = "SELECT COUNT(*) AS count_7day, SUM(money_in) AS sum_in_7day, SUM(money_out) AS sum_out_7day FROM t5_user_gamelog_amazon_3h WHERE writedate >= DATEADD(DAY,(-7),'$today') AND writedate < '$yesterday 00:00:00'";
	    $redshift_gamelog_ama_3h_7days_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_ama_3h_cnt_7days = $redshift_gamelog_ama_3h_7days_data["count_7day"];
	    $redshift_gamelog_ama_3h_moneyin_7days = $redshift_gamelog_ama_3h_7days_data["sum_in_7day"];
	    $redshift_gamelog_ama_3h_moneyout_7days = $redshift_gamelog_ama_3h_7days_data["sum_out_7day"];
	    
	    //어제
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(moneyin) AS sum_in_yesterday, SUM(moneyout) AS sum_out_yesterday FROM tbl_user_gamelog_amazon_3h WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $mysql_gamelog_ama_3h_yesterday_data = $db_other->getarray($sql);
	    
	    $mysql_gamelog_ama_3h_cnt_yesterday = $mysql_gamelog_ama_3h_yesterday_data["count_yesterday"];
	    $mysql_gamelog_ama_3h_moneyin_yesterday = $mysql_gamelog_ama_3h_yesterday_data["sum_in_yesterday"];
	    $mysql_gamelog_ama_3h_moneyout_yesterday = $mysql_gamelog_ama_3h_yesterday_data["sum_out_yesterday"];
	    
	    //어제(redshift)
	    $sql = "SELECT COUNT(*) AS count_yesterday, SUM(money_in) AS sum_in_yesterday, SUM(money_out) AS sum_out_yesterday FROM t5_user_gamelog_amazon_3h WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	    $redshift_gamelog_ama_3h_yesterday_data = $db_redshift->getarray($sql);
	    
	    $redshift_gamelog_ama_3h_cnt_yesterday = $redshift_gamelog_ama_3h_yesterday_data["count_yesterday"];
	    $redshift_gamelog_ama_3h_moneyin_yesterday = $redshift_gamelog_ama_3h_yesterday_data["sum_in_yesterday"];
	    $redshift_gamelog_ama_3h_moneyout_yesterday = $redshift_gamelog_ama_3h_yesterday_data["sum_out_yesterday"];
	    
	    $sql = "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(3, 6, 1, '$mysql_gamelog_ama_3h_cnt_7days', '$redshift_gamelog_ama_3h_cnt_7days', 'RowCount(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ama_3h_cnt_7days', redshift_value='$redshift_gamelog_ama_3h_cnt_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(3, 6, 2, '$mysql_gamelog_ama_3h_moneyin_7days', '$redshift_gamelog_ama_3h_moneyin_7days', 'Moneyin(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ama_3h_moneyin_7days', redshift_value='$redshift_gamelog_ama_3h_moneyin_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(3, 6, 3, '$mysql_gamelog_ama_3h_moneyout_7days', '$redshift_gamelog_ama_3h_moneyout_7days', 'Moneyout(7days)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ama_3h_moneyout_7days', redshift_value='$redshift_gamelog_ama_3h_moneyout_7days', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(3, 6, 4, '$mysql_gamelog_ama_3h_cnt_yesterday', '$redshift_gamelog_ama_3h_cnt_yesterday', 'RowCount(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ama_3h_cnt_yesterday', redshift_value='$redshift_gamelog_ama_3h_cnt_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(3, 6, 5, '$mysql_gamelog_ama_3h_moneyin_yesterday', '$redshift_gamelog_ama_3h_moneyin_yesterday', 'Moneyin(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ama_3h_moneyin_yesterday', redshift_value='$redshift_gamelog_ama_3h_moneyin_yesterday', writedate=now();";
	    $sql .= "INSERT INTO tbl_redshift_dashboard_stat(platform, type, subtype, mysql_value, redshift_value, description, writedate) VALUES(3, 6, 6, '$mysql_gamelog_ama_3h_moneyout_yesterday', '$redshift_gamelog_ama_3h_moneyout_yesterday', 'Moneyout(yesterday)', now()) ON DUPLICATE KEY UPDATE mysql_value='$mysql_gamelog_ama_3h_moneyout_yesterday', redshift_value='$redshift_gamelog_ama_3h_moneyout_yesterday', writedate=now();";
	    
	    $db_analysis->execute($sql);
	    
	}
	catch (Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	$db_redshift->end();
	$slave_main->end();
	$db_analysis->end();
	$db_other->end();
	$db_main2->end();
?>