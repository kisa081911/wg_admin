<?
	include("../common/common_include.inc.php");
	include("../common/dbconnect/db_util_redshift.inc.php");
	
	$db_main2 = new CDatabase_Main2();
	$db_redshift = new CDatabase_Redshift();
	
	$db_main2->execute("SET wait_timeout=72000");
	
	$today = date("Y-m-d", time());
	$before_14day = date("Y-m-d", time() - 60 * 60 * 24 * 14);
	$before_60day = date("Y-m-d", time() - 60 * 60 * 24 * 60);
	
	$ods_list = "25, 26, 27";
	
	try 
	{
		//1. Normal
		// 전체 - ODS 제외
		$sql = "SELECT slottype, sum(playcount*(betlevel+1)) as step_1 ".
				"FROM t5_user_gamelog ".
				"WHERE writedate >= '$before_14day 00:00:00' AND MODE NOT IN (4, 5, 9, 26) AND slottype > 0 AND betlevel < 10 AND slottype NOT IN ($ods_list) ".
				"GROUP BY slottype ";				
		$slot_info_step1 = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i<sizeof($slot_info_step1); $i++)
		{
			$step1_slottype = $slot_info_step1[$i]["slottype"];
			$step1_amount = $slot_info_step1[$i]["step_1"];
			
			$insert_sql = "INSERT INTO tbl_slot_ranking_normal_info(slottype, step_1) VALUES($step1_slottype, $step1_amount) ".
			     		 	"ON DUPLICATE KEY UPDATE step_1=VALUES(step_1);";
			$db_main2->execute($insert_sql);
		}
		
		// 전체 - ODS만
		$sql = "SELECT 25 as slottype, AVG(step_1) as step_1 ".
				"FROM ".
				"(	".
				"	SELECT slottype, sum(playcount*(betlevel+1)) as step_1 ".
				"	FROM t5_user_gamelog ".
				"	WHERE writedate >= '$before_14day 00:00:00' AND MODE NOT IN (4, 5, 9, 26) AND betlevel < 10 AND slottype IN ($ods_list) ".
				"	GROUP BY slottype ". 
				"	ORDER BY step_1 DESC LIMIT 3 ".
				") t1";
		$slot_info_step1 = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i<sizeof($slot_info_step1); $i++)
		{
			$step1_slottype = $slot_info_step1[$i]["slottype"];
			$step1_amount = $slot_info_step1[$i]["step_1"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_normal_info(slottype, step_1) VALUES($step1_slottype, $step1_amount) ".
				"ON DUPLICATE KEY UPDATE step_1=VALUES(step_1);";
			$db_main2->execute($insert_sql);
		}
		
		//결제 - ODS 제외
		$sql = "SELECT slottype, sum(playcount*(betlevel+1)) as step_2 ".
				"FROM ( ".
  				"	select distinct useridx ".
  				"	from t5_product_order ".
  				"	where useridx > 20000 and status and writedate >= '$before_60day 00:00:00' ".
				") t1 join t5_user_gamelog as t2 on t1.useridx = t2.useridx	".
				"WHERE writedate >= '$before_14day 00:00:00' AND MODE NOT IN (4, 5, 9, 26) AND betlevel < 10 AND slottype > 0 AND slottype NOT IN ($ods_list)	". 
				"GROUP BY slottype";
		$slot_info_step2 = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i<sizeof($slot_info_step2); $i++)
		{
			$step2_slottype = $slot_info_step2[$i]["slottype"];
			$step2_amount = $slot_info_step2[$i]["step_2"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_normal_info(slottype, step_2) VALUES($step2_slottype, $step2_amount) ".
			     		 	"ON DUPLICATE KEY UPDATE step_2=VALUES(step_2);";
			$db_main2->execute($insert_sql);
		}
		
		//결제 - ODS만
		$sql = "SELECT 25 AS slottype, avg(step_2) as step_2 ".
				"FROM ".
				"(	".
				"	SELECT slottype, sum(playcount*(betlevel+1)) as step_2 ". 
				"	FROM ( ". 
				"		SELECT DISTINCT useridx ". 
				"		FROM t5_product_order ". 
				"		where useridx > 20000 and status and writedate >= '$before_60day 00:00:00' ". 
				"	)t1 join t5_user_gamelog as t2 on t1.useridx = t2.useridx ".	
				" 	WHERE writedate >= '$before_14day 00:00:00' AND MODE NOT IN (4, 5, 9, 26) AND betlevel < 10 AND slottype IN ($ods_list) ".	
				"	GROUP BY slottype ". 
				"	ORDER BY step_2 DESC LIMIT 3 ".
				") t1";
		$slot_info_step2 = $db_redshift->gettotallist($sql);

		for($i = 0; $i<sizeof($slot_info_step2); $i++)
		{
			$step2_slottype = $slot_info_step2[$i]["slottype"];
			$step2_amount = $slot_info_step2[$i]["step_2"];

			$insert_sql = "INSERT INTO tbl_slot_ranking_normal_info(slottype, step_2) VALUES($step2_slottype, $step2_amount) ".
							"ON DUPLICATE KEY UPDATE step_2=VALUES(step_2);";
			$db_main2->execute($insert_sql);
		}

		// MAX(전체), MAX(결제) 일치하도록 Normalize
		$sql = "SELECT ROUND(MAX(step_1)/MAX(step_2),0) AS multiple FROM `tbl_slot_ranking_normal_info`";
		$ranking_multiple = $db_main2->getvalue($sql);
		
		$sql = "SELECT slottype, step_2 * $ranking_multiple AS step2_balance FROM tbl_slot_ranking_normal_info";
		$ranking_step2_balance = $db_main2->gettotallist($sql);
		
		for($i = 0; $i<sizeof($ranking_step2_balance); $i++)
		{
			$step2_balance_slottype = $ranking_step2_balance[$i]["slottype"];
			$step2_balance_amount = $ranking_step2_balance[$i]["step2_balance"];
			
			$insert_sql = "INSERT INTO tbl_slot_ranking_normal_info(slottype, step_2) VALUES($step2_balance_slottype, $step2_balance_amount) ".
			     		 	"ON DUPLICATE KEY UPDATE step_2=VALUES(step_2);";
			$db_main2->execute($insert_sql);
		}
		
		//전체 X 4 + 결제 X 6 하여 최종 순위값 도출
		$sql = "SELECT slottype, ((step_1*4) +  (step_2*6)) AS ranking FROM `tbl_slot_ranking_normal_info`";
		$ranking_info = $db_main2->gettotallist($sql);
		
		for($i = 0; $i<sizeof($ranking_info); $i++)
		{
			$ranking_slottype = $ranking_info[$i]["slottype"];
			$ranking_amount = $ranking_info[$i]["ranking"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_normal_info(slottype, ranking) VALUES($ranking_slottype, $ranking_amount) ".
			     		 	"ON DUPLICATE KEY UPDATE ranking=VALUES(ranking);";
			$db_main2->execute($insert_sql);
		}
		
		$sql = "SELECT  @ROWNUM := @ROWNUM+1 AS ROWNUM, t1.* ".
				"FROM ".
				"(	".
				"	SELECT slottype FROM tbl_slot_ranking_normal_info ORDER BY ranking DESC ".
				") t1, (SELECT @ROWNUM := 0) R";
		$ranking_num_info = $db_main2->gettotallist($sql);
		
		for($i = 0; $i<sizeof($ranking_num_info); $i++)
		{
			$ranking_num_slottype = $ranking_num_info[$i]["slottype"];
			$rownum = $ranking_num_info[$i]["ROWNUM"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_normal_info(slottype, ranking_num) VALUES($ranking_num_slottype, $rownum) ".
							"ON DUPLICATE KEY UPDATE ranking_num=VALUES(ranking_num);";
			$db_main2->execute($insert_sql);
		}
		
		$sql = "SELECT * FROM tbl_slot_ranking_normal_info";
		$ranking_info_log = $db_main2->gettotallist($sql);
		
		for($i = 0; $i<sizeof($ranking_info_log); $i++)
		{
			$slottype = $ranking_info_log[$i]["slottype"];
			$step_1 = $ranking_info_log[$i]["step_1"];
			$step_2 = $ranking_info_log[$i]["step_2"];
			$ranking = $ranking_info_log[$i]["ranking"];
			$ranking_num = $ranking_info_log[$i]["ranking_num"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_normal_info_log(today, slottype, step_1, step_2, ranking, ranking_num) VALUES('$today', $slottype, $step_1, $step_2, $ranking, $ranking_num) ".
							"ON DUPLICATE KEY UPDATE step_1=VALUES(step_1), step_2=VALUES(step_2), ranking=VALUES(ranking), ranking_num=VALUES(ranking_num);";
			$db_main2->execute($insert_sql);
		}
		
		//최신 슬롯이 3위 아래에 있으면 3위로 만들어주는 로직
		$sql = "SELECT ranking_num, slottype FROM tbl_slot_ranking_normal_info WHERE slottype = (SELECT MAX(slottype) FROM tbl_slot_ranking_normal_info);";
		$newslot_rank_info = $db_main2->getarray($sql);
		
		$newslot_rank = $newslot_rank_info["ranking_num"];
		$newslot_type = $newslot_rank_info["slottype"];
		
		if($newslot_rank > 3)
		{
			$insert_sql = "UPDATE tbl_slot_ranking_normal_info SET ranking_num = ranking_num+1 WHERE slottype!=$newslot_type AND ranking_num >= 3;";
			$insert_sql .= "UPDATE tbl_slot_ranking_normal_info SET ranking_num = 3 WHERE slottype=$newslot_type;";
			$insert_sql .= "UPDATE tbl_slot_ranking_normal_info SET ranking_num = ranking_num-1 WHERE ranking_num > $newslot_rank;";
			
			$db_main2->execute($insert_sql);
		}
		
		//2. Highroller
		// 전체 - ODS 제외
		$sql = "SELECT slottype, sum(playcount*(betlevel+1)) as step_1 ".
				"FROM t5_user_gamelog ".
				"WHERE writedate >= '$before_14day 00:00:00' AND MODE NOT IN (4, 5, 9, 26) AND slottype > 0 AND betlevel >= 10 AND slottype NOT IN ($ods_list) ".
				"GROUP BY slottype ";
		$slot_info_step1 = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i<sizeof($slot_info_step1); $i++)
		{
			$step1_slottype = $slot_info_step1[$i]["slottype"];
			$step1_amount = $slot_info_step1[$i]["step_1"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_highroller_info(slottype, step_1) VALUES($step1_slottype, $step1_amount) ".
							"ON DUPLICATE KEY UPDATE step_1=VALUES(step_1);";
			$db_main2->execute($insert_sql);
		}
		
		// 전체 - ODS만
		$sql = "SELECT 25 as slottype, AVG(step_1) as step_1 ".
				"FROM ".
				"(	".
				"	SELECT slottype, sum(playcount*(betlevel+1)) as step_1 ".
				"	FROM t5_user_gamelog ".
				"	WHERE writedate >= '$before_14day 00:00:00' AND MODE NOT IN (4, 5, 9, 26) AND betlevel >= 10 AND slottype IN ($ods_list) ".
				"	GROUP BY slottype ".
				"	ORDER BY step_1 DESC LIMIT 3 ".
				") t1";
		$slot_info_step1 = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i<sizeof($slot_info_step1); $i++)
		{
			$step1_slottype = $slot_info_step1[$i]["slottype"];
			$step1_amount = $slot_info_step1[$i]["step_1"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_highroller_info(slottype, step_1) VALUES($step1_slottype, $step1_amount) ".
						"ON DUPLICATE KEY UPDATE step_1=VALUES(step_1);";
			$db_main2->execute($insert_sql);
		}
		
		//결제 - ODS 제외
		$sql = "SELECT slottype, sum(playcount*(betlevel+1)) as step_2 ".
				"FROM ( ".
				"	select distinct useridx ".
  				"	from t5_product_order ".
  				"	where useridx > 20000 and status and writedate >= '$before_60day 00:00:00' ".
		  		") t1 join t5_user_gamelog as t2 on t1.useridx = t2.useridx	".
				"WHERE writedate >= '$before_14day 00:00:00' AND MODE NOT IN (4, 5, 9, 26) AND betlevel >= 10 AND slottype > 0 AND slottype NOT IN ($ods_list)	".
				"GROUP BY slottype";
		$slot_info_step2 = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i<sizeof($slot_info_step2); $i++)
		{
			$step2_slottype = $slot_info_step2[$i]["slottype"];
			$step2_amount = $slot_info_step2[$i]["step_2"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_highroller_info(slottype, step_2) VALUES($step2_slottype, $step2_amount) ".
							"ON DUPLICATE KEY UPDATE step_2=VALUES(step_2);";
			$db_main2->execute($insert_sql);
		}
		
		//결제 - ODS만	
		$sql = "SELECT 25 AS slottype, avg(step_2) as step_2 ".
				"FROM ".
				"(	".
				"	SELECT slottype, sum(playcount*(betlevel+1)) as step_2 ".
				"	FROM ( ".
				"		SELECT DISTINCT useridx ".
				"		FROM t5_product_order ".
				"		where useridx > 20000 and status and writedate >= '$before_60day 00:00:00' ".
				"	)t1 join t5_user_gamelog as t2 on t1.useridx = t2.useridx ".
				" 	WHERE writedate >= '$before_14day 00:00:00' AND MODE NOT IN (4, 5, 9, 26) AND betlevel >= 10 AND slottype IN ($ods_list) ".
				"	GROUP BY slottype ".
				"	ORDER BY step_2 DESC LIMIT 3 ".
				") t1";
		$slot_info_step2 = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i<sizeof($slot_info_step2); $i++)
		{
			$step2_slottype = $slot_info_step2[$i]["slottype"];
			$step2_amount = $slot_info_step2[$i]["step_2"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_highroller_info(slottype, step_2) VALUES($step2_slottype, $step2_amount) ".
							"ON DUPLICATE KEY UPDATE step_2=VALUES(step_2);";
			$db_main2->execute($insert_sql);
		}
		
		// MAX(전체), MAX(결제) 일치하도록 Normalize
		$sql = "SELECT ROUND(MAX(step_1)/MAX(step_2),0) AS multiple FROM `tbl_slot_ranking_highroller_info`";
		$ranking_multiple = $db_main2->getvalue($sql);
		
		$sql = "SELECT slottype, step_2 * $ranking_multiple AS step2_balance FROM tbl_slot_ranking_highroller_info";
		$ranking_step2_balance = $db_main2->gettotallist($sql);
		
		for($i = 0; $i<sizeof($ranking_step2_balance); $i++)
		{
			$step2_balance_slottype = $ranking_step2_balance[$i]["slottype"];
			$step2_balance_amount = $ranking_step2_balance[$i]["step2_balance"];
					
			$insert_sql = "INSERT INTO tbl_slot_ranking_highroller_info(slottype, step_2) VALUES($step2_balance_slottype, $step2_balance_amount) ".
							"ON DUPLICATE KEY UPDATE step_2=VALUES(step_2);";
			$db_main2->execute($insert_sql);
		}
		
		//전체 X 4 + 결제 X 6 하여 최종 순위값 도출
		$sql = "SELECT slottype, ((step_1*4) +  (step_2*6)) AS ranking FROM `tbl_slot_ranking_highroller_info`";
		$ranking_info = $db_main2->gettotallist($sql);
		
		for($i = 0; $i<sizeof($ranking_info); $i++)
		{
			$ranking_slottype = $ranking_info[$i]["slottype"];
			$ranking_amount = $ranking_info[$i]["ranking"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_highroller_info(slottype, ranking) VALUES($ranking_slottype, $ranking_amount) ".
						"ON DUPLICATE KEY UPDATE ranking=VALUES(ranking);";
			$db_main2->execute($insert_sql);
		}
		
		$sql = "SELECT  @ROWNUM := @ROWNUM+1 AS ROWNUM, t1.* ".
				"FROM ".
				"(	".
				"	SELECT slottype FROM tbl_slot_ranking_highroller_info ORDER BY ranking DESC ".
				") t1, (SELECT @ROWNUM := 0) R";
		$ranking_num_info = $db_main2->gettotallist($sql);
		
		for($i = 0; $i<sizeof($ranking_num_info); $i++)
		{
			$ranking_num_slottype = $ranking_num_info[$i]["slottype"];
			$rownum = $ranking_num_info[$i]["ROWNUM"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_highroller_info(slottype, ranking_num) VALUES($ranking_num_slottype, $rownum) ".
							"ON DUPLICATE KEY UPDATE ranking_num=VALUES(ranking_num);";
			$db_main2->execute($insert_sql);
		}
		
		$sql = "SELECT * FROM tbl_slot_ranking_highroller_info";
		$ranking_info_log = $db_main2->gettotallist($sql);
		
		for($i = 0; $i<sizeof($ranking_info_log); $i++)
		{
			$slottype = $ranking_info_log[$i]["slottype"];
			$step_1 = $ranking_info_log[$i]["step_1"];
			$step_2 = $ranking_info_log[$i]["step_2"];
			$ranking = $ranking_info_log[$i]["ranking"];
			$ranking_num = $ranking_info_log[$i]["ranking_num"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_highroller_info_log(today, slottype, step_1, step_2, ranking, ranking_num) VALUES('$today', $slottype, $step_1, $step_2, $ranking, $ranking_num) ".
							"ON DUPLICATE KEY UPDATE step_1=VALUES(step_1), step_2=VALUES(step_2), ranking=VALUES(ranking), ranking_num=VALUES(ranking_num);";
			$db_main2->execute($insert_sql);
		}
		
		//최신 슬롯이 3위 아래에 있으면 3위로 만들어주는 로직
		$sql = "SELECT ranking_num, slottype FROM tbl_slot_ranking_highroller_info WHERE slottype = (SELECT MAX(slottype) FROM tbl_slot_ranking_highroller_info);";
		$newslot_rank_info = $db_main2->getarray($sql);
		
		$newslot_rank = $newslot_rank_info["ranking_num"];
		$newslot_type = $newslot_rank_info["slottype"];
		
		if($newslot_rank > 3)
		{
			$insert_sql = "UPDATE tbl_slot_ranking_highroller_info SET ranking_num = ranking_num+1 WHERE slottype!=$newslot_type AND ranking_num >= 3;";
			$insert_sql .= "UPDATE tbl_slot_ranking_highroller_info SET ranking_num = 3 WHERE slottype=$newslot_type;";
			$insert_sql .= "UPDATE tbl_slot_ranking_highroller_info SET ranking_num = ranking_num-1 WHERE ranking_num > $newslot_rank;";
			$db_main2->execute($insert_sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
	$db_redshift->end();
?>