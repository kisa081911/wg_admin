<?
    include("../../common/common_include.inc.php");
    
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    $db_friend = new CDatabase_Friend();
    $db_livestats = new CDatabase_Livestats();
    $db_inbox = new CDatabase_Inbox();
    
    $facebook = new Facebook(array(
        'appId'  => FACEBOOK_APP_ID,
        'secret' => FACEBOOK_SECRET_KEY,
        'cookie' => true,
    ));
    
    $post_body = file_get_contents('php://input');
    $obj = json_decode($post_body, true);
    $productidx = "0";
    
    $sql = "SELECT * FROM tbl_product_order  WHERE currency != 'USD' AND currency != '' AND writedate < '2019-09-18 00:00:00' AND STATUS = 1";
    $order_abuse_log = $db_main->gettotallist($sql);
    
    for($l=0;$l<sizeof($order_abuse_log);$l++)
    {
        $order_id = $order_abuse_log[$l]['orderno'];
        $useridx = $order_abuse_log[$l]['useridx'];
        $productkey = $order_abuse_log[$l]['productkey'];
        
        try
        {
            write_log("/$order_id?fields=user,actions,refundable_amount,items,disputes");
            $order_info = $facebook->api("/$order_id?fields=user,actions,refundable_amount,items,disputes");
            
            if(sizeof($order_info["user"]) > 0)
            {
                $userid = $order_info["user"]["id"];
            }
            
            if(sizeof($order_info["actions"]) > 0)
            {
                $action_info = $order_info["actions"][0];
                
                $type = $action_info["type"];
                $status = $action_info["status"];
                $amount = $action_info["amount"];
                
                $currency = $action_info["currency"];
                $currency_amount = $amount;
            }
            
            if(sizeof($order_info["actions"]) > 1)
            {
                $chargeback_info = $order_info["actions"][1];
                
                $type = $chargeback_info["type"];
                $status = $chargeback_info["status"];
                
                $chargeback_currency = $chargeback_info["currency"];
                $chargeback_amount = $chargeback_info["amount"];
            }
            
            if(sizeof($order_info["items"]) > 0)
            {
                $item_info = $order_info["items"][0];
                
                $product_url = $item_info["product"];
                
                $productkey_info = explode('=', $product_url);
                $productkey = $productkey_info[1];
            }
        }
        catch (FacebookApiException $e)
        {
            $ipaddress = get_ipaddress();
            
            $params = array('cmd'=>'facebook_error_log', 'ipaddress'=>$ipaddress, 'facebookid'=>$v['uid'], 'data_cmd'=>401, 'errcode'=>$e->getMessage());
            log_node_async("log", $params);
            
            write_log($e->getMessage());
        }
        
        if($type == "charge" && $status == "completed")
        {
            $sql = "SELECT productidx, product_type, vip_point, special_more, special_discount FROM tbl_product WHERE productkey='$productkey';";
            $order_info = $db_main->getarray($sql);
            
            $productidx = $order_info["productidx"];
            $product_type = $order_info["product_type"];
            $special_more = $order_info["special_more"];
            $special_discount = $order_info["special_discount"];
            $product_vip_point = $order_info["vip_point"];
            
            if ($productkey != "")
            {
                $sql = "SELECT productidx,category,amount,special_more,special_discount, facebookcredit, IF(category = 9, FLOOR(ROUND((facebookcredit/10) / ((100 - special_discount) / 100))*10), facebookcredit) AS targetcredit  FROM tbl_product WHERE productidx=$productidx";
                $info = $db_main->getarray($sql);
                
                if ($info["productidx"] == "")
                {
                    exit();
                }
                
                $category = $info["category"];
                $amount = $info["amount"];
                $product_facebookcredit = $info["facebookcredit"];
                $product_targetcredit  = $info["targetcredit"];
                
                if($category == 0) // Coins - Base Products
                {
                    if($couponidx != 0)
                        $special_more = $coupon_more;
                        
                        $basecoin = $amount;
                        $coin = ceil($amount * (100 + $special_more) / 100);
                }
                else if($category == 1 || $category == 2) //first products, second products
                {
                    $basecoin = $amount;
                    $coin = $amount;
                }
                else if($category == 3)
                {
                    if($couponidx != 0)
                        $special_more = $coupon_more;
                        
                        $recommend_product_info = $product_facebookcredit/10;
                        
                        $sql = "SELECT max_pot, IF(recommend_product = $recommend_product_info, multiple_1, multiple_2) AS multiple  FROM tbl_user_piggy_pot_".($useridx%10)." WHERE useridx = $useridx";
                        $piggy_pot_product_coin_info = $db_main2->getarray($sql);
                        
                        $piggy_pot_max_pot = $piggy_pot_product_coin_info["max_pot"];
                        $piggy_pot_multiple = $piggy_pot_product_coin_info["multiple"];
                        
                        $amount = $piggy_pot_max_pot * $piggy_pot_multiple;
                        
                        $basecoin = $amount;
                        
                        $coin = ceil($amount * (100 + $special_more) / 100);
                }
                else if($category == 4)
                {
                    if($couponidx != 0)
                        $special_more = $coupon_more;
                        
                        $basecoin = ceil($amount * (100 + $special_more) / 100);
                        
                        $recommend_product_info = $product_facebookcredit/10;
                        
                        $sql = "SELECT max_pot, IF(recommend_product = $recommend_product_info, multiple_1, multiple_2) AS multiple, IF(recommend_product = $recommend_product_info, 1, 2) AS location_product FROM tbl_user_piggy_pot_".($useridx%10)." WHERE useridx = $useridx";
                        $piggy_pot_product_coin_info = $db_main2->getarray($sql);
                        
                        $piggy_pot_max_pot = $piggy_pot_product_coin_info["max_pot"];
                        $piggy_pot_multiple = $piggy_pot_product_coin_info["multiple"];
                        $piggy_pot_location_product = $piggy_pot_product_coin_info["location_product"];
                        
                        $basecoin_piggy_pot = $piggy_pot_max_pot * ceil($piggy_pot_multiple * (100 + $special_more) / 100);
                        
                        $coin = $basecoin + $basecoin_piggy_pot;
                        
                        $basecoin = $amount;
                }
                else if($category == 8)
                {
                    $basecoin = $amount;
                    $coin = ceil($amount * (100 + $special_more) / 100);
                }
                else if($category == 9)
                {
                    $coin = $amount;
                    
                    $product_basecoin_arr = array(
                        array("facebookcredit"=>"10", "basecoin"=>"300000"),
                        array("facebookcredit"=>"50", "basecoin"=>"400000"),
                        array("facebookcredit"=>"90", "basecoin"=>"500000"),
                        array("facebookcredit"=>"190", "basecoin"=>"600000"),
                        array("facebookcredit"=>"390", "basecoin"=>"750000"),
                        array("facebookcredit"=>"590", "basecoin"=>"1200000"),
                        array("facebookcredit"=>"990", "basecoin"=>"2000000"),
                        array("facebookcredit"=>"1990", "basecoin"=>"2700000"),
                        array("facebookcredit"=>"2990", "basecoin"=>"3500000"),
                        array("facebookcredit"=>"4990", "basecoin"=>"4500000")
                    );
                    
                    if($product_targetcredit == 50)
                    {
                        // $9 Discount( $5 ~ $9 )
                        
                        $min_facebookcredit = 10;
                        $max_facebookcredit = 50;
                    }
                    else if($product_targetcredit == 90)
                    {
                        // $9 Discount( $5 ~ $9 )
                        
                        $min_facebookcredit = 50;
                        $max_facebookcredit = 90;
                    }
                    else if($product_targetcredit == 190)
                    {
                        // $19 Discount ( $9 ~ $19 )
                        
                        $min_facebookcredit = 90;
                        $max_facebookcredit = 190;
                    }
                    else if($product_targetcredit == 390)
                    {
                        // $39 Discount ( $19 ~ $39 )
                        
                        $min_facebookcredit = 190;
                        $max_facebookcredit = 390;
                    }
                    else if($product_targetcredit == 590)
                    {
                        // $59 Discount ( $39 ~ $59 )
                        
                        $min_facebookcredit = 390;
                        $max_facebookcredit = 590;
                    }
                    else if($product_targetcredit == 990)
                    {
                        // $99 Discount ( $59 ~ $99 )
                        
                        $min_facebookcredit = 590;
                        $max_facebookcredit = 990;
                    }
                    else if($product_targetcredit == 1990)
                    {
                        // $199 Discount ( $99 ~ $199 )
                        
                        $min_facebookcredit = 990;
                        $max_facebookcredit = 1990;
                    }
                    else if($product_targetcredit == 2990)
                    {
                        // $299 Discount ( $199 ~ $299 )
                        
                        $min_facebookcredit = 1990;
                        $max_facebookcredit = 2990;
                    }
                    else if($product_targetcredit == 4990)
                    {
                        // $499 Discount ( $299 ~ $499 )
                        
                        $min_facebookcredit = 2990;
                        $max_facebookcredit = 4990;
                    }
                    
                    for($i=0; $i<sizeof($product_basecoin_arr); $i++)
                    {
                        if($product_basecoin_arr[$i]["facebookcredit"] == $min_facebookcredit)
                            $min_basecoin = $product_basecoin_arr[$i]["basecoin"];
                    }
                    
                    for($i=0; $i<sizeof($product_basecoin_arr); $i++)
                    {
                        if($product_basecoin_arr[$i]["facebookcredit"] == $max_facebookcredit)
                            $max_basecoin = $product_basecoin_arr[$i]["basecoin"];
                    }
                    
                    $per_facebookcredit = $max_facebookcredit - $min_facebookcredit;
                    $per_basecoin = $max_basecoin - $min_basecoin;
                    
                    $discount_facebookcredit = $facebookcredit - $min_facebookcredit;
                    
                    $add_basecoin = ($per_basecoin/$per_facebookcredit)*$discount_facebookcredit;
                    
                    $discount_basecoin = $min_basecoin + $add_basecoin;
                    
                    
                    $basecoin = $discount_basecoin*($facebookcredit/10);
                }
            }
                
            $sql = "SELECT * FROM tbl_user_speedwheel_info WHERE orderno='$order_id' AND useridx=$useridx";
            $speedwheel_info_arr = $db_main2->getarray($sql);
            $speedwheel_info_orderidx = ($speedwheel_info_arr["orderno"] == "")? 0 : $speedwheel_info_arr["orderno"];
            
            if($product_type == 19 && $speedwheel_info_orderidx == 0)
            {
                
                $seed = 0;
                $seed  = rand(0, 10000);
                
                if($seed >= 0 && $seed < 1250)
                    $speedwheel_amount = 200000;
                else if($seed >= 1250 && $seed < 1875)
                    $speedwheel_amount = 220000;
                else if($seed >= 1875 && $seed < 2500)
                    $speedwheel_amount = 230000;
                else if($seed >= 2500 && $seed < 3700)
                    $speedwheel_amount = 300000;
                else if($seed >= 3700 && $seed < 4950)
                    $speedwheel_amount = 350000;
                else if($seed >= 4950 && $seed < 6200)
                    $speedwheel_amount = 400000;
                else if($seed >= 6200 && $seed < 7450)
                    $speedwheel_amount = 420000;
                else if($seed >= 7450 && $seed < 8075)
                    $speedwheel_amount = 430000;
                else if($seed >= 8075 && $seed < 8700)
                    $speedwheel_amount = 450000;
                else if($seed >= 8700   && $seed < 9325)
                    $speedwheel_amount = 700000;
                else if($seed >= 9325   && $seed < 10000)
                    $speedwheel_amount = 900000;
                    
                $sql = "SELECT count(*) FROM tbl_user_friend_".($useridx%20)." WHERE useridx = $useridx";
                $totalfriend = $db_friend->getvalue($sql);
                $totalfriend = ($totalfriend < 0)? 0 : $totalfriend;
                
                $sql = "SELECT count(*) FROM tbl_user_friend_".($useridx%20)." a, tbl_user_gamedata b WHERE a.friendidx = b.useridx AND logindate > date_sub(now(), interval 2 day) AND a.useridx = $useridx";
                $activefriend = $db_friend->getvalue($sql);
                $activefriend = ($activefriend < 0)? 0 : $activefriend;
                
                $friendbonus = (($totalfriend > 30)? 30 : $totalfriend) * 1000;
                $activefriendbonus = (($activefriend > 100)? 100 : $activefriend) * 300;
                
                if ($activefriendbonus > 30000)
                    $activefriendbonus = 30000;
                    
                $coin = $speedwheel_amount  + $friendbonus + $activefriendbonus;
                
                $sql = "UPDATE tbl_user_speedwheel_info SET reward_amount = $coin, buydate = NOW(),speedwheel_amount=$speedwheel_amount,friendbonus=$friendbonus,activefriendbonus= $activefriendbonus,orderno='$order_id' WHERE useridx = $useridx";
                $db_main2->execute($sql);
                
                $wheel_remaintime = 21600;
                
                $basecoin = $coin;
            }
            else if ($product_type == 19 && $speedwheel_info_orderidx > 0)
            {
                $speedwheel_amount = ($speedwheel_info_arr["speedwheel_amount"] == "")? 0 : $speedwheel_info_arr["speedwheel_amount"];
                $friendbonus = ($speedwheel_info_arr["friendbonus"] == "")? 0 : $speedwheel_info_arr["friendbonus"];
                $activefriendbonus = ($speedwheel_info_arr["activefriendbonus"] == "")? 0 : $speedwheel_info_arr["activefriendbonus"];
                $coin = $speedwheel_amount  + $friendbonus + $activefriendbonus;
                $wheel_remaintime = 21600;
                
                $basecoin = $coin;
            }
    
    
            if($category == 0) // 
            {
                $sql = "INSERT INTO tbl_user_cache_add(useridx, coin, writedate) VALUES($useridx, $coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate = VALUES(writedate);";
                write_log($sql);
                $db_main2->execute($sql);
            }
            else if($category == 1 || $category == 2  || $category == 9) 
            {
                $sql = "INSERT INTO tbl_user_cache_add(useridx, coin, writedate) VALUES($useridx, $coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate = VALUES(writedate);";
                write_log($sql);
                $db_main2->execute($sql);
            }
            else if($category == 3 || $category == 4) // piggypot
            {
                $sql = "INSERT INTO tbl_user_cache_add(useridx, coin, writedate) VALUES($useridx, $coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate = VALUES(writedate);";
                write_log($sql);
                $db_main2->execute($sql);
            }
        
            else if($product_targetcredit >= 50)
            {
                $sql = "UPDATE tbl_user_first_buy SET otherbuy = 1, otherbuy2 = 1 WHERE useridx =  $useridx";
                write_log($sql);
                $db_main->execute($sql);
            }
            else
            {
                $sql = "UPDATE tbl_user_first_buy SET otherbuy2 = 1 WHERE useridx =  $useridx";
                write_log($sql);
                $db_main->execute($sql);
            }
        
            $sql = "SELECT vip_level FROM tbl_user_detail WHERE useridx=$useridx";
            $before_viplevel = $db_main->getvalue($sql);
        
            $before_viplevel = ($before_viplevel == "") ? 0 : $before_viplevel;
        
            // VIP Point Update
            $sql = "SELECT ".
                   " (SELECT IFNULL(SUM(vip_point), 0) AS vip_point FROM tbl_product_order WHERE useridx = $useridx AND status = 1) + ".
                   " (SELECT IFNULL(SUM(vip_point), 0) AS vip_point FROM tbl_product_order_mobile WHERE useridx = $useridx AND status = 1) AS total_vip_point";
            $vip_point = $db_main->getvalue($sql);
        
            $vip_level = get_vip_level($vip_point);
                
            $sql = "UPDATE tbl_user_detail SET vip_level=$vip_level, vip_point=$vip_point WHERE useridx = $useridx;";
            write_log($sql);
            $db_main->execute($sql);
                
            $sql = "UPDATE tbl_user_detail SET vip_level=$vip_level, vip_point=$vip_point WHERE useridx = $useridx;";
            write_log($sql);
            $db_friend->execute($sql);
                
            $sql = "SELECT ".
                "(SELECT IFNULL(SUM(facebookcredit), 0) AS totalcredit FROM tbl_product_order WHERE useridx = $useridx AND status = 1) + ".
                "(SELECT IFNULL(SUM(facebookcredit), 0) AS totalcredit FROM tbl_product_order_mobile WHERE useridx = $useridx AND status = 1);";
            $totalcredit = $db_main->getvalue($sql);
            
            if($vip_level > 0)
            {
                if($before_viplevel < $vip_level)
                {
                    // VIP Level Up�떆
                    
                    $sql = "SELECT member_level, member_reward FROM tbl_membership_info WHERE $before_viplevel < member_level AND member_level <= $vip_level";
                    $vip_reward_info = $db_main->gettotallist($sql);
                    
                    $vip_reward = 0;
                    
                    for($i=0; $i<sizeof($vip_reward_info); $i++)
                    {
                        $info_level = $vip_reward_info[$i]["member_level"];
                        $info_reward = $vip_reward_info[$i]["member_reward"];
                        
                        $sql = "INSERT tbl_user_vip_levelup_log(useridx, vip_level, reward, collect_status,writedate) VALUES($useridx, $info_level, $info_reward, 1,NOW());";
                        write_log($sql);
                        $db_main2->execute($sql);
                        
                        $vip_reward += $info_reward;
                    }
                    
                    $sql ="INSERT INTO tbl_user_cache_add(useridx, coin, writedate) VALUE($useridx, $vip_reward, NOW())".
                        "ON DUPLICATE KEY UPDATE coin = coin + $vip_reward, writedate = NOW()";
                    write_log($sql);
                    $db_main2->execute($sql);
                    
                    $is_v2 = $db_main->getvalue("SELECT is_v2 FROM tbl_user_switch WHERE useridx = $useridx");
                    $is_v2 = ($is_v2 == "")? 0 : $is_v2;
                    
                    $sql="insert into tbl_user_freecoin_log_".($useridx%10)." (useridx, category, type, amount, inbox_type, is_v2, writedate) values ($useridx, 0, 4, $vip_reward, 0, $is_v2, now());";
                    write_log($sql);
                    $db_main2->execute($sql);
                    
                }
            }
    
            //Premium Booster
            if($product_targetcredit == 390 )
            {
                
                $premium_type = 2;
                $sql = "UPDATE tbl_user_detail SET ispremium = 1 WHERE useridx = $useridx";
                $db_main->execute($sql);
                
                $sql = " INSERT INTO tbl_user_premium_booster(useridx, premium_type, os_type, end_date, writedate) ".
                    " VALUES($useridx, $premium_type, 0, DATE_ADD(NOW(), INTERVAL 7 DAY),now()) ON DUPLICATE KEY UPDATE end_date = IF((end_date < NOW()), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(end_date , INTERVAL 7 DAY)), os_type=VALUES(os_type);";
                write_log($sql);
                $db_main2->execute($sql);
                
            }
            //Bonus Logic
            if($product_targetcredit == 390 && $productidx != 1261)
            {
                $bonus_coin = 700000;
                
                $sql = "INSERT INTO tbl_user_buy_3day_gift(useridx, userid, platform, orderno, coin, purchasedate, bonus_coin, leftcount, senddate) VALUES($useridx, '$userid', 0, $order_id, $coin, NOW(), $bonus_coin, 3, DATE_ADD(NOW(), INTERVAL 1 DAY));";
                write_log($sql);
                $db_main2->execute($sql);
            }
            else if($product_targetcredit == 590 )
            {
                $bonus_coin = 1200000;
                
                $sql = "INSERT INTO tbl_user_buy_3day_gift(useridx, userid, orderno, coin, purchasedate, bonus_coin, leftcount, senddate) VALUES($useridx, '$userid', $order_id, $coin, NOW(), $bonus_coin, 3, DATE_ADD(NOW(), INTERVAL 1 DAY));";
                write_log($sql);
                $db_main2->execute($sql);
                
                $sql = "SELECT IF(NOW() < spindateX4, 1, 0) AS exist, spindateX4  FROM tbl_user_buy_7day_gift WHERE useridx = $useridx;";
                $gift_x4_info = $db_main2->getarray($sql);
                
                $gift_x4_exist = $gift_x4_info["exist"];
                $gift_x4_time = $gift_x4_info["spindateX4"];
                
                if($gift_x4_exist == "" || $gift_x4_exist == 0)
                {
                    $sql = "INSERT INTO tbl_user_buy_7day_gift(useridx, userid, spindate, writedate) VALUES($useridx, '$userid', DATE_ADD(NOW(), INTERVAL 7 DAY), NOW()) ".
                        "ON DUPLICATE KEY UPDATE spindate = DATE_ADD(IF(spindate < NOW(), NOW(), spindate), INTERVAL 7 DAY), writedate=VALUES(writedate);";
                    write_log($sql);
                    $db_main2->execute($sql);
                }
                else
                {
                    $sql = "INSERT INTO tbl_user_buy_7day_gift(useridx, userid, spindate, writedate) VALUES($useridx, '$userid', DATE_ADD('$gift_x4_time', INTERVAL 7 DAY), NOW()) ".
                        "ON DUPLICATE KEY UPDATE spindate = DATE_ADD(IF(spindate > '$gift_x4_time', spindate, '$gift_x4_time'), INTERVAL 7 DAY), writedate=VALUES(writedate);";
                    write_log($sql);
                    $db_main2->execute($sql);
                }
                
                //Premium Booster
                $premium_type = 2;
                
                $sql = "UPDATE tbl_user_detail SET ispremium = 1 WHERE useridx = $useridx";
                write_log($sql);
                $db_main->execute($sql);
                
                $sql = " INSERT INTO tbl_user_premium_booster(useridx, premium_type, os_type, end_date, writedate) ".
                    " VALUES($useridx, $premium_type, 0, DATE_ADD(NOW(), INTERVAL 10 DAY),now()) ON DUPLICATE KEY UPDATE end_date = IF((end_date < NOW()), DATE_ADD(NOW(), INTERVAL 10 DAY), DATE_ADD(end_date , INTERVAL 10 DAY)), os_type=VALUES(os_type);";
                write_log($sql);
                $db_main2->execute($sql);
            }
            else if($product_targetcredit == 990 )
            {
                $bonus_coin = 2300000;
                
                $sql = "INSERT INTO tbl_user_buy_3day_gift(useridx, userid, orderno, coin, purchasedate, bonus_coin, leftcount, senddate) VALUES($useridx, '$userid', $order_id, $coin, NOW(), $bonus_coin, 3, DATE_ADD(NOW(), INTERVAL 1 DAY));";
                write_log($sql);
                $db_main2->execute($sql);
                
                $sql = "SELECT IF(NOW() < spindateX4, 1, 0) AS exist, spindateX4  FROM tbl_user_buy_7day_gift WHERE useridx = $useridx;";
                $gift_x4_info = $db_main2->getarray($sql);
                
                $gift_x4_exist = $gift_x4_info["exist"];
                $gift_x4_time = $gift_x4_info["spindateX4"];
                
                if($gift_x4_exist == "" || $gift_x4_exist == 0)
                {
                    $sql = "INSERT INTO tbl_user_buy_7day_gift(useridx, userid, spindate, stampdate, writedate) VALUES($useridx, '$userid', DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), NOW()) ".
                        "ON DUPLICATE KEY UPDATE spindate = DATE_ADD(IF(spindate < NOW(), NOW(), spindate), INTERVAL 7 DAY), stampdate = DATE_ADD(IF(stampdate < NOW(), NOW(), stampdate), INTERVAL 7 DAY), writedate=VALUES(writedate);";
                    write_log($sql);
                    $db_main2->execute($sql);
                }
                else
                {
                    $sql = "INSERT INTO tbl_user_buy_7day_gift(useridx, userid, spindate, stampdate, writedate) VALUES($useridx, '$userid', DATE_ADD('$gift_x4_time', INTERVAL 7 DAY), DATE_ADD('$gift_x4_time', INTERVAL 7 DAY), NOW()) ".
                        "ON DUPLICATE KEY UPDATE spindate = DATE_ADD(IF(spindate > '$gift_x4_time', spindate, '$gift_x4_time'), INTERVAL 7 DAY), stampdate = DATE_ADD(IF(stampdate > '$gift_x4_time', stampdate, '$gift_x4_time'), INTERVAL 7 DAY), writedate=VALUES(writedate);";
                    write_log($sql);
                    $db_main2->execute($sql);
                }
                
                
                //Premium Booster
                $premium_type = 2;
                
                $sql = "UPDATE tbl_user_detail SET ispremium = 1 WHERE useridx = $useridx";
                write_log($sql);
                $db_main->execute($sql);
                
                $sql = " INSERT INTO tbl_user_premium_booster(useridx, premium_type, os_type, end_date, writedate) ".
                    " VALUES($useridx, $premium_type, 0, DATE_ADD(NOW(), INTERVAL 15 DAY),now()) ON DUPLICATE KEY UPDATE end_date = IF((end_date < NOW()), DATE_ADD(NOW(), INTERVAL 15 DAY), DATE_ADD(end_date , INTERVAL 15 DAY)), os_type=VALUES(os_type);";
                write_log($sql);
                $db_main2->execute($sql);
            }
            else if($product_targetcredit == 1990 )
            {
                $bonus_coin = 5000000;
                
                $sql = "INSERT INTO tbl_user_buy_3day_gift(useridx, userid, orderno, coin, purchasedate, bonus_coin, leftcount, senddate) VALUES($useridx, '$userid', $order_id, $coin, NOW(), $bonus_coin, 3, DATE_ADD(NOW(), INTERVAL 1 DAY));";
                write_log($sql);
                $db_main2->execute($sql);
                
                $sql = "SELECT IF(NOW() < spindateX4, 1, 0) AS exist, spindateX4  FROM tbl_user_buy_7day_gift WHERE useridx = $useridx;";
                $gift_x4_info = $db_main2->getarray($sql);
                
                $gift_x4_exist = $gift_x4_info["exist"];
                $gift_x4_time = $gift_x4_info["spindateX4"];
                
                if($gift_x4_exist == "" || $gift_x4_exist == 0)
                {
                    $sql = "INSERT INTO tbl_user_buy_7day_gift(useridx, userid, spindate, stampdate, inboxdate, writedate) VALUES($useridx, '$userid', DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), NOW()) ".
                        "ON DUPLICATE KEY UPDATE spindate = DATE_ADD(IF(spindate < NOW(), NOW(), spindate), INTERVAL 7 DAY), stampdate = DATE_ADD(IF(stampdate < NOW(), NOW(), stampdate), INTERVAL 7 DAY), ".
                        "inboxdate = DATE_ADD(IF(inboxdate < NOW(), NOW(), inboxdate), INTERVAL 7 DAY), writedate=VALUES(writedate);";
                    write_log($sql);
                    $db_main2->execute($sql);
                }
                else
                {
                    $sql = "INSERT INTO tbl_user_buy_7day_gift(useridx, userid, spindate, stampdate, inboxdate, writedate) VALUES($useridx, '$userid', DATE_ADD('$gift_x4_time', INTERVAL 7 DAY), DATE_ADD('$gift_x4_time', INTERVAL 7 DAY), DATE_ADD('$gift_x4_time', INTERVAL 7 DAY), NOW()) ".
                        "ON DUPLICATE KEY UPDATE spindate = DATE_ADD(IF(spindate > '$gift_x4_time', spindate, '$gift_x4_time'), INTERVAL 7 DAY), stampdate = DATE_ADD(IF(stampdate > '$gift_x4_time', stampdate, '$gift_x4_time'), INTERVAL 7 DAY), ".
                        "inboxdate = DATE_ADD(IF(inboxdate > '$gift_x4_time', inboxdate, '$gift_x4_time'), INTERVAL 7 DAY), writedate=VALUES(writedate);";
                    write_log($sql);
                    $db_main2->execute($sql);
                }
                
                //Premium Booster
                $premium_type = 2;
                
                $sql = "UPDATE tbl_user_detail SET ispremium = 1 WHERE useridx = $useridx";
                write_log($sql);
                $db_main->execute($sql);
                
                $sql = " INSERT INTO tbl_user_premium_booster(useridx, premium_type, os_type, end_date, writedate) ".
                    " VALUES($useridx, $premium_type, 0, DATE_ADD(NOW(), INTERVAL 20 DAY),now()) ON DUPLICATE KEY UPDATE end_date = IF((end_date < NOW()), DATE_ADD(NOW(), INTERVAL 20 DAY), DATE_ADD(end_date , INTERVAL 20 DAY)), os_type=VALUES(os_type);";
                write_log($sql);
                $db_main2->execute($sql);
            }
            else if($product_targetcredit == 2990 )
            {
                $bonus_coin = 9000000;
                
                $sql = "INSERT INTO tbl_user_buy_3day_gift(useridx, userid, orderno, coin, purchasedate, bonus_coin, leftcount, senddate) VALUES($useridx, '$userid', $order_id, $coin, NOW(), $bonus_coin, 3, DATE_ADD(NOW(), INTERVAL 1 DAY));";
                write_log($sql);
                $db_main2->execute($sql);
                
                $sql = "SELECT spindate FROM tbl_user_buy_7day_gift WHERE useridx = $useridx;";
                $gift_x2 = $db_main2->getvalue($sql);
                
                if($gift_x2 == "" || $gift_x2 == "0000-00-00 00:00:00")
                {
                    $sql = "INSERT INTO tbl_user_buy_7day_gift(useridx, userid, spindate, stampdate, inboxdate, spindateX4, stampdateX4, inboxdateX4, writedate) ".
                        "VALUES($useridx, '$userid', DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), NOW()) ".
                        "ON DUPLICATE KEY UPDATE spindate = DATE_ADD(NOW(), INTERVAL 7 DAY), stampdate = DATE_ADD(NOW(), INTERVAL 7 DAY), inboxdate = DATE_ADD(NOW(), INTERVAL 7 DAY), ".
                        "spindateX4 = DATE_ADD(IF(spindateX4 < NOW(), NOW(), spindateX4), INTERVAL 7 DAY), stampdateX4 = DATE_ADD(IF(stampdateX4 < NOW(), NOW(), stampdateX4), INTERVAL 7 DAY), inboxdateX4 = DATE_ADD(IF(inboxdateX4 < NOW(), NOW(), inboxdateX4), INTERVAL 7 DAY), writedate=VALUES(writedate);";
                    write_log($sql);
                    $db_main2->execute($sql);
                }
                else
                {
                    $sql = "INSERT INTO tbl_user_buy_7day_gift(useridx, userid, spindateX4, stampdateX4, inboxdateX4, writedate) VALUES($useridx, '$userid', DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), NOW()) ".
                        "ON DUPLICATE KEY UPDATE spindate = IF(spindate > NOW(), DATE_ADD(spindate, INTERVAL 7 DAY), spindate), stampdate = IF(stampdate > NOW(), DATE_ADD(stampdate, INTERVAL 7 DAY), stampdate), inboxdate = IF(inboxdate > NOW(), DATE_ADD(inboxdate, INTERVAL 7 DAY), inboxdate), ".
                        "spindateX4 = DATE_ADD(IF(spindateX4 < NOW(), NOW(), spindateX4), INTERVAL 7 DAY), stampdateX4 = DATE_ADD(IF(stampdateX4 < NOW(), NOW(), stampdateX4), INTERVAL 7 DAY), inboxdateX4 = DATE_ADD(IF(inboxdateX4 < NOW(), NOW(), inboxdateX4), INTERVAL 7 DAY), writedate=VALUES(writedate);";
                    write_log($sql);
                    $db_main2->execute($sql);
                }
                
                //Premium Booster
                $premium_type = 2;
                
                $sql = "UPDATE tbl_user_detail SET ispremium = 1 WHERE useridx = $useridx";
                write_log($sql);
                $db_main->execute($sql);
                
                $sql = " INSERT INTO tbl_user_premium_booster(useridx, premium_type, os_type, end_date, writedate) ".
                    " VALUES($useridx, $premium_type, 0, DATE_ADD(NOW(), INTERVAL 25 DAY),now()) ON DUPLICATE KEY UPDATE end_date = IF((end_date < NOW()), DATE_ADD(NOW(), INTERVAL 25 DAY), DATE_ADD(end_date , INTERVAL 25 DAY)), os_type=VALUES(os_type);";
                write_log($sql);
                $db_main2->execute($sql);
                
            }
            else if($product_targetcredit == 4990 )
            {
                $bonus_coin = 16500000;
                
                $sql = "INSERT INTO tbl_user_buy_3day_gift(useridx, userid, platform, orderno, coin, purchasedate, bonus_coin, leftcount, senddate) VALUES($useridx, '$userid', 0, '$order_id', $coin, NOW(), $bonus_coin, 3, DATE_ADD(NOW(), INTERVAL 1 DAY));";
                write_log($sql);
                $db_main2->execute($sql);
                
                $sql = "SELECT spindate FROM tbl_user_buy_7day_gift WHERE useridx = $useridx;";
                $gift_x2 = $db_main2->getvalue($sql);
                
                if($gift_x2 == "" || $gift_x2 == "0000-00-00 00:00:00")
                {
                    $sql = "INSERT INTO tbl_user_buy_7day_gift(useridx, userid, spindate, stampdate, inboxdate, spindateX4, stampdateX4, inboxdateX4, writedate) ".
                        "VALUES($useridx, '$userid', DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), NOW()) ".
                        "ON DUPLICATE KEY UPDATE spindate = DATE_ADD(NOW(), INTERVAL 7 DAY), stampdate = DATE_ADD(NOW(), INTERVAL 7 DAY), inboxdate = DATE_ADD(NOW(), INTERVAL 7 DAY), ".
                        "spindateX4 = DATE_ADD(IF(spindateX4 < NOW(), NOW(), spindateX4), INTERVAL 7 DAY), stampdateX4 = DATE_ADD(IF(stampdateX4 < NOW(), NOW(), stampdateX4), INTERVAL 7 DAY), inboxdateX4 = DATE_ADD(IF(inboxdateX4 < NOW(), NOW(), inboxdateX4), INTERVAL 7 DAY), writedate=VALUES(writedate);";
                    write_log($sql);
                    $db_main2->execute($sql);
                }
                else
                {
                    $sql = "INSERT INTO tbl_user_buy_7day_gift(useridx, userid, spindateX4, stampdateX4, inboxdateX4, writedate) VALUES($useridx, '$userid', DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), NOW()) ".
                        "ON DUPLICATE KEY UPDATE spindate = IF(spindate > NOW(), DATE_ADD(spindate, INTERVAL 7 DAY), spindate), stampdate = IF(stampdate > NOW(), DATE_ADD(stampdate, INTERVAL 7 DAY), stampdate), inboxdate = IF(inboxdate > NOW(), DATE_ADD(inboxdate, INTERVAL 7 DAY), inboxdate), ".
                        "spindateX4 = DATE_ADD(IF(spindateX4 < NOW(), NOW(), spindateX4), INTERVAL 7 DAY), stampdateX4 = DATE_ADD(IF(stampdateX4 < NOW(), NOW(), stampdateX4), INTERVAL 7 DAY), inboxdateX4 = DATE_ADD(IF(inboxdateX4 < NOW(), NOW(), inboxdateX4), INTERVAL 7 DAY), writedate=VALUES(writedate);";
                    write_log($sql);
                    $db_main2->execute($sql);
                }
                
                //Premium Booster
                $premium_type = 2;
                
                $sql = "UPDATE tbl_user_detail SET ispremium = 1 WHERE useridx = $useridx";
                write_log($sql);
                $db_main->execute($sql);
                
                $sql = " INSERT INTO tbl_user_premium_booster(useridx, premium_type, os_type, end_date, writedate) ".
                    " VALUES($useridx, $premium_type, 0, DATE_ADD(NOW(), INTERVAL 30 DAY),now()) ON DUPLICATE KEY UPDATE end_date = IF((end_date < NOW()), DATE_ADD(NOW(), INTERVAL 30 DAY), DATE_ADD(end_date , INTERVAL 30 DAY)), os_type=VALUES(os_type);";
                write_log($sql);
                $db_main2->execute($sql);
            }
            else if($product_targetcredit == 7770)
            {
                $bonus_coin = 25900000;
                
                $sql = "INSERT INTO tbl_user_buy_3day_gift(useridx, userid, platform, orderno, coin, purchasedate, bonus_coin, leftcount, senddate) VALUES($useridx, '$userid', 0, '$order_id', $coin, NOW(), $bonus_coin, 3, DATE_ADD(NOW(), INTERVAL 1 DAY));";
                write_log($sql);
                $db_main2->execute($sql);
                
                $sql = "SELECT spindate FROM tbl_user_buy_7day_gift WHERE useridx = $useridx;";
                $gift_x2 = $db_main2->getvalue($sql);
                
                if($gift_x2 == "" || $gift_x2 == "0000-00-00 00:00:00")
                {
                    $sql = "INSERT INTO tbl_user_buy_7day_gift(useridx, userid, spindate, stampdate, inboxdate, spindateX4, stampdateX4, inboxdateX4, writedate) ".
                        "VALUES($useridx, '$userid', DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), NOW()) ".
                        "ON DUPLICATE KEY UPDATE spindate = DATE_ADD(NOW(), INTERVAL 7 DAY), stampdate = DATE_ADD(NOW(), INTERVAL 7 DAY), inboxdate = DATE_ADD(NOW(), INTERVAL 7 DAY), ".
                        "spindateX4 = DATE_ADD(IF(spindateX4 < NOW(), NOW(), spindateX4), INTERVAL 7 DAY), stampdateX4 = DATE_ADD(IF(stampdateX4 < NOW(), NOW(), stampdateX4), INTERVAL 7 DAY), inboxdateX4 = DATE_ADD(IF(inboxdateX4 < NOW(), NOW(), inboxdateX4), INTERVAL 7 DAY), writedate=VALUES(writedate);";
                    write_log($sql);
                    $db_main2->execute($sql);
                }
                else
                {
                    $sql = "INSERT INTO tbl_user_buy_7day_gift(useridx, userid, spindateX4, stampdateX4, inboxdateX4, writedate) VALUES($useridx, '$userid', DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), NOW()) ".
                        "ON DUPLICATE KEY UPDATE spindate = IF(spindate > NOW(), DATE_ADD(spindate, INTERVAL 7 DAY), spindate), stampdate = IF(stampdate > NOW(), DATE_ADD(stampdate, INTERVAL 7 DAY), stampdate), inboxdate = IF(inboxdate > NOW(), DATE_ADD(inboxdate, INTERVAL 7 DAY), inboxdate), ".
                        "spindateX4 = DATE_ADD(IF(spindateX4 < NOW(), NOW(), spindateX4), INTERVAL 7 DAY), stampdateX4 = DATE_ADD(IF(stampdateX4 < NOW(), NOW(), stampdateX4), INTERVAL 7 DAY), inboxdateX4 = DATE_ADD(IF(inboxdateX4 < NOW(), NOW(), inboxdateX4), INTERVAL 7 DAY), writedate=VALUES(writedate);";
                    write_log($sql);
                    $db_main2->execute($sql);
                }
                
                //Premium Booster
                $premium_type = 2;
                
                $sql = "UPDATE tbl_user_detail SET ispremium = 1 WHERE useridx = $useridx";
                write_log($sql);
                $db_main->execute($sql);
                
                $sql = " INSERT INTO tbl_user_premium_booster(useridx, premium_type, os_type, end_date, writedate) ".
                    " VALUES($useridx, $premium_type, 0, DATE_ADD(NOW(), INTERVAL 40 DAY),now()) ON DUPLICATE KEY UPDATE end_date = IF((end_date < NOW()), DATE_ADD(NOW(), INTERVAL 40 DAY), DATE_ADD(end_date , INTERVAL 40 DAY)), os_type=VALUES(os_type);";
                write_log($sql);
                $db_main2->execute($sql);
            }
            else if($product_targetcredit == 9990)
            {
                $bonus_coin = 33300000;
                
                $sql = "INSERT INTO tbl_user_buy_3day_gift(useridx, userid, platform, orderno, coin, purchasedate, bonus_coin, leftcount, senddate) VALUES($useridx, '$userid', 0, '$order_id', $coin, NOW(), $bonus_coin, 3, DATE_ADD(NOW(), INTERVAL 1 DAY));";
                write_log($sql);
                $db_main2->execute($sql);
                
                $sql = "SELECT spindate FROM tbl_user_buy_7day_gift WHERE useridx = $useridx;";
                $gift_x2 = $db_main2->getvalue($sql);
                
                if($gift_x2 == "" || $gift_x2 == "0000-00-00 00:00:00")
                {
                    $sql = "INSERT INTO tbl_user_buy_7day_gift(useridx, userid, spindate, stampdate, inboxdate, spindateX4, stampdateX4, inboxdateX4, writedate) ".
                        "VALUES($useridx, '$userid', DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), NOW()) ".
                        "ON DUPLICATE KEY UPDATE spindate = DATE_ADD(NOW(), INTERVAL 7 DAY), stampdate = DATE_ADD(NOW(), INTERVAL 7 DAY), inboxdate = DATE_ADD(NOW(), INTERVAL 7 DAY), ".
                        "spindateX4 = DATE_ADD(IF(spindateX4 < NOW(), NOW(), spindateX4), INTERVAL 7 DAY), stampdateX4 = DATE_ADD(IF(stampdateX4 < NOW(), NOW(), stampdateX4), INTERVAL 7 DAY), inboxdateX4 = DATE_ADD(IF(inboxdateX4 < NOW(), NOW(), inboxdateX4), INTERVAL 7 DAY), writedate=VALUES(writedate);";
                    write_log($sql);
                    $db_main2->execute($sql);
                }
                else
                {
                    $sql = "INSERT INTO tbl_user_buy_7day_gift(useridx, userid, spindateX4, stampdateX4, inboxdateX4, writedate) VALUES($useridx, '$userid', DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), DATE_ADD(NOW(), INTERVAL 7 DAY), NOW()) ".
                        "ON DUPLICATE KEY UPDATE spindate = IF(spindate > NOW(), DATE_ADD(spindate, INTERVAL 7 DAY), spindate), stampdate = IF(stampdate > NOW(), DATE_ADD(stampdate, INTERVAL 7 DAY), stampdate), inboxdate = IF(inboxdate > NOW(), DATE_ADD(inboxdate, INTERVAL 7 DAY), inboxdate), ".
                        "spindateX4 = DATE_ADD(IF(spindateX4 < NOW(), NOW(), spindateX4), INTERVAL 7 DAY), stampdateX4 = DATE_ADD(IF(stampdateX4 < NOW(), NOW(), stampdateX4), INTERVAL 7 DAY), inboxdateX4 = DATE_ADD(IF(inboxdateX4 < NOW(), NOW(), inboxdateX4), INTERVAL 7 DAY), writedate=VALUES(writedate);";
                    write_log($sql);
                    $db_main2->execute($sql);
                }
                
                //Premium Booster
                $premium_type = 2;
                
                $sql = "UPDATE tbl_user_detail SET ispremium = 1 WHERE useridx = $useridx";
                write_log($sql);
                $db_main->execute($sql);
                
                $sql = " INSERT INTO tbl_user_premium_booster(useridx, premium_type, os_type, end_date, writedate) ".
                    " VALUES($useridx, $premium_type, 0, DATE_ADD(NOW(), INTERVAL 50 DAY),now()) ON DUPLICATE KEY UPDATE end_date = DATE_ADD(end_date, INTERVAL 50 DAY), os_type=VALUES(os_type);";
                write_log($sql);
                $db_main2->execute($sql);
                
            }
        }
    }
    
    $db_main->end();
    $db_main2->end();
    $db_friend->end();
    $db_livestats->end();
    $db_inbox->end();
?>
