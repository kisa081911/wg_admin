<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/marketing/marketing_stat_daily") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/marketing/marketing_stat_daily") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("marketing/marketing_stat_daily Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
 	$db_main2 = new CDatabase_Main2();
 	$db_slave_main2 = new CDatabase_Slave_Main2();
 	$db_redshift = new CDatabase_Redshift();
 	
 	$db_main2->execute("SET wait_timeout=7200");
 	$db_slave_main2->execute("SET wait_timeout=7200");
 	
 	//2020-04-23
 	//2020-05-25
 	
 	//$today = date("Y-m-d", time() - 60 * 60 * 24 * 1);
 	//$current_date = date("Y-m-d");
 	
 	
 	
 	$day  = '2020-04-23';
 	$end_today = '2020-05-25';
 	
 	while($day < $end_today)
 	{
 	    $today = $day;
 	    $current_date = date("Y-m-d", strtotime($today ."1 day"));
 	/************************* 신규 광고 ************************/
 	      write_log("Marketing Stat Daily Scheduler Start".$today);
 	
 	####################################################################################################################
	###################################################### Mobile ######################################################
 	####################################################################################################################
	// 신규 유입자수 (모바일 광고)
	$insert_sql = "";
	
	for($i=1; $i<=4; $i++)
	{
		$sql = "SELECT platform, adflag, nvl(COUNT(deviceid), 0) AS newuser, nvl(SUM(CASE WHEN experience > 0 or honor_point > 0 THEN 0 ELSE 1 END), 0) AS newuser_noplay
				FROM (
					SELECT platform, adflag, deviceid, MAX(experience) AS experience,  max(honor_point) as honor_point
					FROM t5_user
					WHERE platform > 0 AND useridx > 20000
					AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple Search Ads%')
					AND dateadd(DAY,((-7 * $i)), '$current_date') <= createdate AND createdate < '$current_date 00:00:00'
					GROUP BY platform, adflag, deviceid
				) t1
				GROUP BY platform, adflag";
		$newuser_cnt_list = $db_redshift->gettotallist($sql);
		
		for($j=0; $j<sizeof($newuser_cnt_list); $j++)
		{
			$platform = $newuser_cnt_list[$j]["platform"];
			$adflag = $newuser_cnt_list[$j]["adflag"];
			$newuser = $newuser_cnt_list[$j]["newuser"];
			$newuser_noplay = $newuser_cnt_list[$j]["newuser_noplay"];
			
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_marketing_stat_daily (today, type, subtype, platform, adflag, newuser, newuser_noplay) VALUES('$today', 1, $i, $platform, '$adflag', $newuser, $newuser_noplay)";
			else
				$insert_sql .= ",('$today', 1, $i, $platform, '$adflag', $newuser, $newuser_noplay)";
		}
	}

	if($insert_sql != "")
	{
		$insert_sql .= "ON DUPLICATE KEY UPDATE newuser = VALUES(newuser), newuser_noplay = VALUES(newuser_noplay);";
		$execute_sql .= $insert_sql;
	}

	// 신규 유입자 결제 정보 (모바일 광고)
	$insert_sql = "";
	
	for($i=1; $i<=4; $i++)
	{
		$sql = "SELECT platform, adflag, nvl(COUNT(DISTINCT deviceid), 0) AS newuser_payer, nvl(SUM(money), 0) AS newuser_money
				FROM (
					SELECT t1.platform, t1.useridx, deviceid, adflag, (facebookcredit::float/10::float) AS money
					FROM (
						SELECT platform, useridx, deviceid, adflag, createdate
						FROM t5_user
						WHERE platform > 0 AND useridx > 20000
						AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple Search Ads%')
						AND dateadd(day,((-7 * $i)), '$current_date') <= createdate AND createdate < '$current_date 00:00:00'
					) t1 JOIN t5_product_order t2 ON t1.useridx = t2.useridx
					WHERE status = 1 AND datediff(day, createdate, writedate) <= 7 * $i AND writedate < '$current_date 00:00:00'
					UNION ALL
					SELECT t1.platform, t1.useridx, deviceid, adflag, money
					FROM (
						SELECT platform, useridx, deviceid, adflag, createdate
						FROM t5_user
						WHERE platform > 0 AND useridx > 20000
						AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple Search Ads%')
						AND dateadd(day,((-7 * $i)), '$current_date') <= createdate AND createdate < '$current_date 00:00:00'
					) t1 JOIN t5_product_order_mobile t2 ON t1.useridx = t2.useridx
					WHERE status = 1 AND datediff(day, createdate, writedate) <= 7 * $i AND writedate < '$current_date 00:00:00'
				) total
				GROUP BY platform, adflag";
		$newuser_payer_cnt_list = $db_redshift->gettotallist($sql);
		
		for($j=0; $j<sizeof($newuser_payer_cnt_list); $j++)
		{
			$platform = $newuser_payer_cnt_list[$j]["platform"];
			$adflag = $newuser_payer_cnt_list[$j]["adflag"];
			$newuser_payer = $newuser_payer_cnt_list[$j]["newuser_payer"];
			$newuser_money = $newuser_payer_cnt_list[$j]["newuser_money"];
			
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_marketing_stat_daily (today, type, subtype, platform, adflag, newuser_payer, newuser_money) VALUES('$today', 1, $i, $platform, '$adflag', $newuser_payer, $newuser_money)";
			else
				$insert_sql .= ",('$today', 1, $i, $platform, '$adflag', $newuser_payer, $newuser_money)";
		}
	}
	
	if($insert_sql != "")
	{
		$insert_sql .= "ON DUPLICATE KEY UPDATE newuser_payer = VALUES(newuser_payer), newuser_money = VALUES(newuser_money);";
		$execute_sql .= $insert_sql;
	}
	
	// 신규 광고 리텐션 유입자수 (모바일 광고)
	$insert_sql = "";
	
	for($i=1; $i<=4; $i++)
	{
		$sql = "SELECT platform, adflag, nvl(COUNT(distinct useridx), 0) AS reuser, nvl(AVG(leavedays), 0) AS reuser_leavedays
				FROM (
					SELECT 0 AS platform, useridx, adflag, leavedays, createdate, writedate AS retentiondate
					FROM t5_user_retention_log
					WHERE useridx > 20000 AND leavedays >= 28
					AND dateadd(day,((-7 * $i)), '$current_date') <= writedate AND writedate < '$current_date 00:00:00'
					AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple Search Ads%')
					UNION ALL
					SELECT platform, useridx, adflag, leavedays, createdate, writedate AS retentiondate
					FROM t5_user_retention_mobile_log
					WHERE useridx > 20000 AND leavedays >= 28
					AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple Search Ads%')
					AND dateadd(day,((-7 * $i)), '$current_date') <= writedate AND writedate < '$current_date 00:00:00'
				) t1
				GROUP BY platform, adflag";
		$reuser_cnt_list = $db_redshift->gettotallist($sql);
		
		for($j=0; $j<sizeof($reuser_cnt_list); $j++)
		{
			$platform = $reuser_cnt_list[$j]["platform"];
			$adflag = $reuser_cnt_list[$j]["adflag"];
			$reuser = $reuser_cnt_list[$j]["reuser"];
			$reuser_leavedays = $reuser_cnt_list[$j]["reuser_leavedays"];
			
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_marketing_stat_daily (today, type, subtype, platform, adflag, reuser, reuser_leavedays) VALUES('$today', 1, $i, $platform, '$adflag', $reuser, $reuser_leavedays)";
			else
				$insert_sql .= ",('$today', 1, $i, $platform, '$adflag', $reuser, $reuser_leavedays)";
		}
	}
	
	if($insert_sql != "")
	{
		$insert_sql .= "ON DUPLICATE KEY UPDATE reuser = VALUES(reuser), reuser_leavedays = VALUES(reuser_leavedays);";
		$execute_sql .= $insert_sql;
	}
	
 	// 신규 광고 리텐션 결제 정보 (모바일 광고)
 	$insert_sql = "";
	
 	for($i=1; $i<=4; $i++)
 	{
 		$sql = "SELECT platform, adflag, nvl(COUNT(distinct useridx), 0) AS reuser_payer, nvl(SUM(money), 0) AS reuser_money, nvl(avg(leavedays), 0) AS reuser_payer_leavedays
				FROM (
 					SELECT 0 AS platform, useridx, adflag, AVG(leavedays) AS leavedays, SUM(money) AS money
 					FROM (
 						SELECT t3.useridx, adflag, leavedays, multi_value, retentiondate, datediff(day, retentiondate, writedate) AS dayafterretention, writedate, round((facebookcredit::float/10::float) * multi_value::float, 2) money
 						FROM (
 							SELECT t1.useridx, t1.adflag, leavedays, (CASE WHEN leavedays >= 365 THEN 1 ELSE round(leavedays::float/365::float, 2) END) AS multi_value, retentiondate
 							FROM (
 								SELECT useridx, adflag, leavedays, createdate, writedate AS retentiondate
 								FROM t5_user_retention_log
 								WHERE useridx > 20000 AND leavedays >= 28
 								AND dateadd(day,((-7 * $i)), '$current_date') <= writedate AND writedate < '$current_date 00:00:00'
 								AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple Search Ads%')
 							) t1 JOIN t5_user t2 ON t1.useridx = t2.useridx
 						) t3 JOIN t5_product_order t4 ON t3.useridx=t4.useridx AND t3.retentiondate <= t4.writedate
 						WHERE status = 1 AND datediff(day, retentiondate, writedate) < 28 AND writedate < '$current_date 00:00:00'
				    	UNION ALL
					    SELECT t3.useridx, adflag, leavedays, multi_value, retentiondate, datediff(day, retentiondate, writedate) AS dayafterretention, writedate, round(money::float * multi_value::float, 2) money
					    FROM (
	 						SELECT t1.useridx, t1.adflag, leavedays, (CASE WHEN leavedays >= 365 THEN 1 ELSE round(leavedays::float/365::float, 2) END) AS multi_value, retentiondate
	 						FROM (
	 							SELECT useridx, adflag, leavedays, createdate, writedate AS retentiondate
	 							FROM t5_user_retention_log
	 							WHERE useridx > 20000 AND leavedays >= 28
	 							AND dateadd(day,((-7 * $i)), '$current_date') <= writedate AND writedate < '$current_date 00:00:00'
	 							AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple Search Ads%')
	 						) t1 JOIN t5_user t2 ON t1.useridx = t2.useridx
	 					) t3 JOIN t5_product_order_mobile t4 ON t3.useridx=t4.useridx AND t3.retentiondate <= t4.writedate
	 					WHERE status = 1 AND datediff(day, retentiondate, writedate) < 28 AND writedate < '$current_date 00:00:00'
 					) total
 					GROUP BY useridx, adflag
 					UNION ALL
	 				SELECT platform, useridx, adflag, AVG(leavedays) AS leavedays, SUM(money) AS money
	 				FROM (
	 					SELECT platform, t3.useridx, adflag, leavedays, multi_value, retentiondate, datediff(day, retentiondate, writedate) AS dayafterretention, writedate, round((facebookcredit::float/10::float) * multi_value::float, 2) money
	 					FROM (
	 						SELECT t1.platform, t1.useridx, t1.adflag, leavedays, (CASE WHEN leavedays >= 365 THEN 1 ELSE round(leavedays::float/365::float, 2) END) AS multi_value, retentiondate
	 						FROM (
						        SELECT platform, useridx, adflag, leavedays, createdate, writedate AS retentiondate
						        FROM t5_user_retention_mobile_log
						        WHERE useridx > 20000 AND leavedays >= 28 AND platform > 0
		 						AND dateadd(day,((-7 * $i)), '$current_date') <= writedate AND writedate < '$current_date 00:00:00'
		 						AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple Search Ads%')
	 						) t1 JOIN t5_user t2 ON t1.useridx = t2.useridx
	 					) t3 JOIN t5_product_order t4 ON t3.useridx=t4.useridx AND t3.retentiondate <= t4.writedate
		 				WHERE status = 1 AND datediff(day, retentiondate, writedate) < 28 AND writedate < '$current_date 00:00:00'
		 				UNION ALL
		 				SELECT platform, t3.useridx, adflag, leavedays, multi_value, retentiondate, datediff(day, retentiondate, writedate) AS dayafterretention, writedate, round(money::float * multi_value::float, 2) money
		 				FROM (
		 					SELECT t1.platform, t1.useridx, t1.adflag, leavedays, (CASE WHEN leavedays >= 365 THEN 1 ELSE round(leavedays::float/365::float, 2) END) AS multi_value, retentiondate
		 					FROM (
		 						SELECT platform, useridx, adflag, leavedays, createdate, writedate AS retentiondate
		 						FROM t5_user_retention_mobile_log
		 						WHERE useridx > 20000 AND leavedays >= 28 AND platform > 0
		 						AND dateadd(day,((-7 * $i)), '$current_date') <= writedate AND writedate < '$current_date 00:00:00'
		 						AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple Search Ads%')
		 					) t1 JOIN t5_user t2 ON t1.useridx = t2.useridx
		 				) t3 JOIN t5_product_order_mobile t4 ON t3.useridx=t4.useridx AND t3.retentiondate <= t4.writedate
 						WHERE status = 1 AND datediff(day, retentiondate, writedate) < 28 AND writedate < '$current_date 00:00:00'
 					) total
 					GROUP BY platform, useridx, adflag
 				) t5
				GROUP BY platform, adflag";
 		$reuser_payer_cnt_list = $db_redshift->gettotallist($sql);

 		for($j=0; $j<sizeof($reuser_payer_cnt_list); $j++)
 		{
 			$platform = $reuser_payer_cnt_list[$j]["platform"];
 			$adflag = $reuser_payer_cnt_list[$j]["adflag"];
 			$reuser_payer = $reuser_payer_cnt_list[$j]["reuser_payer"];
 			$reuser_money = $reuser_payer_cnt_list[$j]["reuser_money"];
 			$reuser_payer_leavedays = $reuser_payer_cnt_list[$j]["reuser_payer_leavedays"];

 			if($insert_sql == "")
 				$insert_sql = "INSERT INTO tbl_marketing_stat_daily (today, type, subtype, platform, adflag, reuser_payer, reuser_money, reuser_payer_leavedays) VALUES('$today', 1, $i, $platform, '$adflag', $reuser_payer, $reuser_money, $reuser_payer_leavedays)";
 			else
 				$insert_sql .= ",('$today', 1, $i, $platform, '$adflag', $reuser_payer, $reuser_money, $reuser_payer_leavedays)";
 		}
 	}
	
 	if($insert_sql != "")
 	{
	 	$insert_sql .= "ON DUPLICATE KEY UPDATE reuser_payer = VALUES(reuser_payer), reuser_money = VALUES(reuser_money), reuser_payer_leavedays = VALUES(reuser_payer_leavedays);";
	 	$execute_sql .= $insert_sql;
 	}
 	
	// 신규 광고 28일 미만 리텐션 유저 유입자수
	$insert_sql = "";
	
	for($i=1; $i<=4; $i++)
	{
		$sql = "SELECT platform, adflag, nvl(COUNT(distinct useridx), 0) AS lessthan28_user, nvl(AVG(leavedays), 0) AS lessthan28_user_leavedays
				FROM (
					SELECT 0 AS platform, useridx, adflag, leavedays, createdate, writedate AS retentiondate
					FROM t5_user_retention_log
					WHERE useridx > 20000 AND leavedays < 28
					AND dateadd(day,((-7 * $i)), '$current_date') <= writedate AND writedate < '$current_date 00:00:00'
					AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple Search Ads%')
					UNION ALL
					SELECT platform, useridx, adflag, leavedays, createdate, writedate AS retentiondate
					FROM t5_user_retention_mobile_log
					WHERE useridx > 20000 AND leavedays < 28
					AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple Search Ads%')
					AND dateadd(day,((-7 * $i)), '$current_date') <= writedate AND writedate < '$current_date 00:00:00'
				) t1
				GROUP BY platform, adflag";
		$reuser_cnt_list = $db_redshift->gettotallist($sql);
		
		for($j=0; $j<sizeof($reuser_cnt_list); $j++)
		{
			$platform = $reuser_cnt_list[$j]["platform"];
			$adflag = $reuser_cnt_list[$j]["adflag"];
			$lessthan28_user = $reuser_cnt_list[$j]["lessthan28_user"];
			$lessthan28_user_leavedays = $reuser_cnt_list[$j]["lessthan28_user_leavedays"];
			
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_marketing_stat_daily (today, type, subtype, platform, adflag, lessthan28_user, lessthan28_user_leavedays) VALUES('$today', 1, $i, $platform, '$adflag', $lessthan28_user, $lessthan28_user_leavedays)";
			else
				$insert_sql .= ",('$today', 1, $i, $platform, '$adflag', $lessthan28_user, $lessthan28_user_leavedays)";
		}
	}
	
	if($insert_sql != "")
	{
		$insert_sql .= "ON DUPLICATE KEY UPDATE lessthan28_user = VALUES(lessthan28_user), lessthan28_user_leavedays = VALUES(lessthan28_user_leavedays);";
		$execute_sql .= $insert_sql;
	}
	
	############################################################################################################################
	###################################################### Mobile Organic ######################################################
	############################################################################################################################
 	// Mobile Organic 가입자수
 	$insert_sql = "";
 	
 	for($i=1; $i<=4; $i++)
 	{
 		$sql = "SELECT platform, nvl(COUNT(deviceid), 0) AS newuser, nvl(SUM(CASE WHEN experience > 0 or honor_point > 0 THEN 0 ELSE 1 END), 0) AS newuser_noplay
				FROM (
					SELECT platform, deviceid, MAX(experience) AS experience,  max(honor_point) as honor_point
					FROM t5_user
					WHERE platform > 0 AND useridx > 20000 AND adflag  = ''
 					AND dateadd(DAY,((-7 * $i)), '$current_date') <= createdate AND createdate < '$current_date 00:00:00'
					GROUP BY platform, deviceid
				) t1
				GROUP BY platform
				ORDER BY platform ASC";
 		$newuser_cnt_list = $db_redshift->gettotallist($sql);
 		
 		for($j=0; $j<sizeof($newuser_cnt_list); $j++)
 		{
 			$platform = $newuser_cnt_list[$j]["platform"];
 			$newuser = $newuser_cnt_list[$j]["newuser"];
 			$newuser_noplay = $newuser_cnt_list[$j]["newuser_noplay"];
 				
 			if($insert_sql == "")
 				$insert_sql = "INSERT INTO tbl_organic_stat_daily (today, subtype, platform, newuser, newuser_noplay) VALUES('$today', $i, $platform, $newuser, $newuser_noplay)";
 			else
 				$insert_sql .= ",('$today', $i, $platform, $newuser, $newuser_noplay)";
 		}
 	}
 	
 	if($insert_sql != "")
 	{
	 	$insert_sql .= "ON DUPLICATE KEY UPDATE newuser = VALUES(newuser), newuser_noplay = VALUES(newuser_noplay);";
	 	$execute_sql .= $insert_sql;
 	}
 	
 	// Mobile Organic 결제 정보
 	$insert_sql = "";
	
	for($i=1; $i<=4; $i++)
	{
		$sql = "SELECT platform, nvl(COUNT(DISTINCT useridx), 0) AS newuser_payer, nvl(SUM(money), 0) AS newuser_money
				FROM (
					SELECT platform, t1.useridx, (facebookcredit::FLOAT/10::FLOAT) AS money
					FROM (
						SELECT platform, useridx, createdate
						FROM t5_user 
						WHERE platform > 0 AND useridx > 20000 AND adflag  = ''
						AND dateadd(DAY,((-7 * $i)), '$current_date') <= createdate AND createdate < '$current_date 00:00:00'
					) t1 JOIN t5_product_order t2 ON t1.useridx = t2.useridx
					WHERE STATUS = 1 AND t1.createdate <= t2.writedate AND DATEDIFF(DAY, createdate, writedate) <= 7 * $i AND writedate < '$current_date 00:00:00'
					UNION ALL
					SELECT platform, t1.useridx, money
					FROM (
						SELECT platform, useridx, createdate
						FROM t5_user 
						WHERE platform > 0 AND useridx > 20000 AND adflag  = ''
						AND dateadd(DAY,((-7 * $i)), '$current_date') <= createdate AND createdate < '$current_date 00:00:00'
					) t1 JOIN t5_product_order_mobile t2 ON t1.useridx = t2.useridx
					WHERE STATUS = 1 AND t1.createdate <= t2.writedate AND DATEDIFF(DAY, createdate, writedate) <= 7 * $i AND writedate < '$current_date 00:00:00'
				) t1
				GROUP BY platform
				ORDER BY platform ASC";
		$newuser_payer_cnt_list = $db_redshift->gettotallist($sql);
		
		for($j=0; $j<sizeof($newuser_payer_cnt_list); $j++)
		{
			$platform = $newuser_payer_cnt_list[$j]["platform"];
			$newuser_payer = $newuser_payer_cnt_list[$j]["newuser_payer"];
			$newuser_money = $newuser_payer_cnt_list[$j]["newuser_money"];
			
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_organic_stat_daily (today, subtype, platform, newuser_payer, newuser_money) VALUES('$today', $i, $platform, $newuser_payer, $newuser_money)";
			else
				$insert_sql .= ",('$today', $i, $platform, $newuser_payer, $newuser_money)";
		}
	}
	
	if($insert_sql != "")
	{
		$insert_sql .= "ON DUPLICATE KEY UPDATE newuser_payer = VALUES(newuser_payer), newuser_money = VALUES(newuser_money);";
		$execute_sql .= $insert_sql;
	}
 	
 	// FBRetention - Mobile
 	$insert_sql = "";
 	
 	for($i=1; $i<=2; $i++)
 	{
	 	for($j=1; $j<=4; $j++)
	 	{
		 	$sql = "SELECT DATE_FORMAT(DATE_SUB('$current_date', INTERVAL $i DAY), '%Y-%m-%d') AS today, platform, CONCAT('m_', SUBSTRING_INDEX(SUBSTRING_INDEX(campaign_name, '_m_', -1), '_', 1)) AS adflag, IFNULL(ROUND(SUM(spend), 2), 0) AS spend 
					FROM `tbl_ad_retention_stats_mobile` 
					WHERE DATE_ADD(DATE_FORMAT(DATE_SUB('$current_date', INTERVAL ($i - 1) DAY), '%Y-%m-%d'), INTERVAL (-7 * $j) DAY) <= today AND today < DATE_FORMAT(DATE_SUB('$current_date', INTERVAL ($i - 1) DAY), '%Y-%m-%d')
					GROUP BY platform, CONCAT('m_', SUBSTRING_INDEX(SUBSTRING_INDEX(campaign_name, '_m_', -1), '_', 1))  ";
		 	$mobile_fbretention_spend_list = $db_slave_main2->gettotallist($sql);
		 	
		 	for($k=0; $k<sizeof($mobile_fbretention_spend_list); $k++)
		 	{
		 		$today = $mobile_fbretention_spend_list[$k]["today"];
		 		$platform = $mobile_fbretention_spend_list[$k]["platform"];
		 		$adflag = $mobile_fbretention_spend_list[$k]["adflag"];
		 		$spend = $mobile_fbretention_spend_list[$k]["spend"];
		 		
		 		if($insert_sql == "")
		 			$insert_sql = "INSERT INTO tbl_marketing_stat_daily (today, type, subtype, platform, adflag, spend) VALUES('$today', 2, $j, $platform, '$adflag', $spend)";
		 		else
		 			$insert_sql .= ",('$today', 2, $j, $platform, '$adflag', $spend)";
		 	}
 		}
 	}
 	
	if($insert_sql != "")
 	{
	 	$insert_sql .= "ON DUPLICATE KEY UPDATE spend = VALUES(spend);";
	 	$execute_sql .= $insert_sql;
 	}
 	 	
 	// Mobile Agency
 	$insert_sql = "";
 	
 	for($i=1; $i<=7; $i++)
 	{
	 	for($j=1; $j<=4; $j++)
	 	{
	 		$sql = "SELECT DATE_FORMAT(DATE_SUB('$current_date', INTERVAL $i DAY), '%Y-%m-%d') AS today, platform, agencyname AS adflag, IFNULL(ROUND(SUM(spend), 2), 0) AS spend
					FROM `tbl_agency_spend_daily`
					WHERE agencyname <> '' AND DATE_ADD(DATE_FORMAT(DATE_SUB('$current_date', INTERVAL ($i - 1) DAY), '%Y-%m-%d'), INTERVAL (-7 * $j) DAY) <= today AND today < DATE_FORMAT(DATE_SUB('$current_date', INTERVAL ($i - 1) DAY), '%Y-%m-%d') AND spend <> 0
					GROUP BY platform, adflag";
// 					AND agencyname NOT LIKE '%Facebook Ads%'
	 		$mobile_agency_spend_list = $db_slave_main2->gettotallist($sql);
	 		
	 		for($k=0; $k<sizeof($mobile_agency_spend_list); $k++)
	 		{
	 			$today = $mobile_agency_spend_list[$k]["today"];
	 			$platform = $mobile_agency_spend_list[$k]["platform"];
	 			$adflag = $mobile_agency_spend_list[$k]["adflag"];
	 			$spend = $mobile_agency_spend_list[$k]["spend"];
	 				
	 			if($insert_sql == "")
	 				$insert_sql = "INSERT INTO tbl_marketing_stat_daily (today, type, subtype, platform, adflag, spend) VALUES('$today', 1, $j, $platform, '$adflag', $spend)";
	 			else
	 				$insert_sql .= ",('$today', 1, $j, $platform, '$adflag', $spend)";
	 		}
	 	}
 	}
 	
 	if($insert_sql != "")
 	{
 		$insert_sql .= "ON DUPLICATE KEY UPDATE spend = VALUES(spend);";
 		$execute_sql .= $insert_sql;
 	}
 	
//  	// m_duc & m_ddi - Mobile
//  	$insert_sql = "";
 	
//  	for($i=1; $i<=1; $i++)
//  	{
//  		for($j=1; $j<=4; $j++)
//  		{
//  			$sql = "SELECT DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL $i DAY), '%Y-%m-%d') AS today, platform, CONCAT(SUBSTRING_INDEX(campaign_name, '_', -2), '_int') AS adflag, IFNULL(ROUND(SUM(spend), 2), 0) AS spend
// 		 			FROM `tbl_ad_stats_mobile`
// 		 			WHERE DATE_ADD(DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL ($i - 1) DAY), '%Y-%m-%d'), INTERVAL (-7 * $j) DAY) <= today AND today < DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL ($i - 1) DAY), '%Y-%m-%d')
// 		 			AND (campaign_name LIKE '%m_duc%' OR campaign_name LIKE '%m_ddi%')
// 		 			GROUP BY platform, adflag";
//  			$mobile_fbself_spend_list = $db_slave_main2->gettotallist($sql);
 	
//  			for($k=0; $k<sizeof($mobile_fbself_spend_list); $k++)
//  			{
//  				$today = $mobile_fbself_spend_list[$k]["today"];
//  				$platform = $mobile_fbself_spend_list[$k]["platform"];
//  				$adflag = $mobile_fbself_spend_list[$k]["adflag"];
//  				$spend = $mobile_fbself_spend_list[$k]["spend"];
 	
//  				if($insert_sql == "")
// 					$insert_sql .= "INSERT INTO tbl_marketing_stat_daily (today, type, subtype, platform, adflag, spend) VALUES('$today', 1, $j, $platform, '$adflag', $spend)";
//  				else
//  					$insert_sql .= ",('$today', 1, $j, $platform, '$adflag', $spend)";
//  			}
//  		}
//  	}
 	
//  	if($insert_sql != "")
//  	{
//  		$insert_sql .= "ON DUPLICATE KEY UPDATE spend = VALUES(spend);";
//  		$execute_sql .= $insert_sql;
//  	}
 	
 	if($execute_sql != "")
 		$db_main2->execute($execute_sql);
 	
 	$day = date("Y-m-d", strtotime($day ."1 day"));
}
 	write_log("Marketing Stat Daily Scheduler End");
 	
 	$db_main2->end();
 	$db_slave_main2->end();
 	$db_redshift->end();
?>