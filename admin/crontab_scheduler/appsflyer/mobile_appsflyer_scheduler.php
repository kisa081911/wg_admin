<?
    include("../../common/common_include.inc.php");
    
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_mobile = new CDatabase_Mobile();
	
	$db_main->execute("SET wait_timeout=60");
	$db_main2->execute("SET wait_timeout=60");
	$db_mobile->execute("SET wait_timeout=60");
	
	$today = date("Y-m-d");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/appsflyer/mobile_appsflyer_scheduler") !== false)
		{
			$count++;			
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
		
		$killcontents = "#!/bin/bash\n";
		
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
		
		flock($fp, LOCK_EX);
		
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
		
		flock($fp, LOCK_UN);
		
		$content = "#!/bin/bash\n";
		
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/appsflyer/mobile_appsflyer_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
				
				$content .= "kill -9 ".$process_list[0]."\n";
				
				write_log("mobile_appsflyer_schedulerr Dead Lock Kill!");
			}
		}
		
		fwrite($fp, $content, strlen($content));
		
		fclose($fp);
		
		exit();
	}
		
	try
	{	
		$sql = "SELECT logidx, appsflyer_device_id, platform, device_id, media_source, fb_ad_id, writedate FROM tbl_appsflyer_queue WHERE trycount < 120 ORDER BY logidx ASC LIMIT 50";
		$apps_list = $db_main2->gettotallist($sql);
		
		for($i=0; $i<sizeof($apps_list); $i++)
		{
			$logidx = $apps_list[$i]["logidx"];
			$platform = $apps_list[$i]["platform"];
			$appsflyer_device_id = $apps_list[$i]["appsflyer_device_id"];
			$device_id = $apps_list[$i]["device_id"];
			$media_source = $apps_list[$i]["media_source"];
			$fb_ad_id = $apps_list[$i]["fb_ad_id"];
			$createdate = $apps_list[$i]["writedate"];
			
			$sql = "SELECT useridx ".
					"FROM tbl_user_mobile_appsflyer ".
					"WHERE os_type=$platform AND appsflyerid='$appsflyer_device_id' ".
					"ORDER BY writedate DESC ".
					"LIMIT 1";
			$useridx = $db_mobile->getvalue($sql);

			$adflag = "";
			
			if($platform == 1)
				$adflag = "mobile";
			else if($platform == 2)
				$adflag = "mobile_ad";
			else if($platform == 3)
				$adflag = "mobile_kindle";
			
			if($useridx != "")
			{
				$sql = "SELECT COUNT(*) FROM tbl_user WHERE useridx = $useridx AND createdate >= DATE_SUB('$createdate', INTERVAL 2 DAY)";
				$is_first_join = $db_main->getvalue($sql);
					
				if($is_first_join > 0)
				{
					$sql = "UPDATE tbl_user_ext SET adflag='$media_source',chain_adflag='$media_source' WHERE useridx=$useridx AND platform=$platform;";
					$db_main->execute($sql);
					
					$sql = "DELETE FROM tbl_appsflyer_queue WHERE logidx = $logidx;";
					$db_main2->execute($sql);
				}
				else 
				{
					$sql = "SELECT adflag, fbsource FROM tbl_user_ext WHERE useridx = $useridx";
					$rejoin_data = $db_main->getarray($sql);
					
					$join_adflag = $rejoin_data["adflag"];
					$join_fbsource = $rejoin_data["fbsource"];
					
					// 재가입 유저
					$sql = "INSERT INTO tbl_appsflyer_rejoin(logidx, platform, category, device_id, useridx, adflag, fbsource, media_source, fb_ad_id, writedate) VALUES($logidx, $platform, 1, '$device_id', $useridx, '$join_adflag', '$join_fbsource', '$media_source', $fb_ad_id, '$createdate');";
					$sql .= "DELETE FROM tbl_appsflyer_queue WHERE logidx = $logidx;";
					$db_main2->execute($sql);
				}
			}
			else
			{
				$sql = "UPDATE tbl_appsflyer_queue SET trycount = trycount + 1 WHERE logidx=$logidx";
				$db_main2->execute($sql);
				
				$sql = "SELECT trycount FROM tbl_appsflyer_queue WHERE logidx=$logidx";
				$trycount = $db_main2->getvalue($sql);
				
				// 가입 정보가 없는 유저(인스톨만하고 가입은 하지않은 유저)
				if($trycount >= 120)
				{
					$sql = "INSERT INTO tbl_appsflyer_rejoin(logidx, platform, category, device_id, media_source, fb_ad_id, writedate) VALUES($logidx, $platform, 0, '$device_id', '$media_source', $fb_ad_id, '$createdate');";
					$sql .= "DELETE FROM tbl_appsflyer_queue WHERE logidx = $logidx;";
					$db_main2->execute($sql);
				}
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
 
	$db_main->end();
    $db_main2->end();
    $db_mobile->end();
?>