<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	try
	{
		$db_redshift = new CDatabase_Redshift();
	
	$filename = "t5_appsflyer_install_organic_order_90.xls";

	$db_redshift = new CDatabase_Redshift();

	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
						"<table border=1>".
						"<tr>".
						"<td style='font-weight:bold;'>joindate</td>".
						"<td style='font-weight:bold;'>today</td>".
						"<td style='font-weight:bold;'>money</td>".
						"<td style='font-weight:bold;'>paycount</td>".
						"</tr>";
	$today = '2017-01-19';	
	$end_today = '2017-03-26';

	while($today < $end_today)
	{	
		// join 테이블, platform 조건만 바꿔주시면 됩니다.
		$sql = "select today as joindate, sum(money) as money ,writedate, count(t1.useridx) as paycount
				from
				(
				    select date(installtime) AS today, useridx from _tmp_t5_appsflyer_install_organic_order where diffday < -89 and installtime >= '$today 00:00:00' and installtime <= '$today 23:59:59'
				) t1 join
				(
				     select useridx, date(writedate) as writedate , money  from t5_product_order_mobile where status = 1 and writedate >= '$today 00:00:00'
				    union all
				    select useridx, date(writedate) as writedate , facebookcredit as money from t5_product_order where status = 1 and writedate >= '$today 00:00:00'
				) t2 on t1.useridx = t2.useridx
				group by joindate, writedate
				order by joindate asc,  writedate asc ";
		$contents_list = $db_redshift->gettotallist($sql);

		for ($j=0; $j<sizeof($contents_list); $j++)
		{
			$joindate = $contents_list[$j]["joindate"];
			$payday = $contents_list[$j]["writedate"];
			$money = $contents_list[$j]["money"];
			$paycount = $contents_list[$j]["paycount"];


			$excel_contents .= "<tr>".
					"<td>".$joindate."</td>".
					"<td>".$payday."</td>".
					"<td>".$money."</td>".
					"<td>".number_format($paycount)."</td>".
					"</tr>";
		}

		$today = date("Y-m-d", strtotime($today."1 day"));

		$excel_contents .= "<tr>".
				"<td>-------------</td>".
				"</tr>";
	}

	$db_redshift->end();

	$excel_contents .= "</table>";

	Header("Content-type: application/x-msdownload");
	Header("Content-type: application/x-msexcel");
	Header("Content-Disposition: attachment; filename=$filename");

	echo($excel_contents);
	
		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>