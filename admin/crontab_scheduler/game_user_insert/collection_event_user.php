<?
   	include("../../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();
	
	$db_main->execute("SET wait_timeout=72000");
	$db_main2->execute("SET wait_timeout=72000");
	$db_other->execute("SET wait_timeout=72000");
	
	ini_set("memory_limit", "-1");
	
	$today = date("Y-m-d");
	
	write_log("meta_event_target_user Start");
	
	$sql = "SELECT useridx , COUNT(DISTINCT today) AS playcount
            FROM (
            SELECT * FROM `tbl_user_playstat_daily` WHERE today >= DATE_SUB(NOW(),INTERVAL 14 DAY) 
            UNION ALL
            SELECT * FROM `tbl_user_playstat_daily_amazon` WHERE today >= DATE_SUB(NOW(),INTERVAL 14 DAY)
            UNION ALL
            SELECT * FROM `tbl_user_playstat_daily_android` WHERE today >= DATE_SUB(NOW(),INTERVAL 14 DAY)
            UNION ALL
            SELECT * FROM `tbl_user_playstat_daily_ios` WHERE today >= DATE_SUB(NOW(),INTERVAL 14 DAY)
            )t1  GROUP BY useridx";
	$user_list = $db_other->gettotallist($sql);
	
	$insert_sql = "";
	$insert_cnt = 0;
	
	for($i=0; $i<sizeof($user_list); $i++)
	{
		$useridx = $user_list[$i]["useridx"];
		$dailyplaycount = $user_list[$i]["playcount"];
		
		if($dailyplaycount >= 5)
		  $type = 1;
	    else if($dailyplaycount >= 3 && $dailyplaycount < 5)
	      $type = 2;
	    else if($dailyplaycount < 3)
	      $type = 3;
	    
		if($insert_sql == "")
		    $insert_sql = "INSERT INTO tbl_collection_event_target_user VALUES($useridx, $type, $dailyplaycount, now())";
		else
			$insert_sql .= ", ($useridx, $type, $dailyplaycount, now())";
		
		$insert_cnt++;
		
		if($insert_cnt >= 1000)
		{
		    $db_main2->execute($insert_sql);
			$insert_sql = "";
			$insert_cnt = 0;
		}
	}
	
	if($insert_sql != "")
	{
	    $db_main2->execute($insert_sql);
		$insert_sql = "";
		$insert_cnt = 0;
	}
	
	write_log("meta_event_target_user End");
    
    $db_main->end();
    $db_main2->end();
    $db_other->end();
?>