<?
	include("../../common/common_include.inc.php");

	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();	
	
	$db_main2->execute("SET wait_timeout=3600");	
	$db_other->execute("SET wait_timeout=3600");	
	
	$port = "";
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/game_user_insert/insert_lucky_wheel_user") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
					$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
			flock($fp, LOCK_EX);
	
			if (!$fp) {
			echo "Fail";
				exit;
		}
			else
			{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
		
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/game_user_insert/insert_lucky_wheel_user") !== false)
			{
				$process_list = explode("|", $result[$i]);
		
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("insert_lucky_wheel_user Dead Lock Kill!");
			}
		}
		
		fwrite($fp, $content, strlen($content));
		fclose($fp);
		
		exit();
	}
	
	try
	{
		$yesterday = date("Y-m-d");
		
		$insert_sql = "";
		$insertcount = 0;
		
		$sql = "UPDATE tbl_lucky_wheel_target SET is_new = 0";
		$db_main2->execute($sql);
		
		$sql = "SELECT useridx, COUNT(DISTINCT today) AS d28_play_days
				FROM		 
				(		
					SELECT useridx, today	
					FROM tbl_user_playstat_daily		 
					WHERE DATE_SUB('$yesterday', INTERVAL 28 DAY) <= today AND today < '$yesterday'	
					UNION ALL		
					SELECT useridx, today		 
					FROM tbl_user_playstat_daily_ios		 
					WHERE DATE_SUB('$yesterday', INTERVAL 28 DAY) <= today AND today < '$yesterday'		
					UNION ALL		
					SELECT useridx, today		 
					FROM tbl_user_playstat_daily_android		
					WHERE DATE_SUB('$yesterday', INTERVAL 28 DAY) <= today AND today < '$yesterday'		
					UNION ALL		
					SELECT useridx, today		  
					FROM tbl_user_playstat_daily_amazon		 
					WHERE DATE_SUB('$yesterday', INTERVAL 28 DAY) <= today AND today < '$yesterday'		
				) tt GROUP BY useridx HAVING COUNT(DISTINCT today) >= 23;";
		$user_list = $db_other->gettotallist($sql);
		
		for($i=0; $i<sizeof($user_list); $i++)
		{
			$useridx = $user_list[$i]["useridx"];
			$d28_play_days = $user_list[$i]["d28_play_days"];
			
			if($insert_sql == "" && $insertcount == 0)
			{
				$insert_sql = "INSERT INTO tbl_lucky_wheel_target(useridx, d28_play_days, is_new, writedate) VALUES($useridx, $d28_play_days, 1, NOW())";
				$insertcount ++;
			}
			else if($insertcount < 100)
			{
				$insert_sql .= ",($useridx, $d28_play_days, 1, NOW())";
				$insertcount ++;
			}
			else 
			{
				$insert_sql .= ",($useridx, $d28_play_days, 1, NOW()) ";
				$insert_sql .= " ON DUPLICATE KEY UPDATE d28_play_days = VALUES(d28_play_days), is_new = VALUES(is_new), writedate = VALUES(writedate)";				
				$db_main2->execute($insert_sql);
				
				$insertcount = 0;
				$insert_sql = "";
			}
		}
		
		if($insert_sql != "")
		{
			$insert_sql .= ",($useridx, $d28_play_days, 1, NOW()) ";
			$insert_sql .= " ON DUPLICATE KEY UPDATE d28_play_days = VALUES(d28_play_days), is_new = VALUES(is_new), writedate = VALUES(writedate)";				
			$db_main2->execute($insert_sql);
			
			$insertcount = 0;
			$insert_sql = "";
		}
		
		$delete_sql = "DELETE FROM tbl_lucky_wheel_target WHERE is_new = 0";
		$db_main2->execute($delete_sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	$db_main2->end();
	$db_other->end();	
?>