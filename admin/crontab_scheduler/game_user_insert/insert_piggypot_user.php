<?
	include("../../common/common_include.inc.php");

	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();	
	
	$db_main2->execute("SET wait_timeout=3600");	
	$db_other->execute("SET wait_timeout=3600");	
	
	$port = "";
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/game_user_insert/insert_piggypot_user") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
					$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
			flock($fp, LOCK_EX);
	
			if (!$fp) {
			echo "Fail";
				exit;
		}
			else
			{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
		
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/game_user_insert/insert_piggypot_user") !== false)
			{
				$process_list = explode("|", $result[$i]);
		
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("insert_piggypot_user Dead Lock Kill!");
			}
		}
		
		fwrite($fp, $content, strlen($content));
		fclose($fp);
		
		exit();
	}
	

	
	try
	{
		$sql = "SELECT useridx, currentcoin
				FROM	
				(	
					SELECT today, useridx, SUM(moneyin) AS moneyin, SUM(h_moneyin) AS h_moneyin, currentcoin	
					FROM	
					(	
						SELECT today, useridx, moneyin, h_moneyin, currentcoin FROM `tbl_user_playstat_daily` WHERE today >= DATE_SUB(NOW(), INTERVAL 1 WEEK)	
						UNION ALL	
						SELECT today, useridx, moneyin, h_moneyin, currentcoin FROM `tbl_user_playstat_daily_ios` WHERE today >= DATE_SUB(NOW(), INTERVAL 1 WEEK)	
						UNION ALL	
						SELECT today, useridx, moneyin, h_moneyin, currentcoin FROM `tbl_user_playstat_daily_android` WHERE today >= DATE_SUB(NOW(), INTERVAL 1 WEEK)	
						UNION ALL	
						SELECT today, useridx, moneyin, h_moneyin, currentcoin FROM `tbl_user_playstat_daily_amazon` WHERE today >= DATE_SUB(NOW(), INTERVAL 1 WEEK)	
					) t1 GROUP BY today, useridx ORDER BY today DESC	
				) t2 GROUP BY useridx HAVING currentcoin/ROUND(AVG(moneyin + h_moneyin)*0.03, 0) <= 2;";
		$piggypot_user_info = $db_other->gettotallist($sql);
		
		$user_cnt = 0;
		$insert_useridx = "";
		
		for($i=0; $i<sizeof($piggypot_user_info); $i++)
		{
			$piggypot_user = $piggypot_user_info[$i]["useridx"];

			$sql = "SELECT COUNT(*) FROM tbl_user_piggy_pot_".($piggypot_user % 10)." WHERE useridx = $piggypot_user AND collectdate != '0000-00-00 00:00:00' AND collectdate <= DATE_SUB(NOW(), INTERVAL 1 DAY);";
			$check_piggypot_user = $db_main2->getvalue($sql);		

			if($check_piggypot_user > 0)
			{
				if($insert_useridx== "")
					$insert_useridx = "($piggypot_user)";
				else
					$insert_useridx .= ",($piggypot_user)";
			
			
				$user_cnt++;
			}

			if($user_cnt != 0 && $user_cnt % 100 == 0)
			{
				$sql = "INSERT INTO tbl_piggy_pot_no_interval_user(useridx) VALUES $insert_useridx ON DUPLICATE KEY UPDATE useridx = VALUES(useridx);";
				$db_main2->execute($sql);
				
				$user_cnt = 0;
				$insert_useridx = "";
			}			
		}

		if($insert_useridx != "")
		{
			$sql = "INSERT INTO tbl_piggy_pot_no_interval_user(useridx) VALUES $insert_useridx ON DUPLICATE KEY UPDATE useridx = VALUES(useridx);";
			$db_main2->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	$db_main2->end();
	$db_other->end();	
?>
