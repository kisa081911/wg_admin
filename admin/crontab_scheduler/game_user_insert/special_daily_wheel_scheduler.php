<?
    include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");

	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	$db_main->execute("SET wait_timeout=72000");
	$db_main2->execute("SET wait_timeout=72000");
	$db_other->execute("SET wait_timeout=72000");
	
	ini_set("memory_limit", "-1");
	
	$today = date("Y-m-d");
	
	write_log("Special Daily Wheel Data Backup Start");
	
	$sql = "SELECT
				LEFT(writedate,10) AS today, useridx, type, category, is_enable, logincount,
				usercoin_1, moneyin_1, spincount_1, freecoin_1, losscoin_1,
				usercoin_2, moneyin_2, spincount_2, freecoin_2, losscoin_2
			FROM tbl_special_daily_wheel_target_user ";
	$backup_list = $db_main2->gettotallist($sql);
	
	$insert_sql = "";
	$insert_cnt = 0;
	
	for($i=0; $i<sizeof($backup_list); $i++)
	{
		//$today = $backup_list[$i]["today"];
		$useridx = $backup_list[$i]["useridx"];
		$type = $backup_list[$i]["type"];
		$category = $backup_list[$i]["category"];
		$is_enable = $backup_list[$i]["is_enable"];
		$logincount = $backup_list[$i]["logincount"];
		$usercoin_1 = $backup_list[$i]["usercoin_1"];
		$moneyin_1 = $backup_list[$i]["moneyin_1"];
		$spincount_1 = $backup_list[$i]["spincount_1"];
		$freecoin_1 = $backup_list[$i]["freecoin_1"];
		$losscoin_1 = $backup_list[$i]["losscoin_1"];
		$usercoin_2 = $backup_list[$i]["usercoin_2"];
		$moneyin_2 = $backup_list[$i]["moneyin_2"];
		$spincount_2 = $backup_list[$i]["spincount_2"];
		$freecoin_2 = $backup_list[$i]["freecoin_2"];
		$losscoin_2 = $backup_list[$i]["losscoin_2"];
	
		if($insert_sql == "")
			$insert_sql = "INSERT INTO tbl_special_daily_wheel_target_user_log VALUES('$today', $useridx, $type, $category, $is_enable, $logincount, $usercoin_1, $moneyin_1, $spincount_1, $freecoin_1, $losscoin_1, $usercoin_2, $moneyin_2, $spincount_2, $freecoin_2, $losscoin_2)";
		else
			$insert_sql .= ", ('$today', $useridx, $type, $category, $is_enable, $logincount, $usercoin_1, $moneyin_1, $spincount_1, $freecoin_1, $losscoin_1, $usercoin_2, $moneyin_2, $spincount_2, $freecoin_2, $losscoin_2)";
	
		$insert_cnt++;
	
		if($insert_cnt >= 1000)
		{
			$db_other->execute($insert_sql);
			$insert_sql = "";
			$insert_cnt = 0;
		}
	}
	
	if($insert_sql != "")
	{
		$db_other->execute($insert_sql);
		$insert_sql = "";
		$insert_cnt = 0;
	}
	
	write_log("Special Daily Wheel Data Backup End");
	
	write_log("Special Daily Wheel Scheduler Start");
	
	// 데이터 초기화 및 준비 작업
	$truncate_sql = "truncate table t5_special_wheel_user_buydate";
	$db_redshift->execute($truncate_sql);
	
	$truncate_sql = "TRUNCATE TABLE tbl_special_wheel_user_type";
	$db_other->execute($truncate_sql);
	
	$update_sql = "UPDATE tbl_special_daily_wheel_target_user SET is_update = 0";
	$db_main2->execute($update_sql);
	
	$truncate_sql = "TRUNCATE TABLE _t5_payer_299";
	$db_other->execute($truncate_sql);
	
	// 기본 대상자
	// 누적 결제 $299 불 Check -> $999 변경
	$sql = "SELECT useridx FROM tbl_user_detail WHERE useridx > 20000 AND vip_point >= 2 AND vip_point >= 999 ";
	$payer_list = $db_main->gettotallist($sql);
	
	$insert_sql = "";
	$insert_cnt = 0;
	
	for($i=0; $i<sizeof($payer_list); $i++)
	{
		$useridx = $payer_list[$i]["useridx"];
		
		if($insert_cnt == 0)
			$insert_sql = "INSERT INTO _t5_payer_299 VALUES($useridx)";
		else
			$insert_sql .= ",($useridx)";
		
		$insert_cnt++;
		
		if($insert_count >= 1000)
		{
			$db_other->execute($insert_sql); 
			
			$insert_sql = "";
			$insert_cnt = 0;
		}	
	}
	
	if($insert_sql != "")
	{
		$db_other->execute($insert_sql);
			
		$insert_sql = "";
		$insert_cnt = 0;
	}
	
	// 최근 14일 동안 4일 이상 슬롯 플레이, 누적결제 $299불 이상, 결제 이탈 28일 이상 유저 체크.
    $sql = "SELECT useridx, COUNT(today) AS login_count, MIN(ellapsed) AS ellapsed
			FROM
			(
				SELECT today, useridx, MIN(ellapsed) AS ellapsed
				FROM
				(
					SELECT today, t1.useridx, (IF(purchasecount > 0, 0, days_after_purchase) + DATEDIFF((SELECT MAX(today) FROM tbl_user_playstat_daily), today)) AS ellapsed 
					FROM tbl_user_playstat_daily t1 JOIN `_t5_payer_299` t2 ON t1.useridx = t2.useridx
					WHERE today >= DATE_SUB((SELECT MAX(today) FROM tbl_user_playstat_daily), INTERVAL 13 DAY)
					UNION ALL
					SELECT today, t1.useridx, (IF(purchasecount > 0, 0, days_after_purchase) + DATEDIFF((SELECT MAX(today) FROM tbl_user_playstat_daily_ios), today)) AS ellapsed 
					FROM tbl_user_playstat_daily_ios t1 JOIN `_t5_payer_299` t2 ON t1.useridx = t2.useridx
					WHERE today >= DATE_SUB((SELECT MAX(today) FROM tbl_user_playstat_daily_ios), INTERVAL 13 DAY)
					UNION ALL
					SELECT today, t1.useridx, (IF(purchasecount > 0, 0, days_after_purchase) + DATEDIFF((SELECT MAX(today) FROM tbl_user_playstat_daily_android), today)) AS ellapsed 
					FROM tbl_user_playstat_daily_android t1 JOIN `_t5_payer_299` t2 ON t1.useridx = t2.useridx
					WHERE today >= DATE_SUB((SELECT MAX(today) FROM tbl_user_playstat_daily_android), INTERVAL 13 DAY)
					UNION ALL
					SELECT today, t1.useridx, (IF(purchasecount > 0, 0, days_after_purchase) + DATEDIFF((SELECT MAX(today) FROM tbl_user_playstat_daily_amazon), today)) AS ellapsed 
					FROM tbl_user_playstat_daily_amazon t1 JOIN `_t5_payer_299` t2 ON t1.useridx = t2.useridx
					WHERE today >= DATE_SUB((SELECT MAX(today) FROM tbl_user_playstat_daily_amazon), INTERVAL 13 DAY)
				) AS t1
				GROUP BY today, useridx 	
			) AS t2
			GROUP BY useridx
			HAVING login_count >= 4 AND ellapsed >= 28";

    $ntl_datalist = $db_other->gettotallist($sql);
    
    $ntl_type1_userlist = "";
    
    $insert_sql = "";
    $insert_count = 0;
    
    for($i=0; $i<sizeof($ntl_datalist); $i++)
    {
    	$useridx = $ntl_datalist[$i]["useridx"];
    	$login_playcount = $ntl_datalist[$i]["login_count"];
    	
    	$sql = "SELECT COUNT(*)
		    	FROM (
			    	SELECT orderidx FROM tbl_product_order WHERE STATUS = 1 AND useridx = $useridx AND writedate > DATE_SUB(NOW(), INTERVAL 28 DAY) LIMIT 1
			    	UNION ALL
			    	SELECT orderidx FROM tbl_product_order_mobile WHERE STATUS = 1 AND useridx = $useridx AND writedate > DATE_SUB(NOW(), INTERVAL 28 DAY) LIMIT 1
		    	) t1 ";
    	$purchase_count = $db_main->getvalue($sql);
    	
    	if($purchase_count == "0")
    	{
    		if($insert_sql == "")
    			$insert_sql = "INSERT INTO tbl_special_wheel_user_type VALUES($useridx, $login_playcount, 0)";
    		else
    			$insert_sql .= ", ($useridx, $login_playcount, 0)";
    				 
    		$insert_count++;
    				 
    		if($insert_count >= 1000)
    		{
    			$db_other->execute($insert_sql);
    			$insert_sql = "";
    			$insert_count = 0;
    		}
    	}
    }
    
    if($insert_sql != "")
    {
    	$db_other->execute($insert_sql);
    	$insert_sql = "";
    	$insert_count = 0;
    }
    
    $insert_sql = "";
    
    for($i=0; $i<10; $i++)
    {
    	$sql = "SELECT t1.useridx, MAX(writedate) AS last_buydate, t2.login_count
				FROM tbl_product_order_all t1 JOIN tbl_special_wheel_user_type t2 ON t1.useridx = t2.useridx
				WHERE t2.useridx%10 = $i AND STATUS = 1 AND money >= 18 AND writedate >= DATE_SUB(NOW(), INTERVAL 1 YEAR) 
				GROUP BY useridx ";
	    $pay_userlist = $db_other->gettotallist($sql);
	    		
	    for($j=0; $j<sizeof($pay_userlist); $j++)
	    {
	    	$pay_useridx = $pay_userlist[$j]["useridx"];
	    	$last_buydate = $pay_userlist[$j]["last_buydate"];
	    	$login_count = $pay_userlist[$j]["login_count"];
	    			
	    	if($ntl_type1_userlist == "")
	    		$ntl_type1_userlist = $pay_useridx;
	    	else
	    		$ntl_type1_userlist .= ",".$pay_useridx;
	    			
	    	if($insert_sql == "")
	    		$insert_sql = "INSERT INTO t5_special_wheel_user_buydate VALUES($pay_useridx, '$last_buydate', $login_count)";
	    	else
	    		$insert_sql .= ", ($pay_useridx, '$last_buydate', $login_count)";
	    }
	    
	    if($insert_sql != "")
	    {
	    	$db_redshift->execute($insert_sql);
	    	$insert_sql = "";
	    }
    }
    
    $update_sql = "UPDATE tbl_special_wheel_user_type SET type = 1 WHERE useridx in ($ntl_type1_userlist)";
    $db_other->execute($update_sql);
    
    $update_sql = "UPDATE tbl_special_wheel_user_type SET type = 2 WHERE type = 0";
    $db_other->execute($update_sql);
    

    // Type 1
    // 마지막 구매 날짜 기준 정보 가져오기
    $sql = "SELECT useridx, login_count, AVG(currentcoin) as currentcoin, ROUND(SUM(moneyin/10000)) as moneyin, SUM(playcount) as playcount, SUM(freecoin) as freecoin, ROUND(SUM(loss_amount)) as loss_amount
			FROM
			(
				SELECT t1.useridx, login_count, currentcoin,  moneyin, playcount, freecoin, (moneyin - (moneyin * winrate / 100)) AS loss_amount
				FROM t5_user_playstat_daily t1 JOIN t5_special_wheel_user_buydate t2 ON t1.useridx = t2.useridx
				WHERE today >= dateadd(day, -12, last_buydate) AND today <= dateadd(day, +1, last_buydate)
				UNION ALL 
				SELECT t1.useridx, login_count, currentcoin,  moneyin, playcount, freecoin, (moneyin - (moneyin * winrate / 100)) AS loss_amount
				FROM t5_user_playstat_daily_ios t1 JOIN t5_special_wheel_user_buydate t2 ON t1.useridx = t2.useridx
				WHERE today >= dateadd(day, -12, last_buydate) AND today <= dateadd(day, +1, last_buydate)
				UNION ALL
				SELECT t1.useridx, login_count, currentcoin,  moneyin, playcount, freecoin, (moneyin - (moneyin * winrate / 100)) AS loss_amount
				FROM t5_user_playstat_daily_android t1 JOIN t5_special_wheel_user_buydate t2 ON t1.useridx = t2.useridx
				WHERE today >= dateadd(day, -12, last_buydate) AND today <= dateadd(day, +1, last_buydate)
				UNION ALL
				SELECT t1.useridx, login_count, currentcoin,  moneyin, playcount, freecoin, (moneyin - (moneyin * winrate / 100)) AS loss_amount
				FROM t5_user_playstat_daily_amazon t1 JOIN t5_special_wheel_user_buydate t2 ON t1.useridx = t2.useridx
				WHERE today >= dateadd(day, -12, last_buydate) AND today <= dateadd(day, +1, last_buydate)
			) AS t1
			GROUP BY useridx, login_count ";
    $last_purchase_list = $db_redshift->gettotallist($sql);
    
    $insert_sql = "";
    $insert_count = 0;
    	
    for($i=0; $i<sizeof($last_purchase_list); $i++)
    {
    	$useridx = $last_purchase_list[$i]["useridx"];
    	$login_count = $last_purchase_list[$i]["login_count"];
    	$current_coin = $last_purchase_list[$i]["currentcoin"];
    	$moneyin = $last_purchase_list[$i]["moneyin"];
    	$playcount = $last_purchase_list[$i]["playcount"];
    	$freecoin = $last_purchase_list[$i]["freecoin"];
    	$loss_amount = $last_purchase_list[$i]["loss_amount"];
    	
    	$is_enable = 1;
    		
    	if($insert_sql == "")
    		$insert_sql = "INSERT INTO tbl_special_daily_wheel_target_user(useridx, type, category, is_enable, logincount, usercoin_2, moneyin_2, spincount_2, freecoin_2, losscoin_2, is_update, writedate) VALUES($useridx, 1, 0, $is_enable, $login_count, $current_coin, $moneyin, $playcount, $freecoin, $loss_amount, 1, NOW())";
    	else
    		$insert_sql .= ", ($useridx, 1, 0, $is_enable, $login_count, $current_coin, $moneyin, $playcount, $freecoin, $loss_amount, 1, NOW())";
    	
    	$insert_count++;
    		
    	if($insert_count > 1000)
    	{
    		$insert_sql .= " ON DUPLICATE KEY UPDATE type = VALUES(type), category = VALUES(category), is_enable = VALUES(is_enable), logincount = VALUES(logincount), usercoin_2 = VALUES(usercoin_2), moneyin_2 = VALUES(moneyin_2), spincount_2 = VALUES(spincount_2), freecoin_2 = VALUES(freecoin_2), losscoin_2 = VALUES(losscoin_2), is_update = VALUES(is_update), writedate = VALUES(writedate)";

    		$db_main2->execute($insert_sql);
    		
    		$insert_sql = "";
    		$insert_count = 0;
    	}
    }
    	
    if($insert_sql != "")
    {
    	$insert_sql .= " ON DUPLICATE KEY UPDATE type = VALUES(type), category = VALUES(category), is_enable = VALUES(is_enable), logincount = VALUES(logincount), usercoin_2 = VALUES(usercoin_2), moneyin_2 = VALUES(moneyin_2), spincount_2 = VALUES(spincount_2), freecoin_2 = VALUES(freecoin_2), losscoin_2 = VALUES(losscoin_2), is_update = VALUES(is_update), writedate = VALUES(writedate)";

    	$db_main2->execute($insert_sql);
    	
    	$insert_sql = "";
    	$insert_count = 0;
    }
    
    // 최근 2주 정보 가져오기
    $sql = "SELECT useridx, AVG(currentcoin) AS currentcoin, ROUND(SUM(moneyin/10000)) AS moneyin, SUM(playcount) AS playcount, SUM(freecoin) AS freecoin, ROUND(SUM(loss_amount)) AS loss_amount
			FROM
			(
				SELECT useridx, currentcoin,  moneyin, playcount, freecoin, (moneyin - (moneyin * winrate / 100)) AS loss_amount
				FROM tbl_user_playstat_daily WHERE useridx IN ($ntl_type1_userlist) AND today >= DATE_SUB((SELECT MAX(today) FROM tbl_user_playstat_daily), INTERVAL 13 DAY)
				UNION ALL 
				SELECT useridx, currentcoin,  moneyin, playcount, freecoin, (moneyin - (moneyin * winrate / 100)) AS loss_amount
				FROM tbl_user_playstat_daily_ios WHERE useridx IN ($ntl_type1_userlist) AND today >= DATE_SUB((SELECT MAX(today) FROM tbl_user_playstat_daily_ios), INTERVAL 13 DAY)
				UNION ALL
				SELECT useridx, currentcoin,  moneyin, playcount, freecoin, (moneyin - (moneyin * winrate / 100)) AS loss_amount
				FROM tbl_user_playstat_daily_android WHERE useridx IN ($ntl_type1_userlist) AND today >= DATE_SUB((SELECT MAX(today) FROM tbl_user_playstat_daily_android), INTERVAL 13 DAY)
				UNION ALL
				SELECT useridx, currentcoin,  moneyin, playcount, freecoin, (moneyin - (moneyin * winrate / 100)) AS loss_amount
				FROM tbl_user_playstat_daily_amazon WHERE useridx IN ($ntl_type1_userlist) AND today >= DATE_SUB((SELECT MAX(today) FROM tbl_user_playstat_daily_amazon), INTERVAL 13 DAY)
			) AS t1
			GROUP BY useridx";
    $recent_2week_list = $db_other->gettotallist($sql);
    
    $insert_sql = "";
    $insert_count = 0;
    	
    for($i=0; $i<sizeof($recent_2week_list); $i++)
    {
    	$useridx = $recent_2week_list[$i]["useridx"];
    	$current_coin = $recent_2week_list[$i]["currentcoin"];
    	$moneyin = $recent_2week_list[$i]["moneyin"];
    	$playcount = $recent_2week_list[$i]["playcount"];
    	$freecoin = $recent_2week_list[$i]["freecoin"];
    	$loss_amount = $recent_2week_list[$i]["loss_amount"];
    	
    	$is_enable = 1;
    		
    	if($insert_sql == "")
    		$insert_sql = "INSERT INTO tbl_special_daily_wheel_target_user(useridx, type, category, is_enable, usercoin_1, moneyin_1, spincount_1, freecoin_1, losscoin_1, is_update, writedate) VALUES($useridx, 1, 0, $is_enable, $current_coin, $moneyin, $playcount, $freecoin, $loss_amount, 1, NOW())";
    	else
    		$insert_sql .= ", ($useridx, 1, 0, $is_enable, $current_coin, $moneyin, $playcount, $freecoin, $loss_amount, 1, NOW()) ";
    	
    	$insert_count++;
    		
    	if($insert_count > 1000)
    	{
    		$insert_sql .= "ON DUPLICATE KEY UPDATE type = VALUES(type), category = VALUES(category), is_enable = VALUES(is_enable), usercoin_1 = VALUES(usercoin_1), moneyin_1 = VALUES(moneyin_1), spincount_1 = VALUES(spincount_1), freecoin_1 = VALUES(freecoin_1), losscoin_1 = VALUES(losscoin_1), is_update = VALUES(is_update), writedate = VALUES(writedate)";

    		$db_main2->execute($insert_sql);
    		
    		$insert_sql = "";
    		$insert_count = 0;
    	}
    }
    	
    if($insert_sql != "")
    {
    	$insert_sql .= "ON DUPLICATE KEY UPDATE type = VALUES(type), category = VALUES(category), is_enable = VALUES(is_enable), usercoin_1 = VALUES(usercoin_1), moneyin_1 = VALUES(moneyin_1), spincount_1 = VALUES(spincount_1), freecoin_1 = VALUES(freecoin_1), losscoin_1 = VALUES(losscoin_1), is_update = VALUES(is_update), writedate = VALUES(writedate)";

    	$db_main2->execute($insert_sql);
    	
    	$insert_sql = "";
    	$insert_count = 0;
    }
    
    // Type1 Category 설정
    $sql = "SELECT * FROM tbl_special_daily_wheel_setting ORDER BY TYPE, category ASC";
    $setting_list = $db_main2->gettotallist($sql);
    
    $wheel_type1_condition = array();
    $wheel_type2_condition = 0;
    
    for($i=0; $i<sizeof($setting_list); $i++)
    {
    	$type = $setting_list[$i]["type"];
    	$category = $setting_list[$i]["category"];
    	$condition_0 = $setting_list[$i]["condition_0"];
    	$condition_1 = $setting_list[$i]["condition_1"];
    	$condition_2 = $setting_list[$i]["condition_2"];
    	
    	if($type == "1")
    	{
    		$wheel_type1_condition[$category]["condition_0"] =  $condition_0;
    		$wheel_type1_condition[$category]["condition_1"] =  $condition_1;
    		$wheel_type1_condition[$category]["condition_2"] =  $condition_2;
    	}
    	else
    		$wheel_type2_condition = $condition_0;
    }
    
    // Special Daily Wheel Type 1 Category Update
    $sql = "SELECT
				useridx,
				(usercoin_1/usercoin_2 * 100) AS c_value_0,
				(moneyin_1/moneyin_2 * 100) AS c_value_1,
				(spincount_1/spincount_2 * 100) AS c_value_2
			FROM tbl_special_daily_wheel_target_user WHERE TYPE = 1 ";
    $type1_list = $db_main2->gettotallist($sql);
    
    $update_sql = "";
    
    for($i=0; $i<sizeof($type1_list); $i++)
    {
    	$useridx = $type1_list[$i]["useridx"];
    	$c_value_0 = $type1_list[$i]["c_value_0"];
    	$c_value_1 = $type1_list[$i]["c_value_1"];
    	$c_value_2 = $type1_list[$i]["c_value_2"];
    	 
    	if ($c_value_0 < $wheel_type1_condition[1]["condition_0"] && $c_value_1 < $wheel_type1_condition[1]["condition_1"] && $c_value_2 < $wheel_type1_condition[1]["condition_2"])
    	{
    		$update_sql = "UPDATE tbl_special_daily_wheel_target_user SET category = 1 WHERE useridx = $useridx";
    		$db_main2->execute($update_sql);
    	}
    }
    
    // 사용하지 않는 데이터 삭제
    $delete_sql = "DELETE FROM tbl_special_daily_wheel_target_user WHERE TYPE = 1 AND (is_update = 0 OR category = 0)";
    $db_main2->execute($delete_sql);
    
    // Type 2
    $sql = "SELECT useridx, login_count, AVG(currentcoin) AS currentcoin, ROUND(SUM(moneyin/10000)) AS moneyin, SUM(playcount) AS playcount, SUM(freecoin) AS freecoin, ROUND(SUM(loss_amount)) AS loss_amount
			FROM
			(
				SELECT t1.useridx, login_count, currentcoin,  moneyin, playcount, freecoin, (moneyin - (moneyin * winrate / 100)) AS loss_amount
				FROM tbl_user_playstat_daily t1 JOIN tbl_special_wheel_user_type t2 ON t1.useridx = t2.useridx
				WHERE TYPE = 2 AND today >= DATE_SUB((SELECT MAX(today) FROM tbl_user_playstat_daily), INTERVAL 13 DAY)
				UNION ALL
				SELECT t1.useridx, login_count, currentcoin,  moneyin, playcount, freecoin, (moneyin - (moneyin * winrate / 100)) AS loss_amount
				FROM tbl_user_playstat_daily_ios t1 JOIN tbl_special_wheel_user_type t2 ON t1.useridx = t2.useridx
				WHERE TYPE = 2 AND today >= DATE_SUB((SELECT MAX(today) FROM tbl_user_playstat_daily_ios), INTERVAL 13 DAY)
				UNION ALL
				SELECT t1.useridx, login_count, currentcoin,  moneyin, playcount, freecoin, (moneyin - (moneyin * winrate / 100)) AS loss_amount
				FROM tbl_user_playstat_daily_android t1 JOIN tbl_special_wheel_user_type t2 ON t1.useridx = t2.useridx
				WHERE TYPE = 2 AND today >= DATE_SUB((SELECT MAX(today) FROM tbl_user_playstat_daily_android), INTERVAL 13 DAY)
				UNION ALL
				SELECT t1.useridx, login_count, currentcoin,  moneyin, playcount, freecoin, (moneyin - (moneyin * winrate / 100)) AS loss_amount
				FROM tbl_user_playstat_daily_amazon t1 JOIN tbl_special_wheel_user_type t2 ON t1.useridx = t2.useridx
				WHERE TYPE = 2 AND today >= DATE_SUB((SELECT MAX(today) FROM tbl_user_playstat_daily_amazon), INTERVAL 13 DAY)
			) AS t1
			GROUP BY useridx HAVING (SUM(playcount) / login_count) < $wheel_type2_condition ";
    
    $recent_2week_list = $db_other->gettotallist($sql);
    
    // Type 2 Special Daily Wheel
    $insert_sql = "";
    $insert_count = 0;
    
    for($i=0; $i<sizeof($recent_2week_list); $i++)
    {
    	$useridx = $recent_2week_list[$i]["useridx"];
    	$login_count = $recent_2week_list[$i]["login_count"];
    	$currentcoin = $recent_2week_list[$i]["currentcoin"];
    	$moneyin = $recent_2week_list[$i]["moneyin"];
    	$playcount = $recent_2week_list[$i]["playcount"];
    	$freecoin = $recent_2week_list[$i]["freecoin"];
    	$loss_amount = $recent_2week_list[$i]["loss_amount"];
    	
    	$is_enable = 1;
    	
    	$sql = "SELECT usercoin + coin AS last_coin FROM tbl_product_order_all WHERE useridx = $useridx ORDER BY writedate DESC LIMIT 1";
    	$last_coin = $db_other->getvalue($sql);
    	
    	if($currentcoin / $last_coin < 0.15)
    	{
    		if($insert_sql == "")
    		{
    			$insert_sql = "INSERT INTO tbl_special_daily_wheel_target_user(useridx, type, category, is_enable, logincount, usercoin_1, moneyin_1, spincount_1, freecoin_1, losscoin_1, is_update, writedate) VALUES($useridx, 2, 0, $is_enable, $login_count, $currentcoin, $moneyin, $playcount, $freecoin, $loss_amount, 1, NOW())";
    			$insert_count++;
    		}
    		else
    		{
    			$insert_sql .= ", ($useridx, 2, 0, $is_enable, $login_count, $currentcoin, $moneyin, $playcount, $freecoin, $loss_amount, 1, NOW()) ";
    			$insert_count++;
    		}
    		
    		if($insert_count > 1000)
    		{
    			$insert_sql .= "ON DUPLICATE KEY UPDATE type = VALUES(type), category = VALUES(category), is_enable = VALUES(is_enable), logincount = VALUES(logincount), usercoin_1 = VALUES(usercoin_1), moneyin_1 = VALUES(moneyin_1), spincount_1 = VALUES(spincount_1), freecoin_1 = VALUES(freecoin_1), losscoin_1 = VALUES(losscoin_1), is_update = VALUES(is_update), writedate = VALUES(writedate)";
    			$db_main2->execute($insert_sql);
    			
    			$insert_sql = "";
    			$insert_count = 0;
    		}
    	}
    }
    
    if($insert_sql != "")
    {
    	$insert_sql .= "ON DUPLICATE KEY UPDATE type = VALUES(type), category = VALUES(category), is_enable = VALUES(is_enable), logincount = VALUES(logincount), usercoin_1 = VALUES(usercoin_1), moneyin_1 = VALUES(moneyin_1), spincount_1 = VALUES(spincount_1), freecoin_1 = VALUES(freecoin_1), losscoin_1 = VALUES(losscoin_1), is_update = VALUES(is_update), writedate = VALUES(writedate)";
    	$db_main2->execute($insert_sql);
    }
    
    $delete_sql = "DELETE FROM tbl_special_daily_wheel_target_user WHERE TYPE = 2 AND is_update = 0;";
    $db_main2->execute($delete_sql);
    
    write_log("Special Daily Wheel Scheduler End");
    
    $db_main->end();
    $db_main2->end();
    $db_other->end();
	$db_redshift->end();
?>