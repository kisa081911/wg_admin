<?
    include("../../common/common_include.inc.php");
    require_once '../../common/android_library/ResponseData.php';
 	require_once '../../common/android_library/ResponseValidator.php';

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/mobile_purchase/mobile_android_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/mobile_purchase/mobile_android_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("mobile_android_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_friend = new CDatabase_Friend();
	$db_livestats = new CDatabase_Livestats();
	$db_inbox = new CDatabase_Inbox();
	
	$db_main->execute("SET wait_timeout=60");
	$db_main2->execute("SET wait_timeout=60");
	$db_friend->execute("SET wait_timeout=60");
	$db_livestats->execute("SET wait_timeout=60");
	$db_inbox->execute("SET wait_timeout=60");

	//Your key, copy and paste from https://market.android.com/publish/editProfile
	define('PUBLIC_KEY', 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtku9HCIPaIMUbfoUawSkYiGiOHqZXCUiJPvTN84khXBtvjEK49s2Ex4Zg9TCzOt0J64mcgY4jse60oYZtyeRXz71BFUoVwfCnNNBE4AkprdyC13xGHU/GI/+m9xnW3BEbLmcoGEmmMmvz9sQjIVQLW5SP9z/e1mziiQphIxo/Nh7v99jiCS4nMGvPPFZcsqJto1ornLOXOCeY6IxMrB39Leg3Yr32Liz4XYZHN2v6V0pQttxPFcp1syGX+0Ni/Eyd3ho0wlmaKkod8N3n/JWDxuaAtuzWIPf0Aw3fiNtXvPw/eo5hjwG10EDcxmdBEyU/ajfCXg4UY9LRI/6KQzmdQIDAQAB');
	
	//Your app's package name, e.g. com.example.yourapp
	define('PACKAGE_NAME', '');
	
	$sql = "SELECT *, (SELECT facebookcredit FROM tbl_product_mobile WHERE productidx = tbl_product_order_mobile. productidx) AS targetcredit ".
			", (SELECT category FROM tbl_product_mobile WHERE productidx = tbl_product_order_mobile. productidx) AS product_category ".
			" FROM tbl_product_order_mobile WHERE os_type=2 AND status IN (0, 4, 5) ORDER BY writedate ASC";
	$receipt_list = $db_main->gettotallist($sql);
	
	for ($i=0; $i<sizeof($receipt_list); $i++)
	{
		$useridx = $receipt_list[$i]["useridx"];
		$productidx = $receipt_list[$i]["productidx"];
		$receipt = $receipt_list[$i]["receipt"];
		$errcount = $receipt_list[$i]["errcount"];
		$orderno = $receipt_list[$i]["orderno"];
		$orderidx = $receipt_list[$i]["orderidx"];
		$money = $receipt_list[$i]["money"];
		$mobile_facebookcredit = $receipt_list[$i]["facebookcredit"];
		$usercoin = $receipt_list[$i]["usercoin"];
		$coin = $receipt_list[$i]["coin"];
		$basecoin = $receipt_list[$i]["basecoin"];
		$gift_coin = $receipt_list[$i]["gift_coin"];
		$special_more = $receipt_list[$i]["special_more"];	
		$signature = $receipt_list[$i]["signature"];
		$product_targetcredit = $receipt_list[$i]["targetcredit"];
		$product_category = $receipt_list[$i]["product_category"];

		// verify the receipt
		try
		{
			//The | delimited receipt data from the licensing server
			//The signature provided with the response data (Base64)
			//if you wish to inspect or use the response data, you can create
			//a response object and pass that in to the Validator's constructor
			//$response = new AndroidMarket_Licensing_ResponseData($responseData);
			$validator = new AndroidMarket_Licensing_ResponseValidator(PUBLIC_KEY, PACKAGE_NAME);
				
			$valid = $validator->verify($receipt, $signature);

			
			if($valid)
			{
				
				// receipt is valid, now do something with $info
				$sql = "UPDATE tbl_product_order_mobile SET status = 1 WHERE orderidx=".$receipt_list[$i]["orderidx"];
				$db_main->execute($sql);
				
				$sql = "SELECT vip_level FROM tbl_user_detail WHERE useridx=$useridx";
				$before_viplevel = $db_main->getvalue($sql);
				
				$before_viplevel = ($before_viplevel == "") ? 0 : $before_viplevel;
				
				// VIP Point Update
				$sql = "SELECT ".
						"(SELECT IFNULL(SUM(vip_point), 0) AS vip_point FROM tbl_product_order WHERE useridx = $useridx AND status = 1) + ".
						"	(SELECT IFNULL(SUM(vip_point), 0) AS vip_point FROM tbl_product_order_mobile WHERE useridx = $useridx AND status = 1) AS total_vip_point";
				$vip_point = $db_main->getvalue($sql);
								
				$vip_level = get_vip_level($vip_point);
				
				$sql = "UPDATE tbl_user_detail SET vip_level=$vip_level, vip_point=$vip_point WHERE useridx = $useridx;";
				$db_main->execute($sql);
				
				$sql = "UPDATE tbl_user_detail SET vip_level=$vip_level, vip_point=$vip_point WHERE useridx = $useridx;";
				$db_friend->execute($sql);
				
				$sql = "SELECT ".
						"(SELECT IFNULL(SUM(facebookcredit), 0) AS totalcredit FROM tbl_product_order WHERE useridx = $useridx AND status = 1) + ".
						"(SELECT IFNULL(SUM(facebookcredit), 0) AS totalcredit FROM tbl_product_order_mobile WHERE useridx = $useridx AND status = 1);";
				$totalcredit = $db_main->getvalue($sql);
				
				try
				{
					// user_order Table Update
					$params = array('cmd'=>'update_user_order', 'useridx'=>$useridx, 'totalcredit'=>$totalcredit);
					log_node_async("log", $params);
				}
				catch (Exception $e)
				{
					write_log($e->getMessage());
				}
				
				$is_vip_up = 0;				
				
				// VIP_Level Up
				if($before_viplevel < $vip_level)
				{
					// VIP Level Up시 웰컴기프트, 데일리기프트 바로 발급
					$is_vip_up = 1;
						
					$sql = "SELECT member_level, member_reward FROM tbl_membership_info WHERE $before_viplevel < member_level AND member_level <= $vip_level";
					$vip_reward_info = $db_main->gettotallist($sql);
						
					for($j=0; $j<sizeof($vip_reward_info); $j++)
					{
						$info_level = $vip_reward_info[$j]["member_level"];
						$info_reward = $vip_reward_info[$j]["member_reward"];
				
						$sql = "INSERT tbl_user_vip_levelup_log(useridx, vip_level, reward, writedate) VALUES($useridx, $info_level, $info_reward, NOW());";
						$db_main2->execute($sql);
					}
				}
				
				$sql = "SELECT COUNT(*) FROM tbl_user_first_order WHERE useridx=$useridx";
				$isfirstorder = $db_main2->getvalue($sql);
					
				if($isfirstorder == 0)
				{
					// 첫 결제 정보 추가					
					$sql = "SELECT IFNULL(DATE_FORMAT(MIN(writedate), '%Y-%m-%d'), '0000-00-00') AS firstbuydate ".
							"FROM ( ".
							"	SELECT writedate ".
							"	FROM ( ".
							"		SELECT writedate ".
							"		FROM tbl_product_order ".
							"		WHERE useridx = $useridx AND status = 1 ".
							"		ORDER BY orderidx ASC ".
							"		LIMIT 1 ".
							"	) t1 ".
							"	UNION ALL ".
							"	SELECT writedate ".
							"	FROM ( ".
							"		SELECT writedate ".
							"		FROM tbl_product_order_mobile ".
							"		WHERE useridx = $useridx AND status = 1 ".
							"		ORDER BY orderidx ASC ".
							"		LIMIT 1 ".
							"	) t2 ".
							") t3";
					$firstbuydate = $db_main->getvalue($sql);
						
					if($firstbuydate != "0000-00-00 00:00:00")
					{
						$sql = "SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS createdate, adflag, platform FROM tbl_user_ext WHERE useridx=$useridx";
						$join_info = $db_main->getarray($sql);
							
						$join_createdate = $join_info["createdate"];
						$join_adflag = $join_info["adflag"];
						$join_platform = $join_info["platform"];
							
						$sql = "INSERT INTO tbl_user_first_order(useridx, platform, join_date, purchase_date) VALUES($useridx, $join_platform, '$join_createdate', '$firstbuydate') ON DUPLICATE KEY UPDATE purchase_date=VALUES(purchase_date);";
						$db_main2->execute($sql);	
					}
				}
				
				if(9 <= $productidx && $productidx <= 13)
				{				
					// 첫 결제자 5회 선물 지급 -- start				
					$sql = "SELECT COUNT(*) FROM tbl_user_firstbuy_gift WHERE useridx=$useridx";
					$is_first_gift = $db_main2->getvalue($sql);
					
					if($is_first_gift == 0)
					{
						// 구매 코인의 20%로 아래식 계산
						$bonus_coin = 500000;
							
						if(($coin / 5) >= 2000000)
							$bonus_coin = 2000000;
						else if(($coin / 5) >= 1500000)
							$bonus_coin = 1500000;
						else if(($coin / 5) >= 1000000)
							$bonus_coin = 1000000;
							
					
						$sql = "INSERT INTO tbl_user_firstbuy_gift(useridx, userid, platform, purchasedate, bonus_coin, leftcount, sendmode, senddate) ".
								"VALUES($useridx, '$userid', 0, NOW(), $bonus_coin, 5, 1, NOW());";
						$db_main2->execute($sql);
					
						$sql = "UPDATE tbl_user_newuser_gift SET leftcount = 0 WHERE useridx=$useridx;";
						$db_main2->execute($sql);
					}
					// 첫 결제자 5회 선물 지급 -- end
				}
				
				$sql = "SELECT platform, purchase_date, DATEDIFF(NOW(), purchase_date) AS after_day_firstbuy FROM tbl_user_first_order WHERE useridx=$useridx";
				$firtbuy_info = $db_main2->getarray($sql);
					
				$platform = $firtbuy_info["platform"];
				$purchase_date = $firtbuy_info["purchase_date"];
				$after_day_firstbuy = $firtbuy_info["after_day_firstbuy"];
					
				if($after_day_firstbuy != "" && $after_day_firstbuy < 91)
				{
					$today = date("Y-m-d");					
				
					$sql = "SELECT COUNT(*) FROM tbl_user_pay_retention_log_".($useridx % 10)." WHERE today='$today' AND useridx=$useridx";
					$isinsert = $db_livestats->getvalue($sql);
				
					if($isinsert == 0)
					{
						$sql = "SELECT adflag, platform FROM tbl_user_ext WHERE useridx=$useridx";
						$join_info = $db_main->getarray($sql);
							
						$join_adflag = $join_info["adflag"];
						$join_platform = $join_info["platform"];
						
						$sql = "INSERT INTO tbl_user_pay_retention_log_".($useridx % 10)."(today, useridx, category, adflag, retention_adflag, firstbuydate, after_day_firstbuy) ".
								"VALUES('$today', $useridx, $join_platform, '$join_adflag', '', '$purchase_date', '$after_day_firstbuy') ".
								"ON DUPLICATE KEY UPDATE firstbuydate=VALUES(firstbuydate), after_day_firstbuy=VALUES(after_day_firstbuy);";
							
						$db_livestats->execute($sql);
					}
				}
				
				//신규 유저 부양 결제시 승률 제외
				$sql = "SELECT COUNT(*) FROM tbl_user_boost WHERE useridx  = $useridx AND boosttype = 1";
				$new_user_boost_check = $db_main->getvalue($sql);
				
				if($new_user_boost_check > 0)
				{
					$sql = "UPDATE tbl_user_boost SET boosttype = 0 WHERE useridx  = $useridx AND boosttype = 1";
					$db_main->execute($sql);
				}
					
				// 신규 Threshodl Offer
				// 사용자가 $4.9 이상 결제 시
				if($product_targetcredit >= 49)
				{
				    $th_flag = 0; // 대상자 여부
				    
				    $sql = "UPDATE tbl_threshold_offer SET enable = 0 WHERE useridx = $useridx";
				    $db_main2->execute($sql);
				    
				    // 1차 대상자 체크(비할인상품 제외)
				    $sql = "SELECT IFNULL(SUM(ccount), 0) AS totalcount
                			FROM (
                				SELECT COUNT(*) AS ccount
                				FROM tbl_product_order
                				WHERE useridx = $useridx AND STATUS = 1
                				AND writedate > DATE_SUB(NOW(), INTERVAL 4 WEEK)
                				AND facebookcredit >= 49 AND basecoin > 0 AND coin > basecoin AND coin <= basecoin * 1.7
                				UNION ALL
                				SELECT COUNT(*) AS ccount
                				FROM tbl_product_order_mobile
                				WHERE useridx = $useridx AND STATUS = 1
                				AND writedate > DATE_SUB(NOW(), INTERVAL 4 WEEK)
                				AND money >= 4.9 AND basecoin > 0 AND coin > basecoin AND coin <= basecoin * 1.7 AND (couponidx > 0 OR product_type NOT IN (1))
                			) AS a";
				    $th_totalcount = $db_main->getvalue($sql);
				    
				    if($th_totalcount >= 3)
				    {
				        $th_flag = 1;
				        $discount_web_tail = " AND coin > basecoin";
				        $discount_mobile_tail = " AND coin > basecoin AND (couponidx > 0 OR product_type NOT IN (1))";
				    }
				    else
				    {
				        // 2차 대상자 체크(비할인상품 포함)
				        $sql = "SELECT IFNULL(SUM(ccount), 0) AS totalcount
                				FROM (
                					SELECT COUNT(*) AS ccount
                					FROM tbl_product_order
                					WHERE useridx = $useridx AND STATUS = 1
                					AND writedate > DATE_SUB(NOW(), INTERVAL 4 WEEK)
                					AND facebookcredit >= 49 AND basecoin > 0 AND coin <= basecoin * 1.7
                					UNION ALL
                					SELECT COUNT(*) AS ccount
                					FROM tbl_product_order_mobile
                					WHERE useridx = $useridx AND STATUS = 1
                					AND writedate > DATE_SUB(NOW(), INTERVAL 4 WEEK)
                					AND money >= 4.9 AND basecoin > 0 AND coin <= basecoin * 1.7
                				) AS a";
				        $th_totalcount = $db_main->getvalue($sql);
				        
				        if($th_totalcount >= 2)
				        {
				            $th_flag = 1;
				            $discount_web_tail = "";
				            $discount_mobile_tail = "";
				        }
				    }
				    
				    if($th_flag == 1)
				    {
				        $sql = "SELECT
                					IF(SUM(basecoin) > 0, ROUND(SUM(coin-basecoin) * 100 / SUM(basecoin)), 0) AS salerate,
                					MAX(maxcredit) AS maxcredit,
                					IF(SUM(ccount) > 0, ROUND(SUM(sumcredit) / SUM(ccount)), 0) AS avgcredit
                				FROM (
                					SELECT
                						IFNULL(SUM(coin), 0) AS coin,
                						IFNULL(SUM(basecoin), 0) AS basecoin,
                						IFNULL(MAX(facebookcredit), 0) AS maxcredit,
                						IFNULL(SUM(facebookcredit), 0) AS sumcredit,
                						COUNT(*) AS ccount
                					FROM tbl_product_order
                					WHERE useridx = $useridx AND STATUS = 1
                					AND writedate > DATE_SUB(NOW(), INTERVAL 4 WEEK)
                					AND facebookcredit >= 49 AND basecoin > 0 AND coin <= basecoin * 1.7 $discount_web_tail
                					UNION ALL
                					SELECT
                						IFNULL(SUM(coin), 0) AS coin,
                						IFNULL(SUM(basecoin), 0) AS basecoin,
                						IFNULL(MAX(ROUND(money*10)), 0) AS maxcredit,
                						IFNULL(SUM(ROUND(money*10)), 0) AS sumcredit,
                						COUNT(*) AS ccount
                					FROM tbl_product_order_mobile
                					WHERE useridx = $useridx AND STATUS = 1
                					AND writedate > DATE_SUB(NOW(), INTERVAL 4 WEEK)
                					AND money >= 4.9 AND basecoin > 0 AND coin <= basecoin * 1.7 $discount_mobile_tail
                				) AS a ";
				        $threshold_data = $db_main->getarray($sql);
				        
				        if($threshold_data != "")
				        {
				            $th_salerate = $threshold_data["salerate"];
				            $th_maxcredit = $threshold_data["maxcredit"];
				            $th_avgcredit = $threshold_data["avgcredit"];
				            
				            $th_salerate = $th_salerate - ($th_salerate % 5);
				            
				            if($th_salerate < 25)
				                $th_salerate = 25;
				                
				                // 근접한 상품으로 적용
				                if ($th_maxcredit > (4990 + 2990) / 2)
				                    $th_maxcredit = 4990;
			                    else if ($th_maxcredit > (2990 + 1990) / 2)
			                        $th_maxcredit = 2990;
		                        else if ($th_maxcredit > (1990 + 990) / 2)
		                            $th_maxcredit = 1990;
	                            else if ($th_maxcredit > (990 + 590) / 2)
	                                $th_maxcredit = 990;
                                else if ($th_maxcredit > (590 + 390) / 2)
                                    $th_maxcredit = 590;
                                else
                                    $th_maxcredit = 390;
                                    
                                // 근접한 상품으로 적용
                                if ($th_avgcredit > (4990 + 2990) / 2)
                                    $th_avgcredit = 4990;
                                else if ($th_avgcredit > (2990 + 1990) / 2)
                                    $th_avgcredit = 2990;
                                else if ($th_avgcredit > (1990 + 990) / 2)
                                    $th_avgcredit = 1990;
                                else if ($th_avgcredit > (990 + 590) / 2)
                                    $th_avgcredit = 990;
                                else if ($th_avgcredit > (590 + 390) / 2)
                                    $th_avgcredit = 590;
                                else if ($th_avgcredit > (390 + 190) / 2)
                                    $th_avgcredit = 390;
                                else
                                    $th_avgcredit = 190;
                                    
                                    
                                    // 상품 정보가 같은 경우 최고 상품을 한단계 위 상품으로 처리.
                                    $maxcredit_list = array(190, 390, 590, 990, 1990, 2990, 4990);
                                    
                                    if(($th_maxcredit == $th_avgcredit) && $th_maxcredit != 9990)
                                    {
                                        for($i=0; $i<9; $i++)
                                        {
                                            if($maxcredit_list[$i] == $th_maxcredit)
                                            {
                                                $th_maxcredit = $maxcredit_list[$i+1];
                                                break;
                                            }
                                        }
                                    }
                                    
                                    // 평균 상품이 플랫폼 허용 최대 상품일 경우
                                    // 1. 최고 상품을 플랫폼 허용 최고 상품으로 설정
                                    // 2. 평균 상품은 한단계 아래 상품으로 설정.
                                    if($th_avgcredit == 4990)
                                    {
                                        $th_maxcredit = 4990;
                                        $th_avgcredit = 2990;
                                    }
                                    
                                    $th_coin_p = $usercoin + $coin;
                                    $target_type = 1;
                                    
                                    $sql = "SELECT COUNT(*) FROM tbl_threshold_offer WHERE useridx = $useridx";
                                    $offer_count = $db_main2->getvalue($sql);
                                    
                                    if($offer_count > 0)
                                    {
                                        $sql = "UPDATE tbl_threshold_offer SET target_type = $target_type, activated = 0, coin_p = $th_coin_p, max_product = $th_maxcredit, avg_product = $th_avgcredit, sale_rate = $th_salerate, thresholdcount = 0, enable = 1, offer_checkdate = '0000-00-00 00:00:00', updatedate = NOW(), writedate = NOW() WHERE useridx = $useridx ";
                                    }
                                    else
                                    {
                                        $sql = "INSERT INTO tbl_threshold_offer(useridx, target_type, enable, coin_p, max_product, avg_product, sale_rate, updatedate, writedate) VALUES($useridx, $target_type, 1, $th_coin_p, $th_maxcredit, $th_avgcredit, $th_salerate, NOW(), NOW());";
                                    }
                                    
                                    $db_main2->execute($sql);
				        }
				    }
				}
				
				$mode33_flag = 0;
					
				// mode33
				// $5 이상 첫결제
				if($product_targetcredit >= 50)
				{
					//life time에서 5불 이상 결제 횟수
					$sql = "SELECT COUNT(*)	".
							"FROM	".
							"(	".
							"	SELECT useridx, (facebookcredit/10) AS money FROM tbl_product_order  WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50	".
							"	UNION ALL	".
							"	SELECT useridx, money FROM tbl_product_order_mobile  WHERE useridx = $useridx AND STATUS = 1 AND money >= 4.99	".
							") tt;";
				
					$payment_first_5usd_count = $db_main->getvalue($sql);
				
					if($payment_first_5usd_count == 1)
					{
						$mode33_flag = 1;
							
						$current_usercoin = $usercoin + $coin;
							
						$sql = "SELECT COUNT(*) FROM tbl_user_boost WHERE useridx = $useridx ";
						$already_boost = $db_main->getvalue($sql);
							
						$sql = "INSERT INTO tbl_user_boost(useridx, boosttype, product_credit, usercoin, logincount, moneyin, moneyout, playcount, support_rate, writedate, startdate, cyclecount, mode) VALUES($useridx, 33, $product_targetcredit, $current_usercoin, 1, 0, 0, 0, 0, NOW(), NOW(), 1, 0) ".
								"ON DUPLICATE KEY UPDATE boosttype=VALUES(boosttype), product_credit=VALUES(product_credit), usercoin=VALUES(usercoin), logincount=VALUES(logincount), moneyin=VALUES(moneyin), moneyout=VALUES(moneyout), playcount=VALUES(playcount), support_rate=VALUES(support_rate), writedate=VALUES(writedate), startdate=VALUES(startdate), cyclecount=VALUES(cyclecount), mode=VALUES(mode);";

						$db_main->execute($sql);
							
						$sql = "INSERT INTO tbl_user_boost_history (useridx, boosttype, writedate) VALUES($useridx, 33, NOW());";
						$db_main2->execute($sql);
					}
				}
					
				// mode33 예외처리
				$sql = "SELECT COUNT(*) AS is_exist, UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(startdate) AS diff_hour FROM tbl_user_boost WHERE useridx = $useridx AND boosttype = 33";
				$mode33_data = $db_main->getarray($sql);
					
				$mode33_exist = $mode33_data["is_exist"];
				$mode33_diffhour = $mode33_data["diff_hour"];
					
				if($mode33_exist == 1 && $mode33_diffhour < 60 * 60 * 5)
				{
					$mode33_flag = 1;
					
					$current_usercoin = $usercoin + $coin;
					
					$sql = "INSERT INTO tbl_user_boost(useridx, boosttype, product_credit, usercoin, logincount, moneyin, moneyout, support_rate, writedate) VALUES($useridx, 33, $product_targetcredit, $current_usercoin, 1, 0, 0, 0, NOW()) ".
								"ON DUPLICATE KEY UPDATE boosttype=VALUES(boosttype), product_credit=VALUES(product_credit), usercoin=VALUES(usercoin), logincount=VALUES(logincount), moneyin=VALUES(moneyin), moneyout=VALUES(moneyout), support_rate=VALUES(support_rate), writedate=VALUES(writedate);";
					$db_main->execute($sql);
				}
				else
					$mode33_flag = 0;

				//ODD USER
				if($product_targetcredit >= 50 && $mode33_flag == 0)
				{
					// 미스
// 					$rand_miss_check = 0;
// 					$rand_miss_num = rand(0, 99);
					
// 					$sql = "SELECT bet_miss_num FROM tbl_27mode_boost_bet_amount WHERE os_type = 2";
// 					$bet_miss_num = $db_main2->getvalue($sql);
					
// 					if($rand_miss_num < $bet_miss_num)
// 						$rand_miss_check = 1;

					$rand_miss_check = 0;
					$rand_miss_num = rand(0, 9);
						
					$sql = "SELECT COUNT(orderidx) AS cnt_pay  ".
							"	FROM ".
							"	( ".
							"		SELECT facebookcredit AS money, orderidx ".
							"		FROM tbl_product_order ".
							"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND writedate < DATE_SUB(NOW(), INTERVAL 1 DAY) ".
							"		UNION ALL ".
							"		SELECT facebookcredit AS money, orderidx ".
							"		FROM tbl_product_order_mobile ".
							"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND writedate < DATE_SUB(NOW(), INTERVAL 1 DAY) ".
							"	) t1";
					$cnt_pay = $db_main->getvalue($sql);
						
					if($cnt_pay < 10)
					{
						$rand_miss_check = 0;
					}
					else if($totalcredit < 990)
					{
						if($product_targetcredit < 90)
							$rand_miss_check = 1;
					}
					else if(990 <= $totalcredit && $totalcredit < 4990)
					{
						if($rand_miss_num < 1 ||$product_targetcredit < 190) // 미스 10%
							$rand_miss_check = 1;
					}
					else if(4990 <= $totalcredit && $totalcredit < 9990)
					{
						if($rand_miss_num < 2 || $product_targetcredit < 390) // 미스 20%
							$rand_miss_check = 1;
					}
					else if(9990 <= $totalcredit && $totalcredit < 19990)
					{
						if($rand_miss_num < 3 || $product_targetcredit < 390) // 미스 30%
							$rand_miss_check = 1;
					}
					else if(19990 <= $totalcredit && $totalcredit < 49990)
					{
						if($rand_miss_num < 5 || $product_targetcredit < 590) // 미스 50%
							$rand_miss_check = 1;
					}
					else if(49990 <= $totalcredit && $totalcredit < 99990)
					{
						if($rand_miss_num < 7 || $product_targetcredit < 990) // 미스 70%
							$rand_miss_check = 1;
					}
					else if(99990 <= $totalcredit)
					{
						if($rand_miss_num < 9 || $product_targetcredit < 990) // 미스 90%
							$rand_miss_check = 1;
					}
						
					if($rand_miss_check == 1)
					{
						$sql = "SELECT IFNULL(DATEDIFF(NOW(), MAX(recent_buydate)), 0) AS day_after_pay ".
								"FROM (".
								"	SELECT MAX(writedate) AS recent_buydate FROM tbl_product_order WHERE useridx = $useridx  AND ".
								"	orderidx < (SELECT MAX(orderidx) FROM tbl_product_order WHERE useridx = $useridx AND STATUS = 1) AND STATUS = 1 AND facebookcredit >= 50 ".
								"	UNION ALL ".
								"	SELECT MAX(writedate) AS recent_buydate FROM tbl_product_order_mobile WHERE useridx = $useridx AND ".
								"	orderidx < (SELECT MAX(orderidx) FROM tbl_product_order_mobile WHERE useridx = $useridx AND STATUS = 1) AND STATUS = 1 AND facebookcredit >= 50 ".
								") t1";
						$pay_28days = $db_main->getvalue($sql);
							
						if($pay_28days >= 28)
							$rand_miss_check = 0;
					}

					if($is_lucky_offer_boost == 1)
						$rand_miss_check = 1;

					if($rand_miss_check == 0)
					{
						$sql = "SELECT COUNT(*) FROM tbl_user_boost WHERE useridx = $useridx AND boosttype = 27 AND NOW() < DATE_ADD(writedate, INTERVAL 1 HOUR)";
						$check_27mode_user = $db_main->getvalue($sql);
				
						if($check_27mode_user == 1)
							$rand_miss_check = 1;
					}
					
					$sql = "SELECT COUNT(*) FROM tbl_user_boost WHERE useridx = $useridx AND boosttype IN (3, 4)  AND NOW() < DATE_ADD(writedate, INTERVAL 24 HOUR);";
					$retention_highroller_user_boost_check = $db_main->getvalue($sql);
						
					$sql = "SELECT COUNT(*) FROM tbl_user_boost WHERE useridx = $useridx AND boosttype = 33 AND NOW() < DATE_ADD(writedate, INTERVAL 48 HOUR);";
					$first_buy_boost_check = $db_main->getvalue($sql);
						
					if($rand_miss_check == 0)
					{
						
						$sql = "SELECT DATEDIFF(NOW(), createdate) AS day_after_install FROM tbl_user WHERE useridx=$useridx";
						$dayafterinstall = $db_main->getvalue($sql);
						
						// 1 week
						$sql = "SELECT SUM(pa) AS pa, SUM(freq) AS freq, SUM(threshold) AS threshold ".
								"FROM ".
								"(	 ".
								"	SELECT IFNULL(ROUND(AVG(money/10), 2), 0) AS pa, COUNT(orderidx)*4 AS freq, 0 AS threshold ".
								"	FROM ".
								"	( ".
								"		SELECT facebookcredit AS money, orderidx ".
								"		FROM tbl_product_order ".
								"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND DATE_SUB(NOW(), INTERVAL 7 DAY) <= writedate AND writedate < DATE_SUB(NOW(), INTERVAL 1 DAY) ".
								"		UNION ALL ".
								"		SELECT facebookcredit AS money, orderidx ".
								"		FROM tbl_product_order_mobile ".
								"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND DATE_SUB(NOW(), INTERVAL 7 DAY) <= writedate AND writedate < DATE_SUB(NOW(), INTERVAL 1 DAY) ".
								"	) t1 ".
								"	UNION ALL ".
								"	SELECT 0 AS pa, 0 AS freq, IFNULL(ROUND(AVG(usercoin/coin)*100), 10) AS threshold ".
								"	FROM ".
								"	( ".
								"		SELECT usercoin, coin ".
								"		FROM tbl_product_order ".
								"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND  DATE_SUB(NOW(), INTERVAL 7 DAY) <= writedate AND writedate < DATE_SUB(NOW(), INTERVAL 1 DAY) AND usercoin < coin ".
								"		UNION ALL ".
								"		SELECT usercoin, coin ".
								"		FROM tbl_product_order_mobile ".
								"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND  DATE_SUB(NOW(), INTERVAL 7 DAY) <= writedate AND writedate < DATE_SUB(NOW(), INTERVAL 1 DAY) AND usercoin < coin ".
								"	) t2 ".
								") t3";
						$pay_info_1w = $db_main->getarray($sql);
							
						$pa_1w = $pay_info_1w["pa"];
						$freq_1w = $pay_info_1w["freq"];
						$threshold_1w = $pay_info_1w["threshold"];
							
						// 4 week
						$sql = "SELECT SUM(pa) AS pa, SUM(freq) AS freq, SUM(threshold) AS threshold ".
								"FROM ".
								"(	 ".
								"	SELECT IFNULL(ROUND(AVG(money/10), 2), 0) AS pa, COUNT(orderidx) AS freq, 0 AS threshold ".
								"	FROM ".
								"	( ".
								"		SELECT facebookcredit AS money, orderidx ".
								"		FROM tbl_product_order ".
								"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND DATE_SUB(NOW(), INTERVAL 28 DAY) <= writedate AND writedate < DATE_SUB(NOW(), INTERVAL 1 DAY) ".
								"		UNION ALL ".
								"		SELECT facebookcredit AS money, orderidx ".
								"		FROM tbl_product_order_mobile ".
								"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND DATE_SUB(NOW(), INTERVAL 28 DAY) <= writedate AND writedate < DATE_SUB(NOW(), INTERVAL 1 DAY) ".
								"	) t1 ".
								"	UNION ALL ".
								"	SELECT 0 AS pa, 0 AS freq, IFNULL(ROUND(AVG(usercoin/coin)*100), 10) AS threshold ".
								"	FROM ".
								"	( ".
								"		SELECT usercoin, coin ".
								"		FROM tbl_product_order ".
								"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND DATE_SUB(NOW(), INTERVAL 28 DAY) <= writedate AND writedate < DATE_SUB(NOW(), INTERVAL 1 DAY) AND usercoin < coin ".
								"		UNION ALL ".
								"		SELECT usercoin, coin ".
								"		FROM tbl_product_order_mobile ".
								"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND DATE_SUB(NOW(), INTERVAL 28 DAY) <= writedate AND writedate < DATE_SUB(NOW(), INTERVAL 1 DAY) AND usercoin < coin ".
								"	) t2 ".
								") t3";
						$pay_info_4w = $db_main->getarray($sql);
							
						$pa_4w = $pay_info_4w["pa"];
						$freq_4w = $pay_info_4w["freq"];
						$threshold_4w = $pay_info_4w["threshold"];
							
						// LifeTime
						$sql = "SELECT SUM(pa) AS pa, SUM(freq) AS freq, SUM(threshold) AS threshold  ".
								"FROM ".
								"(	 ".
								"	SELECT IFNULL(ROUND(AVG(money/10), 2), 0) AS pa, FLOOR(COUNT(orderidx)*28/$dayafterinstall) AS freq, 0 AS threshold  ".
								"	FROM ".
								"	( ".
								"		SELECT facebookcredit AS money, orderidx ".
								"		FROM tbl_product_order ".
								"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND writedate < DATE_SUB(NOW(), INTERVAL 1 DAY) ".
								"		UNION ALL ".
								"		SELECT facebookcredit AS money, orderidx ".
								"		FROM tbl_product_order_mobile ".
								"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND writedate < DATE_SUB(NOW(), INTERVAL 1 DAY) ".
								"	) t1 ".
								"	UNION ALL ".
								"	SELECT 0 AS pa, 0 AS freq, IFNULL(ROUND(AVG(usercoin/coin)*100), 10) AS threshold ".
								"	FROM ".
								"	( ".
								"		SELECT usercoin, coin ".
								"		FROM tbl_product_order ".
								"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND writedate < DATE_SUB(NOW(), INTERVAL 1 DAY) AND usercoin < coin ".
								"		UNION ALL ".
								"		SELECT usercoin, coin ".
								"		FROM tbl_product_order_mobile ".
								"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND writedate < DATE_SUB(NOW(), INTERVAL 1 DAY) AND usercoin < coin ".
								"	) t2 ".
								") t3";
						$pay_info_lt = $db_main->getarray($sql);
							
						$pa_lt = $pay_info_lt["pa"];
						$freq_lt = $pay_info_lt["freq"];
						$threshold_lt = $pay_info_lt["threshold"];
							
						$pa = $pa_1w;
						$freq = $freq_1w;
						$threshold = $threshold_1w;
							
						if($pa < $pa_4w)
						{
							$pa = $pa_4w;
							$freq = $freq_4w;
							$threshold = $threshold_4w;
						}
							
						if($pa < $pa_lt)
						{
							$pa = $pa_lt;
							$freq = $freq_lt;
							$threshold = $threshold_lt;
						}
							
						if($threshold == 0)
							$threshold = 30;
				
						$odd_support2_coin = round($coin * min(20, min(100, $threshold+10)) / 100);
				
						if($retention_highroller_user_boost_check == 0 && $first_buy_boost_check == 0)
						{
							$sql = "INSERT INTO tbl_user_boost(useridx, boosttype, logincount, moneyin, moneyout, support_rate, max_moneyin, support2_coin, max_moneyin2, mode, product_credit, writedate) ".
									"VALUES($useridx, 30, 1, 0, 0, 0, 0, $odd_support2_coin, 0, 0, 0, NOW()) ".
									"ON DUPLICATE KEY UPDATE boosttype=VALUES(boosttype), logincount=VALUES(logincount), moneyin=VALUES(moneyin), moneyout=VALUES(moneyout), ".
									"support_rate=VALUES(support_rate), max_moneyin=VALUES(max_moneyin), support2_coin=VALUES(support2_coin), max_moneyin2=VALUES(max_moneyin2), mode=VALUES(mode), product_credit=VALUES(product_credit), writedate=VALUES(writedate);";
							$db_main->execute($sql);
							
							$sql = "INSERT INTO tbl_user_boost_history (useridx, boosttype, writedate) VALUES($useridx, 30, NOW());";
							$db_main2->execute($sql);
						}
					}
				}
				
				//mode 27
				//$18 이상 구매 시 승률 부양 27번 전에 purchase log에  로그 남김
				if($money >= 18)
				{
					$sql = "SELECT COUNT(*) FROM tbl_user_boost_winrate WHERE useridx = $useridx AND NOW() < DATE_ADD(writedate, INTERVAL 1 HOUR)";
					$boost_winrate_condition = $db_main2->getvalue($sql);
				
					if($boost_winrate_condition < 1)
					{
						$sql = "SELECT useridx, purchase_amount, purchase_coin,  TIMESTAMPDIFF(HOUR, writedate, NOW()) AS purchase_interval , money_in, money_out, play_count, usercoin, writedate ".
								"FROM tbl_user_boost_winrate WHERE useridx = $useridx";
						$boost_winrate_list = $db_main2->getarray($sql);
				
						$boost_winrate_useridx = $boost_winrate_list["useridx"];
						$boost_winrate_purchase_amount = $boost_winrate_list["purchase_amount"];
						$boost_winrate_purchase_coin = $boost_winrate_list["purchase_coin"];
						$boost_winrate_purchase_interval = $boost_winrate_list["purchase_interval"];
						$boost_winrate_money_in = $boost_winrate_list["money_in"];
						$boost_winrate_money_out = $boost_winrate_list["money_out"];
						$boost_winrate_play_count = $boost_winrate_list["play_count"];
						$boost_winrate_usercoin = $boost_winrate_list["usercoin"];
						$boost_winrate_writedate = $boost_winrate_list["writedate"];
				
						$boost_winrate_purchase_amount =  ($boost_winrate_purchase_amount == ""? 0:$boost_winrate_purchase_amount);
						$boost_winrate_purchase_coin =  ($boost_winrate_purchase_coin == ""? 0:$boost_winrate_purchase_coin);
						$boost_winrate_purchase_interval =  ($boost_winrate_purchase_interval == ""? 0:$boost_winrate_purchase_interval);
						$boost_winrate_money_in =  ($boost_winrate_money_in == ""? 0:$boost_winrate_money_in);
						$boost_winrate_money_out =  ($boost_winrate_money_out == ""? 0:$boost_winrate_money_out);
						$boost_winrate_play_count =  ($boost_winrate_play_count == ""? 0:$boost_winrate_play_count);
						$boost_winrate_usercoin =  ($boost_winrate_usercoin == ""? 0:$boost_winrate_usercoin);
				
						if($boost_winrate_useridx != "")
						{
							$sql = "INSERT INTO tbl_user_purchase_play_log_".($useridx%10)."(useridx, purchaseamount, purchase_coin, purchase_interval, usercoin, money_in, money_out, play_count, purchase_writedate, writedate) ".
									"VALUES ($useridx, $boost_winrate_purchase_amount, $boost_winrate_purchase_coin, $boost_winrate_purchase_interval, $boost_winrate_usercoin, $boost_winrate_money_in, $boost_winrate_money_out, $boost_winrate_play_count, '$boost_winrate_writedate', NOW());";
							$db_main2->execute($sql);
						}
					}
				}
				
				$sql = "SELECT terms_money FROM tbl_27mode_boost_bet_amount WHERE os_type = 2";
				$mode27_money = $db_main2->getvalue($sql);
				
				if($mode27_money == "")
					$mode27_money = 18;				

				if($money >= $mode27_money && $rand_miss_check == 0)
				{
					//life time에서 18불 이상 결제 횟수
					$sql = "SELECT COUNT(*)	".
							"FROM	".
							"(	".
							"	SELECT useridx, (facebookcredit/10) AS money FROM tbl_product_order  WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 180	".
							"	UNION ALL	".
							"	SELECT useridx, money FROM tbl_product_order_mobile  WHERE useridx = $useridx AND STATUS = 1 AND money >= 18	".
							") tt;";
						
					$payment_18usd_count = $db_main->getvalue($sql);
						
					//N값 구함
					if($payment_18usd_count == 2)
						$n_count = 1;
					else if(3 <= $payment_18usd_count && $payment_18usd_count <= 19)
						$n_count = 2;
					else if(20 <= $payment_18usd_count)
						$n_count = 3;
						
					if($n_count > 0)
					{
						// N개 존재, 가장 오래된 writedate
						$sql = "SELECT COUNT(*) as purchase_cnt, min(purchase_writedate) as min_purchase_date  ".
								"FROM	".
								"(	".
								"	SELECT * FROM tbl_user_purchase_play_log_".($useridx%10)." WHERE useridx = $useridx AND purchaseamount >= 18 AND play_count >= 50 ORDER BY purchase_writedate DESC LIMIT $n_count ".
								") tt";
						$purchase_play_log_info = $db_main2->getarray($sql);
							
						$purchase_play_log_cnt = $purchase_play_log_info["purchase_cnt"];
						$min_purchase_date = $purchase_play_log_info["min_purchase_date"];	
						
						if($purchase_play_log_cnt == $n_count)
						{
							//N개 중 가장 오래된 writedate 이후 잭팟 기록 있으면 제외
							$sql = "SELECT COUNT(*) FROM tbl_jackpot_log WHERE useridx = $useridx AND writedate >= '$min_purchase_date';";
							$after_purchase_jackpot = $db_main->getvalue($sql);
							
							if($after_purchase_jackpot < 1)
							{
								$sql = "SELECT bet_amount, ROUND(bet_winrate, 2) AS bet_winrate FROM tbl_27mode_boost_bet_amount WHERE os_type = 2";
								$bet_info = $db_main2->getarray($sql);
								
								$bet_amount = $bet_info["bet_amount"];
								$bet_winrate = $bet_info["bet_winrate"];
								
								if($bet_amount == "")
									$bet_amount = 5000000;
								
								if($bet_winrate == "")
									$bet_winrate = 90;
							
								//money_out X 100 / money_in이 모두 디비설정값보다 낮은 경우
								$sql = "SELECT COUNT(*)	".
										"FROM	".
										"(	".
										"	SELECT IF(money_out*100/money_in > $bet_winrate, 1, 0) AS check_winrate FROM tbl_user_purchase_play_log_".($useridx%10)." WHERE useridx = $useridx AND purchaseamount >= 18 AND play_count >= 50 ORDER BY purchase_writedate DESC LIMIT $n_count	".
										") tt WHERE  check_winrate = 0";
								$purcahse_play_log_winrate = $db_main2->getvalue($sql);
			
								if($purcahse_play_log_winrate == $n_count)
								{				
									$diff_max_moneyin_first = round($money * $bet_amount);
									$diff_max_moneyin_second = round($money * 500000);
									
									$diff_max_moneyin_first =  ($diff_max_moneyin_first == ""? 0:$diff_max_moneyin_first);
									$diff_max_moneyin_second =  ($diff_max_moneyin_second == ""? 0:$diff_max_moneyin_second);
									
									$sql = "SELECT GREATEST(LEAST(money_in, $diff_max_moneyin_first), $diff_max_moneyin_second) FROM tbl_user_boost_winrate WHERE useridx = $useridx;";
									$max_moneyin = $db_main2->getvalue($sql);
							
									$sql = "SELECT bet_pay_winrate1, bet_pay_winrate2, bet_pay_winrate3, bet_t FROM tbl_27mode_boost_bet_amount WHERE os_type = 2";
									$bet_info = $db_main2->getarray($sql);
									
									$bet_pay_winrate1 = $bet_info["bet_pay_winrate1"];
									$bet_pay_winrate2 = $bet_info["bet_pay_winrate2"];
									$bet_pay_winrate3 = $bet_info["bet_pay_winrate3"];
									$bet_t = $bet_info["bet_t"];
										
									if($money < 40)
										$boost_winrate = $bet_pay_winrate1;
									else if($money < 100)
										$boost_winrate = $bet_pay_winrate2;
									else
										$boost_winrate = $bet_pay_winrate3;	
			
									if(($usercoin/$coin) > $bet_t)
										$rand_miss_check = 1;	
							
									if($rand_miss_check == 0)
									{
										$sql = "INSERT INTO tbl_user_boost(useridx, boosttype, logincount, moneyin, moneyout, support_rate, max_moneyin, support2_coin, max_moneyin2, mode, product_credit, writedate) ".
												"VALUES($useridx, 27, 1, 0, 0, $boost_winrate, $max_moneyin, $odd_support2_coin, 0, 0, 0, NOW()) ".
												"ON DUPLICATE KEY UPDATE boosttype=VALUES(boosttype), logincount=VALUES(logincount), moneyin=VALUES(moneyin), moneyout=VALUES(moneyout), ".
												"support_rate=VALUES(support_rate), max_moneyin=VALUES(max_moneyin), support2_coin=VALUES(support2_coin), max_moneyin2=VALUES(max_moneyin2), mode=VALUES(mode), product_credit=VALUES(product_credit), writedate=VALUES(writedate);";
										$db_main->execute($sql);	
										
										$sql = "INSERT INTO tbl_user_boost_history (useridx, boosttype, writedate) VALUES($useridx, 27, NOW());";
										$db_main2->execute($sql);
									}						
								
									if($rand_miss_check == 0)
									{
										$sql = "INSERT INTO tbl_individual_policy_history_".($useridx%10)."(useridx ,applytype, subtype, applydate) VALUES($useridx, 3, 1, NOW())";
										$db_main2->execute($sql);
									}
								}
							}
						}
					}
				}

				//$18 이상 구매 시 tbl_user_boost_winrate 로그 남김
				if($money >= 18)
				{
					if($boost_winrate_condition < 1)
					{
						//boost winrate
						$sql = "INSERT INTO tbl_user_boost_winrate(useridx, purchase_amount, purchase_coin, money_in, money_out, play_count, usercoin, writedate) VALUES($useridx, $money, $coin, 0, 0, 0, $usercoin, NOW()) ".
								"ON DUPLICATE KEY UPDATE purchase_amount = VALUES(purchase_amount), purchase_coin = VALUES(purchase_coin), money_in = VALUES(money_in), money_out = VALUES(money_out), play_count = VALUES(play_count), usercoin = VALUES(usercoin), writedate = VALUES(writedate);";
						$db_main2->execute($sql);
					}
					else
					{
						$sql = "UPDATE tbl_user_boost_winrate SET purchase_amount = purchase_amount + $money, purchase_coin = purchase_coin + $coin, writedate = NOW() WHERE useridx = $useridx";
						$db_main2->execute($sql);
					}
				}				

				if(26 <= $productidx && $productidx <= 41) // 스페셜 오퍼 상품
				{
					$sql = "UPDATE tbl_user_boost_special_offer SET buyable = 0, buydate = NOW(), showdate = DATE_SUB(NOW(), INTERVAL 24 HOUR) WHERE useridx = $useridx;";
					$sql .= "UPDATE tbl_user_special_offer SET buyable = 0, buydate = NOW() WHERE useridx = $useridx;";
					$sql .= "DELETE FROM tbl_user_boost WHERE useridx = $useridx AND boosttype = 21;";
					$db_main->execute($sql);
				
					$sql = "DELETE FROM tbl_whale_user_leave WHERE useridx=$useridx;";
					$sql .= "DELETE FROM tbl_whale_user_offer WHERE useridx=$useridx;";
					$sql .= "DELETE FROM tbl_whale_user_offer_28d WHERE useridx=$useridx;";
					$db_main2->execute($sql);
				}
				else if($product_targetcredit >= 50) // 5달러 이상
				{
					$sql = "UPDATE tbl_user_boost_special_offer SET buyable = 0, showdate = DATE_SUB(NOW(), INTERVAL 24 HOUR) WHERE useridx = $useridx;";
					$sql .= "UPDATE tbl_user_special_offer SET buyable = 0  WHERE useridx = $useridx AND buyable = 1 AND buydate < DATE_SUB(NOW(), interval 4 DAY);";
					$sql .= "DELETE FROM tbl_user_boost WHERE useridx = $useridx AND boosttype = 21;";
					$db_main->execute($sql);
				
					$sql = "DELETE FROM tbl_whale_user_leave WHERE useridx=$useridx;";
					$sql .= "DELETE FROM tbl_whale_user_offer WHERE useridx=$useridx;";	
					$sql .= "DELETE FROM tbl_whale_user_offer_28d WHERE useridx=$useridx;";
					$db_main2->execute($sql);
				}

				if($product_targetcredit >= 50) // 5달러 이상
				{
					$sql .= "UPDATE tbl_user_betting_status SET is_buy = 1, writedate = NOW() WHERE useridx=$useridx;";
					$sql .= "UPDATE tbl_user_betting_status_tmp SET is_buy = 1, writedate = NOW() WHERE useridx=$useridx;";
					$sql .= "UPDATE tbl_vip_nopurchase SET is_buy = 1, writedate = NOW() WHERE useridx=$useridx;";
					$sql .= "DELETE FROM tbl_vip_nopurchase_tmp WHERE useridx=$useridx;";
					$sql .= "UPDATE tbl_vip_purchase SET is_buy = 1, writedate = NOW() WHERE useridx=$useridx;";
					$sql .= "DELETE FROM tbl_vip_purchase_tmp WHERE useridx=$useridx;";
					$sql .= "UPDATE tbl_target_user SET is_buy = 1, writedate = NOW() WHERE useridx=$useridx;";
					$sql .= "UPDATE tbl_target_user_new SET isbuy = 1 WHERE useridx=$useridx;";
					$sql .= "DELETE FROM tbl_target_user_tmp WHERE useridx=$useridx;";
					$db_main2->execute($sql);
					
					$sql = "SELECT money*10 FROM tbl_except_payer_info";
					$except_money = $db_main2->getvalue($sql);
						
					if($product_targetcredit >= $except_money)
					{
						$sql = "INSERT INTO tbl_except_payer(useridx, money, writedate) VALUES ($useridx, $product_targetcredit/10, NOW()) ".
								"ON DUPLICATE KEY UPDATE money = VALUES(money), writedate = VALUES(writedate);";
						$db_main2->execute($sql);
					}
				}				
				
				if( $product_targetcredit >=500 )
				{
				    $sql = "SELECT IFNULL(TIMESTAMPDIFF(DAY, purchase_date, NOW()), 1) AS after_day_buy FROM tbl_bankrupt_log WHERE useridx = $useridx";
				    $after_day_buy = $db_main2->getvalue($sql);
				    $after_day_buy = ($after_day_buy == "")? 2 : $after_day_buy;
				    
				    if($after_day_buy >= 1)
				    {
				        $sql ="INSERT INTO `tbl_bankrupt_log` (`useridx`,  `receive_cnt`, `purchase_date`) VALUES('$useridx', 1, NOW()) ON DUPLICATE KEY UPDATE receive_cnt=1, purchase_date=VALUES(purchase_date);";
				        $db_main2->execute($sql);
				        $bankrupt_credit = $product_targetcredit;
				    }
				}
				
			}
			else
			{
				if($errcount == 0)
				{
					$sql = "UPDATE tbl_product_order_mobile SET errcount = 1 WHERE orderidx=".$receipt_list[$i]["orderidx"];
					$db_main->execute($sql);
				}
				else
				{
					// orderidx 중복 체크
					//unable to verify receipt, or receipt is not valid
					$sql = "INSERT INTO tbl_product_order_mobile_error(orderidx, category, errorno, message, writedate) VALUES('".$receipt_list[$i]["orderidx"]."', 2, '300', 'ordernum is not the same.(".$orderno.")', now());";
					$sql .= "UPDATE tbl_product_order_mobile SET status = 3, canceldate=NOW() WHERE orderidx=".$receipt_list[$i]["orderidx"];
					$db_main->execute($sql);

					$sql = "SELECT orderidx, useridx, orderno, b.money, a.facebookcredit, b.facebookcredit AS targetcredit, coin FROM ( SELECT * FROM tbl_product_order_mobile WHERE orderidx='".$receipt_list[$i]["orderidx"]."' ) a LEFT JOIN tbl_product_mobile b ON a.productidx = b.productidx";
					$data = $db_main->getarray($sql);
						
					$orderidx = $data["orderidx"];
					$money = $data["money"];
					$mobile_facebookcredit = $data["facebookcredit"];
					$coin = $data["coin"];
					$useridx = $data["useridx"];
					$org_coin = $data["coin"];
					$order_id = $data["orderno"];
					$product_targetcredit = $data["targetcredit"];
						
					// 취소 전 VIP Level 정보
					$sql = "SELECT vip_level FROM tbl_user_detail WHERE useridx=$useridx";
					$before_viplevel = $db_main->getvalue($sql);
						
					// VIP Point Update
					$sql = "SELECT SUM(vip_point) AS vip_point ".
							"FROM	".
							"(	".
							"	SELECT IFNULL(SUM(vip_point), 0) AS vip_point FROM tbl_product_order WHERE useridx = $useridx AND STATUS = 1	".
							"	UNION ALL	".
							"	SELECT IFNULL(SUM(vip_point), 0) AS vip_point FROM tbl_product_order_mobile WHERE useridx = $useridx AND STATUS = 1	".
							") t1";
					$vip_point = $db_main->getvalue($sql);
						
					$vip_level = get_vip_level($vip_point);
						
					$sql = "UPDATE tbl_user_detail SET vip_level=$vip_level, vip_point=$vip_point WHERE useridx = $useridx;";
					$db_main->execute($sql);
						
					$sql = "UPDATE tbl_user_detail SET vip_level=$vip_level, vip_point=$vip_point WHERE useridx = $useridx;";
					$db_friend->execute($sql);
						
					// Coin 회수
					$sql = "SELECT coin FROM tbl_user WHERE useridx=$useridx";
					$leftcoin = $db_main->getvalue($sql);
						
					if ($coin > 0)
					{
						if ($leftcoin >= $coin)
						{
							$sql = "INSERT INTO tbl_freecoin_admin_log(useridx, freecoin, reason, message, admin_id, writedate) VALUES($useridx, -$coin, 'refund로 인한 코인 차감', '', 'RealTime Update', NOW());";
							$db_main->execute($sql);
								
							$sql = "INSERT INTO tbl_user_cache_update(useridx, coin, writedate) VALUES($useridx, -$coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate=VALUES(writedate);";
							$db_main2->execute($sql);
						}
						else
						{
							$sql = "UPDATE tbl_product_order_mobile SET cancelleftcoin=".($coin-$leftcoin)." WHERE orderidx=$orderidx;".
									"INSERT INTO tbl_freecoin_admin_log(useridx, freecoin, reason, message, admin_id, writedate) VALUES($useridx, -$coin, 'refund로 인한 코인 차감(차감코인부족:".($coin-$leftcoin).")', '', 'RealTime Update', NOW());";
							$db_main->execute($sql);
								
							$sql = "INSERT INTO tbl_user_cache_update(useridx, coin, writedate) VALUES($useridx, -$coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate=VALUES(writedate);";
							$db_main2->execute($sql);
						}
					}
				
					$sql = "SELECT IFNULL(MAX(writedate), '0000-00-00 00:00:00') AS firstbuydate ".
							"FROM	".
							"(	".
							"	SELECT writedate FROM tbl_product_order WHERE useridx  = $useridx AND STATUS = 1	".
							"	UNION ALL	".
							"	SELECT writedate FROM tbl_product_order_mobile WHERE useridx  = $useridx AND STATUS = 1	".
							") t1 LIMIT 1";
					$lastbuydate = $db_main->getvalue($sql);
						
					// 쿠폰 반환 및 초기화
					$sql = "DELETE FROM tbl_coupon WHERE useridx=$useridx AND status=0;";
					$sql .= "UPDATE tbl_coupon_issue SET vip_level = $vip_level, base_credit=0, lastbuydate='$lastbuydate' WHERE useridx=$useridx;";
					$db_main2->execute($sql);
					
					//프리미엄 부스트 초기화
					$sql = "UPDATE tbl_user_premium_booster SET end_date = now() WHERE useridx=$useridx AND premium_type = 2;";
					$db_main2->execute($sql);
					
					if($db_main2->getvalue("SELECT COUNT(*) FROM tbl_user_retention_offer WHERE useridx = $useridx") > 0)
					{
					    if($product_type == 16)
					        $udpate_sql = "UPDATE tbl_user_retention_offer SET type = 0 , duedate =  DATE_SUB(NOW(), INTERVAL 1 DAY), buydate = DATE_SUB(NOW(), INTERVAL 28 DAY), facebookcredit = 0  WHERE useridx = $useridx";
				        else
				            $udpate_sql = "UPDATE tbl_user_retention_offer SET type = 0 , duedate =  DATE_SUB(NOW(), INTERVAL 1 DAY), buydate = DATE_SUB(NOW(), INTERVAL 28 DAY) WHERE useridx = $useridx";
				        
					    $db_main2->execute($udpate_sql);
					}
						
					// 첫 결제 정보 추가
					$sql = "SELECT IFNULL(MIN(writedate), '0000-00-00 00:00:00') AS firstbuydate ".
							"FROM	".
							"(	".
							"	SELECT writedate FROM tbl_product_order WHERE useridx  = $useridx AND STATUS = 1	".
							"	UNION ALL	".
							"	SELECT writedate FROM tbl_product_order_mobile WHERE useridx  = $useridx AND STATUS = 1	".
							") t1 LIMIT 1";
					$firstbuydate = $db_main->getvalue($sql);
						
					if($firstbuydate != "0000-00-00 00:00:00")
					{
						$sql = "SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS createdate FROM tbl_user WHERE useridx=$useridx";
						$join_createdate = $db_main->getvalue($sql);
				
						$sql = "INSERT INTO tbl_user_first_order(useridx, join_date, purchase_date) VALUES($useridx, '$join_createdate', '$firstbuydate') ON DUPLICATE KEY UPDATE purchase_date=VALUES(purchase_date);";
						$db_main2->execute($sql);
					}
					else
					{
						$sql = "DELETE FROM tbl_user_first_order WHERE useridx=$useridx";
						$db_main2->execute($sql);
					}

					$sql=" SELECT SUM(cnt) FROM ".
									"	( ".
									" SELECT COUNT(*) AS cnt FROM tbl_product_order WHERE useridx = $useridx AND facebookcredit >= 50 AND status = 1 ".
									"	    UNION ALL ".
									" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx = $useridx AND facebookcredit >= 50 AND status = 1 ".
									" ) t1";
					
					if($db_main->getvalue($sql) == 0)
					{
					    $sql = "UPDATE tbl_user_first_buy SET type = 0, otherbuy=0, shownow=0, buywritedate='000-00-00 00:00:00', lastshowdate='000-00-00 00:00:00' WHERE useridx=$useridx;";
					    $db_main->execute($sql);
					}
					
					// 첫 결제자 5회 선물 반환 및 지급 제거
					if($product_type != 6)
					{
						
						$sql ="SELECT bonus_coin, ABS(leftcount-5) AS cnt FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='';";
						$user_firstbuy_gift = $db_main2->getarray($sql);
						$return_coin = ($user_firstbuy_gift[0]*$user_firstbuy_gift[1]);
						
						$sql = "DELETE FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='';";
						$db_main2->execute($sql);
					}
					else if ($product_type == 6)
					{
						$sql ="SELECT bonus_coin, ABS(leftcount-5) AS cnt FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='$productkey' AND purchasedate = '$writedate';";
						$user_firstbuy_gift = $db_main2->getarray($sql);
						$return_coin = ($user_firstbuy_gift[0]*$user_firstbuy_gift[1]);
							
						$sql = "DELETE FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='$productkey';";
						$db_main2->execute($sql);
					}
					
					if($return_coin != 0 && $return_coin != "")
					{
						$sql = "INSERT INTO tbl_user_cache_update(useridx, coin, writedate) VALUES($useridx, -$return_coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate=VALUES(writedate);";
						$db_main2->execute($sql);
					}
					
					$sql = "DELETE FROM tbl_user_firstbuy_gift WHERE useridx=$useridx;";
					$db_main2->execute($sql);
					
					// 신규 사용자 7회 선물 지급 제거
					$sql = "UPDATE tbl_user_newuser_gift SET leftcount = 0 WHERE useridx=$useridx;";
					$db_main2->execute($sql);
					
					if($product_type == 14 || $product_type == 17)
					{
					    // Safe2Q 상품 제거
					    $sql = "DELETE FROM tbl_subscribe_user_bonus_info WHERE useridx=$useridx;";
					    $db_main2->execute($sql);
					}
									
					// New 승률부양 삭제
					$sql = "SELECT boosttype, max_moneyin FROM tbl_user_boost WHERE useridx = $useridx;";
					$user_boost_info = $db_main->getarray($sql);
				
					$user_boost_type = $user_boost_info["boosttype"];
					$user_boost_max_moneyin = $user_boost_info["max_moneyin"];
						
					if(($user_boost_type == 7 && $user_boost_max_moneyin > 0) || ($user_boost_type == 8 && $user_boost_max_moneyin > 0))
					{
						$change_max_moneyin = $user_boost_max_moneyin - $org_coin;
				
						if($change_max_moneyin < 0)
							$change_max_moneyin = 0;
				
						$sql = "UPDATE tbl_user_boost SET max_moneyin = $change_max_moneyin WHERE useridx = $useridx AND boosttype = $user_boost_type";
						$db_main->execute($sql);
					}
					else if($user_boost_type == 9 && $user_boost_max_moneyin > 0)
					{
						$sql = "SELECT MAX(multiple) FROM tbl_boost_metrics;";
						$max_multiple = $db_main->getvalue($sql);
							
						$change_max_moneyin = $user_boost_max_moneyin - ($org_coin * $max_multiple);
							
						if($change_max_moneyin < 0)
							$change_max_moneyin = 0;
				
						$sql = "UPDATE tbl_user_boost SET max_moneyin = $change_max_moneyin WHERE useridx = $useridx AND boosttype = $user_boost_type";
						$db_main->execute($sql);
					}
					else
					{
						$sql = "DELETE FROM tbl_user_boost WHERE useridx = $useridx;";
						$db_main->execute($sql);
					}

					// Bonus Coin 차감(3Day & 7Day)
					if(400 <= $product_targetcredit && $product_targetcredit <= 9990)
					{
						// 3Day Bonus coin 지급 제거
						$sql = "UPDATE tbl_user_buy_3day_gift SET leftcount = 0 WHERE useridx = $useridx  AND platform = 2 AND orderno = '$order_id'";
						$db_main2->execute($sql);
							
						// 3Day Bonus coin 발급 inbox삭제
						$sql = "SELECT inboxidx FROM tbl_user_buy_3day_gift_log WHERE useridx = $useridx AND type = 1 AND orderno = '$order_id'";
						$refund_3day_inbox_list = $db_main2->gettotallist($sql);
						
						$delete_3day_inboxidx_info = "";
							
						for($j = 0; $j < sizeof($refund_3day_inbox_list); $j++)
						{
							$delete_3day_inboxidx = $refund_3day_inbox_list[$j]["inboxidx"];
				
							if($delete_3day_inboxidx_info == "")
								$delete_3day_inboxidx_info = "$delete_3day_inboxidx";
							else
								$delete_3day_inboxidx_info .= ",$delete_3day_inboxidx"; 
						}
							
						if($delete_3day_inboxidx_info != "")
						{
							$sql_delete_log = "DELETE FROM tbl_user_inbox_".($useridx%20)." WHERE useridx = $useridx AND category = 111 AND inboxidx IN ($delete_3day_inboxidx_info);";						
							$db_inbox->execute($sql_delete_log);
						}
					}
				
					// 7Day Bonus coin 지급 제거
					if($product_targetcredit == 600)
					{
						$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY) WHERE useridx = $useridx";
						$db_main2->execute($sql);
					}
					else if($product_targetcredit == 1000)
					{
						$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY) WHERE useridx = $useridx";
						$db_main2->execute($sql);
					}
					else if($product_targetcredit == 2000)
					{
						$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY), inboxdate  = DATE_SUB(inboxdate, INTERVAL 7 DAY) WHERE useridx = $useridx";
						$db_main2->execute($sql);
					}
					else if($product_targetcredit == 3000 || $product_targetcredit == 5000 || $product_targetcredit == 7770 || $product_targetcredit == 9990)
					{
						$sql = "SELECT IF(spindate != '0000-00-00' , 1, 0) AS spin_check, IF(stampdate != '0000-00-00' , 1, 0) AS stamp_check, IF(inboxdate != '0000-00-00', 1, 0) AS inbox_check FROM tbl_user_buy_7day_gift WHERE useridx = $useridx ";
						$date_info = $db_main2->getarray($sql);
				
						$spin_check = $date_info["spin_check"];
						$stamp_check = $date_info["stamp_check"];
						$inbox_check = $date_info["inbox_check"];
				
						if($inbox_check == 1)
						{
							$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY), inboxdate  = DATE_SUB(inboxdate, INTERVAL 7 DAY), ".
									"spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
							$db_main2->execute($sql);
						}
						else if($stamp_check == 1)
						{
							$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY), ".
									"spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
							$db_main2->execute($sql);
						}
						else if($spin_check == 1)
						{
							$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), ".
									"spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
							$db_main2->execute($sql);
						}
						else
						{
							$sql = "UPDATE tbl_user_buy_7day_gift SET spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
							$db_main2->execute($sql);
						}
				
						$sql = "DELETE FROM  tbl_user_buy_vip_wheel WHERE useridx = $useridx AND orderno = '$order_id'";
						$db_main2->execute($sql);
					}
							
					// tbl_user_online 삭제
					$sql = "DELETE FROM tbl_user_online_".($useridx%10)." WHERE useridx=$useridx;";
					$sql .= "DELETE FROM tbl_user_online_sync2 WHERE useridx=$useridx;";
					$db_friend->execute($sql);
				}
			}
		}
		catch (Exception $ex)
		{
			if($errcount == 0)
			{
				$sql = "UPDATE tbl_product_order_mobile SET errcount = 1 WHERE orderidx=".$receipt_list[$i]["orderidx"];
				$db_main->execute($sql);
			}
			else
			{
				// orderidx 중복 체크
				//unable to verify receipt, or receipt is not valid
				$sql = "INSERT INTO tbl_product_order_mobile_error(orderidx, category, errorno, message, writedate) VALUES('".$receipt_list[$i]["orderidx"]."', 2, '".$ex->getCode()."', '".$ex->getMessage()."', now());";
				$sql .= "UPDATE tbl_product_order_mobile SET status = 3 WHERE orderidx=".$receipt_list[$i]["orderidx"];
				$db_main->execute($sql);

				$sql = "SELECT orderidx, useridx, orderno, b.money, a.facebookcredit, b.facebookcredit AS targetcredit, coin FROM ( SELECT * FROM tbl_product_order_mobile WHERE orderidx='".$receipt_list[$i]["orderidx"]."' ) a LEFT JOIN tbl_product_mobile b ON a.productidx = b.productidx";
				$data = $db_main->getarray($sql);
			
				$orderidx = $data["orderidx"];
				$money = $data["money"];
				$coin = $data["coin"];
				$mobile_facebookcredit = $data["facebookcredit"];
				$useridx = $data["useridx"];
				$org_coin = $data["coin"];
				$order_id = $data["orderno"];
				$product_targetcredit = $data["targetcredit"];
			
				// 취소 전 VIP Level 정보
				$sql = "SELECT vip_level FROM tbl_user_detail WHERE useridx=$useridx";
				$before_viplevel = $db_main->getvalue($sql);
			
				// VIP Point Update
				$sql = "SELECT SUM(vip_point) AS vip_point ".
						"FROM	".
						"(	".
						"	SELECT IFNULL(SUM(vip_point), 0) AS vip_point FROM tbl_product_order WHERE useridx = $useridx AND STATUS = 1	".
						"	UNION ALL	".
						"	SELECT IFNULL(SUM(vip_point), 0) AS vip_point FROM tbl_product_order_mobile WHERE useridx = $useridx AND STATUS = 1	".
						") t1";
				$vip_point = $db_main->getvalue($sql);
			
				$vip_level = get_vip_level($vip_point);
			
				$sql = "UPDATE tbl_user_detail SET vip_level=$vip_level, vip_point=$vip_point WHERE useridx = $useridx;";
				$db_main->execute($sql);
			
				$sql = "UPDATE tbl_user_detail SET vip_level=$vip_level, vip_point=$vip_point WHERE useridx = $useridx;";
				$db_friend->execute($sql);
			
				// Coin 회수
				$sql = "SELECT coin FROM tbl_user WHERE useridx=$useridx";
				$leftcoin = $db_main->getvalue($sql);
			
				if ($coin > 0)
				{
					if ($leftcoin >= $coin)
					{
						$sql = "INSERT INTO tbl_freecoin_admin_log(useridx, freecoin, reason, message, admin_id, writedate) VALUES($useridx, -$coin, 'refund로 인한 코인 차감', '', 'RealTime Update', NOW());";
						$db_main->execute($sql);
			
						$sql = "INSERT INTO tbl_user_cache_update(useridx, coin, writedate) VALUES($useridx, -$coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate=VALUES(writedate);";
						$db_main2->execute($sql);
					}
					else
					{
						$sql = "UPDATE tbl_product_order_mobile SET cancelleftcoin=".($coin-$leftcoin)." WHERE orderidx=$orderidx;".
								"INSERT INTO tbl_freecoin_admin_log(useridx, freecoin, reason, message, admin_id, writedate) VALUES($useridx, -$coin, 'refund로 인한 코인 차감(차감코인부족:".($coin-$leftcoin).")', '', 'RealTime Update', NOW());";
						$db_main->execute($sql);
			
						$sql = "INSERT INTO tbl_user_cache_update(useridx, coin, writedate) VALUES($useridx, -$coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate=VALUES(writedate);";
						$db_main2->execute($sql);
					}
				}
			
				$sql = "SELECT IFNULL(MAX(writedate), '0000-00-00 00:00:00') AS firstbuydate ".
						"FROM	".
						"(	".
						"	SELECT writedate FROM tbl_product_order WHERE useridx  = $useridx AND STATUS = 1	".
						"	UNION ALL	".
						"	SELECT writedate FROM tbl_product_order_mobile WHERE useridx  = $useridx AND STATUS = 1	".
						") t1 LIMIT 1";
				$lastbuydate = $db_main->getvalue($sql);
			
				// 쿠폰 반환 및 초기화
				$sql = "DELETE FROM tbl_coupon WHERE useridx=$useridx AND status=0;";
				$sql .= "UPDATE tbl_coupon_issue SET vip_level = $vip_level, base_credit=0, lastbuydate='$lastbuydate' WHERE useridx=$useridx;";
				$db_main2->execute($sql);
				
				//프리미엄 부스트 초기화
				$sql = "UPDATE tbl_user_premium_booster SET end_date = now() WHERE useridx=$useridx AND premium_type = 2;";
				$db_main2->execute($sql);
				
				if($db_main2->getvalue("SELECT COUNT(*) FROM tbl_user_retention_offer WHERE useridx = $useridx") > 0)
				{
				    if($product_type == 16)
				        $udpate_sql = "UPDATE tbl_user_retention_offer SET type = 0 , duedate =  DATE_SUB(NOW(), INTERVAL 1 DAY), buydate = DATE_SUB(NOW(), INTERVAL 28 DAY), facebookcredit = 0  WHERE useridx = $useridx";
			        else
			            $udpate_sql = "UPDATE tbl_user_retention_offer SET type = 0 , duedate =  DATE_SUB(NOW(), INTERVAL 1 DAY), buydate = DATE_SUB(NOW(), INTERVAL 28 DAY) WHERE useridx = $useridx";
				            
				    $db_main2->execute($udpate_sql);
				}
			
				// 첫 결제 정보 추가
				$sql = "SELECT IFNULL(MIN(writedate), '0000-00-00 00:00:00') AS firstbuydate ".
						"FROM	".
						"(	".
						"	SELECT writedate FROM tbl_product_order WHERE useridx  = $useridx AND STATUS = 1	".
						"	UNION ALL	".
						"	SELECT writedate FROM tbl_product_order_mobile WHERE useridx  = $useridx AND STATUS = 1	".
						") t1 LIMIT 1";
				$firstbuydate = $db_main->getvalue($sql);
			
				if($firstbuydate != "0000-00-00 00:00:00")
				{
					$sql = "SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS createdate FROM tbl_user WHERE useridx=$useridx";
					$join_createdate = $db_main->getvalue($sql);
			
					$sql = "INSERT INTO tbl_user_first_order(useridx, join_date, purchase_date) VALUES($useridx, '$join_createdate', '$firstbuydate') ON DUPLICATE KEY UPDATE purchase_date=VALUES(purchase_date);";
					$db_main2->execute($sql);
				}
				else
				{
					$sql = "DELETE FROM tbl_user_first_order WHERE useridx=$useridx";
					$db_main2->execute($sql);
				}

				$sql=" SELECT SUM(cnt) FROM ".
								"	( ".
								" SELECT COUNT(*) AS cnt FROM tbl_product_order WHERE useridx = $useridx AND facebookcredit >= 50 AND status = 1 ".
								"	    UNION ALL ".
								" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx = $useridx AND facebookcredit >= 50 AND status = 1 ".
								" ) t1";
				
				if($db_main->getvalue($sql) == 0)
				{
				    $sql = "UPDATE tbl_user_first_buy SET type = 0, otherbuy=0, shownow=0, buywritedate='000-00-00 00:00:00', lastshowdate='000-00-00 00:00:00' WHERE useridx=$useridx;";
				    $db_main->execute($sql);
				}
				
				// 첫 결제자 5회 선물 반환 및 지급 제거
				if($product_type != 6)
				{
					
					$sql ="SELECT bonus_coin, ABS(leftcount-5) AS cnt FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='';";
					$user_firstbuy_gift = $db_main2->getarray($sql);
					$return_coin = ($user_firstbuy_gift[0]*$user_firstbuy_gift[1]);
					
					$sql = "DELETE FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='';";
					$db_main2->execute($sql);
				}
				else if ($product_type == 6)
				{
					$sql ="SELECT bonus_coin, ABS(leftcount-5) AS cnt FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='$productkey' AND purchasedate = '$writedate';";
					$user_firstbuy_gift = $db_main2->getarray($sql);
					$return_coin = ($user_firstbuy_gift[0]*$user_firstbuy_gift[1]);
						
					$sql = "DELETE FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='$productkey';";
					$db_main2->execute($sql);
				}
				
				$sql = "INSERT INTO tbl_user_cache_update(useridx, coin, writedate) VALUES($useridx, -$return_coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate=VALUES(writedate);";
				$db_main2->execute($sql);
				
				$sql = "DELETE FROM tbl_user_firstbuy_gift WHERE useridx=$useridx;";
				$db_main2->execute($sql);
				
				// 신규 사용자 7회 선물 지급 제거
				$sql = "UPDATE tbl_user_newuser_gift SET leftcount = 0 WHERE useridx=$useridx;";
				$db_main2->execute($sql);
				
				if($product_type == 14 || $product_type == 17)
				{
				    // Safe2Q 상품 제거
				    $sql = "DELETE FROM tbl_subscribe_user_bonus_info WHERE useridx=$useridx;";
				    $db_main2->execute($sql);
				}

				// New 승률부양 삭제
				$sql = "SELECT boosttype, max_moneyin FROM tbl_user_boost WHERE useridx = $useridx;";
				$user_boost_info = $db_main->getarray($sql);
			
				$user_boost_type = $user_boost_info["boosttype"];
				$user_boost_max_moneyin = $user_boost_info["max_moneyin"];
			
				if(($user_boost_type == 7 && $user_boost_max_moneyin > 0) || ($user_boost_type == 8 && $user_boost_max_moneyin > 0))
				{
					$change_max_moneyin = $user_boost_max_moneyin - $org_coin;
			
					if($change_max_moneyin < 0)
						$change_max_moneyin = 0;
			
					$sql = "UPDATE tbl_user_boost SET max_moneyin = $change_max_moneyin WHERE useridx = $useridx AND boosttype = $user_boost_type";
					$db_main->execute($sql);
				}
				else if($user_boost_type == 9 && $user_boost_max_moneyin > 0)
				{
					$sql = "SELECT MAX(multiple) FROM tbl_boost_metrics;";
					$max_multiple = $db_main->getvalue($sql);
						
					$change_max_moneyin = $user_boost_max_moneyin - ($org_coin * $max_multiple);
						
					if($change_max_moneyin < 0)
						$change_max_moneyin = 0;
			
					$sql = "UPDATE tbl_user_boost SET max_moneyin = $change_max_moneyin WHERE useridx = $useridx AND boosttype = $user_boost_type";
					$db_main->execute($sql);
				}
				else
				{
					$sql = "DELETE FROM tbl_user_boost WHERE useridx = $useridx;";
					$db_main->execute($sql);
				}

				// Bonus Coin 차감(3Day & 7Day)
				if(400 <= $product_targetcredit && $product_targetcredit <= 9990)
				{
					// 3Day Bonus coin 지급 제거
					$sql = "UPDATE tbl_user_buy_3day_gift SET leftcount = 0 WHERE useridx = $useridx  AND platform = 2 AND orderno = '$order_id'";
					$db_main2->execute($sql);
						
					// 3Day Bonus coin 발급 inbox삭제
					$sql = "SELECT inboxidx FROM tbl_user_buy_3day_gift_log WHERE useridx = $useridx AND type = 1 AND orderno = '$order_id'";
					$refund_3day_inbox_list = $db_main2->gettotallist($sql);
					
					$delete_3day_inboxidx_info = "";
						
					for($j = 0; $j < sizeof($refund_3day_inbox_list); $j++)
					{
						$delete_3day_inboxidx = $refund_3day_inbox_list[$j]["inboxidx"];
			
						if($delete_3day_inboxidx_info == "")
							$delete_3day_inboxidx_info = "$delete_3day_inboxidx";
						else
							$delete_3day_inboxidx_info .= ",$delete_3day_inboxidx"; 
					}
						
					if($delete_3day_inboxidx_info != "")
					{
						$sql_delete_log = "DELETE FROM tbl_user_inbox_".($useridx%20)." WHERE useridx = $useridx AND category = 111 AND inboxidx IN ($delete_3day_inboxidx_info);";						
						$db_inbox->execute($sql_delete_log);
					}
				}
			
				// 7Day Bonus coin 지급 제거
				if($product_targetcredit == 600)
				{
					$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY) WHERE useridx = $useridx";
					$db_main2->execute($sql);
				}
				else if($product_targetcredit == 1000)
				{
					$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY) WHERE useridx = $useridx";
					$db_main2->execute($sql);
				}
				else if($product_targetcredit == 2000)
				{
					$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY), inboxdate  = DATE_SUB(inboxdate, INTERVAL 7 DAY) WHERE useridx = $useridx";
					$db_main2->execute($sql);
				}
				else if($product_targetcredit == 3000 || $product_targetcredit == 5000 || $product_targetcredit == 7770 || $product_targetcredit == 9990)
				{
					$sql = "SELECT IF(spindate != '0000-00-00' , 1, 0) AS spin_check, IF(stampdate != '0000-00-00' , 1, 0) AS stamp_check, IF(inboxdate != '0000-00-00', 1, 0) AS inbox_check FROM tbl_user_buy_7day_gift WHERE useridx = $useridx ";
					$date_info = $db_main2->getarray($sql);
			
					$spin_check = $date_info["spin_check"];
					$stamp_check = $date_info["stamp_check"];
					$inbox_check = $date_info["inbox_check"];
			
					if($inbox_check == 1)
					{
						$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY), inboxdate  = DATE_SUB(inboxdate, INTERVAL 7 DAY), ".
								"spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
						$db_main2->execute($sql);
					}
					else if($stamp_check == 1)
					{
						$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY), ".
								"spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
						$db_main2->execute($sql);
					}
					else if($spin_check == 1)
					{
						$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), ".
								"spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
						$db_main2->execute($sql);
					}
					else
					{
						$sql = "UPDATE tbl_user_buy_7day_gift SET spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
						$db_main2->execute($sql);
					}
			
						$sql = "DELETE FROM  tbl_user_buy_vip_wheel WHERE useridx = $useridx AND orderno = '$order_id'";
						$db_main2->execute($sql);
					}

					// tbl_user_online 삭제
					$sql = "DELETE FROM tbl_user_online_".($useridx%10)." WHERE useridx=$useridx;";
					$sql .= "DELETE FROM tbl_user_online_sync2 WHERE useridx=$useridx;";
					$db_friend->execute($sql);
				}
			}
		}
				
	$db_main->end();
	$db_main2->end();
	$db_friend->end();
	$db_livestats->end();
	$db_inbox->end();
?>
