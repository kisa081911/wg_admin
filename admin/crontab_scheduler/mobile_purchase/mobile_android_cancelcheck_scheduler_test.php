<?
    include("../../common/common_include.inc.php");
    require_once '../../common/android_library/ResponseData.php';
 	require_once '../../common/android_library/ResponseValidator.php';

 	$std_start_time = date("Y-m-d H:i:00", strtotime("-3 hours"));
 	$std_end_time = date("Y-m-d H:i:00", strtotime("-3 hours +1 minutes"));
 	$param_orderidx = ($_GET["orderidx"] =="")? 0:$_GET["orderidx"];
 	
 	$result = array();
 	exec("ps -ef | grep wget", $result);
 	
 	$count = 0;
 	
 	for ($i=0; $i<sizeof($result); $i++)
 	{
 		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/mobile_purchase/mobile_android_cancelcheck_scheduler") !== false)
 		{
 			$count++;
 		}
 	}
 	
 	if ($count > 1)
 		exit();
 	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_friend = new CDatabase_Friend();
	$db_livestats = new CDatabase_Livestats();
	$db_inbox = new CDatabase_Inbox();
	
	$db_main->execute("SET wait_timeout=60");
	$db_main2->execute("SET wait_timeout=60");
	$db_friend->execute("SET wait_timeout=60");
	$db_livestats->execute("SET wait_timeout=60");
	$db_inbox->execute("SET wait_timeout=60");
	
	//Your key, copy and paste from https://market.android.com/publish/editProfile
	define('PUBLIC_KEY', 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtku9HCIPaIMUbfoUawSkYiGiOHqZXCUiJPvTN84khXBtvjEK49s2Ex4Zg9TCzOt0J64mcgY4jse60oYZtyeRXz71BFUoVwfCnNNBE4AkprdyC13xGHU/GI/+m9xnW3BEbLmcoGEmmMmvz9sQjIVQLW5SP9z/e1mziiQphIxo/Nh7v99jiCS4nMGvPPFZcsqJto1ornLOXOCeY6IxMrB39Leg3Yr32Liz4XYZHN2v6V0pQttxPFcp1syGX+0Ni/Eyd3ho0wlmaKkod8N3n/JWDxuaAtuzWIPf0Aw3fiNtXvPw/eo5hjwG10EDcxmdBEyU/ajfCXg4UY9LRI/6KQzmdQIDAQAB');
	
	//Your app's package name, e.g. com.example.yourapp
	define('PACKAGE_NAME', '');
	
// 	$sql = "SELECT * FROM tbl_product_order_mobile WHERE os_type=2 AND status = 1 AND '$std_start_time' <= writedate AND writedate < '$std_end_time' ORDER BY writedate ASC";

	$sql = "SELECT * FROM tbl_product_order_mobile WHERE os_type=2 AND status = 1  AND orderidx = $param_orderidx";
	$receipt_list = $db_main->gettotallist($sql);
	
	for ($i=0; $i<sizeof($receipt_list); $i++)
	{
		$useridx = $receipt_list[$i]["useridx"];
		$productidx = $receipt_list[$i]["productidx"];
		$receipt = $receipt_list[$i]["receipt"];
		$errcount = $receipt_list[$i]["errcount"];
		$orderno = $receipt_list[$i]["orderno"];
		$orderidx = $receipt_list[$i]["orderidx"];
		$product_type = $receipt_list[$i]["product_type"];
		$money = $receipt_list[$i]["money"];
		$mobile_facebookcredit = $receipt_list[$i]["facebookcredit"];
		$usercoin = $receipt_list[$i]["usercoin"];
		$coin = $receipt_list[$i]["coin"];
		$basecoin = $receipt_list[$i]["basecoin"];
		$gift_coin = $receipt_list[$i]["gift_coin"];
		$special_more = $receipt_list[$i]["special_more"];
		$signature = $receipt_list[$i]["signature"];
		
		// verify the receipt
		try
		{
			//The | delimited receipt data from the licensing server
			//The signature provided with the response data (Base64)
			//if you wish to inspect or use the response data, you can create
			//a response object and pass that in to the Validator's constructor
			//$response = new AndroidMarket_Licensing_ResponseData($responseData);
			$validator = new AndroidMarket_Licensing_ResponseValidator(PUBLIC_KEY, PACKAGE_NAME);
		
			$valid = $validator->verify($receipt, $signature);
		
// 			if($valid)
            if($param_orderidx == $orderno)
// 			if($info['transaction_id'] == $orderno))
			{
			}
			else
			{
				// 구매 후 환불 및 취소처리
				//unable to verify receipt, or receipt is not valid
				$sql = "UPDATE tbl_product_order_mobile SET status = 2, canceldate=NOW() WHERE orderidx=$orderidx";
				$db_main->execute($sql);
				
				$sql = "SELECT orderidx, useridx, orderno, b.money, a.facebookcredit, coin FROM ( SELECT * FROM tbl_product_order_mobile WHERE orderidx='$orderidx' ) a LEFT JOIN tbl_product_mobile b ON a.productidx = b.productidx";
				$data = $db_main->getarray($sql);
				$orderidx = $data["orderidx"];
				$money = $data["money"];
				$mobile_facebookcredit = $data["facebookcredit"];
				$coin = $data["coin"];
				$useridx = $data["useridx"];
				$org_coin = $data["coin"];
				$order_id = $data["orderno"];
				
				//개평 제거
				write_log($mobile_facebookcredit);
				if( $mobile_facebookcredit >=500 )
				{
				    $sql ="INSERT INTO `tbl_bankrupt_log` (`useridx`,  `receive_cnt`) VALUES('$useridx', 0) ON DUPLICATE KEY UPDATE receive_cnt=0 ";
				    write_log($sql);
				    $db_main2->execute($sql);
				}
				
				
				// 취소 전 VIP Level 정보
				$sql = "SELECT vip_level FROM tbl_user_detail WHERE useridx=$useridx";
				$before_viplevel = $db_main->getvalue($sql);
				
				// VIP Point Update
				$sql = "SELECT SUM(vip_point) AS vip_point ".
						"FROM	".
						"(	".
						"	SELECT IFNULL(SUM(vip_point), 0) AS vip_point FROM tbl_product_order WHERE useridx = $useridx AND STATUS = 1	".
						"	UNION ALL	".
						"	SELECT IFNULL(SUM(vip_point), 0) AS vip_point FROM tbl_product_order_mobile WHERE useridx = $useridx AND STATUS = 1	".
						") t1";
				$vip_point = $db_main->getvalue($sql);
				
				$vip_level = get_vip_level($vip_point);
				
				$sql = "UPDATE tbl_user_detail SET vip_level=$vip_level, vip_point=$vip_point WHERE useridx = $useridx;";
				$db_main->execute($sql);
				
				$sql = "UPDATE tbl_user_detail SET vip_level=$vip_level, vip_point=$vip_point WHERE useridx = $useridx;";
				$db_friend->execute($sql);
					
				// Coin 회수
				$sql = "SELECT coin FROM tbl_user WHERE useridx=$useridx";
				$leftcoin = $db_main->getvalue($sql);
				
				if ($coin > 0)
				{
					if ($leftcoin >= $coin)
					{
						$sql = "INSERT INTO tbl_freecoin_admin_log(useridx, freecoin, reason, message, admin_id, writedate) VALUES($useridx, -$coin, 'refund로 인한 코인 차감', '', 'RealTime Update', NOW());";
						$db_main->execute($sql);
				
						$sql = "INSERT INTO tbl_user_cache_update(useridx, coin, writedate) VALUES($useridx, -$coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate=VALUES(writedate);";
						$db_main2->execute($sql);
					}
					else
					{
						$sql = "UPDATE tbl_product_order_mobile SET cancelleftcoin=".($coin-$leftcoin)." WHERE orderidx=$orderidx;".
								"INSERT INTO tbl_freecoin_admin_log(useridx, freecoin, reason, message, admin_id, writedate) VALUES($useridx, -$coin, 'refund로 인한 코인 차감(차감코인부족:".($coin-$leftcoin).")', '', 'RealTime Update', NOW());";
						$db_main->execute($sql);
				
						$sql = "INSERT INTO tbl_user_cache_update(useridx, coin, writedate) VALUES($useridx, -$coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate=VALUES(writedate);";
						$db_main2->execute($sql);
					}
				}
				
				$sql = "UPDATE tbl_product_order_mobile SET status=2, canceldate=NOW() WHERE orderidx = $orderidx;".
						"INSERT INTO tbl_product_order_mobile_cancel(orderidx, useridx, os_type, productidx, orderno, usercoin, coin, receipt, signature, money, status, gift_status, gift_coin, basecoin, special_more, canceldate, cancelleftcoin, writedate) ".
						"SELECT orderidx, useridx, 2 AS os_type, productidx, orderno, usercoin, coin, receipt, signature, money, 2, gift_status, gift_coin, basecoin, special_more, NOW(), cancelleftcoin, writedate ".
						"FROM tbl_product_order_mobile ".
						"WHERE orderidx = $orderidx;";				
				$db_main->execute($sql);
					
				$sql = "SELECT IFNULL(MAX(writedate), '0000-00-00 00:00:00') AS firstbuydate ".
						"FROM	".
						"(	".
						"	SELECT writedate FROM tbl_product_order WHERE useridx  = $useridx AND STATUS = 1	".
						"	UNION ALL	".
						"	SELECT writedate FROM tbl_product_order_mobile WHERE useridx  = $useridx AND STATUS = 1	".
						") t1 LIMIT 1";
				$lastbuydate = $db_main->getvalue($sql);
				
				// 쿠폰 반환 및 초기화
				$sql = "DELETE FROM tbl_coupon WHERE useridx=$useridx AND status=0;";
				$sql .= "UPDATE tbl_coupon_issue SET vip_level = $vip_level, base_credit=0, lastbuydate='$lastbuydate' WHERE useridx=$useridx;";
				$db_main2->execute($sql);
					
				//프리미엄 부스트 초기화
				$sql = "UPDATE tbl_user_premium_booster SET end_date = now() WHERE useridx=$useridx AND premium_type = 2;";
				$db_main2->execute($sql);
				
				// 첫 결제 정보 추가
				$sql = "SELECT IFNULL(MIN(writedate), '0000-00-00 00:00:00') AS firstbuydate ".
						"FROM	".
						"(	".
						"	SELECT writedate FROM tbl_product_order WHERE useridx  = $useridx AND STATUS = 1	".
						"	UNION ALL	".
						"	SELECT writedate FROM tbl_product_order_mobile WHERE useridx  = $useridx AND STATUS = 1	".
						") t1 LIMIT 1";
				$firstbuydate = $db_main->getvalue($sql);
				
				if($firstbuydate != "0000-00-00 00:00:00")
				{
					$sql = "SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS createdate FROM tbl_user WHERE useridx=$useridx";
					$join_createdate = $db_main->getvalue($sql);
				
					$sql = "INSERT INTO tbl_user_first_order(useridx, join_date, purchase_date) VALUES($useridx, '$join_createdate', '$firstbuydate') ON DUPLICATE KEY UPDATE purchase_date=VALUES(purchase_date);";
					$db_main2->execute($sql);
				}
				else
				{
					$sql = "DELETE FROM tbl_user_first_order WHERE useridx=$useridx";
					$db_main2->execute($sql);
					
				}

				$sql=" SELECT SUM(cnt) FROM ".
								"	( ".
								" SELECT COUNT(*) AS cnt FROM tbl_product_order WHERE useridx = $useridx AND facebookcredit >= 50 AND status = 1 ".
								"	    UNION ALL ".
								" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx = $useridx AND facebookcredit >= 50 AND status = 1 ".
								" ) t1";
				
				if($db_main->getvalue($sql) == 0)
				{
				    $sql = "UPDATE tbl_user_first_buy SET type = 0, otherbuy=0, shownow=0, buywritedate='000-00-00 00:00:00', lastshowdate='000-00-00 00:00:00' WHERE useridx=$useridx;";
				    $db_main->execute($sql);
				}
				
				// 첫 결제자 5회 선물 반환 및 지급 제거
				if($product_type != 6)
				{
					
					$sql ="SELECT bonus_coin, ABS(leftcount-5) AS cnt FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='';";
					$user_firstbuy_gift = $db_main2->getarray($sql);
					$return_coin = ($user_firstbuy_gift[0]*$user_firstbuy_gift[1]);
					
					$sql = "DELETE FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='';";
					$db_main2->execute($sql);
				}
				else if ($product_type == 6)
				{
					$sql ="SELECT bonus_coin, ABS(leftcount-5) AS cnt FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='$productkey' AND purchasedate = '$writedate';";
					$user_firstbuy_gift = $db_main2->getarray($sql);
					$return_coin = ($user_firstbuy_gift[0]*$user_firstbuy_gift[1]);
						
					$sql = "DELETE FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='$productkey';";
					$db_main2->execute($sql);
				}

				if($return_coin != 0 && $return_coin != "")
				{
					$sql = "INSERT INTO tbl_user_cache_update(useridx, coin, writedate) VALUES($useridx, -$return_coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate=VALUES(writedate);";
					$db_main2->execute($sql);
				}
				
				$sql = "DELETE FROM tbl_user_firstbuy_gift WHERE useridx=$useridx;";
				$db_main2->execute($sql);
				
				// 신규 사용자 7회 선물 지급 제거
				$sql = "UPDATE tbl_user_newuser_gift SET leftcount = 0 WHERE useridx=$useridx;";
				$db_main2->execute($sql);
				
				if($product_type == 14 || $product_type == 17)
				{
				    // Safe2Q 상품 제거
				    $sql = "DELETE FROM tbl_subscribe_user_bonus_info WHERE useridx=$useridx;";
				    $db_main2->execute($sql);
				}

				// New 승률부양 삭제
				$sql = "SELECT boosttype, max_moneyin FROM tbl_user_boost WHERE useridx = $useridx;";
				$user_boost_info = $db_main->getarray($sql);
				
				$user_boost_type = $user_boost_info["boosttype"];
				$user_boost_max_moneyin = $user_boost_info["max_moneyin"];
				
				if(($user_boost_type == 7 && $user_boost_max_moneyin > 0) || ($user_boost_type == 8 && $user_boost_max_moneyin > 0))
				{
					$change_max_moneyin = $user_boost_max_moneyin - $org_coin;
				
					if($change_max_moneyin < 0)
						$change_max_moneyin = 0;
				
					$sql = "UPDATE tbl_user_boost SET max_moneyin = $change_max_moneyin WHERE useridx = $useridx AND boosttype = $user_boost_type";
					$db_main->execute($sql);
				}
				else if($user_boost_type == 9 && $user_boost_max_moneyin > 0)
				{
					$sql = "SELECT MAX(multiple) FROM tbl_boost_metrics;";
					$max_multiple = $db_main->getvalue($sql);
				
					$change_max_moneyin = $user_boost_max_moneyin - ($org_coin * $max_multiple);
				
					if($change_max_moneyin < 0)
						$change_max_moneyin = 0;
				
					$sql = "UPDATE tbl_user_boost SET max_moneyin = $change_max_moneyin WHERE useridx = $useridx AND boosttype = $user_boost_type";
					$db_main->execute($sql);
				}
				else
				{
					$sql = "DELETE FROM tbl_user_boost WHERE useridx = $useridx;";
					$db_main->execute($sql);
				}
				
				// Bonus Coin 차감(3Day & 7Day)
				if(390 <= $mobile_facebookcredit && $mobile_facebookcredit <= 9990)
				{
					// 3Day Bonus coin 지급 제거
					$sql = "UPDATE tbl_user_buy_3day_gift SET leftcount = 0 WHERE useridx = $useridx  AND platform = 2 AND orderno = '$order_id'";
					$db_main2->execute($sql);
						
					// 3Day Bonus coin 발급 inbox삭제
					$sql = "SELECT inboxidx FROM tbl_user_buy_3day_gift_log WHERE useridx = $useridx AND type = 1 AND orderno = '$order_id'";
					$refund_3day_inbox_list = $db_main2->gettotallist($sql);
					
					$delete_3day_inboxidx_info = "";
						
					for($j = 0; $j < sizeof($refund_3day_inbox_list); $j++)
					{
						$delete_3day_inboxidx = $refund_3day_inbox_list[$i]["inboxidx"];
			
						if($delete_3day_inboxidx_info == "")
							$delete_3day_inboxidx_info = "$delete_3day_inboxidx";
						else
							$delete_3day_inboxidx_info .= ",$delete_3day_inboxidx"; 
					}
						
					if($delete_3day_inboxidx_info != "")
					{
						$sql_delete_log = "DELETE FROM tbl_user_inbox_".($useridx%20)." WHERE useridx = $useridx AND category = 111 AND inboxidx IN ($delete_3day_inboxidx_info);";						
						$db_inbox->execute($sql_delete_log);
					}
				}
				
				// 7Day Bonus coin 지급 제거
				if($mobile_facebookcredit == 590)
				{
					$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY) WHERE useridx = $useridx";
					$db_main2->execute($sql);
				}
				else if($mobile_facebookcredit == 990)
				{
					$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY) WHERE useridx = $useridx";
					$db_main2->execute($sql);
				}
				else if($mobile_facebookcredit == 1990)
				{
					$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY), inboxdate  = DATE_SUB(inboxdate, INTERVAL 7 DAY) WHERE useridx = $useridx";
					$db_main2->execute($sql);
				}
				else if($mobile_facebookcredit == 2990 || $mobile_facebookcredit == 4990 || $mobile_facebookcredit == 7770 || $mobile_facebookcredit == 9990)
				{
					$sql = "SELECT IF(spindate != '0000-00-00' , 1, 0) AS spin_check, IF(stampdate != '0000-00-00' , 1, 0) AS stamp_check, IF(inboxdate != '0000-00-00', 1, 0) AS inbox_check FROM tbl_user_buy_7day_gift WHERE useridx = $useridx ";
					$date_info = $db_main2->getarray($sql);
						
					$spin_check = $date_info["spin_check"];
					$stamp_check = $date_info["stamp_check"];
					$inbox_check = $date_info["inbox_check"];
							
					if($inbox_check == 1)
					{
						$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY), inboxdate  = DATE_SUB(inboxdate, INTERVAL 7 DAY), ".
								"spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
						$db_main2->execute($sql);
					}
					else if($stamp_check == 1)
					{
						$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY), ".
								"spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
						$db_main2->execute($sql);
					}
					else if($spin_check == 1)
					{
						$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), ".
								"spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
						$db_main2->execute($sql);
					}
					else
					{
						$sql = "UPDATE tbl_user_buy_7day_gift SET spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
						$db_main2->execute($sql);
					}
							
					$sql = "DELETE FROM  tbl_user_buy_vip_wheel WHERE useridx = $useridx AND orderno = '$order_id'";
					$db_main2->execute($sql);
				}
			
				// tbl_user_online 삭제
				$sql = "DELETE FROM tbl_user_online_".($useridx%10)." WHERE useridx=$useridx;";
				$sql .= "DELETE FROM tbl_user_online_sync2 WHERE useridx=$useridx;";
				$db_friend->execute($sql);
			}
		}
		catch (Exception $ex)
		{
			// 구매 후 환불 및 취소처리
			//unable to verify receipt, or receipt is not valid
			$sql = "UPDATE tbl_product_order_mobile SET status = 3, canceldate=NOW() WHERE orderidx=$orderidx";
			$db_main->execute($sql);
				
			$sql = "SELECT orderidx, useridx, orderno, b.money, a.facebookcredit, coin FROM ( SELECT * FROM tbl_product_order_mobile WHERE orderidx='$orderidx' ) a LEFT JOIN tbl_product_mobile b ON a.productidx = b.productidx";
			$data = $db_main->getarray($sql);
				
			$orderidx = $data["orderidx"];
			$money = $data["money"];
			$mobile_facebookcredit = $data["facebookcredit"];
			$coin = $data["coin"];
			$useridx = $data["useridx"];
			$org_coin = $data["coin"];
			$order_id = $data["orderno"];
				
			// 취소 전 VIP Level 정보
			$sql = "SELECT vip_level FROM tbl_user_detail WHERE useridx=$useridx";
			$before_viplevel = $db_main->getvalue($sql);
				
			// VIP Point Update
			$sql = "SELECT SUM(vip_point) AS vip_point ".
					"FROM	".
					"(	".
					"	SELECT IFNULL(SUM(vip_point), 0) AS vip_point FROM tbl_product_order WHERE useridx = $useridx AND STATUS = 1	".
					"	UNION ALL	".
					"	SELECT IFNULL(SUM(vip_point), 0) AS vip_point FROM tbl_product_order_mobile WHERE useridx = $useridx AND STATUS = 1	".
					") t1";
			$vip_point = $db_main->getvalue($sql);
				
			$vip_level = get_vip_level($vip_point);
				
			$sql = "UPDATE tbl_user_detail SET vip_level=$vip_level, vip_point=$vip_point WHERE useridx = $useridx;";
			$db_main->execute($sql);
				
			$sql = "UPDATE tbl_user_detail SET vip_level=$vip_level, vip_point=$vip_point WHERE useridx = $useridx;";
			$db_friend->execute($sql);
			
			// Coin 회수
			$sql = "SELECT coin FROM tbl_user WHERE useridx=$useridx";
			$leftcoin = $db_main->getvalue($sql);
				
			if ($coin > 0)
			{
				if ($leftcoin >= $coin)
				{
					$sql = "INSERT INTO tbl_freecoin_admin_log(useridx, freecoin, reason, message, admin_id, writedate) VALUES($useridx, -$coin, 'refund로 인한 코인 차감', '', 'RealTime Update', NOW());";
					$db_main->execute($sql);
						
					$sql = "INSERT INTO tbl_user_cache_update(useridx, coin, writedate) VALUES($useridx, -$coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate=VALUES(writedate);";
					$db_main2->execute($sql);
				}
				else
				{
					$sql = "UPDATE tbl_product_order_mobile SET cancelleftcoin=".($coin-$leftcoin)." WHERE orderidx=$orderidx;".
							"INSERT INTO tbl_freecoin_admin_log(useridx, freecoin, reason, message, admin_id, writedate) VALUES($useridx, -$coin, 'refund로 인한 코인 차감(차감코인부족:".($coin-$leftcoin).")', '', 'RealTime Update', NOW());";
					$db_main->execute($sql);
						
					$sql = "INSERT INTO tbl_user_cache_update(useridx, coin, writedate) VALUES($useridx, -$coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate=VALUES(writedate);";
					$db_main2->execute($sql);
				}
			}
			
			$sql = "UPDATE tbl_product_order_mobile SET status=2, canceldate=NOW() WHERE orderidx = $orderidx;".
					"INSERT INTO tbl_product_order_mobile_cancel(orderidx, useridx, os_type, productidx, orderno, usercoin, coin, receipt, signature, money, status, gift_status, gift_coin, basecoin, special_more, canceldate, cancelleftcoin, writedate) ".
					"SELECT orderidx, useridx, 2 AS os_type, productidx, orderno, usercoin, coin, receipt, signature, money, 2, gift_status, gift_coin, basecoin, special_more, NOW(), cancelleftcoin, writedate ".
					"FROM tbl_product_order_mobile ".
					"WHERE orderidx = $orderidx;";
			$db_main->execute($sql);
			
			$sql = "SELECT IFNULL(MAX(writedate), '0000-00-00 00:00:00') AS firstbuydate ".
					"FROM	".
					"(	".
					"	SELECT writedate FROM tbl_product_order WHERE useridx  = $useridx AND STATUS = 1	".
					"	UNION ALL	".
					"	SELECT writedate FROM tbl_product_order_mobile WHERE useridx  = $useridx AND STATUS = 1	".
					") t1 LIMIT 1";
			$lastbuydate = $db_main->getvalue($sql);
				
			// 쿠폰 반환 및 초기화
			$sql = "DELETE FROM tbl_coupon WHERE useridx=$useridx AND status=0;";
			$sql .= "UPDATE tbl_coupon_issue SET vip_level = $vip_level, base_credit=0, lastbuydate='$lastbuydate' WHERE useridx=$useridx;";
			$db_main2->execute($sql);
			
			//프리미엄 부스트 초기화
			$sql = "UPDATE tbl_user_premium_booster SET end_date = now() WHERE useridx=$useridx AND premium_type = 2;";
			$db_main2->execute($sql);
			
			// 첫 결제 정보 추가
			$sql = "SELECT IFNULL(MIN(writedate), '0000-00-00 00:00:00') AS firstbuydate ".
					"FROM	".
					"(	".
					"	SELECT writedate FROM tbl_product_order WHERE useridx  = $useridx AND STATUS = 1	".
					"	UNION ALL	".
					"	SELECT writedate FROM tbl_product_order_mobile WHERE useridx  = $useridx AND STATUS = 1	".
					") t1 LIMIT 1";
			$firstbuydate = $db_main->getvalue($sql);
				
			if($firstbuydate != "0000-00-00 00:00:00")
			{
				$sql = "SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS createdate FROM tbl_user WHERE useridx=$useridx";
				$join_createdate = $db_main->getvalue($sql);
			
				$sql = "INSERT INTO tbl_user_first_order(useridx, join_date, purchase_date) VALUES($useridx, '$join_createdate', '$firstbuydate') ON DUPLICATE KEY UPDATE purchase_date=VALUES(purchase_date);";
				$db_main2->execute($sql);
			}
			else
			{
				$sql = "DELETE FROM tbl_user_first_order WHERE useridx=$useridx";
				$db_main2->execute($sql);
			}
				
			// 첫 결제자 5회 선물 반환 및 지급 제거
			if($product_type != 6)
			{
				
				$sql ="SELECT bonus_coin, ABS(leftcount-5) AS cnt FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='';";
				$user_firstbuy_gift = $db_main2->getarray($sql);
				$return_coin = ($user_firstbuy_gift[0]*$user_firstbuy_gift[1]);
				
				$sql = "DELETE FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='';";
				$db_main2->execute($sql);
			}
			else if ($product_type == 6)
			{
				$sql ="SELECT bonus_coin, ABS(leftcount-5) AS cnt FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='$productkey' AND purchasedate = '$writedate';";
				$user_firstbuy_gift = $db_main2->getarray($sql);
				$return_coin = ($user_firstbuy_gift[0]*$user_firstbuy_gift[1]);
					
				$sql = "DELETE FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='$productkey';";
				$db_main2->execute($sql);
			}
			
			if($return_coin != 0 && $return_coin != "")
			{
				$sql = "INSERT INTO tbl_user_cache_update(useridx, coin, writedate) VALUES($useridx, -$return_coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate=VALUES(writedate);";
				$db_main2->execute($sql);
			}
			
			$sql = "DELETE FROM tbl_user_firstbuy_gift WHERE useridx=$useridx;";
			$db_main2->execute($sql);
			
			$sql=" SELECT SUM(cnt) FROM ".
			 			"	( ".
			 			" SELECT COUNT(*) AS cnt FROM tbl_product_order WHERE useridx = $useridx AND facebookcredit >= 50 AND status = 1 ".
			 			"	    UNION ALL ".
			 			" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx = $useridx AND facebookcredit >= 50 AND status = 1 ".
			 			" ) t1";
			
			if($db_main->getvalue($sql) == 0)
			{
			    $sql = "UPDATE tbl_user_first_buy SET type = 0, otherbuy=0, shownow=0, buywritedate='000-00-00 00:00:00', lastshowdate='000-00-00 00:00:00' WHERE useridx=$useridx;";
			    $db_main->execute($sql);
			}
			
			// 신규 사용자 7회 선물 지급 제거
			$sql = "UPDATE tbl_user_newuser_gift SET leftcount = 0 WHERE useridx=$useridx;";
			$db_main2->execute($sql);

			if($product_type == 14 || $product_type == 17)
			{
			    // Safe2Q 상품 제거
			    $sql = "DELETE FROM tbl_subscribe_user_bonus_info WHERE useridx=$useridx;";
			    $db_main2->execute($sql);
			}
			
			// New 승률부양 삭제

			$sql = "SELECT boosttype, max_moneyin FROM tbl_user_boost WHERE useridx = $useridx;";
			$user_boost_info = $db_main->getarray($sql);
			
			$user_boost_type = $user_boost_info["boosttype"];
			$user_boost_max_moneyin = $user_boost_info["max_moneyin"];
				
			if(($user_boost_type == 7 && $user_boost_max_moneyin > 0) || ($user_boost_type == 8 && $user_boost_max_moneyin > 0))
			{
				$change_max_moneyin = $user_boost_max_moneyin - $org_coin;
			
				if($change_max_moneyin < 0)
					$change_max_moneyin = 0;
			
				$sql = "UPDATE tbl_user_boost SET max_moneyin = $change_max_moneyin WHERE useridx = $useridx AND boosttype = $user_boost_type";
				$db_main->execute($sql);
			}
			else if($user_boost_type == 9 && $user_boost_max_moneyin > 0)
			{
				$sql = "SELECT MAX(multiple) FROM tbl_boost_metrics;";
				$max_multiple = $db_main->getvalue($sql);
					
				$change_max_moneyin = $user_boost_max_moneyin - ($org_coin * $max_multiple);
					
				if($change_max_moneyin < 0)
					$change_max_moneyin = 0;
			
				$sql = "UPDATE tbl_user_boost SET max_moneyin = $change_max_moneyin WHERE useridx = $useridx AND boosttype = $user_boost_type";
				$db_main->execute($sql);
			}
			else
			{
				$sql = "DELETE FROM tbl_user_boost WHERE useridx = $useridx;";
				$db_main->execute($sql);
			}

			// Bonus Coin 차감(3Day & 7Day)
			if(390 <= $mobile_facebookcredit && $mobile_facebookcredit <= 9990)
			{
				// 3Day Bonus coin 지급 제거
				$sql = "UPDATE tbl_user_buy_3day_gift SET leftcount = 0 WHERE useridx = $useridx  AND platform = 2 AND orderno = '$order_id'";
				$db_main2->execute($sql);
					
				// 3Day Bonus coin 발급 inbox삭제
				$sql = "SELECT inboxidx FROM tbl_user_buy_3day_gift_log WHERE useridx = $useridx AND type = 1 AND orderno = '$order_id'";
				$refund_3day_inbox_list = $db_main2->gettotallist($sql);
				
				$delete_3day_inboxidx_info = "";
					
				for($j = 0; $j < sizeof($refund_3day_inbox_list); $j++)
				{
					$delete_3day_inboxidx = $refund_3day_inbox_list[$j]["inboxidx"];
		
					if($delete_3day_inboxidx_info == "")
						$delete_3day_inboxidx_info = "$delete_3day_inboxidx";
					else
						$delete_3day_inboxidx_info .= ",$delete_3day_inboxidx"; 
				}
					
				if($delete_3day_inboxidx_info != "")
				{
					$sql_delete_log = "DELETE FROM tbl_user_inbox_".($useridx%20)." WHERE useridx = $useridx AND category = 111 AND inboxidx IN ($delete_3day_inboxidx_info);";						
					$db_inbox->execute($sql_delete_log);
				}
			}
			
			// 7Day Bonus coin 지급 제거
			if($mobile_facebookcredit == 590)
			{
				$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY) WHERE useridx = $useridx";
				$db_main2->execute($sql);
			}
			else if($mobile_facebookcredit == 990)
			{
				$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY) WHERE useridx = $useridx";
				$db_main2->execute($sql);
			}
			else if($mobile_facebookcredit == 1990)
			{
				$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY), inboxdate  = DATE_SUB(inboxdate, INTERVAL 7 DAY) WHERE useridx = $useridx";
						$db_main2->execute($sql);
			}
			else if($mobile_facebookcredit == 2990 || $mobile_facebookcredit == 4990 || $mobile_facebookcredit == 7770 || $mobile_facebookcredit == 9990)
			{
				$sql = "SELECT IF(spindate != '0000-00-00' , 1, 0) AS spin_check, IF(stampdate != '0000-00-00' , 1, 0) AS stamp_check, IF(inboxdate != '0000-00-00', 1, 0) AS inbox_check FROM tbl_user_buy_7day_gift WHERE useridx = $useridx ";
				$date_info = $db_main2->getarray($sql);
			
				$spin_check = $date_info["spin_check"];
				$stamp_check = $date_info["stamp_check"];
				$inbox_check = $date_info["inbox_check"];
			
				if($inbox_check == 1)
				{
						$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY), inboxdate  = DATE_SUB(inboxdate, INTERVAL 7 DAY), ".
								"spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
						$db_main2->execute($sql);
				}
				else if($stamp_check == 1)
				{
					$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY), ".
							"spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
					$db_main2->execute($sql);
				}
				else if($spin_check == 1)
				{
					$sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), ".
						"spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
					$db_main2->execute($sql);
				}
				else
				{
					$sql = "UPDATE tbl_user_buy_7day_gift SET spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
					$db_main2->execute($sql);
				}
				
				$sql = "DELETE FROM  tbl_user_buy_vip_wheel WHERE useridx = $useridx AND orderno = '$order_id'";
				$db_main2->execute($sql);
			}
					
			// tbl_user_online 삭제
			$sql = "DELETE FROM tbl_user_online_".($useridx%10)." WHERE useridx=$useridx;";
			$sql .= "DELETE FROM tbl_user_online_sync2 WHERE useridx=$useridx;";
			$db_friend->execute($sql);
		}		
	}
	
	$db_main->end();
	$db_main2->end();
	$db_friend->end();
	$db_livestats->end();
	$db_inbox->end();
	
?>