<?
    include("../../common/common_include.inc.php");
    require_once '../../common/android_library/ResponseData.php';
 	require_once '../../common/android_library/ResponseValidator.php';

 	$std_start_time = date("Y-m-d H:i:00", strtotime("-3 hours"));
 	$std_end_time = date("Y-m-d H:i:00", strtotime("-3 hours +1 minutes"));
 	$order_id = ($_GET["order_id"] =="")? 0:$_GET["order_id"];
 	
 	$result = array();
 	exec("ps -ef | grep wget", $result);
 	
 	$count = 0;
 	
 	for ($i=0; $i<sizeof($result); $i++)
 	{
 		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/mobile_purchase/mobile_android_cancelcheck_scheduler") !== false)
 		{
 			$count++;
 		}
 	}
 	
 	if ($count > 1)
 		exit();
 	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_friend = new CDatabase_Friend();
	$db_livestats = new CDatabase_Livestats();
	$db_inbox = new CDatabase_Inbox();
	
	$db_main->execute("SET wait_timeout=60");
	$db_main2->execute("SET wait_timeout=60");
	$db_friend->execute("SET wait_timeout=60");
	$db_livestats->execute("SET wait_timeout=60");
	$db_inbox->execute("SET wait_timeout=60");
    
	
	$sql = "SELECT t1.productidx, category, orderidx, coin, useridx, t1.facebookcredit, DATEDIFF(NOW(), t1.writedate) AS diff_canceldate, IF(category = 9, FLOOR(t1.facebookcredit / ((100 - special_discount) / 100)), t1.facebookcredit) AS targetcredit ".
	   	" ,(SELECT COUNT(*) FROM tbl_product_orderV20_log WHERE orderno ='$order_id') AS is_hybrid ".
	   	" FROM ( ".
	   	"	SELECT productidx, orderidx,coin,gift_coin,useridx,facebookcredit,writedate ".
	   	"	FROM tbl_product_order ".
	   	"	WHERE orderno='$order_id' AND status=1 ".
	   	" ) t1 LEFT JOIN tbl_product t2 ON t1.productidx = t2.productidx";
	
	$data = $db_main->getarray($sql);
	
	$category = $data["category"];
	$productidx = $data["productidx"];
	$orderidx = $data["orderidx"];
	$facebookcredit = $data["facebookcredit"];
	$coin = $data["coin"];
	$useridx = $data["useridx"];
	$org_coin = $data["coin"];
	$diff_canceldate = $data["diff_canceldate"];
	$product_targetcredit  = $data["targetcredit"];
	$is_hybrid  = $data["is_hybrid"];
	
	if ($orderidx != "")
	{
	    // 환불 정보 기록
	    $sql = "SELECT disputeidx FROM tbl_product_order_dispute WHERE orderno='$order_id'";
	    $disputeidx = $db_main->getvalue($sql);
	    
	    if($disputeidx != "")
	    {
	        $sql = "UPDATE tbl_product_order_dispute SET status = CONCAT(status, '_$type'), iscomplete='1', writedate=now() WHERE orderno='$order_id'";
	    }
	    else
	    {
	        $sql = "INSERT INTO tbl_product_order_dispute(useridx, orderno, currency, amount, coin, user_comment, user_email, status, iscomplete, writedate) ".
	   	        "VALUES('$useridx', '$order_id', '$chargeback_currency', '$chargeback_amount', $coin, 'facebook refund', '', '$type', 1, now());";
	    }
	    
	    $db_main->execute($sql);
	    
	    if($diff_canceldate > 90)
	    {
	        // 결제 후  90일 이후 취소 처리
	        $sql = "UPDATE tbl_product_order SET status=3, canceldate=NOW() WHERE orderidx=$orderidx;";
	        $db_main->execute($sql);
	    }
	    else
	    {
	        // 취소 처리
	        $sql = "UPDATE tbl_product_order SET status=2, canceldate=NOW() WHERE orderidx=$orderidx;";
	        $db_main->execute($sql);
	    }
	    
	    
	    // 개평 제거
	    if( $product_targetcredit >=500 )
	    {
	        $sql ="INSERT INTO `tbl_bankrupt_log` (`useridx`,  `receive_cnt`) VALUES('$useridx', 0) ON DUPLICATE KEY UPDATE receive_cnt=0 ";
	        $db_main2->execute($sql);
	    }
	    
	    // 취소 전 VIP Level 정보
	    $sql = "SELECT vip_level FROM tbl_user_detail WHERE useridx=$useridx";
	    $before_viplevel = $db_main->getvalue($sql);
	    
	    // VIP Point Update
	    $sql = "SELECT ".
	   	    "(SELECT IFNULL(SUM(vip_point), 0) AS vip_point FROM tbl_product_order WHERE useridx = $useridx AND status = 1) + ".
	   	    "	(SELECT IFNULL(SUM(vip_point), 0) AS vip_point FROM tbl_product_order_mobile WHERE useridx = $useridx AND status = 1) AS total_vip_point";
	    $vip_point = $db_main->getvalue($sql);
	    
	    $vip_level = get_vip_level($vip_point);
	    
	    $sql = "UPDATE tbl_user_detail SET vip_level=$vip_level, vip_point=$vip_point WHERE useridx = $useridx;";
	    $db_main->execute($sql);
	    
	    $sql = "UPDATE tbl_user_detail SET vip_level=$vip_level, vip_point=$vip_point WHERE useridx = $useridx;";
	    $db_friend->execute($sql);
	    
	    $sql = "SELECT ".
	   	    "(SELECT IFNULL(SUM(facebookcredit), 0) AS totalcredit FROM tbl_product_order WHERE useridx = $useridx AND status = 1) + ".
	   	    "(SELECT IFNULL(SUM(facebookcredit), 0) AS totalcredit FROM tbl_product_order_mobile WHERE useridx = $useridx AND status = 1);";
	    $totalcredit = $db_main->getvalue($sql);
	    
	    try
	    {
	        // user_order Table Update
	        $params = array('cmd'=>'update_user_order', 'useridx'=>$useridx, 'totalcredit'=>$totalcredit);
	        log_node_async("log", $params);
	    }
	    catch (Exception $e)
	    {
	        write_log($e->getMessage());
	    }
	    
	    // VIP Level UP 보상 회수
	    if($before_viplevel > $vip_level)
	    {
	        $reward_info = array(100000, 100000, 300000, 500000, 1000000, 2000000, 3000000, 5000000, 7000000, 10000000);
	        
	        for($i=$vip_level+1; $i<=$before_viplevel; $i++)
	        {
	            $sql = "SELECT COUNT(reward) FROM tbl_user_vip_levelup_log WHERE useridx=$useridx AND vip_level=$i;";
	            $isrewardlog = $db_main2->getvalue($sql);
	            
	            if($isrewardlog > 0)
	            {
	                $coin += $reward_info[$i-1];
	                $sql = "DELETE FROM tbl_user_vip_levelup_log WHERE useridx=$useridx AND vip_level=$i;";
	                $db_main2->execute($sql);
	            }
	        }
	    }
	    
	    // Coin 회수
	    $sql = "SELECT coin FROM tbl_user WHERE useridx=$useridx";
	    $leftcoin = $db_main->getvalue($sql);
	    
	    $sql = "SELECT coin FROM tbl_user_cache_add WHERE useridx=$useridx";
	    $cache_coin = $db_main2->getvalue($sql);
	    
	    if ($coin > 0)
	    {
	        if ($leftcoin >= $coin)
	        {
	            $sql = "INSERT INTO tbl_freecoin_admin_log(useridx, freecoin, reason, message, admin_id, writedate) VALUES($useridx, -$coin, 'refund로 인한 코인 차감', '', 'RealTime Update', NOW());";
	            $db_main->execute($sql);
	            
	            $sql = "INSERT INTO tbl_user_cache_update(useridx, coin, writedate) VALUES($useridx, -$coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate=VALUES(writedate);";
	            $db_main2->execute($sql);
	        }
	        else if($cache_coin >= $coin)
	        {
	            $sql = "INSERT INTO tbl_freecoin_admin_log(useridx, freecoin, reason, message, admin_id, writedate) VALUES($useridx, -$coin, 'refund로 인한 코인 차감', '', 'RealTime Update', NOW());";
	            $db_main->execute($sql);
	            
	            $sql = "INSERT INTO tbl_user_cache_add(useridx,coin,writedate) VALUES($useridx,$coin,NOW()) ON DUPLICATE KEY UPDATE coin = coin - VALUES(coin), writedate = VALUES(writedate);";
	            $db_main2->execute($sql);
	        }
	        else
	        {
	            $sql = "UPDATE tbl_product_order SET cancelleftcoin=".($coin-$leftcoin)." WHERE orderidx=$orderidx;".
	   	            "INSERT INTO tbl_freecoin_admin_log(useridx, freecoin, reason, message, admin_id, writedate) VALUES($useridx, -$coin, 'refund로 인한 코인 차감(차감코인부족:".($coin-$leftcoin).")', '', 'RealTime Update', NOW());";
	            $db_main->execute($sql);
	            
	            $sql = "INSERT INTO tbl_user_cache_update(useridx, coin, writedate) VALUES($useridx, -$coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate=VALUES(writedate);";
	            $db_main2->execute($sql);
	        }
	    }
	    
	    $sql = "SELECT IFNULL(MAX(writedate), '0000-00-00 00:00:00') AS firstbuydate ".
	   	    "FROM tbl_product_order ".
	   	    "WHERE useridx = $useridx AND status = 1 ".
	   	    "LIMIT 1";
	    $lastbuydate = $db_main->getvalue($sql);
	    
	    // 쿠폰 반환 및 초기화
	    $sql = "DELETE FROM tbl_coupon WHERE useridx=$useridx AND status=0;";
	    $sql .= "UPDATE tbl_coupon SET  expiredate = now() WHERE useridx = $useridx AND expiredate > NOW() AND coupon_more != 100 AND category != 1;";
	    $sql .= "UPDATE tbl_coupon_issue SET vip_level = $vip_level, base_credit=0, lastbuydate='$lastbuydate' WHERE useridx=$useridx;";
	    $db_main2->execute($sql);
	    
	    //프리미엄 부스트 초기화
	    $sql = "UPDATE tbl_user_premium_booster SET end_date = now() WHERE useridx=$useridx AND premium_type = 2;";
	    $db_main2->execute($sql);
	    
	    if($db_main2->getvalue("SELECT COUNT(*) FROM tbl_user_retention_offer WHERE useridx = $useridx") > 0)
	    {
	        if($product_type == 16)
	            $udpate_sql = "UPDATE tbl_user_retention_offer SET type = 0 , duedate =  DATE_SUB(NOW(), INTERVAL 1 DAY), buydate = DATE_SUB(NOW(), INTERVAL 28 DAY), facebookcredit = 0  WHERE useridx = $useridx";
	            else
	                $udpate_sql = "UPDATE tbl_user_retention_offer SET type = 0 , duedate =  DATE_SUB(NOW(), INTERVAL 1 DAY), buydate = DATE_SUB(NOW(), INTERVAL 28 DAY) WHERE useridx = $useridx";
	                
	                $db_main2->execute($udpate_sql);
	    }
	    
	    // 첫 결제 정보 추가
	    $sql = "SELECT IFNULL(DATE_FORMAT(MIN(writedate), '%Y-%m-%d'), '0000-00-00') AS firstbuydate ".
	   	    "FROM ( ".
	   	    "	SELECT writedate ".
	   	    "	FROM ( ".
	   	    "		SELECT writedate ".
	   	    "		FROM tbl_product_order ".
	   	    "		WHERE useridx = $useridx AND status = 1 ".
	   	    "		ORDER BY orderidx ASC ".
	   	    "		LIMIT 1 ".
	   	    "	) t1 ".
	   	    "	UNION ALL ".
	   	    "	SELECT writedate ".
	   	    "	FROM ( ".
	   	    "		SELECT writedate ".
	   	    "		FROM tbl_product_order_mobile ".
	   	    "		WHERE useridx = $useridx AND status = 1 ".
	   	    "		ORDER BY orderidx ASC ".
	   	    "		LIMIT 1 ".
	   	    "	) t2 ".
	   	    ") t3";
	    $firstbuydate = $db_main->getvalue($sql);
	    
	    if($firstbuydate != "0000-00-00")
	    {
	        $sql = "SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS createdate FROM tbl_user WHERE useridx=$useridx";
	        $join_createdate = $db_main->getvalue($sql);
	        
	        $sql = "INSERT INTO tbl_user_first_order(useridx, join_date, purchase_date) VALUES($useridx, '$join_createdate', '$firstbuydate') ON DUPLICATE KEY UPDATE purchase_date=VALUES(purchase_date);";
	        $db_main2->execute($sql);
	    }
	    else
	    {
	        $sql = "DELETE FROM tbl_user_first_order WHERE useridx=$useridx";
	        $db_main2->execute($sql);
	        
	    }
	    
	    $sql=" SELECT SUM(cnt) FROM ".
	   	    "	( ".
	   	    " SELECT COUNT(*) AS cnt FROM tbl_product_order WHERE useridx = $useridx AND facebookcredit >= 50 AND status = 1 ".
	   	    "	    UNION ALL ".
	   	    " SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx = $useridx AND facebookcredit >= 50 AND status = 1 ".
	   	    " ) t1";
	    
	    if($db_main->getvalue($sql) == 0)
	    {
	        $sql = "UPDATE tbl_user_first_buy SET type = 0, otherbuy=0, shownow=0, buywritedate='000-00-00 00:00:00', lastshowdate='000-00-00 00:00:00' WHERE useridx=$useridx;";
	        $db_main->execute($sql);
	    }
	    
	    // 첫 결제자 5회 선물 반환 및 지급 제거
	    write_log($productidx);
	    if(46 <= $productidx && $productidx <= 50 || ($productidx == 141))
	    {
	        write_log("22222");
	        $sql ="SELECT bonus_coin, ABS(leftcount-5) AS cnt FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='0';";
	        $user_firstbuy_gift = $db_main2->getarray($sql);
	        $return_coin = ($user_firstbuy_gift[0]*$user_firstbuy_gift[1]);
	        
	        $sql = "DELETE FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='0';";
	        $db_main2->execute($sql);
	    }
	    else if ($product_type == 11)
	    {
	        $sql ="SELECT bonus_coin, ABS(leftcount-5) AS cnt FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='$productkey' AND purchasedate = '$writedate';";
	        $user_firstbuy_gift = $db_main2->getarray($sql);
	        $return_coin = ($user_firstbuy_gift[0]*$user_firstbuy_gift[1]);
	        
	        $sql = "DELETE FROM tbl_user_firstbuy_gift WHERE useridx=$useridx AND productkey ='$productkey';";
	        $db_main2->execute($sql);
	    }
	    
	    if($return_coin != 0 && $return_coin != "")
	    {
	        $sql = "INSERT INTO tbl_user_cache_update(useridx, coin, writedate) VALUES($useridx, -$return_coin, NOW()) ON DUPLICATE KEY UPDATE coin = coin + VALUES(coin), writedate=VALUES(writedate);";
	        $db_main2->execute($sql);
	    }
	    
	    
	    // 신규 사용자 7회 선물 지급 제거
	    $sql = "UPDATE tbl_user_newuser_gift SET leftcount = 0 WHERE useridx=$useridx;";
	    $db_main2->execute($sql);
	    
	    if(981 <= $productidx && $productidx <= 1260)
	    {
	        // Safe2Q 상품 제거
	        $sql = "DELETE FROM tbl_subscribe_user_bonus_info WHERE useridx=$useridx;";
	        $db_main2->execute($sql);
	    }
	    
	    // New 승률부양 삭제
	    $sql = "SELECT boosttype, max_moneyin FROM tbl_user_boost WHERE useridx = $useridx;";
	    $user_boost_info = $db_main->getarray($sql);
	    
	    $user_boost_type = $user_boost_info["boosttype"];
	    $user_boost_max_moneyin = $user_boost_info["max_moneyin"];
	    
	    if(($user_boost_type == 7 && $user_boost_max_moneyin > 0) || ($user_boost_type == 8 && $user_boost_max_moneyin > 0))
	    {
	        $change_max_moneyin = $user_boost_max_moneyin - $org_coin;
	        
	        if($change_max_moneyin < 0)
	            $change_max_moneyin = 0;
	            
	            $sql = "UPDATE tbl_user_boost SET max_moneyin = $change_max_moneyin WHERE useridx = $useridx AND boosttype = $user_boost_type";
	            $db_main->execute($sql);
	    }
	    else if($user_boost_type == 9 && $user_boost_max_moneyin > 0)
	    {
	        $sql = "SELECT MAX(multiple) FROM tbl_boost_metrics;";
	        $max_multiple = $db_main->getvalue($sql);
	        
	        $change_max_moneyin = $user_boost_max_moneyin - ($org_coin * $max_multiple);
	        
	        if($change_max_moneyin < 0)
	            $change_max_moneyin = 0;
	            
	            $sql = "UPDATE tbl_user_boost SET max_moneyin = $change_max_moneyin WHERE useridx = $useridx AND boosttype = $user_boost_type";
	            $db_main->execute($sql);
	    }
	    else
	    {
	        $sql = "DELETE FROM tbl_user_boost WHERE useridx = $useridx;";
	        $db_main->execute($sql);
	    }
	    
	    // Bonus Coin 차감(3Day & 7Day)
	    if(390 <= $product_targetcredit && $product_targetcredit <= 9990 && $productidx != 1261)
	    {
	        // 3Day Bonus coin 지급 제거
	        $sql = "UPDATE tbl_user_buy_3day_gift SET leftcount = 0 WHERE useridx = $useridx AND orderno = '$order_id'";
	        $db_main2->execute($sql);
	        
	        // 3Day Bonus coin 발급 inbox삭제
	        $sql = "SELECT inboxidx FROM tbl_user_buy_3day_gift_log WHERE useridx = $useridx AND type = 1 AND orderno = '$order_id'";
	        $refund_3day_inbox_list = $db_main2->gettotallist($sql);
	        
	        for($i = 0; $i < sizeof($refund_3day_inbox_list); $i++)
	        {
	            $delete_3day_inboxidx = $refund_3day_inbox_list[$i]["inboxidx"];
	            
	            $sql_delete_log .= "DELETE FROM tbl_user_inbox_".($useridx%20)." WHERE useridx = $useridx AND category = 111 AND inboxidx = $delete_3day_inboxidx;";
	        }
	        
	        if($sql_delete_log != "")
	        {
	            $db_inbox->execute($sql_delete_log);
	        }
	    }
	    
	    // 7Day Bonus coin 지급 제거
	    if($product_targetcredit == 590 )
	    {
	        $sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY) WHERE useridx = $useridx";
	        $db_main2->execute($sql);
	    }
	    else if($product_targetcredit == 990 )
	    {
	        $sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY) WHERE useridx = $useridx";
	        $db_main2->execute($sql);
	    }
	    else if($product_targetcredit == 1990 )
	    {
	        $sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY), inboxdate  = DATE_SUB(inboxdate, INTERVAL 7 DAY) WHERE useridx = $useridx";
	        $db_main2->execute($sql);
	    }
	    else if($product_targetcredit >= 2990 )
	    {
	        $sql = "SELECT IF(spindate != '0000-00-00' , 1, 0) AS spin_check, IF(stampdate != '0000-00-00' , 1, 0) AS stamp_check, IF(inboxdate != '0000-00-00', 1, 0) AS inbox_check FROM tbl_user_buy_7day_gift WHERE useridx = $useridx ";
	        $date_info = $db_main2->getarray($sql);
	        
	        $spin_check = $date_info["spin_check"];
	        $stamp_check = $date_info["stamp_check"];
	        $inbox_check = $date_info["inbox_check"];
	        
	        if($inbox_check == 1)
	        {
	            $sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY), inboxdate  = DATE_SUB(inboxdate, INTERVAL 7 DAY), ".
	   	            "spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
	            $db_main2->execute($sql);
	        }
	        else if($stamp_check == 1)
	        {
	            $sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), stampdate  = DATE_SUB(stampdate, INTERVAL 7 DAY), ".
	   	            "spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
	            $db_main2->execute($sql);
	        }
	        else if($spin_check == 1)
	        {
	            $sql = "UPDATE tbl_user_buy_7day_gift SET spindate = DATE_SUB(spindate, INTERVAL 7 DAY), ".
	   	            "spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
	            $db_main2->execute($sql);
	        }
	        else
	        {
	            $sql = "UPDATE tbl_user_buy_7day_gift SET spindateX4 = DATE_SUB(spindateX4, INTERVAL 7 DAY), stampdateX4  = DATE_SUB(stampdateX4, INTERVAL 7 DAY), inboxdateX4  = DATE_SUB(inboxdateX4, INTERVAL 7 DAY) WHERE useridx = $useridx";
	            $db_main2->execute($sql);
	        }
	        
	        $sql = "DELETE FROM  tbl_user_buy_vip_wheel WHERE useridx = $useridx AND orderno = '$order_id'";
	        $db_main2->execute($sql);
	        
	    }
	}
	
	// tbl_user_online 삭제
	$sql = "DELETE FROM tbl_user_online_".($useridx%10)." WHERE useridx=$useridx;";
	$sql .= "DELETE FROM tbl_user_online_sync2 WHERE useridx=$useridx;";
	$db_friend->execute($sql);
	
	
	$db_main->end();
	$db_main2->end();
	$db_friend->end();
	$db_livestats->end();
	$db_inbox->end();
	
?>