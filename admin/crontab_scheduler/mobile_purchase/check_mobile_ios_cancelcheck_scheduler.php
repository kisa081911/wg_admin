<?
	include("../../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	
	$db_main->execute("SET wait_timeout=60");
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
		$isSandbox = true;
	else
		$isSandbox = false;
	
	function getReceiptData($receipt, $isSandbox)
	{
		// determine which endpoint to use for verifying the receipt
		if ($isSandbox)
		{
			$endpoint = 'https://sandbox.itunes.apple.com/verifyReceipt';
		}
		else
		{
			$endpoint = 'https://buy.itunes.apple.com/verifyReceipt';
		}
	
		// build the post data
		$postData = json_encode(
				array('receipt-data' => $receipt)
		);
	
		// create the cURL request
		$ch = curl_init($endpoint);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
	
		// execute the cURL request and fetch response data
		$response = curl_exec($ch);
		$errno    = curl_errno($ch);
		$errmsg   = curl_error($ch);
		curl_close($ch);
	
		// ensure the request succeeded
		if ($errno != 0)
		{
			throw new Exception($errmsg, $errno);
		}
	
		// parse the response data
		$data = json_decode($response);
	
		// ensure response data was a valid JSON string
		if (!is_object($data))
		{
			throw new Exception('Invalid response data', 100);
		}
	
		// ensure the expected data is present
		if (!isset($data->status) || $data->status != 0)
		{
			throw new Exception('Invalid receipt', 200);
		}
	
		// build the response array with the returned data
		return array(
				'quantity'       =>  $data->receipt->quantity,
				'product_id'     =>  $data->receipt->product_id,
				'transaction_id' =>  $data->receipt->transaction_id,
				'purchase_date'  =>  $data->receipt->purchase_date,
				'app_item_id'    =>  $data->receipt->app_item_id,
				'bid'            =>  $data->receipt->bid,
				'bvrs'           =>  $data->receipt->bvrs
		);
	}
	
	try
	{
		$sql = "SELECT * FROM tbl_product_order_mobile WHERE orderidx = '562750'";
		$receipt_list = $db_main->gettotallist($sql);
		
		for ($i=0; $i<sizeof($receipt_list); $i++)
		{
			$useridx = $receipt_list[$i]["useridx"];
			$productidx = $receipt_list[$i]["productidx"];
			$product_type = $receipt_list[$i]["product_type"];
			$receipt = $receipt_list[$i]["receipt"];
			$errcount = $receipt_list[$i]["errcount"];
			$orderno = $receipt_list[$i]["orderno"];
			$orderidx = $receipt_list[$i]["orderidx"];
			$money = $receipt_list[$i]["money"];
			$mobile_facebookcredit = $receipt_list[$i]["facebookcredit"];
			$usercoin = $receipt_list[$i]["usercoin"];
			$coin = $receipt_list[$i]["coin"];
			$basecoin = $receipt_list[$i]["basecoin"];
			$gift_coin = $receipt_list[$i]["gift_coin"];
			$special_more = $receipt_list[$i]["special_more"];
			$status = $receipt_list[$i]["status"];
			$parent_orderidx = $receipt_list[$i]["parent_orderidx"];
			
			$info = getReceiptData($receipt, $isSandbox);
			
			write_log($info['transaction_id']."|".$orderno);
			
			if($info['transaction_id'] == $orderno)
			{
				write_log("OK");
			}
			else
			{
				write_log("Cancel");
			}
		}
				
	}
	catch (Exception $ex)
	{		
	}
	
	$db_main->end();
?>
