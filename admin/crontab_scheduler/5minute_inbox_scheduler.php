<?
	include("../common/common_include.inc.php");

	$db_analysis = new CDatabase_Analysis();
	$db_livestats = new CDatabase_Slave_Livestats();

	$db_analysis->execute("SET wait_timeout=7200");
	$db_livestats->execute("SET wait_timeout=7200");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/5minute_inbox_scheduler") !== false)
		{
			$count++;
		}
	}
	
	
	if ($count > 1)
		exit();

	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$std_useridx = 10000;
	}
	
	try 
	{
		if (date("H") == "00")
		{
			$today = date("Y-m-d", time() - 60 * 60);
			$tomorrow = date("Y-m-d", time() + 60 * 60);
		}
		else
		{
			$today = date("Y-m-d");
			$tomorrow = date("Y-m-d", time() + 24 * 60 * 60);
		}
		
		// tbl_inbox_collect
		$inbox_sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, type AS category, category AS type, COUNT(DISTINCT useridx) AS usercount, COUNT(*) AS freecount, IFNULL(SUM(coin), 0) AS freeamount, MAX(coin) AS max_coin
					FROM
					(
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_0` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_1` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_2` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_3` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_4` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_5` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_6` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_7` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_8` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_9` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_10` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_11` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_12` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_13` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_14` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_15` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_16` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_17` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_18` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_19` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					)t1
					GROUP BY TYPE, category";
		$inbox_list = $db_livestats->gettotallist($inbox_sql);
		
		for($j=0; $j<sizeof($inbox_list); $j++)
		{
			$writedate = $inbox_list[$j]["writedate"];
			$category = $inbox_list[$j]["category"];
			$inbox_type = $inbox_list[$j]["type"];
			$usercount = $inbox_list[$j]["usercount"];
			$freecount = $inbox_list[$j]["freecount"];
			$freeamount = $inbox_list[$j]["freeamount"];
			$max_coin = $inbox_list[$j]["max_coin"];
				
			$sql = "INSERT INTO tbl_inbox_coin_daily(today, category, inbox_type, usercount, freecount, freeamount, max_amount) VALUES ".
					"('$writedate', $category, $inbox_type, $usercount, $freecount, $freeamount, $max_coin) ".
					"ON DUPLICATE KEY UPDATE usercount = VALUES(usercount), freecount = VALUES(freecount), freeamount = VALUES(freeamount), max_amount = VALUES(max_amount);";
			$db_analysis->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_analysis->end();
	$db_livestats->end();
?>
