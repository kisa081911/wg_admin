<?
	include("../common/common_include.inc.php");

	$db_main2 = new CDatabase_Main2();
	
	$db_main2->execute("SET wait_timeout=72000");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$port = "";
	
	$str_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/daily_freespin_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/daily_freespin_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("daily_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	try
	{
    	   $sql="SELECT 	DATE_FORMAT(writedate,'%Y-%m-%d') AS today ".
        	                " ,days ".
                        	" ,slottype ".
                        	" ,os_type ".
                        	" , COUNT(useridx) AS total_count ".
                        	" , SUM(IF(spin_cnt = 0 AND refreshcount >=5,1,0)) AS finish_user_count ".
                        	" , SUM(IF(spin_cnt > 0 AND refreshcount >=5,1,0)) AS notfinish_user_count ".
                        	" , SUM(winamount) AS total_winamount ".
                        	" , ROUND(SUM(winamount)/COUNT(useridx),2) AS avg_winamount ".
                        	" , SUM(IF(spin_cnt = 0 AND refreshcount >=5,winamount,0)) AS finish_user_winamount ".
                        	", ifnull(ROUND(SUM(IF(spin_cnt = 0 AND refreshcount >=5,winamount,0))/SUM(IF(spin_cnt = 0 AND refreshcount >=5,1,0)),2),0)AS notfinish_user_winamount ".
                        " FROM ".
                        " ( ".
                        	" SELECT * FROM `tbl_daily_freespin_0` WHERE writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' ".
                            " UNION ALL ".
                            " SELECT * FROM `tbl_daily_freespin_1` WHERE writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' ".
                        	" UNION ALL ".
                        	" SELECT * FROM `tbl_daily_freespin_2` WHERE writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' ".
                            " UNION ALL ".
                        	" SELECT * FROM `tbl_daily_freespin_3` WHERE writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' ".
                            " UNION ALL ".
                        	" SELECT * FROM `tbl_daily_freespin_4` WHERE writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' ".
                            " UNION ALL ".
                        	" SELECT * FROM `tbl_daily_freespin_5` WHERE writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' ".
                            " UNION ALL ".
                        	" SELECT * FROM `tbl_daily_freespin_6` WHERE writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' ".
                        	" UNION ALL ".
                            " SELECT * FROM `tbl_daily_freespin_7` WHERE writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' ".
                            " UNION ALL ".
                        	" SELECT * FROM `tbl_daily_freespin_8` WHERE writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' ".
                            " UNION ALL ".
                            " SELECT * FROM `tbl_daily_freespin_9` WHERE writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' ".
                          "  ) t1 ".
                        " GROUP BY slottype,days,os_type ".
                        " ORDER BY days ASC";
    	   $daily_freespin_stat_list = $db_main2->gettotallist($sql);
    	   
    	   for($i = 0; $i < sizeof($daily_freespin_stat_list); $i++)
    	   {
    	       $today = $daily_freespin_stat_list[$i]["today"];
    	       $days = $daily_freespin_stat_list[$i]["days"];
    	       $slottype = $daily_freespin_stat_list[$i]["slottype"];
    	       $os_type = $daily_freespin_stat_list[$i]["os_type"];
    	       $total_count = $daily_freespin_stat_list[$i]["total_count"];
    	       $finish_user_count = $daily_freespin_stat_list[$i]["finish_user_count"];
    	       $notfinish_user_count = $daily_freespin_stat_list[$i]["notfinish_user_count"];
    	       $total_winamount = $daily_freespin_stat_list[$i]["total_winamount"];
    	       $avg_winamount = $daily_freespin_stat_list[$i]["avg_winamount"];
    	       $finish_user_winamount = $daily_freespin_stat_list[$i]["finish_user_winamount"];
    	       $notfinish_user_winamount = $daily_freespin_stat_list[$i]["notfinish_user_winamount"];
    	       
    	       $sql = " INSERT INTO tbl_daily_freespin_stat(today, days, slottype,os_type, total_count, finish_user_count, notfinish_user_count,total_winamount,avg_winamount,finish_user_winamount,notfinish_user_winamount) ".
        	          " VALUES('$today', $days, $slottype, $os_type, $total_count, $finish_user_count, $notfinish_user_count,$total_winamount,$avg_winamount,$finish_user_winamount,$notfinish_user_winamount) ".
                      " ON DUPLICATE KEY UPDATE total_count=VALUES(total_count), finish_user_count=VALUES(finish_user_count), notfinish_user_count=VALUES(notfinish_user_count),total_winamount=VALUES(total_winamount),avg_winamount=VALUES(avg_winamount),finish_user_winamount=VALUES(finish_user_winamount),notfinish_user_winamount =VALUES(notfinish_user_winamount);";
    	       $db_main2->execute($sql);
    	   }
    	   
    	   
    	   $sql="SELECT DATE_FORMAT(coupon_writedate,'%Y-%m-%d') as coupon_writedate ".
        	                " , count(useridx) AS coupon_count ".
        					" , SUM(if(is_buy=1,1,0)) AS use_count ".
        					" , SUM(coupon_more) as total_more ".
        					" , ROUND(avg(coupon_more),1) AS avg_more ".
        					" FROM tbl_user_mobilecoupon_log where coupon_writedate != '0000-00-00 00:00:00' AND coupon_writedate >= '$today 00:00:00' AND coupon_writedate <= '$today 23:59:59'  ".
         			" GROUP BY DATE_FORMAT(coupon_writedate,'%Y-%m-%d')";
    	   $user_mobilecoupon_stat_list = $db_main2->gettotallist($sql);
    	   
    	   for($i = 0; $i < sizeof($user_mobilecoupon_stat_list); $i++)
    	   {
    	       $today = $user_mobilecoupon_stat_list[$i]["coupon_writedate"];
    	       $coupon_count = $user_mobilecoupon_stat_list[$i]["coupon_count"];
    	       $use_count = $user_mobilecoupon_stat_list[$i]["use_count"];
    	       $total_more = $user_mobilecoupon_stat_list[$i]["total_more"];
    	       $avg_more = $user_mobilecoupon_stat_list[$i]["avg_more"];
    	       
    	       $sql = " insert into `tbl_user_mobilecoupon_stat` (today, coupon_count,use_count,total_more,avg_more) ".
        	          " VALUES('$today',$coupon_count,$use_count,$total_more,$avg_more) ".
                      " ON DUPLICATE KEY UPDATE coupon_count=VALUES(coupon_count),use_count=VALUES(use_count),total_more=VALUES(total_more),avg_more=VALUES(avg_more);";
    	       $db_main2->execute($sql);
    	   }
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	$db_main2->end();
?>