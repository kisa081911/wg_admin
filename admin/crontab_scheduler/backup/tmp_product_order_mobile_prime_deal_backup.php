<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
 	$today = date("Y-m-d");
 	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
 	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
	
	ini_set("memory_limit", "-1");	
	
	$sdate = "2019-06-11";
	$edate = "2019-06-27";
	
	while($sdate <= $edate)
	{
		$yesterday = $sdate;
		$yesterday_str = date("Ymd", strtotime($yesterday));
		
		write_log("rebackup_primedeal : $yesterday|$yesterday_str");
	
		try
		{
			$db_redshift = new CDatabase_Redshift();
		
			// Create a staging table
			$sql = "CREATE TABLE t5_product_order_mobile_prime_tmp (LIKE t5_product_order_mobile);";
			$db_redshift->execute($sql);
				
			// Load data into the staging table
			$sql = "copy t5_product_order_mobile_prime_tmp ".
					"from 's3://wg-redshift/take5/product_order_mobile_prime/$yesterday_str' ".
					"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
					"delimiter '|' ".
					"csv;";
			$db_redshift->execute($sql);
				
			// Update records
			$sql = "UPDATE t5_product_order_mobile ".
					"SET usercoin = s.usercoin, userlevel = s.userlevel, coin = s.coin, orderno = s.orderno, receipt = s.receipt, signature = s.signature, facebookcredit = s.facebookcredit, ".
					" money = s.money, vip_point = s.vip_point, status = s.status, gift_status = s.gift_status, gift_coin = s.gift_coin, basecoin = s.basecoin, canceldate = s.canceldate, ".
					" cancelleftcoin = s.cancelleftcoin, product_type = s.product_type, parent_orderidx = s.parent_orderidx, writedate = s.writedate ".
					"FROM t5_product_order_mobile_prime_tmp s ".
					"WHERE t5_product_order_mobile.orderidx = s.orderidx;";
			$db_redshift->execute($sql);
	
			// Insert records
			$sql = "INSERT INTO t5_product_order_mobile ".
					"SELECT s.* FROM t5_product_order_mobile_prime_tmp s LEFT JOIN t5_product_order_mobile ".
					"ON s.orderidx = t5_product_order_mobile.orderidx ".
					"WHERE t5_product_order_mobile.orderidx IS NULL;";
			$db_redshift->execute($sql);
	
			// Drop the staging table
			$sql = "DROP TABLE t5_product_order_mobile_prime_tmp;";
			$db_redshift->execute($sql);
	
		
			$db_redshift->end();
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));
	}
?>