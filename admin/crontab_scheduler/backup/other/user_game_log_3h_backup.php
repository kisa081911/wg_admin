<?
    include("../../../common/common_include.inc.php");
    include("../../../common/dbconnect/db_util_redshift.inc.php");
    require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$today = date("Y-m-d");
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
  
	// tbl_user_gamelog_3h - Facebook
	try
	{
		write_log("redshift_user_gamelog_3h - Facebook start : ".date("Y-m-d H:i:s"));
		
		$db_other = new CDatabase_Other();
		$db_other->execute("SET wait_timeout=3600");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_gamelog_3h'";
		$columns_count = $db_other->getvalue($sql);
		
		$sql = 	"SELECT MIN(logidx) AS min_logidx, MAX(logidx) AS max_logidx
				 FROM tbl_user_gamelog_3h
				 WHERE '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00' ";
		$index_info = $db_other->getarray($sql);
		
		$s_index = $index_info["min_logidx"];
		$e_index = $index_info["max_logidx"];
		
		while($s_index < $e_index)
		{
			$temp_index = $s_index + 10000;
		
			$output = "";
				
			$sql = 	"SELECT logidx, useridx, moneyin, moneyout, playcount, coin, bet_ratio, writedate
					 FROM tbl_user_gamelog_3h
					 WHERE $s_index <= logidx AND logidx < $temp_index ";
			$log_list = $db_other->gettotallist($sql);
				
			for($i=0; $i<sizeof($log_list); $i++)
			{
				for($j=0; $j<$columns_count; $j++)
				{
					$log_list[$i][$j] = str_replace("\"", "\"\"", $log_list[$i][$j]);
					
					if($log_list[$i][$j] == "0000-00-00 00:00:00")
						$log_list[$i][$j] = "1900-01-01 00:00:00";
						
					if($j == $columns_count - 1)
						$output .= '"'.$log_list[$i][$j].'"';
					else
						$output .= '"'.$log_list[$i][$j].'"|';
				}
				$output .="\n";
			}
				
			$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_gamelog_3h_".$yesterday_str.".txt", 'a+');
		
			fwrite($fp, $output);
				
			fclose($fp);
		
			$s_index = $temp_index;
		}
		
		$db_other->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	 
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_gamelog_3h/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_gamelog_3h_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_gamelog_3h_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_gamelog_3h_tmp (LIKE t5_user_gamelog_3h);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy t5_user_gamelog_3h_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_gamelog_3h/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
		// Update records
		$sql = "UPDATE t5_user_gamelog_3h ". 
				"SET useridx = s.useridx, money_in = s.money_in, money_out = s.money_out, playcount = s.playcount, coin = s.coin, bet_ratio = s.bet_ratio, writedate = s.writedate ".
				"FROM t5_user_gamelog_3h_tmp s ".
				"WHERE t5_user_gamelog_3h.logidx = s.logidx;";
		$db_redshift->execute($sql); 

		// Insert records
		$sql = "INSERT INTO t5_user_gamelog_3h ".
				"SELECT s.* FROM t5_user_gamelog_3h_tmp s LEFT JOIN t5_user_gamelog_3h ".
				"ON s.logidx = t5_user_gamelog_3h.logidx ".
				"WHERE t5_user_gamelog_3h.logidx IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_user_gamelog_3h_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();
		
		write_log("redshift_user_gamelog_3h - Facebook end : ".date("Y-m-d H:i:s"));
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	// tbl_user_gamelog_3h - android
	try
	{
		write_log("redshift_user_gamelog_3h - android start : ".date("Y-m-d H:i:s"));
		
		$db_other = new CDatabase_Other();
		$db_other->execute("SET wait_timeout=3600");
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_gamelog_android_3h'";
		$columns_count = $db_other->getvalue($sql);
	
		$sql = 	"SELECT MIN(logidx) AS min_logidx, MAX(logidx) AS max_logidx
				 FROM tbl_user_gamelog_android_3h
				 WHERE '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00' ";
		$index_info = $db_other->getarray($sql);
	
		$s_index = $index_info["min_logidx"];
		$e_index = $index_info["max_logidx"];
	
		while($s_index < $e_index)
		{
			$temp_index = $s_index + 10000;
	
			$output = "";
	
			$sql = 	"SELECT logidx, useridx, moneyin, moneyout, playcount, coin, bet_ratio, writedate
					 FROM tbl_user_gamelog_android_3h
					 WHERE $s_index <= logidx AND logidx < $temp_index ";
			$log_list = $db_other->gettotallist($sql);
	
			for($i=0; $i<sizeof($log_list); $i++)
			{
				for($j=0; $j<$columns_count; $j++)
				{
					$log_list[$i][$j] = str_replace("\"", "\"\"", $log_list[$i][$j]);
					
					if($log_list[$i][$j] == "0000-00-00 00:00:00")
						$log_list[$i][$j] = "1900-01-01 00:00:00";
	
					if($j == $columns_count - 1)
						$output .= '"'.$log_list[$i][$j].'"';
					else
						$output .= '"'.$log_list[$i][$j].'"|';
				}
				
				$output .="\n";
			}
	
			$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_gamelog_android_3h_".$yesterday_str.".txt", 'a+');
	
			fwrite($fp, $output);
	
			fclose($fp);
	
			$s_index = $temp_index;
		}
		
		$db_other->end();
	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
		
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_gamelog_android_3h/$yesterday_str";
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_gamelog_android_3h_$yesterday_str.txt";
		
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
			'Bucket' 		=> $bucket,
			'Key'    		=> 'tbl_user_gamelog_android_3h_'.$yesterday_str.'.txt',
			'ACL'	 		=> 'public-read',
			'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
		
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_gamelog_android_3h_tmp (LIKE t5_user_gamelog_android_3h);";
		$db_redshift->execute($sql);
	
		// Load data into the staging table
		$sql = 	"copy t5_user_gamelog_android_3h_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_gamelog_android_3h/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
	
		// Update records
		$sql = "UPDATE t5_user_gamelog_android_3h ".
				"SET useridx = s.useridx, money_in = s.money_in, money_out = s.money_out, playcount = s.playcount, coin = s.coin, bet_ratio = s.bet_ratio, writedate = s.writedate ".
				"FROM t5_user_gamelog_android_3h_tmp s ".
				"WHERE t5_user_gamelog_android_3h.logidx = s.logidx;";
		$db_redshift->execute($sql);
	
		// Insert records
		$sql = "INSERT INTO t5_user_gamelog_android_3h ".
				"SELECT s.* FROM t5_user_gamelog_android_3h_tmp s LEFT JOIN t5_user_gamelog_android_3h ".
				"ON s.logidx = t5_user_gamelog_android_3h.logidx ".
				"WHERE t5_user_gamelog_android_3h.logidx IS NULL;";
		$db_redshift->execute($sql);
		
		// Drop the staging table
		$sql = "DROP TABLE t5_user_gamelog_android_3h_tmp;";
		$db_redshift->execute($sql);
	
		$db_redshift->end();
		
		write_log("redshift_user_gamelog_3h - android end : ".date("Y-m-d H:i:s"));
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	// tbl_user_gamelog_3h - ios	
	try
	{
		write_log("redshift_user_gamelog_3h - ios start : ".date("Y-m-d H:i:s"));
		
		$db_other = new CDatabase_Other();
		$db_other->execute("SET wait_timeout=3600");
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_gamelog_ios_3h'";
		$columns_count = $db_other->getvalue($sql);
	
		$sql = 	"SELECT MIN(logidx) AS min_logidx, MAX(logidx) AS max_logidx
				 FROM tbl_user_gamelog_ios_3h
				 WHERE '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00' ";
		$index_info = $db_other->getarray($sql);
	
		$s_index = $index_info["min_logidx"];
		$e_index = $index_info["max_logidx"];
	
		while($s_index < $e_index)
		{
			$temp_index = $s_index + 10000;
	
			$output = "";
	
			$sql = 	"SELECT logidx, useridx, moneyin, moneyout, playcount, coin, bet_ratio, writedate
					 FROM tbl_user_gamelog_ios_3h
					 WHERE $s_index <= logidx AND logidx < $temp_index ";
			$log_list = $db_other->gettotallist($sql);
	
			for($i=0; $i<sizeof($log_list); $i++)
			{
				for($j=0; $j<$columns_count; $j++)
				{
					$log_list[$i][$j] = str_replace("\"", "\"\"", $log_list[$i][$j]);
					
					if($log_list[$i][$j] == "0000-00-00 00:00:00")
						$log_list[$i][$j] = "1900-01-01 00:00:00";
	
					if($j == $columns_count - 1)
						$output .= '"'.$log_list[$i][$j].'"';
					else
						$output .= '"'.$log_list[$i][$j].'"|';
				}
				
				$output .="\n";
			}
	
			$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_gamelog_ios_3h_".$yesterday_str.".txt", 'a+');
	
			fwrite($fp, $output);
	
			fclose($fp);
	
			$s_index = $temp_index;
		}
		
		$db_other->end();
	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_gamelog_ios_3h/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_gamelog_ios_3h_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
			'Bucket' 		=> $bucket,
			'Key'    		=> 'tbl_user_gamelog_ios_3h_'.$yesterday_str.'.txt',
			'ACL'	 		=> 'public-read',
			'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
			if($ObjectURL != "")
			{
				@unlink($filepath);
			}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();

		// Create a staging table
		$sql = "CREATE TABLE t5_user_gamelog_ios_3h_tmp (LIKE t5_user_gamelog_ios_3h);";
		$db_redshift->execute($sql);

		// Load data into the staging table
		$sql = "copy t5_user_gamelog_ios_3h_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_gamelog_ios_3h/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);

		// Update records
		$sql = "UPDATE t5_user_gamelog_ios_3h ".
				"SET useridx = s.useridx, money_in = s.money_in, money_out = s.money_out, playcount = s.playcount, coin = s.coin, bet_ratio = s.bet_ratio, writedate = s.writedate ".
				"FROM t5_user_gamelog_ios_3h_tmp s ".
				"WHERE t5_user_gamelog_ios_3h.logidx = s.logidx;";
		$db_redshift->execute($sql);

		// Insert records
		$sql = "INSERT INTO t5_user_gamelog_ios_3h ".
				"SELECT s.* FROM t5_user_gamelog_ios_3h_tmp s LEFT JOIN t5_user_gamelog_ios_3h ".
				"ON s.logidx = t5_user_gamelog_ios_3h.logidx ".
				"WHERE t5_user_gamelog_ios_3h.logidx IS NULL;";
		$db_redshift->execute($sql);
	
		// Drop the staging table
		$sql = "DROP TABLE t5_user_gamelog_ios_3h_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();
		
		write_log("redshift_user_gamelog_3h - ios end : ".date("Y-m-d H:i:s"));
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
		
	// tbl_user_gamelog_3h - amazon		
	try
	{
		write_log("redshift_user_gamelog_3h - amazon start : ".date("Y-m-d H:i:s"));
		
		$db_other = new CDatabase_Other();
		$db_other->execute("SET wait_timeout=3600");
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_gamelog_amazon_3h'";
		$columns_count = $db_other->getvalue($sql);
	
		$sql = 	"SELECT MIN(logidx) AS min_logidx, MAX(logidx) AS max_logidx
				 FROM tbl_user_gamelog_amazon_3h
				 WHERE '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'";
		$index_info = $db_other->getarray($sql);
	
		$s_index = $index_info["min_logidx"];
		$e_index = $index_info["max_logidx"];
	
		while($s_index < $e_index)
		{
			$temp_index = $s_index + 10000;
	
			$output = "";
	
			$sql = 	"SELECT logidx, useridx, moneyin, moneyout, playcount, coin, bet_ratio, writedate
					 FROM tbl_user_gamelog_amazon_3h
					 WHERE $s_index <= logidx AND logidx < $temp_index";
			$log_list = $db_other->gettotallist($sql);
	
			for($i=0; $i<sizeof($log_list); $i++)
			{
				for($j=0; $j<$columns_count; $j++)
				{
					$log_list[$i][$j] = str_replace("\"", "\"\"", $log_list[$i][$j]);
					
					if($log_list[$i][$j] == "0000-00-00 00:00:00")
						$log_list[$i][$j] = "1900-01-01 00:00:00";
						
					if($j == $columns_count - 1)
						$output .= '"'.$log_list[$i][$j].'"';
					else
						$output .= '"'.$log_list[$i][$j].'"|';
				}
				
				$output .="\n";
			}
	
			$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_gamelog_amazon_3h_".$yesterday_str.".txt", 'a+');
	
			fwrite($fp, $output);
	
			fclose($fp);
	
			$s_index = $temp_index;
		}
		
		$db_other->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_gamelog_amazon_3h/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_gamelog_amazon_3h_$yesterday_str.txt";

		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_gamelog_amazon_3h_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
				
		));
		
		$ObjectURL = $result["ObjectURL"];
		
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_gamelog_amazon_3h_tmp (LIKE t5_user_gamelog_amazon_3h);";
		$db_redshift->execute($sql);
	
		// Load data into the staging table
		$sql = "copy t5_user_gamelog_amazon_3h_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_gamelog_amazon_3h/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
	
		// Update records
		$sql = "UPDATE t5_user_gamelog_amazon_3h ".
				"SET useridx = s.useridx, money_in = s.money_in, money_out = s.money_out, playcount = s.playcount, coin = s.coin, bet_ratio = s.bet_ratio, writedate = s.writedate ".
				"FROM t5_user_gamelog_amazon_3h_tmp s ".
				"WHERE t5_user_gamelog_amazon_3h.logidx = s.logidx;";
		$db_redshift->execute($sql);
	
		// Insert records
		$sql = "INSERT INTO t5_user_gamelog_amazon_3h ".
				"SELECT s.* FROM t5_user_gamelog_amazon_3h_tmp s LEFT JOIN t5_user_gamelog_amazon_3h ".
				"ON s.logidx = t5_user_gamelog_amazon_3h.logidx ".
				"WHERE t5_user_gamelog_amazon_3h.logidx IS NULL;";
		$db_redshift->execute($sql);
	
		// Drop the staging table
		$sql = "DROP TABLE t5_user_gamelog_amazon_3h_tmp;";
		$db_redshift->execute($sql);
	
		$db_redshift->end();
		
		write_log("redshift_user_gamelog_3h - amazon end : ".date("Y-m-d H:i:s"));
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>