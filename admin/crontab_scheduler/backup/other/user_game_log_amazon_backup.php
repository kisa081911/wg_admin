<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/other/user_game_log_amazon_backup") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/other/user_game_log_amazon_backup") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("backup/user_game_log_amazon_backup Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$today = date("Y-m-d");
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
	
  	try
	{
		$db_other = new CDatabase_Other();
		$db_other->execute("SET wait_timeout=3600");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_gamelog_amazon'";
		$columns_count = $db_other->getvalue($sql);
		
		for($u=0; $u<10; $u++)
		{
			$sql = "SELECT logidx, useridx, slottype, betlevel, mode, moneyin, moneyout, treatamount, playcount, usercoin, bet_rate, writedate, is_v2 ".
					" FROM tbl_user_gamelog_amazon WHERE '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00' AND useridx%10 = $u ";
			$log_list = $db_other->gettotallist($sql);
			
			for($i=0; $i<sizeof($log_list); $i++)
			{
				$output = "";
				
				for($j=0; $j<$columns_count; $j++)
				{
					$log_list[$i][$j] = str_replace("\"", "\"\"", $log_list[$i][$j]);
					
					if($log_list[$i][$j] == "0000-00-00 00:00:00")
						$log_list[$i][$j] = "1900-01-01 00:00:00";
						
					if($j == $columns_count - 1)
						$output .= '"'.$log_list[$i][$j].'"';
					else
						$output .= '"'.$log_list[$i][$j].'"|';
				}
				
				$output .="\n";
				
				if($output != "")
				{
				  $fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_gamelog_amazon_".$yesterday_str.".txt", 'a+');
				  
				  fwrite($fp, $output);
				  
				  fclose($fp);
				}
			}
		}
		
		$db_other->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	 
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/take5/tbl_user_gamelog_amazon/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_gamelog_amazon_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_gamelog_amazon_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_gamelog_amazon_tmp (LIKE t5_user_gamelog_amazon);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy t5_user_gamelog_amazon_tmp ".
				"from 's3://wg-redshift/take5/tbl_user_gamelog_amazon/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
		// Update records
		$sql = "UPDATE t5_user_gamelog_amazon ". 
				"SET slottype = s.slottype, betlevel = s.betlevel, mode = s.mode, money_in = s.money_in, money_out = s.money_out, treatamount = s.treatamount,  playcount = s.playcount".
				",  usercoin = s.usercoin, bet_rate = s.bet_rate, writedate = s.writedate, is_v2 = s.is_v2 ".				
				"FROM t5_user_gamelog_amazon_tmp s ".
				"WHERE t5_user_gamelog_amazon.logidx = s.logidx;";
		$db_redshift->execute($sql); 

		// Insert records
		$sql = "INSERT INTO t5_user_gamelog_amazon ".
				"SELECT s.* FROM t5_user_gamelog_amazon_tmp s LEFT JOIN t5_user_gamelog_amazon ".
				"ON s.logidx = t5_user_gamelog_amazon.logidx ".
				"WHERE t5_user_gamelog_amazon.logidx IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_user_gamelog_amazon_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
?>