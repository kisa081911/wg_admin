<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/other/user_buyer_leave_stat_backup") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/other/user_buyer_leave_stat_backup") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("backup/user_buyer_leave_stat_backup Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$today = date("Y-m-d");
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
	
	try
	{		
		$db_other = new CDatabase_Other();
		$db_other->execute("SET wait_timeout=3600");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_buyer_leave_stat'";
		$columns_count = $db_other->getvalue($sql);
		
		$sql = 	"SELECT * FROM tbl_user_buyer_leave_stat WHERE today = '$yesterday'";
		$log_list = $db_other->gettotallist($sql);
		
		for($i=0; $i<sizeof($log_list); $i++)
		{
			$output = "";
				
			for($j=0; $j<$columns_count; $j++)
			{
				$log_list[$i][$j] = str_replace("\"", "\"\"", $log_list[$i][$j]);
					
				if($log_list[$i][$j] == "0000-00-00 00:00:00")
					$log_list[$i][$j] = "1900-01-01 00:00:00";
						
				if($j == $columns_count - 1)
					$output .= '"'.$log_list[$i][$j].'"';
				else
					$output .= '"'.$log_list[$i][$j].'"|';
			}
				
			$output .="\n";
			
			if($output != "")
			{
			  $fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_buyer_leave_stat_".$yesterday_str.".txt", 'a+');
			  
			  fwrite($fp, $output);
			  
			  fclose($fp);
			}
		}
		
		$db_other->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$is_file_exist = file_exists("$DOCUMENT_ROOT/redshift_temp/tbl_user_buyer_leave_stat_".$yesterday_str.".txt");
	
	if($is_file_exist)
	{	 
		try
		{
			// Create the AWS service builder, providing the path to the config file
			$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
		
			$s3Client = $aws->get('s3');
			$bucket = "wg-redshift/t5/tbl_user_buyer_leave_stat/$yesterday_str";
		
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_buyer_leave_stat_$yesterday_str.txt";
		
			// Upload an object to Amazon S3
			$result = $s3Client->putObject(array(
					'Bucket' 		=> $bucket,
					'Key'    		=> 'tbl_user_buyer_leave_stat_'.$yesterday_str.'.txt',
					'ACL'	 		=> 'public-read',
					'SourceFile'   	=> $filepath
			));
		
			$ObjectURL = $result["ObjectURL"];
		
			if($ObjectURL != "")
			{
				@unlink($filepath);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
	
		try
		{
			$db_redshift = new CDatabase_Redshift();
			
		
			// Create a staging table
			$sql = "CREATE TABLE t5_user_buyer_leave_stat_tmp (LIKE t5_user_buyer_leave_stat);";
			$db_redshift->execute($sql);
			
			// Load data into the staging table
			$sql = "copy t5_user_buyer_leave_stat_tmp ".
					"from 's3://wg-redshift/t5/tbl_user_buyer_leave_stat/$yesterday_str' ".
					"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
					"delimiter '|' ".
					"csv;";
			$db_redshift->execute($sql);
			
			// Update records
			$sql = "UPDATE t5_user_buyer_leave_stat ". 
					"SET last_buy_platform = s.last_buy_platform, last_login_platform = s.last_login_platform, recent_avg_buy_coin = s.recent_avg_buy_coin, recent_avg_buy_money = s.recent_avg_buy_money, ".
					"recent_avg_rebuyday = s.recent_avg_rebuyday, recent_avg_buy_more = s.recent_avg_buy_more, recent_buy_count = s.recent_buy_count, recent_coin_p = s.recent_coin_p, ".
					"avg_buy_coin = s.avg_buy_coin, avg_buy_money = s.avg_buy_money, ".
					"current_coin = s.current_coin, avg_rebuyday = s.avg_rebuyday, avg_buy_more = s.avg_buy_more, buy_count = s.buy_count, buy_leave_day = s.buy_leave_day, ".
					"login_leave_day = s.login_leave_day, money_in = s.money_in, money_out = s.money_out, playcount = s.playcount, coin_p = s.coin_p, ".
					"basic_coupon_remainday = s.basic_coupon_remainday, promotion_coupon2_remainday = s.promotion_coupon2_remainday, promotion_coupon3_remainday = s.promotion_coupon3_remainday, ".
					"coupon1 = s.coupon1, coupon2 = s.coupon2, coupon3 = s.coupon3, ".
					"web_season_product1 = s.web_season_product1, web_season_product2 = s.web_season_product2, web_season_product3 = s.web_season_product3, ".
					"web_season_product_more1 = s.web_season_product_more1, web_season_product_more2 = s.web_season_product_more2, web_season_product_more3 = s.web_season_product_more3, ".
					"mobile_season_product1 = s.mobile_season_product1, mobile_season_product2 = s.mobile_season_product2, mobile_season_product3 = s.mobile_season_product3, ".
					"mobile_season_product_more1 = s.web_season_product_more1, mobile_season_product_more2 = s.mobile_season_product_more2, mobile_season_product_more3 = s.mobile_season_product_more3, ".
					"whale_product = s.whale_product, whale_product_more = s.whale_product_more, ".
					"threshold_product1 = s.threshold_product1, threshold_product2 = s.threshold_product2, threshold_product_more1 = s.threshold_product_more1,  threshold_product_more2 = s.threshold_product_more2, ".
					"piggypot_stat = s.piggypot_stat ".
					"FROM t5_user_buyer_leave_stat_tmp s ".
					"WHERE t5_user_buyer_leave_stat.today = s.today AND t5_user_buyer_leave_stat.useridx = s.useridx;";
			$db_redshift->execute($sql); 
	
			// Insert records
			$sql = "INSERT INTO t5_user_buyer_leave_stat ".
					"SELECT s.* FROM t5_user_buyer_leave_stat_tmp s LEFT JOIN t5_user_buyer_leave_stat ".
					"ON s.today = t5_user_buyer_leave_stat.today AND  s.useridx = t5_user_buyer_leave_stat.useridx ".
					"WHERE t5_user_buyer_leave_stat.today IS NULL AND t5_user_buyer_leave_stat.useridx IS NULL;";
			$db_redshift->execute($sql);
				
			// Drop the staging table
			$sql = "DROP TABLE t5_user_buyer_leave_stat_tmp;";
			$db_redshift->execute($sql);
	
			$db_redshift->end();
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
	}
	
?>