<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/other/redshift_user_playstat_gather_daily_backup") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/other/redshift_user_playstat_gather_daily_backup") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("backup/redshift_user_playstat_gather_daily_backup Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
	
	//Web
	try
	{
		$db_other = new CDatabase_Other();
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_playstat_gather_daily'";
		$columns_count = $db_other->getvalue($sql);

		$sql = "SELECT * FROM tbl_user_playstat_gather_daily WHERE today = '$yesterday'";
		$log_list = $db_other->gettotallist($sql);
			
		for($i=0; $i<sizeof($log_list); $i++)
		{
			$output = "";
				
			for($j=0; $j<$columns_count; $j++)
			{
				$log_list[$i][$j] = str_replace("\"", "\"\"", $log_list[$i][$j]);
					
				if($log_list[$i][$j] == "0000-00-00 00:00:00")
					$log_list[$i][$j] = "1900-01-01 00:00:00";
						
				if($j == $columns_count - 1)
					$output .= '"'.$log_list[$i][$j].'"';
				else
					$output .= '"'.$log_list[$i][$j].'"|';
			}
				
			$output .="\n";
			
			if($output != "")
			{
			  $fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_playstat_gather_daily_".$yesterday_str.".txt", 'a+');
			  
			  fwrite($fp, $output);
			  
			  fclose($fp);
			}
		}

		
		$db_other->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_playstat_gather_daily/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_playstat_gather_daily_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_playstat_gather_daily_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_playstat_gather_daily_tmp (LIKE t5_user_playstat_gather_daily);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy t5_user_playstat_gather_daily_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_playstat_gather_daily/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
		// Update records
		$sql = "UPDATE t5_user_playstat_gather_daily ".
				"SET usercount = s.usercount, currentcoin = s.currentcoin, moneyin = s.moneyin, moneyout = s.moneyout, playtime = s.playtime, ".
				" playcount = s.playcount, winrate = s.winrate, lostcoin = s.lostcoin, h_moneyin = s.h_moneyin, h_moneyout = s.h_moneyout, ".
				" h_playtime = s.h_playtime, h_playcount = s.h_playcount, h_winrate = s.h_winrate, h_lostcoin = s.h_lostcoin, treatamount = s.treatamount, redeem = s.redeem, ".
				" freecoin = s.freecoin, purchase_usd = s.purchase_usd, purchase_coin = s.purchase_coin, purchase_passed = s.purchase_passed, install_passed = s.install_passed , bet_rate = s.bet_rate ".				
				"FROM t5_user_playstat_gather_daily_tmp s ".
				"WHERE t5_user_playstat_gather_daily.statcode = s.statcode AND t5_user_playstat_gather_daily.today = s.today;";
		$db_redshift->execute($sql);

		// Insert records
		$sql = "INSERT INTO t5_user_playstat_gather_daily ".
				"SELECT s.* FROM t5_user_playstat_gather_daily_tmp s LEFT JOIN t5_user_playstat_gather_daily ".
				"ON s.statcode = t5_user_playstat_gather_daily.statcode AND s.today = t5_user_playstat_gather_daily.today ".
				"WHERE t5_user_playstat_gather_daily.statcode IS NULL AND t5_user_playstat_gather_daily.today IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_user_playstat_gather_daily_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	// Ios
	try
	{
		$db_other = new CDatabase_Other();
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_playstat_gather_daily_ios'";
		$columns_count = $db_other->getvalue($sql);
	
		$sql = "SELECT * FROM tbl_user_playstat_gather_daily_ios WHERE today = '$yesterday'";
		$log_list = $db_other->gettotallist($sql);
			
		for($i=0; $i<sizeof($log_list); $i++)
		{
			$output = "";
	
			for($j=0; $j<$columns_count; $j++)
			{
				$log_list[$i][$j] = str_replace("\"", "\"\"", $log_list[$i][$j]);
				
				if($log_list[$i][$j] == "0000-00-00 00:00:00")
					$log_list[$i][$j] = "1900-01-01 00:00:00";
		
				if($j == $columns_count - 1)
					$output .= '"'.$log_list[$i][$j].'"';
				else
					$output .= '"'.$log_list[$i][$j].'"|';
			}
		
			$output .="\n";
											
			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_playstat_gather_daily_ios_".$yesterday_str.".txt", 'a+');
													
				fwrite($fp, $output);
												
				fclose($fp);
			}
		}		
		
		$db_other->end();	
	}
	catch(Exception $e)
		{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_playstat_gather_daily_ios/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_playstat_gather_daily_ios_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_playstat_gather_daily_ios_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_playstat_gather_daily_ios_tmp (LIKE t5_user_playstat_gather_daily_ios);";
		$db_redshift->execute($sql);
	
		// Load data into the staging table
		$sql = "copy t5_user_playstat_gather_daily_ios_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_playstat_gather_daily_ios/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
	
		// Update records
		$sql = "UPDATE t5_user_playstat_gather_daily_ios ".
				"SET usercount = s.usercount, currentcoin = s.currentcoin, moneyin = s.moneyin, moneyout = s.moneyout, playtime = s.playtime, ".
				" playcount = s.playcount, winrate = s.winrate, lostcoin = s.lostcoin, h_moneyin = s.h_moneyin, h_moneyout = s.h_moneyout, ".
				" h_playtime = s.h_playtime, h_playcount = s.h_playcount, h_winrate = s.h_winrate, h_lostcoin = s.h_lostcoin, treatamount = s.treatamount, redeem = s.redeem, ".
				" freecoin = s.freecoin, purchase_usd = s.purchase_usd, purchase_coin = s.purchase_coin, purchase_passed = s.purchase_passed, install_passed = s.install_passed, bet_rate = s.bet_rate ".
				"FROM t5_user_playstat_gather_daily_ios_tmp s ".
				"WHERE t5_user_playstat_gather_daily_ios.statcode = s.statcode AND t5_user_playstat_gather_daily_ios.today = s.today;";
		$db_redshift->execute($sql);
	
		// Insert records
		$sql = "INSERT INTO t5_user_playstat_gather_daily_ios ".
				"SELECT s.* FROM t5_user_playstat_gather_daily_ios_tmp s LEFT JOIN t5_user_playstat_gather_daily_ios ".
				"ON s.statcode = t5_user_playstat_gather_daily_ios.statcode AND s.today = t5_user_playstat_gather_daily_ios.today ".
				"WHERE t5_user_playstat_gather_daily_ios.statcode IS NULL AND t5_user_playstat_gather_daily_ios.today IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_user_playstat_gather_daily_ios_tmp;";
		$db_redshift->execute($sql);
	
		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	// Android
	try
	{
		$db_other = new CDatabase_Other();
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_playstat_gather_daily_android'";
		$columns_count = $db_other->getvalue($sql);
	
		$sql = "SELECT * FROM tbl_user_playstat_gather_daily_android WHERE today = '$yesterday'";
		$log_list = $db_other->gettotallist($sql);
			
		for($i=0; $i<sizeof($log_list); $i++)
		{
			$output = "";
	
			for($j=0; $j<$columns_count; $j++)
			{
				$log_list[$i][$j] = str_replace("\"", "\"\"", $log_list[$i][$j]);
	
				if($log_list[$i][$j] == "0000-00-00 00:00:00")
					$log_list[$i][$j] = "1900-01-01 00:00:00";
	
				if($j == $columns_count - 1)
					$output .= '"'.$log_list[$i][$j].'"';
				else
					$output .= '"'.$log_list[$i][$j].'"|';
			}
	
			$output .="\n";
												
			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_playstat_gather_daily_android_".$yesterday_str.".txt", 'a+');
													
				fwrite($fp, $output);
	
				fclose($fp);
			}
		}
	
		$db_other->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_playstat_gather_daily_android/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_playstat_gather_daily_android_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_playstat_gather_daily_android_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_playstat_gather_daily_android_tmp (LIKE t5_user_playstat_gather_daily_android);";
		$db_redshift->execute($sql);
	
		// Load data into the staging table
		$sql = "copy t5_user_playstat_gather_daily_android_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_playstat_gather_daily_android/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
	
		// Update records
		$sql = "UPDATE t5_user_playstat_gather_daily_android ".
				"SET usercount = s.usercount, currentcoin = s.currentcoin, moneyin = s.moneyin, moneyout = s.moneyout, playtime = s.playtime, ".
				" playcount = s.playcount, winrate = s.winrate, lostcoin = s.lostcoin, h_moneyin = s.h_moneyin, h_moneyout = s.h_moneyout, ".
				" h_playtime = s.h_playtime, h_playcount = s.h_playcount, h_winrate = s.h_winrate, h_lostcoin = s.h_lostcoin, treatamount = s.treatamount, redeem = s.redeem, ".
				" freecoin = s.freecoin, purchase_usd = s.purchase_usd, purchase_coin = s.purchase_coin, purchase_passed = s.purchase_passed, install_passed = s.install_passed, bet_rate = s.bet_rate ".
				"FROM t5_user_playstat_gather_daily_android_tmp s ".
				"WHERE t5_user_playstat_gather_daily_android.statcode = s.statcode AND t5_user_playstat_gather_daily_android.today = s.today;";
		$db_redshift->execute($sql);
	
		// Insert records
		$sql = "INSERT INTO t5_user_playstat_gather_daily_android ".
				"SELECT s.* FROM t5_user_playstat_gather_daily_android_tmp s LEFT JOIN t5_user_playstat_gather_daily_android ".
				"ON s.statcode = t5_user_playstat_gather_daily_android.statcode AND s.today = t5_user_playstat_gather_daily_android.today ".
				"WHERE t5_user_playstat_gather_daily_android.statcode IS NULL AND t5_user_playstat_gather_daily_android.today IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_user_playstat_gather_daily_android_tmp;";
		$db_redshift->execute($sql);
	
		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	// Aamazon
	try
	{
		$db_other = new CDatabase_Other();
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_playstat_gather_daily_amazon'";
		$columns_count = $db_other->getvalue($sql);
	
		$sql = "SELECT * FROM tbl_user_playstat_gather_daily_amazon WHERE today = '$yesterday'";
		$log_list = $db_other->gettotallist($sql);
			
		for($i=0; $i<sizeof($log_list); $i++)
		{
			$output = "";
	
			for($j=0; $j<$columns_count; $j++)
			{
				$log_list[$i][$j] = str_replace("\"", "\"\"", $log_list[$i][$j]);
	
				if($log_list[$i][$j] == "0000-00-00 00:00:00")
					$log_list[$i][$j] = "1900-01-01 00:00:00";
	
					if($j == $columns_count - 1)
						$output .= '"'.$log_list[$i][$j].'"';
					else
						$output .= '"'.$log_list[$i][$j].'"|';
			}
	
			$output .="\n";
	
			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_playstat_gather_daily_amazon_".$yesterday_str.".txt", 'a+');
										
				fwrite($fp, $output);

				fclose($fp);
			}
		}
	
		$db_other->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_playstat_gather_daily_amazon/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_playstat_gather_daily_amazon_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
			'Bucket' 		=> $bucket,
			'Key'    		=> 'tbl_user_playstat_gather_daily_amazon_'.$yesterday_str.'.txt',
			'ACL'	 		=> 'public-read',
			'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];

		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_playstat_gather_daily_amazon_tmp (LIKE t5_user_playstat_gather_daily_amazon);";
		$db_redshift->execute($sql);
	
		// Load data into the staging table
		$sql = "copy t5_user_playstat_gather_daily_amazon_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_playstat_gather_daily_amazon/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
	
		// Update records
		$sql = "UPDATE t5_user_playstat_gather_daily_amazon ".
				"SET usercount = s.usercount, currentcoin = s.currentcoin, moneyin = s.moneyin, moneyout = s.moneyout, playtime = s.playtime, ".
				" playcount = s.playcount, winrate = s.winrate, lostcoin = s.lostcoin, h_moneyin = s.h_moneyin, h_moneyout = s.h_moneyout, ".
				" h_playtime = s.h_playtime, h_playcount = s.h_playcount, h_winrate = s.h_winrate, h_lostcoin = s.h_lostcoin, treatamount = s.treatamount, redeem = s.redeem, ".
				" freecoin = s.freecoin, purchase_usd = s.purchase_usd, purchase_coin = s.purchase_coin, purchase_passed = s.purchase_passed, install_passed = s.install_passed, bet_rate = s.bet_rate ".
				"FROM t5_user_playstat_gather_daily_amazon_tmp s ".
				"WHERE t5_user_playstat_gather_daily_amazon.statcode = s.statcode AND t5_user_playstat_gather_daily_amazon.today = s.today;";
		$db_redshift->execute($sql);
	
		// Insert records
		$sql = "INSERT INTO t5_user_playstat_gather_daily_amazon ".
				"SELECT s.* FROM t5_user_playstat_gather_daily_amazon_tmp s LEFT JOIN t5_user_playstat_gather_daily_amazon ".
				"ON s.statcode = t5_user_playstat_gather_daily_amazon.statcode AND s.today = t5_user_playstat_gather_daily_amazon.today ".
				"WHERE t5_user_playstat_gather_daily_amazon.statcode IS NULL AND t5_user_playstat_gather_daily_amazon.today IS NULL;";
		$db_redshift->execute($sql);
										
		// Drop the staging table
		$sql = "DROP TABLE t5_user_playstat_gather_daily_amazon_tmp;";
		$db_redshift->execute($sql);
	
		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

?>