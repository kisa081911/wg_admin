<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/other/payer_stat_daily_backup") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/other/payer_stat_daily_backup") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("backup/payer_stat_daily_backup Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$today = date("Y-m-d");
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
  

	try
	{
		write_log("redshift_payer_stat_daily start : ".date("Y-m-d H:i:s"));
		
		$db_other = new CDatabase_Other();
		$db_other->execute("SET wait_timeout=3600");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_payer_stat_daily'";
		$columns_count = $db_other->getvalue($sql);
		
		for($type=1;$type < 4; $type++)
		{
			$sql = " SELECT COUNT(*) FROM tbl_payer_stat_daily WHERE '$yesterday 00:00:00' <= today AND today < '$today 00:00:00' AND `type` = $type ";
			$totalcount = $db_other->getvalue($sql);
			
			$s_index = 0;
			
			while($totalcount > 0)
			{
				$sql = " SELECT * ".
						"	FROM tbl_payer_stat_daily ".
						"	WHERE '$yesterday 00:00:00' <= today AND today < '$today 00:00:00' ".
						"	AND `type` = $type ".
						"	AND useridx > $s_index ".
						"	ORDER BY useridx ASC LIMIT 10000 ";
			
				$user_list = $db_other->gettotallist($sql);
					
				for($i=0; $i<sizeof($user_list); $i++)
				{
				  $output = "";
				  
				  for($j=0; $j<$columns_count; $j++)
					{
						$user_list[$i][$j] = str_replace("\"", "\"\"", $user_list[$i][$j]);
						
						if($user_list[$i][$j] == "0000-00-00 00:00:00")
							$user_list[$i][$j] = "1900-01-01 00:00:00";
							
						if($j == $columns_count - 1)
							$output .= '"'.$user_list[$i][$j].'"';
						else
							$output .= '"'.$user_list[$i][$j].'"|';
					}
					
					$output .="\n";
					
					if($output != "")
					{
						$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_payer_stat_daily_$yesterday_str.txt", 'a+');
						
						fwrite($fp, $output);
						
						fclose($fp);
					}
					
					if(sizeof($user_list)-1 == $i)
					{
						$s_index = 	$user_list[$i]["useridx"];
						$totalcount = $totalcount - 10000;
					}
				}
			}
			
		}
		$db_other->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	 
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_payer_stat_daily/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_payer_stat_daily_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_payer_stat_daily_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		//$sql = "CREATE TABLE duc_payer_stat_daily_tmp (LIKE duc_payer_stat_daily);";
		//$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy t5_payer_stat_daily ".
				"from 's3://wg-redshift/t5/tbl_payer_stat_daily/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
		// Update records
		/*$sql = "UPDATE duc_payer_stat_daily ". 
				"SET pa = s.pa, freq = s.freq, threshold = s.threshold, ".
				"groupcode = s.groupcode, coin = s.coin, createdate = s.createdate, logindate = s.logindate ".
				"FROM duc_payer_stat_daily_tmp s ".
				"WHERE duc_payer_stat_daily.today = s.today ".
				"AND duc_payer_stat_daily.useridx = s.useridx ".
				"AND duc_payer_stat_daily.type = s.type ; ";
		$db_redshift->execute($sql); 

		// Insert records
		$sql = "INSERT INTO duc_payer_stat_daily ".
				"SELECT s.* FROM duc_payer_stat_daily_tmp s LEFT JOIN duc_payer_stat_daily ".
				"ON s.today = duc_payer_stat_daily.today ".
				"AND s.type = duc_payer_stat_daily.type ".
				"AND s.useridx = duc_payer_stat_daily.useridx ;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE duc_payer_stat_daily_tmp;";
		$db_redshift->execute($sql);*/

		$db_redshift->end();
		
		write_log("redshift_payer_stat_daily end : ".date("Y-m-d H:i:s"));
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	
?>