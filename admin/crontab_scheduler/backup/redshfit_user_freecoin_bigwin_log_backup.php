<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	$str_useridx = 20000;
	$today = date("Y-m-d");
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
  
  try
	{
		$db_main2 = new CDatabase_Main2();
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
		
			$output = "";
			$sql = "SELECT @RN:=@RN+1 AS logidx , t1.* ".
					" FROM( ".
					" SELECT useridx, os_type AS platform , 95 AS category , writedate, ROUND(amount/2) AS amount ". 
					" FROM tbl_bigwin_event_win_user ".
					"  WHERE useridx > 20000 ) t1 ,(SELECT @RN:=0) AS R";
			$log_list = $db_main2->gettotallist($sql);
			
			for($i=0; $i<sizeof($log_list); $i++)
			{		
				if($log_list[$i]["writedate"] == "0000-00-00 00:00:00")
					$log_list[$i]["writedate"] = "1900-01-01 00:00:00";
				
				$output .= '"'.$log_list[$i]["logidx"].'"|';
				$output .= '"'.$log_list[$i]["useridx"].'"|';
				$output .= '"'.$log_list[$i]["platform"].'"|';				
				$output .= '"'.$log_list[$i]["category"].'"|';				
				$output .= '"'.$log_list[$i]["writedate"].'"|';
				$output .= '"'.$log_list[$i]["amount"].'"|';
				
				$output .="\n";
				
			}
			
			if($output != "")
			{
			  $fp = fopen("$DOCUMENT_ROOT/redshift_temp/t5_freecoin_log_".$yesterday_str.".txt", 'a+');
			  
			  fwrite($fp, $output);
			  
			  fclose($fp);
			}
		
		$db_main2->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	 
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/t5_freecoin_log/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/t5_freecoin_log_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 't5_freecoin_log_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_freecoin_log_tmp (LIKE t5_freecoin_log);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy t5_freecoin_log_tmp ".
				"from 's3://wg-redshift/t5/t5_freecoin_log/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
		// Insert records
		$sql = "INSERT INTO t5_freecoin_log (useridx,platform,amount,category,writedate) ". 
				" (SELECT s.useridx,  s.platform, s.amount, s.category,  s.writedate FROM t5_freecoin_log_tmp s );";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_freecoin_log_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
		
?>