<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/main2/redshift_event_result_backup") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/main2/redshift_event_result_backup") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("backup/redshift_event_result_backup Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$today = date("Y-m-d");
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);

	try
	{
		$db_main2 = new CDatabase_Slave_Main2();
		
		$db_main2->execute("SET wait_timeout=3600");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];

		$sql = "SELECT * FROM tbl_event WHERE writedate >= '$yesterday 00:00:00' AND writedate < '$today 00:00:00'";
		$result_list = $db_main2->gettotallist($sql);
		
		$db_main2->end();
		
		$output = "";
		
		for($i=0; $i<sizeof($result_list); $i++)
		{		
			if($result_list[$i]["writedate"] == "0000-00-00 00:00:00")
				$result_list[$i]["writedate"] = "1900-01-01 00:00:00";
			
			if($result_list[$i]["reservation_date"] == "0000-00-00 00:00:00")
				$result_list[$i]["reservation_date"] = "1900-01-01 00:00:00";
			
			if($result_list[$i]["reservation_fanpage_date"] == "0000-00-00 00:00:00")
				$result_list[$i]["reservation_fanpage_date"] = "1900-01-01 00:00:00";
			
				$output .= '"'.$result_list[$i]["eventidx"].'"|';
				$output .= '"'.$result_list[$i]["category"].'"|';				
				$output .= '"'.$result_list[$i]["reward_type"].'"|';				
				$output .= '"'.$result_list[$i]["reward_amount"].'"|';				
				$output .= '"'.$result_list[$i]["reward_amount_mobile"].'"|';				
				$output .= '"'.$result_list[$i]["limit_join"].'"|';				
				$output .= '"'.$result_list[$i]["limit_total"].'"|';				
				$output .= '"'.$result_list[$i]["start_eventdate"].'"|';				
				$output .= '"'.$result_list[$i]["end_eventdate"].'"|';				
				$output .= '"'.$result_list[$i]["event_imagepath"].'"|';				
				$output .= '"'.$result_list[$i]["eventcode"].'"|';				
				$output .= '"'.$result_list[$i]["slotidx"].'"|';				
				$output .= '"'.$result_list[$i]["shorturl"].'"|';	
				$output .= '"'.$result_list[$i]["fbid"].'"|';
				$output .= '"'.$result_list[$i]["reservation_fanpage_status"].'"|';
				$output .= '"'.$result_list[$i]["share_enabled"].'"|';
				$output .= '"'.$result_list[$i]["writedate"].'"|';
				
				$output .="\n";
				
		}
		
		if($output != "")
		{
			$fp = fopen("$DOCUMENT_ROOT/redshift_temp/t5_event_$yesterday_str.txt", 'a+');
			
			fwrite($fp, $output);
			
			fclose($fp);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$is_file_exist = file_exists("$DOCUMENT_ROOT/redshift_temp/t5_event_".$yesterday_str.".txt");
	
	if($is_file_exist)
	{
		try
		{
			// Create the AWS service builder, providing the path to the config file
			$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
		
			$s3Client = $aws->get('s3');
			$bucket = "wg-redshift/t5/t5_event/$yesterday_str";
		
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			$filepath = $DOCUMENT_ROOT."/redshift_temp/t5_event_$yesterday_str.txt";
		
			// Upload an object to Amazon S3
			$result = $s3Client->putObject(array(
					'Bucket' 		=> $bucket,
					'Key'    		=> 't5_event_'.$yesterday_str.'.txt',
					'ACL'	 		=> 'public-read',
					'SourceFile'   	=> $filepath
			));
		
			$ObjectURL = $result["ObjectURL"];
		
			if($ObjectURL != "")
			{
				@unlink($filepath);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		try
		{
			$db_redshift = new CDatabase_Redshift();
		
			// Create a staging table
			$sql = "CREATE TABLE t5_event_tmp (LIKE t5_event);";
			$db_redshift->execute($sql);
			
			// Load data into the staging table
			$sql = "copy t5_event_tmp ".
					"from 's3://wg-redshift/t5/t5_event/$yesterday_str' ".
					"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
					"delimiter '|' ".
					"csv;";
			$db_redshift->execute($sql);
	
			// Insert records
			$sql = "INSERT INTO t5_event ".
					"SELECT s.* FROM t5_event_tmp s LEFT JOIN t5_event ".
					"ON s.eventidx = t5_event.eventidx ".
					"WHERE t5_event.eventidx IS NULL;";
			$db_redshift->execute($sql);
				
			// Drop the staging table
			$sql = "DROP TABLE t5_event_tmp;";
			$db_redshift->execute($sql);
	
			$db_redshift->end();
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
	}
?>