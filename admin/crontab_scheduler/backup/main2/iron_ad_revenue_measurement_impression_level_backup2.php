<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/main2/iron_ad_revenue_measurement_impression_level_backup.php") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/main2/iron_ad_revenue_measurement_impression_level_backup.php") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("backup/iron_ad_revenue_measurement_impression_level_backup Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$today = date("Y-m-d");
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
	
	//Web
	try
	{
		$db_main2 = new CDatabase_Slave_Main2();
		$db_main2->execute("SET wait_timeout=3600");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_iron_ad_revenue_measurement_impression_level'";
		$columns_count = $db_main2->getvalue($sql);
		
		$columns_count = $columns_count;
		
		$sql = " SELECT logidx, platform, advertising_id, advertising_id_type, user_id, ad_unit, ad_network, instance_name, country, placement, ".
		  		"       segment, AB_Testing, revenue, event_timestamp, writedate ".
		  		" FROM tbl_iron_ad_revenue_measurement_impression_level";
		
		$user_list = $db_main2->gettotallist($sql);
		
		for($i=0; $i<sizeof($user_list); $i++)
		{
		    $output = "";
		    
		    for($j=0; $j<$columns_count; $j++)
			{
				$user_list[$i][$j] = str_replace("\"", "\"\"", $user_list[$i][$j]);
				
				if($user_list[$i][$j] == "0000-00-00 00:00:00")
					$user_list[$i][$j] = "1900-01-01 00:00:00";
					
				if($j == $columns_count - 1)
					$output .= '"'.$user_list[$i][$j].'"';
				else
					$output .= '"'.$user_list[$i][$j].'"|';
			}
			$output .="\n";
			
			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_iron_ad_revenue_measurement_impression_level_$yesterday_str.txt", 'a+');
					
				fwrite($fp, $output);
					
				fclose($fp);
			}
		}
		  
		$db_main2->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	 
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_iron_ad_revenue_measurement_impression_level/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_iron_ad_revenue_measurement_impression_level_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_iron_ad_revenue_measurement_impression_level_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_iron_ad_revenue_measurement_impression_level_tmp (LIKE t5_iron_ad_revenue_measurement_impression_level);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy t5_iron_ad_revenue_measurement_impression_level_tmp ".
				"from 's3://wg-redshift/t5/tbl_iron_ad_revenue_measurement_impression_level/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
		// Update records
		$sql = "UPDATE t5_iron_ad_revenue_measurement_impression_level ". 
				"SET platform = s.platform, advertising_id = s.advertising_id, advertising_id_type = s.advertising_id_type, user_id = s.user_id, ad_unit = s.ad_unit, ad_network = s.ad_network, instance_name = s.instance_name, ".
				"   country = s.country, placement = s.placement, segment = s.segment, AB_Testing = s.AB_Testing, revenue = s.revenue, event_timestamp = s.event_timestamp, writedate = s.writedate ".
				"FROM t5_iron_ad_revenue_measurement_impression_level_tmp s ".
				"WHERE t5_iron_ad_revenue_measurement_impression_level.logidx = s.logidx;";
		$db_redshift->execute($sql); 

		// Insert records
		$sql = "INSERT INTO t5_iron_ad_revenue_measurement_impression_level ".
				"SELECT s.* FROM t5_iron_ad_revenue_measurement_impression_level_tmp s LEFT JOIN t5_iron_ad_revenue_measurement_impression_level ".
				"ON s.logidx = t5_iron_ad_revenue_measurement_impression_level.logidx ".
				"WHERE t5_iron_ad_revenue_measurement_impression_level.logidx IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_iron_ad_revenue_measurement_impression_level_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
?>