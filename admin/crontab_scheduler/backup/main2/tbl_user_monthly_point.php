<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$yesterday = date("Ym", time());
	$yesterday_str = date("Ymd", time());

	write_log("tbl_user_monthly_point : $yesterday");

	try
	{
		$db_main2 = new CDatabase_Slave_Main2();
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];

		$output = "";
		$sql = "SELECT useridx, userid, nickname, monthly, honor_level, honor_point, fameball ".
				"FROM tbl_user_monthly_point WHERE monthly = '$yesterday'";
		$log_list = $db_main2->gettotallist($sql);
			
		for($i=0; $i<sizeof($log_list); $i++)
		{
			$output .= '"'.$log_list[$i]["useridx"].'"|';
			$output .= '"'.$log_list[$i]["userid"].'"|';
			$output .= '"'.str_replace("\"", "\"\"", $log_list[$i]["nickname"]).'"|';
			$output .= '"'.$log_list[$i]["monthly"].'"|';
			$output .= '"'.$log_list[$i]["honor_level"].'"|';
			$output .= '"'.$log_list[$i]["honor_point"].'"|';
			$output .= '"'.$log_list[$i]["fameball"].'"|';

			$output .="\n";

		}
			
		if($output != "")
		{
			$fp = fopen("$DOCUMENT_ROOT/redshift_temp/t5_user_monthly_point_".$yesterday_str.".txt", 'a+');
				
			fwrite($fp, $output);
				
			fclose($fp);
		}
			
		$db_main2->end();		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/t5_user_monthly_point/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/t5_user_monthly_point_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 't5_user_monthly_point_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_monthly_point_tmp (LIKE t5_user_monthly_point);";
		$db_redshift->execute($sql);
	
		// Load data into the staging table
		$sql = "copy t5_user_monthly_point_tmp ".
				"from 's3://wg-redshift/t5/t5_user_monthly_point/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
	
		// Update records 
		$sql = "UPDATE t5_user_monthly_point ".
				"SET userid = s.userid, nickname = s.nickname, honor_level = s.honor_level, honor_point = s.honor_point, fameball = s.fameball ".
				"FROM t5_user_monthly_point_tmp s ". 
				"WHERE t5_user_monthly_point.useridx = s.useridx AND t5_user_monthly_point.monthly = s.monthly;";
		$db_redshift->execute($sql);
		
		// Insert records
		$sql = "INSERT INTO t5_user_monthly_point ".
				"SELECT s.* FROM t5_user_monthly_point_tmp s LEFT JOIN t5_user_monthly_point ".
				"ON s.useridx = t5_user_monthly_point.useridx AND s.monthly = t5_user_monthly_point.monthly ".
				"WHERE t5_user_monthly_point.useridx IS NULL AND t5_user_monthly_point.monthly IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_user_monthly_point_tmp;";
		$db_redshift->execute($sql);
	
		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>