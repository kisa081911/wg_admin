<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	
	ini_set("memory_limit", "-1");
	
	$sdate = "2018-12-22";
	$edate = "2019-10-28";
	
	while($sdate <= $edate)
	{
		$today = date('Y-m-d', strtotime($sdate));
		$yesterday = date('Y-m-d', strtotime($today.' - 1 day'));
		$yesterday_str = date('Ymd', strtotime($today.' - 1 day'));
	
		write_log("event_rebackup : ".$yesterday);

		try
		{
			$db_main2 = new CDatabase_Main2();
			
			$db_main2->execute("SET wait_timeout=21600");
			
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
			$sql = "SELECT * FROM tbl_event WHERE writedate >= '$yesterday 00:00:00' AND writedate < '$today 00:00:00'";
			$result_list = $db_main2->gettotallist($sql);
			
			$db_main2->end();
			
			$output = "";
			
			for($i=0; $i<sizeof($result_list); $i++)
			{		
				if($result_list[$i]["writedate"] == "0000-00-00 00:00:00")
					$result_list[$i]["writedate"] = "1900-01-01 00:00:00";
				
				if($result_list[$i]["reservation_date"] == "0000-00-00 00:00:00")
					$result_list[$i]["reservation_date"] = "1900-01-01 00:00:00";
				
				if($result_list[$i]["reservation_fanpage_date"] == "0000-00-00 00:00:00")
					$result_list[$i]["reservation_fanpage_date"] = "1900-01-01 00:00:00";
				
					$output .= '"'.$result_list[$i]["eventidx"].'"|';
					$output .= '"'.$result_list[$i]["category"].'"|';				
					$output .= '"'.$result_list[$i]["reward_type"].'"|';				
					$output .= '"'.$result_list[$i]["reward_amount"].'"|';				
					$output .= '"'.$result_list[$i]["reward_amount_mobile"].'"|';				
					$output .= '"'.$result_list[$i]["limit_join"].'"|';				
					$output .= '"'.$result_list[$i]["limit_total"].'"|';				
					$output .= '"'.$result_list[$i]["start_eventdate"].'"|';				
					$output .= '"'.$result_list[$i]["end_eventdate"].'"|';				
					$output .= '"'.$result_list[$i]["event_imagepath"].'"|';				
					$output .= '"'.$result_list[$i]["eventcode"].'"|';				
					$output .= '"'.$result_list[$i]["slotidx"].'"|';				
					$output .= '"'.$result_list[$i]["shorturl"].'"|';	
					$output .= '"'.$result_list[$i]["fbid"].'"|';
					$output .= '"'.$result_list[$i]["reservation_fanpage_status"].'"|';
					$output .= '"'.$result_list[$i]["share_enabled"].'"|';
					$output .= '"'.$result_list[$i]["writedate"].'"|';
					
					$output .="\n";
					
			}
			
			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/t5_event_$yesterday_str.txt", 'a+');
				
				fwrite($fp, $output);
				
				fclose($fp);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		$is_file_exist = file_exists("$DOCUMENT_ROOT/redshift_temp/t5_event_".$yesterday_str.".txt");
		
		if($is_file_exist)
		{	
			try
			{
				// Create the AWS service builder, providing the path to the config file
				$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
			
				$s3Client = $aws->get('s3');
				$bucket = "wg-redshift/t5/t5_event/$yesterday_str";
			
				$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
				$filepath = $DOCUMENT_ROOT."/redshift_temp/t5_event_$yesterday_str.txt";
			
				// Upload an object to Amazon S3
				$result = $s3Client->putObject(array(
						'Bucket' 		=> $bucket,
						'Key'    		=> 't5_event_'.$yesterday_str.'.txt',
						'ACL'	 		=> 'public-read',
						'SourceFile'   	=> $filepath
				));
			
				$ObjectURL = $result["ObjectURL"];
			
				if($ObjectURL != "")
				{
					@unlink($filepath);
				}
			}
			catch(Exception $e)
			{
				write_log($e->getMessage());
			}
			
			try
			{
				$db_redshift = new CDatabase_Redshift();
			
				// Create a staging table
				$sql = "CREATE TABLE t5_event_tmp (LIKE t5_event);";
				$db_redshift->execute($sql);
				
				// Load data into the staging table
				$sql = "copy t5_event_tmp ".
						"from 's3://wg-redshift/t5/t5_event/$yesterday_str' ".
						"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
						"delimiter '|' ".
						"csv;";
				$db_redshift->execute($sql);
		
				// Insert records
				$sql = "INSERT INTO t5_event ".
						"SELECT s.* FROM t5_event_tmp s LEFT JOIN t5_event ".
						"ON s.eventidx = t5_event.eventidx ".
						"WHERE t5_event.eventidx IS NULL;";
				$db_redshift->execute($sql);
					
				// Drop the staging table
				$sql = "DROP TABLE t5_event_tmp;";
				$db_redshift->execute($sql);
		
				$db_redshift->end();
			}
			catch(Exception $e)
			{
				write_log($e->getMessage());
			}
		}
			
		$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));
	}
?>