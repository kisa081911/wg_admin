<?
    include("../../../common/common_include.inc.php");
    include("../../../common/dbconnect/db_util_redshift.inc.php");
    require '../../../common/aws_sdk/vendor/autoload.php';
    
    use Aws\Common\Aws;
    use Aws\S3\S3Client;
    
    ini_set("memory_limit", "-1");
    $str_useridx = 20000;
    
    $today = date("Y-m-d", time() - 60 * 60 * 24 * 1);
    $yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 2);
    $yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 2);
    
    try
    {
        $db_main2 = new CDatabase_Slave_Main2();
        
        $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
        
        $output = "";
        $sql = "SELECT * FROM `tbl_user_first_order` WHERE purchase_date >= '2019-07-29'";
        $order_list = $db_main2->gettotallist($sql);
        
        for($i=0; $i<sizeof($order_list); $i++)
        {
            
            $output .= '"'.$order_list[$i]["useridx"].'"|';
            $output .= '"'.$order_list[$i]["platform"].'"|';
            $output .= '"'.$order_list[$i]["join_date"].'"|';
            $output .= '"'.$order_list[$i]["purchase_date"].'"|';
            $output .="\n";
            
        }
        
        if($output != "")
        {
            $fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_first_order_".$yesterday_str.".txt", 'a+');
            
            fwrite($fp, $output);
            
            fclose($fp);
        }
        
        $db_main2->end();
        
    }
    catch(Exception $e)
    {
        write_log($e->getMessage());
    }
    
    try
    {
        // Create the AWS service builder, providing the path to the config file
        $aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
        
        $s3Client = $aws->get('s3');
        $bucket = "wg-redshift/t5/tbl_user_first_order/$yesterday_str";
        
        $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
        $filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_first_order_$yesterday_str.txt";
        
        // Upload an object to Amazon S3
        $result = $s3Client->putObject(array(
            'Bucket' 		=> $bucket,
            'Key'    		=> 'tbl_user_first_order_'.$yesterday_str.'.txt',
            'ACL'	 		=> 'public-read',
            'SourceFile'   	=> $filepath
        ));
        
        $ObjectURL = $result["ObjectURL"];
        
        if($ObjectURL != "")
        {
            @unlink($filepath);
        }
    }
    catch(Exception $e)
    {
        write_log($e->getMessage());
    }
    
    try
    {
        $db_redshift = new CDatabase_Redshift();
        
        // Create a staging table
        $sql = "CREATE TABLE t5_user_first_order_tmp (LIKE t5_user_first_order);";
        $db_redshift->execute($sql);
        
        // Load data into the staging table
        $sql = "copy t5_user_first_order_tmp ".
            "from 's3://wg-redshift/t5/tbl_user_first_order/$yesterday_str' ".
            "credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
            "delimiter '|' ".
            "csv;";
        $db_redshift->execute($sql);
        
        // Insert records
        $sql = "INSERT INTO t5_user_first_order (useridx,platform,join_date,purchase_date) ".
            " (SELECT s.useridx,s.platform,s.join_date,s.purchase_date FROM t5_user_first_order_tmp s );";
        $db_redshift->execute($sql);
        
        // Drop the staging table
        $sql = "DROP TABLE t5_user_first_order_tmp;";
        $db_redshift->execute($sql);
        
        $db_redshift->end();
    }
    catch(Exception $e)
    {
        write_log($e->getMessage());
    }
?>