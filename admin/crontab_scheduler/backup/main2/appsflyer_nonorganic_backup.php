<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/main2/appsflyer_nonorganic_backup") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/main2/appsflyer_nonorganic_backup") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("backup/appsflyer_nonorganic_backup Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	
	$today = date("Y-m-d");
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
	
	// tbl_appsflyer_install
	try
	{
		$db_main2 = new CDatabase_Slave_Main2();
		$db_main2->execute("SET wait_timeout=3600");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_appsflyer_install'";
		$columns_count = $db_main2->getvalue($sql);
		
		$columns_count = $columns_count;
	
		$sql = " SELECT logidx, platform, fb_ad_id, install_time, agency, media_source, fb_campaign_id, fb_campaign_name, fb_adset_id, fb_adset_name, fb_adgroup_name, ".
				"		campaign, channel, ".
				"		IF(LENGTH(keyword) <= 50, keyword, SUBSTRING(keyword, 1, CHAR_LENGTH(keyword) / 2)) as keyword, ".
				"		IF(LENGTH(site_id) <= 255, site_id, SUBSTRING(site_id, 1, CHAR_LENGTH(site_id) / 2)) as site_id, ".
				"		sub1, sub2, sub3, sub4, sub5, country_code, city, languege, appsflyer_device_id, adset_id, adset, ".
				"		ad_id, ad, adv_id, device_id, device_type, is_retargeting, install_useridx, retention_logidx ".
				"	FROM tbl_appsflyer_install".
				"	WHERE install_time >= '$yesterday 00:00:00' ".
				"	AND install_time <= '$yesterday 23:59:59' ";
		
		$user_list = $db_main2->gettotallist($sql);
		
		for($i=0; $i<sizeof($user_list); $i++)
		{
		  $output = ""; 
		  
		  for($j=0; $j<$columns_count; $j++)
			{
				$user_list[$i][$j] = str_replace("\"", "\"\"", $user_list[$i][$j]);
				
				if($user_list[$i][$j] == "0000-00-00 00:00:00")
					$user_list[$i][$j] = "1900-01-01 00:00:00";
					
				if($j == $columns_count - 1)
					$output .= '"'.$user_list[$i][$j].'"';
				else
					$output .= '"'.$user_list[$i][$j].'"|';
			}
			$output .="\n";
			
			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_appsflyer_install_$yesterday_str.txt", 'a+');
					
				fwrite($fp, $output);
					
				fclose($fp);
			}
	  }
		  
		
		$db_main2->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	 
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_appsflyer_install/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_appsflyer_install_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_appsflyer_install_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_appsflyer_install_tmp (LIKE t5_appsflyer_install);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy t5_appsflyer_install_tmp ".
				"from 's3://wg-redshift/t5/tbl_appsflyer_install/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
		// Update records
		$sql = "UPDATE t5_appsflyer_install ". 
				"SET platform = s.platform,  fb_ad_id = s.fb_ad_id, install_time = s.install_time, agency = s.agency, media_source = s.media_source, fb_campaign_id = s.fb_campaign_id, ".
				"	fb_campaign_name = s.fb_campaign_name, fb_adset_id = s.fb_adset_id, fb_adset_name = s.fb_adset_name, fb_adgroup_name = s.fb_adgroup_name, campaign = s.campaign, channel = s.channel, site_id = s.site_id, ".
				"	sub1 = s.sub1, sub2 = s.sub2, sub3 = s.sub3, sub4 = s.sub4, sub5 = s.sub5, country_code = s.country_code, city = s.city, languege = s.languege, appsflyer_device_id = s.appsflyer_device_id, adset_id = s.adset_id, ".
				"	adset = s.adset, ad_id = s.ad_id, ad = s.ad, adv_id = s.adv_id, device_id = s.device_id, device_type = s.device_type, is_retargeting = s.is_retargeting, install_useridx = s.install_useridx, retention_logidx = s.retention_logidx ".
				"FROM t5_appsflyer_install_tmp s ".
				"WHERE t5_appsflyer_install.logidx = s.logidx;";
		$db_redshift->execute($sql); 

		// Insert records
		$sql = "INSERT INTO t5_appsflyer_install ".
				"SELECT s.* FROM t5_appsflyer_install_tmp s LEFT JOIN t5_appsflyer_install ".
				"ON s.logidx = t5_appsflyer_install.logidx ".
				"WHERE t5_appsflyer_install.logidx IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_appsflyer_install_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	//tbl_appsflyer_inappevent
	try
	{
		$db_main2 = new CDatabase_Slave_Main2();
		$db_main2->execute("SET wait_timeout=3600");
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_appsflyer_inappevent'";
		$columns_count = $db_main2->getvalue($sql);
	
		$sql = 	" SELECT logidx, platform, useridx, install_time, event_name, event_time, event_value, agency, media_source, fb_campaign_id, fb_campaign_name, fb_adset_id, ".
				" 		fb_adset_name, fb_adgroup_name, campaign, channel,keyword, site_id, sub1, sub2, sub3, sub4, sub5, country_code, city, languege, appsflyer_device_id, adset_id,  ".
				"		adset, ad_id, ad, device_id, device_type, is_retargeting, adv_id ".
				"	FROM tbl_appsflyer_inappevent ".
				"	WHERE event_time >= '$yesterday 00:00:00' ".
				"	AND event_time <= '$yesterday 23:59:59' ";
					
		$user_list = $db_main2->gettotallist($sql);
				
		for($i=0; $i<sizeof($user_list); $i++)
		{
			$output = "";
					
			for($j=0; $j<$columns_count; $j++)
			{
				$user_list[$i][$j] = str_replace("\"", "\"\"", $user_list[$i][$j]);
					
				if($user_list[$i][$j] == "0000-00-00 00:00:00")
					$user_list[$i][$j] = "1900-01-01 00:00:00";

				if($j == $columns_count - 1)
					$output .= '"'.$user_list[$i][$j].'"';
				else
					$output .= '"'.$user_list[$i][$j].'"|';
			}
			$output .="\n";

			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_appsflyer_inappevent_$yesterday_str.txt", 'a+');

				fwrite($fp, $output);

				fclose($fp);
			}
		}
	
		$db_main2->end();
	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_appsflyer_inappevent/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_appsflyer_inappevent_$yesterday_str.txt";
	
									// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_appsflyer_inappevent_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_appsflyer_inappevent_tmp (LIKE t5_appsflyer_inappevent);";
		$db_redshift->execute($sql);
	
		// Load data into the staging table
		$sql = "copy t5_appsflyer_inappevent_tmp ".
				"from 's3://wg-redshift/t5/tbl_appsflyer_inappevent/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
			$db_redshift->execute($sql);
	
		// Update records
		$sql = "UPDATE t5_appsflyer_inappevent ".
				"SET platform = s.platform, useridx = s.useridx, install_time = s.install_time, event_name = s.event_name, event_time = s.event_time, event_value = s.event_value, agency = s.agency, ".
				"	media_source = s.media_source, fb_campaign_id = s.fb_campaign_id, fb_campaign_name = s.fb_campaign_name, fb_adset_id = s.fb_adset_id, fb_adset_name = s.fb_adset_name, fb_adgroup_name = s.fb_adgroup_name, campaign = s.campaign,  ".
				"	channel = s.channel, keyword = s.keyword, site_id = s.site_id, sub1 = s.sub1, sub2 = s.sub2, sub3 = s.sub3, sub4 = s.sub4, sub5 = s.sub5, country_code = s.country_code, city = s.city, languege = s.languege,  ".
				"	appsflyer_device_id = s.appsflyer_device_id, adset_id = s.adset_id, adset = s.adset, ad_id = s.ad_id, ad = s.ad, device_id = s.device_id, device_type = s.device_type, is_retargeting = s.is_retargeting, adv_id=s.adv_id ".
				"FROM t5_appsflyer_inappevent_tmp s ".
				"WHERE t5_appsflyer_inappevent.logidx = s.logidx;";
		$db_redshift->execute($sql);
	
		// Insert records
		$sql = "INSERT INTO t5_appsflyer_inappevent ".
				"SELECT s.* FROM t5_appsflyer_inappevent_tmp s LEFT JOIN t5_appsflyer_inappevent ".
				"ON s.logidx = t5_appsflyer_inappevent.logidx ".
				"WHERE t5_appsflyer_inappevent.logidx IS NULL;";
		$db_redshift->execute($sql);
		
		// Drop the staging table
		$sql = "DROP TABLE t5_appsflyer_inappevent_tmp;";
				$db_redshift->execute($sql);
	
		$db_redshift->end();
	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
?>