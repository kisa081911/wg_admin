<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
// 	$sdate = "2018-01-20";
// 	$edate = "2018-03-06";
	
// 	while($sdate <= $edate)
// 	{

// 		$today = date('Y-m-d', strtotime($sdate.' + 1 day'));
// 		$sdate = $today;
// 		write_log($today);
// 		$yesterday = date('Y-m-d', strtotime($today.' - 1 day'));
// 		$yesterday_str = date('Ymd', strtotime($today.' - 1 day'));

		$today = date("Y-m-d");
		$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
		$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
		
		try
		{
			$db_main2 = new CDatabase_Slave_Main2();
			
			$db_main2->execute("SET wait_timeout=3600");
			
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			
			$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_target_user_history_new'";
			$columns_count = $db_main2->getvalue($sql);
			
			for($u=0; $u<10; $u++)
			{
				$sql = " SELECT * ".
						"	FROM (SELECT logidx, useridx, deduct_rate, isbuyer, writedate FROM tbl_target_user_history_new ".
						"	WHERE '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00' ".
						"	) t1 ".
						"	WHERE t1.useridx % 10 = $u ";
				
				$user_list = $db_main2->gettotallist($sql);
				
				for($i=0; $i<sizeof($user_list); $i++)
				{
				  $output = "";
				  
				  for($j=0; $j<$columns_count; $j++)
					{
						$user_list[$i][$j] = str_replace("\"", "\"\"", $user_list[$i][$j]);
						
						if($user_list[$i][$j] == "0000-00-00 00:00:00")
							$user_list[$i][$j] = "1900-01-01 00:00:00";
							
						if($j == $columns_count - 1)
							$output .= '"'.$user_list[$i][$j].'"';
						else
							$output .= '"'.$user_list[$i][$j].'"|';
					}
					$output .="\n";
					
					if($output != "")
					{
						$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_target_user_history_new_".$yesterday_str.".txt", 'a+');
						fwrite($fp, $output);
							
						fclose($fp);
					}
			  }
			  
			}
			
			$db_main2->end();		
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		try
		{
			// Create the AWS service builder, providing the path to the config file
			$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
		
			$s3Client = $aws->get('s3');
			$bucket = "wg-redshift/t5/tbl_target_user_history_new/$yesterday_str";
		
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_target_user_history_new_$yesterday_str.txt";
		
			// Upload an object to Amazon S3
			$result = $s3Client->putObject(array(
					'Bucket' 		=> $bucket,
					'Key'    		=> 'tbl_target_user_history_new_'.$yesterday_str.'.txt',
					'ACL'	 		=> 'public-read',
					'SourceFile'   	=> $filepath
			));
		
			$ObjectURL = $result["ObjectURL"];
		
			if($ObjectURL != "")
			{
				@unlink($filepath);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		try
		{
			$db_redshift = new CDatabase_Redshift();
		
			// Create a staging table
			$sql = "CREATE TABLE t5_target_user_history_new_tmp (LIKE t5_target_user_history_new);";
			$db_redshift->execute($sql);
		
			// Load data into the staging table
			$sql = "copy t5_target_user_history_new_tmp ".
					"from 's3://wg-redshift/t5/tbl_target_user_history_new/$yesterday_str' ".
					"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
					"delimiter '|' ".
					"csv;";
			$db_redshift->execute($sql);
		
			// Update records
			$sql = "UPDATE t5_target_user_history_new ".
					"SET useridx = s.useridx, deduct_rate = s.deduct_rate, isbuyer = s.isbuyer, writedate = s.writedate ".
					"FROM t5_target_user_history_new_tmp s ".
					"WHERE t5_target_user_history_new.logidx = s.logidx;";
			$db_redshift->execute($sql);
		
			// Insert records
			$sql = "INSERT INTO t5_target_user_history_new ".
					"SELECT s.* FROM t5_target_user_history_new_tmp s LEFT JOIN t5_target_user_history_new ".
					"ON s.logidx = t5_target_user_history_new.logidx ".
					"WHERE t5_target_user_history_new.logidx IS NULL;";
			$db_redshift->execute($sql);
				
			// Drop the staging table
			$sql = "DROP TABLE t5_target_user_history_new_tmp;";
			$db_redshift->execute($sql);
		
			$db_redshift->end();
		
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
	//}
	
?>	