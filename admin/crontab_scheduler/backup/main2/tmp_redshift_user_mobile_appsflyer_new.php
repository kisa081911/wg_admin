<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");

	$sdate = "2020-08-27";
	$edate = "2020-08-27";
	
	while($sdate <= $edate)
	{
		$today = date('Y-m-d', strtotime($sdate));
		$yesterday = date('Y-m-d', strtotime($today.' - 1 day'));
		$yesterday_str = date('Ymd', strtotime($today.' - 1 day'))."_back";
		
		write_log("tbl_user_mobile_appsflyer_new_rebackup : ".$yesterday);

		try
		{
			$db_slave_main2 = new CDatabase_Slave_Main2();
			
			$db_slave_main2->execute("SET wait_timeout=7200");
			
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			
			$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_mobile_appsflyer_new'";
			$columns_count = $db_slave_main2->getvalue($sql);
			
			$sql = " SELECT useridx, appsflyerid, os_type, deviceid, advid, writedate ".
					"	FROM tbl_user_mobile_appsflyer_new WHERE '$yesterday 00:00:00' <= writedate AND writedate < '$today 08:00:00';";		
			$user_mobile_appsflye_list = $db_slave_main2->gettotallist($sql);
			
			for($i=0; $i<sizeof($user_mobile_appsflye_list); $i++)
			{
			  $output = ""; 
			  
			  for($j=0; $j<$columns_count; $j++)
				{
					$user_mobile_appsflye_list[$i][$j] = str_replace("\"", "\"\"", $user_mobile_appsflye_list[$i][$j]);
					
					if($user_mobile_appsflye_list[$i][$j] == "0000-00-00 00:00:00")
						$user_mobile_appsflye_list[$i][$j] = "1900-01-01 00:00:00";
						
					if($j == $columns_count - 1)
						$output .= '"'.$user_mobile_appsflye_list[$i][$j].'"';
					else
						$output .= '"'.$user_mobile_appsflye_list[$i][$j].'"|';
				}
				$output .="\n";
				
				if($output != "")
				{
					$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_mobile_appsflyer_new_$yesterday_str.txt", 'a+');
						
					fwrite($fp, $output);
						
					fclose($fp);
				}
		  }
			  
			
			$db_slave_main2->end();
			
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		$is_file_exist = file_exists("$DOCUMENT_ROOT/redshift_temp/tbl_user_mobile_appsflyer_new_".$yesterday_str.".txt");
		
		if($is_file_exist)
		{
			try
			{
				// Create the AWS service builder, providing the path to the config file
				$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
			
				$s3Client = $aws->get('s3');
				$bucket = "wg-redshift/t5/tbl_user_mobile_appsflyer_new/$yesterday_str";
			
				$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
				$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_mobile_appsflyer_new_$yesterday_str.txt";
			
				// Upload an object to Amazon S3
				$result = $s3Client->putObject(array(
						'Bucket' 		=> $bucket,
						'Key'    		=> 'tbl_user_mobile_appsflyer_new_'.$yesterday_str.'.txt',
						'ACL'	 		=> 'public-read',
						'SourceFile'   	=> $filepath
				));
			
				$ObjectURL = $result["ObjectURL"];
			
				if($ObjectURL != "")
				{
					@unlink($filepath);
				}
			}
			catch(Exception $e)
			{
				write_log($e->getMessage());
			}
			
			try
			{
				$db_redshift = new CDatabase_Redshift();
			
				// Create a staging table
				$sql = "CREATE TABLE t5_user_mobile_appsflyer_new_temp (LIKE t5_user_mobile_appsflyer_new);";
				$db_redshift->execute($sql);
			
				// Load data into the staging table
				$sql = "copy t5_user_mobile_appsflyer_new_temp ".
						"from 's3://wg-redshift/t5/tbl_user_mobile_appsflyer_new/$yesterday_str' ".
						"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
						"delimiter '|' ".
						"csv;";
				$db_redshift->execute($sql);
			
				// Update records
				$sql = "UPDATE t5_user_mobile_appsflyer_new ".
						"SET os_type = s.os_type, deviceid = s.deviceid, advid = s.advid, writedate = s.writedate ".
						"FROM t5_user_mobile_appsflyer_new_temp s ".
						"WHERE t5_user_mobile_appsflyer_new.useridx = s.useridx AND t5_user_mobile_appsflyer_new.appsflyerid = s.appsflyerid;";
				$db_redshift->execute($sql);
			
				// Insert records
				$sql = "INSERT INTO t5_user_mobile_appsflyer_new ".
						"SELECT s.* FROM t5_user_mobile_appsflyer_new_temp s LEFT JOIN t5_user_mobile_appsflyer_new ".
						"ON s.useridx = t5_user_mobile_appsflyer_new.useridx AND s.appsflyerid = t5_user_mobile_appsflyer_new.appsflyerid ".
						"WHERE t5_user_mobile_appsflyer_new.useridx IS NULL AND t5_user_mobile_appsflyer_new.appsflyerid IS NULL;";
				$db_redshift->execute($sql);
					
				// Drop the staging table
				$sql = "DROP TABLE t5_user_mobile_appsflyer_new_temp;";
				$db_redshift->execute($sql);
			
				$db_redshift->end();
			
			}
			catch(Exception $e)
			{
				write_log($e->getMessage());
			}
		}
		
		$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));
	}
?>