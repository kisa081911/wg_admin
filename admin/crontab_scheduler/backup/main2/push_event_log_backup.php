<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);

		
	try
	{
	    $db_main2 = new CDatabase_Slave_Main2();
	    
	    $db_main2->execute("SET wait_timeout=7200");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$sql = "SELECT * FROM `tbl_push_event_log` WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$yesterday'";
		
		$log_list = $db_main2->gettotallist($sql);
		
		for($i=0; $i<sizeof($log_list); $i++)
		{
			$output = "";
				
			$useridx = $log_list[$i]["useridx"];
		
			$output .= '"'.$useridx.'"|';
			$output .= '"'.$log_list[$i]["pushcode"].'"|';
			$output .= '"'.$log_list[$i]["os_type"].'"|';
			$output .= '"'.$log_list[$i]["count"].'"|';
			$output .= '"'.$log_list[$i]["is_return"].'"|';
			$output .= '"'.$log_list[$i]["leavedays"].'"|';
			$output .= '"'.$log_list[$i]["writedate"].'"|';
			$output .="\n";
				
			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_push_event_log_$yesterday_str.txt", 'a+');
					
				fwrite($fp, $output);
					
				fclose($fp);
			}
		}
		
		$db_main2->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_push_event_log/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_push_event_log_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_push_event_log_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_push_event_log_tmp (LIKE t5_push_event_log);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy t5_push_event_log_tmp ".
				"from 's3://wg-redshift/t5/tbl_push_event_log/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		

		// Insert records
		$sql = "INSERT INTO t5_push_event_log ".
				"SELECT s.* FROM t5_push_event_log_tmp s LEFT JOIN t5_push_event_log ".
				"ON s.useridx = t5_push_event_log.useridx ".
				"AND s.pushcode = t5_push_event_log.pushcode ".
				"WHERE t5_push_event_log.useridx IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_push_event_log_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>