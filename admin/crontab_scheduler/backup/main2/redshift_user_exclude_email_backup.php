<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);

		
	try
	{
		$db_main2 = new CDatabase_Slave_Main2();
		
		$db_main2->execute("SET wait_timeout=7200");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$sql = "SELECT useridx, email FROM tbl_user_email WHERE unsubscribe = 1";
		
		$mail_list = $db_main2->gettotallist($sql);
		
		for($i=0; $i<sizeof($mail_list); $i++)
		{
			$output = "";
				
			$mail = $mail_list[$i]["email"];
			$useridx = $mail_list[$i]["useridx"];
		
			$output .= '"'.$useridx.'"|';
			$output .= '"'.$mail.'"|';
			$output .="\n";
				
			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_t5_user_exclude_email_$yesterday_str.txt", 'a+');
					
				fwrite($fp, $output);
					
				fclose($fp);
			}
		}
		
		$db_main2->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_t5_user_exclude_email/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_t5_user_exclude_email_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_t5_user_exclude_email_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE tbl_t5_user_exclude_email_tmp (LIKE tbl_t5_user_exclude_email);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy tbl_t5_user_exclude_email_tmp ".
				"from 's3://wg-redshift/t5/tbl_t5_user_exclude_email/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
		// Update records
		$sql = "UPDATE tbl_t5_user_exclude_email ".
				" SET email = s.email".
				" FROM tbl_t5_user_exclude_email_tmp s ".
				" WHERE tbl_t5_user_exclude_email.useridx= s.useridx;";
		$db_redshift->execute($sql);

		// Insert records
		$sql = "INSERT INTO tbl_t5_user_exclude_email ".
				" SELECT s.* FROM tbl_t5_user_exclude_email_tmp s LEFT JOIN tbl_t5_user_exclude_email ".
				" ON s.useridx = tbl_t5_user_exclude_email.useridx ".
				" WHERE tbl_t5_user_exclude_email.useridx IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE tbl_t5_user_exclude_email_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>