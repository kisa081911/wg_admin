<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/main2/user_retention_log_backup.php") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/main2/user_retention_log_backup.php") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("backup/user_retention_log_backup Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$today = date("Y-m-d");
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
	
	//Web
	try
	{
		$db_main2 = new CDatabase_Slave_Main2();
		$db_main2->execute("SET wait_timeout=3600");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_retention_log'";
		$columns_count = $db_main2->getvalue($sql);
		
		$columns_count = $columns_count + 1;
		
		for($u=0; $u<10; $u++)
		{
			$sql = " SELECT * ".
					"	FROM (SELECT rtidx, useridx, adflag, fbsource, is_payer, level, leavedays, createdate, writedate, 0 AS platform FROM tbl_user_retention_log ".
					"	WHERE '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00' ".
					"	) t1 ".
					"	WHERE t1.useridx % 10 = $u ";
			
			$user_list = $db_main2->gettotallist($sql);
			
			for($i=0; $i<sizeof($user_list); $i++)
			{
			  $output = "";
			  
			  for($j=0; $j<$columns_count; $j++)
				{
					$user_list[$i][$j] = str_replace("\"", "\"\"", $user_list[$i][$j]);
					
					if($user_list[$i][$j] == "0000-00-00 00:00:00")
						$user_list[$i][$j] = "1900-01-01 00:00:00";
						
					if($j == $columns_count - 1)
						$output .= '"'.$user_list[$i][$j].'"';
					else
						$output .= '"'.$user_list[$i][$j].'"|';
				}
				$output .="\n";
				
				if($output != "")
				{
					$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_retention_log_$yesterday_str.txt", 'a+');
						
					fwrite($fp, $output);
						
					fclose($fp);
				}
		  }
		  
		}
		
		$db_main2->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	 
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_retention_log/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_retention_log_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_retention_log_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_retention_log_tmp (LIKE t5_user_retention_log);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy t5_user_retention_log_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_retention_log/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
		// Update records
		$sql = "UPDATE t5_user_retention_log ". 
				"SET adflag = s.adflag, fbsource = s.fbsource, is_payer = s.is_payer, level = s.level, leavedays = s.leavedays, createdate = s.createdate, writedate = s.writedate, platform = s.platform ".
				"FROM t5_user_retention_log_tmp s ".
				"WHERE t5_user_retention_log.rtidx = s.rtidx;";
		$db_redshift->execute($sql); 

		// Insert records
		$sql = "INSERT INTO t5_user_retention_log ".
				"SELECT s.* FROM t5_user_retention_log_tmp s LEFT JOIN t5_user_retention_log ".
				"ON s.rtidx = t5_user_retention_log.rtidx ".
				"WHERE t5_user_retention_log.rtidx IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_user_retention_log_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	//Mobile
	try
	{
		$db_main2 = new CDatabase_Main2();
		$db_main2->execute("SET wait_timeout=3600");
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_retention_mobile_log'";
		$columns_count = $db_main2->getvalue($sql);
	
		for($u=0; $u<10; $u++)
		{
			$sql = " SELECT * ".
					"	FROM (SELECT rtidx, platform, useridx, adflag, eventidx, is_payer, level, leavedays, createdate, writedate, site_id FROM tbl_user_retention_mobile_log ".
					"	WHERE '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00' ".
					"	) t1 ".
					"	WHERE t1.useridx % 10 = $u ";
					
			$user_list = $db_main2->gettotallist($sql);
					
			for($i=0; $i<sizeof($user_list); $i++)
			{
				$output = "";
	
				for($j=0; $j<$columns_count; $j++)
				{
					$user_list[$i][$j] = str_replace("\"", "\"\"", $user_list[$i][$j]);
	
					if($user_list[$i][$j] == "0000-00-00 00:00:00")
						$user_list[$i][$j] = "1900-01-01 00:00:00";
	
					if($j == $columns_count - 1)
						$output .= '"'.$user_list[$i][$j].'"';
					else
						$output .= '"'.$user_list[$i][$j].'"|';
				}
				
				$output .="\n";

				if($output != "")
				{
					$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_retention_mobile_log_$yesterday_str.txt", 'a+');
	
					fwrite($fp, $output);
	
					fclose($fp);
				}
			}
		}
	
		$db_main2->end();
	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');

		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_retention_mobile_log/$yesterday_str";

		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_retention_mobile_log_$yesterday_str.txt";

		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_retention_mobile_log_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));

		$ObjectURL = $result["ObjectURL"];

		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();

		// Create a staging table
		$sql = "CREATE TABLE t5_user_retention_mobile_log_tmp (LIKE t5_user_retention_mobile_log);";
		$db_redshift->execute($sql);

		// Load data into the staging table
		$sql = "copy t5_user_retention_mobile_log_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_retention_mobile_log/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);

		// Update records
		$sql = "UPDATE t5_user_retention_mobile_log ".
				"SET platform = s.platform, adflag = s.adflag, eventidx = s.eventidx, is_payer = s.is_payer, level = s.level, leavedays = s.leavedays, createdate = s.createdate, writedate = s.writedate, site_id = s.site_id ".
				"FROM t5_user_retention_mobile_log_tmp s ".
				"WHERE t5_user_retention_mobile_log.rtidx = s.rtidx;";
		$db_redshift->execute($sql);

		// Insert records
		$sql = "INSERT INTO t5_user_retention_mobile_log ".
				"SELECT s.* FROM t5_user_retention_mobile_log_tmp s LEFT JOIN t5_user_retention_mobile_log ".
				"ON s.rtidx = t5_user_retention_mobile_log.rtidx ".
				"WHERE t5_user_retention_mobile_log.rtidx IS NULL;";
		$db_redshift->execute($sql);

		// Drop the staging table
		$sql = "DROP TABLE t5_user_retention_mobile_log_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();

	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	//Update Mobile adflag
	try
	{
		$adflag_today = date("Ymd", time() - 60 * 60 * 24 * 2);
		$adflag_yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 3);
		$adflag_yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
		
		$db_main2 = new CDatabase_Main2();
		$db_main2->execute("SET wait_timeout=3600");
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_retention_mobile_log'";
		$columns_count = $db_main2->getvalue($sql);
	
		for($u=0; $u<10; $u++)
		{
			$sql = " SELECT * ".
					"	FROM (SELECT rtidx, platform, useridx, adflag, eventidx, is_payer, level, leavedays, createdate, writedate, site_id FROM tbl_user_retention_mobile_log ".
					"	WHERE '$adflag_yesterday 00:00:00' <= writedate AND writedate < '$adflag_today 00:00:00' ".
					"	) t1 ".
					"	WHERE t1.useridx % 10 = $u AND adflag != '' ";
				
			$user_list = $db_main2->gettotallist($sql);
				
			for($i=0; $i<sizeof($user_list); $i++)
			{
				$output = "";
	
				for($j=0; $j<$columns_count; $j++)
				{
					$user_list[$i][$j] = str_replace("\"", "\"\"", $user_list[$i][$j]);
	
					if($user_list[$i][$j] == "0000-00-00 00:00:00")
						$user_list[$i][$j] = "1900-01-01 00:00:00";
	
					if($j == $columns_count - 1)
						$output .= '"'.$user_list[$i][$j].'"';
					else
						$output .= '"'.$user_list[$i][$j].'"|';
				}
	
				$output .="\n";
	
				if($output != "")
				{
					$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_retention_mobile_log_adflag_$adflag_yesterday_str.txt", 'a+');
	
					fwrite($fp, $output);
	
					fclose($fp);
				}
			}
		}
	
		$db_main2->end();
	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_retention_mobile_log_adflag/$adflag_yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_retention_mobile_log_adflag_$adflag_yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_retention_mobile_log_adflag_'.$adflag_yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_retention_mobile_log_tmp (LIKE t5_user_retention_mobile_log);";
		$db_redshift->execute($sql);
	
		// Load data into the staging table
		$sql = "copy t5_user_retention_mobile_log_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_retention_mobile_log_adflag/$adflag_yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
	
		// Update records
		$sql = "UPDATE t5_user_retention_mobile_log ".
				"SET platform = s.platform, adflag = s.adflag, eventidx = s.eventidx, is_payer = s.is_payer, level = s.level, leavedays = s.leavedays, createdate = s.createdate, writedate = s.writedate, site_id = s.site_id ".
				"FROM t5_user_retention_mobile_log_tmp s ".
				"WHERE t5_user_retention_mobile_log.rtidx = s.rtidx AND t5_user_retention_mobile_log.adflag = '';";
		$db_redshift->execute($sql);
	
		// Drop the staging table
		$sql = "DROP TABLE t5_user_retention_mobile_log_tmp;";
		$db_redshift->execute($sql);
	
		$db_redshift->end();
	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>