<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/main2/redshift_user_promotion_live_backup") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/main2/redshift_user_promotion_live_backup") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("backup/redshift_user_promotion_live_backup Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);

		
	try
	{
		$db_main2 = new CDatabase_Slave_Main2();
		
		$db_main2->execute("SET wait_timeout=7200");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$sql = " SELECT logidx, promoidx, useridx, type, status, viewdate, clickdate, closedate, writedate ". 
			   " FROM tbl_promotion_user_live  ".
			   " WHERE writedate >= '$yesterday 00:00:00' ".
			   " AND writedate <= '$yesterday 23:59:59'";
		
		$user_list = $db_main2->gettotallist($sql);
		
		for($i=0; $i<sizeof($user_list); $i++)
		{
			$output = "";
				
			$logidx = $user_list[$i]["logidx"];
			$promoidx = $user_list[$i]["promoidx"];
			$useridx = $user_list[$i]["useridx"];
			$type = $user_list[$i]["type"];
			$status = $user_list[$i]["status"];
			
			if($user_list[$i]["viewdate"] == "0000-00-00 00:00:00")
				$user_list[$i]["viewdate"] = "1900-01-01 00:00:00";
			
			if($user_list[$i]["clickdate"] == "0000-00-00 00:00:00")
				$user_list[$i]["clickdate"] = "1900-01-01 00:00:00";
			
			if($user_list[$i]["closedate"] == "0000-00-00 00:00:00")
				$user_list[$i]["closedate"] = "1900-01-01 00:00:00";
			
			$viewdate = $user_list[$i]["viewdate"];
			$clickdate = $user_list[$i]["clickdate"];
			$closedate = $user_list[$i]["closedate"];
			$writedate = $user_list[$i]["writedate"];
			
		
			$output .= '"'.$logidx.'"|';
			$output .= '"'.$promoidx.'"|';
			$output .= '"'.$useridx.'"|';
			$output .= '"'.$type.'"|';
			$output .= '"'.$status.'"|';
			$output .= '"'.$viewdate.'"|';
			$output .= '"'.$clickdate.'"|';
			$output .= '"'.$closedate.'"|';
			$output .= '"'.$writedate.'"|';
			$output .="\n";
				
			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/t5_promotion_popup_user_new_$yesterday_str.txt", 'a+');
					
				fwrite($fp, $output);
					
				fclose($fp);
			}
		}
		
		$db_main2->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$is_file_exist = file_exists("$DOCUMENT_ROOT/redshift_temp/t5_promotion_popup_user_new_".$yesterday_str.".txt");
	
	if($is_file_exist)
	{	
		try
		{
			// Create the AWS service builder, providing the path to the config file
			$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
		
			$s3Client = $aws->get('s3');
			$bucket = "wg-redshift/t5/t5_promotion_popup_user_new/$yesterday_str";
		
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			$filepath = $DOCUMENT_ROOT."/redshift_temp/t5_promotion_popup_user_new_$yesterday_str.txt";
		
			// Upload an object to Amazon S3
			$result = $s3Client->putObject(array(
					'Bucket' 		=> $bucket,
					'Key'    		=> 't5_promotion_popup_user_new_'.$yesterday_str.'.txt',
					'ACL'	 		=> 'public-read',
					'SourceFile'   	=> $filepath
			));
		
			$ObjectURL = $result["ObjectURL"];
		
			if($ObjectURL != "")
			{
				@unlink($filepath);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
	
		try
		{
			$db_redshift = new CDatabase_Redshift();
		
			// Create a staging table
			$sql = "CREATE TABLE t5_promotion_popup_user_new_tmp (LIKE t5_promotion_popup_user_new);";
			$db_redshift->execute($sql);
			
			// Load data into the staging table
			$sql = "copy t5_promotion_popup_user_new_tmp ".
					"from 's3://wg-redshift/t5/t5_promotion_popup_user_new/$yesterday_str' ".
					"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
					"delimiter '|' ".
					"csv;";
			$db_redshift->execute($sql);
			
			// Update records
			$sql = "UPDATE t5_promotion_popup_user_new ".
					" SET viewdate = s.viewdate".
					" FROM t5_promotion_popup_user_new_tmp s ".
					" WHERE t5_promotion_popup_user_new.logidx= s.logidx;";
			$db_redshift->execute($sql);
	
			// Insert records
			$sql = "INSERT INTO t5_promotion_popup_user_new ".
					" SELECT s.* FROM t5_promotion_popup_user_new_tmp s LEFT JOIN t5_promotion_popup_user_new ".
					" ON s.logidx = t5_promotion_popup_user_new.logidx ".
					" WHERE t5_promotion_popup_user_new.logidx IS NULL;";
			$db_redshift->execute($sql);
				
			// Drop the staging table
			$sql = "DROP TABLE t5_promotion_popup_user_new_tmp;";
			$db_redshift->execute($sql);
	
			$db_redshift->end();
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
	}
?>