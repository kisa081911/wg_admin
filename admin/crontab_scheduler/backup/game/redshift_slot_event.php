<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
		
	try
	{
		$db_game = new CDatabase_Game();
		
		$db_game->execute("SET wait_timeout=7200");
		
		for($u=0; $u<10; $u++)
		{
			$output = "";
			
			$sql = "SELECT eventidx, roomidx, bigwinidx, useridx, senderidx, eventtype, amount, shareamount, sharetype, jackpothallidx, commonidx, os_type, writedate ".
					"FROM tbl_slot_event_$u";
			write_log($sql);
			$log_list = $db_game->gettotallist($sql);
							
			for($i=0; $i<sizeof($log_list); $i++)
			{
						
				if($log_list[$i]["writedate"] == "0000-00-00 00:00:00")
					$log_list[$i]["writedate"] = "1900-01-01 00:00:00";

				$output .= '"'.$log_list[$i]["eventidx"].'"|';
				$output .= '"'.$log_list[$i]["roomidx"].'"|';
				$output .= '"'.$log_list[$i]["bigwinidx"].'"|';
				$output .= '"'.$log_list[$i]["useridx"].'"|';
				$output .= '"'.$log_list[$i]["senderidx"].'"|';
				$output .= '"'.$log_list[$i]["eventtype"].'"|';
				$output .= '"'.$log_list[$i]["amount"].'"|';
				$output .= '"'.$log_list[$i]["shareamount"].'"|';
				$output .= '"'.$log_list[$i]["sharetype"].'"|';
				$output .= '"'.$log_list[$i]["jackpothallidx"].'"|';
				$output .= '"'.$log_list[$i]["commonidx"].'"|';
				$output .= '"'.$log_list[$i]["os_type"].'"|';
				$output .= '"'.$log_list[$i]["writedate"].'"|';

				$output .="\n";
					
			}
						
			if($output != "")
			{
				$fp = fopen("/take5/html/redshift_temp/t5_slot_event_".$yesterday_str.".txt", 'a+');

				fwrite($fp, $output);
						
				fclose($fp);
			}
		}
		
		$db_game->end();

	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/t5_slot_event/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/t5_slot_event_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 't5_slot_event_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_slot_event_tmp (LIKE t5_slot_event);";
		$db_redshift->execute($sql);
	
		// Load data into the staging table
		$sql = "copy t5_slot_event_tmp ".
				"from 's3://wg-redshift/t5/t5_slot_event/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
	
		// Insert records
		$sql = "INSERT INTO t5_slot_event (roomidx, bigwinidx, useridx, senderidx, eventtype, amount, shareamount, sharetype, jackpothallidx, commonidx, os_type, writedate) ".
				"SELECT roomidx, bigwinidx, useridx, senderidx, eventtype, amount, shareamount, sharetype, jackpothallidx, commonidx, os_type, writedate FROM t5_slot_event_tmp;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_slot_event_tmp;";
		$db_redshift->execute($sql);
	
		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>