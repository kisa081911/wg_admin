<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/redshfit_user_boost_history_backup") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/redshfit_user_boost_history_backup") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("backup/redshfit_user_boost_history_backup Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	
	$str_useridx = 20000;
	$today = date("Y-m-d");
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
  
  try
	{
		$db_main2 = new CDatabase_Main2();
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
		for($u=0; $u<10; $u++)
		{
			$output = "";
 			$sql = "SELECT logidx, useridx, boosttype, writedate FROM `tbl_user_boost_history` WHERE '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'";
		
			$log_list = $db_main2->gettotallist($sql);
			
			for($i=0; $i<sizeof($log_list); $i++)
			{
					
				if($log_list[$i]["writedate"] == "0000-00-00 00:00:00")
					$log_list[$i]["writedate"] = "1900-01-01 00:00:00";

				$output .= '"'.$log_list[$i]["logidx"].'"|';
				$output .= '"'.$log_list[$i]["useridx"].'"|';
				$output .= '"'.$log_list[$i]["boosttype"].'"|';				
				$output .= '"'.$log_list[$i]["writedate"].'"|';	
				
				$output .="\n";
				
			}
			
			if($output != "")
			{
			  $fp = fopen("$DOCUMENT_ROOT/redshift_temp/t5_user_boost_history_".$yesterday_str.".txt", 'a+');
			  
			  fwrite($fp, $output);
			  
			  fclose($fp);
			}
		}
		
		$db_main2->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	 
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/t5_user_boost_history/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/t5_user_boost_history_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 't5_user_boost_history_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_boost_history_tmp (LIKE t5_user_boost_history);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy t5_user_boost_history_tmp ".
				"from 's3://wg-redshift/t5/t5_user_boost_history/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
		// Insert records
		$sql = "INSERT INTO t5_user_boost_history (logidx, useridx, boosttype, writedate) ". 
				" (SELECT s.logidx, useridx, boosttype, writedate FROM t5_user_boost_history_tmp s );";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_user_boost_history_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
?>