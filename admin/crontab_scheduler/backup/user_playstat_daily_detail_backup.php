<?
	include("../../common/common_include.inc.php");

	$db_other = new CDatabase_Other();
	$db_other_bak = new CDatabase_Otherbak();
	
	$db_other->execute("SET wait_timeout=3600");
	$db_other_bak->execute("SET wait_timeout=3600");
	
	$today = date("Y-m-d");

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/backup/user_playstat_daily_detail_backup") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/backup/user_playstat_daily_detail_backup") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("user_playstat_daily_detail_backup Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	try
	{
		$yearmonth = date("Ym", time() - 60 * 60 * 24 * 1);
		$year_month = date("Y-m", time() - 60 * 60 * 24 * 1);
		
		$sql = "SELECT COUNT(*) AS COUNT FROM `information_schema`.`TABLES` WHERE TABLE_SCHEMA='takefive_other_bak' AND TABLE_NAME = 'tbl_user_playstat_daily_detail_$yearmonth'";
		$is_exists = $db_other_bak->getvalue($sql);
		
		if($is_exists == 0) // 테이블이 존재하지 않는 경우
		{
			$sql = 	" CREATE TABLE `tbl_user_playstat_daily_detail_$yearmonth` ( ".
					"   `statidx` bigint(20) NOT NULL AUTO_INCREMENT, ".
					"   `today` date NOT NULL,   ".
					"   `useridx` bigint(20) NOT NULL,  ".
					"   `slottype` int(11) NOT NULL,   ".
					"   `moneyin` bigint(20) DEFAULT NULL,  ".
					"   `moneyout` bigint(20) DEFAULT NULL, ".
					"   `winrate` int(11) DEFAULT NULL,  ".
					"   `bigwin` bigint(20) DEFAULT NULL,  ".
					"   `playtime` int(11) DEFAULT NULL,  ".
					"   `playcount` int(11) DEFAULT NULL,  ".
					"   `h_moneyin` bigint(20) NOT NULL DEFAULT '0', ".
					"   `h_moneyout` bigint(20) NOT NULL DEFAULT '0', ".
					"   `h_winrate` int(11) NOT NULL DEFAULT '0', ".
					"   `h_bigwin` bigint(20) NOT NULL DEFAULT '0', ".
					"   `h_playtime` int(11) NOT NULL DEFAULT '0', ".
					"   `h_playcount` int(11) NOT NULL DEFAULT '0', ".
					"   `treatamount` bigint(20) NOT NULL DEFAULT '0', ".
					"   `redeem` bigint(20) NOT NULL DEFAULT '0', ".
					"   PRIMARY KEY (`statidx`), ".
					"	  KEY `today` (`today`,`useridx`), ".
					"	  KEY `today2` (`today`,`slottype`) ".
					" ) ENGINE=InnoDB DEFAULT CHARSET=utf8;  ";
			
			$db_other_bak->execute($sql);
		}

		$sql = "SELECT logidx FROM tbl_backup_stat WHERE category = 30";
		$lastidx = $db_other_bak->getvalue($sql);
		
		$sql = "SELECT MAX(statidx) ".
				"FROM tbl_user_playstat_daily_detail ".
				"WHERE statidx > $lastidx AND '$year_month-01 00:00:00' <= today AND today < DATE_ADD('$year_month-01 00:00:00', INTERVAL 1 MONTH)";
		$maxidx = $db_other->getvalue($sql);
		
		$totalcount = $maxidx - $lastidx;
		
		while($totalcount > 0)
		{
			$sql = "SELECT logidx FROM tbl_backup_stat WHERE category = 30";
			$lastidx = $db_other_bak->getvalue($sql);

			$sql = "SELECT statidx, today, useridx, slottype, moneyin, moneyout, winrate, bigwin, playtime, playcount, ".
					"h_moneyin, h_moneyout, h_winrate, h_bigwin,h_playtime, h_playcount, treatamount, redeem ".
					"FROM `tbl_user_playstat_daily_detail` ".
					"WHERE statidx > $lastidx AND '$year_month-01 00:00:00' <= today AND today < DATE_ADD('$year_month-01 00:00:00', INTERVAL 1 MONTH) ".
					"ORDER BY statidx ASC ".
					"LIMIT 5000";
			$daily_detail_list = $db_other->gettotallist($sql);
				
			$sql = "";
			$insertcount = 0;
				
			for($i=0; $i<sizeof($daily_detail_list); $i++)
			{
				$statidx = $daily_detail_list[$i]["statidx"];      
				$today   = $daily_detail_list[$i]["today"];             
				$useridx   = $daily_detail_list[$i]["useridx"];
				$slottype   = $daily_detail_list[$i]["slottype"];
				$moneyin   = $daily_detail_list[$i]["moneyin"];               
				$moneyout   = $daily_detail_list[$i]["moneyout"];              
				$winrate   = $daily_detail_list[$i]["winrate"];               
				$bigwin   = $daily_detail_list[$i]["bigwin"];                
				$playtime   = $daily_detail_list[$i]["playtime"];              
				$playcount   = $daily_detail_list[$i]["playcount"];             
				$h_moneyin   = $daily_detail_list[$i]["h_moneyin"];             
				$h_moneyout  = $daily_detail_list[$i]["h_moneyout"];            
				$h_winrate   = $daily_detail_list[$i]["h_winrate"];             
				$h_bigwin   = $daily_detail_list[$i]["h_bigwin"];              
				$h_playtime  = $daily_detail_list[$i]["h_playtime"];            
				$h_playcount  = $daily_detail_list[$i]["h_playcount"];           
				$treatamount  = $daily_detail_list[$i]["treatamount"];           
				$redeem  = $daily_detail_list[$i]["redeem"];           
						
				if ($insertcount == 0)
				{
					$sql = 	"INSERT INTO tbl_user_playstat_daily_detail_$yearmonth(`statidx`, `today`, `useridx`,`slottype`,`moneyin`,`moneyout`, `winrate`, `bigwin`, `playtime`, `playcount`, `h_moneyin`, `h_moneyout`, `h_winrate`, `h_bigwin`,  ".
							" `h_playtime`, `h_playcount`, `treatamount`, `redeem`)  ".
							"VALUES($statidx, '$today', $useridx, $slottype, $moneyin, $moneyout, $winrate, $bigwin, $playtime, $playcount, $h_moneyin, $h_moneyout, $h_winrate, $h_bigwin, $h_playtime, $h_playcount, $treatamount, $redeem) ";
							
		
					$insertcount++;
				}
				else if ($insertcount < 250)
				{
					$sql .= ", ($statidx, '$today', $useridx, $slottype, $moneyin, $moneyout, $winrate, $bigwin, $playtime, $playcount, $h_moneyin, $h_moneyout, $h_winrate, $h_bigwin, $h_playtime, $h_playcount, $treatamount, $redeem) ";
					
					$insertcount++;
				}
				else
				{
					$sql .= ", ($statidx, '$today', $useridx, $slottype, $moneyin, $moneyout, $winrate, $bigwin, $playtime, $playcount, $h_moneyin, $h_moneyout, $h_winrate, $h_bigwin, $h_playtime, $h_playcount, $treatamount, $redeem) ON DUPLICATE KEY UPDATE statidx=VALUES(statidx); ";
					
					$insertcount = 0;
		
					$db_other_bak->execute($sql);
		
					$sql = "";
				}
			}
			
			if($totalcount%100000 == 0)
				sleep(1);
				
			if($sql != "")
			{
				$sql .= " ON DUPLICATE KEY UPDATE statidx=VALUES(statidx);";
				$db_other_bak->execute($sql);
			}
				
			$totalcount -= 5000;
				
			$sql = "UPDATE tbl_backup_stat ".
					"SET logidx = (SELECT MAX(statidx) FROM tbl_user_playstat_daily_detail_$yearmonth) ".
					"WHERE category = 30";
			$db_other_bak->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_other->end();
	$db_other_bak->end();
?>