<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$sdate = "2019-05-10";
	$edate = "2019-05-29";
	
	while($sdate <= $edate)
	{
		$today = date('Y-m-d', strtotime($sdate));
		$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));
		write_log("tbl_ad_stats_backup : ".$today);
		$yesterday = date('Y-m-d', strtotime($today.' - 1 day'));
		$yesterday_str = date('Ymd', strtotime($today.' - 1 day'));
		
		// tbl_ad_retention_stats, tbl_ad_retention_stats_mobile
		try
		{
			$db_main2 = new CDatabase_Main2();
			$db_main2->execute("SET wait_timeout=3600");
			
			//$today = date("Y-m-d");
			//$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
			//$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
			
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			
			$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_ad_stats'";
			$columns_count = $db_main2->getvalue($sql);
			
			$columns_count = $columns_count + 2;
			
			$sql = " SELECT today, account_id, ad_id, ad_name, adset_id, adset_name, campaign_id, campaign_name, platform, impressions, cpm, cpc, ad_reach, adset_reach, campaign_reach, app_install_cost, app_install_result,
					user_install_count, clicks, ctr, spend, ad_effective_status, ad_configured_status, adset_effective_status, adset_configured_status, campaign_effective_status, `status`, objective, join_count, play_count, relevance_score, created_time,
					updated_time, start_time, IFNULL(sales_account, '0.0') AS sales_account, thumbnail_url, daily_budget
					FROM
					(
						SELECT today, '' AS account_id, ad_id, ad_name, adset_id, adset_name, campaign_id, campaign_name, 0 AS platform, impressions, cpm, cpc, ad_reach, adset_reach, campaign_reach, app_install_cost, app_install_result,
						user_install_count, clicks, ctr, spend, ad_effective_status, ad_configured_status, adset_effective_status, adset_configured_status, campaign_effective_status, `status`, objective, join_count, play_count, relevance_score, created_time,
						updated_time, start_time, sales_account, thumbnail_url, daily_budget
						FROM `tbl_ad_stats` WHERE '$yesterday' <= today AND today < '$today'
						UNION ALL
						SELECT today, account_id, ad_id, ad_name, adset_id, adset_name, campaign_id, campaign_name, platform, impressions, cpm, cpc, ad_reach, adset_reach, campaign_reach, app_install_cost, app_install_result,
						user_install_count, clicks, ctr, spend, ad_effective_status, ad_configured_status, adset_effective_status, adset_configured_status, campaign_effective_status, `status`, objective, join_count, play_count, relevance_score, created_time,
						updated_time, start_time, sales_account, thumbnail_url, daily_budget
						FROM `tbl_ad_stats_mobile` WHERE '$yesterday' <= today AND today < '$today'
					) tt;";
			
			$ad_list = $db_main2->gettotallist($sql);
			
			for($i=0; $i<sizeof($ad_list); $i++)
			{
			  $output = ""; 
			  
			  for($j=0; $j<$columns_count; $j++)
				{
					$ad_list[$i][$j] = str_replace("\"", "\"\"", $ad_list[$i][$j]);
					
					if($ad_list[$i][$j] == "0000-00-00 00:00:00")
						$ad_list[$i][$j] = "1900-01-01 00:00:00";
						
					if($j == $columns_count - 1)
						$output .= '"'.$ad_list[$i][$j].'"';
					else
						$output .= '"'.$ad_list[$i][$j].'"|';
				}
				$output .="\n";
				
				if($output != "")
				{
					$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_ad_stats_$yesterday_str.txt", 'a+');
						
					fwrite($fp, $output);
						
					fclose($fp);
				}
		  }		  
			
			$db_main2->end();		
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		$is_file_exist = file_exists("$DOCUMENT_ROOT/redshift_temp/tbl_ad_stats_".$yesterday_str.".txt");
		
		if($is_file_exist)
		{		
			try
			{
				// Create the AWS service builder, providing the path to the config file
				$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
			
				$s3Client = $aws->get('s3');
				$bucket = "wg-redshift/t5/tbl_ad_stats/$yesterday_str";
			
				$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
				$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_ad_stats_$yesterday_str.txt";
			
				// Upload an object to Amazon S3
				$result = $s3Client->putObject(array(
						'Bucket' 		=> $bucket,
						'Key'    		=> 'tbl_ad_stats_'.$yesterday_str.'.txt',
						'ACL'	 		=> 'public-read',
						'SourceFile'   	=> $filepath
				));
			
				$ObjectURL = $result["ObjectURL"];
			
				if($ObjectURL != "")
				{
					@unlink($filepath);
				}
			}
			catch(Exception $e)
			{
				write_log($e->getMessage());
			}
			
			try
			{
				$db_redshift = new CDatabase_Redshift();
			
				// Create a staging table
				$sql = "CREATE TABLE t5_ad_stats_tmp (LIKE t5_ad_stats);";
				$db_redshift->execute($sql);
			
				// Load data into the staging table
				$sql = "copy t5_ad_stats_tmp ".
						"from 's3://wg-redshift/t5/tbl_ad_stats/$yesterday_str' ".
						"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
						"delimiter '|' ".
						"csv;";
				$db_redshift->execute($sql);
			
				// Insert records
				$sql = "INSERT INTO t5_ad_stats ".
						"SELECT s.* FROM t5_ad_stats_tmp s LEFT JOIN t5_ad_stats ".
						"ON s.today = t5_ad_stats.today AND s.account_id = t5_ad_stats.account_id AND s.ad_id = t5_ad_stats.ad_id AND s.adset_id = t5_ad_stats.adset_id AND s.campaign_id = t5_ad_stats.campaign_id ".
						"WHERE t5_ad_stats.today IS NULL AND t5_ad_stats.account_id IS NULL AND t5_ad_stats.ad_id IS NULL AND t5_ad_stats.adset_id IS NULL AND t5_ad_stats.campaign_id IS NULL;";
				$db_redshift->execute($sql);
					
				// Drop the staging table
				$sql = "DROP TABLE t5_ad_stats_tmp;";
				$db_redshift->execute($sql);
			
				$db_redshift->end();
			
			}
			catch(Exception $e)
			{
				write_log($e->getMessage());
			}
		}
	}
?>