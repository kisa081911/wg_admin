<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/marketing/tbl_agency_spend_daily_backup") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/marketing/tbl_agency_spend_daily_backup") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("tbl_agency_spend_daily_backup Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	// tbl_agency_spend_daily
	try
	{
		$db_main2 = new CDatabase_Slave_Main2();
		$db_main2->execute("SET wait_timeout=3600");
		
		$today = date("Y-m-d");
		$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
		$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_agency_spend_daily'";
		$columns_count = $db_main2->getvalue($sql);
		
		$sql = "SELECT today, platform, agencyidx, agencyname, spend FROM tbl_agency_spend_daily WHERE '$yesterday' <= today AND today < '$today'";		
		$spend_daily_list = $db_main2->gettotallist($sql);
		
		for($i=0; $i<sizeof($spend_daily_list); $i++)
		{
		  $output = ""; 
		  
		  for($j=0; $j<$columns_count; $j++)
			{
				$spend_daily_list[$i][$j] = str_replace("\"", "\"\"", $spend_daily_list[$i][$j]);
				
				if($spend_daily_list[$i][$j] == "0000-00-00 00:00:00")
					$spend_daily_list[$i][$j] = "1900-01-01 00:00:00";
					
				if($j == $columns_count - 1)
					$output .= '"'.$spend_daily_list[$i][$j].'"';
				else
					$output .= '"'.$spend_daily_list[$i][$j].'"|';
			}
			$output .="\n";
			
			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_agency_spend_daily_$yesterday_str.txt", 'a+');
					
				fwrite($fp, $output);
					
				fclose($fp);
			}
	  }		  
		
		$db_main2->end();		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$is_file_exist = file_exists("$DOCUMENT_ROOT/redshift_temp/tbl_agency_spend_daily_".$yesterday_str.".txt");
	
	if($is_file_exist)
	{	
		try
		{
			// Create the AWS service builder, providing the path to the config file
			$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
		
			$s3Client = $aws->get('s3');
			$bucket = "wg-redshift/t5/tbl_agency_spend_daily/$yesterday_str";
		
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_agency_spend_daily_$yesterday_str.txt";
		
			// Upload an object to Amazon S3
			$result = $s3Client->putObject(array(
					'Bucket' 		=> $bucket,
					'Key'    		=> 'tbl_agency_spend_daily_'.$yesterday_str.'.txt',
					'ACL'	 		=> 'public-read',
					'SourceFile'   	=> $filepath
			));
		
			$ObjectURL = $result["ObjectURL"];
		
			if($ObjectURL != "")
			{
				@unlink($filepath);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		try
		{
			$db_redshift = new CDatabase_Redshift();
		
			// Create a staging table
			$sql = "CREATE TABLE t5_agency_spend_daily_tmp (LIKE t5_agency_spend_daily);";
			$db_redshift->execute($sql);
		
			// Load data into the staging table
			$sql = "copy t5_agency_spend_daily_tmp ".
					"from 's3://wg-redshift/t5/tbl_agency_spend_daily/$yesterday_str' ".
					"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
					"delimiter '|' ".
					"csv;";
			$db_redshift->execute($sql);
			
			// Update records
			$sql = "UPDATE t5_agency_spend_daily ".
					"SET agencyname = s.agencyname, spend = s.spend ".
					"FROM t5_agency_spend_daily_tmp s ".
					"WHERE t5_agency_spend_daily.today = s.today AND t5_agency_spend_daily.platform = s.platform AND t5_agency_spend_daily.agencyidx = s.agencyidx";
			$db_redshift->execute($sql);
		
			// Insert records
			$sql = "INSERT INTO t5_agency_spend_daily ".
					"SELECT s.* FROM t5_agency_spend_daily_tmp s LEFT JOIN t5_agency_spend_daily ".
					"ON s.today = t5_agency_spend_daily.today AND s.platform = t5_agency_spend_daily.platform AND s.agencyidx = t5_agency_spend_daily.agencyidx ".
					"WHERE t5_agency_spend_daily.today IS NULL AND t5_agency_spend_daily.platform IS NULL AND t5_agency_spend_daily.agencyidx IS NULL;";
			$db_redshift->execute($sql);
				
			// Drop the staging table
			$sql = "DROP TABLE t5_agency_spend_daily_tmp;";
			$db_redshift->execute($sql);
		
			$db_redshift->end();
		
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
	}
?>