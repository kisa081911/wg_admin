<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	$str_useridx = 20000;
	
		$today = date("Y-m-d");
		$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
		$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
	  
	  try
		{
			$db_main2 = new CDatabase_Main2();
			
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			
			$output = "";
			$sql = " SELECT * FROM tbl_threshold_offer";
			$threshold_offer_list = $db_main2->gettotallist($sql);
			
			for($i=0; $i<sizeof($threshold_offer_list); $i++)
			{
			    
			    if($threshold_offer_list[$i]["offer_checkdate"] == "0000-00-00 00:00:00")
			        $threshold_offer_list[$i]["offer_checkdate"] = "1900-01-01 00:00:00";
			    
			    if($threshold_offer_list[$i]["updatedate"] == "0000-00-00 00:00:00")
			        $threshold_offer_list[$i]["updatedate"] = "1900-01-01 00:00:00";
			    
			    if($threshold_offer_list[$i]["writedate"] == "0000-00-00 00:00:00")
			        $threshold_offer_list[$i]["writedate"] = "1900-01-01 00:00:00";
					
			    $output .= '"'.$threshold_offer_list[$i]["useridx"].'"|';
			    $output .= '"'.$threshold_offer_list[$i]["target_type"].'"|';
			    $output .= '"'.$threshold_offer_list[$i]["enable"].'"|';				
			    $output .= '"'.$threshold_offer_list[$i]["activated"].'"|';				
			    $output .= '"'.$threshold_offer_list[$i]["coin_p"].'"|';
			    $output .= '"'.$threshold_offer_list[$i]["threshold"].'"|';
			    $output .= '"'.$threshold_offer_list[$i]["max_product"].'"|';
			    $output .= '"'.$threshold_offer_list[$i]["avg_product"].'"|';
			    $output .= '"'.$threshold_offer_list[$i]["sale_rate"].'"|';
			    $output .= '"'.$threshold_offer_list[$i]["thresholdcount"].'"|';
			    $output .= '"'.$threshold_offer_list[$i]["thresholdcount_max"].'"|';
			    $output .= '"'.$threshold_offer_list[$i]["cooltime"].'"|';
			    $output .= '"'.$threshold_offer_list[$i]["offer_checkdate"].'"|';
			    $output .= '"'.$threshold_offer_list[$i]["updatedate"].'"|';
			    $output .= '"'.$threshold_offer_list[$i]["writedate"].'"|';
				
				$output .="\n";
				
			}
			
			if($output != "")
			{
			  $fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_threshold_offer_".$yesterday_str.".txt", 'a+');
			  
			  fwrite($fp, $output);
			  
			  fclose($fp);
			}
			
			$db_main2->end();
			
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		 
		try
		{
			// Create the AWS service builder, providing the path to the config file
			$aws = Aws::factory('../../common/aws_sdk/aws_config.php');
		
			$s3Client = $aws->get('s3');
			$bucket = "wg-redshift/t5/tbl_threshold_offer/$yesterday_str";
		
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_threshold_offer_$yesterday_str.txt";
		
			// Upload an object to Amazon S3
			$result = $s3Client->putObject(array(
					'Bucket' 		=> $bucket,
					'Key'    		=> 'tbl_threshold_offer_'.$yesterday_str.'.txt',
					'ACL'	 		=> 'public-read',
					'SourceFile'   	=> $filepath
			));
		
			$ObjectURL = $result["ObjectURL"];
		
			if($ObjectURL != "")
			{
				@unlink($filepath);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
	
		try
		{
			$db_redshift = new CDatabase_Redshift();
		
			// Create a staging table
			$sql = "CREATE TABLE t5_threshold_offer_tmp (LIKE t5_threshold_offer);";
			$db_redshift->execute($sql);
			
			// Load data into the staging table
			$sql = "copy t5_threshold_offer_tmp ".
					"from 's3://wg-redshift/t5/tbl_threshold_offer/$yesterday_str' ".
					"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
					"delimiter '|' ".
					"csv;";
			$db_redshift->execute($sql);
			
			// Update records
			$sql = "UPDATE t5_threshold_offer ".
			 			"SET target_type = s.target_type, status = s.status, activated = s.activated, coin_p = s.coin_p, threshold = s.threshold, max_product = s.max_product
                            , avg_product = s.avg_product, sale_rate = s.sale_rate, thresholdcount = s.thresholdcount, thresholdcount_max = s.thresholdcount_max, cooltime = s.cooltime
                            , offer_checkdate = s.offer_checkdate, updatedate = s.updatedate".
			 			"FROM t5_threshold_offer_tmp s ".
			 			"WHERE t5_threshold_offer.useridx = s.useridx;";
			$db_redshift->execute($sql);
			
			// Insert records
			$sql = "INSERT INTO t5_threshold_offer (useridx, target_type, status, activated, coin_p, threshold, max_product, avg_product, sale_rate, thresholdcount, thresholdcount_max, cooltime, offer_checkdate, updatedate, writedate) ". 
					" (SELECT s.useridx, s.target_type, s.status, s.activated, s.coin_p, s.threshold, s.max_product, s.avg_product, s.sale_rate, s.thresholdcount, s.thresholdcount_max, s.cooltime, s.offer_checkdate, s.updatedate, s.writedate FROM t5_threshold_offer_tmp s );";
			$db_redshift->execute($sql);
				
			// Drop the staging table
			$sql = "DROP TABLE t5_threshold_offer_tmp;";
			$db_redshift->execute($sql);
	
			$db_redshift->end();
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
?>