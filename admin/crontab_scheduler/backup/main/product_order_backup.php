<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/main/product_order_backup") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/main/product_order_backup") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("product_order_backup Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	if (date("H") == "00")
	{
		$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
		$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
	}
	else
	{
		$yesterday = date("Y-m-d");
		$yesterday_str = date("Ymd", time());
	}

	try
	{
		$db_main = new CDatabase_Main();
		$db_redshift = new CDatabase_Redshift();
		
		$db_main->execute("SET wait_timeout=3600");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];

		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_product_order'";
		$columns_count = $db_main->getvalue($sql);
		
		$sql = "SELECT MAX(writedate) FROM t5_product_order ";
		$last_writedate = $db_redshift->getvalue($sql);
		
		$last_canceldate = date("Y-m-d", time() - 60 * 60 * 24 * 3);
		
		$sql = "SELECT orderidx, useridx, productidx, usercoin, userlevel, coin, orderno, facebookcredit, vip_point, status, gift_status, gift_coin, couponidx, special_discount, special_more, ".
				" basecoin, canceldate, cancelleftcoin, disputed, writedate, product_type, coupon_type, is_v2, currency, currency_amount, tax_country, tax_amount, payout_foreign_exchange_rate FROM tbl_product_order ".
				"WHERE (status = 1 AND '$last_writedate' <= writedate AND writedate < CONCAT(DATE_FORMAT(NOW(), '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5, ':00')) ".
				"OR (status = 2 AND '$last_canceldate' <= canceldate AND canceldate < CONCAT(DATE_FORMAT(NOW(), '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5, ':00')) ".
				"OR (status = 3 AND '$last_canceldate' <= canceldate AND canceldate < CONCAT(DATE_FORMAT(NOW(), '%Y-%m-%d %H:'), IF((FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5 < 10), '0', ''), FLOOR(DATE_FORMAT(NOW(), '%i')/5)*5, ':00')) ";
		$log_list = $db_main->gettotallist($sql);
		
		$db_main->end();
		$db_redshift->end();
		
		$output = "";
		
		for($i=0; $i<sizeof($log_list); $i++)
		{
			for($j=0; $j<$columns_count; $j++)
			{
				$log_list[$i][$j] = str_replace("\"", "\"\"", $log_list[$i][$j]);
				
				if($log_list[$i][$j] == "0000-00-00 00:00:00")
					$log_list[$i][$j] = "1900-01-01 00:00:00";
				
				if($j == $columns_count - 1)
					$output .= '"'.$log_list[$i][$j].'"';
				else
					$output .= '"'.$log_list[$i][$j].'"|';
			}
				
			$output .="\n";
		}
		
		if($output != "")
		{
			$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_product_order_$yesterday_str.txt", 'a+');
			
			fwrite($fp, $output);
			
			fclose($fp);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/take5/product_order/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_product_order_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_product_order_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$db_redshift = new CDatabase_Redshift();

		// Create a staging table 
		$sql = "CREATE TABLE t5_product_order_tmp (LIKE t5_product_order);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table 
		$sql = "copy t5_product_order_tmp ".
				"from 's3://wg-redshift/take5/product_order/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
		// Update records 
		$sql = "UPDATE t5_product_order ".
				"SET status = s.status, canceldate = s.canceldate, cancelleftcoin = s.cancelleftcoin ".
				"FROM t5_product_order_tmp s ". 
				"WHERE t5_product_order.orderidx = s.orderidx;";
		$db_redshift->execute($sql);
		
		// Insert records
		$sql = "INSERT INTO t5_product_order ". 
				"SELECT s.* FROM t5_product_order_tmp s LEFT JOIN t5_product_order ". 
				"ON s.orderidx = t5_product_order.orderidx ".
				"WHERE t5_product_order.orderidx IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_product_order_tmp;";
		$db_redshift->execute($sql);
	
		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>