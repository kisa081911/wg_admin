<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);

		
	try
	{
		$slave_main = new CDatabase_Slave_Main();
		$db_mobile = new CDatabase_Mobile();
		
		$slave_main->execute("SET wait_timeout=7200");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$sql = "SELECT * FROM `tbl_user_gdpr_info`
                UNION ALL
                SELECT * FROM `tbl_user_gdpr_info_mobile`";
		
		$user_list = $slave_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($user_list); $i++)
		{
			$output = "";
				
			$useridx = $user_list[$i]["useridx"];
		
			$output .= '"'.$useridx.'"|';
			$output .= '"'.$user_list[$i]["login_type"].'"|';
			$output .= '"'.$user_list[$i]["platform_type"].'"|';
			$output .= '"'.$user_list[$i]["version"].'"|';
			$output .= '"'.$user_list[$i]["is_agree"].'"|';
			$output .= '"'.$user_list[$i]["country"].'"|';
			$output .= '"'.$user_list[$i]["ipaddress"].'"|';
			$output .= '"'.$user_list[$i]["writedate"].'"|';
			$output .="\n";
				
			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_gdpr_info_$yesterday_str.txt", 'a+');
					
				fwrite($fp, $output);
					
				fclose($fp);
			}
		}
		
		$slave_main->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_gdpr_info/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_gdpr_info_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_gdpr_info_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_gdpr_info_tmp (LIKE t5_user_gdpr_info);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy t5_user_gdpr_info_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_gdpr_info/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
		// Update records
		$sql = "UPDATE t5_user_gdpr_info ".
				" SET is_agree = s.is_agree ".
				" FROM t5_user_gdpr_info_tmp s ".
				" WHERE t5_user_gdpr_info.useridx = s.useridx; ";
		$db_redshift->execute($sql);

		// Insert records
		$sql = "INSERT INTO t5_user_gdpr_info ".
				"SELECT s.* FROM t5_user_gdpr_info_tmp s LEFT JOIN t5_user_gdpr_info ".
				"ON s.useridx = t5_user_gdpr_info.useridx ".
				"WHERE t5_user_gdpr_info.useridx IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_user_gdpr_info_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>