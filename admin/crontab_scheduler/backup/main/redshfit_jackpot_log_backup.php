<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	$str_useridx = 20000;
	
	$today = date("Y-m-d", time());
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
	  
	  try
		{
			$db_main = new CDatabase_Main();
			
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			
			$output = "";
			$sql = " select *,ifnull((select owner from tbl_jackpot_hall_member where t1.useridx = useridx and t1.jackpothallidx = jackpothallidx ),0) as owner from `tbl_jackpot_log` t1 ". 
                   " where writedate >= '$yesterday 00:00:00' and writedate < '$today 00:00:00'";
			$jackpot_list = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($jackpot_list); $i++)
			{
					
			    $output .= '"'.$jackpot_list[$i]["jackpotidx"].'"|';
			    $output .= '"'.$jackpot_list[$i]["useridx"].'"|';
			    $output .= '"'.$jackpot_list[$i]["objectidx"].'"|';				
			    $output .= '"'.$jackpot_list[$i]["slottype"].'"|';				
			    $output .= '"'.$jackpot_list[$i]["amount"].'"|';
			    $output .= '"'.$jackpot_list[$i]["payout_amount"].'"|';
			    $output .= '"'.$jackpot_list[$i]["jackpothallidx"].'"|';
			    $output .= '"'.$jackpot_list[$i]["partyidx"].'"|';
			    $output .= '"'.$jackpot_list[$i]["devicetype"].'"|';
				$output .= '"'.$jackpot_list[$i]["remain_jackpot"].'"|';
				$output .= '"'.$jackpot_list[$i]["betlevel"].'"|';
				$output .= '"'.$jackpot_list[$i]["totalbet"].'"|';
				$output .= '"'.$jackpot_list[$i]["clienttime"].'"|';
				$output .= '"'.$jackpot_list[$i]["fiestaidx"].'"|';
				$output .= '"'.$jackpot_list[$i]["writedate"].'"|';
				$output .="\n";
				
			}
			
			if($output != "")
			{
			  $fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_jackpot_log_".$yesterday_str.".txt", 'a+');
			  
			  fwrite($fp, $output);
			  
			  fclose($fp);
			}
			
			$db_main->end();
			
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		$is_file_exist = file_exists("$DOCUMENT_ROOT/redshift_temp/tbl_jackpot_log_".$yesterday_str.".txt");
		
		if($is_file_exist)
		{		 
			try
			{
				// Create the AWS service builder, providing the path to the config file
				$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
			
				$s3Client = $aws->get('s3');
				$bucket = "wg-redshift/t5/tbl_jackpot_log/$yesterday_str";
			
				$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
				$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_jackpot_log_$yesterday_str.txt";
			
				// Upload an object to Amazon S3
				$result = $s3Client->putObject(array(
						'Bucket' 		=> $bucket,
						'Key'    		=> 'tbl_jackpot_log_'.$yesterday_str.'.txt',
						'ACL'	 		=> 'public-read',
						'SourceFile'   	=> $filepath
				));
			
				$ObjectURL = $result["ObjectURL"];
			
				if($ObjectURL != "")
				{
					@unlink($filepath);
				}
			}
			catch(Exception $e)
			{
				write_log($e->getMessage());
			}
		
			try
			{
				$db_redshift = new CDatabase_Redshift();
	
				// Create a staging table
				$sql = "CREATE TABLE t5_jackpot_log_tmp (LIKE t5_jackpot_log);";
				$db_redshift->execute($sql);
				
				// Load data into the staging table
				$sql = "copy t5_jackpot_log_tmp ".
						"from 's3://wg-redshift/t5/tbl_jackpot_log/$yesterday_str' ".
						"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
						"delimiter '|' ".
						"csv;";
				$db_redshift->execute($sql);
				
				// Insert records
				$sql = "INSERT INTO t5_jackpot_log (jackpotidx,useridx,objectidx,slottype,amount,payout_amount,jackpothallidx,partyidx,devicetype,remain_jackpot,betlevel,totalbet,clienttime,fiestaidx,writedate) ". 
						" (SELECT s.jackpotidx,s.useridx,s.objectidx,s.slottype,s.amount,s.payout_amount,s.jackpothallidx,s.partyidx,s.devicetype,s.remain_jackpot,s.betlevel,s.totalbet,s.clienttime,s.fiestaidx,s.writedate FROM t5_jackpot_log_tmp s );";
				$db_redshift->execute($sql);
					
				// Drop the staging table
				$sql = "DROP TABLE t5_jackpot_log_tmp;";
				$db_redshift->execute($sql);
		
				$db_redshift->end();
			}
			catch(Exception $e)
			{
				write_log($e->getMessage());
			}
		}
?>