<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	$str_useridx = 20000;
	
		$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
		$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
	  
	    try
		{
			$db_main = new CDatabase_Main();
			
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			
			$output = "";
			$sql = "SELECT * FROM `tbl_notification_group_log_new` WHERE senddate >= '$yesterday 00:00:00' AND senddate <= '$yesterday 23:59:59' ";
			$log_list = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($log_list); $i++)
			{
				
			    if($log_list[$i]["senddate"] == "0000-00-00 00:00:00")
			        $log_list[$i]["senddate"] = "1900-01-01 00:00:00";
			        
			    if($log_list[$i]["clickdate"] == "0000-00-00 00:00:00")
			         $log_list[$i]["clickdate"] = "1900-01-01 00:00:00";
			            
                if($log_list[$i]["collectdate"] == "0000-00-00 00:00:00")
			         $log_list[$i]["collectdate"] = "1900-01-01 00:00:00";
			    
			    $output .= '"'.$log_list[$i]["logidx"].'"|';
			    $output .= '"'.$log_list[$i]["useridx"].'"|';
			    $output .= '"'.$log_list[$i]["type"].'"|';				
			    $output .= '"'.$log_list[$i]["group_no"].'"|';				
			    $output .= '"'.$log_list[$i]["inboxidx"].'"|';
			    $output .= '"'.$log_list[$i]["platform"].'"|';
			    $output .= '"'.$log_list[$i]["leavedays"].'"|';
			    $output .= '"'.$log_list[$i]["bonus_coin"].'"|';
			    $output .= '"'.$log_list[$i]["senddate"].'"|';
				$output .= '"'.$log_list[$i]["clickdate"].'"|';
				$output .= '"'.$log_list[$i]["collectdate"].'"|';
				
				$output .="\n";
				
			}
			
			if($output != "")
			{
			  $fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_notification_group_log_new_".$yesterday_str.".txt", 'a+');
			  
			  fwrite($fp, $output);
			  
			  fclose($fp);
			}
			
			$db_main->end();
			
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		 
		try
		{
			// Create the AWS service builder, providing the path to the config file
			$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
		
			$s3Client = $aws->get('s3');
			$bucket = "wg-redshift/t5/tbl_notification_group_log_new/$yesterday_str";
		
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_notification_group_log_new_$yesterday_str.txt";
		
			// Upload an object to Amazon S3
			$result = $s3Client->putObject(array(
					'Bucket' 		=> $bucket,
					'Key'    		=> 'tbl_notification_group_log_new_'.$yesterday_str.'.txt',
					'ACL'	 		=> 'public-read',
					'SourceFile'   	=> $filepath
			));
		
			$ObjectURL = $result["ObjectURL"];
		
			if($ObjectURL != "")
			{
				@unlink($filepath);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
	
		try
		{
			$db_redshift = new CDatabase_Redshift();
		
			// Create a staging table
			$sql = "CREATE TABLE t5_notification_group_log_new_tmp (LIKE t5_notification_group_log_new);";
			$db_redshift->execute($sql);
			
			// Load data into the staging table
			$sql = "copy t5_notification_group_log_new_tmp ".
					"from 's3://wg-redshift/t5/tbl_notification_group_log_new/$yesterday_str' ".
					"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
					"delimiter '|' ".
					"csv;";
			$db_redshift->execute($sql);
			
			// Update records
			$sql = "UPDATE t5_notification_group_log_new ".
			 			"SET clickdate = s.clickdate, collectdate = s.collectdate ".
			 			"FROM t5_notification_group_log_new_tmp s ".
			 			"WHERE t5_notification_group_log_new.logidx = s.logidx;";
			$db_redshift->execute($sql); 
			
			
			// Insert records
			$sql = "INSERT INTO t5_notification_group_log_new (logidx,useridx,type,group_no,inboxidx,platform,leavedays,bonus_coin,senddate,clickdate,collectdate) ". 
					" (SELECT s.logidx,s.useridx,s.type,s.group_no,s.inboxidx,s.platform,s.leavedays,s.bonus_coin,s.senddate,s.clickdate,s.collectdate FROM t5_notification_group_log_new_tmp s );";
			$db_redshift->execute($sql);
				
			// Drop the staging table
			$sql = "DROP TABLE t5_notification_group_log_new_tmp;";
			$db_redshift->execute($sql);
	
			$db_redshift->end();
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
?>