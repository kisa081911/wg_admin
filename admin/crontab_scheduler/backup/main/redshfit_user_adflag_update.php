<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);

		
	try
	{
		$slave_main = new CDatabase_Slave_Main();
		$db_mobile = new CDatabase_Mobile();
		
		$slave_main->execute("SET wait_timeout=7200");
		$db_mobile->execute("SET wait_timeout=7200");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		for($u=0; $u<100; $u++)
		{
			$sql = " SELECT t1.useridx, userid, platform, ".
					"	SUBSTRING(t1.nickname, 1, 100) AS nickname, sex, coin, level,  experience,  honor_level, honor_point, totalspin, totalamount, ".
					"	IFNULL((SELECT vip_level FROM tbl_user_detail WHERE useridx=t1.useridx), 0) AS vip_level,isblock, email, isguest,  ".
					"	SUBSTRING(LOWER(email), 1, 2) AS country, locale, birthday, birthyear, adflag, fbsource, ifcontext, logincount as daylogincount, ".
					"	t1.createdate, t2.logindate  ".
					" FROM tbl_user t1 JOIN tbl_user_ext t2 ON t1.useridx=t2.useridx ".
					" WHERE '$yesterday 00:00:00' <= t1.logindate AND t1.useridx % 100 = $u ";
			
			$user_list = $slave_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($user_list); $i++)
			{
				$output = "";
					
				$useridx = $user_list[$i]["useridx"];
			
				if($user_list[$i]["createdate"] == "0000-00-00 00:00:00")
					$user_list[$i]["createdate"] = "1900-01-01 00:00:00";
			
				if($user_list[$i]["logindate"] == "0000-00-00 00:00:00")
					$user_list[$i]["logindate"] = "1900-01-01 00:00:00";
			
				$sql = "SELECT fb_token FROM tbl_user_fb_token WHERE useridx = $useridx ";
				$fb_token = $slave_main->getvalue($sql);
				
				$sql = "SELECT app_version as mobile_version, push_enabled, device_name as device, os_version, adv_id as advid, t1.device_id as deviceid ".
						" FROM `tbl_mobile` t1 JOIN `tbl_user_mobile_connection_log` t2 ON t1.device_id = t2.device_id WHERE useridx = $useridx";
				$mobile_data = $db_mobile->getarray($sql);
				
				$push_enabled = ($mobile_data["push_enabled"] == "") ? 0 : $mobile_data["push_enabled"];
					
				$output .= '"'.$useridx.'"|';
				$output .= '"'.$user_list[$i]["userid"].'"|';
				$output .= '"'.$user_list[$i]["platform"].'"|';
				$output .= '"'.str_replace("\"", "\"\"", $user_list[$i]["nickname"]).'"|';
				$output .= '"'.$user_list[$i]["sex"].'"|';
				$output .= '"'.$user_list[$i]["coin"].'"|';
				$output .= '"'.$user_list[$i]["level"].'"|';
				$output .= '"'.$user_list[$i]["experience"].'"|';
				$output .= '"'.$user_list[$i]["honor_level"].'"|';
				$output .= '"'.$user_list[$i]["honor_point"].'"|';
				$output .= '"'.$user_list[$i]["totalspin"].'"|';
				$output .= '"'.$user_list[$i]["totalamount"].'"|';
				$output .= '"'.$user_list[$i]["vip_level"].'"|';
				$output .= '"'.$user_list[$i]["isblock"].'"|';
				$output .= '"'.$fb_token.'"|';
				$output .= '"'.str_replace("\"", "\"\"", $user_list[$i]["email"]).'"|';
				$output .= '"'.$user_list[$i]["isguest"].'"|';
				$output .= '"'.$mobile_data["mobile_version"].'"|';
				$output .= '"'.$push_enabled.'"|';
				$output .= '"'.str_replace("\"", "\"\"", $mobile_data["device"]).'"|';
				$output .= '"'.$mobile_data["os_version"].'"|';
				$output .= '"'.$mobile_data["advid"].'"|';
				$output .= '"'.$mobile_data["deviceid"].'"|';
				$output .= '"'.$user_list[$i]["country"].'"|';
				$output .= '"'.$user_list[$i]["locale"].'"|';
				$output .= '"'.$user_list[$i]["birthday"].'"|';
				$output .= '"'.$user_list[$i]["birthyear"].'"|';
				$output .= '"'.str_replace("\"", "\"\"", $user_list[$i]["adflag"]).'"|';
				$output .= '"'.str_replace("\"", "\"\"", $user_list[$i]["fbsource"]).'"|';
				$output .= '"'.str_replace("\"", "\"\"", $user_list[$i]["ifcontext"]).'"|';
				$output .= '"'.$user_list[$i]["daylogincount"].'"|';
				$output .= '"'.$user_list[$i]["createdate"].'"|';
				$output .= '"'.$user_list[$i]["logindate"].'"|';
				$output .="\n";
					
				if($output != "")
				{
					$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_adflag_$yesterday_str.txt", 'a+');
						
					fwrite($fp, $output);
						
					fclose($fp);
				}
			}
		}
		
		$slave_main->end();
		$db_mobile->end();		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_adflag/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_adflag_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_adflag_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_tmp (LIKE t5_user);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy t5_user_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_adflag/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
		// Update records
		$sql = "UPDATE t5_user ".
				"SET adflag = s.adflag, fbsource = s.fbsource, ifcontext = s.ifcontext ".
				"FROM t5_user_tmp s ".
				"WHERE t5_user.useridx = s.useridx;";
		$db_redshift->execute($sql);

		// Drop the staging table
		$sql = "DROP TABLE t5_user_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>