<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/redshift_user_mobile") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/redshift_user_mobile") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("backup/redshift_user_mobile Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);

		
	try
	{
		$db_mobile = new CDatabase_Mobile();
		
		$db_mobile->execute("SET wait_timeout=7200");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		for($u=0; $u<10; $u++)
		{
			$sql = "SELECT useridx, app_version as mobile_version, push_enabled, device_name as device, os_version, adv_id as advid, t1.device_id as deviceid, t1.createdate AS createdate, logindate ".
						" FROM `tbl_mobile` t1 JOIN `tbl_user_mobile_connection_log` t2 ON t1.device_id = t2.device_id WHERE  '$yesterday 00:00:00' <= logindate AND useridx % 10 = $u ";
			$mobile_data = $db_mobile->gettotallist($sql);
			
			for($i=0; $i<sizeof($mobile_data); $i++)
			{
				$output = "";
					
				$useridx = $mobile_data[$i]["useridx"];
			
				if($mobile_data[$i]["createdate"] == "0000-00-00 00:00:00")
					$mobile_data[$i]["createdate"] = "1900-01-01 00:00:00";
			
				if($mobile_data[$i]["logindate"] == "0000-00-00 00:00:00")
					$mobile_data[$i]["logindate"] = "1900-01-01 00:00:00";
				
				$push_enabled = ($mobile_data[$i]["push_enabled"] == "") ? 0 : $mobile_data[$i]["push_enabled"];
					
				$output .= '"'.$useridx.'"|';
				$output .= '"'.$mobile_data[$i]["deviceid"].'"|';				
				$output .= '"'.$mobile_data[$i]["mobile_version"].'"|';
				$output .= '"'.$push_enabled.'"|';
				$output .= '"'.str_replace("\"", "\"\"", $mobile_data[$i]["device"]).'"|';
				$output .= '"'.$mobile_data[$i]["os_version"].'"|';
				$output .= '"'.$mobile_data[$i]["advid"].'"|';				
				$output .= '"'.$mobile_data[$i]["createdate"].'"|';
				$output .= '"'.$mobile_data[$i]["logindate"].'"|';
				$output .="\n";
					
				if($output != "")
				{
					$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_mobile_$yesterday_str.txt", 'a+');
						
					fwrite($fp, $output);
						
					fclose($fp);
				}
			}
		}
		
		$db_mobile->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_mobile/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_mobile_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_mobile_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_mobile_tmp (LIKE t5_user_mobile);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy t5_user_mobile_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_mobile/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
		// Update records
		$sql = "UPDATE t5_user_mobile ".
				"SET  mobile_version = s.mobile_version, push_enabled = s.push_enabled, device = s.device, os_version = s.os_version, advid = s.advid, createdate = s.createdate, logindate = s.logindate ".
				"FROM t5_user_mobile_tmp s ".
				"WHERE t5_user_mobile.useridx = s.useridx AND t5_user_mobile.deviceid = s.deviceid;";
		$db_redshift->execute($sql);

		// Insert records
		$sql = "INSERT INTO t5_user_mobile ".
				"SELECT s.* FROM t5_user_mobile_tmp s LEFT JOIN t5_user_mobile ".
				"ON s.useridx = t5_user_mobile.useridx AND s.deviceid = t5_user_mobile.deviceid ".
				"WHERE t5_user_mobile.useridx IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_user_mobile_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>