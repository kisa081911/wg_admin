<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$today = date("Y-m-d");
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
	
	try
	{
		$db_slave = new CDatabase_Slave_Main();
		
		$db_slave->execute("SET wait_timeout=3600");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_remove'";
		$columns_count = $db_slave->getvalue($sql);
		
		$sql = 	"SELECT useridx, removedate ".
				"FROM tbl_user_remove ".
				"WHERE '$yesterday 00:00:00' <= removedate";
		$log_list = $db_slave->gettotallist($sql);
		
		$output = "";
		
		for($i=0; $i<sizeof($log_list); $i++)
		{
			for($j=0; $j<$columns_count; $j++)
			{
				$log_list[$i][$j] = str_replace("\"", "\"\"", $log_list[$i][$j]);
				
				if($log_list[$i][$j] == "0000-00-00 00:00:00")
					$log_list[$i][$j] = "1900-01-01 00:00:00";
				
				if($j == $columns_count - 1)
					$output .= '"'.$log_list[$i][$j].'"';
				else
					$output .= '"'.$log_list[$i][$j].'"|';
			}
				
			$output .="\n";
		}
		
			
		if($output != "")
		{
			$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_remove_$yesterday_str.txt", 'a+');
	
			fwrite($fp, $output);
	
			fclose($fp);
		}
		
		$db_slave->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	

	$is_file_exist = file_exists("$DOCUMENT_ROOT/redshift_temp/tbl_user_remove_".$yesterday_str.".txt");
	
	if($is_file_exist)
	{	
		try
		{
			// Create the AWS service builder, providing the path to the config file
			$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
		
			$s3Client = $aws->get('s3');
			$bucket = "wg-redshift/t5/tbl_user_remove/$yesterday_str";
		
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_remove_$yesterday_str.txt";
		
			// Upload an object to Amazon S3
			$result = $s3Client->putObject(array(
					'Bucket' 		=> $bucket,
					'Key'    		=> 'tbl_user_remove_'.$yesterday_str.'.txt',
					'ACL'	 		=> 'public-read',
					'SourceFile'   	=> $filepath
			));
		
			$ObjectURL = $result["ObjectURL"];
		
			if($ObjectURL != "")
			{
				@unlink($filepath);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		try
		{
			$db_redshift = new CDatabase_Redshift();
		
			// Create a staging table
			$sql = "CREATE TABLE t5_user_remove_tmp (LIKE t5_user_remove);";
			$db_redshift->execute($sql);
		
			// Load data into the staging table
			$sql = "copy t5_user_remove_tmp ".
					"from 's3://wg-redshift/t5/tbl_user_remove/$yesterday_str' ".
					"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
					"delimiter '|' ".
					"csv;";
			$db_redshift->execute($sql);
		
			// Update records
			$sql = "UPDATE t5_user_remove ".
					"SET useridx = s.useridx, removedate = s.removedate ".				
					"FROM t5_user_remove_tmp s ".
					"WHERE t5_user_remove.useridx = s.useridx;";
			$db_redshift->execute($sql);
			
			// Insert records
			$sql = "INSERT INTO t5_user_remove ".
					"SELECT s.* FROM t5_user_remove_tmp s LEFT JOIN t5_user_remove ".
					"ON s.useridx = t5_user_remove.useridx ".
					"WHERE t5_user_remove.useridx IS NULL;";
			$db_redshift->execute($sql);
				
			// Drop the staging table
			$sql = "DROP TABLE t5_user_remove_tmp;";
			$db_redshift->execute($sql);
		
			$db_redshift->end();
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
	}	
?>