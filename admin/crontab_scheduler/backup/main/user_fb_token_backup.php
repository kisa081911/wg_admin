<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");

	$today = date("Y-m-d");
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
	
	try
	{
		$db_main = new CDatabase_Main();
		
		$db_main->execute("SET wait_timeout=3600");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];

		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_fb_token'";
		$columns_count = $db_main->getvalue($sql);
		
		$sql = "SELECT * FROM tbl_user_fb_token WHERE '$yesterday 00:00:00' <= createdate AND createdate < '$today 00:00:00'";
		$log_list = $db_main->gettotallist($sql);
		
		$db_main->end();
		
		$output = "";
		
		for($i=0; $i<sizeof($log_list); $i++)
		{
			for($j=0; $j<$columns_count; $j++)
			{
				if($j == $columns_count - 1)
					$output .= '"'.$log_list[$i][$j].'"';
				else
					$output .= '"'.$log_list[$i][$j].'"|';
			}
				
			$output .="\n";
		}
		
		if($output != "")
		{
			$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_fb_token_$yesterday_str.txt", 'a+');
			
			fwrite($fp, $output);
			
			fclose($fp);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/take5/user_fb_token/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_fb_token_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_fb_token_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		$sql = "SELECT COUNT(*) FROM t5_user_fb_token WHERE '$yesterday 00:00:00' <= createdate AND createdate <= '$yesterday 23:59:59'";
	
		$row_count = $db_redshift->getvalue($sql);
	
		if($row_count == 0)
		{
			$sql = "copy t5_user_fb_token ".
					"from 's3://wg-redshift/take5/user_fb_token/$yesterday_str' ".
					"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
					"delimiter '|' ".
					"csv;";
				
			$db_redshift->execute($sql);
		}
	
		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>