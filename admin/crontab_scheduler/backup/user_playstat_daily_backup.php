<?
	include("../../common/common_include.inc.php");

	$db_other = new CDatabase_Other();
	$db_other_bak = new CDatabase_Otherbak();
	
	$db_other->execute("SET wait_timeout=3600");
	$db_other_bak->execute("SET wait_timeout=3600");
	
	$today = date("Y-m-d");

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/backup/user_playstat_daily_backup") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/backup/user_playstat_daily_backup") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("user_playstat_daily_backup Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	try
	{
		$yearmonth = date("Ym", time() - 60 * 60 * 24 * 1);
		$year_month = date("Y-m", time() - 60 * 60 * 24 * 1);
		
		$sql = "SELECT COUNT(*) AS COUNT FROM `information_schema`.`TABLES` WHERE TABLE_SCHEMA='takefive_other_bak' AND TABLE_NAME = 'tbl_user_playstat_daily_$yearmonth'";
		$is_exists = $db_other_bak->getvalue($sql);
		
		if($is_exists == 0) // 테이블이 존재하지 않는 경우
		{
			$sql = 	" CREATE TABLE `tbl_user_playstat_daily_$yearmonth` ( ".
					"   `statidx` bigint(20) NOT NULL AUTO_INCREMENT, ".
					"   `today` date NOT NULL,   ".
					"   `useridx` bigint(20) NOT NULL,  ".
					"   `moneyin` bigint(20) NOT NULL,  ".
					"   `moneyout` bigint(20) NOT NULL,  ".
					"   `winrate` smallint(6) NOT NULL,  ".
					"   `bigwin` bigint(20) NOT NULL,  ".
					"   `playtime` int(11) NOT NULL,  ".
					"   `playcount` int(11) NOT NULL,  ".
					"   `h_moneyin` bigint(20) NOT NULL DEFAULT '0', ".
					"   `h_moneyout` bigint(20) NOT NULL DEFAULT '0', ".
					"   `h_winrate` smallint(6) NOT NULL DEFAULT '0', ".
					"   `h_bigwin` bigint(20) NOT NULL DEFAULT '0', ".
					"   `h_playtime` int(11) NOT NULL DEFAULT '0', ".
					"   `h_playcount` int(11) NOT NULL DEFAULT '0', ".
					"   `treatamount` bigint(20) NOT NULL DEFAULT '0', ".
					"   `currentcoin` bigint(20) DEFAULT NULL, ".
					"   `purchasecount` int(11) NOT NULL,  ".
					"   `purchaseamount` int(11) NOT NULL,  ".
					"   `purchasecoin` bigint(20) NOT NULL,  ".
					"   `days_after_purchase` int(11) NOT NULL, ".
					"   `freecoin` bigint(20) NOT NULL,  ".
					"   `jackpot` bigint(20) NOT NULL,  ".
					"   `vip_level` tinyint(4) unsigned NOT NULL, ".
					"   `vip_point` int(11) NOT NULL,  ".
					"   `famelevel` tinyint(4) unsigned NOT NULL, ".
					"   `exp` int(11) NOT NULL,   ".
					"   `ty_point` int(11) NOT NULL,  ".
					"   `friend_count` smallint(6) NOT NULL, ".
					"   `crowntype` tinyint(4) NOT NULL,  ".
					"   `days_after_install` int(11) NOT NULL, ".
					"   PRIMARY KEY (`statidx`),   ".
					"   KEY `today` (`today`,`useridx`)  ".
					" ) ENGINE=InnoDB DEFAULT CHARSET=utf8; ";
			
			$db_other_bak->execute($sql);
		}

		$sql = "SELECT logidx FROM tbl_backup_stat WHERE category = 20";
		$lastidx = $db_other_bak->getvalue($sql);
		
		$sql = "SELECT MAX(statidx) ".
				"FROM tbl_user_playstat_daily ".
				"WHERE statidx > $lastidx AND '$year_month-01 00:00:00' <= today AND today < DATE_ADD('$year_month-01 00:00:00', INTERVAL 1 MONTH)";
		$maxidx = $db_other->getvalue($sql);
		
		$totalcount = $maxidx - $lastidx;
		
		while($totalcount > 0)
		{
			$sql = "SELECT logidx FROM tbl_backup_stat WHERE category = 20";
			$lastidx = $db_other_bak->getvalue($sql);

			$sql = "SELECT statidx, today, useridx,moneyin, moneyout, winrate, bigwin, playtime, playcount, ".
					"h_moneyin, h_moneyout, h_winrate, h_bigwin,h_playtime, h_playcount, treatamount, ".
					"currentcoin, purchasecount, purchaseamount, purchasecoin, days_after_purchase, freecoin, ".
					"jackpot, vip_level, vip_point, famelevel, exp, ty_point, friend_count, crowntype, days_after_install ".
					"FROM `tbl_user_playstat_daily` ".
					"WHERE statidx > $lastidx AND '$year_month-01 00:00:00' <= today AND today < DATE_ADD('$year_month-01 00:00:00', INTERVAL 1 MONTH) ".
					"ORDER BY statidx ASC ".
					"LIMIT 5000";
			$daily_list = $db_other->gettotallist($sql);
				
			$sql = "";
			$insertcount = 0;
				
			for($i=0; $i<sizeof($daily_list); $i++)
			{
				$statidx   = $daily_list[$i]["statidx"];            
				$today   = $daily_list[$i]["today"];             
				$useridx   = $daily_list[$i]["useridx"];               
				$moneyin   = $daily_list[$i]["moneyin"];               
				$moneyout   = $daily_list[$i]["moneyout"];              
				$winrate   = $daily_list[$i]["winrate"];               
				$bigwin   = $daily_list[$i]["bigwin"];                
				$playtime   = $daily_list[$i]["playtime"];              
				$playcount   = $daily_list[$i]["playcount"];             
				$h_moneyin   = $daily_list[$i]["h_moneyin"];             
				$h_moneyout  = $daily_list[$i]["h_moneyout"];            
				$h_winrate   = $daily_list[$i]["h_winrate"];             
				$h_bigwin   = $daily_list[$i]["h_bigwin"];              
				$h_playtime  = $daily_list[$i]["h_playtime"];            
				$h_playcount  = $daily_list[$i]["h_playcount"];           
				$treatamount  = $daily_list[$i]["treatamount"];           
				$currentcoin  = $daily_list[$i]["currentcoin"];           
				$purchasecount  = $daily_list[$i]["purchasecount"];         
				$purchaseamount  = $daily_list[$i]["purchaseamount"];        
				$purchasecoin  = $daily_list[$i]["purchasecoin"];          
				$days_after_purchase = $daily_list[$i]["days_after_purchase"];   
				$freecoin   = $daily_list[$i]["freecoin"];              
				$jackpot   = $daily_list[$i]["jackpot"];               
				$vip_level   = $daily_list[$i]["vip_level"];             
				$vip_point   = $daily_list[$i]["vip_point"];             
				$famelevel   = $daily_list[$i]["famelevel"];             
				$exp     = $daily_list[$i]["exp"];                   
				$ty_point   = $daily_list[$i]["ty_point"];              
				$friend_count  = $daily_list[$i]["friend_count"];          
				$crowntype   = $daily_list[$i]["crowntype"];             
				$days_after_install = $daily_list[$i]["days_after_install"];
				
		
				if ($insertcount == 0)
				{
					$sql = 	"INSERT INTO tbl_user_playstat_daily_$yearmonth(`statidx`, `today`, `useridx`,`moneyin`,`moneyout`, `winrate`, `bigwin`, `playtime`, `playcount`, `h_moneyin`, `h_moneyout`, `h_winrate`, `h_bigwin`, `h_playtime`, `h_playcount`, `treatamount`, ".
							"`currentcoin`, `purchasecount`, `purchaseamount`, `purchasecoin`, `days_after_purchase`, `freecoin`, `jackpot`, `vip_level`, `vip_point`, `famelevel`, `EXP`, `ty_point`, `friend_count`, `crowntype`, `days_after_install`)  ".
							"VALUES($statidx, '$today', $useridx, $moneyin, $moneyout, $winrate, $bigwin, $playtime, $playcount, $h_moneyin, $h_moneyout, $h_winrate, $h_bigwin, $h_playtime, $h_playcount, $treatamount, $currentcoin, $purchasecount, $purchaseamount,".
							" $purchasecoin, $days_after_purchase, $freecoin, $jackpot, $vip_level, $vip_point, $famelevel, $exp, $ty_point, $friend_count, $crowntype, $days_after_install )";
							
		
					$insertcount++;
				}
				else if ($insertcount < 250)
				{
					$sql .= ", ($statidx, '$today', $useridx, $moneyin, $moneyout, $winrate, $bigwin, $playtime, $playcount, $h_moneyin, $h_moneyout, $h_winrate, $h_bigwin, $h_playtime, $h_playcount, $treatamount, $currentcoin, $purchasecount, $purchaseamount,".
							" $purchasecoin, $days_after_purchase, $freecoin, $jackpot, $vip_level, $vip_point, $famelevel, $exp, $ty_point, $friend_count, $crowntype, $days_after_install )";
							
					$insertcount++;
				}
				else
				{
					$sql .= ", ($statidx, '$today', $useridx, $moneyin, $moneyout, $winrate, $bigwin, $playtime, $playcount, $h_moneyin, $h_moneyout, $h_winrate, $h_bigwin, $h_playtime, $h_playcount, $treatamount, $currentcoin, $purchasecount, $purchaseamount,".
							" $purchasecoin, $days_after_purchase, $freecoin, $jackpot, $vip_level, $vip_point, $famelevel, $exp, $ty_point, $friend_count, $crowntype, $days_after_install ) ON DUPLICATE KEY UPDATE statidx=VALUES(statidx);";
					$insertcount = 0;
		
					$db_other_bak->execute($sql);
		
					$sql = "";
				}
			}
			
			if($totalcount%100000 == 0)
				sleep(1);
				
			if($sql != "")
			{
				$sql .= " ON DUPLICATE KEY UPDATE statidx=VALUES(statidx);";
				$db_other_bak->execute($sql);
			}
				
			$totalcount -= 5000;
				
			$sql = "UPDATE tbl_backup_stat ".
					"SET logidx = (SELECT MAX(statidx) FROM tbl_user_playstat_daily_$yearmonth) ".
					"WHERE category = 20";
			$db_other_bak->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_other->end();
	$db_other_bak->end();
?>