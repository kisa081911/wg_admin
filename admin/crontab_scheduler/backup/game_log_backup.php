<?
	include("../../common/common_include.inc.php");

	$db_other = new CDatabase_Other();
	$db_other_bak = new CDatabase_Otherbak();
	
	$db_other->execute("SET wait_timeout=3600");
	$db_other_bak->execute("SET wait_timeout=3600");
	
	$today = date("Y-m-d");

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/backup/game_log_backup") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/backup/game_log_backup") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("game_log_backup Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	try
	{
		$yearmonth = date("Ym", time() - 60 * 60 * 24 * 1);
		$year_month = date("Y-m", time() - 60 * 60 * 24 * 1);
		
		$sql = "SELECT COUNT(*) AS COUNT FROM `information_schema`.`TABLES` WHERE TABLE_SCHEMA='takefive_other_bak' AND TABLE_NAME = 'tbl_user_gamelog_$yearmonth'";
		$is_exists = $db_other_bak->getvalue($sql);
		
		if($is_exists == 0) // 테이블이 존재하지 않는 경우
		{
			$sql = "CREATE TABLE `tbl_user_gamelog_$yearmonth` ( ".
  					"	`logidx` BIGINT(20) NOT NULL AUTO_INCREMENT, ".
  					"	`useridx` BIGINT(20) NOT NULL, ".
  					"	`slottype` INT(4) NOT NULL, ".
  					"	`betlevel` TINYINT(6) NOT NULL DEFAULT '0', ".
  					"	`mode` TINYINT(6) NOT NULL DEFAULT '0', ".
  					"	`moneyin` BIGINT(20) NOT NULL, ".
  					"	`moneyout` BIGINT(20) NOT NULL, ".
  					"	`treatamount` bigint(20) NOT NULL DEFAULT '0',".
  					"	`playcount` INT(4) NOT NULL, ".
  					"	`writedate` DATETIME NOT NULL, ".
  					"	PRIMARY KEY (`logidx`), ".
  					"	KEY `useridx` (`useridx`), ".
  					"	KEY `slottype` (`slottype`), ".
  					"	KEY `writedate` (`writedate`) ".
					") ENGINE=INNODB DEFAULT CHARSET=utf8;";
			
			$db_other_bak->execute($sql);
		}

		$sql = "SELECT logidx FROM tbl_backup_stat WHERE category = 1";
		$lastidx = $db_other_bak->getvalue($sql);
		
		$sql = "SELECT MAX(logidx) ".
				"FROM tbl_user_gamelog ".
				"WHERE logidx > $lastidx AND '$year_month-01 00:00:00' <= writedate AND writedate < DATE_ADD('$year_month-01 00:00:00', INTERVAL 1 MONTH)";
		$maxidx = $db_other->getvalue($sql);
		
		$totalcount = $maxidx - $lastidx;
		
		while($totalcount > 0)
		{
			$sql = "SELECT logidx FROM tbl_backup_stat WHERE category = 1";
			$lastidx = $db_other_bak->getvalue($sql);

			$sql = "SELECT logidx, useridx, slottype, betlevel, mode, moneyin, moneyout, treatamount, playcount, writedate ".
					"FROM `tbl_user_gamelog` ".
					"WHERE logidx > $lastidx AND '$year_month-01 00:00:00' <= writedate AND writedate < DATE_ADD('$year_month-01 00:00:00', INTERVAL 1 MONTH) ".
					"ORDER BY logidx ASC ".
					"LIMIT 5000";
			$log_list = $db_other->gettotallist($sql);
				
			$sql = "";
			$insertcount = 0;
				
			for($i=0; $i<sizeof($log_list); $i++)
			{
				$logidx = $log_list[$i]["logidx"];
				$useridx = $log_list[$i]["useridx"];
				$slottype = $log_list[$i]["slottype"];
				$betlevel = $log_list[$i]["betlevel"];
				$mode = $log_list[$i]["mode"];
				$moneyin = $log_list[$i]["moneyin"];
				$moneyout = $log_list[$i]["moneyout"];
				$treatamount = $log_list[$i]["treatamount"];
				$playcount = $log_list[$i]["playcount"];
				$writedate = $log_list[$i]["writedate"];
		
				if ($insertcount == 0)
				{
					$sql = "INSERT INTO tbl_user_gamelog_$yearmonth(logidx, useridx, slottype, betlevel, mode, moneyin, moneyout, treatamount, playcount, writedate) VALUES($logidx, $useridx, $slottype, $betlevel, $mode, $moneyin, $moneyout, $treatamount, $playcount, '$writedate')";
		
					$insertcount++;
				}
				else if ($insertcount < 250)
				{
					$sql .= ", ($logidx, $useridx, $slottype, $betlevel, $mode, $moneyin, $moneyout, $treatamount, $playcount, '$writedate')";
					$insertcount++;
				}
				else
				{
					$sql .= ", ($logidx, $useridx, $slottype, $betlevel, $mode, $moneyin, $moneyout, $treatamount, $playcount, '$writedate') ON DUPLICATE KEY UPDATE logidx=VALUES(logidx);";
					$insertcount = 0;
		
					$db_other_bak->execute($sql);
		
					$sql = "";
				}
			}
			
			if($totalcount%100000 == 0)
				sleep(1);
				
			if($sql != "")
			{
				$sql .= " ON DUPLICATE KEY UPDATE logidx=VALUES(logidx);";
				$db_other_bak->execute($sql);
			}
				
			$totalcount -= 5000;
				
			$sql = "UPDATE tbl_backup_stat ".
					"SET logidx = (SELECT MAX(logidx) FROM tbl_user_gamelog_$yearmonth) ".
					"WHERE category = 1";
			$db_other_bak->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_other->end();
	$db_other_bak->end();
?>