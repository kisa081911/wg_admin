<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	
	$sdate = "2019-12-17";
	$edate = "2019-12-24";
	
	while($sdate <= $edate)
	{
		$yesterday = $sdate;
		$today = date('Y-m-d', strtotime($sdate.' + 1 day'));
		$yesterday_str = date("Ymd", strtotime($yesterday));
		
		write_log("rebackup_product_all : $yesterday|$today|$yesterday_str");

		try
		{
			$db_other = new CDatabase_Other();
			
			$db_other->execute("SET wait_timeout=3600");
			
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
			$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_product_order_all'";
			$columns_count = $db_other->getvalue($sql);
			
			$sql = "SELECT * FROM tbl_product_order_all WHERE ((status = 1 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00') OR (STATUS = 2 AND canceldate >= DATE_SUB('$today 00:00:00', INTERVAL 2 WEEK)) OR (STATUS = 3 AND canceldate >= DATE_SUB('$today 00:00:00', INTERVAL 5 WEEK)) ) AND orderidx != 1568347";
			write_log($sql);
			$log_list = $db_other->gettotallist($sql);
			
			$db_other->end();
			
			$output = "";
			
			for($i=0; $i<sizeof($log_list); $i++)
			{
				for($j=0; $j<$columns_count; $j++)
				{
					$log_list[$i][$j] = str_replace("\"", "\"\"", $log_list[$i][$j]);
					
					if($log_list[$i][$j] == "0000-00-00 00:00:00")
						$log_list[$i][$j] = "1900-01-01 00:00:00";
					
					if($j == $columns_count - 1)
						$output .= '"'.$log_list[$i][$j].'"';
					else
						$output .= '"'.$log_list[$i][$j].'"|';
				}
					
				$output .="\n";
			}
			
			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_product_order_all_$yesterday_str.txt", 'a+');
				
				fwrite($fp, $output);
				
				fclose($fp);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		try
		{
			// Create the AWS service builder, providing the path to the config file
			$aws = Aws::factory('../../common/aws_sdk/aws_config.php');
		
			$s3Client = $aws->get('s3');
			$bucket = "wg-redshift/take5/product_order_all/$yesterday_str";
		
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_product_order_all_$yesterday_str.txt";
		
			// Upload an object to Amazon S3
			$result = $s3Client->putObject(array(
					'Bucket' 		=> $bucket,
					'Key'    		=> 'tbl_product_order_all_'.$yesterday_str.'.txt',
					'ACL'	 		=> 'public-read',
					'SourceFile'   	=> $filepath
			));
		
			$ObjectURL = $result["ObjectURL"];
		
			if($ObjectURL != "")
			{
				@unlink($filepath);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		try
		{
			$db_redshift = new CDatabase_Redshift();
			
			$sql = "DELETE FROM t5_product_order_all WHERE '$yesterday 00:00:00' <= writedate AND writedate <= '$yesterday 23:59:59'";
			$db_redshift->execute($sql);
			
			sleep(1);

			// Create a staging table 
			$sql = "CREATE TABLE t5_product_order_all_tmp (LIKE t5_product_order_all);";
			$db_redshift->execute($sql);
			
			// Load data into the staging table 
			$sql = "copy t5_product_order_all_tmp ".
					"from 's3://wg-redshift/take5/product_order_all/$yesterday_str' ".
					"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
					"delimiter '|' ".
					"csv;";
			$db_redshift->execute($sql);
			
			// Update records 
			$sql = "UPDATE t5_product_order_all ".
					"SET status = s.status, canceldate = s.canceldate, cancelleftcoin = s.cancelleftcoin ".
					"FROM t5_product_order_all_tmp s ". 
					"WHERE t5_product_order_all.orderidx = s.orderidx;";
			$db_redshift->execute($sql);
			
			// Insert records
			$sql = "INSERT INTO t5_product_order_all ". 
					"SELECT s.* FROM t5_product_order_all_tmp s LEFT JOIN t5_product_order_all ". 
					"ON s.orderidx = t5_product_order_all.orderidx ".
					"WHERE t5_product_order_all.orderidx IS NULL;";
			$db_redshift->execute($sql);
			
			// Drop the staging table
			$sql = "DROP TABLE t5_product_order_all_tmp;";
			$db_redshift->execute($sql);
		
			$db_redshift->end();
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));
	}
?>