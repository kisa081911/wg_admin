<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/redshfit_user_action_log_backup") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/redshfit_user_action_log_backup") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("backup/redshfit_user_action_log_backup Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	
	$sdate = date('Y-m-d', strtotime('2019-08-07'));
	$edate = date('Y-m-d', strtotime('2019-08-08'));
	
	while($sdate < $edate)
	{
	    $temp_date = date('Y-m-d', strtotime($sdate.' + 1 day'));
	    
	    $join_sdate = "$sdate";
	    $join_edate = "$temp_date";
	    
	    $yesterday = $join_sdate;
	    $today = $join_edate;
	    $yesterday_str = date('Ymd', strtotime($yesterday))."_str_back";
      
        try
    	{
    		$db_analysis = new CDatabase_Analysis();
    		
    		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
    		
    		for($u=6; $u<10; $u++)  
    		{
    			$output = "";
    			$sql = "SELECT actionidx, ipaddress, facebookid, category, category_info, action, staytime, isplaynow, coin, is_v2, writedate ".
    			       "FROM user_action_log_$u WHERE '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'";
    			$log_list = $db_analysis->gettotallist($sql);  
    			
    			for($i=0; $i<sizeof($log_list); $i++)
    			{
    					
    				if($log_list[$i]["writedate"] == "0000-00-00 00:00:00")
    					$log_list[$i]["writedate"] = "1900-01-01 00:00:00"; 
    
    				$output .= '"'.$log_list[$i]["actionidx"].'"|';
    				$output .= '"'.$log_list[$i]["ipaddress"].'"|';
    				$output .= '"'.$log_list[$i]["facebookid"].'"|';				
    				$output .= '"'.encode_db_field($log_list[$i]["category"]).'"|';				
    				$output .= '"'.encode_db_field($log_list[$i]["category_info"]).'"|';				
    				$output .= '"'.encode_db_field($log_list[$i]["action"]).'"|';							
    				$output .= '"'.$log_list[$i]["staytime"].'"|';	 			
    				$output .= '"'.$log_list[$i]["isplaynow"].'"|';				
    				$output .= '"'.$log_list[$i]["coin"].'"|';
    				$output .= '"'.$log_list[$i]["is_v2"].'"|';
    				$output .= '"'.$log_list[$i]["writedate"].'"|';
    				
    				$output .="\n";
    				
    			}
    			
    			if($output != "")
    			{
    			  $fp = fopen("$DOCUMENT_ROOT/redshift_temp/t5_action_log_".$yesterday_str.".txt", 'a+');
    			  
    			  fwrite($fp, $output);
    			  
    			  fclose($fp);
    			}
    		}
    		
    		$db_analysis->end();
    		
    	}
    	catch(Exception $e)
    	{
    		write_log($e->getMessage());
    	} 
    	 
    	try
    	{
    		// Create the AWS service builder, providing the path to the config file
    		$aws = Aws::factory('../../common/aws_sdk/aws_config.php');
    	
    		$s3Client = $aws->get('s3');
    		$bucket = "wg-redshift/t5/t5_action_log/$yesterday_str";
    	
    		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
    		$filepath = $DOCUMENT_ROOT."/redshift_temp/t5_action_log_$yesterday_str.txt";
    	
    		// Upload an object to Amazon S3
    		$result = $s3Client->putObject(array(
    				'Bucket' 		=> $bucket,
    				'Key'    		=> 't5_action_log_'.$yesterday_str.'.txt',
    				'ACL'	 		=> 'public-read',
    				'SourceFile'   	=> $filepath
    		));
    	
    		$ObjectURL = $result["ObjectURL"];
    	
    		if($ObjectURL != "")
    		{
    			@unlink($filepath);
    		}
    	}
    	catch(Exception $e)
    	{
    		write_log($e->getMessage());
    	}
    
    	try
    	{
    		$db_redshift = new CDatabase_Redshift();
    	
    		// Create a staging table
    		$sql = "CREATE TABLE t5_user_action_log_tmp (LIKE t5_user_action_log);";
    		$db_redshift->execute($sql);
    		
    		// Load data into the staging table
    		$sql = "copy t5_user_action_log_tmp ".
    				"from 's3://wg-redshift/t5/t5_action_log/$yesterday_str' ".
    				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
    				"delimiter '|' ".
    				"csv;";
    		$db_redshift->execute($sql);
    		
    		// Insert records
    		$sql = "INSERT INTO t5_user_action_log (actionidx, ipaddress, facebookid, category, category_info, action, staytime, isplaynow, coin, is_v2, writedate) ". 
    				" (SELECT s.actionidx, s.ipaddress, s.facebookid, s.category,  s.category_info, s.action, s.staytime, s.isplaynow, s.coin, s.is_v2, s.writedate FROM t5_user_action_log_tmp s );";
    		$db_redshift->execute($sql);
    			
    		// Drop the staging table
    		$sql = "DROP TABLE t5_user_action_log_tmp;"; 
    		$db_redshift->execute($sql);
    
    		$db_redshift->end();
    	}
    	catch(Exception $e)
    	{
    		write_log($e->getMessage());
    	}
    	
    	write_log("$yesterday ~ $today Redshift action_log");
    	
    	$sdate = $temp_date;
    	
	}
		
?>