<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';

	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	$db_other->execute("SET wait_timeout=72000");
	
	ini_set("memory_limit", "-1");
	
	$startdate = "2019-06-01";
	$enddate = "2019-07-01";
	$insert_table = "tbl_user_playstat_daily_201906";
	
	while($startdate < $enddate)
	{
		$temp_date = date('Y-m-d', strtotime($startdate.' + 1 day'));

		write_log("$startdate ~ $temp_date : reverse playstat");
		
		for($i=0; $i<4; $i++)
		{
			$db_table = "";
			
			if($i == 0)
				$db_table = "t5_user_playstat_daily";
			else if($i == 1)
				$db_table = "t5_user_playstat_daily_ios";
			else if($i == 2)
				$db_table = "t5_user_playstat_daily_android";
			else if($i == 3)
				$db_table = "t5_user_playstat_daily_amazon";			
			
			$sql = "select ".
					"   today, $i as platform, useridx, moneyin, moneyout, winrate, bigwin, playtime, playcount, h_moneyin, h_moneyout, h_winrate, h_bigwin, h_playtime, h_playcount, treatamount, redeem, currentcoin, ".
					"   purchasecount, purchaseamount, purchasecoin, days_after_purchase, freecoin, jackpot, vip_level, vip_point, famelevel, exp, ty_point, friend_count, crowntype, days_after_install, nvl(bet_rate, 0) AS bet_rate ".
					"from $db_table where '$startdate' <= today  and today < '$temp_date'";
			$data_list = $db_redshift->gettotallist($sql);
			
			$insert_sql = "";
			
			$insertcount = 0;
			
			for($j=0; $j<sizeof($data_list); $j++)
			{
				$today = $data_list[$j]["today"];
				$platform = $data_list[$j]["platform"];
				$useridx = $data_list[$j]["useridx"];
				
				$moneyin = $data_list[$j]["moneyin"];
				$moneyout = $data_list[$j]["moneyout"];			
				$winrate = $data_list[$j]["winrate"];
				$bigwin = $data_list[$j]["bigwin"];
				$playtime = $data_list[$j]["playtime"];
				$playcount = $data_list[$j]["playcount"];
				
				$h_moneyin = $data_list[$j]["h_moneyin"];
				$h_moneyout = $data_list[$j]["h_moneyout"];
				$h_winrate = $data_list[$j]["h_winrate"];
				$h_bigwin = $data_list[$j]["h_bigwin"];
				$h_playtime = $data_list[$j]["h_playtime"];			
				$h_playcount = $data_list[$j]["h_playcount"];
				
				$treatamount = $data_list[$j]["treatamount"];
				$redeem = $data_list[$j]["redeem"];
				$currentcoin = $data_list[$j]["currentcoin"];
				$purchasecount = $data_list[$j]["purchasecount"];
				$purchaseamount = $data_list[$j]["purchaseamount"];
				$purchasecoin = $data_list[$j]["purchasecoin"];
				$days_after_purchase = $data_list[$j]["days_after_purchase"];
				$freecoin = $data_list[$j]["freecoin"];
				$jackpot = $data_list[$j]["jackpot"];
				
				$vip_level = $data_list[$j]["vip_level"];
				$vip_point = $data_list[$j]["vip_point"];
				$famelevel = $data_list[$j]["famelevel"];
				$exp = $data_list[$j]["exp"];
				$ty_point = $data_list[$j]["ty_point"];
				$friend_count = $data_list[$j]["friend_count"];
				$crowntype = $data_list[$j]["crowntype"];
				$days_after_install = $data_list[$j]["days_after_install"];
				$bet_rate = $data_list[$j]["bet_rate"];
				
				if($bet_rate == "")
					$bet_rate = 0;
				 
				if($insertcount == 0)
				{
					$insert_sql = "INSERT INTO ".$insert_table." VALUES('$today', $platform, $useridx, $moneyin, $moneyout, $winrate, $bigwin, $playtime, $playcount, $h_moneyin, $h_moneyout, $h_winrate, $h_bigwin, $h_playtime, $h_playcount, ".
									"$treatamount, $redeem, $currentcoin, $purchasecount, $purchaseamount, $purchasecoin, $days_after_purchase, $freecoin, $jackpot, $vip_level, $vip_point, $famelevel, $exp, $ty_point, $friend_count, $crowntype, $days_after_install, $bet_rate)";
					 
					$insertcount++;
				}
				else if ($insertcount < 1000)
				{
					$insert_sql .= ", ('$today', $platform, $useridx, $moneyin, $moneyout, $winrate, $bigwin, $playtime, $playcount, $h_moneyin, $h_moneyout, $h_winrate, $h_bigwin, $h_playtime, $h_playcount, ".
									"$treatamount, $redeem, $currentcoin, $purchasecount, $purchaseamount, $purchasecoin, $days_after_purchase, $freecoin, $jackpot, $vip_level, $vip_point, $famelevel, $exp, $ty_point, $friend_count, $crowntype, $days_after_install, $bet_rate)";
					 
					$insertcount++;
				}
				else
				{
					$insert_sql .= ", ('$today', $platform, $useridx, $moneyin, $moneyout, $winrate, $bigwin, $playtime, $playcount, $h_moneyin, $h_moneyout, $h_winrate, $h_bigwin, $h_playtime, $h_playcount, ".
									"$treatamount, $redeem, $currentcoin, $purchasecount, $purchaseamount, $purchasecoin, $days_after_purchase, $freecoin, $jackpot, $vip_level, $vip_point, $famelevel, $exp, $ty_point, $friend_count, $crowntype, $days_after_install, $bet_rate)";
					$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin=VALUES(moneyin), moneyout=VALUES(moneyout), winrate=VALUES(winrate), bigwin=VALUES(bigwin), playtime=VALUES(playtime), playcount=VALUES(playcount), ".
									"h_moneyin=VALUES(h_moneyin), h_moneyout=VALUES(h_moneyout), h_winrate=VALUES(h_winrate), h_bigwin=VALUES(h_bigwin), h_playtime=VALUES(h_playtime), h_playcount=VALUES(h_playcount), ".
									"treatamount=VALUES(treatamount), redeem=VALUES(redeem), currentcoin=VALUES(currentcoin), purchasecount=VALUES(purchasecount), purchaseamount=VALUES(purchaseamount), purchasecoin=VALUES(purchasecoin), days_after_purchase=VALUES(days_after_purchase), ".
									"freecoin=VALUES(freecoin), jackpot=VALUES(jackpot), vip_level=VALUES(vip_level), vip_point=VALUES(vip_point), famelevel=VALUES(famelevel),`exp`=VALUES(`exp`), ty_point=VALUES(ty_point), friend_count=VALUES(friend_count), crowntype=VALUES(crowntype), days_after_install=VALUES(days_after_install), bet_rate=VALUES(bet_rate);";
					//write_log($insert_sql);
					$db_other->execute($insert_sql);					
					 
					$insertcount = 0;
					$insert_sql = "";
				}
			}
			
			if($insert_sql != '')
			{
				//write_log($insert_sql);
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin=VALUES(moneyin), moneyout=VALUES(moneyout), winrate=VALUES(winrate), bigwin=VALUES(bigwin), playtime=VALUES(playtime), playcount=VALUES(playcount), ".
									"h_moneyin=VALUES(h_moneyin), h_moneyout=VALUES(h_moneyout), h_winrate=VALUES(h_winrate), h_bigwin=VALUES(h_bigwin), h_playtime=VALUES(h_playtime), h_playcount=VALUES(h_playcount), ".
									"treatamount=VALUES(treatamount), redeem=VALUES(redeem), currentcoin=VALUES(currentcoin), purchasecount=VALUES(purchasecount), purchaseamount=VALUES(purchaseamount), purchasecoin=VALUES(purchasecoin), days_after_purchase=VALUES(days_after_purchase), ".
									"freecoin=VALUES(freecoin), jackpot=VALUES(jackpot), vip_level=VALUES(vip_level), vip_point=VALUES(vip_point), famelevel=VALUES(famelevel),`exp`=VALUES(`exp`), ty_point=VALUES(ty_point), friend_count=VALUES(friend_count), crowntype=VALUES(crowntype), days_after_install=VALUES(days_after_install), bet_rate=VALUES(bet_rate);";
				$db_other->execute($insert_sql);
			}
		}
		
		sleep(1);

		$startdate = $temp_date;
	}
    	
    
    $db_other->end();
	$db_redshift->end();
?>
