<?
	include("../../common/common_include.inc.php");

	$slave_main = new CDatabase_Main();
	$db_other = new CDatabase_Other();
	
	$slave_main->execute("SET wait_timeout=3600");
	$db_other->execute("SET wait_timeout=3600");	
	
	$sdate = "2019-09-12";
	$edate = "2019-09-18";
	
	while($sdate < $edate)
	{
		$today = $sdate;
		$yesterday = date('Y-m-d', strtotime($sdate.' - 1 day'));
		
		write_log("tbl_product_order_all : $yesterday~$today");
	
		try
		{
			
			$sql = "DELETE FROM tbl_product_order_all WHERE os_type = 0 AND '$yesterday 00:00:00' <= writedate AND  writedate < '$today 00:00:00'";
			$db_other->gettotallist($sql);
			
			sleep(1);
			
			$sql = "SELECT * FROM 
					(
						SELECT orderidx AS org_orderidx, useridx, productidx, 0 AS os_type, usercoin, userlevel, coin, orderno, '' AS receipt, '' AS signature, facebookcredit/10 AS money, vip_point, status, gift_status, gift_coin, couponidx, special_discount, special_more, basecoin, canceldate, cancelleftcoin, disputed, 0 AS errcount, writedate,coupon_type,product_type  
						FROM `tbl_product_order` WHERE useridx > 20000 AND '$yesterday 00:00:00' <= writedate AND  writedate < '$today 00:00:00'
					) t1 ORDER BY writedate ASC";	
			$product_order = $slave_main->gettotallist($sql);
			
			$insert_sql = "";
			$insert_cnt = 0;
			
			for($i=0; $i<sizeof($product_order); $i++)
			{
				$org_orderidx = $product_order[$i]['org_orderidx'];
				$useridx = $product_order[$i]['useridx'];
				$productidx = $product_order[$i]['productidx'];
				$os_type = $product_order[$i]['os_type'];
				$usercoin = $product_order[$i]['usercoin'];
				$userlevel = $product_order[$i]['userlevel'];
				$coin = $product_order[$i]['coin'];
				$orderno = $product_order[$i]['orderno'];
				$receipt = $product_order[$i]['receipt'];
				$signature = $product_order[$i]['signature'];
				$money = $product_order[$i]['money'];
				$vip_point = $product_order[$i]['vip_point'];
				$status = $product_order[$i]['status'];
				$gift_status = $product_order[$i]['gift_status'];
				$gift_coin = $product_order[$i]['gift_coin'];
				$couponidx = $product_order[$i]['couponidx'];
				$special_discount = $product_order[$i]['special_discount'];
				$special_more = $product_order[$i]['special_more'];
				$basecoin = $product_order[$i]['basecoin'];
				$canceldate = $product_order[$i]['canceldate'];
				$cancelleftcoin = $product_order[$i]['cancelleftcoin'];
				$disputed = $product_order[$i]['disputed'];
				$errcount = $product_order[$i]['errcount'];
				$writedate = $product_order[$i]['writedate'];
				$coupon_type = $product_order[$i]['coupon_type'];
				$product_type = $product_order[$i]['product_type'];
				
				if($insert_sql == "")
				{
					$insert_sql = "INSERT INTO tbl_product_order_all(org_orderidx, useridx, productidx, os_type, usercoin, userlevel, coin, orderno, receipt, signature, money, vip_point, status, gift_status, gift_coin, couponidx, special_discount, special_more, basecoin, canceldate, cancelleftcoin, disputed, errcount,  writedate,coupon_type,product_type) ".
									" VALUES ($org_orderidx, $useridx, $productidx, $os_type, $usercoin, $userlevel, $coin, '$orderno', '$receipt', '$signature', '$money', $vip_point, $status, $gift_status, $gift_coin, $couponidx, $special_discount, $special_more, $basecoin, '$canceldate', $cancelleftcoin, $disputed, $errcount, '$writedate',$coupon_type,$product_type)";		
				}
				else  
					$insert_sql .= ",($org_orderidx, $useridx, $productidx, $os_type, $usercoin, $userlevel, $coin, '$orderno', '$receipt', '$signature', '$money', $vip_point, $status, $gift_status, $gift_coin, $couponidx, $special_discount, $special_more, $basecoin, '$canceldate', $cancelleftcoin, $disputed, $errcount, '$writedate',$coupon_type,$product_type)";
				
				$insert_cnt++;
				
				if($insert_cnt == 500)
				{
					$db_other->execute($insert_sql);
					
					$insert_sql = "";
					$insert_cnt = 0;					
				}
			}
			
			if($insert_sql != "")
				$db_other->execute($insert_sql);
				
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));
	}
	
	$slave_main->end();
    $db_other->end();
	
?>