<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	$filename = "ios_install.xls";

	$db_redshift = new CDatabase_Redshift();

	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
			"<table border=1>".
			"<tr>".
			"<td style='font-weight:bold;'>today</td>".
			"<td style='font-weight:bold;'>install</td>".
			"</tr>";

		$sql = "select date(createdate) AS today, count(*) as install_cnt
				from t5_user
				where createdate between '2016-11-11 00:00:00' and '2017-03-19 23:59:59'
				and platform = 1
				group by today
				order by today asc";
		$contents_list = $db_redshift->gettotallist($sql);

		for ($i=0; $i<sizeof($contents_list); $i++)
		{
			$today = $contents_list[$i]["today"];
			$install = $contents_list[$i]["install_cnt"];

			$excel_contents .= "<tr>".
					"<td>".$today."</td>".
					"<td>".number_format($install)."</td>".
					"</tr>";
		}

	$db_redshift->end();

	$excel_contents .= "</table>";

	Header("Content-type: application/x-msdownload");
	Header("Content-type: application/x-msexcel");
	Header("Content-Disposition: attachment; filename=$filename");

	echo($excel_contents);
?>