<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$sdate = "2019-05-13";
	$edate = "2019-05-14";
	
	while($sdate < $edate)
	{
		$yesterday = $sdate;
		$yesterday_str = date('Ymd', strtotime($sdate));
		$today = date('Y-m-d', strtotime($sdate.' + 1 day'));
		
		write_log("$yesterday ~ $today user marking log");
		
		// tbl_marking_info
		try
		{
			$db_main2 = new CDatabase_Slave_Main2();
			$db_main2->execute("SET wait_timeout=3600");
		
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
			$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_marking_log'";
			$columns_count = $db_main2->getvalue($sql);
		
			//$sql = "SELECT * FROM tbl_user_marking_log WHERE '$yesterday' <= today AND today < '$today';";
			$sql = "SELECT * FROM tbl_user_marking_log;";
			write_log($sql);
			$user_marking_log_list = $db_main2->gettotallist($sql);
		
			for($i=0; $i<sizeof($user_marking_log_list); $i++)
			{
				$output = "";
		
				for($j=0; $j<$columns_count; $j++)
				{
					$user_list[$i][$j] = str_replace("\"", "\"\"", $user_marking_log_list[$i][$j]);
		
					if($user_marking_log_list[$i][$j] == "0000-00-00 00:00:00")
						$user_marking_log_list[$i][$j] = "1900-01-01 00:00:00";
						
					if($j == $columns_count - 1)
						$output .= '"'.$user_marking_log_list[$i][$j].'"';
					else
						$output .= '"'.$user_marking_log_list[$i][$j].'"|';
				}
				$output .="\n";
		
				if($output != "")
				{
					$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_marking_log_$yesterday_str.txt", 'a+');
						
					fwrite($fp, $output);
						
					fclose($fp);
				}
			}
		
			$db_main2->end();
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		$is_file_exist = file_exists("$DOCUMENT_ROOT/redshift_temp/tbl_user_marking_log_".$yesterday_str.".txt");
		
		if($is_file_exist)
		{	
			try
			{
				// Create the AWS service builder, providing the path to the config file
				$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
			
				$s3Client = $aws->get('s3');
				$bucket = "wg-redshift/t5/tbl_user_marking_log/$yesterday_str";
			
				$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
				$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_marking_log_$yesterday_str.txt";
			
				// Upload an object to Amazon S3
				$result = $s3Client->putObject(array(
						'Bucket' 		=> $bucket,
						'Key'    		=> 'tbl_user_marking_log_'.$yesterday_str.'.txt',
						'ACL'	 		=> 'public-read',
						'SourceFile'   	=> $filepath
				));
			
				$ObjectURL = $result["ObjectURL"];
			
				if($ObjectURL != "")
				{
					@unlink($filepath);
				}
			}
			catch(Exception $e)
			{
				write_log($e->getMessage());
			}
			
			try
			{
				$db_redshift = new CDatabase_Redshift();
			
				$sql = "DELETE FROM t5_user_marking_log WHERE today = '$yesterday'";
				write_log($sql);
				$db_redshift->execute($sql);
				
				sleep(1);
				
				// Create a staging table
				$sql = "CREATE TABLE t5_user_marking_log_tmp (LIKE t5_user_marking_log);";
				$db_redshift->execute($sql);
			
				// Load data into the staging table
				$sql = "copy t5_user_marking_log_tmp ".
						"from 's3://wg-redshift/t5/tbl_user_marking_log/$yesterday_str' ".
						"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
						"delimiter '|' ".
						"csv;";
				$db_redshift->execute($sql);
			
				// Update records
				$sql = "UPDATE t5_user_marking_log ".
						"SET useridx = s.useridx, markingidx = s.markingidx, today = s.today, is_test = s.is_test, logindate = s.logindate, actiondate = s.actiondate, is_payer = s.is_payer, leavedays = s.leavedays, ".
						"	buy_leavedays = s.buy_leavedays, play_leavedays = s.play_leavedays, total_pay_amount = s.total_pay_amount, total_pay_count = s.total_pay_count, d28_pay_amount = s.d28_pay_amount, ".
						"	d28_pay_count = s.d28_pay_count, d28_money_in = s.d28_money_in, d28_playcount = s.d28_playcount, d28_play_days = s.d28_play_days ".
						"FROM t5_user_marking_log_tmp s ".
						"WHERE t5_user_marking_log.logidx = s.logidx;";
				$db_redshift->execute($sql);
			
				// Insert records
				$sql = "INSERT INTO t5_user_marking_log ".
						"SELECT s.* FROM t5_user_marking_log_tmp s LEFT JOIN t5_user_marking_log ".
						"ON s.logidx = t5_user_marking_log.logidx ".
						"WHERE t5_user_marking_log.logidx IS NULL;";
				$db_redshift->execute($sql);
					
				// Drop the staging table
				$sql = "DROP TABLE t5_user_marking_log_tmp;";
				$db_redshift->execute($sql);
			
				$db_redshift->end();
			
			}
			catch(Exception $e)
			{
				write_log($e->getMessage());
			}
		}
		
		$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));
	}
?>