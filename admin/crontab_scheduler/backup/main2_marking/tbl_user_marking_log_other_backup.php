<?
	include("../../../common/common_include.inc.php");

	$db_main2 = new CDatabase_Slave_Main2();
	$db_other = new CDatabase_Other();
	
	$db_main2->execute("SET wait_timeout=7200");
	$db_other->execute("SET wait_timeout=7200");
	
	ini_set("memory_limit", "-1");
	
	try 
	{		
		$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);		
		
		$sql = "SELECT * FROM tbl_user_marking_log WHERE today = '$yesterday'";
		$marking_log_info = $db_main2->gettotallist($sql);
		
		$insert_sql = "";
		$insert_cnt = "";
		
		for($i=0; $i<sizeof($marking_log_info); $i++)
		{
			$logidx = $marking_log_info[$i]["logidx"];
			$useridx = $marking_log_info[$i]["useridx"];
			$markingidx = $marking_log_info[$i]["markingidx"];
			$today = $marking_log_info[$i]["today"];
			$type = $marking_log_info[$i]["type"];
			$category = $marking_log_info[$i]["category"];
			$is_test = $marking_log_info[$i]["is_test"];
			$logindate = $marking_log_info[$i]["logindate"];
			$actiondate = $marking_log_info[$i]["actiondate"];
			$is_payer = $marking_log_info[$i]["is_payer"];
			$leavedays = $marking_log_info[$i]["leavedays"];
			$buy_leavedays = $marking_log_info[$i]["buy_leavedays"];
			$play_leavedays = $marking_log_info[$i]["play_leavedays"];
			$total_pay_amount = $marking_log_info[$i]["total_pay_amount"];
			$total_pay_count = $marking_log_info[$i]["total_pay_count"];
			$d28_pay_amount = $marking_log_info[$i]["d28_pay_amount"];
			$d28_pay_count = $marking_log_info[$i]["d28_pay_count"];
			$d28_money_in = $marking_log_info[$i]["d28_money_in"];
			$d28_playcount = $marking_log_info[$i]["d28_playcount"];
			$d28_play_days = $marking_log_info[$i]["d28_play_days"];
			
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO _tbl_user_marking_log(logidx, useridx, markingidx, today, category, is_test, logindate, actiondate, is_payer, leavedays, ".
							"buy_leavedays, play_leavedays, total_pay_amount, total_pay_count, d28_pay_amount, d28_pay_count, d28_money_in, d28_playcount, d28_play_days) ".
							"VALUES($logidx, $useridx, $markingidx, '$today', $category, $is_test, '$logindate', '$actiondate', $is_payer, $leavedays, ".
							"$buy_leavedays, $play_leavedays, $total_pay_amount, $total_pay_count, $d28_pay_amount, $d28_pay_count, $d28_money_in, $d28_playcount, $d28_play_days)";
				
				$insert_cnt++;
			}
			else
			{
				$insert_sql .= ",($logidx, $useridx, $markingidx, '$today', $category, $is_test, '$logindate', '$actiondate', $is_payer, $leavedays, ".
							"$buy_leavedays, $play_leavedays, $total_pay_amount, $total_pay_count, $d28_pay_amount, $d28_pay_count, $d28_money_in, $d28_playcount, $d28_play_days)";
				
				$insert_cnt++;
			}
			
			if($insert_cnt >= 1000)
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE is_test = VALUES(is_test)";
				$db_other->execute($insert_sql);
				
				$insert_sql = "";
				$insert_cnt = 0;
			}			
		}
		
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE is_test = VALUES(is_test)";
			$db_other->execute($insert_sql);
			
			$insert_sql = "";
			$insert_cnt = 0;
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
	$db_other->end();	
?>