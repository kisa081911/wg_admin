<?
	include("../../../common/common_include.inc.php");
	include("../../../common/dbconnect/db_util_redshift.inc.php");
	require '../../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);

		
	try
	{
		
		$db_friend = new CDatabase_Friend();
		
		$db_friend->execute("SET wait_timeout=7200");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$sql = "SELECT * FROM `tbl_wakeup_request` WHERE wakeupidx > 50";
		
		$user_list = $db_friend->gettotallist($sql);
		
		for($i=0; $i<sizeof($user_list); $i++)
		{
			$output = "";
			
			if($user_list[$i]["senddate"] == "0000-00-00 00:00:00")
			    $user_list[$i]["senddate"] = "1900-01-01 00:00:00";
			
			if($user_list[$i]["updatedate"] == "0000-00-00 00:00:00")
			    $user_list[$i]["updatedate"] = "1900-01-01 00:00:00";
			
			if($user_list[$i]["writedate"] == "0000-00-00 00:00:00")
			    $user_list[$i]["writedate"] = "1900-01-01 00:00:00";
		
			$output .= '"'.$user_list[$i]["wakeupidx"].'"|';
			$output .= '"'.$user_list[$i]["sender_idx"].'"|';
			$output .= '"name"|';
			$output .= '"'.$user_list[$i]["receiver_idx"].'"|';
			$output .= '"'.$user_list[$i]["receiver_id"].'"|';
			$output .= '"'.$user_list[$i]["group_no"].'"|';
			$output .= '"'.$user_list[$i]["send_status"].'"|';
			$output .= '"'.$user_list[$i]["status"].'"|';
			$output .= '"'.$user_list[$i]["senddate"].'"|';
			$output .= '"'.$user_list[$i]["updatedate"].'"|';
			$output .= '"'.$user_list[$i]["writedate"].'"|';
			$output .="\n";
				
			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_wakeup_request_$yesterday_str.txt", 'a+');
					
				fwrite($fp, $output);
					
				fclose($fp);
			}
		}
		
		$db_friend->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_wakeup_request/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_wakeup_request_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_wakeup_request_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_wakeup_request_tmp (LIKE t5_wakeup_request);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy t5_wakeup_request_tmp ".
				"from 's3://wg-redshift/t5/tbl_wakeup_request/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
// 		// Update records
// 		$sql = "UPDATE t5_wakeup_request ".
// 				" SET is_agree = s.is_agree ".
// 				" FROM t5_wakeup_request_tmp s ".
// 				" WHERE t5_wakeup_request.wakeupidx = s.wakeupidx AND t5_wakeup_request.sender_idx = s.sender_idx AND t5_wakeup_request.receiver_idx = s.receiver_idx AND t5_wakeup_request.receiver_idx = s.receiver_id; ";
// 		$db_redshift->execute($sql);

// 		// Insert records
		$sql = "INSERT INTO t5_wakeup_request ".
				"SELECT s.* FROM t5_wakeup_request_tmp s LEFT JOIN t5_wakeup_request ".
				"ON t5_wakeup_request.wakeupidx = s.wakeupidx ".
				"AND t5_wakeup_request.sender_idx = s.sender_idx ".
				"AND t5_wakeup_request.receiver_idx = s.receiver_idx ".
				"AND t5_wakeup_request.receiver_id = s.receiver_id ";
		$db_redshift->execute($sql);
			
// 		// Drop the staging table
 		$sql = "DROP TABLE t5_wakeup_request_tmp;";
 		$db_redshift->execute($sql);

		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>