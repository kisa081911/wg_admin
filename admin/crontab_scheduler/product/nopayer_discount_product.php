<?
	include("../../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$filename = "nopayer_discount_product.xls";
	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
 						"<table border=1>".
 						"<tr>".
 						"<td style='font-weight:bold;'>productidx</td>".
						"<td style='font-weight:bold;'>productkey</td>".
						"<td style='font-weight:bold;'>facebookcredit</td>".
						"<td style='font-weight:bold;'>title</td>".
						"<td style='font-weight:bold;'>description</td>".
						"</tr>";
	try
	{
		///// season
		$season_offer_discount_arr = array(
				array("facebookcredit"=>"50", "basecoin"=>"2000000", "vip_point"=>5, "discount"=>26),
				array("facebookcredit"=>"50", "basecoin"=>"2000000", "vip_point"=>5, "discount"=>30),
				array("facebookcredit"=>"50", "basecoin"=>"2000000", "vip_point"=>5, "discount"=>31),
				array("facebookcredit"=>"50", "basecoin"=>"2000000", "vip_point"=>5, "discount"=>34),
				array("facebookcredit"=>"50", "basecoin"=>"2000000", "vip_point"=>5, "discount"=>37),
				array("facebookcredit"=>"50", "basecoin"=>"2000000", "vip_point"=>5, "discount"=>40),
				array("facebookcredit"=>"90", "basecoin"=>"4500000", "vip_point"=>10, "discount"=>26),
				array("facebookcredit"=>"90", "basecoin"=>"4500000", "vip_point"=>10, "discount"=>30),
				array("facebookcredit"=>"90", "basecoin"=>"4500000", "vip_point"=>10, "discount"=>31),
				array("facebookcredit"=>"90", "basecoin"=>"4500000", "vip_point"=>10, "discount"=>34),
				array("facebookcredit"=>"90", "basecoin"=>"4500000", "vip_point"=>10, "discount"=>37),
				array("facebookcredit"=>"90", "basecoin"=>"4500000", "vip_point"=>10, "discount"=>40),
				array("facebookcredit"=>"190", "basecoin"=>"11400000", "vip_point"=>20, "discount"=>27),
				array("facebookcredit"=>"190", "basecoin"=>"11400000", "vip_point"=>20, "discount"=>31),
				array("facebookcredit"=>"190", "basecoin"=>"11400000", "vip_point"=>20, "discount"=>34),
				array("facebookcredit"=>"190", "basecoin"=>"11400000", "vip_point"=>20, "discount"=>37),
				array("facebookcredit"=>"190", "basecoin"=>"11400000", "vip_point"=>20, "discount"=>40),
				array("facebookcredit"=>"190", "basecoin"=>"11400000", "vip_point"=>20, "discount"=>41),
				array("facebookcredit"=>"390", "basecoin"=>"29250000", "vip_point"=>40, "discount"=>27),
				array("facebookcredit"=>"390", "basecoin"=>"29250000", "vip_point"=>40, "discount"=>31),
				array("facebookcredit"=>"390", "basecoin"=>"29250000", "vip_point"=>40, "discount"=>34),
				array("facebookcredit"=>"390", "basecoin"=>"29250000", "vip_point"=>40, "discount"=>37),
				array("facebookcredit"=>"390", "basecoin"=>"29250000", "vip_point"=>40, "discount"=>40),
				array("facebookcredit"=>"390", "basecoin"=>"29250000", "vip_point"=>40, "discount"=>41),
				array("facebookcredit"=>"590", "basecoin"=>"70800000", "vip_point"=>60, "discount"=>18),
				array("facebookcredit"=>"590", "basecoin"=>"70800000", "vip_point"=>60, "discount"=>20),
				array("facebookcredit"=>"590", "basecoin"=>"70800000", "vip_point"=>60, "discount"=>22),
				array("facebookcredit"=>"590", "basecoin"=>"70800000", "vip_point"=>60, "discount"=>24),
				array("facebookcredit"=>"590", "basecoin"=>"70800000", "vip_point"=>60, "discount"=>26),
				array("facebookcredit"=>"590", "basecoin"=>"70800000", "vip_point"=>60, "discount"=>28)
		);
		
		$insert_sql = "INSERT INTO tbl_product(productkey,category,productname,description,imageurl,amount,facebookcredit,vip_point,special_more,special_discount,status,product_type,writedate)";
		
		$excel_contents .= 	"<tr>".
								"<td>discount</td>".
							"</tr>";
		for($p=0; $p<sizeof($season_offer_discount_arr);$p++)
		{
			$facebookcredit = $season_offer_discount_arr[$p]["facebookcredit"];
			$basecoin = $season_offer_discount_arr[$p]["basecoin"];
			$vip_point = $season_offer_discount_arr[$p]["vip_point"];
			$discount = $season_offer_discount_arr[$p]["discount"];
			$category = 9;
			
			$key_facebookcredit = $facebookcredit/10;
			$money = $facebookcredit/10;
			
			if(strlen($key_facebookcredit)<3)
			{
				$key_facebookcredit="0".$key_facebookcredit;
			}
			
			$productkey = sprintf ("%02d",$category)."_".sprintf ("%03d",$key_facebookcredit)."_$discount".substr(uniqid(), -5);
			$product_name = number_format($basecoin)." Coins (New Player Offer)";
			$discount_facebookcredit = round($facebookcredit-($facebookcredit*($discount/100)));
			
			if($money == 39)
				$descripton = number_format(floor($basecoin/($discount_facebookcredit/10)))." Coins Per $1, VIP Club $vip_point Points, + 1 Extra Bonus";
			else if ($money == 59)
				$descripton = number_format(floor($basecoin/($discount_facebookcredit/10)))." Coins Per $1, VIP Club $vip_point Points, + 2 Extra Bonus";
			else
				$descripton = number_format(floor($basecoin/($discount_facebookcredit/10)))." Coins Per $1, VIP Club $vip_point Points.";
			
			if($p==0)
					$insert_sql .= "VALUES('$productkey', $category, '$product_name', '$descripton'";
			else
				
			$insert_sql .= ",('$productkey', $category, '$product_name', '$descripton'";
			
			$insert_sql .= ", 'https://take5slots.com/facebook/images/product/product_special_$discount.png', '$basecoin','$discount_facebookcredit','$vip_point',0,$discount,'1',11,now())";
			
			if(($i+1)==sizeof($product_list))
								$insert_sql .= ";";
			
			$excel_contents .= "<tr>".
					"<td>0</td>".
					"<td>".$productkey."</td>".
					"<td>".$facebookcredit."</td>".
					"<td>".$product_name."</td>".
					"<td>".$descripton."</td>".
					"</tr>";
		}

		$db_main->execute($insert_sql);
		$excel_contents .= "</table>";
	
		Header("Content-type: application/x-msdownload");
		Header("Content-type: application/x-msexcel");
		Header("Content-Disposition: attachment; filename=$filename");
	
		echo($excel_contents);
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	$db_main->end();
?>