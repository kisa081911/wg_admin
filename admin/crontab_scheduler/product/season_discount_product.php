<?
	include("../../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$filename = "season_discount_product.xls";
	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
 						"<table border=1>".
 						"<tr>".
 						"<td style='font-weight:bold;'>productidx</td>".
						"<td style='font-weight:bold;'>productkey</td>".
						"<td style='font-weight:bold;'>facebookcredit</td>".
						"<td style='font-weight:bold;'>title</td>".
						"<td style='font-weight:bold;'>description</td>".
						"</tr>";
	try
	{
		///// season
		$season_offer_discount_arr = array(
				array("facebookcredit"=>"90", "basecoin"=>"4500000", "vip_point"=>10, "discount"=>17),
				array("facebookcredit"=>"190", "basecoin"=>"11400000", "vip_point"=>20, "discount"=>21),
				array("facebookcredit"=>"390", "basecoin"=>"29250000", "vip_point"=>40, "discount"=>22),
				
				array("facebookcredit"=>"190", "basecoin"=>"11400000", "vip_point"=>20, "discount"=>20),
				array("facebookcredit"=>"390", "basecoin"=>"29250000", "vip_point"=>40, "discount"=>20),
				array("facebookcredit"=>"590", "basecoin"=>"70800000", "vip_point"=>60, "discount"=>20),
				
				array("facebookcredit"=>"390", "basecoin"=>"29250000", "vip_point"=>40, "discount"=>20),
				array("facebookcredit"=>"590", "basecoin"=>"70800000", "vip_point"=>60, "discount"=>20),
				array("facebookcredit"=>"990", "basecoin"=>"198000000", "vip_point"=>100, "discount"=>20),
				
				array("facebookcredit"=>"590", "basecoin"=>"70800000", "vip_point"=>60, "discount"=>15),
				array("facebookcredit"=>"990", "basecoin"=>"198000000", "vip_point"=>100, "discount"=>17),
				array("facebookcredit"=>"1990", "basecoin"=>"537300000", "vip_point"=>200, "discount"=>20),
				
				array("facebookcredit"=>"990", "basecoin"=>"198000000", "vip_point"=>100, "discount"=>13),
				array("facebookcredit"=>"1990", "basecoin"=>"537300000", "vip_point"=>200, "discount"=>18),
				array("facebookcredit"=>"2990", "basecoin"=>"1046500000", "vip_point"=>300, "discount"=>18),
				
				array("facebookcredit"=>"1990", "basecoin"=>"537300000", "vip_point"=>200, "discount"=>16),
				array("facebookcredit"=>"2990", "basecoin"=>"1046500000", "vip_point"=>300, "discount"=>17),
				array("facebookcredit"=>"4990", "basecoin"=>"2245500000", "vip_point"=>500, "discount"=>20)
		);
		
		$insert_sql = "INSERT INTO tbl_product(productkey,category,productname,description,imageurl,amount,facebookcredit,vip_point,special_more,special_discount,status,product_type,writedate)";
		
		$excel_contents .= 	"<tr>".
								"<td>discount</td>".
							"</tr>";
		for($p=0; $p<sizeof($season_offer_discount_arr);$p++)
		{
			$facebookcredit = $season_offer_discount_arr[$p]["facebookcredit"];
			$basecoin = $season_offer_discount_arr[$p]["basecoin"];
			$vip_point = $season_offer_discount_arr[$p]["vip_point"];
			$discount = $season_offer_discount_arr[$p]["discount"];
			$category = 9;
			
			$key_facebookcredit = $facebookcredit/10;
			$money = $facebookcredit/10;
			
			if(strlen($key_facebookcredit)<3)
			{
				$key_facebookcredit="0".$key_facebookcredit;
			}
			
			$productkey = sprintf ("%02d",$category)."_".sprintf ("%03d",$key_facebookcredit)."_$discount".substr(uniqid(), -5);
			$product_name = number_format($basecoin)." Coins (Special Offer)";
			$discount_facebookcredit = round($facebookcredit-($facebookcredit*($discount/100)));
			
			if($money == 39)
				$descripton = "Includes + $vip_point VIP Club Points, + 1 Extra Bonus";
			else if ($money == 59)
				$descripton = "Includes + $vip_point VIP Club Points, + 2 Extra Bonus";
			else if ($money == 99)
				$descripton = "Includes + $vip_point VIP Club Points, + 3 Extra Bonus";
			else if ($money == 199)
				$descripton = "Includes + $vip_point VIP Club Points, + 4 Extra Bonus";
			else if ($money == 299)
				$descripton = "Includes + $vip_point VIP Club Points, + 4 Extra Bonus";
			else if ($money == 499)
				$descripton = "Includes + $vip_point VIP Club Points, + 5 Extra Bonus";
			else
				$descripton = "Includes + $vip_point VIP Club Points.";
			
			if($p==0)
					$insert_sql .= "VALUES('$productkey', $category, '$product_name', '$descripton'";
			else
				
			$insert_sql .= ",('$productkey', $category, '$product_name', '$descripton'";
			
			$insert_sql .= ", 'https://take5slots.com/facebook/images/product/product_special_$discount.png', '$basecoin','$discount_facebookcredit','$vip_point',0,$discount,'1',2,now())";
			
			if(($i+1)==sizeof($product_list))
								$insert_sql .= ";";
			
			$excel_contents .= "<tr>".
					"<td>0</td>".
					"<td>".$productkey."</td>".
					"<td>".$facebookcredit."</td>".
					"<td>".$product_name."</td>".
					"<td>".$descripton."</td>".
					"</tr>";
		}

		$db_main->execute($insert_sql);
		$excel_contents .= "</table>";
	
		Header("Content-type: application/x-msdownload");
		Header("Content-type: application/x-msexcel");
		Header("Content-Disposition: attachment; filename=$filename");
	
		echo($excel_contents);
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	$db_main->end();
?>