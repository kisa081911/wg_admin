<?
	include("../../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$filename = "threadhold_more_product.xls";
	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
 						"<table border=1>".
 						"<tr>".
 						"<td style='font-weight:bold;'>productidx</td>".
						"<td style='font-weight:bold;'>productkey</td>".
						"<td style='font-weight:bold;'>facebookcredit</td>".
						"<td style='font-weight:bold;'>title</td>".
						"<td style='font-weight:bold;'>description</td>".
						"</tr>";
	try
	{
		///// Basic
		$product_basecoin_arr = array(
				array("facebookcredit"=>"190", "basecoin"=>"11400000", "vip_point"=>20),
				array("facebookcredit"=>"390", "basecoin"=>"29250000", "vip_point"=>40),
				array("facebookcredit"=>"590", "basecoin"=>"70800000", "vip_point"=>60),
				array("facebookcredit"=>"990", "basecoin"=>"198000000", "vip_point"=>100),
				array("facebookcredit"=>"1990", "basecoin"=>"537300000", "vip_point"=>200),
				array("facebookcredit"=>"2990", "basecoin"=>"1046500000", "vip_point"=>300),
				array("facebookcredit"=>"4990", "basecoin"=>"2245500000", "vip_point"=>500)
		);
		
		$insert_sql = "INSERT INTO tbl_product(productkey,category,productname,description,imageurl,amount,facebookcredit,vip_point,special_more,special_discount,status,product_type,writedate)";
		
		$excel_contents .= 	"<tr>".
								"<td>More</td>".
							"</tr>";

		$more = 40;
		
		for($p=0; $p<sizeof($product_basecoin_arr);$p++)
		{
			$facebookcredit = $product_basecoin_arr[$p]["facebookcredit"];
			$basecoin = $product_basecoin_arr[$p]["basecoin"];
			$vip_point = $product_basecoin_arr[$p]["vip_point"];
			
			while($more < 45)
			{
				$category = 8;
				$key_facebookcredit = $facebookcredit/10;
				$money = $facebookcredit/10;
				
				if(strlen($key_facebookcredit)<3)
				{
					$key_facebookcredit="0".$key_facebookcredit;
				}
				$more_amount = round($basecoin+($basecoin*($more/100)));
				
				$productkey = sprintf ("%02d",$category)."_".sprintf ("%03d",$key_facebookcredit)."_$more".substr(uniqid(), -5);
				$product_name = number_format($more_amount)." Coins (Special Offer Just FOR You!)";
				
				$extra_coins = $more_amount-(300000*($facebookcredit/10));
				$descripton = "Includes +".number_format($extra_coins)." Extra Coins & ".$vip_point." VIP Club Points.";
					
				if($more==40)
						$insert_sql .= "VALUES('$productkey', $category, '$product_name', '$descripton'";
				else
						$insert_sql .= ",('$productkey', $category, '$product_name', '$descripton'";
				
				$insert_sql .= ", 'https://take5slots.com/facebook/images/product/product_special_$more.png', '$basecoin','$facebookcredit','$vip_point',$more,0,'1',3,now())";
				
				if($more==40)
							$insert_sql .= ";";
				
				$excel_contents .= "<tr>".
						"<td>0</td>".
						"<td>".$productkey."</td>".
						"<td>".$facebookcredit."</td>".
						"<td>".$product_name."</td>".
						"<td>".$descripton."</td>".
						"</tr>";
				$more = $more+5;
			}
			$db_main->execute($insert_sql);
			$insert_sql = "INSERT INTO tbl_product(productkey,category,productname,description,imageurl,amount,facebookcredit,vip_point,special_more,special_discount,status,product_type,writedate)";
			$more = 40;
		}
		$excel_contents .= "</table>";
	
		Header("Content-type: application/x-msdownload");
		Header("Content-type: application/x-msexcel");
		Header("Content-Disposition: attachment; filename=$filename");
	
		echo($excel_contents);
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	$db_main->end();
?>