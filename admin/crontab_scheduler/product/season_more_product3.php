<?
	include("../../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$filename = "basic_product.xls";
	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
 						"<table border=1>".
 						"<tr>".
 						"<td style='font-weight:bold;'>productidx</td>".
						"<td style='font-weight:bold;'>productkey</td>".
						"<td style='font-weight:bold;'>facebookcredit</td>".
						"<td style='font-weight:bold;'>title</td>".
						"<td style='font-weight:bold;'>description</td>".
						"</tr>";
	try
	{
		///// season
		$product_basecoin_arr = array(
				array("facebookcredit"=>"10", "basecoin"=>"300000", "vip_point"=>1),
				array("facebookcredit"=>"50", "basecoin"=>"2000000", "vip_point"=>5),
				array("facebookcredit"=>"90", "basecoin"=>"4500000", "vip_point"=>10),
				array("facebookcredit"=>"190", "basecoin"=>"11400000", "vip_point"=>20),
				array("facebookcredit"=>"390", "basecoin"=>"29250000", "vip_point"=>40),
				array("facebookcredit"=>"590", "basecoin"=>"70800000", "vip_point"=>60),
				array("facebookcredit"=>"990", "basecoin"=>"198000000", "vip_point"=>100),
				array("facebookcredit"=>"1990", "basecoin"=>"537300000", "vip_point"=>200),
				array("facebookcredit"=>"2990", "basecoin"=>"1046500000", "vip_point"=>300),
				array("facebookcredit"=>"4990", "basecoin"=>"2245500000", "vip_point"=>500),
				array("facebookcredit"=>"7770", "basecoin"=>"4662000000", "vip_point"=>777),
				array("facebookcredit"=>"9990", "basecoin"=>"7992000000", "vip_point"=>999)
		);
		
		$season_offer_more_arr = array(
				array("facebookcredit"=>"90", "basecoin"=>"5670000", "vip_point"=>10, "more"=>40),
				array("facebookcredit"=>"190", "basecoin"=>"13775000", "vip_point"=>20, "more"=>45),
				array("facebookcredit"=>"390", "basecoin"=>"32175000", "vip_point"=>40, "more"=>50),
				
				array("facebookcredit"=>"190", "basecoin"=>"13300000", "vip_point"=>20, "more"=>40),
				array("facebookcredit"=>"390", "basecoin"=>"31102500", "vip_point"=>40, "more"=>45),
				array("facebookcredit"=>"590", "basecoin"=>"63720000", "vip_point"=>60, "more"=>50),
				
				array("facebookcredit"=>"390", "basecoin"=>"30030000", "vip_point"=>40, "more"=>40),
				array("facebookcredit"=>"590", "basecoin"=>"61596000", "vip_point"=>60, "more"=>45),
				array("facebookcredit"=>"990", "basecoin"=>"135135000", "vip_point"=>100, "more"=>50),
				
				array("facebookcredit"=>"590", "basecoin"=>"59472000", "vip_point"=>60, "more"=>40),
				array("facebookcredit"=>"990", "basecoin"=>"130630500", "vip_point"=>100, "more"=>45),
				array("facebookcredit"=>"1990", "basecoin"=>"358200000", "vip_point"=>200, "more"=>50),
				
				array("facebookcredit"=>"990", "basecoin"=>"126126000", "vip_point"=>100, "more"=>40),
				array("facebookcredit"=>"1990", "basecoin"=>"346260000", "vip_point"=>200, "more"=>45),
				array("facebookcredit"=>"2990", "basecoin"=>"6862050000", "vip_point"=>300, "more"=>50),
				
				array("facebookcredit"=>"1990", "basecoin"=>"334320000", "vip_point"=>200, "more"=>40),
				array("facebookcredit"=>"2990", "basecoin"=>"663331500", "vip_point"=>300, "more"=>45),
				array("facebookcredit"=>"4990", "basecoin"=>"1497000000", "vip_point"=>500, "more"=>50)
		);
		
		$insert_sql = "INSERT INTO tbl_product(productkey,category,productname,description,imageurl,amount,facebookcredit,vip_point,special_more,special_discount,status,product_type,writedate)";
		
		$excel_contents .= 	"<tr>".
								"<td>More</td>".
							"</tr>";
		for($p=0; $p<sizeof($season_offer_more_arr);$p++)
		{
			$facebookcredit = $season_offer_more_arr[$p]["facebookcredit"];
			$basecoin = $season_offer_more_arr[$p]["basecoin"];
			$vip_point = $season_offer_more_arr[$p]["vip_point"];
			$more = $season_offer_more_arr[$p]["more"];
			$category = 8;
			
			$key_facebookcredit = $facebookcredit/10;
			$money = $facebookcredit/10;
			
			if(strlen($key_facebookcredit)<3)
			{
				$key_facebookcredit="0".$key_facebookcredit;
			}
			
			$productkey = sprintf ("%02d",$category)."_".sprintf ("%03d",$key_facebookcredit)."_$more".substr(uniqid(), -5);
			$product_name = number_format($basecoin)." Coins (Extra Coin Pack)";
			$extra_coins =0;
			$basecoin_base_ = 0; 
			for($h=0;$h<sizeof($product_basecoin_arr);$h++)
			{
				$facebookcredit_base=$product_basecoin_arr[$h]["facebookcredit"];
				$basecoin_base = $product_basecoin_arr[$h]["basecoin"];
			
				if( $facebookcredit == $facebookcredit_base)
				{
					$extra_coins =  $basecoin-$basecoin_base;
					$basecoin_base_ = $basecoin_base;
				}
			}
			
			$descripton = "Includes +".number_format($extra_coins)." Extra Coins, & ".$vip_point." VIP Club Points.";
			
			
			if($p==0)
					$insert_sql .= "VALUES('$productkey', $category, '$product_name', '$descripton'";
			else
				
			$insert_sql .= ",('$productkey', $category, '$product_name', '$descripton'";
			
			$insert_sql .= ", 'https://take5slots.com/facebook/images/product/product_special_$more.png', '$basecoin_base_','$facebookcredit','$vip_point',$more,0,'1',2,now())";
			
			if(($i+1)==sizeof($product_list))
								$insert_sql .= ";";
			
			$excel_contents .= "<tr>".
					"<td>0</td>".
					"<td>".$productkey."</td>".
					"<td>".$facebookcredit."</td>".
					"<td>".$product_name."</td>".
					"<td>".$descripton."</td>".
					"</tr>";
		}

		$db_main->execute($insert_sql);
		$excel_contents .= "</table>";
	
		Header("Content-type: application/x-msdownload");
		Header("Content-type: application/x-msexcel");
		Header("Content-Disposition: attachment; filename=$filename");
	
		echo($excel_contents);
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	$db_main->end();
?>
