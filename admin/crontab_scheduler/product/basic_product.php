<?
	include("../../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$filename = "basic_product.xls";
	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
 						"<table border=1>".
 						"<tr>".
 						"<td style='font-weight:bold;'>productidx</td>".
						"<td style='font-weight:bold;'>productkey</td>".
						"<td style='font-weight:bold;'>facebookcredit</td>".
						"<td style='font-weight:bold;'>title</td>".
						"<td style='font-weight:bold;'>description</td>".
						"</tr>";
	try
	{
		///// Basic
		$product_basecoin_arr = array(
				array("facebookcredit"=>"10", "basecoin"=>"300000", "vip_point"=>1),
				array("facebookcredit"=>"50", "basecoin"=>"2000000", "vip_point"=>5),
				array("facebookcredit"=>"90", "basecoin"=>"4500000", "vip_point"=>10),
				array("facebookcredit"=>"190", "basecoin"=>"11400000", "vip_point"=>20),
				array("facebookcredit"=>"390", "basecoin"=>"29250000", "vip_point"=>40),
				array("facebookcredit"=>"590", "basecoin"=>"70800000", "vip_point"=>60),
				array("facebookcredit"=>"990", "basecoin"=>"198000000", "vip_point"=>100),
				array("facebookcredit"=>"1990", "basecoin"=>"537300000", "vip_point"=>200),
				array("facebookcredit"=>"2990", "basecoin"=>"1046500000", "vip_point"=>300),
				array("facebookcredit"=>"4990", "basecoin"=>"2245500000", "vip_point"=>500),
				array("facebookcredit"=>"7770", "basecoin"=>"4662000000", "vip_point"=>777),
				array("facebookcredit"=>"9990", "basecoin"=>"7992000000", "vip_point"=>999)
		);
		
		$insert_sql = "INSERT INTO tbl_product(productkey,category,productname,description,imageurl,amount,facebookcredit,vip_point,special_more,special_discount,status,product_type,writedate)";
		
		$excel_contents .= 	"<tr>".
								"<td>More</td>".
							"</tr>";

		for($p=0; $p<sizeof($product_basecoin_arr);$p++)
		{
			$facebookcredit = $product_basecoin_arr[$p]["facebookcredit"];
			$basecoin = $product_basecoin_arr[$p]["basecoin"];
			$vip_point = $product_basecoin_arr[$p]["vip_point"];
			$category = 0;
			$key_facebookcredit = $facebookcredit/10;
			$money = $facebookcredit/10;
			
			if(strlen($key_facebookcredit)<3)
			{
				$key_facebookcredit="0".$key_facebookcredit;
			}

			$productkey = sprintf ("%02d",$category)."_".sprintf ("%03d",$key_facebookcredit)."_".substr(uniqid(), -7);
			
			if($facebookcredit == 10)
			{
				$product_name = number_format($basecoin)." Coins (+".$vip_point." VIP Club Point)";
				$descripton = "+".$vip_point." VIP Club Point, ".number_format($basecoin)." Coins Per $1";
			}
			else if($facebookcredit == 50)
			{
					$extra_coins = $basecoin-(300000*($facebookcredit/10));
					$product_name = number_format($basecoin)." Coins (+".$vip_point." VIP Club Point)";
					$descripton = "+ ".number_format($extra_coins)." Extra Coins, +".$vip_point." VIP Club Point, ".number_format(($basecoin/($facebookcredit/10)))." Coins Per $1";
			}
			else if($facebookcredit == 90)			
			{
					$extra_coins = $basecoin-(300000*($facebookcredit/10));
					$product_name = number_format($basecoin)." Coins (+".$vip_point." VIP Club Point)";
					$descripton = "+ ".number_format($extra_coins)." Extra Coins, +".$vip_point." VIP Club Point, ".number_format(($basecoin/($facebookcredit/10)))." Coins Per $1";
			}
			else if($facebookcredit == 190)
			{
					$extra_coins = $basecoin-(300000*($facebookcredit/10));
					$product_name = number_format($basecoin)." Coins (+".$vip_point." VIP Club Point)";
					$descripton = "+ ".number_format($extra_coins)." Extra Coins, +".$vip_point." VIP Club Point, ".number_format(($basecoin/($facebookcredit/10)))." Coins Per $1";
			}
			else if($facebookcredit == 390)
			{
					$extra_coins = $basecoin-(300000*($facebookcredit/10));
					$product_name = number_format($basecoin)." Coins (+1 Extra Bonus, +".$vip_point." VIP Club Point)";
					$descripton = "+ 1 Extra Bonus, + ".number_format($extra_coins)." Extra Coins, +".$vip_point." VIP Club Point, ".number_format(($basecoin/($facebookcredit/10)))." Coins Per $1";
			}
			else if($facebookcredit == 590)
			{
					$extra_coins = $basecoin-(300000*($facebookcredit/10));
					$product_name = number_format($basecoin)." Coins (+2 Extra Bonus, +".$vip_point." VIP Club Point)";
					$descripton = "+ 2 Extra Bonus, + ".number_format($extra_coins)." Extra Coins, +".$vip_point." VIP Club Point, ".number_format(($basecoin/($facebookcredit/10)))." Coins Per $1";
			}
			else if($facebookcredit == 990)
			{
					$extra_coins = $basecoin-(300000*($facebookcredit/10));
					$product_name = number_format($basecoin)." Coins (+3 Extra Bonus, +".$vip_point." VIP Club Point)";
					$descripton = "+ 3 Extra Bonus, + ".number_format($extra_coins)." Extra Coins, +".$vip_point." VIP Club Point, ".number_format(($basecoin/($facebookcredit/10)))." Coins Per $1";
			}
			else if($facebookcredit == 1990)
			{
					$extra_coins = $basecoin-(300000*($facebookcredit/10));
					$product_name = number_format($basecoin)." Coins (+4 Extra Bonus, +".$vip_point." VIP Club Point)";
					$descripton = "+ 4 Extra Bonus, + ".number_format($extra_coins)." Extra Coins, +".$vip_point." VIP Club Point, ".number_format(($basecoin/($facebookcredit/10)))." Coins Per $1";
			}
			else if($facebookcredit == 2990)
			{
					$extra_coins = $basecoin-(300000*($facebookcredit/10));
					$product_name = number_format($basecoin)." Coins (+4 Extra Bonus, +".$vip_point." VIP Club Point)";
					$descripton = "+ 4 Extra Bonus, + ".number_format($extra_coins)." Extra Coins, +".$vip_point." VIP Club Point, ".number_format(($basecoin/($facebookcredit/10)))." Coins Per $1";
			}
			else if($facebookcredit == 4990)
			{
					$extra_coins = $basecoin-(300000*($facebookcredit/10));
					$product_name = number_format($basecoin)." Coins (+5 Extra Bonus, +".$vip_point." VIP Club Point)";
					$descripton = "+ 5 Extra Bonus, + ".number_format($extra_coins)." Extra Coins, +".$vip_point." VIP Club Point, ".number_format(($basecoin/($facebookcredit/10)))." Coins Per $1";
			}
			else if($facebookcredit == 7770)
			{
					$extra_coins = $basecoin-(300000*($facebookcredit/10));
					$product_name = number_format($basecoin)." Coins (+5 Extra Bonus, +".$vip_point." VIP Club Point)";
					$descripton = "+ 5 Extra Bonus, + ".number_format($extra_coins)." Extra Coins, +".$vip_point." VIP Club Point, ".number_format(($basecoin/($facebookcredit/10)))." Coins Per $1";
			}
			else if($facebookcredit == 9990)
			{
					$extra_coins = $basecoin-(300000*($facebookcredit/10));
					$product_name = number_format($basecoin)." Coins (+5 Extra Bonus, +".$vip_point." VIP Club Point)";
					$descripton = "+ 5 Extra Bonus, + ".number_format($extra_coins)." Extra Coins, +".$vip_point." VIP Club Point, ".number_format(($basecoin/($facebookcredit/10)))." Coins Per $1";
			}
				
			if($p==0)
					$insert_sql .= "VALUES('$productkey', $category, '$product_name', '$descripton'";
			else
				
			$insert_sql .= ",('$productkey', $category, '$product_name', '$descripton'";
			
			$insert_sql .= ", 'https://take5slots.com/facebook/images/product/product_coin_$money.png', '0','$facebookcredit','$vip_point',0,0,'1',1,now())";
			
			if(($i+1)==sizeof($product_list))
								$insert_sql .= ";";
			
			$excel_contents .= "<tr>".
					"<td>0</td>".
					"<td>".$productkey."</td>".
					"<td>".$facebookcredit."</td>".
					"<td>".$product_name."</td>".
					"<td>".$descripton."</td>".
					"</tr>";
		}
		$db_main->execute($insert_sql);
		$excel_contents .= "</table>";
	
		Header("Content-type: application/x-msdownload");
		Header("Content-type: application/x-msexcel");
		Header("Content-Disposition: attachment; filename=$filename");
	
		echo($excel_contents);
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	$db_main->end();
?>