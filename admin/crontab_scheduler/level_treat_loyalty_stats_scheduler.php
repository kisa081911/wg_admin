<?
	include("../common/common_include.inc.php");
	include("../common/dbconnect/db_util_redshift.inc.php");
	
	$db_main = new CDatabase_Main();	
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	$db_main->execute("SET wait_timeout=72000");	
	$db_other->execute("SET wait_timeout=72000");
	
	$issuccess = "1";
		
	$port = "";
	
	$str_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/level_treat_loyalty_stats_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/level_treat_loyalty_stats_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("level_treat_loyalty_stats_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	try 
	{
		$today = date("Y-m-d", time() - 60 * 60 * 24);
		
		//전체
		$sql = "SELECT ROUND(SUM(sum_treatamount) / COUNT(recv_useridx),2) AS avg_have_treatamount ".
				"FROM ".
				"( ".
				"	SELECT recv_useridx, SUM(treatamount) AS sum_treatamount ". 
				"	FROM tbl_treat_ticket ". 
				"	WHERE recv_useridx > $str_useridx ". 
				"	GROUP BY recv_useridx ".
				") t1";
		$avg_have_treatamount_0 = $db_main->getvalue($sql);		
		
		$sql = "SELECT ROUND(SUM(loyaltypot)/COUNT(useridx), 2) AS avg_have_loyaltypot ".
				"FROM tbl_user_treat ". 
				"WHERE useridx > $str_useridx";
		$avg_have_loyaltypot_0 = $db_main->getvalue($sql);
		
		$treat_loyalty_rate_0 = ($avg_have_treatamount_0/$avg_have_loyaltypot_0)*100;
		
		$sql = "SELECT ROUND(SUM(total_treatamount) / SUM(cnt), 2) AS avg_treatamount ".
				"FROM ".
				"( ".
				"	SELECT treatamount, COUNT(treatamount) AS cnt, treatamount*COUNT(treatamount) AS total_treatamount ". 
				"	FROM tbl_treat_ticket ". 
				"	WHERE recv_useridx > $str_useridx ". 
				"	GROUP BY treatamount ".
				") t1;";
		$avg_treatamount_0 = $db_main->getvalue($sql);
		
		//일별 로열티 팟 &트리트 티켓 생성 금액
		$sql=" SELECT SUM(treatamount) AS sum_treatamount ".  
		  	  " FROM tbl_treat_ticket  ".
	          " WHERE recv_useridx > $str_useridx AND DATE_FORMAT(writedate,'%Y-%m-%d') = '$today'";
		$daily_total_treatamount_0 = $db_main->getvalue($sql);
		
		//일별 트리트 티켓 환전 금액
		$sql = "  SELECT sum(amount) FROM t5_user_freecoin_log WHERE type IN (5,26) ".
		         "AND writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59'";
		$daily_cheange_treatamount_0 = $db_redshift->getvalue($sql);
		
		$sql = "INSERT INTO tbl_level_treat_loyaltypot_stats_daily(today, level, avg_have_treatamount, avg_have_loyaltyamount, treat_loyalty_rate, avg_treatamount,daily_total_treatamount,daily_change_treatamount) VALUES('$today', 0, '$avg_have_treatamount_0', '$avg_have_loyaltypot_0', '$treat_loyalty_rate_0', '$avg_treatamount_0','$daily_total_treatamount_0','$daily_cheange_treatamount_0') ";
		$sql .=" ON DUPLICATE KEY UPDATE avg_have_treatamount=VALUES(avg_have_treatamount),avg_have_loyaltyamount=VALUES(avg_have_loyaltyamount),treat_loyalty_rate=VALUES(treat_loyalty_rate),avg_treatamount=VALUES(avg_treatamount),daily_total_treatamount=VALUES(daily_total_treatamount),daily_change_treatamount=VALUES(daily_change_treatamount);";
		$db_other->execute($sql);		
		
		//VIP 전체
		$sql = "SELECT ROUND(SUM(sum_treatamount)/COUNT(useridx),2) AS avg_have_treatamount ".
				"FROM ".
				"( ".
				"	SELECT t1.useridx AS useridx, IFNULL(SUM(treatamount),0) AS sum_treatamount ".
				"	FROM ".
				"	( ".
				"		SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ".
				"		FROM tbl_product_order ". 
				"		WHERE useridx > $str_useridx AND STATUS = 1 ". 
				"		GROUP BY useridx ".
				"		HAVING sum_credit >= 990 AND sum_vip >= 99 ".
				"	) t1 LEFT JOIN tbl_treat_ticket t2 ON t1.useridx = t2.recv_useridx ".
				"	GROUP BY t1.useridx ".
				") t3;";
		$avg_have_treatamount_1 = $db_main->getvalue($sql);
		
		
		$sql = "SELECT ROUND(SUM(sum_loyaltypot)/COUNT(useridx), 2) AS avg_have_loyaltypot ".
				"FROM ".
				"( ".
				"	SELECT t1.useridx AS useridx, SUM(loyaltypot) AS sum_loyaltypot ". 
				"	FROM ".
				"	( ".
				"		SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ".
				"		FROM tbl_product_order ". 
				"		WHERE useridx > $str_useridx AND STATUS = 1 ". 
				"		GROUP BY useridx ".
				"		HAVING sum_credit >= 990 AND sum_vip >= 99 ".
				"	) t1 LEFT JOIN tbl_user_treat t2 ON t1.useridx = t2.useridx ".
				"	GROUP BY t1.useridx".
				") t3;";
		$avg_have_loyaltypot_1 = $db_main->getvalue($sql);
		
		$treat_loyalty_rate_1 = ($avg_have_treatamount_1/$avg_have_loyaltypot_1)*100;
		
		$sql = "SELECT ROUND(SUM(total_treatamount) / SUM(cnt), 2) AS avg_treatamount ".
				"FROM ".
				"( ".
				"	SELECT treatamount, COUNT(treatamount) AS cnt, treatamount*COUNT(treatamount) AS total_treatamount ". 
				"	FROM ".
				"	( ".
				"		SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ".
				"		FROM tbl_product_order ". 
				"		WHERE useridx > $str_useridx AND STATUS = 1 ". 
				"		GROUP BY useridx ".
				"		HAVING sum_credit >= 990 AND sum_vip >= 99 ".
				"	) t1 LEFT JOIN tbl_treat_ticket t2 ON t1.useridx = t2.recv_useridx ".
				"	GROUP BY t2.treatamount ".
				") t3;";		
		$avg_treatamount_1 = $db_main->getvalue($sql);
		
		//일별 로열티 팟 &트리트 티켓 생성 금액
		$sql=" SELECT SUM(total_treatamount) AS total_treatamount ".
		  		" FROM ".
                " ( ".
                " SELECT treatamount, COUNT(treatamount) AS cnt, treatamount*COUNT(treatamount) AS total_treatamount ".
                "	FROM  ".
                "	( ".
                "		SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ". 
                "	    FROM tbl_product_order  ".
                "		WHERE useridx > $str_useridx AND STATUS = 1 ".  
                "	    GROUP BY useridx ".
                "		HAVING sum_credit >= 990 AND sum_vip >= 99 ". 
                "	    ) t1 LEFT JOIN tbl_treat_ticket t2 ON t1.useridx = t2.recv_useridx AND DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' ".
                "	GROUP BY t2.treatamount ".
                " ) t3;";
		$daily_total_treatamount_1 = $db_main->getvalue($sql);
		
		//일별 트리트 티켓 환전 금액
		$sql = " select sum(amount) From t5_user_freecoin_log t1 ".
		  		" join ( ".
                "       SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ". 
		  	    "	    FROM t5_product_order   ".
                " 	    	WHERE useridx > $str_useridx AND STATUS = 1 ".  
		  		"    GROUP BY useridx ".
                "		    HAVING SUM(facebookcredit) >= 990 AND SUM(vip_point) >= 99 ". 
		  		"   ) t2 ".
                " on t1.useridx = t2.useridx ".
                " where type IN (5,26) ".
                " AND writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59'";
		$daily_cheange_treatamount_1 = $db_redshift->getvalue($sql);
		
		$sql = "INSERT INTO tbl_level_treat_loyaltypot_stats_daily(today, level, avg_have_treatamount, avg_have_loyaltyamount, treat_loyalty_rate, avg_treatamount,daily_total_treatamount,daily_change_treatamount) VALUES('$today', 1, '$avg_have_treatamount_1', '$avg_have_loyaltypot_1', '$treat_loyalty_rate_1', '$avg_treatamount_1','$daily_total_treatamount_1','$daily_cheange_treatamount_1') ";
		$sql .=" ON DUPLICATE KEY UPDATE avg_have_treatamount=VALUES(avg_have_treatamount),avg_have_loyaltyamount=VALUES(avg_have_loyaltyamount),treat_loyalty_rate=VALUES(treat_loyalty_rate),avg_treatamount=VALUES(avg_treatamount),daily_total_treatamount=VALUES(daily_total_treatamount),daily_change_treatamount=VALUES(daily_change_treatamount);";
		$db_other->execute($sql);
		
		//결제자
		$sql = "SELECT ROUND(SUM(sum_treatamount)/COUNT(useridx),2) AS avg_have_treatamount ".
				"FROM ".
				"( ".
				"	SELECT t1.useridx AS useridx, IFNULL(SUM(treatamount),0) AS sum_treatamount ".
				"	FROM ".
				"	( ".
				"		SELECT useridx ".
				"		FROM tbl_product_order ". 
				"		WHERE useridx > $str_useridx AND STATUS = 1 ". 
				"		GROUP BY useridx ".
				"	) t1 LEFT JOIN tbl_treat_ticket t2 ON t1.useridx = t2.recv_useridx ".
				"	GROUP BY t1.useridx ".
				") t3;";
		$avg_have_treatamount_2 = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(SUM(sum_loyaltypot)/COUNT(useridx), 2) AS avg_have_loyaltypot ".
				"FROM ".
				"( ".
				"	SELECT t1.useridx AS useridx, SUM(loyaltypot) AS sum_loyaltypot ". 
				"	FROM ".
				"	( ".
				"		SELECT useridx ".
				"		FROM tbl_product_order ". 
				"		WHERE useridx > $str_useridx AND STATUS = 1 ". 
				"		GROUP BY useridx ".		
				"	) t1 LEFT JOIN tbl_user_treat t2 ON t1.useridx = t2.useridx ".
				"	GROUP BY t1.useridx ".
				") t3;";
		$avg_have_loyaltypot_2 = $db_main->getvalue($sql);
		
		$treat_loyalty_rate_2 = ($avg_have_treatamount_2/$avg_have_loyaltypot_2)*100;
		
		$sql = "SELECT ROUND(SUM(total_treatamount) / SUM(cnt), 2) AS avg_treatamount ".
				"FROM ".
				"( ".
				"	SELECT treatamount, COUNT(treatamount) AS cnt, treatamount*COUNT(treatamount) AS total_treatamount ". 
				"	FROM tbl_treat_ticket t1 ".
				"	WHERE recv_useridx > $str_useridx ". 
				"	AND NOT EXISTS (SELECT * FROM tbl_product_order WHERE useridx=t1.recv_useridx AND STATUS = 1) ".
				"	GROUP BY treatamount ".
				") t1;";
		$avg_treatamount_2 = $db_main->getvalue($sql);
		
		
		//일별 로열티 팟 &트리트 티켓 생성 금액
		$sql=" SELECT SUM(total_treatamount) AS total_treatamount ".
		  		" FROM ".
		  		" ( ".
		  		" SELECT treatamount, COUNT(treatamount) AS cnt, treatamount*COUNT(treatamount) AS total_treatamount ".
		  		"	FROM  ".
		  		"	( ".
		  		"		SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ".
		  		"	    FROM tbl_product_order  ".
		  		"		WHERE useridx > $str_useridx AND STATUS = 1 ".
		  		"	    GROUP BY useridx ".
		  		"	    ) t1 LEFT JOIN tbl_treat_ticket t2 ON t1.useridx = t2.recv_useridx AND DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' ".
		  		"	GROUP BY t2.treatamount ".
		  		" ) t3;";
		$daily_total_treatamount_2 = $db_main->getvalue($sql);
		
		//일별 트리트 티켓 환전 금액
		$sql = " select sum(amount) From t5_user_freecoin_log t1 ".
		  		" join ( ".
		  		"       SELECT useridx ".
		  		"	    FROM t5_product_order   ".
		  		" 	    WHERE useridx > $str_useridx AND STATUS = 1 ".
		  		"       GROUP BY useridx ".
		  		"   ) t2 ".
		  		" on t1.useridx = t2.useridx ".
		  		" where type IN (5,26) ".
		  		" AND writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59'";
		$daily_cheange_treatamount_2 = $db_redshift->getvalue($sql);
		
		$sql = "INSERT INTO tbl_level_treat_loyaltypot_stats_daily(today, level, avg_have_treatamount, avg_have_loyaltyamount, treat_loyalty_rate, avg_treatamount,daily_total_treatamount,daily_change_treatamount) VALUES('$today', 2, '$avg_have_treatamount_2', '$avg_have_loyaltypot_2', '$treat_loyalty_rate_2', '$avg_treatamount_2','$daily_total_treatamount_2','$daily_cheange_treatamount_2') ";
		$sql .=" ON DUPLICATE KEY UPDATE avg_have_treatamount=VALUES(avg_have_treatamount),avg_have_loyaltyamount=VALUES(avg_have_loyaltyamount),treat_loyalty_rate=VALUES(treat_loyalty_rate),avg_treatamount=VALUES(avg_treatamount),daily_total_treatamount=VALUES(daily_total_treatamount),daily_change_treatamount=VALUES(daily_change_treatamount);";
		$db_other->execute($sql);
		
		//당일 결제 VIP
		$sql = "SELECT ROUND(SUM(sum_treatamount)/COUNT(useridx),2) AS avg_have_treatamount ". 
				"FROM ".
				"( ".
				"	SELECT t1.useridx AS useridx, IFNULL(SUM(treatamount),0) AS sum_treatamount ".
				"	FROM ".
				"	( ".
				"		SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ".
				"		FROM tbl_product_order ". 
				"		WHERE useridx > $str_useridx AND STATUS = 1 ". 
				"		GROUP BY useridx ".
				"		HAVING sum_credit >= 990 AND sum_vip >= 99 ".
				"	) t1 LEFT JOIN tbl_treat_ticket t2 ON t1.useridx = t2.recv_useridx ". 
				"	WHERE EXISTS (SELECT * FROM tbl_product_order WHERE useridx = t1.useridx AND STATUS = 1 AND '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59') ". 
				"	GROUP BY t1.useridx ".
				") t3;";
		$avg_have_treatamount_3 = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(SUM(sum_loyaltypot)/COUNT(useridx), 2) AS avg_have_loyaltypot ". 
				"FROM ".	
				"( ".
				"	SELECT t1.useridx AS useridx, SUM(loyaltypot) AS sum_loyaltypot ".  
				"	FROM ".
				"	( ".
				"		SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ".
				"		FROM tbl_product_order ". 
				"		WHERE useridx > $str_useridx AND STATUS = 1 ". 
				"		GROUP BY useridx ".
				"		HAVING sum_credit >= 990 AND sum_vip >= 99 ".
				"	) t1 LEFT JOIN tbl_user_treat t2 ON t1.useridx = t2.useridx ". 
				"	WHERE EXISTS (SELECT * FROM tbl_product_order WHERE useridx = t1.useridx AND STATUS = 1 AND '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59') ". 
				"	GROUP BY t1.useridx ".
				") t3;";
		$avg_have_loyaltypot_3 = $db_main->getvalue($sql);
		
		$treat_loyalty_rate_3 = ($avg_have_treatamount_3/$avg_have_loyaltypot_3)*100;
		
		$sql = "SELECT ROUND(SUM(total_treatamount) / SUM(cnt), 2) AS avg_treatamount ".
				"FROM ".
				"( ".
				"	SELECT treatamount, COUNT(treatamount) AS cnt, treatamount*COUNT(treatamount) AS total_treatamount ". 
				"	FROM ".
				"	( ".
				"		SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ".
				"		FROM tbl_product_order ". 
				"		WHERE useridx > $str_useridx AND STATUS = 1 ". 
				"		GROUP BY useridx ".
				"		HAVING sum_credit >= 990 AND sum_vip >= 99 ".
				"	) t1 LEFT JOIN tbl_treat_ticket t2 ON t1.useridx = t2.recv_useridx ". 
				"	WHERE EXISTS (SELECT * FROM tbl_product_order WHERE useridx = t1.useridx AND STATUS = 1 AND '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59') ". 
				"	GROUP BY t2.treatamount ".
				"	) t3;";
		$avg_treatamount_3 = $db_main->getvalue($sql);
		
		//일별 로열티 팟 &트리트 티켓 생성 금액
		$sql=" SELECT SUM(total_treatamount) AS total_treatamount ".
		  		" FROM ".
		  		" ( ".
		  		" SELECT treatamount, COUNT(treatamount) AS cnt, treatamount*COUNT(treatamount) AS total_treatamount ".
		  		"	FROM  ".
		  		"	( ".
		  		"		SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ".
		  		"	    FROM tbl_product_order  ".
		  		"		WHERE useridx > $str_useridx AND STATUS = 1 ".
		  		"	    GROUP BY useridx ".
		  		"		HAVING sum_credit >= 990 AND sum_vip >= 99 ".
		  		"	    ) t1 LEFT JOIN tbl_treat_ticket t2 ON t1.useridx = t2.recv_useridx AND DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' ".
		  		"	WHERE EXISTS (SELECT * FROM tbl_product_order WHERE useridx = t1.useridx AND STATUS = 1 AND '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59') ".
		  		"	GROUP BY t2.treatamount ".
		  		" ) t3;";
		$daily_total_treatamount_3 = $db_main->getvalue($sql);
		
		//일별 트리트 티켓 환전 금액
		$sql = " select sum(amount) From t5_user_freecoin_log t1 ".
		  		" join ( ".
		  		"       SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ".
		  		"	    FROM t5_product_order   ".
		  		" 	    	WHERE useridx > $str_useridx AND STATUS = 1 ".
		  		"    GROUP BY useridx ".
		  		"		    HAVING SUM(facebookcredit) >= 990 AND SUM(vip_point) >= 99 ".
		  		"   ) t2 ".
		  		" on t1.useridx = t2.useridx ".
		  		" where type IN (5,26) ".
		  		" AND EXISTS (SELECT * FROM t5_product_order WHERE useridx = t1.useridx AND STATUS = 1 AND '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59') ".
		  		" AND writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59'";
		$daily_cheange_treatamount_3 = $db_redshift->getvalue($sql);
		
		$sql = "INSERT INTO tbl_level_treat_loyaltypot_stats_daily(today, level, avg_have_treatamount, avg_have_loyaltyamount, treat_loyalty_rate, avg_treatamount,daily_total_treatamount,daily_change_treatamount) VALUES('$today', 3, '$avg_have_treatamount_3', '$avg_have_loyaltypot_3', '$treat_loyalty_rate_3', '$avg_treatamount_3','$daily_total_treatamount_3','$daily_cheange_treatamount_3') ";
		$sql .=" ON DUPLICATE KEY UPDATE avg_have_treatamount=VALUES(avg_have_treatamount),avg_have_loyaltyamount=VALUES(avg_have_loyaltyamount),treat_loyalty_rate=VALUES(treat_loyalty_rate),avg_treatamount=VALUES(avg_treatamount),daily_total_treatamount=VALUES(daily_total_treatamount),daily_change_treatamount=VALUES(daily_change_treatamount);";
		$db_other->execute($sql);
		
		//비결제자
		$sql = "SELECT ROUND(SUM(sum_treatamount)/COUNT(useridx),2) AS avg_have_treatamount ". 
				"FROM ".
				"( ".
				"	SELECT t1.recv_useridx AS useridx, IFNULL(SUM(treatamount),0) AS sum_treatamount ".
				"	FROM tbl_treat_ticket t1 ". 
				"	WHERE t1.recv_useridx > $str_useridx ". 
				"	AND NOT EXISTS (SELECT * FROM tbl_product_order WHERE useridx=t1.recv_useridx AND STATUS = 1) ". 
				"	GROUP BY t1.recv_useridx ".
				") t3;";
		$avg_have_treatamount_4 = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(SUM(loyaltypot)/COUNT(useridx), 2) AS avg_have_loyaltypot ".
				"FROM tbl_user_treat t1 ".
				"WHERE useridx > $str_useridx ".
				"AND NOT EXISTS (SELECT * FROM tbl_product_order WHERE useridx=t1.useridx AND STATUS = 1);";
		$avg_have_loyaltypot_4 = $db_main->getvalue($sql);
		
		$treat_loyalty_rate_4 = ($avg_have_treatamount_4/$avg_have_loyaltypot_4)*100;
		
		$sql = "SELECT ROUND(SUM(total_treatamount) / SUM(cnt), 2) AS avg_treatamount ".
				"FROM ".
				"( ".
				"	SELECT treatamount, COUNT(treatamount) AS cnt, treatamount*COUNT(treatamount) AS total_treatamount ".
				"	FROM ".
				"	( ".
				"		SELECT useridx ".
				"		FROM tbl_product_order ". 
				"		WHERE useridx > $str_useridx AND STATUS = 1 ". 
				"		GROUP BY useridx ".		
				"	) t1 LEFT JOIN tbl_treat_ticket t2 ON t1.useridx = t2.recv_useridx ".
				"	GROUP BY t2.treatamount ".
				") t3;";
		$avg_treatamount_4 = $db_main->getvalue($sql);
		
		//일별 로열티 팟 &트리트 티켓 생성 금액
		$sql=" SELECT SUM(treatamount) AS sum_treatamount ".
		  		" FROM tbl_treat_ticket t1  ".
		  		" WHERE recv_useridx > $str_useridx AND DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' ".
		  		" AND NOT EXISTS (SELECT * FROM tbl_product_order WHERE useridx=t1.useridx AND STATUS = 1); ";
		$daily_total_treatamount_4 = $db_main->getvalue($sql);
		
		//일별 트리트 티켓 환전 금액
		$sql = "  SELECT sum(amount) FROM t5_user_freecoin_log t1 WHERE type IN (5,26) ".
		  	   "  AND writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59'".
		  	   "  AND NOT EXISTS (SELECT * FROM t5_product_order WHERE useridx = t1.useridx AND STATUS = 1)";
		$daily_cheange_treatamount_4 = $db_redshift->getvalue($sql);
		
		$sql = "INSERT INTO tbl_level_treat_loyaltypot_stats_daily(today, level, avg_have_treatamount, avg_have_loyaltyamount, treat_loyalty_rate, avg_treatamount,daily_total_treatamount,daily_change_treatamount) VALUES('$today', 4, '$avg_have_treatamount_4', '$avg_have_loyaltypot_4', '$treat_loyalty_rate_4', '$avg_treatamount_4','$daily_total_treatamount_4','$daily_cheange_treatamount_4') ";
		$sql .=" ON DUPLICATE KEY UPDATE avg_have_treatamount=VALUES(avg_have_treatamount),avg_have_loyaltyamount=VALUES(avg_have_loyaltyamount),treat_loyalty_rate=VALUES(treat_loyalty_rate),avg_treatamount=VALUES(avg_treatamount),daily_total_treatamount=VALUES(daily_total_treatamount),daily_change_treatamount=VALUES(daily_change_treatamount);";
		$db_other->execute($sql);
		
		//당일 비결제 VIP
		$sql = "SELECT ROUND(SUM(sum_treatamount)/COUNT(useridx),2) AS avg_have_treatamount ".
				"FROM ".
				"( ".
				"	SELECT t1.useridx AS useridx, IFNULL(SUM(treatamount),0) AS sum_treatamount ".
				"	FROM ".
				"	( ".
				"		SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ".
				"		FROM tbl_product_order ". 
				"		WHERE useridx > $str_useridx AND STATUS = 1 ". 
				"		GROUP BY useridx ".
				"		HAVING sum_credit >= 990 AND sum_vip >= 99 ".
				"	) t1 LEFT JOIN tbl_treat_ticket t2 ON t1.useridx = t2.recv_useridx ". 
				"	WHERE NOT EXISTS (SELECT * FROM tbl_product_order WHERE useridx = t1.useridx AND STATUS = 1 AND '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59') ". 
				"	GROUP BY t1.useridx ".
				") t3;";
		$avg_have_treatamount_5 = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(SUM(sum_loyaltypot)/COUNT(useridx), 2) AS avg_have_loyaltypot ". 
				"FROM ".
				"( ".
				"	SELECT t1.useridx AS useridx, SUM(loyaltypot) AS sum_loyaltypot ". 
				"	FROM ".
				"	( ".
				"		SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ".
				"		FROM tbl_product_order ". 
				"		WHERE useridx > $str_useridx AND STATUS = 1 ". 
				"		GROUP BY useridx ".
				"		HAVING sum_credit >= 990 AND sum_vip >= 99 ".
				"	) t1 LEFT JOIN tbl_user_treat t2 ON t1.useridx = t2.useridx	". 
				"	WHERE NOT EXISTS (SELECT * FROM tbl_product_order WHERE useridx = t1.useridx AND STATUS = 1 AND '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59') ". 
				"	GROUP BY t1.useridx ".
				") t3;"; 
		$avg_have_loyaltypot_5 = $db_main->getvalue($sql);
		
		$treat_loyalty_rate_5 = ($avg_have_treatamount_5/$avg_have_loyaltypot_5)*100;
		
		$sql = "SELECT ROUND(SUM(total_treatamount) / SUM(cnt), 2) AS avg_treatamount ".
				"FROM ".
				"( ".
				"	SELECT treatamount, COUNT(treatamount) AS cnt, treatamount*COUNT(treatamount) AS total_treatamount FROM ".
				"	( ".
				"		SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ".
				"		FROM tbl_product_order ". 
				"		WHERE useridx > $str_useridx AND STATUS = 1 ". 
				"		GROUP BY useridx ".
				"		HAVING sum_credit >= 990 AND sum_vip >= 99 ".
				"	) t1 JOIN tbl_treat_ticket t2 ON t1.useridx = t2.recv_useridx ".
				"	WHERE NOT EXISTS (SELECT * FROM tbl_product_order WHERE useridx = t1.useridx AND STATUS = 1 AND '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59') ". 
				"	GROUP BY t2.treatamount ".
				") t3;";
		$avg_treatamount_5 = $db_main->getvalue($sql);
		
		//일별 로열티 팟 &트리트 티켓 생성 금액
		$sql=" SELECT SUM(total_treatamount) AS total_treatamount ".
		  		" FROM ".
		  		" ( ".
		  		" SELECT treatamount, COUNT(treatamount) AS cnt, treatamount*COUNT(treatamount) AS total_treatamount ".
		  		"	FROM  ".
		  		"	( ".
		  		"		SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ".
		  		"	    FROM tbl_product_order  ".
		  		"		WHERE useridx > $str_useridx AND STATUS = 1 ".
		  		"	    GROUP BY useridx ".
		  		"		HAVING sum_credit >= 990 AND sum_vip >= 99 ".
		  		"	    ) t1 LEFT JOIN tbl_treat_ticket t2 ON t1.useridx = t2.recv_useridx AND DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' ".
		  		"	WHERE NOT EXISTS (SELECT * FROM tbl_product_order WHERE useridx = t1.useridx AND STATUS = 1 AND '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59') ".
		  		"	GROUP BY t2.treatamount ".
		  		" ) t3;";
		$daily_total_treatamount_5 = $db_main->getvalue($sql);
		
		//일별 트리트 티켓 환전 금액
		$sql = " select sum(amount) From t5_user_freecoin_log t1 ".
		  		" join ( ".
		  		"       SELECT useridx, SUM(facebookcredit) AS sum_credit, SUM(vip_point) AS sum_vip ".
		  		"	    FROM t5_product_order   ".
		  		" 	    	WHERE useridx > $str_useridx AND STATUS = 1 ".
		  		"    GROUP BY useridx ".
		  		"		    HAVING SUM(facebookcredit) >= 990 AND SUM(vip_point) >= 99 ".
		  		"   ) t2 ".
		  		" on t1.useridx = t2.useridx ".
		  		" where type IN (5,26) ".
		  		" AND NOT EXISTS (SELECT * FROM t5_product_order WHERE useridx = t1.useridx AND STATUS = 1 AND '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59') ".
		  		" AND writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59'";
		$daily_cheange_treatamount_5 = $db_redshift->getvalue($sql);
		
		$sql = "INSERT INTO tbl_level_treat_loyaltypot_stats_daily(today, level, avg_have_treatamount, avg_have_loyaltyamount, treat_loyalty_rate, avg_treatamount,daily_total_treatamount,daily_change_treatamount) VALUES('$today', 5, '$avg_have_treatamount_5', '$avg_have_loyaltypot_5', '$treat_loyalty_rate_5', '$avg_treatamount_5','$daily_total_treatamount_5','$daily_cheange_treatamount_5') ";
		$sql .=" ON DUPLICATE KEY UPDATE avg_have_treatamount=VALUES(avg_have_treatamount),avg_have_loyaltyamount=VALUES(avg_have_loyaltyamount),treat_loyalty_rate=VALUES(treat_loyalty_rate),avg_treatamount=VALUES(avg_treatamount),daily_total_treatamount=VALUES(daily_total_treatamount),daily_change_treatamount=VALUES(daily_change_treatamount);";
		$db_other->execute($sql);		
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_redshift->end();
	$db_other->end();
?>