<?
	include("../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_livestats = new CDatabase_Livestats();
	$db_other = new CDatabase_Other();
	
	$db_main->execute("SET wait_timeout=300");
	$db_main2->execute("SET wait_timeout=300");
	$db_analysis->execute("SET wait_timeout=300");
	$db_livestats->execute("SET wait_timeout=300");
	$db_other->execute("SET wait_timeout=300");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$port = "";
	
	$str_useridx = 20000;
	
	$standard_date = date("Y-m-d H:i").":00";
	$standard_shortdate = substr($standard_date, 0, 10);
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}

	try
	{
		$sql = "INSERT INTO tbl_scheduler_log(today, type, status, writedate) VALUES('$today', 201, 0, NOW());";
		$db_analysis->execute($sql);
		$sql = "SELECT LAST_INSERT_ID();";
		$logidx = $db_analysis->getvalue($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/5minute_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/5minute_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("5minute_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}

	try
	{
		$standard_date = date("Y-m-d H:i").":00";
		$standard_shortdate = date("Y-m-d");
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		
		// tbl_inbox_collect 통계(WakeUp)
		for ($i=0; $i<20; $i++)
		{
			$sql = "SELECT DATE_FORMAT(MAX(writedate), '%Y-%m-%d %H:%i:00') AS writedate, type AS category, COUNT(logidx) AS freecount, SUM(coin) AS freeamount ".
					"FROM tbl_user_inbox_collect_$i ".
					"WHERE useridx > $str_useridx AND category = 500 AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
					"GROUP BY type";
			$free_list = $db_livestats->gettotallist($sql);
			
			$free_sql = "";
			
			for($j=0; $j<sizeof($free_list); $j++)
			{
				$free_writedate = $free_list[$j]["writedate"];
				$free_category = $free_list[$j]["category"];
				$free_type = 500; // Wake up
				$free_freecount = $free_list[$j]["freecount"];
				$free_freeamount = $free_list[$j]["freeamount"];
			
				if($free_sql == "")
					$free_sql = "INSERT INTO tbl_user_freecoin_stat(category, type, freecount, freeamount, writedate) VALUES($free_category, $free_type, $free_freecount, $free_freeamount, '$free_writedate')";
				else
					$free_sql .= ", ($free_category, $free_type, $free_freecount, $free_freeamount, '$free_writedate')";
			}
			
			if($free_sql != "")
				$db_analysis->execute($free_sql);
		}
		
		// tbl_inbox_collect 통계(Invite)
		for ($i=0; $i<20; $i++)
		{
			$sql = "SELECT DATE_FORMAT(MAX(writedate), '%Y-%m-%d %H:%i:00') AS writedate, type AS category, COUNT(logidx) AS freecount, SUM(coin) AS freeamount ".
					"FROM tbl_user_inbox_collect_$i ".
					"WHERE useridx > $str_useridx AND category = 400 AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
					"GROUP BY type";
			$free_list = $db_livestats->gettotallist($sql);
						
			$free_sql = "";
						
			for($j=0; $j<sizeof($free_list); $j++)
			{
				$free_writedate = $free_list[$j]["writedate"];
				$free_category = $free_list[$j]["category"];
				$free_type = 21; // Inbox Invite
				$free_freecount = $free_list[$j]["freecount"];
				$free_freeamount = $free_list[$j]["freeamount"];
						
				if($free_sql == "")
					$free_sql = "INSERT INTO tbl_user_freecoin_stat(category, type, freecount, freeamount, writedate) VALUES($free_category, $free_type, $free_freecount, $free_freeamount, '$free_writedate')";
				else
					$free_sql .= ", ($free_category, $free_type, $free_freecount, $free_freeamount, '$free_writedate')";
			}
						
			if($free_sql != "")
				$db_analysis->execute($free_sql);
		}
		
// 		// tbl_freecoin_daily 통계
// 		$sql = "INSERT INTO tbl_freecoin_daily(today, category, type, freecount, freeamount) ".
// 				"SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, category, type, SUM(freecount) AS total_cnt, SUM(freeamount) AS total_amount ".
// 				"FROM tbl_freecoin_stat ".
// 				"WHERE '$standard_shortdate 00:00:00' <= writedate AND writedate < '$standard_shortdate 23:59:59' ".
// 				"GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), category, type ".
// 				"ON DUPLICATE KEY UPDATE freecount = VALUES(freecount), freeamount = VALUES(freeamount)";
// 		$db_analysis->execute($sql);
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	
	try
	{
		// tbl_user_freecoin_log 통계
		for ($i=0; $i<10; $i++)
		{
			$sql = "SELECT DATE_FORMAT(MAX(writedate), '%Y-%m-%d %H:%i:00') AS writedate, category, type, inbox_type, COUNT(logidx) AS freecount, SUM(amount) AS freeamount, is_v2 ".
					"FROM tbl_user_freecoin_log_$i ".
					"WHERE useridx > $str_useridx AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
					"GROUP BY category, type, inbox_type, is_v2";
			$free_list = $db_main2->gettotallist($sql);
	
			$free_sql = "";
				
			for($j=0; $j<sizeof($free_list); $j++)
			{
				$free_writedate = $free_list[$j]["writedate"];
				$free_category = $free_list[$j]["category"];
				$free_type = $free_list[$j]["type"];
				$inbox_type = $free_list[$j]["inbox_type"];
				$free_freecount = $free_list[$j]["freecount"];
				$free_freeamount = $free_list[$j]["freeamount"];
				$is_v2 = $free_list[$j]["is_v2"];
	
				if($free_sql == "")
					$free_sql = "INSERT INTO tbl_user_freecoin_stat(category, type, inbox_type, freecount, freeamount, writedate, is_v2) VALUES($free_category, $free_type, $inbox_type, $free_freecount, $free_freeamount, '$free_writedate', '$is_v2')";
					else
						$free_sql .= ", ($free_category, $free_type, $inbox_type, $free_freecount, $free_freeamount, '$free_writedate', '$is_v2')";
			}
				
			if($free_sql != "")
				$db_analysis->execute($free_sql);
		}
	
		
		// tbl_freecoin_daily 통계
 		$sql = "INSERT INTO tbl_user_freecoin_daily(today, category, type, inbox_type, freecount, freeamount, is_v2) ".
 				"SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, category, type, inbox_type, SUM(freecount) AS total_cnt, SUM(freeamount) AS total_amount, is_v2 ".
 				"FROM tbl_user_freecoin_stat ".
 				"WHERE '$standard_shortdate 00:00:00' <= writedate AND writedate < '$standard_shortdate 23:59:59' ".
 				"GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), category, type, inbox_type, is_v2 ".
 				"ON DUPLICATE KEY UPDATE freecount = VALUES(freecount), freeamount = VALUES(freeamount)";
 		$db_analysis->execute($sql);
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try 
	{
		// tbl_newuser_retention_event_log 통계
		for ($i=0; $i<10; $i++)
		{
			$sql = "SELECT '$today' AS writedate, 0 AS category, $i AS type, ".
					"IFNULL(SUM(IF(amount = 100000, 1, 0)), 0) AS day_cnt_1, ".
					"IFNULL(SUM(IF(amount = 180000, 1, 0)), 0) AS day_cnt_2, ".
					"IFNULL(SUM(IF(amount = 200000, 1, 0)), 0) AS day_cnt_3, ".
					"IFNULL(SUM(IF(amount = 400000, 1, 0)), 0) AS day_cnt_4, ".
					"IFNULL(SUM(IF(amount = 600000, 1, 0)), 0) AS day_cnt_5, ".
					"IFNULL(SUM(IF(amount = 800000, 1, 0)), 0) AS day_cnt_6, ".
					"IFNULL(SUM(IF(amount = 1000000, 1, 0)), 0) AS day_cnt_7 ".
					"FROM `tbl_user_freecoin_log_$i` WHERE useridx > $str_useridx AND TYPE = 20 AND '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59'";
			$free_retention_list = $db_main2->gettotallist($sql);
		
			for($j=0; $j<sizeof($free_retention_list); $j++)
			{
				$free_writedate = $free_retention_list[$j]["writedate"];
				$free_category = $free_retention_list[$j]["category"];
				$free_type = $free_retention_list[$j]["type"];
				$free_day_1 = $free_retention_list[$j]["day_cnt_1"];
				$free_day_2 = $free_retention_list[$j]["day_cnt_2"];
				$free_day_3 = $free_retention_list[$j]["day_cnt_3"];
				$free_day_4 = $free_retention_list[$j]["day_cnt_4"];
				$free_day_5 = $free_retention_list[$j]["day_cnt_5"];
				$free_day_6 = $free_retention_list[$j]["day_cnt_6"];
				$free_day_7 = $free_retention_list[$j]["day_cnt_7"];
		
				
				$sql = "INSERT INTO tbl_newuser_retention_reward_daily(today, category, type, day_1, day_2, day_3, day_4, day_5, day_6, day_7) ".
						"VALUES ('$free_writedate', $free_category, $free_type, $free_day_1, $free_day_2, $free_day_3, $free_day_4, $free_day_5, $free_day_6, $free_day_7) ".
						"ON DUPLICATE KEY UPDATE day_1 = VALUES(day_1), day_2 = VALUES(day_2), day_3 = VALUES(day_3), day_4 = VALUES(day_4), day_5 = VALUES(day_5), day_6 = VALUES(day_6), day_7 = VALUES(day_7)";
				$db_analysis->execute($sql);
 			}		
		}
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		// Dash Board 결제
		// Web - start
		$today = date("Y-m-d");
		$yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));

		$dashstat_where = "WHERE useridx > $str_useridx ";
		
		// 최근 28일 결제 금액
		$sql = "SELECT SUM(total_money) AS total_money ".
				"FROM ( ".
				"	SELECT ROUND(IFNULL(SUM(facebookcredit),0)/10, 2) AS total_money ".
				"	FROM tbl_product_order ".
				"	$dashstat_where AND status IN (1, 3) AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate <= NOW() ".
				"	UNION ALL ".
				"	SELECT ROUND(IFNULL(SUM(money),0), 2) AS total_money ".
				"	FROM tbl_product_order_earn ".
				"	$dashstat_where AND status=1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate <= NOW() ".
				") t1";
		
		$order_28day = $db_main->getvalue($sql);
		
		// 최근 28일 결제 취소 금액
		$sql = "SELECT ROUND(IFNULL(SUM(facebookcredit),0)/10, 2) AS cancel_money ".
				"FROM tbl_product_order ".
				"$dashstat_where AND status=2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < canceldate AND canceldate <= NOW()";
		$order_cancel_28day = $db_main->getvalue($sql);

		// 최근 28일 결제 건수
		$sql = "SELECT SUM(total_count) AS total_count ".
				"FROM ( ".
				"	SELECT COUNT(*) AS total_count ".
				"	FROM tbl_product_order ".
				"	$dashstat_where AND status IN (1, 3) AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate <= NOW() ".
				"	UNION ALL ".
				"	SELECT COUNT(*) AS total_count ".
				"	FROM tbl_product_order_earn ".
				"	$dashstat_where AND status=1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate <= NOW() ".
				") t1";
		$count_28day = $db_main->getvalue($sql);
		
		// 최근 7일 결제 금액
		$sql = "SELECT SUM(total_money) AS total_money ".
				"FROM ( ".
				"	SELECT ROUND(IFNULL(SUM(facebookcredit),0)/10, 2) AS total_money ".
				"	FROM tbl_product_order ".
				"	$dashstat_where AND status IN (1, 3) AND DATE_SUB(NOW(), INTERVAL 1 WEEK) < writedate AND writedate <= NOW() ".
				"	UNION ALL ".
				"	SELECT ROUND(IFNULL(SUM(money),0), 2) AS total_money ".
				"	FROM tbl_product_order_earn ".
				"	$dashstat_where AND status=1 AND DATE_SUB(NOW(), INTERVAL 1 WEEK) < writedate AND writedate <= NOW() ".
				") t1";
		
		$order_7day = $db_main->getvalue($sql);
		
		// 최근 7일 결제 취소 금액
		$sql = "SELECT ROUND(IFNULL(SUM(facebookcredit),0)/10, 2) AS cancel_money ".
				"FROM tbl_product_order ".
				"$dashstat_where AND status=2 AND DATE_SUB(NOW(), INTERVAL 1 WEEK) < canceldate AND canceldate <= NOW()";
		$order_cancel_7day = $db_main->getvalue($sql);
		
		// 최근 7일 결제 건수
		$sql = "SELECT SUM(total_count) AS total_count ".
				"FROM ( ".
				"	SELECT COUNT(*) AS total_count ".
				"	FROM tbl_product_order ".
				"	$dashstat_where AND status IN (1, 3) AND DATE_SUB(NOW(), INTERVAL 1 WEEK) < writedate AND writedate <= NOW() ".
				"	UNION ALL ".
				"	SELECT COUNT(*) AS total_count ".
				"	FROM tbl_product_order_earn ".
				"	$dashstat_where AND status=1 AND DATE_SUB(NOW(), INTERVAL 1 WEEK) < writedate AND writedate <= NOW() ".
				") t1";
		$count_7day = $db_main->getvalue($sql);
		
		// 어제 결제 금액
		$sql = "SELECT SUM(total_money) AS total_money ".
				"FROM ( ".
				"	SELECT ROUND(IFNULL(SUM(facebookcredit),0)/10, 2) AS total_money ".
				"	FROM tbl_product_order ".
				"	$dashstat_where AND status IN (1, 3) AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
				"	UNION ALL ".
				"	SELECT ROUND(IFNULL(SUM(money),0), 2) AS total_money ".
				"	FROM tbl_product_order_earn ".
				"	$dashstat_where AND status=1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
				") t1";
		$order_yesterday = $db_main->getvalue($sql);

		// 어제 결제 취소 금액
		$sql = "SELECT ROUND(IFNULL(SUM(facebookcredit),0)/10, 2) AS cancel_money ".
				"FROM tbl_product_order ".
				"$dashstat_where AND status=2 AND canceldate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$order_cancel_yesterday = $db_main->getvalue($sql);
		
		// 어제 결제 건수
		$sql = "SELECT SUM(total_count) AS total_count ".
				"FROM ( ".
				"	SELECT COUNT(*) AS total_count ".
				"	FROM tbl_product_order ".
				"	$dashstat_where AND status IN (1, 3) AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
				"	UNION ALL ".
				"	SELECT COUNT(*) AS total_count ".
				"	FROM tbl_product_order_earn ".
				"	$dashstat_where AND status=1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
				") t1";
		$count_yesterday = $db_main->getvalue($sql);
			
		//type 1 : Web
		//subtype 
		//1 : 최근 28일 결제 금액, 2 : 최근 28일 결제 취소 금액, 3 : 최근 28일 결제 수
		//4 : 최근 7일 결제 금액, 5 : 최근 7일 결제 취소 금액, 6 : 최근 7일 결제 수
		//7 : 어제 결제 금액, 8 : 어제 결제 취소 금액, 9 : 어제 결제 수
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1, 1, '$order_28day', 'Pay(order_28day)', now()) ON DUPLICATE KEY UPDATE value='$order_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1, 2, '$order_cancel_28day', 'Pay(order_cancel_28day)', now()) ON DUPLICATE KEY UPDATE value='$order_cancel_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1, 3, '$count_28day', 'Pay(count_28day)', now()) ON DUPLICATE KEY UPDATE value='$count_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1, 4, '$order_7day', 'Pay(order_7day)', now()) ON DUPLICATE KEY UPDATE value='$order_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1, 5, '$order_cancel_7day', 'Pay(order_cancel_7day)', now()) ON DUPLICATE KEY UPDATE value='$order_cancel_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1, 6, '$count_7day', 'Pay(count_7day)', now()) ON DUPLICATE KEY UPDATE value='$count_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1, 7, '$order_yesterday', 'Pay(order_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$order_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1, 8, '$order_cancel_yesterday', 'Pay(order_cancel_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$order_cancel_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1, 9, '$count_yesterday', 'Pay(count_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$count_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// 기간별 할인율 계산
		$sql = "SELECT  ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				"FROM tbl_product_order ".
				"WHERE STATUS = 1 AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW()";
		$salerate_28days = $db_main->getvalue($sql);
		
		$sql = "SELECT  ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ". 
				" FROM ( ".
				"	SELECT coin, basecoin FROM tbl_product_order WHERE basecoin > 0 AND STATUS IN (1, 3) AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
				"	UNION ALL  ".
				"	SELECT coin, basecoin FROM tbl_product_order_mobile WHERE basecoin > 0 AND STATUS IN (1, 3) AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
				" ) t1";
		$total_salerate_28days = $db_main->getvalue($sql);
			
		$sql = "SELECT  ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				"FROM tbl_product_order ".
				"WHERE STATUS = 1 AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW()";
		$salerate_7days = $db_main->getvalue($sql);
		
		$sql = "SELECT  ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				" FROM ( ".
				"	SELECT coin, basecoin FROM tbl_product_order WHERE basecoin > 0 AND STATUS IN (1, 3) AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
				"	UNION ALL  ".
				"	SELECT coin, basecoin FROM tbl_product_order_mobile WHERE basecoin > 0 AND STATUS IN (1, 3) AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
				" ) t1";
		$total_salerate_7days = $db_main->getvalue($sql);
		
		$sql = "SELECT  ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				"FROM tbl_product_order ".
				"WHERE STATUS = 1 AND useridx > $str_useridx AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$salerate_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT  ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				" FROM ( ".
				"	SELECT coin, basecoin FROM tbl_product_order WHERE basecoin > 0 AND STATUS IN (1, 3) AND useridx > $str_useridx AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
				"	UNION ALL  ".
				"	SELECT coin, basecoin FROM tbl_product_order_mobile WHERE basecoin > 0 AND STATUS IN (1, 3) AND useridx > $str_useridx AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
				" ) t1";
		$total_salerate_yesterday = $db_main->getvalue($sql);
			
		//type 2 : Web
		//subtype
		//1 : 웹 최근 28일 기간별 할인율 , 2 : 웹 최근 7일 기간별 할인율, 3 : 웹 어제 기간별 할인율
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(2, 1, '$salerate_28days', 'Pay(salerate_28days)', now()) ON DUPLICATE KEY UPDATE value='$salerate_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(2, 2, '$salerate_7days', 'Pay(salerate_7days)', now()) ON DUPLICATE KEY UPDATE value='$salerate_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(2, 3, '$salerate_yesterday', 'Pay(salerate_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$salerate_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		//type 402 : Total
		//subtype
		//1 : 전체 최근 28일 기간별 할인율 , 2 : 전체 최근 7일 기간별 할인율, 3 : 전체 어제 기간별 할인율		
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(402, 1, '$total_salerate_28days', 'Pay(total_salerate_28days)', now()) ON DUPLICATE KEY UPDATE value='$total_salerate_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(402, 2, '$total_salerate_7days', 'Pay(total_salerate_7days)', now()) ON DUPLICATE KEY UPDATE value='$total_salerate_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(402, 3, '$total_salerate_yesterday', 'Pay(total_salerate_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$total_salerate_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// 기간별 비할인 구매율 계산
		$sql = 	"SELECT SUM(facebookcredit)/10 FROM tbl_product_order ".
				"WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND couponidx = 0 AND special_discount = 0 AND special_more = 0";				
		$normalpurchase_yesterday = $db_main->getvalue($sql);
			
		$sql = "SELECT SUM(facebookcredit)/10 FROM tbl_product_order ".
			   "WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND date_sub(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0";		
		$normalpurchase_7days = $db_main->getvalue($sql);
			
		$sql = 	"	SELECT SUM(facebookcredit)/10 FROM tbl_product_order ".
				"	WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND date_sub(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0";	
		$normalpurchase_28days = $db_main->getvalue($sql);
		
		$sql = 	"SELECT SUM(facebookcredit)/10 ". 
				" FROM ( ".
				"	SELECT facebookcredit FROM tbl_product_order  WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND couponidx = 0 AND special_discount = 0 AND special_more = 0 ".
				"	UNION ALL ".
				"	SELECT facebookcredit FROM tbl_product_order_mobile  WHERE STATUS = 1 AND useridx > $str_useridx AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND couponidx = 0 AND special_discount = 0 AND special_more = 0 ".
				"   UNION ALL ".
				"   SELECT ROUND(IFNULL(SUM(money),0), 1) AS facebookcredit FROM tbl_product_order_ironsource ".
				"   WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
				" ) t1";
		$total_normalpurchase_yesterday = $db_main->getvalue($sql);
		
		
		$sql = 	"	SELECT SUM(facebookcredit)/10 ".
				" FROM ( ".
				"    SELECT facebookcredit FROM tbl_product_order ".
				"    WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 ".
				"    UNION ALL ".
				"    SELECT facebookcredit FROM tbl_product_order_mobile ".
				"    WHERE STATUS = 1 AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 ".
				"    UNION ALL ".
				"    SELECT ROUND(IFNULL(SUM(money),0), 1) AS facebookcredit FROM tbl_product_order_ironsource ".
				"    WHERE DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
				" ) t1";
		$total_normalpurchase_7days = $db_main->getvalue($sql);
		
		
		$sql = 	"	SELECT SUM(facebookcredit)/10 ". 
					" FROM ( ".
					"    SELECT facebookcredit FROM tbl_product_order ". 
					"    WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 ".
					"    UNION ALL ".
					"    SELECT facebookcredit FROM tbl_product_order_mobile ".  
					"    WHERE STATUS = 1 AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 ".
					"    UNION ALL ".
					"    SELECT ROUND(IFNULL(SUM(money),0), 1) AS facebookcredit FROM tbl_product_order_ironsource ".  
					"    WHERE DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
					" ) t1";	
		$total_normalpurchase_28days = $db_main->getvalue($sql);
			
		//type 3 : Web
		//subtype
		//1 : 웹 최근 28일 비할인 구매율 , 2 : 웹 최근 7일 비할인 구매율, 3 : 웹 어제 비할인 구매율
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(3, 1, '$normalpurchase_28days', 'Pay(normalpurchase_28days)', now()) ON DUPLICATE KEY UPDATE value='$normalpurchase_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(3, 2, '$normalpurchase_7days', 'Pay(normalpurchase_7days)', now()) ON DUPLICATE KEY UPDATE value='$normalpurchase_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(3, 3, '$normalpurchase_yesterday', 'Pay(normalpurchase_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$normalpurchase_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		//type 403 : Total
		//subtype
		//1 : 전체 최근 28일 비할인 구매율 , 2 : 전체 최근 7일 비할인 구매율, 3 : 전체 어제 비할인 구매율
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(403, 1, '$total_normalpurchase_28days', 'Pay(total_normalpurchase_28days)', now()) ON DUPLICATE KEY UPDATE value='$total_normalpurchase_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(403, 2, '$total_normalpurchase_7days', 'Pay(total_normalpurchase_7days)', now()) ON DUPLICATE KEY UPDATE value='$total_normalpurchase_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(403, 3, '$total_normalpurchase_yesterday', 'Pay(total_normalpurchase_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$total_normalpurchase_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// Web - End
		// IOS - Start
		$today = date("Y-m-d");
		$yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));		
			
		// 최근 28일 결제 통계
		$sql = "SELECT SUM(money) FROM  ".
		  		" ( ".
                	" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		    " UNION ALL  ".
                	" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_ironsource WHERE os_type = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
                " ) t1";
		$ios_order_28day = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile_cancel WHERE useridx > $str_useridx AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < canceldate AND canceldate < NOW()";
		$ios_order_cancel_28day = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(cnt) AS cnt FROM ". 
		  		" ( ".
                	" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		    " UNION ALL ".
                	" SELECT usercnt FROM tbl_product_order_ironsource WHERE os_type = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
                " ) t1";
		$ios_count_28day = $db_main->getvalue($sql);
		
		// 최근 7일 결제 통계
		$sql = "SELECT SUM(money) FROM  ".
		  		" ( ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" UNION ALL  ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_ironsource WHERE os_type = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1";
		$ios_order_7day = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile_cancel WHERE useridx > $str_useridx AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < canceldate AND canceldate < NOW()";
		$ios_order_cancel_7day = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(cnt) AS cnt FROM ".
		  		" ( ".
		  		" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" UNION ALL ".
		  		" SELECT usercnt FROM tbl_product_order_ironsource WHERE os_type = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1";
		$ios_count_7day = $db_main->getvalue($sql);
		
		// 어제 결제 통계
		$sql = "SELECT SUM(money) FROM  ".
		  		" ( ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" UNION ALL  ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_ironsource WHERE os_type = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" ) t1";
		$ios_order_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile_cancel WHERE useridx > $str_useridx AND os_type = 1 AND canceldate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$ios_order_cancel_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(cnt) AS cnt FROM ".
		  		" ( ".
		  		" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" UNION ALL ".
		  		" SELECT usercnt FROM tbl_product_order_ironsource WHERE os_type = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" ) t1";
		$ios_count_yesterday = $db_main->getvalue($sql);
		
		//type 101 : IOS
		//subtype
		//1 : 최근 28일 결제 금액, 2 : 최근 28일 결제 취소 금액, 3 : 최근 28일 결제 수
		//4 : 최근 7일 결제 금액, 5 : 최근 7일 결제 취소 금액, 6 : 최근 7일 결제 수
		//7 : 어제 결제 금액, 8 : 어제 결제 취소 금액, 9 : 어제 결제 수
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 1, '$ios_order_28day', 'IOS - Pay(order_28day)', now()) ON DUPLICATE KEY UPDATE value='$ios_order_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 2, '$ios_order_cancel_28day', 'IOS - Pay(order_cancel_28day)', now()) ON DUPLICATE KEY UPDATE value='$ios_order_cancel_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 3, '$ios_count_28day', 'IOS - Pay(count_28day)', now()) ON DUPLICATE KEY UPDATE value='$ios_count_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 4, '$ios_order_7day', 'IOS - Pay(order_7day)', now()) ON DUPLICATE KEY UPDATE value='$ios_order_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 5, '$ios_order_cancel_7day', 'IOS - Pay(order_cancel_7day)', now()) ON DUPLICATE KEY UPDATE value='$ios_order_cancel_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 6, '$ios_count_7day', 'IOS - Pay(count_7day)', now()) ON DUPLICATE KEY UPDATE value='$ios_count_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 7, '$ios_order_yesterday', 'IOS - Pay(order_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$ios_order_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 8, '$ios_order_cancel_yesterday', 'IOS - Pay(order_cancel_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$ios_order_cancel_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 9, '$ios_count_yesterday', 'IOS - Pay(count_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$ios_count_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// IOS 기간별 할인율 계산
		$sql = 	"SELECT ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".			
				"FROM tbl_product_order_mobile ".
				"WHERE status = 1 AND useridx > $str_useridx AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate <  NOW()";
		$ios_salerate_28days = $db_main->getvalue($sql);
			
		$sql = 	"SELECT ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				"FROM tbl_product_order_mobile ".
				"WHERE status = 1 AND useridx > $str_useridx AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate <  NOW()";
		$ios_salerate_7days = $db_main->getvalue($sql);
			
		$sql = 	"SELECT ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				"FROM tbl_product_order_mobile ".
				"WHERE status = 1 AND useridx > $str_useridx AND os_type = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$ios_salerate_yesterday = $db_main->getvalue($sql);
			
		//type 102 : IOS
		//subtype
		//1 : IOS 최근 28일 기간별 할인율 , 2 : IOS 최근 7일 기간별 할인율, 3 : IOS 어제 기간별 할인율		
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(102, 1, '$ios_salerate_28days', 'IOS - Pay(salerate_28days)', now()) ON DUPLICATE KEY UPDATE value='$ios_salerate_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(102, 2, '$ios_salerate_7days', 'IOS - Pay(salerate_7days)', now()) ON DUPLICATE KEY UPDATE value='$ios_salerate_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(102, 3, '$ios_salerate_yesterday', 'IOS - Pay(salerate_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$ios_salerate_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// IOS 기간별 비할인 구매율 계산
		$sql = "SELECT SUM(money) FROM ".
		  		" ( ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_mobile t1 ".
		  		" WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND couponidx = 0 AND special_discount = 0 AND special_more = 0 AND coin=basecoin ".
		  		" UNION ALL ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_ironsource t1 ".
		  		" WHERE os_type = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" ) t1;";
		$ios_normalpurchase_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(money) FROM ".
		  		" ( ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_mobile t1 ".
		  		" WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 AND coin=basecoin ".
		  		" UNION ALL ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_ironsource t1 ".
		  		" WHERE os_type = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1;";
		$ios_normalpurchase_7days = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(money) FROM ". 
		  		" ( ".
                	" SELECT SUM(money) AS money FROM tbl_product_order_mobile t1 ". 
		  		    " WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 AND coin=basecoin ".
                	" UNION ALL ".
		  		    " SELECT SUM(money) AS money FROM tbl_product_order_ironsource t1 ".
                	" WHERE os_type = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ". 
            	" ) t1;";
		$ios_normalpurchase_28days = $db_main->getvalue($sql);
			
		//type 103 : IOS
		//subtype
		//1 : IOS 최근 28일 비할인 구매율 , 2 : IOS 최근 7일 비할인 구매율, 3 : IOS 어제 비할인 구매율
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(103, 1, '$ios_normalpurchase_28days', 'IOS - Pay(normalpurchase_28days)', now()) ON DUPLICATE KEY UPDATE value='$ios_normalpurchase_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(103, 2, '$ios_normalpurchase_7days', 'IOS - Pay(normalpurchase_7days)', now()) ON DUPLICATE KEY UPDATE value='$ios_normalpurchase_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(103, 3, '$ios_normalpurchase_yesterday', 'IOS - Pay(normalpurchase_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$ios_normalpurchase_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// IOS - End
		// Android - Start
		$today = date("Y-m-d");
		$yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));
			
		// 최근 28일 결제 통계
		$sql = "SELECT SUM(money) FROM  ".
		  		" ( ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" UNION ALL  ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_ironsource WHERE os_type = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1";
		$android_order_28day = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile_cancel WHERE useridx > $str_useridx AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < canceldate AND canceldate < NOW()";
		$android_order_cancel_28day = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(cnt) AS cnt FROM ".
		  		" ( ".
		  		" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" UNION ALL ".
		  		" SELECT usercnt FROM tbl_product_order_ironsource WHERE os_type = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1";
		$android_count_28day = $db_main->getvalue($sql);
		
		// 최근 7일 결제 통계
		$sql = "SELECT SUM(money) FROM  ".
		  		" ( ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" UNION ALL  ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_ironsource WHERE os_type = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1";
		$android_order_7day = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile_cancel WHERE useridx > $str_useridx AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < canceldate AND canceldate < NOW()";
		$android_order_cancel_7day = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(cnt) AS cnt FROM ".
		  		" ( ".
		  		" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" UNION ALL ".
		  		" SELECT usercnt FROM tbl_product_order_ironsource WHERE os_type = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1";
		$android_count_7day = $db_main->getvalue($sql);
		
		// 어제 결제 통계
		$sql = "SELECT SUM(money) FROM  ".
		  		" ( ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 2 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" UNION ALL  ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_ironsource WHERE os_type = 2 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" ) t1";
		$android_order_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile_cancel WHERE useridx > $str_useridx AND os_type = 2 AND canceldate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$android_order_cancel_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(cnt) AS cnt FROM ".
		  		" ( ".
		  		" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 2 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" UNION ALL ".
		  		" SELECT usercnt FROM tbl_product_order_ironsource WHERE os_type = 2 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" ) t1";
		$android_count_yesterday = $db_main->getvalue($sql);
			
		//type 201 : Android
		//subtype
		//1 : 최근 28일 결제 금액, 2 : 최근 28일 결제 취소 금액, 3 : 최근 28일 결제 수
		//4 : 최근 7일 결제 금액, 5 : 최근 7일 결제 취소 금액, 6 : 최근 7일 결제 수
		//7 : 어제 결제 금액, 8 : 어제 결제 취소 금액, 9 : 어제 결제 수
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 1, '$android_order_28day', 'Android - Pay(order_28day)', now()) ON DUPLICATE KEY UPDATE value='$android_order_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 2, '$android_order_cancel_28day', 'Android - Pay(order_cancel_28day)', now()) ON DUPLICATE KEY UPDATE value='$android_order_cancel_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 3, '$android_count_28day', 'Android - Pay(count_28day)', now()) ON DUPLICATE KEY UPDATE value='$android_count_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 4, '$android_order_7day', 'Android - Pay(order_7day)', now()) ON DUPLICATE KEY UPDATE value='$android_order_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 5, '$android_order_cancel_7day', 'Android - Pay(order_cancel_7day)', now()) ON DUPLICATE KEY UPDATE value='$android_order_cancel_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 6, '$android_count_7day', 'Android - Pay(count_7day)', now()) ON DUPLICATE KEY UPDATE value='$android_count_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 7, '$android_order_yesterday', 'Android - Pay(order_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$android_order_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 8, '$android_order_cancel_yesterday', 'Android - Pay(order_cancel_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$android_order_cancel_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 9, '$android_count_yesterday', 'Android - Pay(count_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$android_count_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// Android 기간별 할인율 계산
		$sql = 	"SELECT ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				"FROM tbl_product_order_mobile ".
				"WHERE status = 1 AND useridx > $str_useridx AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate <  NOW()";
		$android_salerate_28days = $db_main->getvalue($sql);
			
		$sql = 	"SELECT ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				"FROM tbl_product_order_mobile ".
				"WHERE status = 1 AND useridx > $str_useridx AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate <  NOW()";
		$android_salerate_7days = $db_main->getvalue($sql);
			
		$sql = 	"SELECT ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				"FROM tbl_product_order_mobile ".
				"WHERE status = 1 AND useridx > $str_useridx AND os_type = 2 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$android_salerate_yesterday = $db_main->getvalue($sql);	
			
		//type 202 : Android
		//subtype
		//1 : Android 최근 28일 기간별 할인율 , 2 : Android 최근 7일 기간별 할인율, 3 : Android 어제 기간별 할인율
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(202, 1, '$android_salerate_28days', 'Android - Pay(salerate_28days)', now()) ON DUPLICATE KEY UPDATE value='$android_salerate_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(202, 2, '$android_salerate_7days', 'Android - Pay(salerate_7days)', now()) ON DUPLICATE KEY UPDATE value='$android_salerate_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(202, 3, '$android_salerate_yesterday', 'Android - Pay(salerate_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$android_salerate_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// Android 기간별 비할인 구매율 계산		
		$sql = "SELECT SUM(money) FROM ".
		  		" ( ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_mobile t1 ".
		  		" WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 2 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND couponidx = 0 AND special_discount = 0 AND special_more = 0 AND coin=basecoin ".
		  		" UNION ALL ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_ironsource t1 ".
		  		" WHERE os_type = 2 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" ) t1;";
		$android_normalpurchase_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(money) FROM ".
		  		" ( ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_mobile t1 ".
		  		" WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 AND coin=basecoin ".
		  		" UNION ALL ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_ironsource t1 ".
		  		" WHERE os_type = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1;";
		$android_normalpurchase_7days = $db_main->getvalue($sql);
		
		
		$sql = "SELECT SUM(money) FROM ".
		  		" ( ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_mobile t1 ".
		  		" WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 AND coin=basecoin ".
		  		" UNION ALL ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_ironsource t1 ".
		  		" WHERE os_type = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1;";
		$android_normalpurchase_28days = $db_main->getvalue($sql);
		
		//type 203 : Android
		//subtype
		//1 : Android 최근 28일 비할인 구매율 , 2 : Android 최근 7일 비할인 구매율, 3 : Android 어제 비할인 구매율
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(203, 1, '$android_normalpurchase_28days', 'Android - Pay(normalpurchase_28days)', now()) ON DUPLICATE KEY UPDATE value='$android_normalpurchase_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(203, 2, '$android_normalpurchase_7days', 'Android - Pay(normalpurchase_7days)', now()) ON DUPLICATE KEY UPDATE value='$android_normalpurchase_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(203, 3, '$android_normalpurchase_yesterday', 'Android - Pay(normalpurchase_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$android_normalpurchase_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// Android - End
		// Amazon - Start
		$today = date("Y-m-d");
		$yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));
			
		// 최근 28일 결제 통계
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND status=1 AND os_type = 3 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW();";
		$amazon_order_28day = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile_cancel WHERE useridx > $str_useridx AND os_type = 3 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < canceldate AND canceldate < NOW()";
		$amazon_order_cancel_28day = $db_main->getvalue($sql);
		
		$sql = "SELECT COUNT(*) FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 3 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW()";
		$amazon_count_28day = $db_main->getvalue($sql);
		
		// 최근 7일 결제 통계
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND status=1 AND os_type = 3 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW();";
		$amazon_order_7day = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile_cancel WHERE useridx > $str_useridx AND os_type = 3 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < canceldate AND canceldate < NOW()";
		$amazon_order_cancel_7day = $db_main->getvalue($sql);
		
		$sql = "SELECT COUNT(*) FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 3 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW()";
		$amazon_count_7day = $db_main->getvalue($sql);
		
		// 어제 결제 통계
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND status=1 AND os_type = 3 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59';";
		$amazon_order_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile_cancel WHERE useridx > $str_useridx AND os_type = 3 AND canceldate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$amazon_order_cancel_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT COUNT(*) FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 3 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$amazon_count_yesterday = $db_main->getvalue($sql);
			
		//type 301 : Amazon
		//subtype
		//1 : 최근 28일 결제 금액, 2 : 최근 28일 결제 취소 금액, 3 : 최근 28일 결제 수
		//4 : 최근 7일 결제 금액, 5 : 최근 7일 결제 취소 금액, 6 : 최근 7일 결제 수
		//7 : 어제 결제 금액, 8 : 어제 결제 취소 금액, 9 : 어제 결제 수
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(301, 1, '$amazon_order_28day', 'Amazon - Pay(order_28day)', now()) ON DUPLICATE KEY UPDATE value='$amazon_order_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(301, 2, '$amazon_order_cancel_28day', 'Amazon - Pay(order_cancel_28day)', now()) ON DUPLICATE KEY UPDATE value='$amazon_order_cancel_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(301, 3, '$amazon_count_28day', 'Amazon - Pay(count_28day)', now()) ON DUPLICATE KEY UPDATE value='$amazon_count_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(301, 4, '$amazon_order_7day', 'Amazon - Pay(order_7day)', now()) ON DUPLICATE KEY UPDATE value='$amazon_order_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(301, 5, '$amazon_order_cancel_7day', 'Amazon - Pay(order_cancel_7day)', now()) ON DUPLICATE KEY UPDATE value='$amazon_order_cancel_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(301, 6, '$amazon_count_7day', 'Amazon - Pay(count_7day)', now()) ON DUPLICATE KEY UPDATE value='$amazon_count_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(301, 7, '$amazon_order_yesterday', 'Amazon - Pay(order_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$amazon_order_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(301, 8, '$amazon_order_cancel_yesterday', 'Amazon - Pay(order_cancel_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$amazon_order_cancel_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(301, 9, '$amazon_count_yesterday', 'Amazon - Pay(count_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$amazon_count_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// Amazon 기간별 할인율 계산		
		$sql = 	"SELECT ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				"FROM tbl_product_order_mobile ".
				"WHERE status = 1 AND useridx > $str_useridx AND os_type = 3 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate <  NOW()";
		$amazon_salerate_28days = $db_main->getvalue($sql);
			
		$sql = 	"SELECT ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				"FROM tbl_product_order_mobile ".
				"WHERE status = 1 AND useridx > $str_useridx AND os_type = 3 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate <  NOW()";
		$amazon_salerate_7days = $db_main->getvalue($sql);
			
		$sql = 	"SELECT ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				"FROM tbl_product_order_mobile ".
				"WHERE status = 1 AND useridx > $str_useridx AND os_type = 3 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$amazon_salerate_yesterday = $db_main->getvalue($sql);
			
		//type 302 : Amazon
		//subtype
		//1 : Amazon 최근 28일 기간별 할인율 , 2 : Amazon 최근 7일 기간별 할인율, 3 : Amazon 어제 기간별 할인율
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(302, 1, '$amazon_salerate_28days', 'Amazon - Pay(salerate_28days)', now()) ON DUPLICATE KEY UPDATE value='$amazon_salerate_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(302, 2, '$amazon_salerate_7days', 'Amazon - Pay(salerate_7days)', now()) ON DUPLICATE KEY UPDATE value='$amazon_salerate_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(302, 3, '$amazon_salerate_yesterday', 'Amazon - Pay(salerate_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$amazon_salerate_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// Amazon 기간별 비할인 구매율 계산		
		$sql = "SELECT SUM(money) FROM tbl_product_order_mobile t1 ".
				"WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 3 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND couponidx = 0 AND special_discount = 0 AND special_more = 0;";
		$amazon_normalpurchase_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(money) FROM tbl_product_order_mobile t1 ".
				"WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 3 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0;";
		$amazon_normalpurchase_7days = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(money) FROM tbl_product_order_mobile t1 ".
				"WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 3 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0;";
		$amazon_normalpurchase_28days = $db_main->getvalue($sql);
			
		//type 303 : Amazon
		//subtype
		//1 : Amazon 최근 28일 비할인 구매율 , 2 : Amazon 최근 7일 비할인 구매율, 3 : Amazon 어제 비할인 구매율
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(303, 1, '$amazon_normalpurchase_28days', 'Amazon - Pay(normalpurchase_28days)', now()) ON DUPLICATE KEY UPDATE value='$amazon_normalpurchase_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(303, 2, '$amazon_normalpurchase_7days', 'Amazon - Pay(normalpurchase_7days)', now()) ON DUPLICATE KEY UPDATE value='$amazon_normalpurchase_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(303, 3, '$amazon_normalpurchase_yesterday', 'Amazon - Pay(normalpurchase_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$amazon_normalpurchase_yesterday', writedate=now();";
		$db_analysis->execute($sql);		
		// Amazon - End
		
		// Web2.0 - Start
		$today = date("Y-m-d");
		$yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));
			
		// 최근 28일 결제 통계
		$sql = "SELECT ROUND(IFNULL(SUM(facebookcredit),0)/10, 2) AS total_money FROM tbl_product_order WHERE useridx > $str_useridx AND status = 1 AND is_v2 = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW();";
		$web_v2_order_28day = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(facebookcredit),0)/10, 2) AS total_money FROM tbl_product_order WHERE useridx > $str_useridx AND status = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < canceldate AND canceldate <= NOW()";
		$web_v2_order_cancel_28day = $db_main->getvalue($sql);
		
		$sql = "SELECT COUNT(*) FROM tbl_product_order WHERE useridx > $str_useridx AND status = 1 AND is_v2 = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW()";
		$web_v2_count_28day = $db_main->getvalue($sql);
		
		// 최근 7일 결제 통계
		$sql = "SELECT ROUND(IFNULL(SUM(facebookcredit),0)/10, 2) AS total_money FROM tbl_product_order WHERE useridx > $str_useridx AND status = 1 AND is_v2 = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW();";
		$web_v2_order_7day = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(facebookcredit),0)/10, 2) AS total_money FROM tbl_product_order WHERE useridx > $str_useridx AND status = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < canceldate AND canceldate < NOW()";
		$web_v2_order_cancel_7day = $db_main->getvalue($sql);
		
		$sql = "SELECT COUNT(*) FROM tbl_product_order WHERE useridx > $str_useridx AND status = 1 AND is_v2 = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW()";
		$web_v2_count_7day = $db_main->getvalue($sql);
		
		// 어제 결제 통계
		$sql = "SELECT ROUND(IFNULL(SUM(facebookcredit),0)/10, 2) AS total_money FROM tbl_product_order WHERE useridx > $str_useridx AND status = 1 AND is_v2 = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59';";
		$web_v2_order_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(facebookcredit),0)/10, 2) AS total_money FROM tbl_product_order WHERE useridx > $str_useridx AND status = 2 AND canceldate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$web_v2_order_cancel_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT COUNT(*) FROM tbl_product_order WHERE useridx > $str_useridx AND status = 1 AND is_v2 = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$web_v2_count_yesterday = $db_main->getvalue($sql);
		
		//type 501 : Web_2.0
		//subtype
		//1 : 최근 28일 결제 금액, 2 : 최근 28일 결제 취소 금액, 3 : 최근 28일 결제 수
		//4 : 최근 7일 결제 금액, 5 : 최근 7일 결제 취소 금액, 6 : 최근 7일 결제 수
		//7 : 어제 결제 금액, 8 : 어제 결제 취소 금액, 9 : 어제 결제 수
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(501, 1, '$web_v2_order_28day', 'Web_v2 - Pay(order_28day)', now()) ON DUPLICATE KEY UPDATE value='$web_v2_order_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(501, 2, '$web_v2_order_cancel_28day', 'Web_v2 - Pay(order_cancel_28day)', now()) ON DUPLICATE KEY UPDATE value='$web_v2_order_cancel_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(501, 3, '$web_v2_count_28day', 'Web_v2 - Pay(count_28day)', now()) ON DUPLICATE KEY UPDATE value='$web_v2_count_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(501, 4, '$web_v2_order_7day', 'Web_v2 - Pay(order_7day)', now()) ON DUPLICATE KEY UPDATE value='$web_v2_order_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(501, 5, '$web_v2_order_cancel_7day', 'Web_v2 - Pay(order_cancel_7day)', now()) ON DUPLICATE KEY UPDATE value='$web_v2_order_cancel_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(501, 6, '$web_v2_count_7day', 'Web_v2 - Pay(count_7day)', now()) ON DUPLICATE KEY UPDATE value='$web_v2_count_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(501, 7, '$web_v2_order_yesterday', 'Web_v2 - Pay(order_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$web_v2_order_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(501, 8, '$web_v2_order_cancel_yesterday', 'web_v2 - Pay(order_cancel_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$web_v2_order_cancel_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(501, 9, '$web_v2_count_yesterday', 'Web_v2 - Pay(count_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$web_v2_count_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// Web_2.0 기간별 할인율 계산
		$sql = "SELECT  ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				"FROM tbl_product_order ".
				"WHERE STATUS = 1 AND is_v2 = 1 AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW()";
		$web_v2_salerate_28days = $db_main->getvalue($sql);
			
		$sql = "SELECT  ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				"FROM tbl_product_order ".
				"WHERE STATUS = 1 AND is_v2 = 1 AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW()";
		$web_v2_salerate_7days = $db_main->getvalue($sql);
			
		$sql = "SELECT  ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
				"FROM tbl_product_order ".
				"WHERE STATUS = 1 AND is_v2 = 1 AND useridx > $str_useridx AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$web_v2_salerate_yesterday = $db_main->getvalue($sql);
			
		//type 502 : Web_2.0
		//subtype
		//1 : Web_2.0 최근 28일 기간별 할인율 , 2 : Web_2.0 최근 7일 기간별 할인율, 3 : Web_2.0 어제 기간별 할인율
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(502, 1, '$web_v2_salerate_28days', 'Web_v2 - Pay(salerate_28days)', now()) ON DUPLICATE KEY UPDATE value='$web_v2_salerate_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(502, 2, '$web_v2_salerate_7days', 'Web_v2 - Pay(salerate_7days)', now()) ON DUPLICATE KEY UPDATE value='$web_v2_salerate_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(502, 3, '$web_v2_salerate_yesterday', 'Web_v2 - Pay(salerate_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$web_v2_salerate_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// Web_2.0 기간별 비할인 구매율 계산
		$sql = 	"SELECT SUM(facebookcredit)/10 FROM tbl_product_order ".
				"WHERE STATUS IN (1, 3) AND is_v2 = 1 AND useridx > $str_useridx AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND couponidx = 0 AND special_discount = 0 AND special_more = 0";				
		$web_v2_normalpurchase_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(facebookcredit)/10 FROM tbl_product_order ".
			   "WHERE STATUS IN (1, 3) AND is_v2 = 1 AND useridx > $str_useridx AND date_sub(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0";		
		$web_v2_normalpurchase_7days = $db_main->getvalue($sql);
		
		$sql = 	"	SELECT SUM(facebookcredit)/10 FROM tbl_product_order ".
				"	WHERE STATUS IN (1, 3) AND is_v2 = 1 AND useridx > $str_useridx AND date_sub(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0";	
		$web_v2_normalpurchase_28days = $db_main->getvalue($sql);
			
		//type 503 : Web_2.0
		//subtype
		//1 : Web_2.0 최근 28일 비할인 구매율 , 2 : Web_2.0 최근 7일 비할인 구매율, 3 : Web_2.0 어제 비할인 구매율
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(503, 1, '$web_v2_normalpurchase_28days', 'Web_v2 - Pay(normalpurchase_28days)', now()) ON DUPLICATE KEY UPDATE value='$web_v2_normalpurchase_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(503, 2, '$web_v2_normalpurchase_7days', 'Web_v2 - Pay(normalpurchase_7days)', now()) ON DUPLICATE KEY UPDATE value='$web_v2_normalpurchase_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(503, 3, '$web_v2_normalpurchase_yesterday', 'Web_v2 - Pay(normalpurchase_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$web_v2_normalpurchase_yesterday', writedate=now();";
		$db_analysis->execute($sql);		
		// Web2.0 - End
		
		// 회원가입
		//최근 28일 가입 인원수
		$sql = "SELECT COUNT(*) AS join_28day FROM tbl_user WHERE useridx > $str_useridx AND createdate >= DATE_SUB(NOW(), INTERVAL 28 DAY)";
		$join_28days = $db_main->getvalue($sql);
		
		$sql = "SELECT COUNT(*) AS join_28day FROM tbl_user_ext WHERE useridx > $str_useridx AND createdate >= DATE_SUB(NOW(), INTERVAL 28 DAY) AND platform = 0";
		$web_join_28days = $db_main->getvalue($sql);
		
		$sql = "SELECT COUNT(*) AS join_28day FROM tbl_user_ext WHERE useridx > 20000 AND createdate >= DATE_SUB(NOW(), INTERVAL 28 DAY) AND platform IN (1,2,3)";
		$mobile_join_28days = $db_main->getvalue($sql);
		
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS join_nogame_28day ".
				"FROM tbl_user ".
				"WHERE useridx > $str_useridx AND createdate >= DATE_SUB(NOW(), INTERVAL 28 DAY)";
		$join_nogame_28days = $db_main->getvalue($sql);
		
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS join_nogame_28day ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND createdate >= DATE_SUB(NOW(), INTERVAL 28 DAY) AND platform = 0";
		$web_join_nogame_28days = $db_main->getvalue($sql);
		
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS join_nogame_28day ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND createdate >= DATE_SUB(NOW(), INTERVAL 28 DAY) AND platform IN (1,2,3)";
		$mobile_nogame_28days = $db_main->getvalue($sql);
		
		//최근 7일
		$sql = "SELECT COUNT(*) AS join_7day FROM tbl_user WHERE useridx > $str_useridx AND createdate >= DATE_SUB(NOW(), INTERVAL 7 DAY)";
		$join_7days = $db_main->getvalue($sql);
		
		$sql = "SELECT COUNT(*) AS join_7day FROM tbl_user_ext WHERE useridx > $str_useridx AND createdate >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND platform = 0";
		$web_join_7days = $db_main->getvalue($sql);
		
		$sql = "SELECT COUNT(*) AS join_7day FROM tbl_user_ext WHERE useridx > 20000 AND createdate >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND platform IN (1,2,3)";
		$mobile_join_7days = $db_main->getvalue($sql);
		
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS join_nogame_7day ".
				"FROM tbl_user ".
				"WHERE useridx > $str_useridx AND createdate >= DATE_SUB(NOW(), INTERVAL 7 DAY)";
		$join_nogame_7days = $db_main->getvalue($sql);
		
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS join_nogame_7day ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND createdate >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND platform = 0";
		$web_join_nogame_7days = $db_main->getvalue($sql);
		
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS join_nogame_7day ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND createdate >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND platform IN (1,2,3)";
		$mobile_nogame_7days = $db_main->getvalue($sql);
		
		//어제
		$sql = "SELECT COUNT(*) AS join_yesterday FROM tbl_user WHERE useridx > $str_useridx AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$join_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT COUNT(*) AS join_7day FROM tbl_user_ext WHERE useridx > $str_useridx AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND platform = 0";
		$web_join_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT COUNT(*) AS join_7day FROM tbl_user_ext WHERE useridx > 20000 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND platform IN (1,2,3)";
		$mobile_join_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS join_nogame_yesterday ".
				"FROM tbl_user ".
				"WHERE useridx > $str_useridx AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$join_nogame_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS join_nogame_yesterday ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$web_join_nogame_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS join_nogame_yesterday ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND platform IN (1,2,3)";
		$mobile_nogame_yesterday = $db_main->getvalue($sql);
		
		//Web 미게임
		//최근 28일
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS web_join_nogame_28day ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND platform NOT IN (1, 2, 3) AND createdate >= DATE_SUB(NOW(), INTERVAL 28 DAY)";
		$web_join_nogame_28days = $db_main->getvalue($sql);
		
		//최근 7일
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS web_join_nogame_7day ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND platform NOT IN (1, 2, 3) AND createdate >= DATE_SUB(NOW(), INTERVAL 7 DAY)";
		$web_join_nogame_7days = $db_main->getvalue($sql);
		
		//어제
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS web_join_nogame_yesterday ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND platform NOT IN (1, 2, 3) AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$web_join_nogame_yesterday = $db_main->getvalue($sql);
		
		//Web 미설치, 튜토리얼
		//최근 28일
		$sql = "SELECT COUNT(*) AS total_count, SUM(IF(IFNULL(install,0)=0, 1, 0)) AS install_count, SUM(IF(IFNULL(tutorial,0) < 4, 1, 0)) AS tutorial_count ".
				"FROM ( ".
				"SELECT useridx FROM tbl_user_ext WHERE useridx > $str_useridx AND platform NOT IN (1, 2, 3) AND createdate >= DATE_SUB(NOW(), INTERVAL 28 DAY) ".
				") t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx ";
		$web_28days_data = $db_main->getarray($sql);
		
		$web_join_28days = $web_28days_data["total_count"];
		$web_noinstall_28days = $web_28days_data["install_count"];
		$web_notutorial_28days = $web_28days_data["tutorial_count"];
		
		//최근 7일
		$sql = "SELECT COUNT(*) AS total_count, SUM(IF(IFNULL(install,0)=0, 1, 0)) AS install_count, SUM(IF(IFNULL(tutorial,0) < 4, 1, 0)) AS tutorial_count ".
				"FROM ( ".
				"SELECT useridx FROM tbl_user_ext WHERE useridx > $str_useridx AND platform NOT IN (1, 2, 3) AND createdate >= DATE_SUB(NOW(), INTERVAL 7 DAY) ".
				") t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx ";
		$web_7days_data = $db_main->getarray($sql);
		
		$web_join_7days = $web_7days_data["total_count"];
		$web_noinstall_7days = $web_7days_data["install_count"];
		$web_notutorial_7days = $web_7days_data["tutorial_count"];
		
		//어제
		$sql = "SELECT COUNT(*) AS total_count, SUM(IF(IFNULL(install,0)=0, 1, 0)) AS install_count, SUM(IF(IFNULL(tutorial,0) < 4, 1, 0)) AS tutorial_count ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_user_ext WHERE useridx > $str_useridx AND platform NOT IN (1, 2, 3) AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
				") t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx";
		$web_yesterday_data = $db_main->getarray($sql);
		
		$web_join_yesterday = $web_yesterday_data["total_count"];
		$web_noinstall_yesterday = $web_yesterday_data["install_count"];
		$web_notutorial_yesterday = $web_yesterday_data["tutorial_count"];
		
		//mobile 미설치, 튜토리얼
		//최근 28일
		$sql = "SELECT COUNT(*) AS total_count, SUM(IF(IFNULL(install,0)=0, 1, 0)) AS install_count, SUM(IF(IFNULL(tutorial,0) < 4, 1, 0)) AS tutorial_count ".
				"FROM ( ".
				"SELECT useridx FROM tbl_user_ext WHERE useridx > $str_useridx AND platform  IN (1, 2, 3) AND createdate >= DATE_SUB(NOW(), INTERVAL 28 DAY) ".
				") t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx ";
		$mobile_28days_data = $db_main->getarray($sql);
		
		$mobile_join_28days = $mobile_28days_data["total_count"];
		$mobile_noinstall_28days = $mobile_28days_data["install_count"];
		$mobile_notutorial_28days = $mobile_28days_data["tutorial_count"];
		
		//최근 7일
		$sql = "SELECT COUNT(*) AS total_count, SUM(IF(IFNULL(install,0)=0, 1, 0)) AS install_count, SUM(IF(IFNULL(tutorial,0) < 4, 1, 0)) AS tutorial_count ".
				"FROM ( ".
				"SELECT useridx FROM tbl_user_ext WHERE useridx > $str_useridx AND platform  IN (1, 2, 3) AND createdate >= DATE_SUB(NOW(), INTERVAL 7 DAY) ".
				") t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx ";
		$mobile_7days_data = $db_main->getarray($sql);
		
		$mobile_join_7days = $mobile_7days_data["total_count"];
		$mobile_noinstall_7days = $mobile_7days_data["install_count"];
		$mobile_notutorial_7days = $mobile_7days_data["tutorial_count"];
		
		//어제
		$sql = "SELECT COUNT(*) AS total_count, SUM(IF(IFNULL(install,0)=0, 1, 0)) AS install_count, SUM(IF(IFNULL(tutorial,0) < 4, 1, 0)) AS tutorial_count ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_user_ext WHERE useridx > $str_useridx AND platform  IN (1, 2, 3) AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
				") t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx";
		$mobile_yesterday_data = $db_main->getarray($sql);
		
		$mobile_join_yesterday = $mobile_yesterday_data["total_count"];
		$mobile_noinstall_yesterday = $mobile_yesterday_data["install_count"];
		$mobile_notutorial_yesterday = $mobile_yesterday_data["tutorial_count"];
		
		
		//type 4 : Total
		//subtype
		//1 : 최근 28일 가입자 수, 2 : 최근 7일 가입자 수, 3 : 어제 가입자 수
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(4, 1, '$join_28days', 'Join(28days)', now()) ON DUPLICATE KEY UPDATE value='$join_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(4, 2, '$join_7days', 'Join(7days)', now()) ON DUPLICATE KEY UPDATE value='$join_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(4, 3, '$join_yesterday', 'Join(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$join_yesterday', writedate=now();";
		
		//type 5 : Total
		//subtype
		//1 : 최근 28일 미게임건수 , 2 : 최근 7일 미게임건수 , 3 : 어제 미게임건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(5, 1, '$join_nogame_28days', 'NoGame(28days)', now()) ON DUPLICATE KEY UPDATE value='$join_nogame_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(5, 2, '$join_nogame_7days', 'NoGame(7days)', now()) ON DUPLICATE KEY UPDATE value='$join_nogame_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(5, 3, '$join_nogame_yesterday', 'NoGame(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$join_nogame_yesterday', writedate=now();";
		
		//type 6 : Web
		//subtype
		//1 : 최근 28일 미설치건수 , 2 : 최근 7일 미설치건수 , 3 : 어제 미설치건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(6, 1, '$web_noinstall_28days', 'NoInstall(28days)', now()) ON DUPLICATE KEY UPDATE value='$web_noinstall_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(6, 2, '$web_noinstall_7days', 'NoInstall(7days)', now()) ON DUPLICATE KEY UPDATE value='$web_noinstall_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(6, 3, '$web_noinstall_yesterday', 'NoInstall(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$web_noinstall_yesterday', writedate=now();";
		
		//type 7 : Web
		//subtype
		//1 : 최근 28일  가입자 수 , 2 : 최근 7일 가입자 수 , 3 : 어제 가입자 수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(7, 1, '$web_join_28days', 'Web Join(28days)', now()) ON DUPLICATE KEY UPDATE value='$web_join_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(7, 2, '$web_join_7days', 'Web Join(7days)', now()) ON DUPLICATE KEY UPDATE value='$web_join_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(7, 3, '$web_join_yesterday', 'Web Join(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$web_join_yesterday', writedate=now();";
		
		//type 8 : Web
		//subtype
		//1 : 최근 28일 미튜토리얼건수 , 2 : 최근 7일 미튜토리얼건수 , 3 : 어제 미튜토리얼건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(8, 1, '$web_notutorial_28days', 'Web NoTutorial(28days)', now()) ON DUPLICATE KEY UPDATE value='$web_notutorial_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(8, 2, '$web_notutorial_7days', 'Web NoTutorial(7days)', now()) ON DUPLICATE KEY UPDATE value='$web_notutorial_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(8, 3, '$web_notutorial_yesterday', 'Web NoTutorial(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$web_notutorial_yesterday', writedate=now();";
		
		//type 9 : Web
		//subtype
		//1 : 최근 28일 미게임건수 , 2 : 최근 7일 미게임건수 , 3 : 어제 미게임건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(9, 1, '$web_join_nogame_28days', 'Web NoGame(28days)', now()) ON DUPLICATE KEY UPDATE value='$web_join_nogame_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(9, 2, '$web_join_nogame_7days', 'Web NoGame(7days)', now()) ON DUPLICATE KEY UPDATE value='$web_join_nogame_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(9, 3, '$web_join_nogame_yesterday', 'Web NoGame(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$web_join_nogame_yesterday', writedate=now();";
		
		//type 10 : Web
		//subtype
		//1 : 최근 28일 가입건수 , 2 : 최근 7일 가입건수 , 3 : 어제 가입건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(10, 1, '$web_join_28days', 'Web Join(28days)', now()) ON DUPLICATE KEY UPDATE value='$web_join_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(10, 2, '$web_join_7days', 'Web Join(7days)', now()) ON DUPLICATE KEY UPDATE value='$web_join_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(10, 3, '$web_join_yesterday', 'Web Join(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$web_join_yesterday', writedate=now();";
		
		//type 11 : Web
		//subtype
		//1 : 최근 28일 미게임건수 , 2 : 최근 7일 미게임건수 , 3 : 어제 미게임건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(11, 1, '$web_join_nogame_28days', 'Web NoGame(28days)', now()) ON DUPLICATE KEY UPDATE value='$web_join_nogame_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(11, 2, '$web_join_nogame_7days', 'Web NoGame(7days)', now()) ON DUPLICATE KEY UPDATE value='$web_join_nogame_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(11, 3, '$web_join_nogame_yesterday', 'Web NoGame(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$web_join_nogame_yesterday', writedate=now();";
		
		//type 12 : Mobile
		//subtype
		//1 : 최근 28일 가입건수 , 2 : 최근 7일 가입건수 , 3 : 어제 가입건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(12, 1, '$mobile_join_28days', 'Mobile Join(28days)', now()) ON DUPLICATE KEY UPDATE value='$mobile_join_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(12, 2, '$mobile_join_7days', 'Mobile Join(7days)', now()) ON DUPLICATE KEY UPDATE value='$mobile_join_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(12, 3, '$mobile_join_yesterday', 'Mobile Join(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$mobile_join_yesterday', writedate=now();";
		
		//type 13 : Mobile
		//subtype
		//1 : 최근 28일 미게임건수 , 2 : 최근 7일 미게임건수 , 3 : 어제 미게임건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(13, 1, '$mobile_nogame_28days', 'Mobile NoGame(28days)', now()) ON DUPLICATE KEY UPDATE value='$mobile_nogame_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(13, 2, '$mobile_nogame_7days', 'Mobile NoGame(7days)', now()) ON DUPLICATE KEY UPDATE value='$mobile_nogame_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(13, 3, '$mobile_nogame_yesterday', 'Mobile NoGame(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$mobile_nogame_yesterday', writedate=now();";
		
		//type 14 : Mobile
		//subtype
		//1 : 최근 28일 미설치건수 , 2 : 최근 7일 미설치건수 , 3 : 어제 미설치건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(14, 1, '$mobile_noinstall_28days', 'Mobile NoInstall(28days)', now()) ON DUPLICATE KEY UPDATE value='$mobile_noinstall_28days', writedate=now();";		
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(14, 2, '$mobile_noinstall_7days', 'Mobile NoInstall(7days)', now()) ON DUPLICATE KEY UPDATE value='$mobile_noinstall_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(14, 3, '$mobile_noinstall_yesterday', 'Mobile NoInstall(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$mobile_noinstall_yesterday', writedate=now();";
		
		//type 15 : Mobile
		//subtype
		//1 : 최근 28일 가입건수 , 2 : 최근 7일 가입건수 , 3 : 어제 가입건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(15, 1, '$mobile_join_28days', 'Mobile Join(28days)', now()) ON DUPLICATE KEY UPDATE value='$mobile_join_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(15, 2, '$mobile_join_7days', 'Mobile Join(7days)', now()) ON DUPLICATE KEY UPDATE value='$mobile_join_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(15, 3, '$mobile_join_yesterday', 'Mobile Join(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$mobile_join_yesterday', writedate=now();";
		
		//type 16 : Mobile
		//subtype
		//1 : 최근 28일 미튜토리얼건수 , 2 : 최근 7일 미튜토리얼건수 , 3 : 어제 미튜토리얼건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(16, 1, '$mobile_notutorial_28days', 'Mobile NoTutorial(28days)', now()) ON DUPLICATE KEY UPDATE value='$mobile_notutorial_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(16, 2, '$mobile_notutorial_7days', 'Mobile NoTutorial(7days)', now()) ON DUPLICATE KEY UPDATE value='$mobile_notutorial_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(16, 3, '$mobile_notutorial_yesterday', 'Mobile NoTutorial(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$mobile_notutorial_yesterday', writedate=now();";		
		$db_analysis->execute($sql);
		
		//iOS 미게임
		//최근 28일
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS web_join_nogame_28day ".
				"FROM tbl_user_ext ".
				"WHERE platform = 1 AND createdate >= DATE_SUB(NOW(), INTERVAL 28 DAY)";
		$ios_join_nogame_28days = $db_main->getvalue($sql);
		
		//최근 7일
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS web_join_nogame_7day ".
				"FROM tbl_user_ext ".
				"WHERE platform = 1 AND createdate >= DATE_SUB(NOW(), INTERVAL 7 DAY)";
		$ios_join_nogame_7days = $db_main->getvalue($sql);
		
		//어제
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS web_join_nogame_yesterday ".
				"FROM tbl_user_ext ".
				"WHERE platform = 1 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$ios_join_nogame_yesterday = $db_main->getvalue($sql);
		
		$sql = "";
		
		//type 109 : IOS
		//subtype
		//1 : 최근 28일 미게임건수 , 2 : 최근 7일 미게임건수 , 3 : 어제 미게임건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(109, 1, '$ios_join_nogame_28days', 'iOS NoGame(28days)', now()) ON DUPLICATE KEY UPDATE value='$ios_join_nogame_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(109, 2, '$ios_join_nogame_7days', 'iOS NoGame(7days)', now()) ON DUPLICATE KEY UPDATE value='$ios_join_nogame_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(109, 3, '$ios_join_nogame_yesterday', 'iOS NoGame(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$ios_join_nogame_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		//Android 미게임
		//최근 28일
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS web_join_nogame_28day ".
				"FROM tbl_user_ext ".
				"WHERE platform = 2 AND createdate >= DATE_SUB(NOW(), INTERVAL 28 DAY)";
		$and_join_nogame_28days = $db_main->getvalue($sql);
		
		//최근 7일
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS web_join_nogame_7day ".
				"FROM tbl_user_ext ".
				"WHERE platform = 2 AND createdate >= DATE_SUB(NOW(), INTERVAL 7 DAY)";
		$and_join_nogame_7days = $db_main->getvalue($sql);
		
		//어제
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS web_join_nogame_yesterday ".
				"FROM tbl_user_ext ".
				"WHERE platform = 2 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$and_join_nogame_yesterday = $db_main->getvalue($sql);
		
		$sql = "";
		
		//type 209 : Android
		//subtype
		//1 : 최근 28일 미게임건수 , 2 : 최근 7일 미게임건수 , 3 : 어제 미게임건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(209, 1, '$and_join_nogame_28days', 'Android NoGame(28days)', now()) ON DUPLICATE KEY UPDATE value='$and_join_nogame_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(209, 2, '$and_join_nogame_7days', 'Android NoGame(7days)', now()) ON DUPLICATE KEY UPDATE value='$and_join_nogame_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(209, 3, '$and_join_nogame_yesterday', 'Android NoGame(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$and_join_nogame_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		//Amazon 미게임
		//최근 28일
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS web_join_nogame_28day ".
				"FROM tbl_user_ext ".
				"WHERE platform = 3 AND createdate >= DATE_SUB(NOW(), INTERVAL 28 DAY)";
		$ama_join_nogame_28days = $db_main->getvalue($sql);
		
		//최근 7일
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS web_join_nogame_7day ".
				"FROM tbl_user_ext ".
				"WHERE platform = 3 AND createdate >= DATE_SUB(NOW(), INTERVAL 7 DAY)";
		$ama_join_nogame_7days = $db_main->getvalue($sql);
		
		//어제
		$sql = "SELECT ABS(IFNULL( ".
				"SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
				",0)) AS web_join_nogame_yesterday ".
				"FROM tbl_user_ext ".
				"WHERE platform = 3 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$ama_join_nogame_yesterday = $db_main->getvalue($sql);
		
		$sql = "";
		
		//type 309 : Amazon
		//subtype
		//1 : 최근 28일 미게임건수 , 2 : 최근 7일 미게임건수 , 3 : 어제 미게임건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(309, 1, '$ama_join_nogame_28days', 'Amazon NoGame(28days)', now()) ON DUPLICATE KEY UPDATE value='$ama_join_nogame_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(309, 2, '$ama_join_nogame_7days', 'Amazon NoGame(7days)', now()) ON DUPLICATE KEY UPDATE value='$ama_join_nogame_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(309, 3, '$ama_join_nogame_yesterday', 'Amazon NoGame(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$ama_join_nogame_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		
		// Mobile 튜토리얼
		// 최근 28일
		$sql = "SELECT platform, COUNT(*) AS total_count, SUM(IF(IFNULL(tutorial,0) < 4, 1, 0)) AS tutorial_count
				FROM (
					SELECT useridx, platform
					FROM tbl_user_ext
					WHERE createdate >= DATE_SUB(NOW(), INTERVAL 28 DAY)
				) t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx
				GROUP BY platform";
		$mobile_tutorial_28days_data = $db_main->gettotallist($sql);
		
		$ios_join_28days = 0;
		$and_join_28days = 0;
		$ama_join_28days = 0;
		
		$ios_tutorial_28days = 0;
		$and_tutorial_28days = 0;
		$ama_tutorial_28days = 0;
		
		for($i=0; $i<sizeof($mobile_tutorial_28days_data); $i++)
		{
			$platform = $mobile_tutorial_28days_data[$i]["platform"];
				
			if($platform == 1)
			{
				$ios_join_28days = $mobile_tutorial_28days_data[$i]["total_count"];
				$ios_tutorial_28days = $mobile_tutorial_28days_data[$i]["tutorial_count"];
			}
			else if($platform == 2)
			{
				$and_join_28days = $mobile_tutorial_28days_data[$i]["total_count"];
				$and_tutorial_28days = $mobile_tutorial_28days_data[$i]["tutorial_count"];
			}
			else if($platform == 3)
			{
				$ama_join_28days = $mobile_tutorial_28days_data[$i]["total_count"];
				$ama_tutorial_28days = $mobile_tutorial_28days_data[$i]["tutorial_count"];
			}
		}
		
		// 최근 7일
		$sql = "SELECT platform, COUNT(*) AS total_count, SUM(IF(IFNULL(tutorial,0) < 4, 1, 0)) AS tutorial_count
				FROM (
					SELECT useridx, platform
					FROM tbl_user_ext
					WHERE createdate >= DATE_SUB(NOW(), INTERVAL 7 DAY)
				) t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx
				GROUP BY platform";
		$mobile_tutorial_7days_data = $db_main ->gettotallist($sql);
		
		$ios_join_7days = 0;
		$and_join_7days = 0;
		$ama_join_7days = 0;
		
		$ios_tutorial_7days = 0;
		$and_tutorial_7days = 0;
		$ama_tutorial_7days = 0;
		
		for($i=0; $i<sizeof($mobile_tutorial_7days_data); $i++)
		{
			$platform = $mobile_tutorial_7days_data[$i]["platform"];
		
			if($platform == 1)
			{
				$ios_join_7days = $mobile_tutorial_7days_data[$i]["total_count"];
				$ios_tutorial_7days = $mobile_tutorial_7days_data[$i]["tutorial_count"];
			}
			else if($platform == 2)
			{
				$and_join_7days = $mobile_tutorial_7days_data[$i]["total_count"];
				$and_tutorial_7days = $mobile_tutorial_7days_data[$i]["tutorial_count"];
			}
			else if($platform == 3)
			{
				$ama_join_7days = $mobile_tutorial_7days_data[$i]["total_count"];
				$ama_tutorial_7days = $mobile_tutorial_7days_data[$i]["tutorial_count"];
			}
		}
		
		// 어제
		$sql = "SELECT platform, COUNT(*) AS total_count, SUM(IF(IFNULL(tutorial,0) < 4, 1, 0)) AS tutorial_count
				FROM (
				SELECT useridx, platform
				FROM tbl_user_ext
				WHERE createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'
				) t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx
				GROUP BY platform";
		$mobile_tutorial_yesterday_data = $db_main->gettotallist($sql);
		
		$ios_join_yesterday = 0;
		$and_join_yesterday = 0;
		$ama_join_yesterday = 0;
		
		$ios_tutorial_yesterday = 0;
		$and_tutorial_yesterday = 0;
		$ama_tutorial_yesterday = 0;
		
		for($i=0; $i<sizeof($mobile_tutorial_yesterday_data); $i++)
		{
			$platform = $mobile_tutorial_yesterday_data[$i]["platform"];
		
			if($platform == 1)
			{
				$ios_join_yesterday = $mobile_tutorial_yesterday_data[$i]["total_count"];
				$ios_tutorial_yesterday = $mobile_tutorial_yesterday_data[$i]["tutorial_count"];
			}
			else if($platform == 2)
			{
				$and_join_yesterday = $mobile_tutorial_yesterday_data[$i]["total_count"];
				$and_tutorial_yesterday = $mobile_tutorial_yesterday_data[$i]["tutorial_count"];
			}
			else if($platform == 3)
			{
				$ama_join_yesterday = $mobile_tutorial_yesterday_data[$i]["total_count"];
				$ama_tutorial_yesterday = $mobile_tutorial_yesterday_data[$i]["tutorial_count"];
			}
		}
		
		$sql = "";
		
		//type 106 : Ios, 107 : Android, 108 : Amazon
		//subtype
		//1 : 최근 28일 가입건수 , 2 : 최근 7일 가입건수 , 3 : 어제 가입건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(106, 1, '$ios_join_28days', 'iOS Join(28days)', now()) ON DUPLICATE KEY UPDATE value='$ios_join_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(106, 2, '$ios_join_7days', 'iOS Join(7days)', now()) ON DUPLICATE KEY UPDATE value='$ios_join_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(106, 3, '$ios_join_yesterday', 'iOS Join(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$ios_join_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(206, 1, '$and_join_28days', 'Android Join(28days)', now()) ON DUPLICATE KEY UPDATE value='$and_join_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(206, 2, '$and_join_7days', 'Android Join(7days)', now()) ON DUPLICATE KEY UPDATE value='$and_join_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(206, 3, '$and_join_yesterday', 'Android Join(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$and_join_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(306, 1, '$ama_join_28days', 'Amazon Join(28days)', now()) ON DUPLICATE KEY UPDATE value='$ama_join_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(306, 2, '$ama_join_7days', 'Amazon Join(7days)', now()) ON DUPLICATE KEY UPDATE value='$ama_join_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(306, 3, '$ama_join_yesterday', 'Amazon Join(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$ama_join_yesterday', writedate=now();";
		
		//type 108 : Ios, 108 : Android, 109 : Amazon
		//subtype
		//1 : 최근 28일 튜토리얼건수 , 2 : 최근 7일 튜토리얼건수 , 3 : 어제 튜토리얼건수
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(108, 1, '$ios_tutorial_28days', 'iOS NoTutorial(28days)', now()) ON DUPLICATE KEY UPDATE value='$ios_tutorial_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(108, 2, '$ios_tutorial_7days', 'iOS NoTutorial(7days)', now()) ON DUPLICATE KEY UPDATE value='$ios_tutorial_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(108, 3, '$ios_tutorial_yesterday', 'iOS NoTutorial(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$ios_tutorial_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(208, 1, '$and_tutorial_28days', 'Android NoTutorial(28days)', now()) ON DUPLICATE KEY UPDATE value='$and_tutorial_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(208, 2, '$and_tutorial_7days', 'Android NoTutorial(7days)', now()) ON DUPLICATE KEY UPDATE value='$and_tutorial_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(208, 3, '$and_tutorial_yesterday', 'Android NoTutorial(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$and_tutorial_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(308, 1, '$ama_tutorial_28days', 'Amazon NoTutorial(28days)', now()) ON DUPLICATE KEY UPDATE value='$ama_tutorial_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(308, 2, '$ama_tutorial_7days', 'Amazon NoTutorial(7days)', now()) ON DUPLICATE KEY UPDATE value='$ama_tutorial_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(308, 3, '$ama_tutorial_yesterday', 'Amazon NoTutorial(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$ama_tutorial_yesterday', writedate=now();";
		
		$db_analysis->execute($sql);
		
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		$sql = "SELECT * FROM tbl_server_status";
		$server_status_list = $db_main2->gettotallist($sql);
	
		$insert_sql = "";
	
		for($i=0; $i<sizeof($server_status_list); $i++)
		{
			$serverip = $server_status_list[$i]["serverip"];
			$servername = $server_status_list[$i]["servername"];
    		$diskuse = $server_status_list[$i]["diskuse"];
    		$cpu = $server_status_list[$i]["cpu"];
	    	$memtotal = $server_status_list[$i]["memtotal"];
	    	$memfree = $server_status_list[$i]["memfree"];
	    	$vmem = $server_status_list[$i]["vmem"];
	    	$cached = $server_status_list[$i]["cached"];
	    	$buffers = $server_status_list[$i]["buffers"];
	    	$established = $server_status_list[$i]["established"];
	    	$timewait = $server_status_list[$i]["timewait"];
	    	$closewait = $server_status_list[$i]["closewait"];
	    	$sync_recv = $server_status_list[$i]["sync_recv"];
	    	$writedate = $server_status_list[$i]["writedate"];
	
	    	$insert_sql = "INSERT INTO tbl_server_status_log(serverip, servername, diskuse, cpu, memtotal, memfree, vmem, cached, buffers, established, timewait, closewait, sync_recv, writedate)".
    		 				" VALUES('$serverip','$servername','$diskuse', '$cpu', '$memtotal', '$memfree', '$vmem', '$cached', '$buffers', '$established', '$timewait', '$closewait', '$sync_recv', '$writedate')";
	    	$db_analysis->execute($insert_sql);
		}
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
	    //web
	    $insert_sql = " INSERT INTO tbl_action_log_stat ".
	   	    " SELECT 1 AS TYPE, 0 AS os_type, COUNT(facebookid) AS cnt, '$standard_date' AS wrtedate ".
	   	    " FROM ".
	   	    " ( ".
	   	    " 	SELECT facebookid FROM `user_action_log_0` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT facebookid FROM `user_action_log_1` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT facebookid FROM `user_action_log_2` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT facebookid FROM `user_action_log_3` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT facebookid FROM `user_action_log_4` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT facebookid FROM `user_action_log_5` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT facebookid FROM `user_action_log_6` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT facebookid FROM `user_action_log_7` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT facebookid FROM `user_action_log_8` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    "	    SELECT facebookid FROM `user_action_log_9` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "    ) t1 ".
	   	    " ON DUPLICATE KEY UPDATE cnt = VALUES(cnt)";
	    $db_analysis->execute($insert_sql);
	    
	    //ios
	    $insert_sql = " INSERT INTO tbl_action_log_stat ".
	   	    " SELECT 1 AS TYPE, 1 AS os_type, COUNT(useridx) AS cnt, '$standard_date' AS wrtedate ".
	   	    " FROM ".
	   	    " ( ".
	   	    " 	SELECT useridx FROM `user_action_log_ios_0` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_ios_1` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_ios_2` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_ios_3` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_ios_4` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_ios_5` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_ios_6` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_ios_7` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_ios_8` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    "	    SELECT useridx FROM `user_action_log_ios_9` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "    ) t1 ".
	   	    " ON DUPLICATE KEY UPDATE cnt = VALUES(cnt)";
	    $db_analysis->execute($insert_sql);
	    
	    //android
	    $insert_sql = " INSERT INTO tbl_action_log_stat ".
	   	    " SELECT 1 AS TYPE, 2 AS os_type, COUNT(useridx) AS cnt, '$standard_date' AS wrtedate ".
	   	    " FROM ".
	   	    " ( ".
	   	    " 	SELECT useridx FROM `user_action_log_android_0` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_android_1` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_android_2` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_android_3` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_android_4` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_android_5` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_android_6` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_android_7` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    " 	SELECT useridx FROM `user_action_log_android_8` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "     UNION ALL ".
	   	    "	    SELECT useridx FROM `user_action_log_android_9` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "    ) t1 ".
	   	    " ON DUPLICATE KEY UPDATE cnt = VALUES(cnt)";
	    $db_analysis->execute($insert_sql);
	    
	    //amazon
	    $insert_sql = " INSERT INTO tbl_action_log_stat ".
	   	    " SELECT 1 AS TYPE, 3 AS os_type, COUNT(useridx) AS cnt, '$standard_date' AS wrtedate ".
	   	    " FROM ".
	   	    " ( ".
	   	    " 	SELECT useridx FROM `user_action_log_amazon` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	   	    "    ) t1 ".
	   	    " ON DUPLICATE KEY UPDATE cnt = VALUES(cnt)";
	    $db_analysis->execute($insert_sql);
	}
	catch (Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	try
	{
		$sql = "UPDATE tbl_scheduler_log SET status = $issuccess, writedate = NOW() WHERE logidx = $logidx;";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();
	$db_analysis->end();
	$db_livestats->end();
	$db_other->end();
?>
