<?
	include("../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();
	$db_livestats = new CDatabase_Livestats();
	$db_other = new CDatabase_Other();
	
	$db_main->execute("SET wait_timeout=72000");
	$db_analysis->execute("SET wait_timeout=72000");
	$db_livestats->execute("SET wait_timeout=72000");
	$db_other->execute("SET wait_timeout=72000");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$port = "";
	
	$str_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}
		
	// 첫 결제자 retention 통계
	try
	{
		$today = date("Y-m-d");
		
		$sdate = '2016-11-27';
		$edate = '2016-12-11';
		
		while($sdate < $edate)
		{
			$temp_date = date('Y-m-d', strtotime($sdate.' + 1 day'));
				
			write_log($temp_date);
	
			$sql = "SELECT firstbuydate, category,  adflag, after_day_firstbuy, SUM(total_cnt) AS total_cnt
			FROM (
			SELECT firstbuydate, category,  adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
			FROM tbl_user_pay_retention_log_0
			WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$temp_date' AND category = 1 AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
			GROUP BY firstbuydate, category,  adflag, after_day_firstbuy
			UNION ALL
			SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
			FROM tbl_user_pay_retention_log_1
			WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$temp_date' AND category = 1 AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
			GROUP BY firstbuydate, category, adflag, after_day_firstbuy
			UNION ALL
			SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
			FROM tbl_user_pay_retention_log_2
			WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$temp_date' AND category = 1 AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
			GROUP BY firstbuydate, category, adflag, after_day_firstbuy
			UNION ALL
			SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
			FROM tbl_user_pay_retention_log_3
			WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$temp_date' AND category = 1 AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
			GROUP BY firstbuydate, category, adflag, after_day_firstbuy
			UNION ALL
			SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
			FROM tbl_user_pay_retention_log_4
			WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$temp_date' AND category = 1 AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
			GROUP BY firstbuydate, category, adflag, after_day_firstbuy
			UNION ALL
			SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
			FROM tbl_user_pay_retention_log_5
			WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$temp_date' AND category = 1 AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
			GROUP BY firstbuydate, category, adflag, after_day_firstbuy
			UNION ALL
			SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
			FROM tbl_user_pay_retention_log_6
			WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$temp_date' AND category = 1 AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
			GROUP BY firstbuydate, category, adflag, after_day_firstbuy
			UNION ALL
			SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
			FROM tbl_user_pay_retention_log_7
			WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$temp_date' AND category = 1 AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
			GROUP BY firstbuydate, category, adflag, after_day_firstbuy
			UNION ALL
			SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
			FROM tbl_user_pay_retention_log_8
			WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$temp_date' AND category = 1 AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
			GROUP BY firstbuydate, category, adflag, after_day_firstbuy
			UNION ALL
			SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
			FROM tbl_user_pay_retention_log_9
			WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$temp_date' AND category = 1 AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
			GROUP BY firstbuydate, category, adflag, after_day_firstbuy
			) t1
			GROUP BY firstbuydate, category, adflag, after_day_firstbuy
			ORDER BY firstbuydate ASC, after_day_firstbuy ASC, category ASC, adflag ASC";
		
			$pay_retention_stat_list = $db_livestats->gettotallist($sql);
		
			for($i=0; $i<sizeof($pay_retention_stat_list); $i++)
			{
				$firstbuydate = $pay_retention_stat_list[$i]["firstbuydate"];
				$category = $pay_retention_stat_list[$i]["category"];
				$adflag = $pay_retention_stat_list[$i]["adflag"];
				$after_day_firstbuy = $pay_retention_stat_list[$i]["after_day_firstbuy"];
				$total_cnt = $pay_retention_stat_list[$i]["total_cnt"];
		
				$sql = 	"INSERT INTO tbl_user_pay_retention_daily(firstbuydate, platform, adflag, day_$after_day_firstbuy) ".
						"VALUES('$firstbuydate', $category, '$adflag', $total_cnt) ON DUPLICATE KEY UPDATE day_$after_day_firstbuy=VALUES(day_$after_day_firstbuy);";
				$db_other->execute($sql);
			}
			
			$sdate = $temp_date;
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	


	
	$db_main->end();
	$db_analysis->end();
	$db_livestats->end();
	$db_other->end();
?>