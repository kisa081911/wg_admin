<?
	include("../common/common_include.inc.php");

	$db_slave_main = new CDatabase_Slave_Main();
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();	
	
	$db_slave_main->execute("SET wait_timeout=7200");
	$db_main2->execute("SET wait_timeout=7200");
	$db_other->execute("SET wait_timeout=7200");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/newuser_gamelog_3h_stats") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/newuser_gamelog_3h_stats") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("newuser_gamelog_3h_stats Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	try 
	{
		$sql = "SELECT t1.useridx AS useridx ".
				"FROM tbl_user_ext t1 JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx WHERE platform = 0 AND DATE_SUB(NOW(), INTERVAL 24 HOUR) <= createdate AND createdate <= DATE_SUB(NOW(), INTERVAL 3 HOUR) AND tutorial = 4;";
		$useridx_data =  $db_slave_main->gettotallist($sql);
		
		$count = 0;		
		
		for($i = 0; $i < sizeof($useridx_data); $i++)
		{
			$insert_useridx = $useridx_data[$i]["useridx"];
			
			$sql = "SELECT useridx AS exist_user FROM tbl_user_gamelog_3h WHERE useridx = $insert_useridx;";
			$useridx_exsit = $db_other->getvalue($sql);
			
			if($useridx_exsit == "")
			{
				$sql = "SELECT useridx, moneyin, moneyout, playcount, writedate ".
						"FROM tbl_user_gamelog ".
						"WHERE ".
						"useridx = $insert_useridx AND ". 
						"(SELECT MIN(writedate) AS first_play FROM tbl_user_gamelog WHERE useridx = $insert_useridx) <= writedate ". 
						"AND writedate <= (SELECT DATE_ADD(MIN(writedate), INTERVAL 3 HOUR) AS play_3h FROM tbl_user_gamelog WHERE useridx = $insert_useridx)";
				$user_data = $db_other->gettotallist($sql);
				
				for($j=0; $j<sizeof($user_data); $j++)
				{
					$useridx = $user_data[$j]["useridx"];
					$moneyin = $user_data[$j]["moneyin"];
					$moneyout = $user_data[$j]["moneyout"];			
					$playcount = $user_data[$j]["playcount"];
					$writedate = $user_data[$j]["writedate"];
					
					//처음으로 넣어줄 때 
					if($j == 0)
					{
						$sql = "SELECT MIN(writedate) AS first_play_time, DATE_ADD(MIN(writedate), INTERVAL 3 HOUR) AS play_time_3h FROM tbl_user_gamelog WHERE useridx = $useridx";
						$user_time_data = $db_other->getarray($sql);		
						
						$first_play_time = $user_time_data["first_play_time"];
						$play_time_3h = $user_time_data["play_time_3h"];
								
						//5달러 이상이면 제외
						$sql = "SELECT COUNT(*) AS sum_money FROM tbl_product_order ".
								"WHERE useridx = $useridx AND status = 1 AND facebookcredit >= 50";
						$user_cnt_pay = $db_slave_main->getvalue($sql);
					}
					
					if($user_cnt_pay == 0)
					{
						if($j == 0)
						{
							$defalt_coin = 3000000;
							
							if($useridx < 1381894)
								$defalt_coin = 3000000;
							else if($useridx >= 1381894 && $useridx < 1381932)
								$defalt_coin = 2000000;
							else if($useridx >= 1381932)
								$defalt_coin = 1000000;
							
							$bet_rate = ($playcount == 0) ? 0 :  $defalt_coin / ($moneyin / $playcount);
							
							$insert_sql = "INSERT INTO tbl_user_gamelog_3h(useridx, moneyin, moneyout, playcount, coin, bet_ratio, writedate) ".
											"VALUES($useridx, $moneyin, $moneyout, $playcount, $defalt_coin, '$bet_rate', '$writedate');";
							$db_other->execute($insert_sql);
							
							$sql = "SELECT LAST_INSERT_ID()";
							$logidx = $db_other->getvalue($sql);
						}
						else 
						{
							$sql = "SELECT moneyin, moneyout, coin FROM tbl_user_gamelog_3h WHERE useridx = $useridx and logidx = $logidx;";
							$newuser_3h_data = $db_other->getarray($sql);
			
							$newuser_3h_moneyin = $newuser_3h_data["moneyin"];
							$newuser_3h_moenyout = $newuser_3h_data["moneyout"];
							$newuser_3h_default_coin = $newuser_3h_data["coin"];
							
							$newuser_3h_money_change = $newuser_3h_moenyout - $newuser_3h_moneyin;
							
							$defalt_coin = $newuser_3h_default_coin + $newuser_3h_money_change;
							$bet_rate = ($playcount == 0) ? 0 :  $defalt_coin / ($moneyin / $playcount);
							
							$insert_sql = "INSERT INTO tbl_user_gamelog_3h(useridx, moneyin, moneyout, playcount, coin, bet_ratio, writedate) ".
									"VALUES($useridx, $moneyin, $moneyout, $playcount, $defalt_coin, '$bet_rate', '$writedate');";
							$db_other->execute($insert_sql);
							
							$sql = "SELECT LAST_INSERT_ID()";
							$logidx = $db_other->getvalue($sql);				
						}
					}
					else
					{
						break;
					}
					
					$count++;
					
					if($count == 10000)
						sleep(5);
				}
			}
		}
		
		// ios
		$sql = "SELECT t1.useridx AS useridx ".
				"FROM tbl_user_ext t1 JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx WHERE platform = 1 AND DATE_SUB(NOW(), INTERVAL 24 HOUR) <= createdate AND createdate <= DATE_SUB(NOW(), INTERVAL 3 HOUR) AND tutorial = 4;";
		$useridx_data =  $db_slave_main->gettotallist($sql);
		
		$count = 0;
		
		for($i = 0; $i < sizeof($useridx_data); $i++)
		{
			$insert_useridx = $useridx_data[$i]["useridx"];
					
			$sql = "SELECT useridx AS exist_user FROM tbl_user_gamelog_ios_3h WHERE useridx = $insert_useridx;";
			$useridx_exsit = $db_other->getvalue($sql);
			
			if($useridx_exsit == "")
			{
				$sql = "SELECT useridx, moneyin, moneyout, playcount, writedate ".
						"FROM tbl_user_gamelog_ios ".
						"WHERE ".
						"useridx = $insert_useridx AND ".
						"(SELECT MIN(writedate) AS first_play FROM tbl_user_gamelog_ios WHERE useridx = $insert_useridx) <= writedate ".
						"AND writedate <= (SELECT DATE_ADD(MIN(writedate), INTERVAL 3 HOUR) AS play_3h FROM tbl_user_gamelog_ios WHERE useridx = $insert_useridx)";
				$user_data = $db_other->gettotallist($sql);
			
				for($j=0; $j<sizeof($user_data); $j++)
				{
					$useridx = $user_data[$j]["useridx"];
					$moneyin = $user_data[$j]["moneyin"];
					$moneyout = $user_data[$j]["moneyout"];
					$playcount = $user_data[$j]["playcount"];
					$writedate = $user_data[$j]["writedate"];
						
					if($j == 0)
					{
						$sql = "SELECT MIN(writedate) AS first_play_time, DATE_ADD(MIN(writedate), INTERVAL 3 HOUR) AS play_time_3h FROM tbl_user_gamelog_ios WHERE useridx = $useridx";
						$user_time_data = $db_other->getarray($sql);
			
						$first_play_time = $user_time_data["first_play_time"];
						$play_time_3h = $user_time_data["play_time_3h"];
			
						$sql = "SELECT COUNT(*) AS sum_money FROM tbl_product_order_mobile ".
								"WHERE useridx = $useridx AND status = 1 AND money >= 4.99";					
						$user_cnt_pay = $db_slave_main->getvalue($sql);
					}
						
					if($user_cnt_pay == 0)
					{
						if($j == 0)
						{
							$defalt_coin = 3000000;
														
							if($useridx < 1381894)
								$defalt_coin = 3000000;
							else if($useridx >= 1381894 && $useridx < 1381932)
								$defalt_coin = 2000000;
							else if($useridx >= 1381932)
								$defalt_coin = 1000000;
																
							$bet_rate = ($playcount == 0) ? 0 :  $defalt_coin / ($moneyin / $playcount);
																
							$insert_sql = "INSERT INTO tbl_user_gamelog_ios_3h(useridx, moneyin, moneyout, playcount, coin, bet_ratio, writedate) ".
											"VALUES($useridx, $moneyin, $moneyout, $playcount, $defalt_coin, '$bet_rate', '$writedate');";
							$db_other->execute($insert_sql);
																	
							$sql = "SELECT LAST_INSERT_ID()";
							$logidx = $db_other->getvalue($sql);
						}
						else
						{
							$sql = "SELECT moneyin, moneyout, coin FROM tbl_user_gamelog_ios_3h WHERE useridx = $useridx and logidx = $logidx;";
							$newuser_3h_data = $db_other->getarray($sql);
							
							$newuser_3h_moneyin = $newuser_3h_data["moneyin"];
							$newuser_3h_moenyout = $newuser_3h_data["moneyout"];
							$newuser_3h_default_coin = $newuser_3h_data["coin"];
								
							$newuser_3h_money_change = $newuser_3h_moenyout - $newuser_3h_moneyin;
								
							$defalt_coin = $newuser_3h_default_coin + $newuser_3h_money_change;
							$bet_rate = ($playcount == 0) ? 0 :  $defalt_coin / ($moneyin / $playcount);
								
							$insert_sql = "INSERT INTO tbl_user_gamelog_ios_3h(useridx, moneyin, moneyout, playcount, coin, bet_ratio, writedate) ".
									"VALUES($useridx, $moneyin, $moneyout, $playcount, $defalt_coin, '$bet_rate', '$writedate');";
							$db_other->execute($insert_sql);
									
							$sql = "SELECT LAST_INSERT_ID()";
							$logidx = $db_other->getvalue($sql);
						}
					}
					else
					{
						break;
					}
						
					$count++;
					
					if($count == 10000)
					sleep(5);
				}
			}
		}
		
		// android
		$sql = "SELECT t1.useridx AS useridx ".
				"FROM tbl_user_ext t1 JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx WHERE platform = 2 AND DATE_SUB(NOW(), INTERVAL 24 HOUR) <= createdate AND createdate <= DATE_SUB(NOW(), INTERVAL 3 HOUR) AND tutorial = 4;";
		$useridx_data =  $db_slave_main->gettotallist($sql);
		
		$count = 0;
		
		$first_and_flag = 0;
		$first_and_useridx = 0;
		
		for($i = 0; $i < sizeof($useridx_data); $i++)
		{
			$insert_useridx = $useridx_data[$i]["useridx"];
					
			$sql = "SELECT useridx AS exist_user FROM tbl_user_gamelog_android_3h WHERE useridx = $insert_useridx;";
			$useridx_exsit = $db_other->getvalue($sql);
			
			if($useridx_exsit == "")
			{
				$sql = "SELECT useridx, moneyin, moneyout, playcount, writedate ".
						"FROM tbl_user_gamelog_android ".
						"WHERE ".
						"useridx = $insert_useridx AND ".
						"(SELECT MIN(writedate) AS first_play FROM tbl_user_gamelog_android WHERE useridx = $insert_useridx) <= writedate ".
						"AND writedate <= (SELECT DATE_ADD(MIN(writedate), INTERVAL 3 HOUR) AS play_3h FROM tbl_user_gamelog_android WHERE useridx = $insert_useridx)";
				$user_data = $db_other->gettotallist($sql);
													
				for($j=0; $j<sizeof($user_data); $j++)
				{
					$useridx = $user_data[$j]["useridx"];
					$moneyin = $user_data[$j]["moneyin"];
					$moneyout = $user_data[$j]["moneyout"];
					$playcount = $user_data[$j]["playcount"];
					$writedate = $user_data[$j]["writedate"];
		
					if($j == 0)
					{
						$sql = "SELECT MIN(writedate) AS first_play_time, DATE_ADD(MIN(writedate), INTERVAL 3 HOUR) AS play_time_3h FROM tbl_user_gamelog_android WHERE useridx = $useridx";
						$user_time_data = $db_other->getarray($sql);
								
						$first_play_time = $user_time_data["first_play_time"];
						$play_time_3h = $user_time_data["play_time_3h"];
		
						$sql = "SELECT COUNT(*) AS sum_money FROM tbl_product_order_mobile ".
									"WHERE useridx = $useridx AND status = 1 AND money >= 4.99";
						$user_cnt_pay = $db_slave_main->getvalue($sql);
					}
		
					if($user_cnt_pay == 0)
					{
						if($j == 0)
						{
							$defalt_coin = 3000000;
		
							if($useridx < 1381894)
								$defalt_coin = 3000000;
							else if($useridx >= 1381894 && $useridx < 1381932)
								$defalt_coin = 2000000;
							else if($useridx >= 1381932)
								$defalt_coin = 1000000;
		
							$bet_rate = ($playcount == 0) ? 0 :  $defalt_coin / ($moneyin / $playcount);
		
							$insert_sql = "INSERT INTO tbl_user_gamelog_android_3h(useridx, moneyin, moneyout, playcount, coin, bet_ratio, writedate) ".
									"VALUES($useridx, $moneyin, $moneyout, $playcount, $defalt_coin, '$bet_rate', '$writedate');";
							$db_other->execute($insert_sql);
										
							$sql = "SELECT LAST_INSERT_ID()";
							$logidx = $db_other->getvalue($sql);
						}
						else
						{
							$sql = "SELECT moneyin, moneyout, coin FROM tbl_user_gamelog_android_3h WHERE useridx = $useridx and logidx = $logidx;";
							$newuser_3h_data = $db_other->getarray($sql);
														
							$newuser_3h_moneyin = $newuser_3h_data["moneyin"];
							$newuser_3h_moenyout = $newuser_3h_data["moneyout"];
							$newuser_3h_default_coin = $newuser_3h_data["coin"];

							$newuser_3h_money_change = $newuser_3h_moenyout - $newuser_3h_moneyin;

							$defalt_coin = $newuser_3h_default_coin + $newuser_3h_money_change;
							$bet_rate = ($playcount == 0) ? 0 :  $defalt_coin / ($moneyin / $playcount);
		
							$insert_sql = "INSERT INTO tbl_user_gamelog_android_3h(useridx, moneyin, moneyout, playcount, coin, bet_ratio, writedate) ".
										"VALUES($useridx, $moneyin, $moneyout, $playcount, $defalt_coin, '$bet_rate', '$writedate');";
							$db_other->execute($insert_sql);
										
							$sql = "SELECT LAST_INSERT_ID()";
							$logidx = $db_other->getvalue($sql);
						}
					}
					else
					{
						break;
					}
		
					$count++;
			
					if($count == 10000)
					sleep(5);
				}
			}
		}
		
		// amazon
		$sql = "SELECT t1.useridx AS useridx ".
				"FROM tbl_user_ext t1 JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx WHERE platform = 3 AND DATE_SUB(NOW(), INTERVAL 24 HOUR) <= createdate AND createdate <= DATE_SUB(NOW(), INTERVAL 3 HOUR) AND tutorial = 4;";
		$useridx_data =  $db_slave_main->gettotallist($sql);
		
		$count = 0;
		
		for($i = 0; $i < sizeof($useridx_data); $i++)
		{
			$insert_useridx = $useridx_data[$i]["useridx"];
						
			$sql = "SELECT useridx AS exist_user FROM tbl_user_gamelog_amazon_3h WHERE useridx = $insert_useridx;";
			$useridx_exsit = $db_other->getvalue($sql);
				
			if($useridx_exsit == "")
			{
				$sql = "SELECT useridx, moneyin, moneyout, playcount, writedate ".
						"FROM tbl_user_gamelog_amazon ".
						"WHERE ".
						"useridx = $insert_useridx AND ".
						"(SELECT MIN(writedate) AS first_play FROM tbl_user_gamelog_amazon WHERE useridx = $insert_useridx) <= writedate ".
						"AND writedate <= (SELECT DATE_ADD(MIN(writedate), INTERVAL 3 HOUR) AS play_3h FROM tbl_user_gamelog_amazon WHERE useridx = $insert_useridx)";
				$user_data = $db_other->gettotallist($sql);
													
				for($j=0; $j<sizeof($user_data); $j++)
				{
					$useridx = $user_data[$j]["useridx"];
					$moneyin = $user_data[$j]["moneyin"];
					$moneyout = $user_data[$j]["moneyout"];
					$playcount = $user_data[$j]["playcount"];
					$writedate = $user_data[$j]["writedate"];

					if($j == 0)
					{
						$sql = "SELECT MIN(writedate) AS first_play_time, DATE_ADD(MIN(writedate), INTERVAL 3 HOUR) AS play_time_3h FROM tbl_user_gamelog_amazon WHERE useridx = $useridx";
						$user_time_data = $db_other->getarray($sql);
		
						$first_play_time = $user_time_data["first_play_time"];
						$play_time_3h = $user_time_data["play_time_3h"];
		
						$sql = "SELECT COUNT(*) AS sum_money FROM tbl_product_order_mobile ".
								"WHERE useridx = $useridx AND status = 1 AND money >= 4.99";
						$user_cnt_pay = $db_slave_main->getvalue($sql);
					}
		
					if($user_cnt_pay == 0)
					{
						if($j == 0)
						{
							$defalt_coin = 3000000;
		
							if($useridx < 1381894)
								$defalt_coin = 3000000;
							else if($useridx >= 1381894 && $useridx < 1381932)
								$defalt_coin = 2000000;
							else if($useridx >= 1381932)
								$defalt_coin = 1000000;
		
							$bet_rate = ($playcount == 0) ? 0 :  $defalt_coin / ($moneyin / $playcount);
		
							$insert_sql = "INSERT INTO tbl_user_gamelog_amazon_3h(useridx, moneyin, moneyout, playcount, coin, bet_ratio, writedate) ".
											"VALUES($useridx, $moneyin, $moneyout, $playcount, $defalt_coin, '$bet_rate', '$writedate');";
							$db_other->execute($insert_sql);
		
							$sql = "SELECT LAST_INSERT_ID()";
							$logidx = $db_other->getvalue($sql);
						}
						else
						{
							$sql = "SELECT moneyin, moneyout, coin FROM tbl_user_gamelog_amazon_3h WHERE useridx = $useridx and logidx = $logidx;";
							$newuser_3h_data = $db_other->getarray($sql);

							$newuser_3h_moneyin = $newuser_3h_data["moneyin"];
							$newuser_3h_moenyout = $newuser_3h_data["moneyout"];
							$newuser_3h_default_coin = $newuser_3h_data["coin"];

							$newuser_3h_money_change = $newuser_3h_moenyout - $newuser_3h_moneyin;

							$defalt_coin = $newuser_3h_default_coin + $newuser_3h_money_change;
							$bet_rate = ($playcount == 0) ? 0 :  $defalt_coin / ($moneyin / $playcount);

							$insert_sql = "INSERT INTO tbl_user_gamelog_amazon_3h(useridx, moneyin, moneyout, playcount, coin, bet_ratio, writedate) ".
											"VALUES($useridx, $moneyin, $moneyout, $playcount, $defalt_coin, '$bet_rate', '$writedate');";
							$db_other->execute($insert_sql);

							$sql = "SELECT LAST_INSERT_ID()";
							$logidx = $db_other->getvalue($sql);
						}
					}
					else
					{
						break;
					}
		
					$count++;
		
					if($count == 10000)
					sleep(5);
				}
			}
		}
		
		// 웨일 유저 가능성 판단체크
		$whale_today = date("Y-m-d", time());
		
		//web
		$sql = "SELECT useridx, (CASE WHEN std_value < 6 THEN 5 WHEN std_value < 12 THEN 9 WHEN std_value < 18 THEN 19 WHEN std_value < 24 THEN 39 ELSE 59 END) AS offer ". 
		  		"FROM ( ".
                	   " SELECT a.useridx, ROUND(COUNT(DISTINCT(a.moneyin/a.playcount)) + MAX(a.moneyin/a.playcount)/MIN(a.moneyin/a.playcount) + MAX(a.moneyin/a.playcount)/AVG(a.moneyin/a.playcount)) AS std_value ". 
		  		       " FROM ".
                	   " (	".
		  		              " SELECT useridx, moneyin, playcount ".
                		      " FROM tbl_user_gamelog_3h  ".
		  		              " WHERE playcount >= 10 AND coin >= 1000000 AND coin < 10000000 AND writedate >= DATE_SUB(NOW(), INTERVAL 4 WEEK) ".
                		      " UNION ALL ".
		  		              " SELECT useridx, moneyin, playcount ".
                		      " FROM tbl_user_gamelog_ios_3h ".
		  		              " WHERE playcount >= 10 AND coin >= 1000000 AND coin < 10000000 AND writedate >= DATE_SUB(NOW(), INTERVAL 4 WEEK) ".
                		      " UNION ALL ".
		  		              " SELECT useridx, moneyin, playcount ".
                		      " FROM tbl_user_gamelog_android_3h ". 
		  		              " WHERE playcount >= 10 AND coin >= 1000000 AND coin < 10000000 AND writedate >= DATE_SUB(NOW(), INTERVAL 4 WEEK) ".
                		      " UNION ALL ".
		  		              " SELECT useridx, moneyin, playcount ".
                		      " FROM tbl_user_gamelog_amazon_3h ".
		  		              " WHERE playcount >= 10 AND coin >= 1000000 AND coin < 10000000 AND writedate >= DATE_SUB(NOW(), INTERVAL 4 WEEK) ".
                	   " ) a  ".
		  		"  GROUP BY a.useridx ".
                " ) t1";
			
		$offer_list = $db_other->gettotallist($sql);
			
		$sql = "";
		$insertcount = 0;
	
		for($i=0; $i<sizeof($offer_list); $i++)
		{
			$useridx = $offer_list[$i]["useridx"];
			$offer = $offer_list[$i]["offer"];
			
			$check_sql = "SELECT SUM(pay_count) ".
							"FROM ( ".
							"	SELECT COUNT(*) AS pay_count FROM tbl_product_order WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 ".
							"	UNION ALL ".
							"	SELECT COUNT(*) AS pay_count FROM tbl_product_order_mobile WHERE useridx = $useridx AND STATUS = 1 AND money >= 4.99 ".
							") t1";
			$check_pay = $db_slave_main->getvalue($check_sql);
			
			$user_create_check_sql = "SELECT COUNT(*) FROM tbl_user WHERE useridx = $useridx AND createdate > DATE_SUB(NOW(), INTERVAL 28 DAY);";
			$user_create_check = $db_slave_main->getvalue($user_create_check_sql);
	
			if($check_pay == 0 && $user_create_check == 1)
			{
				if ($insertcount == 0)
				{
					$sql = "INSERT INTO tbl_whale_user_offer(useridx, offer, writedate) VALUES($useridx, $offer, NOW())";
		
					$insertcount++;
				}
				else if ($insertcount < 100)
				{
					$sql .= ", ($useridx, $offer, NOW())";
					$insertcount++;
				}
				else
				{
					$sql .= " ON DUPLICATE KEY UPDATE offer=VALUES(offer), writedate=VALUES(writedate);";
					$db_main2->execute($sql);
					
					$insertcount = 0;
					$sql = "";
				}
			}
		}
	
		if($sql != "")
		{
			$sql .= " ON DUPLICATE KEY UPDATE offer=VALUES(offer), writedate=VALUES(writedate);";
			$db_main2->execute($sql);
		}
		
		$sql = "DELETE FROM tbl_whale_user_offer WHERE writedate < '$whale_today 00:00:00'";
		$db_main2->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());		
	}

	$db_slave_main->end();
	$db_main2->end();
	$db_other->end();
?>
	