<?
    include("../../common/common_include.inc.php");
    include "Sendmail.php";
    
    $today = date("Y-m-d");
    
    $now = date("Y-m-d H:i", time());
    $now_hour = date("H", time());
    
    $prev_5minute_hour = date("Y-m-d H", time() - (60 * 5));
    $prev_5minute_minute = date("i", time() - (60 * 5));
    
    $loop_check_index = 0;
    
    if($now_hour == "00")
    {
    	$day = explode(" ", $prev_5minute_hour)[0];
    }
    else
    {
    	$day = explode(" ", $now)[0];
    }
    
    $config = array(
    		'host'=>'10.0.25.103',
    		'debug'=>1,
    		'charset'=>'utf-8',
    		'ctype'=>'text/html'
    );
     
    $sendmail = new Sendmail($config);
    
    $to = "yhjeong@doubleugames.com, kisa0819@doubleugames.com, seo0882@doubleugames.com, minewat@doubleugames.com";
    $from = "plat@doubleucasino.com";
    
    $title = "[DUC] ".$today." - Error Log Check";
    $contents = "";
    
    // 특정 시간의 에러 파일명 가져오기
    $row = exec("ls /vol/logs | sort -u | grep LOG_".$day, $output, $error);
    $filename = $row;
    
    if($now_hour == "00")
    {
    	$exec_str = "cat /vol/logs/$filename|grep 'SQL Query Error' -n | grep '$prev_5minute_hour:5[5-9]:[0-5][0-9]' |cut -d: -f 1";
    }
    else
    {
    	if($prev_5minute_hour / 10 >= 1)
    	{
    		if($prev_5minute_minute % 10 == 0)
    		{
    			$exec_str = "cat /vol/logs/$filename|grep 'SQL Query Error' -n | grep '$prev_5minute_hour:".floor($prev_5minute_minute / 10)."[0-4]:[0-5][0-9]' |cut -d: -f 1";
    		}
    		else
    		{
    			$exec_str = "cat /vol/logs/$filename|grep 'SQL Query Error' -n | grep '$prev_5minute_hour:".floor($prev_5minute_minute / 10)."[5-9]:[0-5][0-9]' |cut -d: -f 1";
    		}
    	}
    	else
    	{
    		if($prev_5minute_minute % 10 == 0)
    		{
    			$exec_str = "cat /vol/logs/$filename|grep 'SQL Query Error' -n | grep '$prev_5minute_hour:0[0-4]:[0-5][0-9]' |cut -d: -f 1";
    		}
    		else
    		{
    			$exec_str = "cat /vol/logs/$filename|grep 'SQL Query Error' -n | grep '$prev_5minute_hour:0[5-9]:[0-5][0-9]' |cut -d: -f 1";
    		}
    	}
    }
    
    $line = exec($exec_str, $result_line);
    
    for($i=0; $i<sizeof($result_line); $i++)
    {
    	// 발생한 에러 출력
    	$exec_str = "sed -n '$result_line[$i] ,$result_line[$i]p' /vol/logs/$filename";
    	$sub_row = exec($exec_str, $result);
    	
    	$contents .= "Error : ".substr($sub_row, 0, 1500)." <br/>";
    	
    	$start_index = $result_line[$i];
    	
    	if(strpos($sub_row, "STEP") !== false)
    	{
	    	while(true)
	    	{
	    		if($loop_check_index > 150)
	    		{
	    			$loop_check_index = 0;
	    			break;
	    		}
	    		
	    		$exec_str = "sed -n '".$start_index." ,".$start_index."p' /vol/logs/$filename";
	    		$sub_row = exec($exec_str, $result);
	    		
	    		if(strpos($sub_row, ".php") !== false)
	    		{
	    			$contents .= "DB : $sub_row <br/>";
	    			
	    			$exec_str = "sed -n '".($start_index + 1)." ,".($start_index + 1)."p' /vol/logs/$filename";
	    			$sub_row = exec($exec_str, $result);
	    			
	    			$contents .= "File : $sub_row <br/>";
	    			
	    			break;
	    		}
	    		
	    		$start_index++;
	    		$loop_check_index++;
	    	}
    	}
    	
    	$contents .= "<br/>";
    }
    
    if($contents != "")
    {
    	$contents .= "<br/>check time : $prev_5minute_hour:$prev_5minute_minute ~ $now<br/>";
    	
    	$sendmail->send_mail($to, $from, $title, $contents);
    }
?>