<?
	include("../../common/common_include.inc.php");
	include "Sendmail.php";
	
	include('./Net/SSH2.php');
	include('./Crypt/RSA.php');
	include('./Net/SFTP.php');
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/monitor/webserver_error_log_check_scheduler") !== false)
		{
			$count++;
		}
	}
	
	$issuccess = "1";
	
	if ($count > 1)
		exit();
	
	$config = array(
			'host'=>'localhost',
			'debug'=>1,
			'charset'=>'utf-8',
			'ctype'=>'text/html'
	);
	
	$sendmail = new Sendmail($config);
	
	//$to = "yhjeong@doubleugames.com, kisa0819@doubleugames.com, seo0882@doubleugames.com, minewat@doubleugames.com";
	$to = "kisa0819@doubleugames.com, minewat@doubleugames.com";
	$from = "plat@doubleucasino.com";
	
	$today = date("Y-m-d");
	
	$title = "[Take5] ".$today." - Web Server Error Log Check";
	$contents = "";
	
	DEFINE('USER','ec2-user');
	DEFINE('KEY','private_minewat.ppk');
	
	$rsa = new Crypt_RSA();
	$rsa->loadKey(file_get_contents(KEY));
	
	$webserver_01_ip = "52.21.146.71";
	$webserver_02_ip = "52.2.15.89";
	
	// Web Error
	$webserver_log_list = array();
	$contents = array();
	
	$sftp = new Net_SFTP($webserver_01_ip, 7302);
	$sftp->login(USER, $rsa);
	$webserver_01_log = $sftp->get("/vol/logs/LOG_$today.txt");
	unset($sftp);
	
	$sftp = new Net_SFTP($webserver_02_ip, 7302);
	$sftp->login(USER, $rsa);
	$webserver_02_log = $sftp->get("/vol/logs/LOG_$today.txt");
	unset($sftp);
	
	$webserver_log_list[0] = $webserver_01_log;
	$webserver_log_list[1] = $webserver_02_log;
	
	$prev_10minute_time = date("H:i:s", time() - (60 * 10));
	$now_time = date("H:i:s", time());
	
	for($i=0; $i<sizeof($webserver_log_list); $i++)
	{
		$contents[$i] = "";
		
		$error_log_arr = explode("[$today", $webserver_log_list[$i]);
		
		for($j=0; $j<sizeof($error_log_arr); $j++)
		{
			$error_msg = $error_log_arr[$j];
			
			if(strpos($error_msg, "SQL Query Error") !== false)
			{
				$error_msg_time_arr = explode(":", explode("] SQL", $error_msg)[0]); 
				$error_msg_time = "$error_msg_time_arr[0]:$error_msg_time_arr[1]:$error_msg_time_arr[2]";
				$error_msg_time = date("H:i:s" , strtotime($error_msg_time."+0 days") );
				
				if(($prev_10minute_time <= $error_msg_time && $error_msg_time <= $now_time))
				{
					if($contents[$i] == "")
					{
						$contents[$i] .= "<br/><br/><br/>[WebServer 0".($i + 1)." / Web Error]<br/>";
					}
					
					$contents[$i] .= "[$today".$error_msg."<br/>";
				}
			}
		}
	}
	
	$send_contents = "";
	
	for($i=0; $i<sizeof($contents); $i++)
	{
		if($contents[$i] != "")
		{
			$send_contents .= $contents[$i];
		}
	}
		
	// Mobile Error
	$webserver_log_list = array();
	$contents = array();
	
	$sftp = new Net_SFTP($webserver_01_ip, 7302);
	$sftp->login(USER, $rsa);
	$webserver_01_log = $sftp->get("/vol/logs/LOG_MOBILE_$today.txt");
	unset($sftp);
	
	$sftp = new Net_SFTP($webserver_02_ip, 7302);
	$sftp->login(USER, $rsa);
	$webserver_02_log = $sftp->get("/vol/logs/LOG_MOBILE_$today.txt");
	unset($sftp);
	
	$webserver_log_list[0] = $webserver_01_log;
	$webserver_log_list[1] = $webserver_02_log;
	
	$prev_10minute_time = date("H:i:s", time() - (60 * 10));
	$now_time = date("H:i:s", time());
	
	for($i=0; $i<sizeof($webserver_log_list); $i++)
	{
		$contents[$i] = "";
		
		$error_log_arr = explode("[$today", $webserver_log_list[$i]);
		
		for($j=0; $j<sizeof($error_log_arr); $j++)
		{
			$error_msg = $error_log_arr[$j];
			
			if(strpos($error_msg, "SQL Query Error") !== false)
			{
				$error_msg_time_arr = explode(":", explode("] SQL", $error_msg)[0]); 
				$error_msg_time = "$error_msg_time_arr[0]:$error_msg_time_arr[1]:$error_msg_time_arr[2]";
				$error_msg_time = date("H:i:s" , strtotime($error_msg_time."+0 days") );
				
				if(($prev_10minute_time <= $error_msg_time && $error_msg_time <= $now_time))
				{
					if($contents[$i] == "")
					{
						$contents[$i] .= "<br/><br/><br/>[WebServer 0".($i + 1)." / Mobile Error]<br/>";
					}
					
					$contents[$i] .= "[$today".$error_msg."<br/>";
				}
			}
		}
	}
	
	
	
	for($i=0; $i<sizeof($contents); $i++)
	{
		if($contents[$i] != "")
		{
			$send_contents .= $contents[$i];
		}
	}
	
	if($send_contents != "")
	{
		$send_contents .= "<br/>check time : $prev_10minute_time ~ $now_time<br/>";
		 
		$sendmail->send_mail($to, $from, $title, $send_contents);
	}
?>
