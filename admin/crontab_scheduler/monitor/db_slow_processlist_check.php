<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/monitor/db_slow_processlist_check") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
		exit();
	
	$db_main2 = new CDatabase_Main2();
	
	$db_main2->execute("SET wait_timeout=60");

	
	try 
	{
		$to = "yhjeong@afewgoodsoft.com,kisa0819@doubleugames.com";		
		$from = "support@doubleucasino.com";		
		
		$title = "[T5] 슬로우 쿼리 발생";
		
		$content = "";
		
		$sql = "SELECT USER, HOST, db, command, TIME, state, info ".
				"FROM `information_schema`.`PROCESSLIST` WHERE TIME > 1 AND COMMAND NOT IN ('Sleep', 'Binlog Dump') AND state IS NOT NULL;";
		$db2_slowprocesslist_info = $db_main2->gettotallist($sql);
		
		for($i = 0; $i < sizeof($db2_slowprocesslist_info); $i++)
		{
			$user = $db2_slowprocesslist_info[$i]["USER"];
			$host = $db2_slowprocesslist_info[$i]["HOST"];
			$db = $db2_slowprocesslist_info[$i]["db"];
			$command = $db2_slowprocesslist_info[$i]["command"];
			$time = $db2_slowprocesslist_info[$i]["TIME"];
			$state = $db2_slowprocesslist_info[$i]["state"];
			$info = $db2_slowprocesslist_info[$i]["info"];
			
			$contents .= "[DB]<br/>";
			$contents .= "user : ".$user.", host : ".$host.", db : ".$db.", command : ".$command.", time : ".$time.", state : ".$state.", info : ".$info."<br/>";
		}	
		
		if($db2_slowprocesslist_info != null)
			sendmail($to, $from, $title, $contents);		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
?>
