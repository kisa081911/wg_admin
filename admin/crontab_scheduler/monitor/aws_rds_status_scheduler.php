<?
	include("../../common/common_include.inc.php");
	require("../../common/aws_sdk/vendor/autoload.php");
	
	use Aws\Common\Aws;
	use Aws\CloudWatch\CloudWatchClient;
	use Aws\Exception\AwsException;
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/monitor/aws_rds_status_scheduler.php") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
		exit();
	
	$db_main2 = new CDatabase_Main2();
	
	$db_main2->execute("SET wait_timeout=60");
	
	$aws = Aws::factory('../../common/aws_sdk/aws_config.php');
	
	// Get the client from the builder by namespace
	$rdsClient = $aws->get('cloudwatch');
	
	try 
	{
		// MainDB1
		$result = $rdsClient->getMetricStatistics(array(
				'Dimensions' => [
						[
								'Name' => 'DBClusterIdentifier', // REQUIRED
								'Value' => 'take5-main', // REQUIRED
						],
						[
								'Name' => 'Role', // REQUIRED
								'Value' => 'WRITER', // REQUIRED
						]
				],
				'Namespace' => 'AWS/RDS',
				'MetricName' => 'CPUUtilization',
				'StartTime' => strtotime('-3 minute'),
				'EndTime' => strtotime('now'),
				'Period' => 60,
				'Statistics' => array('Maximum', 'Minimum')
		));
		
		$datapoints = $result["Datapoints"][0];
		$cpu = $datapoints["Maximum"];
		
		$result = $rdsClient->getMetricStatistics(array(
				'Dimensions' => [
						[
								'Name' => 'DBClusterIdentifier', // REQUIRED
								'Value' => 'take5-main', // REQUIRED
						],
						[
								'Name' => 'Role', // REQUIRED
								'Value' => 'WRITER', // REQUIRED
						]
				],
				'Namespace' => 'AWS/RDS',
				'MetricName' => 'DatabaseConnections',
				'StartTime' => strtotime('-3 minute'),
				'EndTime' => strtotime('now'),
				'Period' => 60,
				'Statistics' => array('Maximum', 'Minimum')
		));
		
		$datapoints = $result["Datapoints"][0];
		$db_connection = $datapoints["Maximum"];
		
		$sql = "SELECT COUNT(*) FROM tbl_server_status WHERE serverip = 'Aurora_MainDB'";
		$status_cnt = $db_main2->getvalue($sql);
		
		if($status_cnt == 0)
		{
			$sql = "INSERT INTO tbl_server_status(serverip, servername, diskuse, cpu, memtotal, memfree, vmem, cached, buffers, established, timewait, closewait, sync_recv, writedate) ".
					"VALUES('Aurora_MainDB', 'Aurora_MainDB',  0, '$cpu', 10, 2, 0, 0, 0, '$db_connection', 0, 0, 0, NOW());";
		}
		else
		{
			$sql = "UPDATE tbl_server_status SET cpu = '$cpu', established = '$db_connection', writedate = NOW() WHERE serverip = 'Aurora_MainDB'";
		}
		
		$db_main2->execute($sql);		
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// MainDB2
		$result = $rdsClient->getMetricStatistics(array(
				'Dimensions' => [
						[
								'Name' => 'DBClusterIdentifier', // REQUIRED
								'Value' => 'take5-main2', // REQUIRED
						],
						[
								'Name' => 'Role', // REQUIRED
								'Value' => 'WRITER', // REQUIRED
						]
				],
				'Namespace' => 'AWS/RDS',
				'MetricName' => 'CPUUtilization',
				'StartTime' => strtotime('-3 minute'),
				'EndTime' => strtotime('now'),
				'Period' => 60,
				'Statistics' => array('Maximum', 'Minimum')
		));
	
		$datapoints = $result["Datapoints"][0];
		$cpu = $datapoints["Maximum"];
	
		$result = $rdsClient->getMetricStatistics(array(
				'Dimensions' => [
						[
								'Name' => 'DBClusterIdentifier', // REQUIRED
								'Value' => 'take5-main2', // REQUIRED
						],
						[
								'Name' => 'Role', // REQUIRED
								'Value' => 'WRITER', // REQUIRED
						]
				],
				'Namespace' => 'AWS/RDS',
				'MetricName' => 'DatabaseConnections',
				'StartTime' => strtotime('-3 minute'),
				'EndTime' => strtotime('now'),
				'Period' => 60,
				'Statistics' => array('Maximum', 'Minimum')
		));
	
		$datapoints = $result["Datapoints"][0];
		$db_connection = $datapoints["Maximum"];
	
		$sql = "SELECT COUNT(*) FROM tbl_server_status WHERE serverip = 'Aurora_MainDB2'";
		$status_cnt = $db_main2->getvalue($sql);
	
		if($status_cnt == 0)
		{
			$sql = "INSERT INTO tbl_server_status(serverip, servername, diskuse, cpu, memtotal, memfree, vmem, cached, buffers, established, timewait, closewait, sync_recv, writedate) ".
					"VALUES('Aurora_MainDB2', 'Aurora_MainDB2',  0, '$cpu', 10, 2, 0, 0, 0, '$db_connection', 0, 0, 0, NOW());";
		}
		else
		{
			$sql = "UPDATE tbl_server_status SET cpu = '$cpu', established = '$db_connection', writedate = NOW() WHERE serverip = 'Aurora_MainDB2'";
		}
	
		$db_main2->execute($sql);
	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		//  Take5-Other(friend, other, analysis)
		$result = $rdsClient->getMetricStatistics(array(
				'Dimensions' => [
						[
								'Name' => 'DBClusterIdentifier', // REQUIRED
								'Value' => 'take5-other', // REQUIRED
						],
						[
								'Name' => 'Role', // REQUIRED
								'Value' => 'WRITER', // REQUIRED
						]
				],
				'Namespace' => 'AWS/RDS',
				'MetricName' => 'CPUUtilization',
				'StartTime' => strtotime('-3 minute'),
				'EndTime' => strtotime('now'),
				'Period' => 60,
				'Statistics' => array('Maximum', 'Minimum')
		));
	
		$datapoints = $result["Datapoints"][0];
		$cpu = $datapoints["Maximum"];
	
		$result = $rdsClient->getMetricStatistics(array(
				'Dimensions' => [
						[
								'Name' => 'DBClusterIdentifier', // REQUIRED
								'Value' => 'take5-other', // REQUIRED
						],
						[
								'Name' => 'Role', // REQUIRED
								'Value' => 'WRITER', // REQUIRED
						]
				],
				'Namespace' => 'AWS/RDS',
				'MetricName' => 'DatabaseConnections',
				'StartTime' => strtotime('-3 minute'),
				'EndTime' => strtotime('now'),
				'Period' => 60,
				'Statistics' => array('Maximum', 'Minimum')
		));
	
		$datapoints = $result["Datapoints"][0];
		$db_connection = $datapoints["Maximum"];
	
		$sql = "SELECT COUNT(*) FROM tbl_server_status WHERE serverip = 'Aurora_OtherDB'";
		$status_cnt = $db_main2->getvalue($sql);
	
		if($status_cnt == 0)
		{
			$sql = "INSERT INTO tbl_server_status(serverip, servername, diskuse, cpu, memtotal, memfree, vmem, cached, buffers, established, timewait, closewait, sync_recv, writedate) ".
					"VALUES('Aurora_OtherDB', 'Aurora_OtherDB',  0, '$cpu', 10, 2, 0, 0, 0, '$db_connection', 0, 0, 0, NOW());";
		}
		else
		{
			$sql = "UPDATE tbl_server_status SET cpu = '$cpu', established = '$db_connection', writedate = NOW() WHERE serverip = 'Aurora_OtherDB'";
		}
	
		$db_main2->execute($sql);
	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
?>