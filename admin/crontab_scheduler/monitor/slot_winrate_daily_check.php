<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");

	ini_set("memory_limit", "-1");

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/monitor/slot_winrate_daily_check") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/monitor/slot_winrate_daily_check") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("monitor/slot_winrate_daily_check Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	try
	{
		$db_main2 = new CDatabase_Main2();
		$db_other = new CDatabase_Other();
		$db_redshift = new CDatabase_Redshift();
		$current_date = date("Y-m-d");
		
		$db_main2->execute("SET wait_timeout=7200");
		$db_other->execute("SET wait_timeout=7200");

		$sql = "SELECT 0 AS platform, slottype, slotname ". 
				"FROM `tbl_slot_list` ".
				"WHERE web = 1 ".
				"UNION ALL ".
				"SELECT 1 AS platform, slottype, slotname ". 
				"FROM `tbl_slot_list` ".
				"WHERE ios = 1 ".
				"UNION ALL ".
				"SELECT 2 AS platform, slottype, slotname ". 
				"FROM `tbl_slot_list` ".
				"WHERE android = 1 ".
				"UNION ALL ".
				"SELECT 3 AS platform, slottype, slotname ". 
				"FROM `tbl_slot_list` ".
				"WHERE amazon = 1 ";
		
		$slot_list = $db_main2->gettotallist($sql);
		
		for($i=0; $i<sizeof($slot_list); $i++)
		{
			$platform = $slot_list[$i]["platform"];
			$slottype = $slot_list[$i]["slottype"];
			$slotname = encode_db_field($slot_list[$i]["slotname"]);
			
			$sql = "INSERT INTO tbl_slot_daily_check(platform, slottype, slotname, winrate, playcount, weekcount, writedate) VALUES($platform, $slottype, '$slotname', 0, 0, 0, NOW()) ON DUPLICATE KEY UPDATE ".
					"slotname=VALUES(slotname), winrate=VALUES(winrate), playcount=VALUES(playcount), writedate=VALUES(writedate);";
			$db_other->execute($sql);
		}
		
		// Web
		for($week=1; $week<=7; $week++)
		{
			$sql = "SELECT platform, slottype FROM tbl_slot_daily_check WHERE platform = 0 AND playcount < 3000000";
			$recheck_list = $db_other->gettotallist($sql);
			
			if(sizeof($recheck_list) > 0)
			{
				$str_slot_list = "";
					
				for($i=0; $i<sizeof($recheck_list); $i++)
				{
					if($i == 0)
						$str_slot_list = $recheck_list[$i]["slottype"];
					else
						$str_slot_list .= ",".$recheck_list[$i]["slottype"];
				}
					
// 				$sql = "SELECT 0 AS platform, slottype, SUM(r*n)/SUM(n) AS winrate, SUM(n) as playcount ".
// 						"FROM ( ".
// 						"	SELECT useridx, slottype, SUM(money_out)::float/SUM(money_in)::float*100::float AS r, SUM(playcount) AS n ".
// 						"	FROM t5_user_gamelog ".
// 						"	WHERE mode = 0 and writedate >= dateadd(week,-$week,'$current_date'::date) AND slottype IN ($str_slot_list) ".
// 						"	GROUP BY useridx, slottype ".
// 						"	HAVING SUM(playcount) >= 100 ".
// 						") a ".
// 						"GROUP BY slottype";

				$sql = "select 0 as platform, slottype, sum(r_out)/SUM(r_in)*100 AS winrate, sum(n) AS playcount ".
						"from	".
						"(	".
						"	SELECT useridx, t1.slottype, SUM(money_out/bet_amount)::FLOAT as r_out, SUM(money_in/bet_amount) as r_in, SUM(playcount) AS n	". 
						"	FROM t5_user_gamelog t1 join t5_slot_bet_info t2 on t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
						"	WHERE mode IN (0,30) AND writedate >= dateadd(WEEK,-$week,'$current_date'::date) AND t1.slottype IN ($str_slot_list)	". 
						"	GROUP BY useridx, t1.slottype	". 
						//"	HAVING SUM(playcount) >= 100	".
						") a	".
						"GROUP BY slottype";
					
				$slot_result = $db_redshift->gettotallist($sql);
					
				for($i=0; $i<sizeof($slot_result); $i++)
				{
					$platform = $slot_result[$i]["platform"];
					$slottype = $slot_result[$i]["slottype"];
					$winrate = $slot_result[$i]["winrate"];
					$playcount = $slot_result[$i]["playcount"];
						
					$sql = "INSERT INTO tbl_slot_daily_check(platform, slottype, winrate, playcount, weekcount, writedate) VALUES($platform, $slottype, $winrate, $playcount, $week, NOW()) ON DUPLICATE KEY UPDATE ".
							"winrate=VALUES(winrate), playcount=VALUES(playcount), weekcount=VALUES(weekcount), writedate=VALUES(writedate);";
					$db_other->execute($sql);
				}
			}
			else
				break;
		}
		
		// Android
		for($week=1; $week<=7; $week++)
		{
			$sql = "SELECT platform, slottype FROM tbl_slot_daily_check WHERE platform = 2 AND playcount < 3000000";
			$recheck_list = $db_other->gettotallist($sql);
			
			if(sizeof($recheck_list) > 0)
			{
				$str_slot_list = "";
					
				for($i=0; $i<sizeof($recheck_list); $i++)
				{
					if($i == 0)
						$str_slot_list = $recheck_list[$i]["slottype"];
					else
						$str_slot_list .= ",".$recheck_list[$i]["slottype"];
				}
					
// 				$sql = "SELECT 2 AS platform, slottype, SUM(r*n)/SUM(n) AS winrate, SUM(n) as playcount ".
// 						"FROM ( ".
// 						"	SELECT useridx, slottype, SUM(money_out)::float/SUM(money_in)::float*100::float AS r, SUM(playcount) AS n ".
// 						"	FROM t5_user_gamelog_android ".
// 						"	WHERE mode = 0 and writedate >= dateadd(week,-$week,'$current_date'::date) AND slottype IN ($str_slot_list) ".
// 						"	GROUP BY useridx, slottype ".
// 						"	HAVING SUM(playcount) >= 100 ".
// 						") a ".
// 						"GROUP BY slottype";

				$sql = "select 2 as platform, slottype, sum(r_out)/SUM(r_in)*100 AS winrate, sum(n) AS playcount ".
						"from	".
						"(	".
						"	SELECT useridx, t1.slottype, SUM(money_out/bet_amount)::FLOAT as r_out, SUM(money_in/bet_amount) as r_in, SUM(playcount) AS n	".
						"	FROM t5_user_gamelog_android t1 join t5_slot_bet_info t2 on t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
						"	WHERE mode IN (0,30) AND writedate >= dateadd(WEEK,-$week,'$current_date'::date) AND t1.slottype IN ($str_slot_list)	".
						"	GROUP BY useridx, t1.slottype	".
						//"	HAVING SUM(playcount) >= 100	".
						") a	".
						"GROUP BY slottype";
					
				$slot_result = $db_redshift->gettotallist($sql);
					
				for($i=0; $i<sizeof($slot_result); $i++)
				{
					$platform = $slot_result[$i]["platform"];
					$slottype = $slot_result[$i]["slottype"];
					$winrate = $slot_result[$i]["winrate"];
					$playcount = $slot_result[$i]["playcount"];
						
					$sql = "INSERT INTO tbl_slot_daily_check(platform, slottype, winrate, playcount, weekcount, writedate) VALUES($platform, $slottype, $winrate, $playcount, $week, NOW()) ON DUPLICATE KEY UPDATE ".
							"winrate=VALUES(winrate), playcount=VALUES(playcount), weekcount=VALUES(weekcount), writedate=VALUES(writedate);";
					$db_other->execute($sql);
				}
			}
			else
				break;
		}
		
		// IOS
		for($week=1; $week<=7; $week++)
		{
			$sql = "SELECT platform, slottype FROM tbl_slot_daily_check WHERE platform = 1 AND playcount < 3000000";
			$recheck_list = $db_other->gettotallist($sql);
					
			if(sizeof($recheck_list) > 0)
			{
				$str_slot_list = "";
					
				for($i=0; $i<sizeof($recheck_list); $i++)
				{
					if($i == 0)
						$str_slot_list = $recheck_list[$i]["slottype"];
					else
						$str_slot_list .= ",".$recheck_list[$i]["slottype"];
				}
					
// 				$sql = "SELECT 1 AS platform, slottype, SUM(r*n)/SUM(n) AS winrate, SUM(n) as playcount ".
// 						"FROM ( ".
// 						"	SELECT useridx, slottype, SUM(money_out)::float/SUM(money_in)::float*100::float AS r, SUM(playcount) AS n ".
// 						"	FROM t5_user_gamelog_ios ".
// 						"	WHERE mode = 0 and writedate >= dateadd(week,-$week,'$current_date'::date) AND slottype IN ($str_slot_list) ".
// 								"	GROUP BY useridx, slottype ".
// 						"	HAVING SUM(playcount) >= 100 ".
// 						") a ".
// 						"GROUP BY slottype";

				$sql = "select 1 as platform, slottype, sum(r_out)/SUM(r_in)*100 AS winrate, sum(n) AS playcount ".
						"from	".
						"(	".
						"	SELECT useridx, t1.slottype, SUM(money_out/bet_amount)::FLOAT as r_out, SUM(money_in/bet_amount) as r_in, SUM(playcount) AS n	".
						"	FROM t5_user_gamelog_ios t1 join t5_slot_bet_info t2 on t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
						"	WHERE mode IN (0,30) AND writedate >= dateadd(WEEK,-$week,'$current_date'::date) AND t1.slottype IN ($str_slot_list)	".
						"	GROUP BY useridx, t1.slottype	".
						//"	HAVING SUM(playcount) >= 100	".
						") a	".
						"GROUP BY slottype";
									
				$slot_result = $db_redshift->gettotallist($sql);
							
				for($i=0; $i<sizeof($slot_result); $i++)
				{
					$platform = $slot_result[$i]["platform"];
					$slottype = $slot_result[$i]["slottype"];
					$winrate = $slot_result[$i]["winrate"];
					$playcount = $slot_result[$i]["playcount"];
		
					$sql = "INSERT INTO tbl_slot_daily_check(platform, slottype, winrate, playcount, weekcount, writedate) VALUES($platform, $slottype, $winrate, $playcount, $week, NOW()) ON DUPLICATE KEY UPDATE ".
						"winrate=VALUES(winrate), playcount=VALUES(playcount), weekcount=VALUES(weekcount), writedate=VALUES(writedate);";
					$db_other->execute($sql);
				}
			}
			else
				break;
		}
		
		// Amazon
		for($week=1; $week<=7; $week++)
		{
			$sql = "SELECT platform, slottype FROM tbl_slot_daily_check WHERE platform = 3 AND playcount < 3000000";
			$recheck_list = $db_other->gettotallist($sql);
					
			if(sizeof($recheck_list) > 0)
			{
				$str_slot_list = "";
					
				for($i=0; $i<sizeof($recheck_list); $i++)
				{
					if($i == 0)
						$str_slot_list = $recheck_list[$i]["slottype"];
					else
						$str_slot_list .= ",".$recheck_list[$i]["slottype"];
				}
						
// 				$sql = "SELECT 3 AS platform, slottype, SUM(r*n)/SUM(n) AS winrate, SUM(n) as playcount ".
// 						"FROM ( ".
// 						"	SELECT useridx, slottype, SUM(money_out)::float/SUM(money_in)::float*100::float AS r, SUM(playcount) AS n ".
// 						"	FROM t5_user_gamelog_amazon ".
// 						"	WHERE mode = 0 and writedate >= dateadd(week,-$week,'$current_date'::date) AND slottype IN ($str_slot_list) ".
// 						"	GROUP BY useridx, slottype ".
// 						"	HAVING SUM(playcount) >= 100 ".
// 						") a ".
// 						"GROUP BY slottype";

				$sql = "select 3 as platform, slottype, sum(r_out)/SUM(r_in)*100 AS winrate, sum(n) AS playcount ".
						"from	".
						"(	".
						"	SELECT useridx, t1.slottype, SUM(money_out/bet_amount)::FLOAT as r_out, SUM(money_in/bet_amount) as r_in, SUM(playcount) AS n	".
						"	FROM t5_user_gamelog_amazon t1 join t5_slot_bet_info t2 on t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
						"	WHERE mode IN (0,30) AND writedate >= dateadd(WEEK,-$week,'$current_date'::date) AND t1.slottype IN ($str_slot_list)	".
						"	GROUP BY useridx, t1.slottype	".
						//"HAVING SUM(playcount) >= 100	".
						") a	".
						"GROUP BY slottype";
											
				$slot_result = $db_redshift->gettotallist($sql);
									
				for($i=0; $i<sizeof($slot_result); $i++)
				{
					$platform = $slot_result[$i]["platform"];
					$slottype = $slot_result[$i]["slottype"];
					$winrate = $slot_result[$i]["winrate"];
					$playcount = $slot_result[$i]["playcount"];
		
					$sql = "INSERT INTO tbl_slot_daily_check(platform, slottype, winrate, playcount, weekcount, writedate) VALUES($platform, $slottype, $winrate, $playcount, $week, NOW()) ON DUPLICATE KEY UPDATE ".
							"winrate=VALUES(winrate), playcount=VALUES(playcount), weekcount=VALUES(weekcount), writedate=VALUES(writedate);";
					$db_other->execute($sql);
				}
			}
			else
				break;
		}
		
		$db_main2->end();
		$db_other->end();
		$db_redshift->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>