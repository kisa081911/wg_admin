<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/realtime_winrate_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
		exit();

	$db_main2 = new CDatabase_Main2();
	
	try 
	{
		$to = "take5-slotdev@doubleugames.com,yhjung@doubleugames.com";
		$from = "report@take5slots.com";
		
		$title = "[Take5] 최근 7일간 승률 100%이상인 슬롯";
		$contents = "";
		
		//Web Normal
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t1.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100,2) AS winrate ". 
				"FROM tbl_game_cash_stats_daily2 t1	".
				"WHERE MODE IN (0,31) AND betlevel < 10 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate ".
				"GROUP BY slottype HAVING winrate >= 100 ORDER BY winrate DESC";
		$web_normal_list = $db_main2->gettotallist($sql);		
		
		if(sizeof($web_normal_list) > 0)
		{
			$contents .= "[Web Normal]<br/>";
		
			for($i=0;$i<sizeof($web_normal_list);$i++)
			{
				$slotname = $web_normal_list[$i]["slotname"];
				$winrate = $web_normal_list[$i]["winrate"];
									
				$contents .= $slotname." winrate : ".$winrate."%<br/>";
			}
		}
		
		//Web Highroller
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t1.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100,2) AS winrate ".
				"FROM tbl_game_cash_stats_daily2 t1	".
				"WHERE MODE IN (0,31) AND betlevel >= 10 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate ".
				"GROUP BY slottype HAVING winrate >= 100 ORDER BY winrate DESC";
		$web_high_list = $db_main2->gettotallist($sql);
		
		if(sizeof($web_high_list) > 0)
		{
			$contents .= "<br/><br/>[Web Highroller]<br/>";
		
			for($i=0;$i<sizeof($web_high_list);$i++)
			{
				$slotname = $web_high_list[$i]["slotname"];
				$winrate = $web_high_list[$i]["winrate"];
								
				$contents .= $slotname." winrate : ".$winrate."%<br/>";
			}
		}
		
		//Ios Normal
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t1.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100,2) AS winrate ".
				"FROM tbl_game_cash_stats_ios_daily2 t1	".
				"WHERE MODE IN (0,31) AND betlevel < 10 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate ".
				"GROUP BY slottype HAVING winrate >= 100 ORDER BY winrate DESC";
		$ios_normal_list = $db_main2->gettotallist($sql);
		
		if(sizeof($ios_normal_list) > 0)
		{
			$contents .= "<br/><br/>[Ios Normal]<br/>";
		
			for($i=0;$i<sizeof($ios_normal_list);$i++)
			{
				$slotname = $ios_normal_list[$i]["slotname"];
				$winrate = $ios_normal_list[$i]["winrate"];
								
				$contents .= $slotname." winrate : ".$winrate."%<br/>";
			}
		}
		
		//Ios Highroller
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t1.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100,2) AS winrate ".
				"FROM tbl_game_cash_stats_ios_daily2 t1	".
				"WHERE MODE IN (0,31) AND betlevel >= 10 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate ".
				"GROUP BY slottype HAVING winrate >= 100 ORDER BY winrate DESC";
		$ios_high_list = $db_main2->gettotallist($sql);
		
		if(sizeof($ios_high_list) > 0)
		{
			$contents .= "<br/><br/>[Ios Highroller]<br/>";
		
			for($i=0;$i<sizeof($ios_high_list);$i++)
			{
				$slotname = $ios_high_list[$i]["slotname"];
				$winrate = $ios_high_list[$i]["winrate"];
		
				$contents .= $slotname." winrate : ".$winrate."%<br/>";
			}
		}
		
		//Android Normal
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t1.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100,2) AS winrate ".
				"FROM tbl_game_cash_stats_android_daily2 t1	".
				"WHERE MODE IN (0,31) AND betlevel < 10 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate ".
				"GROUP BY slottype HAVING winrate >= 100 ORDER BY winrate DESC";
		$and_normal_list = $db_main2->gettotallist($sql);
		
		if(sizeof($and_normal_list) > 0)
		{
			$contents .= "<br/><br/>[Android Normal]<br/>";
		
			for($i=0;$i<sizeof($and_normal_list);$i++)
			{
				$slotname = $and_normal_list[$i]["slotname"];
				$winrate = $and_normal_list[$i]["winrate"];
		
				$contents .= $slotname." winrate : ".$winrate."%<br/>";
			}
		}
		
		//Android Highroller
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t1.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100,2) AS winrate ".
				"FROM tbl_game_cash_stats_android_daily2 t1	".
				"WHERE MODE IN (0,31) AND betlevel >= 10 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate ".
				"GROUP BY slottype HAVING winrate >= 100 ORDER BY winrate DESC";
		$and_high_list = $db_main2->gettotallist($sql);
		
		if(sizeof($and_high_list) > 0)
		{
			$contents .= "<br/><br/>[Android Highroller]<br/>";
		
			for($i=0;$i<sizeof($and_high_list);$i++)
			{
				$slotname = $and_high_list[$i]["slotname"];
				$winrate = $and_high_list[$i]["winrate"];
		
				$contents .= $slotname." winrate : ".$winrate."%<br/>";
			}
		}
		
		//Aamazon Normal
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t1.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100,2) AS winrate ".
				"FROM tbl_game_cash_stats_amazon_daily2 t1	".
				"WHERE MODE IN (0,31) AND betlevel < 10 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate ".
				"GROUP BY slottype HAVING winrate >= 100 ORDER BY winrate DESC";
		$ama_normal_list = $db_main2->gettotallist($sql);
		
		if(sizeof($ama_normal_list) > 0)
		{
			$contents .= "<br/><br/>[Aamazon Normal]<br/>";
		
			for($i=0;$i<sizeof($ama_normal_list);$i++)
			{
				$slotname = $ama_normal_list[$i]["slotname"];
				$winrate = $ama_normal_list[$i]["winrate"];
		
				$contents .= $slotname." winrate : ".$winrate."%<br/>";
			}
		}
		
		//Aamazon Highroller
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t1.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100,2) AS winrate ".
				"FROM tbl_game_cash_stats_amazon_daily2 t1	".
				"WHERE MODE IN (0,31) AND betlevel >= 10 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate ".
				"GROUP BY slottype HAVING winrate >= 100 ORDER BY winrate DESC";
		$ama_high_list = $db_main2->gettotallist($sql);
		
		if(sizeof($ama_high_list) > 0)
		{
			$contents .= "<br/><br/>[Aamazon Highroller]<br/>";
		
			for($i=0;$i<sizeof($ama_high_list);$i++)
			{
				$slotname = $ama_high_list[$i]["slotname"];
				$winrate = $ama_high_list[$i]["winrate"];
		
				$contents .= $slotname." winrate : ".$winrate."%<br/>";
			}
		}						
							
		
		if($contents != "")
			sendmail($to, $from, $title, $contents);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
?>