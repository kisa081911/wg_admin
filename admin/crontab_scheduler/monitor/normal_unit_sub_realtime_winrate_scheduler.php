<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/normal_unit_sub_realtime_winrate_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
		exit();

	$db_main2 = new CDatabase_Main2();
	
	try 
	{
		$to = "take5-slotdev@doubleugames.com,yhjung@doubleugames.com,kisa0819@doubleugames.com";
		$from = "report@take5slots.com";
		
		$title = "[Take5]일반 승률 - 단위 승률 5이상 차이(3일 기준)";
		$contents = "";
		
		//Web - Total
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t3.slottype) AS slotname, ". 
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS win_rate, ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS unit_win_rate,	". 
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2)-ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS sub_winrate, sum(playcount) as playcount	".
				"FROM	".
				"(	".
				"	SELECT t1.slottype, moneyin, moneyout, playcount, moneyin/bet_amount AS unit_moneyin, moneyout/bet_amount AS unit_moneyout	".
				"	FROM tbl_game_cash_stats_daily2 t1 JOIN tbl_slot_bet_info t2	". 
				"	ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
				"	WHERE MODE IN(0) AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY)	".							
				") t3	".
				"GROUP BY slottype HAVING sub_winrate >= 5 ORDER BY sub_winrate DESC LIMIT 5;";
		$web_total_list = $db_main2->gettotallist($sql);		
		
		if(sizeof($web_total_list) > 0)
		{
			$contents .= "[Web 전체]<br/>";
		
			for($i=0;$i<sizeof($web_total_list);$i++)
			{
				$slotname = $web_total_list[$i]["slotname"];
				$win_rate = $web_total_list[$i]["win_rate"];
				$unit_win_rate = $web_total_list[$i]["unit_win_rate"];
				$sub_winrate = $web_total_list[$i]["sub_winrate"];
				$playcount = $web_total_list[$i]["playcount"];

				$contents .= $slotname." => 일반 승률: ".$win_rate."%, 단위 승률: ".$unit_win_rate."%, 차이 : ".$sub_winrate.", Playcount : ".$playcount."<br/>";
			}
		}
				
		//Web - Normal
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t3.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS win_rate, ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS unit_win_rate,	".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2)-ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS sub_winrate, sum(playcount) as playcount	".
				"FROM	".
				"(	".
				"	SELECT t1.slottype, moneyin, moneyout, playcount, moneyin/bet_amount AS unit_moneyin, moneyout/bet_amount AS unit_moneyout	".
				"	FROM tbl_game_cash_stats_daily2 t1 JOIN tbl_slot_bet_info t2	".
				"	ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
				"	WHERE MODE IN (0) AND t1.betlevel < 10 AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY)	".
				") t3	".
				"GROUP BY slottype HAVING sub_winrate >= 5 ORDER BY sub_winrate DESC LIMIT 5;";
		$web_normal_list = $db_main2->gettotallist($sql);
		
		if(sizeof($web_normal_list) > 0)
		{
			$contents .= "<br/><br/>[Web Normal]<br/>";
		
			for($i=0;$i<sizeof($web_normal_list);$i++)
			{
				$slotname = $web_normal_list[$i]["slotname"];
				$win_rate = $web_normal_list[$i]["win_rate"];
				$unit_win_rate = $web_normal_list[$i]["unit_win_rate"];
				$sub_winrate = $web_normal_list[$i]["sub_winrate"];
				$playcount = $web_normal_list[$i]["playcount"];
		
				$contents .= $slotname." => 일반 승률: ".$win_rate."%, 단위 승률: ".$unit_win_rate."%, 차이 : ".$sub_winrate.", Playcount : ".$playcount."<br/>";
			}
		}
		
		//Web - Highroller
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t3.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS win_rate, ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS unit_win_rate,	".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2)-ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS sub_winrate, sum(playcount) as playcount	".
				"FROM	".
				"(	".
				"	SELECT t1.slottype, moneyin, moneyout, playcount, moneyin/bet_amount AS unit_moneyin, moneyout/bet_amount AS unit_moneyout	".
				"	FROM tbl_game_cash_stats_daily2 t1 JOIN tbl_slot_bet_info t2	".
				"	ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
				"	WHERE MODE IN (0) AND t1.betlevel >= 10 AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY)	".
				") t3	".
				"GROUP BY slottype HAVING sub_winrate >= 5 ORDER BY sub_winrate DESC LIMIT 5;";
		$web_high_list = $db_main2->gettotallist($sql);
		

		if(sizeof($web_high_list) > 0)
		{
			$contents .= "<br/><br/>[Web Highroller]<br/>";
		
			for($i=0;$i<sizeof($web_high_list);$i++)
			{
				$slotname = $web_high_list[$i]["slotname"];
				$win_rate = $web_high_list[$i]["win_rate"];
				$unit_win_rate = $web_high_list[$i]["unit_win_rate"];
				$sub_winrate = $web_high_list[$i]["sub_winrate"];
				$playcount = $web_high_list[$i]["playcount"];
		
				$contents .= $slotname." => 일반 승률: ".$win_rate."%, 단위 승률: ".$unit_win_rate."%, 차이 : ".$sub_winrate.", Playcount : ".$playcount."<br/>";
			}
		}
		
		//IOS - Total
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t3.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS win_rate, ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS unit_win_rate,	".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2)-ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS sub_winrate, SUM(playcount) AS playcount	".
				"FROM	".
				"(	".
				"	SELECT t1.slottype, moneyin, moneyout, playcount, moneyin/bet_amount AS unit_moneyin, moneyout/bet_amount AS unit_moneyout	".
				"	FROM tbl_game_cash_stats_ios_daily2 t1 JOIN tbl_slot_bet_info t2	".
				"	ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
				"	WHERE MODE IN ( 0,30) AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY)	".
				") t3	".
				"GROUP BY slottype HAVING sub_winrate >= 5 ORDER BY sub_winrate DESC LIMIT 5;";
		$ios_total_list = $db_main2->gettotallist($sql);
		
		if(sizeof($ios_total_list) > 0)
		{
			$contents .= "<br/><br/>[Ios 전체]<br/>";
		
			for($i=0;$i<sizeof($ios_total_list);$i++)
			{
				$slotname = $ios_total_list[$i]["slotname"];
				$win_rate = $ios_total_list[$i]["win_rate"];
				$unit_win_rate = $ios_total_list[$i]["unit_win_rate"];
				$sub_winrate = $ios_total_list[$i]["sub_winrate"];
				$playcount = $ios_total_list[$i]["playcount"];
		
				$contents .= $slotname." => 일반 승률: ".$win_rate."%, 단위 승률: ".$unit_win_rate."%, 차이 : ".$sub_winrate.", Playcount : ".$playcount."<br/>";
			}
		}
		
		//Ios - Normal
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t3.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS win_rate, ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS unit_win_rate,	".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2)-ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS sub_winrate, SUM(playcount) AS playcount	".
				"FROM	".
				"(	".
				"	SELECT t1.slottype, moneyin, moneyout, playcount, moneyin/bet_amount AS unit_moneyin, moneyout/bet_amount AS unit_moneyout	".
				"	FROM tbl_game_cash_stats_ios_daily2 t1 JOIN tbl_slot_bet_info t2	".
				"	ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
				"	WHERE MODE IN ( 0 ,30) AND t1.betlevel < 10 AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY)	".
				") t3	".
				"GROUP BY slottype HAVING sub_winrate >= 5 ORDER BY sub_winrate DESC LIMIT 5;";
		$ios_normal_list = $db_main2->gettotallist($sql);
		
		if(sizeof($ios_normal_list) > 0)
		{
			$contents .= "<br/><br/>[Ios Normal]<br/>";
		
			for($i=0;$i<sizeof($ios_normal_list);$i++)
			{
				$slotname = $ios_normal_list[$i]["slotname"];
				$win_rate = $ios_normal_list[$i]["win_rate"];
				$unit_win_rate = $ios_normal_list[$i]["unit_win_rate"];
				$sub_winrate = $ios_normal_list[$i]["sub_winrate"];
				$playcount = $ios_normal_list[$i]["playcount"];
		
				$contents .= $slotname." => 일반 승률: ".$win_rate."%, 단위 승률: ".$unit_win_rate."%, 차이 : ".$sub_winrate.", Playcount : ".$playcount."<br/>";
			}
		}
		
		//Ios - Highroller
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t3.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS win_rate, ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS unit_win_rate,	".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2)-ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS sub_winrate, SUM(playcount) AS playcount	".
				"FROM	".
				"(	".
				"	SELECT t1.slottype, moneyin, moneyout, playcount, moneyin/bet_amount AS unit_moneyin, moneyout/bet_amount AS unit_moneyout	".
				"	FROM tbl_game_cash_stats_ios_daily2 t1 JOIN tbl_slot_bet_info t2	".
				"	ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
				"	WHERE MODE IN ( 0,30) AND t1.betlevel >= 10 AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY)	".
				") t3	".
				"GROUP BY slottype HAVING sub_winrate >= 5 ORDER BY sub_winrate DESC LIMIT 5;";
		$ios_high_list = $db_main2->gettotallist($sql);
				
		if(sizeof($ios_high_list) > 0)
		{
			$contents .= "<br/><br/>[Ios Highroller]<br/>";
		
			for($i=0;$i<sizeof($ios_high_list);$i++)
			{
				$slotname = $ios_high_list[$i]["slotname"];
				$win_rate = $ios_high_list[$i]["win_rate"];
				$unit_win_rate = $ios_high_list[$i]["unit_win_rate"];
				$sub_winrate = $ios_high_list[$i]["sub_winrate"];
				$playcount = $ios_high_list[$i]["playcount"];
		
				$contents .= $slotname." => 일반 승률: ".$win_rate."%, 단위 승률: ".$unit_win_rate."%, 차이 : ".$sub_winrate.", Playcount : ".$playcount."<br/>";
			}
		}
		
		//Android - Total
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t3.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS win_rate, ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS unit_win_rate,	".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2)-ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS sub_winrate, SUM(playcount) AS playcount	".
				"FROM	".
				"(	".
				"	SELECT t1.slottype, moneyin, moneyout, playcount, moneyin/bet_amount AS unit_moneyin, moneyout/bet_amount AS unit_moneyout	".
				"	FROM tbl_game_cash_stats_android_daily2 t1 JOIN tbl_slot_bet_info t2	".
				"	ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
				"	WHERE MODE IN( 0,30) AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY)	".
				") t3	".
				"GROUP BY slottype HAVING sub_winrate >= 5 ORDER BY sub_winrate DESC LIMIT 5;";
		$and_total_list = $db_main2->gettotallist($sql);
		
		if(sizeof($and_total_list) > 0)
		{
			$contents .= "<br/><br/>[Android 전체]<br/>";
		
			for($i=0;$i<sizeof($and_total_list);$i++)
			{
				$slotname = $and_total_list[$i]["slotname"];
				$win_rate = $and_total_list[$i]["win_rate"];
				$unit_win_rate = $and_total_list[$i]["unit_win_rate"];
				$sub_winrate = $and_total_list[$i]["sub_winrate"];
				$playcount = $and_total_list[$i]["playcount"];
		
				$contents .= $slotname." => 일반 승률: ".$win_rate."%, 단위 승률: ".$unit_win_rate."%, 차이 : ".$sub_winrate.", Playcount : ".$playcount."<br/>";
			}
		}

		//Android - Normal
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t3.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS win_rate, ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS unit_win_rate,	".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2)-ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS sub_winrate, SUM(playcount) AS playcount	".
				"FROM	".
				"(	".
				"	SELECT t1.slottype, moneyin, moneyout, playcount, moneyin/bet_amount AS unit_moneyin, moneyout/bet_amount AS unit_moneyout	".
				"	FROM tbl_game_cash_stats_android_daily2 t1 JOIN tbl_slot_bet_info t2	".
				"	ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
				"	WHERE MODE IN ( 0,30) AND t1.betlevel < 10 AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY)	".
				") t3	".
				"GROUP BY slottype HAVING sub_winrate >= 5 ORDER BY sub_winrate DESC LIMIT 5;";
		$and_normal_list = $db_main2->gettotallist($sql);
		
		if(sizeof($and_normal_list) > 0)
		{
			$contents .= "<br/><br/>[Android Normal]<br/>";
		
			for($i=0;$i<sizeof($and_normal_list);$i++)
			{
				$slotname = $and_normal_list[$i]["slotname"];
				$win_rate = $and_normal_list[$i]["win_rate"];
				$unit_win_rate = $and_normal_list[$i]["unit_win_rate"];
				$sub_winrate = $and_normal_list[$i]["sub_winrate"];
				$playcount = $and_normal_list[$i]["playcount"];
		
				$contents .= $slotname." => 일반 승률: ".$win_rate."%, 단위 승률: ".$unit_win_rate."%, 차이 : ".$sub_winrate.", Playcount : ".$playcount."<br/>";
			}
		}
		
		//Android - Highroller
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t3.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS win_rate, ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS unit_win_rate,	".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2)-ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS sub_winrate, SUM(playcount) AS playcount	".
				"FROM	".
				"(	".
				"	SELECT t1.slottype, moneyin, moneyout, playcount, moneyin/bet_amount AS unit_moneyin, moneyout/bet_amount AS unit_moneyout	".
				"	FROM tbl_game_cash_stats_android_daily2 t1 JOIN tbl_slot_bet_info t2	".
				"	ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
				"	WHERE MODE IN ( 0,30) AND t1.betlevel >= 10 AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY)	".
				") t3	".
				"GROUP BY slottype HAVING sub_winrate >= 5 ORDER BY sub_winrate DESC LIMIT 5;";
		$and_high_list = $db_main2->gettotallist($sql);
		
		if(sizeof($and_high_list) > 0)
		{
			$contents .= "<br/><br/>[Android Highroller]<br/>";
		
			for($i=0;$i<sizeof($and_high_list);$i++)
			{
				$slotname = $and_high_list[$i]["slotname"];
				$win_rate = $and_high_list[$i]["win_rate"];
				$unit_win_rate = $and_high_list[$i]["unit_win_rate"];
				$sub_winrate = $and_high_list[$i]["sub_winrate"];
				$playcount = $and_high_list[$i]["playcount"];
		
				$contents .= $slotname." => 일반 승률: ".$win_rate."%, 단위 승률: ".$unit_win_rate."%, 차이 : ".$sub_winrate.", Playcount : ".$playcount."<br/>";
			}
		}
		
		//Aamazon - Total
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t3.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS win_rate, ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS unit_win_rate,	".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2)-ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS sub_winrate, SUM(playcount) as playcount	".
				"FROM	".
				"(	".
				"	SELECT t1.slottype, moneyin, moneyout, playcount, moneyin/bet_amount AS unit_moneyin, moneyout/bet_amount AS unit_moneyout	".
				"	FROM tbl_game_cash_stats_amazon_daily2 t1 JOIN tbl_slot_bet_info t2	".
				"	ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
				"	WHERE MODE IN (0) AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY)	".
				") t3	".
				"GROUP BY slottype HAVING sub_winrate >= 5 ORDER BY sub_winrate DESC LIMIT 5;";
		$ama_total_list = $db_main2->gettotallist($sql);
		
		if(sizeof($ama_total_list) > 0)
		{
			$contents .= "<br/><br/>[Amazon 전체]<br/>";
		
			for($i=0;$i<sizeof($ama_total_list);$i++)
			{
				$slotname = $ama_total_list[$i]["slotname"];
				$win_rate = $ama_total_list[$i]["win_rate"];
				$unit_win_rate = $ama_total_list[$i]["unit_win_rate"];
				$sub_winrate = $ama_total_list[$i]["sub_winrate"];
				$playcount = $ama_total_list[$i]["playcount"];
		
				$contents .= $slotname." => 일반 승률: ".$win_rate."%, 단위 승률: ".$unit_win_rate."%, 차이 : ".$sub_winrate.", Playcount : ".$playcount."<br/>";
			}
		}
		
		//Aamazon - Normal
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t3.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS win_rate, ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS unit_win_rate,	".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2)-ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS sub_winrate, SUM(playcount) AS playcount	".
				"FROM	".
				"(	".
				"	SELECT t1.slottype, moneyin, moneyout, playcount, moneyin/bet_amount AS unit_moneyin, moneyout/bet_amount AS unit_moneyout	".
				"	FROM tbl_game_cash_stats_amazon_daily2 t1 JOIN tbl_slot_bet_info t2	".
				"	ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
				"	WHERE MODE IN ( 0,30) AND t1.betlevel < 10 AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY)	".
				") t3	".
				"GROUP BY slottype HAVING sub_winrate >= 5 ORDER BY sub_winrate DESC LIMIT 5;";
		$ama_normal_list = $db_main2->gettotallist($sql);
		
		if(sizeof($ama_normal_list) > 0)
		{
			$contents .= "<br/><br/>[Aamazon Normal]<br/>";
		
			for($i=0;$i<sizeof($ama_normal_list);$i++)
			{
				$slotname = $ama_normal_list[$i]["slotname"];
				$win_rate = $ama_normal_list[$i]["win_rate"];
				$unit_win_rate = $ama_normal_list[$i]["unit_win_rate"];
				$sub_winrate = $ama_normal_list[$i]["sub_winrate"];
				$playcount = $ama_normal_list[$i]["playcount"];
		
				$contents .= $slotname ." => 일반 승률: ".$win_rate."%, 단위 승률: ".$unit_win_rate."%, 차이 : ".$sub_winrate.", Playcount : ".$playcount."<br/>";
			}
		}
		
		//Aamazon - Highroller
		$sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t3.slottype) AS slotname, ".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS win_rate, ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS unit_win_rate,	".
				"ROUND(SUM(moneyout)/SUM(moneyin)*100, 2)-ROUND(SUM(unit_moneyout)/SUM(unit_moneyin)*100, 2) AS sub_winrate, SUM(playcount) AS playcount	".
				"FROM	".
				"(	".
				"	SELECT t1.slottype, moneyin, moneyout, playcount, moneyin/bet_amount AS unit_moneyin, moneyout/bet_amount AS unit_moneyout	".
				"	FROM tbl_game_cash_stats_amazon_daily2 t1 JOIN tbl_slot_bet_info t2	".
				"	ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
				"	WHERE MODE IN ( 0,30) AND t1.betlevel >= 10 AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY)	".
				") t3	".
				"GROUP BY slottype HAVING sub_winrate >= 5 ORDER BY sub_winrate DESC LIMIT 5;";
		$ama_high_list = $db_main2->gettotallist($sql);
		
		if(sizeof($ama_high_list) > 0)
		{
			$contents .= "<br/><br/>[Aamazon Highroller]<br/>";
		
			for($i=0;$i<sizeof($ama_high_list);$i++)
			{
				$slotname = $ama_high_list[$i]["slotname"];
				$win_rate = $ama_high_list[$i]["win_rate"];
				$unit_win_rate = $ama_high_list[$i]["unit_win_rate"];
				$sub_winrate = $ama_high_list[$i]["sub_winrate"];
				$playcount = $ama_high_list[$i]["playcount"];
		
				$contents .= $slotname." => 일반 승률: ".$win_rate."%, 단위 승률: ".$unit_win_rate."%, 차이 : ".$sub_winrate.", Playcount : ".$playcount."<br/>";
			}
		}
	
		if($contents != "")
			sendmail($to, $from, $title, $contents);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
?>
