<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/monitor/winrate_change") !== false)
		{
			$count++;
		}
	}

	$issuccess = "1";
	
	if ($count > 1)
		exit();
	
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	
	$db_main2->execute("SET wait_timeout=600");
	$db_analysis->execute("SET wait_timeout=600");

	try
	{
		$standard_date = date("Y-m-d H:i").":00";
		
		$to = "take5-qa@doubleugames.com,plat@doubleugames.com,dq@afewgoodsoft.com,sangmin@doubleugames.com,ttlrere@afewgoodsoft.com,sbkim@doubleugames.com,slot-qa@afewgoodsoft.com,slot-dev@afewgoodsoft.com,yeowoojin@doubleugames.com,blue98mh@doubleugames.com,eaglesunny@afewgoodsoft.com";
		$from = "plat@doubleugames.com";
		
		$title = "[Take5] 승률조정";
		$content = "";

		//Web
		$sql = "SELECT adminid FROM tbl_slotratio_change_history ". 
				"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'web' ".
				"GROUP BY adminid	".
				"UNION ALL	".
				"SELECT adminid FROM tbl_slotratio3_change_history	". 
				"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'web' ".
				"GROUP BY adminid";	
		
		$adminid_list = $db_analysis->gettotallist($sql);
		
		if($adminid_list != null)
		{
		
			for($i=0; $i<sizeof($adminid_list); $i++)
			{
				if($i == sizeof($adminid_list) - 1)
					$adminid .= "'".$adminid_list[$i]["adminid"]."'";
				else
					$adminid .= "'".$adminid_list[$i]["adminid"]."',";
			}
			
			$adminname_list = 0;
			
			if($adminid != "")
			{			
				$sql = "SELECT adminname FROM tbl_admin WHERE adminid IN ($adminid)";
				$adminname_list = $db_analysis->gettotallist($sql);
			}
			
			for($i=0; $i<sizeof($adminname_list); $i++)
			{
				if($i == sizeof($adminname_list) - 1)
					$adminname .= $adminname_list[$i]["adminname"];
				else
					$adminname .= $adminname_list[$i]["adminname"].", ";
			}
			
			$content = "수정한 사람 : $adminname<br/>";
			
			$sql = "SELECT * FROM tbl_slotratio_change_history ".
					"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'web' ".
					"ORDER BY slottype ASC, historyidx DESC";
			$web_ratio_history_list = $db_analysis->gettotallist($sql);
			
			$sql = "SELECT slottype,slotname FROM tbl_slot_list";
			$slotlist = $db_main2->gettotallist($sql);
			
			if($web_ratio_history_list != null)
			{
				$content .= "<br/>[Web Ratio1]<br/>";
			
				$slot_tmp = $web_ratio_history_list[0]["slottype"];
			
				for($i=0; $i<sizeof($web_ratio_history_list); $i++)
				{
					for($j=0; $j<sizeof($slotlist); $j++)
					{
						if($web_ratio_history_list[$i]["slottype"] == $slotlist[$j]["slottype"])
							$slotname = $slotlist[$j]["slotname"];
					}
			
					if($i == 0 || $slot_tmp != $web_ratio_history_list[$i]["slottype"])
					{
						$ratio = number_format(($web_ratio_history_list[$i]["current_ratio"] - $web_ratio_history_list[$i]["before_ratio"]), 1);
			
						if($ratio > 0)
							$ratio = "+".$ratio;
			
						$content .= "$slotname $ratio<br/>";
					}
			
					$slot_tmp = $web_ratio_history_list[$i]["slottype"];
				}
			}
			
			$sql = "SELECT * FROM tbl_slotratio3_change_history ".
					"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'web' ".
					"ORDER BY slottype ASC, historyidx DESC";
			$web_ratio3_history_list = $db_analysis->gettotallist($sql);
			
			$sql = "SELECT slottype,slotname FROM tbl_slot_list";
			$slotlist = $db_main2->gettotallist($sql);
			
			if($web_ratio3_history_list != null)
			{
				$content .= "<br/>[Web Ratio3]<br/>";
			
				$slot_tmp = $web_ratio3_history_list[0]["slottype"];
			
				for($i=0; $i<sizeof($web_ratio3_history_list); $i++)
				{
					for($j=0; $j<sizeof($slotlist); $j++)
					{
						if($web_ratio3_history_list[$i]["slottype"] == $slotlist[$j]["slottype"])
							$slotname = $slotlist[$j]["slotname"];
					}
			
					if($i == 0 || $slot_tmp != $web_ratio3_history_list[$i]["slottype"])
					{
						$ratio3 = number_format(($web_ratio3_history_list[$i]["current_ratio"] - $web_ratio3_history_list[$i]["before_ratio"])/10, 1);
			
						if($ratio3 > 0)
							$ratio3 = "+".$ratio3;
			
						$content .= "$slotname $ratio3<br/>";
					}
			
					$slot_tmp = $web_ratio3_history_list[$i]["slottype"];
				}
			}
		}

		//ios
		$sql = "SELECT adminid FROM tbl_slotratio_change_history ".
				"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'ios' ".
				"GROUP BY adminid	".
				"UNION ALL	".
				"SELECT adminid FROM tbl_slotratio3_change_history	".
				"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'ios' ".
				"GROUP BY adminid";
		
		$adminid_list = $db_analysis->gettotallist($sql);
		
		if($adminid_list != null)
		{
		
			for($i=0; $i<sizeof($adminid_list); $i++)
			{
				if($i == sizeof($adminid_list) - 1)
					$adminid .= "'".$adminid_list[$i]["adminid"]."'";
				else
					$adminid .= "'".$adminid_list[$i]["adminid"]."',";
			}
									
			$adminname_list = 0;
										
			if($adminid != "")
			{
				$sql = "SELECT adminname FROM tbl_admin WHERE adminid IN ($adminid)";
				$adminname_list = $db_analysis->gettotallist($sql);
			}
									
			for($i=0; $i<sizeof($adminname_list); $i++)
			{
				if($i == sizeof($adminname_list) - 1)
					$adminname .= $adminname_list[$i]["adminname"];
				else
					$adminname .= $adminname_list[$i]["adminname"].", ";
			}
	
			$content .= "수정한 사람 : $adminname<br/>";
						
			$sql = "SELECT * FROM tbl_slotratio_change_history ".
					"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'ios' ".
					"ORDER BY slottype ASC, historyidx DESC";
			$ios_ratio_history_list = $db_analysis->gettotallist($sql);
										
			$sql = "SELECT slottype,slotname FROM tbl_slot_list";
			$slotlist = $db_main2->gettotallist($sql);
	
			if($ios_ratio_history_list != null)
			{
				$content .= "<br/>[Ios Ratio1]<br/>";
								
				$slot_tmp = $ios_ratio_history_list[0]["slottype"];
								
				for($i=0; $i<sizeof($ios_ratio_history_list); $i++)
				{
					for($j=0; $j<sizeof($slotlist); $j++)
					{
						if($ios_ratio_history_list[$i]["slottype"] == $slotlist[$j]["slottype"])
							$slotname = $slotlist[$j]["slotname"];
					}
									
					if($i == 0 || $slot_tmp != $ios_ratio_history_list[$i]["slottype"])
					{
						$ratio = number_format(($ios_ratio_history_list[$i]["current_ratio"] - $ios_ratio_history_list[$i]["before_ratio"]), 1);
								
						if($ratio > 0)
							$ratio = "+".$ratio;
									
						$content .= "$slotname $ratio<br/>";
					}
		
					$slot_tmp = $ios_ratio_history_list[$i]["slottype"];
				}
			}
										
			$sql = "SELECT * FROM tbl_slotratio3_change_history ".
					"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'ios' ".
					"ORDER BY slottype ASC, historyidx DESC";
			$ios_ratio3_history_list = $db_analysis->gettotallist($sql);
									
			$sql = "SELECT slottype,slotname FROM tbl_slot_list";
			$slotlist = $db_main2->gettotallist($sql);
						
			if($ios_ratio3_history_list != null)
			{
				$content .= "<br/>[Ios Ratio3]<br/>";
						
				$slot_tmp = $ios_ratio3_history_list[0]["slottype"];
						
				for($i=0; $i<sizeof($ios_ratio3_history_list); $i++)
				{
					for($j=0; $j<sizeof($slotlist); $j++)
					{
						if($ios_ratio3_history_list[$i]["slottype"] == $slotlist[$j]["slottype"])
							$slotname = $slotlist[$j]["slotname"];
					}
						
					if($i == 0 || $slot_tmp != $ios_ratio3_history_list[$i]["slottype"])
					{
						$ratio3 = number_format(($ios_ratio3_history_list[$i]["current_ratio"] - $ios_ratio3_history_list[$i]["before_ratio"])/10, 1);
						
						if($ratio3 > 0)
							$ratio3 = "+".$ratio3;
							
						$content .= "$slotname $ratio3<br/>";
					}
				
						$slot_tmp = $ios_ratio3_history_list[$i]["slottype"];
				}
			}
		}
		
		//android
		$sql = "SELECT adminid FROM tbl_slotratio_change_history ".
				"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'android' ".
				"GROUP BY adminid	".
				"UNION ALL	".
				"SELECT adminid FROM tbl_slotratio3_change_history	".
				"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'android' ".
				"GROUP BY adminid";
		
		$adminid_list = $db_analysis->gettotallist($sql);
		
		if($adminid_list != null)
		{
		
			for($i=0; $i<sizeof($adminid_list); $i++)
			{
				if($i == sizeof($adminid_list) - 1)
					$adminid .= "'".$adminid_list[$i]["adminid"]."'";
				else
					$adminid .= "'".$adminid_list[$i]["adminid"]."',";
			}
			
			$adminname_list = 0;
		
			if($adminid != "")
			{
				$sql = "SELECT adminname FROM tbl_admin WHERE adminid IN ($adminid)";
				$adminname_list = $db_analysis->gettotallist($sql);
			}
										
			for($i=0; $i<sizeof($adminname_list); $i++)
			{
				if($i == sizeof($adminname_list) - 1)
					$adminname .= $adminname_list[$i]["adminname"];
				else
					$adminname .= $adminname_list[$i]["adminname"].", ";
			}
		
			$content .= "수정한 사람 : $adminname<br/>";
		
			$sql = "SELECT * FROM tbl_slotratio_change_history ".
					"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'android' ".
					"ORDER BY slottype ASC, historyidx DESC";
			$and_ratio_history_list = $db_analysis->gettotallist($sql);
		
			$sql = "SELECT slottype,slotname FROM tbl_slot_list";
			$slotlist = $db_main2->gettotallist($sql);
		
			if($and_ratio_history_list != null)
			{
				$content .= "<br/>[Android Ratio1]<br/>";
		
				$slot_tmp = $and_ratio_history_list[0]["slottype"];
		
				for($i=0; $i<sizeof($and_ratio_history_list); $i++)
				{
					for($j=0; $j<sizeof($slotlist); $j++)
					{
						if($and_ratio_history_list[$i]["slottype"] == $slotlist[$j]["slottype"])
							$slotname = $slotlist[$j]["slotname"];
					}
				
					if($i == 0 || $slot_tmp != $and_ratio_history_list[$i]["slottype"])
					{
						$ratio = number_format(($and_ratio_history_list[$i]["current_ratio"] - $and_ratio_history_list[$i]["before_ratio"]), 1);
		
						if($ratio > 0)
							$ratio = "+".$ratio;
				
						$content .= "$slotname $ratio<br/>";
					}
		
					$slot_tmp = $and_ratio_history_list[$i]["slottype"];
				}
			}
		
			$sql = "SELECT * FROM tbl_slotratio3_change_history ".
					"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'android' ".
					"ORDER BY slottype ASC, historyidx DESC";
			$and_ratio3_history_list = $db_analysis->gettotallist($sql);
					
			$sql = "SELECT slottype,slotname FROM tbl_slot_list";
					$slotlist = $db_main2->gettotallist($sql);
		
			if($and_ratio3_history_list != null)
			{
				$content .= "<br/>[Android Ratio3]<br/>";
		
				$slot_tmp = $and_ratio3_history_list[0]["slottype"];
		
				for($i=0; $i<sizeof($and_ratio3_history_list); $i++)
				{
					for($j=0; $j<sizeof($slotlist); $j++)
					{
						if($and_ratio3_history_list[$i]["slottype"] == $slotlist[$j]["slottype"])
							$slotname = $slotlist[$j]["slotname"];
					}
		
					if($i == 0 || $slot_tmp != $and_ratio3_history_list[$i]["slottype"])
					{
						$ratio3 = number_format(($and_ratio3_history_list[$i]["current_ratio"] - $and_ratio3_history_list[$i]["before_ratio"])/10, 1);
		
						if($ratio3 > 0)
							$ratio3 = "+".$ratio3;
										
						$content .= "$slotname $ratio3<br/>";
					}
		
					$slot_tmp = $and_ratio3_history_list[$i]["slottype"];
				}
			}
		}
		
		//amazon
		$sql = "SELECT adminid FROM tbl_slotratio_change_history ".
				"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'amazon' ".
				"GROUP BY adminid	".
				"UNION ALL	".
				"SELECT adminid FROM tbl_slotratio3_change_history	".
				"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'amazon' ".
				"GROUP BY adminid";
		
		$adminid_list = $db_analysis->gettotallist($sql);
		
		if($adminid_list != null)
		{
		
			for($i=0; $i<sizeof($adminid_list); $i++)
			{
				if($i == sizeof($adminid_list) - 1)
					$adminid .= "'".$adminid_list[$i]["adminid"]."'";
				else
					$adminid .= "'".$adminid_list[$i]["adminid"]."',";
			}
				
			$adminname_list = 0;
			
			if($adminid != "")
			{
				$sql = "SELECT adminname FROM tbl_admin WHERE adminid IN ($adminid)";
				$adminname_list = $db_analysis->gettotallist($sql);
			}
			
			for($i=0; $i<sizeof($adminname_list); $i++)
			{
				if($i == sizeof($adminname_list) - 1)
					$adminname .= $adminname_list[$i]["adminname"];
				else
					$adminname .= $adminname_list[$i]["adminname"].", ";
			}
			
			$content .= "수정한 사람 : $adminname<br/>";
			
			$sql = "SELECT * FROM tbl_slotratio_change_history ".
				"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'amazon' ".
				"ORDER BY slottype ASC, historyidx DESC";
				$ama_ratio_history_list = $db_analysis->gettotallist($sql);
			
			$sql = "SELECT slottype,slotname FROM tbl_slot_list";
			$slotlist = $db_main2->gettotallist($sql);
			
			if($ama_ratio_history_list != null)
			{
				$content .= "<br/>[Amazon Ratio1]<br/>";
			
				$slot_tmp = $ama_ratio_history_list[0]["slottype"];
			
				for($i=0; $i<sizeof($ama_ratio_history_list); $i++)
				{
					for($j=0; $j<sizeof($slotlist); $j++)
					{
						if($ama_ratio_history_list[$i]["slottype"] == $slotlist[$j]["slottype"])
							$slotname = $slotlist[$j]["slotname"];
					}
			
					if($i == 0 || $slot_tmp != $ama_ratio_history_list[$i]["slottype"])
					{
						$ratio = number_format(($ama_ratio_history_list[$i]["current_ratio"] - $ama_ratio_history_list[$i]["before_ratio"]), 1);
			
						if($ratio > 0)
							$ratio = "+".$ratio;
			
						$content .= "$slotname $ratio<br/>";
					}
			
					$slot_tmp = $and_ratio_history_list[$i]["slottype"];
				}
			}
	
			$sql = "SELECT * FROM tbl_slotratio3_change_history ".
					"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'amazon' ".
					"ORDER BY slottype ASC, historyidx DESC";
			$ama_ratio3_history_list = $db_analysis->gettotallist($sql);
				
			$sql = "SELECT slottype,slotname FROM tbl_slot_list";
			$slotlist = $db_main2->gettotallist($sql);
			
			if($ama_ratio3_history_list != null)
			{
				$content .= "<br/>[Amazon Ratio3]<br/>";
			
				$slot_tmp = $ama_ratio3_history_list[0]["slottype"];
			
				for($i=0; $i<sizeof($ama_ratio3_history_list); $i++)
				{
					for($j=0; $j<sizeof($slotlist); $j++)
					{
						if($ama_ratio3_history_list[$i]["slottype"] == $slotlist[$j]["slottype"])
							$slotname = $slotlist[$j]["slotname"];
					}
			
					if($i == 0 || $slot_tmp != $ama_ratio3_history_list[$i]["slottype"])
					{
						$ratio3 = number_format(($ama_ratio3_history_list[$i]["current_ratio"] - $ama_ratio3_history_list[$i]["before_ratio"])/10, 1);
			
						if($ratio3 > 0)
							$ratio3 = "+".$ratio3;
			
						$content .= "$slotname $ratio3<br/>";
					}
			
					$slot_tmp = $ama_ratio3_history_list[$i]["slottype"];
				}
			}
		}
		
		//2.0
		$sql = "SELECT adminid FROM tbl_slotratio_change_history ".
		  		"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'V20' ".
		  		"GROUP BY adminid	".
		  		"UNION ALL	".
		  		"SELECT adminid FROM tbl_slotratio3_change_history	".
		  		"WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'V20' ".
		  		"GROUP BY adminid";
		
		$adminid_list = $db_analysis->gettotallist($sql);
		
		if($adminid_list != null)
		{
		    
		    for($i=0; $i<sizeof($adminid_list); $i++)
		    {
		        if($i == sizeof($adminid_list) - 1)
		            $adminid .= "'".$adminid_list[$i]["adminid"]."'";
		            else
		                $adminid .= "'".$adminid_list[$i]["adminid"]."',";
		    }
		    
		    $adminname_list = 0;
		    
		    if($adminid != "")
		    {
		        $sql = "SELECT adminname FROM tbl_admin WHERE adminid IN ($adminid)";
		        $adminname_list = $db_analysis->gettotallist($sql);
		    }
		    
		    for($i=0; $i<sizeof($adminname_list); $i++)
		    {
		        if($i == sizeof($adminname_list) - 1)
		            $adminname .= $adminname_list[$i]["adminname"];
		            else
		                $adminname .= $adminname_list[$i]["adminname"].", ";
		    }
		    
		    $content .= "수정한 사람 : $adminname<br/>";
		    
		    $sql = "SELECT * FROM tbl_slotratio_change_history ".
		  		    "WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'V20' ".
		  		    "ORDER BY slottype ASC, historyidx DESC";
		    $web_ratio_history_list = $db_analysis->gettotallist($sql);
		    
		    $sql = "SELECT slottype,slotname FROM tbl_slot_list";
		    $slotlist = $db_main2->gettotallist($sql);
		    
		    if($web_ratio_history_list != null)
		    {
		        $content .= "<br/>[2.0 Ratio1]<br/>";
		        
		        $slot_tmp = $web_ratio_history_list[0]["slottype"];
		        
		        for($i=0; $i<sizeof($web_ratio_history_list); $i++)
		        {
		            for($j=0; $j<sizeof($slotlist); $j++)
		            {
		                if($web_ratio_history_list[$i]["slottype"] == $slotlist[$j]["slottype"])
		                    $slotname = $slotlist[$j]["slotname"];
		            }
		            
		            if($i == 0 || $slot_tmp != $web_ratio_history_list[$i]["slottype"])
		            {
		                $ratio = number_format(($web_ratio_history_list[$i]["current_ratio"] - $web_ratio_history_list[$i]["before_ratio"]), 1);
		                
		                if($ratio > 0)
		                    $ratio = "+".$ratio;
		                    
		                    $content .= "$slotname $ratio<br/>";
		            }
		            
		            $slot_tmp = $web_ratio_history_list[$i]["slottype"];
		        }
		    }
		    
		    $sql = "SELECT * FROM tbl_slotratio3_change_history ".
		  		    "WHERE writedate > DATE_SUB('$standard_date', INTERVAL 30 MINUTE) AND category = 'V20' ".
		  		    "ORDER BY slottype ASC, historyidx DESC";
		    $web_ratio3_history_list = $db_analysis->gettotallist($sql);
		    
		    $sql = "SELECT slottype,slotname FROM tbl_slot_list";
		    $slotlist = $db_main2->gettotallist($sql);
		    
		    if($web_ratio3_history_list != null)
		    {
		        $content .= "<br/>[2.0 Ratio3]<br/>";
		        
		        $slot_tmp = $web_ratio3_history_list[0]["slottype"];
		        
		        for($i=0; $i<sizeof($web_ratio3_history_list); $i++)
		        {
		            for($j=0; $j<sizeof($slotlist); $j++)
		            {
		                if($web_ratio3_history_list[$i]["slottype"] == $slotlist[$j]["slottype"])
		                    $slotname = $slotlist[$j]["slotname"];
		            }
		            
		            if($i == 0 || $slot_tmp != $web_ratio3_history_list[$i]["slottype"])
		            {
		                $ratio3 = number_format(($web_ratio3_history_list[$i]["current_ratio"] - $web_ratio3_history_list[$i]["before_ratio"])/10, 1);
		                
		                if($ratio3 > 0)
		                    $ratio3 = "+".$ratio3;
		                    
		                    $content .= "$slotname $ratio3<br/>";
		            }
		            
		            $slot_tmp = $web_ratio3_history_list[$i]["slottype"];
		        }
		    }
		}

		if($web_ratio_history_list != null || $web_ratio3_history_list != null || $ios_ratio_history_list != null || $ios_ratio3_history_list != null || $and_ratio_history_list != null || $and_ratio3_history_list != null || $ama_ratio_history_list != null || $ama_ratio3_history_list != null)
			sendmail($to, $from, $title, $content);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	$db_main2->end();
	$db_analysis->end();
?>
