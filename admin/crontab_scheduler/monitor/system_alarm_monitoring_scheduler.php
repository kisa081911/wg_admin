<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/monitor/system_alarm_monitoring_scheduler") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/monitor/system_alarm_monitoring_scheduler") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("monitor/system_alarm_monitoring_scheduler Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	$today = date('Y-m-d');
	
	$db_analysis = new CDatabase_Analysis();
	
	try
	{
		$sql = "INSERT INTO tbl_scheduler_log(today, type, status, writedate) VALUES('$today', 400, 0, now());";
		$db_analysis->execute($sql);
	
		$sql = "SELECT LAST_INSERT_ID();";
		$logidx = $db_analysis->getvalue($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$sql = "SELECT logidx, email_list, title, message, writedate
			FROM system_alarm_send_message
			WHERE is_send = 0
			ORDER BY writedate DESC
			LIMIT 1";
	$system_alarm_info = $db_analysis->getarray($sql);
	
	if($system_alarm_info != "")
	{
		$alarm_logidx = $system_alarm_info["logidx"];
		$email_list = $system_alarm_info["email_list"];
		$title = "[DUC & Take5] - ".$system_alarm_info["title"];
		$message = "[Take5 - System Alarm Send Message]<br/>".$system_alarm_info["message"];
		$writedate = $system_alarm_info["writedate"];
		
		$to = $email_list;
		$from = "support@doubleucasino.com";
		
		sendmail($to, $from, $title, $message);
		
		$sql = "UPDATE system_alarm_send_message SET is_send = 1, senddate = NOW() WHERE logidx = $alarm_logidx;";
		$db_analysis->execute($sql);
	}
	
	try
	{
		$sql = "UPDATE tbl_scheduler_log SET status = 1, writedate = now() WHERE logidx = $logidx;";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_analysis->end();
?>