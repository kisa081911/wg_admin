<?
	//매시 1분에 시작
    include("../../common/common_include.inc.php");
	
	write_log("Download Fail Scheduler Start");
	
	$db_analysis = new CDatabase_Analysis();
	
	// Download Fail Action Log
	$sql = "SELECT * FROM `action_log_ios_actionidx` ORDER BY table_number ASC";
	$ios_actionidx_list = $db_analysis->gettotallist($sql);
	
	$sql = "SELECT * FROM `action_log_android_actionidx` ORDER BY table_number ASC";
	$android_actionidx_list = $db_analysis->gettotallist($sql);
	
	$sql = "SELECT actionidx FROM `action_log_amazon_actionidx`";
	$amazon_actionidx = $db_analysis->getvalue($sql);
	
	// iOS
	for($i=0; $i<10; $i++)
	{
		$ios_actionidx = $ios_actionidx_list[$i]["actionidx"];
		
		$sql = "SELECT 
				(SELECT IFNULL(MAX(actionidx), 0) FROM user_action_log_ios_$i WHERE actionidx > $ios_actionidx AND category = 'DOWNLOAD_FAILED' AND category_info != '' AND category_info != 'zip_open_fail') AS search_max_actionidx,
				(SELECT MAX(actionidx) FROM user_action_log_ios_$i WHERE actionidx > $ios_actionidx) AS max_actionidx";
		$actionidx_data = $db_analysis->getarray($sql);
		
		$search_max_actionidx = $actionidx_data["search_max_actionidx"];
		$max_actionidx = $actionidx_data["max_actionidx"];
		
		if($search_max_actionidx > 0)
		{
			$sql = "INSERT INTO user_action_log_ios_down_fail ( ".
					"SELECT actionidx, ipaddress, useridx, category, category_info, writedate ".
					"FROM user_action_log_ios_$i ".
					"WHERE actionidx > $ios_actionidx AND actionidx <= $search_max_actionidx AND category = 'DOWNLOAD_FAILED' ".
					"AND category_info != '' AND category_info != 'zip_open_fail') ";
			$db_analysis->execute($sql);
		}
		
		$update_sql = "UPDATE action_log_ios_actionidx SET actionidx = $max_actionidx WHERE table_number = $i";
		$db_analysis->execute($update_sql);
	}
	
	sleep(1);
	
	// Android
	for($i=0; $i<10; $i++)
	{
		$android_actionidx = $android_actionidx_list[$i]["actionidx"];
		
		$sql = "SELECT 
				(SELECT IFNULL(MAX(actionidx), 0) FROM user_action_log_android_$i WHERE actionidx > $android_actionidx AND category = 'DOWNLOAD_FAILED' AND category_info != '' AND category_info != 'zip_open_fail') AS search_max_actionidx,
				(SELECT MAX(actionidx) FROM user_action_log_android_$i WHERE actionidx > $android_actionidx) AS max_actionidx";
		$actionidx_data = $db_analysis->getarray($sql);
		
		$search_max_actionidx = $actionidx_data["search_max_actionidx"];
		$max_actionidx = $actionidx_data["max_actionidx"];
		
		if($search_max_actionidx > 0)
		{
			$sql = "INSERT INTO user_action_log_android_down_fail ( ".
					"SELECT actionidx, ipaddress, useridx, category, category_info, writedate ".
					"FROM user_action_log_android_$i ".
					"WHERE actionidx > $android_actionidx AND actionidx <= $search_max_actionidx AND category = 'DOWNLOAD_FAILED' ".
					"AND category_info != '' AND category_info != 'zip_open_fail')";
			$db_analysis->execute($sql);
		}
		
		$update_sql = "UPDATE action_log_android_actionidx SET actionidx = $max_actionidx WHERE table_number = $i";
		$db_analysis->execute($update_sql);
	}
	
	sleep(1);
	
	// Amazon
	$sql = "SELECT 
				(SELECT IFNULL(MAX(actionidx), 0) FROM user_action_log_amazon WHERE actionidx > $amazon_actionidx AND category = 'DOWNLOAD_FAILED' AND category_info != '' AND category_info != 'zip_open_fail') AS search_max_actionidx,
				(SELECT MAX(actionidx) FROM user_action_log_amazon WHERE actionidx > $amazon_actionidx) AS max_actionidx";
	$actionidx_data = $db_analysis->getarray($sql);
	
	$search_max_actionidx = $actionidx_data["search_max_actionidx"];
	$max_actionidx = $actionidx_data["max_actionidx"];
	
	if($search_max_actionidx > 0)
	{
		$sql = "INSERT INTO user_action_log_amazon_down_fail ( ".
				"SELECT actionidx, ipaddress, useridx, category, category_info, writedate ".
				"FROM user_action_log_amazon ".
				"WHERE actionidx > $amazon_actionidx AND actionidx <= $search_max_actionidx AND category = 'DOWNLOAD_FAILED' ".
				"AND category_info != '' AND category_info != 'zip_open_fail')";
		$db_analysis->execute($sql);
	}
		
	$update_sql = "UPDATE action_log_amazon_actionidx SET actionidx = $max_actionidx";
	$db_analysis->execute($update_sql);

	// 시간별 통계
	
	$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d %H') AS today, COUNT(*) AS total_count, COUNT(DISTINCT useridx) AS user_count
			FROM `user_action_log_ios_down_fail`
			GROUP BY today";
	$ios_stat_list = $db_analysis->gettotallist($sql);
	
	$insert_sql = "";
	
	for($i=0; $i<sizeof($ios_stat_list); $i++)
	{
		$today = $ios_stat_list[$i]["today"];
		$total_count = $ios_stat_list[$i]["total_count"];
		$user_count = $ios_stat_list[$i]["user_count"];
		
		if($insert_sql == "")
			$insert_sql = "INSERT INTO tbl_download_fail_stat VALUES('$today', 1, $total_count, $user_count)";
		else
			$insert_sql .= ", ('$today', 1, $total_count, $user_count)";
	}
	
	if($insert_sql != "")
	{
		$insert_sql .= " ON DUPLICATE KEY UPDATE total_count = VALUES(total_count), user_count = VALUES(user_count);";
		$db_analysis->execute($insert_sql);
	}
	
	$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d %H') AS today, COUNT(*) AS total_count, COUNT(DISTINCT useridx) AS user_count
			FROM `user_action_log_android_down_fail`
			GROUP BY today";
	$and_stat_list = $db_analysis->gettotallist($sql);
	
	$insert_sql = "";
	
	for($i=0; $i<sizeof($and_stat_list); $i++)
	{
		$today = $and_stat_list[$i]["today"];
		$total_count = $and_stat_list[$i]["total_count"];
		$user_count = $and_stat_list[$i]["user_count"];
		
		if($insert_sql == "")
			$insert_sql = "INSERT INTO tbl_download_fail_stat VALUES('$today', 2, $total_count, $user_count)";
		else
			$insert_sql .= ", ('$today', 2, $total_count, $user_count)";
	}
	
	if($insert_sql != "")
	{
		$insert_sql .= " ON DUPLICATE KEY UPDATE total_count = VALUES(total_count), user_count = VALUES(user_count);";
		$db_analysis->execute($insert_sql);
	}
	
	$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d %H') AS today, COUNT(*) AS total_count, COUNT(DISTINCT useridx) AS user_count
			FROM `user_action_log_amazon_down_fail`
			GROUP BY today";
	$ama_stat_list = $db_analysis->gettotallist($sql);
	
	$insert_sql = "";
	
	for($i=0; $i<sizeof($ama_stat_list); $i++)
	{
		$today = $ama_stat_list[$i]["today"];
		$total_count = $ama_stat_list[$i]["total_count"];
		$user_count = $ama_stat_list[$i]["user_count"];
		
		if($insert_sql == "")
			$insert_sql = "INSERT INTO tbl_download_fail_stat VALUES('$today', 3, $total_count, $user_count)";
		else
			$insert_sql .= ", ('$today', 3, $total_count, $user_count)";
	}
	
	if($insert_sql != "")
	{
		$insert_sql .= " ON DUPLICATE KEY UPDATE total_count = VALUES(total_count), user_count = VALUES(user_count);";
		$db_analysis->execute($insert_sql);
	}
	
	// 일별 category 통계
	// iOS
	$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, category_info, COUNT(*) AS total_count, COUNT(DISTINCT useridx) AS user_count
			FROM `user_action_log_ios_down_fail`
			GROUP BY today, category_info";
	$ios_stat_list = $db_analysis->gettotallist($sql);
	
	$insert_sql = "";
	
	for($i=0; $i<sizeof($ios_stat_list); $i++)
	{
		$today = $ios_stat_list[$i]["today"];
		$category_info = $ios_stat_list[$i]["category_info"];
		$total_count = $ios_stat_list[$i]["total_count"];
		$user_count = $ios_stat_list[$i]["user_count"];
		
		if($insert_sql == "")
			$insert_sql = "INSERT INTO tbl_download_fail_stat_detail VALUES('$today', 1, '$category_info', $total_count, $user_count)";
		else
			$insert_sql .= ", ('$today', 1, '$category_info', $total_count, $user_count)";
	}
	
	if($insert_sql != "")
	{
		$insert_sql .= " ON DUPLICATE KEY UPDATE category_info = VALUES(category_info), total_count = VALUES(total_count), user_count = VALUES(user_count);";
		$db_analysis->execute($insert_sql);
	}
	
	// Android
	$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, category_info, COUNT(*) AS total_count, COUNT(DISTINCT useridx) AS user_count
			FROM `user_action_log_android_down_fail`
			GROUP BY today, category_info";
	$android_stat_list = $db_analysis->gettotallist($sql);
	
	$insert_sql = "";
	
	for($i=0; $i<sizeof($android_stat_list); $i++)
	{
		$today = $android_stat_list[$i]["today"];
		$category_info = $android_stat_list[$i]["category_info"];
		$total_count = $android_stat_list[$i]["total_count"];
		$user_count = $android_stat_list[$i]["user_count"];
		
		if($insert_sql == "")
			$insert_sql = "INSERT INTO tbl_download_fail_stat_detail VALUES('$today', 2, '$category_info', $total_count, $user_count)";
		else
			$insert_sql .= ", ('$today', 2, '$category_info', $total_count, $user_count)";
	}
	
	if($insert_sql != "")
	{
		$insert_sql .= " ON DUPLICATE KEY UPDATE category_info = VALUES(category_info), total_count = VALUES(total_count), user_count = VALUES(user_count);";
		$db_analysis->execute($insert_sql);
	}
	
	// Amazon
	$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, category_info, COUNT(*) AS total_count, COUNT(DISTINCT useridx) AS user_count
			FROM `user_action_log_amazon_down_fail`
			GROUP BY today, category_info";
	$amazon_stat_list = $db_analysis->gettotallist($sql);
	
	$insert_sql = "";
	
	for($i=0; $i<sizeof($amazon_stat_list); $i++)
	{
		$today = $amazon_stat_list[$i]["today"];
		$category_info = $amazon_stat_list[$i]["category_info"];
		$total_count = $amazon_stat_list[$i]["total_count"];
		$user_count = $amazon_stat_list[$i]["user_count"];
		
		if($insert_sql == "")
			$insert_sql = "INSERT INTO tbl_download_fail_stat_detail VALUES('$today', 3, '$category_info', $total_count, $user_count)";
		else
			$insert_sql .= ", ('$today', 3, '$category_info', $total_count, $user_count)";
	}
	
	if($insert_sql != "")
	{
		$insert_sql .= " ON DUPLICATE KEY UPDATE category_info = VALUES(category_info), total_count = VALUES(total_count), user_count = VALUES(user_count);";
		$db_analysis->execute($insert_sql);
	}
	
	write_log("Download Fail Scheduler End");

	$db_analysis->end();
?>