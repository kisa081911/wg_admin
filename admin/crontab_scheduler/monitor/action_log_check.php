<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/action_log_check") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
		exit();
	
	$db_analysis = new CDatabase_Analysis();
	
	$db_analysis->execute("SET wait_timeout=300");
	
	try
	{
		$from = "plat@doubleucasino.com";
		$to = "plat@doubleugames.com,take5-qa@afewgoodsoft.com";		
		 
		$title = "[Take5] Action Log Check";
		$contents = "";
		$send_mail_check = 0;
		
		//web
		$web_sql = "SELECT SUM(cnt_0) AS cnt_0, SUM(cnt_1) AS cnt_1, SUM(cnt_2) AS cnt_2, SUM(cnt_3) AS cnt_3, SUM(cnt_4) AS cnt_4, SUM(cnt_5) AS cnt_5, SUM(cnt_6) AS cnt_6, SUM(cnt_7) AS cnt_7, SUM(cnt_8) AS cnt_8, SUM(cnt_9) AS cnt_9 ".
				"FROM	".
				"(	".
				"	SELECT COUNT(*) AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_0` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, COUNT(*) AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_1` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, COUNT(*) AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_2` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, COUNT(*) AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_3` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, COUNT(*) AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_4` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, COUNT(*) AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_5` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, COUNT(*) AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_6` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, COUNT(*) AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_7` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, COUNT(*) AS cnt_8, 0 AS cnt_9 FROM `user_action_log_8` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, COUNT(*) AS cnt_9 FROM `user_action_log_9` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				") tt";
		
		$web_action_log = $db_analysis->gettotallist($web_sql);
		
		for($i=0;$i<sizeof($web_action_log);$i++)
		{
			$cnt_0 = $web_action_log[$i]["cnt_0"];
			
			if($cnt_0 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_0 No Data"."<br/>";
			}
			
			$cnt_1 = $web_action_log[$i]["cnt_1"];
			
			if($cnt_1 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_1 No Data"."<br/>";
			}
			
			$cnt_2 = $web_action_log[$i]["cnt_2"];
			
			if($cnt_2 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_2 No Data"."<br/>";
			}
			
			$cnt_3 = $web_action_log[$i]["cnt_3"];
			
			if($cnt_3 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_3 No Data"."<br/>";
			}
			
			$cnt_4 = $web_action_log[$i]["cnt_4"];
			
			if($cnt_4 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_4 No Data"."<br/>";	
			}
			
			$cnt_5 = $web_action_log[$i]["cnt_5"];
			
			if($cnt_5 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_5 No Data"."<br/>";
			}
			
			$cnt_6 = $web_action_log[$i]["cnt_6"];
			
			if($cnt_6 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_6 No Data"."<br/>";
			}
			
			$cnt_7 = $web_action_log[$i]["cnt_7"];
			
			if($cnt_7 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_7 No Data"."<br/>";
			}
			
			$cnt_8 = $web_action_log[$i]["cnt_8"];
			
			if($cnt_8 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_8 No Data"."<br/>";
			}
			
			$cnt_9 = $web_action_log[$i]["cnt_9"];
			
			if($cnt_9 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_9 No Data"."<br/>";
			}
		}
		
		//ios
		$ios_sql = "SELECT SUM(cnt_0) AS cnt_0, SUM(cnt_1) AS cnt_1, SUM(cnt_2) AS cnt_2, SUM(cnt_3) AS cnt_3, SUM(cnt_4) AS cnt_4, SUM(cnt_5) AS cnt_5, SUM(cnt_6) AS cnt_6, SUM(cnt_7) AS cnt_7, SUM(cnt_8) AS cnt_8, SUM(cnt_9) AS cnt_9 ".
				"FROM	".
				"(	".
				"	SELECT COUNT(*) AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_ios_0` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, COUNT(*) AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_ios_1` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, COUNT(*) AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_ios_2` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, COUNT(*) AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_ios_3` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, COUNT(*) AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_ios_4` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, COUNT(*) AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_ios_5` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, COUNT(*) AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_ios_6` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, COUNT(*) AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_ios_7` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, COUNT(*) AS cnt_8, 0 AS cnt_9 FROM `user_action_log_ios_8` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, COUNT(*) AS cnt_9 FROM `user_action_log_ios_9` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				") tt";
		
		$ios_action_log = $db_analysis->gettotallist($ios_sql);
		
		for($i=0;$i<sizeof($ios_action_log);$i++)
		{
			$cnt_0 = $ios_action_log[$i]["cnt_0"];
				
			if($cnt_0 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_ios_0 No Data"."<br/>";
			}
				
			$cnt_1 = $ios_action_log[$i]["cnt_1"];
				
			if($cnt_1 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_ios_1 No Data"."<br/>";
			}
				
			$cnt_2 = $ios_action_log[$i]["cnt_2"];
				
			if($cnt_2 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_ios_2 No Data"."<br/>";
			}
				
			$cnt_3 = $ios_action_log[$i]["cnt_3"];
				
			if($cnt_3 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_ios_3 No Data"."<br/>";
			}
				
			$cnt_4 = $ios_action_log[$i]["cnt_4"];
				
			if($cnt_4 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_ios_4 No Data"."<br/>";
			}
				
			$cnt_5 = $ios_action_log[$i]["cnt_5"];
				
			if($cnt_5 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_ios_5 No Data"."<br/>";
			}
				
			$cnt_6 = $ios_action_log[$i]["cnt_6"];
				
			if($cnt_6 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_ios_6 No Data"."<br/>";
			}
				
			$cnt_7 = $ios_action_log[$i]["cnt_7"];
				
			if($cnt_7 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_ios_7 No Data"."<br/>";
			}
				
			$cnt_8 = $ios_action_log[$i]["cnt_8"];
				
			if($cnt_8 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_ios_8 No Data"."<br/>";
			}
				
			$cnt_9 = $ios_action_log[$i]["cnt_9"];
				
			if($cnt_9 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_ios_9 No Data"."<br/>";
			}
		}
		
		//android
		$android_sql = "SELECT SUM(cnt_0) AS cnt_0, SUM(cnt_1) AS cnt_1, SUM(cnt_2) AS cnt_2, SUM(cnt_3) AS cnt_3, SUM(cnt_4) AS cnt_4, SUM(cnt_5) AS cnt_5, SUM(cnt_6) AS cnt_6, SUM(cnt_7) AS cnt_7, SUM(cnt_8) AS cnt_8, SUM(cnt_9) AS cnt_9 ".
				"FROM	".
				"(	".
				"	SELECT COUNT(*) AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_android_0` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, COUNT(*) AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_android_1` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, COUNT(*) AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_android_2` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, COUNT(*) AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_android_3` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, COUNT(*) AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_android_4` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, COUNT(*) AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_android_5` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, COUNT(*) AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_android_6` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, COUNT(*) AS cnt_7, 0 AS cnt_8, 0 AS cnt_9 FROM `user_action_log_android_7` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, COUNT(*) AS cnt_8, 0 AS cnt_9 FROM `user_action_log_android_8` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				"	UNION ALL	".
				"	SELECT 0 AS cnt_0, 0 AS cnt_1, 0 AS cnt_2, 0 AS cnt_3, 0 AS cnt_4, 0 AS cnt_5, 0 AS cnt_6, 0 AS cnt_7, 0 AS cnt_8, COUNT(*) AS cnt_9 FROM `user_action_log_android_9` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)	".
				") tt";
		
		$android_action_log = $db_analysis->gettotallist($android_sql);
		
		for($i=0;$i<sizeof($android_action_log);$i++)
		{
			$cnt_0 = $android_action_log[$i]["cnt_0"];
		
			if($cnt_0 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_android_0 No Data"."<br/>";
			}
		
			$cnt_1 = $android_action_log[$i]["cnt_1"];
		
			if($cnt_1 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_android_1 No Data"."<br/>";
			}
		
			$cnt_2 = $android_action_log[$i]["cnt_2"];
		
			if($cnt_2 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_android_2 No Data"."<br/>";
			}
		
			$cnt_3 = $android_action_log[$i]["cnt_3"];
		
			if($cnt_3 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_android_3 No Data"."<br/>";
			}
		
			$cnt_4 = $android_action_log[$i]["cnt_4"];
		
			if($cnt_4 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_android_4 No Data"."<br/>";
			}
		
			$cnt_5 = $android_action_log[$i]["cnt_5"];
		
			if($cnt_5 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_android_5 No Data"."<br/>";
			}
		
			$cnt_6 = $android_action_log[$i]["cnt_6"];
		
			if($cnt_6 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_android_6 No Data"."<br/>";
			}
		
			$cnt_7 = $android_action_log[$i]["cnt_7"];
		
			if($cnt_7 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_android_7 No Data"."<br/>";
			}
		
			$cnt_8 = $android_action_log[$i]["cnt_8"];
		
			if($cnt_8 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_android_8 No Data"."<br/>";
			}
		
			$cnt_9 = $android_action_log[$i]["cnt_9"];
		
			if($cnt_9 == 0)
			{
				$send_mail_check = 1;
				$contents .= "user_action_log_android_9 No Data"."<br/>";
			}
		}
		
		//amazon
		$amazon_sql = "SELECT COUNT(*) AS cnt FROM `user_action_log_amazon` WHERE writedate >= DATE_SUB(NOW(), INTERVAL 3 MINUTE)";
		$amazon_action_log = $db_analysis->getvalue($amazon_sql);
		
		if($amazon_action_log == 0)
		{
			$send_mail_check = 1;
			$contents .= "user_action_log_amazon No Data"."<br/>";
		}
		
		if($send_mail_check == 1)
			sendmail($to, $from, $title, $contents);
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_analysis->end();
?>