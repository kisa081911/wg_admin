<?
	include("../../common/common_include.inc.php");
	
	$db_livestats = new CDatabase_Livestats();	
	$db_livestats->execute("SET wait_timeout=7200");

	try 
	{
		$to = "yhjeong@afewgoodsoft.com,kisa0819@doubleugames.com,hyotng@afewgoodsoft.com";
		$from = "plat@doubleugames.com";
			
		$title = "[Take5] 리얼타임 결제 확인요청";
		$contents = "";
		
		$sql = "SELECT COUNT(*) FROM tbl_realtime_order_log WHERE writedate >= DATE_SUB(NOW(), INTERVAL 6 HOUR)";	
		$check_count = $db_livestats->getvalue($sql);
		
		if($check_count == 0)
		{
			$contents .= "RealTime Order 0 COUNT";			
			sendmail($to, $from, $title, $contents);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_livestats->end();
?>
