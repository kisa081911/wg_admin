<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/jackpot_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
		exit();

	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	
	try 
	{
		$to = "take5-qa@doubleugames.com,plat@doubleugames.com,dq@afewgoodsoft.com,sangmin@doubleugames.com,ttlrere@afewgoodsoft.com,sbkim@doubleugames.com,slot-qa@afewgoodsoft.com,slot-dev@afewgoodsoft.com";
		$from = "plat@doubleucasino.com";
		
		$title = "[Take5] Jackpot Check";
		$contents = "";
		
		//Web
		$sql = "SELECT jackpotidx, useridx, objectidx, slottype, amount, devicetype, writedate FROM tbl_jackpot_log WHERE DATE_SUB(NOW(), INTERVAL 10 MINUTE) <= writedate AND writedate <= NOW() AND fiestaidx = 0";
		$jackpot_list = $db_main->gettotallist($sql);		
		
		if(sizeof($jackpot_list) > 0)
		{
			//Slot 정보
			$sql = "SELECT slottype, slotname FROM tbl_slot_list";
			$slottype_list = $db_main2->gettotallist($sql);
		
			for($i=0;$i<sizeof($jackpot_list);$i++)
			{
				$jackpotidx = $jackpot_list[$i]["jackpotidx"];
				$useridx = $jackpot_list[$i]["useridx"];
				$objectidx = $jackpot_list[$i]["objectidx"];
				$slottype = $jackpot_list[$i]["slottype"];
				$amount = $jackpot_list[$i]["amount"];
				$devicetype = $jackpot_list[$i]["devicetype"];
				$writedate = $jackpot_list[$i]["writedate"];
				
				if($devicetype == 0)
					$device_name = "Web";
				else if($devicetype == 1)
					$device_name = "IOS";
				else if($devicetype == 2)
					$device_name = "Android";
				else if($devicetype == 3)
					$device_name = "Amazon";
				
				for($j=0; $j<sizeof($slottype_list); $j++)
				{
					if($slottype_list[$j]["slottype"] == $slottype)
					{
						$slot_name = $slottype_list[$j]["slotname"];
						break;
					}
					else
					{
						$slot_name = "Unkown";
					}
				}
										
				$contents .= "jackpotidx : ".$jackpotidx.", OS : ".$device_name.", useridx : ".$useridx.", objectidx : ".$objectidx.", slot : ".$slot_name.", amount : ".number_format($amount).", writedate : ".$writedate."<br/>";
			}
			
			write_log($contents);
		}
		
		if($jackpot_list != null)
			sendmail($to, $from, $title, $contents);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
	    $to = "take5-qa@doubleugames.com,plat@doubleugames.com,dq@afewgoodsoft.com,sangmin@doubleugames.com,ttlrere@afewgoodsoft.com,sbkim@doubleugames.com,slot-qa@afewgoodsoft.com,slot-dev@afewgoodsoft.comm";
	    $from = "plat@doubleucasino.com";
// 	    $to = "kisa0819@afewgoodsoft.com";
	    
	    $title = "[Take5] Jackpot Fail Log Check";
	    $contents = "";
	    
	    //Web
	    $sql = "SELECT * FROM tbl_jackpot_server_fail_log WHERE DATE_SUB(NOW(), INTERVAL 10 MINUTE) <= writedate AND writedate <= NOW()";
// 	    $sql = "SELECT * FROM tbl_jackpot_server_fail_log WHERE writedate > '2018-05-30 00:00:00'";
	    $jackpot_list = $db_main2->gettotallist($sql);
	    
	    if(sizeof($jackpot_list) > 0)
	    {
	        //Slot 정보
	        $sql = "SELECT slottype, slotname FROM tbl_slot_list";
	        $slottype_list = $db_main2->gettotallist($sql);
	        
	        for($i=0;$i<sizeof($jackpot_list);$i++)
	        {
	            $logindx = $jackpot_list[$i]["logindx"];
	            $useridx = $jackpot_list[$i]["useridx"];
	            $objectidx = $jackpot_list[$i]["objectidx"];
	            $slottype = $jackpot_list[$i]["slottype"];
	            $betlevel = $jackpot_list[$i]["betlevel"];
	            $potmoney = $jackpot_list[$i]["potmoney"];
	            $basemoney = $jackpot_list[$i]["basemoney"];
	            $devicetype = $jackpot_list[$i]["devicetype"];
    	            $writedate = $jackpot_list[$i]["writedate"];
	            
	            if($devicetype == 0)
	                $device_name = "Web";
	            else if($devicetype == 1)
	                 $device_name = "IOS";
	            else if($devicetype == 2)
	                 $device_name = "Android";
                else if($devicetype == 3)
	                 $device_name = "Amazon";
	                            
                for($j=0; $j<sizeof($slottype_list); $j++)
                {
                    if($slottype_list[$j]["slottype"] == $slottype)
                    {
                        $slot_name = $slottype_list[$j]["slotname"];
                        break;
                    }
                    else
                    {
                        $slot_name = "Unkown";
                    }
                }
                
                $contents .= "logindx : ".$logindx.", OS : ".$device_name.", useridx : ".$useridx.", objectidx : ".$objectidx.", slot : ".$slot_name.
                             ", betlevel : ".$betlevel.", potmoney : ".number_format($potmoney).", basemoney : ".$basemoney.", writedate : ".$writedate."<br/>";
	        }
	        
	        write_log($contents);
	    }
	    
	    if($jackpot_list != null)
	        sendmail($to, $from, $title, $contents);
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();
?>
