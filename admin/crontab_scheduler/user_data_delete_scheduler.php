<?
	include("../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/user_data_delete_scheduler") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/user_data_delete_scheduler") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("user_data_delete_scheduler Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$db_main = new CDatabase_Main();
	$db_friend = new CDatabase_Friend();
	
	$sql = "SELECT useridx FROM tbl_user_delete_info WHERE status = 0";
	$user_delete_list = $db_main->gettotallist($sql);
	
	for($i=0; $i<sizeof($user_delete_list); $i++)
	{
		$useridx = $user_delete_list[$i]["useridx"];
		
		if($useridx != "")
		{
			$sql = "UPDATE tbl_user SET userid = userid*-1 WHERE useridx = $useridx";
			$db_main->execute($sql);
			
			$sql = "UPDATE tbl_user_auth SET uid = uid*-1 WHERE useridx = $useridx";
			$db_main->execute($sql);
			
			
			$sql = "DELETE FROM tbl_user_friend_0 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_1 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_2 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_3 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_4 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_5 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_6 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_7 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_8 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_9 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_10 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_11 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_12 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_13 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_14 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_15 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_16 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_17 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_18 WHERE useridx = $useridx OR friendidx = $useridx; ".
					"DELETE FROM tbl_user_friend_19 WHERE useridx = $useridx OR friendidx = $useridx;";
			$db_friend->execute($sql);

			$sql = "UPDATE tbl_user_delete_info SET status = 1 WHERE status = 0 AND useridx = $useridx";
			$db_main->execute($sql);
		}
	}
	
	$db_main->end();
	$db_friend->end();
?>
