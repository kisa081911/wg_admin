<?
    include("../../common/common_include.inc.php");
	
	/*
	 * A2U Group 6
	 * 조건 : 결제자, 비결제자 && 보유코인 1억 이상, 10억 미만
	 * 발송 기준 시각 : 최종 접속 시간
	 * 발송 주기 및 노티 수신량 : 1일당 1회 발송
	 * */
	
	$db_main = new CDatabase_Main();

	$db_main->execute("SET wait_timeout=7200");	

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/notifications/game_request_group6_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;

		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);

		if (!$fp) 
		{
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/notifications/game_request_group6_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("game request group6 scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	$facebook = new Facebook(array(
	    'appId'  => FACEBOOK_APP_ID,
	    'secret' => FACEBOOK_SECRET_KEY,
	    'cookie' => true,
	));
	
	function utf8_bytes($cp)
	{
	    if ($cp > 0x10000){
	        # 4 bytes
	        return	chr(0xF0 | (($cp & 0x1C0000) >> 18)).
	        chr(0x80 | (($cp & 0x3F000) >> 12)).
	        chr(0x80 | (($cp & 0xFC0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else if ($cp > 0x800){
	        # 3 bytes
	        return	chr(0xE0 | (($cp & 0xF000) >> 12)).
	        chr(0x80 | (($cp & 0xFC0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else if ($cp > 0x80){
	        # 2 bytes
	        return	chr(0xC0 | (($cp & 0x7C0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else{
	        # 1 byte
	        return chr($cp);
	    }
	}
	
	try
	{	
		$noti_cnt = 0;
		$category = 6;
		
		$sql = "SELECT t1.useridx, userid
                FROM tbl_user t1 JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx
                WHERE platform = 0 AND t1.logindate < DATE_SUB(NOW(), INTERVAL 2 WEEK) AND HOUR(t1.logindate) = HOUR(NOW())
                	AND NOT EXISTS(SELECT useridx FROM tbl_a2u_user_delete WHERE useridx = t1.useridx)
                    AND userid > 0
                	AND NOT EXISTS(SELECT useridx FROM tbl_notification_group_log_new WHERE useridx = t1.useridx AND group_no = 6 AND senddate > DATE_SUB(NOW(), INTERVAL 1 WEEK))
                	AND 100000000 <= coin AND coin < 1000000000
                LIMIT 5000";
		$req_user = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($req_user); $i++)
		{
			$useridx = $req_user[$i]["useridx"];
			$facebookid = $req_user[$i]["userid"];

			$day1 = "Up to 100,000,000 Coins are waiting for you! ".utf8_bytes(0x1F4EB)." Click to Claim them Now!".utf8_bytes(0x1F4EB);
			$day2 = "Welcome back @[$facebookid]! Have some fun spinning with Free Coins we`ve just sent to your Gift Box!".utf8_bytes(0x1F3B0);
			$day3 = "@[$facebookid]! We`ve been waiting for your return! Click and collect up to 100,000,000 Special Bonus Coins!".utf8_bytes(0x1F381);
			$day4 = "A Special Gift of up to 100,000,000 Coins is waiting for you. Click and Collect it from your Gift box!".utf8_bytes(0x1F381);		
			$day5 = "Enjoy playing on our epic slots with up to 100,000,000 Bonus Coins! Click here and check them out!".utf8_bytes(0x1F609);			
			
			$daily_template = array($day1, $day2, $day3, $day4, $day5);
				
			$date_flag = rand(0, 4);
			
			$template = $daily_template[$date_flag];
		    
			$adflag = "gamenotifygroup6";
			
			$sql = "INSERT INTO tbl_notification_group_log_new(useridx, platform, group_no, type, bonus_coin, senddate) VALUES($useridx, 0, $category, 1, 0, NOW());";					
			$db_main->execute($sql);
				
			$sql = "SELECT LAST_INSERT_ID()";
			$notiidx = $db_main->getvalue($sql);

			try
			{
			    $args = array('template' => "$template",
			        'href' => "?adflag=$adflag&notiidx=$notiidx",
			        'ref' => "group_base_group6");
			    
			    $info = $facebook->api("/$facebookid/notifications", "POST", $args);
				
				$noti_cnt++;
			}
			catch (FacebookApiException $e)
			{
			    if($e->getMessage() == "Unsupported operation" || $e->getMessage() == "(#200) Cannot send notifications to a user who has not installed the app")
			    {
			        $sql = "DELETE FROM tbl_notification_group_log_new WHERE logidx = $notiidx;";
			        $sql .= "INSERT IGNORE INTO tbl_a2u_user_delete(useridx, writedate) VALUES($useridx, NOW());";
			        $db_main->execute($sql);
			    }
			    else
			    {
			        $str_message = explode(".", $e->getMessage());
			        $not_allowed_message = explode(")", $e->getMessage());
			        
			        if($str_message[0] == "Unsupported post request" || $not_allowed_message[0] == "(#100")
			        {
			            $sql = "DELETE FROM tbl_notification_group_log_new WHERE logidx = $notiidx;";
			            $sql .= "INSERT IGNORE INTO tbl_a2u_user_delete(useridx, writedate) VALUES($useridx, NOW());";
			            $db_main->execute($sql);
			            
			            if($not_allowed_message[0] == "(#100")
			            {
			                write_log("group6 - useridx: ".$useridx." / ".$e->getMessage()." / ".$template);
			            }
			        }
			        else
			        {
			            write_log("group6 - ".$e->getMessage());
			        }
			    }
			}
		}
		
		write_log("A2U Group 6 Send Count : ".$noti_cnt);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();		
?>
