<?
	include("../../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();

	$db_main->execute("SET wait_timeout=7200");
	$db_main2->execute("SET wait_timeout=7200");

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/game_request_group1_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
		exit();
	
	$facebook = new Facebook(array(
	    'appId'  => FACEBOOK_APP_ID,
	    'secret' => FACEBOOK_SECRET_KEY,
	    'cookie' => true,
	));
		
	function utf8_bytes($cp)
	{
	    if ($cp > 0x10000){
	        # 4 bytes
	        return	chr(0xF0 | (($cp & 0x1C0000) >> 18)).
	        chr(0x80 | (($cp & 0x3F000) >> 12)).
	        chr(0x80 | (($cp & 0xFC0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else if ($cp > 0x800){
	        # 3 bytes
	        return	chr(0xE0 | (($cp & 0xF000) >> 12)).
	        chr(0x80 | (($cp & 0xFC0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else if ($cp > 0x80){
	        # 2 bytes
	        return	chr(0xC0 | (($cp & 0x7C0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else{
	        # 1 byte
	        return chr($cp);
	    }
	}
		
	try
	{
	    $noti_cnt = 0;
	    $category = 1;
	    
	    $bonus_coin = 100000;
	    
		//신규 가입자(10만코인 리워드, 총 7회 노티발송)
		$sql = "SELECT t1.useridx, userid, t1.createdate, t1.logindate, DATEDIFF(NOW(), t1.createdate) AS dayafterinstall
                FROM tbl_user t1 JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx
                WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d 00:00:00') <= t1.createdate AND t1.createdate <= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d 23:59:59') 
                	AND HOUR(t1.createdate) = HOUR(NOW()) AND platform = 0
                	AND NOT EXISTS(SELECT useridx FROM tbl_a2u_user_delete WHERE useridx = t1.useridx)
                    AND userid > 0
                	AND NOT EXISTS(SELECT useridx FROM tbl_notification_group_log_new WHERE useridx = t1.useridx AND group_no = 1 AND DATE(senddate) = DATE(NOW()))";
		$req_user = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($req_user); $i++)
		{
			$useridx = $req_user[$i]["useridx"];
			$facebookid = $req_user[$i]["userid"];
			$leftcount = $req_user[$i]["dayafterinstall"];
			
			try
			{
				$sql = "INSERT INTO tbl_user_newuser_gift_log(useridx, bonus_coin, type, platform, senddate) VALUES($useridx, $bonus_coin, 1, 0, now())";
				$db_main2->execute($sql);
				
				$sql = "INSERT INTO tbl_notification_group_log_new (useridx, platform, group_no, type, bonus_coin, senddate) VALUES($useridx, 0, $category, 1, $bonus_coin, NOW());";
				$db_main->execute($sql);
				
				$sql = "SELECT LAST_INSERT_ID()";
				$notiidx = $db_main2->getvalue($sql);
				
				$date_flag = $leftcount - 1;
				
				$day1 = " Welcome @[$facebookid]! ".utf8_bytes(0x1F609)." Take your First Spin with 100,000 Bonus Coins at Take5 Slots!".utf8_bytes(0x1F4B8);
				$day2 = "Hi @[$facebookid], ".utf8_bytes(0x1F4B0)." Claim 100,000 Coins and Check Out our Stunning Slots!";
				$day3 = "Hello @[$facebookid], ".utf8_bytes(0x1F381)." Here is Today`s Newcomer Bonus! Come & Get it NOW!".utf8_bytes(0x1F446);
				$day4 = "Hi @[$facebookid], We`ve just sent you some FREE Coins for another new day at Take5!";
				$day5 = "Hi @[$facebookid], ".utf8_bytes(0x1F609)." Having Fun with Take5 So Far? Collect some FREE Coins & Enjoy the Slots!".utf8_bytes(0x1F381);
				$day6 = "NEW Slots are always FREE for our newbie players in Take5! ".utf8_bytes(0x1F923)." All you need is just one Click away! ".utf8_bytes(0x1F446);
				$day7 = "@[$facebookid], ".utf8_bytes(0x1F446)." Click & Enjoy 100,000 Bonus Coins! Beginner`s Luck should never end!".utf8_bytes(0x1F340);
				
				$daily_template = array($day1, $day2, $day3, $day4, $day5, $day6, $day7);
				
				$template = $daily_template[$date_flag];
				
				$args = array('template' => "$template",
								'href' => "?adflag=gamenotifygroup1&notiidx=$notiidx",
								'ref' => "group_base_group1");
					
				$info = $facebook->api("/$facebookid/notifications", "POST", $args);
				
				$noti_cnt++;
			}
			catch (FacebookApiException $e)
			{
			    if($e->getMessage() == "Unsupported operation" || $e->getMessage() == "(#200) Cannot send notifications to a user who has not installed the app")
			    {
			        $sql = "DELETE FROM tbl_notification_group_log_new WHERE logidx = $notiidx;";
			        $sql .= "INSERT IGNORE INTO tbl_a2u_user_delete(useridx, writedate) VALUES($useridx, NOW());";
			        $db_main->execute($sql);
			    }
			    else
			    {
			        $str_message = explode(".", $e->getMessage());
			        $not_allowed_message = explode(")", $e->getMessage());
			        
			        if($str_message[0] == "Unsupported post request" || $not_allowed_message[0] == "(#100")
			        {
			            $sql = "DELETE FROM tbl_notification_group_log_new WHERE logidx = $notiidx;";
			            $sql .= "INSERT IGNORE INTO tbl_a2u_user_delete(useridx, writedate) VALUES($useridx, NOW());";
			            $db_main->execute($sql);
			            
			            if($not_allowed_message[0] == "(#100")
			            {
			                write_log("group1 - useridx: ".$useridx." / ".$e->getMessage()." / ".$template);
			            }
			        }
			        else
			        {
			            write_log("group1 - ".$e->getMessage());
			        }
			    }
			}
		}	
		
		write_log("A2U Group 1 Send Count : ".$noti_cnt);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();
?>