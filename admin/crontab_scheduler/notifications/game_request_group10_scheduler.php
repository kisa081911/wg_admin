<?
	include("../../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();

	$db_main->execute("SET wait_timeout=7200");
	$db_main2->execute("SET wait_timeout=7200");

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/notifications/game_request_group10_scheduler") !== false)
		{
			$count++;
		}
	}
	
	$facebook = new Facebook(array(
	    'appId'  => FACEBOOK_APP_ID,
	    'secret' => FACEBOOK_SECRET_KEY,
	    'cookie' => true,
	));
	
	function utf8_bytes($cp)
	{
	    if ($cp > 0x10000){
	        # 4 bytes
	        return	chr(0xF0 | (($cp & 0x1C0000) >> 18)).
	        chr(0x80 | (($cp & 0x3F000) >> 12)).
	        chr(0x80 | (($cp & 0xFC0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else if ($cp > 0x800){
	        # 3 bytes
	        return	chr(0xE0 | (($cp & 0xF000) >> 12)).
	        chr(0x80 | (($cp & 0xFC0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else if ($cp > 0x80){
	        # 2 bytes
	        return	chr(0xC0 | (($cp & 0x7C0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else{
	        # 1 byte
	        return chr($cp);
	    }
	}
	
	if ($count > 1)
		exit();
	
	try
	{
	    $noti_cnt = 0;
	    $category = 10;
	    
		$sql = "SELECT useridx, userid
                FROM `tbl_user_firstbuy_gift`
                WHERE leftcount > 0 AND sendmode = 1 AND platform = 0 AND CHAR_LENGTH(userid) > 10
		AND userid > 0
                	AND (senddate < DATE_SUB(NOW(), INTERVAL 1 DAY) OR senddate = '0000-00-00 00:00:00') AND purchasedate >= DATE_SUB(NOW(), INTERVAL 1 WEEK) 
                    AND HOUR(purchasedate) = HOUR(NOW())";		
		$req_user = $db_main2->gettotallist($sql);
		
		for($i=0; $i<sizeof($req_user); $i++)
		{
			$useridx = $req_user[$i]["useridx"];
			$facebookid = $req_user[$i]["userid"];
			
			try
			{
			    $sql = "SELECT COUNT(DISTINCT useridx)
                        FROM (
                        	SELECT useridx 
                        	FROM tbl_notification_group_log_new 
                        	WHERE useridx = $useridx AND group_no = 10 AND DATE(senddate) = DATE(NOW())
                        	UNION ALL
                        	SELECT useridx
                        	FROM tbl_a2u_user_delete
                        	WHERE useridx = $useridx
                        ) t1";
				$delete_user_check = $db_main->getvalue($sql);
				
				if($delete_user_check == 0)
				{				
				    $sql = "INSERT INTO tbl_notification_group_log_new(useridx, platform, group_no, type, bonus_coin, senddate) VALUES($useridx, 0, $category, 1, 0, NOW());";
				    $db_main->execute($sql);
				    
				    $sql = "SELECT LAST_INSERT_ID()";
				    $notiidx = $db_main->getvalue($sql);

					$day1 = utf8_bytes(0x1F38A)."What a surprise! Extra Daily Bonus is now available! Check out your Gift Box!".utf8_bytes(0x1F381);
					$day2 = utf8_bytes(0x1F4EE)."Extra Daily Bonus has been sent to your Gift Box!".utf8_bytes(0x1F381)." Collect the Bonus and PLAY NOW!";
					$day3 = utf8_bytes(0x1F389)."No time to wait! Collect your Extra Daily Bonus and let it spin!".utf8_bytes(0x1F3B0);
					
					$daily_template = array($day1, $day2, $day3);
					
					$date_flag = rand(0, 2);
					
					$template = $daily_template[$date_flag];
					
					$args = array('template' => "$template",
									'href' => "?adflag=gamenotifygroup10&notiidx=$notiidx",
									'ref' => "group_base_group10");
						
					$info = $facebook->api("/$facebookid/notifications", "POST", $args);
					
					$noti_cnt++;
				}
				else
				{
					$sql = "UPDATE tbl_user_firstbuy_gift SET leftcount = 0 WHERE useridx = $useridx";
					$db_main2->execute($sql);
				}
			}
			catch (FacebookApiException $e)
			{
			    if($e->getMessage() == "Unsupported operation" || $e->getMessage() == "(#200) Cannot send notifications to a user who has not installed the app")
			    {
			        $sql = "DELETE FROM tbl_notification_group_log_new WHERE logidx = $notiidx;";
			        $sql .= "INSERT IGNORE INTO tbl_a2u_user_delete(useridx, writedate) VALUES($useridx, NOW());";
			        $db_main->execute($sql);
			    }
			    else
			    {
			        $str_message = explode(".", $e->getMessage());
			        $not_allowed_message = explode(")", $e->getMessage());
			        
			        if($str_message[0] == "Unsupported post request" || $not_allowed_message[0] == "(#100")
			        {
			            $sql = "DELETE FROM tbl_notification_group_log_new WHERE logidx = $notiidx;";
			            $sql .= "INSERT IGNORE INTO tbl_a2u_user_delete(useridx, writedate) VALUES($useridx, NOW());";
			            $db_main->execute($sql);
			            
			            if($not_allowed_message[0] == "(#100")
			            {
			                write_log("group10 - useridx: ".$useridx." / ".$e->getMessage()." / ".$template);
			            }
			        }
			        else
			        {
			            write_log("group10 -".$e->getMessage());
			        }
			    }
			}
		}		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();
?>