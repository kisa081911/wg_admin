<?
    include("../../common/common_include.inc.php");
	
	/*
	 * A2U Group 11
	 * 조건 : 7일 이상 14일 미만 이탈 그룹 결제자
	 * 발송 기준 시각 : 서버시각 15:00
	 * 발송 주기 및 노티 수신량 : 1회 발송
	 * */
	
	$db_main = new CDatabase_Main();

	$db_main->execute("SET wait_timeout=7200");	

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/notifications/game_request_group11_scheduler") !== false)
		{
			$count++;
		}
	}

	if ($count > 1)
	{
		$count = 0;
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);

		if (!$fp) 
		{
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/notifications/game_request_group11_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("game request group11 scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	$facebook = new Facebook(array(
	    'appId'  => FACEBOOK_APP_ID,
	    'secret' => FACEBOOK_SECRET_KEY,
	    'cookie' => true,
	));
	
	function utf8_bytes($cp)
	{
	    if ($cp > 0x10000){
	        # 4 bytes
	        return	chr(0xF0 | (($cp & 0x1C0000) >> 18)).
	        chr(0x80 | (($cp & 0x3F000) >> 12)).
	        chr(0x80 | (($cp & 0xFC0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else if ($cp > 0x800){
	        # 3 bytes
	        return	chr(0xE0 | (($cp & 0xF000) >> 12)).
	        chr(0x80 | (($cp & 0xFC0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else if ($cp > 0x80){
	        # 2 bytes
	        return	chr(0xC0 | (($cp & 0x7C0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else{
	        # 1 byte
	        return chr($cp);
	    }
	}
	
	try
	{	
		$noti_cnt = 0;
		$category = 11;
		
		$sql = "SELECT t1.useridx, userid, vip_point
                FROM tbl_user t1 JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx
                WHERE DATE_SUB(NOW(), INTERVAL 2 WEEK) <= logindate AND logindate < DATE_SUB(NOW(), INTERVAL 1 WEEK) AND HOUR(logindate) = HOUR(NOW())
                	AND vip_point > 0 AND CHAR_LENGTH(userid) > 10
                    AND userid > 0
                	AND NOT EXISTS(SELECT useridx FROM tbl_a2u_user_delete WHERE useridx = t1.useridx)
                	AND NOT EXISTS(SELECT useridx FROM tbl_notification_group_log_new WHERE useridx = t1.useridx AND group_no = 11 AND senddate > DATE_SUB(NOW(), INTERVAL 1 WEEK))";
		$req_user = $db_main->gettotallist($sql);

		for($i=0; $i<sizeof($req_user); $i++)
		{
		    $useridx = $req_user[$i]["useridx"];
		    $facebookid = $req_user[$i]["userid"];
		    $vip_point = $req_user[$i]["vip_point"];
		    
	        if($vip_point >= 1 && $vip_point <= 38)
	        {
		        $reward = 500000;
	        }
	        else if($vip_point >= 39 && $vip_point <= 98)
	        {
	            $reward = 1500000;
	        }
	        else if($vip_point >= 99 && $vip_point <= 498)
	        {
	            $reward = 2000000;
	        }
	        else if($vip_point >= 499)
	        {
	            $reward = 3500000;
	        }
		        
	        $day1 = "@[$facebookid],".utf8_bytes(0x1F4B0)." Click to collect an awesome GIFT & enjoy spinning! ".utf8_bytes(0x1F3B0);
	        $day2 = "@[$facebookid],".utf8_bytes(0x1F60D)." Do You Love FREE COINS? ".utf8_bytes(0x1F4B2)." COLLECT NOW! ".utf8_bytes(0x1F618);
	        $day3 = utf8_bytes(0x1F611)." Having a boring week? This special GIFT & Take5 Slots will spice it up! ".utf8_bytes(0x1F606);
	        $day4 = "@[$facebookid], I've prepared a special treat! ".utf8_bytes(0x1F60D)." Check it Out! ".utf8_bytes(0x2705);
	        $day5 = "Want some FREE COINS to spin? ".utf8_bytes(0x1F929)." All you need is just one CLICK! ".utf8_bytes(0x1F446)."".utf8_bytes(0x1F381);
	        
	        $daily_template = array($day1, $day2, $day3, $day4, $day5);
	        
	        $date_flag = rand(0, 4);
	        
	        $template = $daily_template[$date_flag];
	        
	        $adflag = "gamenotifygroup11";
	        
	        $sql = "INSERT INTO tbl_notification_group_log_new(useridx, platform, group_no, type, bonus_coin, senddate) VALUES($useridx, 0, $category, 1, $reward, NOW());";
	        $db_main->execute($sql);
	        
	        $sql = "SELECT LAST_INSERT_ID()";
	        $notiidx = $db_main->getvalue($sql);
	        
	        try
	        {
	            $args = array('template' => "$template",
	                'href' => "?adflag=$adflag&notiidx=$notiidx",
	                'ref' => "group_base_group11");
	            
	            $info = $facebook->api("/$facebookid/notifications", "POST", $args);
	            
	            $noti_cnt++;
	        }
	        catch (FacebookApiException $e)
	        {
	            if($e->getMessage() == "Unsupported operation" || $e->getMessage() == "(#200) Cannot send notifications to a user who has not installed the app")
	            {
	                $sql = "DELETE FROM tbl_notification_group_log_new WHERE logidx = $notiidx;";
	                $sql .= "INSERT IGNORE INTO tbl_a2u_user_delete(useridx, writedate) VALUES($useridx, NOW());";
	                $db_main->execute($sql);
	            }
	            else
	            {
	                $str_message = explode(".", $e->getMessage());
	                $not_allowed_message = explode(")", $e->getMessage());
	                
	                if($str_message[0] == "Unsupported post request" || $not_allowed_message[0] == "(#100")
	                {
	                    $sql = "DELETE FROM tbl_notification_group_log_new WHERE logidx = $notiidx;";
	                    $sql .= "INSERT IGNORE INTO tbl_a2u_user_delete(useridx, writedate) VALUES($useridx, NOW());";
	                    $db_main->execute($sql);
	                    
	                    if($not_allowed_message[0] == "(#100")
	                    {
	                        write_log("group11 - useridx: ".$useridx." / ".$e->getMessage()." / ".$template);
	                    }
	                }
	                else
	                {
	                    write_log("group11 - ".$e->getMessage());
	                }
	            }
	        }
	    }
		
		write_log("A2U Group 11 Send Count : ".$noti_cnt);
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	}
	
	$db_main->end();	
?>
