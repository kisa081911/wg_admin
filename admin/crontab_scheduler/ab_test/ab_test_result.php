<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$db_redshift = new CDatabase_Redshift();
	$db_other = new CDatabase_Other();
	
	$db_other->execute("SET wait_timeout=3600");
	
	$ab_markingidx = array(33,34);
	
	for($ab = 0; $ab < sizeof($ab_markingidx); $ab++)
	{
		$markingidx = $ab_markingidx[$ab];
		
		write_log($markingidx);
		
		if($markingidx == 33 || $markingidx == 34)
			$date = "t1.logindate";
		else
			$date = "t1.actiondate";			
	
		//Total
		try
		{			
			$sql = "select markingidx, is_test, count(distinct useridx) as cnt_user, sum(before_4week_money) as sum_before_4week_money, sum(after_4week_money) as sum_after_4week_money,
	        			sum(after_8week_money) as sum_after_8week_money, sum(after_12week_money) as sum_after_12week_money, sum(after_16week_money) as sum_after_16week_money
					from
					(
					  select markingidx, useridx, is_test, min($date),
					  (select nvl(sum(money), 0) :: float from t5_product_order_all where useridx = t1.useridx and status = 1 and  dateadd(w, -4, min($date)) <= writedate and writedate < min($date)) as before_4week_money,
					  (select nvl(sum(money), 0) :: float from t5_product_order_all where useridx = t1.useridx and status = 1 and  min($date) <= writedate and writedate < dateadd(w, 4, min($date))) as after_4week_money,
					  (select nvl(sum(money), 0) :: float from t5_product_order_all where useridx = t1.useridx and status = 1 and  dateadd(w, 4, min($date)) <= writedate and writedate < dateadd(w, 8, min($date))) as after_8week_money,
					  (select nvl(sum(money), 0) :: float from t5_product_order_all where useridx = t1.useridx and status = 1 and  dateadd(w, 8, min($date)) <= writedate and writedate < dateadd(w, 12, min($date))) as after_12week_money,
					  (select nvl(sum(money), 0) :: float from t5_product_order_all where useridx = t1.useridx and status = 1 and  dateadd(w, 12, min($date)) <= writedate and writedate < dateadd(w, 16, min($date))) as after_16week_money
					  from t5_user_marking_log t1
					  where markingidx = $markingidx and useridx > 20000 and useridx not in(select useridx from t5_user_whitelist)
					  group by useridx, markingidx, is_test
					)
					group by markingidx, is_test;";
			$ab_list = $db_redshift->gettotallist($sql);
			
			$insert_sql = "";
			
			for($i=0; $i<sizeof($ab_list); $i++)
			{
				$markingidx = $ab_list[$i]["markingidx"];
				$is_test = $ab_list[$i]["is_test"];
				$cnt_user = $ab_list[$i]["cnt_user"];
				$sum_before_4week_money = $ab_list[$i]["sum_before_4week_money"];
				$sum_after_4week_money = $ab_list[$i]["sum_after_4week_money"];
				$sum_after_8week_money = $ab_list[$i]["sum_after_8week_money"];
				$sum_after_12week_money = $ab_list[$i]["sum_after_12week_money"];
				$sum_after_16week_money = $ab_list[$i]["sum_after_16week_money"];
				
				if($insert_sql == "")
					$insert_sql = "INSERT INTO tbl_ab_test_list(markingidx, type, is_test, sample, se, before_4week_money, after_4week_money, after_8week_money, after_12week_money, after_16week_money, writedate) VALUES($markingidx, 0, $is_test, $cnt_user, 0, $sum_before_4week_money, $sum_after_4week_money, $sum_after_8week_money, $sum_after_12week_money, $sum_after_16week_money, NOW())";
				else
					$insert_sql .= ",($markingidx, 0, $is_test, $cnt_user, 0, $sum_before_4week_money, $sum_after_4week_money, $sum_after_8week_money, $sum_after_12week_money, $sum_after_16week_money, NOW())";
			}
			
			if($insert_sql != "")
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE sample = VALUES(sample), se = VALUES(se), before_4week_money = VALUES(before_4week_money), after_4week_money = VALUES(after_4week_money), after_8week_money = VALUES(after_8week_money), after_12week_money = VALUES(after_12week_money), after_16week_money = VALUES(after_16week_money), writedate = VALUES(writedate);";
				$db_other->execute($insert_sql);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		try
		{
			$sql = "SELECT markingidx, SQRT(123100609/((b_before_4week_money+a_before_4week_money)/2)) AS se, (b_before_4week_money/a_before_4week_money) -1 AS change_rate
					FROM
					(
						SELECT markingidx, SUM(a_before_4week_money) AS a_before_4week_money, SUM(b_before_4week_money) AS b_before_4week_money
						FROM
						(
							SELECT markingidx, IF(is_test = 0, before_4week_money, 0) AS a_before_4week_money, IF(is_test = 1, before_4week_money, 0) AS b_before_4week_money
							FROM tbl_ab_test_list WHERE markingidx = $markingidx AND type = 0
						) ta 
						GROUP BY markingidx
					) tb
					GROUP BY markingidx;;";
			$se_formula = $db_other->gettotallist($sql);
			
			for($i=0; $i<sizeof($se_formula); $i++)
			{
				$markingidx = $se_formula[$i]["markingidx"];
				$se = $se_formula[$i]["se"];
				$change_rate = $se_formula[$i]["change_rate"];
				
				if($se == "")
					$se = 0;
				
				if($change_rate == "")
					$change_rate = 0;
				
				$update_sql = "UPDATE tbl_ab_test_list SET se = $se, change_rate = $change_rate WHERE markingidx = $markingidx AND type = 0";
				$db_other->execute($update_sql); 
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		//고액제외(Ratio >= 5)
		try
		{
			$sql = "select markingidx, is_test, count(distinct useridx) as cnt_user,  sum(before_4week_money) as sum_before_4week_money, sum(after_4week_money) as sum_after_4week_money,
	        		sum(after_8week_money) as sum_after_8week_money, sum(after_12week_money) as sum_after_12week_money, sum(after_16week_money) as sum_after_16week_money
	                from
	                (
	                    select *, trunc(cum/total::float*100,0) as ratio 
	                    from
	                    ( 
	                        select *,sum(max28) over(partition by grp) as total, sum(max28) over(partition by grp order by max28 desc rows unbounded preceding) as cum from
	                        (
	                              select *, case when before_4week_money >= after_4week_money then before_4week_money else after_4week_money end as max28 
	                              from
	                              (
		                                select markingidx, useridx, is_test, min($date), case when useridx > 0 then 1 else 0 end as grp, 
		                                (select nvl(sum(money), 0) :: float from t5_product_order_all where useridx = t1.useridx and status = 1 and  dateadd(w, -4, min($date)) <= writedate and writedate < min($date)) as before_4week_money,
		                                (select nvl(sum(money), 0) :: float from t5_product_order_all where useridx = t1.useridx and status = 1 and  min($date) <= writedate and writedate < dateadd(w, 4, min($date))) as after_4week_money,
		                                (select nvl(sum(money), 0) :: float from t5_product_order_all where useridx = t1.useridx and status = 1 and  dateadd(w, 4, min($date)) <= writedate and writedate < dateadd(w, 8, min($date))) as after_8week_money,
		                                (select nvl(sum(money), 0) :: float from t5_product_order_all where useridx = t1.useridx and status = 1 and  dateadd(w, 8, min($date)) <= writedate and writedate < dateadd(w, 12, min($date))) as after_12week_money,
		                                (select nvl(sum(money), 0) :: float from t5_product_order_all where useridx = t1.useridx and status = 1 and  dateadd(w, 12, min($date)) <= writedate and writedate < dateadd(w, 16, min($date))) as after_16week_money
		                                from t5_user_marking_log t1
		                                where markingidx = $markingidx and useridx > 10090 and useridx not in(select useridx from t5_user_whitelist)
		                                group by useridx, markingidx, is_test
	                              )
							)
						)
					)
	                where ratio >= 5
	                group by markingidx, is_test";
			$ab_list = $db_redshift->gettotallist($sql);
		
			$insert_sql = "";
		
			for($i=0; $i<sizeof($ab_list); $i++)
			{
				$markingidx = $ab_list[$i]["markingidx"];
				$is_test = $ab_list[$i]["is_test"];
				$cnt_user = $ab_list[$i]["cnt_user"];
				$sum_before_4week_money = $ab_list[$i]["sum_before_4week_money"];
				$sum_after_4week_money = $ab_list[$i]["sum_after_4week_money"];
				$sum_after_8week_money = $ab_list[$i]["sum_after_8week_money"];
				$sum_after_12week_money = $ab_list[$i]["sum_after_12week_money"];
				$sum_after_16week_money = $ab_list[$i]["sum_after_16week_money"];
					
				if($insert_sql == "")
					$insert_sql = "INSERT INTO tbl_ab_test_list(markingidx, type, is_test,  sample, se, before_4week_money, after_4week_money, after_8week_money, after_12week_money, after_16week_money, writedate) VALUES($markingidx, 1, $is_test, $cnt_user, 0, $sum_before_4week_money, $sum_after_4week_money, $sum_after_8week_money, $sum_after_12week_money, $sum_after_16week_money, NOW())";
				else
					$insert_sql .= ",($markingidx, 1, $is_test, $cnt_user, 0, $sum_before_4week_money, $sum_after_4week_money, $sum_after_8week_money, $sum_after_12week_money, $sum_after_16week_money, NOW())";
			}
		
			if($insert_sql != "")
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE sample = VALUES(sample), se = VALUES(se), before_4week_money = VALUES(before_4week_money), after_4week_money = VALUES(after_4week_money), after_8week_money = VALUES(after_8week_money), after_12week_money = VALUES(after_12week_money), after_16week_money = VALUES(after_16week_money), writedate = VALUES(writedate);";
				$db_other->execute($insert_sql);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		try
		{
			$sql = "SELECT markingidx, SQRT(123100609/((b_before_4week_money+a_before_4week_money)/2)) AS se, (b_before_4week_money/a_before_4week_money) -1 AS change_rate
					FROM
					(
						SELECT markingidx, SUM(a_before_4week_money) AS a_before_4week_money, SUM(b_before_4week_money) AS b_before_4week_money
						FROM
						(
							SELECT markingidx, IF(is_test = 0, before_4week_money, 0) AS a_before_4week_money, IF(is_test = 1, before_4week_money, 0) AS b_before_4week_money
							FROM tbl_ab_test_list WHERE markingidx = $markingidx AND type = 1
						) ta
						GROUP BY markingidx
					) tb
					GROUP BY markingidx;;";
			$se_formula = $db_other->gettotallist($sql);
		
			for($i=0; $i<sizeof($se_formula); $i++)
			{
				$markingidx = $se_formula[$i]["markingidx"];
				$se = $se_formula[$i]["se"];
				$change_rate = $se_formula[$i]["change_rate"];
				
				if($se == "")
					$se = 0;
				
				if($change_rate == "")
					$change_rate = 0;
					
				$update_sql = "UPDATE tbl_ab_test_list SET se = $se, change_rate = $change_rate WHERE markingidx = $markingidx AND type = 1";
				$db_other->execute($update_sql);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		//개선율
		try 
		{
			$sql = "SELECT markingidx, `type`, is_test, 
					IFNULL((after_4week_money/before_4week_money-1)*1+1, 0) * 100 AS i1, 
					IFNULL((after_8week_money/after_4week_money-1)*0.75+1, 0) * 100 AS i2, 
					IFNULL((after_12week_money/after_8week_money-1)*0.5+1, 0) * 100 AS i3, 
					IFNULL((after_16week_money/after_12week_money-1)*0.25+1, 0) * 100 AS i4
					FROM `tbl_ab_test_list`
					WHERE markingidx = $markingidx
					GROUP BY markingidx, `type`, is_test";	
			$improvement_data = $db_other->gettotallist($sql);
			
			$insert_sql = "";
			$ab_markingidx_i1_array = array();
			$ab_markingidx_i2_array = array();
			$ab_markingidx_i3_array = array();
			$ab_markingidx_i4_array = array();
			
			for($i=0; $i<sizeof($improvement_data); $i++)
			{
				$markingidx = $improvement_data[$i]["markingidx"];
				$type = $improvement_data[$i]["type"];
				$is_test = $improvement_data[$i]["is_test"];
				$i1 = $improvement_data[$i]["i1"];
				$i2 = $improvement_data[$i]["i2"];
				$i3 = $improvement_data[$i]["i3"];
				$i4 = $improvement_data[$i]["i4"];
				
				$ab_markingidx_i1_array[$markingidx][$type][$is_test] = $i1;
				$ab_markingidx_i2_array[$markingidx][$type][$is_test] = $i2;
				$ab_markingidx_i3_array[$markingidx][$type][$is_test] = $i3;
				$ab_markingidx_i4_array[$markingidx][$type][$is_test] = $i4;
				
				if($insert_sql == "")
					$insert_sql = "INSERT INTO tbl_ab_test_list(markingidx, type, is_test, i1, i2, i3, i4) VALUES($markingidx, $type, $is_test, $i1, $i2, $i3, $i4)";
				else
					$insert_sql .= ",($markingidx, $type, $is_test, $i1, $i2, $i3, $i4)";
			}
		
			if($insert_sql != "")
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE i1 = VALUES(i1), i2 = VALUES(i2), i3 = VALUES(i3), i4 = VALUES(i4);";
				$db_other->execute($insert_sql);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		//최종 개선율
		try
		{
			$a00 = $ab_markingidx_i1_array[$markingidx][0][0];
			$a01 = $ab_markingidx_i1_array[$markingidx][0][1];
			$a10 = $ab_markingidx_i1_array[$markingidx][1][0];
			$a11 = $ab_markingidx_i1_array[$markingidx][1][1];
			
			$b00 = $ab_markingidx_i2_array[$markingidx][0][0];
			$b01 = $ab_markingidx_i2_array[$markingidx][0][1];
			$b10 = $ab_markingidx_i2_array[$markingidx][1][0];
			$b11 = $ab_markingidx_i2_array[$markingidx][1][1];
			
			$c00 = $ab_markingidx_i3_array[$markingidx][0][0];
			$c01 = $ab_markingidx_i3_array[$markingidx][0][1];
			$c10 = $ab_markingidx_i3_array[$markingidx][1][0];
			$c11 = $ab_markingidx_i3_array[$markingidx][1][1];
			
			$d00 = $ab_markingidx_i4_array[$markingidx][0][0];
			$d01 = $ab_markingidx_i4_array[$markingidx][0][1];
			$d10 = $ab_markingidx_i4_array[$markingidx][1][0];
			$d11 = $ab_markingidx_i4_array[$markingidx][1][1];
			
			
			$t0_f_i1 = ($a00 == 0 ? 0:(($a01/$a00) - 1) * 100);
			$t1_f_i1 = ($a10 == 0 ? 0:(($a11/$a10) - 1) * 100);
			
			$t0_f_i2 = ($b00 == 0 ? 0:(($b01/$b00) - 1) * 100);
			$t1_f_i2 = ($b10 == 0 ? 0:(($b11/$b10) - 1) * 100);
	
			$t0_f_i3 = ($c00 == 0 ? 0:(($c01/$c00) - 1) * 100);
			$t1_f_i3 = ($c10 == 0 ? 0:(($c11/$c10) - 1) * 100);
	
			$t0_f_i4 = ($d00 == 0 ? 0:(($d01/$d00) - 1) * 100);
			$t1_f_i4 = ($d10 == 0 ? 0:(($d11/$d10) - 1) * 100);
			
			if($t0_f_i1 == "" || $t0_f_i1 == "NAN") $t0_f_i1 = 0;
			if($t1_f_i1 == "" || $t1_f_i1 == "NAN") $t1_f_i1 = 0;
			if($t0_f_i2 == "" || $t0_f_i2 == "NAN") $t0_f_i2 = 0;
			if($t1_f_i2 == "" || $t1_f_i2 == "NAN") $t1_f_i2 = 0;
			if($t0_f_i3 == "" || $t0_f_i3 == "NAN") $t0_f_i3 = 0;
			if($t1_f_i3 == "" || $t1_f_i3 == "NAN") $t1_f_i3 = 0;
			if($t0_f_i4 == "" || $t0_f_i4 == "NAN") $t0_f_i4 = 0;
			if($t1_f_i4 == "" || $t1_f_i4 == "NAN") $t1_f_i4 = 0;		
			
			$total_t0_f = $t0_f_i1 + $t0_f_i2 + $t0_f_i3 + $t0_f_i4;
			$total_t1_f = $t1_f_i1 + $t1_f_i2 + $t1_f_i3 + $t1_f_i4;
			
			$update_sql = "UPDATE tbl_ab_test_list SET f_i1 = $t0_f_i1, f_i2 = $t0_f_i2, f_i3 = $t0_f_i3, f_i4 = $t0_f_i4, final = $total_t0_f WHERE markingidx = $markingidx AND type = 0;";
			$update_sql .= "UPDATE tbl_ab_test_list SET f_i1 = $t1_f_i1, f_i2 = $t1_f_i2, f_i3 = $t1_f_i3, f_i4 = $t1_f_i4, final = $total_t1_f WHERE markingidx = $markingidx AND type = 1;";
			$db_other->execute($update_sql);
			
			
			
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		try
		{
			$sql = "SELECT markingidx, `type`, SUM(before_4week_money) * (final/100) AS earning
					FROM `tbl_ab_test_list`
					WHERE markingidx = $markingidx
					GROUP BY markingidx, `type`";
			$earning_data = $db_other->gettotallist($sql);
			
			
			for($i=0; $i<sizeof($earning_data); $i++)
			{
				$markingidx = $earning_data[$i]["markingidx"];
				$type = $earning_data[$i]["type"];
				$earning = $earning_data[$i]["earning"];
				
				$update_sql = "UPDATE tbl_ab_test_list SET earning = $earning WHERE markingidx = $markingidx and type = $type;";
				$db_other->execute($update_sql);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		write_log($markingidx);
	}
	
	
	$db_redshift->end();
	$db_other->end();
?>
