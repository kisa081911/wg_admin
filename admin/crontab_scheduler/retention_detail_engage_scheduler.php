<?
	include("../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();
	$db_livestats = new CDatabase_Livestats();
	$db_other = new CDatabase_Other();
	
	$db_main->execute("SET wait_timeout=72000");
	$db_analysis->execute("SET wait_timeout=72000");
	$db_livestats->execute("SET wait_timeout=72000");
	$db_other->execute("SET wait_timeout=72000");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$port = "";
	
	$str_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}

	try
	{
		$sql = "INSERT INTO tbl_scheduler_log(today, type, status, writedate) VALUES('$today', 403, 0, NOW());";
		$db_analysis->execute($sql);
		$sql = "SELECT LAST_INSERT_ID();";
		$logidx = $db_analysis->getvalue($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/retention_detail_engage_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/retention_detail_engage_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("retention_engage_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}

	// 일별 통계
	try 
	{
		$today = date("Y-m-d", time() - 60 * 60 * 24);

		$sql = "SELECT createdate, category, adflag, after_day_install, SUM(total_cnt) AS total_cnt ".
				"FROM ( ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_0 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category,  adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_1 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90) ". 
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate,category,  adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_2 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_3 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_4 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_5 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_6 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_7 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_8 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_9 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				") t1 ".
				"GROUP BY createdate, category, adflag, after_day_install ".
				"ORDER BY createdate ASC, after_day_install ASC, category ASC, adflag ASC";

		$retention_stat_list = $db_livestats->gettotallist($sql);
		
		for($i=0; $i<sizeof($retention_stat_list); $i++)
		{
			$reg_date = $retention_stat_list[$i]["createdate"];
			$category = $retention_stat_list[$i]["category"];
			$adflag = $retention_stat_list[$i]["adflag"];
			$after_day_install = $retention_stat_list[$i]["after_day_install"];
			$total_cnt = $retention_stat_list[$i]["total_cnt"];

			if($after_day_install == 0)
			{
				$sql = "INSERT INTO tbl_user_detail_retention_daily(reg_date, platform, adflag, reg_count) ".
						"VALUES('$reg_date', $category, '$adflag', $total_cnt) ON DUPLICATE KEY UPDATE reg_count=VALUES(reg_count);";
				$sql .= "INSERT INTO tbl_user_detail_retention_daily_qa(reg_date, platform, adflag, reg_count) ".
						"VALUES('$reg_date', $category, '$adflag', $total_cnt) ON DUPLICATE KEY UPDATE reg_count=VALUES(reg_count);";
				$db_other->execute($sql);
			}
			else
			{
				$sql = "INSERT INTO tbl_user_detail_retention_daily(reg_date, platform, adflag, day_$after_day_install) ".
						"VALUES('$reg_date', $category, '$adflag', $total_cnt) ON DUPLICATE KEY UPDATE day_$after_day_install=VALUES(day_$after_day_install);";
				$sql .= "INSERT INTO tbl_user_detail_retention_daily_qa(reg_date, platform, adflag, day_$after_day_install) ".
						"VALUES('$reg_date', $category, '$adflag', $total_cnt) ON DUPLICATE KEY UPDATE day_$after_day_install=VALUES(day_$after_day_install);";
				$db_other->execute($sql);
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	// tbl_user 통합쿼리 넣어서  anaylsisDB 테이블 업데이트
	try
	{
		$index = 91;
		$indexMinus = 90;
		while(true){
			if($index < 1) {
				break;
			}
			$index = $index - 1;
			$indexMinus = $indexMinus - 1;
			
			$sql = "SELECT ".
					"	DATE_FORMAT(createdate, '%Y-%m-%d') AS createdate, ".
					"	platform, adflag, ".
					"	COUNT(*) AS total_count, ".
					"	ABS(IFNULL( ".
					"		SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=t1.useridx) THEN 1 ELSE 0 END) ".
					"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=t1.useridx) THEN 1 ELSE 0 END),0) ".
					"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=t1.useridx) THEN 1 ELSE 0 END),0) ".
					"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=t1.useridx) THEN 1 ELSE 0 END),0) ".
					"	,0)) AS play_count, ".
					"	SUM(INSTALL) AS install_count, ".			
					"	SUM(CASE WHEN tutorial >= 3 THEN 1 ELSE 0 END) AS tutorial_count, ".
					"	SUM(CASE WHEN daylogincount >= 3 THEN 1 ELSE 0 END) AS loyal_count ".
					"  FROM ".
					"  tbl_user_ext t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx ".
					"  WHERE t1.useridx > $str_useridx AND createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL '$index' DAY), '%Y-%m-%d') ".
					"  AND createdate < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL '$indexMinus' DAY), '%Y-%m-%d') ".
					"  GROUP BY DATE_FORMAT(createdate, '%Y-%m-%d'), platform, adflag";
			
			$retention_update_list = $db_main ->gettotallist($sql);

			for($i=0; $i<sizeof($retention_update_list); $i++)
			{
				$reg_date = $retention_update_list[$i]["createdate"];
				$platform = $retention_update_list[$i]["platform"];
				$adflag = $retention_update_list[$i]["adflag"];
				$total_count = $retention_update_list[$i]["total_count"];
				$play_count = $retention_update_list[$i]["play_count"];
				$install_count = $retention_update_list[$i]["install_count"];
				$tutorial_count = $retention_update_list[$i]["tutorial_count"];
				$loyal_count = $retention_update_list[$i]["loyal_count"];
					
				$sql = "INSERT INTO tbl_user_detail_retention_daily(reg_date, platform, adflag, reg_count, play_count, install_count, tutorial_count, loyal_count) ".
						" VALUES('$reg_date', $platform, '$adflag', $total_count, $play_count, $install_count, $tutorial_count, $loyal_count) ON DUPLICATE KEY UPDATE reg_count=VALUES(reg_count), play_count=VALUES(play_count), install_count=VALUES(install_count), tutorial_count=values(tutorial_count), loyal_count=values(loyal_count);";
				$sql .= "INSERT IGNORE INTO tbl_user_detail_retention_daily_qa(reg_date, platform, adflag, reg_count, play_count, install_count, tutorial_count, loyal_count) ".
						" VALUES('$reg_date', $platform, '$adflag', $total_count, $play_count, $install_count, $tutorial_count, $loyal_count);";
				$db_other->execute($sql);
			}
		}
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	// tbl_user 통합쿼리 넣어서  anaylsisDB_qa 테이블 업데이트
	try
	{
		$index = 1;
		$indexMinus = 0;
		
		$sql = "SELECT ".
				"	DATE_FORMAT(createdate, '%Y-%m-%d') AS createdate, ".
				"	platform, adflag, ".
				"	COUNT(*) AS total_count, ".
				"	ABS(IFNULL( ".
				"		SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=t1.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=t1.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=t1.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=t1.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0)) AS play_count, ".
				"	SUM(INSTALL) AS install_count, ".			
				"	SUM(CASE WHEN tutorial >= 3 THEN 1 ELSE 0 END) AS tutorial_count, ".
				"	SUM(CASE WHEN daylogincount >= 3 THEN 1 ELSE 0 END) AS loyal_count ".
				"  FROM ".
				"  tbl_user_ext t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx ".
				"  WHERE t1.useridx > $str_useridx AND createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL '$index' DAY), '%Y-%m-%d') ".
				"  AND createdate < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL '$indexMinus' DAY), '%Y-%m-%d') ".
				"  GROUP BY DATE_FORMAT(createdate, '%Y-%m-%d'), platform, adflag";
		
		$retention_update_list = $db_main ->gettotallist($sql);

		for($i=0; $i<sizeof($retention_update_list); $i++)
		{
			$reg_date = $retention_update_list[$i]["createdate"];
			$platform = $retention_update_list[$i]["platform"];
			$adflag = $retention_update_list[$i]["adflag"];
			$total_count = $retention_update_list[$i]["total_count"];
			$play_count = $retention_update_list[$i]["play_count"];
			$install_count = $retention_update_list[$i]["install_count"];
			$tutorial_count = $retention_update_list[$i]["tutorial_count"];
			$loyal_count = $retention_update_list[$i]["loyal_count"];
				
			$sql = "INSERT INTO tbl_user_detail_retention_daily_qa(reg_date, platform, adflag, reg_count, play_count, install_count, tutorial_count, loyal_count) ".
						" VALUES('$reg_date', $platform, '$adflag', $total_count, $play_count, $install_count, $tutorial_count, $loyal_count) ON DUPLICATE KEY UPDATE reg_count=VALUES(reg_count), play_count=VALUES(play_count), install_count=VALUES(install_count), tutorial_count=values(tutorial_count), loyal_count=values(loyal_count);";
			$db_other->execute($sql);
		}
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	// 첫 결제자 retention 통계
	try
	{
		$today = date("Y-m-d", time() - 60 * 60 * 24);
	
		$sql = "SELECT firstbuydate, category,  adflag, after_day_firstbuy, SUM(total_cnt) AS total_cnt
		FROM (
		SELECT firstbuydate, category,  adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
		FROM tbl_user_pay_retention_log_0
		WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY firstbuydate, category,  adflag, after_day_firstbuy
		UNION ALL
		SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
		FROM tbl_user_pay_retention_log_1
		WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY firstbuydate, category, adflag, after_day_firstbuy
		UNION ALL
		SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
		FROM tbl_user_pay_retention_log_2
		WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY firstbuydate, category, adflag, after_day_firstbuy
		UNION ALL
		SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
		FROM tbl_user_pay_retention_log_3
		WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY firstbuydate, category, adflag, after_day_firstbuy
		UNION ALL
		SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
		FROM tbl_user_pay_retention_log_4
		WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY firstbuydate, category, adflag, after_day_firstbuy
		UNION ALL
		SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
		FROM tbl_user_pay_retention_log_5
		WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY firstbuydate, category, adflag, after_day_firstbuy
		UNION ALL
		SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
		FROM tbl_user_pay_retention_log_6
		WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY firstbuydate, category, adflag, after_day_firstbuy
		UNION ALL
		SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
		FROM tbl_user_pay_retention_log_7
		WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY firstbuydate, category, adflag, after_day_firstbuy
		UNION ALL
		SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
		FROM tbl_user_pay_retention_log_8
		WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY firstbuydate, category, adflag, after_day_firstbuy
		UNION ALL
		SELECT firstbuydate, category, adflag, after_day_firstbuy, COUNT(useridx) AS total_cnt
		FROM tbl_user_pay_retention_log_9
		WHERE firstbuydate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_firstbuy IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY firstbuydate, category, adflag, after_day_firstbuy
		) t1
		GROUP BY firstbuydate, category, adflag, after_day_firstbuy
		ORDER BY firstbuydate ASC, after_day_firstbuy ASC, category ASC, adflag ASC";
	
		$pay_retention_stat_list = $db_livestats->gettotallist($sql);
	
		for($i=0; $i<sizeof($pay_retention_stat_list); $i++)
		{
			$firstbuydate = $pay_retention_stat_list[$i]["firstbuydate"];
			$category = $pay_retention_stat_list[$i]["category"];
			$adflag = $pay_retention_stat_list[$i]["adflag"];
			$after_day_firstbuy = $pay_retention_stat_list[$i]["after_day_firstbuy"];
			$total_cnt = $pay_retention_stat_list[$i]["total_cnt"];
	
			$sql = 	"INSERT INTO tbl_user_pay_retention_daily(firstbuydate, platform, adflag, day_$after_day_firstbuy) ".
					"VALUES('$firstbuydate', $category, '$adflag', $total_cnt) ON DUPLICATE KEY UPDATE day_$after_day_firstbuy=VALUES(day_$after_day_firstbuy);";
			$db_other->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	// 30일 이탈자 retention 통계
	try
	{
		$today = date("Y-m-d", time() - 60 * 60 * 24);
	
		$sql = "SELECT user_retention_write, category, adflag, count_after_retention, SUM(total_cnt) AS total_cnt
		FROM (
		SELECT user_retention_write, category, adflag, count_after_retention, COUNT(useridx) AS total_cnt
		FROM tbl_user_30day_retention_log_0
		WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY user_retention_write, category, adflag, count_after_retention
		UNION ALL
		SELECT user_retention_write, category, adflag, count_after_retention, COUNT(useridx) AS total_cnt
		FROM tbl_user_30day_retention_log_1
		WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY user_retention_write, category, adflag, count_after_retention
		UNION ALL
		SELECT user_retention_write, category, adflag, count_after_retention, COUNT(useridx) AS total_cnt
		FROM tbl_user_30day_retention_log_2
		WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY user_retention_write, category, adflag, count_after_retention
		UNION ALL
		SELECT user_retention_write, category, adflag, count_after_retention, COUNT(useridx) AS total_cnt
		FROM tbl_user_30day_retention_log_3
		WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY user_retention_write, category, adflag, count_after_retention
		UNION ALL
		SELECT user_retention_write, category, adflag, count_after_retention, COUNT(useridx) AS total_cnt
		FROM tbl_user_30day_retention_log_4
		WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY user_retention_write, category, adflag, count_after_retention
		UNION ALL
		SELECT user_retention_write, category, adflag, count_after_retention, COUNT(useridx) AS total_cnt
		FROM tbl_user_30day_retention_log_5
		WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY user_retention_write, category, adflag, count_after_retention
		UNION ALL
		SELECT user_retention_write, category, adflag, count_after_retention, COUNT(useridx) AS total_cnt
		FROM tbl_user_30day_retention_log_6
		WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY user_retention_write, category, adflag, count_after_retention
		UNION ALL
		SELECT user_retention_write, category, adflag, count_after_retention, COUNT(useridx) AS total_cnt
		FROM tbl_user_30day_retention_log_7
		WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY user_retention_write, category, adflag, count_after_retention
		UNION ALL
		SELECT user_retention_write, category, adflag, count_after_retention, COUNT(useridx) AS total_cnt
		FROM tbl_user_30day_retention_log_8
		WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY user_retention_write, category, adflag, count_after_retention
		UNION ALL
		SELECT user_retention_write, category, adflag, count_after_retention, COUNT(useridx) AS total_cnt
		FROM tbl_user_30day_retention_log_9
		WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
		GROUP BY user_retention_write, category, adflag, count_after_retention
		) t1
		GROUP BY user_retention_write, category, adflag, count_after_retention
		ORDER BY user_retention_write ASC, count_after_retention ASC,  category ASC, adflag ASC";
	
		$pay_retention_stat_list = $db_livestats->gettotallist($sql);
	
		for($i=0; $i<sizeof($pay_retention_stat_list); $i++)
		{
			$user_retention_write = $pay_retention_stat_list[$i]["user_retention_write"];
			$category = $pay_retention_stat_list[$i]["category"];
			$adflag = $pay_retention_stat_list[$i]["adflag"];
			$count_after_retention = $pay_retention_stat_list[$i]["count_after_retention"];
			$total_cnt = $pay_retention_stat_list[$i]["total_cnt"];
	
			$sql = 	"INSERT INTO tbl_user_30day_retention_daily(user_logindate, category, adflag, day_$count_after_retention) ".
				"VALUES('$user_retention_write', $category, '$adflag', $total_cnt) ON DUPLICATE KEY UPDATE day_$count_after_retention=VALUES(day_$count_after_retention);";
				$db_other->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}

	// 30일 이탈자 retention 통계_2
	try
	{
		$today = date("Y-m-d", time() - 60 * 60 * 24);
	
		$sql = "SELECT user_retention_write, category, retention_adflag, count_after_retention, SUM(total_cnt) AS total_cnt, SUM(sum_money) AS sum_money, SUM(cnt_money) AS cnt_money
				FROM (		
					SELECT user_retention_write, category, retention_adflag, count_after_retention, COUNT(useridx) AS total_cnt, IFNULL(SUM(money), 0) AS sum_money, SUM(IF(money>0,1,0)) AS cnt_money
					FROM tbl_user_30day_retention_log_0
					WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND useridx > 20000 AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
					GROUP BY user_retention_write, category, retention_adflag, count_after_retention
					UNION ALL
					SELECT user_retention_write, category, retention_adflag, count_after_retention, COUNT(useridx) AS total_cnt, IFNULL(SUM(money), 0) AS sum_money, SUM(IF(money>0,1,0)) AS cnt_money
					FROM tbl_user_30day_retention_log_1
					WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND useridx > 20000 AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
					GROUP BY user_retention_write, category, retention_adflag, count_after_retention
					UNION ALL
					SELECT user_retention_write, category, retention_adflag, count_after_retention, COUNT(useridx) AS total_cnt, IFNULL(SUM(money), 0) AS sum_money, SUM(IF(money>0,1,0)) AS cnt_money
					FROM tbl_user_30day_retention_log_2
					WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND useridx > 20000 AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
					GROUP BY user_retention_write, category, retention_adflag, count_after_retention
					UNION ALL
					SELECT user_retention_write, category, retention_adflag, count_after_retention, COUNT(useridx) AS total_cnt, IFNULL(SUM(money), 0) AS sum_money, SUM(IF(money>0,1,0)) AS cnt_money
					FROM tbl_user_30day_retention_log_3
					WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND useridx > 20000 AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
					GROUP BY user_retention_write, category, retention_adflag, count_after_retention
					UNION ALL
					SELECT user_retention_write, category, retention_adflag, count_after_retention, COUNT(useridx) AS total_cnt, IFNULL(SUM(money), 0) AS sum_money, SUM(IF(money>0,1,0)) AS cnt_money
					FROM tbl_user_30day_retention_log_4
					WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND useridx > 20000 AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
					GROUP BY user_retention_write, category, retention_adflag, count_after_retention
					UNION ALL
					SELECT user_retention_write, category, retention_adflag, count_after_retention, COUNT(useridx) AS total_cnt, IFNULL(SUM(money), 0) AS sum_money, SUM(IF(money>0,1,0)) AS cnt_money
					FROM tbl_user_30day_retention_log_5
					WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND useridx > 20000 AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
					GROUP BY user_retention_write, category, retention_adflag, count_after_retention
					UNION ALL
					SELECT user_retention_write, category, retention_adflag, count_after_retention, COUNT(useridx) AS total_cnt, IFNULL(SUM(money), 0) AS sum_money, SUM(IF(money>0,1,0)) AS cnt_money
					FROM tbl_user_30day_retention_log_6
					WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND useridx > 20000 AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
					GROUP BY user_retention_write, category, retention_adflag, count_after_retention
					UNION ALL
					SELECT user_retention_write, category, retention_adflag, count_after_retention, COUNT(useridx) AS total_cnt, IFNULL(SUM(money), 0) AS sum_money, SUM(IF(money>0,1,0)) AS cnt_money
					FROM tbl_user_30day_retention_log_7
					WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND useridx > 20000 AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
					GROUP BY user_retention_write, category, retention_adflag, count_after_retention		
					UNION ALL
					SELECT user_retention_write, category, retention_adflag, count_after_retention, COUNT(useridx) AS total_cnt, IFNULL(SUM(money), 0) AS sum_money, SUM(IF(money>0,1,0)) AS cnt_money
					FROM tbl_user_30day_retention_log_8
					WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND useridx > 20000 AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
					GROUP BY user_retention_write, category, retention_adflag, count_after_retention		
					UNION ALL
					SELECT user_retention_write, category, retention_adflag, count_after_retention, COUNT(useridx) AS total_cnt, IFNULL(SUM(money), 0) AS sum_money, SUM(IF(money>0,1,0)) AS cnt_money
					FROM tbl_user_30day_retention_log_9
					WHERE user_retention_write >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND useridx > 20000 AND today = '$today' AND count_after_retention IN (0, 1, 2, 3, 4, 5, 6, 7, 14, 28, 60, 90)
					GROUP BY user_retention_write, category, retention_adflag, count_after_retention	
				) t1
				GROUP BY user_retention_write, category, retention_adflag, count_after_retention
				ORDER BY user_retention_write ASC, count_after_retention ASC, category ASC, retention_adflag ASC";
	
		$pay_retention_stat_list = $db_livestats->gettotallist($sql);
	
		for($i=0; $i<sizeof($pay_retention_stat_list); $i++)
		{	
			$user_retention_write = $pay_retention_stat_list[$i]["user_retention_write"];
			$category = $pay_retention_stat_list[$i]["category"];
			$adflag = $pay_retention_stat_list[$i]["retention_adflag"];
			$count_after_retention = $pay_retention_stat_list[$i]["count_after_retention"];
			$total_cnt = $pay_retention_stat_list[$i]["total_cnt"];
			$sum_money = $pay_retention_stat_list[$i]["sum_money"];
			$cnt_money = $pay_retention_stat_list[$i]["cnt_money"];
			
			$sql = 	"INSERT INTO tbl_user_30day_retention_daily2(user_logindate, type, category, retention_adflag, pay_count, day_$count_after_retention) ".
					"VALUES('$user_retention_write', 1, $category, '$adflag', 0, $total_cnt) ON DUPLICATE KEY UPDATE day_$count_after_retention=VALUES(day_$count_after_retention), pay_count=VALUES(pay_count);";
			$db_other->execute($sql);
			
			$sql = 	"INSERT INTO tbl_user_30day_retention_daily2(user_logindate, type, category, retention_adflag, pay_count, day_$count_after_retention) ".
					"VALUES('$user_retention_write', 2, $category, '$adflag', $cnt_money, $sum_money) ON DUPLICATE KEY UPDATE day_$count_after_retention=VALUES(day_$count_after_retention), pay_count=pay_count+VALUES(pay_count);";
			$db_other->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	try
	{
		$sql = "UPDATE tbl_scheduler_log SET status = $issuccess, writedate = NOW() WHERE logidx = $logidx;";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_analysis->end();
	$db_livestats->end();
	$db_other->end();
?>