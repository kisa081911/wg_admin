<?
	include("../../common/common_include.inc.php");
    include_once('../../common/simple_html_dom.php');

    //android
    try
    {
    	$today = date("Y-m-d", strtotime("-3 day"));
		
		$db_main2 = new CDatabase_Main2();
		
		$sql = "SELECT COUNT(*) FROM tbl_iron_ad_revenue_measurement_user_level_etc WHERE platform = 2 AND  '$today 00:00:00' <= event_timestamp AND event_timestamp <= '$today 23:59:59'";
		$exist_count = $db_main2->getvalue($sql);
		
		if($exist_count == 0)
		{		
			//1. 파일 url 가져오기
			$authURL = 'https://platform.ironsrc.com/partners/publisher/auth';
			$authHeaders = array(
		 	        'secretkey: c2ad9b2a35facc9efc6384a7ec726f25',
		 	        'refreshToken: ba8cf6073ab37c2ff79e9d1db07af11f',
		 	);
		
			$curlClient = curl_init($authURL);
			curl_setopt($curlClient, CURLOPT_HTTPHEADER, $authHeaders);
			curl_setopt($curlClient, CURLOPT_RETURNTRANSFER, true);
			$bearerTokenResponse = curl_exec($curlClient);
			$bearerToken = str_replace('"','',$bearerTokenResponse);
			
			$ironsource_ch = curl_init();
			$header = array();
			$header[] = 'Authorization: Bearer '. $bearerToken;
			//$URL = 'https://platform.ironsrc.com/partners/adRevenueMeasurements/v1?appKey=e2d7d771&date='.$today;
			$URL = 'https://platform.ironsrc.com/partners/userAdRevenue/v3?appKey=e2d7d771&date='.$today.'&reportType=1';
			
			curl_setopt($ironsource_ch, CURLOPT_URL, $URL);
			curl_setopt($ironsource_ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ironsource_ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ironsource_ch, CURLOPT_RETURNTRANSFER, 1);
			
			$response = curl_exec($ironsource_ch);
		
			$ironsource_objs = json_decode($response);
			
			$ironsource_urls_array =  $ironsource_objs->urls;
			$ironrouce_urls = $ironsource_urls_array[0];
		
			curl_close($ironsource_ch);
			curl_close($curlClient);	
		
			//2. file 다운로드
			$src = "$ironrouce_urls";
			$targetFile = "/take5/html/crontab_scheduler/ironsource_revenue/report_and_userlevel_".$today.".csv.gz";
			$fp = fopen($targetFile, "w");
			$file_down_ch = curl_init();
			curl_setopt($file_down_ch, CURLOPT_URL, $src);
			curl_setopt($file_down_ch, CURLOPT_HEADER, 0);
			curl_setopt($file_down_ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($file_down_ch, CURLOPT_FILE, $fp);
			curl_exec($file_down_ch);
			fclose($fp);
			curl_close($file_down_ch);
			
			//3. file 압축풀기
			sleep(5);
			exec("gunzip report_and_userlevel_".$today.".csv.gz");
			
			//4. csv 파일 용해서 데이터 추출
			sleep(5);
			$handle = fopen("/take5/html/crontab_scheduler/ironsource_revenue/report_and_userlevel_".$today.".csv", "r");
			
			$insert_sql = "";
			
			while (($data = fgetcsv($handle)) !== FALSE) {
			  //$line is an array of the csv elements	
				if($data[1] == "advertising_id")
					continue;
					
				if($insert_sql == "")
					$insert_sql = "INSERT INTO tbl_iron_ad_revenue_measurement_user_level_etc(platform, ad_unit, advertising_id, advertising_id_type, user_id,  segment, placement,  ad_network,  AB_Testing, impression, revenue, event_timestamp, writedate) VALUES (2, '$data[0]', '$data[1]', '$data[2]', '$data[3]', '$data[4]', '$data[5]', '$data[6]', '$data[7]', '$data[8]', '$data[9]', '$today', NOW())";
				else
					$insert_sql .= ",(2, '$data[0]', '$data[1]', '$data[2]', '$data[3]', '$data[4]', '$data[5]', '$data[6]', '$data[7]', '$data[8]', '$data[9]', '$today', NOW())";
			}
			
			if($insert_sql != "")
				$db_main2->execute($insert_sql);
			
			fclose($handle);
			
		}
		
		$db_main2->end();			
    }
    catch(Exception $e)
    {
    	write_log($e->getMessage());
    }
    
    //ios
    try
    {
    	$today = date("Y-m-d", strtotime("-3 day"));
    
    	$db_main2 = new CDatabase_Main2();
    
    	$sql = "SELECT COUNT(*) FROM tbl_iron_ad_revenue_measurement_user_level_etc WHERE platform = 1 AND '$today 00:00:00' <= event_timestamp AND event_timestamp <= '$today 23:59:59'";
    	$exist_count = $db_main2->getvalue($sql);
    
    	if($exist_count == 0)
    	{
    		//1. 파일 url 가져오기
    		$authURL = 'https://platform.ironsrc.com/partners/publisher/auth';
    		$authHeaders = array(
    				'secretkey: c2ad9b2a35facc9efc6384a7ec726f25',
    				'refreshToken: ba8cf6073ab37c2ff79e9d1db07af11f',
    		);
    
    		$curlClient = curl_init($authURL);
    		curl_setopt($curlClient, CURLOPT_HTTPHEADER, $authHeaders);
    		curl_setopt($curlClient, CURLOPT_RETURNTRANSFER, true);
    		$bearerTokenResponse = curl_exec($curlClient);
    		$bearerToken = str_replace('"','',$bearerTokenResponse);
    			
    		$ironsource_ch = curl_init();
    		$header = array();
    		$header[] = 'Authorization: Bearer '. $bearerToken;
    		//$URL = 'https://platform.ironsrc.com/partners/adRevenueMeasurements/v1?appKey=e2d76461&date='.$today;
    		$URL = 'https://platform.ironsrc.com/partners/userAdRevenue/v3?appKey=e2d76461&date='.$today.'&reportType=1';
    			
    		curl_setopt($ironsource_ch, CURLOPT_URL, $URL);
    		curl_setopt($ironsource_ch, CURLOPT_HTTPHEADER, $header);
    		curl_setopt($ironsource_ch, CURLOPT_SSL_VERIFYPEER, false);
    		curl_setopt($ironsource_ch, CURLOPT_RETURNTRANSFER, 1);
    			
    		$response = curl_exec($ironsource_ch);
    
    		$ironsource_objs = json_decode($response);
    			
    		$ironsource_urls_array =  $ironsource_objs->urls;
    		$ironrouce_urls = $ironsource_urls_array[0];
    
    		curl_close($ironsource_ch);
    		curl_close($curlClient);
    
    		//2. file 다운로드
    		$src = "$ironrouce_urls";
    		$targetFile = "/take5/html/crontab_scheduler/ironsource_revenue/report_ios_userlevel_".$today.".csv.gz";
    		$fp = fopen($targetFile, "w");
    		$file_down_ch = curl_init();
    		curl_setopt($file_down_ch, CURLOPT_URL, $src);
    		curl_setopt($file_down_ch, CURLOPT_HEADER, 0);
    		curl_setopt($file_down_ch, CURLOPT_RETURNTRANSFER, 1);
    		curl_setopt($file_down_ch, CURLOPT_FILE, $fp);
    		curl_exec($file_down_ch);
    		fclose($fp);
    		curl_close($file_down_ch);
    			
    		//3. file 압축풀기
    		sleep(5);
    		exec("gunzip report_ios_userlevel_".$today.".csv.gz");
    			
    		//4. csv 파일 용해서 데이터 추출
    		sleep(5);    			
    		$handle = fopen("/take5/html/crontab_scheduler/ironsource_revenue/report_ios_userlevel_".$today.".csv", "r");
    			
    		$insert_sql = "";
    			
    		while (($data = fgetcsv($handle)) !== FALSE) {
    			//$line is an array of the csv elements
    			if($data[1] == "advertising_id")
    				continue;
    				
				if($insert_sql == "")
					$insert_sql = "INSERT INTO tbl_iron_ad_revenue_measurement_user_level_etc(platform, ad_unit, advertising_id, advertising_id_type, user_id,  segment, placement,  ad_network,  AB_Testing, impression, revenue, event_timestamp, writedate) VALUES (1, '$data[0]', '$data[1]', '$data[2]', '$data[3]', '$data[4]', '$data[5]', '$data[6]', '$data[7]', '$data[8]', '$data[9]', '$today', NOW())";
				else
					$insert_sql .= ",(1, '$data[0]', '$data[1]', '$data[2]', '$data[3]', '$data[4]', '$data[5]', '$data[6]', '$data[7]', '$data[8]', '$data[9]', '$today', NOW())";
    		}
    			
    		if($insert_sql != "")
    			$db_main2->execute($insert_sql);
    			
    		fclose($handle);
    			
    	}
    
    	$db_main2->end();
    }
    catch(Exception $e)
    {
    	write_log($e->getMessage());
    }
?>