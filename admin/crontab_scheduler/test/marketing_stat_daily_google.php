<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");

 	$db_main2 = new CDatabase_Main2();
 	$db_redshift = new CDatabase_Redshift();
 	
 	for($p=1;$p<6;$p++)
 	{
	 	$today = date("Y-m-d", strtotime("-$p days"));
	 	
	 	// 신규 유입자수 (모바일 광고)
    	$insert_sql = "";
    	
    	for($i=1; $i<=4; $i++)
    	{
    		$sql = "SELECT platform, adflag, nvl(COUNT(deviceid), 0) AS newuser, nvl(SUM(CASE WHEN experience > 0 THEN 0 ELSE 1 END), 0) AS newuser_noplay
    				FROM (
    					SELECT platform, adflag, deviceid, MAX(experience) AS experience
    					FROM t5_user
    					WHERE platform > 0 AND useridx > 20000
    					AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%')
    					AND to_char(dateadd(DAY,((-7 * $i)), '$today 00:00:00'::date), 'YYYY-MM-DD 00:00:00')  <= createdate AND createdate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
    					GROUP BY platform, adflag, deviceid
    				) t1
    				GROUP BY platform, adflag";
    		$newuser_cnt_list = $db_redshift->gettotallist($sql);
    		
    		for($j=0; $j<sizeof($newuser_cnt_list); $j++)
    		{
    			$platform = $newuser_cnt_list[$j]["platform"];
    			$adflag = $newuser_cnt_list[$j]["adflag"];
    			$newuser = $newuser_cnt_list[$j]["newuser"];
    			$newuser_noplay = $newuser_cnt_list[$j]["newuser_noplay"];
    			
    			if($insert_sql == "")
    				$insert_sql = "INSERT INTO tbl_marketing_stat_daily (today, type, subtype, platform, adflag, newuser, newuser_noplay) VALUES('$today', 1, $i, $platform, '$adflag', $newuser, $newuser_noplay)";
    			else
    				$insert_sql .= ",('$today', 1, $i, $platform, '$adflag', $newuser, $newuser_noplay)";
    		}
    	}
    
    	if($insert_sql != "")
    	{
    		$insert_sql .= "ON DUPLICATE KEY UPDATE newuser = VALUES(newuser), newuser_noplay = VALUES(newuser_noplay);";
    		$execute_sql .= $insert_sql;
    	}
    
    	// 신규 유입자 결제 정보 (모바일 광고)
    	$insert_sql = "";
    	
    	for($i=1; $i<=4; $i++)
    	{
    		$sql = "SELECT platform, adflag, nvl(COUNT(DISTINCT deviceid), 0) AS newuser_payer, nvl(SUM(money), 0) AS newuser_money
    				FROM (
    					SELECT t1.platform, t1.useridx, deviceid, adflag, (facebookcredit::float/10::float) AS money
    					FROM (
    						SELECT platform, useridx, deviceid, adflag, createdate
    						FROM t5_user
    						WHERE platform > 0 AND useridx > 20000
    						AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%')
    						AND to_char(dateadd(day,((-7 * $i)), '$today 00:00:00'::date), 'YYYY-MM-DD 00:00:00')  <= createdate AND createdate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
    					) t1 JOIN t5_product_order t2 ON t1.useridx = t2.useridx
    					WHERE status = 1 AND datediff(day, createdate, writedate) <= 7 * $i AND writedate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
    					UNION ALL
    					SELECT t1.platform, t1.useridx, deviceid, adflag, money
    					FROM (
    						SELECT platform, useridx, deviceid, adflag, createdate
    						FROM t5_user
    						WHERE platform > 0 AND useridx > 20000
    						AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%')
    						AND to_char(dateadd(day,((-7 * $i)), '$today 00:00:00'::date), 'YYYY-MM-DD 00:00:00')  <= createdate AND createdate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
    					) t1 JOIN t5_product_order_mobile t2 ON t1.useridx = t2.useridx
    					WHERE status = 1 AND datediff(day, createdate, writedate) <= 7 * $i AND writedate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
    				) total
    				GROUP BY platform, adflag";
    		$newuser_payer_cnt_list = $db_redshift->gettotallist($sql);
    		
    		for($j=0; $j<sizeof($newuser_payer_cnt_list); $j++)
    		{
    			$platform = $newuser_payer_cnt_list[$j]["platform"];
    			$adflag = $newuser_payer_cnt_list[$j]["adflag"];
    			$newuser_payer = $newuser_payer_cnt_list[$j]["newuser_payer"];
    			$newuser_money = $newuser_payer_cnt_list[$j]["newuser_money"];
    			
    			if($insert_sql == "")
    				$insert_sql = "INSERT INTO tbl_marketing_stat_daily (today, type, subtype, platform, adflag, newuser_payer, newuser_money) VALUES('$today', 1, $i, $platform, '$adflag', $newuser_payer, $newuser_money)";
    			else
    				$insert_sql .= ",('$today', 1, $i, $platform, '$adflag', $newuser_payer, $newuser_money)";
    		}
    	}
    	
    	if($insert_sql != "")
    	{
    		$insert_sql .= "ON DUPLICATE KEY UPDATE newuser_payer = VALUES(newuser_payer), newuser_money = VALUES(newuser_money);";
    		$execute_sql .= $insert_sql;
    	}
    	
    	// 신규 광고 리텐션 유입자수 (모바일 광고)
    	$insert_sql = "";
    	
    	for($i=1; $i<=4; $i++)
    	{
    		$sql = "SELECT platform, adflag, nvl(COUNT(distinct useridx), 0) AS reuser, nvl(AVG(leavedays), 0) AS reuser_leavedays
    				FROM (
    					SELECT 0 AS platform, useridx, adflag, leavedays, createdate, writedate AS retentiondate
    					FROM t5_user_retention_log
    					WHERE useridx > 20000 AND leavedays >= 28
    					AND to_char(dateadd(day,((-7 * $i)), '$today 00:00:00'::date), 'YYYY-MM-DD 00:00:00')  <= writedate AND writedate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
    					AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%')
    					UNION ALL
    					SELECT platform, useridx, adflag, leavedays, createdate, writedate AS retentiondate
    					FROM t5_user_retention_mobile_log
    					WHERE useridx > 20000 AND leavedays >= 28
    					AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%')
    					AND to_char(dateadd(day,((-7 * $i)), '$today 00:00:00'::date), 'YYYY-MM-DD 00:00:00')  <= writedate AND writedate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
    				) t1
    				GROUP BY platform, adflag";
    		$reuser_cnt_list = $db_redshift->gettotallist($sql);
    		
    		for($j=0; $j<sizeof($reuser_cnt_list); $j++)
    		{
    			$platform = $reuser_cnt_list[$j]["platform"];
    			$adflag = $reuser_cnt_list[$j]["adflag"];
    			$reuser = $reuser_cnt_list[$j]["reuser"];
    			$reuser_leavedays = $reuser_cnt_list[$j]["reuser_leavedays"];
    			
    			if($insert_sql == "")
    				$insert_sql = "INSERT INTO tbl_marketing_stat_daily (today, type, subtype, platform, adflag, reuser, reuser_leavedays) VALUES('$today', 1, $i, $platform, '$adflag', $reuser, $reuser_leavedays)";
    			else
    				$insert_sql .= ",('$today', 1, $i, $platform, '$adflag', $reuser, $reuser_leavedays)";
    		}
    	}
    	
    	if($insert_sql != "")
    	{
    		$insert_sql .= "ON DUPLICATE KEY UPDATE reuser = VALUES(reuser), reuser_leavedays = VALUES(reuser_leavedays);";
    		$execute_sql .= $insert_sql;
    	}
    	
     	// 신규 광고 리텐션 결제 정보 (모바일 광고)
     	$insert_sql = "";
    	
     	for($i=1; $i<=4; $i++)
     	{
     		$sql = "SELECT platform, adflag, nvl(COUNT(distinct useridx), 0) AS reuser_payer, nvl(SUM(money), 0) AS reuser_money, nvl(avg(leavedays), 0) AS reuser_payer_leavedays
    				FROM (
     					SELECT 0 AS platform, useridx, adflag, AVG(leavedays) AS leavedays, SUM(money) AS money
     					FROM (
     						SELECT t3.useridx, adflag, leavedays, multi_value, retentiondate, datediff(day, retentiondate, writedate) AS dayafterretention, writedate, round((facebookcredit::float/10::float) * multi_value::float, 2) money
     						FROM (
     							SELECT t1.useridx, t1.adflag, leavedays, (CASE WHEN leavedays >= 365 THEN 1 ELSE round(leavedays::float/365::float, 2) END) AS multi_value, retentiondate
     							FROM (
     								SELECT useridx, adflag, leavedays, createdate, writedate AS retentiondate
     								FROM t5_user_retention_log
     								WHERE useridx > 20000 AND leavedays >= 28
     								AND to_char(dateadd(day,((-7 * $i)), '$today 00:00:00'::date), 'YYYY-MM-DD 00:00:00')  <= writedate AND writedate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
     								AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%')
     							) t1 JOIN t5_user t2 ON t1.useridx = t2.useridx
     						) t3 JOIN t5_product_order t4 ON t3.useridx=t4.useridx AND t3.retentiondate <= t4.writedate
     						WHERE status = 1 AND datediff(day, retentiondate, writedate) < 28 AND writedate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
    				    	UNION ALL
    					    SELECT t3.useridx, adflag, leavedays, multi_value, retentiondate, datediff(day, retentiondate, writedate) AS dayafterretention, writedate, round(money::float * multi_value::float, 2) money
    					    FROM (
    	 						SELECT t1.useridx, t1.adflag, leavedays, (CASE WHEN leavedays >= 365 THEN 1 ELSE round(leavedays::float/365::float, 2) END) AS multi_value, retentiondate
    	 						FROM (
    	 							SELECT useridx, adflag, leavedays, createdate, writedate AS retentiondate
    	 							FROM t5_user_retention_log
    	 							WHERE useridx > 20000 AND leavedays >= 28
    	 							AND to_char(dateadd(day,((-7 * $i)), '$today 00:00:00'::date), 'YYYY-MM-DD 00:00:00')  <= writedate AND writedate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
    	 							AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%')
    	 						) t1 JOIN t5_user t2 ON t1.useridx = t2.useridx
    	 					) t3 JOIN t5_product_order_mobile t4 ON t3.useridx=t4.useridx AND t3.retentiondate <= t4.writedate
    	 					WHERE status = 1 AND datediff(day, retentiondate, writedate) < 28 AND writedate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
     					) total
     					GROUP BY useridx, adflag
     					UNION ALL
    	 				SELECT platform, useridx, adflag, AVG(leavedays) AS leavedays, SUM(money) AS money
    	 				FROM (
    	 					SELECT platform, t3.useridx, adflag, leavedays, multi_value, retentiondate, datediff(day, retentiondate, writedate) AS dayafterretention, writedate, round((facebookcredit::float/10::float) * multi_value::float, 2) money
    	 					FROM (
    	 						SELECT t1.platform, t1.useridx, t1.adflag, leavedays, (CASE WHEN leavedays >= 365 THEN 1 ELSE round(leavedays::float/365::float, 2) END) AS multi_value, retentiondate
    	 						FROM (
    						        SELECT platform, useridx, adflag, leavedays, createdate, writedate AS retentiondate
    						        FROM t5_user_retention_mobile_log
    						        WHERE useridx > 20000 AND leavedays >= 28 AND platform > 0
    		 						AND to_char(dateadd(day,((-7 * $i)), '$today 00:00:00'::date), 'YYYY-MM-DD 00:00:00')  <= writedate AND writedate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
    		 						AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%')
    	 						) t1 JOIN t5_user t2 ON t1.useridx = t2.useridx
    	 					) t3 JOIN t5_product_order t4 ON t3.useridx=t4.useridx AND t3.retentiondate <= t4.writedate
    		 				WHERE status = 1 AND datediff(day, retentiondate, writedate) < 28 AND writedate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
    		 				UNION ALL
    		 				SELECT platform, t3.useridx, adflag, leavedays, multi_value, retentiondate, datediff(day, retentiondate, writedate) AS dayafterretention, writedate, round(money::float * multi_value::float, 2) money
    		 				FROM (
    		 					SELECT t1.platform, t1.useridx, t1.adflag, leavedays, (CASE WHEN leavedays >= 365 THEN 1 ELSE round(leavedays::float/365::float, 2) END) AS multi_value, retentiondate
    		 					FROM (
    		 						SELECT platform, useridx, adflag, leavedays, createdate, writedate AS retentiondate
    		 						FROM t5_user_retention_mobile_log
    		 						WHERE useridx > 20000 AND leavedays >= 28 AND platform > 0
    		 						AND to_char(dateadd(day,((-7 * $i)), '$today 00:00:00'::date), 'YYYY-MM-DD 00:00:00')  <= writedate AND writedate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
    		 						AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%')
    		 					) t1 JOIN t5_user t2 ON t1.useridx = t2.useridx
    		 				) t3 JOIN t5_product_order_mobile t4 ON t3.useridx=t4.useridx AND t3.retentiondate <= t4.writedate
     						WHERE status = 1 AND datediff(day, retentiondate, writedate) < 28 AND writedate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
     					) total
     					GROUP BY platform, useridx, adflag
     				) t5
    				GROUP BY platform, adflag";
     		$reuser_payer_cnt_list = $db_redshift->gettotallist($sql);
    
     		for($j=0; $j<sizeof($reuser_payer_cnt_list); $j++)
     		{
     			$platform = $reuser_payer_cnt_list[$j]["platform"];
     			$adflag = $reuser_payer_cnt_list[$j]["adflag"];
     			$reuser_payer = $reuser_payer_cnt_list[$j]["reuser_payer"];
     			$reuser_money = $reuser_payer_cnt_list[$j]["reuser_money"];
     			$reuser_payer_leavedays = $reuser_payer_cnt_list[$j]["reuser_payer_leavedays"];
    
     			if($insert_sql == "")
     				$insert_sql = "INSERT INTO tbl_marketing_stat_daily (today, type, subtype, platform, adflag, reuser_payer, reuser_money, reuser_payer_leavedays) VALUES('$today', 1, $i, $platform, '$adflag', $reuser_payer, $reuser_money, $reuser_payer_leavedays)";
     			else
     				$insert_sql .= ",('$today', 1, $i, $platform, '$adflag', $reuser_payer, $reuser_money, $reuser_payer_leavedays)";
     		}
     	}
    	
     	if($insert_sql != "")
     	{
    	 	$insert_sql .= "ON DUPLICATE KEY UPDATE reuser_payer = VALUES(reuser_payer), reuser_money = VALUES(reuser_money), reuser_payer_leavedays = VALUES(reuser_payer_leavedays);";
    	 	$execute_sql .= $insert_sql;
     	}
     	
    	// 신규 광고 28일 미만 리텐션 유저 유입자수
    	$insert_sql = "";
    	
    	for($i=1; $i<=4; $i++)
    	{
    		$sql = "SELECT platform, adflag, nvl(COUNT(distinct useridx), 0) AS lessthan28_user, nvl(AVG(leavedays), 0) AS lessthan28_user_leavedays
    				FROM (
    					SELECT 0 AS platform, useridx, adflag, leavedays, createdate, writedate AS retentiondate
    					FROM t5_user_retention_log
    					WHERE useridx > 20000 AND leavedays < 28
    					AND to_char(dateadd(day,((-7 * $i)), '$today 00:00:00'::date), 'YYYY-MM-DD 00:00:00')  <= writedate AND writedate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
    					AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%')
    					UNION ALL
    					SELECT platform, useridx, adflag, leavedays, createdate, writedate AS retentiondate
    					FROM t5_user_retention_mobile_log
    					WHERE useridx > 20000 AND leavedays < 28
    					AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%')
    					AND to_char(dateadd(day,((-7 * $i)), '$today 00:00:00'::date), 'YYYY-MM-DD 00:00:00')  <= writedate AND writedate < to_char('$today 00:00:00'::date, 'YYYY-MM-DD 00:00:00')
    				) t1
    				GROUP BY platform, adflag";
    		$reuser_cnt_list = $db_redshift->gettotallist($sql);
    		
    		for($j=0; $j<sizeof($reuser_cnt_list); $j++)
    		{
    			$platform = $reuser_cnt_list[$j]["platform"];
    			$adflag = $reuser_cnt_list[$j]["adflag"];
    			$lessthan28_user = $reuser_cnt_list[$j]["lessthan28_user"];
    			$lessthan28_user_leavedays = $reuser_cnt_list[$j]["lessthan28_user_leavedays"];
    			
    			if($insert_sql == "")
    				$insert_sql = "INSERT INTO tbl_marketing_stat_daily (today, type, subtype, platform, adflag, lessthan28_user, lessthan28_user_leavedays) VALUES('$today', 1, $i, $platform, '$adflag', $lessthan28_user, $lessthan28_user_leavedays)";
    			else
    				$insert_sql .= ",('$today', 1, $i, $platform, '$adflag', $lessthan28_user, $lessthan28_user_leavedays)";
    		}
    	}
    	
    	if($insert_sql != "")
    	{
    		$insert_sql .= "ON DUPLICATE KEY UPDATE lessthan28_user = VALUES(lessthan28_user), lessthan28_user_leavedays = VALUES(lessthan28_user_leavedays);";
    		$execute_sql .= $insert_sql;
    	}
    	
     	
     	if($execute_sql != "")
     		$db_main2->execute($execute_sql);
	
 	}
	
 
 	
 	write_log("Marketing Stat Daily Scheduler End");
 	
 	$db_main2->end();
 	$db_redshift->end();
?>