<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");

	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/game/slot_rank_type_scheduler") !== false)
		{
			$count++;
		}
	}
	
	ini_set("memory_limit", "-1");
	
	$issuccess = "1";
	
	if ($count > 1)
		exit();
		
	$db_redshift = new CDatabase_Redshift();
	$db_other = new CDatabase_Other();

	$db_other->execute("SET wait_timeout=72000");
	
	$sdate = '2018-02-08';
	$edate = '2018-02-12';
		
	while($sdate <= $edate)
	{
		try
		{
			// 최근 1주일 결제자 결제액을 최근 2주 슬롯별 머니인 기준 배분 - Daily Rank
			$sql = "SELECT today, type, t1.slottype, user_cnt, money, dayafterinstall ".
					"FROM ( ".
					"	SELECT today, type, slottype, COUNT(useridx) AS user_cnt, ROUND(SUM(money*moneyin / total_moneyin), 2) AS money ".
					"	FROM tbl_slot_info_raw_log ".
					"	WHERE today = '$sdate' AND type = 0 ".
					"	GROUP BY today, type, slottype ".
					"	HAVING money > 0 ".
					") t1 JOIN ( ".
					"	SELECT slottype, ROUND(SUM(money/total_money*dayafterinstall)) AS dayafterinstall ".
					"	FROM ( ".
					"		SELECT useridx, slottype, ROUND(AVG(money), 2) AS money, ROUND(AVG(total_money), 2) AS total_money, ROUND(AVG(dayafterinstall)) AS dayafterinstall ".
					"		FROM tbl_slot_info_raw_log ".
					"		WHERE today = '$sdate' AND type = 0 ".
					"		GROUP BY slottype, useridx ".
					"	) t1 ".
					"	GROUP BY slottype ".
					") t2 ON t1.slottype = t2.slottype ".
					"ORDER BY money DESC";
		
			$day_rank_list = $db_other->gettotallist($sql);
		
			for($i=0; $i<sizeof($day_rank_list); $i++)
			{
				$today = $day_rank_list[$i]["today"];
				$type = $day_rank_list[$i]["type"];
				$slottype = $day_rank_list[$i]["slottype"];
				$user_cnt = $day_rank_list[$i]["user_cnt"];
				$money = $day_rank_list[$i]["money"];
				$dayafterinstall = $day_rank_list[$i]["dayafterinstall"];
				$rank = $i+1;
					
				$yesterday = date('Y-m-d', strtotime($sdate.' - 1 day'));
				$sql = "SELECT rank FROM tbl_slot_rank_daily WHERE today='$yesterday' AND type=$type AND platform=4 AND slottype=$slottype";
				$pre_rank = $db_other->getvalue($sql);
				$pre_rank = ($pre_rank == "") ? 0 : $pre_rank;
					
				$sql = "INSERT INTO tbl_slot_rank_daily(today, type, platform, slottype, user_cnt, money, dayafterinstall, pre_rank, rank) ".
						"VALUES('$today', $type, 4, $slottype, $user_cnt, $money, $dayafterinstall, $pre_rank, $rank) ON DUPLICATE KEY UPDATE user_cnt=VALUES(user_cnt), money=VALUES(money), dayafterinstall=VALUES(dayafterinstall), pre_rank=VALUES(pre_rank), rank=VALUES(rank);";
				$db_other->execute($sql);
			}
		
			$type = 0;
		
			$sql = "SELECT IFNULL(SUM(money), 0) ".
					"FROM `tbl_slot_rank_daily` ".
					"WHERE today = '$sdate' AND type = $type AND platform = 4 ";
			$total_money = $db_other->getvalue($sql);
		
			if($total_money == 0)
				$sql = "UPDATE tbl_slot_rank_daily SET share_rate = 0 WHERE today = '$sdate' AND type = $type AND platform=4";
				else
					$sql = "UPDATE tbl_slot_rank_daily SET share_rate = ROUND(money/$total_money*100, 2) WHERE today = '$sdate' AND type = $type AND platform=4";
		
					$db_other->execute($sql);
		
					// 최근 4주이내 가입자의 결제액을 최근 4주 슬롯별 머니인 기준 배분 - Daily Rank
					$sql = "SELECT today, type, t1.slottype, user_cnt, money, dayafterinstall ".
							"FROM ( ".
							"	SELECT today, type, slottype, COUNT(useridx) AS user_cnt, ROUND(SUM(money*moneyin / total_moneyin), 2) AS money ".
							"	FROM tbl_slot_info_raw_log ".
							"	WHERE today = '$sdate' AND type = 1 ".
							"	GROUP BY today, type, slottype ".
							"	HAVING money > 0 ".
							") t1 JOIN ( ".
							"	SELECT slottype, ROUND(SUM(money/total_money*dayafterinstall)) AS dayafterinstall ".
							"	FROM ( ".
							"		SELECT useridx, slottype, ROUND(AVG(money), 2) AS money, ROUND(AVG(total_money), 2) AS total_money, ROUND(AVG(dayafterinstall)) AS dayafterinstall ".
							"		FROM tbl_slot_info_raw_log ".
							"		WHERE today = '$sdate' AND type = 1 ".
							"		GROUP BY slottype, useridx ".
							"	) t1 ".
							"	GROUP BY slottype ".
							") t2 ON t1.slottype = t2.slottype ".
							"ORDER BY money DESC";
					$day_rank_list = $db_other->gettotallist($sql);
		
					for($i=0; $i<sizeof($day_rank_list); $i++)
					{
						$today = $day_rank_list[$i]["today"];
						$type = $day_rank_list[$i]["type"];
						$slottype = $day_rank_list[$i]["slottype"];
						$user_cnt = $day_rank_list[$i]["user_cnt"];
						$money = $day_rank_list[$i]["money"];
						$dayafterinstall = $day_rank_list[$i]["dayafterinstall"];
						$rank = $i+1;
							
						$yesterday = date('Y-m-d', strtotime($sdate.' - 1 day'));
						$sql = "SELECT rank FROM tbl_slot_rank_daily WHERE today='$yesterday' AND type=$type AND platform=4 AND slottype=$slottype";
						$pre_rank = $db_other->getvalue($sql);
						$pre_rank = ($pre_rank == "") ? 0 : $pre_rank;
							
						$sql = "INSERT INTO tbl_slot_rank_daily(today, type, platform, slottype, user_cnt, money, dayafterinstall, pre_rank, rank) ".
								"VALUES('$today', $type, 4, $slottype, $user_cnt, $money, $dayafterinstall, $pre_rank, $rank) ON DUPLICATE KEY UPDATE user_cnt=VALUES(user_cnt), money=VALUES(money), dayafterinstall=VALUES(dayafterinstall), pre_rank=VALUES(pre_rank), rank=VALUES(rank);";
						$db_other->execute($sql);
					}
		
					$type = 1;
		
					$sql = "SELECT IFNULL(SUM(money), 0) ".
							"FROM `tbl_slot_rank_daily` ".
							"WHERE today = '$sdate' AND type = $type AND platform = 4 ";
					$total_money = $db_other->getvalue($sql);
		
					if($total_money == 0)
						$sql = "UPDATE tbl_slot_rank_daily SET share_rate = 0 WHERE today = '$sdate' AND type = $type AND platform=4";
						else
							$sql = "UPDATE tbl_slot_rank_daily SET share_rate = ROUND(money/$total_money*100, 2) WHERE today = '$sdate' AND type = $type AND platform=4";
		
							$db_other->execute($sql);
		
							// 최근 4주이내 복귀자(D28이상)의 결제액을 최근 4주 슬롯별 머니인 기준 배분 - Daily Rank
							$sql = "SELECT today, type, t1.slottype, user_cnt, money, dayafterinstall ".
									"FROM ( ".
									"	SELECT today, type, slottype, COUNT(useridx) AS user_cnt, ROUND(SUM(money*moneyin / total_moneyin), 2) AS money ".
									"	FROM tbl_slot_info_raw_log ".
									"	WHERE today = '$sdate' AND type = 2 ".
									"	GROUP BY today, type, slottype ".
									"	HAVING money > 0 ".
									") t1 JOIN ( ".
									"	SELECT slottype, ROUND(SUM(money/total_money*dayafterinstall)) AS dayafterinstall ".
									"	FROM ( ".
									"		SELECT useridx, slottype, ROUND(AVG(money), 2) AS money, ROUND(AVG(total_money), 2) AS total_money, ROUND(AVG(dayafterinstall)) AS dayafterinstall ".
									"		FROM tbl_slot_info_raw_log ".
									"		WHERE today = '$sdate' AND type = 2 ".
									"		GROUP BY slottype, useridx ".
									"	) t1 ".
									"	GROUP BY slottype ".
									") t2 ON t1.slottype = t2.slottype ".
									"ORDER BY money DESC";
							$day_rank_list = $db_other->gettotallist($sql);
		
							for($i=0; $i<sizeof($day_rank_list); $i++)
							{
								$today = $day_rank_list[$i]["today"];
								$type = $day_rank_list[$i]["type"];
								$slottype = $day_rank_list[$i]["slottype"];
								$user_cnt = $day_rank_list[$i]["user_cnt"];
								$money = $day_rank_list[$i]["money"];
								$dayafterinstall = $day_rank_list[$i]["dayafterinstall"];
								$rank = $i+1;
									
								$yesterday = date('Y-m-d', strtotime($sdate.' - 1 day'));
								$sql = "SELECT rank FROM tbl_slot_rank_daily WHERE today='$yesterday' AND type=$type AND platform=4 AND slottype=$slottype";
								$pre_rank = $db_other->getvalue($sql);
								$pre_rank = ($pre_rank == "") ? 0 : $pre_rank;
									
								$sql = "INSERT INTO tbl_slot_rank_daily(today, type, platform, slottype, user_cnt, money, dayafterinstall, pre_rank, rank) ".
										"VALUES('$today', $type, 4, $slottype, $user_cnt, $money, $dayafterinstall, $pre_rank, $rank) ON DUPLICATE KEY UPDATE user_cnt=VALUES(user_cnt), money=VALUES(money), dayafterinstall=VALUES(dayafterinstall), pre_rank=VALUES(pre_rank), rank=VALUES(rank);";
								$db_other->execute($sql);
							}
		
							$type = 2;
		
							$sql = "SELECT IFNULL(SUM(money), 0) ".
									"FROM `tbl_slot_rank_daily` ".
									"WHERE today = '$sdate' AND type = $type AND platform = 4 ";
							$total_money = $db_other->getvalue($sql);
		
							if($total_money == 0)
								$sql = "UPDATE tbl_slot_rank_daily SET share_rate = 0 WHERE today = '$sdate' AND type = $type AND platform=4";
								else
									$sql = "UPDATE tbl_slot_rank_daily SET share_rate = ROUND(money/$total_money*100, 2) WHERE today = '$sdate' AND type = $type AND platform=4";
		
									$db_other->execute($sql);
		
									// 최근 1주 이내 결제복귀자(D28이상)의 결제액은 결제복귀시점 1주 슬롯별 머니인 기준 배분 - Daily Rank
									$sql = "SELECT today, type, t1.slottype, user_cnt, money, dayafterinstall ".
											"FROM ( ".
											"	SELECT today, type, slottype, COUNT(useridx) AS user_cnt, ROUND(SUM(money*moneyin / total_moneyin), 2) AS money ".
											"	FROM tbl_slot_info_raw_log ".
											"	WHERE today = '$sdate' AND type = 3 ".
											"	GROUP BY today, type, slottype ".
											"	HAVING money > 0 ".
											") t1 JOIN ( ".
											"	SELECT slottype, ROUND(SUM(money/total_money*dayafterinstall)) AS dayafterinstall ".
											"	FROM ( ".
											"		SELECT useridx, slottype, ROUND(AVG(money), 2) AS money, ROUND(AVG(total_money), 2) AS total_money, ROUND(AVG(dayafterinstall)) AS dayafterinstall ".
											"		FROM tbl_slot_info_raw_log ".
											"		WHERE today = '$sdate' AND type = 3 ".
											"		GROUP BY slottype, useridx ".
											"	) t1 ".
											"	GROUP BY slottype ".
											") t2 ON t1.slottype = t2.slottype ".
											"ORDER BY money DESC";
		
									$day_rank_list = $db_other->gettotallist($sql);
		
									for($i=0; $i<sizeof($day_rank_list); $i++)
									{
										$today = $day_rank_list[$i]["today"];
										$type = $day_rank_list[$i]["type"];
										$slottype = $day_rank_list[$i]["slottype"];
										$user_cnt = $day_rank_list[$i]["user_cnt"];
										$money = $day_rank_list[$i]["money"];
										$dayafterinstall = $day_rank_list[$i]["dayafterinstall"];
										$rank = $i+1;
											
										$yesterday = date('Y-m-d', strtotime($sdate.' - 1 day'));
										$sql = "SELECT rank FROM tbl_slot_rank_daily WHERE today='$yesterday' AND type=$type AND platform=4 AND slottype=$slottype";
										$pre_rank = $db_other->getvalue($sql);
										$pre_rank = ($pre_rank == "") ? 0 : $pre_rank;
											
										$sql = "INSERT INTO tbl_slot_rank_daily(today, type, platform, slottype, user_cnt, money, dayafterinstall, pre_rank, rank) ".
												"VALUES('$today', $type, 4, $slottype, $user_cnt, $money, $dayafterinstall, $pre_rank, $rank) ON DUPLICATE KEY UPDATE user_cnt=VALUES(user_cnt), money=VALUES(money), dayafterinstall=VALUES(dayafterinstall), pre_rank=VALUES(pre_rank), rank=VALUES(rank);";
										$db_other->execute($sql);
									}
		
									$type = 3;
		
									$sql = "SELECT IFNULL(SUM(money), 0) ".
											"FROM `tbl_slot_rank_daily` ".
											"WHERE today = '$sdate' AND type = $type AND platform = 4 ";
									$total_money = $db_other->getvalue($sql);
		
									if($total_money == 0)
										$sql = "UPDATE tbl_slot_rank_daily SET share_rate = 0 WHERE today = '$sdate' AND type = $type AND platform=4";
										else
											$sql = "UPDATE tbl_slot_rank_daily SET share_rate = ROUND(money/$total_money*100, 2) WHERE today = '$sdate' AND type = $type AND platform=4";
		
											$db_other->execute($sql);
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));
	}
	
	$db_redshift->end();
	$db_other->end();
?>
