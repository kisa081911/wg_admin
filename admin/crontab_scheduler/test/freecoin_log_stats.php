<?
	include("../../common/common_include.inc.php");

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/stats/freecoin_log_stats") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/stats/freecoin_log_stats") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("stats/freecoin_log_stats Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_livestats = new CDatabase_Livestats();

	$db_main2->execute("SET wait_timeout=7200");
	$db_analysis->execute("SET wait_timeout=7200");
	$db_livestats->execute("SET wait_timeout=7200");

	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$std_useridx = 10000;
	}
	write_log("freecoin_log_stats.php");
	try 
	{
		$today = "2019-07-28";
		$tomorrow = "2019-07-29";
		
		// 		//일반 freecoin
		// 		$normal_sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, category, type, COUNT(DISTINCT useridx) AS usercount, COUNT(*) AS freecount, IFNULL(SUM(amount), 0) AS freeamount, MAX(amount) AS max_amount ".
		// 				"FROM	".
		// 				"(	".
		// 				"	SELECT writedate, category, type, useridx, amount	".
		// 				"	FROM `tbl_freecoin_log_0` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		// 				"	UNION ALL	".
		// 				"	SELECT writedate, category, type, useridx, amount	".
		// 				"	FROM `tbl_freecoin_log_1` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		// 				"	UNION ALL	".
		// 				"	SELECT writedate, category, type, useridx, amount	".
		// 				"	FROM `tbl_freecoin_log_2` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		// 				"	UNION ALL	".
		// 				"	SELECT writedate, category, type, useridx, amount	".
		// 				"	FROM `tbl_freecoin_log_3` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		// 				"	UNION ALL	".
		// 				"	SELECT writedate, category, type, useridx, amount	".
		// 				"	FROM `tbl_freecoin_log_4` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		// 				"	UNION ALL	".
		// 				"	SELECT writedate, category, type, useridx, amount	".
		// 				"	FROM `tbl_freecoin_log_5` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		// 				"	UNION ALL	".
		// 				"	SELECT writedate, category, type, useridx, amount	".
		// 				"	FROM `tbl_freecoin_log_6` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		// 				"	UNION ALL	".
		// 				"	SELECT writedate, category, type, useridx, amount	".
		// 				"	FROM `tbl_freecoin_log_7` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		// 				"	UNION ALL	".
		// 				"	SELECT writedate, category, type, useridx, amount	".
		// 				"	FROM `tbl_freecoin_log_8` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		// 				"	UNION ALL	".
		// 				"	SELECT writedate, category, type, useridx, amount	".
		// 				"	FROM `tbl_freecoin_log_9` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		// 				") t1 GROUP BY category, TYPE";
		//일반 freecoin
		$normal_sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, category, type, COUNT(DISTINCT useridx) AS usercount, COUNT(*) AS freecount, IFNULL(SUM(amount), 0) AS freeamount, MAX(amount) AS max_amount ".
		  		"FROM	".
		  		"(	".
		  		"	SELECT writedate, category, type, useridx, amount	".
		  		"	FROM `tbl_user_freecoin_log_0` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		  		"	UNION ALL	".
		  		"	SELECT writedate, category, type, useridx, amount	".
		  		"	FROM `tbl_user_freecoin_log_1` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		  		"	UNION ALL	".
		  		"	SELECT writedate, category, type, useridx, amount	".
		  		"	FROM `tbl_user_freecoin_log_2` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		  		"	UNION ALL	".
		  		"	SELECT writedate, category, type, useridx, amount	".
		  		"	FROM `tbl_user_freecoin_log_3` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		  		"	UNION ALL	".
		  		"	SELECT writedate, category, type, useridx, amount	".
		  		"	FROM `tbl_user_freecoin_log_4` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		  		"	UNION ALL	".
		  		"	SELECT writedate, category, type, useridx, amount	".
		  		"	FROM `tbl_user_freecoin_log_5` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		  		"	UNION ALL	".
		  		"	SELECT writedate, category, type, useridx, amount	".
		  		"	FROM `tbl_user_freecoin_log_6` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		  		"	UNION ALL	".
		  		"	SELECT writedate, category, type, useridx, amount	".
		  		"	FROM `tbl_user_freecoin_log_7` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		  		"	UNION ALL	".
		  		"	SELECT writedate, category, type, useridx, amount	".
		  		"	FROM `tbl_user_freecoin_log_8` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		  		"	UNION ALL	".
		  		"	SELECT writedate, category, type, useridx, amount	".
		  		"	FROM `tbl_user_freecoin_log_9` WHERE useridx > $std_useridx AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'	".
		  		") t1 GROUP BY category, TYPE";
		$normal_list = $db_main2->gettotallist($normal_sql);
		
		for($i=0; $i<sizeof($normal_list); $i++)
		{
		    $writedate = $normal_list[$i]["writedate"];
		    $category = $normal_list[$i]["category"];
		    $type = $normal_list[$i]["type"];
		    $inbox_type = 0;
		    $usercount = $normal_list[$i]["usercount"];
		    $freecount = $normal_list[$i]["freecount"];
		    $freeamount = $normal_list[$i]["freeamount"];
		    $max_amount = $normal_list[$i]["max_amount"];
		    
		    $sql = "INSERT INTO tbl_inbox_freecoin_daily(today, category, type, inbox_type, usercount, freecount, freeamount, max_amount) VALUES ".
		  		    "('$writedate', $category, $type, $inbox_type, $usercount, $freecount, $freeamount, $max_amount) ".
		  		    "ON DUPLICATE KEY UPDATE usercount = VALUES(usercount), freecount = VALUES(freecount), freeamount = VALUES(freeamount), max_amount = VALUES(max_amount);";
		    $db_analysis->execute($sql);
		}
		
		// tbl_inbox_collect 통계
		$inbox_sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, type AS category, category AS type, COUNT(DISTINCT useridx) AS usercount, COUNT(*) AS freecount, IFNULL(SUM(coin), 0) AS freeamount, MAX(coin) AS max_coin
						FROM
						(
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_0` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_1` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_2` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_3` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_4` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_5` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_6` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_7` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_8` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_9` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_10` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_11` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_12` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_13` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_14` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_15` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_16` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_17` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_18` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
							UNION ALL
							SELECT writedate, type, category, useridx, coin
							FROM `tbl_user_inbox_collect_19` WHERE useridx > $std_useridx AND category < 300 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						)t1
						GROUP BY TYPE, category";
		$inbox_list = $db_livestats->gettotallist($inbox_sql);
		
		for($j=0; $j<sizeof($inbox_list); $j++)
		{
		    $writedate = $inbox_list[$j]["writedate"];
		    $category = $inbox_list[$j]["category"];
		    $type = 100; // inbox
		    $inbox_type = $inbox_list[$j]["type"];
		    $usercount = $inbox_list[$j]["usercount"];
		    $freecount = $inbox_list[$j]["freecount"];
		    $freeamount = $inbox_list[$j]["freeamount"];
		    $max_coin = $inbox_list[$j]["max_coin"];
		    
		    $sql = "INSERT INTO tbl_inbox_freecoin_daily(today, category, type, inbox_type, usercount, freecount, freeamount, max_amount) VALUES ".
		  		    "('$writedate', $category, $type, $inbox_type, $usercount, $freecount, $freeamount, $max_coin) ".
		  		    "ON DUPLICATE KEY UPDATE usercount = VALUES(usercount), freecount = VALUES(freecount), freeamount = VALUES(freeamount), max_amount = VALUES(max_amount);";
		    $db_analysis->execute($sql);
		}
		
		// tbl_inbox_collect 통계(Wake up)
		$inbox_sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, type AS category, category AS type, COUNT(DISTINCT useridx) AS usercount, COUNT(*) AS freecount, IFNULL(SUM(coin), 0) AS freeamount, MAX(coin) AS max_coin
						FROM
						(
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_0` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_1` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_2` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_3` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_4` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_5` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_6` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_7` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_8` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_9` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_10` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_11` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_12` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_13` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_14` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_15` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_16` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_17` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_18` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_19` WHERE useridx > $std_useridx AND category = 500 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						)t1
						GROUP BY TYPE, category";
		$inbox_list = $db_livestats->gettotallist($inbox_sql);
		
		for($j=0; $j<sizeof($inbox_list); $j++)
		{
		    $writedate = $inbox_list[$j]["writedate"];
		    $category = $inbox_list[$j]["category"];
		    $type = 100; // inbox
		    $inbox_type = $inbox_list[$j]["type"];
		    $usercount = $inbox_list[$j]["usercount"];
		    $freecount = $inbox_list[$j]["freecount"];
		    $freeamount = $inbox_list[$j]["freeamount"];
		    $max_coin = $inbox_list[$j]["max_coin"];
		    
		    $sql = "INSERT INTO tbl_inbox_freecoin_daily(today, category, type, inbox_type, usercount, freecount, freeamount, max_amount) VALUES ".
		  		    "('$writedate', $category, $type, $inbox_type, $usercount, $freecount, $freeamount, $max_coin) ".
		  		    "ON DUPLICATE KEY UPDATE usercount = VALUES(usercount), freecount = VALUES(freecount), freeamount = VALUES(freeamount), max_amount = VALUES(max_amount);";
		    $db_analysis->execute($sql);
		}
		
		// tbl_inbox_collect 통계(Invite)
		$inbox_sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, type AS category, category AS type, COUNT(DISTINCT useridx) AS usercount, COUNT(*) AS freecount, IFNULL(SUM(coin), 0) AS freeamount, MAX(coin) AS max_coin
						FROM
						(
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_0` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_1` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_2` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_3` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_4` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_5` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_6` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_7` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_8` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_9` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_10` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_11` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_12` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_13` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_14` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_15` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_16` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_17` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_18` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						UNION ALL
						SELECT writedate, type, category, useridx, coin
						FROM `tbl_user_inbox_collect_19` WHERE useridx > $std_useridx AND category = 400 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
						)t1
						GROUP BY TYPE, category";
		$inbox_list = $db_livestats->gettotallist($inbox_sql);
		
		for($j=0; $j<sizeof($inbox_list); $j++)
		{
		    $writedate = $inbox_list[$j]["writedate"];
		    $category = $inbox_list[$j]["category"];
		    $type = 100; // inbox
		    $inbox_type = $inbox_list[$j]["type"];
		    $usercount = $inbox_list[$j]["usercount"];
		    $freecount = $inbox_list[$j]["freecount"];
		    $freeamount = $inbox_list[$j]["freeamount"];
		    $max_coin = $inbox_list[$j]["max_coin"];
		    
		    $sql = "INSERT INTO tbl_inbox_freecoin_daily(today, category, type, inbox_type, usercount, freecount, freeamount, max_amount) VALUES ".
		  		    "('$writedate', $category, $type, $inbox_type, $usercount, $freecount, $freeamount, $max_coin) ".
		  		    "ON DUPLICATE KEY UPDATE usercount = VALUES(usercount), freecount = VALUES(freecount), freeamount = VALUES(freeamount), max_amount = VALUES(max_amount);";
		    $db_analysis->execute($sql);
		}
		
		// tbl_inbox_collect 통계(총 금액 19이상, 4주이내 미 결제자 보너스)
		$inbox_sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, type AS category, category AS type, COUNT(DISTINCT useridx) AS usercount, COUNT(*) AS freecount, IFNULL(SUM(coin), 0) AS freeamount, MAX(coin) AS max_coin
					FROM
					(
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_0` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_1` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_2` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_3` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_4` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_5` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_6` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_7` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_8` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_9` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_10` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_11` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_12` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_13` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_14` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_15` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_16` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_17` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_18` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_19` WHERE useridx > $std_useridx AND category = 410 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					)t1
					GROUP BY TYPE, category";
		$inbox_list = $db_livestats->gettotallist($inbox_sql);
		
		for($j=0; $j<sizeof($inbox_list); $j++)
		{
		    $writedate = $inbox_list[$j]["writedate"];
		    $category = $inbox_list[$j]["category"];
		    $type = 100; // inbox
		    $inbox_type = $inbox_list[$j]["type"];
		    $usercount = $inbox_list[$j]["usercount"];
		    $freecount = $inbox_list[$j]["freecount"];
		    $freeamount = $inbox_list[$j]["freeamount"];
		    $max_coin = $inbox_list[$j]["max_coin"];
		    
		    $sql = "INSERT INTO tbl_inbox_freecoin_daily(today, category, type, inbox_type, usercount, freecount, freeamount, max_amount) VALUES ".
		  		    "('$writedate', $category, $type, $inbox_type, $usercount, $freecount, $freeamount, $max_coin) ".
		  		    "ON DUPLICATE KEY UPDATE usercount = VALUES(usercount), freecount = VALUES(freecount), freeamount = VALUES(freeamount), max_amount = VALUES(max_amount);";
		    $db_analysis->execute($sql);
		}
		
		// tbl_inbox_collect 통계(Coin Box Reward)
		$inbox_sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, type AS category, category AS type, COUNT(DISTINCT useridx) AS usercount, COUNT(*) AS freecount, IFNULL(SUM(coin), 0) AS freeamount, MAX(coin) AS max_coin
					FROM
					(
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_0` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_1` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_2` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_3` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_4` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_5` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_6` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_7` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_8` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_9` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_10` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_11` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_12` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_13` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_14` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_15` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_16` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_17` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_18` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_19` WHERE useridx > $std_useridx AND category = 401 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					)t1
					GROUP BY TYPE, category";
		$inbox_list = $db_livestats->gettotallist($inbox_sql);
		
		for($j=0; $j<sizeof($inbox_list); $j++)
		{
		    $writedate = $inbox_list[$j]["writedate"];
		    $category = $inbox_list[$j]["category"];
		    $type = 100; // inbox
		    $inbox_type = $inbox_list[$j]["type"];
		    $usercount = $inbox_list[$j]["usercount"];
		    $freecount = $inbox_list[$j]["freecount"];
		    $freeamount = $inbox_list[$j]["freeamount"];
		    $max_coin = $inbox_list[$j]["max_coin"];
		    
		    $sql = "INSERT INTO tbl_inbox_freecoin_daily(today, category, type, inbox_type, usercount, freecount, freeamount, max_amount) VALUES ".
		  		    "('$writedate', $category, $type, $inbox_type, $usercount, $freecount, $freeamount, $max_coin) ".
		  		    "ON DUPLICATE KEY UPDATE usercount = VALUES(usercount), freecount = VALUES(freecount), freeamount = VALUES(freeamount), max_amount = VALUES(max_amount);";
		    $db_analysis->execute($sql);
		}
		
		// tbl_inbox_collect 통계(Mobile FB)
		$inbox_sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, type AS category, category AS type, COUNT(DISTINCT useridx) AS usercount, COUNT(*) AS freecount, IFNULL(SUM(coin), 0) AS freeamount, MAX(coin) AS max_coin
					FROM
					(
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_0` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_1` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_2` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_3` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_4` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_5` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_6` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_7` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_8` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_9` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_10` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_11` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_12` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_13` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_14` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_15` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_16` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_17` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_18` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_19` WHERE useridx > $std_useridx AND category = 600 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					)t1
					GROUP BY TYPE, category";
		$inbox_list = $db_livestats->gettotallist($inbox_sql);
		
		for($j=0; $j<sizeof($inbox_list); $j++)
		{
		    $writedate = $inbox_list[$j]["writedate"];
		    $category = $inbox_list[$j]["category"];
		    $type = 100; // inbox
		    $inbox_type = $inbox_list[$j]["type"];
		    $usercount = $inbox_list[$j]["usercount"];
		    $freecount = $inbox_list[$j]["freecount"];
		    $freeamount = $inbox_list[$j]["freeamount"];
		    $max_coin = $inbox_list[$j]["max_coin"];
		    
		    $sql = "INSERT INTO tbl_inbox_freecoin_daily(today, category, type, inbox_type, usercount, freecount, freeamount, max_amount) VALUES ".
		  		    "('$writedate', $category, $type, $inbox_type, $usercount, $freecount, $freeamount, $max_coin) ".
		  		    "ON DUPLICATE KEY UPDATE usercount = VALUES(usercount), freecount = VALUES(freecount), freeamount = VALUES(freeamount), max_amount = VALUES(max_amount);";
		    $db_analysis->execute($sql);
		}
		
		// tbl_inbox_collect 통계(Claimed Bonus)
		$inbox_sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, type AS category, category AS type, COUNT(DISTINCT useridx) AS usercount, COUNT(*) AS freecount, IFNULL(SUM(coin), 0) AS freeamount, MAX(coin) AS max_coin
					FROM
					(
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_0` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_1` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_2` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_3` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_4` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_5` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_6` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_7` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_8` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_9` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_10` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_11` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_12` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_13` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_14` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_15` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_16` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_17` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_18` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					UNION ALL
					SELECT writedate, type, category, useridx, coin
					FROM `tbl_user_inbox_collect_19` WHERE useridx > $std_useridx AND category = 501 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'
					)t1
					GROUP BY TYPE, category";
		$inbox_list = $db_livestats->gettotallist($inbox_sql);
		
		for($j=0; $j<sizeof($inbox_list); $j++)
		{
		    $writedate = $inbox_list[$j]["writedate"];
		    $category = $inbox_list[$j]["category"];
		    $type = 100; // inbox
		    $inbox_type = $inbox_list[$j]["type"];
		    $usercount = $inbox_list[$j]["usercount"];
		    $freecount = $inbox_list[$j]["freecount"];
		    $freeamount = $inbox_list[$j]["freeamount"];
		    $max_coin = $inbox_list[$j]["max_coin"];
		    
		    $sql = "INSERT INTO tbl_inbox_freecoin_daily(today, category, type, inbox_type, usercount, freecount, freeamount, max_amount) VALUES ".
		  		    "('$writedate', $category, $type, $inbox_type, $usercount, $freecount, $freeamount, $max_coin) ".
		  		    "ON DUPLICATE KEY UPDATE usercount = VALUES(usercount), freecount = VALUES(freecount), freeamount = VALUES(freeamount), max_amount = VALUES(max_amount);";
		    $db_analysis->execute($sql);
		}
		
		//vip bankrupt
		// 		$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, COUNT(DISTINCT useridx) AS usercount, COUNT(*) AS freecount, IFNULL(SUM(collect_amount),0) as freeamount, MAX(collect_amount) AS max_coin ".
		// 				"FROM tbl_bankrupt_vip_collect_log WHERE useridx > 20000 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00'";
		// 		$vip_bankrupt_list = $db_main2->getarray($sql);
		
		// 		$vip_bankrupt_writedate = $vip_bankrupt_list["writedate"];
		// 		$vip_bankrupt_usercount = $vip_bankrupt_list["usercount"];
		// 		$vip_bankrupt_freecount = $vip_bankrupt_list["freecount"];
		// 		$vip_bankrupt_freeamount = $vip_bankrupt_list["freeamount"];
		// 		$vip_bankrupt_max_coin = $vip_bankrupt_list["max_coin"];
		
		// 		$sql = "INSERT INTO tbl_inbox_freecoin_daily(today, category, type, inbox_type, usercount, freecount, freeamount, max_amount) VALUES ".
		// 				"('$vip_bankrupt_writedate', 0, 10, 0, $vip_bankrupt_usercount, $vip_bankrupt_freecount, $vip_bankrupt_freeamount, $vip_bankrupt_max_coin) ".
		// 				"ON DUPLICATE KEY UPDATE usercount = VALUES(usercount), freecount = VALUES(freecount), freeamount = VALUES(freeamount), max_amount = VALUES(max_amount);";
		// 		$db_analysis->execute($sql);
		
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	}
	
	$db_main2->end();
	$db_analysis->end();
	$db_livestats->end();
?>
