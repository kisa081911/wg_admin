<?
	include("../../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	
	$db_main->execute("SET wait_timeout=43200");
	$db_main2->execute("SET wait_timeout=43200");
	
	ini_set("memory_limit", "-1");
	
	try
	{
	    $sdate = "2019-08-13";
	    $edate =  "2019-08-17";

	    while($sdate < $edate)
	    {
	        $temp_date = date('Y-m-d', strtotime($sdate.' + 1 day'));
	        
	        $join_sdate = "$sdate";
	        $join_edate = "$temp_date";
	        
	        $write_sdate = $join_sdate." 00:00:00";
	        $write_edate = $join_edate." 00:00:00";

	        // Appsflyer
	        $sql = "SELECT useridx, createdate ".
                    "FROM tbl_user_ext ".
                    "WHERE platform > 0 AND '$write_sdate' <= createdate AND createdate < '$write_edate' AND chain_adflag = ''";
	        write_log($sql);
	        $apps_log_list = $db_main->gettotallist($sql);

	        $update_cnt = 0;
	        $re_attribution_update_cnt = 0;
	        
	        for($i=0; $i<sizeof($apps_log_list); $i++)
	        {
	            $useridx = $apps_log_list[$i]["useridx"];
	            $createdate = $apps_log_list[$i]["createdate"];

	           	$sql = "SELECT appsflyerid FROM tbl_user_mobile_appsflyer_new WHERE useridx=$useridx ORDER BY writedate ASC";
	           	$appsid_list = $db_main2->gettotallist($sql);
	            
	           	$str_appsid = "";
	           	
	           	for($j=0; $j<sizeof($appsid_list); $j++)
	           	{
	           	    $appsflyerid = $appsid_list[$j]["appsflyerid"];
	           	    
	           	    if($appsflyerid != "")
	           	    {
    	           	    if($str_appsid == "")
    	           	    {
    	           	        $str_appsid = "'".$appsflyerid."'";
    	           	    }
    	           	    else
    	           	    {
    	           	        $str_appsid .= ",'".$appsflyerid."'";
    	           	    }
	           	    }
	           	}
	           	
	           	if($str_appsid != "")
	           	{
	           	    // appsflyerinstall 정보 가져옴
	           	    $sql = "SELECT  agency, media_source, fb_campaign_name, fb_adset_name, install_time, appsflyer_device_id ".
	   	           	       "FROM tbl_appsflyer_install WHERE appsflyer_device_id IN ($str_appsid) ORDER BY logidx ASC LIMIT 1";
	           	    $apps_info = $db_main2->getarray($sql);
	           	    
	           	    $agency = $apps_info["agency"];
	           	    $media_source = $apps_info["media_source"];
	           	    $fb_campaign_name = $apps_info["fb_campaign_name"];
	           	    $fb_adset_name = $apps_info["fb_adset_name"];
	           	    $install_time = $apps_info["install_time"];
	           	    $appsflyer_device_id = $apps_info["appsflyer_device_id"];
	           	    
	           	    if($media_source == "Facebook Ads")
	           	    {
	           	        if($agency == "nanigans")
	           	            $media_source = $agency;
           	            else if($agency == "socialclicks" )
           	                $media_source = $agency;
	           	    }
	           	    else if($media_source == "")
	           	    {
	           	        if($agency == "amazon")
	           	            $media_source = $agency;
	           	    }
	           	    
	           	    if(strpos($fb_campaign_name, "m_retention") !== false)
	           	    {
	           	        $campaign_name_arr = explode("_", $fb_campaign_name);
	           	        $media_source = "m_".$campaign_name_arr[2];
	           	    }
	           	    else if(strpos($fb_campaign_name, "retention") !== false)
	           	    {
	           	        $campaign_name_arr = explode("_", $fb_campaign_name);
	           	        $media_source = $campaign_name_arr[1];
	           	    }
	           	    else if(strpos($fb_campaign_name, "m_duc") !== false || strpos($fb_campaign_name, "m_ddi") !== false)
	           	    {
	           	        $campaign_name_arr = explode("_", $fb_campaign_name);
	           	        
	           	        if($campaign_name_arr[1] == "m")
	           	            $media_source = "m_".$campaign_name_arr[2]."_int";
           	            else
           	                $media_source = "m_".$campaign_name_arr[3]."_int";
	           	    }
	           	    
	           	    if($media_source == "Facebook Ads")
	           	    {
	           	    	if(strpos($fb_campaign_name, "reengagement") !== false)
	           	    	{
	            			if(strpos($fb_adset_name, "reengagement") !== false)
	            			{
	            				$fb_adset_name_arr = explode("_", $fb_adset_name);
	            				$media_source = "m_".$fb_adset_name_arr[4];
	            			}
	            			else 
	            			{
	            				$media_source = "m_ur";
	            			}
	           	    	}
	           	    }
	           	    
	           	    // 조회되면 같은 appsflyerid 조회하여 가장 상위 useridx 가져옴
	           	    $sql = "SELECT useridx FROM tbl_user_mobile_appsflyer_new WHERE appsflyerid='$appsflyer_device_id'";
                    $user_list = $db_main2->gettotallist($sql);
                    
                    $str_useridx = "";
                    
                    for($k=0; $k<=sizeof($user_list);$k++)
                    {
                        $tmp_useridx = $user_list[$k]["useridx"];
                        
                        if($tmp_useridx != "")
                        {
                            if($str_useridx == "")
                            {
                                $str_useridx = $tmp_useridx;
                            }
                            else
                            {
                                $str_useridx .= ",".$tmp_useridx;
                            }
                        }
                    }
	           	    
                    if($str_useridx != "")
                    {
                        $sql = "SELECT useridx, createdate FROM tbl_user_ext WHERE useridx IN ($str_useridx) AND createdate >= DATE_SUB('$install_time', INTERVAL 1 HOUR) AND createdate <= '$createdate' ORDER BY useridx ASC LIMIT 1";
                        $chain_info = $db_main->getarray($sql);
                        
                        $chain_useridx = $chain_info["useridx"];
                        $chain_createdate = $chain_info["createdate"];
                        
                        if($chain_useridx != '' && $chain_createdate != '')
                        {
                            $sql = "UPDATE tbl_user_ext SET chain_useridx=$chain_useridx, adflag='$media_source', chain_adflag='$media_source', chain_createdate='$chain_createdate' WHERE useridx=$useridx";
                            $db_main->execute($sql);
                            $update_cnt++;
                        }
                    }
                    
                    // appsflyerinstall_re_attribution 정보 가져옴
                    $sql = "SELECT  agency, media_source, fb_campaign_name, fb_adset_name, install_time, appsflyer_device_id, is_retargeting ".
                    		"FROM tbl_appsflyer_install_re_attribution WHERE appsflyer_device_id IN ($str_appsid) ORDER BY logidx ASC LIMIT 1";
                    $re_apps_info = $db_main2->getarray($sql);
                     
                    $agency = $re_apps_info["agency"];
                    $media_source = $re_apps_info["media_source"];
                    $fb_campaign_name = $re_apps_info["fb_campaign_name"];
                    $fb_adset_name = $re_apps_info["fb_adset_name"];
                    $install_time = $re_apps_info["install_time"];
                    $appsflyer_device_id = $re_apps_info["appsflyer_device_id"];
                     
                    if($media_source == "Facebook Ads")
                    {
                    	if($agency == "nanigans")
                    		$media_source = $agency;
                    	else if($agency == "socialclicks" )
                    		$media_source = $agency;
                    }
                    else if($media_source == "")
                    {
                    	if($agency == "amazon")
                    		$media_source = $agency;
                    }
                     
                    if(strpos($fb_campaign_name, "m_retention") !== false)
                    {
                    	$campaign_name_arr = explode("_", $fb_campaign_name);
                    	$media_source = "m_".$campaign_name_arr[2];
                    }
                    else if(strpos($fb_campaign_name, "retention") !== false)
                    {
                    	$campaign_name_arr = explode("_", $fb_campaign_name);
                    	$media_source = $campaign_name_arr[1];
                    }
                    else if(strpos($fb_campaign_name, "m_duc") !== false || strpos($fb_campaign_name, "m_ddi") !== false)
                    {
                    	$campaign_name_arr = explode("_", $fb_campaign_name);
                    	 
                    	if($campaign_name_arr[1] == "m")
                    		$media_source = "m_".$campaign_name_arr[2]."_int";
                    	else
                    		$media_source = "m_".$campaign_name_arr[3]."_int";
                    }
                    
                    if($media_source == "Facebook Ads")
                    {
                    	if(strpos($fb_campaign_name, "reengagement") !== false)
                    	{
                    		if(strpos($fb_adset_name, "reengagement") !== false)
                    		{
                    			$fb_adset_name_arr = explode("_", $fb_adset_name);
                    			$media_source = "m_".$fb_adset_name_arr[4];
                    		}
                    		else
                    		{
                    			$media_source = "m_ur";
                    		}
                    	}
                    }
                     
                    // 조회되면 같은 appsflyerid 조회하여 가장 상위 useridx 가져옴
                    $sql = "SELECT useridx FROM tbl_user_mobile_appsflyer_new WHERE appsflyerid='$appsflyer_device_id'";
                    $user_list = $db_main2->gettotallist($sql);
                    
                    $re_str_useridx = "";
                    
                    for($j=0; $j<=sizeof($user_list);$j++)
                    {
                    	$tmp_useridx = $user_list[$j]["useridx"];
                    
                    	if($tmp_useridx != "")
                    	{
                    		if($re_str_useridx == "")
                    		{
                    			$re_str_useridx = $tmp_useridx;
                    		}
                    		else
                    		{
                    			$re_str_useridx .= ",".$tmp_useridx;
                    		}
                    	}
                    }
                     
                    if($re_str_useridx != "")
                    {
                    	$sql = "SELECT useridx, createdate, IF(TIMESTAMPDIFF(HOUR, '$install_time' , createdate) BETWEEN 0 AND 48, 1, 0) AS is_install ".
                    			"FROM tbl_user_ext WHERE useridx IN ($re_str_useridx) ORDER BY useridx ASC LIMIT 1";
                    	$re_chain_info = $db_main->getarray($sql);
                    
                    	$re_chain_useridx = $re_chain_info["useridx"];
                    	$re_chain_createdate = $re_chain_info["createdate"];
                    	$re_is_install = $re_chain_info["is_install"];
                    
                    	if($re_chain_useridx != '' && $re_chain_createdate != '' && $re_is_install > 0)
                    	{
                    		$sql = "UPDATE tbl_user_ext SET chain_useridx=$re_chain_useridx, chain_adflag='$media_source', chain_createdate='$re_chain_createdate' WHERE useridx=$useridx";
                    		$db_main->execute($sql);
                    		$re_attribution_update_cnt++;
                    	}
                    }
	           	}
	        }	        

	        write_log("$write_sdate ~ $write_edate Appsflyer Chain update cnt:".$update_cnt." re attribution chain update cnt :".$re_attribution_update_cnt);      

	        $sdate = $temp_date;
	    }
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();
?>