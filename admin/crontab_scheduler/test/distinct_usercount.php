<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");

	try
	{
		$db_main2 = new CDatabase_Main2();
		$db_redshift = new CDatabase_Redshift();
		$db_other = new CDatabase_Other();
		
		$sdate = "2017-11-07";
		$edate = "2017-11-08";
		
		while($sdate <= $edate)
		{
			$tomorrow = date('Y-m-d', strtotime($sdate.' + 1 day'));
			write_log("Date : ".$sdate." ~ ".$tomorrow);
			
			
			/*$sql = "SELECT  to_char(writedate, 'YYYY-mm-dd') AS writedate, 1 AS cnt_type, 0 AS cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user ".
					"FROM t5_user_gamelog_amazon WHERE writedate >= '$sdate 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 GROUP BY to_char(writedate, 'YYYY-mm-dd')	".
					"union all	".
					"SELECT  to_char(writedate, 'YYYY-mm-dd') AS writedate, 2 as cnt_type, slottype as cnt_type_value, COUNT(DISTINCT useridx) AS cnt_user	". 
					"FROM t5_user_gamelog_amazon WHERE writedate >= '$sdate 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 GROUP BY to_char(writedate, 'YYYY-mm-dd'), slottype	".
					"union all	".
					"select  to_char(writedate, 'YYYY-mm-dd') as writedate, 3 AS cnt_type, mode as cnt_type_value, count(distinct useridx) as cnt_user	". 
					"from t5_user_gamelog_amazon where writedate >= '$sdate 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 group by to_char(writedate, 'YYYY-mm-dd'), mode	".
					"union all	".
					"select  to_char(writedate, 'YYYY-mm-dd') as writedate, 4 AS cnt_type, betlevel as cnt_type_value, count(distinct useridx) as cnt_user	". 
					"from t5_user_gamelog_amazon where writedate >= '$sdate 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 group by to_char(writedate, 'YYYY-mm-dd'), betlevel";*/
			/*$sql = "SELECT  to_char(writedate, 'YYYY-mm-dd') AS writedate, 1 AS cnt_type, 1 as betmode, 0 as slottype, 0 as mode, COUNT(DISTINCT useridx) AS cnt_user	".
					"FROM t5_user_gamelog WHERE writedate >= '$sdate 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel between 0 AND 9 GROUP BY to_char(writedate, 'YYYY-mm-dd')	".
					"union all	".
					"SELECT  to_char(writedate, 'YYYY-mm-dd') AS writedate, 1 AS cnt_type, 2 as betmode, 0 as slottype,  0 as mode, COUNT(DISTINCT useridx) AS cnt_user	".
					"FROM t5_user_gamelog WHERE writedate >= '$sdate 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel between 10 AND 19 GROUP BY to_char(writedate, 'YYYY-mm-dd')	".
					"union all	".
					"SELECT  to_char(writedate, 'YYYY-mm-dd') AS writedate, 2 AS cnt_type, 1 as betmode, slottype, 0 as mode, COUNT(DISTINCT useridx) AS cnt_user	".
					"FROM t5_user_gamelog WHERE writedate >= '$sdate 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel between 0 AND 9 GROUP BY to_char(writedate, 'YYYY-mm-dd'), slottype	".
					"union all	".
					"SELECT  to_char(writedate, 'YYYY-mm-dd') AS writedate, 2 AS cnt_type, 2 as betmode, slottype, 0 as mode, COUNT(DISTINCT useridx) AS cnt_user	".
					"FROM t5_user_gamelog WHERE writedate >= '$sdate 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel between 10 AND 19 GROUP BY to_char(writedate, 'YYYY-mm-dd'), slottype	".
					"union all	".
					"SELECT  to_char(writedate, 'YYYY-mm-dd') AS writedate, 3 AS cnt_type, 1 as betmode, 0 as slottype, mode, COUNT(DISTINCT useridx) AS cnt_user	".
					"FROM t5_user_gamelog WHERE writedate >= '$sdate 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel between 0 AND 9 GROUP BY to_char(writedate, 'YYYY-mm-dd'), mode	".
					"union all	".
					"SELECT  to_char(writedate, 'YYYY-mm-dd') AS writedate, 3 AS cnt_type, 2 as betmode, 0 as slottype, mode, COUNT(DISTINCT useridx) AS cnt_user	".
					"FROM t5_user_gamelog WHERE writedate >= '$sdate 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel between 10 AND 19 GROUP BY to_char(writedate, 'YYYY-mm-dd'), mode";	
			$usercount_data = $db_redshift->gettotallist($sql);*/
			$sql = "SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 1 AS betmode, 0 AS slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user	".
					"FROM tbl_user_gamelog_amazon WHERE writedate >= '$sdate 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')	".
					"UNION ALL	".
					"SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 1 AS cnt_type, 2 AS betmode, 0 AS slottype,  0 AS mode, COUNT(DISTINCT useridx) AS cnt_user	".
					"FROM tbl_user_gamelog_amazon WHERE writedate >= '$sdate 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')	".
					"UNION ALL	".
					"SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 1 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user	".
					"FROM tbl_user_gamelog_amazon WHERE writedate >= '$sdate 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype	".
					"UNION ALL	".
					"SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 2 AS cnt_type, 2 AS betmode, slottype, 0 AS mode, COUNT(DISTINCT useridx) AS cnt_user	".
					"FROM tbl_user_gamelog_amazon WHERE writedate >= '$sdate 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), slottype	".
					"UNION ALL	".
					"SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 1 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user	".
					"FROM tbl_user_gamelog_amazon WHERE writedate >= '$sdate 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 0 AND 9 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE	".
					"UNION ALL	".
					"SELECT  DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 3 AS cnt_type, 2 AS betmode, 0 AS slottype, mode, COUNT(DISTINCT useridx) AS cnt_user	".
					"FROM tbl_user_gamelog_amazon WHERE writedate >= '$sdate 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 AND betlevel BETWEEN 10 AND 19 GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), MODE;";
			$usercount_data = $db_other->gettotallist($sql);
			
			$insert_cnt = 0;
			$insert_sql = "";
			
			for($i = 0; $i < sizeof($usercount_data); $i++)
			{
				$writedate = $usercount_data[$i]["writedate"];
				$os_type = 3;
				$betmode = $usercount_data[$i]["betmode"];
				$cnt_type = $usercount_data[$i]["cnt_type"];
				$betmode = $usercount_data[$i]["betmode"];
				$slottype = $usercount_data[$i]["slottype"];
				$mode = $usercount_data[$i]["mode"];
				$cnt_user = $usercount_data[$i]["cnt_user"];
				
				if($insert_sql == "")
				{
					$insert_sql = "INSERT INTO tbl_game_betmode_distinct_usercnt(writedate, os_type, betmode, cnt_type, slottype, mode, usercount) VALUES('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
				}
				else 
				{
					$insert_sql .= ",('$writedate', $os_type, $betmode, $cnt_type, $slottype, $mode, $cnt_user)";
				}
				
				$insert_cnt++;
	
				if($insert_cnt == 1000)
				{
					$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
					$db_other->execute($insert_sql);
					
					$insert_cnt = 0;
					$insert_sql = "";
				}
			}
			
			if($insert_sql != "")
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
				$db_other->execute($insert_sql);
			}
			
			sleep(1);
			$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));
		}
		
		$db_main2->end();
		$db_redshift->end();
		$db_other->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>