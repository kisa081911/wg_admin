<?
	include("../../common/common_include.inc.php");
    include_once('../../common/simple_html_dom.php');
    
	$result = array();
	exec("ps -ef | grep wget", $result);

	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/marketing_scheduler") !== false)
		{
			$count++;			
		}
	}
	
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	
	$db_main2->execute("SET wait_timeout=7200");
	$db_analysis->execute("SET wait_timeout=7200");
	
	$title = "[Take5 - Marketing Scheduler] ".date("Y-m-d")." Marketing Scheduler Fail";
	
	$to = "yhjung@doubleugames.com";
	$from = "report@doubleucasino.com";
	
	//================================ -1 Days ======================================
	$today = date("Y-m-d", strtotime("-1 day"));
	
	try
	{
		for ($d=1; $d<6; $d++)
		{
		
			$today = date("Y-m-d", strtotime("-$d day"));
			
			//vungle
			for($i=0; $i<3; $i++)
			{
				$flag = false;
				
				$vungle_spend_ios = 0;
				$vungle_spend_and = 0;
				
				$vungle_json = file_get_html('https://ssl.vungle.com/api/campaigns?key=e8db5220ab5b65779efa53477c846064')->plaintext;
				
				if(!empty($vungle_json))
				{				
					$vungle_objs = json_decode($vungle_json);
					
					foreach ($vungle_objs as $vungle_obj)
					{
						if(strpos($vungle_obj->{'name'}, "Take") !== false)
						{
							$vungle_campaign_json = file_get_html('http://ssl.vungle.com/api/campaigns/'.$vungle_obj->{'campaignId'}.'?key=e8db5220ab5b65779efa53477c846064&date='.$today)->plaintext;
							
							if(!empty($vungle_campaign_json))
							{
								$vungle_campaign_objs = json_decode($vungle_campaign_json);
								$vungle_campaign_name = strtolower($vungle_campaign_objs[0]->{'name'});
								
								if(strpos($vungle_campaign_name, "take") !== false)
								{
									if(strpos($vungle_campaign_name, "android") !== false)
										$vungle_spend_and += $vungle_campaign_objs[0]->{'dailySpend'};
									else if(strpos($vungle_campaign_name, "ios") !== false)
										$vungle_spend_ios += $vungle_campaign_objs[0]->{'dailySpend'};
								}
									
								$flag = true;
							}
						}
					}
					
					if(flag)
					{
						$sql = "INSERT INTO tbl_agency_spend_daily VALUES ('$today', 1, 2, 'vungle_int', $vungle_spend_ios) ON DUPLICATE KEY UPDATE spend = $vungle_spend_ios;".
								"INSERT INTO tbl_agency_spend_daily VALUES ('$today', 2, 2, 'vungle_int', $vungle_spend_and) ON DUPLICATE KEY UPDATE spend = $vungle_spend_and;";
						echo ($sql);
						echo ("</br>");
						//$db_main2->execute($sql);
						break;
					}
				}
			}
		}

			$insert_sql = "";
			echo("</br>");
			echo("</br>");
			echo("</br>");
			
			
			for($i=1; $i<=10; $i++)
			{
				for($j=1; $j<=4; $j++)
				{
					$sql = "SELECT DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL $i DAY), '%Y-%m-%d') AS today, platform, agencyname AS adflag, IFNULL(ROUND(SUM(spend), 2), 0) AS spend
					FROM `tbl_agency_spend_daily`
					WHERE agencyname <> '' AND DATE_ADD(DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL ($i - 1) DAY), '%Y-%m-%d'), INTERVAL (-7 * $j) DAY) <= today AND today < DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL ($i - 1) DAY), '%Y-%m-%d') AND spend <> 0
					AND agencyname ='vungle_int'
					GROUP BY platform, adflag";
					$mobile_agency_spend_list = $db_main2->gettotallist($sql);
			
					for($k=0; $k<sizeof($mobile_agency_spend_list); $k++)
					{
						$today = $mobile_agency_spend_list[$k]["today"];
						$platform = $mobile_agency_spend_list[$k]["platform"];
						$adflag = $mobile_agency_spend_list[$k]["adflag"];
						$spend = $mobile_agency_spend_list[$k]["spend"];
							
						if($insert_sql == "")
							$insert_sql = "INSERT INTO tbl_marketing_stat_daily (today, type, subtype, platform, adflag, spend) VALUES('$today', 1, $j, $platform, '$adflag', $spend)";
							else
								$insert_sql .= ",('$today', 1, $j, $platform, '$adflag', $spend)";
					}
				} 
			}
			
			if($insert_sql != "")
			{
				$insert_sql .= "ON DUPLICATE KEY UPDATE spend = VALUES(spend);";
				$execute_sql .= $insert_sql;
			}
			echo($insert_sql);
			echo("</br>");
			//$db_main2->execute($insert_sql);

	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		sendmail($to, $from, $title, $e->getMessage());
	}

    $db_main2->end();
    $db_analysis->end();
?>