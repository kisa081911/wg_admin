<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	
	
		$d = 17;
		$today = date("Y-m-d", time() - 60 * 60 * 24 * $d);
		$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * ($d+1));
		$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * ($d+1));
		try
		{
			$db_main2 = new CDatabase_Main2();
			
			$db_main2->execute("SET wait_timeout=3600");
			
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
			$sql = "SELECT * FROM tbl_event_result WHERE writedate > '$yesterday 00:00:00' AND writedate < '$today 00:00:00'";
			write_log($sql);
			$result_list = $db_main2->gettotallist($sql);
			
			$db_main2->end();
			
			$output = "";
			
			for($i=0; $i<sizeof($result_list); $i++)
			{		
				if($result_list[$i]["writedate"] == "0000-00-00 00:00:00")
					$result_list[$i]["writedate"] = "1900-01-01 00:00:00";
					
					$output .= '"'.$result_list[$i]["eventidx"].'"|';
					$output .= '"'.$result_list[$i]["useridx"].'"|';
					$output .= '"'.$result_list[$i]["reward_type"].'"|';				
					$output .= '"'.$result_list[$i]["reward_amount"].'"|';				
					$output .= '"'.$result_list[$i]["rank"].'"|';
					$output .= '"'.$result_list[$i]["isnew"].'"|';
					$output .= '"'.$result_list[$i]["isreturn"].'"|';
					$output .= '"'.$result_list[$i]["ordercredit"].'"|';
					$output .= '"'.$result_list[$i]["writedate"].'"|';
					
					$output .="\n";
					
			}
			
			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/t5_event_result_$yesterday_str.txt", 'a+');
				
				fwrite($fp, $output);
				
				fclose($fp);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
	
		try
		{
			// Create the AWS service builder, providing the path to the config file
			$aws = Aws::factory('../../common/aws_sdk/aws_config.php');
		
			$s3Client = $aws->get('s3');
			$bucket = "wg-redshift/t5/t5_event_result/$yesterday_str";
		
			$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
			$filepath = $DOCUMENT_ROOT."/redshift_temp/t5_event_result_$yesterday_str.txt";
		
			// Upload an object to Amazon S3
			$result = $s3Client->putObject(array(
					'Bucket' 		=> $bucket,
					'Key'    		=> 't5_event_result_'.$yesterday_str.'.txt',
					'ACL'	 		=> 'public-read',
					'SourceFile'   	=> $filepath
			));
		
			$ObjectURL = $result["ObjectURL"];
		
			if($ObjectURL != "")
			{
				@unlink($filepath);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		
		try
		{
				$db_redshift = new CDatabase_Redshift();
			
				// Create a staging table
				$sql = "CREATE TABLE t5_event_result_tmp (LIKE t5_event_result);";
				$db_redshift->execute($sql);
				
				// Load data into the staging table
				$sql = "copy t5_event_result_tmp ".
						"from 's3://wg-redshift/t5/t5_event_result/$yesterday_str' ".
						"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
						"delimiter '|' ".
						"csv;";
				$db_redshift->execute($sql);
				
				// Update records
				$sql = "UPDATE t5_event_result ".
						"SET reward_type = s.reward_type, reward_amount = s.reward_amount, rank = s.rank, isnew = s.isnew, isreturn = s.isreturn, ordercredit = s.ordercredit ".
						"FROM t5_event_result_tmp s ".
						"WHERE t5_event_result.eventidx = s.eventidx AND t5_event_result.useridx = s.useridx;";

				$db_redshift->execute($sql);
		
				// Insert records
				$sql = "INSERT INTO t5_event_result ".
						"SELECT s.* FROM t5_event_result_tmp s LEFT JOIN t5_event_result ".
						"ON s.eventidx = t5_event_result.eventidx ".
						"AND s.useridx = t5_event_result.useridx ".
						"WHERE t5_event_result.eventidx IS NULL;";

				$db_redshift->execute($sql);
					
				// Drop the staging table
				$sql = "DROP TABLE t5_event_result_tmp;";
				$db_redshift->execute($sql);
		
				$db_redshift->end();
			}
			catch(Exception $e)
			{
				write_log($e->getMessage());
			}


?>
