<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$db_main2 = new CDatabase_Main2();
	$db_redshift = new CDatabase_Redshift();
	$db_main2->execute("SET wait_timeout=3600");
	
	$s_date = date('Y-m-d', strtotime('2019-11-01'));
	$e_date= date('Y-m-d', strtotime('2019-11-11'));
	
	while($s_date < $e_date)
	{
	    $temp_date = date('Y-m-d', strtotime($s_date.' + 1 day'));
	    
	    $sdate = "$s_date";
	    $edate = "$temp_date";
	    
	    /*try
	    {
	       $sql = "SELECT * FROM t5_user_retention_log WHERE writedate >= '$sdate' AND writedate < '$edate' ORDER BY rtidx ASC ";
	        $retention_list = $db_redshift->gettotallist($sql);
	        
	        $insertcount = 0;
	        $insert_sql = "";
	        
	        for($i=0;$i<sizeof($retention_list);$i++)
	        {
	            $rtidx = $retention_list[$i]["rtidx"];
	            $useridx = $retention_list[$i]["useridx"];
	            $adflag = $retention_list[$i]["adflag"];
	            $fbsource = $retention_list[$i]["fbsource"];
	            $is_payer = $retention_list[$i]["is_payer"];
	            $level = $retention_list[$i]["level"];
	            $leavedays = $retention_list[$i]["leavedays"];
	            $createdate = $retention_list[$i]["createdate"];
	            $writedate = $retention_list[$i]["writedate"];
	            
	            if($insert_sql == "" && $insertcount == 0)
	            {
	                $insert_sql .= 	" INSERT INTO _tbl_user_retention_log_backup(rtidx,  useridx, adflag, fbsource, is_payer, level, leavedays, createdate, writedate) ".
	   	                " VALUES($rtidx,  $useridx, '$adflag', '$fbsource', $is_payer, $level, $leavedays, '$createdate','$writedate') ";
	                $insertcount ++;
	            }
	            else if($insertcount < 1000)
	            {
	                $insert_sql .= ",($rtidx,  $useridx, '$adflag', '$fbsource', $is_payer, $level, $leavedays, '$createdate','$writedate')";
	                $insertcount ++;
	            }
	            else
	            {
	                $insert_sql .= ",($rtidx,  $useridx, '$adflag', '$fbsource', $is_payer, $level, $leavedays, '$createdate','$writedate') ".
	   	                "ON DUPLICATE KEY UPDATE useridx=VALUES(useridx), adflag=VALUES(adflag), fbsource=VALUES(fbsource), level=VALUES(level),  ".
	   	                "is_payer=VALUES(is_payer), leavedays=VALUES(leavedays), createdate=VALUES(createdate), writedate=VALUES(writedate); ";
	                $db_main2->execute($insert_sql);
	                
	                $insertcount = 0;
	                $insert_sql = "";
	            }
	        }
	        
	        if($insert_sql != "")
	        {
	            $insert_sql .= "ON DUPLICATE KEY UPDATE useridx=VALUES(useridx), adflag=VALUES(adflag), fbsource=VALUES(fbsource), level=VALUES(level),  ".
	   	                       "is_payer=VALUES(is_payer), leavedays=VALUES(leavedays), createdate=VALUES(createdate), writedate=VALUES(writedate); ";
	            
	            $db_main2->execute($insert_sql);
	            
	            $insert_sql = "";
	        }
	    }
	    catch(Exception $e)
	    {
	        write_log($e->getMessage());
	    }
	    
	    write_log("$s_date _tbl_user_retention_log_backup ");*/
	    
	    try
	    {
	        $sql = "SELECT * FROM t5_user_retention_mobile_log WHERE writedate >= '$sdate' AND writedate < '$edate' ORDER BY rtidx ASC ";
	        $retention_list = $db_redshift->gettotallist($sql);
	        
	        $insertcount = 0;
	        $insert_sql = "";
	        
	        for($i=0;$i<sizeof($retention_list);$i++)
	        {
	            $rtidx = $retention_list[$i]["rtidx"];
	            $platform = $retention_list[$i]["platform"];
	            $useridx = $retention_list[$i]["useridx"];
	            $adflag = $retention_list[$i]["adflag"];
	            $eventidx = $retention_list[$i]["eventidx"];
	            $is_payer = $retention_list[$i]["is_payer"];
	            $level = $retention_list[$i]["level"];
	            $leavedays = $retention_list[$i]["leavedays"];
	            $site_id = $retention_list[$i]["site_id"];
	            $createdate = $retention_list[$i]["createdate"];
	            $writedate = $retention_list[$i]["writedate"];
	            
	            if($insert_sql == "" && $insertcount == 0)
	            {
	                $insert_sql .= 	" INSERT INTO _tbl_user_retention_mobile_log_201909_201910(rtidx, platform, useridx, adflag, site_id, eventidx, is_payer, level, leavedays, createdate, writedate) ".
	   	                " VALUES($rtidx, $platform, $useridx, '$adflag', '$site_id', $eventidx,  $is_payer, $level, $leavedays, '$createdate','$writedate') ";
	                $insertcount ++;
	            }
	            else if($insertcount < 1000)
	            {
	                $insert_sql .= ",($rtidx, $platform, $useridx, '$adflag', '$site_id', $eventidx,  $is_payer, $level, $leavedays, '$createdate','$writedate') ";
	                $insertcount ++;
	            }
	            else
	            {
	                $insert_sql .= ",($rtidx, $platform, $useridx, '$adflag', '$site_id', $eventidx,  $is_payer, $level, $leavedays, '$createdate','$writedate')  ".
	   	                "ON DUPLICATE KEY UPDATE platform = VALUES(platform), useridx=VALUES(useridx), adflag=VALUES(adflag), site_id=VALUES(site_id), eventidx=VALUES(eventidx), level=VALUES(level),  ".
	   	                "is_payer=VALUES(is_payer), leavedays=VALUES(leavedays), createdate=VALUES(createdate), writedate=VALUES(writedate); ";
	                $db_main2->execute($insert_sql);
	                
	                $insertcount = 0;
	                $insert_sql = "";
	            }
	        }
	        
	        if($insert_sql != "")
	        {
	            $insert_sql .= "ON DUPLICATE KEY UPDATE platform = VALUES(platform), useridx=VALUES(useridx), adflag=VALUES(adflag), site_id=VALUES(site_id), eventidx=VALUES(eventidx), level=VALUES(level),  ".
	   	                       "is_payer=VALUES(is_payer), leavedays=VALUES(leavedays), createdate=VALUES(createdate), writedate=VALUES(writedate); ";
	            
	            $db_main2->execute($insert_sql);
	            
	            $insert_sql = "";
	        }
	    }
	    catch(Exception $e)
	    {
	        write_log($e->getMessage());
	    }
		
		write_log("$s_date _tbl_user_retention_mobile_log_backup ");
	    
	    $s_date = $temp_date;
	}
	
	$db_main2->end();
	$db_redshift->end();
?>