<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_friend = new CDatabase_Friend();
	$db_game = new CDatabase_Game();
	$db_other = new CDatabase_Other();
	$db_livestats = new CDatabase_Livestats();
	$db_mobile = new CDatabase_Mobile();
	$db_inbox = new CDatabase_Inbox();
	$db_slave_main = new CDatabase_Slave_Main();
	$db_redshift = new CDatabase_Redshift();
	
	$db_main->execute("SET wait_timeout=7200");
	$db_main2->execute("SET wait_timeout=7200");
	$db_analysis->execute("SET wait_timeout=7200");
	$db_friend->execute("SET wait_timeout=7200");
	$db_game->execute("SET wait_timeout=7200");
	$db_other->execute("SET wait_timeout=7200");
	$db_livestats->execute("SET wait_timeout=7200");
	$db_mobile->execute("SET wait_timeout=7200");
	$db_inbox->execute("SET wait_timeout=7200");
	$db_slave_main->execute("SET wait_timeout=7200");
	
	$port = "";
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}	

	
	try
	{
		for($d=110;$d<127;$d++)
		{
			$today_str = date("Y-m-d", strtotime("-$d days"));
			write_log($today_str);
			
			$sql="select  '$today_str'  as today , category, type, inbox_type, COUNT(logidx) AS freecount, SUM(amount) AS freeamount 
					from t5_user_freecoin_log 
					where type = 100
					and writedate >= '$today_str 00:00:00'
					and writedate <= '$today_str 23:59:59'
					GROUP BY category, TYPE ,inbox_type";
			$inbox_log_list=$db_redshift->gettotallist($sql);
			$update_sql="";
			
			for($i=0;$i<sizeof($inbox_log_list);$i++)
			{
				$today = $inbox_log_list[$i]["today"];
				$category = $inbox_log_list[$i]["category"];
				$type = $inbox_log_list[$i]["type"];
				$inbox_type = $inbox_log_list[$i]["inbox_type"];
				$freecount = $inbox_log_list[$i]["freecount"];
				$freeamount = $inbox_log_list[$i]["freeamount"];
				
				$update_sql ="UPDATE tbl_user_freecoin_daily SET freecount=$freecount,freeamount=$freeamount WHERE today='$today' AND category = $category AND type = $type and inbox_type = $inbox_type;";
				$db_analysis->execute($update_sql);
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}

	
	
	$db_main->end();
	$db_main2->end();
	$db_analysis->end();
	$db_friend->end();
	$db_game->end();
	$db_other->end();
	$db_livestats->end();
	$db_mobile->end();
	$db_inbox->end();
	$db_slave_main->end();	
	$db_redshift->end();
?>
