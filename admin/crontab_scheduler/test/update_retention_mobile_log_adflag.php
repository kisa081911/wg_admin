<?
	include("../../common/common_include.inc.php");

	$db_main2 = new CDatabase_Main2();

	$db_main2->execute("SET wait_timeout=21600");	
	
	try 
	{
		$sdate = "2019-02-13";
		$edate = "2019-02-14";
		 
		while($sdate < $edate)
		{
			$temp_date = date('Y-m-d', strtotime($sdate.' + 1 day'));
			
			$join_sdate = "$sdate";
			$join_edate = "$temp_date";
			 
			$write_sdate = $join_sdate." 00:00:00";
			$write_edate = $join_edate." 00:00:00";			 
			
			$sql = "SELECT useridx, adflag, COUNT(*) FROM tbl_user_retention_mobile_log WHERE '$write_sdate' <= writedate AND writedate < '$write_edate' ". 
					"AND (adflag LIKE '%_int' OR adflag LIKE 'Facebook%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple%')	".
					"GROUP BY useridx, adflag HAVING COUNT(*) > 1 ORDER BY COUNT(*) DESC;";
			write_log($sql);
			$useridx_info_list = $db_main2->gettotallist($sql);
			
			for($i = 0; $i < sizeof($useridx_info_list); $i++)
			{
				$info_useridx = $useridx_info_list[$i]["useridx"];
				$info_adflag = $useridx_info_list[$i]["adflag"];
				
				$retention_sql = "SELECT * FROM tbl_user_retention_mobile_log WHERE useridx = $info_useridx AND '$write_sdate' <= writedate AND writedate < '$write_edate' ORDER BY rtidx ASC";
				$retention_info_list = $db_main2->gettotallist($retention_sql);
				
				$temp_rtidx = "";
				$temp_adflag = "";
				$update_cnt = 0;
				$update_rtidx = "";
				
				$delete_cnt = 0;
				$delete_rtidx = "";
				
				for($j = 0; $j < sizeof($retention_info_list); $j++)
				{
					$rtidx = $retention_info_list[$j]["rtidx"];
					$adflag = $retention_info_list[$j]["adflag"];
					
					if($info_adflag == $adflag)
					{
						if($temp_adflag != $adflag)
						{					
							$temp_rtidx = $rtidx;
							$temp_adflag = $adflag;
						}
						else
						{
							if($temp_adflag == $adflag)
							{
								// rtidx 추출해서 빈값 update 또는 삭제를 위해 rtidx 저장
								if($update_cnt == 0)
									$update_rtidx = "$rtidx";
								
								if($delete_cnt == 0 && $update_cnt > 0)
								{
									$delete_rtidx = "$rtidx";
									$delete_cnt++;
								}
								else if($delete_cnt > 0 && $update_cnt > 0)
									$delete_rtidx .= ",$rtidx";
								
								$update_cnt++;
								
							}
						}				
					}
				}
				
				$update_sql = "UPDATE tbl_user_retention_mobile_log SET adflag = '' WHERE rtidx = $update_rtidx;";				
				
				if($delete_rtidx != "")
					$update_sql .= "DELETE FROM tbl_user_retention_mobile_log WHERE rtidx IN ($delete_rtidx);";		
				
				$db_main2->execute($update_sql);
							
			}
			
			sleep(1);
			
			$sdate = $temp_date;
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();	
?>
