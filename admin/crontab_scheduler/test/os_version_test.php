<?
	include("../../common/common_include.inc.php");

	$db_mobile = new CDatabase_Mobile();
	$db_analysis = new CDatabase_Analysis();
	
	$db_mobile->execute("SET wait_timeout=3600");	
	$db_analysis->execute("SET wait_timeout=3600");	
	
	$port = "";
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}	

	for($k=1;$k<=20;$k++)
	{
		// Mobile OS Version
		try
		{
			$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * $k);
		
			$sql = "SELECT os_type, app_version, count(useridx) as count
				FROM 
				(
					SELECT useridx, t1.os_type, app_version, logindate	
					FROM tbl_mobile t1 
					JOIN `tbl_user_mobile_connection_log` t2 
					ON t1.device_id = t2.device_id 
					WHERE  t2.logindate >= '$yesterday 00:00:00' AND t2.logindate <= '$yesterday 23:59:59'
				)t5 GROUP BY os_type, app_version ";
			$mobile_data = $db_mobile->gettotallist($sql);
	
			$insert_sql = "";
	
			for($i=0; $i<sizeof($mobile_data); $i++)
			{
					$app_version = $mobile_data[$i]["app_version"];
					$os_type = $mobile_data[$i]["os_type"];
					$count = $mobile_data[$i]["count"];
		
					if($insert_sql == "")
						$insert_sql = "INSERT INTO mobile_version_daily VALUES('$yesterday', '$app_version', $os_type,$count);";
					else
						$insert_sql .= "INSERT INTO mobile_version_daily VALUES('$yesterday', '$app_version', $os_type,$count);";
			}
		
			if($insert_sql != "")
				$db_mobile->execute($insert_sql);
			}
		catch (Exception $e)
		{
			write_log($e->getMessage());
			$issuccess = 0;
		}
	}
	$db_mobile->end();	
	$db_analysis->end();	
?>
