<?
	include("../../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	
	$db_main->execute("SET wait_timeout=7200");
	
	ini_set("memory_limit", "-1");
	
	try 
	{
		$facebook = new Facebook(array(
				'appId'  => FACEBOOK_APP_ID,
				'secret' => FACEBOOK_SECRET_KEY,
				'cookie' => true,
		));
		
		try
		{
			
			$sql = "select * from tbl_product_order WHERE tax_country ='' AND writedate >= '2019-06-01 00:00:00' AND writedate < '2019-07-01 00:00:00' AND orderno NOT IN (SELECT orderno FROM _update_tbl_product_order_fail)";
			$check_product = $db_main->gettotallist($sql);
			
			for ($i=0; $i<sizeof($check_product); $i++)
			{
				$orderno = $check_product[$i]["orderno"];
				
				$order_info = $facebook->api("/$orderno?fields=user,actions,refundable_amount,items,disputes,tax,tax_country,payout_foreign_exchange_rate,application,fraud_status,fulfillment_status,test,request_id,phone_support_eligible,is_from_page_post,is_from_ad");
			
				if(sizeof($order_info["user"]) > 0)
				{
					$userid = $order_info["user"]["id"];
				}
			
				if(sizeof($order_info["actions"]) > 0)
				{
					$action_info = $order_info["actions"][0];
						
					$type = $action_info["type"];
					$status = $action_info["status"];
					$amount = $action_info["amount"];
			
					$currency = $action_info["currency"];
					$currency_amount = $amount;					
					
					$tax_amount = $action_info["tax_amount"];
				}
			
				if(sizeof($order_info["actions"]) > 1)
				{
					$chargeback_info = $order_info["actions"][1];
						
					$type = $chargeback_info["type"];
					$status = $chargeback_info["status"];
						
					$chargeback_currency = $chargeback_info["currency"];
					$chargeback_amount = $chargeback_info["amount"];					
				}
			
				if(sizeof($order_info["items"]) > 0)
				{
					$item_info = $order_info["items"][0];
						
					$product_url = $item_info["product"];
						
					$productkey_info = explode('=', $product_url);
					$productkey = $productkey_info[1];
				}
				
				$tax_country = $order_info["tax_country"];
				$payout_foreign_exchange_rate = $order_info["payout_foreign_exchange_rate"];
				
				//write_log("$orderno|$tax_amount|$payout_foreign_exchange_rate");
				
				$update_sql = "UPDATE tbl_product_order SET currency = '$currency', currency_amount = '$currency_amount', tax_country = '$tax_country', tax_amount = '$tax_amount', payout_foreign_exchange_rate = '$payout_foreign_exchange_rate' WHERE orderno = '$orderno'";
				$db_main->execute($update_sql);
				
				if($i%1000 == 0)
					sleep(3);
			}
		}
		catch (FacebookApiException $e)
		{		
			$insert_sql = "INSERT INTO _update_tbl_product_order_fail(orderno) VALUES ('$orderno');";
			$db_main->execute($insert_sql);		
			
			write_log($e->getMessage());
		}
	}
	catch(Exception $e)
	{
		
	}
	
	$db_main->end();
?>