<?
	include("../../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$db_inbox = new CDatabase_Inbox();
	$db_Slave_main = new CDatabase_Slave_Main();
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_livestats = new CDatabase_Livestats();
	$db_other = new CDatabase_Other();
	
	$db_main->execute("SET wait_timeout=300");
	$db_inbox->execute("SET wait_timeout=300");
	$db_Slave_main->execute("SET wait_timeout=300");
	$db_main2->execute("SET wait_timeout=300");
	$db_analysis->execute("SET wait_timeout=300");
	$db_livestats->execute("SET wait_timeout=300");
	$db_other->execute("SET wait_timeout=300");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$port = "";
	
	$str_useridx = 20000;
	
	$standard_date = date("Y-m-d H:i").":00";
	$standard_shortdate = substr($standard_date, 0, 10);
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}

	try
	{
	    //web
	    $insert_sql = " INSERT INTO tbl_action_log_stat ".
	    " SELECT 1 AS TYPE, 0 AS os_type, COUNT(facebookid) AS cnt, '$standard_date' AS wrtedate ".
	    " FROM ".
	    " ( ".
	    " 	SELECT facebookid FROM `user_action_log_0` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT facebookid FROM `user_action_log_1` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT facebookid FROM `user_action_log_2` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT facebookid FROM `user_action_log_3` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT facebookid FROM `user_action_log_4` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT facebookid FROM `user_action_log_5` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT facebookid FROM `user_action_log_6` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT facebookid FROM `user_action_log_7` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT facebookid FROM `user_action_log_8` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    "	    SELECT facebookid FROM `user_action_log_9` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "    ) t1 ".
	    " ON DUPLICATE KEY UPDATE cnt = VALUES(cnt)";
	    $db_analysis->execute($insert_sql);
	    
	    //ios
	    $insert_sql = " INSERT INTO tbl_action_log_stat ".
	    " SELECT 1 AS TYPE, 1 AS os_type, COUNT(useridx) AS cnt, '$standard_date' AS wrtedate ".
	    " FROM ".
	    " ( ".
	    " 	SELECT useridx FROM `user_action_log_ios_0` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_ios_1` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_ios_2` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_ios_3` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_ios_4` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_ios_5` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_ios_6` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_ios_7` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_ios_8` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    "	    SELECT useridx FROM `user_action_log_ios_9` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "    ) t1 ".
	    " ON DUPLICATE KEY UPDATE cnt = VALUES(cnt)";
	    $db_analysis->execute($insert_sql);
	    
	    //android
	    $insert_sql = " INSERT INTO tbl_action_log_stat ".
	    " SELECT 1 AS TYPE, 2 AS os_type, COUNT(useridx) AS cnt, '$standard_date' AS wrtedate ".
	    " FROM ".
	    " ( ".
	    " 	SELECT useridx FROM `user_action_log_android_0` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_android_1` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_android_2` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_android_3` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_android_4` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_android_5` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_android_6` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_android_7` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    " 	SELECT useridx FROM `user_action_log_android_8` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "     UNION ALL ".
	    "	    SELECT useridx FROM `user_action_log_android_9` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "    ) t1 ".
	    " ON DUPLICATE KEY UPDATE cnt = VALUES(cnt)";
	    $db_analysis->execute($insert_sql);
	    
	    //amazon
	    $insert_sql = " INSERT INTO tbl_action_log_stat ".
	    " SELECT 1 AS TYPE, 3 AS os_type, COUNT(useridx) AS cnt, '$standard_date' AS wrtedate ".
	    " FROM ".
	    " ( ".
	    " 	SELECT useridx FROM `user_action_log_amazon` WHERE ACTION LIKE '%Spin Time Over 9 second%' AND DATE_SUB('$standard_date', INTERVAL 5 MINUTE) <= writedate AND writedate < '$standard_date' ".
	    "    ) t1 ".
	    " ON DUPLICATE KEY UPDATE cnt = VALUES(cnt)";
	    $db_analysis->execute($insert_sql);
	}
	catch (Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	$db_main->end();
	$db_Slave_main->end();
	$db_main2->end();
	$db_analysis->end();
	$db_livestats->end();
	$db_other->end();
?>