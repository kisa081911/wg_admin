<?
	include("../../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_mobile = new CDatabase_Mobile();
	
	$db_main->execute("SET wait_timeout=3600");
	$db_main2->execute("SET wait_timeout=3600");
	$db_mobile->execute("SET wait_timeout=3600");
	
	ini_set("memory_limit", "-1");
	
	$back_day = date("Y-m-d", time() - 60 * 60 * 24 * 3);
	$yesterday = '2018-09-06';
	$today = '2018-09-07';
		
	try
	{
	    // retention
	    $sql = "SELECT rtidx, platform, useridx, adflag, createdate, writedate
				FROM tbl_user_retention_mobile_log
				WHERE writedate >= '$yesterday 00:00:00' AND writedate < '$today 00:00:00'
				AND (adflag LIKE '%_int' OR adflag LIKE 'Facebook%' OR adflag LIKE 'amazon' OR adflag LIKE 'm_reten%')";
	    $retention_list = $db_main2->gettotallist($sql);
	    
	    for($i=0; $i<sizeof($retention_list); $i++)
	    {
	        $rtidx = $retention_list[$i]["rtidx"];
	        $platform = $retention_list[$i]["platform"];
	        $useridx = $retention_list[$i]["useridx"];
	        $media_source = $retention_list[$i]["adflag"];
	        $createdate = $retention_list[$i]["createdate"];
	        $writedate = $retention_list[$i]["writedate"];
	        $site_id = "";
	        
	        //adjust
	        $sql = "SELECT adid, advid AS deviceid
					FROM tbl_user_mobile_adjust
					WHERE useridx = $useridx AND os_type = $platform
					ORDER BY writedate DESC
					LIMIT 1";
	        $user_info = $db_main2->getarray($sql);
	        
	        $adid = $user_info["adid"];
	        $device_id = $user_info["deviceid"];
	        
	        if($adid != "")
	        {
	            $sql = "SELECT fb_adgroup_id AS fb_ad_id, adgroup_name AS site_id, adid, installed_at AS install_time, fb_campaign_id, adwords_campaign_id
						FROM tbl_adjust_install
						WHERE adid = '$adid' AND platform = $platform AND 0 <= DATEDIFF('$writedate', installed_at) AND DATEDIFF('$writedate', installed_at) <= 2
						ORDER BY logidx DESC
						LIMIT 1";
	            $apps_info = $db_main2->getarray($sql);
	            
	            $fb_campaign_id = $apps_info["fb_campaign_id"];
	            $adwords_campaign_id = $apps_info["adwords_campaign_id"];
	            
	            if($fb_campaign_id != "" && $fb_campaign_id != "0")
	            {
	                $site_id = $fb_campaign_id;
	            }
	            else if($adwords_campaign_id != "" && $adwords_campaign_id != "0")
	            {
	                $site_id = $adwords_campaign_id;
	            }
	            
	            if($site_id != "")
	            {
	                $sql = "UPDATE tbl_user_retention_mobile_log SET site_id = '$site_id' WHERE rtidx = $rtidx";

	                $db_main2->execute($sql);
	            }
	        }
	    }
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	}	
	
	$db_main->end();
	$db_main2->end();
	$db_mobile->end();
?>