<?
	include("../../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();

	$db_main->execute("SET wait_timeout=3600");
		
	try 
	{
		// AND productkey IN ('com.doubleugames.take5.amazon.0000','com.doubleugames.take5.amazon.0002','com.doubleugames.take5.amazon.0003','com.doubleugames.take5.amazon.0004','com.doubleugames.take5.amazon.0005','com.doubleugames.take5.amazon.0006','com.doubleugames.take5.amazon.0007','com.doubleugames.take5.amazon.0100','com.doubleugames.take5.amazon.0503')
		
		$sql = "SELECT useridx, orderidx, receipt, signature, t1.money, t1.writedate FROM tbl_product_order_mobile t1  WHERE os_type = 3 AND  useridx > 20000 and status = 5";		
		$receipt_list = $db_main->gettotallist($sql);
		
		$total_money = 0;
		
		for ($i=0; $i<sizeof($receipt_list); $i++)
		{
			$useridx = $receipt_list[$i]["useridx"];			
			$receipt = $receipt_list[$i]["receipt"];			
			$orderidx = $receipt_list[$i]["orderidx"];
			$money = $receipt_list[$i]["money"];
			$signature = $receipt_list[$i]["signature"];
			$writedate = $receipt_list[$i]["writedate"];
			
			// verify the receipt
			$sharedsecret = "2:t-5NYpQx-KjqMU5WYE7umFxbYS8Zo-s02KpyqyqGa8I7bFTCk-CwWRqo31ONVBzg:OSO6YhmCPL-Iv0xTHYrTAw==";
				
			$verify_url = "https://appstore-sdk.amazon.com/version/1.0/verifyReceiptId/developer/".$sharedsecret."/user/$signature/receiptId/$receipt";
				
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $verify_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$head = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);			
			curl_close($ch);
			
			$amazon_objs = json_decode($head, true);
				
			$isexception = false;
				
			if($receipt == "" || $signature == "")
				$isexception = true;
			
			if($amazon_objs["testTransaction"] == 1)
			{
				write_log($useridx."|".$orderidx."|".$amazon_objs["testTransaction"]);
				
				$sql = "UPDATE tbl_product_order_mobile SET status = 6 WHERE useridx = $useridx AND orderidx = $orderidx";
				$db_main->execute($sql);	
			}			
				
		}
		
		
	}
	catch(Exception $e)
	{
	}
	
	$db_main->end();	
?>