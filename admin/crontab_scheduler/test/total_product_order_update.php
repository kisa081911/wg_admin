<?
	include("../../common/common_include.inc.php");

	$slave_main = new CDatabase_Main();
	$db_other = new CDatabase_Other();
	
	$slave_main->execute("SET wait_timeout=3600");
	$db_other->execute("SET wait_timeout=3600");
	
	try
	{
		$yesterday = date("Y-m-d",strtotime("-1 days"));
		$today = date("Y-m-d");
		
		$sql = "SELECT orderidx, useridx, gift_status, gift_coin  FROM `tbl_product_order_mobile` WHERE os_type = 3 AND useridx > 20000 AND gift_coin > 0 AND writedate < '2017-07-06 00:00:00';";	
		$product_order = $slave_main->gettotallist($sql);
		
		$insert_sql = "";
		$insert_cnt = 0;
		
		for($i=0; $i<sizeof($product_order); $i++)
		{
			$org_orderidx = $product_order[$i]['orderidx'];
			$useridx = $product_order[$i]['useridx'];
			$gift_status = $product_order[$i]['gift_status'];
			$gift_coin = $product_order[$i]['gift_coin'];
			
			$update_sql = "UPDATE tbl_product_order_all SET gift_status = $gift_status, gift_coin = $gift_coin WHERE os_type = 3 AND useridx = $useridx AND org_orderidx = $org_orderidx;";
			$db_other->execute($update_sql);
			
			$insert_cnt++;
			
			if($insert_cnt == 500)
				sleep(1);
		}		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>