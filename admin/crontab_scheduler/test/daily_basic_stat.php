<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/stats/daily_basic_stat") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/stats/daily_basic_stat") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("stats/daily_basic_stat Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$db_slave = new CDatabase_Slave_Main();
	$db2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_other = new CDatabase_Other();
	$db_game = new CDatabase_Game();
	$db_redshift = new CDatabase_Redshift();
	
	$today = date("Y-m-d", time() - 60 * 60 * 24 * 2);
	$current_date = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	
	// 22.전체 VIP 유저수 
	// 최근 52주
	$sql = "SELECT ROUND(AVG(vip_level56)) AS vip_level56, ROUND(AVG(vip_level78)) AS vip_level78, ROUND(AVG(vip_level910)) AS vip_level910 ".
			" FROM ( ".
				" SELECT today, SUM(vip_level_5 + vip_level_6) AS vip_level56,SUM(vip_level_7 + vip_level_8) AS vip_level78,SUM(vip_level_9 + vip_level_10) AS vip_level910 ".
				" FROM `tbl_vip_level_stat_log` ".
				" WHERE TYPE = 0 AND DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(),INTERVAL 1 DAY), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 1 DAY), '%Y-%m-%d') ".
				" GROUP BY today ".
			" ) t1";
	$total_vip_count_52week = $db_analysis->getarray($sql);
	
	$total_vip56_count_52week = $total_vip_count_52week['vip_level56'];
	$total_vip78_count_52week = $total_vip_count_52week['vip_level78'];
	$total_vip910_count_52week = $total_vip_count_52week['vip_level910'];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, web_sub1, web_sub2) ".
			"VALUES('$today', 22, 1, $total_vip56_count_52week, $total_vip78_count_52week, $total_vip910_count_52week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2);";
	$db_other->execute($sql);
	// 최근 13주
	$sql = "SELECT ROUND(AVG(vip_level56)) AS vip_level56, ROUND(AVG(vip_level78)) AS vip_level78, ROUND(AVG(vip_level910)) AS vip_level910 ".
			" FROM ( ".
			" SELECT today, SUM(vip_level_5 + vip_level_6) AS vip_level56,SUM(vip_level_7 + vip_level_8) AS vip_level78,SUM(vip_level_9 + vip_level_10) AS vip_level910 ".
			" FROM `tbl_vip_level_stat_log` ".
			" WHERE TYPE = 0 AND DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(),INTERVAL 1 DAY), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 1 DAY), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$total_vip_count_13week = $db_analysis->getarray($sql);
	
	$total_vip56_count_13week = $total_vip_count_13week['vip_level56'];
	$total_vip78_count_13week = $total_vip_count_13week['vip_level78'];
	$total_vip910_count_13week = $total_vip_count_13week['vip_level910'];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, web_sub1, web_sub2) ".
			"VALUES('$today', 22, 2, $total_vip56_count_13week, $total_vip78_count_13week, $total_vip910_count_13week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2);";
	$db_other->execute($sql);
	// 최근 4주
	$sql = "SELECT ROUND(AVG(vip_level56)) AS vip_level56, ROUND(AVG(vip_level78)) AS vip_level78, ROUND(AVG(vip_level910)) AS vip_level910 ".
			" FROM ( ".
			" SELECT today, SUM(vip_level_5 + vip_level_6) AS vip_level56,SUM(vip_level_7 + vip_level_8) AS vip_level78,SUM(vip_level_9 + vip_level_10) AS vip_level910 ".
			" FROM `tbl_vip_level_stat_log` ".
			" WHERE TYPE = 0 AND DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(),INTERVAL 1 DAY), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 1 DAY), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$total_vip_count_4week = $db_analysis->getarray($sql);
	
	$total_vip56_count_4week = $total_vip_count_4week['vip_level56'];
	$total_vip78_count_4week = $total_vip_count_4week['vip_level78'];
	$total_vip910_count_4week = $total_vip_count_4week['vip_level910'];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, web_sub1, web_sub2) ".
			"VALUES('$today', 22, 3, $total_vip56_count_4week, $total_vip78_count_4week, $total_vip910_count_4week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2);";
	$db_other->execute($sql);
	// 최근 1주
	$sql = "SELECT ROUND(AVG(vip_level56)) AS vip_level56, ROUND(AVG(vip_level78)) AS vip_level78, ROUND(AVG(vip_level910)) AS vip_level910 ".
			" FROM ( ".
			" SELECT today, SUM(vip_level_5 + vip_level_6) AS vip_level56,SUM(vip_level_7 + vip_level_8) AS vip_level78,SUM(vip_level_9 + vip_level_10) AS vip_level910 ".
			" FROM `tbl_vip_level_stat_log` ".
			" WHERE TYPE = 0 AND DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(),INTERVAL 1 DAY), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 1 DAY), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$total_vip_count_1week = $db_analysis->getarray($sql);
	
	$total_vip56_count_1week = $total_vip_count_1week['vip_level56'];
	$total_vip78_count_1week = $total_vip_count_1week['vip_level78'];
	$total_vip910_count_1week = $total_vip_count_1week['vip_level910'];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, web_sub1, web_sub2) ".
			"VALUES('$today', 22, 4, $total_vip56_count_1week, $total_vip78_count_1week, $total_vip910_count_1week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2);";
	$db_other->execute($sql);
	// 어제
	$sql = "SELECT ROUND(AVG(vip_level56)) AS vip_level56, ROUND(AVG(vip_level78)) AS vip_level78, ROUND(AVG(vip_level910)) AS vip_level910 ".
			" FROM ( ".
			" SELECT today, SUM(vip_level_5 + vip_level_6) AS vip_level56,SUM(vip_level_7 + vip_level_8) AS vip_level78,SUM(vip_level_9 + vip_level_10) AS vip_level910 ".
			" FROM `tbl_vip_level_stat_log` ".
			" WHERE TYPE = 0 AND DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(),INTERVAL 1 DAY), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 1 DAY), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$total_vip_count_day = $db_analysis->getarray($sql);
	
	$total_vip56_count_day = $total_vip_count_day['vip_level56'];
	$total_vip78_count_day = $total_vip_count_day['vip_level78'];
	$total_vip910_count_day = $total_vip_count_day['vip_level910'];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, web_sub1, web_sub2) ".
			"VALUES('$today', 22, 5, $total_vip56_count_day, $total_vip78_count_day, $total_vip910_count_day) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	$sql = "SELECT ROUND(AVG(vip_level56)) AS vip_level56, ROUND(AVG(vip_level78)) AS vip_level78, ROUND(AVG(vip_level910)) AS vip_level910 ". 
			 " FROM ( ".
				 " SELECT today, SUM(vip_level_5 + vip_level_6) AS vip_level56,SUM(vip_level_7 + vip_level_8) AS vip_level78,SUM(vip_level_9 + vip_level_10) AS vip_level910 ". 
				 " FROM `tbl_vip_level_stat_log` ". 
				 " WHERE TYPE = 0 AND DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(),INTERVAL 1 DAY), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(),INTERVAL 1 DAY), INTERVAL 1 WEEK), '%Y-%m-%d') ".
				 " AND DAYOFWEEK(DATE_SUB(DATE_SUB(NOW(),INTERVAL 1 DAY), INTERVAL 1 DAY)) = DAYOFWEEK(today) ".
				 " GROUP BY today ".
			 " ) t1";
	$total_vip_count_dayweek = $db_analysis->getarray($sql);
	
	$total_vip56_count_dayweek = $total_vip_count_dayweek['vip_level56'];
	$total_vip78_count_dayweek = $total_vip_count_dayweek['vip_level78'];
	$total_vip910_count_dayweek = $total_vip_count_dayweek['vip_level910'];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, web_sub1, web_sub2) ".
			"VALUES('$today', 22, 6, $total_vip56_count_dayweek, $total_vip78_count_dayweek, $total_vip910_count_dayweek) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2);";
	$db_other->execute($sql);
	
	$db_slave->end();
	$db2->end();
	$db_analysis->end();
	$db_other->end();
	$db_redshift->end();
	$db_game->end();
?>