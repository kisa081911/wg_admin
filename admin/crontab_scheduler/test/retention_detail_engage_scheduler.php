<?
	include("../../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();
	$db_livestats = new CDatabase_Livestats();
	$db_other = new CDatabase_Other();
	
	$db_main->execute("SET wait_timeout=72000");
	$db_analysis->execute("SET wait_timeout=72000");
	$db_livestats->execute("SET wait_timeout=72000");
	$db_other->execute("SET wait_timeout=72000");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$port = "";
	
	$str_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/retention_detail_engage_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/retention_detail_engage_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("retention_engage_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}



	// tbl_user 통합쿼리 넣어서  anaylsisDB_qa 테이블 업데이트
	try
	{
		$index = 1;
		$indexMinus = 0;
		
		$sql = "SELECT ".
				"	DATE_FORMAT(createdate, '%Y-%m-%d') AS createdate, ".
				"	platform, adflag, ".
				"	COUNT(*) AS total_count, ".
				"	ABS(IFNULL( ".
				"		SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=t1.useridx) THEN 1 ELSE 0 END) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=t1.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=t1.useridx) THEN 1 ELSE 0 END),0) ".
				"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=t1.useridx) THEN 1 ELSE 0 END),0) ".
				"	,0)) AS play_count, ".
				"	SUM(INSTALL) AS install_count, ".			
				"	SUM(CASE WHEN tutorial >= 3 THEN 1 ELSE 0 END) AS tutorial_count, ".
				"	SUM(CASE WHEN daylogincount >= 3 THEN 1 ELSE 0 END) AS loyal_count ".
				"  FROM ".
				"  tbl_user_ext t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx ".
				"  WHERE t1.useridx > $str_useridx AND createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL '$index' DAY), '%Y-%m-%d') ".
				"  AND createdate < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL '$indexMinus' DAY), '%Y-%m-%d') ".
				"  GROUP BY DATE_FORMAT(createdate, '%Y-%m-%d'), platform, adflag";
		
		$retention_update_list = $db_main ->gettotallist($sql);

		for($i=0; $i<sizeof($retention_update_list); $i++)
		{
			$reg_date = $retention_update_list[$i]["createdate"];
			$platform = $retention_update_list[$i]["platform"];
			$adflag = $retention_update_list[$i]["adflag"];
			$total_count = $retention_update_list[$i]["total_count"];
			$play_count = $retention_update_list[$i]["play_count"];
			$install_count = $retention_update_list[$i]["install_count"];
			$tutorial_count = $retention_update_list[$i]["tutorial_count"];
			$loyal_count = $retention_update_list[$i]["loyal_count"];
				
			$sql = "INSERT INTO tbl_user_detail_retention_daily_qa(reg_date, platform, adflag, reg_count, play_count, install_count, tutorial_count, loyal_count) ".
						" VALUES('$reg_date', $platform, '$adflag', $total_count, $play_count, $install_count, $tutorial_count, $loyal_count) ON DUPLICATE KEY UPDATE reg_count=VALUES(reg_count), play_count=VALUES(play_count), install_count=VALUES(install_count), tutorial_count=values(tutorial_count), loyal_count=values(loyal_count);";
			$db_other->execute($sql);
		}
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = 0;
	}
	
	$db_main->end();
	$db_analysis->end();
	$db_livestats->end();
	$db_other->end();
?>