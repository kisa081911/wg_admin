<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	ini_set("memory_limit", "-1");
	
	$db_main2 = new CDatabase_Main2();
	$db_redshift = new CDatabase_Redshift();
	
	try 
	{
		$sql = "select * from _tmp_t5_delete_marking_log_list";
		$log_list = $db_redshift->gettotallist($sql);
		
		$delete_cnt = 0;
		$delete_str = "";
		
		for($i=0; $i<sizeof($log_list); $i++)
		{
			$logidx = $log_list[$i]["logidx"];
			
			if($delete_cnt == 0 && $delete_str == "")
			{
				$delete_str = $logidx;
			}
			else if($delete_cnt < 1000)
			{
				$delete_str .= ",$logidx";
			}			
			else
			{
				$delete_str .= ",$logidx";
				$delete_sql = "DELETE FROM tbl_user_marking_log WHERE logidx IN ($delete_str) AND markingidx = 5";
				$db_main2->execute($delete_sql);
				
				$delete_cnt = 0;
				$delete_str = "";				
			}		
		}
		
		if($delete_str != "")
		{
			$delete_sql = "DELETE FROM tbl_user_marking_log WHERE logidx IN ($delete_str) AND markingidx = 5";
			$db_main2->execute($delete_sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
	$db_redshift->end();
?>