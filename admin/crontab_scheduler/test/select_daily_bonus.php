<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	
	$filename = "basic_product.xls";
	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
			"<table border=1>".
			"<tr>".
							"<td>actionidx</td>".
							"<td>useridx</td>".
							"<td>coin</td>".
							"<td>maxcoin</td>".
							"<td>mincoin</td>".
							"<td>writedate</td>".
						"</tr>";
	
	ini_set("memory_limit", "-1");	
	$db_analysis = new CDatabase_Analysis();
	$db_main = new CDatabase_Main();
	
	try
	{
// 		$today = date("Y-m-d", time() - 60 * 60 * 24 * 1);

			$db_redshift = new CDatabase_Redshift();
			
			//t5_user_retention_log
			$sql = "select * from t5_user_freecoin_log where type in (107,67,57) and writedate > '2017-10-24 00:00:00' and writedate < '2017-11-02 00:00:00' and category != 0";
			$date_list= $db_redshift->gettotallist($sql);
			

			for ($i = 0; $i < sizeof($date_list); $i++) {
				
				$useridx = $date_list[$i]['useridx'];
				$writedate = $date_list[$i]['writedate'];
				$category = $date_list[$i]['category'];
				
				if($category == 1)
				{
// 					$select_sql ="SELECT actionidx,useridx,coin, '$writedate' as writedate, IF(MIN(coin)*2 < MAX(coin),1,0) AS ischeck, MAX(coin) AS maxcoin, MIN(coin) AS mincoin FROM user_action_log_ios_".($useridx%10)." WHERE useridx = $useridx AND writedate > DATE_SUB('$writedate', INTERVAL 3 MINUTE)  AND writedate < DATE_ADD('$writedate', INTERVAL 3 MINUTE) ";
// 					$select_sql ="SELECT '$useridx' as useridx
// 								  , '$writedate' as writedate
// 								  , (SELECT MAX(coin) FROM  user_action_log_ios_".($useridx%10)." WHERE useridx = '$useridx'  AND writedate < DATE_ADD('$writedate', INTERVAL 3 MINUTE) AND writedate > '$writedate') AS maxcoin
// 	  							  , (SELECT MAX(coin) FROM  user_action_log_ios_".($useridx%10)." WHERE useridx = '$useridx'  AND writedate > DATE_SUB('$writedate', INTERVAL 3 MINUTE) AND writedate < '$writedate') AS mincoin ";
					$select_sql ="SELECT '$useridx' as useridx
								  , '$writedate' as writedate
								  , (SELECT coin FROM  user_action_log_ios_".($useridx%10)." WHERE useridx = '$useridx'  AND writedate > '$writedate' ORDER BY writedate ASC LIMIT 1) AS maxcoin
	  							  , (SELECT coin FROM  user_action_log_ios_".($useridx%10)." WHERE useridx = '$useridx'  AND writedate < '$writedate' ORDER BY writedate DESC LIMIT 1) AS mincoin ";
				}
				else if($category == 2)
				{
// 					$select_sql ="SELECT actionidx,useridx,coin, '$writedate' as writedate, IF(MIN(coin)*2 < MAX(coin),1,0) AS ischeck, MAX(coin) AS maxcoin, MIN(coin) AS mincoin FROM user_action_log_android_".($useridx%10)." WHERE useridx = $useridx AND writedate > DATE_SUB('$writedate', INTERVAL 3 MINUTE) AND writedate < DATE_ADD('$writedate', INTERVAL 3 MINUTE) ";
					$select_sql ="SELECT '$useridx' as useridx
								, '$writedate' as writedate
								, (SELECT coin FROM  user_action_log_android_".($useridx%10)." WHERE useridx = '$useridx'  AND writedate > '$writedate' ORDER BY writedate ASC LIMIT 1) AS maxcoin
								, (SELECT coin FROM  user_action_log_android_".($useridx%10)." WHERE useridx = '$useridx'  AND writedate < '$writedate' ORDER BY writedate DESC LIMIT 1) AS mincoin ";
				}
				else if($category == 3)
				{
// 					$select_sql ="SELECT actionidx,useridx,coin, '$writedate' as writedate, IF(MIN(coin)*2 < MAX(coin),1,0) AS ischeck, MAX(coin) AS maxcoin, MIN(coin) AS mincoin FROM user_action_log_amazon WHERE useridx = $useridx AND writedate > DATE_SUB('$writedate', INTERVAL 3 MINUTE) AND writedate < DATE_ADD('$writedate', INTERVAL 3 MINUTE)";
					$select_sql ="SELECT '$useridx' as useridx
					, '$writedate' as writedate
					, (SELECT coin FROM  user_action_log_amazon WHERE useridx = '$useridx'  AND writedate > '$writedate' ORDER BY writedate ASC LIMIT 1) AS maxcoin
					, (SELECT coin FROM  user_action_log_amazon WHERE useridx = '$useridx'  AND writedate < '$writedate' ORDER BY writedate DESC LIMIT 1) AS mincoin ";
				}
				
				$action_list = $db_analysis->gettotallist($select_sql);
				$coin = $db_main->getvalue("SELECT coin FROM tbl_user WHERE useridx = '$useridx'");
				
				for ($d = 0; $d < sizeof($action_list); $d++) {
					
					$useridx = $action_list[$d]["useridx"];
					$writedate = $action_list[$d]["writedate"];
					$maxcoin = ($action_list[$d]["maxcoin"] == "")?0:$action_list[$d]["maxcoin"];
					$mincoin = ($action_list[$d]["mincoin"] == "")?0:$action_list[$d]["mincoin"];
					
					if(($mincoin*2) < $maxcoin)
					{
						if($mincoin > 100000000)
						{
							$excel_contents .= "<tr>".
										"<td>".$useridx."</td>".
										"<td>".$coin."</td>".
										"<td>".$maxcoin."</td>".
										"<td>".$mincoin."</td>".
										"<td>".$writedate."</td>".
									"</tr>";
						}
					}
					
				}			
			}
			
			$excel_contents .= "</table>";
			
			Header("Content-type: application/x-msdownload");
			Header("Content-type: application/x-msexcel");
			Header("Content-Disposition: attachment; filename=$filename");
			
			echo($excel_contents);
			
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	$db_analysis->end();
	$db_main->end();
?>