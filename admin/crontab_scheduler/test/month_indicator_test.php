<?
	include("../../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();
	$db_mobile = new CDatabase_Mobile();
	
	$db_main->execute("SET wait_timeout=72000");
	$db_analysis->execute("SET wait_timeout=72000");
	$db_mobile->execute("SET wait_timeout=72000");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$port = "";
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}

	
	try
	{
		$indicator_month_log = date("Y-m", strtotime("-1 month"));
		$indicator_current_month = date("Y-m")."-01";
		$indicator_month = date("Y-m", strtotime("-1 month"))."-01";		
		$indicator_before_month = date("Y-m", strtotime("-2 month"))."-01";
		$indicator_two_before_month = date("Y-m", strtotime("-3 month"))."-01";
		
		// 1.신규회원가입수
		// (1) 전체
		
		// 당월
		$sql = "SELECT IFNULL(COUNT(useridx),0) FROM tbl_user_ext WHERE createdate >= '$indicator_month 00:00:00' AND createdate < '$indicator_current_month 00:00:00';";
		$month_all_join = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(COUNT(useridx),0) FROM tbl_user_ext WHERE createdate >= '$indicator_before_month 00:00:00' AND createdate < '$indicator_month 00:00:00';";
		$before_month_all_join = $db_main->getvalue($sql);
		
		//전전월
		$sql = "SELECT IFNULL(COUNT(useridx),0) FROM tbl_user_ext WHERE createdate >= '$indicator_two_before_month 00:00:00' AND createdate < '$indicator_before_month 00:00:00';";
		$before_two_month_all_join = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 10, $month_all_join, $before_month_all_join, $before_two_month_all_join) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);
		
		// (2) facebook
		// 당월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE '$indicator_month 00:00:00' <= createdate AND createdate < '$indicator_current_month 00:00:00' AND platform = 0";
		$month_facebook_join = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE '$indicator_before_month 00:00:00' <= createdate AND createdate < '$indicator_month 00:00:00' AND platform = 0";
		$before_month_facebook_join = $db_main->getvalue($sql);
		
		// 전전월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE '$indicator_two_before_month 00:00:00' <= createdate AND createdate < '$indicator_before_month 00:00:00' AND platform = 0";
		$before_two_month_facebook_join = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 11, $month_facebook_join, $before_month_facebook_join, $before_two_month_facebook_join) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);
		
		// (3) Apple
		// 당월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE '$indicator_month 00:00:00' <= createdate AND createdate < '$indicator_current_month 00:00:00' AND platform = 1";
		$month_apple_join = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE '$indicator_before_month 00:00:00' <= createdate AND createdate < '$indicator_month 00:00:00' AND platform = 1";
		$before_month_apple_join = $db_main->getvalue($sql);
		
		// 전전월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE '$indicator_two_before_month 00:00:00' <= createdate AND createdate < '$indicator_before_month 00:00:00' AND platform = 1";
		$before_two_month_apple_join = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 12, $month_apple_join, $before_month_apple_join, $before_two_month_apple_join) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);
		
		// (4) Google
		// 당월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE '$indicator_month 00:00:00' <= createdate AND createdate < '$indicator_current_month 00:00:00' AND platform = 2";
		$month_google_join = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE '$indicator_before_month 00:00:00' <= createdate AND createdate < '$indicator_month 00:00:00' AND platform = 2";
		$before_month_google_join = $db_main->getvalue($sql);
		
		// 전전월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE '$indicator_two_before_month 00:00:00' <= createdate AND createdate < '$indicator_before_month 00:00:00' AND platform = 2";
		$before_two_month_google_join = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 13, $month_google_join, $before_month_google_join, $before_two_month_google_join) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);
		
		// (5) Amazon
		// 당월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE '$indicator_month 00:00:00' <= createdate AND createdate < '$indicator_current_month 00:00:00' AND platform = 3";
		$month_amazon_join = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE '$indicator_before_month 00:00:00' <= createdate AND createdate < '$indicator_month 00:00:00' AND platform = 3";
		$before_month_amazon_join = $db_main->getvalue($sql);
		
		// 전전월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE '$indicator_two_before_month 00:00:00' <= createdate AND createdate < '$indicator_before_month 00:00:00' AND platform = 3";
		$before_two_month_amazon_join = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 14, $month_amazon_join, $before_month_amazon_join, $before_two_month_amazon_join) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);
		
		// 2.활동유저수
		// (1) 전체
		// 당월
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_month 00:00:00' <= logindate AND logindate < '$indicator_current_month 00:00:00'";
		$month_all_active = $db_main->getvalue($sql);
		
		//전월
		$sql = "SELECT month FROM game_indicator_month WHERE writedate = '".date("Y-m", strtotime("-2 month"))."' AND typeid = 20";
		$before_month_all_active = $db_analysis->getvalue($sql);
		$before_month_all_active = ($before_month_all_active == "") ? 0 : $before_month_all_active;
		
		//전전월
		$sql = "SELECT month FROM game_indicator_month WHERE writedate = '".date("Y-m", strtotime("-3 month"))."' AND typeid = 20";
		$before_two_month_all_active = $db_analysis->getvalue($sql);
		$before_two_month_all_active = ($before_two_month_all_active == "") ? 0 : $before_two_month_all_active;
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 20, $month_all_active, $before_month_all_active, $before_two_month_all_active) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);
		
		// (2) Facebook
		// 당월
		//$sql = "SELECT COUNT(useridx) FROM user_facebook_login_log WHERE '$indicator_month 00:00:00' <= logindate AND logindate < '$indicator_current_month 00:00:00'";
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE platform = 0 AND  '$indicator_month 00:00:00' <= logindate AND logindate < '$indicator_current_month 00:00:00'";
		$month_facebook_active = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(SUM(MONTH), 0) FROM game_indicator_month WHERE writedate = '".date("Y-m", strtotime("-2 month"))."' AND typeid = 21";
		$before_month_facebook_active = $db_analysis->getvalue($sql);
		
		// 전전월
		$sql = "SELECT IFNULL(SUM(MONTH), 0) FROM game_indicator_month WHERE writedate = '".date("Y-m", strtotime("-3 month"))."' AND typeid = 21";
		$before_two_month_facebook_active = $db_analysis->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 21, $month_facebook_active, $before_month_facebook_active, $before_two_month_facebook_active) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);
		
		// (3) Apple
		// 당월
		$sql = "SELECT COUNT(DISTINCT useridx) FROM tbl_user_mobile_connection_log WHERE os_type = 1 AND '$indicator_month 00:00:00' <= logindate AND logindate < '$indicator_current_month 00:00:00'";
		$month_apple_active = $db_mobile->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(SUM(MONTH), 0) FROM game_indicator_month WHERE writedate = '".date("Y-m", strtotime("-2 month"))."' AND typeid = 22";
		$before_month_apple_active = $db_analysis->getvalue($sql);
		
		// 전전월
		$sql = "SELECT IFNULL(SUM(MONTH), 0) FROM game_indicator_month WHERE writedate = '".date("Y-m", strtotime("-3 month"))."' AND typeid = 22";
		$before_two_month_apple_active = $db_analysis->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 22, $month_apple_active, $before_month_apple_active, $before_two_month_apple_active) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);
		
		// (4) Google
		// 당월
		$sql = "SELECT COUNT(DISTINCT useridx) FROM tbl_user_mobile_connection_log WHERE os_type = 2 AND '$indicator_month 00:00:00' <= logindate AND logindate < '$indicator_current_month 00:00:00'";
		$month_google_active = $db_mobile->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(SUM(MONTH), 0) FROM game_indicator_month WHERE writedate = '".date("Y-m", strtotime("-2 month"))."' AND typeid = 23";
		$before_month_google_active = $db_analysis->getvalue($sql);
		
		// 전전월
		$sql = "SELECT IFNULL(SUM(MONTH), 0) FROM game_indicator_month WHERE writedate = '".date("Y-m", strtotime("-3 month"))."' AND typeid = 23";
		$before_two_month_google_active = $db_analysis->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 23, $month_google_active, $before_month_google_active, $before_two_month_google_active) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);
		
		// (5) Amazon
		// 당월
		$sql = "SELECT COUNT(DISTINCT useridx) FROM tbl_user_mobile_connection_log WHERE os_type = 3 AND '$indicator_month 00:00:00' <= logindate AND logindate < '$indicator_current_month 00:00:00'";
		$month_amazon_active = $db_mobile->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(SUM(MONTH), 0) FROM game_indicator_month WHERE writedate = '".date("Y-m", strtotime("-2 month"))."' AND typeid = 24";
		$before_month_amazon_active = $db_analysis->getvalue($sql);
		
		// 전전월
		$sql = "SELECT IFNULL(SUM(MONTH), 0) FROM game_indicator_month WHERE writedate = '".date("Y-m", strtotime("-3 month"))."' AND typeid = 24";
		$before_two_month_amazon_active = $db_analysis->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 24, $month_amazon_active, $before_month_amazon_active, $before_two_month_amazon_active) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);
		
		// 3.결제유저수
		// (1) 전체			
		// 당월
		$sql = "SELECT IFNULL(COUNT(DISTINCT useridx), 0) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND writedate >= '$indicator_month 00:00:00' AND writedate < '$indicator_current_month 00:00:00' ".
				"	UNION ALL ".
				"	SELECT useridx FROM tbl_product_order_earn WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate < '$indicator_current_month 00:00:00' ".
				"	UNION ALL ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate < '$indicator_current_month 00:00:00' ".
				") t1";

		$month_purchase_all_user = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(COUNT(DISTINCT useridx), 0) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND writedate >= '$indicator_before_month 00:00:00' AND writedate < '$indicator_month 00:00:00' ".
				"	UNION ALL ".
				"	SELECT useridx FROM tbl_product_order_earn WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_before_month 00:00:00' <= writedate AND writedate < '$indicator_month 00:00:00' ".
				"	UNION ALL ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_before_month 00:00:00' <= writedate AND writedate < '$indicator_month 00:00:00' ".
				") t1";
		
		$before_month_purchase_all_user = $db_main->getvalue($sql);
		
		// 전전월
		$sql = "SELECT IFNULL(COUNT(DISTINCT useridx), 0) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND writedate >= '$indicator_two_before_month 00:00:00' AND writedate < '$indicator_before_month 00:00:00' ".
				"	UNION ALL ".
				"	SELECT useridx FROM tbl_product_order_earn WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_two_before_month 00:00:00' <= writedate AND writedate < '$indicator_before_month 00:00:00' ".
				"	UNION ALL ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_two_before_month 00:00:00' <= writedate AND writedate < '$indicator_before_month 00:00:00' ".
				") t1";
		
		$before_two_month_purchase_all_user = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 30, $month_purchase_all_user, $before_month_purchase_all_user, $before_two_month_purchase_all_user) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);
		
		// (2) facebook
		// 당월
		$sql = 	"SELECT IFNULL(COUNT(DISTINCT useridx), 0) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate < '$indicator_current_month 00:00:00' ".
				"	UNION ALL".
				"	SELECT useridx FROM tbl_product_order_earn WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate < '$indicator_current_month 00:00:00' ".
				") t1";
			
		$month_purchase_facebook_user = $db_main->getvalue($sql);
		
		// 전월
		$sql = 	"SELECT IFNULL(COUNT(DISTINCT useridx), 0) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_before_month 00:00:00' <= writedate AND writedate < '$indicator_month 00:00:00' ".
				"	UNION ALL".
				"	SELECT useridx FROM tbl_product_order_earn WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_before_month 00:00:00' <= writedate AND writedate < '$indicator_month 00:00:00' ".
				") t1";
		
		$before_month_purchase_facebook_user = $db_main->getvalue($sql);
		
		// 전전월
		$sql = 	"SELECT IFNULL(COUNT(DISTINCT useridx), 0) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_two_before_month 00:00:00' <= writedate AND writedate < '$indicator_before_month 00:00:00' ".
				"	UNION ALL".
				"	SELECT useridx FROM tbl_product_order_earn WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_two_before_month 00:00:00' <= writedate AND writedate < '$indicator_before_month 00:00:00' ".
				") t1";
		
		$before_two_month_purchase_facebook_user = $db_main->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 31, $month_purchase_facebook_user, $before_month_purchase_facebook_user, $before_two_month_purchase_facebook_user) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);
		
		// (3) Apple
		// 당월
		$sql = 	"SELECT IFNULL(COUNT(DISTINCT useridx), 0) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 1 AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate < '$indicator_current_month 00:00:00' ".
				") t1";
		
		$month_purchase_apple_user = $db_main->getvalue($sql);
		
		// 전월
		$sql = 	"SELECT IFNULL(COUNT(DISTINCT useridx), 0) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 1 AND STATUS = 1 AND '$indicator_before_month 00:00:00' <= writedate AND writedate < '$indicator_month 00:00:00' ".
				") t1";
		
		$before_month_purchase_apple_user = $db_main->getvalue($sql);
		
		// 전전월
		$sql = 	"SELECT IFNULL(COUNT(DISTINCT useridx), 0) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 1 AND STATUS = 1 AND '$indicator_two_before_month 00:00:00' <= writedate AND writedate < '$indicator_before_month 00:00:00' ".
				") t1";
		
		$before_two_month_purchase_apple_user = $db_main->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 32, $month_purchase_apple_user, $before_month_purchase_apple_user, $before_two_month_purchase_apple_user) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);
		
		// (4) Google
		// 당월
		$sql = 	"SELECT IFNULL(COUNT(DISTINCT useridx), 0) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 2 AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate < '$indicator_current_month 00:00:00' ".
				") t1";
									
		$month_purchase_google_user = $db_main->getvalue($sql);
		
		// 전월
		$sql = 	"SELECT IFNULL(COUNT(DISTINCT useridx), 0) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 2 AND STATUS = 1 AND '$indicator_before_month 00:00:00' <= writedate AND writedate < '$indicator_month 00:00:00' ".
				") t1";
		
		$before_month_purchase_google_user = $db_main->getvalue($sql);
		
		// 전전월
		$sql = 	"SELECT IFNULL(COUNT(DISTINCT useridx), 0) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 2 AND STATUS = 1 AND '$indicator_two_before_month 00:00:00' <= writedate AND writedate < '$indicator_before_month 00:00:00' ".
				") t1";

		$before_two_month_purchase_google_user = $db_main->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 33, $month_purchase_google_user, $before_month_purchase_google_user, $before_two_month_purchase_google_user) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
						
		$db_analysis->execute($sql);
		
		// (5) Amazon
		// 당월
		$sql = 	"SELECT IFNULL(COUNT(DISTINCT useridx), 0) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 3 AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate < '$indicator_current_month 00:00:00' ".
				") t1";
		
		$month_purchase_amazon_user = $db_main->getvalue($sql);
		
		// 전월
		$sql = 	"SELECT IFNULL(COUNT(DISTINCT useridx), 0) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 3 AND STATUS = 1 AND '$indicator_before_month 00:00:00' <= writedate AND writedate < '$indicator_month 00:00:00' ".
				") t1";
		
				$before_month_purchase_amazon_user = $db_main->getvalue($sql);
		
		// 전전월
		$sql = 	"SELECT IFNULL(COUNT(DISTINCT useridx), 0) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 3 AND STATUS = 1 AND '$indicator_two_before_month 00:00:00' <= writedate AND writedate < '$indicator_before_month 00:00:00' ".
				") t1";
		
		$before_two_month_purchase_amazon_user = $db_main->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 34, $month_purchase_amazon_user, $before_month_purchase_amazon_user, $before_two_month_purchase_amazon_user) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
							
		$db_analysis->execute($sql);
		
	
		// 4.결제금액
		// (1) 젠체
		// 당월
		$sql = "SELECT ( ".
				" 	SELECT IFNULL(SUM(facebookcredit), 0) ".
				"	FROM tbl_product_order ".
				"	WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND writedate >= '$indicator_month 00:00:00' AND writedate < '$indicator_current_month 00:00:00' ".
				") + ( ".
				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
				"	FROM tbl_product_order_earn ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate < '$indicator_current_month 00:00:00' ".
				") + ( ".
				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate < '$indicator_current_month 00:00:00' ".
				") AS total_amount";
		$month_purchase_all_amount = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT ( ".
				" 	SELECT IFNULL(SUM(facebookcredit), 0) ".
				"	FROM tbl_product_order ".
				"	WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND writedate >= '$indicator_before_month 00:00:00' AND writedate < '$indicator_month 00:00:00' ".
				") + ( ".
				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
				"	FROM tbl_product_order_earn ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_before_month 00:00:00' <= writedate AND writedate < '$indicator_month 00:00:00' ".
				") + ( ".
				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_before_month 00:00:00' <= writedate AND writedate < '$indicator_month 00:00:00' ".
				") AS total_amount";
		$before_month_purchase_all_amount = $db_main->getvalue($sql);
		
		
		// 전전월
		$sql = "SELECT ( ".
				" 	SELECT IFNULL(SUM(facebookcredit), 0) ".
				"	FROM tbl_product_order ".
				"	WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND writedate >= '$indicator_two_before_month 00:00:00' AND writedate < '$indicator_before_month 00:00:00' ".
				") + ( ".
				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
				"	FROM tbl_product_order_earn ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_two_before_month 00:00:00' <= writedate AND writedate < '$indicator_before_month 00:00:00' ".
				") + ( ".
				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_two_before_month 00:00:00' <= writedate AND writedate < '$indicator_before_month 00:00:00' ".
				") AS total_amount";
		$before_two_month_purchase_all_amount = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 40, $month_purchase_all_amount, $before_month_purchase_all_amount, $before_two_month_purchase_all_amount) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);

		// (2) facebook
		// 당월
		$sql = "SELECT IFNULL(SUM(facebookcredit), 0) FROM tbl_product_order WHERE useridx > $std_useridx AND status IN (1, 3) AND writedate >= '$indicator_month 00:00:00' AND writedate < '$indicator_current_month 00:00:00'";
		$month_purchase_facebook_amount = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(SUM(facebookcredit), 0) FROM tbl_product_order WHERE useridx > $std_useridx AND status IN (1, 3) AND writedate >= '$indicator_before_month 00:00:00' AND writedate < '$indicator_month 00:00:00'";
		$before_month_purchase_facebook_amount = $db_main->getvalue($sql);
		
		// 전전월
		$sql = "SELECT IFNULL(SUM(facebookcredit), 0) FROM tbl_product_order WHERE useridx > $std_useridx AND status IN (1, 3) AND writedate >= '$indicator_two_before_month 00:00:00' AND writedate < '$indicator_before_month 00:00:00'";
		$before_two_month_purchase_facebook_amount = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 41, $month_purchase_facebook_amount, $before_month_purchase_facebook_amount, $before_two_month_purchase_facebook_amount) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);
		
		// (3) Apple
		// 당월
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 1 AND status = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate < '$indicator_current_month 00:00:00'";
		$month_purchase_apple_amount = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 1 AND status = 1 AND '$indicator_before_month 00:00:00' <= writedate AND writedate < '$indicator_month 00:00:00'";
		$before_month_purchase_apple_amount = $db_main->getvalue($sql);
		
		// 전전월
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 1 AND status = 1 AND '$indicator_two_before_month 00:00:00' <= writedate AND writedate < '$indicator_before_month 00:00:00'";
		$before_two_month_purchase_apple_amount = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 42, $month_purchase_apple_amount, $before_month_purchase_apple_amount, $before_two_month_purchase_apple_amount) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);
		
		// (4) Google
		// 당월
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 2 AND status = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate < '$indicator_current_month 00:00:00'";
		$month_purchase_google_amount = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 2 AND status = 1 AND '$indicator_before_month 00:00:00' <= writedate AND writedate < '$indicator_month 00:00:00'";
		$before_month_purchase_google_amount = $db_main->getvalue($sql);
		
		// 전전월
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 2 AND status = 1 AND '$indicator_two_before_month 00:00:00' <= writedate AND writedate < '$indicator_before_month 00:00:00'";
		$before_two_month_purchase_google_amount = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 43, $month_purchase_google_amount, $before_month_purchase_google_amount, $before_two_month_purchase_google_amount) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);
		
		// (5) Amazon
		// 당월
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 3 AND status = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate < '$indicator_current_month 00:00:00'";
		$month_purchase_amazon_amount = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 3 AND status = 1 AND '$indicator_before_month 00:00:00' <= writedate AND writedate < '$indicator_month 00:00:00'";
		$before_month_purchase_amazon_amount = $db_main->getvalue($sql);
		
		// 전전월
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 3 AND status = 1 AND '$indicator_two_before_month 00:00:00' <= writedate AND writedate < '$indicator_before_month 00:00:00'";
		$before_two_month_purchase_amazon_amount = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 44, $month_purchase_amazon_amount, $before_month_purchase_amazon_amount, $before_two_month_purchase_amazon_amount) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);
		

		// (5) trialpay	
		// 당월
		$sql = "SELECT IFNULL(SUM(money)*10, 0) FROM tbl_product_order_earn WHERE useridx > $std_useridx AND status = 1 AND writedate >= '$indicator_month 00:00:00' AND writedate < '$indicator_current_month 00:00:00'";
		$month_purchase_trialpay_amount = $db_main->getvalue($sql);

		// 전월
		$sql = "SELECT IFNULL(SUM(money)*10, 0) FROM tbl_product_order_earn WHERE useridx > $std_useridx AND status = 1 AND writedate >= '$indicator_before_month 00:00:00' AND writedate < '$indicator_month 00:00:00'";
		$before_month_purchase_trialpay_amount = $db_main->getvalue($sql);
		
		// 전전월
		$sql = "SELECT IFNULL(SUM(money)*10, 0) FROM tbl_product_order_earn WHERE useridx > $std_useridx AND status = 1 AND writedate >= '$indicator_two_before_month 00:00:00' AND writedate < '$indicator_before_month 00:00:00'";
		$before_two_month_purchase_trialpay_amount = $db_main->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 45, $month_purchase_trialpay_amount, $before_month_purchase_trialpay_amount, $before_two_month_purchase_trialpay_amount) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);

		// 5-1.ARPU
		// (1) 전체
		// 당월		
		$month_all_arpu = ($month_all_active == 0) ? 0 : round($month_purchase_all_amount / $month_all_active * 100);
		
		// 전월
		$before_month_all_arpu = ($before_month_all_active == 0) ? 0 : round($before_month_purchase_all_amount / $before_month_all_active * 100);
		
		// 전전월
		$before_two_month_all_arpu = ($before_two_month_all_active == 0) ? 0 : round($before_two_month_purchase_all_amount / $before_two_month_all_active * 100);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 50, $month_all_arpu, $before_month_all_arpu, $before_two_month_all_arpu) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);
		
		// (2) facebook
		// 당월
		$month_facebook_arpu = ($month_facebook_active == 0) ? 0 : round($month_purchase_facebook_amount / $month_facebook_active * 100);
		
		// 전월
		$before_month_facebook_arpu = ($before_month_facebook_active == 0) ? 0 : round($before_month_purchase_facebook_amount / $before_month_facebook_active * 100);
		
		// 전전월
		$before_two_month_facebook_arpu = ($before_two_month_facebook_active == 0) ? 0 : round($before_two_month_purchase_facebook_amount / $before_two_month_facebook_active * 100);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 51, $month_facebook_arpu, $before_month_facebook_arpu, $before_two_month_facebook_arpu) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);
		
		// (3) Apple
		// 당월
		$month_apple_arpu = ($month_apple_active == 0) ? 0 : round($month_purchase_apple_amount / $month_apple_active * 100);
		
		// 전월
		$before_month_apple_arpu = ($before_month_apple_active == 0) ? 0 : round($before_month_purchase_apple_amount / $before_month_apple_active * 100);
		
		// 전전월
		$befor_two_month_apple_arpu = ($before_two_month_apple_active == 0) ? 0 : round($before_two_month_purchase_apple_amount / $before_two_month_apple_active * 100);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 52, $month_apple_arpu, $before_month_apple_arpu, $befor_two_month_apple_arpu) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);
		
		// (4) Google
		// 당월
		$month_google_arpu = ($month_google_active == 0) ? 0 : round($month_purchase_google_amount / $month_google_active * 100);
		
		// 전월
		$before_month_google_arpu = ($before_month_google_active == 0) ? 0 : round($before_month_purchase_google_amount / $before_month_google_active * 100);
		
		// 전전월
		$before_tow_month_google_arpu = ($before_two_month_google_active == 0) ? 0 : round($before_two_month_purchase_google_amount / $before_two_month_google_active * 100);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 53, $month_google_arpu, $before_month_google_arpu, $before_tow_month_google_arpu) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);

		// 6.ARPPU
		// (1) 전체
		// 당월		
		$month_all_arppu = ($month_purchase_all_user == 0) ? 0 : round($month_purchase_all_amount / $month_purchase_all_user * 100);
		
		//전월
		$before_month_all_arppu = ($before_month_purchase_all_user == 0) ? 0 : round($before_month_purchase_all_amount / $before_month_purchase_all_user * 100);
		
		//전전월
		$before_two_month_all_arppu = ($before_two_month_purchase_all_user == 0) ? 0 : round($before_two_month_purchase_all_amount / $before_two_month_purchase_all_user * 100);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 60, $month_all_arppu, $before_month_all_arppu, $before_two_month_all_arppu) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);
		
		// (2) Facebook
		// 당월
		$month_facebook_arppu = ($month_purchase_facebook_user == 0) ? 0 : round(($month_purchase_facebook_amount + $month_purchase_trialpay_amount) / $month_purchase_facebook_user * 100);
		
		// 전월
		$before_month_facebook_arppu = ($before_month_purchase_facebook_user == 0) ? 0 : round(($before_month_purchase_facebook_amount + $before_month_purchase_trialpay_amount) / $before_month_purchase_facebook_user * 100);
		
		// 전전월
		$before_two_month_facebook_arppu = ($before_two_month_purchase_facebook_user == 0) ? 0 : round(($before_two_month_purchase_facebook_amount + $before_two_month_purchase_trialpay_amount) / $before_two_month_purchase_facebook_user * 100);
		
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 61, $month_facebook_arppu, $before_month_facebook_arppu, $before_two_month_facebook_arppu) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);
		
		// (3) Apple
		// 당월
		$month_apple_arppu = ($month_purchase_apple_user == 0) ? 0 : round($month_purchase_apple_amount / $month_purchase_apple_user * 100);
		
		// 전월
		$before_month_apple_arppu = ($before_month_purchase_apple_user == 0) ? 0 : round($before_month_purchase_apple_amount / $before_month_purchase_apple_user * 100);
		
		// 전전월
		$before_two_month_apple_arppu = ($before_two_month_purchase_apple_user == 0) ? 0 : round($before_two_month_purchase_apple_amount / $before_two_month_purchase_apple_user * 100);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 62, $month_apple_arppu, $before_month_apple_arppu, $before_two_month_apple_arppu) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);
		
		// (4) Google
		// 당월
		$month_google_arppu = ($month_purchase_google_user == 0) ? 0 : round($month_purchase_google_amount / $month_purchase_google_user * 100);
		
		// 전월
		$before_month_google_arppu = ($before_month_purchase_google_user == 0) ? 0 : round($before_month_purchase_google_amount / $before_month_purchase_google_user * 100);
		
		// 전전월
		$before_two_month_google_arppu = ($before_two_month_purchase_google_user == 0) ? 0 : round($before_two_month_purchase_google_amount / $before_two_month_purchase_google_user * 100);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 63, $month_google_arppu, $before_month_google_arppu, $before_two_month_google_arppu) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);
		
		// (5) Amazon
		// 당월
		$month_amazon_arppu = ($month_purchase_amazon_user == 0) ? 0 : round($month_purchase_amazon_amount / $month_purchase_amazon_user * 100);
		
		// 전월
		$before_month_amazon_arppu = ($before_month_purchase_amazon_user == 0) ? 0 : round($before_month_purchase_amazon_amount / $before_month_purchase_amazon_user * 100);
		
		// 전전월
		$before_two_month_amazon_arppu = ($before_two_month_purchase_amazon_user == 0) ? 0 : round($before_two_month_purchase_amazon_amount / $before_two_month_purchase_amazon_user * 100);
		
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 64, $month_amazon_arppu, $before_month_amazon_arppu, $before_two_month_amazon_arppu) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);
		
		// 7.신규회원가입 & 3회 이상 로그인
		// (1) 전체
		// 당월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE logincount >= 3 AND '$indicator_month 00:00:00' <= createdate AND createdate < '$indicator_current_month 00:00:00';";
		$month_all_join_count = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(month, 0) FROM game_indicator_month WHERE typeid = 70 AND writedate = '$indicator_before_month_log'";
		$before_month_all_join_count = $db_analysis->getvalue($sql);
		
		if($before_month_all_join_count == "")
			$before_month_all_join_count = 0;
		
		// 전전월
		$sql = "SELECT IFNULL(month, 0) FROM game_indicator_month WHERE typeid = 70 AND writedate = '$indicator_two_before_month_log'";
		$before_two_month_all_join_count = $db_analysis->getvalue($sql);
		
		if($before_two_month_all_join_count == "")
			$before_two_month_all_join_count = 0;
					
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 70, $month_all_join_count, $before_month_all_join_count, $before_two_month_all_join_count) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		$db_analysis->execute($sql);
		
		// (2) facebook
		// 당월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE logincount >= 3 AND '$indicator_month 00:00:00' <= createdate AND createdate < '$indicator_current_month 00:00:00' AND platform = 0";
		$month_facebook_join_count = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(month, 0) FROM game_indicator_month WHERE typeid = 71 AND writedate = '$indicator_before_month_log'";
		$before_month_facebook_join_count = $db_analysis->getvalue($sql);
		
		if($before_month_facebook_join_count == "")
			$before_month_facebook_join_count = 0;
		
		// 전전월
		$sql = "SELECT IFNULL(month, 0) FROM game_indicator_month WHERE typeid = 71 AND writedate = '$indicator_two_before_month_log'";
		$before_two_month_facebook_join_count = $db_analysis->getvalue($sql);
		
		if($before_two_month_facebook_join_count == "")
			$before_two_month_facebook_join_count = 0;
			
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 71, $month_facebook_join_count, $before_month_facebook_join_count, $before_two_month_facebook_join_count) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
			
		$db_analysis->execute($sql);
		
		// (3) Apple
		// 당월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE logincount >= 3 AND '$indicator_month 00:00:00' <= createdate AND createdate < '$indicator_current_month 00:00:00' AND platform = 1";
		$month_apple_join_count = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(month, 0) FROM game_indicator_month WHERE typeid = 72 AND writedate = '$indicator_before_month_log'";
		$before_month_apple_join_count = $db_analysis->getvalue($sql);
		
		if($before_month_apple_join_count == "")
			$before_month_apple_join_count = 0;
		
		// 전전월
		$sql = "SELECT IFNULL(month, 0) FROM game_indicator_month WHERE typeid = 72 AND writedate = '$indicator_two_before_month_log'";
		$before_two_month_apple_join_count = $db_analysis->getvalue($sql);
		
		if($before_two_month_apple_join_count == "")
			$before_two_month_apple_join_count = 0;
			
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 72, $month_apple_join_count, $before_month_apple_join_count, $before_two_month_apple_join_count) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);
		
		// (4) Google
		// 당월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE logincount >= 3 AND '$indicator_month 00:00:00' <= createdate AND createdate < '$indicator_current_month 00:00:00' AND platform = 2";
		$month_google_join_count = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(month, 0) FROM game_indicator_month WHERE typeid = 73 AND writedate = '$indicator_before_month_log'";
		$before_month_google_join_count = $db_analysis->getvalue($sql);
		
		if($before_month_google_join_count == "")
			$before_month_google_join_count = 0;
		
		// 전전월
		$sql = "SELECT IFNULL(month, 0) FROM game_indicator_month WHERE typeid = 73 AND writedate = '$indicator_two_before_month_log'";
		$before_two_month_google_join_count = $db_analysis->getvalue($sql);
		
		if($before_two_month_google_join_count == "")
			$before_two_month_google_join_count = 0;
			
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 73, $month_google_join_count, $before_month_google_join_count, $before_two_month_google_join_count) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);
		
		// (5) Amazon
		// 당월
		$sql = "SELECT IFNULL(COUNT(useridx), 0) FROM tbl_user_ext WHERE logincount >= 3 AND '$indicator_month 00:00:00' <= createdate AND createdate < '$indicator_current_month 00:00:00' AND platform = 3";
		$month_amazon_join_count = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(month, 0) FROM game_indicator_month WHERE typeid = 74 AND writedate = '$indicator_before_month_log'";
		$before_month_amazon_join_count = $db_analysis->getvalue($sql);
		
		if($before_month_amazon_join_count == "")
			$before_month_amazon_join_count = 0;
		
		// 전전월
		$sql = "SELECT IFNULL(month, 0) FROM game_indicator_month WHERE typeid = 74 AND writedate = '$indicator_two_before_month_log'";
		$before_two_month_amazon_join_count = $db_analysis->getvalue($sql);
		
		if($before_two_month_amazon_join_count == "")
			$before_two_month_amazon_join_count = 0;
			
		$sql = "INSERT INTO game_indicator_month(writedate, typeid, month, before_month, before_two_month) ".
				"VALUES('$indicator_month_log', 74, $month_amazon_join_count, $before_month_amazon_join_count, $before_two_month_amazon_join_count) ".
				"ON DUPLICATE KEY UPDATE month = VALUES(month), before_month = VALUES(before_month), before_two_month = VALUES(before_two_month)";
		
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_analysis->end();
	$db_mobile->end();
?>