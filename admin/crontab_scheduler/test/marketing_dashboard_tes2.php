<?
	include("../../common/common_include.inc.php");

	$db_mobile = new CDatabase_Mobile();
	$db_analysis = new CDatabase_Analysis();
	$db_other = new CDatabase_Other();
	$db_slave_main = new CDatabase_Slave_Main();
	$db_main2 = new CDatabase_Main2();
	
	$db_mobile->execute("SET wait_timeout=3600");	
	$db_analysis->execute("SET wait_timeout=3600");	
	$db_other->execute("SET wait_timeout=3600");	
	$db_slave_main->execute("SET wait_timeout=3600");	
	$db_main2->execute("SET wait_timeout=3600");	
	
	$port = "";
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}	
	
	$today = date("Y-m-d");
	$yesterday = date("Y-m-d",strtotime("-1 days"));
	
	// ios - fbself 최근 30일 가입자 수
	$sql = "SELECT COUNT(useridx) ".
			"FROM tbl_user_ext ".
			"WHERE DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND adflag = 'nanigans' AND platform = 1";
	$join_count_30day = $db_slave_main->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
			"FROM tbl_ad_stats_mobile ".
			"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 30 DAY), '%Y-%m-%d') < today AND platform = 1 ";
	$spend_30day = $db_main2->getvalue($sql);
	
	// 최근 30일 미게임수
	$sql = "SELECT IFNULL( ".
			"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"	,0) AS join_nogame_today ".
			"FROM tbl_user_ext ".
			"WHERE adflag = 'nanigans' AND platform = 1 AND DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate";
	$nogame_30day = $db_slave_main->getvalue($sql);
	
	// 최근 30일 매출
	$sql = "SELECT IFNULL(ROUND(SUM(facebookcredit)/10, 2), 0) AS money, COUNT(DISTINCT t1.useridx) AS user_cnt ".
			"FROM ( ".
			"	SELECT useridx ".
			"	FROM tbl_user_ext ".
			"	WHERE DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND adflag = 'nanigans' AND platform = 1 ".
			") t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
			"WHERE t1.useridx > 20000 AND status = 1";
	$pay_info_30day = $db_slave_main->getarray($sql);
	
	$sql = "SELECT IFNULL(ROUND(SUM(money), 2), 0) AS money, COUNT(DISTINCT t1.useridx) AS user_cnt ".
			"FROM ( ".
			"	SELECT useridx ".
			"	FROM tbl_user_ext ".
			"	WHERE DATE_SUB(NOW(), INTERVAL 30 DAY) < createdate AND adflag = 'nanigans' AND platform = 1 ".
			") t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
			"WHERE t1.useridx > 20000 AND status = 1";
	$pay_info_30day_mobile = $db_slave_main->getarray($sql);
	
	$pay_30day = $pay_info_30day["money"] + $pay_info_30day_mobile["money"];
	$pay_user_cnt_30day = $pay_info_30day["user_cnt"] + $pay_info_30day_mobile["user_cnt"];
	
	// 최근 14일 가입자 수
	$sql = "SELECT COUNT(useridx) ".
			"FROM tbl_user_ext ".
			"WHERE DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND adflag = 'nanigans' AND platform = 1";
	$join_count_14day = $db_slave_main->getvalue($sql);
	
	// 최근 14일 비용
	$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
			"FROM tbl_ad_stats_mobile ".
			"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 14 DAY), '%Y-%m-%d') < today AND platform = 1 ";
	$spend_14day = $db_main2->getvalue($sql);
	
	// 최근 14일 미게임수
	$sql = "SELECT IFNULL( ".
			"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"	,0) AS join_nogame_today ".
			"FROM tbl_user_ext ".
			"WHERE adflag = 'nanigans' AND platform = 1 AND DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate";
	$nogame_14day = $db_slave_main->getvalue($sql);
	
	// 최근 14일 매출
	$sql = "SELECT IFNULL(ROUND(SUM(facebookcredit)/10, 2), 0) AS money, COUNT(DISTINCT t1.useridx) AS user_cnt ".
			"FROM ( ".
			"	SELECT useridx ".
			"	FROM tbl_user_ext ".
			"	WHERE DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND adflag = 'nanigans' AND platform = 1 ".
			") t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
			"WHERE t1.useridx > 20000 AND status = 1";
	$pay_info_14day = $db_slave_main->getarray($sql);
	
	$sql = "SELECT IFNULL(ROUND(SUM(money), 2), 0) AS money, COUNT(DISTINCT t1.useridx) AS user_cnt ".
			"FROM ( ".
			"	SELECT useridx ".
			"	FROM tbl_user_ext ".
			"	WHERE DATE_SUB(NOW(), INTERVAL 14 DAY) < createdate AND adflag = 'nanigans' AND platform = 1 ".
			") t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
			"WHERE t1.useridx > 20000 AND status = 1";
	$pay_info_14day_mobile = $db_slave_main->getarray($sql);
	
	$pay_14day = $pay_info_14day["money"] + $pay_info_14day_mobile["money"];
	$pay_user_cnt_14day = $pay_info_14day["user_cnt"] + $pay_info_14day_mobile["user_cnt"];
	
	// 최근 7일 가입자 수
	$sql = "SELECT COUNT(useridx) ".
			"FROM tbl_user_ext ".
			"WHERE DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND adflag = 'nanigans' AND platform = 1";
	$join_count_7day = $db_slave_main->getvalue($sql);
	
	// 최근 7일 비용
	$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
			"FROM tbl_ad_stats_mobile ".
			"WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7 DAY), '%Y-%m-%d') < today AND platform = 1 ";
	$spend_7day = $db_main2->getvalue($sql);
	
	// 최근 7일 미게임수
	$sql = "SELECT IFNULL( ".
			"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"	,0) AS join_nogame_today ".
			"FROM tbl_user_ext ".
			"WHERE adflag = 'nanigans' AND platform = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate";
	$nogame_7day = $db_slave_main->getvalue($sql);
	
	// 최근 7일 매출
	$sql = "SELECT IFNULL(ROUND(SUM(facebookcredit)/10, 2), 0) AS money, COUNT(DISTINCT t1.useridx) AS user_cnt ".
			"FROM ( ".
			"	SELECT useridx ".
			"	FROM tbl_user_ext ".
			"	WHERE DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND adflag = 'nanigans' AND platform = 1 ".
			") t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
			"WHERE t1.useridx > 20000 AND status = 1";
	$pay_info_7day = $db_slave_main->getarray($sql);
	
	$sql = "SELECT IFNULL(ROUND(SUM(money), 2), 0) AS money, COUNT(DISTINCT t1.useridx) AS user_cnt ".
			"FROM ( ".
			"	SELECT useridx ".
			"	FROM tbl_user_ext ".
			"	WHERE DATE_SUB(NOW(), INTERVAL 7 DAY) < createdate AND adflag = 'nanigans' AND platform = 1 ".
			") t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
			"WHERE t1.useridx > 20000 AND status = 1";
	$pay_info_7day_mobile = $db_slave_main->getarray($sql);
	
	$pay_7day = $pay_info_7day["money"] + $pay_info_7day_mobile["money"];
	$pay_user_cnt_7day = $pay_info_7day["user_cnt"] + $pay_info_7day_mobile["user_cnt"];
	
	// 어제 가입자 수
	$sql = "SELECT COUNT(useridx) ".
			"FROM tbl_user_ext ".
			"WHERE createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND adflag = 'nanigans' AND platform = 1";
	write_log($sql);
	$join_count_yesterday = $db_slave_main->getvalue($sql);
	
	// 어제 비용
	$sql = "SELECT IFNULL(ROUND(SUM(spend), 2), 0) ".
			"FROM tbl_ad_stats_mobile ".
			"WHERE today BETWEEN '$yesterday' AND '$yesterday' AND platform = 1 ";
	$spend_yesterday = $db_main2->getvalue($sql);
	
	// 어제 미게임수
	$sql = "SELECT IFNULL( ".
			"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"	,0) AS join_nogame_today ".
			"FROM tbl_user_ext ".
			"WHERE adflag = 'nanigans' AND platform = 1 AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	$nogame_yesterday = $db_slave_main->getvalue($sql);
	
	// 어제 매출
	$sql = "SELECT IFNULL(ROUND(SUM(facebookcredit)/10, 2), 0) AS money, COUNT(DISTINCT t1.useridx) AS user_cnt ".
			"FROM ( ".
			"	SELECT useridx ".
			"	FROM tbl_user_ext ".
			"	WHERE createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND adflag = 'nanigans' AND platform = 1 ".
			") t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
			"WHERE t1.useridx > 20000 AND status = 1";
	$pay_info_yesterday = $db_slave_main->getarray($sql);
	
	$sql = "SELECT IFNULL(ROUND(SUM(money), 2), 0) AS money, COUNT(DISTINCT t1.useridx) AS user_cnt ".
			"FROM ( ".
			"	SELECT useridx ".
			"	FROM tbl_user_ext ".
			"	WHERE createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND adflag = 'nanigans' AND platform = 1 ".
			") t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
			"WHERE t1.useridx > 20000 AND status = 1";
	$pay_info_yesterday_mobile = $db_slave_main->getarray($sql);
	
	$pay_yesterday = $pay_info_yesterday["money"] + $pay_info_yesterday_mobile["money"];
	$pay_user_cnt_yesterday = $pay_info_yesterday["user_cnt"] + $pay_info_yesterday_mobile["user_cnt"];
	
	$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 101, '$join_count_30day', 'Join(30day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_30day', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 102, '$spend_30day', 'Spend(30day)', now()) ON DUPLICATE KEY UPDATE value='$spend_30day', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 103, '$nogame_30day', 'No Game(30day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_30day', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 104, '$pay_30day', 'Pay(30day)', now()) ON DUPLICATE KEY UPDATE value='$pay_30day', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 105, '$pay_user_cnt_30day', 'Pay User Count(30day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_30day', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 106, '$join_count_14day', 'Join(14day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_14day', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 107, '$spend_14day', 'Spend(14day)', now()) ON DUPLICATE KEY UPDATE value='$spend_14day', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 108, '$nogame_14day', 'No Game(14day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_14day', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 109, '$pay_14day', 'Pay(14day)', now()) ON DUPLICATE KEY UPDATE value='$pay_14day', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 110, '$pay_user_cnt_14day', 'Pay User Count(14day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_14day', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 111, '$join_count_7day', 'Join(7day)', now()) ON DUPLICATE KEY UPDATE value='$join_count_7day', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 112, '$spend_7day', 'Spend(7day)', now()) ON DUPLICATE KEY UPDATE value='$spend_7day', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 113, '$nogame_7day', 'No Game(7day)', now()) ON DUPLICATE KEY UPDATE value='$nogame_7day', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 114, '$pay_7day', 'Pay(7day)', now()) ON DUPLICATE KEY UPDATE value='$pay_7day', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 115, '$pay_user_cnt_7day', 'Pay User Count(7day)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_7day', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 116, '$join_count_yesterday', 'Join(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$join_count_yesterday', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 117, '$spend_yesterday', 'Spend(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$spend_yesterday', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 118, '$nogame_yesterday', 'No Game(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$nogame_yesterday', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 119, '$pay_yesterday', 'Pay(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$pay_yesterday', writedate=now();";
	$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(1001, 120, '$pay_user_cnt_yesterday', 'Pay User Count(yesterday)', now()) ON DUPLICATE KEY UPDATE value='$pay_user_cnt_yesterday', writedate=now();";
	write_log($sql);
	$db_analysis->execute($sql);
	
	$sql = "INSERT INTO tbl_marketing_dash_daily(today, platform, market_type, category, reg_count, spend, nogame_count, pay_user_count, pay_amount) ".
			"VALUES('$today', 1, 0, 1, $join_count_30day, $spend_30day, $nogame_30day, $pay_user_cnt_30day, $pay_30day), ".
			"	('$today', 1, 0, 2, $join_count_14day, $spend_14day, $nogame_14day, $pay_user_cnt_14day, $pay_14day), ".
			"	('$today', 1, 0, 3, $join_count_7day, $spend_7day, $nogame_7day, $pay_user_cnt_7day, $pay_7day), ".
			"	('$today', 1, 0, 4, $join_count_yesterday, $spend_yesterday, $nogame_yesterday, $pay_user_cnt_yesterday, $pay_yesterday)".
			"ON DUPLICATE KEY UPDATE reg_count=VALUES(reg_count), spend=VALUES(spend), nogame_count=VALUES(nogame_count), pay_user_count=VALUES(pay_user_count), pay_amount=VALUES(pay_amount)";
	$db_other->execute($sql);
	
	$db_mobile->end();	
	$db_analysis->end();
	$db_other->end();
	$db_slave_main->end();
	$db_main2->end();
?>
