<?
	include("../../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();
		
	$db_main->execute("SET wait_timeout=7200");
	$db_analysis->execute("SET wait_timeout=7200");
	
	// 일별 구매 상품 통계 - Mobile
	try
	{
		$today = date("Y-m-d");
		
		$sdate = '2016-11-15';
		//$sdate = '2017-01-28';
		$edate = $today;
		$std_useridx = 20000;
		
		while($sdate < $edate)
		{
			$temp_date = date('Y-m-d', strtotime($sdate.' + 1 day'));
				
			write_log($temp_date);
			
			// 카테고리별
			$sql = "SELECT os_type, category, SUM(total_count) AS total_count, ROUND(IFNULL(SUM(total_money), 0), 2) AS total_money, SUM(total_coin) AS total_coin ".
					"FROM ".
					"( ".
					"	SELECT t2.os_type, IFNULL(IF(couponidx > 0, 100, category), -1) AS category, COUNT(orderidx) AS total_count, SUM(t1.money*10) AS total_money, IFNULL(SUM(coin), 0) AS total_coin ".
					"	FROM ".
					"	( ".
					"		SELECT productidx, orderidx, money, coin, couponidx ".
					"		FROM tbl_product_order_mobile ".
					"		WHERE useridx > $std_useridx AND status = 1 AND writedate >= '$temp_date 00:00:00' AND writedate <= '$temp_date 23:59:59' ".
					"	) t1 LEFT JOIN tbl_product_mobile t2 ON t1.productidx = t2.productidx ".
					"	GROUP BY category, couponidx, t2.os_type ".
					") t3 GROUP BY category, os_type";
		
			$category_list = $db_main->gettotallist($sql);
		
			for($i=0; $i<sizeof($category_list); $i++)
			{
				$order_os_type = $category_list[$i]["os_type"];
				$order_category = $category_list[$i]["category"];
				$order_count = $category_list[$i]["total_count"];
				$order_money = $category_list[$i]["total_money"];
				$order_coin = $category_list[$i]["total_coin"];
										
				$sql = "INSERT INTO tbl_product_stat_os_mobile_daily(writedate, type, os_type, category, productidx, coupon_type, total_count, use_coupon, total_money, total_coin) ".
				 		"VALUES('$temp_date', 0, $order_os_type, $order_category, 0, 0, $order_count, 0, '$order_money', $order_coin) ".
						"ON DUPLICATE KEY UPDATE total_count = VALUES(total_count), total_money = VALUES(total_money), total_coin = VALUES(total_coin)";
		
				$db_analysis->execute($sql);
			}
		
			// 상품별
			$sql = "SELECT t2.os_type, t1.productidx, COUNT(orderidx) AS total_count, SUM(coupon) AS use_coupon, SUM(t1.money*10) AS total_money, IFNULL(SUM(coin), 0) AS total_coin ".
					"FROM ( ".
					"	SELECT productidx, orderidx, money, coin, IF(couponidx > 0, 1, 0) AS coupon ".
					"	FROM tbl_product_order_mobile ".
					"	WHERE useridx > $std_useridx AND status =  1 AND writedate >= '$temp_date 00:00:00' AND writedate <= '$temp_date 23:59:59' ".
					") t1 LEFT JOIN tbl_product_mobile t2 ON t1.productidx = t2.productidx ".
					"GROUP BY productidx, coupon, t2.os_type ORDER BY t2.os_type";
		
			$product_list = $db_main->gettotallist($sql);
		
			for($i=0; $i<sizeof($product_list); $i++)
			{
				$order_os_type = $product_list[$i]["os_type"];
				$order_product = $product_list[$i]["productidx"];
				$order_count = $product_list[$i]["total_count"];
				$order_money = $product_list[$i]["total_money"];
				$order_use_coupon = $product_list[$i]["use_coupon"];
				$order_coin = $product_list[$i]["total_coin"];
		
				if($order_use_coupon > 0)
					$coupon_type = 1;
				else
					$coupon_type = 0;
		
				$sql = "INSERT INTO tbl_product_stat_os_mobile_daily(writedate, type, os_type, category, productidx, coupon_type, total_count, use_coupon, total_money, total_coin) ".
						"VALUES('$temp_date', 1, $order_os_type, 0, $order_product, $coupon_type, $order_count, $order_use_coupon, $order_money, $order_coin) ".
						"ON DUPLICATE KEY UPDATE total_count = VALUES(total_count), use_coupon = VALUES(use_coupon), total_money = VALUES(total_money), total_coin = VALUES(total_coin)";
		
				$db_analysis->execute($sql);
			}
			
			$sdate = $temp_date;
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
		
	$db_main->end();
	$db_analysis->end();
	
?>
