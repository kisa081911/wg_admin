<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");

	try
	{
		$db_main2 = new CDatabase_Main2();
		$db_redshift = new CDatabase_Redshift();
		$db_other = new CDatabase_Other();
		
		$sdate = "2017-01-01";
		$edate = "2017-11-01";
		
		while($sdate <= $edate)
		{
			$tomorrow = date('Y-m-d', strtotime($sdate.' + 1 day'));
			write_log("Date : ".$sdate." ~ ".$tomorrow);
			
			
			$sql = "select  to_char(writedate, 'YYYY-mm-dd') as writedate, slottype, mode, betlevel, count(distinct useridx) as cnt_user from t5_user_gamelog_amazon ".
					"where writedate >= '$sdate 00:00:00' AND writedate < '$tomorrow 00:00:00' AND slottype > 0 group by to_char(writedate, 'YYYY-mm-dd'), slottype, mode, betlevel;";
			//$sql = "select  date_format(writedate, '%Y-%m-%d') as writedate, slottype, mode, betlevel, count(distinct useridx) as cnt_user from tbl_user_gamelog_android ".
			//		"where writedate >= '$sdate 00:00:00' ANd writedate < '$tomorrow 00:00:00' ANd slottype > 0 group by date_format(writedate, '%Y-%m-%d'), slottype, mode, betlevel;";
			//$usercount_data = $db_other->gettotallist($sql);
			$usercount_data = $db_redshift->gettotallist($sql);
			
			$insert_cnt = 0;
			$insert_sql = "";
			
			for($i = 0; $i < sizeof($usercount_data); $i++)
			{
				$writedate = $usercount_data[$i]["writedate"];
				$slottype = $usercount_data[$i]["slottype"];
				$mode = $usercount_data[$i]["mode"];
				$betlevel = $usercount_data[$i]["betlevel"];
				$cnt_user = $usercount_data[$i]["cnt_user"];
				
				if($insert_sql == "")
				{
					$insert_sql = "INSERT INTO tbl_game_cash_stats_amazon_daily2(writedate, slottype, mode, betlevel, usercount) VALUES('$writedate', $slottype, $mode, $betlevel, $cnt_user)";
				}
				else 
				{
					$insert_sql .= ",('$writedate', $slottype, $mode, $betlevel, $cnt_user)";
				}
				
				$insert_cnt++;
	
				if($insert_cnt == 1000)
				{
					$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
					$db_main2->execute($insert_sql);
					
					$insert_cnt = 0;
					$insert_sql = "";
				}
			}
			
			if($insert_sql != "")
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE usercount = VALUES(usercount)";
				$db_main2->execute($insert_sql);
			}
			
			sleep(1);
			$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));
		}
		
		$db_main2->end();
		$db_redshift->end();
		$db_other->end();
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>