<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$db_other = new CDatabase_Other();
	$db_main2 = new CDatabase_Main2();
	$db_redshift = new CDatabase_Redshift();
	
	for($u=0;$u<10;$u++)
	{
		$total_start_time = array_sum(explode(' ', microtime()));
		//사용자 기본 정보
		//19미만 3회 미만 비결제자
		$sql = "SELECT DISTINCT t1.useridx FROM tbl_user_retention_log t1 ".
				" JOIN tbl_nopayer_user_$u t2 ".
				" ON t1.useridx = t2.useridx ".
				" WHERE DATE_FORMAT(writedate,'%y-%m-%d') = DATE_FORMAT(NOW(),'%y-%m-%d') ".
				" AND t2.useridx > 20000";
		
		$nopayer_usaer_list = $db_main2->gettotallist($sql);
		
		for($i=0; $i<sizeof($nopayer_usaer_list); $i++)
		{
			$useridx = $nopayer_usaer_list[$i]["useridx"];
		
			//최근 2주 간 가장 많이 스핀 한 슬롯
			$sql="select slottype as playcount  from (
			select slottype,  playcount from t5_user_gamelog where useridx = $useridx and writedate > TO_CHAR(dateadd(week,-2,CURRENT_DATE),'YYYY-MM-DD HH24:MI:SS')
			UNION ALL
			select slottype,  playcount from t5_user_gamelog_amazon where useridx = $useridx and  writedate > TO_CHAR(dateadd(week,-2,CURRENT_DATE),'YYYY-MM-DD HH24:MI:SS')
			UNION ALL
			select slottype,  playcount from t5_user_gamelog_android where useridx = $useridx and  writedate > TO_CHAR(dateadd(week,-2,CURRENT_DATE),'YYYY-MM-DD HH24:MI:SS')
			UNION ALL
			select slottype,  playcount from t5_user_gamelog_ios where useridx = $useridx and  writedate > TO_CHAR(dateadd(week,-2,CURRENT_DATE),'YYYY-MM-DD HH24:MI:SS')
			)group by slottype  order by playcount desc limit 1";
			$playcount_slot = $db_redshift->getvalue($sql);
		
			//최근 2주 간 가장 많이 머니인 한 슬롯
			$sql="select slottype as money_in  from (
			select slottype,  money_in from t5_user_gamelog where useridx = $useridx and writedate > TO_CHAR(dateadd(week,-2,CURRENT_DATE),'YYYY-MM-DD HH24:MI:SS')
			UNION ALL
			select slottype,  money_in from t5_user_gamelog_amazon where useridx = $useridx and  writedate > TO_CHAR(dateadd(week,-2,CURRENT_DATE),'YYYY-MM-DD HH24:MI:SS')
			UNION ALL
			select slottype,  money_in from t5_user_gamelog_android where useridx = $useridx and  writedate > TO_CHAR(dateadd(week,-2,CURRENT_DATE),'YYYY-MM-DD HH24:MI:SS')
			UNION ALL
			select slottype,  money_in from t5_user_gamelog_ios where useridx = $useridx and  writedate > TO_CHAR(dateadd(week,-2,CURRENT_DATE),'YYYY-MM-DD HH24:MI:SS')
			)group by slottype  order by money_in desc limit 1";
			$money_in_slot = $db_redshift->getvalue($sql);
		
			//최근 2주 간 가장 많이 머니아웃 한 슬롯
			$sql="select slottype as money_out  from (
			select slottype,  money_out from t5_user_gamelog where useridx = $useridx and writedate > TO_CHAR(dateadd(week,-2,CURRENT_DATE),'YYYY-MM-DD HH24:MI:SS')
			UNION ALL
			select slottype,  money_out from t5_user_gamelog_amazon where useridx = $useridx and  writedate > TO_CHAR(dateadd(week,-2,CURRENT_DATE),'YYYY-MM-DD HH24:MI:SS')
			UNION ALL
			select slottype,  money_out from t5_user_gamelog_android where useridx = $useridx and  writedate > TO_CHAR(dateadd(week,-2,CURRENT_DATE),'YYYY-MM-DD HH24:MI:SS')
			UNION ALL
			select slottype,  money_out from t5_user_gamelog_ios where useridx = $useridx and  writedate > TO_CHAR(dateadd(week,-2,CURRENT_DATE),'YYYY-MM-DD HH24:MI:SS')
			)group by slottype  order by money_out desc limit 1";
			$money_out_slot = $db_redshift->getvalue($sql);
		
			//최근 2주 간 가장 많이 빅윈 한 슬롯
			$sql = "SELECT slottype, COUNT(bigwinidx) AS cnt FROM `tbl_bigwin_log` WHERE useridx = $useridx AND writedate > DATE_SUB(NOW(),INTERVAL 2 WEEK) GROUP BY slottype ORDER BY cnt DESC LIMIT 1";
			$bigwin_slot_arr = $db_main2->getarray($sql);
			$bigwin_slot = $bigwin_slot_arr["slottype"];
		
			$playcount_slot = ($playcount_slot=="")? 0:$playcount_slot;
			$money_in_slot = ($money_in_slot=="")? 0:$money_in_slot;
			$money_out_slot = ($money_out_slot=="")? 0:$money_out_slot;
			$bigwin_slot = ($bigwin_slot=="")? 0:$bigwin_slot;
		
			$sql = "UPDATE tbl_nopayer_user_$u SET 2week_top_spinslot = $playcount_slot,2week_top_moneyinslot = $money_in_slot,2week_top_moneyoutslot= $money_out_slot,2week_top_bigwinslot = $bigwin_slot WHERE useridx = $useridx;";
			$db_main2->execute($sql);
		}
		$total_end_time = array_sum(explode(' ', microtime()));
		write_log("tbl_nopayer_user_$u.TIME : ". ( $total_end_time - $total_start_time ));
	}
	
	$db_redshift->end();
	$db_main2->end();
?>