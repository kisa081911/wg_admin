<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/purchase_gamelog_type_scheduler2") !== false)
		{
			$count++;
		}
	}	
	
	$issuccess = "1";
	
	if ($count > 1)
		exit();
	
	$db_redshift = new CDatabase_Redshift();	
	$db_main = new CDatabase_Main();
	$db_other = new CDatabase_Other();
	
	$db_main->execute("SET wait_timeout=21600");
	$db_other->execute("SET wait_timeout=21600");
	
	$before_12week = date("Y-m-d", time() - 60 * 60 * 24 * 7 * 12);
	
	//tmp 초기화
	$sql = "TRUNCATE TABLE t5_user_purchase_gamelog_type_tmp";
	$db_redshift->execute($sql);
	
	$sql = "SELECT t1.useridx ".
			"FROM ( ".
			"	SELECT useridx FROM tbl_user WHERE useridx > 20000 AND logindate > DATE_SUB(NOW(), INTERVAL 1 MONTH) ".
			") t1 JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx ".
			"WHERE t2.vip_point > 0 ";
	$user_list = $db_main->gettotallist($sql);
	
	$insert_sql = "";
	$insert_cnt = 0;
	
	for($i=0; $i<sizeof($user_list); $i++)
	{
		$useridx = $user_list[$i]["useridx"];
		
		$sql = "SELECT COUNT(*) FROM tbl_user_purchase_log WHERE useridx = $useridx AND money >= 0.99";
		$is_exist = $db_other->getvalue($sql);
	
		if($is_exist > 0)
		{
	
			$sql = "SELECT useridx, ".
					"ROUND(SUM(weight_avg_user_coin)/SUM(money),2) AS weight_avg_user_coin, ".
					"ROUND(AVG(product_coin), 2) AS avg_product_coin, ".
					"SUM(product_coin) AS total_product_coin, ".
					"ROUND(SUM(money),2) AS total_money, ".
					"ROUND(AVG(money),2) AS avg_buy_money, ".
					"ROUND((CASE WHEN SUM(weight_avg_more_rate) = 0 THEN 0 ELSE (SUM(weight_avg_more_rate)/SUM(money)) END), 2) AS weight_avg_more_rate, ".
					"IFNULL(ROUND((CASE WHEN SUM(weight_avg_rebuydate) = 0 THEN 0 ELSE (SUM(weight_avg_rebuydate)/SUM(money)) END), 2), 0) AS weight_avg_rebuydate, ".
					"ROUND((CASE WHEN SUM(coupon_product_coin) = 0 THEN 0 ELSE (SUM(coupon_product_coin)/SUM(product_coin))*100 END), 2) AS coupon_buy_rate, ".
					"ROUND((CASE WHEN SUM(basic_buy_coin) = 0 THEN 0 ELSE (SUM(basic_buy_coin)/SUM(product_coin))*100 END), 2) AS basic_buy_rate, ".
					"COUNT(money) AS buy_count, ".
					"ROUND((CASE WHEN SUM(web_money) = 0 THEN 0 ELSE ((SUM(web_money)/SUM(money))*100) END)) AS web_purchase_rate, ".
					"ROUND((CASE WHEN SUM(ios_money) = 0 THEN 0 ELSE ((SUM(ios_money)/SUM(money))*100) END)) AS ios_purchase_rate, ".
					"ROUND((CASE WHEN SUM(android_money) = 0 THEN 0 ELSE ((SUM(android_money)/SUM(money))*100) END)) AS android_purchase_rate, ".
					"ROUND((CASE WHEN SUM(amazon_money) = 0 THEN 0 ELSE ((SUM(amazon_money)/SUM(money))*100) END)) AS amazon_purchase_rate ".
					"FROM ( ".
					"	SELECT useridx, platform, user_coin, product_coin, money, is_coupon, more_rate, ".
					"		(money * user_coin) AS weight_avg_user_coin, ".
					"		(money * more_rate) AS weight_avg_more_rate, ".
					"		(CASE WHEN DATEDIFF(rebuydate, writedate) < 0 THEN 0 ELSE (money * DATEDIFF(rebuydate, writedate)) END) AS weight_avg_rebuydate, ".
					"		(CASE WHEN DATEDIFF(rebuydate, writedate) < 0 THEN 0 ELSE DATEDIFF(rebuydate, writedate) END) AS rebuydate, ".
					"		(CASE WHEN is_coupon = 1 THEN product_coin ELSE 0 END) AS coupon_product_coin, ".
					"		(CASE WHEN base_coin = product_coin THEN product_coin ELSE 0 END) AS basic_buy_coin, ".
					"		(CASE WHEN platform = 0  THEN money ELSE 0 END) AS web_money, ".
					"		(CASE WHEN platform = 1  THEN money ELSE 0 END) AS ios_money, ".
					"		(CASE WHEN platform = 2  THEN money ELSE 0 END) AS android_money, ".
					"		(CASE WHEN platform = 3  THEN money ELSE 0 END) AS amazon_money ".
					"	FROM tbl_user_purchase_log WHERE useridx = $useridx AND money >= 0.99 ORDER BY writedate DESC LIMIT 20 ".
					") t1 ".
					"GROUP BY useridx ";
				
			$purchase_data = $db_other->getarray($sql);
			
			$weight_avg_user_coin = $purchase_data["weight_avg_user_coin"];
			$avg_product_coin = $purchase_data["avg_product_coin"];
			$total_product_coin = $purchase_data["total_product_coin"];
			$total_money = $purchase_data["total_money"];
			$avg_buy_money = $purchase_data["avg_buy_money"];
			$weight_avg_more_rate = $purchase_data["weight_avg_more_rate"];
			$weight_avg_rebuydate = $purchase_data["weight_avg_rebuydate"];
			$coupon_buy_rate = $purchase_data["coupon_buy_rate"];
			$basic_buy_rate = $purchase_data["basic_buy_rate"];
			$buy_count = $purchase_data["buy_count"];
			$web_purchase_rate = $purchase_data["web_purchase_rate"];
			$ios_purchase_rate = $purchase_data["ios_purchase_rate"];
			$android_purchase_rate = $purchase_data["android_purchase_rate"];
			$amazon_purchase_rate = $purchase_data["amazon_purchase_rate"];
				
			if($insert_cnt == 0)
			{
				$insert_sql = "INSERT INTO t5_user_purchase_gamelog_type_tmp VALUES($useridx, $weight_avg_user_coin, $avg_product_coin, $total_product_coin, $avg_buy_money, $total_money, $weight_avg_more_rate, $weight_avg_rebuydate, $coupon_buy_rate, ".
							"$basic_buy_rate, $buy_count, $web_purchase_rate, $ios_purchase_rate, $android_purchase_rate, $amazon_purchase_rate)";
			
				$insert_cnt++;
			}
			else if($insert_cnt < 1000)
			{
				$insert_sql .= ",($useridx, $weight_avg_user_coin, $avg_product_coin, $total_product_coin, $avg_buy_money, $total_money, $weight_avg_more_rate, $weight_avg_rebuydate, $coupon_buy_rate, ".
							"$basic_buy_rate, $buy_count, $web_purchase_rate, $ios_purchase_rate, $android_purchase_rate, $amazon_purchase_rate)";
			
				$insert_cnt++;
			}
			else
			{
				$insert_sql .= ",($useridx, $weight_avg_user_coin, $avg_product_coin, $total_product_coin, $avg_buy_money, $total_money, $weight_avg_more_rate, $weight_avg_rebuydate, $coupon_buy_rate, ".
							"$basic_buy_rate, $buy_count, $web_purchase_rate, $ios_purchase_rate, $android_purchase_rate, $amazon_purchase_rate)";
			
				$db_redshift->execute($insert_sql);
			
				sleep(1);
			
				$insert_cnt = 0;
				$insert_sql = "";
			}
		}
	}
		
	if($insert_sql != "")
		$db_redshift->execute($insert_sql);
	
	sleep(1);
		
	$sql = "select t1.useridx, avg_user_coin, avg_product_coin, total_buy_coin, avg_buy_money, total_money, avg_buy_more, avg_rebuyday, couponbuy_rate, basicbuy_rate, buy_count, ".
      		"web_purchase_rate, ios_purchase_rate, android_purchase_rate, amazon_purchase_rate, nvl(money_in, 0) as money_in, nvl(money_out, 0) as money_out, nvl(playcount, 0) as playcount ".
			"from ( ".
  			"	select * from t5_user_purchase_gamelog_type_tmp ".
			") t1 join ( ".
  			"	select useridx, sum(money_in) as money_in, sum(money_out) as money_out, sum(playcount) as playcount ".
  			"	from ( ".
    		"		select useridx, slottype, money_in, money_out, playcount from t5_user_gamelog where mode not in (3, 4, 5, 26) and writedate >= '$before_12week 00:00:00' ".
  			"		union all ". 
  			"		select useridx, slottype, money_in, money_out, playcount from t5_user_gamelog_ios where mode not in (3, 4, 5, 26) and writedate >= '$before_12week 00:00:00' ".
  			"		union all ". 
  			"		select useridx, slottype, money_in, money_out, playcount from t5_user_gamelog_android where  mode not in (3, 4, 5, 26) and writedate >= '$before_12week 00:00:00' ".
  			"		union all ". 
  			"		select useridx, slottype, money_in, money_out, playcount from t5_user_gamelog_amazon where  mode not in (3, 4, 5, 26) and writedate >= '$before_12week 00:00:00' ".
  			"	) sub ".
  			"	group by useridx ".
			") t2 on t1.useridx = t2.useridx ".
			"group by t1.useridx, avg_user_coin, avg_product_coin, total_buy_coin, avg_buy_money, total_money, avg_buy_more, avg_rebuyday, couponbuy_rate, basicbuy_rate, buy_count, ". 
			"web_purchase_rate, ios_purchase_rate, android_purchase_rate, amazon_purchase_rate, t2.money_in, money_out, playcount ";
	
	$recent_list = $db_redshift->gettotallist($sql);
	
	$insert_sql = "";
	$insert_cnt = 0;
	$type = 2;
	
	for($i=0; $i<sizeof($recent_list); $i++)
	{
		$recent_useridx = $recent_list[$i]["useridx"];
		$recent_avg_user_coin = $recent_list[$i]["avg_user_coin"];
		$recent_avg_product_coin = $recent_list[$i]["avg_product_coin"];
		$recent_total_buy_coin = $recent_list[$i]["total_buy_coin"];
		$recent_avg_buy_money = $recent_list[$i]["avg_buy_money"];
		$recent_total_money = $recent_list[$i]["total_money"];
		$recent_avg_buy_more = $recent_list[$i]["avg_buy_more"];
		$recent_avg_rebuyday = $recent_list[$i]["avg_rebuyday"];
		$recent_couponbuy_rate = $recent_list[$i]["couponbuy_rate"];
		$recent_basicbuy_rate = $recent_list[$i]["basicbuy_rate"];
		$recent_buy_count = $recent_list[$i]["buy_count"];
		$recent_web_purchase_rate = $recent_list[$i]["web_purchase_rate"];
		$recent_ios_purchase_rate = $recent_list[$i]["ios_purchase_rate"];
		$recent_android_purchase_rate = $recent_list[$i]["android_purchase_rate"];
		$recent_amazon_purchase_rate = $recent_list[$i]["amazon_purchase_rate"];
		$recent_money_in = $recent_list[$i]["money_in"];
		$recent_money_out = $recent_list[$i]["money_out"];
		$recent_playcount = $recent_list[$i]["playcount"];
		
		if($insert_cnt == 0)
		{
			$insert_sql = "INSERT INTO tbl_user_purchase_gamelog_type2(useridx, type, recent_avg_user_coin, recent_avg_product_coin, recent_total_buy_coin, recent_avg_buy_money, recent_total_money, recent_avg_buy_more, ".
							"recent_avg_rebuyday, recent_couponbuy_rate, recent_basicbuy_rate, recent_buy_count, money_in, money_out, playcount, recent_web_purchase_rate, recent_ios_purchase_rate, recent_android_purchase_rate, recent_amazon_purchase_rate) ".
							"VALUES($recent_useridx, $type, $recent_avg_user_coin, $recent_avg_product_coin, $recent_total_buy_coin, $recent_avg_buy_money, $recent_total_money, $recent_avg_buy_more, $recent_avg_rebuyday, ".
							"$recent_couponbuy_rate, $recent_basicbuy_rate, $recent_buy_count, $recent_money_in, $recent_money_out, $recent_playcount, $recent_web_purchase_rate, $recent_ios_purchase_rate, $recent_android_purchase_rate, $recent_amazon_purchase_rate)";

			$insert_cnt++;
		}
		else if($insert_cnt < 1000)
		{
			$insert_sql .= ",($recent_useridx, $type, $recent_avg_user_coin, $recent_avg_product_coin, $recent_total_buy_coin, $recent_avg_buy_money, $recent_total_money, $recent_avg_buy_more, $recent_avg_rebuyday, ".
							"$recent_couponbuy_rate, $recent_basicbuy_rate, $recent_buy_count, $recent_money_in, $recent_money_out, $recent_playcount, $recent_web_purchase_rate, $recent_ios_purchase_rate, $recent_android_purchase_rate, $recent_amazon_purchase_rate)";

			$insert_cnt++;
		}
		else
		{
			$insert_sql .= ",($recent_useridx, $type, $recent_avg_user_coin, $recent_avg_product_coin, $recent_total_buy_coin, $recent_avg_buy_money, $recent_total_money, $recent_avg_buy_more, $recent_avg_rebuyday, ".
							"$recent_couponbuy_rate, $recent_basicbuy_rate, $recent_buy_count, $recent_money_in, $recent_money_out, $recent_playcount, $recent_web_purchase_rate, $recent_ios_purchase_rate, $recent_android_purchase_rate, $recent_amazon_purchase_rate)";
			$insert_sql .= " ON DUPLICATE KEY UPDATE recent_avg_user_coin = VALUES(recent_avg_user_coin), recent_avg_product_coin = VALUES(recent_avg_product_coin), recent_total_buy_coin = VALUES(recent_total_buy_coin),".
							" recent_avg_buy_money = VALUES(recent_avg_buy_money), recent_total_money = VALUES(recent_total_money), recent_avg_buy_more = VALUES(recent_avg_buy_more), recent_avg_rebuyday = VALUES(recent_avg_rebuyday),".
							" recent_couponbuy_rate = VALUES(recent_couponbuy_rate), recent_basicbuy_rate = VALUES(recent_basicbuy_rate), recent_buy_count = VALUES(recent_buy_count), recent_web_purchase_rate = VALUES(recent_web_purchase_rate),".
							" recent_ios_purchase_rate = VALUES(recent_ios_purchase_rate), recent_android_purchase_rate = VALUES(recent_android_purchase_rate), recent_amazon_purchase_rate = VALUES(recent_amazon_purchase_rate), money_in = VALUES(money_in), money_out = VALUES(money_out), playcount = VALUES(playcount);";

			$db_other->execute($insert_sql);

			sleep(1);

			$insert_cnt = 0;
			$insert_sql = "";
		}
	}
	
	if($insert_sql != "")
	{
		$insert_sql .= " ON DUPLICATE KEY UPDATE recent_avg_user_coin = VALUES(recent_avg_user_coin), recent_avg_product_coin = VALUES(recent_avg_product_coin), recent_total_buy_coin = VALUES(recent_total_buy_coin),".
						" recent_avg_buy_money = VALUES(recent_avg_buy_money), recent_total_money = VALUES(recent_total_money), recent_avg_buy_more = VALUES(recent_avg_buy_more), recent_avg_rebuyday = VALUES(recent_avg_rebuyday),".
						" recent_couponbuy_rate = VALUES(recent_couponbuy_rate), recent_basicbuy_rate = VALUES(recent_basicbuy_rate), recent_buy_count = VALUES(recent_buy_count), recent_web_purchase_rate = VALUES(recent_web_purchase_rate),".
						" recent_ios_purchase_rate = VALUES(recent_ios_purchase_rate), recent_android_purchase_rate = VALUES(recent_android_purchase_rate), recent_amazon_purchase_rate = VALUES(recent_amazon_purchase_rate), money_in = VALUES(money_in), money_out = VALUES(money_out), playcount = VALUES(playcount);";
		
		$db_other->execute($insert_sql); 
	}
	
	// Redshift Update
	$sql = "SELECT * FROM tbl_user_purchase_gamelog_type2";
	$type_list = $db_other->gettotallist($sql);
	
	$insert_sql = "";
	$insert_cnt = 0;
	$today = date("Y-m-d");
	
	for($i=0; $i<sizeof($type_list); $i++)
	{
		$useridx = $type_list[$i]["useridx"];
		$type = $type_list[$i]["type"];
		$avg_user_coin = $type_list[$i]["avg_user_coin"];
		$avg_product_coin = $type_list[$i]["avg_product_coin"];
		$total_buy_coin = $type_list[$i]["total_buy_coin"];
		$avg_buy_money = $type_list[$i]["avg_buy_money"];
		$total_money = $type_list[$i]["total_money"];
		$avg_buy_more = $type_list[$i]["avg_buy_more"];
		$avg_rebuyday = $type_list[$i]["avg_rebuyday"];
		$couponbuy_rate = $type_list[$i]["couponbuy_rate"];
		$basicbuy_rate = $type_list[$i]["basicbuy_rate"];
		$buy_count = $type_list[$i]["buy_count"];
		$money_in = $type_list[$i]["money_in"];
		$money_out = $type_list[$i]["money_out"];
		$playcount = $type_list[$i]["playcount"];
		$web_purchase_rate = $type_list[$i]["web_purchae_rate"];
		$ios_purchase_rate = $type_list[$i]["ios_purchae_rate"];
		$android_purchase_rate = $type_list[$i]["android_purchae_rate"];
		$amazon_purchase_rate = $type_list[$i]["amazon_purchae_rate"];
		
		$recent_avg_user_coin = $type_list[$i]["recent_avg_user_coin"];
		$recent_avg_product_coin = $type_list[$i]["recent_avg_product_coin"];
		$recent_total_product_coin = $type_list[$i]["recent_total_buy_coin"];
		$recent_avg_buy_money = $type_list[$i]["recent_avg_buy_money"];
		$recent_total_money = $type_list[$i]["recent_total_money"];
		$recent_avg_buy_more = $type_list[$i]["recent_avg_buy_more"];
		$recent_avg_rebuyday = $type_list[$i]["recent_avg_rebuyday"];
		$recent_couponbuy_rate = $type_list[$i]["recent_couponbuy_rate"];
		$recent_basicbuy_rate = $type_list[$i]["recent_basicbuy_rate"];
		$recent_buy_count = $type_list[$i]["recent_buy_count"];
		$recent_web_purchase_rate = $type_list[$i]["recent_web_purchase_rate"];
		$recent_ios_purchase_rate = $type_list[$i]["recent_ios_purchase_rate"];
		$recent_android_purchase_rate = $type_list[$i]["recent_android_purchase_rate"];
		$recent_amazon_purchase_rate = $type_list[$i]["recent_amazon_purchase_rate"];
		
		if($insert_cnt == 0)
		{
			$insert_sql = "INSERT INTO t5_user_purchase_gamelog_type2(useridx, type, avg_user_coin, avg_product_coin, total_buy_coin, total_money, avg_buy_money, avg_buy_more, avg_rebuyday, couponbuy_rate, basicbuy_rate, buy_count, ".
						"money_in, money_out, playcount, web_purchase_rate, ios_purchase_rate, android_purchase_rate, amazon_purchase_rate, recent_avg_user_coin, recent_avg_product_coin, recent_total_buy_coin, recent_avg_buy_money, ".
						"recent_total_money, recent_avg_buy_more, recent_avg_rebuyday, recent_couponbuy_rate, recent_basicbuy_rate, recent_buy_count, recent_web_purchase_rate, recent_ios_purchase_rate, recent_android_purchase_rate, recent_amazon_purchase_rate, writedate) ".
						"VALUES($useridx, $type, $avg_user_coin, $avg_product_coin, $total_buy_coin, $total_money, $avg_buy_money, $avg_buy_more, $avg_rebuyday, $couponbuy_rate, $basicbuy_rate, $buy_count, ".
						"$money_in, $money_out, $playcount, $web_purchase_rate, $ios_purchase_rate, $android_purchase_rate, $amazon_purchase_rate, $recent_avg_user_coin, $recent_avg_product_coin, $recent_total_product_coin, ".
						"$recent_avg_buy_money, $recent_total_money, $recent_avg_buy_more, $recent_avg_rebuyday, $recent_couponbuy_rate, $recent_basicbuy_rate, $recent_buy_count, $recent_web_purchase_rate, $recent_ios_purchase_rate, ".
						"$recent_android_purchase_rate, $recent_amazon_purchase_rate, '$today')";
		
			$insert_cnt++;
		}
		else if($insert_cnt < 1000)
		{
			$insert_sql .= ",($useridx, $type, $avg_user_coin, $avg_product_coin, $total_buy_coin, $total_money, $avg_buy_money, $avg_buy_more, $avg_rebuyday, $couponbuy_rate, $basicbuy_rate, $buy_count, ".
						"$money_in, $money_out, $playcount, $web_purchase_rate, $ios_purchase_rate, $android_purchase_rate, $amazon_purchase_rate, $recent_avg_user_coin, $recent_avg_product_coin, $recent_total_product_coin, ".
						"$recent_avg_buy_money, $recent_total_money, $recent_avg_buy_more, $recent_avg_rebuyday, $recent_couponbuy_rate, $recent_basicbuy_rate, $recent_buy_count, $recent_web_purchase_rate, $recent_ios_purchase_rate, ".
						"$recent_android_purchase_rate, $recent_amazon_purchase_rate, '$today')";
		
			$insert_cnt++;
		}
		else
		{
			$insert_sql .= ",($useridx, $type, $avg_user_coin, $avg_product_coin, $total_buy_coin, $total_money, $avg_buy_money, $avg_buy_more, $avg_rebuyday, $couponbuy_rate, $basicbuy_rate, $buy_count, ".
						"$money_in, $money_out, $playcount, $web_purchase_rate, $ios_purchase_rate, $android_purchase_rate, $amazon_purchase_rate, $recent_avg_user_coin, $recent_avg_product_coin, $recent_total_product_coin, ".
						"$recent_avg_buy_money, $recent_total_money, $recent_avg_buy_more, $recent_avg_rebuyday, $recent_couponbuy_rate, $recent_basicbuy_rate, $recent_buy_count, $recent_web_purchase_rate, $recent_ios_purchase_rate, ".
						"$recent_android_purchase_rate, $recent_amazon_purchase_rate, '$today')";
		
			$db_redshift->execute($insert_sql);
		
			sleep(1);
		
			$insert_cnt = 0;
			$insert_sql = "";
		}
	}
	
	if($insert_sql != "")
		$db_redshift->execute($insert_sql);
	
	$db_main->end();
	$db_other->end();
	$db_redshift->end();
?>