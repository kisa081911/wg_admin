<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$db_redshift = new CDatabase_Redshift();
	$db_main = new CDatabase_Main();
	
	$db_main->execute("SET wait_timeout=7200");
	
	try
	{
		
		/*$sql = "select useridx, productidx, usercoin, userlevel, coin, orderno, facebookcredit, vip_point, status, gift_status, gift_coin, couponidx, special_discount,  special_more, basecoin, canceldate, ".
				"cancelleftcoin, disputed, product_type, coupon_type, writedate	FROM t5_product_order WHERE useridx % 10000 < 5000 AND writedate >= '2019-05-27 09:00:00' AND writedate <= '2019-05-28 09:30:00'";*/
		
		$sql = "SELECT t2.useridx, t2.productidx, t2.usercoin, t2.userlevel, t2.coin, t2.orderno, t2.facebookcredit, t2.vip_point, t2.status, t2.gift_status, t2.gift_coin, t2.couponidx, t2.special_discount,  
				t2.special_more, t2.basecoin, t2.canceldate,
				t2.cancelleftcoin, t2.disputed, t2.product_type, t2.coupon_type
				FROM
				(
				SELECT * FROM `tbl_product_order` WHERE writedate >= '2019-05-28 00:00:00' AND productidx = 0
				) t1 JOIN `_tbl_product_order` t2 ON t1.orderno = t2.orderno";
		$sql_list = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($sql_list);$i++)
		{
			$useridx = $sql_list[$i]["useridx"];
			$productidx = $sql_list[$i]["productidx"];
			$usercoin = $sql_list[$i]["usercoin"];
			$userlevel = $sql_list[$i]["userlevel"];
			$coin = $sql_list[$i]["coin"];
			$orderno = $sql_list[$i]["orderno"];
			$facebookcredit = $sql_list[$i]["facebookcredit"];
			$vip_point = $sql_list[$i]["vip_point"];
			$status = $sql_list[$i]["status"];
			$gift_status = $sql_list[$i]["gift_status"];
			$gift_coin = $sql_list[$i]["gift_coin"];
			$couponidx = $sql_list[$i]["couponidx"];
			$special_discount = $sql_list[$i]["special_discount"];
			$special_more = $sql_list[$i]["special_more"];
			$basecoin = $sql_list[$i]["basecoin"];
			$canceldate = $sql_list[$i]["canceldate"];
			$cancelleftcoin = $sql_list[$i]["cancelleftcoin"];
			$disputed = $sql_list[$i]["disputed"];
			$product_type = $sql_list[$i]["product_type"];
			$coupon_type = $sql_list[$i]["coupon_type"];
			
			$insert_sql = "INSERT INTO tbl_product_order(useridx, productidx, usercoin, userlevel, coin, orderno, facebookcredit, vip_point, status, gift_status, gift_coin, couponidx, special_discount, special_more,  basecoin, canceldate, cancelleftcoin, disputed, product_type, coupon_type) ".
						"VALUES ($useridx, $productidx, $usercoin, $userlevel, $coin, '$orderno', $facebookcredit, $vip_point, $status, $gift_status, $gift_coin, $couponidx, $special_discount, $special_more, $basecoin, '$canceldate', $cancelleftcoin, $disputed, $product_type, $coupon_type) ON DUPLICATE KEY UPDATE ".
						" productidx=VALUES(productidx), usercoin=VALUES(usercoin), userlevel=VALUES(userlevel), coin=VALUES(coin), vip_point=VALUES(vip_point), gift_status=VALUES(gift_status), gift_coin=VALUES(gift_coin),  couponidx=VALUES(couponidx), special_discount=VALUES(special_discount), special_more=VALUES(special_more), basecoin=VALUES(basecoin), canceldate=VALUES(canceldate), cancelleftcoin=VALUES(cancelleftcoin), disputed=VALUES(disputed), product_type=VALUES(product_type), coupon_type=VALUES(coupon_type);";
			echo $insert_sql."<br/>";
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_redshift->end();
	$db_main->end();
?>