<?
	include("../../common/common_include.inc.php");

	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	
	$db_main2->execute("SET wait_timeout=72000");
	$db_analysis->execute("SET wait_timeout=72000");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/marketing/fb_ad_info") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/marketing/fb_ad_info") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("marketing/fb_ad_info Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$sql = "SELECT accesstoken FROM page_accesstoken WHERE userid = '0'";
	$access_token = $db_analysis->getvalue($sql);
	
	//--------------------------------facebook_casino_int-----------------------------------------
	function get_data_parsing($dataarrObj, $ad_type)
	{
		$ad_id = $dataarrObj->{'id'};
		$ad_status = $dataarrObj->{'effective_status'};
		$ad_name = $dataarrObj->{'name'};
		$campaign_id = $dataarrObj->{'campaign'}->{'id'};
		$campaign_name = $dataarrObj->{'campaign'}->{'name'};
		$campaign_status = $dataarrObj->{'campaign'}->{'effective_status'};
		$campaign_objective = $dataarrObj->{'campaign'}->{'objective'};
		$adset_id = $dataarrObj->{'adset'}->{'id'};
		$adset_name = $dataarrObj->{'adset'}->{'name'};
		$start_time = date("Y-m-d H:i:s"  ,strtotime($dataarrObj->{'adset'}->{'start_time'}));
		$buying_type = $dataarrObj->{'campaign'}->{'buying_type'};
		$link = $dataarrObj->{'creative'}->{'object_story_spec'}->{'link_data'}->{'call_to_action'}->{'value'}->{'link'};
		$link_object_id = $dataarrObj->{'creative'}->{'object_story_spec'}->{'page_id'};
		$call_to_action = $dataarrObj->{'creative'}->{'object_story_spec'}->{'link_data'}->{'call_to_action'}->{'type'};
		$application_id = $dataarrObj->{'tracking_and_conversion_with_defaults'}->{'default_tracking'}[0]->{'application'}[0];
		$application_id = ($application_id=="")?0:$application_id;
		$countries="";
		$countriesarr = $dataarrObj->{'targeting'}->{'geo_locations'}->{'countries'};
		
		for($i=0;$i<sizeof($countriesarr);$i++)
		{
			if($i==sizeof($countriesarr)-1)
			{
				$countries.=$countriesarr[$i];
			}
			else
			{
				$countries.=$countriesarr[$i].",";
			}
		}
		
		$genders = ($dataarrObj->{'targeting'}->{'genders'}[0] =="") ? 0 : $dataarrObj->{'targeting'}->{'genders'}[0];
		$age_max = $dataarrObj->{'targeting'}->{'age_max'};
		$age_min = $dataarrObj->{'targeting'}->{'age_min'};
	
		$behaviors="";
		$behaviorsarr = $dataarrObj->{'targeting'}->{'flexible_spec'}[0]->{'behaviors'};
		
		for ($i=0;$i<sizeof($behaviorsarr);$i++)
		{
			if($i==sizeof($behaviorsarr)-1)
			{
				$behaviors.=addslashes($behaviorsarr[$i]->{'id'}.":".$behaviorsarr[$i]->{'name'});
			}
			else 
			{
				$behaviors.=addslashes($behaviorsarr[$i]->{'id'}.":".$behaviorsarr[$i]->{'name'}).",";
			}
		}
	
		$excluded_connections="";
		$excluded_connectionsarr = $dataarrObj->{'targeting'}->{'excluded_connections'};
		
		for ($i=0;$i<sizeof($excluded_connectionsarr);$i++)
		{
			if($i==sizeof($excluded_connectionsarr)-1)
			{
				$excluded_connections.=$excluded_connectionsarr[$i]->{'id'};
			}
			else 
			{
				$excluded_connections.=$excluded_connectionsarr[$i]->{'id'}.",";
			}
		}
	
		$custom_audiences="";
		$custom_audiencesarr = $dataarrObj->{'targeting'}->{'custom_audiences'};
		
		for ($i=0;$i<sizeof($custom_audiencesarr);$i++)
		{
			if($i==sizeof($custom_audiencesarr)-1)
			{
				$custom_audiences.=addslashes($custom_audiencesarr[$i]->{'id'}.":".$custom_audiencesarr[$i]->{'name'});
			}
			else 
			{
				$custom_audiences.=addslashes($custom_audiencesarr[$i]->{'id'}.":".$custom_audiencesarr[$i]->{'name'}).",";
			}
		}
	
		$excluded_custom_audiences="";
		$excluded_custom_audiencesarr = $dataarrObj->{'targeting'}->{'excluded_custom_audiences'};
		
		for ($i=0;$i<sizeof($excluded_custom_audiencesarr);$i++)
		{
			if($i==sizeof($excluded_custom_audiencesarr)-1)
			{
				$excluded_custom_audiences.=addslashes($excluded_custom_audiencesarr[$i]->{'id'}.":".$excluded_custom_audiencesarr[$i]->{'name'});
			}
			else 
			{
				$excluded_custom_audiences.=addslashes($excluded_custom_audiencesarr[$i]->{'id'}.":".$excluded_custom_audiencesarr[$i]->{'name'}).",";
			}
		}
	
		$flexible_inclusions = addslashes(json_encode($dataarrObj->{'targeting'}->{'flexible_spec'}));
		$flexible_exclusions = addslashes(json_encode($dataarrObj->{'targeting'}->{'exclusions'}));
	
		$page_types="";
		$page_typesarr = $dataarrObj->{'targeting'}->{'page_types'};
		
		for ($i=0;$i<sizeof($page_typesarr);$i++)
		{
			if($i==sizeof($page_typesarr)-1)
			{
				$page_types.=$page_typesarr[$i];
			}
			else 
			{
				$page_types.=$page_typesarr[$i].",";
			}
		}
	
		$title = addslashes($dataarrObj->{'creative'}->{'object_story_spec'}->{'link_data'}->{'call_to_action'}->{'value'}->{'link_title'});
		$body = addslashes($dataarrObj->{'creative'}->{'object_story_spec'}->{'link_data'}->{'message'});
		$image_hash = $dataarrObj->{'account_id'}.":". $dataarrObj->{'creative'}->{'image_hash'};
	
		$video_thumbnail_url = $dataarrObj->{'creative'}->{'object_story_spec'}->{'video_data'}->{'image_url'};
		$url_tags = $dataarrObj->{'creative'}->{'url_tags'};
		$creative_type = $dataarrObj->{'creative'}->{'object_type'}." Page Post Ad";
		$video_id = ($dataarrObj->{'creative'}->{'object_story_spec'}->{'video_data'}->{'video_id'}=="") ? 0 : $dataarrObj->{'creative'}->{'object_story_spec'}->{'video_data'}->{'video_id'};
		$call_to_action = $dataarrObj->{'creative'}->{'object_story_spec'}->{'link_data'}->{'call_to_action'}->{'type'};
		$call_to_action_link = $dataarrObj->{'creative'}->{'object_story_spec'}->{'link_data'}->{'call_to_action'}->{'value'}->{'link'};
		
		$sql = " INSERT IGNORE INTO tbl_ad_info(ad_type, ad_id ,ad_status, ad_name, campaign_id, campaign_name, campaign_status, campaign_objective, adset_id, adset_name, start_time, buying_type, link,link_object_id ".
				" ,application_id, countries, genders, age_max, age_min, behaviors, excluded_connections, custom_audiences, excluded_custom_audiences, flexible_inclusions, flexible_exclusions ".
				" ,page_types, title, body, image_hash, video_thumbnail_url, url_tags, creative_type, video_id, call_to_action, call_to_action_link) ".
				" VALUES('$ad_type', '$ad_id', '$ad_status', '$ad_name', '$campaign_id', '$campaign_name', '$campaign_status', '$campaign_objective', '$adset_id', '$adset_name', '$start_time', '$buying_type', '$link' ".
				" , '$link_object_id', '$application_id', '$countries', '$genders', '$age_max', '$age_min', '$behaviors', '$excluded_connections', '$custom_audiences', '$excluded_custom_audiences', '$flexible_inclusions' ".
				" , '$flexible_exclusions', '$page_types', '$title', '$body', '$image_hash', '$video_thumbnail_url', '$url_tags', '$creative_type','$video_id', '$call_to_action', '$call_to_action_link');";
	
		return $sql;
	}
	
	try
	{
		set_time_limit(60 * 60);
		
		for($k=10;$k<20;$k++)
		{
    		$today = date("Y-m-d"  ,strtotime("-$k day"));
    		$yesterday = date("Y-m-d"  ,strtotime("-".($k+1)." day"));
    	
    
    		$account_list = array();
    		
    		$account_list[0] = array("ad_table" =>"tbl_ad_stats" , "ad_type"=>"11" );
    		$account_list[1] = array("ad_table" =>"tbl_ad_stats_mobile" , "ad_type"=>"12" );
    		$account_list[2] = array("ad_table" =>"tbl_ad_retention_stats" , "ad_type"=>"21" );
    		$account_list[3] = array("ad_table" =>"tbl_ad_retention_stats_mobile" , "ad_type"=>"22" );
    		
    		foreach ($account_list as $account_obj)
    		{
    			$ad_table = $account_obj['ad_table'];
    			$ad_type = $account_obj['ad_type'];
    			
    			$sql =" SELECT ad_id from $ad_table ".
    				  " WHERE today <'$today' ".
    	              " AND today >= '$yesterday' ";
    			
    			$ad_id_list = $db_main2->gettotallist($sql);
    			
    			foreach ($ad_id_list as $ad_idobj){
    				$ad_id=$ad_idobj['ad_id'];
    				// 				$facebook_json = file_get_contents('https://graph.facebook.com/v2.10/'.$ad_id.'?fields=%20[%22creative{actor_id,applink_treatment,body,call_to_action_type,dynamic_ad_voice,id,image_crops,image_hash,image_url,instagram_actor_id,instagram_permalink_url,instagram_story_id,link_og_id,link_url,object_id,object_story_id,object_story_spec{instagram_actor_id,link_data{additional_image_index,attachment_style,branded_content_sponsor_page_id,call_to_action,caption,child_attachments,description,event_id,image_crops,image_hash,link,message,multi_share_end_card,multi_share_optimized,name,picture},page_id,photo_data,template_data,text_data,video_data},object_type,object_url,place_page_set_id,platform_customizations,product_set_id,template_url,title,url_tags,video_id}%22,%22impression_control_map%22,%22account_id%22,%22ad_review_feedback%22,%22bid_amount%22,%22bid_info%22,%22bid_type%22,%22campaign{name,objective,effective_status,buying_type}%22,%22adset{name,start_time,configured_status,effective_status,id}%22,%22conversion_specs%22,%22created_time%22,%22demolink_hash%22,%22display_sequence%22,%22effective_status%22,%22engagement_audience%22,%22id%22,%22last_updated_by_app_id%22,%22locations%22,%22name%22,%22objective_source%22,%22priority%22,%22targeting%22,%22status%22,%22tracking_and_conversion_with_defaults%22,%22tracking_specs%22,%22updated_time%22,%22url_override%22]&limit=500&access_token='.$access_token);
    				
    				$url = 'https://graph.facebook.com/v3.2/'.$ad_id.'?fields=%20[%22creative{actor_id,applink_treatment,body,call_to_action_type,dynamic_ad_voice,id,image_crops,image_hash,image_url,instagram_actor_id,instagram_permalink_url,instagram_story_id,link_og_id,link_url,object_id,object_story_id,object_story_spec{instagram_actor_id,link_data{additional_image_index,attachment_style,branded_content_sponsor_page_id,call_to_action,caption,child_attachments,description,event_id,image_crops,image_hash,link,message,multi_share_end_card,multi_share_optimized,name,picture},page_id,photo_data,template_data,text_data,video_data},object_type,object_url,place_page_set_id,platform_customizations,product_set_id,template_url,title,url_tags,video_id}%22,%22account_id%22,%22ad_review_feedback%22,%22bid_amount%22,%22bid_info%22,%22bid_type%22,%22campaign{name,objective,effective_status,buying_type}%22,%22adset{name,start_time,effective_status,id}%22,%22conversion_specs%22,%22created_time%22,%22demolink_hash%22,%22display_sequence%22,%22effective_status%22,%22engagement_audience%22,%22id%22,%22last_updated_by_app_id%22,%22name%22,%22objective_source%22,%22priority%22,%22targeting%22,%22status%22,%22tracking_and_conversion_with_defaults%22,%22tracking_specs%22,%22updated_time%22]&limit=500&access_token='.$access_token;
    				
    				$cSession = curl_init();
    				
    				curl_setopt($cSession,CURLOPT_URL, $url);
    				curl_setopt($cSession,CURLOPT_RETURNTRANSFER, true);
    				curl_setopt($cSession,CURLOPT_HEADER, false);
    				
    				$facebook_json = curl_exec($cSession);
    				curl_close($cSession);
    				if ($facebook_json!="")
    				{
    					$facebook_objs = json_decode($facebook_json);
    					$db_main2->execute(get_data_parsing($facebook_objs, $ad_type));
    				}
    			}
    		}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}

	$db_main2->end();
	$db_analysis->end();
?>