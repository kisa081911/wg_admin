<?
	include("../../common/common_include.inc.php");
    include_once('../../common/simple_html_dom.php');
    
    $db_main2 = new CDatabase_Main2();
   	
    $db_main2->execute("SET wait_timeout=14400");
    
    $sdate = "2019-08-13";
    $edate = "2019-08-16";
    //$edate = "2019-05-01";
    
    while($sdate < $edate)
    {   
    	$today = $sdate;
    	
    	write_log("INSTALL : $today");
	    #
	    # appsflyer raw data DB 저장
	    #		
		// Android
		$handle = fopen('https://hq.appsflyer.com/export/id1122387239/installs_report/v5?api_token=6ab0e527-8106-44f9-81e2-d6e27fe254bb&from='.$sdate.'&to='.$sdate.'&additional_fields=install_app_store,match_type,contributor1_match_type,contributor2_match_type,contributor3_match_type,device_category,gp_referrer,gp_click_time,gp_install_begin,amazon_aid,keyword_match_type&reattr=true','r');
    	//$handle = fopen('https://hq.appsflyer.com/export/com.d8games.dfs_tw_g/installs_report/v5?api_token=424e8586-1975-4bd5-b4d5-59e6914a2607&from=2019-07-01&to=2019-08-01&additional_fields=install_app_store,match_type,contributor1_match_type,contributor2_match_type,contributor3_match_type,device_category,gp_referrer,gp_click_time,gp_install_begin,amazon_aid,keyword_match_type','r');
				
		if($handle)
		{
			$insert_sql = "";
			
			while (($data = fgetcsv($handle) ) != FALSE )
			{
			    $obj = json_decode($handle);
			    
			    $platform = 2;
			    
			    $event_type = $obj->event_type;
			    
			    if($event_type == "install" || $event_type == "re-engagement")
			    {
			        $advertising_id = $obj->advertising_id;
			        $install_time = $obj->install_time;
			        $agency = $obj->agency;
			        $media_source = $obj->media_source;
			        $channel = ($obj->af_channel == "") ? '' : $obj->af_channel;
			        $keyword = ($obj->af_keywords == "") ? '' : $obj->af_keywords;
			        
			        $fb_campaign_id = $obj->fb_campaign_id;
			        $fb_campaign_name = $obj->fb_campaign_name;
			        $fb_adset_id = $obj->fb_adset_id;
			        $fb_adset_name = $obj->fb_adset_name;
			        $fb_adgroup_name = $obj->fb_adgroup_name;
			        
			        $af_ad = $obj->af_ad;
			        $af_ad_id = $obj->af_ad_id;
			        $af_adset = $obj->af_adset;
			        $af_adset_id = $obj->af_adset_id;
			        
			        $campaign = encode_db_field($obj->campaign);
			        $site_id = encode_db_field($obj->af_siteid);
			        $country_code = $obj->country_code;
			        $city = $obj->city;
			        $languege = $obj->languege;
			        $appsflyer_device_id = $obj->appsflyer_device_id;
			        $fb_ad_id = ($obj->fb_adgroup_id == "") ? 0 : $obj->fb_adgroup_id;
			        
			        $device_id = $obj->android_id;
			        $device_type = $obj->device_model;
			        $is_retargeting = $obj->is_retargeting;
			        $retargeting_conversion_type = $obj->retargeting_conversion_type;
			        
			        $re_targeting_conversion_type = $obj->re_targeting_conversion_type;
			        
			        $attributed_touch_time = $obj->attributed_touch_time;
			        
			        
			        //if(($re_targeting_conversion_type == "re-engagement" || $re_targeting_conversion_type == "re-attribution") && $media_source == "" && $agency == "")
			        //	$media_source = "remerge_int";
			        
			        if($retargeting_conversion_type == "" && $re_targeting_conversion_type != "")
			            $retargeting_conversion_type = $re_targeting_conversion_type;
			            
			            $af_sub1 = encode_db_field($obj->af_sub1);
			            $af_sub2 = encode_db_field($obj->af_sub2);
			            $af_sub3 = encode_db_field($obj->af_sub3);
			            $af_sub4 = encode_db_field($obj->af_sub4);
			            $af_sub5 = encode_db_field($obj->af_sub5);
			            
			            $sql = "INSERT IGNORE INTO tmp_tbl_appsflyer_install_re_attribution(platform, is_retargeting, retargeting_conversion_type, fb_ad_id, attributed_touch_time, install_time, agency, media_source, fb_campaign_id, fb_campaign_name, fb_adset_id, fb_adset_name, fb_adgroup_name, campaign, channel, keyword, site_id, sub1, sub2, sub3, sub4, sub5, country_code, city, languege, appsflyer_device_id, adset_id, adset, ad_id, ad, adv_id, device_id, device_type) ".
			 			            "VALUES($platform, '$is_retargeting', '$retargeting_conversion_type', '$fb_ad_id', '$attributed_touch_time', '$install_time', '$agency', '$media_source', '$fb_campaign_id', '$fb_campaign_name', '$fb_adset_id', '$fb_adset_name', '$fb_adgroup_name', '$campaign', '$channel', '$keyword', '$site_id', '$af_sub1', '$af_sub2', '$af_sub3', '$af_sub4', '$af_sub5', '$country_code', '".encode_db_field($city)."', '$languege', '$appsflyer_device_id', '$af_adset_id', '$af_adset', '$af_ad_id', '$af_ad', '$advertising_id', '$device_id', '$device_type');";
			            $db_main2->execute($sql);
			    }
			    else
			    {
			        $install_time = $obj->install_time;
			        $event_name = $obj->event_name;
			        $event_time = $obj->event_time;
			        $event_value = $obj->event_value;
			        
			        $pos = strpos($event_value, "af_revenue");
			        
			        if($pos === false)
			        {}
			        else
			        {
			            $event_info = json_decode($event_value);
			            
			            $event_value = $event_info->af_revenue;
			        }
			        
			        $agency = $obj->agency;
			        $media_source = $obj->media_source;
			        $channel = ($obj->af_channel == "") ? '' : $obj->af_channel;
			        $keyword = ($obj->af_keywords == "") ? '' : $obj->af_keywords;
			        
			        $fb_campaign_id = $obj->fb_campaign_id;
			        $fb_campaign_name = $obj->fb_campaign_name;
			        $fb_adset_id = $obj->fb_adset_id;
			        $fb_adset_name = $obj->fb_adset_name;
			        $fb_adgroup_name = $obj->fb_adgroup_name;
			        
			        $af_ad = $obj->af_ad;
			        $af_ad_id = $obj->af_ad_id;
			        $af_adset = $obj->af_adset;
			        $af_adset_id = $obj->af_adset_id;
			        
			        $campaign = encode_db_field($obj->campaign);
			        $site_id = encode_db_field($obj->af_siteid);
			        $country_code = $obj->country_code;
			        $city = $obj->city;
			        $languege = $obj->languege;
			        $appsflyer_device_id = $obj->appsflyer_device_id;
			        $device_id = $obj->android_id;
			        $device_type = $obj->device_model;
			        $is_retargeting = $obj->is_retargeting;
			        $retargeting_conversion_type = $obj->retargeting_conversion_type;
			        
			        $re_targeting_conversion_type = $obj->re_targeting_conversion_type;
			        
			        //if(($re_targeting_conversion_type == "re-engagement" || $re_targeting_conversion_type == "re-attribution") && $media_source == "" && $agency == "")
			        //	$media_source = "remerge_int";
			        
			        if($retargeting_conversion_type == "" && $re_targeting_conversion_type != "")
			            $retargeting_conversion_type = $re_targeting_conversion_type;
			            
			            $af_sub1 = $obj->af_sub1;
			            $af_sub2 = $obj->af_sub2;
			            $af_sub3 = $obj->af_sub3;
			            $af_sub4 = $obj->af_sub4;
			            $af_sub5 = $obj->af_sub5;
			            
			            $is_number = true;
			            
			            $checks = "0123456789.";
			            
			            for ($i=0; $i<strlen($event_value); $i++)
			            {
			                if (strpos($checks, substr($event_value, $i, 1)) === false)
			                {
			                    $is_number = false;
			                }
			            }
			            
			            $is_insert = false;
			            
			            if($is_number)
			            {
			                $is_insert = true;
			            }
			            else
			            {
			                $event_info = json_decode($event_value);
			                
			                if($event_info->af_revenue != "")
			                {
			                    $event_value = $event_info->af_revenue;
			                    
			                    $is_insert = true;
			                }
			            }
			            
			            if($event_value == "")
			                $event_value = 0;
			                
			                if($is_insert)
			                {
			                    $sql = "SELECT useridx ".
			 			                    "FROM tbl_user_mobile_appsflyer_new ".
			 			                    "WHERE os_type=$platform AND appsflyerid='$appsflyer_device_id' ".
			 			                    "ORDER BY writedate DESC ".
			 			                    "LIMIT 1";
			                    $useridx = $db_main2->getvalue($sql);
			                    
			                    $sql = "INSERT INTO tmp_tbl_appsflyer_inappevent_re_attribution(platform, is_retargeting, retargeting_conversion_type, useridx, install_time, event_name, event_time, event_value, agency, media_source, fb_campaign_id, fb_campaign_name, fb_adset_id, fb_adset_name, fb_adgroup_name, campaign, channel, keyword, site_id, sub1, sub2, sub3, sub4, sub5, country_code, city, languege, appsflyer_device_id, adset_id, adset, ad_id, ad, device_id, device_type) ".
			 			                    "VALUES($platform, '$is_retargeting', '$retargeting_conversion_type', '$useridx', '$install_time', '$event_name', '$event_time', '$event_value', '$agency', '$media_source', '$fb_campaign_id', '$fb_campaign_name', '$fb_adset_id', '$fb_adset_name', '$fb_adgroup_name', '$campaign', '$channel', '$keyword', '$site_id', '$af_sub1', '$af_sub2', '$af_sub3', '$af_sub4', '$af_sub5', '$country_code', '".encode_db_field($city)."', '$languege', '$appsflyer_device_id', '$af_adset_id', '$af_adset', '$af_ad_id', '$af_ad', '$device_id', '$device_type');";
			                    $db_main2->execute($sql);
			                }
			    }
				
		}
		
		sleep(1);
		
		$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));	
    }
    }
	$db_main2->end();
?>