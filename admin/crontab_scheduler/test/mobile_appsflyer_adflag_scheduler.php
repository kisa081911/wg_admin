<?
	include("../../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_mobile = new CDatabase_Mobile();
	
	$db_main->execute("SET wait_timeout=3600");
	$db_main2->execute("SET wait_timeout=3600");
	$db_mobile->execute("SET wait_timeout=3600");
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/marketing/mobile_appsflyer_adflag_scheduler") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/marketing/mobile_appsflyer_adflag_scheduler") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("marketing/mobile_appsflyer_adflag_scheduler Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$back_day = date("Y-m-d", time() - 60 * 60 * 24 * 7);
	
	$yesterday = "2019-08-13";
	$today = date("Y-m-d", time());
// 	$dumy_tomorrow = date("Y-m-d", time() - 60 * 60 * 24 * 3);
	$dumy_tomorrow = "2019-08-13";
		
	try
	{
		// adflag 없는 유저 보정
		$sql = "SELECT useridx, platform, createdate
				FROM tbl_user_ext
				WHERE useridx in (
            4365219
            ,3119603
            ,4365089
            ,4365057
            ,1036730
            ,4364908
            ,2135461
            ,4364891
            ,1645787
            ,3197232
            ,2359443
            ,2133652
            ,1027571
            ,322247
            ,491977
            ,2584311
            ,2582755
            ,2659933
            ,4366274
            ,4366218
            ,4105826
            ,4366190
            ,153461
            ,4212778
            ,4365932
            ,3155351
            ,323010
            ,1077334
            ,3148117
            ,207979
            ,4370255
            ,2146614
            ,81674
            ,1679214
            ,3873022
            ,588800
            ,3211768
            ,4194900
            ,3850158
            ,2737373
            ,2596120
            ,4367895
            ,3208363
            ,2540941
            ,3275182
            ,3826558
            ,4363883
            ,3019482
            ,3047159
            ,1027571
            ,4367517
            ,4367232
            ,4367122
            ,2064281
            ,4365826
            ,4365953
            ,1679089
            ,666106
            ,4370356
            ,4072044
            ,4369920
            ,3882729
            ,4369493
            ,1546989
            ,2410569
            ,1797906
            ,1121807
            ,2383039
            ,2421296
            ,2757553
            ,4376940
            ,2925113
            ,2396859
            ,2499266
            )";
		$user_list = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($user_list); $i++)
		{
			$useridx = $user_list[$i]["useridx"];
			$platform = $user_list[$i]["platform"];
			$createdate = $user_list[$i]["createdate"];
			
			$sql = "SELECT logidx, platform, agency, media_source, fb_ad_id, fb_campaign_name, campaign, site_id, t2.appsflyer_device_id, t1.deviceid, install_time, 
						DATEDIFF('$createdate', install_time) AS install_diff, IF(TIMESTAMPDIFF(HOUR, install_time, '$createdate') BETWEEN 0 AND 48, 1, 0) AS is_install	
					FROM (
						SELECT appsflyerid, IF(os_type = 1, advid, deviceid) AS deviceid
						FROM tbl_user_mobile_appsflyer_new
						WHERE useridx = $useridx AND os_type = $platform
					) t1 JOIN tbl_appsflyer_install t2 ON t1.appsflyerid = t2.appsflyer_device_id
					ORDER BY logidx DESC
					LIMIT 1";
			write_log($sql);
			$apps_info = $db_main2->getarray($sql);
			
			$platform = $apps_info["platform"];
			
			if($platform != "")
			{
				$logidx = $apps_info["logidx"];
				$agency = $apps_info["agency"];
				$media_source = $apps_info["media_source"];
				$fb_ad_id = $apps_info["fb_ad_id"];
				$fb_campaign_name = $apps_info["fb_campaign_name"];
				$site_id = $apps_info["site_id"];
				$device_id = $apps_info["deviceid"];
				$appsflyer_device_id = $apps_info["appsflyer_device_id"];
				$install_diff = $apps_info["install_diff"];
				$is_install = $apps_info["is_install"];

				if($is_install > 0)
				{
				    $campaign_id = 0;
				    
				    if($media_source == "googleadwords_int")
				    {
				        $campaign_id = $apps_list[$i]["campaign"];
				    }
				    
				    if($media_source == "Facebook Ads")
				    {
				        if($agency == "nanigans")
				            $media_source = $agency;
				            else if($agency == "socialclicks" )
				                $media_source = $agency;
				    }
				    else if($media_source == "")
				    {
				        if($agency == "amazon")
				            $media_source = $agency;
				    }
				    
				    if(strpos($fb_campaign_name, "m_retention") !== false)
				    {
				        $campaign_name_arr = explode("_", $fb_campaign_name);
				        $media_source = "m_".$campaign_name_arr[2];
				    }
				    else if(strpos($fb_campaign_name, "retention") !== false)
				    {
				        $campaign_name_arr = explode("_", $fb_campaign_name);
				        $media_source = $campaign_name_arr[1];
				    }
				    else if(strpos($fb_campaign_name, "m_duc") !== false || strpos($fb_campaign_name, "m_ddi") !== false)
				    {
				        $campaign_name_arr = explode("_", $fb_campaign_name);
				        
				        if($campaign_name_arr[1] == "m")
				            $media_source = "m_".$campaign_name_arr[2]."_int";
			            else
			                $media_source = "m_".$campaign_name_arr[3]."_int";
				    }
				    
				    $sql = "UPDATE tbl_user_ext SET adflag = '$media_source', chain_adflag = '$media_source', ifcontext = '$site_id' WHERE useridx=$useridx";
				    $db_main->execute($sql);
				    
				    if($logidx > 0)
				    {
				    	$sql = "UPDATE tbl_appsflyer_install SET install_useridx = $useridx WHERE logidx = $logidx";
					write_log($sql);
				    	$db_main2->execute($sql);
				    }
				}
			}
		}
		
// 		// retention
// 		$sql = "SELECT rtidx, platform, useridx, adflag, createdate, writedate
// 				FROM tbl_user_retention_mobile_log
// 				WHERE writedate >= '$yesterday 00:00:00' AND writedate <= '$today 23:59:59'
// 				AND (adflag LIKE '%_int' OR adflag LIKE 'Facebook%' OR adflag LIKE 'amazon')";
// 		$retention_list = $db_main2->gettotallist($sql);
		
// 		for($i=0; $i<sizeof($retention_list); $i++)
// 		{
// 			$rtidx = $retention_list[$i]["rtidx"];
// 			$platform = $retention_list[$i]["platform"];
// 			$useridx = $retention_list[$i]["useridx"];
// 			$media_source = $retention_list[$i]["adflag"];
// 			$createdate = $retention_list[$i]["createdate"];
// 			$writedate = $retention_list[$i]["writedate"];
// 			$site_id = "";
			
// 			//appsflyer
// 			$sql = "SELECT appsflyerid, IF(os_type = 1, advid, deviceid) AS deviceid
// 					FROM tbl_user_mobile_appsflyer_new
// 					WHERE useridx = $useridx AND os_type = $platform
// 					ORDER BY writedate DESC
// 					LIMIT 1";
// 			$user_info = $db_main2->getarray($sql);
			
// 			$appsflyerid = $user_info["appsflyerid"];
// 			$device_id = $user_info["deviceid"];
						
// 			if($appsflyerid != "")
// 			{
// 				$sql = "SELECT site_id
// 						FROM tbl_appsflyer_install 
// 						WHERE appsflyer_device_id = '$appsflyerid' AND platform = $platform AND 0 <= DATEDIFF('$writedate', install_time) AND DATEDIFF('$writedate', install_time) <= 2
// 						ORDER BY logidx DESC
// 						LIMIT 1";
// 				$apps_info = $db_main2->getarray($sql);
				
// 				$site_id = $apps_info["site_id"];
				
// 				if($site_id != "")
// 				{
// 					$sql = "UPDATE tbl_user_retention_mobile_log SET site_id = '$site_id' WHERE rtidx = $rtidx";
// 					$db_main2->execute($sql);
// 				}
// 			}
// 		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();
	$db_mobile->end();
?>