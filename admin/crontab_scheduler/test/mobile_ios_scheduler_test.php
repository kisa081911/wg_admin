<?
	include("../../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_friend = new CDatabase_Friend();
	$db_livestats = new CDatabase_Livestats();
	$db_inbox = new CDatabase_Inbox();
	
	$db_main->execute("SET wait_timeout=60");
	$db_main2->execute("SET wait_timeout=60");
	$db_friend->execute("SET wait_timeout=60");
	$db_livestats->execute("SET wait_timeout=60");
	$db_inbox->execute("SET wait_timeout=60");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/mobile_purchase/mobile_ios_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/mobile_purchase/mobile_ios_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("mobile_ios_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
		$isSandbox = true;
	else
		$isSandbox = false;
	
	function getReceiptData($receipt, $isSandbox)
	{
		// determine which endpoint to use for verifying the receipt
		if ($isSandbox)
		{
			$endpoint = 'https://sandbox.itunes.apple.com/verifyReceipt';
		}
		else
		{
			$endpoint = 'https://buy.itunes.apple.com/verifyReceipt';
		}
	
		// build the post data
		$postData = json_encode(
				array('receipt-data' => $receipt)
		);
	
		// create the cURL request
		$ch = curl_init($endpoint);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
	
		// execute the cURL request and fetch response data
		$response = curl_exec($ch);
		$errno    = curl_errno($ch);
		$errmsg   = curl_error($ch);
		curl_close($ch);
	
		// ensure the request succeeded
		if ($errno != 0)
		{
			throw new Exception($errmsg, $errno);
		}
	
		// parse the response data
		$data = json_decode($response);
	
		// ensure response data was a valid JSON string
		if (!is_object($data))
		{
			throw new Exception('Invalid response data', 100);
		}
	
		// ensure the expected data is present
		if (!isset($data->status) || $data->status != 0)
		{
			throw new Exception('Invalid receipt', 200);
		}
	
		// build the response array with the returned data
		return array(
				'quantity'       =>  $data->receipt->quantity,
				'product_id'     =>  $data->receipt->product_id,
				'transaction_id' =>  $data->receipt->transaction_id,
				'purchase_date'  =>  $data->receipt->purchase_date,
				'app_item_id'    =>  $data->receipt->app_item_id,
				'bid'            =>  $data->receipt->bid,
				'bvrs'           =>  $data->receipt->bvrs
		);
	}
	
	$sql = " SELECT * FROM tbl_product_order_mobile where orderidx IN (74445,73958,73603,73295,73244,73122,73028,72875)".
			" ORDER BY writedate ASC";
	$receipt_list = $db_main->gettotallist($sql);
	
	for ($i=0; $i<sizeof($receipt_list); $i++)
	{
		$useridx = $receipt_list[$i]["useridx"];
		$productidx = $receipt_list[$i]["productidx"];
		$receipt = $receipt_list[$i]["receipt"];
		$errcount = $receipt_list[$i]["errcount"];
		$orderno = $receipt_list[$i]["orderno"];
		$orderidx = $receipt_list[$i]["orderidx"];
		$money = $receipt_list[$i]["money"];
		$mobile_facebookcredit = $receipt_list[$i]["facebookcredit"];
		$usercoin = $receipt_list[$i]["usercoin"];
		$coin = $receipt_list[$i]["coin"];
		$basecoin = $receipt_list[$i]["basecoin"];
		$gift_coin = $receipt_list[$i]["gift_coin"];
		$special_more = $receipt_list[$i]["special_more"];
		$product_targetcredit = $receipt_list[$i]["targetcredit"];
		$product_category = $receipt_list[$i]["product_category"];
	
		// verify the receipt
		try
		{
			$info = getReceiptData($receipt, $isSandbox);
						
			if($info['transaction_id'] == $orderno)
			{
				write_log("orderno - ".$orderno);
			}
			else
			{
				write_log("check fail orderno - ".$orderno);
			}
		}
		catch (Exception $ex)
		{}
	}
	
	$db_main->end();
	$db_main2->end();
	$db_friend->end();
	$db_livestats->end();
	$db_inbox->end();
?>