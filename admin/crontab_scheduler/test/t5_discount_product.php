<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");

	try
	{
		$db_main = new CDatabase_Main();
		$db_redshift = new CDatabase_Redshift();
		
		$db_main->execute("SET wait_timeout=3600");		
		
		$sql = "SELECT orderidx, useridx, basecoin FROM tbl_product_order WHERE useridx > 20000 AND STATUS = 1 AND special_discount > 0 AND orderidx <= 443388";
		$log_list = $db_main->gettotallist($sql);
		
		for($i = 0; $i < sizeof($log_list); $i++)
		{
			$orderidx = $log_list[$i]["orderidx"];
			$useridx = $log_list[$i]["useridx"];
			$basecoin = $log_list[$i]["basecoin"];
			
			$update_sql = "UPDATE t5_product_order SET basecoin = $basecoin WHERE orderidx = $orderidx AND useridx = $useridx";
			$db_redshift->execute($update_sql);
		}
		
		$db_main->end();
		$db_redshift->end();		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>