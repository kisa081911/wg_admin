<?	
    include("../../common/common_include.inc.php");

	$db_main_slave = new CDatabase_Slave_Main();
	$db_other = new CDatabase_Other();
	
	$db_main_slave->execute("SET wait_timeout=21600");
	$db_other->execute("SET wait_timeout=21600");
	
	ini_set("memory_limit", "-1");
	
	try
	{
	    //$sdate = date("Y-m-d", time() - 24 * 60 * 60 * 1);
	    //$edate = date("Y-m-d", time() + 24 * 60 * 60);

		$sql = "SELECT orderidx, platform,  useridx, money, basecoin,  coin, special_more, special_discount, usercoin, vip_point,  product_type, coupon_type, status, canceldate, writedate, ".
				  	"	IF(coupon_type IN (2, 3, 4), 1, 0) AS is_sale, DAYOFWEEK(writedate) AS today_week, WEEK(writedate) AS weeks, DATE_FORMAT(writedate, '%Y-%m-%d') AS ymd,   ".
					"	(SELECT createdate FROM tbl_user WHERE useridx = tt.useridx) AS user_createdate, ".
					"	DATEDIFF(tt.writedate, (SELECT createdate FROM tbl_user WHERE useridx = tt.useridx)) AS dayafterinstall,	".
					"	IFNULL((	".
					"		SELECT SUM(facebookcredit/10)FROM tbl_product_order WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate < tt.writedate	".
					"	), 0) AS web_accrue_money,	".
					"	(SELECT COUNT(*) FROM tbl_product_order	WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate < tt.writedate) AS web_accrue_buycnt,	".
					"	IFNULL((	".
					"		SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate < tt.writedate	".
					"	), 0) AS mobile_accrue_money,	".
					"	(SELECT COUNT(*) FROM tbl_product_order_mobile	WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate < tt.writedate) AS mobile_accrue_buycnt,	".
					"	IFNULL((	".
					"		SELECT SUM(facebookcredit/10) FROM tbl_product_order WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate <= tt.writedate	".
					"	), 0) AS web_total_money,	".
					"	IFNULL((	".
					"		SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate <= tt.writedate	".
					"	), 0) AS mobile_total_money	".
					"FROM	".
					"(	".
					"	SELECT orderidx, 0 AS platform, useridx, (facebookcredit/10) AS money, basecoin,  coin, special_more, special_discount, usercoin, vip_point,  product_type, coupon_type, STATUS, canceldate, writedate	".
					"	FROM tbl_product_order t1	".
					"	WHERE useridx > 20000 AND STATUS IN (1, 3) AND '2018-11-19 00:00:00' <= writedate AND writedate < '2018-11-20 00:00:00'	". 
					"	UNION ALL	".
					"	SELECT orderidx, os_type AS platform, useridx, money, basecoin,  coin, special_more, special_discount, usercoin, vip_point,  product_type, coupon_type, STATUS, canceldate, writedate	".
					"	FROM tbl_product_order_mobile t2	".
					"	WHERE useridx > 20000 AND STATUS IN (1, 3) AND '2018-11-19 00:00:00' <= writedate AND writedate < '2018-11-20 00:00:00'	". 
					") tt";
		   $order_list = $db_main_slave->gettotallist($sql);
		   
		   $insert_cnt = 0;
		   $insert_sql = "";
		    
		   for($i=0; $i<sizeof($order_list); $i++)
		   {
		   		$orderidx = $order_list[$i]["orderidx"];
		   		$platform = $order_list[$i]["platform"];
		   		$useridx = $order_list[$i]["useridx"];
		   		$money = $order_list[$i]["money"];
		   		$basecoin = $order_list[$i]["basecoin"];
		   		$coin = $order_list[$i]["coin"];
		   		$special_more = $order_list[$i]["special_more"];
		   		$special_discount = $order_list[$i]["special_discount"];
		   		$usercoin = $order_list[$i]["usercoin"];
		   		$vip_point = $order_list[$i]["vip_point"];
		   		$product_type = $order_list[$i]["product_type"];
		   		$coupon_type = $order_list[$i]["coupon_type"];
		   		$status = $order_list[$i]["status"];
		   		$canceldate = $order_list[$i]["canceldate"];
		   		$writedate = $order_list[$i]["writedate"];
		   		$is_sale = $order_list[$i]["is_sale"];
		   		$dayweek = $order_list[$i]["today_week"];
		   		$weeks = $order_list[$i]["weeks"];
		   		$ymd = $order_list[$i]["ymd"];	   		
		   		$dayafterinstall = $order_list[$i]["dayafterinstall"];
		   		
		   		$web_accrue_money = $order_list[$i]["web_accrue_money"];
		   		$web_accrue_buycnt = $order_list[$i]["web_accrue_buycnt"];
		   		
		   		$mobile_accrue_money = $order_list[$i]["mobile_accrue_money"];
		   		$mobile_accrue_buycnt = $order_list[$i]["mobile_accrue_buycnt"];
		   		
		   		$web_total_money = $order_list[$i]["web_total_money"];
		   		$mobile_total_money = $order_list[$i]["mobile_total_money"];
		   			
		   			
		   		$accrue_money = $web_accrue_money + $mobile_accrue_money;
		   		$accrue_buycnt = $web_accrue_buycnt + $mobile_accrue_buycnt;
		   		
		   		$total_money = $web_total_money + $mobile_total_money;	   		
		   		
		   		if($total_money >= 9999)
		   			$vip_level = 9;
		   		else if($total_money >= 7999)
		   			$vip_level = 8;
		   		else if($total_money >= 5999)
		   			$vip_level = 7;
		   		else if($total_money >= 2999)
		   			$vip_level = 6;
		   		else if($total_money >= 1999)
		   			$vip_level = 5;
		   		else if($total_money >= 999)
		   			$vip_level = 4;
		   		else if($total_money >= 499)
		   			$vip_level = 3;
		   		else if($total_money >= 99)
		   			$vip_level = 2;
		   		else if($total_money >= 39)
		   			$vip_level = 1;
		   		else 
		   			$vip_level = 0;
		   		
		   		$dayafterpurchase = "0000-00-00 00:00:00";
		   		
		   		// 이전 직전 결제와 현재 결제 diff
		   		if($writedate != "")
		   		{
			   		$sql = "SELECT DATEDIFF('$writedate', writedate) ".
							"FROM	".
							"(	".
							"	SELECT writedate	".
							"	FROM tbl_product_order t1	".
							"	WHERE useridx = $useridx AND STATUS IN (1, 3) AND writedate < '$writedate'	". 
							"	UNION ALL	".
							"	SELECT writedate	".
							"	FROM tbl_product_order_mobile t2	".
							"	WHERE useridx = $useridx AND STATUS IN (1, 3) AND writedate < '$writedate'	". 
							") tt ORDER BY writedate DESC LIMIT 1";
			   		$dayafterpurchase = $db_main_slave->getvalue($sql);
				}
				
				if($dayafterpurchase == "")
					$dayafterpurchase = 0;
				
				$recent_4w_money = 0;
				$recent_4w_buycnt = 0;
				
				//현재 결제 기준을 00:00:00로 변경 후 최근 4week(28day) 기준
				if($ymd != "")
				{
			   		$sql = "SELECT IFNULL(SUM(money),0) AS recent_4week_money, COUNT(*) AS recent_4week_cnt ".
							"FROM	".
							"(	".
							"	SELECT (facebookcredit/10) AS money, writedate	".
							"	FROM tbl_product_order t1	".
							"	WHERE useridx = $useridx AND STATUS IN (1, 3) AND writedate >= DATE_SUB('$ymd 00:00:00', INTERVAL 4 WEEK) AND writedate < '$ymd 00:00:00'	".
							"	UNION ALL	".
							"	SELECT money, writedate	".
							"	FROM tbl_product_order_mobile t2	".
							"	WHERE useridx = $useridx AND STATUS IN (1, 3) AND writedate >= DATE_SUB('$ymd 00:00:00', INTERVAL 4 WEEK) AND writedate < '$ymd 00:00:00'	".
							") tt";		   		
			   		$recent_4week_info = $db_main_slave->getarray($sql);
			   		
			   		$recent_4w_money = $recent_4week_info["recent_4week_money"];
			   		$recent_4w_buycnt = $recent_4week_info["recent_4week_cnt"];		   		
				}
				
				if($insert_cnt == 0)
				{
		   			$insert_sql = "INSERT INTO tbl_product_order_detail(orderidx, platform, useridx, money, basecoin, coin, special_more, special_discount, usercoin, vip_level, vip_point, product_type, coupon_type, status, canceldate, writedate, is_sale, weeks, dayweek, dayafterpurchase, dayafterinstall, recent_4w_money, recent_4w_buycnt, accrue_money, accrue_buycnt) VALUES ".
				   					" ('$orderidx', '$platform', '$useridx', '$money', '$basecoin', '$coin', '$special_more', '$special_discount', '$usercoin', '$vip_level', '$vip_point', '$product_type', '$coupon_type', '$status', '$canceldate', '$writedate', '$is_sale', '$weeks', '$dayweek', '$dayafterpurchase', '$dayafterinstall', '$recent_4w_money', '$recent_4w_buycnt', '$accrue_money', '$accrue_buycnt') ";
		   			$insert_cnt++;
				}
				else if($insert_cnt <= 100)
				{
					$insert_sql .= ", ('$orderidx', '$platform', '$useridx', '$money', '$basecoin', '$coin', '$special_more', '$special_discount', '$usercoin', '$vip_level', '$vip_point', '$product_type', '$coupon_type', '$status', '$canceldate', '$writedate', '$is_sale', '$weeks', '$dayweek', '$dayafterpurchase', '$dayafterinstall', '$recent_4w_money', '$recent_4w_buycnt', '$accrue_money', '$accrue_buycnt') ";
					
					$insert_cnt++;
				}
				else 
				{
					$insert_sql .= " ON DUPLICATE KEY UPDATE useridx=VALUES(useridx), money=VALUES(money), basecoin=VALUES(basecoin), coin=VALUES(coin), special_more=VALUES(special_more), special_discount=VALUES(special_discount), usercoin=VALUES(usercoin), vip_level=VALUES(vip_level), vip_point=VALUES(vip_point), product_type=VALUES(product_type), coupon_type=VALUES(coupon_type), status=VALUES(status), canceldate=VALUES(canceldate), writedate=VALUES(writedate), ".
					" is_sale=VALUES(is_sale), weeks=VALUES(weeks), dayweek=VALUES(dayweek), dayafterpurchase=VALUES(dayafterpurchase), dayafterinstall=VALUES(dayafterinstall), recent_4w_money=VALUES(recent_4w_money), recent_4w_buycnt=VALUES(recent_4w_buycnt), accrue_money=VALUES(accrue_money), accrue_buycnt=VALUES(accrue_buycnt);";
					$db_other->execute($insert_sql);					
					
					$insert_sql = "";
					$insert_cnt = 0;
				}
		   }
		   
		   if($insert_sql != "")
		   {
			   	$insert_sql .= " ON DUPLICATE KEY UPDATE useridx=VALUES(useridx), money=VALUES(money), basecoin=VALUES(basecoin), coin=VALUES(coin), special_more=VALUES(special_more), special_discount=VALUES(special_discount), usercoin=VALUES(usercoin), vip_level=VALUES(vip_level), vip_point=VALUES(vip_point), product_type=VALUES(product_type), coupon_type=VALUES(coupon_type), status=VALUES(status), canceldate=VALUES(canceldate), writedate=VALUES(writedate), ".
			   			" is_sale=VALUES(is_sale), weeks=VALUES(weeks), dayweek=VALUES(dayweek), dayafterpurchase=VALUES(dayafterpurchase), dayafterinstall=VALUES(dayafterinstall), recent_4w_money=VALUES(recent_4w_money), recent_4w_buycnt=VALUES(recent_4w_buycnt), accrue_money=VALUES(accrue_money), accrue_buycnt=VALUES(accrue_buycnt);";
			   	
			   	$db_other->execute($insert_sql);
		   }
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	}
	
	try 
	{
		//4주전 기준으로 status가 2나 3인 데이터를 가져옴
		$sql = "SELECT * FROM ". 
				"(	".
				"	SELECT orderidx, useridx, 0 AS os_type, status, canceldate, writedate, cancelleftcoin, disputed	".
				"	FROM `tbl_product_order` WHERE useridx > 20000 AND DATE_SUB(NOW(), INTERVAL 4 WEEK) <=  canceldate  AND status IN (2,3)	".
				"	UNION ALL	".
				"	SELECT orderidx, useridx, os_type, STATUS, canceldate, writedate, cancelleftcoin, disputed	".
				"	FROM `tbl_product_order_mobile` WHERE useridx > 20000 AND DATE_SUB(NOW(), INTERVAL 4 WEEK) <=  canceldate  AND STATUS IN (2,3)	".
				") t1 ORDER BY canceldate ASC";
		$delete_data = $db_main_slave->gettotallist($sql);
		
		for($i=0; $i<sizeof($delete_data); $i++)
		{
			$orderidx =	$delete_data[$i]["orderidx"];
			$useridx = $delete_data[$i]["useridx"];
			$os_type = $delete_data[$i]["os_type"];
			$status = $delete_data[$i]["status"];
			$canceldate = $delete_data[$i]["canceldate"];
			$writedate = $delete_data[$i]["writedate"];
			$cancelleftcoin = $delete_data[$i]["cancelleftcoin"];
			$disputed = $delete_data[$i]["disputed"];
			
			
			//tbl_product_order_detail에 위 데이터가 있는지 조회(존재하면  status가 변경된 데이터)
			$sql = "SELECT COUNT(*) FROM tbl_product_order_detail WHERE orderidx = $orderidx AND platform = $os_type AND useridx = $useridx";
			$exist_data = $db_other->getvalue($sql);
				
			if($exist_data == 1)
			{			
				if(os_type == 0)
				{				
					$from_statement = "SELECT orderidx, 0 AS platform, useridx, (facebookcredit/10) AS money, basecoin,  coin, special_more, special_discount, usercoin, vip_point,  product_type, coupon_type, STATUS, canceldate, writedate	".
										"	FROM tbl_product_order t1	".
										"	WHERE useridx = $useridx AND STATUS IN (1, 3) AND '$writedate' <= writedate";
				}
				else
				{			
					$from_statement = "SELECT orderidx, os_type AS platform, useridx, money, basecoin,  coin, special_more, special_discount, usercoin, vip_point,  product_type, coupon_type, STATUS, canceldate, writedate	".
										"	FROM tbl_product_order_mobile t2	".
										"	WHERE useridx = $useridx AND STATUS IN (1, 3) AND '$writedate' <= writedate	";
				}

				$update_sql = "UPDATE tbl_product_order_detail SET status = $status , canceldate = '$canceldate' WHERE orderidx = $orderidx AND platform = $os_type AND useridx = $useridx;";
				$db_other->execute($update_sql);
				
				//status 2(취소)일 경우 그 이후 데이터 보정
				if($status == 2)
				{
					$sql = "SELECT orderidx, platform,  useridx, money, basecoin,  coin, special_more, special_discount, usercoin, vip_point,  product_type, coupon_type, status, canceldate, writedate, ".
							"	IF(coupon_type IN (2, 3, 4), 1, 0) AS is_sale, DAYOFWEEK(writedate) AS today_week, WEEK(writedate) AS weeks, DATE_FORMAT(writedate, '%Y-%m-%d') AS ymd,   ".
							"	(SELECT createdate FROM tbl_user WHERE useridx = tt.useridx) AS user_createdate, ".
							"	DATEDIFF(tt.writedate, (SELECT createdate FROM tbl_user WHERE useridx = tt.useridx)) AS dayafterinstall,	".
							"	IFNULL((	".
							"		SELECT SUM(facebookcredit/10)FROM tbl_product_order WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate < tt.writedate	".
							"	), 0) AS web_accrue_money,	".
							"	(SELECT COUNT(*) FROM tbl_product_order	WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate < tt.writedate) AS web_accrue_buycnt,	".
							"	IFNULL((	".
							"		SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate < tt.writedate	".
							"	), 0) AS mobile_accrue_money,	".
							"	(SELECT COUNT(*) FROM tbl_product_order_mobile	WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate < tt.writedate) AS mobile_accrue_buycnt,	".
							"	IFNULL((	".
							"		SELECT SUM(facebookcredit/10) FROM tbl_product_order WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate <= tt.writedate	".
							"	), 0) AS web_total_money,	".
							"	IFNULL((	".
							"		SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate <= tt.writedate	".
							"	), 0) AS mobile_total_money	".
							"FROM	".
							"(	".
							"	$from_statement	".
							") tt";
					write_log($sql);
					$order_list = $db_main_slave->gettotallist($sql);
					
					$insert_cnt = 0;
					$insert_sql = "";
					 
					for($i=0; $i<sizeof($order_list); $i++)
					{
						$orderidx = $order_list[$i]["orderidx"];
						$platform = $order_list[$i]["platform"];
						$useridx = $order_list[$i]["useridx"];
						$money = $order_list[$i]["money"];
						$basecoin = $order_list[$i]["basecoin"];
						$coin = $order_list[$i]["coin"];
						$special_more = $order_list[$i]["special_more"];
						$special_discount = $order_list[$i]["special_discount"];
						$usercoin = $order_list[$i]["usercoin"];
						$vip_point = $order_list[$i]["vip_point"];
						$product_type = $order_list[$i]["product_type"];
						$coupon_type = $order_list[$i]["coupon_type"];
						$status = $order_list[$i]["status"];
						$canceldate = $order_list[$i]["canceldate"];
						$writedate = $order_list[$i]["writedate"];
						$is_sale = $order_list[$i]["is_sale"];
						$dayweek = $order_list[$i]["today_week"];
						$weeks = $order_list[$i]["weeks"];
						$ymd = $order_list[$i]["ymd"];
						$dayafterinstall = $order_list[$i]["dayafterinstall"];
					
						$web_accrue_money = $order_list[$i]["web_accrue_money"];
						$web_accrue_buycnt = $order_list[$i]["web_accrue_buycnt"];
					
						$mobile_accrue_money = $order_list[$i]["mobile_accrue_money"];
						$mobile_accrue_buycnt = $order_list[$i]["mobile_accrue_buycnt"];
					
						$web_total_money = $order_list[$i]["web_total_money"];
						$mobile_total_money = $order_list[$i]["mobile_total_money"];
						 
						 
						$accrue_money = $web_accrue_money + $mobile_accrue_money;
						$accrue_buycnt = $web_accrue_buycnt + $mobile_accrue_buycnt;
					
						$total_money = $web_total_money + $mobile_total_money;
					
						if($total_money >= 9999)
							$vip_level = 9;
						else if($total_money >= 7999)
							$vip_level = 8;
						else if($total_money >= 5999)
							$vip_level = 7;
						else if($total_money >= 2999)
							$vip_level = 6;
						else if($total_money >= 1999)
							$vip_level = 5;
						else if($total_money >= 999)
							$vip_level = 4;
						else if($total_money >= 499)
							$vip_level = 3;
						else if($total_money >= 99)
							$vip_level = 2;
						else if($total_money >= 39)
							$vip_level = 1;
						else
							$vip_level = 0;
					
						$dayafterpurchase = "0000-00-00 00:00:00";
					
						// 이전 직전 결제와 현재 결제 diff
						if($writedate != "")
						{
							$sql = "SELECT DATEDIFF('$writedate', writedate) ".
									"FROM	".
									"(	".
									"	SELECT writedate	".
									"	FROM tbl_product_order t1	".
									"	WHERE useridx = $useridx AND STATUS IN (1, 3) AND writedate < '$writedate'	".
									"	UNION ALL	".
									"	SELECT writedate	".
									"	FROM tbl_product_order_mobile t2	".
									"	WHERE useridx = $useridx AND STATUS IN (1, 3) AND writedate < '$writedate'	".
									") tt ORDER BY writedate DESC LIMIT 1";
							$dayafterpurchase = $db_main_slave->getvalue($sql);
						}
							
						if($dayafterpurchase == "")
							$dayafterpurchase = 0;
							
						$recent_4w_money = 0;
						$recent_4w_buycnt = 0;
							
						//현재 결제 기준을 00:00:00로 변경 후 최근 4week(28day) 기준
						if($ymd != "")
						{
							$sql = "SELECT IFNULL(SUM(money),0) AS recent_4week_money, COUNT(*) AS recent_4week_cnt ".
									"FROM	".
									"(	".
									"	SELECT (facebookcredit/10) AS money, writedate	".
									"	FROM tbl_product_order t1	".
									"	WHERE useridx = $useridx AND STATUS IN (1, 3) AND writedate >= DATE_SUB('$ymd 00:00:00', INTERVAL 4 WEEK) AND writedate < '$ymd 00:00:00'	".
									"	UNION ALL	".
									"	SELECT money, writedate	".
									"	FROM tbl_product_order_mobile t2	".
									"	WHERE useridx = $useridx AND STATUS IN (1, 3) AND writedate >= DATE_SUB('$ymd 00:00:00', INTERVAL 4 WEEK) AND writedate < '$ymd 00:00:00'	".
									") tt";
							$recent_4week_info = $db_main_slave->getarray($sql);
							 
							$recent_4w_money = $recent_4week_info["recent_4week_money"];
							$recent_4w_buycnt = $recent_4week_info["recent_4week_cnt"];
						}
					
						if($insert_cnt == 0)
						{
							$insert_sql = "INSERT INTO tbl_product_order_detail(orderidx, platform, useridx, money, basecoin, coin, special_more, special_discount, usercoin, vip_level, vip_point, product_type, coupon_type, status, canceldate, writedate, is_sale, weeks, dayweek, dayafterpurchase, dayafterinstall, recent_4w_money, recent_4w_buycnt, accrue_money, accrue_buycnt) VALUES ".
									" ('$orderidx', '$platform', '$useridx', '$money', '$basecoin', '$coin', '$special_more', '$special_discount', '$usercoin', '$vip_level', '$vip_point', '$product_type', '$coupon_type', '$status', '$canceldate', '$writedate', '$is_sale', '$weeks', '$dayweek', '$dayafterpurchase', '$dayafterinstall', '$recent_4w_money', '$recent_4w_buycnt', '$accrue_money', '$accrue_buycnt') ";
							$insert_cnt++;
						}
						else if($insert_cnt <= 100)
						{
							$insert_sql .= ", ('$orderidx', '$platform', '$useridx', '$money', '$basecoin', '$coin', '$special_more', '$special_discount', '$usercoin', '$vip_level', '$vip_point', '$product_type', '$coupon_type', '$status', '$canceldate', '$writedate', '$is_sale', '$weeks', '$dayweek', '$dayafterpurchase', '$dayafterinstall', '$recent_4w_money', '$recent_4w_buycnt', '$accrue_money', '$accrue_buycnt') ";
					
							$insert_cnt++;
						}
						else
						{
							$insert_sql .= " ON DUPLICATE KEY UPDATE useridx=VALUES(useridx), money=VALUES(money), basecoin=VALUES(basecoin), coin=VALUES(coin), special_more=VALUES(special_more), special_discount=VALUES(special_discount), usercoin=VALUES(usercoin), vip_level=VALUES(vip_level), vip_point=VALUES(vip_point), product_type=VALUES(product_type), coupon_type=VALUES(coupon_type), status=VALUES(status), canceldate=VALUES(canceldate), writedate=VALUES(writedate), ".
									" is_sale=VALUES(is_sale), weeks=VALUES(weeks), dayweek=VALUES(dayweek), dayafterpurchase=VALUES(dayafterpurchase), dayafterinstall=VALUES(dayafterinstall), recent_4w_money=VALUES(recent_4w_money), recent_4w_buycnt=VALUES(recent_4w_buycnt), accrue_money=VALUES(accrue_money), accrue_buycnt=VALUES(accrue_buycnt);";
							$db_other->execute($insert_sql);
					
							$insert_sql = "";
							$insert_cnt = 0;;
						}
					}
					
					if($insert_sql != "")
					{
						$insert_sql .= " ON DUPLICATE KEY UPDATE useridx=VALUES(useridx), money=VALUES(money), basecoin=VALUES(basecoin), coin=VALUES(coin), special_more=VALUES(special_more), special_discount=VALUES(special_discount), usercoin=VALUES(usercoin), vip_level=VALUES(vip_level), vip_point=VALUES(vip_point), product_type=VALUES(product_type), coupon_type=VALUES(coupon_type), status=VALUES(status), canceldate=VALUES(canceldate), writedate=VALUES(writedate), ".
								" is_sale=VALUES(is_sale), weeks=VALUES(weeks), dayweek=VALUES(dayweek), dayafterpurchase=VALUES(dayafterpurchase), dayafterinstall=VALUES(dayafterinstall), recent_4w_money=VALUES(recent_4w_money), recent_4w_buycnt=VALUES(recent_4w_buycnt), accrue_money=VALUES(accrue_money), accrue_buycnt=VALUES(accrue_buycnt);";
					
						$db_other->execute($insert_sql);
					}
				}
			}
		}		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}	

	$db_main_slave->end();
	$db_other->end();
?>