<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
		
	$sdate = "2017-09-24";
	//$edate = date("Y-m-d");
	$edate = "2018-01-15";
	
	$db_redshift = new CDatabase_Redshift();
	$db_other = new CDatabase_Other();
	
	$db_other->execute("SET wait_timeout=3600");
	
	
	/* tbl_user_playstat_daily - web*/
	try
	{
		while($sdate <= $edate)
		{
			$temp_date = date('Y-m-d', strtotime($sdate));
			
			$sql = "select * from t5_user_playstat_daily_amazon where today = '$temp_date' ORDER BY statidx ASC";
			write_log($sql);
			$playstat_daily_data = $db_redshift->gettotallist($sql);
			
			$insert_cnt = 0;
			$insert_sql = "";
			
			write_log(sizeof($playstat_daily_data));
			
			for($i = 0; $i < sizeof($playstat_daily_data); $i++)
			{
				$today = $playstat_daily_data[$i]["today"];
				$useridx = $playstat_daily_data[$i]["useridx"];
				$moneyin = $playstat_daily_data[$i]["moneyin"];
				$moneyout = $playstat_daily_data[$i]["moneyout"];
				$winrate = $playstat_daily_data[$i]["winrate"];
				$bigwin = $playstat_daily_data[$i]["bigwin"];
				$playtime = $playstat_daily_data[$i]["playtime"];
				$playcount = $playstat_daily_data[$i]["playcount"];
				$h_moneyin = $playstat_daily_data[$i]["h_moneyin"];
				$h_moneyout = $playstat_daily_data[$i]["h_moneyout"];
				$h_winrate = $playstat_daily_data[$i]["h_winrate"];
				$h_bigwin = $playstat_daily_data[$i]["h_bigwin"];
				$h_playtime = $playstat_daily_data[$i]["h_playtime"];
				$h_playcount = $playstat_daily_data[$i]["h_playcount"];
				$treatamount = $playstat_daily_data[$i]["treatamount"];
				$redeem = $playstat_daily_data[$i]["redeem"];
				$currentcoin = $playstat_daily_data[$i]["currentcoin"];
				$purchasecount = $playstat_daily_data[$i]["purchasecount"];
				$purchaseamount = $playstat_daily_data[$i]["purchaseamount"];
				$purchasecoin = $playstat_daily_data[$i]["purchasecoin"];
				$days_after_purchase = $playstat_daily_data[$i]["days_after_purchase"];
				$freecoin = $playstat_daily_data[$i]["freecoin"];
				$jackpot = $playstat_daily_data[$i]["jackpot"];
				$vip_level = $playstat_daily_data[$i]["vip_level"];
				$vip_point = $playstat_daily_data[$i]["vip_point"];
				$famelevel = $playstat_daily_data[$i]["famelevel"];
				$exp = $playstat_daily_data[$i]["exp"];
				$ty_point = $playstat_daily_data[$i]["ty_point"];
				$friend_count = $playstat_daily_data[$i]["friend_count"];
				$crowntype = $playstat_daily_data[$i]["crowntype"];
				$days_after_install = $playstat_daily_data[$i]["days_after_install"];
				
				// Insert
				if($insert_cnt == 0)
				{
					$insert_sql = "INSERT INTO _tbl_user_playstat_daily_amazon_20180116(today, useridx, moneyin, moneyout, winrate, bigwin, playtime, playcount, h_moneyin, h_moneyout, h_winrate, h_bigwin, h_playtime, h_playcount, treatamount, redeem, currentcoin, purchasecount, purchaseamount, purchasecoin, days_after_purchase, freecoin, jackpot, vip_level, vip_point, famelevel, exp, ty_point, friend_count, crowntype, days_after_install) ".
									"VALUES('$today', $useridx, $moneyin, $moneyout, $winrate, $bigwin, $playtime, $playcount, $h_moneyin, $h_moneyout, $h_winrate, $h_bigwin, $h_playtime, $h_playcount, $treatamount, $redeem, $currentcoin, $purchasecount, $purchaseamount, $purchasecoin, $days_after_purchase, $freecoin, $jackpot, $vip_level, $vip_point, $famelevel, $exp, $ty_point, $friend_count, $crowntype, $days_after_install)";
				
					$insert_cnt++;
				}
				else if($insert_cnt < 2000)
				{
					$insert_sql .= ",('$today', $useridx, $moneyin, $moneyout, $winrate, $bigwin, $playtime, $playcount, $h_moneyin, $h_moneyout, $h_winrate, $h_bigwin, $h_playtime, $h_playcount, $treatamount, $redeem, $currentcoin, $purchasecount, $purchaseamount, $purchasecoin, $days_after_purchase, $freecoin, $jackpot, $vip_level, $vip_point, $famelevel, $exp, $ty_point, $friend_count, $crowntype, $days_after_install)";
				
					$insert_cnt++;
				}
				else
				{
					$insert_sql .= ",('$today', $useridx, $moneyin, $moneyout, $winrate, $bigwin, $playtime, $playcount, $h_moneyin, $h_moneyout, $h_winrate, $h_bigwin, $h_playtime, $h_playcount, $treatamount, $redeem, $currentcoin, $purchasecount, $purchaseamount, $purchasecoin, $days_after_purchase, $freecoin, $jackpot, $vip_level, $vip_point, $famelevel, $exp, $ty_point, $friend_count, $crowntype, $days_after_install)";				
				
					$db_other->execute($insert_sql);
				
					$insert_cnt = 0;
					$insert_sql = "";
				}
				
			}
			
			if($insert_sql != "")
				$db_other->execute($insert_sql);

			sleep(1);
			
			write_log("playstat_daily_restore : $temp_date");
			
			$sdate = date('Y-m-d', strtotime($temp_date.' + 1 day'));
		}		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_redshift->end();
	$db_other->end();
?>