<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");	
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/test/standard_first_payment_log") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/test/standard_first_payment_log") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("test/standard_first_payment_log Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	
	//Web
	try 
	{
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(facebookcredit::float/10::float) AS pay, count(distinct t2.useridx) AS payer_cnt	".
				"from	".
				"(	".
  				"	(	".
    			"		select * from  t5_user where platform = 0 and useridx > 20000	".
  				"	) t1 join t5_product_order t2 on t1.useridx = t2.useridx	".
 				") where status = 1 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate) ORDER BY create_date ASC,  purchase_date ASC;";
		$web_web_list = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i < sizeof($web_web_list); $i++)
		{
			$create_date = $web_web_list[$i]["create_date"];
			$purchase_date = $web_web_list[$i]["purchase_date"];
			$facebookcredit = $web_web_list[$i]["pay"];
			$payer_cnt = $web_web_list[$i]["payer_cnt"];
			
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
							"(0,0,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}
		
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(facebookcredit::float/10::float) AS pay, count(distinct t2.useridx) AS payer_cnt ".
				"from	".
				"(	".
  				"	(	".
    			"		select useridx, min(writedate) as createdate	". 
    			"		from t5_product_order t1	". 
    			"		where status = 1 and useridx > 20000 and exists (select useridx from t5_user where platform = 1 and useridx > 20000 and useridx = t1.useridx) group by useridx	".
  				"	) t1 join t5_product_order t2 on t1.useridx = t2.useridx	".
 				") where status = 1 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate)	". 
 				"ORDER BY create_date ASC,  purchase_date ASC;";
		$web_ios_list = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i < sizeof($web_ios_list); $i++)
		{
			$create_date = $web_ios_list[$i]["create_date"];
			$purchase_date = $web_ios_list[$i]["purchase_date"];
			$facebookcredit = $web_ios_list[$i]["pay"];
			$payer_cnt = $web_ios_list[$i]["payer_cnt"];
									
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
							"(0,1,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}
		
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(facebookcredit::float/10::float) AS pay, count(distinct t2.useridx) AS payer_cnt ".
				"from	".
				"(	".
				"	(	".
				"		select useridx, min(writedate) as createdate	".
				"		from t5_product_order t1	".
				"		where status = 1 and useridx > 20000 and exists (select useridx from t5_user where platform = 2 and useridx > 20000 and useridx = t1.useridx) group by useridx	".
				"	) t1 join t5_product_order t2 on t1.useridx = t2.useridx	".
				") where status = 1 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate)	".
				"ORDER BY create_date ASC,  purchase_date ASC;";
		$web_android_list = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i < sizeof($web_android_list); $i++)
		{
			$create_date = $web_android_list[$i]["create_date"];
			$purchase_date = $web_android_list[$i]["purchase_date"];
			$facebookcredit = $web_android_list[$i]["pay"];
			$payer_cnt = $web_android_list[$i]["payer_cnt"];
									
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
							"(0,2,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}
		
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(facebookcredit::float/10::float) AS pay, count(distinct t2.useridx) AS payer_cnt ".
				"from	".
				"(	".
				"	(	".
				"		select useridx, min(writedate) as createdate	".
				"		from t5_product_order t1	".
				"		where status = 1 and useridx > 20000 and exists (select useridx from t5_user where platform = 3 and useridx > 20000 and useridx = t1.useridx) group by useridx	".
				"	) t1 join t5_product_order t2 on t1.useridx = t2.useridx	".
				") where status = 1 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate)	".
				"ORDER BY create_date ASC,  purchase_date ASC;";
		$web_amazon_list = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i < sizeof($web_amazon_list); $i++)
		{
			$create_date = $web_amazon_list[$i]["create_date"];
			$purchase_date = $web_amazon_list[$i]["purchase_date"];
			$facebookcredit = $web_amazon_list[$i]["pay"];
			$payer_cnt = $web_amazon_list[$i]["payer_cnt"];
									
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
							"(0,3,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	//Ios
	try
	{
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(money::float) AS pay, count(distinct t2.useridx) AS payer_cnt	".
				"from	".
				"(	".
				"	(	".
				"		select * from  t5_user where platform = 1 and useridx > 20000	".
				"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx	".
				") where status = 1 and os_type = 1 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate) ORDER BY create_date ASC,  purchase_date ASC;";
		$ios_ios_list = $db_redshift->gettotallist($sql);
	
		for($i = 0; $i < sizeof($ios_ios_list); $i++)
		{
			$create_date = $ios_ios_list[$i]["create_date"];
			$purchase_date = $ios_ios_list[$i]["purchase_date"];
			$facebookcredit = $ios_ios_list[$i]["pay"];
			$payer_cnt = $ios_ios_list[$i]["payer_cnt"];
							
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
							"(1,1,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}
		
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(money::float) AS pay, count(distinct t2.useridx) AS payer_cnt ".
				"from	".
				"(	".
				"	(	".
				"		select useridx, min(writedate) as createdate	".
				"		from t5_product_order_mobile t1	".
				"		where status = 1 and useridx > 20000 and os_type = 1 and exists (select useridx from t5_user where platform = 0 and useridx > 20000 and useridx = t1.useridx) group by useridx	".
				"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx	".
				") where status = 1 and os_type = 1 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate)	".
				"ORDER BY create_date ASC,  purchase_date ASC;";
		$ios_web_list = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i < sizeof($ios_web_list); $i++)
		{
			$create_date = $ios_web_list[$i]["create_date"];
			$purchase_date = $ios_web_list[$i]["purchase_date"];
			$facebookcredit = $ios_web_list[$i]["pay"];
			$payer_cnt = $ios_web_list[$i]["payer_cnt"];
									
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
							"(1,0,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}
		
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(money::float) AS pay, count(distinct t2.useridx) AS payer_cnt ".
				"from	".
				"(	".
				"	(	".
				"		select useridx, min(writedate) as createdate	".
				"		from t5_product_order_mobile t1	".
				"		where status = 1 and useridx > 20000 and os_type = 1 and exists (select useridx from t5_user where platform = 2 and useridx > 20000 and useridx = t1.useridx) group by useridx	".
				"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx	".
				") where status = 1 and os_type = 1 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate)	".
				"ORDER BY create_date ASC,  purchase_date ASC;";
		$ios_android_list = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i < sizeof($ios_android_list); $i++)
		{
			$create_date = $ios_android_list[$i]["create_date"];
			$purchase_date = $ios_android_list[$i]["purchase_date"];
			$facebookcredit = $ios_android_list[$i]["pay"];
			$payer_cnt = $ios_android_list[$i]["payer_cnt"];
									
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
							"(1,2,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}
		
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(money::float) AS pay, count(distinct t2.useridx) AS payer_cnt ".
				"from	".
				"(	".
				"	(	".
				"		select useridx, min(writedate) as createdate	".
				"		from t5_product_order_mobile t1	".
				"		where status = 1 and useridx > 20000 and os_type = 1 and exists (select useridx from t5_user where platform = 3 and useridx > 20000 and useridx = t1.useridx) group by useridx	".
				"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx	".
				") where status = 1 and os_type = 1 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate)	".
				"ORDER BY create_date ASC,  purchase_date ASC;";
		$ios_amazon_list = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i < sizeof($ios_amazon_list); $i++)
		{
			$create_date = $ios_amazon_list[$i]["create_date"];
			$purchase_date = $ios_amazon_list[$i]["purchase_date"];
			$facebookcredit = $ios_amazon_list[$i]["pay"];
			$payer_cnt = $ios_amazon_list[$i]["payer_cnt"];
									
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
							"(1,3,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	//android
	try
	{
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(money::float) AS pay, count(distinct t2.useridx) AS payer_cnt	".
				"from	".
				"(	".
				"	(	".
				"		select * from  t5_user where platform = 2 and useridx > 20000	".
				"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx	".
				") where status = 1 and os_type = 2 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate) ORDER BY create_date ASC,  purchase_date ASC;";
		$android_android_list = $db_redshift->gettotallist($sql);
	
		for($i = 0; $i < sizeof($android_android_list); $i++)
		{
			$create_date = $android_android_list[$i]["create_date"];
			$purchase_date = $android_android_list[$i]["purchase_date"];
			$facebookcredit = $android_android_list[$i]["pay"];
			$payer_cnt = $android_android_list[$i]["payer_cnt"];
							
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
							"(2,2,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}
	
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(money::float) AS pay, count(distinct t2.useridx) AS payer_cnt ".
				"from	".
				"(	".
				"	(	".
				"		select useridx, min(writedate) as createdate	".
				"		from t5_product_order_mobile t1	".
				"		where status = 1 and useridx > 20000 and os_type = 2 and exists (select useridx from t5_user where platform = 0 and useridx > 20000 and useridx = t1.useridx) group by useridx	".
				"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx	".
				") where status = 1 and os_type = 2 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate)	".
				"ORDER BY create_date ASC,  purchase_date ASC;";
		$android_web_list = $db_redshift->gettotallist($sql);
	
		for($i = 0; $i < sizeof($android_web_list); $i++)
		{
			$create_date = $android_web_list[$i]["create_date"];
			$purchase_date = $android_web_list[$i]["purchase_date"];
			$facebookcredit = $android_web_list[$i]["pay"];
			$payer_cnt = $android_web_list[$i]["payer_cnt"];
			
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
							"(2,0,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}
	
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(money::float) AS pay, count(distinct t2.useridx) AS payer_cnt ".
				"from	".
				"(	".
				"	(	".
				"		select useridx, min(writedate) as createdate	".
				"		from t5_product_order_mobile t1	".
				"		where status = 1 and useridx > 20000 and os_type = 2 and exists (select useridx from t5_user where platform = 1 and useridx > 20000 and useridx = t1.useridx) group by useridx	".
				"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx	".
				") where status = 1 and os_type = 2 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate)	".
				"ORDER BY create_date ASC,  purchase_date ASC;";
		$android_ios_list = $db_redshift->gettotallist($sql);
	
		for($i = 0; $i < sizeof($android_ios_list); $i++)
		{
			$create_date = $android_ios_list[$i]["create_date"];
			$purchase_date = $android_ios_list[$i]["purchase_date"];
			$facebookcredit = $android_ios_list[$i]["pay"];
			$payer_cnt = $android_ios_list[$i]["payer_cnt"];
					
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
						"(2,1,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}
	
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(money::float) AS pay, count(distinct t2.useridx) AS payer_cnt ".
				"from	".
				"(	".
				"	(	".
				"		select useridx, min(writedate) as createdate	".
				"		from t5_product_order_mobile t1	".
				"		where status = 1 and useridx > 20000 and os_type = 2 and exists (select useridx from t5_user where platform = 3 and useridx > 20000 and useridx = t1.useridx) group by useridx	".
				"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx	".
				") where status = 1 and os_type = 2 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate)	".
				"ORDER BY create_date ASC,  purchase_date ASC;";
		$android_amazon_list = $db_redshift->gettotallist($sql);
	
		for($i = 0; $i < sizeof($ios_android_list); $i++)
		{
			$create_date = $android_amazon_list[$i]["create_date"];
			$purchase_date = $android_amazon_list[$i]["purchase_date"];
			$facebookcredit = $android_amazon_list[$i]["pay"];
			$payer_cnt = $android_amazon_list[$i]["payer_cnt"];
					
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
							"(2,3,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	//amazon
	try
	{
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(money::float) AS pay, count(distinct t2.useridx) AS payer_cnt	".
				"from	".
				"(	".
				"	(	".
				"		select * from  t5_user where platform = 3 and useridx > 20000	".
				"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx	".
				") where status = 1 and os_type = 3 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate) ORDER BY create_date ASC,  purchase_date ASC;";
		$amazon_amazon_list = $db_redshift->gettotallist($sql);
	
		for($i = 0; $i < sizeof($amazon_amazon_list); $i++)
		{
			$create_date = $amazon_amazon_list[$i]["create_date"];
			$purchase_date = $amazon_amazon_list[$i]["purchase_date"];
			$facebookcredit = $amazon_amazon_list[$i]["pay"];
			$payer_cnt = $amazon_amazon_list[$i]["payer_cnt"];
							
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
							"(3,3,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}
	
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(money::float) AS pay, count(distinct t2.useridx) AS payer_cnt ".
				"from	".
				"(	".
				"	(	".
				"		select useridx, min(writedate) as createdate	".
				"		from t5_product_order_mobile t1	".
				"		where status = 1 and useridx > 20000 and os_type = 3 and exists (select useridx from t5_user where platform = 0 and useridx > 20000 and useridx = t1.useridx) group by useridx	".
				"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx	".
				") where status = 1 and os_type = 3 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate)	".
				"ORDER BY create_date ASC,  purchase_date ASC;";
		$amazon_web_list = $db_redshift->gettotallist($sql);
	
		for($i = 0; $i < sizeof($amazon_web_list); $i++)
		{
			$create_date = $amazon_web_list[$i]["create_date"];
			$purchase_date = $amazon_web_list[$i]["purchase_date"];
			$facebookcredit = $amazon_web_list[$i]["pay"];
			$payer_cnt = $amazon_web_list[$i]["payer_cnt"];
						
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
							"(3,0,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}
	
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(money::float) AS pay, count(distinct t2.useridx) AS payer_cnt ".
				"from	".
				"(	".
				"	(	".
				"		select useridx, min(writedate) as createdate	".
				"		from t5_product_order_mobile t1	".
				"		where status = 1 and useridx > 20000 and os_type = 3 and exists (select useridx from t5_user where platform = 1 and useridx > 20000 and useridx = t1.useridx) group by useridx	".
				"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx	".
				") where status = 1 and os_type = 3 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate)	".
				"ORDER BY create_date ASC,  purchase_date ASC;";
		$amazon_ios_list = $db_redshift->gettotallist($sql);
	
		for($i = 0; $i < sizeof($amazon_ios_list); $i++)
		{
			$create_date = $amazon_ios_list[$i]["create_date"];
			$purchase_date = $amazon_ios_list[$i]["purchase_date"];
			$facebookcredit = $amazon_ios_list[$i]["pay"];
			$payer_cnt = $amazon_ios_list[$i]["payer_cnt"];
						
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
						"(3,1,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}
	
		$sql = "select date_trunc('month', createdate) AS create_date, date_trunc('month', writedate) AS purchase_date, sum(money::float) AS pay, count(distinct t2.useridx) AS payer_cnt ".
				"from	".
				"(	".
				"	(	".
				"		select useridx, min(writedate) as createdate	".
				"		from t5_product_order_mobile t1	".
				"		where status = 1 and useridx > 20000 and os_type = 3 and exists (select useridx from t5_user where platform = 2 and useridx > 20000 and useridx = t1.useridx) group by useridx	".
				"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx	".
				") where status = 1 and os_type = 3 GROUP BY date_trunc('month', createdate), date_trunc('month', writedate)	".
				"ORDER BY create_date ASC,  purchase_date ASC;";
		$amazon_android_list = $db_redshift->gettotallist($sql);
	
		for($i = 0; $i < sizeof($amazon_android_list); $i++)
		{
			$create_date = $amazon_android_list[$i]["create_date"];
			$purchase_date = $amazon_android_list[$i]["purchase_date"];
			$facebookcredit = $amazon_android_list[$i]["pay"];
			$payer_cnt = $amazon_android_list[$i]["payer_cnt"];
						
			$insert_sql = "INSERT INTO tbl_user_month_first_pay_statics(pay_platform, join_platform, first_month, purchase_month, total_credit, payer_cnt) VALUES ".
							"(3,2,'$create_date','$purchase_date','$facebookcredit','$payer_cnt') ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), payer_cnt=VALUES(payer_cnt);";
			$db_other->execute($insert_sql);
		}
	}
	catch(Exception $e)
	{
			write_log($e->getMessage());
		}
	
	$db_other->end();
	$db_redshift->end();
?>