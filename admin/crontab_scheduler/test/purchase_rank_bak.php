<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	$db_redshift = new CDatabase_Redshift();
	$db_other = new CDatabase_Other();
	
	$db_other->execute("SET wait_timeout=72000");
	
	$having_tail = "";
	$k=1;
	
	while ($today != "2015-11-15")
	{
		$today = date("Y-m-d", strtotime("-$k days"));
		write_log($today);
		for($i=0; $i<5; $i++)
		{
			if($i == 0)
				$having_tail = "having  SUM(money) < 9 ";
			else if($i == 1)
				$having_tail = "having  SUM(money) >= 9 and SUM(money) < 59 ";
			else if($i == 2)
				$having_tail = "having  SUM(money) >= 59 and SUM(money) < 199 ";
			else if($i == 3)
				$having_tail = "having  SUM(money) >= 199 and SUM(money) < 499 ";
			else if($i == 4)
				$having_tail = "having  SUM(money) >= 499 ";
									
			$sql = "select useridx, SUM(money) as money ".
					"from ( ".
					"	select useridx, (facebookcredit::float/10::float) as money ".
					"	from t5_product_order ".
					"	where useridx > 20000 and status = 1 and 0 <= datediff(day, writedate, '$today 00:00:00') and datediff(day, writedate, '$today 00:00:00') <= 28 ".
					"	union all ".
					"	select useridx, money ".
					"	from t5_product_order_mobile ".
					"	where useridx > 20000 and status = 1 and 0 <= datediff(day, writedate, '$today 00:00:00') and datediff(day, writedate, '$today 00:00:00') <= 28 ".
					") t1 ".
					"group by useridx ".
					"$having_tail";
	
			$data_list = $db_redshift->gettotallist($sql);
	
			$userlist = "";
	
			$usercount = sizeof($data_list);
	
			for($j=0; $j<sizeof($data_list); $j++)
			{
				$useridx = $data_list[$j]["useridx"];
	
				if($userlist == "")
					$userlist = $useridx;
				else
					$userlist .= ",".$useridx;
			}
	
			if($usercount > 0)
			{
				// Platform
				$sql = "select os_type, SUM(money) as money ".
					"from ( ".
					"	select useridx, os_type, SUM(money) as money ".
					"	from ( ".
					"		select useridx, 0 AS os_type, (facebookcredit::float/10::float) as money ".
					"		from t5_product_order ".
					"		where useridx > 20000 and status = 1 and 0 <= datediff(day, writedate, '$today 00:00:00') and datediff(day, writedate, '$today 00:00:00') <= 28 and useridx in ($userlist) ".
					"		union all ".
					"		select useridx, os_type, money ".
					"		from t5_product_order_mobile ".
					"		where useridx > 20000 and status = 1 and 0 <= datediff(day, writedate, '$today 00:00:00') and datediff(day, writedate, '$today 00:00:00') <= 28 and useridx in ($userlist)".
					"	) t1 ".
					"	group by useridx, os_type ".
					") t2 ".
					"group by os_type order by os_type asc ";
				
				$purchase_list = $db_redshift->gettotallist($sql);
	
				$money = $purchase_list[0]["money"];
				$ios_money = $purchase_list[1]["money"];
				$and_money = $purchase_list[2]["money"];
				$ama_money = $purchase_list[3]["money"];
				
				if($money == "") 
					$money = 0;
				
				if($ios_money == "") 
					$ios_money = 0;
				
				if($and_money == "") 
					$and_money = 0;	
				
				if($ama_money == "") 
					$ama_money = 0;
			}
			else
			{
				$usercount = 0;
				$money = 0;
				$ios_money = 0;
				$and_money = 0;
				$ama_money = 0;
			}
	
	
	
			$insert_sql = " INSERT INTO tbl_buyer_grade_stat VALUES('$today', $i, $usercount, $money, $ios_money, $and_money, $ama_money) ". 
						  " ON DUPLICATE KEY UPDATE usercount=VALUES(usercount), web_paymoney=VALUES(web_paymoney), ios_paymoney=VALUES(ios_paymoney), and_paymoney=VALUES(and_paymoney), ama_paymoney=VALUES(ama_paymoney)";
			$db_other->execute($insert_sql);
		}
		$k = $k+1;
	}
	
	$db_other->end();
	$db_redshift->end();
?>
