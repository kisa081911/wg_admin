<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$db_other = new CDatabase_Other();
	$db_main2 = new CDatabase_Main2();
	$db_redshift = new CDatabase_Redshift();
	$db_analysis = new CDatabase_Analysis();
	
	//그룹별 인원 수
	$sql = "SELECT DATE_FORMAT(now(), '%Y-%m-%d') AS today, groupidx, productidx, nopaycnt, offerbuyusercount, offerbuycount, (nopaycnt+offerbuyusercount) AS usercount
			FROM 
			(
				SELECT groupidx, productidx, COUNT(DISTINCT useridx) AS nopaycnt
				      ,(SELECT COUNT(DISTINCT useridx) FROM `tbl_nopayer_product_order` WHERE DATE_FORMAT(writedate, '%Y-%m-%d') = DATE_FORMAT(now(), '%Y-%m-%d') AND groupidx = t1.groupidx AND productidx=t1.productidx)  AS offerbuyusercount
				      ,(SELECT COUNT(useridx) FROM `tbl_nopayer_product_order` WHERE DATE_FORMAT(writedate, '%Y-%m-%d') = DATE_FORMAT(now(), '%Y-%m-%d') AND groupidx = t1.groupidx AND productidx=t1.productidx)  AS offerbuycount
				FROM
				(	SELECT groupidx, productidx,useridx
					FROM `tbl_nopayer_product` WHERE  DATE_FORMAT(startdate, '%Y-%m-%d') = DATE_FORMAT(now(), '%Y-%m-%d')
					UNION ALL
					SELECT groupidx, productidx,useridx
					FROM `tbl_nopayer_product_mobile` WHERE DATE_FORMAT(startdate, '%Y-%m-%d') = DATE_FORMAT(now(), '%Y-%m-%d')
				)t1 GROUP BY groupidx, productidx
			)t2";
	
	$group_usercount_list = $db_main2 -> gettotallist($sql);
	
	$insert_sql = "INSERT INTO nopayer_stat_daily (today,groupidx,productidx,offerbuycount,offerbuyusercount,usercount)";
	
	for($i=0;$i<sizeof($group_usercount_list);$i++)
	{
		$today = $group_usercount_list[$i]["today"];
		$groupidx = $group_usercount_list[$i]["groupidx"];
		$productidx = $group_usercount_list[$i]["productidx"];
		$offerbuycount = $group_usercount_list[$i]["offerbuycount"];
		$offerbuyusercount = $group_usercount_list[$i]["offerbuyusercount"];
		$usercount = $group_usercount_list[$i]["usercount"];
		
		if($i == 0)
			$insert_sql .= "VALUES('$today','$groupidx','$productidx','$offerbuycount','$offerbuyusercount','$usercount')";
		else 
			$insert_sql .= ",('$today','$groupidx','$productidx','$offerbuycount','$offerbuyusercount','$usercount')";
	}		
	
	$insert_sql .=  "ON DUPLICATE KEY UPDATE usercount = VALUES(usercount), offerbuycount = VALUES(offerbuycount), offerbuyusercount=VALUES(offerbuyusercount);";
	write_log($insert_sql);
	$db_analysis->execute($insert_sql);
	
	$db_analysis->end();
	$db_other->end();
	$db_redshift->end();
	$db_main2->end();
?>