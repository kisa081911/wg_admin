<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");

	$today = '2017-06-28';
	$yesterday = '2017-06-27';
	$yesterday_str = '20170627';
	
	//Mobile
	try
	{
		$db_main2 = new CDatabase_Main2();
		$db_main2->execute("SET wait_timeout=3600");
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_retention_mobile_log'";
		$columns_count = $db_main2->getvalue($sql);
	
		for($u=0; $u<10; $u++)
		{
			$sql = " SELECT * ".
					"	FROM (SELECT rtidx, platform, useridx, adflag, eventidx, is_payer, level, leavedays, createdate, writedate, site_id FROM tbl_user_retention_mobile_log ".
					"	WHERE '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00' ".
					"	) t1 ".
					"	WHERE t1.useridx % 10 = $u ";
					
			$user_list = $db_main2->gettotallist($sql);
					
			for($i=0; $i<sizeof($user_list); $i++)
			{
				$output = "";
	
				for($j=0; $j<$columns_count; $j++)
				{
					$user_list[$i][$j] = str_replace("\"", "\"\"", $user_list[$i][$j]);
	
					if($user_list[$i][$j] == "0000-00-00 00:00:00")
						$user_list[$i][$j] = "1900-01-01 00:00:00";
	
					if($j == $columns_count - 1)
						$output .= '"'.$user_list[$i][$j].'"';
					else
						$output .= '"'.$user_list[$i][$j].'"|';
				}
				
				$output .="\n";

				if($output != "")
				{
					$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_retention_mobile_log_$yesterday_str.txt", 'a+');
	
					fwrite($fp, $output);
	
					fclose($fp);
				}
			}
		}
	
		$db_main2->end();
	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../common/aws_sdk/aws_config.php');

		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_retention_mobile_log/$yesterday_str";

		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_retention_mobile_log_$yesterday_str.txt";

		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_retention_mobile_log_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));

		$ObjectURL = $result["ObjectURL"];

		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();

		// Create a staging table
		$sql = "CREATE TABLE t5_user_retention_mobile_log_tmp (LIKE t5_user_retention_mobile_log);";
		$db_redshift->execute($sql);

		// Load data into the staging table
		$sql = "copy t5_user_retention_mobile_log_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_retention_mobile_log/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);

		// Update records
		$sql = "UPDATE t5_user_retention_mobile_log ".
				"SET platform = s.platform, adflag = s.adflag, eventidx = s.eventidx, is_payer = s.is_payer, level = s.level, leavedays = s.leavedays, createdate = s.createdate, writedate = s.writedate, site_id = s.site_id ".
				"FROM t5_user_retention_mobile_log_tmp s ".
				"WHERE t5_user_retention_mobile_log.rtidx = s.rtidx;";
		$db_redshift->execute($sql);

		// Insert records
		$sql = "INSERT INTO t5_user_retention_mobile_log ".
				"SELECT s.* FROM t5_user_retention_mobile_log_tmp s LEFT JOIN t5_user_retention_mobile_log ".
				"ON s.rtidx = t5_user_retention_mobile_log.rtidx ".
				"WHERE t5_user_retention_mobile_log.rtidx IS NULL;";
		$db_redshift->execute($sql);

		// Drop the staging table
		$sql = "DROP TABLE t5_user_retention_mobile_log_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();

	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>