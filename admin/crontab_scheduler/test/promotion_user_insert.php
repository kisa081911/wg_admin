<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	$db_main2 = new CDatabase_Main2();
	$db_redshift = new CDatabase_Redshift();
	
	$db_main2->execute("SET wait_timeout=72000");
	
	try
	{
		$sql = "select t1.useridx AS useridx from t5_user t1 join duc_user t2 on t1.fb_token = t2.fb_token where t1.fb_token != '' AND t1.useridx > 20000 ".
				"AND t1.adflag not like 'fbself%' AND t1.adflag not like 'nanigans%' ".
				"AND t1.logindate >= DATEADD(day, -35, CURRENT_DATE)  AND t2.logindate < DATEADD(day, -14, CURRENT_DATE)";
		$insert_user_info =	$db_redshift->gettotallist($sql);
		
		$cnt = 0;
		$insert_info = "";
		
		for($i=0; $i<sizeof($insert_user_info); $i++)
		{
			$useridx = $insert_user_info[$i]["useridx"];
			
			if($insert_info == "")
				$insert_info = "INSERT INTO tbl_cr_topbar_user_log(useridx, isclick, writedate) VALUES ($useridx, 0, NOW())";
			else 
				$insert_info .= ",($useridx, 0, NOW()) ";
			
			$cnt++;
			
			if($cnt == 500)
			{
				$db_main2->execute($insert_info);
				$insert_info = "";
				$cnt = 0;
			}
		}
		
		if($insert_info != "")
		{
			$db_main2->execute($insert_info);
		}

	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
	$db_redshift->end();
?>