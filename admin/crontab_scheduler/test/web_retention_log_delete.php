<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	
	ini_set("memory_limit", "-1");	
	
	try
	{
// 		$today = date("Y-m-d", time() - 60 * 60 * 24 * 1);

			$db_redshift = new CDatabase_Redshift();
			
			//t5_user_retention_log
			$sql = " select useridx, adflag, cnt, today from (   
					    select useridx, adflag, count(adflag) as cnt, to_char(writedate, 'YYYY-MM-DD') as today from t5_user_retention_log  
					    group by useridx, adflag, to_char(writedate, 'YYYY-MM-DD')
					 )
					  where  cnt > 1";
			$date_list= $db_redshift->gettotallist($sql);
			
			$delete_sql = "";

			write_log("t5_user_retention_log - ".sizeof($date_list));
			for ($i = 0; $i < sizeof($date_list); $i++) {
				
				$useridx = $date_list[$i]['useridx'];
				$adflag = $date_list[$i]['adflag'];
				$today = $date_list[$i]['today'];
				
				$delete_sql = "DELETE FROM t5_user_retention_log  ". 
									" WHERE useridx = $useridx ".
									" AND  rtidx != ( ".
									                  " SELECT rtidx ". 
									                  " FROM t5_user_retention_log  ". 
									                  " WHERE useridx = $useridx ".
									                  " AND adflag='$adflag' ".
									                  " AND to_char(writedate, 'YYYY-MM-DD') = '$today' ". 
									                  " ORDER BY writedate ASC limit 1 ".
									                " ) ".
									" AND to_char(writedate, 'YYYY-MM-DD') = '$today';";
				$db_redshift->execute($delete_sql);
			}
			
			//t5_user_retention_mobile_log
			$sql = " select useridx, adflag, cnt, today from (
					    select useridx, adflag, count(adflag) as cnt, to_char(writedate, 'YYYY-MM-DD') as today from t5_user_retention_mobile_log
					    group by useridx, adflag, to_char(writedate, 'YYYY-MM-DD')
					 )
					  where  cnt > 1";
			$date_list= $db_redshift->gettotallist($sql);
				
			$delete_sql = "";
			
			write_log("t5_user_retention_mobile_log - ".sizeof($date_list));
			for ($i = 0; $i < sizeof($date_list); $i++) {
			
				$useridx = $date_list[$i]['useridx'];
				$adflag = $date_list[$i]['adflag'];
				$today = $date_list[$i]['today'];
			
				$delete_sql = "DELETE FROM t5_user_retention_mobile_log  ".
						" WHERE useridx = $useridx ".
						" AND  rtidx != ( ".
						" SELECT rtidx ".
						" FROM t5_user_retention_mobile_log  ".
						" WHERE useridx = $useridx ".
						" AND adflag='$adflag' ".
						" AND to_char(writedate, 'YYYY-MM-DD') = '$today' ".
						" ORDER BY writedate ASC limit 1 ".
						" ) ".
						" AND to_char(writedate, 'YYYY-MM-DD') = '$today';";
				$db_redshift->execute($delete_sql);
			}
			
			//duc_user_retention_mobile_log
			$sql = " select useridx, adflag, cnt, today from (
					    select useridx, adflag, count(adflag) as cnt, to_char(writedate, 'YYYY-MM-DD') as today from duc_user_retention_mobile_log
					    group by useridx, adflag, to_char(writedate, 'YYYY-MM-DD')
					 )
					  where  cnt > 1";
			$date_list= $db_redshift->gettotallist($sql);
				
			$delete_sql = "";
			
			write_log("duc_user_retention_mobile_log - ".sizeof($date_list));
			for ($i = 0; $i < sizeof($date_list); $i++) {
			
				$useridx = $date_list[$i]['useridx'];
				$adflag = $date_list[$i]['adflag'];
				$today = $date_list[$i]['today'];
			
				$delete_sql = "DELETE FROM duc_user_retention_mobile_log  ".
						" WHERE useridx = $useridx ".
						" AND  rtidx != ( ".
						" SELECT rtidx ".
						" FROM duc_user_retention_mobile_log  ".
						" WHERE useridx = $useridx ".
						" AND adflag='$adflag' ".
						" AND to_char(writedate, 'YYYY-MM-DD') = '$today' ".
						" ORDER BY writedate ASC limit 1 ".
						" ) ".
						" AND to_char(writedate, 'YYYY-MM-DD') = '$today';";
				$db_redshift->execute($delete_sql);
			}
			
			//duc_user_retention_mobile_log
			$sql = " select useridx, adflag, cnt, today from (
					    select useridx, adflag, count(adflag) as cnt, to_char(writedate, 'YYYY-MM-DD') as today from duc_user_retention_mobile_log
					    group by useridx, adflag, to_char(writedate, 'YYYY-MM-DD')
					 )
					  where  cnt > 1";
			$date_list= $db_redshift->gettotallist($sql);
				
			
			write_log("duc_user_retention_mobile_log - ".sizeof($date_list));
			$delete_rdidx_sql = "";
			$delete_count=0;
			for ($i = 0; $i < sizeof($date_list); $i++) {
			
				$useridx = $date_list[$i]['useridx'];
				$adflag = $date_list[$i]['adflag'];
				$today = $date_list[$i]['today'];
			
				$delete_sql = "select rtidx ".
								" from duc_user_retention_mobile_log ".  
								" where rtidx not in ( ".
								        " SELECT rtidx ". 
								        " FROM duc_user_retention_mobile_log ".  
								        " WHERE useridx = $useridx ".
								        " AND adflag='$adflag' ".
								        " AND to_char(writedate, 'YYYY-MM-DD') = '$today' ". 
								        " ORDER BY writedate ASC limit 1 ".
								" ) ".
								" and useridx = $useridx ". 
								" AND adflag='$adflag'  ".
								" AND to_char(writedate, 'YYYY-MM-DD') = '$today' ";
				$delete_rtidx_list= $db_redshift->gettotallist($delete_sql);
				for($d=0;$d<sizeof($delete_rtidx_list);$d++)
				{
					$delete_rtidx = $delete_rtidx_list[$d]["rtidx"];
					
					if($delete_rdidx_sql == "")
						$delete_rdidx_sql .= "DELETE FROM duc_user_retention_mobile_log WHERE rtidx IN($delete_rtidx";
					else
						$delete_rdidx_sql .= ",$delete_rtidx";
					
					if($delete_count % 10000 == 0 || $i+1 == sizeof($date_list))
					{
						$delete_rdidx_sql .= ");";
// 						$db_redshift->execute($delete_rdidx_sql);
						write_log($delete_rdidx_sql);
						$delete_rdidx_sql = "";
					}
					
					$delete_count =$delete_count+1;
				}
				
			}
			
			//duc_user_retention_log
			$sql = " select useridx, adflag, cnt, today from (
					    select useridx, adflag, count(adflag) as cnt, to_char(writedate, 'YYYY-MM-DD') as today from duc_user_retention_log
					    group by useridx, adflag, to_char(writedate, 'YYYY-MM-DD')
					 )
					  where  cnt > 1";
			$date_list= $db_redshift->gettotallist($sql);
				
			$delete_sql = "";
			
			write_log("duc_user_retention_log - ".sizeof($date_list));
			for ($i = 0; $i < sizeof($date_list); $i++) {
			
				$useridx = $date_list[$i]['useridx'];
				$adflag = $date_list[$i]['adflag'];
				$today = $date_list[$i]['today'];
			
				$delete_sql = "DELETE FROM duc_user_retention_log  ".
						" WHERE useridx = $useridx ".
						" AND  rtidx != ( ".
						" SELECT rtidx ".
						" FROM duc_user_retention_log  ".
						" WHERE useridx = $useridx ".
						" AND adflag='$adflag' ".
						" AND to_char(writedate, 'YYYY-MM-DD') = '$today' ".
						" ORDER BY writedate ASC limit 1 ".
						" ) ".
						" AND to_char(writedate, 'YYYY-MM-DD') = '$today';";
				$db_redshift->execute($delete_sql);
			}
			
			$db_redshift->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>