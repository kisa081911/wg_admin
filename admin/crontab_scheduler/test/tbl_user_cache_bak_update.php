<?
	include("../../common/common_include.inc.php");

	$db_mobile = new CDatabase_Mobile();
	$db_analysis = new CDatabase_Analysis();
	$db_slave_main = new CDatabase_Slave_Main();
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();
	
	$db_mobile->execute("SET wait_timeout=3600");	
	$db_analysis->execute("SET wait_timeout=3600");	
	$db_main2->execute("SET wait_timeout=3600");	
	$db_slave_main->execute("SET wait_timeout=3600");	
	$db_other->execute("SET wait_timeout=3600");	
	
	$port = "";
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}	

	try
	{
		
 		while(1)
 		{
			$sql="SELECT * FROM tbl_user_cache_bak_update ORDER BY writedate DESC LIMIT 50";
			$cache_bak_update_list = $db_main2->gettotallist($sql);
			
			write_log(sizeof($cache_bak_update_list));
			
			if(sizeof($cache_bak_update_list) < 1 || sizeof($cache_bak_update_list) == "")
			{
				break;
			}
			else 
			{
				$delete_sql="DELETE FROM tbl_user_cache_bak_update WHERE updateidx IN ( ";
				$insert_sql="insert into `tbl_user_cache_update` (`useridx`, `coin`, `writedate`) values";
				
				for($i=0;$i<sizeof($cache_bak_update_list);$i++)
				{
					$updateidx = $cache_bak_update_list[$i]["updateidx"];
					$useridx = $cache_bak_update_list[$i]["useridx"];
					$coin = $cache_bak_update_list[$i]["coin"];
					$writedate = $cache_bak_update_list[$i]["writedate"];
					
					if($i+1 == sizeof($cache_bak_update_list))
					{
						$delete_sql .="$updateidx);";
						$insert_sql .="('$useridx','$coin','$writedate');";
					}
					else
					{
						$delete_sql .="$updateidx,";
						$insert_sql .="('$useridx','$coin','$writedate'),";
					}
				}
				
				$db_main2->execute($insert_sql);
				$db_main2->execute($delete_sql);
				
				sleep(60);
				
				$insert_sql ="";
				$delete_sql ="";
			}
		}
		
 	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_other->end();	
	$db_main2->end();	
	$db_slave_main->end();	
	$db_mobile->end();	
	$db_analysis->end();	
?>
