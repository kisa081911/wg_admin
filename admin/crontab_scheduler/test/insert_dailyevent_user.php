<?
	include("../../common/common_include.inc.php");
	
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	
	try
	{
		/*$sql = "SELECT useridx
				FROM
				(
					SELECT useridx FROM `tbl_user_retention_log` WHERE ('2020-01-05 00:00:00' <= writedate AND writedate < '2020-01-06 00:00:00') AND (leavedays BETWEEN 14 AND 28)
					UNION ALL
					SELECT useridx FROM `tbl_user_retention_mobile_log` WHERE ('2020-01-05 00:00:00' <= writedate AND writedate < '2020-01-06 00:00:00') AND (leavedays BETWEEN 14 AND 28)
				) tt GROUP BY useridx;";*/
		$sql = "SELECT useridx FROM (	
					SELECT * FROM `takefive2`.`tbl_user_freecoin_log_0` WHERE TYPE = 14 AND writedate >= '2020-01-03 00:00:00' UNION ALL
					SELECT * FROM `takefive2`.`tbl_user_freecoin_log_1` WHERE TYPE = 14 AND writedate >= '2020-01-03 00:00:00' UNION ALL
					SELECT * FROM `takefive2`.`tbl_user_freecoin_log_2` WHERE TYPE = 14 AND writedate >= '2020-01-03 00:00:00' UNION ALL
					SELECT * FROM `takefive2`.`tbl_user_freecoin_log_3` WHERE TYPE = 14 AND writedate >= '2020-01-03 00:00:00' UNION ALL
					SELECT * FROM `takefive2`.`tbl_user_freecoin_log_4` WHERE TYPE = 14 AND writedate >= '2020-01-03 00:00:00' UNION ALL
					SELECT * FROM `takefive2`.`tbl_user_freecoin_log_5` WHERE TYPE = 14 AND writedate >= '2020-01-03 00:00:00' UNION ALL
					SELECT * FROM `takefive2`.`tbl_user_freecoin_log_6` WHERE TYPE = 14 AND writedate >= '2020-01-03 00:00:00' UNION ALL
					SELECT * FROM `takefive2`.`tbl_user_freecoin_log_7` WHERE TYPE = 14 AND writedate >= '2020-01-03 00:00:00' UNION ALL
					SELECT * FROM `takefive2`.`tbl_user_freecoin_log_8` WHERE TYPE = 14 AND writedate >= '2020-01-03 00:00:00' UNION ALL
					SELECT * FROM `takefive2`.`tbl_user_freecoin_log_9` WHERE TYPE = 14 AND writedate >= '2020-01-03 00:00:00' 
				)t1 WHERE useridx NOT IN (SELECT useridx FROM tbl_dailyspin);";
		$user_list = $db_main2->gettotallist($sql);
		
		/*$sql = "SELECT useridx
				FROM
				(
					SELECT SUBSTR(contents, 31, 15) AS useridx FROM `server_log_mobile` WHERE contents LIKE 'Invalid Lucky Wheel useridx%' AND writedate >= '2020-01-07 00:00:00'
					UNION ALL
					SELECT SUBSTR(contents, 31, 15) AS useridx FROM `server_log_v2` WHERE contents LIKE 'Invalid Lucky Wheel useridx%' AND writedate >= '2020-01-07 00:00:00'
				) tt GROUP BY useridx";
		$user_list = $db_analysis->gettotallist($sql);*/
		
		for($i = 0; $i < sizeof($user_list); $i++)
		{
			$useridx = $user_list[$i]["useridx"];
			
			$sql = "SELECT COUNT(*) FROM tbl_dailystamp WHERE useridx = $useridx";
			$is_dailystamp = $db_main2->getvalue($sql);
			
			if($is_dailystamp == 0)
			{
				$sql = "INSERT INTO tbl_dailystamp(useridx, scratch, slot, wheel, day2r, day7r) VALUES($useridx, 1, 1, 1, 0, 0) ".
                       "       ON DUPLICATE KEY UPDATE days = 1, logindate = '2013-01-01 00:00:00', startdate = '2013-01-01 00:00:00';";
				$db_main2->execute($sql);
			}
			
			$sql = "SELECT COUNT(*) FROM tbl_dailyspin WHERE useridx = $useridx";
			$is_dailyspin = $db_main2->getvalue($sql);
				
			if($is_dailyspin == 0)
			{
				$sql = "INSERT INTO tbl_dailyspin(useridx) VALUES($useridx) ON DUPLICATE KEY UPDATE writedate = '2015-11-01 00:00:00';";
				$db_main2->execute($sql);
			}
			
			$sql = "SELECT COUNT(*) FROM tbl_daily_tybonus WHERE useridx = $useridx";
			$is_daily_tybonus = $db_main2->getvalue($sql);
				
			if($is_daily_tybonus == 0)
			{
				$sql = "INSERT INTO tbl_daily_tybonus(useridx, writedate) VALUES($useridx, NOW()) ".
                       "       ON DUPLICATE KEY UPDATE writedate = NOW(), bonus1 = 1, bonus2 = 1, bonus3 = 1, bonus4 = 1, bonus5 = 1;";
				$db_main2->execute($sql);
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
	$db_analysis->end();
?>