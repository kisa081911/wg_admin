<?
	include("../../common/common_include.inc.php");

	$db_main = new CDatabase_Main();

	
	$db_main->execute("SET wait_timeout=3600");	
	try
	{

		$sql = "SELECT t1.orderidx, amount, basecoin, IF(t2.category = 9, FLOOR(t2.facebookcredit / ((100 - t2.special_discount) / 100)), t2.facebookcredit) AS targetcredit, t2.facebookcredit 
				FROM tbl_product_order t1
				JOIN tbl_product t2
				ON  t1.productidx = t2.productidx
				AND category = 9
				WHERE t1.coin  =  t1.basecoin
				AND t1.status = 1
				and useridx > 20000
				ORDER BY t1.writedate DESC " ;
						
		$order_list = $db_main->gettotallist($sql);
	
		for($h=0; $h<sizeof($order_list); $h++)
		{
			$orderidx = $order_list[$h]["orderidx"];
			$amount = $order_list[$h]["amount"];
			$product_targetcredit = $order_list[$h]["targetcredit"];
			$facebookcredit = $order_list[$h]["facebookcredit"];
			
			echo "product_targetcredit - ".$product_targetcredit;
			echo "</br>";
			echo "facebookcredit - ".$facebookcredit;
			echo "</br>";
				
			
			$coin = $amount;
						
			$product_basecoin_arr = array(
					array("facebookcredit"=>"50", "basecoin"=>"400000"),
					array("facebookcredit"=>"90", "basecoin"=>"450000"),
					array("facebookcredit"=>"190", "basecoin"=>"500000"),
					array("facebookcredit"=>"390", "basecoin"=>"550000"),
					array("facebookcredit"=>"590", "basecoin"=>"720000"),
					array("facebookcredit"=>"990", "basecoin"=>"910000"),
					array("facebookcredit"=>"1990", "basecoin"=>"1200000"),
					array("facebookcredit"=>"2990", "basecoin"=>"1530000"),
					array("facebookcredit"=>"4990", "basecoin"=>"2000000")
			);
					
				if($product_targetcredit == 90)
				{
					// $9 Discount( $5 ~ $9 )
						
					$min_facebookcredit = 50;
					$max_facebookcredit = 90;
				}
				else if($product_targetcredit == 190)
				{
					// $19 Discount ( $9 ~ $19 )
						
					$min_facebookcredit = 90;
					$max_facebookcredit = 190;
				}
				else if($product_targetcredit == 390)
				{
					// $39 Discount ( $19 ~ $39 )
						
					$min_facebookcredit = 190;
					$max_facebookcredit = 390;
				}
				else if($product_targetcredit == 590)
				{
					// $59 Discount ( $39 ~ $59 )
						
					$min_facebookcredit = 390;
					$max_facebookcredit = 590;
				}
				else if($product_targetcredit == 990)
				{
					// $99 Discount ( $59 ~ $99 )
						
					$min_facebookcredit = 590;
					$max_facebookcredit = 990;
				}
				else if($product_targetcredit == 1990)
				{
					// $199 Discount ( $99 ~ $199 )
						
					$min_facebookcredit = 990;
					$max_facebookcredit = 1990;
				}
				else if($product_targetcredit == 2990)
				{
					// $299 Discount ( $199 ~ $299 )
						
					$min_facebookcredit = 1990;
					$max_facebookcredit = 2990;
				}
				else if($product_targetcredit == 4990)
				{
					// $499 Discount ( $299 ~ $499 )
						
					$min_facebookcredit = 2990;
					$max_facebookcredit = 4990;
				}
					
				echo "min_facebookcredit - ". $min_facebookcredit;
				echo "</br>";
				echo "max_facebookcredit - ". $max_facebookcredit;
				echo "</br>";
				
				for($i=0; $i<sizeof($product_basecoin_arr); $i++)
				{
					if($product_basecoin_arr[$i]["facebookcredit"] == $min_facebookcredit)
						$min_basecoin = $product_basecoin_arr[$i]["basecoin"];
				}
			
				for($i=0; $i<sizeof($product_basecoin_arr); $i++)
				{
					if($product_basecoin_arr[$i]["facebookcredit"] == $max_facebookcredit)
						$max_basecoin = $product_basecoin_arr[$i]["basecoin"];
				}
					
				$per_facebookcredit = $max_facebookcredit - $min_facebookcredit;
				$per_basecoin = $max_basecoin - $min_basecoin;
				echo "per_facebookcredit - ". $per_facebookcredit ;
				echo "</br>";
				echo "per_basecoin - ". $per_basecoin ;
				echo "</br>";
				$discount_facebookcredit = $facebookcredit - $min_facebookcredit;
					
				$add_basecoin = ($per_basecoin/$per_facebookcredit)*$discount_facebookcredit;
					
				$discount_basecoin = $min_basecoin + $add_basecoin;
					
				$basecoin = $discount_basecoin*($facebookcredit/10);
				$basecoin = round($basecoin);
				$update_sql = "UPDATE tbl_product_order SET basecoin = $basecoin WHERE orderidx = $orderidx; ";
				
				$db_main->execute($update_sql);
			}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}	
	
	$db_main->end();	
?>
