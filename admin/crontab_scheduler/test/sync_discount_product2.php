<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;

	ini_set("memory_limit", "-1");
	
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();	

	$db_other->execute("SET wait_timeout=72000");
	
	$product_basecoin_arr = array(
			array("facebookcredit"=>"50", "basecoin"=>"400000"),
			array("facebookcredit"=>"90", "basecoin"=>"450000"),
			array("facebookcredit"=>"190", "basecoin"=>"500000"),
			array("facebookcredit"=>"390", "basecoin"=>"550000"),
			array("facebookcredit"=>"590", "basecoin"=>"720000"),
			array("facebookcredit"=>"990", "basecoin"=>"910000"),
			array("facebookcredit"=>"1990", "basecoin"=>"1200000"),
			array("facebookcredit"=>"2990", "basecoin"=>"1530000"),
			array("facebookcredit"=>"4990", "basecoin"=>"2000000")
	);
	
	$sql = "SELECT logidx, money, product_coin, base_coin ".
			"FROM tbl_user_purchase_log WHERE platform = 0 AND more_rate = 0 ".
			"AND money NOT IN (1.00, 3.00, 4.00, 5.00, 9.00, 19.00, 39.00, 59.00, 99.00, 199.00, 299.00, 499.00, 777.00, 999.00) ORDER BY writedate DESC;"; 
	$discount_list = $db_other->gettotallist($sql);
	
	$insert_cnt = 0;
	$insert_sql = "";
	
	for($j=0; $j<sizeof($discount_list); $j++)
	{
		$logidx = $discount_list[$j]["logidx"];
		$money = $discount_list[$j]["money"];
		$product_coin = $discount_list[$j]["product_coin"];
		$base_coin = $discount_list[$j]["base_coin"];
				
		if($money > 5 && $money < 9)
		{
			// $9 Discount( $5 ~ $9 )
	
			$min_facebookcredit = 50;
			$max_facebookcredit = 90;
		}
		else if($money > 9 && $money < 19)
		{
			// $19 Discount ( $9 ~ $19 )
	
			$min_facebookcredit = 90;
			$max_facebookcredit = 190;
		}
		else if($money > 19 && $money < 39)
		{
			// $39 Discount ( $19 ~ $39 )
	
			$min_facebookcredit = 190;
			$max_facebookcredit = 390;
		}
		else if($money > 39 && $money < 59)
		{
			// $59 Discount ( $39 ~ $59 )
	
			$min_facebookcredit = 390;
			$max_facebookcredit = 590;
		}
		else if($money > 59 && $money < 99)
		{
			// $99 Discount ( $59 ~ $99 )
	
			$min_facebookcredit = 590;
			$max_facebookcredit = 990;
		}
		else if($money > 99 && $money < 199)
		{
			// $199 Discount ( $99 ~ $199 )
	
			$min_facebookcredit = 990;
			$max_facebookcredit = 1990;
		}
		else if($money > 199 && $money < 299)
		{
			// $299 Discount ( $199 ~ $299 )
	
			$min_facebookcredit = 1990;
			$max_facebookcredit = 2990;
		}
		else if($money > 299 && $money < 499)
		{
			// $499 Discount ( $299 ~ $499 )
	
			$min_facebookcredit = 2990;
			$max_facebookcredit = 4990;
		}
	
		for($i=0; $i<sizeof($product_basecoin_arr); $i++)
		{
			if($product_basecoin_arr[$i]["facebookcredit"] == $min_facebookcredit)
				$min_basecoin = $product_basecoin_arr[$i]["basecoin"];
		}
			
		for($i=0; $i<sizeof($product_basecoin_arr); $i++)
		{
			if($product_basecoin_arr[$i]["facebookcredit"] == $max_facebookcredit)
				$max_basecoin = $product_basecoin_arr[$i]["basecoin"];
		}
	
		$per_facebookcredit = $max_facebookcredit - $min_facebookcredit;
		$per_basecoin = $max_basecoin - $min_basecoin;
		
		$money = $money * 10;
	
		$discount_facebookcredit = $money - $min_facebookcredit;
	
		$add_basecoin = ($per_basecoin/$per_facebookcredit)*$discount_facebookcredit;
	
		$discount_basecoin = $min_basecoin + $add_basecoin;
		
		$basecoin = $discount_basecoin*($money/10);
		
		if($insert_cnt == 0)
		{
			$insert_sql = "UPDATE t5_user_purchase_log SET base_coin = $basecoin WHERE logidx = $logidx;";
		
			$insert_cnt++;
		}
		else if($insert_cnt < 500)
		{
			$insert_sql .= "UPDATE t5_user_purchase_log SET base_coin = $basecoin WHERE logidx = $logidx;";
		
			$insert_cnt++;
		}
		else
		{
			$insert_sql .= "UPDATE t5_user_purchase_log SET base_coin = $basecoin WHERE logidx = $logidx;";
			
			$db_redshift->execute($insert_sql);
		
			sleep(1);
		
			$insert_cnt = 0;
			$insert_sql = "";
		}
	}
	
	if($insert_sql != "")
		$db_redshift->execute($insert_sql);

	$db_other->end();
	$db_redshift->end();
?>
