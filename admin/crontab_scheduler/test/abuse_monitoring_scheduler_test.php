<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$db_slave_main = new CDatabase_Slave_Main();
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	$today = date("Y-m-d",strtotime("-1 days"));
	
	try 
	{
			//$day = date('w', strtotime($today)); // 일(0), 월(1) ..... 토(6)
			
			/*
			 * 기본 조건 : 스케쥴링 기간 동안 각 슬롯별 승률 130% 이상 && 스케쥴링 기간 동안의 (money_out - money_in) 상위 N 명
			 * 										아마존
			 * money_in 상위 10% 슬롯(각 5명) 			/ (각 3명) 
			 * money_in 상위 10% ~ 30% 슬롯(각 3명) 	/ (각 2명)
			 * money_in 상위 30% ~ 50% 슬롯(각 2명) 	/ (각 1명)
			 * money_in 하위 50% 슬롯(각 1명)			/ (각 1명)
			 * 스케쥴링 : 주 2회 - 일요일 밤, 수요일 밤 */
			
// 			if($day == 0 || $day == 3)			
// 			{
// 				// Web
// 				$sql = "SELECT slottype, SUM(money_in) AS money_in
// 						FROM t5_user_gamelog
// 						WHERE useridx > 20000 AND writedate > dateadd(d,-3,'$today')
// 						GROUP BY slottype
// 						ORDER BY money_in DESC";
// 				$web_slot_moneyin_list = $db_redshift->gettotallist($sql);
			
// 				$slot_count = $db_main2->getvalue("SELECT COUNT(*) FROM tbl_slot_list");
// 				$rank = 1;
				
// 				$insert_query = "";
				
// 				for($i=0; $i<sizeof($web_slot_moneyin_list); $i++)
// 				{
// 					$slottype = $web_slot_moneyin_list[$i]["slottype"];
// 					$rank_rate = ($rank / $slot_count) * 100;
					
// 					if($rank_rate < 10) // 상위 10% - 5명
// 						$limit = "5";
// 					else if(10 <= $rank_rate && $rank_rate < 30) // 상위 10% ~ 30% - 3명
// 						$limit = "3";
// 					else if(30 <= $rank_rate && $rank_rate < 50) // 상위 30% ~ 50% - 2명
// 						$limit = "2";
// 					else // 하위 50% - 1명
// 						$limit = "1";
					
// 					$sql = "SELECT useridx, SUM(money_in) AS money_in, SUM(money_out) AS money_out, SUM(money_out - money_in) AS winamount, (SUM(money_out)::FLOAT/ SUM(NULLIF(money_in,0))::FLOAT)*100 AS winrate
// 							FROM t5_user_gamelog
// 							WHERE useridx > 20000 AND writedate > dateadd(d,-3,'$today') AND slottype = $slottype
// 							GROUP BY useridx
// 							HAVING (SUM(money_out)::FLOAT/ SUM(NULLIF(money_in,0))::FLOAT)*100 >= 130
// 							ORDER BY winamount DESC
// 							LIMIT $limit";
// 					$abuse_list = $db_redshift->gettotallist($sql);
						
// 					for($j=0; $j<sizeof($abuse_list); $j++)
// 					{
// 						$useridx = $abuse_list[$j]["useridx"];
// 						$money_in = $abuse_list[$j]["money_in"];
// 						$money_out = $abuse_list[$j]["money_out"];
// 						$winamount = $abuse_list[$j]["winamount"];
// 						$winrate = $abuse_list[$j]["winrate"];
						
// 						$sql = "SELECT userid, nickname, coin, createdate
// 								FROM tbl_user
// 								WHERE useridx = $useridx";
// 						$user_info = $db_slave_main->getarray($sql);
						
// 						$userid = $user_info["userid"]; 
// 						$nickname = encode_db_field($user_info["nickname"]); 
// 						$coin = $user_info["coin"]; 
// 						$createdate = $user_info["createdate"];
						
// 						if($userid < 10)
// 							$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
// 						else
// 							$photourl = "http://graph.facebook.com/".$userid."/picture?type=square";
						
// 						if($insert_query == "")
// 							$insert_query = "INSERT INTO tbl_abuse_slot_user_list_new(useridx, userid, nickname, photourl, coin, platform, money_in, money_out, winamount, winrate, createdate, writedate, status) ".
// 											"VALUES($useridx, '$userid', '$nickname', '$photourl', $coin, 0, $money_in, $money_out, $winamount, $winrate, '$createdate', NOW(), 0)";
// 						else
// 							$insert_query .= ", ($useridx, '$userid', '$nickname', '$photourl', $coin, 0, $money_in, $money_out, $winamount, $winrate, '$createdate', NOW(), 0)";
// 					}
						
// 					$rank++;
// 				}
				
// // 				$db_other->execute($insert_query);
				
// 				// iOS
// 				$sql = "SELECT slottype, SUM(money_in) AS money_in
// 						FROM t5_user_gamelog_ios
// 						WHERE useridx > 20000 AND writedate > dateadd(d,-3,'$today')
// 						GROUP BY slottype
// 						ORDER BY money_in DESC";
// 				$ios_slot_moneyin_list = $db_redshift->gettotallist($sql);
			
// 				$slot_count = $db_main2->getvalue("SELECT COUNT(*) FROM tbl_slot_list");
// 				$rank = 1;
				
// 				$insert_query = "";
				
// 				for($i=0; $i<sizeof($ios_slot_moneyin_list); $i++)
// 				{
// 					$slottype = $ios_slot_moneyin_list[$i]["slottype"];
// 					$rank_rate = ($rank / $slot_count) * 100;
					
// 					if($rank_rate < 10) // 상위 10% - 5명
// 						$limit = "5";
// 					else if(10 <= $rank_rate && $rank_rate < 30) // 상위 10% ~ 30% - 3명
// 						$limit = "3";
// 					else if(30 <= $rank_rate && $rank_rate < 50) // 상위 30% ~ 50% - 2명
// 						$limit = "2";
// 					else // 하위 50% - 1명
// 						$limit = "1";
					
// 					$sql = "SELECT useridx, SUM(money_in) AS money_in, SUM(money_out) AS money_out, SUM(money_out - money_in) AS winamount, (SUM(money_out)::FLOAT/ SUM(NULLIF(money_in,0))::FLOAT)*100 AS winrate
// 							FROM t5_user_gamelog_ios
// 							WHERE useridx > 20000 AND writedate > dateadd(d,-3,'$today') AND slottype = $slottype
// 							GROUP BY useridx
// 							HAVING (SUM(money_out)::FLOAT/ SUM(NULLIF(money_in,0))::FLOAT)*100 >= 130
// 							ORDER BY winamount DESC
// 							LIMIT $limit";
// 					$abuse_list = $db_redshift->gettotallist($sql);
						
// 					for($j=0; $j<sizeof($abuse_list); $j++)
// 					{
// 						$useridx = $abuse_list[$j]["useridx"];
// 						$money_in = $abuse_list[$j]["money_in"];
// 						$money_out = $abuse_list[$j]["money_out"];
// 						$winamount = $abuse_list[$j]["winamount"];
// 						$winrate = $abuse_list[$j]["winrate"];
						
// 						$sql = "SELECT userid, nickname, coin, createdate
// 								FROM tbl_user
// 								WHERE useridx = $useridx";
// 						$user_info = $db_slave_main->getarray($sql);
						
// 						$userid = $user_info["userid"]; 
// 						$nickname = encode_db_field($user_info["nickname"]);
// 						$coin = $user_info["coin"]; 
// 						$createdate = $user_info["createdate"];
						
// 						if($userid < 10)
// 							$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
// 						else
// 							$photourl = "http://graph.facebook.com/".$userid."/picture?type=square";
						
// 						if($insert_query == "")
// 							$insert_query = "INSERT INTO tbl_abuse_slot_user_list_new(useridx, userid, nickname, photourl, coin, platform, money_in, money_out, winamount, winrate, createdate, writedate, status) ".
// 											"VALUES($useridx, '$userid', '$nickname', '$photourl', $coin, 1, $money_in, $money_out, $winamount, $winrate, '$createdate', NOW(), 0)";
// 						else
// 							$insert_query .= ", ($useridx, '$userid', '$nickname', '$photourl', $coin, 1, $money_in, $money_out, $winamount, $winrate, '$createdate', NOW(), 0)";
// 					}
						
// 					$rank++;
// 				}
				
// // 				$db_other->execute($insert_query);
				
// 				// Android
// 				$sql = "SELECT slottype, SUM(money_in) AS money_in
// 						FROM t5_user_gamelog_android
// 						WHERE useridx > 20000 AND writedate > dateadd(d,-3,'$today')
// 						GROUP BY slottype
// 						ORDER BY money_in DESC";
// 				$and_slot_moneyin_list = $db_redshift->gettotallist($sql);
			
// 				$slot_count = $db_main2->getvalue("SELECT COUNT(*) FROM tbl_slot_list");
// 				$rank = 1;
				
// 				$insert_query = "";
				
// 				for($i=0; $i<sizeof($and_slot_moneyin_list); $i++)
// 				{
// 					$slottype = $and_slot_moneyin_list[$i]["slottype"];
// 					$rank_rate = ($rank / $slot_count) * 100;
					
// 					if($rank_rate < 10) // 상위 10% - 5명
// 						$limit = "5";
// 					else if(10 <= $rank_rate && $rank_rate < 30) // 상위 10% ~ 30% - 3명
// 						$limit = "3";
// 					else if(30 <= $rank_rate && $rank_rate < 50) // 상위 30% ~ 50% - 2명
// 						$limit = "2";
// 					else // 하위 50% - 1명
// 						$limit = "1";
					
// 					$sql = "SELECT useridx, SUM(money_in) AS money_in, SUM(money_out) AS money_out, SUM(money_out - money_in) AS winamount, (SUM(money_out)::FLOAT/ SUM(NULLIF(money_in,0))::FLOAT)*100 AS winrate
// 							FROM t5_user_gamelog_android
// 							WHERE useridx > 20000 AND writedate > dateadd(d,-3,'$today') AND slottype = $slottype
// 							GROUP BY useridx
// 							HAVING (SUM(money_out)::FLOAT/ SUM(NULLIF(money_in,0))::FLOAT)*100 >= 130
// 							ORDER BY winamount DESC
// 							LIMIT $limit";
// 					$abuse_list = $db_redshift->gettotallist($sql);
						
// 					for($j=0; $j<sizeof($abuse_list); $j++)
// 					{
// 						$useridx = $abuse_list[$j]["useridx"];
// 						$money_in = $abuse_list[$j]["money_in"];
// 						$money_out = $abuse_list[$j]["money_out"];
// 						$winamount = $abuse_list[$j]["winamount"];
// 						$winrate = $abuse_list[$j]["winrate"];
						
// 						$sql = "SELECT userid, nickname, coin, createdate
// 								FROM tbl_user
// 								WHERE useridx = $useridx";
// 						$user_info = $db_slave_main->getarray($sql);
						
// 						$userid = $user_info["userid"]; 
// 						$nickname = encode_db_field($user_info["nickname"]); 
// 						$coin = $user_info["coin"]; 
// 						$createdate = $user_info["createdate"];
						
// 						if($userid < 10)
// 							$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
// 						else
// 							$photourl = "http://graph.facebook.com/".$userid."/picture?type=square";
						
// 						if($insert_query == "")
// 							$insert_query = "INSERT INTO tbl_abuse_slot_user_list_new(useridx, userid, nickname, photourl, coin, platform, money_in, money_out, winamount, winrate,  createdate, writedate, status) ".
// 											"VALUES($useridx, '$userid', '$nickname', '$photourl', $coin, 2, $money_in, $money_out, $winamount, $winrate, '$createdate', NOW(), 0)";
// 						else
// 							$insert_query .= ", ($useridx, '$userid', '$nickname', '$photourl', $coin, 2, $money_in, $money_out, $winamount, $winrate, '$createdate', NOW(), 0)";
// 					}
						
// 					$rank++;
// 				}
				
// // 				$db_other->execute($insert_query);
				
// 				// Amazon
// 				$sql = "SELECT slottype, SUM(money_in) AS money_in
// 						FROM t5_user_gamelog_amazon
// 						WHERE useridx > 20000 AND writedate > dateadd(d,-3,'$today')
// 						GROUP BY slottype
// 						ORDER BY money_in DESC";
// 				$ama_slot_moneyin_list = $db_redshift->gettotallist($sql);
			
// 				$slot_count = $db_main2->getvalue("SELECT COUNT(*) FROM tbl_slot_list");
// 				$rank = 1;
				
// 				$insert_query = "";
				
// 				for($i=0; $i<sizeof($ama_slot_moneyin_list); $i++)
// 				{
// 					$slottype = $ama_slot_moneyin_list[$i]["slottype"];
// 					$rank_rate = ($rank / $slot_count) * 100;
					
// 					if($rank_rate < 10) // 상위 10% - 3명
// 						$limit = "3";
// 					else if(10 <= $rank_rate && $rank_rate < 30) // 상위 10% ~ 30% - 2명
// 						$limit = "2";
// 					else if(30 <= $rank_rate && $rank_rate < 50) // 상위 30% ~ 50% - 1명
// 						$limit = "1";
// 					else // 하위 50% - 1명
// 						$limit = "1";
					
// 					$sql = "SELECT useridx, SUM(money_in) AS money_in, SUM(money_out) AS money_out, SUM(money_out - money_in) AS winamount, (SUM(money_out)::FLOAT/ SUM(NULLIF(money_in,0))::FLOAT)*100 AS winrate
// 							FROM t5_user_gamelog_amazon
// 							WHERE useridx > 20000 AND writedate > dateadd(d,-3,'$today') AND slottype = $slottype
// 							GROUP BY useridx
// 							HAVING (SUM(money_out)::FLOAT/ SUM(NULLIF(money_in,0))::FLOAT)*100 >= 130
// 							ORDER BY winamount DESC
// 							LIMIT $limit";
// 					$abuse_list = $db_redshift->gettotallist($sql);
						
// 					for($j=0; $j<sizeof($abuse_list); $j++)
// 					{
// 						$useridx = $abuse_list[$j]["useridx"];
// 						$money_in = $abuse_list[$j]["money_in"];
// 						$money_out = $abuse_list[$j]["money_out"];
// 						$winamount = $abuse_list[$j]["winamount"];
// 						$winrate = $abuse_list[$j]["winrate"];
						
// 						$sql = "SELECT userid, nickname, coin, createdate
// 								FROM tbl_user
// 								WHERE useridx = $useridx";
// 						$user_info = $db_slave_main->getarray($sql);
						
// 						$userid = $user_info["userid"]; 
// 						$nickname = encode_db_field($user_info["nickname"]); 
// 						$coin = $user_info["coin"]; 
// 						$createdate = $user_info["createdate"];
						
// 						if($userid < 10)
// 							$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
// 						else
// 							$photourl = "http://graph.facebook.com/".$userid."/picture?type=square";
						
// 						if($insert_query == "")
// 							$insert_query = "INSERT INTO tbl_abuse_slot_user_list_new(useridx, userid, nickname, photourl, coin, platform, money_in, money_out, winamount, winrate, createdate, writedate, status) ".
// 											"VALUES($useridx, '$userid', '$nickname', '$photourl', $coin, 3, $money_in, $money_out, $winamount, $winrate, '$createdate', NOW(), 0)";
// 						else
// 							$insert_query .= ", ($useridx, '$userid', '$nickname', '$photourl', $coin, 3, $money_in, $money_out, $winamount, $winrate, '$createdate', NOW(), 0)";
// 					}
						
// 					$rank++;
// 				}
				
// // 				$db_other->execute($insert_query);
// 			}
		
// 			if($day == 2)
// 			{
				/* 기본 조건 : 최근 일주일 미결제 && 일주일 이전 시점의 코인과 비교하여 급증한 사용자 상위 N 명
				 * Web(30명), IOS(20명), Android(30명), Amazon(5명)
				 * 스케쥴링 : 주 1회 - 화요일 밤
				 */
				
				// Web
				$sql = "SELECT useridx, currentcoin - week_before_coin AS increase_amount
						FROM (
							SELECT useridx, currentcoin, (SELECT currentcoin FROM t5_user_playstat_daily WHERE useridx > 20000 AND purchasecount = 0 AND purchaseamount = 0 AND purchasecoin = 0 AND today = dateadd(d, -7, '$today') AND useridx = t1.useridx) AS week_before_coin
							FROM t5_user_playstat_daily t1
							WHERE useridx > 20000 AND purchasecount = 0 AND purchaseamount = 0 AND purchasecoin = 0 AND today = '$today' AND days_after_purchase > 6
							AND useridx IN (
								SELECT useridx 
								FROM t5_user_playstat_daily 
								WHERE useridx > 20000 AND purchasecount = 0 AND purchaseamount = 0 AND purchasecoin = 0 AND today = dateadd(d, -7, '$today')
							)
							AND useridx NOT IN (
								SELECT DISTINCT useridx
								FROM (
									SELECT useridx
							  		FROM t5_product_order
							  		WHERE status = 1 AND writedate > dateadd(d, -7, '$today')
							  		UNION ALL
							  		SELECT useridx
							  		FROM t5_product_order_mobile
							  		WHERE status = 1 AND writedate > dateadd(d, -7, '$today')
							  	) t2
							)
						) t3
						GROUP BY useridx, currentcoin, week_before_coin
						HAVING week_before_coin*10 < currentcoin
						ORDER BY increase_amount DESC
						LIMIT 30";
				$coin_increase_user_list = $db_redshift->gettotallist($sql);
				
				$insert_query = "";
				
				for($i=0; $i<sizeof($coin_increase_user_list); $i++)
				{
					$useridx = $coin_increase_user_list[$i]["useridx"];
					
					$sql = "SELECT userid, nickname, coin, createdate FROM tbl_user WHERE useridx = $useridx";
					$user_info = $db_slave_main->getarray($sql);
					
					$userid = $user_info["userid"];
					$nickname = encode_db_field($user_info["nickname"]);
					$coin = $user_info["coin"];
					$createdate = $user_info["createdate"];
					
					if($userid < 10)
						$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
					else
						$photourl = "http://graph.facebook.com/".$userid."/picture?type=square";
					
					if($insert_query == "")
						$insert_query = "INSERT INTO tbl_coin_increase_user_list_new(useridx, userid, nickname, photourl, coin, platform, createdate, writedate, status) VALUES($useridx, '$userid', '$nickname', '$photourl', $coin, 0, '$createdate', NOW(), 0)";
					else
						$insert_query .= ", ($useridx, '$userid', '$nickname', '$photourl', $coin, 0, '$createdate', NOW(), 0)";
				}
				
				if($insert_query != "")
					write_log($insert_query);
 					$db_other->execute($insert_query);
				
				// iOS
				$sql = "SELECT useridx, currentcoin - week_before_coin AS increase_amount
						FROM (
							SELECT useridx, currentcoin, (SELECT currentcoin FROM t5_user_playstat_daily_ios WHERE useridx > 20000 AND purchasecount = 0 AND purchaseamount = 0 AND purchasecoin = 0 AND today = dateadd(d, -7, '$today') AND useridx = t1.useridx) AS week_before_coin
							FROM t5_user_playstat_daily_ios t1
							WHERE useridx > 20000 AND purchasecount = 0 AND purchaseamount = 0 AND purchasecoin = 0 AND today = '$today' AND days_after_purchase > 6
							AND useridx IN (
								SELECT useridx 
								FROM t5_user_playstat_daily_ios 
								WHERE useridx > 20000 AND purchasecount = 0 AND purchaseamount = 0 AND purchasecoin = 0 AND today = dateadd(d, -7, '$today')
							)
							AND useridx NOT IN (
								SELECT DISTINCT useridx
								FROM (
									SELECT useridx
							  		FROM t5_product_order
							  		WHERE status = 1 AND writedate > dateadd(d, -7, '$today')
							  		UNION ALL
							  		SELECT useridx
							  		FROM t5_product_order_mobile
							  		WHERE status = 1 AND writedate > dateadd(d, -7, '$today')
							  	) t2
							)
						) t3
						GROUP BY useridx, currentcoin, week_before_coin
						HAVING week_before_coin*10 < currentcoin
						ORDER BY increase_amount DESC
						LIMIT 20";
				$coin_increase_user_list = $db_redshift->gettotallist($sql);
				
				$insert_query = "";
				
				for($i=0; $i<sizeof($coin_increase_user_list); $i++)
				{
					$useridx = $coin_increase_user_list[$i]["useridx"];
					
					$sql = "SELECT userid, nickname, coin, createdate FROM tbl_user WHERE useridx = $useridx";
					$user_info = $db_slave_main->getarray($sql);
					
					$userid = $user_info["userid"];
					$nickname = encode_db_field($user_info["nickname"]);
					$coin = $user_info["coin"];
					$createdate = $user_info["createdate"];
					
					if($userid < 10)
						$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
					else
						$photourl = "http://graph.facebook.com/".$userid."/picture?type=square";
					
					if($insert_query == "")
						$insert_query = "INSERT INTO tbl_coin_increase_user_list_new(useridx, userid, nickname, photourl, coin, platform, createdate, writedate, status) VALUES($useridx, '$userid', '$nickname', '$photourl', $coin, 1, '$createdate', NOW(), 0)";
					else
						$insert_query .= ", ($useridx, '$userid', '$nickname', '$photourl', $coin, 1, '$createdate', NOW(), 0)";
				}
				
				if($insert_query != "")
					$db_other->execute($insert_query);
				
				// Android
				$sql = "SELECT useridx, currentcoin - week_before_coin AS increase_amount
						FROM (
							SELECT useridx, currentcoin, (SELECT currentcoin FROM t5_user_playstat_daily_android WHERE useridx > 20000 AND purchasecount = 0 AND purchaseamount = 0 AND purchasecoin = 0 AND today = dateadd(d, -7, '$today') AND useridx = t1.useridx) AS week_before_coin
							FROM t5_user_playstat_daily_android t1
							WHERE useridx > 20000 AND purchasecount = 0 AND purchaseamount = 0 AND purchasecoin = 0 AND today = '$today' AND days_after_purchase > 6
							AND useridx IN (
								SELECT useridx 
								FROM t5_user_playstat_daily_android 
								WHERE useridx > 20000 AND purchasecount = 0 AND purchaseamount = 0 AND purchasecoin = 0 AND today = dateadd(d, -7, '$today')
							)
							AND useridx NOT IN (
								SELECT DISTINCT useridx
								FROM (
									SELECT useridx
							  		FROM t5_product_order
							  		WHERE status = 1 AND writedate > dateadd(d, -7, '$today')
							  		UNION ALL
							  		SELECT useridx
							  		FROM t5_product_order_mobile
							  		WHERE status = 1 AND writedate > dateadd(d, -7, '$today')
							  	) t2
							)
						) t3
						GROUP BY useridx, currentcoin, week_before_coin
						HAVING week_before_coin*10 < currentcoin
						ORDER BY increase_amount DESC
						LIMIT 30";
				$coin_increase_user_list = $db_redshift->gettotallist($sql);
				
				$insert_query = "";
				
				for($i=0; $i<sizeof($coin_increase_user_list); $i++)
				{
					$useridx = $coin_increase_user_list[$i]["useridx"];
					
					$sql = "SELECT userid, nickname, coin, createdate FROM tbl_user WHERE useridx = $useridx";
					$user_info = $db_slave_main->getarray($sql);
					
					$userid = $user_info["userid"];
					$nickname = encode_db_field($user_info["nickname"]);
					$coin = $user_info["coin"];
					$createdate = $user_info["createdate"];
					
					if($userid < 10)
						$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
					else
						$photourl = "http://graph.facebook.com/".$userid."/picture?type=square";
					
					if($insert_query == "")
						$insert_query = "INSERT INTO tbl_coin_increase_user_list_new(useridx, userid, nickname, photourl, coin, platform, createdate, writedate, status) VALUES($useridx, '$userid', '$nickname', '$photourl', $coin, 2, '$createdate', NOW(), 0)";
					else
						$insert_query .= ", ($useridx, '$userid', '$nickname', '$photourl', $coin, 2, '$createdate', NOW(), 0)";
				}
				
				if($insert_query != "")
					$db_other->execute($insert_query);
				
				// Amazon
				$sql = "SELECT useridx, currentcoin - week_before_coin AS increase_amount
						FROM (
							SELECT useridx, currentcoin, (SELECT currentcoin FROM t5_user_playstat_daily_amazon WHERE useridx > 20000 AND purchasecount = 0 AND purchaseamount = 0 AND purchasecoin = 0 AND today = dateadd(d, -7, '$today') AND useridx = t1.useridx) AS week_before_coin
							FROM t5_user_playstat_daily_amazon t1
							WHERE useridx > 20000 AND purchasecount = 0 AND purchaseamount = 0 AND purchasecoin = 0 AND today = '$today' AND days_after_purchase > 6
							AND useridx IN (
								SELECT useridx 
								FROM t5_user_playstat_daily_amazon 
								WHERE useridx > 20000 AND purchasecount = 0 AND purchaseamount = 0 AND purchasecoin = 0 AND today = dateadd(d, -7, '$today')
							)
							AND useridx NOT IN (
								SELECT DISTINCT useridx
								FROM (
									SELECT useridx
							  		FROM t5_product_order
							  		WHERE status = 1 AND writedate > dateadd(d, -7, '$today')
							  		UNION ALL
							  		SELECT useridx
							  		FROM t5_product_order_mobile
							  		WHERE status = 1 AND writedate > dateadd(d, -7, '$today')
							  	) t2
							)
						) t3
						GROUP BY useridx, currentcoin, week_before_coin
						HAVING week_before_coin*10 < currentcoin
						ORDER BY increase_amount DESC
						LIMIT 5";
				$coin_increase_user_list = $db_redshift->gettotallist($sql);
				
				$insert_query = "";
				
				for($i=0; $i<sizeof($coin_increase_user_list); $i++)
				{
					$useridx = $coin_increase_user_list[$i]["useridx"];
					
					$sql = "SELECT userid, nickname, coin, createdate FROM tbl_user WHERE useridx = $useridx";
					$user_info = $db_slave_main->getarray($sql);
					
					$userid = $user_info["userid"];
					$nickname = encode_db_field($user_info["nickname"]);
					$coin = $user_info["coin"];
					$createdate = $user_info["createdate"];
					
					if($userid < 10)
						$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
					else
						$photourl = "http://graph.facebook.com/".$userid."/picture?type=square";
					
					if($insert_query == "")
						$insert_query = "INSERT INTO tbl_coin_increase_user_list_new(useridx, userid, nickname, photourl, coin, platform, createdate, writedate, status) VALUES($useridx, '$userid', '$nickname', '$photourl', $coin, 3, '$createdate', NOW(), 0)";
					else
						$insert_query .= ", ($useridx, '$userid', '$nickname', '$photourl', $coin, 3, '$createdate', NOW(), 0)";
				}
				
				if($insert_query != "")
					$db_other->execute($insert_query);
				
				
				$sql = "INSERT INTO tbl_coin_increase_user_check_list(useridx, userid, nickname, photourl, coin, adflag, createdate, writedate, status, comment, commentdate) ".
						"(SELECT useridx, userid, nickname, photourl, coin, platform, createdate, writedate, status, comment, commentdate FROM tbl_coin_increase_user_list_new WHERE STATUS = 1)";
				$db_other->execute($sql);
				
				$sql = "DELETE FROM tbl_coin_increase_user_list_new WHERE STATUS = 1";
				$db_other->execute($sql);
				
				
				/* 기본 조건 : 최근 일주일 미결제 && 최근 일주일 Unknown Coin 의 절대값의 합이 큰 사용자 상위 N 명(기준 1억이상)
				 * Web(5명), iOS(10명), Android(15명), Amazon(3명)
				 * 스케쥴링 : 주 1회 - 화요일 밤 */
				
				// Web
				$sql = "SELECT t1.useridx, t2.userid, t2.nickname, t2.coin, t2.createdate, SUM(ABS(coin_change)) AS unknown_coin_total
						FROM tbl_unknown_coin t1 INNER JOIN tbl_user t2 ON t1.useridx = t2.useridx
						WHERE t1.useridx > 20000 AND t1.writedate > DATE_SUB(NOW(), INTERVAL 1 WEEK)
						AND t1.useridx NOT IN (
							SELECT DISTINCT t3.useridx
							FROM (
								SELECT useridx
								FROM tbl_product_order
								WHERE `status` = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 1 WEEK)
								LIMIT 1
								UNION ALL
								SELECT useridx
								FROM tbl_product_order_mobile
								WHERE `status` = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 1 WEEK)
								LIMIT 1
							) t3
						)
						AND os_type = 0
						GROUP BY t1.useridx
						HAVING unknown_coin_total >= 100000000
						ORDER BY unknown_coin_total DESC
						LIMIT 5";
				$unknown_coin_user_list = $db_slave_main->gettotallist($sql);
				
				$insert_query = "";
				
				for($i=0; $i<sizeof($unknown_coin_user_list); $i++)
				{
					$useridx = $unknown_coin_user_list[$i]["useridx"];
					$userid = $unknown_coin_user_list[$i]["userid"];
					$nickname = encode_db_field($unknown_coin_user_list[$i]["nickname"]);
					$coin = $unknown_coin_user_list[$i]["coin"];
					$createdate = $unknown_coin_user_list[$i]["createdate"];
					$unknown_coin_total = $unknown_coin_user_list[$i]["unknown_coin_total"];
					
					if($userid < 10)
						$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
					else
						$photourl = "http://graph.facebook.com/".$userid."/picture?type=square";
					
					if($insert_query == "")
						$insert_query = "INSERT INTO tbl_unknowncoin_daily_new(useridx, userid, nickname, photourl, coin, platform, unknown_coin_total, createdate, writedate, status) ".
										"VALUES($useridx, '$userid', '$nickname', '$photourl', $coin, 0, $unknown_coin_total, '$createdate', NOW(), 0)";
					else
						$insert_query .= ", ($useridx, '$userid', '$nickname', '$photourl', $coin, 0, $unknown_coin_total, '$createdate', NOW(), 0)";
				}
				
				if($insert_query != "")
					$db_other->execute($insert_query);
				
				// iOS
				$sql = "SELECT t1.useridx, t2.userid, t2.nickname, t2.coin, t2.createdate, SUM(ABS(coin_change)) AS unknown_coin_total
						FROM tbl_unknown_coin t1 INNER JOIN tbl_user t2 ON t1.useridx = t2.useridx
						WHERE t1.useridx > 20000 AND t1.writedate > DATE_SUB(NOW(), INTERVAL 1 WEEK)
						AND t1.useridx NOT IN (
							SELECT DISTINCT t3.useridx
							FROM (
								SELECT useridx
								FROM tbl_product_order
								WHERE `status` = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 1 WEEK)
								LIMIT 1
								UNION ALL
								SELECT useridx
								FROM tbl_product_order_mobile
								WHERE `status` = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 1 WEEK)
								LIMIT 1
							) t3
						)
						AND os_type = 1
						GROUP BY t1.useridx
						HAVING unknown_coin_total >= 100000000
						ORDER BY unknown_coin_total DESC
						LIMIT 10";
				$unknown_coin_user_list = $db_slave_main->gettotallist($sql);
				
				$insert_query = "";
				
				for($i=0; $i<sizeof($unknown_coin_user_list); $i++)
				{
					$useridx = $unknown_coin_user_list[$i]["useridx"];
					$userid = $unknown_coin_user_list[$i]["userid"];
					$nickname = encode_db_field($unknown_coin_user_list[$i]["nickname"]);
					$coin = $unknown_coin_user_list[$i]["coin"];
					$createdate = $unknown_coin_user_list[$i]["createdate"];
					$unknown_coin_total = $unknown_coin_user_list[$i]["unknown_coin_total"];
					
					if($userid < 10)
						$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
					else
						$photourl = "http://graph.facebook.com/".$userid."/picture?type=square";
					
					if($insert_query == "")
						$insert_query = "INSERT INTO tbl_unknowncoin_daily_new(useridx, userid, nickname, photourl, coin, platform, unknown_coin_total, createdate, writedate, status) ".
										"VALUES($useridx, '$userid', '$nickname', '$photourl', $coin, 1, $unknown_coin_total, '$createdate', NOW(), 0)";
					else
						$insert_query .= ", ($useridx, '$userid', '$nickname', '$photourl', $coin, 1, $unknown_coin_total, '$createdate', NOW(), 0)";
				}
				
				if($insert_query != "")
					$db_other->execute($insert_query);
						
				// Android
				$sql = "SELECT t1.useridx, t2.userid, t2.nickname, t2.coin, t2.createdate, SUM(ABS(coin_change)) AS unknown_coin_total
						FROM tbl_unknown_coin t1 INNER JOIN tbl_user t2 ON t1.useridx = t2.useridx
						WHERE t1.useridx > 20000 AND t1.writedate > DATE_SUB(NOW(), INTERVAL 1 WEEK)
						AND t1.useridx NOT IN (
							SELECT DISTINCT t3.useridx
							FROM (
								SELECT useridx
								FROM tbl_product_order
								WHERE `status` = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 1 WEEK)
								LIMIT 1
								UNION ALL
								SELECT useridx
								FROM tbl_product_order_mobile
								WHERE `status` = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 1 WEEK)
								LIMIT 1
							) t3
						)
						AND os_type = 2
						GROUP BY t1.useridx
						HAVING unknown_coin_total >= 100000000
						ORDER BY unknown_coin_total DESC
						LIMIT 15";
				$unknown_coin_user_list = $db_slave_main->gettotallist($sql);
				
				$insert_query = "";
				
				for($i=0; $i<sizeof($unknown_coin_user_list); $i++)
				{
					$useridx = $unknown_coin_user_list[$i]["useridx"];
					$userid = $unknown_coin_user_list[$i]["userid"];
					$nickname = encode_db_field($unknown_coin_user_list[$i]["nickname"]);
					$coin = $unknown_coin_user_list[$i]["coin"];
					$createdate = $unknown_coin_user_list[$i]["createdate"];
					$unknown_coin_total = $unknown_coin_user_list[$i]["unknown_coin_total"];
					
					if($userid < 10)
						$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
					else
						$photourl = "http://graph.facebook.com/".$userid."/picture?type=square";
					
					if($insert_query == "")
						$insert_query = "INSERT INTO tbl_unknowncoin_daily_new(useridx, userid, nickname, photourl, coin, platform, unknown_coin_total, createdate, writedate, status) ".
										"VALUES($useridx, '$userid', '$nickname', '$photourl', $coin, 2, $unknown_coin_total, '$createdate', NOW(), 0)";
					else
						$insert_query .= ", ($useridx, '$userid', '$nickname', '$photourl', $coin, 2, $unknown_coin_total, '$createdate', NOW(), 0)";
				}
				
				if($insert_query != "")
					$db_other->execute($insert_query);
						
				// Amazon
				$sql = "SELECT t1.useridx, t2.userid, t2.nickname, t2.coin, t2.createdate, SUM(ABS(coin_change)) AS unknown_coin_total
						FROM tbl_unknown_coin t1 INNER JOIN tbl_user t2 ON t1.useridx = t2.useridx
						WHERE t1.useridx > 20000 AND t1.writedate > DATE_SUB(NOW(), INTERVAL 1 WEEK)
						AND t1.useridx NOT IN (
							SELECT DISTINCT t3.useridx
							FROM (
								SELECT useridx
								FROM tbl_product_order
								WHERE `status` = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 1 WEEK)
								LIMIT 1
								UNION ALL
								SELECT useridx
								FROM tbl_product_order_mobile
								WHERE `status` = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 1 WEEK)
								LIMIT 1
							) t3
						)
						AND os_type = 3
						GROUP BY t1.useridx
						HAVING unknown_coin_total >= 100000000
						ORDER BY unknown_coin_total DESC
						LIMIT 3";
				$unknown_coin_user_list = $db_slave_main->gettotallist($sql);
				
				$insert_query = "";
				
				for($i=0; $i<sizeof($unknown_coin_user_list); $i++)
				{
					$useridx = $unknown_coin_user_list[$i]["useridx"];
					$userid = $unknown_coin_user_list[$i]["userid"];
					$nickname = encode_db_field($unknown_coin_user_list[$i]["nickname"]);
					$coin = $unknown_coin_user_list[$i]["coin"];
					$createdate = $unknown_coin_user_list[$i]["createdate"];
					$unknown_coin_total = $unknown_coin_user_list[$i]["unknown_coin_total"];
					
					if($userid < 10)
						$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
					else
						$photourl = "http://graph.facebook.com/".$userid."/picture?type=square";
					
					if($insert_query == "")
						$insert_query = "INSERT INTO tbl_unknowncoin_daily_new(useridx, userid, nickname, photourl, coin, platform, unknown_coin_total, createdate, writedate, status) ".
										"VALUES($useridx, '$userid', '$nickname', '$photourl', $coin, 3, $unknown_coin_total, '$createdate', NOW(), 0)";
					else
						$insert_query .= ", ($useridx, '$userid', '$nickname', '$photourl', $coin, 3, $unknown_coin_total, '$createdate', NOW(), 0)";
				}
				
				if($insert_query != "")
					$db_other->execute($insert_query);
				
				$sql = "INSERT INTO tbl_unknowncoin_check_daily(useridx, userid, nickname, photourl, coin, os, unknown_coin, writedate, status, comment, commentdate) ".
						"(SELECT useridx, userid, nickname, photourl, coin, platform, unknown_coin_total, writedate, status, comment, commentdate FROM tbl_unknowncoin_daily_new WHERE STATUS = 1)";
				$db_other->execute($sql);
				
				$sql = "DELETE FROM tbl_unknowncoin_daily_new WHERE STATUS = 1";
				$db_other->execute($sql);
			}
// 		}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_slave_main->end();
	$db_main2->end();
	$db_other->end();
	$db_redshift->end();
?>