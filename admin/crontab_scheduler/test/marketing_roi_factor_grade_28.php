<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	$db_other->execute("SET wait_timeout=14400");
	
	$today = date("Y-m-d");
	
	try
	{
		#
		#
		#
		# 결제자
		#
		#
		#
		$sql = "Select platform, grade_28,						
				ROUND(SUM(CASE WHEN progress <= 	29	 AND progress >=0 and distance <=	211	  and distance >=	29	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	30	 AND progress >=0 and distance <= 	212	  and distance >=	30	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	31	 AND progress >=0 and distance <= 	213	  and distance >=	31	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	32	 AND progress >=0 and distance <=	214	  and distance >=	32	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	33	 AND progress >=0 and distance <=	215	  and distance >=	33	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	34	 AND progress >=0 and distance <=	216	  and distance >=	34	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	35	 AND progress >=0 and distance <=	217	  and distance >=	35	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	36	 AND progress >=0 and distance <=	218	  and distance >=	36	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	37	 AND progress >=0 and distance <=	219	  and distance >=	37	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	38	 AND progress >=0 and distance <=	220	  and distance >=	38	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	39	 AND progress >=0 and distance <=	221	  and distance >=	39	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	40	 AND progress >=0 and distance <=	222	  and distance >=	40	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	28	 AND progress >=0 and distance <= 	211	  and distance >=	29	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	29	 AND progress >=0 and distance <= 	212	  and distance >=	30	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	30	 AND progress >=0 and distance <=	213	  and distance >=	31	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	31	 AND progress >=0 and distance <=	214	  and distance >=	32	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	32	 AND progress >=0 and distance <=	215	  and distance >=	33	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	33	 AND progress >=0 and distance <=	216	  and distance >=	34	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	34	 AND progress >=0 and distance <=	217	  and distance >=	35	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	35	 AND progress >=0 and distance <=	218	  and distance >=	36	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	36	 AND progress >=0 and distance <=	219	  and distance >=	37	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	37	 AND progress >=0 and distance <=	220	  and distance >=	38	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	38	 AND progress >=0 and distance <=	221	  and distance >=	39	 then pay::float END),1)
										
										
				*ROUND(SUM(CASE WHEN progress <= 	41	 AND progress >=0 and distance <=	223	  and distance >=	41	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	42	 AND progress >=0 and distance <=	224	  and distance >=	42	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	43	 AND progress >=0 and distance <=	225	  and distance >=	43	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	44	 AND progress >=0 and distance <=	226	  and distance >=	44	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	45	 AND progress >=0 and distance <=	227	  and distance >=	45	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	46	 AND progress >=0 and distance <=	228	  and distance >=	46	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	47	 AND progress >=0 and distance <=	229	  and distance >=	47	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	48	 AND progress >=0 and distance <=	230	  and distance >=	48	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	49	 AND progress >=0 and distance <=	231	  and distance >=	49	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	50	 AND progress >=0 and distance <=	232	  and distance >=	50	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	51	 AND progress >=0 and distance <=	233	  and distance >=	51	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	52	 AND progress >=0 and distance <=	234	  and distance >=	52	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	53	 AND progress >=0 and distance <=	235	  and distance >=	53	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	54	 AND progress >=0 and distance <=	236	  and distance >=	54	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	55	 AND progress >=0 and distance <=	237	  and distance >=	55	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	56	 AND progress >=0 and distance <=	238	  and distance >=	56	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	57	 AND progress >=0 and distance <=	239	  and distance >=	57	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	58	 AND progress >=0 and distance <=	240	  and distance >=	58	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	59	 AND progress >=0 and distance <=	241	  and distance >=	59	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	60	 AND progress >=0 and distance <=	242	  and distance >=	60	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	39	 AND progress >=0 and distance <=	222	  and distance >=	40	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	40	 AND progress >=0 and distance <=	223	  and distance >=	41	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	41	 AND progress >=0 and distance <=	224	  and distance >=	42	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	42	 AND progress >=0 and distance <=	225	  and distance >=	43	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	43	 AND progress >=0 and distance <=	226	  and distance >=	44	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	44	 AND progress >=0 and distance <=	227	  and distance >=	45	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	45	 AND progress >=0 and distance <=	228	  and distance >=	46	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	46	 AND progress >=0 and distance <=	229	  and distance >=	47	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	47	 AND progress >=0 and distance <=	230	  and distance >=	48	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	48	 AND progress >=0 and distance <=	231	  and distance >=	49	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	49	 AND progress >=0 and distance <=	232	  and distance >=	50	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	50	 AND progress >=0 and distance <=	233	  and distance >=	51	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	51	 AND progress >=0 and distance <=	234	  and distance >=	52	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	52	 AND progress >=0 and distance <=	235	  and distance >=	53	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	53	 AND progress >=0 and distance <=	236	  and distance >=	54	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	54	 AND progress >=0 and distance <=	237	  and distance >=	55	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	55	 AND progress >=0 and distance <=	238	  and distance >=	56	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	56	 AND progress >=0 and distance <=	239	  and distance >=	57	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	57	 AND progress >=0 and distance <=	240	  and distance >=	58	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	58	 AND progress >=0 and distance <=	241	  and distance >=	59	 then pay::float END),1)
										
										
				*ROUND(SUM(CASE WHEN progress <= 	61	 AND progress >=0 and distance <=	243	  and distance >=	61	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	62	 AND progress >=0 and distance <=	244	  and distance >=	62	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	63	 AND progress >=0 and distance <=	245	  and distance >=	63	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	64	 AND progress >=0 and distance <=	246	  and distance >=	64	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	65	 AND progress >=0 and distance <=	247	  and distance >=	65	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	66	 AND progress >=0 and distance <=	248	  and distance >=	66	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	67	 AND progress >=0 and distance <=	249	  and distance >=	67	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	68	 AND progress >=0 and distance <=	250	  and distance >=	68	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	69	 AND progress >=0 and distance <=	251	  and distance >=	69	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	70	 AND progress >=0 and distance <=	252	  and distance >=	70	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	71	 AND progress >=0 and distance <=	253	  and distance >=	71	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	72	 AND progress >=0 and distance <=	254	  and distance >=	72	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	73	 AND progress >=0 and distance <=	255	  and distance >=	73	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	74	 AND progress >=0 and distance <=	256	  and distance >=	74	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	75	 AND progress >=0 and distance <=	257	  and distance >=	75	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	76	 AND progress >=0 and distance <=	258	  and distance >=	76	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	77	 AND progress >=0 and distance <=	259	  and distance >=	77	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	78	 AND progress >=0 and distance <=	260	  and distance >=	78	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	79	 AND progress >=0 and distance <=	261	  and distance >=	79	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	80	 AND progress >=0 and distance <=	262	  and distance >=	80	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	59	 AND progress >=0 and distance <=	242	  and distance >=	60	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	60	 AND progress >=0 and distance <=	243	  and distance >=	61	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	61	 AND progress >=0 and distance <=	244	  and distance >=	62	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	62	 AND progress >=0 and distance <=	245	  and distance >=	63	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	63	 AND progress >=0 and distance <=	246	  and distance >=	64	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	64	 AND progress >=0 and distance <=	247	  and distance >=	65	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	65	 AND progress >=0 and distance <=	248	  and distance >=	66	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	66	 AND progress >=0 and distance <=	249	  and distance >=	67	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	67	 AND progress >=0 and distance <=	250	  and distance >=	68	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	68	 AND progress >=0 and distance <=	251	  and distance >=	69	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	69	 AND progress >=0 and distance <=	252	  and distance >=	70	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	70	 AND progress >=0 and distance <=	253	  and distance >=	71	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	71	 AND progress >=0 and distance <=	254	  and distance >=	72	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	72	 AND progress >=0 and distance <=	255	  and distance >=	73	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	73	 AND progress >=0 and distance <=	256	  and distance >=	74	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	74	 AND progress >=0 and distance <=	257	  and distance >=	75	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	75	 AND progress >=0 and distance <=	258	  and distance >=	76	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	76	 AND progress >=0 and distance <=	259	  and distance >=	77	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	77	 AND progress >=0 and distance <=	260	  and distance >=	78	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	78	 AND progress >=0 and distance <=	261	  and distance >=	79	 then pay::float END),1)
										
										
				*ROUND(SUM(CASE WHEN progress <= 	81	 AND progress >=0 and distance <=	263	  and distance >=	81	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	82	 AND progress >=0 and distance <=	264	  and distance >=	82	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	83	 AND progress >=0 and distance <=	265	  and distance >=	83	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	84	 AND progress >=0 and distance <=	266	  and distance >=	84	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	85	 AND progress >=0 and distance <=	267	  and distance >=	85	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	86	 AND progress >=0 and distance <=	268	  and distance >=	86	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	87	 AND progress >=0 and distance <=	269	  and distance >=	87	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	88	 AND progress >=0 and distance <=	270	  and distance >=	88	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	89	 AND progress >=0 and distance <=	271	  and distance >=	89	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	90	 AND progress >=0 and distance <=	272	  and distance >=	90	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	79	 AND progress >=0 and distance <=	262	  and distance >=	80	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	80	 AND progress >=0 and distance <=	263	  and distance >=	81	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	81	 AND progress >=0 and distance <=	264	  and distance >=	82	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	82	 AND progress >=0 and distance <=	265	  and distance >=	83	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	83	 AND progress >=0 and distance <=	266	  and distance >=	84	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	84	 AND progress >=0 and distance <=	267	  and distance >=	85	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	85	 AND progress >=0 and distance <=	268	  and distance >=	86	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	86	 AND progress >=0 and distance <=	269	  and distance >=	87	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	87	 AND progress >=0 and distance <=	270	  and distance >=	88	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	88	 AND progress >=0 and distance <=	271	  and distance >=	89	 then pay::float END),1) as multiple,
				ROUND(SUM(CASE WHEN progress <= 	89	 AND progress >=0 and distance <=	272	  and distance >=	90	 then pay::float END),1) as divd
				
				FROM (						
					SELECT platform, tt.useridx, grade_28, pay, progress, distance					
					FROM (					
						SELECT platform, useridx, money AS pay, ceil(datediff(second, createdate, writedate)/86400::float) as progress, ceil(datediff(second, createdate, dateadd(day, 1, current_date))/86400::float) as distance				
						FROM (				
							SELECT t1.useridx, t2.writedate, ROUND(facebookcredit::float/10::float, 2) AS money, t1.createdate, platform			
							FROM (			
								SELECT useridx, createdate, platform		
								FROM t5_user		
								WHERE useridx > 20000 and createdate >= dateadd(day,(-543), current_date)		
							) t1 JOIN t5_product_order t2 on t1.useridx = t2.useridx			 
							WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 365			
							UNION ALL			
							SELECT t1.useridx, t2.writedate, ROUND(money, 2) AS pay, t1.createdate, platform			
							FROM (			
								SELECT useridx, createdate, platform		
								FROM t5_user		
								WHERE useridx > 20000 and createdate >= dateadd(day,(-543), current_date)		
							) t1 JOIN t5_product_order_mobile t2 on t1.useridx = t2.useridx			
							WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 365			
						) AS payer_t				
					) tt join (					
						SELECT useridx, 		 		
								(case when ROUND(SUM(money), 2) >= 499 then 5		
									  when ROUND(SUM(money), 2) >= 199 and ROUND(SUM(money), 2) < 499 then 4	
									  when ROUND(SUM(money), 2) >= 59 and ROUND(SUM(money), 2) < 199 then 3	
									  when ROUND(SUM(money), 2) >= 9 and ROUND(SUM(money), 2) < 59 then 2	
									  when ROUND(SUM(money), 2) < 9 and ROUND(SUM(money), 2) > 0 then 1	
								end) AS grade_28		
						FROM (				
							SELECT t1.useridx, t2.writedate, ROUND(facebookcredit::float/10::float, 2) AS money, t1.createdate, platform			
							FROM (			
								SELECT useridx, createdate, platform, dateadd(day, 1, current_date) as today  		
								FROM t5_user		
								WHERE useridx > 20000 and createdate >= dateadd(day,(-543), current_date)		
							) t1 JOIN t5_product_order t2 on t1.useridx = t2.useridx			
							WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 28			
							UNION ALL			
							SELECT t1.useridx, t2.writedate, ROUND(money, 2) AS pay, t1.createdate, platform			
							FROM (			
								SELECT useridx, createdate, platform, dateadd(day, 1, current_date) as today  		
								FROM t5_user		
								WHERE useridx > 20000 and createdate >= dateadd(day,(-543), current_date)		
							) t1 JOIN t5_product_order_mobile t2 on t1.useridx = t2.useridx			
							WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 28			
						) AS payer_t				
						GROUP BY useridx				
					) as grade on tt.useridx = grade.useridx					
				) total						
								GROUP BY platform, grade_28		
								ORDER BY platform, grade_28";
		$payer_roi_factor_list = $db_redshift->gettotallist($sql);
		
		$insert_sql1 = "";
		$insert_sql2 = "";
		
		for($i=0; $i<sizeof($payer_roi_factor_list); $i++)
		{
			$platform = $payer_roi_factor_list[$i]["platform"];
			$grade_28 = $payer_roi_factor_list[$i]["grade_28"];
			$multiple = $payer_roi_factor_list[$i]["multiple"];
			$divd = $payer_roi_factor_list[$i]["divd"];
			
			if($insert_sql1 == "")
			{
				$insert_sql1 = "INSERT INTO tbl_roi_factor VALUES('$today', 0, 3, 1, $platform, $grade_28, $multiple, $divd)";
				$insert_sql2 = "INSERT INTO tbl_roi_factor VALUES('$today', 1, 3, 1, $platform, $grade_28, $multiple, $divd)";
			}
			else
			{
				$insert_sql1 .= ",('$today', 0, 3, 1, $platform, $grade_28, $multiple, $divd)";
				$insert_sql2 .= ",('$today', 1, 3, 1, $platform, $grade_28, $multiple, $divd)";
			}
		}
		
		if($insert_sql1 != "")
		{
			$insert_sql1 .= "ON DUPLICATE KEY UPDATE multiple = VALUES(multiple), divd = VALUES(divd) ";
			$insert_sql2 .= "ON DUPLICATE KEY UPDATE multiple = VALUES(multiple), divd = VALUES(divd) ";
 			//$db_other->execute($insert_sql1);
 			//$db_other->execute($insert_sql2);
		}
		
		#
		#
		# 비결제자
		#
		#
		#
		$sql = "
				Select platform,						
										
										
				ROUND(SUM(CASE WHEN progress <= 	29	 AND progress >=0 and distance <=	211	 and distance >=	29	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	30	 AND progress >=0 and distance <= 	212	 and distance >=	30	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	31	 AND progress >=0 and distance <= 	213	 and distance >=	31	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	32	 AND progress >=0 and distance <=	214	 and distance >=	32	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	33	 AND progress >=0 and distance <=	215	 and distance >=	33	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	34	 AND progress >=0 and distance <=	216	 and distance >=	34	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	35	 AND progress >=0 and distance <=	217	 and distance >=	35	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	36	 AND progress >=0 and distance <=	218	 and distance >=	36	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	37	 AND progress >=0 and distance <=	219	 and distance >=	37	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	38	 AND progress >=0 and distance <=	220	 and distance >=	38	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	39	 AND progress >=0 and distance <=	221	 and distance >=	39	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	40	 AND progress >=0 and distance <=	222	 and distance >=	40	 then pay::float END),1)
										
										
				/ROUND(SUM(CASE WHEN progress <= 	28	 AND progress >=0 and distance <= 	211	 and distance >=	29	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	29	 AND progress >=0 and distance <= 	212	 and distance >=	30	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	30	 AND progress >=0 and distance <=	213	 and distance >=	31	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	31	 AND progress >=0 and distance <=	214	 and distance >=	32	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	32	 AND progress >=0 and distance <=	215	 and distance >=	33	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	33	 AND progress >=0 and distance <=	216	 and distance >=	34	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	34	 AND progress >=0 and distance <=	217	 and distance >=	35	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	35	 AND progress >=0 and distance <=	218	 and distance >=	36	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	36	 AND progress >=0 and distance <=	219	 and distance >=	37	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	37	 AND progress >=0 and distance <=	220	 and distance >=	38	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	38	 AND progress >=0 and distance <=	221	 and distance >=	39	 then pay::float END),1)
										
										
				*ROUND(SUM(CASE WHEN progress <= 	41	 AND progress >=0 and distance <=	223	 and distance >=	41	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	42	 AND progress >=0 and distance <=	224	 and distance >=	42	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	43	 AND progress >=0 and distance <=	225	 and distance >=	43	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	44	 AND progress >=0 and distance <=	226	 and distance >=	44	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	45	 AND progress >=0 and distance <=	227	 and distance >=	45	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	46	 AND progress >=0 and distance <=	228	 and distance >=	46	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	47	 AND progress >=0 and distance <=	229	 and distance >=	47	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	48	 AND progress >=0 and distance <=	230	 and distance >=	48	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	49	 AND progress >=0 and distance <=	231	 and distance >=	49	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	50	 AND progress >=0 and distance <=	232	 and distance >=	50	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	51	 AND progress >=0 and distance <=	233	 and distance >=	51	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	52	 AND progress >=0 and distance <=	234	 and distance >=	52	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	53	 AND progress >=0 and distance <=	235	 and distance >=	53	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	54	 AND progress >=0 and distance <=	236	 and distance >=	54	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	55	 AND progress >=0 and distance <=	237	 and distance >=	55	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	56	 AND progress >=0 and distance <=	238	 and distance >=	56	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	57	 AND progress >=0 and distance <=	239	 and distance >=	57	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	58	 AND progress >=0 and distance <=	240	 and distance >=	58	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	59	 AND progress >=0 and distance <=	241	 and distance >=	59	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	60	 AND progress >=0 and distance <=	242	 and distance >=	60	 then pay::float END),1)
										
										
				/ROUND(SUM(CASE WHEN progress <= 	39	 AND progress >=0 and distance <=	222	 and distance >=	40	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	40	 AND progress >=0 and distance <=	223	 and distance >=	41	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	41	 AND progress >=0 and distance <=	224	 and distance >=	42	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	42	 AND progress >=0 and distance <=	225	 and distance >=	43	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	43	 AND progress >=0 and distance <=	226	 and distance >=	44	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	44	 AND progress >=0 and distance <=	227	 and distance >=	45	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	45	 AND progress >=0 and distance <=	228	 and distance >=	46	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	46	 AND progress >=0 and distance <=	229	 and distance >=	47	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	47	 AND progress >=0 and distance <=	230	 and distance >=	48	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	48	 AND progress >=0 and distance <=	231	 and distance >=	49	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	49	 AND progress >=0 and distance <=	232	 and distance >=	50	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	50	 AND progress >=0 and distance <=	233	 and distance >=	51	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	51	 AND progress >=0 and distance <=	234	 and distance >=	52	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	52	 AND progress >=0 and distance <=	235	 and distance >=	53	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	53	 AND progress >=0 and distance <=	236	 and distance >=	54	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	54	 AND progress >=0 and distance <=	237	 and distance >=	55	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	55	 AND progress >=0 and distance <=	238	 and distance >=	56	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	56	 AND progress >=0 and distance <=	239	 and distance >=	57	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	57	 AND progress >=0 and distance <=	240	 and distance >=	58	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	58	 AND progress >=0 and distance <=	241	 and distance >=	59	 then pay::float END),1)
										
										
				*ROUND(SUM(CASE WHEN progress <= 	61	 AND progress >=0 and distance <=	243	 and distance >=	61	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	62	 AND progress >=0 and distance <=	244	 and distance >=	62	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	63	 AND progress >=0 and distance <=	245	 and distance >=	63	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	64	 AND progress >=0 and distance <=	246	 and distance >=	64	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	65	 AND progress >=0 and distance <=	247	 and distance >=	65	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	66	 AND progress >=0 and distance <=	248	 and distance >=	66	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	67	 AND progress >=0 and distance <=	249	 and distance >=	67	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	68	 AND progress >=0 and distance <=	250	 and distance >=	68	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	69	 AND progress >=0 and distance <=	251	 and distance >=	69	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	70	 AND progress >=0 and distance <=	252	 and distance >=	70	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	71	 AND progress >=0 and distance <=	253	 and distance >=	71	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	72	 AND progress >=0 and distance <=	254	 and distance >=	72	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	73	 AND progress >=0 and distance <=	255	 and distance >=	73	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	74	 AND progress >=0 and distance <=	256	 and distance >=	74	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	75	 AND progress >=0 and distance <=	257	 and distance >=	75	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	76	 AND progress >=0 and distance <=	258	 and distance >=	76	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	77	 AND progress >=0 and distance <=	259	 and distance >=	77	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	78	 AND progress >=0 and distance <=	260	 and distance >=	78	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	79	 AND progress >=0 and distance <=	261	 and distance >=	79	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	80	 AND progress >=0 and distance <=	262	 and distance >=	80	 then pay::float END),1)
										
										
				/ROUND(SUM(CASE WHEN progress <= 	59	 AND progress >=0 and distance <=	242	 and distance >=	60	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	60	 AND progress >=0 and distance <=	243	 and distance >=	61	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	61	 AND progress >=0 and distance <=	244	 and distance >=	62	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	62	 AND progress >=0 and distance <=	245	 and distance >=	63	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	63	 AND progress >=0 and distance <=	246	 and distance >=	64	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	64	 AND progress >=0 and distance <=	247	 and distance >=	65	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	65	 AND progress >=0 and distance <=	248	 and distance >=	66	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	66	 AND progress >=0 and distance <=	249	 and distance >=	67	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	67	 AND progress >=0 and distance <=	250	 and distance >=	68	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	68	 AND progress >=0 and distance <=	251	 and distance >=	69	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	69	 AND progress >=0 and distance <=	252	 and distance >=	70	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	70	 AND progress >=0 and distance <=	253	 and distance >=	71	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	71	 AND progress >=0 and distance <=	254	 and distance >=	72	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	72	 AND progress >=0 and distance <=	255	 and distance >=	73	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	73	 AND progress >=0 and distance <=	256	 and distance >=	74	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	74	 AND progress >=0 and distance <=	257	 and distance >=	75	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	75	 AND progress >=0 and distance <=	258	 and distance >=	76	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	76	 AND progress >=0 and distance <=	259	 and distance >=	77	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	77	 AND progress >=0 and distance <=	260	 and distance >=	78	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	78	 AND progress >=0 and distance <=	261	 and distance >=	79	 then pay::float END),1)
										
										
				*ROUND(SUM(CASE WHEN progress <= 	81	 AND progress >=0 and distance <=	263	 and distance >=	81	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	82	 AND progress >=0 and distance <=	264	 and distance >=	82	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	83	 AND progress >=0 and distance <=	265	 and distance >=	83	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	84	 AND progress >=0 and distance <=	266	 and distance >=	84	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	85	 AND progress >=0 and distance <=	267	 and distance >=	85	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	86	 AND progress >=0 and distance <=	268	 and distance >=	86	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	87	 AND progress >=0 and distance <=	269	 and distance >=	87	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	88	 AND progress >=0 and distance <=	270	 and distance >=	88	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	89	 AND progress >=0 and distance <=	271	 and distance >=	89	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	90	 AND progress >=0 and distance <=	272	 and distance >=	90	 then pay::float END),1)
									
										
				/ROUND(SUM(CASE WHEN progress <= 	79	 AND progress >=0 and distance <=	262	 and distance >=	80	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	80	 AND progress >=0 and distance <=	263	 and distance >=	81	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	81	 AND progress >=0 and distance <=	264	 and distance >=	82	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	82	 AND progress >=0 and distance <=	265	 and distance >=	83	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	83	 AND progress >=0 and distance <=	266	 and distance >=	84	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	84	 AND progress >=0 and distance <=	267	 and distance >=	85	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	85	 AND progress >=0 and distance <=	268	 and distance >=	86	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	86	 AND progress >=0 and distance <=	269	 and distance >=	87	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	87	 AND progress >=0 and distance <=	270	 and distance >=	88	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	88	 AND progress >=0 and distance <=	271	 and distance >=	89	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	89	 AND progress >=0 and distance <=	272	 and distance >=	90	 then pay::float END),1)						
										
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	28	 AND progress >=0 and distance <= 	211	 and distance >=	29	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	29	 AND progress >=0 and distance <= 	212	 and distance >=	30	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	30	 AND progress >=0 and distance <= 	213	 and distance >=	31	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	31	 AND progress >=0 and distance <=	214	 and distance >=	32	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	32	 AND progress >=0 and distance <=	215	 and distance >=	33	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	33	 AND progress >=0 and distance <=	216	 and distance >=	34	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	34	 AND progress >=0 and distance <=	217	 and distance >=	35	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	35	 AND progress >=0 and distance <=	218	 and distance >=	36	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	36	 AND progress >=0 and distance <=	219	 and distance >=	37	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	37	 AND progress >=0 and distance <=	220	 and distance >=	38	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	38	 AND progress >=0 and distance <=	221	 and distance >=	39	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	39	 AND progress >=0 and distance <=	222	 and distance >=	40	 then pay::float END),1)
										
										
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	29	 AND progress >=0 and distance <= 	211	 and distance >=	29	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	30	 AND progress >=0 and distance <= 	212	 and distance >=	30	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	31	 AND progress >=0 and distance <=	213	 and distance >=	31	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	32	 AND progress >=0 and distance <=	214	 and distance >=	32	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	33	 AND progress >=0 and distance <=	215	 and distance >=	33	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	34	 AND progress >=0 and distance <=	216	 and distance >=	34	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	35	 AND progress >=0 and distance <=	217	 and distance >=	35	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	36	 AND progress >=0 and distance <=	218	 and distance >=	36	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	37	 AND progress >=0 and distance <=	219	 and distance >=	37	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	38	 AND progress >=0 and distance <=	220	 and distance >=	38	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	39	 AND progress >=0 and distance <=	221	 and distance >=	39	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	40	 AND progress >=0 and distance <=	222	 and distance >=	40	 then pay::float END),1)
										
										
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	40	 AND progress >=0 and distance <=	223	 and distance >=	41	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	41	 AND progress >=0 and distance <=	224	 and distance >=	42	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	42	 AND progress >=0 and distance <=	225	 and distance >=	43	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	43	 AND progress >=0 and distance <=	226	 and distance >=	44	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	44	 AND progress >=0 and distance <=	227	 and distance >=	45	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	45	 AND progress >=0 and distance <=	228	 and distance >=	46	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	46	 AND progress >=0 and distance <=	229	 and distance >=	47	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	47	 AND progress >=0 and distance <=	230	 and distance >=	48	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	48	 AND progress >=0 and distance <=	231	 and distance >=	49	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	49	 AND progress >=0 and distance <=	232	 and distance >=	50	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	50	 AND progress >=0 and distance <=	233	 and distance >=	51	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	51	 AND progress >=0 and distance <=	234	 and distance >=	52	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	52	 AND progress >=0 and distance <=	235	 and distance >=	53	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	53	 AND progress >=0 and distance <=	236	 and distance >=	54	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	54	 AND progress >=0 and distance <=	237	 and distance >=	55	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	55	 AND progress >=0 and distance <=	238	 and distance >=	56	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	56	 AND progress >=0 and distance <=	239	 and distance >=	57	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	57	 AND progress >=0 and distance <=	240	 and distance >=	58	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	58	 AND progress >=0 and distance <=	241	 and distance >=	59	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	59	 AND progress >=0 and distance <=	242	 and distance >=	60	 then pay::float END),1)
										
										
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	41	 AND progress >=0 and distance <=	223	 and distance >=	41	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	42	 AND progress >=0 and distance <=	224	 and distance >=	42	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	43	 AND progress >=0 and distance <=	225	 and distance >=	43	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	44	 AND progress >=0 and distance <=	226	 and distance >=	44	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	45	 AND progress >=0 and distance <=	227	 and distance >=	45	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	46	 AND progress >=0 and distance <=	228	 and distance >=	46	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	47	 AND progress >=0 and distance <=	229	 and distance >=	47	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	48	 AND progress >=0 and distance <=	230	 and distance >=	48	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	49	 AND progress >=0 and distance <=	231	 and distance >=	49	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	50	 AND progress >=0 and distance <=	232	 and distance >=	50	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	51	 AND progress >=0 and distance <=	233	 and distance >=	51	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	52	 AND progress >=0 and distance <=	234	 and distance >=	52	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	53	 AND progress >=0 and distance <=	235	 and distance >=	53	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	54	 AND progress >=0 and distance <=	236	 and distance >=	54	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	55	 AND progress >=0 and distance <=	237	 and distance >=	55	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	56	 AND progress >=0 and distance <=	238	 and distance >=	56	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	57	 AND progress >=0 and distance <=	239	 and distance >=	57	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	58	 AND progress >=0 and distance <=	240	 and distance >=	58	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	59	 AND progress >=0 and distance <=	241	 and distance >=	59	 then pay::float END),1)
										
										
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	60	 AND progress >=0 and distance <=	243	 and distance >=	61	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	61	 AND progress >=0 and distance <=	244	 and distance >=	62	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	62	 AND progress >=0 and distance <=	245	 and distance >=	63	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	63	 AND progress >=0 and distance <=	246	 and distance >=	64	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	64	 AND progress >=0 and distance <=	247	 and distance >=	65	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	65	 AND progress >=0 and distance <=	248	 and distance >=	66	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	66	 AND progress >=0 and distance <=	249	 and distance >=	67	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	67	 AND progress >=0 and distance <=	250	 and distance >=	68	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	68	 AND progress >=0 and distance <=	251	 and distance >=	69	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	69	 AND progress >=0 and distance <=	252	 and distance >=	70	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	70	 AND progress >=0 and distance <=	253	 and distance >=	71	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	71	 AND progress >=0 and distance <=	254	 and distance >=	72	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	72	 AND progress >=0 and distance <=	255	 and distance >=	73	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	73	 AND progress >=0 and distance <=	256	 and distance >=	74	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	74	 AND progress >=0 and distance <=	257	 and distance >=	75	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	75	 AND progress >=0 and distance <=	258	 and distance >=	76	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	76	 AND progress >=0 and distance <=	259	 and distance >=	77	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	77	 AND progress >=0 and distance <=	260	 and distance >=	78	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	78	 AND progress >=0 and distance <=	261	 and distance >=	79	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	79	 AND progress >=0 and distance <=	262	 and distance >=	80	 then pay::float END),1)
										
										
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	60	 AND progress >=0 and distance <=	242	 and distance >=	60	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	61	 AND progress >=0 and distance <=	243	 and distance >=	61	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	62	 AND progress >=0 and distance <=	244	 and distance >=	62	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	63	 AND progress >=0 and distance <=	245	 and distance >=	63	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	64	 AND progress >=0 and distance <=	246	 and distance >=	64	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	65	 AND progress >=0 and distance <=	247	 and distance >=	65	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	66	 AND progress >=0 and distance <=	248	 and distance >=	66	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	67	 AND progress >=0 and distance <=	249	 and distance >=	67	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	68	 AND progress >=0 and distance <=	250	 and distance >=	68	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	69	 AND progress >=0 and distance <=	251	 and distance >=	69	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	70	 AND progress >=0 and distance <=	252	 and distance >=	70	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	71	 AND progress >=0 and distance <=	253	 and distance >=	71	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	72	 AND progress >=0 and distance <=	254	 and distance >=	72	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	73	 AND progress >=0 and distance <=	255	 and distance >=	73	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	74	 AND progress >=0 and distance <=	256	 and distance >=	74	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	75	 AND progress >=0 and distance <=	257	 and distance >=	75	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	76	 AND progress >=0 and distance <=	258	 and distance >=	76	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	77	 AND progress >=0 and distance <=	259	 and distance >=	77	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	78	 AND progress >=0 and distance <=	260	 and distance >=	78	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	79	 AND progress >=0 and distance <=	261	 and distance >=	79	 then pay::float END),1)
										
										
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	80	 AND progress >=0 and distance <=	263	 and distance >=	81	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	81	 AND progress >=0 and distance <=	264	 and distance >=	82	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	82	 AND progress >=0 and distance <=	265	 and distance >=	83	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	83	 AND progress >=0 and distance <=	266	 and distance >=	84	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	84	 AND progress >=0 and distance <=	267	 and distance >=	85	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	85	 AND progress >=0 and distance <=	268	 and distance >=	86	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	86	 AND progress >=0 and distance <=	269	 and distance >=	87	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	87	 AND progress >=0 and distance <=	270	 and distance >=	88	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	88	 AND progress >=0 and distance <=	271	 and distance >=	89	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	89	 AND progress >=0 and distance <=	272	 and distance >=	90	 then pay::float END),1)
									
										
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	80	 AND progress >=0 and distance <=	262	 and distance >=	80	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	81	 AND progress >=0 and distance <=	263	 and distance >=	81	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	82	 AND progress >=0 and distance <=	264	 and distance >=	82	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	83	 AND progress >=0 and distance <=	265	 and distance >=	83	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	84	 AND progress >=0 and distance <=	266	 and distance >=	84	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	85	 AND progress >=0 and distance <=	267	 and distance >=	85	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	86	 AND progress >=0 and distance <=	268	 and distance >=	86	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	87	 AND progress >=0 and distance <=	269	 and distance >=	87	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	88	 AND progress >=0 and distance <=	270	 and distance >=	88	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	89	 AND progress >=0 and distance <=	271	 and distance >=	89	 then pay::float END),1) as multiple,
				ROUND(SUM(CASE WHEN grade_28=1 and progress <= 	90	 AND progress >=0 and distance <=	272	 and distance >=	90	 then pay::float END),1) as divd
				FROM (						
					SELECT platform, payer_t.useridx, pay, progress, distance, nvl(grade_28, 0) as grade_28					
					FROM (					
						SELECT t1.useridx, t2.writedate, ROUND(facebookcredit::float/10::float, 2) AS pay, t1.createdate, platform, ceil(datediff(second, createdate, writedate)/86400::float) AS progress, ceil(datediff(second, createdate, dateadd(day, 1, Current_date))/86400::float) as distance	
						FROM (				
							SELECT useridx, createdate, platform			
							FROM t5_user			
							WHERE useridx > 20000 and createdate >= dateadd(day,(-543), Current_date)			
						) t1 JOIN t5_product_order t2 on t1.useridx = t2.useridx				
						WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 90				
						UNION ALL				
						SELECT t1.useridx, t2.writedate, ROUND(money, 2) AS pay, t1.createdate, platform, ceil(datediff(second, createdate, writedate)/86400::float) AS progress, ceil(datediff(second, createdate, dateadd(day, 1, Current_date))/86400::float) as distance				
						FROM (				
							SELECT useridx, createdate, platform			
							FROM t5_user			
							WHERE useridx > 20000 and createdate >= dateadd(day,(-543), Current_date)			
						) t1 JOIN t5_product_order_mobile t2 on t1.useridx = t2.useridx				
						WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 90		
					) AS payer_t left outer join  (					
						SELECT useridx, 1 AS grade_28				
						FROM (				
							SELECT t1.useridx, t2.writedate, ROUND(facebookcredit::float/10::float, 2) AS money, t1.createdate, platform			
							FROM (			
								SELECT useridx, createdate, platform		
								FROM t5_user		
								WHERE useridx > 20000 and createdate >= dateadd(day,(-543), Current_date)		
							) t1 JOIN t5_product_order t2 on t1.useridx = t2.useridx			
							WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 28			
							UNION ALL			
							SELECT t1.useridx, t2.writedate, ROUND(money, 2) AS pay, t1.createdate, platform			
							FROM (			
								SELECT useridx, createdate, platform		
								FROM t5_user		
								WHERE useridx > 20000 and createdate >= dateadd(day,(-543), Current_date)		
							) t1 JOIN t5_product_order_mobile t2 on t1.useridx = t2.useridx			
							WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 28			
						) AS payer_t				
						GROUP BY useridx				
					) pay14_t on payer_t.useridx = pay14_t.useridx					
				) total						
				GROUP BY platform						
				ORDER BY platform";
		$non_payer_roi_factor_list = $db_redshift->gettotallist($sql);
		
		$insert_sql1 = "";
		$insert_sql2 = "";
		
		for($i=0; $i<sizeof($non_payer_roi_factor_list); $i++)
		{
			$platform = $non_payer_roi_factor_list[$i]["platform"];
			$multiple = $non_payer_roi_factor_list[$i]["multiple"];
			$divd = $non_payer_roi_factor_list[$i]["divd"];
				
			if($insert_sql1 == "")
			{
				$insert_sql1 = "INSERT INTO tbl_roi_factor VALUES('$today', 0, 3, 0, $platform, 0, $multiple, $divd)";
				$insert_sql2 = "INSERT INTO tbl_roi_factor VALUES('$today', 1, 3, 0, $platform, 0, $multiple, $divd)";
			}
			else
			{
				$insert_sql1 .= ",('$today', 0, 3, 0, $platform, 0, $multiple, $divd)";
				$insert_sql2 .= ",('$today', 1, 3, 0, $platform, 0, $multiple, $divd)";
			}
		}
		
		if($insert_sql1 != "")
		{
			$insert_sql1 .= "ON DUPLICATE KEY UPDATE multiple = VALUES(multiple), divd = VALUES(divd) ";
			$insert_sql2 .= "ON DUPLICATE KEY UPDATE multiple = VALUES(multiple), divd = VALUES(divd) ";
 			$db_other->execute($insert_sql1);
 			$db_other->execute($insert_sql2);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_other->end();
	$db_redshift->end();
?>
