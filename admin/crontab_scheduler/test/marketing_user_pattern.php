<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	// 전체 가입자 대상 (2017.01.16 ~ 2017.02.12)
// 	$filename = "ios_playcount_moneyin.xls";

// 	$db_redshift = new CDatabase_Redshift();

// 	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
// 						"<table border=1>".
// 						"<tr>".
// 						"<td style='font-weight:bold;'>today</td>".
// 						"<td style='font-weight:bold;'>game_today</td>".
// 						"<td style='font-weight:bold;'>playcount</td>".
// 						"<td style='font-weight:bold;'>money_in</td>".
// 						"<td style='font-weight:bold;'>cnt_user</td>".
// 						"</tr>";
// 	$today = '2017-04-10';	
// 	$end_today = '2017-05-08';

// 	while($today < $end_today)
// 	{
// 		// join 테이블, platform 조건만 바꿔주시면 됩니다.
// 		$sql = "select to_char(createdate, 'YYYY-MM-DD') AS today, to_char(writedate, 'YYYY-MM-DD') AS game_today, sum(playcount) as playcount, sum(money_in) as moneyin, count(distinct t1.useridx) as cnt_user
// 				from t5_user t1 left join t5_user_gamelog_ios t2 on t1.useridx = t2.useridx
// 				where t1.useridx > 20000
// 				and platform = 1
// 				and createdate between '$today 00:00:00' and '$today 23:59:59'
// 				and writedate between '$today 00:00:00' and '2017-05-14 23:59:59'
// 				group by today, game_today
// 				order by today asc, game_today asc";
// 		$contents_list = $db_redshift->gettotallist($sql);

// 		for ($j=0; $j<sizeof($contents_list); $j++)
// 		{
// 			$today = $contents_list[$j]["today"];
// 			$game_today = $contents_list[$j]["game_today"];
// 			$playcount = $contents_list[$j]["playcount"];
// 			$money_in = $contents_list[$j]["moneyin"];
// 			$cnt_user = $contents_list[$j]["cnt_user"];


// 			$excel_contents .= "<tr>".
// 					"<td>".$today."</td>".
// 					"<td>".$game_today."</td>".
// 					"<td>".number_format($playcount)."</td>".
// 					"<td>".number_format($money_in)."</td>".
// 					"<td>".number_format($cnt_user)."</td>".
// 					"</tr>";
// 		}

// 		$today = date("Y-m-d", strtotime($today."1 day"));

// 		$excel_contents .= "<tr>".
// 				"<td>-------------</td>".
// 				"</tr>";
// 	}

// 	$db_redshift->end();

// 	$excel_contents .= "</table>";

// 	Header("Content-type: application/x-msdownload");
// 	Header("Content-type: application/x-msexcel");
// 	Header("Content-Disposition: attachment; filename=$filename");

// 	echo($excel_contents);
	
	//==============================================================================================================================
	
// 	$filename = "android_playcount_moneyin.xls";
	
// 	$db_redshift = new CDatabase_Redshift();
	
// 	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
// 						"<table border=1>".
// 						"<tr>".
// 						"<td style='font-weight:bold;'>today</td>".
// 						"<td style='font-weight:bold;'>game_today</td>".
// 						"<td style='font-weight:bold;'>playcount</td>".
// 						"<td style='font-weight:bold;'>money_in</td>".
// 						"<td style='font-weight:bold;'>cnt_user</td>".
// 						"</tr>";
// 	$today = '2017-02-27';
	
// 	for($i=0; $i<14; $i++)
// 	{
// 		// join 테이블, platform 조건만 바꿔주시면 됩니다.
// 		$sql = "select to_char(createdate, 'YYYY-MM-DD') AS today, to_char(writedate, 'YYYY-MM-DD') AS game_today, sum(playcount) as playcount, sum(money_in) as moneyin, count(distinct t1.useridx) as cnt_user
// 				from t5_user t1 left join t5_user_gamelog_android t2 on t1.useridx = t2.useridx
// 				where t1.useridx > 20000
// 				and platform = 2
// 				and createdate between '$today 00:00:00' and '$today 23:59:59'
// 				and writedate between '$today 00:00:00' and dateadd(d, 30, '$today 23:59:59')
// 				group by today, game_today
// 				order by today asc, game_today asc";
// 		$contents_list = $db_redshift->gettotallist($sql);
		
// 		for ($j=0; $j<sizeof($contents_list); $j++)
// 		{
// 			$today = $contents_list[$j]["today"];
// 			$game_today = $contents_list[$j]["game_today"];
// 			$playcount = $contents_list[$j]["playcount"];
// 			$money_in = $contents_list[$j]["moneyin"];
// 			$cnt_user = $contents_list[$j]["cnt_user"];
			
			
// 			$excel_contents .= "<tr>".
// 					"<td>".$today."</td>".
// 					"<td>".$game_today."</td>".
// 					"<td>".number_format($playcount)."</td>".
// 					"<td>".number_format($money_in)."</td>".
// 					"<td>".number_format($cnt_user)."</td>".
// 					"</tr>";
// 		}
		
// 		$today = date("Y-m-d", strtotime($today."1 day"));
		
// 		$excel_contents .= "<tr>".
// 				"<td>-------------</td>".
// 				"</tr>";
// 	}
	
// 	$db_redshift->end();
	
// 	$excel_contents .= "</table>";
	
// 	Header("Content-type: application/x-msdownload");
// 	Header("Content-type: application/x-msexcel");
// 	Header("Content-Disposition: attachment; filename=$filename");
	
// 	echo($excel_contents);
	
	//==============================================================================================================================
	
	// 결제자 대상 (2017.01.16 ~ 2017.02.12)
	
// 	$filename = "web_payer_playcount_moneyin.xls";

// 	$db_redshift = new CDatabase_Redshift();

// 	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
// 			"<table border=1>".
// 			"<tr>".
// 			"<td style='font-weight:bold;'>today</td>".
// 			"<td style='font-weight:bold;'>game_today</td>".
// 			"<td style='font-weight:bold;'>playcount</td>".
// 			"<td style='font-weight:bold;'>money_in</td>".
// 			"<td style='font-weight:bold;'>cnt_user</td>".
// 			"</tr>";
// 	$today = '2017-02-27';

// 	for($i=0; $i<14; $i++)
// 		{
// 			$sql = "select '$today' AS today, to_char(writedate, 'YYYY-MM-DD') AS game_today, sum(playcount) as playcount, sum(money_in) as money_in, count(distinct useridx) as cnt_user
// 					from t5_user_gamelog
// 					where useridx > 20000
// 					and useridx in (
// 						select useridx
// 					    from
// 					    (
// 							select useridx, (select count(*) from t5_product_order where status = 1 and useridx = t1.useridx and writedate < min(t1.writedate)) as before_cnt
// 							from
// 							(
// 						        SELECT useridx, facebookcredit, writedate
// 						        FROM t5_product_order
// 						        WHERE status = 1						        
// 						        AND '$today 00:00:00' <= writedate and writedate < '$today 23:59:59'
// 					      	) t1 group by useridx
// 						) t2 where before_cnt = 0
// 					)
// 					and writedate between '$today 00:00:00' and dateadd(d, 30, '$today 23:59:59')
// 					group by game_today
// 					order by game_today asc";
// 			$contents_list = $db_redshift->gettotallist($sql);

// 			for ($j=0; $j<sizeof($contents_list); $j++)
// 				{
// 					$today = $contents_list[$j]["today"];
// 					$game_today = $contents_list[$j]["game_today"];
// 					$playcount = $contents_list[$j]["playcount"];
// 					$money_in = $contents_list[$j]["money_in"];
// 					$cnt_user = $contents_list[$j]["cnt_user"];

// 					$excel_contents .= "<tr>".
// 							"<td>".$today."</td>".
// 							"<td>".$game_today."</td>".
// 							"<td>".number_format($playcount)."</td>".
// 							"<td>".number_format($money_in)."</td>".
// 							"<td>".number_format($cnt_user)."</td>".
// 							"</tr>";
// 				}

// 			$today = date("Y-m-d", strtotime($today."1 day"));

// 			$excel_contents .= "<tr>".
// 					"<td>-------------</td>".
// 					"</tr>";
// 		}

// 	$db_redshift->end();

// 	$excel_contents .= "</table>";

// 	Header("Content-type: application/x-msdownload");
// 	Header("Content-type: application/x-msexcel");
// 	Header("Content-Disposition: attachment; filename=$filename");

// 	echo($excel_contents);
	
	// 	//==============================================================================================================================
	
// 	$filename = "android_payer_playcount_moneyin.xls";
	
// 	$db_redshift = new CDatabase_Redshift();
	
// 	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
// 			"<table border=1>".
// 			"<tr>".
// 			"<td style='font-weight:bold;'>today</td>".
// 			"<td style='font-weight:bold;'>game_today</td>".
// 			"<td style='font-weight:bold;'>playcount</td>".
// 			"<td style='font-weight:bold;'>money_in</td>".
// 			"<td style='font-weight:bold;'>cnt_user</td>".
// 			"</tr>";
	
// 	$today = '2017-04-10';	
// 	$end_today = '2017-05-08';

// 	while($today < $end_today)
// 	{
// 		$sql = "select '$today' AS today, to_char(writedate, 'YYYY-MM-DD') AS game_today, sum(playcount) as playcount, sum(money_in) as money_in, count(distinct useridx) as cnt_user
// 				from t5_user_gamelog_android
// 				where useridx > 20000
// 				and useridx in (
// 					select useridx
// 				    from
// 				    (
// 						select useridx, (select count(*) from t5_product_order_mobile where status = 1 and os_type = 2 and useridx = t1.useridx and writedate < min(t1.writedate)) as before_cnt
// 						from
// 						(
// 					        SELECT useridx, money, writedate
// 					        FROM t5_product_order_mobile
// 					        WHERE status = 1
// 					        AND os_type =2
// 					        AND '$today 00:00:00' <= writedate and writedate < '$today 23:59:59'
// 				      	) t1 group by useridx 
// 					) t2 where before_cnt = 0
// 				)
// 				and writedate between '$today 00:00:00' and '2017-05-14 23:59:59'
// 				group by game_today
// 				order by game_today asc";
// 		$contents_list = $db_redshift->gettotallist($sql);
	
// 		for ($j=0; $j<sizeof($contents_list); $j++)
// 		{
// 			$today = $contents_list[$j]["today"];
// 			$game_today = $contents_list[$j]["game_today"];
// 			$playcount = $contents_list[$j]["playcount"];
// 			$money_in = $contents_list[$j]["money_in"];
// 			$cnt_user = $contents_list[$j]["cnt_user"];
				
// 			$excel_contents .= "<tr>".
// 					"<td>".$today."</td>".
// 					"<td>".$game_today."</td>".
// 					"<td>".number_format($playcount)."</td>".
// 					"<td>".number_format($money_in)."</td>".
// 					"<td>".number_format($cnt_user)."</td>".
// 					"</tr>";
// 		}
	
// 		$today = date("Y-m-d", strtotime($today."1 day"));
	
// 		$excel_contents .= "<tr>".
// 				"<td>-------------</td>".
// 				"</tr>";
// 	}
	
// 	$db_redshift->end();
	
// 	$excel_contents .= "</table>";
	
// 	Header("Content-type: application/x-msdownload");
// 	Header("Content-type: application/x-msexcel");
// 	Header("Content-Disposition: attachment; filename=$filename");
	
// 	echo($excel_contents);

// 	//==============================================================================================================================
	
// 	// install

// 	$filename = "web_install.xls";

// 	$db_redshift = new CDatabase_Redshift();

// 	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
// 			"<table border=1>".
// 			"<tr>".
// 			"<td style='font-weight:bold;'>today</td>".
// 			"<td style='font-weight:bold;'>install</td>".
// 			"</tr>";

// 		$sql = "select to_char(createdate, 'YYYY-MM-DD') AS today, count(*) as install_cnt
// 				from t5_user
// 				where createdate between '2017-02-27 00:00:00' and '2017-03-12 23:59:59'
// 				and platform = 0 
// 				group by today
// 				order by today asc";
// 		$contents_list = $db_redshift->gettotallist($sql);

// 		for ($i=0; $i<sizeof($contents_list); $i++)
// 			{
// 				$today = $contents_list[$i]["today"];
// 				$install = $contents_list[$i]["install_cnt"];

// 				$excel_contents .= "<tr>".
// 						"<td>".$today."</td>".
// 						"<td>".number_format($install)."</td>".
// 						"</tr>";
// 			}

// 	$db_redshift->end();

// 	$excel_contents .= "</table>";

	Header("Content-type: application/x-msdownload");
	Header("Content-type: application/x-msexcel");
	Header("Content-Disposition: attachment; filename=$filename");

	echo($excel_contents);
	
	$filename = "android_install.xls";

	$db_redshift = new CDatabase_Redshift();

	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
			"<table border=1>".
			"<tr>".
			"<td style='font-weight:bold;'>today</td>".
			"<td style='font-weight:bold;'>install</td>".
			"</tr>";

		$sql = "select to_char(createdate, 'YYYY-MM-DD') AS today, count(*) as install_cnt
				from t5_user
				where createdate between '2017-04-10 00:00:00' and '2017-05-14 23:59:59'
				and platform = 2
				group by today
				order by today asc";
		$contents_list = $db_redshift->gettotallist($sql);

		for ($i=0; $i<sizeof($contents_list); $i++)
		{
			$today = $contents_list[$i]["today"];
			$install = $contents_list[$i]["install_cnt"];

			$excel_contents .= "<tr>".
					"<td>".$today."</td>".
					"<td>".number_format($install)."</td>".
					"</tr>";
		}

	$db_redshift->end();

	$excel_contents .= "</table>";

	Header("Content-type: application/x-msdownload");
	Header("Content-type: application/x-msexcel");
	Header("Content-Disposition: attachment; filename=$filename");

	echo($excel_contents);
	
// 	//==============================================================================================================================
	
// 	// 첫 결제자 수 (... order by가 안먹혀서.. 파일 열어서 수동으로 해주셔야 합니다..)
// 	$filename = "web_firstbuy_user_count.xls";

// 	$db_redshift = new CDatabase_Redshift();

// 	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
// 			"<table border=1>".
// 			"<tr>".
// 			"<td style='font-weight:bold;'>today</td>".
// 			"<td style='font-weight:bold;'>첫결제 유저수</td>".
// 			"</tr>";

// 	$sql = "select to_char(writedate, 'YYYY-MM-DD') AS today, COUNT(*) AS firstbuy_cnt
// 			from
// 			(
// 				select useridx, (select count(*) from t5_product_order where status = 1 and useridx = t1.useridx and writedate < min(t1.writedate)) as before_cnt, writedate
// 				from
// 				(
// 				    SELECT useridx, facebookcredit, writedate
// 				    FROM t5_product_order
// 				    WHERE status = 1					
// 				    AND '2017-02-27 00:00:00' <= writedate and writedate <= '2017-03-12 23:59:59'
// 				) t1
// 				group by useridx, writedate
// 				order by writedate asc
// 			) t2
// 			where before_cnt = 0
// 			group by today
// 			order by today asc";
// 	$contents_list = $db_redshift->gettotallist($sql);

// 	for ($i=0; $i<sizeof($contents_list); $i++)
// 		{
// 			$today = $contents_list[$i]["today"];
// 			$install = $contents_list[$i]["firstbuy_cnt"];

// 			$excel_contents .= "<tr>".
// 					"<td>".$today."</td>".
// 					"<td>".number_format($install)."</td>".
// 					"</tr>";
// 		}

// 	$db_redshift->end();

// 	$excel_contents .= "</table>";

// 	Header("Content-type: application/x-msdownload");
// 	Header("Content-type: application/x-msexcel");
// 	Header("Content-Disposition: attachment; filename=$filename");

// 	echo($excel_contents);
	
// 	$filename = "android_firstbuy_user_count.xls";
	
// 	$db_redshift = new CDatabase_Redshift();
	
// 	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
// 			"<table border=1>".
// 			"<tr>".
// 			"<td style='font-weight:bold;'>today</td>".
// 			"<td style='font-weight:bold;'>첫결제 유저수</td>".
// 			"</tr>";
	
// 	$sql = "select to_char(writedate, 'YYYY-MM-DD') AS today, COUNT(*) AS firstbuy_cnt
// 			from
// 			(
// 				select useridx, (select count(*) from t5_product_order_mobile where status = 1 and os_type = 2 and useridx = t1.useridx and writedate < min(t1.writedate)) as before_cnt, writedate
// 				from
// 				(
// 				    SELECT useridx, money, writedate
// 				    FROM t5_product_order_mobile
// 				    WHERE status = 1 
// 					AND os_type = 2
// 				    AND '2017-02-27 00:00:00' <= writedate and writedate <= '2017-03-26 23:59:59'
// 				) t1 
// 				group by useridx, writedate
// 				order by writedate asc
// 			) t2
// 			where before_cnt = 0
// 			group by today
// 			order by today asc";
// 	$contents_list = $db_redshift->gettotallist($sql);
	
// 	for ($i=0; $i<sizeof($contents_list); $i++)
// 	{
// 		$today = $contents_list[$i]["today"];
// 		$install = $contents_list[$i]["firstbuy_cnt"];
	
// 		$excel_contents .= "<tr>".
// 				"<td>".$today."</td>".
// 				"<td>".number_format($install)."</td>".
// 				"</tr>";
// 	}
	
// 	$db_redshift->end();
	
// 	$excel_contents .= "</table>";
	
// 	Header("Content-type: application/x-msdownload");
// 	Header("Content-type: application/x-msexcel");
// 	Header("Content-Disposition: attachment; filename=$filename");
	
// 	echo($excel_contents);

?>