<?
	include("../../common/common_include.inc.php");
	include("../../common/db_util_redshift.inc.php");
	include("../../common/db_util_take5_other.inc.php");
	
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	$db_take5_other = new CDatabase_Take5_Other();
	
	$db_other->execute("SET wait_timeout=21600");
	$db_take5_other->execute("SET wait_timeout=21600");
	
	ini_set("memory_limit", "-1");
	
	$today = date("Y-m-d", strtotime("-1 days"));
	
	try
	{
		$sql = "select count(*) from tbl_roi_factor";
		write_log($db_take5_other->getvalue($sql));
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_other->end();
	$db_redshift->end();
	$db_take5_other->end();
?>