<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$db_other = new CDatabase_Other();
	$db_main2 = new CDatabase_Main2();
	$db_redshift = new CDatabase_Redshift();
	$db_analysis = new CDatabase_Analysis();
	$group_arr = array(80,92,86,102,115,269,222);
	$group_cnt = 0;
	$group_num = 0;
	$sql = "SELECT useridx, productidx,groupidx FROM 
			(
				SELECT useridx, productidx,groupidx FROM `tbl_nopayer_user_0` WHERE productidx > 0 AND groupset_date = DATE_FORMAT(NOW(), '%Y-%m-%d') AND groupidx = 13
				UNION ALL 
				SELECT useridx, productidx,groupidx  FROM `tbl_nopayer_user_1` WHERE productidx > 0 AND groupset_date = DATE_FORMAT(NOW(), '%Y-%m-%d') AND groupidx = 13
				UNION ALL
				SELECT useridx, productidx,groupidx  FROM `tbl_nopayer_user_2` WHERE productidx > 0 AND groupset_date = DATE_FORMAT(NOW(), '%Y-%m-%d') AND groupidx = 13
				UNION ALL 
				SELECT useridx, productidx,groupidx  FROM `tbl_nopayer_user_3` WHERE productidx > 0 AND groupset_date = DATE_FORMAT(NOW(), '%Y-%m-%d') AND groupidx = 13
				UNION ALL
				SELECT useridx, productidx,groupidx  FROM `tbl_nopayer_user_4` WHERE productidx > 0 AND groupset_date = DATE_FORMAT(NOW(), '%Y-%m-%d') AND groupidx = 13
				UNION ALL
				SELECT useridx, productidx,groupidx  FROM `tbl_nopayer_user_5` WHERE productidx > 0 AND groupset_date = DATE_FORMAT(NOW(), '%Y-%m-%d') AND groupidx = 13
				UNION ALL
				SELECT useridx, productidx,groupidx  FROM `tbl_nopayer_user_6` WHERE productidx > 0 AND groupset_date = DATE_FORMAT(NOW(), '%Y-%m-%d') AND groupidx = 13
				UNION ALL
				SELECT useridx, productidx,groupidx  FROM `tbl_nopayer_user_7` WHERE productidx > 0 AND groupset_date = DATE_FORMAT(NOW(), '%Y-%m-%d') AND groupidx = 13
				UNION ALL
				SELECT useridx, productidx,groupidx  FROM `tbl_nopayer_user_8` WHERE productidx > 0 AND groupset_date = DATE_FORMAT(NOW(), '%Y-%m-%d') AND groupidx = 13
				UNION ALL
				SELECT useridx, productidx,groupidx  FROM `tbl_nopayer_user_9` WHERE productidx > 0  AND groupset_date = DATE_FORMAT(NOW(), '%Y-%m-%d') AND groupidx = 13
			)
			t1
			WHERE useridx NOT IN (SELECT useridx FROM tbl_nopayer_product ) ";
	$userlist = $db_main2->gettotallist($sql);
	$brfore_enddate="";
	echo sizeof($userlist);
	echo "<br/>";
	for ($k = 0; $k<sizeof($userlist);$k++)
	{	
		if($group_cnt == $group_arr[$group_num])
		{
			$group_num = $group_num+1;
			$group_cnt = 0;
		}
		
		echo "group_arr ----";
		echo $group_arr[$group_num];
		echo "<br/>";
		echo "group_num ----";		
		echo $group_num;
		echo "<br/>";
		echo "group_cnt ----";		
		echo $group_cnt;
		echo "<br/>";
		$useridx = $userlist[$k]["useridx"];
		$productidx = $userlist[$k]["productidx"];
		$groupidx = $userlist[$k]["groupidx"];
		
		$sql = "SELECT * FROM tbl_nopayer_product_setting WHERE nopayer_productidx IN ($productidx)";
		$product_setting_info_arr = $db_main2->gettotallist($sql);
		
		//상품 선별
		for($p=1;$p<sizeof($product_setting_info_arr);$p++)
		{
			$nowdate = time() - 60 * 60 * 24 * ($group_num+1);
			$productidx = $product_setting_info_arr[$p]["nopayer_productidx"];
			$image_idx = $product_setting_info_arr[$p]["image_idx"];
			$image1_path = $product_setting_info_arr[$p]["image1_path"];
			$image2_path = $product_setting_info_arr[$p]["image2_path"];
			$image3_path = $product_setting_info_arr[$p]["image3_path"];
			$image4_path = $product_setting_info_arr[$p]["image4_path"];
			$image5_path = $product_setting_info_arr[$p]["image5_path"];
			$image6_path = $product_setting_info_arr[$p]["image6_path"];
			$minibox_position = $product_setting_info_arr[$p]["minibox_position"];
			$minibox_imagepath = $product_setting_info_arr[$p]["minibox_imagepath"];
			$limit_second = $product_setting_info_arr[$p]["limit_second"];
			$reoffer_second = $product_setting_info_arr[$p]["reoffer_second"];
			$popup_type = $product_setting_info_arr[$p]["popup_type"];
			
			$product1_key = $product_setting_info_arr[$p]["product1_key"];
			$product1_basemoney = $product_setting_info_arr[$p]["product1_basemoney"];
			$product1_basecoin = $product_setting_info_arr[$p]["product1_basecoin"];
			$product1_type = $product_setting_info_arr[$p]["product1_type"];
			$product1_percent = $product_setting_info_arr[$p]["product1_percent"];
			$product1_money = $product_setting_info_arr[$p]["product1_money"];
			$product1_coin = $product_setting_info_arr[$p]["product1_coin"];
			
			$product2_key = $product_setting_info_arr[$p]["product2_key"];
			$product2_basemoney = $product_setting_info_arr[$p]["product2_basemoney"];
			$product2_basecoin = $product_setting_info_arr[$p]["product2_basecoin"];
			$product2_type = $product_setting_info_arr[$p]["product2_type"];
			$product2_percent = $product_setting_info_arr[$p]["product2_percent"];
			$product2_money = $product_setting_info_arr[$p]["product2_money"];
			$product2_coin = $product_setting_info_arr[$p]["product2_coin"];
			
			if($p == 0)
			{
				$startdate = date("Y-m-d H:i:s", $nowdate);
				$enddate = date("Y-m-d H:i:s", ($nowdate+$limit_second));
			}
			else 
			{
				$startdate = date("Y-m-d H:i:s", ($nowdate+(($limit_second*$p)+($reoffer_second*$p))));
				$enddate = date("Y-m-d H:i:s", (($nowdate+(($limit_second*$p)+($reoffer_second*$p)))+$limit_second));
			}
				
			$nopayer_product_sql = " INSERT INTO tbl_nopayer_product (useridx, groupidx, productidx, startdate, enddate) ".
									" VALUES($useridx, $groupidx, $productidx, '$startdate', '$enddate') ON DUPLICATE KEY UPDATE productidx = VALUES(productidx),startdate = VALUES(startdate), enddate= VALUES(enddate);";
			$db_main2->execute($nopayer_product_sql);
			}
		$group_cnt = $group_cnt+1;
	}
	
	$db_analysis->end();
	$db_other->end();
	$db_redshift->end();
	$db_main2->end();
?>