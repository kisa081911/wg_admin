<?
	include("../../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	
	try 
	{
		$idx_num = 9;
		
		$sql = "SELECT useridx FROM tbl_user_purchase_play_log_$idx_num GROUP BY useridx HAVING COUNT(*) > 1";
		$purchase_play_user_list = $db_main2->gettotallist($sql);
		
		for($i = 0; $i < sizeof($purchase_play_user_list); $i++)
		{
			$useridx = $purchase_play_user_list[$i]["useridx"];
			
			write_log($useridx);
			
			$sql = "SELECT COUNT(*) FROM `tbl_user_purchase_play_log_$idx_num` WHERE useridx = $useridx ORDER BY writedate asc";
			$update_data_cnt = $db_main2->getvalue($sql);
			
			for($j = 0; $j < ($update_data_cnt - 1); $j++)
			{
				if($j == 0)
				{
					$sql = "SELECT * FROM `tbl_user_purchase_play_log_$idx_num` WHERE useridx = $useridx ORDER BY writedate asc limit 2";
					$update_data_list = $db_main2->gettotallist($sql);
				}
				else 
				{
					$sql = "SELECT * FROM `tbl_user_purchase_play_log_$idx_num` WHERE useridx = $useridx AND logidx > $check_logidx ORDER BY writedate asc limit 2";
					$update_data_list = $db_main2->gettotallist($sql);
				}
				
				for($k = 0; $k < sizeof($update_data_list); $k++)
				{
					
					$writedate = $update_data_list[$k]["writedate"];
										
					if($k == 0)
					{
						$check_logidx = $update_data_list[$k]["logidx"];
						$writedate_1 = $writedate;
					}
					else
					{
						$update_logidx = $update_data_list[$k]["logidx"];
						$writedate_2 = $writedate;
					}
				}
				
				$sql = "SELECT MAX(writedate) as max_writedate, count(*) as cnt ".
						"FROM	".
						"(	".
						"	SELECT useridx, (facebookcredit/10) AS money, writedate FROM tbl_product_order WHERE useridx = $useridx AND (facebookcredit/10) >= 18 AND STATUS = 1 AND writedate >= '$writedate_1' AND writedate < DATE_SUB('$writedate_2', INTERVAL 1 SECOND)	".
						"	UNION ALL	".
						"	SELECT useridx, money, writedate FROM tbl_product_order_mobile WHERE useridx = $useridx AND STATUS = 1 AND money >= 18 AND writedate >= date_sub('$writedate_1', INTERVAL 1 MINUTE) AND writedate < date_sub('$writedate_2', INTERVAL 1 MINUTE)	".
						") tt ORDER BY writedate DESC";
				$update_info = $db_main->getarray($sql);
				
				$update_date = $update_info["max_writedate"];
				$update_cnt = $update_info["cnt"];
				
				if($update_cnt > 1)
				{
					$update_sql = "UPDATE tbl_user_purchase_play_log_$idx_num SET purchase_writedate = '$update_date' WHERE logidx = $update_logidx AND useridx = $useridx";
				}
				else
				{
					$update_sql = "UPDATE tbl_user_purchase_play_log_$idx_num SET purchase_writedate = '$writedate_1' WHERE logidx = $update_logidx AND useridx = $useridx";
				}	
				$db_main2->execute($update_sql);
				
			}			
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();
?>