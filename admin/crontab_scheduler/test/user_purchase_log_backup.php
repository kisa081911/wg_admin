<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$today = date("Y-m-d");
	
	//web
	try
	{
		$db_other = new CDatabase_Other();
		$db_other->execute("SET wait_timeout=3600");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_purchase_log'";
		$columns_count = $db_other->getvalue($sql);
		
		for($u=0; $u<10; $u++)
		{
			$sql = " SELECT * ".
					"	FROM (SELECT logidx, useridx, platform, money, user_coin, product_coin, base_coin, user_level, vip_level, product_type, is_coupon, ".
					"	more_rate, is_first, total_buy_amount, dayafterinstall, writedate, rebuydate FROM tbl_user_purchase_log ".
					"	WHERE writedate >= '2015-11-17 11:21:42' OR (rebuydate IS NOT NULL AND rebuydate >= '2015-11-17 11:21:42') ".
					"	) t1 ".
					"	WHERE t1.useridx % 10 = $u ";
			
			$user_list = $db_other->gettotallist($sql);
			
			for($i=0; $i<sizeof($user_list); $i++)
			{
			  $output = "";
			  
			  for($j=0; $j<$columns_count; $j++)
				{
					$user_list[$i][$j] = str_replace("\"", "\"\"", $user_list[$i][$j]);
					
					if($user_list[$i][$j] == "0000-00-00 00:00:00")
						$user_list[$i][$j] = "1900-01-01 00:00:00";
						
					if($j == $columns_count - 1)
						$output .= '"'.$user_list[$i][$j].'"';
					else
						$output .= '"'.$user_list[$i][$j].'"|';
				}
				$output .="\n";
				
				if($output != "")
				{
					$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_purchase_log_$today.txt", 'a+');
						
					fwrite($fp, $output);
						
					fclose($fp);
				}
		  }
		  
		}
		$db_other->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	 
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/t5/tbl_user_purchase_log/$today";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_purchase_log_$today.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_purchase_log_'.$today.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE t5_user_purchase_log_tmp (LIKE t5_user_purchase_log);";
		$db_redshift->execute($sql);
		
		// Load data into the staging table
		$sql = "copy t5_user_purchase_log_tmp ".
				"from 's3://wg-redshift/t5/tbl_user_purchase_log/$today' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
		
		// Update records
		$sql = "UPDATE t5_user_purchase_log ". 
				"SET rebuydate = s.rebuydate ".
				"FROM t5_user_purchase_log_tmp s ".
				"WHERE t5_user_purchase_log.logidx = s.logidx;";
		$db_redshift->execute($sql); 

		// Insert records
		$sql = "INSERT INTO t5_user_purchase_log ".
				"SELECT s.* FROM t5_user_purchase_log_tmp s LEFT JOIN t5_user_purchase_log ".
				"ON s.logidx = t5_user_purchase_log.logidx ".
				"WHERE t5_user_purchase_log.logidx IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE t5_user_purchase_log_tmp;";
		$db_redshift->execute($sql);

		$db_redshift->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>