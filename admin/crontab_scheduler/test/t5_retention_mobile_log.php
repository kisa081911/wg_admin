<?
	include("../../common/common_include.inc.php");

	$db_mobile = new CDatabase_Mobile();
	$db_analysis = new CDatabase_Analysis();
	$db_slave_main = new CDatabase_Slave_Main();
	$db_main2 = new CDatabase_Main2();
	$db_main = new CDatabase_Main();
	$db_other = new CDatabase_Other();
	
	$db_mobile->execute("SET wait_timeout=3600");	
	$db_analysis->execute("SET wait_timeout=3600");	
	$db_main2->execute("SET wait_timeout=3600");	
	$db_main->execute("SET wait_timeout=3600");	
	$db_slave_main->execute("SET wait_timeout=3600");	
	$db_other->execute("SET wait_timeout=3600");	
	
	$port = "";
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}	

	try
	{
		echo "----------------------------------- ios";
		echo "</br>";
		
		// ios 
		
		$sql ="SELECT * FROM tbl_user_retention_mobile_log d1
				JOIN (
					SELECT t1.useridx, LEFT(t2.installed_at, 10) AS installed_at, t2.network_name, t1.deviceid, t2.idfa,t2.campaign_name  FROM `tbl_user_mobile_adjust` t1
					JOIN `tbl_adjust_install` t2
					ON t2.idfa = t1.advid 
					AND t2.network_name LIKE '%Facebook%'
				    AND t2.campaign_name LIKE '%retention%'
					AND idfa != ''
					WHERE installed_at > '2017-12-01 00:00:00'
					)d2
				ON d1.useridx = d2.useridx
				AND DATE_FORMAT(d1.writedate, '%Y-%m-%d') = d2.installed_at
				WHERE adflag = ''
				AND leavedays > 0 "; 
		$adflag_list = $db_main2->gettotallist($sql);
		
		echo sizeof($adflag_list);
		
		for($d=0;$d<sizeof($adflag_list);$d++)
		{
		
			$useridx = $adflag_list[$d]["useridx"];
			$installed_at = $adflag_list[$d]["installed_at"];
			$network_name = $adflag_list[$d]["network_name"];
			$campaign_name_arr=explode('_',$adflag_list[$d]["campaign_name"]);
			$campaign_name = "m_".$campaign_name_arr[2];
			
				
			$sql = "UPDATE t5_user_retention_mobile_log SET adflag = '$campaign_name' WHERE useridx = $useridx AND TO_CHAR(writedate,'YYYY-MM-DD') = '$installed_at';";
			echo $sql;
			echo "</br>";
			
		}
		
		echo "</br>";
		echo "</br>";
		echo "</br>";
		
		
		for($d=0;$d<sizeof($adflag_list);$d++)
		{
		
			$useridx = $adflag_list[$d]["useridx"];
			$installed_at = $adflag_list[$d]["installed_at"];
			$network_name = $adflag_list[$d]["network_name"];
			
			$campaign_name_arr=explode('_',$adflag_list[$d]["campaign_name"]);
			$campaign_name = "m_".$campaign_name_arr[2];
			
			$sql = "UPDATE tbl_user_retention_mobile_log SET adflag = '$campaign_name' WHERE useridx = $useridx AND DATE_FORMAT(d1.writedate, '%Y-%m-%d') = '$installed_at';";
			echo $sql;
			echo "</br>";
			
		}
		
		echo "----------------------------------- android,amazon";
		echo "</br>";
		echo "</br>";
		echo "</br>";
		echo "</br>";
		echo "</br>";
		echo "</br>";
		echo "</br>";
		echo "</br>";
		
		
// 		// android, amazon
		$sql ="
				SELECT * FROM tbl_user_retention_mobile_log d1
				JOIN (
					SELECT t1.useridx, LEFT(t2.installed_at, 10) AS installed_at, t2.network_name, t1.deviceid, t2.gps_adid,t2.campaign_name  FROM `tbl_user_mobile_adjust` t1
					JOIN `tbl_adjust_install` t2
					ON t2.gps_adid = t1.advid 
					AND t2.network_name  LIKE '%Facebook%' 
					AND t2.campaign_name LIKE '%retention%'
					AND gps_adid != ''
					WHERE installed_at > '2017-12-01 00:00:00'
					)d2
				ON d1.useridx = d2.useridx
				AND DATE_FORMAT(d1.writedate, '%Y-%m-%d') = d2.installed_at
				WHERE adflag = ''
				AND leavedays > 0";
		$adflag_list = $db_main2->gettotallist($sql);
		
		echo sizeof($adflag_list);
		
		for($d=0;$d<sizeof($adflag_list);$d++)
		{
		
			$useridx = $adflag_list[$d]["useridx"];
			$installed_at = $adflag_list[$d]["installed_at"];
			$network_name = $adflag_list[$d]["network_name"];
			
			$campaign_name_arr=explode('_',$adflag_list[$d]["campaign_name"]);
			$campaign_name = "m_".$campaign_name_arr[2];
			
			$sql = "UPDATE t5_user_retention_mobile_log SET adflag = '$campaign_name' WHERE useridx = $useridx AND TO_CHAR(writedate,'YYYY-MM-DD') = '$installed_at';";
			echo $sql;
			echo "</br>";
				
		}
		
		
		echo "</br>";
		echo "</br>";
		echo "</br>";
		echo "</br>";
		
		
		for($d=0;$d<sizeof($adflag_list);$d++)
		{
		
			$useridx = $adflag_list[$d]["useridx"];
			$installed_at = $adflag_list[$d]["installed_at"];
			$network_name = $adflag_list[$d]["network_name"];
			
			$campaign_name_arr=explode('_',$adflag_list[$d]["campaign_name"]);
			$campaign_name = "m_".$campaign_name_arr[2];
				
			$sql = "UPDATE tbl_user_retention_mobile_log SET adflag = '$campaign_name' WHERE useridx = $useridx AND DATE_FORMAT(d1.writedate, '%Y-%m-%d') = '$installed_at';";
			echo $sql;
			echo "</br>";
				
		}
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	$db_other->end();	
	$db_main2->end();	
	$db_main->end();	
	$db_slave_main->end();	
	$db_mobile->end();	
	$db_analysis->end();	
?>
