<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/user/unlimited_coupon_target_user") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/user/unlimited_coupon_target_user") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("unlimited_coupon_target_user Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	ini_set("memory_limit", "-1");
	
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();
	
	$db_main2->execute("SET wait_timeout=3600");
	$db_other->execute("SET wait_timeout=3600");
	
	try
	{		
// 		$sql = "SELECT useridx
// 				FROM
// 				(
// 					SELECT useridx, SUM(pCount) recentPCount
// 					FROM
// 					(
// 						SELECT useridx, playcount + h_playcount AS pCount FROM tbl_user_playstat_daily WHERE today >= DATE_SUB(NOW(), INTERVAL 28 DAY) UNION ALL
// 						SELECT useridx, playcount + h_playcount AS pCount FROM tbl_user_playstat_daily_ios WHERE today >= DATE_SUB(NOW(), INTERVAL 28 DAY) UNION ALL
// 						SELECT useridx, playcount + h_playcount AS pCount FROM tbl_user_playstat_daily_android WHERE today >= DATE_SUB(NOW(), INTERVAL 28 DAY) UNION ALL
// 						SELECT useridx, playcount + h_playcount AS pCount FROM tbl_user_playstat_daily WHERE today >= DATE_SUB(NOW(), INTERVAL 28 DAY) 
// 					) AS pt1 
// 					GROUP BY useridx
// 				) AS pt2
// 				WHERE useridx IN
// 				(
// 					SELECT DISTINCT(dis_useridx)
// 					FROM
// 					(
// 						SELECT DISTINCT(t1.useridx) AS dis_useridx, SUM(pay_amount)
// 						FROM
// 						(
// 							SELECT useridx, DATE(writedate) d1 FROM `tbl_product_order_all` WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 180 DAY) GROUP BY useridx, DATE(writedate) 
// 						) t1 JOIN 
// 						(
// 							SELECT DATE(writedate) d2, useridx, SUM(money) pay_amount FROM `tbl_product_order_all` WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 236 DAY) GROUP BY d2, useridx 
// 						) t2
// 						ON t1.useridx = t2.useridx WHERE t1.d1 > t2.d2 AND t2.d2 >= DATE_SUB(t1.d1, INTERVAL 56 DAY) GROUP BY t1.d1, t1.useridx HAVING SUM(pay_amount) >= 500
// 					) tt
// 				)
// 				AND recentPCount < 8000;";

		
		$delete_sql = "TRUNCATE TABLE tbl_unlimited_coupon_target_user;";
		$db_main2->execute($delete_sql);
		
		sleep(5);
		
		
		$sql = "SELECT dis_useridx AS useridx
				FROM
				(
						SELECT DISTINCT(dis_useridx) AS dis_useridx
						FROM
						(
								SELECT DISTINCT(t1.useridx) AS dis_useridx, SUM(pay_amount)
								FROM
								(
										SELECT useridx, DATE(writedate) d1 FROM tbl_product_order_all WHERE useridx > 20000 AND STATUS = 1 GROUP BY useridx, d1
								) t1 JOIN
								(
										SELECT DATE(writedate) d2, useridx, SUM(money) pay_amount FROM `tbl_product_order_all` WHERE useridx > 20000 AND STATUS = 1 GROUP BY d2, useridx
								) t2
								ON t1.useridx = t2.useridx WHERE t1.d1 > t2.d2 AND t2.d2 >= DATE_SUB(t1.d1, INTERVAL 56 DAY) GROUP BY t1.d1, t1.useridx HAVING SUM(pay_amount) >= 300
						) tt
				) tt WHERE dis_useridx NOT IN
				(
						SELECT DISTINCT(useridx)
						FROM
						(
								SELECT useridx, SUM(pCount) recentPCount
								FROM
								(
										SELECT useridx, playcount + h_playcount AS pCount FROM tbl_user_playstat_daily WHERE today >= DATE_SUB(NOW(), INTERVAL 28 DAY) UNION ALL
										SELECT useridx, playcount + h_playcount AS pCount FROM tbl_user_playstat_daily_ios WHERE today >= DATE_SUB(NOW(), INTERVAL 28 DAY) UNION ALL
										SELECT useridx, playcount + h_playcount AS pCount FROM tbl_user_playstat_daily_android WHERE today >= DATE_SUB(NOW(), INTERVAL 28 DAY) UNION ALL
										SELECT useridx, playcount + h_playcount AS pCount FROM tbl_user_playstat_daily WHERE today >= DATE_SUB(NOW(), INTERVAL 28 DAY)
								) AS pt1
								GROUP BY useridx HAVING SUM(pCount) >= 8000
						) pp
				)";
		$user_list = $db_other->gettotallist($sql);
		
		$insert_cnt = 0;
		$insert_sql = "";		
		
		for($i=0; $i<sizeof($user_list); $i++)
		{
			$useridx = $user_list[$i]['useridx'];
			
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_unlimited_coupon_target_user(useridx, writedate) VALUES($useridx, NOW())";
				$insert_cnt++;
			}
			else if($insert_cnt < 500)
			{
				$insert_sql .= ",($useridx, NOW())";
				$insert_cnt++;
			}
			else 
			{
				$insert_sql .= ",($useridx, NOW())";
				$insert_sql .= " ON DUPLICATE KEY UPDATE writedate = VALUES(writedate)";
				
				$db_main2->execute($insert_sql);
				
				$insertcount = 0;
				$insert_sql = "";
			}
		}
		
		
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE writedate = VALUES(writedate)";
			
			$db_main2->execute($insert_sql);
			
			$insertcount = 0;
			$insert_sql = "";
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
	$db_other->end();
?>
