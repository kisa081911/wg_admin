<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/user/whaleuser_leave_check") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/user/whaleuser_leave_check") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("whaleuser_leave_check Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	$db_slave_main = new CDatabase_Slave_Main();
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();
	
	try
	{
		$today = date("Y-m-d", time());
		$yesterday = date("Y-m-d", time() - 60 * 60 * 24);
		
		$sql = "SELECT COUNT(*) FROM tbl_revisit_stats WHERE today = '$yesterday'";
		$is_data_update_check = $db_other->getvalue($sql);
		
		if($is_data_update_check > 0)
		{
			// 웨일 유저 이탈자 업데이트 체크
			$sql = "SELECT COUNT(*) FROM tbl_whale_user_leave ".
					"WHERE '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59'";
			$is_update = $db_main2->getvalue($sql);
			
			if($is_update == 0)
			{
				// 최근 28일 기준 플레이 횟수 14회 이상, 가입한지 28일이 되지 않는 경우는 환산하여 처리, $5이상 구매 횟수가 10회 이상
				$sql = "SELECT useridx, SUM(IF(purchaseamount >= 50, 1, 0)) AS buy_count, COUNT(statidx) AS dayplaycount, MAX(days_after_install) AS days_after_install ".
						"FROM tbl_user_playstat_daily ".
						"WHERE today >= DATE_SUB(NOW(), INTERVAL 4 WEEK) ".
						"GROUP BY useridx ".
						"HAVING (dayplaycount >= 14 OR ROUND(dayplaycount/days_after_install, 2) >= 0.5) AND buy_count >= 10";
			
				$user_list = $db_other->gettotallist($sql);
			
				for($i=0; $i<sizeof($user_list); $i++)
				{
					$useridx = $user_list[$i]["useridx"];
						
					// 최근 3일동안 구매하지 않는 경우
					$sql = "SELECT COUNT(orderidx) AS recent_buy_count ".
							"FROM tbl_product_order ".
							"WHERE useridx = $useridx AND status = 1 AND facebookcredit >= 50 AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY)";
						
					$recent_buy_count = $db_slave_main->getvalue($sql);
						
					if($recent_buy_count == 0)
					{
						// 누적 결제 액 $99이상
						$sql = "SELECT SUM(facebookcredit)/10 ".
								"FROM tbl_product_order ".
								"WHERE useridx = $useridx AND status = 1";
						$buy_amount = $db_slave_main->getvalue($sql);
			
						if($buy_amount >= 99)
						{
							// 최근 2번의 보유코인이 평균 구매 코인의 15%미만인 경우
							$sql = "SELECT ROUND(AVG(coin)/15) AS buy_coin ".
									"FROM tbl_product_order ".
									"WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50";
								
							$avg_buy_coin = $db_slave_main->getvalue($sql);
								
							$sql = "SELECT COUNT(*) ".
									"FROM ( ".
									"	SELECT currentcoin ".
									"	FROM tbl_user_playstat_daily ".
									"	WHERE useridx = $useridx AND today >= DATE_SUB(NOW(), INTERVAL 4 WEEK) ".
									"	ORDER BY statidx DESC ".
									"	LIMIT 2 ".
									") t1 ".
									"WHERE currentcoin < $avg_buy_coin";
								
							$coin_under_count = $db_other->getvalue($sql);
								
							// 최근 2번의 일일 게임 기록 합산 결과가 750스핀 이상이면서 승률이 85%미만인 경우							
							$sql = "SELECT COUNT(*) ".
									"FROM ( ".
									"	SELECT SUM(playcount + h_playcount) AS sum_playcount, ROUND(SUM(moneyout+h_moneyout)/SUM(moneyin+h_moneyin)*100, 2) AS winrate ".
									"	FROM ".
									"	( ".
									"		SELECT playcount, h_playcount, moneyout, h_moneyout, moneyin, h_moneyin ".
									"		FROM tbl_user_playstat_daily ".
									"		WHERE useridx = $useridx AND today >= DATE_SUB(NOW(), INTERVAL 4 WEEK) ".
									"		ORDER BY statidx DESC ".
									"		LIMIT 2 ".
									"	) t2 ".
									") t1 ". 
									"WHERE sum_playcount >= 750 AND winrate < 85";
								
							$winrate_under_count = $db_other->getvalue($sql);
								
							if($coin_under_count == 2 || $winrate_under_count == 1)
							{
								// 웨일급 이탈감지
								$sql = "INSERT INTO tbl_whale_user_leave(useridx, writedate) VALUES($useridx, NOW()) ON DUPLICATE KEY UPDATE writedate=VALUES(writedate);";
								$db_main2->execute($sql);
									
								$sql = "INSERT INTO tbl_whale_user_leave_detect_log(useridx, writedate) VALUES($useridx, NOW());";
								$db_other->execute($sql);
							}
						}
					}
				}
					
				$sql = "DELETE FROM tbl_whale_user_leave WHERE writedate < '$today 00:00:00'";
				$db_main2->execute($sql);
			}
			
			// 웨일 유저 가능성 판단체크
// 			$sql = "SELECT COUNT(*) FROM tbl_whale_user_offer ".
// 					"WHERE '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59'";
// 			$is_offer_update = $db_main2->getvalue($sql);
			
// 			if($is_offer_update == 0)
// 			{
// 				$sql = "SELECT useridx, (CASE WHEN std_value < 4 THEN 5 WHEN std_value < 8 THEN 9 WHEN std_value < 12 THEN 19 WHEN std_value < 17 THEN 39 ELSE 59 END) AS offer ".
// 						"FROM ( ".
// 						"	SELECT a.useridx, ROUND(COUNT(DISTINCT(a.moneyin/a.playcount)) + MAX(a.moneyin/a.playcount)/MIN(a.moneyin/a.playcount) + MAX(a.moneyin/a.playcount)/AVG(a.moneyin/a.playcount)) AS std_value ".
// 						"	FROM tbl_user_gamelog_3h a JOIN tbl_user_playstat_daily b ON a.useridx = b.useridx ".
// 						"	WHERE a.playcount >= 10 AND a.coin >= 5000000 AND a.coin < 50000000 AND b.purchaseamount = 0 AND a.writedate >= DATE_SUB(NOW(), INTERVAL 4 WEEk) ".
// 						"	GROUP BY a.useridx ".
// 						") t1";
					
// 				$offer_list = $db_other->gettotallist($sql);
					
// 				$sql = "";
// 				$insertcount = 0;
				
// 				for($i=0; $i<sizeof($offer_list); $i++)
// 				{
// 					$useridx = $offer_list[$i]["useridx"];
// 					$offer = $offer_list[$i]["offer"];
			
// 					if ($insertcount == 0)
// 					{
// 						$sql = "INSERT INTO tbl_whale_user_offer(useridx, offer, writedate) VALUES($useridx, $offer, NOW())";
						
// 						$insertcount++;
// 					}
// 					else if ($insertcount < 100)
// 					{
// 						$sql .= ", ($useridx, $offer, NOW())";
// 						$insertcount++;
// 					}
// 					else
// 					{
// 						$sql .= " ON DUPLICATE KEY UPDATE offer=VALUES(offer), writedate=VALUES(writedate);";
// 						$db_main2->execute($sql);
// 						$insertcount = 0;
// 						$sql = "";
// 					}
// 				}
				
// 				if($sql != "")
// 				{
// 					$sql .= " ON DUPLICATE KEY UPDATE offer=VALUES(offer), writedate=VALUES(writedate);";
// 					$db_main2->execute($sql);
// 				}
				
// 				$sql = "DELETE FROM tbl_whale_user_offer WHERE writedate < '$today 00:00:00'";
// 				$db_main2->execute($sql);
//			}			
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_slave_main->end();
	$db_main2->end();
	$db_other->end();
?>