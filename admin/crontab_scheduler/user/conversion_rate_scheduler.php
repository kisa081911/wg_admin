<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/user_static/conversion_rate_scheduler") !== false)
		{
			$count++;
		}
	}
	
	ini_set("memory_limit", "-1");

	$issuccess = "1";
	
	if ($count > 1)
		exit();
	
	$db_slave_main = new CDatabase_Slave_Main();
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	$db_slave_main->execute("SET wait_timeout=72000");
	$db_other->execute("SET wait_timeout=72000");

	// Web -> Mobile 전환율 
	try
	{
		$sdate = "2019-01-02";
		$edate = date("Y-m-d");
		
		while($sdate < $edate)
		{
			$startdate = $sdate;
			$enddate = date('Y-m-d', strtotime($sdate.' + 7 day'));
			
			$sql = "select date_part(year,'2019-01-02') as create_year, create_week, count(DISTINCT useridx) as total_count ,  SUM(case when mobile_version != '' then 1 else 0 end) as mobile_user_cnt
					from 
					(
					  select useridx, createdate, date_part(w, createdate) as create_week, mobile_version
					  from t5_user where useridx > 20000 and createdate >= '$startdate 00:00:00' and createdate < '$enddate 00:00:00' and platform = 0
					 ) 
					 group by create_week
					 order by create_week asc";
		
			$conversion_list = $db_redshift->gettotallist($sql);
		
			for($i=0; $i<sizeof($conversion_list); $i++)
			{
				$create_year = $conversion_list[$i]["create_year"];
				$create_week = $conversion_list[$i]["create_week"];
				$total_count = $conversion_list[$i]["total_count"];
				$mobile_user_cnt = $conversion_list[$i]["mobile_user_cnt"];
		
				$sql = "INSERT INTO tbl_user_conversion_daily(year, week, join_count, conversion_count) ".
						"VALUES($create_year,$create_week, $total_count, $mobile_user_cnt) ON DUPLICATE KEY UPDATE join_count = $total_count, conversion_count=$mobile_user_cnt";

				$db_other->execute($sql);
			}
	
			$sdate = $enddate;
		}
		
// 		$sdate = "2017-09-04";
// 		$edate = date("Y-m-d");
		
// 		$sql = "select t3.week, join_count, conversion_count
// 				from (
// 				  select date_part(w, t1.createdate) as week, count(*) as conversion_count
// 				  from (
// 				    select useridx, createdate from duc_user where useridx > 20000 and createdate >= '$sdate 00:00:00' and createdate < '$edate 00:00:00' and platform = 0 
// 				   ) t1 left join duc_user_mobile t2 on t1.useridx = t2.useridx
// 				  where t2.createdate > t1.createdate
// 				  group by date_part(w, t1.createdate)
// 				) t3 join (
// 				    select date_part(w, createdate) as week, count(*) as join_count from duc_user where useridx > 20000 and createdate >= '$sdate 00:00:00' and createdate < '$edate 00:00:00' and platform = 0 group by date_part(w, createdate)
// 				) t4 on t3.week = t4.week ";
		
// 		$conversion_list = $db_redshift->gettotallist($sql);
		
// 		for($i=0; $i<sizeof($conversion_list); $i++)
// 		{
// 			$week = $conversion_list[$i]["week"];
// 			$join_count = $conversion_list[$i]["join_count"];
// 			$conversion_count = $conversion_list[$i]["conversion_count"];
		
// 			$sql = "INSERT INTO tbl_user_conversion_daily(week, join_count, conversion_count) ".
// 					"VALUES($week, $join_count, $conversion_count) ON DUPLICATE KEY UPDATE join_count = $join_count, conversion_count=$conversion_count";
		
// 			$db_other->execute($sql);
// 		}
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	$db_slave_main->end();
	$db_other->end();
	$db_redshift->end();
?>