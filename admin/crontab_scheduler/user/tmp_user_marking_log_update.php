<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");

	$db_main2 = new CDatabase_Main2();	
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	$db_main2->execute("SET wait_timeout=3600");
	$db_other->execute("SET wait_timeout=3600");
	
	try
	{
		$sdate = "2019-05-13";
		$edate = "2019-05-14";
		
		while($sdate < $edate)
		{
			$yesterday = $sdate;
		
			write_log("$yesterday user marking log");
			
			$sql = "TRUNCATE TABLE t5_update_marking_log_user_list;";
			$db_redshift->execute($sql);
				
			sleep(1);
				
			$sql = "SELECT logidx, useridx, markingidx, logindate FROM tbl_user_marking_log WHERE today='$yesterday'";
			write_log($sql);
			$update_user_list = $db_main2->gettotallist($sql);
				
			$insert_sql = "";
			$insert_count = 0;
				
			for($i=0; $i<sizeof($update_user_list); $i++)
			{
				$logidx = $update_user_list[$i]["logidx"];
				$useridx = $update_user_list[$i]["useridx"];
				$markingidx	= $update_user_list[$i]["markingidx"];
				$logindate = $update_user_list[$i]["logindate"];
			
				if($insert_sql == "")
					$insert_sql = "INSERT INTO t5_update_marking_log_user_list VALUES($logidx, $useridx, $markingidx, '$logindate')";
				else
					$insert_sql .= ", ($logidx, $useridx, $markingidx, '$logindate')";
					
				$insert_count++;
					
				if($insert_count >= 1000)
				{
					$db_redshift->execute($insert_sql);
					$insert_sql = "";
					$insert_count = 0;
				}
			}
				
			if($insert_sql != "")
			{
				$db_redshift->execute($insert_sql);
				$insert_sql = "";
				$insert_count = 0;
			}
			
			sleep(1);
			
			$insert_sql = "";
			$insert_count = 0;
			
			$sql = "select tt.logidx, tt.useridx, tt.markingidx, tt.logindate,  count(distinct today) as d28_play_days, SUM(money_in) as d28_money_in, sum(playcount) as d28_playcount
					from
					(
	  					select *
	  					from t5_update_marking_log_user_list tt
					) tt
					join
					(
						SELECT useridx, today, (moneyin + h_moneyin) AS money_in, (playcount + h_playcount) as playcount  FROM t5_user_playstat_daily
						UNION ALL
						SELECT useridx, today, (moneyin + h_moneyin) AS money_in, (playcount + h_playcount) as playcount  FROM t5_user_playstat_daily_ios
						UNION ALL
						SELECT useridx, today, (moneyin + h_moneyin) AS money_in, (playcount + h_playcount) as playcount  FROM t5_user_playstat_daily_android
						UNION ALL
						SELECT useridx, today, (moneyin + h_moneyin) AS money_in, (playcount + h_playcount) as playcount  FROM t5_user_playstat_daily_amazon						
					) td on tt.useridx = td.useridx and dateadd(d, -28, tt.logindate) <= today AND td.today < to_char(tt.logindate, 'yyyy-mm-dd') group by tt.logidx, tt.useridx, tt.markingidx, tt.logindate ";
			$update_28dplay_list = $db_redshift -> gettotallist($sql);
			
			for($i=0; $i<sizeof($update_28dplay_list); $i++)
			{
				$logidx = $update_28dplay_list[$i]["logidx"];
				$d28_play_days = $update_28dplay_list[$i]["d28_play_days"];
				$d28_money_in = $update_28dplay_list[$i]["d28_money_in"];
				$d28_playcount = $update_28dplay_list[$i]["d28_playcount"];
			
				if($insert_sql == "")
					$insert_sql = "INSERT INTO tbl_user_marking_log(logidx, d28_money_in, d28_playcount, d28_play_days) VALUES($logidx, $d28_money_in, $d28_playcount, $d28_play_days)";
				else
					$insert_sql .= ", ($logidx, $d28_money_in, $d28_playcount, $d28_play_days)";
			
				$insert_count++;
			
				if($insert_count >= 1000)
				{
					$insert_sql .= " ON DUPLICATE KEY UPDATE d28_money_in = VALUES(d28_money_in), d28_playcount = VALUES(d28_playcount), d28_play_days = VALUES(d28_play_days);";
					$db_main2->execute($insert_sql);
					$insert_sql = "";
					$insert_count = 0;
				}
			}
				
			if($insert_sql != "")
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE d28_money_in = VALUES(d28_money_in), d28_playcount = VALUES(d28_playcount), d28_play_days = VALUES(d28_play_days);";
				$db_main2->execute($insert_sql);
				$insert_sql = "";
				$insert_count = 0;
			}
			
			sleep(1);
			
			$insert_sql = "";
			$insert_count = 0;
			
			$sql = "select tt.logidx, tt.useridx, tt.markingidx, tt.logindate, datediff(day, MAX(today),  tt.logindate) AS play_leavedays ".
					"from	".
					"(	".
					"	select *	".
					"	from t5_update_marking_log_user_list tt	".
					") tt	".
					"join	".
					"(	".
					"	SELECT today, useridx  FROM t5_user_playstat_daily	".
					"	UNION ALL	".
					"	SELECT today, useridx  FROM t5_user_playstat_daily_ios	".
					"	UNION ALL	".
					"	SELECT today, useridx  FROM t5_user_playstat_daily_android	".
					"	UNION ALL	".
					"	SELECT today, useridx  FROM t5_user_playstat_daily_amazon	".
					") td on tt.useridx = td.useridx and td.today < to_char(tt.logindate, 'yyyy-mm-dd') group by tt.logidx, tt.useridx, tt.markingidx, tt.logindate";
			$update_play_list = $db_redshift -> gettotallist($sql);
				
			for($i=0; $i<sizeof($update_play_list); $i++)
			{
				$logidx = $update_play_list[$i]["logidx"];
				$play_leavedays = $update_play_list[$i]["play_leavedays"];
			
				if($insert_sql == "")
					$insert_sql = "INSERT INTO tbl_user_marking_log(logidx, play_leavedays) VALUES($logidx, $play_leavedays)";
				else
					$insert_sql .= ", ($logidx, $play_leavedays)";
			
				$insert_count++;
			
				if($insert_count >= 1000)
				{
					$insert_sql .= " ON DUPLICATE KEY UPDATE play_leavedays=VALUES(play_leavedays);";
					$db_main2->execute($insert_sql);
					$insert_sql = "";
					$insert_count = 0;
				}
			}
				
			if($insert_sql != "")
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE play_leavedays=VALUES(play_leavedays);";
				$db_main2->execute($insert_sql);
				$insert_sql = "";
				$insert_count = 0;
			}
			
			$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));
			sleep(1);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	$db_main2->end();
	$db_other->end();
	$db_redshift->end();
?>