<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/user/insert_swp_user") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/user/insert_swp_user") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("target_user_status Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}	
	
	
	
	$db_slave = new CDatabase_Slave_Main();
	$db_main2 = new CDatabase_Main2();	
	$db_redshift = new CDatabase_Redshift();
	
	$db_slave->execute("SET wait_timeout=7200");
	$db_main2->execute("SET wait_timeout=7200");
	$current_date = date("Y-m-d");
	
	$to = "kisa0819@doubleugames.com";
	$from = "report@doubleucasino.com";
	$title = "[TAKE5] 슈퍼 휠 파티 사용자 문제";
	$contents = "";
	
	try
	{
		$sql = "SELECT idx, (SELECT COUNT(*) FROM tbl_swp_event_check WHERE partyidx = t1.idx) AS before_check ".
				"FROM tbl_superwheelparty_event t1 WHERE startdate > NOW() AND DATE_SUB(startdate, INTERVAL 30 HOUR) < NOW() ORDER BY idx DESC LIMIT 1";
		$swp_info = $db_main2->getarray($sql);
		
		$partyidx = $swp_info["idx"];
		$party_check = $swp_info["before_check"];
		
		if($partyidx > 0 && $party_check == 0)
		{
			$sql = "INSERT INTO tbl_swp_event_check(partyidx, writedate) VALUES ($partyidx, NOW());";
			$db_main2->execute($sql);
					
			//"" 조건 변경값
			$sql = "SELECT * FROM tbl_swp_condition_info";
			$swp_condition_info = $db_main2->getarray($sql);
				
			$recently_payer_1_date = $swp_condition_info["recently_payer_1_date"];
			$recently_payer_1_money = $swp_condition_info["recently_payer_1_money"];
			$recently_payer_2_date = $swp_condition_info["recently_payer_2_date"];
			$recently_payer_2_money = $swp_condition_info["recently_payer_2_money"];
			$recently_payer_playcount = $swp_condition_info["recently_payer_playcount"];
			$recently_payer_winrate = $swp_condition_info["recently_payer_winrate"];
			
			$leave_payer_life_time_money = $swp_condition_info["leave_payer_life_time_money"];
			$leave_payer_1_date = $swp_condition_info["leave_payer_1_date"];
			$leave_payer_1_money = $swp_condition_info["leave_payer_1_money"];
			$leave_payer_2_money = $swp_condition_info["leave_payer_2_money"];
			$leave_payer_winrate = $swp_condition_info["leave_payer_winrate"];
				
			$nopayer_life_time_money = $swp_condition_info["nopayer_life_time_money"];
			$nopayer_playcount = $swp_condition_info["nopayer_playcount"];
			$nopayer_winrate = $swp_condition_info["nopayer_winrate"];
				
			$retention_user_1_money = $swp_condition_info["retention_user_1_money"];
			$retention_user_2_money = $swp_condition_info["retention_user_2_money"];
			$retention_user_winrate = $swp_condition_info["retention_user_winrate"];	
			
			$sql = "TRUNCATE TABLE tbl_swp_users;";
			$db_main2->execute($sql);
			
			// 낮은 승률 경험자
			
			write_log("SWP_1");
			//최근 결제자
			$sql = "SELECT t2.useridx AS useridx, winrate, playcount 
					FROM	
					(	
						SELECT useridx, SUM(money) AS sum_money 
						FROM 
						( 
							SELECT useridx, (facebookcredit/10) AS money FROM t5_product_order WHERE useridx > 20000 AND STATUS = 1 AND writedate >= dateadd(DAY, -$recently_payer_1_date,  '$current_date') AND (facebookcredit/10) >= $recently_payer_1_money 	
							UNION ALL 
							SELECT useridx, money FROM t5_product_order_mobile WHERE useridx > 20000 AND STATUS = 1 AND writedate >= dateadd(DAY, -$recently_payer_1_date,  '$current_date') AND money >= $recently_payer_1_money	 
						) t1 GROUP BY useridx 
					) t2  join
					(
					  SELECT useridx,  SUM(money_out)::FLOAT/SUM(money_in)::FLOAT*100::FLOAT as winrate, sum(playcount) as playcount
					  FROM	
					  (	
					  	SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out,  (playcount + h_playcount) as playcount	
					  	FROM t5_user_playstat_daily	
					  	WHERE date(dateadd(DAY, -8,  '$current_date')) <= today AND today <= date(dateadd(DAY, -1,  '$current_date'))	
					  	UNION ALL	
					  	SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out,  (playcount + h_playcount) as playcount	
					  	FROM t5_user_playstat_daily_ios	
					  	WHERE date(dateadd(DAY, -8,  '$current_date')) <= today AND today <= date(dateadd(DAY, -1,  '$current_date'))	
					  	UNION ALL	
					  	SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out, (playcount + h_playcount) as playcount	
					  	FROM t5_user_playstat_daily_android	
					  	WHERE date(dateadd(DAY, -8,  '$current_date')) <= today AND today <= date(dateadd(DAY, -1,  '$current_date'))	
					  	UNION ALL	
					  	SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out,  (playcount + h_playcount) as playcount	
					  	FROM t5_user_playstat_daily_amazon	
					  	WHERE date(dateadd(DAY, -8,  '$current_date')) <= today AND today <= date(dateadd(DAY, -1,  '$current_date'))	
					  ) t1	
					  GROUP BY useridx HAVING SUM(playcount) > $recently_payer_playcount AND CASE WHEN SUM(money_in)::FLOAT =0 THEN 0 ELSE SUM(money_out)::FLOAT/SUM(money_in)::FLOAT*100::FLOAT END < $recently_payer_winrate
					) t3 on t2.useridx=t3.useridx
					WHERE  t2.useridx 
					NOT IN ( 
						SELECT useridx 
						FROM 
						( 
							SELECT useridx, (facebookcredit/10) AS money FROM t5_product_order WHERE useridx > 20000  AND STATUS = 1 AND writedate >= dateadd(DAY, -$recently_payer_2_date,  '$current_date') AND (facebookcredit/10) >= $recently_payer_2_money	 
							UNION ALL		
							SELECT useridx, money FROM t5_product_order_mobile WHERE useridx > 20000 AND STATUS = 1 AND writedate >= dateadd(DAY, -$recently_payer_2_date,  '$current_date') AND money >= $recently_payer_2_money	 	 
						) tt GROUP BY useridx
					)";
			$recently_payer_list = $db_redshift->gettotallist($sql);
		
			$recently_payer_cnt = 0;
			$recently_payer_sql = "";
			$recently_payer_history_sql = "";
			
			for($i=0; $i < sizeof($recently_payer_list); $i++)
			{
				$recently_payer = $recently_payer_list[$i]["useridx"];	
				
				$recently_payer_cnt++;
				
				if($recently_payer_sql == "")
				{
					$recently_payer_sql = "INSERT INTO tbl_swp_users(partyidx, useridx, type, writedate) VALUES($partyidx, $recently_payer, 1, NOW())";					
				}
				else
				{
					$recently_payer_sql .= ",($partyidx, $recently_payer, 1, NOW())";
				}
				
				if($recently_payer_cnt == 500)
				{
					$recently_payer_sql .= " ON DUPLICATE KEY UPDATE type=VALUES(type), writedate=VALUES(writedate)";
						
					$db_main2->execute($recently_payer_sql);
						
					$recently_payer_cnt = 0;
					$recently_payer_sql = "";
				}
				
				$recently_payer_history_sql = "INSERT INTO tbl_swp_users_history_".($recently_payer%10)."(partyidx, useridx, type, writedate) VALUES($partyidx, $recently_payer, 1, NOW()) ON DUPLICATE KEY UPDATE type=VALUES(type), writedate=VALUES(writedate);";
				$db_main2->execute($recently_payer_history_sql);
			}
			
			if($recently_payer_sql != "")
			{
				$recently_payer_sql .= " ON DUPLICATE KEY UPDATE type=VALUES(type), writedate=VALUES(writedate)";
				
				$db_main2->execute($recently_payer_sql);
			}	
			
			write_log("SWP_2");
			// 결제 이탈자
			$sql = "SELECT DISTINCT useridx ".
					"FROM	".
					"(	".
					"	SELECT useridx, (facebookcredit/10) AS money FROM tbl_product_order WHERE useridx > 20000 AND STATUS = 1 AND (facebookcredit/10) >= $leave_payer_life_time_money	".
					"	UNION ALL	".
					"	SELECT useridx, money FROM tbl_product_order_mobile WHERE useridx > 20000 AND STATUS = 1  AND money >= $leave_payer_life_time_money	".
					") t1";
			$leave_payer_list = $db_slave->gettotallist($sql);

			$leave_payer_cnt = 0;
			$leave_payer_sql = "";
			$leave_payer_history_sql = "";
			
			for($i=0; $i < sizeof($leave_payer_list); $i++)
			{
				$leave_payer_useridx = $leave_payer_list[$i]["useridx"];
				
				$sql = "SELECT COUNT(*) ".
						"FROM	". 
						"(	". 
						"	SELECT useridx, (facebookcredit/10) AS money FROM tbl_product_order WHERE useridx = $leave_payer_useridx  AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL $leave_payer_1_date DAY) AND (facebookcredit/10) >= $leave_payer_1_money	". 
						"	UNION ALL	".		
						"	SELECT useridx, money FROM tbl_product_order_mobile WHERE useridx = $leave_payer_useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL $leave_payer_1_date DAY) AND money >= $leave_payer_1_money 	". 
						") tt GROUP BY useridx ";
				$week_pay = $db_slave->getvalue($sql);
				
				if($week_pay < 1 || $week_pay = "")
				{
					$sql = "SELECT IF(current_coin/(coin+usercoin)*100 < $leave_payer_winrate, 1, 0) ".
							"FROM	".
							"(	".
							"	SELECT useridx, (SELECT coin FROM tbl_user WHERE useridx = t1.useridx) AS current_coin, coin, usercoin	".
							"	FROM	".
							"	(	".
							"		SELECT useridx, coin, usercoin, writedate FROM tbl_product_order WHERE STATUS = 1 AND useridx = $leave_payer_useridx AND (facebookcredit/10) >= $leave_payer_2_money ".
							"		UNION ALL	".
							"		SELECT useridx, coin, usercoin, writedate FROM tbl_product_order_mobile WHERE STATUS = 1 AND useridx = $leave_payer_useridx AND money >= $leave_payer_2_money	".
							"	) t1 ORDER BY writedate DESC LIMIT 1	".
							") t2";
					$check_leave_payer_user = $db_slave->getvalue($sql);
					
					if($check_leave_payer_user == 1)
					{
						$leave_payer_cnt++;
						
						if($leave_payer_sql == "")
						{
							$leave_payer_sql = "INSERT INTO tbl_swp_users(partyidx, useridx, type, writedate) VALUES($partyidx, $leave_payer_useridx, 2, NOW())";							
						}
						else
						{	
							$leave_payer_sql .= ",($partyidx, $leave_payer_useridx, 2, NOW())";
						}
						
						if($leave_payer_cnt == 500)
						{
							$leave_payer_sql .= " ON DUPLICATE KEY UPDATE type=VALUES(type), writedate=VALUES(writedate)";
							
							$db_main2->execute($leave_payer_sql);
						
							$leave_payer_cnt = 0;
							$leave_payer_sql = "";
						}
						
						$leave_payer_history_sql = "INSERT INTO tbl_swp_users_history_".($leave_payer_useridx%10)."(partyidx, useridx, type, writedate) VALUES($partyidx, $leave_payer_useridx, 2, NOW()) ON DUPLICATE KEY UPDATE type=VALUES(type), writedate=VALUES(writedate)";
						$db_main2->execute($leave_payer_history_sql);
					}
				}
			}
			
			if($leave_payer_sql != "")
			{
				$leave_payer_sql .= " ON DUPLICATE KEY UPDATE type=VALUES(type), writedate=VALUES(writedate)";
				
				$db_main2->execute($leave_payer_sql);
			}

			write_log("SWP_3");
			// 결제 미경험자
			$sql = "select t1.useridx, winrate, playcount
					from
					(
					  select useridx from t5_user t1 where useridx > 20000 and 
					  (not exists ( select * from t5_product_order WHERE useridx = t1.useridx and status = 1 and (facebookcredit/10) >= $nopayer_life_time_money) 
					  and not exists ( select * from t5_product_order_mobile WHERE useridx = t1.useridx and status = 1 and money >= $nopayer_life_time_money))
					) t1 
					join 
					(
					  SELECT useridx, (SUM(money_out)::FLOAT/SUM(money_in)::FLOAT*100::FLOAT) AS winrate, sum(playcount) as playcount 
									FROM	 
									(	
										SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out, (playcount + h_playcount) as playcount
										FROM t5_user_playstat_daily	 
										WHERE date(dateadd(DAY, -8,  '$current_date')) <= today AND today <= date(dateadd(DAY, -1,  '$current_date'))	
										UNION ALL	
										SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out, (playcount + h_playcount) as playcount	 
										FROM t5_user_playstat_daily_ios	 
										WHERE date(dateadd(DAY, -8,  '$current_date')) <= today AND today <= date(dateadd(DAY, -1,  '$current_date'))	
										UNION ALL	
										SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out, (playcount + h_playcount) as playcount	 
										FROM t5_user_playstat_daily_android	
										WHERE date(dateadd(DAY, -8,  '$current_date')) <= today AND today <= date(dateadd(DAY, -1,  '$current_date'))	
										UNION ALL	
										SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out, (playcount + h_playcount) as playcount	  
										FROM t5_user_playstat_daily_amazon	 
										WHERE date(dateadd(DAY, -8,  '$current_date')) <= today AND today <= date(dateadd(DAY, -1,  '$current_date'))	
										) t1	 
									GROUP BY useridx HAVING SUM(playcount) > $nopayer_playcount AND CASE WHEN SUM(money_in)::FLOAT =0 THEN 0 ELSE SUM(money_out)::FLOAT/SUM(money_in)::FLOAT*100::FLOAT END < $nopayer_winrate
					) t2 on t1.useridx = t2.useridx";
			$nopayer_list = $db_redshift->gettotallist($sql);
			
			$nopayer_cnt = 0;
			$nopayer_sql = "";
			$nopayer_history_sql = "";
			
			for($i=0; $i < sizeof($nopayer_list); $i++)
			{
				$nopayer_useridx = $nopayer_list[$i]["useridx"];
				
				$nopayer_cnt++;
				
				if($nopayer_sql == "")
				{
					$nopayer_sql = "INSERT INTO tbl_swp_users(partyidx, useridx, type, writedate) VALUES($partyidx, $nopayer_useridx, 3, NOW())";					
				}
				else
				{
					$nopayer_sql .= ",($partyidx, $nopayer_useridx, 3, NOW())";
				}
				
				if($nopayer_cnt == 500)
				{
					$nopayer_sql .= " ON DUPLICATE KEY UPDATE type=VALUES(type), writedate=VALUES(writedate)";
					
					$db_main2->execute($nopayer_sql);
					
					$nopayer_cnt = 0;
					$nopayer_sql = "";
				}
				
				$nopayer_history_sql = "INSERT INTO tbl_swp_users_history_".($nopayer_useridx%10)."(partyidx, useridx, type, writedate) VALUES($partyidx, $nopayer_useridx, 3, NOW()) ON DUPLICATE KEY UPDATE type=VALUES(type), writedate=VALUES(writedate)";
				$db_main2->execute($nopayer_history_sql);
			}
			
			if($nopayer_sql != "")
			{
				$nopayer_sql .= " ON DUPLICATE KEY UPDATE type=VALUES(type), writedate=VALUES(writedate)";
					
				$db_main2->execute($nopayer_sql);
			}

			write_log("SWP_4");			
			//복귀자
			$sql = "SELECT useridx ".
					"FROM ".
					"	(	".
					"		SELECT useridx, leavedays, writedate FROM tbl_user_retention_log WHERE leavedays >= 28	AND writedate >= DATE_SUB(NOW(), INTERVAL 1 WEEK)	".
					"		UNION ALL	".
					"		SELECT useridx, leavedays, writedate FROM tbl_user_retention_mobile_log WHERE leavedays >= 28 AND writedate >= DATE_SUB(NOW(), INTERVAL 1 WEEK) ".
					"	) t1 group by useridx;";
			$retention_user_list = $db_main2->gettotallist($sql);
			
			$retention_user_cnt = 0;
			$retention_user_sql = "";	
			$retention_user_history_sql = "";
			
			for($i = 0; $i < sizeof($retention_user_list); $i++)
			{
				$retention_user = $retention_user_list[$i]["useridx"];
				
				$sql = "SELECT COUNT(*) ".
						"FROM	".
						"(	".
						"	SELECT useridx, (facebookcredit/10) AS money FROM tbl_product_order WHERE useridx = $retention_user AND STATUS = 1 AND (facebookcredit/10) >= $retention_user_1_money	".
						"	UNION ALL	".
						"	SELECT useridx,  money FROM tbl_product_order_mobile WHERE useridx = $retention_user AND STATUS = 1 AND money >= $retention_user_1_money	".
						") tt	";
				$check_pay = $db_slave->getvalue($sql);
				
				if($check_pay > 0)
				{			
					$sql = "SELECT IF(current_coin/(coin+usercoin)*100 < $retention_user_winrate, 1, 0) ".
							"FROM	".
							"(	".
							"	SELECT useridx, (SELECT coin FROM tbl_user WHERE useridx = t1.useridx) AS current_coin, coin, usercoin	".
							"	FROM	".
							"	(	".
							"		SELECT useridx, coin, usercoin, writedate FROM tbl_product_order WHERE STATUS = 1 AND useridx = $retention_user AND (facebookcredit/10) >= $retention_user_2_money ".
							"		UNION ALL	".
							"		SELECT useridx, coin, usercoin, writedate FROM tbl_product_order_mobile WHERE STATUS = 1 AND useridx = $retention_user AND money >= $retention_user_2_money	".
							"	) t1 ORDER BY writedate DESC LIMIT 1	".
							") t2";
					$check_retention_user = $db_slave->getvalue($sql);
									
					if($check_retention_user == 1)
					{

						$retention_user_cnt++;
						
						if($retention_user_sql == "")
						{
							$retention_user_sql = "INSERT INTO tbl_swp_users(partyidx, useridx, type, writedate) VALUES($partyidx, $retention_user, 4, NOW())";							
						}
						else
						{
							$retention_user_sql .= ",($partyidx, $retention_user, 4, NOW())";
						}
						
						if($retention_user_cnt == 500)
						{
							$retention_user_sql .= " ON DUPLICATE KEY UPDATE type=VALUES(type), writedate=VALUES(writedate)";
							
							$db_main2->execute($retention_user_sql);
						
							$retention_user_cnt = 0;
							$retention_user_sql = "";
						}
						
						$retention_user_history_sql = "INSERT INTO tbl_swp_users_history_".($retention_user%10)."(partyidx, useridx, type, writedate) VALUES($partyidx, $retention_user, 4, NOW()) ON DUPLICATE KEY UPDATE type=VALUES(type), writedate=VALUES(writedate)";
						$db_main2->execute($retention_user_history_sql);
					}
				}
			}			
				
			if($retention_user_sql != "")
			{
				$retention_user_sql .= " ON DUPLICATE KEY UPDATE type=VALUES(type), writedate=VALUES(writedate)";
				
				$db_main2->execute($retention_user_sql);
			}

			write_log("SWP_5");
			// 내부 사용자
			$sql = "SELECT useridx FROM tbl_user_ext WHERE 10000 < useridx AND useridx < 20000";
			$inner_user_list = $db_slave->gettotallist($sql);
			
			$inner_user_sql = "";
			$inner_user_history_sql = "";
			
			for($i=0; $i < sizeof($inner_user_list); $i++)
			{
				$inner_useridx = $inner_user_list[$i]["useridx"];
				
				if($inner_user_sql == "")
				{
					$inner_user_sql = "INSERT INTO tbl_swp_users(partyidx, useridx, type, writedate) VALUES($partyidx, $inner_useridx, 5, NOW())";					
				}
				else
				{
					$inner_user_sql .= ",($partyidx, $inner_useridx, 5, NOW())";
				}				
				
				$inner_user_history_sql = "INSERT INTO tbl_swp_users_history_".($inner_useridx%10)."(partyidx, useridx, type, writedate) VALUES($partyidx, $inner_useridx, 5, NOW()) ON DUPLICATE KEY UPDATE type=VALUES(type), writedate=VALUES(writedate)";
				$db_main2->execute($inner_user_history_sql);
			}
			
			if($inner_user_sql != "")
			{
				$inner_user_sql .= " ON DUPLICATE KEY UPDATE type=VALUES(type), writedate=VALUES(writedate)";
							
				$db_main2->execute($inner_user_sql);
				
			}
			
			$sql = "SELECT type, count(*) AS cnt FROM tbl_swp_users GROUP BY type";
			$result_swp_users = $db_main2->gettotallist($sql);
				
			for($swp = 0; $swp < sizeof($result_swp_users); $swp++)
			{
				$type = $result_swp_users[$swp]['type'];
				$cnt = $result_swp_users[$swp]['cnt'];
			
				$contents .= $type." => COUNT : $cnt <br>";
			}
		}
		
		if($contents != "")
		{
			sendmail($to, $from, $title, $contents);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_slave->end();
	$db_main2->end();
	$db_redshift->end();
	
?>