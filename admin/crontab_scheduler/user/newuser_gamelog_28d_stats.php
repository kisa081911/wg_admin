<?
	include("../../common/common_include.inc.php");
	
	ini_set("memory_limit", "-1");

	$db_slave_main = new CDatabase_Slave_Main();
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();	
	
	$db_slave_main->execute("SET wait_timeout=7200");
	$db_main2->execute("SET wait_timeout=7200");
	$db_other->execute("SET wait_timeout=7200");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/user/newuser_gamelog_28d_stats") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/user/newuser_gamelog_28d_stats") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("newuser_gamelog_28d_stats Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	try
	{
		write_log("newuser_gamelog_28d 1.: ".date("Y-m-d H:i:s"));
		
		$today = date("Y-m-d");
		
		$scheduler_today = "$today 00:00:00";
		
		$insert_sql = "TRUNCATE TABLE tbl_user_gamelog_28d";
		$db_other->execute($insert_sql);

		//가입한지 28일 이상이고 현재 $5결제가 없는 경우(첫결제를 안한 유저)
		$sql = "SELECT t1.useridx AS useridx, createdate ".
				"FROM tbl_user_ext t1 JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx WHERE t1.useridx > 20000 AND DATE_SUB('$scheduler_today', INTERVAL 28 DAY) >= createdate AND t1.logindate >= DATE_SUB('$scheduler_today', INTERVAL 14 DAY) AND tutorial= 4 ".
				"AND t1.useridx NOT IN ". 
				"(	".
				"	SELECT useridx	".
				"	FROM	".
				"	(	".
				"		SELECT useridx	". 
				"		FROM `tbl_product_order` WHERE useridx > 20000 AND STATUS = 1 AND facebookcredit >= 50	".
				"		UNION ALL	".
				"		SELECT useridx	". 
				"		FROM `tbl_product_order_mobile` WHERE useridx > 20000 AND STATUS = 1 AND facebookcredit >= 50	".
				"	) tt GROUP BY useridx	".
				");";
		write_log($sql);
		$useridx_data =  $db_slave_main->gettotallist($sql);

		$count = 0;
		
		for($i = 0; $i < sizeof($useridx_data); $i++)
		{
			$insert_useridx = $useridx_data[$i]["useridx"];
			
			$sql = "SELECT useridx AS exist_user FROM tbl_user_gamelog_28d WHERE useridx = $insert_useridx;";
			$useridx_exsit = $db_other->getvalue($sql);
			
			$insert_sql = "";
			
			if($useridx_exsit == 0)
			{
				//최근 14일동안 3일 이상 플레이를 한 경우
				$sql = "SELECT COUNT(today) AS game_count
						FROM
						(
							SELECT today, useridx
							FROM
							(
								SELECT today, useridx FROM tbl_user_playstat_daily WHERE useridx = $insert_useridx AND today >= DATE_SUB(NOW(), INTERVAL 14 DAY)
								UNION ALL
								SELECT today, useridx FROM tbl_user_playstat_daily_ios WHERE useridx = $insert_useridx AND today >= DATE_SUB(NOW(), INTERVAL 14 DAY)
								UNION ALL
								SELECT today, useridx FROM tbl_user_playstat_daily_android WHERE useridx = $insert_useridx AND today >= DATE_SUB(NOW(), INTERVAL 14 DAY)
								UNION ALL
								SELECT today, useridx FROM tbl_user_playstat_daily_amazon WHERE useridx = $insert_useridx AND today >= DATE_SUB(NOW(), INTERVAL 14 DAY)
				
							) AS t1
							GROUP BY today, useridx
						) AS t2
						GROUP BY useridx";
				$game_play_check = $db_other->getvalue($sql);
				
				if($game_play_check == "")
					$game_play_check = 0;
				
				if($game_play_check >= 3)
				{
					//최근 14일동안 3일 이상 플레이한 유저만
					$sql = "SELECT useridx, platform, moneyin, moneyout, playcount, usercoin, writedate
							FROM
							(
								SELECT useridx, 0 AS platform, moneyin, moneyout, playcount, usercoin, writedate
								FROM tbl_user_gamelog WHERE useridx = $insert_useridx AND DATE_SUB(NOW(), INTERVAL 14 DAY) <= writedate
								UNION ALL
								SELECT useridx, 1 AS platform,  moneyin, moneyout, playcount, usercoin, writedate
								FROM tbl_user_gamelog_ios WHERE useridx = $insert_useridx AND DATE_SUB(NOW(), INTERVAL 14 DAY) <= writedate
								UNION ALL
								SELECT useridx, 2 AS platform,  moneyin, moneyout, playcount, usercoin, writedate
								FROM tbl_user_gamelog_android WHERE useridx = $insert_useridx AND DATE_SUB(NOW(), INTERVAL 14 DAY) <= writedate
								UNION ALL
								SELECT useridx, 3 AS platform,  moneyin, moneyout, playcount, usercoin, writedate
								FROM tbl_user_gamelog_amazon WHERE useridx = $insert_useridx AND DATE_SUB(NOW(), INTERVAL 14 DAY) <= writedate
							) tt ORDER BY writedate ASC";
					$user_data = $db_other->gettotallist($sql);
					
					for($j=0; $j<sizeof($user_data); $j++)
					{
						$useridx = $user_data[$j]["useridx"];
						$platform = $user_data[$j]["platform"];
						$moneyin = $user_data[$j]["moneyin"];
						$moneyout = $user_data[$j]["moneyout"];
						$playcount = $user_data[$j]["playcount"];
						$usercoin = $user_data[$j]["usercoin"];
						$writedate = $user_data[$j]["writedate"];
						
						//처음으로 넣어줄 때
						if($j == 0)
						{
							$defalt_coin = $usercoin;
							
							$bet_rate = ($playcount == 0) ? 0 :  $defalt_coin / ($moneyin / $playcount);
							
							if($insert_sql == "")
							{							
								$insert_sql = "INSERT INTO tbl_user_gamelog_28d(useridx, platform, moneyin, moneyout, playcount, coin, bet_ratio, writedate) ".
												"VALUES($useridx, $platform, $moneyin, $moneyout, $playcount, $defalt_coin, '$bet_rate', '$writedate')";
							}
							
							$sql = "SELECT LAST_INSERT_ID()";
							$logidx = $db_other->getvalue($sql);
						}
						else
						{
							$sql = "SELECT moneyin, moneyout, coin FROM tbl_user_gamelog_28d WHERE useridx = $useridx and logidx = $logidx;";
							$newuser_3h_data = $db_other->getarray($sql);
								
							$newuser_3h_moneyin = $newuser_3h_data["moneyin"];
							$newuser_3h_moenyout = $newuser_3h_data["moneyout"];
							$newuser_3h_default_coin = $newuser_3h_data["coin"];
								
							$newuser_3h_money_change = $newuser_3h_moenyout - $newuser_3h_moneyin;
								
							$defalt_coin = $newuser_3h_default_coin + $newuser_3h_money_change;
								
							$bet_rate = ($playcount == 0) ? 0 :  $defalt_coin / ($moneyin / $playcount);
								
							$insert_sql .= ",($useridx, $platform, $moneyin, $moneyout, $playcount, $defalt_coin, '$bet_rate', '$writedate')";
								
							$sql = "SELECT LAST_INSERT_ID()";
							$logidx = $db_other->getvalue($sql);
						}						

						$count++;

					}

					if($insert_sql != "")
					{
						$db_other->execute($insert_sql);
						
						$insert_sql = "";
					}

					if($count == 10000)
					{
						sleep(1);
						$count = 0;						
					}
				}		
			}
		}
		
		write_log("newuser_gamelog_28d 2.: ".date("Y-m-d H:i:s"));
		
		// 웨일 유저 가능성 판단체크
		$whale_today = date("Y-m-d", time());
		
		$sql = "SELECT useridx,  (CASE WHEN std_value < 6 THEN 5 WHEN std_value < 12 THEN 9 WHEN std_value < 18 THEN 19 WHEN std_value < 24 THEN 39 ELSE 59 END) AS offer  ".
				"FROM ( ".
				"	SELECT a.useridx, ROUND(COUNT(DISTINCT(a.moneyin/a.playcount)) + MAX(a.moneyin/a.playcount)/MIN(a.moneyin/a.playcount) + MAX(a.moneyin/a.playcount)/AVG(a.moneyin/a.playcount)) AS std_value ".
				"	FROM tbl_user_gamelog_28d a ".
				"	WHERE a.playcount >= 10 AND a.coin >= 1000000 AND a.coin < 10000000 ".
				"	GROUP BY a.useridx ".
				") t1";		
		write_log($sql);
		$offer_list = $db_other->gettotallist($sql);
			
		$sql = "";
		$insertcount = 0;
		
		for($i=0; $i<sizeof($offer_list); $i++)
		{
			$useridx = $offer_list[$i]["useridx"];
			$offer = $offer_list[$i]["offer"];
				
			$check_sql = "SELECT SUM(pay_count) ".
					"FROM ( ".
					"	SELECT COUNT(*) AS pay_count FROM tbl_product_order WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 ".
					"	UNION ALL ".
					"	SELECT COUNT(*) AS pay_count FROM tbl_product_order_mobile WHERE useridx = $useridx AND STATUS = 1 AND money >= 4.99 ".
					") t1";
			$check_pay = $db_slave_main->getvalue($check_sql);
		
			if($check_pay == 0)
			{
				if ($insertcount == 0)
				{
					$sql = "INSERT INTO tbl_whale_user_offer_28d(useridx, offer, writedate) VALUES($useridx, $offer, NOW())";
		
					$insertcount++;
				}
				else if ($insertcount < 100)
				{
					$sql .= ", ($useridx, $offer, NOW())";
					$insertcount++;
				}
				else
				{
					$sql .= " ON DUPLICATE KEY UPDATE offer=VALUES(offer), writedate=VALUES(writedate);";
					$db_main2->execute($sql);
						
					$insertcount = 0;
					$sql = "";
				}
			}
		}
		
		if($sql != "")
		{
			$sql .= " ON DUPLICATE KEY UPDATE offer=VALUES(offer), writedate=VALUES(writedate);";
			$db_main2->execute($sql);
		}
		
		$sql = "DELETE FROM tbl_whale_user_offer_28d WHERE writedate < '$whale_today 00:00:00'";
		$db_main2->execute($sql);
		
		write_log("newuser_gamelog_28d 3.: ".date("Y-m-d H:i:s"));
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_slave_main->end();
	$db_main2->end();
	$db_other->end();
?>