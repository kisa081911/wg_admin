<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/user/user_marking_log_update") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/user/user_marking_log_update") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("user_marking_log_update Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	ini_set("memory_limit", "-1");

	$db_main2 = new CDatabase_Main2();	
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	$db_main2->execute("SET wait_timeout=3600");
	$db_other->execute("SET wait_timeout=3600");
	
	try
	{
		$today = date("Y-m-d");
		$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
		$yesterday2 = date("Y-m-d", time() - 60 * 60 * 24 * 2);
		
		$stimer = explode(' ', microtime());
		$stimer = $stimer[1] + $stimer[0];
		
		$sql = "SELECT *, DATE_FORMAT(logindate, '%Y-%m-%d %H') AS logindate2 FROM tbl_user_marking_log WHERE today = '$yesterday'";
		write_log($sql);
		$makring_list = $db_main2->gettotallist($sql);
		
		$insertcount = 0;
		$insert_sql = "";
		
		for($i=0; $i<sizeof($makring_list); $i++)
		{
			$useridx = $makring_list[$i]["useridx"];
			$markingidx = $makring_list[$i]["markingidx"];
			$makring_today = $makring_list[$i]["today"];
			$logindate = $makring_list[$i]["logindate"];
			$logindate2 = $makring_list[$i]["logindate2"];
			
			//login 시간 기준
			$sql = "SELECT DATEDIFF('$logindate', writedate) as buy_leavedays, ".
					"(SELECT IFNULL(sum(money), 0) FROM tbl_product_order_all WHERE useridx = t1.useridx AND status = 1 AND writedate < '$logindate') as total_money, ".
					"(SELECT count(*) FROM tbl_product_order_all WHERE useridx = t1.useridx AND status = 1 AND writedate < '$logindate') as total_count, ".
					"(SELECT IFNULL(sum(money), 0) FROM tbl_product_order_all WHERE useridx = t1.useridx and status = 1 AND DATE_SUB('$logindate', INTERVAL 28 DAY) <= writedate AND writedate < '$logindate') as d28_total_money, ".
					"(SELECT count(*) FROM tbl_product_order_all WHERE useridx = t1.useridx  AND status = 1 AND DATE_SUB('$logindate', INTERVAL 28 DAY) <= writedate AND writedate < '$logindate') as d28_total_count ".
					"FROM tbl_product_order_all t1 WHERE useridx = $useridx AND writedate < '$logindate' AND status = 1 ORDER BY writedate DESC LIMIT 1";
			$buy_data = $db_other->getarray($sql);
			
			$buy_leavedays = $buy_data["buy_leavedays"];
			$total_money = $buy_data["total_money"];
			$total_count = $buy_data["total_count"];
			$d28_total_money = $buy_data["d28_total_money"];
			$d28_total_count = $buy_data["d28_total_count"];
			$is_payer = 0;
			
			if($buy_leavedays == "")
				$buy_leavedays = 0;
			
			if($total_money == "")
				$total_money = 0;
			
			if($total_count == "")
				$total_count = 0;
			
			if($d28_total_money == "")
				$d28_total_money = 0;
			
			if($d28_total_count == "")
				$d28_total_count = 0;			
			
			if($total_count > 0)
				$is_payer = 1;
			
			$check_today = date("Y-m-d", strtotime($logindate));			
			
			//login 날 제외
			$sql = "SELECT useridx, count(distinct today) as d28_play_days, SUM(money_in) as d28_money_in, sum(playcount) as d28_playcount ".
					"FROM	".	 
					"(	".	
  					"	SELECT useridx, today, (moneyin + h_moneyin) AS money_in, (playcount + h_playcount) as playcount	".
  					"	FROM tbl_user_playstat_daily	".	 
  					"	WHERE useridx = $useridx AND DATE_SUB('$check_today', INTERVAL 28 DAY) <= today AND today < '$check_today'	".
  					"	UNION ALL	".	
  					"	SELECT useridx, today, (moneyin + h_moneyin) AS money_in, (playcount + h_playcount) as playcount	".	 
  					"	FROM tbl_user_playstat_daily_ios	".	 
  					"	WHERE useridx = $useridx AND DATE_SUB('$check_today', INTERVAL 28 DAY) <= today AND today < '$check_today'	".	
  					"	UNION ALL	".	
  					"	SELECT useridx, today, (moneyin + h_moneyin) AS money_in, (playcount + h_playcount) as playcount	".	 
  					"	FROM tbl_user_playstat_daily_android	".	
  					"	WHERE useridx = $useridx AND DATE_SUB('$check_today', INTERVAL 28 DAY) <= today AND today < '$check_today'	".	
  					"	UNION ALL	".	
  					"	SELECT useridx, today, (moneyin + h_moneyin) AS money_in, (playcount + h_playcount) as playcount	".	  
  					"	FROM tbl_user_playstat_daily_amazon	".	 
  					"	WHERE useridx = $useridx AND DATE_SUB('$check_today', INTERVAL 28 DAY) <= today AND today < '$check_today'	".	
					") tt GROUP BY useridx;";
			$play_data = $db_other->getarray($sql);
			
			$d28_play_days = $play_data["d28_play_days"];
			$d28_money_in = $play_data["d28_money_in"];
			$d28_playcount = $play_data["d28_playcount"];
			
			if($d28_play_days == "")
				$d28_play_days = 0;
			
			if($d28_money_in == "")
				$d28_money_in = 0;
			
			if($d28_playcount == "")
				$d28_playcount = 0;
			
			$sql = "SELECT datediff(day, MAX(today),  '$check_today') AS play_leavedays ".
  					"FROM	".
  					"(	".
    				"	SELECT today  FROM t5_user_playstat_daily	WHERE useridx = $useridx AND today < '$check_today'	".	
    				"	UNION ALL	".
    				"	SELECT today  FROM t5_user_playstat_daily_ios	WHERE useridx = $useridx AND today < '$check_today'	".	
    				"	UNION ALL	".
    				"	SELECT today  FROM t5_user_playstat_daily_android	WHERE useridx = $useridx AND today < '$check_today'	".	
    				"	UNION ALL	".
    				"	SELECT today  FROM t5_user_playstat_daily_amazon	WHERE useridx = $useridx AND today < '$check_today'	".	  
  					") tt";
			$play_leavedays = $db_redshift->getvalue($sql);
			
			if($play_leavedays == "")
				$play_leavedays = 0;
			
			//$sql = "SELECT datediff(day, MAX(writedate),  '$check_today') AS play_leavedays FROM t5_user_login_log where useridx = $useridx and writedate < '$check_today'";
			$sql = "select max(leavedays) ".
					"from	".
					"(	".
					"	select leavedays, writedate from t5_user_retention_log where useridx = $useridx and writedate <= '$logindate'	".
					"	union all	".
					"	select leavedays, writedate from t5_user_retention_mobile_log where useridx = $useridx and writedate <= '$logindate'	".
					") tt where to_char(writedate, 'YYYY-MM-DD HH24') = '$logindate2'	";
			$login_leavedays = $db_redshift->getvalue($sql);
				
			if($login_leavedays == "")
				$login_leavedays = 0;
			
			if($insert_sql == "" && $insertcount == 0)
			{
				$insert_sql = "INSERT INTO tbl_user_marking_log(useridx, markingidx, today, is_payer, leavedays, buy_leavedays, play_leavedays, total_pay_amount, total_pay_count, d28_pay_amount, d28_pay_count, d28_money_in, d28_playcount, d28_play_days) ".
								"VALUES ($useridx, $markingidx, '$makring_today', $is_payer, $login_leavedays, $buy_leavedays, $play_leavedays, $total_money, $total_count, $d28_total_money, $d28_total_count, $d28_money_in, $d28_playcount, $d28_play_days)";
				$insertcount ++;
			}
			else if($insertcount < 100)
			{
				$insert_sql .= ",($useridx, $markingidx, '$makring_today', $is_payer, $login_leavedays, $buy_leavedays, $play_leavedays, $total_money, $total_count, $d28_total_money, $d28_total_count, $d28_money_in, $d28_playcount, $d28_play_days)";
				$insertcount ++;
			}
			else
			{
				$insert_sql .= ",($useridx, $markingidx, '$makring_today', $is_payer, $login_leavedays, $buy_leavedays, $play_leavedays, $total_money, $total_count, $d28_total_money, $d28_total_count, $d28_money_in, $d28_playcount, $d28_play_days) ";
				$insert_sql .= " ON DUPLICATE KEY UPDATE is_payer = VALUES(is_payer), leavedays = VALUES(leavedays), buy_leavedays = VALUES(buy_leavedays), play_leavedays = VALUES(play_leavedays), total_pay_amount = VALUES(total_pay_amount), total_pay_count = VALUES(total_pay_count),  ";
				$insert_sql .= " d28_pay_amount = VALUES(d28_pay_amount), d28_pay_count = VALUES(d28_pay_count), d28_money_in = VALUES(d28_money_in), d28_playcount = VALUES(d28_playcount), d28_play_days = VALUES(d28_play_days)";			
				$db_main2->execute($insert_sql);
				
				$insertcount = 0;
				$insert_sql = "";
			}			
		}
		
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE is_payer = VALUES(is_payer), leavedays = VALUES(leavedays), buy_leavedays = VALUES(buy_leavedays), play_leavedays = VALUES(play_leavedays), total_pay_amount = VALUES(total_pay_amount), total_pay_count = VALUES(total_pay_count),  ";
			$insert_sql .= " d28_pay_amount = VALUES(d28_pay_amount), d28_pay_count = VALUES(d28_pay_count), d28_money_in = VALUES(d28_money_in), d28_playcount = VALUES(d28_playcount), d28_play_days = VALUES(d28_play_days)";
			$db_main2->execute($insert_sql);
			
			$insertcount = 0;
			$insert_sql = "";
		}
		
		$etimer = explode(' ', microtime());
		$etimer = $etimer[1] + $etimer[0];
		
		$time_delay = number_format(($etimer - $stimer), 0);
		
		write_log("User Marking Log Time Check : $time_delay");
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	$db_main2->end();
	$db_other->end();
	$db_redshift->end();
?>