<?
    include("../../common/common_include.inc.php");
    
    use Aws\Common\Aws;
    use Aws\S3\S3Client;
    
    ini_set("memory_limit", "-1");
    
    $result = array();
    exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
    
    $count = 0;
    
    for ($i=0; $i<sizeof($result); $i++)
    {
        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/user/insert_ABTest_target_user") !== false)
        {
            $count++;
        }
    }
    
    if ($count > 1)
    {
        $count = 0;
        
        $killcontents = "#!/bin/bash\n";
        
        $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
        $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
        
        $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
        
        flock($fp, LOCK_EX);
        
        if (!$fp) {
            echo "Fail";
            exit;
        }
        else
        {
            echo "OK";
        }
        
        flock($fp, LOCK_UN);
        
        $content = "#!/bin/bash\n";
        
        for ($i=0; $i<sizeof($result); $i++)
        {
            if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/user/insert_ABTest_target_user") !== false)
            {
                $process_list = explode("|", $result[$i]);
                
                $content .= "kill -9 ".$process_list[0]."\n";
                
                write_log("insert_ABTest_target_user Dead Lock Kill!");
            }
        }
        
        fwrite($fp, $content, strlen($content));
        fclose($fp);
        
        exit();
    }
    
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    
    $db_main->execute("SET wait_timeout=7200");
    $db_main2->execute("SET wait_timeout=7200");
    
    try
    {
        // 결제자
        $sql = " SELECT t1.useridx, DATEDIFF(NOW(), createdate) AS day_after_install, days_after_purchase, logindate". 
               " FROM tbl_user_ext t1 ".
               " JOIN ( ".
                        " SELECT useridx, DATEDIFF(NOW(), MAX(writedate)) AS days_after_purchase ".
                        " FROM( ".
                        " SELECT useridx, writedate  FROM tbl_product_order WHERE STATUS = 1 ".
                        " UNION ALL ".
                        " SELECT useridx, writedate FROM tbl_product_order_mobile WHERE STATUS IN (0, 1) ".
                        " ) t1 GROUP BY useridx HAVING DATEDIFF(NOW(), MAX(writedate)) >= 28 ".
                    " ) t2 ".
                    " ON t1.useridx = t2.useridx ".
                    " WHERE logindate > DATE_SUB(NOW(),INTERVAL 7 DAY) ".
                    " AND createdate <= DATE_SUB(NOW(),INTERVAL 28 DAY)".
                    " AND t1.useridx < 4462550".
                    " AND NOT EXISTS(SELECT * FROM tbl_html_test_user_1st t3 WHERE t1.useridx = t3.useridx)";
        
        $target_user_info = $db_main->gettotallist($sql);
        
        $insert_sql = "";
        $insert_cnt = 0;
        
        for($i=0; $i < sizeof($target_user_info); $i++)
        {
            $useridx = $target_user_info[$i]["useridx"];
            $day_after_install = $target_user_info[$i]["day_after_install"];
            $days_after_purchase = $target_user_info[$i]["days_after_purchase"];
            $logindate = $target_user_info[$i]["logindate"];
            
            $insert_cnt++;
            
            if($insert_sql == "")
            {
                $insert_sql = " INSERT IGNORE INTO tbl_html_test_user_2st(useridx,days_after_install,days_after_purchase,recently_logindate,writedate) ".
                              " VALUES('$useridx','$day_after_install','$days_after_purchase','$logindate',now())";
            }
            else
            {
                $insert_sql .= " ,('$useridx','$day_after_install','$days_after_purchase','$logindate',now())";
            }
            
            if($insert_cnt == 5000)
            {
                $db_main2->execute($insert_sql);
                
                $insert_cnt = 0;
                $insert_sql = "";
            }
        }
        
        if($insert_sql != ""){
            $db_main2->execute($insert_sql);
        }
    }
    catch(Exception $e)
    {
        write_log($e->getMessage());
    }
    
    $db_main->end();
    $db_main2->end();

?>