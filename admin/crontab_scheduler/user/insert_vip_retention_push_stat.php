<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/user_static/conversion_rate_scheduler") !== false)
		{
			$count++;
		}
	}
	
	ini_set("memory_limit", "-1");

	$issuccess = "1";
	
	if ($count > 1)
		exit();
	
	$db_main2 = new CDatabase_Main2();
	$db_mobile = new CDatabase_Mobile();
	$db_redshift = new CDatabase_Redshift();
	
	$db_mobile->execute("SET wait_timeout=72000");
	$db_main2->execute("SET wait_timeout=72000");

	// Web -> Mobile 전환율 
	try
	{
			
		$sql = " select pushcode ". 
		  		" ,count(useridx) as total_user_count ".
                " ,count(case when leavedays < 7 then useridx else null end) as return_7_count ". 
                " ,count(case when leavedays >= 7 and leavedays < 20   then useridx else null end) as return_720_count ".
                " ,count(case when leavedays >= 20 then useridx else null end) as return_20_count ".
                " ,count(case when money > 0 then useridx else null end) as pay_user_count ".
                " ,sum(money) as total_money ".
                " ,sum(buycount) as total_buycount ".
                " from (  ".
                " select * ".
                "     ,(select sum(money)  From t5_product_order_all where useridx = t5_push_event_log.useridx and  writedate > t5_push_event_log.writedate )  as money ". 
                "     ,(select count(org_orderidx)  From t5_product_order_all where useridx = t5_push_event_log.useridx and  writedate > t5_push_event_log.writedate )  as buycount ".
                "     From t5_push_event_log where pushcode like '%vip%' ".
                " ) group by pushcode";
	
		$conversion_list = $db_redshift->gettotallist($sql);
	
		for($i=0; $i<sizeof($conversion_list); $i++)
		{
		    $pushcode = $conversion_list[$i]["pushcode"];
			$total_user_count = $conversion_list[$i]["total_user_count"];
			$pay_user_count = $conversion_list[$i]["pay_user_count"];
			$total_money = ($conversion_list[$i]["total_money"]=="")?0: $conversion_list[$i]["total_money"];
			$total_buycount = ($conversion_list[$i]["total_buycount"]=="")?0:$conversion_list[$i]["total_buycount"];
			$return_7_count = $conversion_list[$i]["return_7_count"];
			$return_720_count = $conversion_list[$i]["return_720_count"];
			$return_20_count = $conversion_list[$i]["return_20_count"];
			
			$senddate = $db_mobile->getvalue("SELECT DATE_FORMAT(senddate,'%Y-%m-%d') FROM `tbl_individual_push` WHERE push_code = '$pushcode' AND STATUS = 1 LIMIT 1");
			$nopay_user_count = $total_user_count - $pay_user_count;
			$sql = " INSERT INTO tbl_individual_stat(push_code, senddate, usercount, return7count, return720count, return20count, payer, nopayer ,buycredit ,buycount) ".
			 			" VALUES('$pushcode','$senddate', $total_user_count, $return_7_count, $return_720_count, $return_20_count, $pay_user_count, $nopay_user_count, $total_money, $total_buycount) ".
			 			" ON DUPLICATE KEY UPDATE return7count=$return_7_count, return720count=$return_720_count, return20count=$return_20_count, usercount = $total_user_count, buycredit = $total_money, payer=$pay_user_count, nopayer=$nopay_user_count, buycount=$total_buycount";
			$db_main2->execute($sql);
		}

	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	$db_mobile->end();
	$db_main2->end();
	$db_redshift->end();
?>