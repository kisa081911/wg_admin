<?
	include("../../common/common_include.inc.php");	
	
	$slave_main = new CDatabase_Slave_Main();	
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();
	
	$slave_main->execute("SET wait_timeout=72000");
	$db_main2->execute("SET wait_timeout=72000");
	$db_other->execute("SET wait_timeout=72000");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	$str_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/user/vip_nopay90_request_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp)
		{
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/user/vip_nopay90_request_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("vip_nopay90_request_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);

		exit();
	}
	
	// VIP 최근 일주일 플레이 기록있고, 결제 45동안 없는 사용자 추출
	try
	{
		$sql = "SELECT useridx, MIN(d) AS d, AVG(c) AS c, SUM(n) AS n, SUM(web) AS web, SUM(ios) AS ios, SUM(android) AS android, SUM(amazon) AS amazon ".
				"FROM	". 
				"(	". 
				"	SELECT useridx, MIN(days_after_purchase) AS d, AVG(currentcoin) AS c, COUNT(useridx) AS n, 1 AS web, 0 AS ios, 0 AS android, 0 AS amazon	". 
				"	FROM tbl_user_playstat_daily	".  
				"	WHERE today >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND vip_level >= 5 GROUP BY useridx HAVING d >= 45	". 
				"	UNION ALL	". 
				"	SELECT useridx, MIN(days_after_purchase) AS d, AVG(currentcoin) AS c, COUNT(useridx) AS n, 0 AS web, 1 AS ios, 0 AS android, 0 AS amazon	". 
				"	FROM tbl_user_playstat_daily_ios	".  
				"	WHERE today >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND vip_level >= 5 GROUP BY useridx HAVING d >= 45	". 
				"	UNION ALL	". 
				"	SELECT useridx, MIN(days_after_purchase) AS d, AVG(currentcoin) AS c, COUNT(useridx) AS n, 0 AS web, 0 AS ios, 1 AS android, 0 AS amazon	". 
				"	FROM tbl_user_playstat_daily_android ".
				"	WHERE today >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND vip_level >= 5 GROUP BY useridx HAVING d >= 45	". 
				"	UNION ALL	". 
				"	SELECT useridx, MIN(days_after_purchase) AS d, AVG(currentcoin) AS c, COUNT(useridx) AS n, 0 AS web, 0 AS ios, 0 AS android, 1 AS amazon	". 
				"	FROM tbl_user_playstat_daily_amazon ".
				"	WHERE today >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND vip_level >= 5 GROUP BY useridx HAVING d >= 45	". 
				") a	".  
				"GROUP BY useridx	".  
				"HAVING d >= 45 AND n >= 2";
		
		$vip_userlist = $db_other->gettotallist($sql);
		
		$insert_sql = "";
		$noti_cnt = 0;
		$insert_cnt = 0;
		
		for($i=0; $i<sizeof($vip_userlist); $i++)
		{
			$useridx = $vip_userlist[$i]["useridx"];
			$web = $vip_userlist[$i]["web"];
			$ios = $vip_userlist[$i]["ios"];
			$android = $vip_userlist[$i]["android"];
			$amazon = $vip_userlist[$i]["amazon"];
			
			$sql = "SELECT COUNT(*) FROM tbl_nopay_vip_coupon WHERE useridx = $useridx AND is_buy = 1";
			$issue_user = $db_main2->getvalue($sql);
			
			if($issue_user < 1)
			{
				$sql = "SELECT ".
						"	(SELECT COUNT(*) FROM tbl_product_order WHERE useridx = $useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 14 DAY) LIMIT 1) + ".
						"	(SELECT COUNT(*) FROM tbl_product_order_mobile WHERE useridx = $useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 14 DAY) LIMIT 1) AS purchase_count ";
				$purchase_count = $slave_main->getvalue($sql);
				
				if($purchase_count == 0)
				{
					if($web == "1" && $useridx%2 == 0)
					{
						$sql = "SELECT userid FROM tbl_user WHERE useridx = $useridx";
						$facebookid = $slave_main->getvalue($sql);
						
						try
						{
							$facebook = new Facebook(array(
									'appId'  => FACEBOOK_APP_ID,
									'secret' => FACEBOOK_SECRET_KEY,
									'cookie' => true,
							));
						
							$session = $facebook->getUser();
						
							$template = "A 100% More Chips Coupon has been issued! Don't miss this great opportunity!";
						
							$args = array('template' => "$template",
									'href' => "?adflag=vip_coupon",
									'ref' => "group_vip_coupon");
						
							$info = $facebook->api("/$facebookid/notifications", "POST", $args);
						
							$noti_cnt++;
						
						}
						catch (FacebookApiException $e)
						{
							if($e->getMessage() == "Unsupported operation" || $e->getMessage() == "(#200) Cannot send notifications to a user who has not installed the app")
							{
								write_log("A2U Group VIP Coupon : ".$e->getMessage());
							}
						}
					}
					
					if($insert_sql == "")
						$insert_sql = "INSERT IGNORE INTO tbl_nopay_vip_coupon(useridx, web, ios, android, amazon, writedate) VALUES($useridx, $web, $ios, $android, $amazon, NOW())";
					else
						$insert_sql .= ", ($useridx, $web, $ios, $android, $amazon, NOW())";
					
					$insert_cnt++;
					
					if($i%500 == 0)
					{
						$db_main2->execute($insert_sql);
						$insert_sql = "";
					}
				}
			}
		}
		
		if($insert_sql != "")
			$db_main2->execute($insert_sql);
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	write_log("VIP Nopay Coupon Noti Send Count : ".$noti_cnt);
	write_log("VIP Nopay Coupon Insert Count : ".$insert_cnt);	

	
	$slave_main->end();
	$db_main2->end();
	$db_other->end();
?>