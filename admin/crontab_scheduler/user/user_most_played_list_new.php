<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/user/user_most_played_list_new") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/user/user_most_played_list_new") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("user_most_played_list Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	$db_slave_main = new CDatabase_Slave_Main();
	//$db_slave_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	
	try
	{
		for($u = 0; $u < 10; $u++)
		{
			$sql = "SELECT useridx FROM tbl_user WHERE logindate >= DATE_SUB(NOW(), INTERVAL 14 DAY) AND useridx % 10= $u;";
			$useridx_list = $db_slave_main->gettotallist($sql);
			
			$sleep_count = 0;
			$insert_sql = "";
			
			for($i=0; $i<sizeof($useridx_list); $i++)
			{
				$useridx = $useridx_list[$i]["useridx"];
				
				$sql = "select useridx, slottype, max(moneyin) AS moneyin ".
						"from ".
						"(	".
						"	select useridx, if(slottype = 25||slottype = 26||slottype = 27||slottype = 29||slottype = 30, 25, slottype) as slottype, sum(moneyin) as moneyin	".
						"	from	".
						"	(	".
						"		select useridx, slottype, moneyin from `tbl_user_stat` WHERE useridx = $useridx ".
						"		union all	".
						"		SELECT useridx, slottype, moneyin FROM `tbl_user_stat_ios` WHERE useridx = $useridx	".
						"		UNION ALL	".
						"		SELECT useridx, slottype, moneyin FROM `tbl_user_stat_android` WHERE useridx = $useridx ".
						"		UNION ALL	".
						"		SELECT useridx, slottype, moneyin FROM `tbl_user_stat_amazon` WHERE useridx = $useridx	".
						"	) t1 group by useridx, slottype order by sum(moneyin) desc	".
						") t2 group by slottype ORDER BY moneyin DESC limit 20;";
				$user_slot_info_list = $db_slave_main->gettotallist($sql);			
				
				$sleep_count++;
				$insert_slottype = "";
				
				for($j=0; $j<sizeof($user_slot_info_list); $j++)
				{
					$useridx = $user_slot_info_list[$j]["useridx"];
					$slottype = $user_slot_info_list[$j]["slottype"];

					if($insert_slottype == "")
					{
						$insert_slottype = $slottype;
					}
					else
					{
						$insert_slottype .= ",".$slottype;
					}
				}			

				if($insert_sql == "" && $insert_slottype != "")
					$insert_sql = "INSERT INTO tbl_user_most_played_list_$u(useridx, slottype, writedate) VALUES($useridx, '$insert_slottype', NOW()) ";
				else if($insert_slottype != "")
					$insert_sql .= ",($useridx, '$insert_slottype', NOW())";
				
				if($sleep_count == 50)
				{
					if($insert_sql != "")
					{
						$insert_sql .= "  ON DUPLICATE KEY UPDATE slottype=VALUES(slottype), writedate=VALUES(writedate)";
						$db_main2->execute($insert_sql);
					}
					
					$insert_sql = "";
					$sleep_count = 0;
					
					sleep(1);
				}
								
			}
			
			if($insert_sql != "")
			{
				$insert_sql .= "  ON DUPLICATE KEY UPDATE slottype=VALUES(slottype), writedate=VALUES(writedate)";
				$db_main2->execute($insert_sql);
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_slave_main->end();
	$db_main2->end();
?>