<?
	include("../../common/common_include.inc.php");
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/user_static/firstbuy_user_action_log") !== false)
		{
			$count++;
		}
	}
	
	ini_set("memory_limit", "-1");

	$issuccess = "1";
	
	if ($count > 1)
		exit();
	
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_main = new CDatabase_Main();
	
	$db_analysis->execute("SET wait_timeout=72000");
	$db_main->execute("SET wait_timeout=72000");
	$db_main2->execute("SET wait_timeout=72000");

	try
	{
	    $sql = "SELECT useridx FROM tbl_user_first_order WHERE purchase_date > DATE_SUB(NOW(),INTERVAL 9 DAY) AND useridx > 20000";
	    $firstbuy_user_list = $db_main2->gettotallist($sql);
	    
	    for($i=0; $i < sizeof($firstbuy_user_list); $i++)
	    {
	        $useridx = $firstbuy_user_list[$i]["useridx"];
	        
	        $sql=" SELECT writedate, 0 AS os_type  FROM tbl_product_order WHERE useridx  = $useridx AND STATUS = 1 ".  
	   	         " UNION ALL ".
                 " SELECT writedate, os_type  FROM tbl_product_order_mobile WHERE useridx  = $useridx AND STATUS = 1 ". 
                 " ORDER BY writedate DESC LIMIT 1";
	        $firstbuy_info = $db_main->getarray($sql);
	        
	        $firstbuy_date = $firstbuy_info['writedate'];
	        $firstbuy_os_type = $firstbuy_info['os_type'];
	        
	        $sql="SELECT * from tbl_user WHERE useridx = $useridx";
	        $user_info = $db_main->getarray($sql);
	        
	        $userid = $user_info["userid"];
	        $createdate = $user_info["createdate"];
	        $logindate = $user_info["logindate"];
	        $buyafter_login = ($firstbuy_date < $logindate)? 1 : 0;
	        
	        if($userid < 0 || $userid == "")
	        {
	            $sql = "SELECT * ". 
                       "  FROM ( ".
                       "        SELECT actionidx,ipaddress,category,category_info,ACTION,action_info,staytime,isplaynow,coin, 0 AS is_v2,writedate FROM user_action_log_android_".($useridx%10).
                       "        WHERE writedate >= '$firstbuy_date' AND useridx = $useridx ".
                       "        UNION ALL ".  
                       "        SELECT actionidx,ipaddress,category,category_info,ACTION,action_info,staytime,isplaynow,coin, 0 AS is_v2,writedate FROM user_action_log_ios_".($useridx%10).
                       "        WHERE writedate >= '$firstbuy_date' AND useridx = $useridx ".
                       "        UNION ALL ".
                       "        SELECT   actionidx,ipaddress,category,category_info,ACTION,action_info,staytime,isplaynow,coin, 0 AS is_v2,writedate FROM user_action_log_amazon ".
                       "        WHERE writedate >= '$firstbuy_date'  AND useridx = $useridx ".
                     " )t1";
	        }
	        else
	        {
	            $sql = " SELECT * FROM ( ".
	   	               "                   SELECT   actionidx,ipaddress,category,category_info,ACTION,action_info,staytime,isplaynow,coin,is_v2,writedate FROM user_action_log_".($userid%10).
                       "                   WHERE writedate >= '$firstbuy_date' AND facebookid = $userid ".
                       "                   UNION ALL ".
                       "                   SELECT actionidx,ipaddress,category,category_info,ACTION,action_info,staytime,isplaynow,coin, 0 AS is_v2,writedate FROM user_action_log_android_".($useridx%10).
                       "                   WHERE writedate >= '$firstbuy_date' AND useridx = $useridx ".
                       "                   UNION ALL ".
                       "                   SELECT actionidx,ipaddress,category,category_info,ACTION,action_info,staytime,isplaynow,coin, 0 AS is_v2,writedate FROM user_action_log_ios_".($useridx%10).
                       "                   WHERE writedate >= '$firstbuy_date' AND useridx = $useridx ".
                       "                   UNION ALL ".
                       "                   SELECT  actionidx,ipaddress,category,category_info,ACTION,action_info,staytime,isplaynow,coin, 0 AS is_v2,writedate FROM user_action_log_amazon ".
                       "                   WHERE writedate >= '$firstbuy_date' AND useridx = $useridx ".
                       " )t1";
	        }
	        
	        $insert_cnt = 0;
	        
	        $action_list = $db_analysis->gettotallist($sql);
		
	        $insert_sql = "";
		
	        for($p=0; $p < sizeof($action_list); $p++)
	        {
    	        $insert_cnt++;
    	        
    	        $actionidx = $action_list[$p]["actionidx"];
    	        $ipaddress = $action_list[$p]["ipaddress"];
    	        
    	        $category= encode_db_field($action_list[$p]["category"]);
    	        $category_info= encode_db_field($action_list[$p]["category_info"]);
    	        $acion= encode_db_field($action_list[$p]["ACTION"]);
    	        $action_info= encode_db_field($action_list[$p]["action_info"]);
    	        
    	        $staytime= $action_list[$p]["staytime"];
    	        $isplaynow= $action_list[$p]["isplaynow"];
    	        $coin= $action_list[$p]["coin"];
    	        $is_v2= $action_list[$p]["is_v2"];
    	        $writedate= $action_list[$p]["writedate"];
    	        
    	        if($insert_sql == "")
    	        {
    	            $insert_sql = " INSERT IGNORE INTO _tmp_firstbuy_user_action_log(useridx, join_date, buy_date, buy_platform, buyafter_login, actionidx, ipaddress, category, category_info, ACTION, action_info, staytime, isplaynow, coin, is_v2, writedate)".
        	            " VALUES('$useridx', '$createdate', '$firstbuy_date', '$firstbuy_os_type', '$buyafter_login', '$actionidx', '$ipaddress', '$category', '$category_info', '$acion', '$action_info', '$staytime', '$isplaynow', '$coin', '$is_v2', '$writedate' )";
    	        }
    	        else
    	        {
    	            $insert_sql .= " ,('$useridx', '$createdate', '$firstbuy_date', '$firstbuy_os_type', '$buyafter_login', '$actionidx', '$ipaddress', '$category', '$category_info', '$acion', '$action_info', '$staytime', '$isplaynow', '$coin', '$is_v2', '$writedate')";
    	        }
    	        
    	        if($insert_cnt == 5000)
    	        {
    	             	        $db_analysis->execute($insert_sql);
				write_log($insert_cnt);

    	            
    	            $insert_cnt = 0;
    	            $insert_sql = "";
    	        }
    	        
	        }
    	    if($insert_sql != ""){
     	        $db_analysis->execute($insert_sql);
    	        write_log($insert_cnt);
    	        
    	    }
	        
	    }
	    
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	$db_main->end();
	$db_analysis->end();
	$db_main2->end();
?>