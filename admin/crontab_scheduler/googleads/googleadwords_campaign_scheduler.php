<?
	include("../../common/common_include.inc.php");

	// Include the initialization file
	require_once 'init.php';
	require_once 'googleads-php-lib-master/src/Google/Api/Ads/AdWords/Util/v201609/ReportUtils.php';
	
	//$db_main2 = new CDatabase4();
	$db_main2 = new CDatabase_Main2();
	$db_main2->execute("SET wait_timeout=7200");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/googleads/googleadwords_campaign_scheduler") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/googleads/googleadwords_campaign_scheduler") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("googleads/googleadwords_campaign_scheduler Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	$today = date("Y-m-d", strtotime("-1 day"));
	
	function DownloadCriteriaReportExample(AdWordsUser $user, $filePath, $dateRange)
	{
		// Load the service, so that the required classes are available.
		$user->LoadService('ReportDefinitionService', ADWORDS_VERSION);
		
		// Create selector.
		$selector = new Selector();
		$selector->dateRange = array ('min' => $dateRange, 'max' => $dateRange);
		$selector->fields = array (
				'AccountCurrencyCode',
				'AccountDescriptiveName',
				'ActiveViewCpm',
				'ActiveViewCtr',
				'ActiveViewImpressions',
				'ActiveViewMeasurability',
				'ActiveViewMeasurableCost',
				'ActiveViewMeasurableImpressions',
				'ActiveViewViewability',
				'AdNetworkType1',
				'AdNetworkType2', // [10]
				'AdvertisingChannelSubType',
				'AdvertisingChannelType',
				'Amount',
				'AverageCost',
				'AverageCpc',
				'AverageCpm',
				'AverageCpv',
				'AveragePosition',
				'BaseCampaignId',
				'BiddingStrategyId', // [20]
				'BiddingStrategyName',
				'BiddingStrategyType',
				'BudgetId',
				'CampaignId',
				'CampaignName',
				'CampaignStatus',
				'CampaignTrialType',
				'Clicks',
				'ConversionRate',
				'Conversions', // [30]
				'Cost',
				'CostPerConversion',
				'Ctr',
				'CustomerDescriptiveName',
				'EngagementRate',
				'Engagements',
				'EnhancedCpcEnabled',
				'EnhancedCpvEnabled',
				'ExternalCustomerId',
				'Impressions', // [40]
				'InteractionRate',
				'Interactions',
				'InteractionTypes',
				'IsBudgetExplicitlyShared',
				'LabelIds',
				'Labels',
				'OfflineInteractionRate',
				'ServingStatus',
				'StartDate',
				'VideoQuartile100Rate', // [50]
				'VideoQuartile25Rate',
				'VideoQuartile50Rate',
				'VideoQuartile75Rate',
				'VideoViewRate',
				'VideoViews'
		);
		
		// Create report definition.
		$reportDefinition = new ReportDefinition();
		$reportDefinition->selector = $selector;
		$reportDefinition->reportName = 'Criteria performance report #'.uniqid();
		$reportDefinition->dateRangeType = 'CUSTOM_DATE';
		$reportDefinition->reportType = 'CAMPAIGN_PERFORMANCE_REPORT';
		$reportDefinition->downloadFormat = 'CSV';
		
		// Set additional options.
		$options = array ('version' => ADWORDS_VERSION);
		
		// Download report.
		$reportUtils = new ReportUtils();
		
		return $reportUtils->DownloadReport($reportDefinition, $filePath, $user, $options);
	}
	
	function DownloadCriteriaReportRevenue(AdWordsUser $user, $filePath, $dateRange)
	{
		// Load the service, so that the required classes are available.
		$user->LoadService('ReportDefinitionService', ADWORDS_VERSION);
		
		// Create selector.
		$selector = new Selector();
		$selector->dateRange = array ('min' => $dateRange, 'max' => $dateRange);
		$selector->fields = array (
				'AccountCurrencyCode',
				'ConversionTypeName',
				'AllConversionValue',
				'CampaignId',
				'CampaignName',
				'AdNetworkType2'
		);
		
		// Create report definition.
		$reportDefinition = new ReportDefinition();
		$reportDefinition->selector = $selector;
		$reportDefinition->reportName = 'Criteria performance report #'.uniqid();
		$reportDefinition->dateRangeType = 'CUSTOM_DATE';
		$reportDefinition->reportType = 'CAMPAIGN_PERFORMANCE_REPORT';
		$reportDefinition->downloadFormat = 'CSV';
		
		// Set additional options.
		$options = array ('version' => ADWORDS_VERSION);
		
		// Download report.
		$reportUtils = new ReportUtils();
		
		return $reportUtils->DownloadReport($reportDefinition, $filePath, $user, $options);
	}
	
	try
	{
		// Get AdWordsUser from credentials in "../auth.ini"
		// relative to the AdWordsUser.php file's directory.
		$user = new AdWordsUser();
		
		// Download the report to a file in the same directory as the example.
		$filePath = dirname(__FILE__).'/report.csv';
		// Run the example.
		$dateRange = str_replace('-', '', $today);
		
		$response = DownloadCriteriaReportExample($user, $filePath, $dateRange);
		
		$csvArray = explode(')"', $response);
		array_shift($csvArray);
		$newCsv = implode(",", $csvArray);
		
		$array = array_map("str_getcsv", explode("\n", $newCsv));
		$json_obj = json_encode($array);
		
		$temp_count = 0;
		$googleadwords_spend = 0;
		
		foreach($array as $data)
		{
			if((($temp_count++) < 1) || ($data[0] == "Total" || $data[0] == "Currency" || $data[0] == ""))
				continue;
			
			$googleadwords_spend += $data[31];
		}
		
		// Agency 비용 테이블에 데이터 넣기
		$googleadwords_spend /= 1000000;
		
		if($googleadwords_spend != 0)
		{
			$sql = "INSERT INTO tbl_agency_spend_daily VALUES ('$today', 2, 22, 'googleadwords_int', $googleadwords_spend) ON DUPLICATE KEY UPDATE spend = $googleadwords_spend;";
			$db_main2->execute($sql);
		}
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
?>