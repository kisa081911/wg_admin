<?
	include("../../common/common_include.inc.php");
	include("../../common/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");

	$today = date("Y-m-d");
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$yesterday_str = date("Ymd", time() - 60 * 60 * 24 * 1);
	
	$yesterday = "2015-01-01";
	$yesterday_str = "20150101";

	try
	{
		$db4_slave = new CDatabase_Slave_Main4();
		$db4_slave->execute("SET wait_timeout=3600");
		
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		
		$sql = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name='tbl_user_mobile_appsflyer_new'";
		$columns_count = $db4_slave->getvalue($sql);
		
		$sql = " SELECT useridx, appsflyerid, os_type, deviceid, advid, writedate ".
				"	FROM tbl_user_mobile_appsflyer_new ";		
		$user_mobile_appsflye_list = $db4_slave->gettotallist($sql);
		
		for($i=0; $i<sizeof($user_mobile_appsflye_list); $i++)
		{
		  $output = ""; 
		  
		  for($j=0; $j<$columns_count; $j++)
			{
				$user_mobile_appsflye_list[$i][$j] = str_replace("\"", "\"\"", $user_mobile_appsflye_list[$i][$j]);
				
				if($user_mobile_appsflye_list[$i][$j] == "0000-00-00 00:00:00")
					$user_mobile_appsflye_list[$i][$j] = "1900-01-01 00:00:00";
					
				if($j == $columns_count - 1)
					$output .= '"'.$user_mobile_appsflye_list[$i][$j].'"';
				else
					$output .= '"'.$user_mobile_appsflye_list[$i][$j].'"|';
			}
			$output .="\n";
			
			if($output != "")
			{
				$fp = fopen("$DOCUMENT_ROOT/redshift_temp/tbl_user_mobile_appsflyer_new_$yesterday_str.txt", 'a+');
					
				fwrite($fp, $output);
					
				fclose($fp);
			}
	  }
		  
		
		$db4_slave->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		// Create the AWS service builder, providing the path to the config file
		$aws = Aws::factory('../../common/aws_sdk/aws_config.php');
	
		$s3Client = $aws->get('s3');
		$bucket = "wg-redshift/duc/tbl_user_mobile_appsflyer_new/$yesterday_str";
	
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $DOCUMENT_ROOT."/redshift_temp/tbl_user_mobile_appsflyer_new_$yesterday_str.txt";
	
		// Upload an object to Amazon S3
		$result = $s3Client->putObject(array(
				'Bucket' 		=> $bucket,
				'Key'    		=> 'tbl_user_mobile_appsflyer_new_'.$yesterday_str.'.txt',
				'ACL'	 		=> 'public-read',
				'SourceFile'   	=> $filepath
		));
	
		$ObjectURL = $result["ObjectURL"];
	
		if($ObjectURL != "")
		{
			@unlink($filepath);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$db_redshift = new CDatabase_Redshift();
	
		// Create a staging table
		$sql = "CREATE TABLE duc_user_mobile_appsflyer_new_tmp (LIKE duc_user_mobile_appsflyer_new);";
		$db_redshift->execute($sql);
	
		// Load data into the staging table
		$sql = "copy duc_user_mobile_appsflyer_new_tmp ".
				"from 's3://wg-redshift/duc/tbl_user_mobile_appsflyer_new/$yesterday_str' ".
				"credentials 'aws_access_key_id=AKIAIJF4EK5FD6Z5SVYQ;aws_secret_access_key=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' ".
				"delimiter '|' ".
				"csv;";
		$db_redshift->execute($sql);
	
		// Update records
		$sql = "UPDATE duc_user_mobile_appsflyer_new ".
				"SET os_type = s.os_type, deviceid = s.deviceid, advid = s.advid, writedate = s.writedate ".
				"FROM duc_user_mobile_appsflyer_new_tmp s ".
				"WHERE duc_user_mobile_appsflyer_new.useridx = s.useridx AND duc_user_mobile_appsflyer_new.appsflyerid = s.appsflyerid;";
		$db_redshift->execute($sql);
	
		// Insert records
		$sql = "INSERT INTO duc_user_mobile_appsflyer_new ".
				"SELECT s.* FROM duc_user_mobile_appsflyer_new_tmp s LEFT JOIN duc_user_mobile_appsflyer_new ".
				"ON s.useridx = duc_user_mobile_appsflyer_new.useridx AND s.appsflyerid = duc_user_mobile_appsflyer_new.appsflyerid ".
				"WHERE duc_user_mobile_appsflyer_new.useridx IS NULL AND duc_user_mobile_appsflyer_new.appsflyerid IS NULL;";
		$db_redshift->execute($sql);
			
		// Drop the staging table
		$sql = "DROP TABLE duc_user_mobile_appsflyer_new_tmp;";
		$db_redshift->execute($sql);
	
		$db_redshift->end();
	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
?>