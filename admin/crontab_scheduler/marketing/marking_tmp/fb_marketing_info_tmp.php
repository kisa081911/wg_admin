<?
include("../../../common/common_include.inc.php");

$db_main2 = new CDatabase_Main2();
$db_analysis = new CDatabase_Analysis();

$db_main2->execute("SET wait_timeout=72000");
$db_analysis->execute("SET wait_timeout=72000");

$result = array();
exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);

$count = 0;

for ($i=0; $i<sizeof($result); $i++)
{
    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/marketing/fb_marketing_info") !== false)
    {
        $count++;
    }
}

if ($count > 1)
{
    $count = 0;
    
    $killcontents = "#!/bin/bash\n";
    
    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
    
    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
    
    flock($fp, LOCK_EX);
    
    if (!$fp) {
        echo "Fail";
        exit;
    }
    else
    {
        echo "OK";
    }
    
    flock($fp, LOCK_UN);
    
    $content = "#!/bin/bash\n";
    
    for ($i=0; $i<sizeof($result); $i++)
    {
        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/marketing/fb_marketing_info") !== false)
        {
            $process_list = explode("|", $result[$i]);
            
            $content .= "kill -9 ".$process_list[0]."\n";
            
            write_log("marketing/fb_marketing_info Dead Lock Kill!");
        }
    }
    
    fwrite($fp, $content, strlen($content));
    fclose($fp);
    
    exit();
}

// $sql = "SELECT accesstoken FROM page_accesstoken WHERE userid = '0'";
// $acces_token = $db_analysis->getvalue($sql);
   $acces_token = "EAAFZALAjbPT8BAJI54Tr6ZAmaU93gEqQQXfJZAfOv1ugRFEUt14kVdLFEwHYhQfXCQ6xacBiRsZCTbZAIZANKTlUjYgbDDVyZBTwEHgVSWhlyNef5hTyO5JvGOqgevyQtlUFE19xoq1c6co6crISFZAySqaDlMmv8kQZD";



$db_analysis->end();

$today = "2021-02-03";
write_log("today - ".$today);
	// facebook web marketing info
	function getsql($datearr,$today)
	{
		$sql = "";
		foreach ($datearr as $datearrObj)
		{
			//캠패인 정보
			$campaign_reach = 0;
			$campaign_name = $datearrObj->{'campaign'}->{'name'};
			$campaign_id = $datearrObj->{'campaign'}->{'id'};
			$objective = $datearrObj->{'campaign'}->{'objective'};
			$campaign_effective_status = $datearrObj->{'campaign'}->{'effective_status'};
			
			if(isset($datearrObj->{'campaign'}->{'insights'}))
			{
				$campaign_reach = $datearrObj->{'campaign'}->{'insights'}->{'data'}[0]->{'reach'};
			}
			
			//광고 세트 정보
			$adset_reach = 0;
			$daily_budget =0;
			$adset_name = $datearrObj->{'adset'}->{'name'};
			$adset_id = $datearrObj->{'adset'}->{'id'};
			$adset_effective_status = $datearrObj->{'adset'}->{'effective_status'};
			$adset_configured_status = $datearrObj->{'adset'}->{'configured_status'};
			$start_time = date("Y-m-d H:i:s"  ,strtotime($datearrObj->{'adset'}->{'start_time'}));
			
			if(isset($datearrObj->{'adset'}->{'insights'}))
			{
				$adset_reach = $datearrObj->{'adset'}->{'insights'}->{'data'}[0]->{'reach'};
			}
			if(isset($datearrObj->{'campaign'}->{'insights'}))
			{
				$daily_budget = $datearrObj->{'adset'}->{'daily_budget'};
			}
			
			//광고 정보
			$ad_id = $datearrObj->{'id'};
			$ad_name = $datearrObj->{'name'};
			$ad_effective_status = $datearrObj->{'effective_status'};
			$ad_configured_status = $datearrObj->{'configured_status'};
			$created_time = date("Y-m-d H:i:s"  ,strtotime($datearrObj->{'created_time'}));
			$updated_time = date("Y-m-d H:i:s"  ,strtotime($datearrObj->{'updated_time'}));
			$status = $datearrObj->{'status'};
			$thumbnail_url ="";
			
			if(isset($datearrObj->{'adcreatives'}->{'data'}[0]->{'thumbnail_url'}))
			{
				$thumbnail_url = $datearrObj->{'adcreatives'}->{'data'}[0]->{'thumbnail_url'};
			}
			if(isset($datearrObj->{'insights'}))
			{
				foreach ($datearrObj->{'insights'}->{'data'} as $insights)
				{
					$app_install_cost = 0;
					$app_install_result = 0;
					if(isset($insights->{'cost_per_action_type'}))
					{
						foreach ($insights->{'cost_per_action_type'} as $cost_per_action_type)
						{
							if($cost_per_action_type->{'action_type'} == "app_install")
							{
								$app_install_cost = $cost_per_action_type->{'value'};
							}
						}
					}
						
					if(isset($insights->{'actions'}))
					{
						foreach ($insights->{'actions'} as $cost_per_action_type)
						{
							if($cost_per_action_type->{'action_type'} == "app_install")
							{
								$app_install_result = $cost_per_action_type->{'value'};
							}
						}
					}
						
					$relevance_score=0;
						
					if(isset($insights->{'relevance_score'}->{'score'}))
					{
						$relevance_score =$insights->{'relevance_score'}->{'score'};
					}
						
					$impressions = $insights->{'impressions'};
					$cpm = $insights->{'cpm'};
					$cpc = $insights->{'cpc'};
					$ad_reach = $insights->{'reach'};
					$clicks = $insights->{'clicks'};
					$ctr = $insights->{'ctr'};
					$spend = $insights->{'spend'};
						
					$sql .=" INSERT INTO tbl_ad_stats(today, ad_id, ad_name, adset_id, adset_name, campaign_id, campaign_name, impressions, cpm, cpc, ad_reach, adset_reach, campaign_reach, app_install_result ".
							" , app_install_cost, clicks, ctr, spend, ad_effective_status, ad_configured_status, adset_effective_status, adset_configured_status, campaign_effective_status ".
							" , status, objective, relevance_score, created_time, updated_time, start_time,thumbnail_url, daily_budget) ".
							" VALUES('$today','$ad_id','$ad_name','$adset_id','$adset_name','$campaign_id','$campaign_name','$impressions' ".
							" ,'$cpm','$cpc','$ad_reach','$adset_reach','$campaign_reach','$app_install_result','$app_install_cost','$clicks','$ctr','$spend', '$ad_effective_status', '$ad_configured_status' ".
							" ,'$adset_effective_status', '$adset_configured_status', '$campaign_effective_status', '$status','$objective',$relevance_score,'$created_time' ".
							" ,'$updated_time','$start_time','$thumbnail_url','$daily_budget') ".
							" ON DUPLICATE KEY UPDATE ad_name = '$ad_name', adset_name ='$adset_name', campaign_name = '$campaign_name',impressions = '$impressions', cpm = '$cpm', cpc ='$cpc' ".
							" , ad_reach = '$ad_reach',adset_reach = '$adset_reach', campaign_reach = '$campaign_reach', app_install_result = '$app_install_result' ".
							" , app_install_cost = '$app_install_cost', clicks = '$clicks', ctr = '$ctr', spend = '$spend', ad_effective_status='$ad_effective_status' ".
							" , ad_configured_status='$ad_configured_status', adset_effective_status='$adset_effective_status', adset_configured_status='$adset_configured_status' ".
							" , campaign_effective_status='$campaign_effective_status', thumbnail_url='$thumbnail_url' ".
							" ,status = '$status', objective = '$objective', relevance_score = $relevance_score, updated_time = '$updated_time', start_time = '$start_time', daily_budget='$daily_budget' ; ";
				}
			}
		}
		
		return $sql;
	}
	
	//take5_web : 613514812114027
	try
	{
		set_time_limit(60 * 60);
//		$facebook_json = file_get_contents('https://graph.facebook.com/v2.10/act_613514812114027/ads?fields=insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach,cost_per_action_type,actions,relevance_score,impressions,cpm,cpc,clicks,ctr,spend},campaign{name,objective,effective_status,insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach}},adset{name,start_time,configured_status,effective_status,insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach},daily_budget},name,created_time,updated_time,configured_status,effective_status,status&limit=500&access_token='.$acces_token);
		
		$url = 'https://graph.facebook.com/v8.0/act_613514812114027/ads?fields=insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach,cost_per_action_type,actions,impressions,cpm,cpc,clicks,ctr,spend},campaign{name,objective,effective_status,insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach}},adset{name,start_time,configured_status,effective_status,insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach},daily_budget},name,created_time,updated_time,effective_status,status&limit=50&insights.spend>0&access_token='.$acces_token;
		write_log($url);
		$cSession = curl_init();
		
		curl_setopt($cSession,CURLOPT_URL, $url);
		curl_setopt($cSession,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($cSession,CURLOPT_HEADER, false);
		
		$facebook_json = curl_exec($cSession);
		curl_close($cSession);
		
		$facebook_objs = json_decode($facebook_json);
		
		$datearr = $facebook_objs->{'data'};
		$sql ="";
		$db_main2->execute(getsql($datearr,$today));		
		$next= $facebook_objs->{'paging'}->{'next'};
		$flag = true;
		
		if(isset($facebook_objs->{'paging'}->{'next'}))
		{
			while($flag)
			{
				$facebook_json = file_get_contents($next);
				$facebook_objs = json_decode($facebook_json);
				$db_main2->execute(getsql($datearr,$today));				
				
				if(isset($facebook_objs->{'paging'}->{'next'}))
				{
					$next= $facebook_objs->{'paging'}->{'next'};	
				}
				else 
				{
					$flag = false;
				}
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	// facebook mobile marketing info
	function mobile_getsql($datearr,$today)
	{
		$sql = "";
		foreach ($datearr as $datearrObj)
		{		
			//캠패인 정보
			$campaign_reach = 0;
			$campaign_name = $datearrObj->{'campaign'}->{'name'};
			$campaign_id = $datearrObj->{'campaign'}->{'id'};
			$objective = $datearrObj->{'campaign'}->{'objective'};
			$campaign_effective_status = $datearrObj->{'campaign'}->{'effective_status'};
			
			if(isset($datearrObj->{'campaign'}->{'insights'}))
			{
				$campaign_reach = $datearrObj->{'campaign'}->{'insights'}->{'data'}[0]->{'reach'};
			}
			
			//광고 세트 정보
			$adset_reach = 0;
			$daily_budget =0;
			$adset_name = $datearrObj->{'adset'}->{'name'};
			$adset_id = $datearrObj->{'adset'}->{'id'};
			$adset_effective_status = $datearrObj->{'adset'}->{'effective_status'};
			$adset_configured_status = $datearrObj->{'adset'}->{'configured_status'};
			$start_time = date("Y-m-d H:i:s"  ,strtotime($datearrObj->{'adset'}->{'start_time'}));
			
			if(isset($datearrObj->{'adset'}->{'insights'}))
			{
				$adset_reach = $datearrObj->{'adset'}->{'insights'}->{'data'}[0]->{'reach'};
			}
			
			if(isset($datearrObj->{'campaign'}->{'insights'}))
			{
				$daily_budget = $datearrObj->{'adset'}->{'daily_budget'};
			}
			
			//광고 정보
			$ad_id = $datearrObj->{'id'};
			$ad_name = $datearrObj->{'name'};
			$ad_effective_status = $datearrObj->{'effective_status'};
			$ad_configured_status = $datearrObj->{'configured_status'};
			$created_time = date("Y-m-d H:i:s"  ,strtotime($datearrObj->{'created_time'}));
			$updated_time = date("Y-m-d H:i:s"  ,strtotime($datearrObj->{'updated_time'}));
			$status = $datearrObj->{'status'};
			
			$thumbnail_url ="";
			
			if(isset($datearrObj->{'adcreatives'}->{'data'}[0]->{'thumbnail_url'}))
			{
				$thumbnail_url = $datearrObj->{'adcreatives'}->{'data'}[0]->{'thumbnail_url'};
			}	
			
			if(isset($datearrObj->{'insights'}))
			{
				foreach ($datearrObj->{'insights'}->{'data'} as $insights)
				{
					$app_install_cost = 0;
					$app_install_result = 0;
					if(isset($insights->{'cost_per_action_type'}))
					{
						foreach ($insights->{'cost_per_action_type'} as $cost_per_action_type)
						{
							if($cost_per_action_type->{'action_type'} == "mobile_app_install")
							{
								$app_install_cost = $cost_per_action_type->{'value'};
							}
						}
					}
	
					if(isset($insights->{'actions'}))
					{
						foreach ($insights->{'actions'} as $cost_per_action_type)
						{
							if($cost_per_action_type->{'action_type'} == "mobile_app_install")
							{
								$app_install_result = $cost_per_action_type->{'value'};
							}
						}
					}
						
					$relevance_score=0;
						
					if(isset($insights->{'relevance_score'}->{'score'}))
					{
						$relevance_score =$insights->{'relevance_score'}->{'score'};
					}
						
					$impressions = $insights->{'impressions'};
					$cpm = $insights->{'cpm'};
					$cpc = $insights->{'cpc'};
					$ad_reach = $insights->{'reach'};
					$clicks = $insights->{'clicks'};
					$ctr = $insights->{'ctr'};
					$spend = $insights->{'spend'};
					
					$platform = 0;
					if(preg_match("/ios/i",$campaign_name))
					{
						$platform = 1;
					}
					else if(preg_match("/android/i",$campaign_name))
					{
						$platform = 2;
					}
					else if(preg_match("/amazon/i",$campaign_name))
					{
						$platform = 3;
					}
					
					$sql .=" INSERT INTO tbl_ad_stats_mobile(today, ad_id, ad_name, adset_id, adset_name, campaign_id, campaign_name, platform, impressions, cpm, cpc, ad_reach, adset_reach, campaign_reach, app_install_result ".
							" , app_install_cost, clicks, ctr, spend, ad_effective_status, ad_configured_status, adset_effective_status, adset_configured_status, campaign_effective_status ".
							" , status, objective, relevance_score, created_time, updated_time, start_time,thumbnail_url, daily_budget) ".
							" VALUES('$today','$ad_id','$ad_name','$adset_id','$adset_name','$campaign_id','$campaign_name',$platform,'$impressions' ".
							" ,'$cpm','$cpc','$ad_reach','$adset_reach','$campaign_reach','$app_install_result','$app_install_cost','$clicks','$ctr','$spend', '$ad_effective_status', '$ad_configured_status' ".
							" ,'$adset_effective_status', '$adset_configured_status', '$campaign_effective_status', '$status','$objective',$relevance_score,'$created_time' ".
							" ,'$updated_time','$start_time','$thumbnail_url','$daily_budget') ".
							" ON DUPLICATE KEY UPDATE platform=VALUES(platform), ad_name = '$ad_name', adset_name ='$adset_name', campaign_name = '$campaign_name',impressions = '$impressions', cpm = '$cpm', cpc ='$cpc' ".
							" , ad_reach = '$ad_reach',adset_reach = '$adset_reach', campaign_reach = '$campaign_reach', app_install_result = '$app_install_result' ".
							" , app_install_cost = '$app_install_cost', clicks = '$clicks', ctr = '$ctr', spend = '$spend', ad_effective_status='$ad_effective_status' ".
							" , ad_configured_status='$ad_configured_status', adset_effective_status='$adset_effective_status', adset_configured_status='$adset_configured_status' ".
							" , campaign_effective_status='$campaign_effective_status', thumbnail_url='$thumbnail_url' ".
							" ,status = '$status', objective = '$objective', relevance_score = $relevance_score, updated_time = '$updated_time', start_time = '$start_time', daily_budget='$daily_budget' ; ";
				}
			}
		}
		return $sql;
	}
	
	//take5_mobile : 805348042930702
	try
	{
		//$facebook_json = file_get_contents('https://graph.facebook.com/v2.10/act_805348042930702/ads?fields=insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach,cost_per_action_type,actions,impressions,cpm,cpc,clicks,ctr,spend},campaign{name,objective,effective_status,insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach}},adset{name,start_time,configured_status,effective_status,insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach},daily_budget},name,created_time,updated_time,configured_status,effective_status,status}&limit=500&access_token='.$acces_token);
		$url = 'https://graph.facebook.com/v8.0/act_805348042930702/ads?fields=insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach,cost_per_action_type,actions,impressions,cpm,cpc,clicks,ctr,spend},campaign{name,objective,effective_status,insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach}},adset{name,start_time,configured_status,effective_status,insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach},daily_budget},name,created_time,updated_time,configured_status,effective_status,status&limit=50&insights.spend>0&access_token='.$acces_token;
		
		$cSession = curl_init();
		
		curl_setopt($cSession,CURLOPT_URL, $url);
		curl_setopt($cSession,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($cSession,CURLOPT_HEADER, false);
		
		$facebook_json = curl_exec($cSession);
		curl_close($cSession);
		
		$facebook_objs = json_decode($facebook_json);
		
		$datearr = $facebook_objs->{'data'};
		$sql ="";
		$db_main2->execute(mobile_getsql($datearr,$today));
		$next= $facebook_objs->{'paging'}->{'next'};
		$flag = true;
		
		if(isset($facebook_objs->{'paging'}->{'next'}))
		{
			while($flag)
			{
				$facebook_json = file_get_contents($next);
				$facebook_objs = json_decode($facebook_json);
				$db_main2->execute(mobile_getsql($datearr,$today));
				
				if(isset($facebook_objs->{'paging'}->{'next'}))
				{
					$next= $facebook_objs->{'paging'}->{'next'};	
				}
				else 
				{
					$flag = false;
				}
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	//IOS 신규 : 466954324020414
	try
	{		
		$url = 'https://graph.facebook.com/v7.0/act_466954324020414/ads?fields=insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach,cost_per_action_type,actions,impressions,cpm,cpc,clicks,ctr,spend},campaign{name,objective,effective_status,insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach}},adset{name,start_time,configured_status,effective_status,insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach},daily_budget},name,created_time,updated_time,configured_status,effective_status,status&limit=50&insights.spend>0&access_token='.$acces_token;	
		$cSession = curl_init();
	
		curl_setopt($cSession,CURLOPT_URL, $url);
		curl_setopt($cSession,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($cSession,CURLOPT_HEADER, false);
	
		$facebook_json = curl_exec($cSession);
		curl_close($cSession);
	
		$facebook_objs = json_decode($facebook_json);
	
		$datearr = $facebook_objs->{'data'};
		$sql ="";
		$db_main2->execute(mobile_getsql($datearr,$today));
		$next= $facebook_objs->{'paging'}->{'next'};
		$flag = true;
	
		if(isset($facebook_objs->{'paging'}->{'next'}))
		{
			while($flag)
			{
				$facebook_json = file_get_contents($next);
				$facebook_objs = json_decode($facebook_json);
				$db_main2->execute(mobile_getsql($datearr,$today));
	
				if(isset($facebook_objs->{'paging'}->{'next'}))
				{
					$next= $facebook_objs->{'paging'}->{'next'};
				}
				else
				{
					$flag = false;
				}
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	//Android 신규 : 487683898770096
	try
	{	
		$url = 'https://graph.facebook.com/v8.0/act_487683898770096/ads?fields=insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach,cost_per_action_type,actions,impressions,cpm,cpc,clicks,ctr,spend},campaign{name,objective,effective_status,insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach}},adset{name,start_time,configured_status,effective_status,insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}){reach},daily_budget},name,created_time,updated_time,configured_status,effective_status,status&limit=50&insights.spend>0&access_token='.$acces_token;	
		$cSession = curl_init();
	
		curl_setopt($cSession,CURLOPT_URL, $url);
		curl_setopt($cSession,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($cSession,CURLOPT_HEADER, false);
	
		$facebook_json = curl_exec($cSession);
		curl_close($cSession);
	
		$facebook_objs = json_decode($facebook_json);
	
		$datearr = $facebook_objs->{'data'};
		$sql ="";
		$db_main2->execute(mobile_getsql($datearr,$today));
		$next= $facebook_objs->{'paging'}->{'next'};
		$flag = true;
	
		if(isset($facebook_objs->{'paging'}->{'next'}))
		{
			while($flag)
			{
				$facebook_json = file_get_contents($next);
				$facebook_objs = json_decode($facebook_json);
				$db_main2->execute(mobile_getsql($datearr,$today));
	
				if(isset($facebook_objs->{'paging'}->{'next'}))
				{
					$next= $facebook_objs->{'paging'}->{'next'};
				}
				else
				{
					$flag = false;
				}
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
?>