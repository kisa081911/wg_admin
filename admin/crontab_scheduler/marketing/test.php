<?
	include("../../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_mobile = new CDatabase_Mobile();
	
	$db_main->execute("SET wait_timeout=3600");
	$db_main2->execute("SET wait_timeout=3600");
	$db_mobile->execute("SET wait_timeout=3600");
	
	ini_set("memory_limit", "-1");
	
	
	try
	{
	    $sdate = date("Y-m-d", time() - 24 * 60 * 60 * 7);
	    $edate = date("Y-m-d", time() + 24 * 60 * 60);
	    
	    $sdate = "2018-07-01";
	    $edate = "2018-08-01";
	    
	    while($sdate < $edate)
	    {
	        $temp_date = date('Y-m-d', strtotime($sdate.' + 1 day'));
	        
	        $join_sdate = "$sdate";
	        $join_edate = "$temp_date";
	        
	        $write_sdate = $join_sdate." 00:00:00";
	        $write_edate = $join_edate." 00:00:00";
	        
	        write_log("$write_sdate ~ $write_edate retention Start");
	        
	        // 전날 데이터 체크
	        $sql = "SELECT platform, network_name AS media_source, fb_adgroup_id AS fb_ad_id, fb_campaign_name, adwords_campaign_id AS campaign, REPLACE(adgroup_name, ' ', '_') AS site_id, adid AS adjust_id, ".
	   	        "	IF(platform = 1, idfa, gps_adid) AS adv_id, installed_at AS install_time, fb_campaign_id, adwords_campaign_id ".
	   	        "FROM `tbl_adjust_install` ".
	   	        "WHERE installed_at >= '$write_sdate' AND installed_at < '$write_edate' ".
	   	        "ORDER BY logidx ASC";
	        $apps_list = $db_main2->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($apps_list); $i++)
	        {
	            $platform = $apps_list[$i]["platform"];
	            $media_source = $apps_list[$i]["media_source"];
	            $fb_ad_id = $apps_list[$i]["fb_ad_id"];
	            $fb_campaign_name = $apps_list[$i]["fb_campaign_name"];
	            $site_id = $apps_list[$i]["site_id"];
	            $adjust_id = $apps_list[$i]["adjust_id"];
	            $adv_id = $apps_list[$i]["adv_id"];
	            $install_time = $apps_list[$i]["install_time"];
	            $fb_campaign_id = $apps_list[$i]["fb_campaign_id"];
	            $adwords_campaign_id = $apps_list[$i]["adwords_campaign_id"];
	            
	            $campaign_id = 0;
	            
	            if($media_source == "Adwords UAC Installs")
	            {
	                $media_source = "googleadwords_int";
	                $campaign_id = $adwords_campaign_id;
	            }
	            else if($media_source == "Facebook Installs" || $media_source == "Off-Facebook Installs" || $media_source == "Instagram Installs" || $media_source == "Facebook Messenger Installs")
	            {
	                $media_source = "Facebook Ads";
	                $campaign_id = $fb_campaign_id;
	            }
	            
	            if(strpos($site_id, "m_retention") !== false)
	            {
	                $campaign_name_arr = explode("_", $site_id);
	                $media_source = "m_".$campaign_name_arr[2];
	                $campaign_id = $fb_campaign_id;
	            }
	            else if(strpos($site_id, "retention") !== false)
	            {
	                $campaign_name_arr = explode("_", $site_id);
	                $media_source = $campaign_name_arr[1];
	                $campaign_id = $fb_campaign_id;
	            }
	            else if(strpos($site_id, "m_duc") !== false || strpos($site_id, "m_ddi") !== false)
	            {
	                $campaign_name_arr = explode("_", $site_id);
	                
	                if($campaign_name_arr[2] == "m")
	                {
	                    $media_source = "m_".$campaign_name_arr[3]."_int";
	                }
	                else
	                {
	                    if($campaign_name_arr[3] == "m")
	                        $media_source = "m_".$campaign_name_arr[4]."_int";
                        else
                            $media_source = "m_".$campaign_name_arr[5]."_int";
	                }
	                
	                $campaign_id = $fb_campaign_id;
	            }
	            else if(strpos($site_id, "DDI_web") !== false)
	            {
	                $media_source = "fbself201";
	                $campaign_id = $fb_campaign_id;
	            }
	            else if(strpos($site_id, "DUC_web") !== false)
	            {
	                $media_source = "fbself301";
	                $campaign_id = $fb_campaign_id;
	            }
	            
	            $sql = "SELECT useridx, advid AS deviceid
					FROM tbl_user_mobile_adjust
					WHERE adid = '$adjust_id'
					ORDER BY writedate DESC
					LIMIT 1";
	            $user_info = $db_main2->getarray($sql);
	            
	            $useridx = $user_info["useridx"];
	            
	            if($useridx != "")
	            {
	                // 모바일 신규 체크
	                $sql = "SELECT DATEDIFF('$install_time', t2.createdate) AS install_diff, t2.createdate
						FROM `tbl_user_mobile_connection_log` t1 JOIN `tbl_mobile` t2 ON t1.device_id = t2.device_id
						WHERE useridx = $useridx
						ORDER BY createdate ASC
						LIMIT 1";
	                $install_info = $db_mobile->getarray($sql);
	                
	                $install_diff = $install_info["install_diff"];
	                $createdate = $install_info["createdate"];
	                
	                if(0 <= $install_diff && $install_diff <= 2)
	                {
	                    $sql = "SELECT DATEDIFF('$install_time', createdate) AS install_diff FROM tbl_user_ext WHERE useridx=$useridx";
	                    $is_newuser = $db_main->getvalue($sql);
	                    
	                    if($is_newuser <= 2)
	                    {
	                        // 완전 신규
	                        $sql = "UPDATE tbl_user_ext SET adflag = '$media_source', ifcontext = '$campaign_id' WHERE useridx=$useridx";
	                        $db_main->execute($sql);
	                    }
	                }
	            }
	        }
	        
	        // retention
	        $sql = "SELECT rtidx, platform, useridx, adflag, createdate, writedate
				FROM tbl_user_retention_mobile_log
				WHERE writedate >= '$yesterday 00:00:00' AND writedate < '$today 00:00:00'
				AND (adflag LIKE '%_int' OR adflag LIKE 'Facebook%' OR adflag LIKE 'amazon' OR adflag LIKE 'm_reten%') AND site_id = ''";
	        $retention_list = $db_main2->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($retention_list); $i++)
	        {
	            $rtidx = $retention_list[$i]["rtidx"];
	            $platform = $retention_list[$i]["platform"];
	            $useridx = $retention_list[$i]["useridx"];
	            $media_source = $retention_list[$i]["adflag"];
	            $createdate = $retention_list[$i]["createdate"];
	            $writedate = $retention_list[$i]["writedate"];
	            $site_id = "";
	            
	            //adjust
	            $sql = "SELECT adid, advid AS deviceid
					FROM tbl_user_mobile_adjust
					WHERE useridx = $useridx AND os_type = $platform
					ORDER BY writedate DESC
					LIMIT 1";
	            $user_info = $db_main2->getarray($sql);
	            
	            $adid = $user_info["adid"];
	            $device_id = $user_info["deviceid"];
	            
	            if($adid != "")
	            {
	                $sql = "SELECT fb_adgroup_id AS fb_ad_id, adgroup_name AS site_id, adid, installed_at AS install_time, fb_campaign_id, adwords_campaign_id
						FROM tbl_adjust_install
						WHERE adid = '$adid' AND platform = $platform AND 0 <= DATEDIFF('$writedate', installed_at) AND DATEDIFF('$writedate', installed_at) <= 2
						ORDER BY logidx DESC
						LIMIT 1";
	                $apps_info = $db_main2->getarray($sql);
	                
	                $fb_campaign_id = $apps_info["fb_campaign_id"];
	                $adwords_campaign_id = $apps_info["adwords_campaign_id"];
	                
	                if($fb_campaign_id != "" && $fb_campaign_id != "0")
	                {
	                    $site_id = $fb_campaign_id;
	                }
	                else if($adwords_campaign_id != "" && $adwords_campaign_id != "0")
	                {
	                    $site_id = $adwords_campaign_id;
	                }
	                
	                if($site_id != "")
	                {
	                    $sql = "UPDATE tbl_user_retention_mobile_log SET site_id = '$site_id' WHERE rtidx = $rtidx";
	                    $db_main2->execute($sql);
	                }
	            }
	        }
	        	        
	        $sdate = $temp_date;
	    }
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();
	$db_mobile->end();
?>