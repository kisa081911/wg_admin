<?
	include("../../common/common_include.inc.php");
    include_once('../../common/simple_html_dom.php');
    
   	$db_main2 = new CDatabase_Main2();
   	
    $db_main2->execute("SET wait_timeout=14400");
    
    $sdate = "2019-08-13";
    $edate = "2019-08-14";
    //$edate = "2019-05-01";
    
    while($sdate < $edate)
    {   
    	$today = $sdate;
    	
    	write_log("re_attributed : $today");
	    #
	    # appsflyer raw data DB 저장
	    #
	    // iOS
		$handle = fopen('https://hq.appsflyer.com/export/id1122387239/installs_report/v5?api_token=6ab0e527-8106-44f9-81e2-d6e27fe254bb&from=2019-08-13&to=2019-08-17&additional_fields=install_app_store,contributor1_match_type,contributor2_match_type,contributor3_match_type,match_type,device_category,gp_referrer,gp_click_time,gp_install_begin,amazon_aid,keyword_match_type&reattr=true','r');

		if($handle)
		{
			$insert_sql = "";
			
			while (($data = fgetcsv($handle) ) != FALSE )
			{
				if($data[2] == "Install Time")
					continue;
				
				if($insert_sql == "")
				{
					$insert_sql = "insert into `_tmp_tbl_appsflyer_install_re_attribution_20190813_20190817` (`platform`, `is_retargeting`, `retargeting_conversion_type`, `fb_ad_id`, `attributed_touch_time`, `install_time`, `agency`, `media_source`, `fb_campaign_id`, `fb_campaign_name`, `fb_adset_id`, `fb_adset_name`, `fb_adgroup_name`, `campaign`, `channel`, `keyword`, `site_id`, `sub1`, `sub2`, `sub3`, `sub4`, `sub5`, `country_code`, `city`, `languege`, `appsflyer_device_id`, `adset_id`, `adset`, `ad_id`, `ad`, `adv_id`, `device_id`, `device_type`)".
									"VALUES (1, '1', '$data[74]', '$data[20]', '$data[1]', '$data[2]', '', '$data[12]', '$data[16]', '$data[15]', '$data[18]', '".encode_db_field($data[17])."', '".encode_db_field($data[19])."', '".encode_db_field($data[15])."', '$data[13]', '$data[14]', '$data[22]','','','','','', '".encode_db_field($data[48])."', '".encode_db_field($data[50])."', '".encode_db_field($data[57])."', '$data[58]','','','', '','$data[59]', '$data[64]', '$data[66]')";
				}
				else
				{
				    $insert_sql .= ",(1, '1', '$data[74]', '$data[20]', '$data[1]', '$data[2]', '', '$data[12]', '$data[16]', '$data[15]', '$data[18]', '".encode_db_field($data[17])."', '".encode_db_field($data[19])."', '".encode_db_field($data[15])."', '$data[13]', '$data[14]', '$data[22]','','','','','', '".encode_db_field($data[48])."', '".encode_db_field($data[50])."', '".encode_db_field($data[57])."', '$data[58]','','','', '','$data[59]', '$data[64]', '$data[66]')";
				}
			}
			
			ini_set('auto_detect_line_endings',FALSE);
			
			if($insert_sql != "")
				$db_main2->execute($insert_sql);
		}
		
		// Android
		/*$handle = fopen('https://hq.appsflyer.com/export/com.doubleugames.take5/installs_report/v5?api_token=6ab0e527-8106-44f9-81e2-d6e27fe254bb&from=2019-08-13&to=2019-08-17&additional_fields=install_app_store,contributor1_match_type,contributor2_match_type,contributor3_match_type,match_type,device_category,gp_referrer,gp_click_time,gp_install_begin,amazon_aid,keyword_match_type&reattr=true','r');
				
		if($handle)
		{
			$insert_sql = "";
			
			while (($data = fgetcsv($handle) ) != FALSE )
			{
				if($data[2] == "Install Time")
					continue;
				
					if($insert_sql == "")
					{
					    $insert_sql = "insert into `_tmp_tbl_appsflyer_install_re_attribution_20190813_20190817` (`platform`, `is_retargeting`, `retargeting_conversion_type`, `fb_ad_id`, `attributed_touch_time`, `install_time`, `agency`, `media_source`, `fb_campaign_id`, `fb_campaign_name`, `fb_adset_id`, `fb_adset_name`, `fb_adgroup_name`, `campaign`, `channel`, `keyword`, `site_id`, `sub1`, `sub2`, `sub3`, `sub4`, `sub5`, `country_code`, `city`, `languege`, `appsflyer_device_id`, `adset_id`, `adset`, `ad_id`, `ad`, `adv_id`, `device_id`, `device_type`)".
									    "VALUES (2, '1', '$data[74]', '$data[20]', '$data[1]', '$data[2]', '', '$data[12]', '$data[16]', '$data[15]', '$data[18]', '".encode_db_field($data[17])."', '".encode_db_field($data[19])."', '".encode_db_field($data[15])."', '$data[13]', '$data[14]', '$data[22]','','','','','', '".encode_db_field($data[48])."', '".encode_db_field($data[50])."', '".encode_db_field($data[57])."', '$data[58]','','','', '','$data[59]', '$data[64]', '$data[66]')";
					}
					else
					{
					    $insert_sql .= ",(2, '1', '$data[74]', '$data[20]', '$data[1]', '$data[2]', '', '$data[12]', '$data[16]', '$data[15]', '$data[18]', '".encode_db_field($data[17])."', '".encode_db_field($data[19])."', '".encode_db_field($data[15])."', '$data[13]', '$data[14]', '$data[22]','','','','','', '".encode_db_field($data[48])."', '".encode_db_field($data[50])."','".encode_db_field($data[57])."', '$data[58]','','','', '','$data[59]', '$data[64]', '$data[66]')";
					}
			}
			
			ini_set('auto_detect_line_endings',FALSE);
			
			if($insert_sql != "")
				$db_main2->execute($insert_sql);
		}*/
		
		sleep(1);
		
		$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));	
    }
	
	$db_main2->end();
?>