<?
	include("../../common/common_include.inc.php");
    include_once('../../common/simple_html_dom.php');
    
	$result = array();
	exec("ps -ef | grep wget", $result);

	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/marketing_scheduler") !== false)
		{
			$count++;			
		}
	}
	
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	
	$db_main2->execute("SET wait_timeout=7200");
	$db_analysis->execute("SET wait_timeout=7200");
	
	$title = "[Take5 - Marketing Scheduler] ".date("Y-m-d")." Marketing Scheduler Fail";
	
	$to = "yhjung@doubleugames.com";
	$from = "report@doubleucasino.com";
	
	//================================ -1 Days ======================================
	$today = date("Y-m-d", strtotime("-1 day"));
    try 
	{
		//chartboost
		for($i=0; $i<3; $i++)
		{
			$sql = "";
			$chartboost_ios_json = file_get_html('https://analytics.chartboost.com/v3/metrics/campaign?dateMin='.$today.'&dateMax='.$today.'&userId=53aa323489b0bb4f27757f42&userSignature=0a20623b05309031d328818d4fb94e59d84e6ae4&platform=iOS')->plaintext;
			
			if(!empty($chartboost_ios_json))
			{
				$chartboost_spend_ios = 0;
					
				$chartboost_ios_objs = json_decode($chartboost_ios_json);
				
				if(!empty($chartboost_ios_objs))
				{
					foreach ($chartboost_ios_objs as $chartboost_obj)
					{
						$campaign_name = strtolower($chartboost_obj->{'campaign'});
				
						if(strpos($campaign_name, "t5") !== false)
							$chartboost_spend_ios += $chartboost_obj->{'moneySpent'};
					}
				}
			}
			
			$chartboost_and_json = file_get_html('https://analytics.chartboost.com/v3/metrics/campaign?dateMin='.$today.'&dateMax='.$today.'&userId=53aa323489b0bb4f27757f42&userSignature=0a20623b05309031d328818d4fb94e59d84e6ae4&platform=Google%20Play')->plaintext;
		
			if(!empty($chartboost_and_json))
			{
				$chartboost_spend_and = 0;
					
				$chartboost_and_objs = json_decode($chartboost_and_json);
				
				if(!empty($chartboost_and_objs))
				{
					foreach ($chartboost_and_objs as $chartboost_obj)
					{
						$campaign_name = strtolower($chartboost_obj->{'campaign'});
				
						if(strpos($campaign_name, "t5") !== false)
							$chartboost_spend_and += $chartboost_obj->{'moneySpent'};
					}
				}
			}
		
			if($chartboost_spend_ios != 0)
				$sql .= "INSERT INTO tbl_agency_spend_daily VALUES ('$today', 1, 5, 'chartboosts2s_int', $chartboost_spend_ios) ON DUPLICATE KEY UPDATE spend = $chartboost_spend_ios;";
			if($chartboost_spend_and != 0)
				$sql .= "INSERT INTO tbl_agency_spend_daily VALUES ('$today', 2, 5, 'chartboosts2s_int', $chartboost_spend_and) ON DUPLICATE KEY UPDATE spend = $chartboost_spend_and;";
			
			if($sql != "")
			{
				$db_main2->execute($sql);
				break;
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
		
	try
	{
		$sql = "SELECT accesstoken FROM page_accesstoken WHERE userid = 0";
		$access_token = $db_analysis->getvalue($sql);
		$account_id = "805348042930702";
		
// 		//facebook mobile
// 		for($i=0; $i<3; $i++)
// 		{
// 			$facebook_json = file_get_contents('https://graph.facebook.com/v2.12/act_'.$account_id.'/campaigns?fields=insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}),name&limit=1000&access_token='.$access_token);
				
// 			if(!empty($facebook_json))
// 			{
// 				$facebook_objs = json_decode($facebook_json);
// 				$data_arr = $facebook_objs->{'data'};
					
// 				$facebook_spend_ios = 0;
// 				$facebook_spend_android = 0;
					
// 				foreach ($data_arr as $data_arr_Obj)
// 				{
// 					$campaign_name = strtolower($data_arr_Obj->{'name'});
		
// 					if(isset($data_arr_Obj->{'insights'}))
// 					{
// 						foreach ($data_arr_Obj->{'insights'}->{'data'} as $insights)
// 						{
// 							if(!preg_match("/nanigans/",$campaign_name) || !preg_match("/m_ddi/",$campaign_name) || !preg_match("/m_duc/",$campaign_name))
// 							{
// 								$spend = $insights->{'spend'};
								
// 								if(preg_match("/ios/",$campaign_name))
// 								{
// 									$facebook_spend_ios += $spend;
// 								}
// 								else if(preg_match("/android/",$campaign_name))
// 								{
// 									$facebook_spend_android += $spend;
// 								}
// 							}
// 						}
// 					}
// 				}
		
// 				$sql = "INSERT INTO tbl_agency_spend_daily VALUES ('$today', 1, 8, 'Facebook Ads', $facebook_spend_ios) ON DUPLICATE KEY UPDATE spend = $facebook_spend_ios;".
// 						"INSERT INTO tbl_agency_spend_daily VALUES ('$today', 2, 8, 'Facebook Ads', $facebook_spend_android) ON DUPLICATE KEY UPDATE spend = $facebook_spend_android;";
// 				$db_main2->execute($sql);
// 				break;
// 			}
// 		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
// 	//================================ -2 Days ======================================
 	$today = date("Y-m-d", strtotime("-2 day"));
	
	try
	{
		//vungle
		for($i=0; $i<3; $i++)
		{
			$flag = false;
				
			$vungle_spend_ios = 0;
			$vungle_spend_and = 0;
				
			$vungle_json = file_get_html('https://ssl.vungle.com/api/campaigns?key=e8db5220ab5b65779efa53477c846064')->plaintext;
				
			if(!empty($vungle_json))
			{
				$vungle_objs = json_decode($vungle_json);
	
				foreach ($vungle_objs as $vungle_obj)
				{
					$vungle_campaign_json = file_get_html('http://ssl.vungle.com/api/campaigns/'.$vungle_obj->{'campaignId'}.'?key=e8db5220ab5b65779efa53477c846064&date='.$today)->plaintext;
						
					if(!empty($vungle_campaign_json))
					{
						$vungle_campaign_objs = json_decode($vungle_campaign_json);
						$vungle_campaign_name = strtolower($vungle_campaign_objs[0]->{'name'});
						
						if(strpos($vungle_campaign_name, "take") !== false)
						{
							if(strpos($vungle_campaign_name, "android") !== false)
								$vungle_spend_and += $vungle_campaign_objs[0]->{'dailySpend'};
							else if(strpos($vungle_campaign_name, "ios") !== false)
								$vungle_spend_ios += $vungle_campaign_objs[0]->{'dailySpend'};
						}
							
						$flag = true;
					}
				}
	
				if(flag)
				{
					$sql = "INSERT INTO tbl_agency_spend_daily VALUES ('$today', 1, 2, 'vungle_int', $vungle_spend_ios) ON DUPLICATE KEY UPDATE spend = $vungle_spend_ios;".
							"INSERT INTO tbl_agency_spend_daily VALUES ('$today', 2, 2, 'vungle_int', $vungle_spend_and) ON DUPLICATE KEY UPDATE spend = $vungle_spend_and;";
					$db_main2->execute($sql);
					break;
				}
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		sendmail($to, $from, $title, $e->getMessage());
	}
	
	try
	{
		$sql = "SELECT accesstoken FROM page_accesstoken WHERE userid = 0";
		$access_token = $db_analysis->getvalue($sql);
		$account_id = "805348042930702";
		
// 		//facebook mobile
// 		for($i=0; $i<3; $i++)
// 		{
// 			$facebook_json = file_get_contents('https://graph.facebook.com/v2.12/act_'.$account_id.'/campaigns?fields=insights.time_range({%22since%22:%22'.$today.'%22,%22until%22:%22'.$today.'%22}),name&limit=1000&access_token='.$access_token);
			
// 			if(!empty($facebook_json))
// 			{
// 				$facebook_objs = json_decode($facebook_json);
// 				$data_arr = $facebook_objs->{'data'};
					
// 				$facebook_spend_ios = 0;
// 				$facebook_spend_android = 0;
					
// 				foreach ($data_arr as $data_arr_Obj)
// 				{
// 					$campaign_name = strtolower($data_arr_Obj->{'name'});
						
// 					if(isset($data_arr_Obj->{'insights'}))
// 					{
// 						foreach ($data_arr_Obj->{'insights'}->{'data'} as $insights)
// 						{
// 							if(!preg_match("/nanigans/",$campaign_name) || !preg_match("/m_ddi/",$campaign_name) || !preg_match("/m_duc/",$campaign_name))
// 							{
// 								$spend = $insights->{'spend'};
							
// 								if(preg_match("/ios/",$campaign_name))
// 								{
// 									$facebook_spend_ios += $spend;
// 								}
// 								else if(preg_match("/android/",$campaign_name))
// 								{
// 									$facebook_spend_android += $spend;
// 								}
// 							}
// 						}
// 					}
// 				}
				
// 				$sql = "INSERT INTO tbl_agency_spend_daily VALUES ('$today', 1, 8, 'Facebook Ads', $facebook_spend_ios) ON DUPLICATE KEY UPDATE spend = $facebook_spend_ios;".
// 						"INSERT INTO tbl_agency_spend_daily VALUES ('$today', 2, 8, 'Facebook Ads', $facebook_spend_android) ON DUPLICATE KEY UPDATE spend = $facebook_spend_android;";
// 				$db_main2->execute($sql);
// 				break;
// 			}
// 		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		sendmail($to, $from, $title, $e->getMessage());
	}

    $db_main2->end();
    $db_analysis->end();
?>