<?
	include("../../common/common_include.inc.php");		

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/marketing/marketing_week_stat") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/marketing/marketing_week_stat") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("marketing/marketing_week_stat Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$db_slave_main = new CDatabase_Slave_Main();
 	$db_main2 = new CDatabase_Main2();
 	$db_other = new CDatabase_Other();
 	
 	$today = date("Y-m-d", strtotime("-1 days"));
 	
 	// grade_7
 	try
 	{
 		#
 		#
 		# 마케팅 비용
 		#
 		#
 		for($i=1; $i<=4; $i++)
 		{
	 		$sql = "SELECT platform, adflag, SUM(spend) AS spend
					FROM (
						SELECT 0 AS platform, 'nanigans' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 WEEK)
						AND campaign_name LIKE '%Nanigans%'
						UNION ALL
						SELECT 0 AS platform, 'fbself' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 WEEK)
						AND campaign_name NOT LIKE '%Nanigans%'
						UNION ALL
						SELECT 0 AS platform, 'fbself_ddi' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 WEEK)
						AND LOWER(campaign_name) LIKE '%m_ddi%'
						UNION ALL
						SELECT 0 AS platform, 'fbself_duc' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 WEEK)
						AND LOWER(campaign_name) LIKE '%m_duc%'
						UNION ALL
						SELECT platform, 'nanigans' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats_mobile`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 WEEK)
						AND campaign_name LIKE '%Nanigans%'
						GROUP BY platform
						UNION ALL
						SELECT platform, 'Facebook Ads' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats_mobile`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 WEEK)
						AND campaign_name NOT LIKE '%Nanigans%' AND campaign_name NOT LIKE '%m_ddi%' AND campaign_name NOT LIKE '%m_duc%'
						GROUP BY platform
						UNION ALL
						SELECT platform, CONCAT(SUBSTRING_INDEX(campaign_name, '_', -2), '_int') AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats_mobile`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 WEEK)
						AND campaign_name NOT LIKE '%Nanigans%' AND campaign_name LIKE '%m_ddi%' AND campaign_name LIKE '%m_duc%'
						GROUP BY platform, adflag
						UNION ALL	
						SELECT platform, agencyname AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_agency_spend_daily`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 WEEK)
						AND agencyname NOT IN('nanigans')
						GROUP BY platform, agencyname
					) t1
					WHERE spend > 0
                    GROUP BY platform, adflag";
	 		$week_spend_list = $db_main2->gettotallist($sql);
	 		
	 		$insert_sql = "";
	 		
	 		for($j=0; $j<sizeof($week_spend_list); $j++)
	 		{
	 			$platform = $week_spend_list[$j]["platform"];
	 			$adflag = $week_spend_list[$j]["adflag"];
	 			$spend = $week_spend_list[$j]["spend"];
	 			
	 			if($insert_sql == "")
	 				$insert_sql = "INSERT INTO tbl_marketing_week_stat(today, category, platform, adflag, week_type, spend) VALUES('$today', 1, $platform, '$adflag', $i, $spend)";
	 			else
	 				$insert_sql .= ",('$today', 1, $platform, '$adflag', $i, $spend)";
	 		}
	 		
	 		if($insert_sql != "")
	 		{
	 			$insert_sql .= "ON DUPLICATE KEY UPDATE spend = VALUES(spend);";
	 			$db_other->execute($insert_sql);
	 		}
 		}
 		
 		#
 		#
 		# 등급별 결제액, 결제자수
 		#
 		#
 		$sql = "SELECT *
 				FROM tbl_marketing_week_stat
 				WHERE today = '$today'";
 		$marketing_week_stat_list = $db_other->gettotallist($sql);
 		
 		for($i=0; $i<sizeof($marketing_week_stat_list); $i++)
 		{
 			$platform = $marketing_week_stat_list[$i]["platform"];
 			$adflag = $marketing_week_stat_list[$i]["adflag"];
 			$week_type = $marketing_week_stat_list[$i]["week_type"];
 			
 			$adflag_tail = " AND platform = $platform AND adflag LIKE '$adflag%' ";
 			
 			$sql = "SELECT grade_7, SUM(money) AS total_money, COUNT(DISTINCT useridx) AS user_cnt
					FROM (
						SELECT useridx, ROUND(SUM(money), 2) AS money,
							(CASE WHEN ROUND(SUM(money), 2) >= 499 THEN 5
							WHEN ROUND(SUM(money), 2) >= 199 AND ROUND(SUM(money), 2) < 499 THEN 4
							WHEN ROUND(SUM(money), 2) >= 59 AND ROUND(SUM(money), 2) < 199 THEN 3
							WHEN ROUND(SUM(money), 2) >= 9 AND ROUND(SUM(money), 2) < 59 THEN 2
							WHEN ROUND(SUM(money), 2) >= 0 AND ROUND(SUM(money), 2) < 9 THEN 1
							ELSE 0 END ) AS grade_7
						FROM (
							SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money
							FROM (
								SELECT useridx, createdate
								FROM tbl_user_ext
								WHERE useridx > 20000
 								AND DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 1 + $week_type WEEK) <= createdate AND createdate < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 1 WEEK)
 								$adflag_tail
							) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx
							WHERE STATUS = 1 AND DATEDIFF(writedate, createdate) <= 7
							UNION ALL
							SELECT t1.useridx, money
							FROM (
								SELECT useridx, createdate
								FROM tbl_user_ext
								WHERE useridx > 20000
 								AND DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 1 + $week_type WEEK) <= createdate AND createdate < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 1 WEEK)
 								$adflag_tail
							) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx
							WHERE STATUS = 1 AND DATEDIFF(writedate, createdate) <= 7
						) t1
						GROUP BY useridx
					) total
					GROUP BY grade_7";
 			$grade_pay_info_list = $db_slave_main->gettotallist($sql);
 			
 			$insert_sql = "";
 			
 			$mn_money = 0;
 			$mn_cnt = 0;
 			$dp_money = 0;
 			$dp_cnt = 0;
 			$sl_money = 0;
 			$sl_cnt = 0;
 			$wh_money = 0;
 			$wh_cnt = 0;
 			$kw_money = 0;
 			$kw_cnt = 0;
 			
 			for($j=0; $j<sizeof($grade_pay_info_list); $j++)
 			{
 				$grade_7 = $grade_pay_info_list[$j]["grade_7"];
 				
 				if($grade_7 == 1)
 				{
 					$mn_money = $grade_pay_info_list[$j]["total_money"];
 					$mn_cnt = $grade_pay_info_list[$j]["user_cnt"];	
 				}
 				else if($grade_7 == 2)
 				{
 					$dp_money = $grade_pay_info_list[$j]["total_money"];
 					$dp_cnt = $grade_pay_info_list[$j]["user_cnt"];
 				}
 				else if($grade_7 == 3)
 				{
 					$sl_money = $grade_pay_info_list[$j]["total_money"];
 					$sl_cnt = $grade_pay_info_list[$j]["user_cnt"];
 				}
 				else if($grade_7 == 4)
 				{
 					$wh_money = $grade_pay_info_list[$j]["total_money"];
 					$wh_cnt = $grade_pay_info_list[$j]["user_cnt"];
 				}
 				else if($grade_7 == 5)
 				{
 					$kw_money = $grade_pay_info_list[$j]["total_money"];
 					$kw_cnt = $grade_pay_info_list[$j]["user_cnt"];
 				}
 			}
 			
 			if($insert_sql == "")
 			{
 				$insert_sql = "INSERT INTO tbl_marketing_week_stat(today, category, platform, adflag, week_type, kw_money, kw_cnt, wh_money, wh_cnt, sl_money, sl_cnt, dp_money, dp_cnt, mn_money, mn_cnt)".
 								"VALUES('$today', 1, $platform, '$adflag', $week_type, $kw_money, $kw_cnt, $wh_money, $wh_cnt, $sl_money, $sl_cnt, $dp_money, $dp_cnt, $mn_money, $mn_cnt)";
 			}
 			else
 			{
 				$insert_sql .= ",('$today', 1, $platform, '$adflag', $week_type, $kw_money, $kw_cnt, $wh_money, $wh_cnt, $sl_money, $sl_cnt, $dp_money, $dp_cnt, $mn_money, $mn_cnt) ";
 			}
 			
 			if($insert_sql != "")
 			{
	 			$insert_sql .= "ON DUPLICATE KEY UPDATE kw_money = VALUES(kw_money), kw_cnt = VALUES(kw_cnt), wh_money = VALUES(wh_money), wh_cnt = VALUES(wh_cnt), ".
	 							"sl_money = VALUES(sl_money), sl_cnt = VALUES(sl_cnt), dp_money = VALUES(dp_money), dp_cnt = VALUES(dp_cnt), mn_money = VALUES(mn_money), mn_cnt = VALUES(mn_cnt);";
	 			$db_other->execute($insert_sql);
 			}
 		}
 		
 		#
 		#
 		# 가입자수
 		#
 		#
 		$sql = "SELECT *
		 		FROM tbl_marketing_week_stat
		 		WHERE today = '$today'";
 		$marketing_week_stat_list = $db_other->gettotallist($sql);
 		
 		$insert_sql = "";
 		
 		for($i=0; $i<sizeof($marketing_week_stat_list); $i++)
 		{
 			$platform = $marketing_week_stat_list[$i]["platform"];
 			$adflag = $marketing_week_stat_list[$i]["adflag"];
 			$week_type = $marketing_week_stat_list[$i]["week_type"];
 			
 			$adflag_tail = " AND platform = $platform AND adflag LIKE '$adflag%' ";
 			
 			$sql = "SELECT COUNT(useridx) AS join_user_cnt
					FROM tbl_user_ext
					WHERE useridx > 20000
 					AND DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 1 + $week_type WEEK) <= createdate AND createdate < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 1 WEEK)
 					$adflag_tail";
 			$join_user_cnt = $db_slave_main->getvalue($sql);
 			
 			if($insert_sql == "")
 			{
 				$insert_sql = "INSERT INTO tbl_marketing_week_stat(today, category, platform, adflag, week_type, user_cnt) VALUES('$today', 1, $platform, '$adflag', $week_type, $join_user_cnt)";
 			}
 			else
 			{
 				$insert_sql .= ",('$today', 1, $platform, '$adflag', $week_type, $join_user_cnt) ";
 			}
 		}
 		
 		if($insert_sql != "")
 		{
 			$insert_sql .= "ON DUPLICATE KEY UPDATE user_cnt = VALUES(user_cnt);";
 			$db_other->execute($insert_sql);
 		}
 	}
 	catch(Exception $e)
 	{
 		write_log($e->getMessage());
 	}
 	
 	// grade_14
 	try
 	{
 		#
 		#
 		# 마케팅 비용
 		#
 		#
 		for($i=1; $i<=4; $i++)
 		{
	 		$sql = "SELECT platform, adflag, SUM(spend) AS spend
					FROM (
						SELECT 0 AS platform, 'nanigans' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK)
						AND campaign_name LIKE '%Nanigans%'
						UNION ALL
						SELECT 0 AS platform, 'fbself' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK)
						AND campaign_name NOT LIKE '%Nanigans%'
						UNION ALL
						SELECT 0 AS platform, 'fbself_ddi' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK)
						AND LOWER(campaign_name) LIKE '%m_ddi%'
						UNION ALL
						SELECT 0 AS platform, 'fbself_duc' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK)
						AND LOWER(campaign_name) LIKE '%m_duc%'
						UNION ALL
						SELECT platform, 'nanigans' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats_mobile`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK)
						AND campaign_name LIKE '%Nanigans%'
						GROUP BY platform
						UNION ALL
						SELECT platform, 'Facebook Ads' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats_mobile`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK)
						AND campaign_name NOT LIKE '%Nanigans%' AND campaign_name NOT LIKE '%m_ddi%' AND campaign_name NOT LIKE '%m_duc%'
						GROUP BY platform
						UNION ALL
						SELECT platform, CONCAT(SUBSTRING_INDEX(campaign_name, '_', -2), '_int') AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats_mobile`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK)
						AND campaign_name NOT LIKE '%Nanigans%' AND campaign_name LIKE '%m_ddi%' AND campaign_name LIKE '%m_duc%'
						GROUP BY platform, adflag
						UNION ALL
						SELECT platform, agencyname AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_agency_spend_daily`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 2 WEEK)
						AND agencyname NOT IN('nanigans')
						GROUP BY platform, agencyname
					) t1
					WHERE spend > 0
                    GROUP BY platform, adflag";
	 		$week_spend_list = $db_main2->gettotallist($sql);
	 		
	 		$insert_sql = "";
	 		
	 		for($j=0; $j<sizeof($week_spend_list); $j++)
	 		{
	 			$platform = $week_spend_list[$j]["platform"];
	 			$adflag = $week_spend_list[$j]["adflag"];
	 			$spend = $week_spend_list[$j]["spend"];
	 			
	 			if($insert_sql == "")
	 				$insert_sql = "INSERT INTO tbl_marketing_week_stat(today, category, platform, adflag, week_type, spend) VALUES('$today', 2, $platform, '$adflag', $i, $spend)";
	 			else
	 				$insert_sql .= ",('$today', 2, $platform, '$adflag', $i, $spend)";
	 		}
	 		
	 		if($insert_sql != "")
	 		{
	 			$insert_sql .= "ON DUPLICATE KEY UPDATE spend = VALUES(spend);";
	 			$db_other->execute($insert_sql);
	 		}
 		}
 		
 		#
 		#
 		# 등급별 결제액, 결제자수
 		#
 		#
 		$sql = "SELECT *
 				FROM tbl_marketing_week_stat
 				WHERE today = '$today'";
 		$marketing_week_stat_list = $db_other->gettotallist($sql);
 		
 		for($i=0; $i<sizeof($marketing_week_stat_list); $i++)
 		{
 			$platform = $marketing_week_stat_list[$i]["platform"];
 			$adflag = $marketing_week_stat_list[$i]["adflag"];
 			$week_type = $marketing_week_stat_list[$i]["week_type"];
 			
 			$adflag_tail = " AND platform = $platform AND adflag LIKE '$adflag%' ";
 			
 			$sql = "SELECT grade_14, SUM(money) AS total_money, COUNT(DISTINCT useridx) AS user_cnt
					FROM (
						SELECT useridx, ROUND(SUM(money), 2) AS money,
							(CASE WHEN ROUND(SUM(money), 2) >= 499 THEN 5
							WHEN ROUND(SUM(money), 2) >= 199 AND ROUND(SUM(money), 2) < 499 THEN 4
							WHEN ROUND(SUM(money), 2) >= 59 AND ROUND(SUM(money), 2) < 199 THEN 3
							WHEN ROUND(SUM(money), 2) >= 9 AND ROUND(SUM(money), 2) < 59 THEN 2
							WHEN ROUND(SUM(money), 2) >= 0 AND ROUND(SUM(money), 2) < 9 THEN 1
							ELSE 0 END ) AS grade_14
						FROM (
							SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money
							FROM (
								SELECT useridx, createdate
								FROM tbl_user_ext
								WHERE useridx > 20000
 								AND DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 2 + $week_type WEEK) <= createdate AND createdate < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 2 WEEK)
 								$adflag_tail
							) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx
							WHERE STATUS = 1 AND DATEDIFF(writedate, createdate) <= 14
							UNION ALL
							SELECT t1.useridx, money
							FROM (
								SELECT useridx, createdate
								FROM tbl_user_ext
								WHERE useridx > 20000
 								AND DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 2 + $week_type WEEK) <= createdate AND createdate < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 2 WEEK)
 								$adflag_tail
							) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx
							WHERE STATUS = 1 AND DATEDIFF(writedate, createdate) <= 14
						) t1
						GROUP BY useridx
					) total
					GROUP BY grade_14";
 			$grade_pay_info_list = $db_slave_main->gettotallist($sql);
 			
 			$insert_sql = "";
 			
 			$mn_money = 0;
 			$mn_cnt = 0;
 			$dp_money = 0;
 			$dp_cnt = 0;
 			$sl_money = 0;
 			$sl_cnt = 0;
 			$wh_money = 0;
 			$wh_cnt = 0;
 			$kw_money = 0;
 			$kw_cnt = 0;
 			
 			for($j=0; $j<sizeof($grade_pay_info_list); $j++)
 			{
 				$grade_14 = $grade_pay_info_list[$j]["grade_14"];
 				
 				if($grade_14 == 1)
 				{
 					$mn_money = $grade_pay_info_list[$j]["total_money"];
 					$mn_cnt = $grade_pay_info_list[$j]["user_cnt"];	
 				}
 				else if($grade_14 == 2)
 				{
 					$dp_money = $grade_pay_info_list[$j]["total_money"];
 					$dp_cnt = $grade_pay_info_list[$j]["user_cnt"];
 				}
 				else if($grade_14 == 3)
 				{
 					$sl_money = $grade_pay_info_list[$j]["total_money"];
 					$sl_cnt = $grade_pay_info_list[$j]["user_cnt"];
 				}
 				else if($grade_14 == 4)
 				{
 					$wh_money = $grade_pay_info_list[$j]["total_money"];
 					$wh_cnt = $grade_pay_info_list[$j]["user_cnt"];
 				}
 				else if($grade_14 == 5)
 				{
 					$kw_money = $grade_pay_info_list[$j]["total_money"];
 					$kw_cnt = $grade_pay_info_list[$j]["user_cnt"];
 				}
 			}
 			
 			if($insert_sql == "")
 			{
 				$insert_sql = "INSERT INTO tbl_marketing_week_stat(today, category, platform, adflag, week_type, kw_money, kw_cnt, wh_money, wh_cnt, sl_money, sl_cnt, dp_money, dp_cnt, mn_money, mn_cnt)".
 								"VALUES('$today', 2, $platform, '$adflag', $week_type, $kw_money, $kw_cnt, $wh_money, $wh_cnt, $sl_money, $sl_cnt, $dp_money, $dp_cnt, $mn_money, $mn_cnt)";
 			}
 			else
 			{
 				$insert_sql .= ",('$today', 2, $platform, '$adflag', $week_type, $kw_money, $kw_cnt, $wh_money, $wh_cnt, $sl_money, $sl_cnt, $dp_money, $dp_cnt, $mn_money, $mn_cnt) ";
 			}
 			
 			if($insert_sql != "")
 			{
	 			$insert_sql .= "ON DUPLICATE KEY UPDATE kw_money = VALUES(kw_money), kw_cnt = VALUES(kw_cnt), wh_money = VALUES(wh_money), wh_cnt = VALUES(wh_cnt), ".
	 							"sl_money = VALUES(sl_money), sl_cnt = VALUES(sl_cnt), dp_money = VALUES(dp_money), dp_cnt = VALUES(dp_cnt), mn_money = VALUES(mn_money), mn_cnt = VALUES(mn_cnt);";
	 			$db_other->execute($insert_sql);
 			}
 		}
 		
 		#
 		#
 		# 가입자수
 		#
 		#
 		$sql = "SELECT *
		 		FROM tbl_marketing_week_stat
		 		WHERE today = '$today'";
 		$marketing_week_stat_list = $db_other->gettotallist($sql);
 		
 		$insert_sql = "";
 		
 		for($i=0; $i<sizeof($marketing_week_stat_list); $i++)
 		{
 			$platform = $marketing_week_stat_list[$i]["platform"];
 			$adflag = $marketing_week_stat_list[$i]["adflag"];
 			$week_type = $marketing_week_stat_list[$i]["week_type"];
 			
 			$adflag_tail = " AND platform = $platform AND adflag LIKE '$adflag%' ";
 			
 			$sql = "SELECT COUNT(useridx) AS join_user_cnt
					FROM tbl_user_ext
					WHERE useridx > 20000
 					AND DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 2 + $week_type WEEK) <= createdate AND createdate < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 2 WEEK)
 					$adflag_tail";
 			$join_user_cnt = $db_slave_main->getvalue($sql);
 			
 			if($insert_sql == "")
 			{
 				$insert_sql = "INSERT INTO tbl_marketing_week_stat(today, category, platform, adflag, week_type, user_cnt) VALUES('$today', 2, $platform, '$adflag', $week_type, $join_user_cnt)";
 			}
 			else
 			{
 				$insert_sql .= ",('$today', 2, $platform, '$adflag', $week_type, $join_user_cnt) ";
 			}
 		}
 		
 		if($insert_sql != "")
 		{
 			$insert_sql .= "ON DUPLICATE KEY UPDATE user_cnt = VALUES(user_cnt);";
 			$db_other->execute($insert_sql);
 		}
 	}
 	catch(Exception $e)
 	{
 		write_log($e->getMessage());
 	}
 	
 	// grade_28
 	try
 	{
 		#
 		#
 		# 마케팅 비용
 		#
 		#
 		for($i=1; $i<=4; $i++)
 		{
	 		$sql = "SELECT platform, adflag, SUM(spend) AS spend
					FROM (
						SELECT 0 AS platform, 'nanigans' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 WEEK)
						AND campaign_name LIKE '%Nanigans%'
						UNION ALL
						SELECT 0 AS platform, 'fbself' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 WEEK)
						AND campaign_name NOT LIKE '%Nanigans%'
						UNION ALL
						SELECT 0 AS platform, 'fbself_ddi' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 WEEK)
						AND LOWER(campaign_name) LIKE '%m_ddi%'
						UNION ALL
						SELECT 0 AS platform, 'fbself_duc' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 WEEK)
						AND LOWER(campaign_name) LIKE '%m_duc%'
						UNION ALL
						SELECT platform, 'nanigans' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats_mobile`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 WEEK)
						AND campaign_name LIKE '%Nanigans%'
						GROUP BY platform
						UNION ALL
						SELECT platform, 'Facebook Ads' AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats_mobile`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 WEEK)
						AND campaign_name NOT LIKE '%Nanigans%' AND campaign_name NOT LIKE '%m_ddi%' AND campaign_name NOT LIKE '%m_duc%'
						GROUP BY platform
						UNION ALL
						SELECT platform, CONCAT(SUBSTRING_INDEX(campaign_name, '_', -2), '_int') AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_ad_stats_mobile`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 WEEK)
						AND campaign_name NOT LIKE '%Nanigans%' AND campaign_name LIKE '%m_ddi%' AND campaign_name LIKE '%m_duc%'
						GROUP BY platform, adflag
						UNION ALL
						SELECT platform, agencyname AS adflag, ROUND(SUM(spend), 2) AS spend
						FROM `tbl_agency_spend_daily`
						WHERE DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 + $i WEEK) <= today AND today < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 4 WEEK)
						AND agencyname NOT IN('nanigans')
						GROUP BY platform, agencyname
					) t1
					WHERE spend > 0
                    GROUP BY platform, adflag";
	 		$week_spend_list = $db_main2->gettotallist($sql);
	 		
	 		$insert_sql = "";
	 		
	 		for($j=0; $j<sizeof($week_spend_list); $j++)
	 		{
	 			$platform = $week_spend_list[$j]["platform"];
	 			$adflag = $week_spend_list[$j]["adflag"];
	 			$spend = $week_spend_list[$j]["spend"];
	 			
	 			if($insert_sql == "")
	 				$insert_sql = "INSERT INTO tbl_marketing_week_stat(today, category, platform, adflag, week_type, spend) VALUES('$today', 3, $platform, '$adflag', $i, $spend)";
	 			else
	 				$insert_sql .= ",('$today', 3, $platform, '$adflag', $i, $spend)";
	 		}
	 		
	 		if($insert_sql != "")
	 		{
	 			$insert_sql .= "ON DUPLICATE KEY UPDATE spend = VALUES(spend);";
	 			$db_other->execute($insert_sql);
	 		}
 		}
 		
 		#
 		#
 		# 등급별 결제액, 결제자수
 		#
 		#
 		$sql = "SELECT *
 				FROM tbl_marketing_week_stat
 				WHERE today = '$today'";
 		$marketing_week_stat_list = $db_other->gettotallist($sql);
 		
 		for($i=0; $i<sizeof($marketing_week_stat_list); $i++)
 		{
 			$platform = $marketing_week_stat_list[$i]["platform"];
 			$adflag = $marketing_week_stat_list[$i]["adflag"];
 			$week_type = $marketing_week_stat_list[$i]["week_type"];
 			
 			$adflag_tail = " AND platform = $platform AND adflag LIKE '$adflag%' ";
 			
 			$sql = "SELECT grade_28, SUM(money) AS total_money, COUNT(DISTINCT useridx) AS user_cnt
					FROM (
						SELECT useridx, ROUND(SUM(money), 2) AS money,
							(CASE WHEN ROUND(SUM(money), 2) >= 499 THEN 5
							WHEN ROUND(SUM(money), 2) >= 199 AND ROUND(SUM(money), 2) < 499 THEN 4
							WHEN ROUND(SUM(money), 2) >= 59 AND ROUND(SUM(money), 2) < 199 THEN 3
							WHEN ROUND(SUM(money), 2) >= 9 AND ROUND(SUM(money), 2) < 59 THEN 2
							WHEN ROUND(SUM(money), 2) >= 0 AND ROUND(SUM(money), 2) < 9 THEN 1
							ELSE 0 END ) AS grade_28
						FROM (
							SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money
							FROM (
								SELECT useridx, createdate
								FROM tbl_user_ext
								WHERE useridx > 20000
 								AND DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 4 + $week_type WEEK) <= createdate AND createdate < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 4 WEEK)
 								$adflag_tail
							) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx
							WHERE STATUS = 1 AND DATEDIFF(writedate, createdate) <= 28
							UNION ALL
							SELECT t1.useridx, money
							FROM (
								SELECT useridx, createdate
								FROM tbl_user_ext
								WHERE useridx > 20000
 								AND DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 4 + $week_type WEEK) <= createdate AND createdate < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 4 WEEK)
 								$adflag_tail
							) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx
							WHERE STATUS = 1 AND DATEDIFF(writedate, createdate) <= 28
						) t1
						GROUP BY useridx
					) total
					GROUP BY grade_28";
 			$grade_pay_info_list = $db_slave_main->gettotallist($sql);
 			
 			$insert_sql = "";
 			
 			$mn_money = 0;
 			$mn_cnt = 0;
 			$dp_money = 0;
 			$dp_cnt = 0;
 			$sl_money = 0;
 			$sl_cnt = 0;
 			$wh_money = 0;
 			$wh_cnt = 0;
 			$kw_money = 0;
 			$kw_cnt = 0;
 			
 			for($j=0; $j<sizeof($grade_pay_info_list); $j++)
 			{
 				$grade_28 = $grade_pay_info_list[$j]["grade_28"];
 				
 				if($grade_28 == 1)
 				{
 					$mn_money = $grade_pay_info_list[$j]["total_money"];
 					$mn_cnt = $grade_pay_info_list[$j]["user_cnt"];	
 				}
 				else if($grade_28 == 2)
 				{
 					$dp_money = $grade_pay_info_list[$j]["total_money"];
 					$dp_cnt = $grade_pay_info_list[$j]["user_cnt"];
 				}
 				else if($grade_28 == 3)
 				{
 					$sl_money = $grade_pay_info_list[$j]["total_money"];
 					$sl_cnt = $grade_pay_info_list[$j]["user_cnt"];
 				}
 				else if($grade_28 == 4)
 				{
 					$wh_money = $grade_pay_info_list[$j]["total_money"];
 					$wh_cnt = $grade_pay_info_list[$j]["user_cnt"];
 				}
 				else if($grade_28 == 5)
 				{
 					$kw_money = $grade_pay_info_list[$j]["total_money"];
 					$kw_cnt = $grade_pay_info_list[$j]["user_cnt"];
 				}
 			}
 			
 			if($insert_sql == "")
 			{
 				$insert_sql = "INSERT INTO tbl_marketing_week_stat(today, category, platform, adflag, week_type, kw_money, kw_cnt, wh_money, wh_cnt, sl_money, sl_cnt, dp_money, dp_cnt, mn_money, mn_cnt)".
 								"VALUES('$today', 3, $platform, '$adflag', $week_type, $kw_money, $kw_cnt, $wh_money, $wh_cnt, $sl_money, $sl_cnt, $dp_money, $dp_cnt, $mn_money, $mn_cnt)";
 			}
 			else
 			{
 				$insert_sql .= ",('$today', 3, $platform, '$adflag', $week_type, $kw_money, $kw_cnt, $wh_money, $wh_cnt, $sl_money, $sl_cnt, $dp_money, $dp_cnt, $mn_money, $mn_cnt) ";
 			}
 			
 			if($insert_sql != "")
 			{
	 			$insert_sql .= "ON DUPLICATE KEY UPDATE kw_money = VALUES(kw_money), kw_cnt = VALUES(kw_cnt), wh_money = VALUES(wh_money), wh_cnt = VALUES(wh_cnt), ".
	 							"sl_money = VALUES(sl_money), sl_cnt = VALUES(sl_cnt), dp_money = VALUES(dp_money), dp_cnt = VALUES(dp_cnt), mn_money = VALUES(mn_money), mn_cnt = VALUES(mn_cnt);";
	 			$db_other->execute($insert_sql);
 			}
 		}
 		
 		#
 		#
 		# 가입자수
 		#
 		#
 		$sql = "SELECT *
		 		FROM tbl_marketing_week_stat
		 		WHERE today = '$today'";
 		$marketing_week_stat_list = $db_other->gettotallist($sql);
 		
 		$insert_sql = "";
 		
 		for($i=0; $i<sizeof($marketing_week_stat_list); $i++)
 		{
 			$platform = $marketing_week_stat_list[$i]["platform"];
 			$adflag = $marketing_week_stat_list[$i]["adflag"];
 			$week_type = $marketing_week_stat_list[$i]["week_type"];
 			
 			$adflag_tail = " AND platform = $platform AND adflag LIKE '$adflag%' ";
 			
 			$sql = "SELECT COUNT(useridx) AS join_user_cnt
					FROM tbl_user_ext
					WHERE useridx > 20000
 					AND DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 4 + $week_type WEEK) <= createdate AND createdate < DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00'), INTERVAL 4 WEEK)
 					$adflag_tail";
 			$join_user_cnt = $db_slave_main->getvalue($sql);
 			
 			if($insert_sql == "")
 			{
 				$insert_sql = "INSERT INTO tbl_marketing_week_stat(today, category, platform, adflag, week_type, user_cnt) VALUES('$today', 3, $platform, '$adflag', $week_type, $join_user_cnt)";
 			}
 			else
 			{
 				$insert_sql .= ",('$today', 3, $platform, '$adflag', $week_type, $join_user_cnt) ";
 			}
 		}
 		
 		if($insert_sql != "")
 		{
 			$insert_sql .= "ON DUPLICATE KEY UPDATE user_cnt = VALUES(user_cnt);";
 			$db_other->execute($insert_sql);
 		}
 	}
 	catch(Exception $e)
 	{
 		write_log($e->getMessage());
 	}
 	
 	$db_slave_main->end();
 	$db_main2->end();
 	$db_other->end();
?>