<?
	include("../../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_mobile = new CDatabase_Mobile();
	
	$db_main->execute("SET wait_timeout=3600");
	$db_main2->execute("SET wait_timeout=3600");
	$db_mobile->execute("SET wait_timeout=3600");
	
	ini_set("memory_limit", "-1");
		
	try
	{
		$sdate = "2018-12-01";
		$edate = "2019-03-18";
		
		while($sdate < $edate)
		{
			$today = $sdate;
			$yesterday = date('Y-m-d', strtotime($sdate.' - 1 day'));
			
			$tomorrow = date("Y-m-d", strtotime($sdate.' - 2 day'));
			$dumy_tomorrow = date("Y-m-d", strtotime($sdate.' - 3 day'));
			

			$install_cnt = 0;
			$update_cnt = 0;
			
			//신규 install : 48시간, retention : 48시간 기준 WHERE appsflyer_device_id = '1552582738038-8511053335698963078'
			$sql = "SELECT logidx, platform, is_retargeting, agency, media_source, fb_ad_id, fb_campaign_name, campaign, fb_adset_name, site_id, appsflyer_device_id, install_time, install_useridx, retention_logidx 
					FROM _tbl_appsflyer_install_2019
					WHERE install_time >= '$yesterday 00:00:00' AND install_time <= '$today 23:59:59'
					UNION ALL 
					SELECT logidx, platform, is_retargeting, agency, media_source, fb_ad_id, fb_campaign_name, fb_adset_name, campaign, site_id, appsflyer_device_id, install_time, install_useridx, retention_logidx 
					FROM `_tbl_appsflyer_install_2019` 
					WHERE install_time >= '$dumy_tomorrow 00:00:00' AND install_time <= '$tomorrow 23:59:59' AND (retention_logidx = 0 OR install_useridx = 0)";
			write_log($sql);
			$apps_list = $db_main2->gettotallist($sql);
			
			for($i=0; $i<sizeof($apps_list); $i++)
			{
				$logidx = $apps_list[$i]["logidx"];
				$platform = $apps_list[$i]["platform"];
				$is_retargeting = $apps_list[$i]["is_retargeting"];
				$agency = $apps_list[$i]["agency"];
				$media_source = $apps_list[$i]["media_source"];
				$fb_ad_id = $apps_list[$i]["fb_ad_id"];
				$fb_campaign_name = $apps_list[$i]["fb_campaign_name"];
				$campaign = $apps_list[$i]["campaign"];
				$fb_adset_name  = $apps_list[$i]["fb_adset_name"];
				$site_id = $apps_list[$i]["site_id"];
				$appsflyer_device_id = $apps_list[$i]["appsflyer_device_id"];
				$install_time = $apps_list[$i]["install_time"];
				$install_useridx = $apps_list[$i]["install_useridx"];
				$retention_logidx = $apps_list[$i]["retention_logidx"];
								
				$tail_mobile_appflyer_sql = "";
				
				if($install_useridx == 0)
					$install_useridx = "";
				
				if($retention_logidx == 0)
				{
					$retention_logidx = "";
					$str_retention_useridx = "";
				}
				
				//install useridx가 더 추가되는 경우가 있는지 확인는 플래그 
				$first_cnt = 0;
				
				//한번 install된 useridx는 제외(테이블에 데이터 존재)
				if($install_useridx != "")
				{
					$tail_mobile_appflyer_sql =  " AND useridx NOT IN ($install_useridx)";
					
					$first_cnt = 1;
				}
				
				//retention에 한번이라도 적용된 유저 제외(retention_logidx를 이용하여 useridx 구함)
				if($retention_logidx != "")
				{
					$sql = "SELECT useridx FROM _tbl_user_retention_mobile_log_2019 WHERE rtidx IN ($retention_logidx)";
					$retention_user_list = $db_main2->gettotallist($sql);
					
					for($k=0; $k<=sizeof($retention_user_list);$k++)
					{
						$retention_useridx = $retention_user_list[$k]["useridx"];						
						
						if($retention_useridx != "")
						{
							if($str_retention_useridx == "")
							{
								$str_retention_useridx = $retention_useridx;
							}
							else
							{
								$str_retention_useridx .= ",".$retention_useridx;
							}
						}
					}
					
					
					if($install_useridx != "")
					{
						//retenion_logidx에서 뽑은 useridx도 binding 함(install_useridx + retention_log_useridx)
						$install_useridx .= ",".$str_retention_useridx;
						
						$tail_mobile_appflyer_sql =  " AND useridx NOT IN ($install_useridx)";
					}						
					else if($str_retention_useridx != "")
						$tail_mobile_appflyer_sql =  " AND useridx NOT IN ($str_retention_useridx)";
						
				}

				//기존 useridx 제외하고 appsflyerid로 조회 되는 userix 다 가져오기
				$sql = "SELECT useridx FROM tbl_user_mobile_appsflyer_new WHERE appsflyerid='$appsflyer_device_id' $tail_mobile_appflyer_sql";
				$user_list = $db_main2->gettotallist($sql);

				$str_useridx = "";
				$str_install_useridx =  "";
				$str_retention_logidx =  "";
				
				//useridx 제외 시키고 남은 useridx 가공
				for($k=0; $k<=sizeof($user_list);$k++)
				{
					$useridx = $user_list[$k]["useridx"];
					
					//useridx 담기 : tbl_user_ext 공통 chain_useridx binding 위해 필요
					$tmp_useridx = $useridx;				
					
					if($tmp_useridx != "")
					{
						if($str_useridx == "")
						{
							$str_useridx = $tmp_useridx;
						}
						else
						{
							$str_useridx .= ",".$tmp_useridx;
						}
						
						$first_cnt++;
					}

					//media_source 처리
					$campaign_id = 0;
						
					if($media_source == "googleadwords_int")
					{
						$campaign_id = $apps_list[$i]["campaign"];
					}
					
					if($media_source == "Facebook Ads")
					{
						if($agency == "nanigans")
							$media_source = $agency;
						else if($agency == "socialclicks" )
							$media_source = $agency;
					}
					else if($media_source == "")
					{
						if($agency == "amazon")
							$media_source = $agency;
					}
					
					if(strpos($fb_campaign_name, "m_retention") !== false)
					{
						$campaign_name_arr = explode("_", $fb_campaign_name);
						$media_source = "m_".$campaign_name_arr[2];
					}
					else if(strpos($fb_campaign_name, "retention") !== false)
					{
						$campaign_name_arr = explode("_", $fb_campaign_name);
						$media_source = $campaign_name_arr[1];
					}
					else if(strpos($fb_campaign_name, "m_duc") !== false || strpos($fb_campaign_name, "m_ddi") !== false)
					{
						$campaign_name_arr = explode("_", $fb_campaign_name);
					
						if($campaign_name_arr[1] == "m")
							$media_source = "m_".$campaign_name_arr[2]."_int";
						else
							$media_source = "m_".$campaign_name_arr[3]."_int";
					}					
					
					if($useridx != "")
					{
						// 모바일 신규 체크
						$sql = "SELECT DATEDIFF('$install_time', t1.createdate) AS install_diff,
								IF(TIMESTAMPDIFF(HOUR, '$install_time' , t1.createdate) BETWEEN 0 AND 48, 1, 0) AS is_install,  t1.createdate
								FROM tbl_user_mobile_connection_log t1 JOIN tbl_mobile t2 ON t1.device_id = t2.device_id
								WHERE useridx = $useridx
								ORDER BY createdate ASC
								LIMIT 1";
						$install_info = $db_mobile->getarray($sql);
					
						$install_diff = $install_info["install_diff"];
						$is_install = $install_info["is_install"];
						$createdate = $install_info["createdate"];
					
						if($is_install > 0)
						{
							$sql = "SELECT IF(TIMESTAMPDIFF(HOUR, '$install_time' , createdate) BETWEEN 0 AND 48, 1, 0) AS is_install FROM tbl_user_ext WHERE useridx=$useridx";
							$is_newuser = $db_main->getvalue($sql);
					
							$sql = "SELECT adflag FROM tbl_user_ext WHERE useridx = $useridx";
							$ext_adflag = $db_main->getvalue($sql);
								
							if($is_newuser > 0)
							{
								// 완전 신규
								$sql = "UPDATE tbl_user_ext SET adflag = '$media_source', ifcontext = '$site_id' WHERE useridx=$useridx";
								//$db_main->execute($sql);
									
								$sql = "UPDATE tbl_user_ext SET chain_adflag = '$media_source' WHERE useridx=$useridx";
								//$db_main->execute($sql);
					
								if($logidx > 0)
								{
									//appsflyer install_useridx 묶기
									if($install_useridx != "" && $k == 0)
									{
										$str_install_useridx = $install_useridx;
									}
					
									if($str_install_useridx == "")
									{
										$str_install_useridx = $useridx;
									}
									else
									{
										$str_install_useridx .= ",".$useridx;
									}									
								}
							}					
						}
						else
						{
							//appsflyer install 후 가장 최신 데이터 하나만 가져옴
							$sql = "SELECT rtidx, ".
									"IF(TIMESTAMPDIFF(HOUR, '$install_time', writedate) BETWEEN 0 AND 48, 1, 0) AS is_retention, adflag, writedate ".
									"FROM _tbl_user_retention_mobile_log_2019 ".
									"WHERE useridx = $useridx AND platform = $platform AND '$install_time' <= writedate ORDER BY rtidx ASC LIMIT 1;";
							$retention_mobile_log_list = $db_main2->getarray($sql);
								
							$rtidx = $retention_mobile_log_list["rtidx"];
							$is_install = $retention_mobile_log_list["is_install"];
							$is_retention = $retention_mobile_log_list["is_retention"];
							$adflag = $retention_mobile_log_list["adflag"];
							$writedate = $retention_mobile_log_list["writedate"];
								
							if($is_retention > 0)
							{
								if($media_source == "liftoff_int" && $is_retargeting == 1)
								{
									$media_source = $media_source."_retarget";
								}
									
								$sql = "UPDATE _tbl_user_retention_mobile_log_2019 SET adflag = '$media_source' WHERE rtidx=$rtidx ";								
								//$db_main2->execute($sql);
									
								if($logidx > 0)
								{
									//appsflyer retention_logidx 묶기
									if($retention_logidx != "" && $k == 0)
									{
										$str_retention_logidx = $retention_logidx;
									}
									
									if($str_retention_logidx == "")
									{
										$str_retention_logidx = $rtidx;
									}
									else if($retention_logidx != $rtidx)
									{
										$str_retention_logidx .= ",".$rtidx;
									}
								}
							}
						}
					}			
				}
				
				//더 이미 useridx가 추가된 경우에 // AND createdate >= DATE_SUB('$install_time', INTERVAL 1 HOUR) AND createdate <= DATE_ADD('$install_time', INTERVAL 1 HOUR) 
				if($str_install_useridx != "" && $first_cnt > 1)
				{
					$sql = "SELECT useridx, createdate FROM tbl_user_ext ".
							"WHERE useridx IN ($str_install_useridx) ORDER BY useridx ASC LIMIT 1";
					$chain_info = $db_main->getarray($sql);
				
					$chain_useridx = $chain_info["useridx"];
					$chain_createdate = $chain_info["createdate"];
					
					if($chain_useridx != '' && $chain_createdate != '')
					{
						$sql = "UPDATE tbl_user_ext SET chain_useridx=$chain_useridx, chain_adflag='$media_source', chain_createdate='$chain_createdate' WHERE platform > 0 AND useridx IN ($str_useridx) AND createdate >= '$chain_createdate'";
						$db_main->execute($sql);
					}
				}				

					
				if($str_install_useridx != "")
				{
					$sql = "UPDATE _tbl_appsflyer_install_2019 SET install_useridx = '$str_install_useridx' WHERE logidx = $logidx";
					//write_log($sql);
					//$db_main2->execute($sql);
				}
				
				if($str_retention_logidx != "")
				{
					$sql = "UPDATE _tbl_appsflyer_install_2019 SET retention_logidx = '$str_retention_logidx' WHERE logidx = $logidx";
					//write_log($sql);
					//$db_main2->execute($sql);
				}
			}	
			
			write_log("$yesterday ~ $today appsflyer install cnt: $install_cnt update cnt:".$update_cnt);
			
			$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();
	$db_mobile->end();
?>