<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/marketing/marketing_roi_factor_grade_7") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/marketing/marketing_roi_factor_grade_7") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("marketing/marketing_roi_factor_grade_7 Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	$db_other->execute("SET wait_timeout=14400");
	
	$today = date("Y-m-d", strtotime("-1 days"));
	$current_date = date("Y-m-d");
	try
	{
		#
		#
		#
		# 결제자
		#
		#
		#
		$sql = "Select platform, grade,						
		
		ROUND(SUM(CASE WHEN progress <= 	8	 AND progress >=0  and distance <= 	190	 and distance >=	9	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	9	 AND progress >=0  and distance <= 	191	 and distance >=	10	 then pay::float END),1)        
		*ROUND(SUM(CASE WHEN progress <= 	10	 AND progress >=0  and distance <= 	192	 and distance >=	11	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	11	 AND progress >=0  and distance <= 	193	 and distance >=	12	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	12	 AND progress >=0  and distance <= 	194	 and distance >=	13	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	13	 AND progress >=0  and distance <= 	195	 and distance >=	14	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	14	 AND progress >=0  and distance <= 	196	 and distance >=	15	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	15	 AND progress >=0  and distance <= 	197	 and distance >=	15	 then pay::float END),1)        
		/ROUND(SUM(CASE WHEN progress <= 	7	 AND progress >=0  and distance <= 	190	 and distance >=	8	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	8	 AND progress >=0  and distance <= 	191	 and distance >=	9	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	9	 AND progress >=0  and distance <= 	192	 and distance >=	10	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	10	 AND progress >=0  and distance <= 	193	 and distance >=	11	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	11	 AND progress >=0  and distance <= 	194	 and distance >=	12	 then pay::float END),1)        
		/ROUND(SUM(CASE WHEN progress <= 	12	 AND progress >=0  and distance <= 	195	 and distance >=	13	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	13	 AND progress >=0  and distance <= 	196	 and distance >=	14	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	16	 AND progress >=0  and distance <= 	198	 and distance >=	16	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	17	 AND progress >=0  and distance <=	199	 and distance >=	17	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	18	 AND progress >=0  and distance <=	200	 and distance >=	18	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	19	 AND progress >=0  and distance <=	201	 and distance >=	19	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	20	 AND progress >=0  and distance <=	202	 and distance >=	20	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	21	 AND progress >=0  and distance <=	203	 and distance >=	21	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	22	 AND progress >=0  and distance <=	204	 and distance >=	22	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	23	 AND progress >=0  and distance <=	205	 and distance >=	23	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	24	 AND progress >=0  and distance <=	206	 and distance >=	24	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	25	 AND progress >=0  and distance <=	207	 and distance >=	25	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	26	 AND progress >=0  and distance <= 	208	 and distance >=	26	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	27	 AND progress >=0  and distance <= 	209	 and distance >=	27	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	28	 AND progress >=0  and distance <=	210	 and distance >=	28	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	29	 AND progress >=0  and distance <=	211	 and distance >=	29	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	14	 AND progress >=0  and distance <= 	197	 and distance >=	15	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	15	 AND progress >=0  and distance <= 	198	 and distance >=	16	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	16	 AND progress >=0  and distance <= 	199	 and distance >=	17	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	17	 AND progress >=0  and distance <= 	200	 and distance >=	18	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	18	 AND progress >=0  and distance <= 	201	 and distance >=	19	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	19	 AND progress >=0  and distance <= 	202	 and distance >=	20	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	20	 AND progress >=0  and distance <= 	203	 and distance >=	21	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	21	 AND progress >=0  and distance <= 	204	 and distance >=	22	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	22	 AND progress >=0  and distance <= 	205	 and distance >=	23	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	23	 AND progress >=0  and distance <= 	206	 and distance >=	24	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	24	 AND progress >=0  and distance <= 	207	 and distance >=	25	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	25	 AND progress >=0  and distance <= 	208	 and distance >=	26	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	26	 AND progress >=0  and distance <= 	209	 and distance >=	27	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	27	 AND progress >=0  and distance <= 	210	 and distance >=	28	 then pay::float END),1)        
		*ROUND(SUM(CASE WHEN progress <= 	30	 AND progress >=0 and distance <= 212	 and distance >=	30	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	31	 AND progress >=0 and distance <= 213	 and distance >=	31	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	32	 AND progress >=0 and distance <=	214	 and distance >=	32	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	33	 AND progress >=0 and distance <=	215	 and distance >=	33	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	34	 AND progress >=0 and distance <=	216	 and distance >=	34	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	35	 AND progress >=0 and distance <=	217	 and distance >=	35	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	36	 AND progress >=0 and distance <=	218	 and distance >=	36	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	37	 AND progress >=0 and distance <=	219	 and distance >=	37	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	38	 AND progress >=0 and distance <=	220	 and distance >=	38	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	39	 AND progress >=0 and distance <=	221	 and distance >=	39	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	40	 AND progress >=0 and distance <=	222	 and distance >=	40	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	28	 AND progress >=0 and distance <= 211	 and distance >=	29	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	29	 AND progress >=0 and distance <= 212	 and distance >=	30	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	30	 AND progress >=0 and distance <=	213	 and distance >=	31	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	31	 AND progress >=0 and distance <=	214	 and distance >=	32	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	32	 AND progress >=0 and distance <=	215	 and distance >=	33	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	33	 AND progress >=0 and distance <=	216	 and distance >=	34	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	34	 AND progress >=0 and distance <=	217	 and distance >=	35	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	35	 AND progress >=0 and distance <=	218	 and distance >=	36	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	36	 AND progress >=0 and distance <=	219	 and distance >=	37	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	37	 AND progress >=0 and distance <=	220	 and distance >=	38	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	38	 AND progress >=0 and distance <=	221	 and distance >=	39	 then pay::float END),1)
								
								
		*ROUND(SUM(CASE WHEN progress <= 	41	 AND progress >=0 and distance <=	223	 and distance >=	41	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	42	 AND progress >=0 and distance <=	224	 and distance >=	42	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	43	 AND progress >=0 and distance <=	225	 and distance >=	43	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	44	 AND progress >=0 and distance <=	226	 and distance >=	44	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	45	 AND progress >=0 and distance <=	227	 and distance >=	45	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	46	 AND progress >=0 and distance <=	228	 and distance >=	46	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	47	 AND progress >=0 and distance <=	229	 and distance >=	47	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	48	 AND progress >=0 and distance <=	230	 and distance >=	48	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	49	 AND progress >=0 and distance <=	231	 and distance >=	49	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	50	 AND progress >=0 and distance <=	232	 and distance >=	50	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	51	 AND progress >=0 and distance <=	233	 and distance >=	51	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	52	 AND progress >=0 and distance <=	234	 and distance >=	52	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	53	 AND progress >=0 and distance <=	235	 and distance >=	53	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	54	 AND progress >=0 and distance <=	236	 and distance >=	54	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	55	 AND progress >=0 and distance <=	237	 and distance >=	55	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	56	 AND progress >=0 and distance <=	238	 and distance >=	56	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	57	 AND progress >=0 and distance <=	239	 and distance >=	57	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	58	 AND progress >=0 and distance <=	240	 and distance >=	58	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	59	 AND progress >=0 and distance <=	241	 and distance >=	59	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	60	 AND progress >=0 and distance <=	242	 and distance >=	60	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	39	 AND progress >=0 and distance <=	222	 and distance >=	40	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	40	 AND progress >=0 and distance <=	223	 and distance >=	41	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	41	 AND progress >=0 and distance <=	224	 and distance >=	42	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	42	 AND progress >=0 and distance <=	225	 and distance >=	43	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	43	 AND progress >=0 and distance <=	226	 and distance >=	44	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	44	 AND progress >=0 and distance <=	227	 and distance >=	45	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	45	 AND progress >=0 and distance <=	228	 and distance >=	46	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	46	 AND progress >=0 and distance <=	229	 and distance >=	47	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	47	 AND progress >=0 and distance <=	230	 and distance >=	48	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	48	 AND progress >=0 and distance <=	231	 and distance >=	49	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	49	 AND progress >=0 and distance <=	232	 and distance >=	50	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	50	 AND progress >=0 and distance <=	233	 and distance >=	51	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	51	 AND progress >=0 and distance <=	234	 and distance >=	52	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	52	 AND progress >=0 and distance <=	235	 and distance >=	53	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	53	 AND progress >=0 and distance <=	236	 and distance >=	54	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	54	 AND progress >=0 and distance <=	237	 and distance >=	55	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	55	 AND progress >=0 and distance <=	238	 and distance >=	56	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	56	 AND progress >=0 and distance <=	239	 and distance >=	57	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	57	 AND progress >=0 and distance <=	240	 and distance >=	58	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	58	 AND progress >=0 and distance <=	241	 and distance >=	59	 then pay::float END),1)
								
								
		*ROUND(SUM(CASE WHEN progress <= 	61	 AND progress >=0 and distance <=	243	 and distance >=	61	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	62	 AND progress >=0 and distance <=	244	 and distance >=	62	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	63	 AND progress >=0 and distance <=	245	 and distance >=	63	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	64	 AND progress >=0 and distance <=	246	 and distance >=	64	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	65	 AND progress >=0 and distance <=	247	 and distance >=	65	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	66	 AND progress >=0 and distance <=	248	 and distance >=	66	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	67	 AND progress >=0 and distance <=	249	 and distance >=	67	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	68	 AND progress >=0 and distance <=	250	 and distance >=	68	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	69	 AND progress >=0 and distance <=	251	 and distance >=	69	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	70	 AND progress >=0 and distance <=	252	 and distance >=	70	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	71	 AND progress >=0 and distance <=	253	 and distance >=	71	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	72	 AND progress >=0 and distance <=	254	 and distance >=	72	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	73	 AND progress >=0 and distance <=	255	 and distance >=	73	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	74	 AND progress >=0 and distance <=	256	 and distance >=	74	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	75	 AND progress >=0 and distance <=	257	 and distance >=	75	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	76	 AND progress >=0 and distance <=	258	 and distance >=	76	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	77	 AND progress >=0 and distance <=	259	 and distance >=	77	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	78	 AND progress >=0 and distance <=	260	 and distance >=	78	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	79	 AND progress >=0 and distance <=	261	 and distance >=	79	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	80	 AND progress >=0 and distance <=	262	 and distance >=	80	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	59	 AND progress >=0 and distance <=	242	 and distance >=	60	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	60	 AND progress >=0 and distance <=	243	 and distance >=	61	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	61	 AND progress >=0 and distance <=	244	 and distance >=	62	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	62	 AND progress >=0 and distance <=	245	 and distance >=	63	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	63	 AND progress >=0 and distance <=	246	 and distance >=	64	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	64	 AND progress >=0 and distance <=	247	 and distance >=	65	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	65	 AND progress >=0 and distance <=	248	 and distance >=	66	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	66	 AND progress >=0 and distance <=	249	 and distance >=	67	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	67	 AND progress >=0 and distance <=	250	 and distance >=	68	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	68	 AND progress >=0 and distance <=	251	 and distance >=	69	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	69	 AND progress >=0 and distance <=	252	 and distance >=	70	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	70	 AND progress >=0 and distance <=	253	 and distance >=	71	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	71	 AND progress >=0 and distance <=	254	 and distance >=	72	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	72	 AND progress >=0 and distance <=	255	 and distance >=	73	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	73	 AND progress >=0 and distance <=	256	 and distance >=	74	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	74	 AND progress >=0 and distance <=	257	 and distance >=	75	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	75	 AND progress >=0 and distance <=	258	 and distance >=	76	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	76	 AND progress >=0 and distance <=	259	 and distance >=	77	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	77	 AND progress >=0 and distance <=	260	 and distance >=	78	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	78	 AND progress >=0 and distance <=	261	 and distance >=	79	 then pay::float END),1)
								
								
		*ROUND(SUM(CASE WHEN progress <= 	81	 AND progress >=0 and distance <=	263	 and distance >=	81	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	82	 AND progress >=0 and distance <=	264	 and distance >=	82	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	83	 AND progress >=0 and distance <=	265	 and distance >=	83	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	84	 AND progress >=0 and distance <=	266	 and distance >=	84	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	85	 AND progress >=0 and distance <=	267	 and distance >=	85	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	86	 AND progress >=0 and distance <=	268	 and distance >=	86	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	87	 AND progress >=0 and distance <=	269	 and distance >=	87	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	88	 AND progress >=0 and distance <=	270	 and distance >=	88	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	89	 AND progress >=0 and distance <=	271	 and distance >=	89	 then pay::float END),1)
		*ROUND(SUM(CASE WHEN progress <= 	90	 AND progress >=0 and distance <=	272	 and distance >=	90	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	79	 AND progress >=0 and distance <=	262	 and distance >=	80	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	80	 AND progress >=0 and distance <=	263	 and distance >=	81	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	81	 AND progress >=0 and distance <=	264	 and distance >=	82	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	82	 AND progress >=0 and distance <=	265	 and distance >=	83	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	83	 AND progress >=0 and distance <=	266	 and distance >=	84	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	84	 AND progress >=0 and distance <=	267	 and distance >=	85	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	85	 AND progress >=0 and distance <=	268	 and distance >=	86	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	86	 AND progress >=0 and distance <=	269	 and distance >=	87	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	87	 AND progress >=0 and distance <=	270	 and distance >=	88	 then pay::float END),1)
		/ROUND(SUM(CASE WHEN progress <= 	88	 AND progress >=0 and distance <=	271	 and distance >=	89	 then pay::float END),1) as multiple,
		ROUND(SUM(CASE WHEN progress <= 	89	 AND progress >=0 and distance <=	272	 and distance >=	90	 then pay::float END),1) as divd
		
		FROM (						
			SELECT platform, tt.useridx, grade, pay, progress, distance					
			FROM (					
		     		SELECT platform, useridx, money AS pay, ceil(datediff(second, createdate, writedate)/86400::float) as progress, ceil(datediff(second, createdate, dateadd(day, 1, '$current_date'::date))/86400::float) as distance				
		    		FROM (				
		        			SELECT t1.useridx, t2.writedate, ROUND(facebookcredit::float/10::float, 2) AS money, t1.createdate, platform			
		        			FROM (			
		            				SELECT useridx, createdate, platform		
		            				FROM t5_user		
		            				WHERE useridx > 20000 and createdate >= dateadd(day,(-273), '$current_date'::date)		
		              			) t1 JOIN t5_product_order t2 on t1.useridx = t2.useridx			 
		        			WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 90			
		        			UNION ALL			
		        			SELECT t1.useridx, t2.writedate, ROUND(money, 2) AS pay, t1.createdate, platform			
		        			FROM (			
		            				SELECT useridx, createdate, platform		
		            				FROM t5_user		
		            				WHERE useridx > 20000 and createdate >= dateadd(day,(-273), '$current_date'::date)		
		              			) t1 JOIN t5_product_order_mobile t2 on t1.useridx = t2.useridx			
		         			WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 90			
		          		) AS payer_t				
		        	) tt join (					
				SELECT useridx, 		 		
						(case when ROUND(SUM(money), 2) >= 499 then 5		
		   					  when ROUND(SUM(money), 2) >= 199 and ROUND(SUM(money), 2) < 499 then 4	
			  				  when ROUND(SUM(money), 2) >= 59 and ROUND(SUM(money), 2) < 199 then 3	
				  			  when ROUND(SUM(money), 2) >= 9 and ROUND(SUM(money), 2) < 59 then 2	
					  		  when ROUND(SUM(money), 2) < 9 and ROUND(SUM(money), 2) > 0 then 1	
		      				else 0 end) AS grade		
		 		FROM (				
		    			SELECT t1.useridx, t2.writedate, ROUND(facebookcredit::float/10::float, 2) AS money, t1.createdate, platform			
		    			FROM (			
		        				SELECT useridx, createdate, platform, dateadd(day, 1, '$current_date'::date) as today  		
		        				FROM t5_user		
		        				WHERE useridx > 20000 and createdate >= dateadd(day,(-273), '$current_date'::date)		
		          			) t1 JOIN t5_product_order t2 on t1.useridx = t2.useridx			
		    			WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 7			
		    			UNION ALL			
		      		SELECT t1.useridx, t2.writedate, ROUND(money, 2) AS pay, t1.createdate, platform			
					FROM (			
						SELECT useridx, createdate, platform, dateadd(day, 1, '$current_date'::date) as today  		
						FROM t5_user		
						WHERE useridx > 20000 and createdate >= dateadd(day,(-273), '$current_date'::date)		
					) t1 JOIN t5_product_order_mobile t2 on t1.useridx = t2.useridx			
					WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 7			
				) AS payer_t				
				GROUP BY useridx				
			) as grade on tt.useridx = grade.useridx					
		) total
		WHERE platform != 0
						GROUP BY platform, grade		
						ORDER BY platform, grade
						";
		$payer_roi_factor_list = $db_redshift->gettotallist($sql);
		
		$insert_sql1 = "";
		$insert_sql2 = "";
		
		for($i=0; $i<sizeof($payer_roi_factor_list); $i++)
		{
			$platform = $payer_roi_factor_list[$i]["platform"];
			$grade_7 = $payer_roi_factor_list[$i]["grade"];
			$multiple = ($payer_roi_factor_list[$i]["multiple"]=="")?0:$payer_roi_factor_list[$i]["multiple"];
			$divd = ($payer_roi_factor_list[$i]["divd"] == "" )? 0 : $payer_roi_factor_list[$i]["divd"];
			
			if($insert_sql1 == "")
			{
				$insert_sql1 = "INSERT INTO tbl_roi_factor VALUES('$today', 0, 1, 1, $platform, $grade_7, $multiple, $divd)";
				$insert_sql2 = "INSERT INTO tbl_roi_factor VALUES('$today', 1, 1, 1, $platform, $grade_7, $multiple, $divd)";
			}
			else
			{
				$insert_sql1 .= ",('$today', 0, 1, 1, $platform, $grade_7, $multiple, $divd)";
				$insert_sql2 .= ",('$today', 1, 1, 1, $platform, $grade_7, $multiple, $divd)";
			}
		}
		
		if($insert_sql1 != "")
		{
			$insert_sql1 .= "ON DUPLICATE KEY UPDATE multiple = VALUES(multiple), divd = VALUES(divd) ";
			$insert_sql2 .= "ON DUPLICATE KEY UPDATE multiple = VALUES(multiple), divd = VALUES(divd) ";
			$db_other->execute($insert_sql1);
			$db_other->execute($insert_sql2);
		}
		
		#
		#
		# 비결제자
		#
		#
		#
		$sql = "Select platform,						
						
				ROUND(SUM(CASE WHEN progress <=	8	   AND progress >=0  and distance <=	190	 and distance >=	8	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <=	9	   AND progress >=0  and distance <=	191	 and distance >=	9	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <=	10	 AND progress >=0  and distance <=	192	 and distance >=	10	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <=	11	 AND progress >=0  and distance <=	193	 and distance >=	11	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <=	12	 AND progress >=0  and distance <=	194	 and distance >=	12	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <=	13	 AND progress >=0  and distance <=	195	 and distance >=	13	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <=	14	 AND progress >=0  and distance <=	196	 and distance >=	14	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <=	15	 AND progress >=0  and distance <=	197	 and distance >=	15	 then pay::float END),1)													
				/ROUND(SUM(CASE WHEN progress <=	7	   AND progress >=0  and distance <=	190	 and distance >=	8  	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <=	8	   AND progress >=0  and distance <=	191	 and distance >=	9	   then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <=	9	   AND progress >=0  and distance <=	192	 and distance >=	10	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <=	10	 AND progress >=0  and distance <=	193	 and distance >=	11	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <=	11	 AND progress >=0  and distance <=	194	 and distance >=	12	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <=	12	 AND progress >=0  and distance <=	195	 and distance >=	13	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <=	13	 AND progress >=0  and distance <=	196	 and distance >=	14	 then pay::float END),1)	       
				*ROUND(SUM(CASE WHEN progress <= 	16	 AND progress >=0  and distance <= 	198	 and distance >=	16	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <= 	17	 AND progress >=0  and distance <=	199	 and distance >=	17	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <= 	18	 AND progress >=0  and distance <=	200	 and distance >=	18	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <= 	19	 AND progress >=0  and distance <=	201	 and distance >=	19	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <= 	20	 AND progress >=0  and distance <=	202	 and distance >=	20	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <= 	21	 AND progress >=0  and distance <=	203	 and distance >=	21	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <= 	22	 AND progress >=0  and distance <=	204	 and distance >=	22	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <= 	23	 AND progress >=0  and distance <=	205	 and distance >=	23	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <= 	24	 AND progress >=0  and distance <=	206	 and distance >=	24	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <= 	25	 AND progress >=0  and distance <=	207	 and distance >=	25	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <= 	26	 AND progress >=0  and distance <= 	208	 and distance >=	26	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <= 	27	 AND progress >=0  and distance <= 	209	 and distance >=	27	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <= 	28	 AND progress >=0  and distance <=	210	 and distance >=	28	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN progress <= 	29	 AND progress >=0  and distance <=	211	 and distance >=	29	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <= 	14	 AND progress >=0  and distance <= 	197	 and distance >=	15	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <= 	15	 AND progress >=0  and distance <= 	198	 and distance >=	16	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <= 	16	 AND progress >=0  and distance <= 	199	 and distance >=	17	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <= 	17	 AND progress >=0  and distance <= 	200	 and distance >=	18	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <= 	18	 AND progress >=0  and distance <= 	201	 and distance >=	19	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <= 	19	 AND progress >=0  and distance <= 	202	 and distance >=	20	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <= 	20	 AND progress >=0  and distance <= 	203	 and distance >=	21	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <= 	21	 AND progress >=0  and distance <= 	204	 and distance >=	22	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <= 	22	 AND progress >=0  and distance <= 	205	 and distance >=	23	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <= 	23	 AND progress >=0  and distance <= 	206	 and distance >=	24	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <= 	24	 AND progress >=0  and distance <= 	207	 and distance >=	25	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <= 	25	 AND progress >=0  and distance <= 	208	 and distance >=	26	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <= 	26	 AND progress >=0  and distance <= 	209	 and distance >=	27	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN progress <= 	27	 AND progress >=0  and distance <= 	210	 and distance >=	28	 then pay::float END),1)	
										
				
				*ROUND(SUM(CASE WHEN progress <= 	30	 AND progress >=0 and distance <= 212	 and distance >=	30	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	31	 AND progress >=0 and distance <= 213	 and distance >=	31	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	32	 AND progress >=0 and distance <=	214	 and distance >=	32	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	33	 AND progress >=0 and distance <=	215	 and distance >=	33	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	34	 AND progress >=0 and distance <=	216	 and distance >=	34	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	35	 AND progress >=0 and distance <=	217	 and distance >=	35	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	36	 AND progress >=0 and distance <=	218	 and distance >=	36	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	37	 AND progress >=0 and distance <=	219	 and distance >=	37	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	38	 AND progress >=0 and distance <=	220	 and distance >=	38	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	39	 AND progress >=0 and distance <=	221	 and distance >=	39	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	40	 AND progress >=0 and distance <=	222	 and distance >=	40	 then pay::float END),1)
										
										
				/ROUND(SUM(CASE WHEN progress <= 	28	 AND progress >=0 and distance <= 211	 and distance >=	29	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	29	 AND progress >=0 and distance <= 212	 and distance >=	30	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	30	 AND progress >=0 and distance <=	213	 and distance >=	31	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	31	 AND progress >=0 and distance <=	214	 and distance >=	32	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	32	 AND progress >=0 and distance <=	215	 and distance >=	33	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	33	 AND progress >=0 and distance <=	216	 and distance >=	34	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	34	 AND progress >=0 and distance <=	217	 and distance >=	35	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	35	 AND progress >=0 and distance <=	218	 and distance >=	36	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	36	 AND progress >=0 and distance <=	219	 and distance >=	37	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	37	 AND progress >=0 and distance <=	220	 and distance >=	38	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	38	 AND progress >=0 and distance <=	221	 and distance >=	39	 then pay::float END),1)
										
										
				*ROUND(SUM(CASE WHEN progress <= 	41	 AND progress >=0 and distance <=	223	 and distance >=	41	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	42	 AND progress >=0 and distance <=	224	 and distance >=	42	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	43	 AND progress >=0 and distance <=	225	 and distance >=	43	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	44	 AND progress >=0 and distance <=	226	 and distance >=	44	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	45	 AND progress >=0 and distance <=	227	 and distance >=	45	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	46	 AND progress >=0 and distance <=	228	 and distance >=	46	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	47	 AND progress >=0 and distance <=	229	 and distance >=	47	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	48	 AND progress >=0 and distance <=	230	 and distance >=	48	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	49	 AND progress >=0 and distance <=	231	 and distance >=	49	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	50	 AND progress >=0 and distance <=	232	 and distance >=	50	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	51	 AND progress >=0 and distance <=	233	 and distance >=	51	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	52	 AND progress >=0 and distance <=	234	 and distance >=	52	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	53	 AND progress >=0 and distance <=	235	 and distance >=	53	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	54	 AND progress >=0 and distance <=	236	 and distance >=	54	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	55	 AND progress >=0 and distance <=	237	 and distance >=	55	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	56	 AND progress >=0 and distance <=	238	 and distance >=	56	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	57	 AND progress >=0 and distance <=	239	 and distance >=	57	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	58	 AND progress >=0 and distance <=	240	 and distance >=	58	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	59	 AND progress >=0 and distance <=	241	 and distance >=	59	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	60	 AND progress >=0 and distance <=	242	 and distance >=	60	 then pay::float END),1)
										
										
				/ROUND(SUM(CASE WHEN progress <= 	39	 AND progress >=0 and distance <=	222	 and distance >=	40	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	40	 AND progress >=0 and distance <=	223	 and distance >=	41	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	41	 AND progress >=0 and distance <=	224	 and distance >=	42	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	42	 AND progress >=0 and distance <=	225	 and distance >=	43	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	43	 AND progress >=0 and distance <=	226	 and distance >=	44	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	44	 AND progress >=0 and distance <=	227	 and distance >=	45	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	45	 AND progress >=0 and distance <=	228	 and distance >=	46	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	46	 AND progress >=0 and distance <=	229	 and distance >=	47	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	47	 AND progress >=0 and distance <=	230	 and distance >=	48	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	48	 AND progress >=0 and distance <=	231	 and distance >=	49	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	49	 AND progress >=0 and distance <=	232	 and distance >=	50	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	50	 AND progress >=0 and distance <=	233	 and distance >=	51	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	51	 AND progress >=0 and distance <=	234	 and distance >=	52	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	52	 AND progress >=0 and distance <=	235	 and distance >=	53	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	53	 AND progress >=0 and distance <=	236	 and distance >=	54	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	54	 AND progress >=0 and distance <=	237	 and distance >=	55	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	55	 AND progress >=0 and distance <=	238	 and distance >=	56	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	56	 AND progress >=0 and distance <=	239	 and distance >=	57	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	57	 AND progress >=0 and distance <=	240	 and distance >=	58	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	58	 AND progress >=0 and distance <=	241	 and distance >=	59	 then pay::float END),1)
										
										
				*ROUND(SUM(CASE WHEN progress <= 	61	 AND progress >=0 and distance <=	243	 and distance >=	61	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	62	 AND progress >=0 and distance <=	244	 and distance >=	62	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	63	 AND progress >=0 and distance <=	245	 and distance >=	63	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	64	 AND progress >=0 and distance <=	246	 and distance >=	64	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	65	 AND progress >=0 and distance <=	247	 and distance >=	65	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	66	 AND progress >=0 and distance <=	248	 and distance >=	66	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	67	 AND progress >=0 and distance <=	249	 and distance >=	67	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	68	 AND progress >=0 and distance <=	250	 and distance >=	68	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	69	 AND progress >=0 and distance <=	251	 and distance >=	69	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	70	 AND progress >=0 and distance <=	252	 and distance >=	70	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	71	 AND progress >=0 and distance <=	253	 and distance >=	71	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	72	 AND progress >=0 and distance <=	254	 and distance >=	72	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	73	 AND progress >=0 and distance <=	255	 and distance >=	73	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	74	 AND progress >=0 and distance <=	256	 and distance >=	74	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	75	 AND progress >=0 and distance <=	257	 and distance >=	75	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	76	 AND progress >=0 and distance <=	258	 and distance >=	76	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	77	 AND progress >=0 and distance <=	259	 and distance >=	77	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	78	 AND progress >=0 and distance <=	260	 and distance >=	78	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	79	 AND progress >=0 and distance <=	261	 and distance >=	79	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	80	 AND progress >=0 and distance <=	262	 and distance >=	80	 then pay::float END),1)
										
										
				/ROUND(SUM(CASE WHEN progress <= 	59	 AND progress >=0 and distance <=	242	 and distance >=	60	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	60	 AND progress >=0 and distance <=	243	 and distance >=	61	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	61	 AND progress >=0 and distance <=	244	 and distance >=	62	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	62	 AND progress >=0 and distance <=	245	 and distance >=	63	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	63	 AND progress >=0 and distance <=	246	 and distance >=	64	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	64	 AND progress >=0 and distance <=	247	 and distance >=	65	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	65	 AND progress >=0 and distance <=	248	 and distance >=	66	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	66	 AND progress >=0 and distance <=	249	 and distance >=	67	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	67	 AND progress >=0 and distance <=	250	 and distance >=	68	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	68	 AND progress >=0 and distance <=	251	 and distance >=	69	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	69	 AND progress >=0 and distance <=	252	 and distance >=	70	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	70	 AND progress >=0 and distance <=	253	 and distance >=	71	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	71	 AND progress >=0 and distance <=	254	 and distance >=	72	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	72	 AND progress >=0 and distance <=	255	 and distance >=	73	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	73	 AND progress >=0 and distance <=	256	 and distance >=	74	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	74	 AND progress >=0 and distance <=	257	 and distance >=	75	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	75	 AND progress >=0 and distance <=	258	 and distance >=	76	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	76	 AND progress >=0 and distance <=	259	 and distance >=	77	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	77	 AND progress >=0 and distance <=	260	 and distance >=	78	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	78	 AND progress >=0 and distance <=	261	 and distance >=	79	 then pay::float END),1)
										
										
				*ROUND(SUM(CASE WHEN progress <= 	81	 AND progress >=0 and distance <=	263	 and distance >=	81	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	82	 AND progress >=0 and distance <=	264	 and distance >=	82	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	83	 AND progress >=0 and distance <=	265	 and distance >=	83	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	84	 AND progress >=0 and distance <=	266	 and distance >=	84	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	85	 AND progress >=0 and distance <=	267	 and distance >=	85	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	86	 AND progress >=0 and distance <=	268	 and distance >=	86	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	87	 AND progress >=0 and distance <=	269	 and distance >=	87	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	88	 AND progress >=0 and distance <=	270	 and distance >=	88	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	89	 AND progress >=0 and distance <=	271	 and distance >=	89	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN progress <= 	90	 AND progress >=0 and distance <=	272	 and distance >=	90	 then pay::float END),1)
									
										
				/ROUND(SUM(CASE WHEN progress <= 	79	 AND progress >=0 and distance <=	262	 and distance >=	80	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	80	 AND progress >=0 and distance <=	263	 and distance >=	81	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	81	 AND progress >=0 and distance <=	264	 and distance >=	82	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	82	 AND progress >=0 and distance <=	265	 and distance >=	83	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	83	 AND progress >=0 and distance <=	266	 and distance >=	84	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	84	 AND progress >=0 and distance <=	267	 and distance >=	85	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	85	 AND progress >=0 and distance <=	268	 and distance >=	86	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	86	 AND progress >=0 and distance <=	269	 and distance >=	87	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	87	 AND progress >=0 and distance <=	270	 and distance >=	88	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	88	 AND progress >=0 and distance <=	271	 and distance >=	89	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN progress <= 	89	 AND progress >=0 and distance <=	272	 and distance >=	90	 then pay::float END),1)						
				
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	7	   AND progress >=0  and distance <= 	190	 and distance >=	8	   then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	8	   AND progress >=0  and distance <=	191	 and distance >=	9	   then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	9	   AND progress >=0  and distance <=	192	 and distance >=	10	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	10	 AND progress >=0  and distance <=	193	 and distance >=	11	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	11	 AND progress >=0  and distance <=	194	 and distance >=	12	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	12	 AND progress >=0  and distance <=	195	 and distance >=	13	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	13	 AND progress >=0  and distance <=	196	 and distance >=	14	 then pay::float END),1)			
																	
																	
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	8	   AND progress >=0  and distance <= 	190	 and distance >=	8	   then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	9	   AND progress >=0  and distance <=	191	 and distance >=	9	   then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	10	 AND progress >=0  and distance <=	192	 and distance >=	10	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	11	 AND progress >=0  and distance <=	193	 and distance >=	11	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	12	 AND progress >=0  and distance <=	194	 and distance >=	12	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	13	 AND progress >=0  and distance <=	195	 and distance >=	13	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	14	 AND progress >=0  and distance <=	196	 and distance >=	14	 then pay::float END),1)			
																	
																	
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	14	 AND progress >=0  and distance <= 	197	 and distance >=	15	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	15	 AND progress >=0  and distance <= 	198	 and distance >=	16	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	16	 AND progress >=0  and distance <= 	199	 and distance >=	17	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	17	 AND progress >=0  and distance <= 	200	 and distance >=	18	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	18	 AND progress >=0  and distance <= 	201	 and distance >=	19	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	19	 AND progress >=0  and distance <= 	202	 and distance >=	20	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	20	 AND progress >=0  and distance <= 	203	 and distance >=	21	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	21	 AND progress >=0  and distance <= 	204	 and distance >=	22	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	22	 AND progress >=0  and distance <= 	205	 and distance >=	23	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	23	 AND progress >=0  and distance <= 	206	 and distance >=	24	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	24	 AND progress >=0  and distance <= 	207	 and distance >=	25	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	25	 AND progress >=0  and distance <= 	208	 and distance >=	26	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	26	 AND progress >=0  and distance <= 	209	 and distance >=	27	 then pay::float END),1)			
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	27	 AND progress >=0  and distance <= 	210	 and distance >=	28	 then pay::float END),1)			
																	
																	
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	15	 AND progress >=0  and distance <= 	197	 and distance >=	15	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	16	 AND progress >=0  and distance <= 	198	 and distance >=	16	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	17	 AND progress >=0  and distance <= 	199	 and distance >=	17	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	18	 AND progress >=0  and distance <= 	200	 and distance >=	18	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	19	 AND progress >=0  and distance <= 	201	 and distance >=	19	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	20	 AND progress >=0  and distance <= 	202	 and distance >=	20	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	21	 AND progress >=0  and distance <= 	203	 and distance >=	21	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	22	 AND progress >=0  and distance <= 	204	 and distance >=	22	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	23	 AND progress >=0  and distance <= 	205	 and distance >=	23	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	24	 AND progress >=0  and distance <= 	206	 and distance >=	24	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	25	 AND progress >=0  and distance <= 	207	 and distance >=	25	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	26	 AND progress >=0  and distance <= 	208	 and distance >=	26	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	27	 AND progress >=0  and distance <= 	209	 and distance >=	27	 then pay::float END),1)			
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	28	 AND progress >=0  and distance <= 	210	 and distance >=	28	 then pay::float END),1)
										
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	28	 AND progress >=0 and distance <= 211	 and distance >=	29	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	29	 AND progress >=0 and distance <= 212	 and distance >=	30	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	30	 AND progress >=0 and distance <= 213	 and distance >=	31	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	31	 AND progress >=0 and distance <=	214	 and distance >=	32	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	32	 AND progress >=0 and distance <=	215	 and distance >=	33	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	33	 AND progress >=0 and distance <=	216	 and distance >=	34	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	34	 AND progress >=0 and distance <=	217	 and distance >=	35	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	35	 AND progress >=0 and distance <=	218	 and distance >=	36	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	36	 AND progress >=0 and distance <=	219	 and distance >=	37	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	37	 AND progress >=0 and distance <=	220	 and distance >=	38	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	38	 AND progress >=0 and distance <=	221	 and distance >=	39	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	39	 AND progress >=0 and distance <=	222	 and distance >=	40	 then pay::float END),1)
										
										
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	29	 AND progress >=0 and distance <= 211	 and distance >=	29	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	30	 AND progress >=0 and distance <= 212	 and distance >=	30	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	31	 AND progress >=0 and distance <=	213	 and distance >=	31	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	32	 AND progress >=0 and distance <=	214	 and distance >=	32	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	33	 AND progress >=0 and distance <=	215	 and distance >=	33	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	34	 AND progress >=0 and distance <=	216	 and distance >=	34	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	35	 AND progress >=0 and distance <=	217	 and distance >=	35	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	36	 AND progress >=0 and distance <=	218	 and distance >=	36	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	37	 AND progress >=0 and distance <=	219	 and distance >=	37	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	38	 AND progress >=0 and distance <=	220	 and distance >=	38	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	39	 AND progress >=0 and distance <=	221	 and distance >=	39	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	40	 AND progress >=0 and distance <=	222	 and distance >=	40	 then pay::float END),1)
										
										
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	40	 AND progress >=0 and distance <=	223	 and distance >=	41	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	41	 AND progress >=0 and distance <=	224	 and distance >=	42	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	42	 AND progress >=0 and distance <=	225	 and distance >=	43	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	43	 AND progress >=0 and distance <=	226	 and distance >=	44	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	44	 AND progress >=0 and distance <=	227	 and distance >=	45	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	45	 AND progress >=0 and distance <=	228	 and distance >=	46	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	46	 AND progress >=0 and distance <=	229	 and distance >=	47	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	47	 AND progress >=0 and distance <=	230	 and distance >=	48	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	48	 AND progress >=0 and distance <=	231	 and distance >=	49	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	49	 AND progress >=0 and distance <=	232	 and distance >=	50	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	50	 AND progress >=0 and distance <=	233	 and distance >=	51	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	51	 AND progress >=0 and distance <=	234	 and distance >=	52	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	52	 AND progress >=0 and distance <=	235	 and distance >=	53	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	53	 AND progress >=0 and distance <=	236	 and distance >=	54	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	54	 AND progress >=0 and distance <=	237	 and distance >=	55	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	55	 AND progress >=0 and distance <=	238	 and distance >=	56	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	56	 AND progress >=0 and distance <=	239	 and distance >=	57	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	57	 AND progress >=0 and distance <=	240	 and distance >=	58	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	58	 AND progress >=0 and distance <=	241	 and distance >=	59	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	59	 AND progress >=0 and distance <=	242	 and distance >=	60	 then pay::float END),1)
										
										
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	41	 AND progress >=0 and distance <=	223	 and distance >=	41	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	42	 AND progress >=0 and distance <=	224	 and distance >=	42	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	43	 AND progress >=0 and distance <=	225	 and distance >=	43	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	44	 AND progress >=0 and distance <=	226	 and distance >=	44	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	45	 AND progress >=0 and distance <=	227	 and distance >=	45	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	46	 AND progress >=0 and distance <=	228	 and distance >=	46	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	47	 AND progress >=0 and distance <=	229	 and distance >=	47	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	48	 AND progress >=0 and distance <=	230	 and distance >=	48	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	49	 AND progress >=0 and distance <=	231	 and distance >=	49	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	50	 AND progress >=0 and distance <=	232	 and distance >=	50	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	51	 AND progress >=0 and distance <=	233	 and distance >=	51	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	52	 AND progress >=0 and distance <=	234	 and distance >=	52	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	53	 AND progress >=0 and distance <=	235	 and distance >=	53	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	54	 AND progress >=0 and distance <=	236	 and distance >=	54	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	55	 AND progress >=0 and distance <=	237	 and distance >=	55	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	56	 AND progress >=0 and distance <=	238	 and distance >=	56	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	57	 AND progress >=0 and distance <=	239	 and distance >=	57	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	58	 AND progress >=0 and distance <=	240	 and distance >=	58	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	59	 AND progress >=0 and distance <=	241	 and distance >=	59	 then pay::float END),1)
										
										
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	60	 AND progress >=0 and distance <=	243	 and distance >=	61	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	61	 AND progress >=0 and distance <=	244	 and distance >=	62	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	62	 AND progress >=0 and distance <=	245	 and distance >=	63	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	63	 AND progress >=0 and distance <=	246	 and distance >=	64	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	64	 AND progress >=0 and distance <=	247	 and distance >=	65	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	65	 AND progress >=0 and distance <=	248	 and distance >=	66	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	66	 AND progress >=0 and distance <=	249	 and distance >=	67	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	67	 AND progress >=0 and distance <=	250	 and distance >=	68	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	68	 AND progress >=0 and distance <=	251	 and distance >=	69	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	69	 AND progress >=0 and distance <=	252	 and distance >=	70	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	70	 AND progress >=0 and distance <=	253	 and distance >=	71	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	71	 AND progress >=0 and distance <=	254	 and distance >=	72	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	72	 AND progress >=0 and distance <=	255	 and distance >=	73	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	73	 AND progress >=0 and distance <=	256	 and distance >=	74	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	74	 AND progress >=0 and distance <=	257	 and distance >=	75	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	75	 AND progress >=0 and distance <=	258	 and distance >=	76	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	76	 AND progress >=0 and distance <=	259	 and distance >=	77	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	77	 AND progress >=0 and distance <=	260	 and distance >=	78	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	78	 AND progress >=0 and distance <=	261	 and distance >=	79	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	79	 AND progress >=0 and distance <=	262	 and distance >=	80	 then pay::float END),1)
										
										
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	60	 AND progress >=0 and distance <=	242	 and distance >=	60	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	61	 AND progress >=0 and distance <=	243	 and distance >=	61	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	62	 AND progress >=0 and distance <=	244	 and distance >=	62	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	63	 AND progress >=0 and distance <=	245	 and distance >=	63	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	64	 AND progress >=0 and distance <=	246	 and distance >=	64	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	65	 AND progress >=0 and distance <=	247	 and distance >=	65	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	66	 AND progress >=0 and distance <=	248	 and distance >=	66	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	67	 AND progress >=0 and distance <=	249	 and distance >=	67	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	68	 AND progress >=0 and distance <=	250	 and distance >=	68	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	69	 AND progress >=0 and distance <=	251	 and distance >=	69	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	70	 AND progress >=0 and distance <=	252	 and distance >=	70	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	71	 AND progress >=0 and distance <=	253	 and distance >=	71	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	72	 AND progress >=0 and distance <=	254	 and distance >=	72	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	73	 AND progress >=0 and distance <=	255	 and distance >=	73	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	74	 AND progress >=0 and distance <=	256	 and distance >=	74	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	75	 AND progress >=0 and distance <=	257	 and distance >=	75	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	76	 AND progress >=0 and distance <=	258	 and distance >=	76	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	77	 AND progress >=0 and distance <=	259	 and distance >=	77	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	78	 AND progress >=0 and distance <=	260	 and distance >=	78	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	79	 AND progress >=0 and distance <=	261	 and distance >=	79	 then pay::float END),1)
										
										
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	80	 AND progress >=0 and distance <=	263	 and distance >=	81	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	81	 AND progress >=0 and distance <=	264	 and distance >=	82	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	82	 AND progress >=0 and distance <=	265	 and distance >=	83	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	83	 AND progress >=0 and distance <=	266	 and distance >=	84	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	84	 AND progress >=0 and distance <=	267	 and distance >=	85	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	85	 AND progress >=0 and distance <=	268	 and distance >=	86	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	86	 AND progress >=0 and distance <=	269	 and distance >=	87	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	87	 AND progress >=0 and distance <=	270	 and distance >=	88	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	88	 AND progress >=0 and distance <=	271	 and distance >=	89	 then pay::float END),1)
				*ROUND(SUM(CASE WHEN grade=1 and progress <= 	89	 AND progress >=0 and distance <=	272	 and distance >=	90	 then pay::float END),1)
									
										
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	80	 AND progress >=0 and distance <=	262	 and distance >=	80	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	81	 AND progress >=0 and distance <=	263	 and distance >=	81	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	82	 AND progress >=0 and distance <=	264	 and distance >=	82	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	83	 AND progress >=0 and distance <=	265	 and distance >=	83	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	84	 AND progress >=0 and distance <=	266	 and distance >=	84	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	85	 AND progress >=0 and distance <=	267	 and distance >=	85	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	86	 AND progress >=0 and distance <=	268	 and distance >=	86	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	87	 AND progress >=0 and distance <=	269	 and distance >=	87	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	88	 AND progress >=0 and distance <=	270	 and distance >=	88	 then pay::float END),1)
				/ROUND(SUM(CASE WHEN grade=1 and progress <= 	89	 AND progress >=0 and distance <=	271	 and distance >=	89	 then pay::float END),1) as multiple,
				ROUND(SUM(CASE WHEN grade=1 and progress <= 	90	 AND progress >=0 and distance <=	272	 and distance >=	90	 then pay::float END),1) as divd
				FROM (						
					SELECT platform, payer_t.useridx, pay, progress, distance, nvl(grade, 0) as grade					
					FROM (					
						SELECT t1.useridx, t2.writedate, ROUND(facebookcredit::float/10::float, 2) AS pay, t1.createdate, platform, ceil(datediff(second, createdate, writedate)/86400::float) AS progress, ceil(datediff(second, createdate, dateadd(day, 1, '$current_date'::date))/86400::float) as distance	
						FROM (				
							SELECT useridx, createdate, platform			
							FROM t5_user			
							WHERE useridx > 20000 and createdate >= dateadd(day,(-273), '$current_date'::date)			
						) t1 JOIN t5_product_order t2 on t1.useridx = t2.useridx				
						WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 90				
						UNION ALL				
						SELECT t1.useridx, t2.writedate, ROUND(money, 2) AS pay, t1.createdate, platform, ceil(datediff(second, createdate, writedate)/86400::float) AS progress, ceil(datediff(second, createdate, dateadd(day, 1, '$current_date'::date))/86400::float) as distance				
						FROM (				
							SELECT useridx, createdate, platform			
							FROM t5_user			
							WHERE useridx > 20000 and createdate >= dateadd(day,(-273), '$current_date'::date)			
						) t1 JOIN t5_product_order_mobile t2 on t1.useridx = t2.useridx				
						WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 90		
					) AS payer_t left outer join  (					
						SELECT useridx, 1 AS grade				
						FROM (				
							SELECT t1.useridx, t2.writedate, ROUND(facebookcredit::float/10::float, 2) AS money, t1.createdate, platform			
							FROM (			
								SELECT useridx, createdate, platform		
								FROM t5_user		
								WHERE useridx > 20000 and createdate >= dateadd(day,(-273), '$current_date'::date)		
							) t1 JOIN t5_product_order t2 on t1.useridx = t2.useridx			
							WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 7			
							UNION ALL			
							SELECT t1.useridx, t2.writedate, ROUND(money, 2) AS pay, t1.createdate, platform			
							FROM (			
								SELECT useridx, createdate, platform		
								FROM t5_user		
								WHERE useridx > 20000 and createdate >= dateadd(day,(-273), '$current_date'::date)		
							) t1 JOIN t5_product_order_mobile t2 on t1.useridx = t2.useridx			
							WHERE status = 1 AND ceil(datediff(second, createdate, writedate)/86400::float) <= 7			
						) AS payer_t				
						GROUP BY useridx				
					) pay_t on payer_t.useridx = pay_t.useridx					
				) total
				WHERE platform != 0
				GROUP BY platform						
				ORDER BY platform";
		$non_payer_roi_factor_list = $db_redshift->gettotallist($sql);
		
		$insert_sql1 = "";
		$insert_sql2 = "";
		
		for($i=0; $i<sizeof($non_payer_roi_factor_list); $i++)
		{
			$platform = $non_payer_roi_factor_list[$i]["platform"];
			$multiple = ($non_payer_roi_factor_list[$i]["multiple"] == "")? 0 : $non_payer_roi_factor_list[$i]["multiple"];
			$divd = ($non_payer_roi_factor_list[$i]["divd"] == "" )? 0 : $non_payer_roi_factor_list[$i]["divd"];
				
			if($insert_sql1 == "")
			{
				$insert_sql1 = "INSERT INTO tbl_roi_factor VALUES('$today', 0, 1, 0, $platform, 0, $multiple, $divd)";
				$insert_sql2 = "INSERT INTO tbl_roi_factor VALUES('$today', 1, 1, 0, $platform, 0, $multiple, $divd)";
			}
			else
			{
				$insert_sql1 .= ",('$today', 0, 1, 0, $platform, 0, $multiple, $divd)";
				$insert_sql2 .= ",('$today', 1, 1, 0, $platform, 0, $multiple, $divd)";
			}
		}
		
		if($insert_sql1 != "")
		{
			$insert_sql1 .= "ON DUPLICATE KEY UPDATE multiple = VALUES(multiple), divd = VALUES(divd) ";
			$insert_sql2 .= "ON DUPLICATE KEY UPDATE multiple = VALUES(multiple), divd = VALUES(divd) ";
			
			$db_other->execute($insert_sql1);
			$db_other->execute($insert_sql2);
		}
		
		
		$sql = "SELECT * FROM `tbl_roi_factor` WHERE multiple = 0";
		$non_factor_list = $db_other->gettotallist($sql);
		
		for($i=0; $i<sizeof($non_factor_list ); $i++)
		{
			$today = $non_factor_list[$i]["today"];
			$type = $non_factor_list[$i]["type"];
			$category = $non_factor_list[$i]["category"];
			$is_payer = $non_factor_list[$i]["is_payer"];
			$platform = $non_factor_list[$i]["platform"];
			$grade = $non_factor_list[$i]["grade"];
			
			$sql ="SELECT multiple FROM tbl_roi_factor WHERE type = $type and today ='$today' and category= $category and is_payer =$is_payer and platform=".($platform-1)." and grade= $grade";
			$multiple = $db_other->getvalue($sql);
			
			$sql ="UPDATE tbl_roi_factor SET multiple = $multiple WHERE type = $type and today ='$today' and category= $category and is_payer =$is_payer and platform=$platform and grade= $grade";
			$db_other->execute($sql);
		}
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_other->end();
	$db_redshift->end();
?>