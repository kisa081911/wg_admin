<?
	include("../../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_mobile = new CDatabase_Mobile();
	
	$db_main->execute("SET wait_timeout=3600");
	$db_main2->execute("SET wait_timeout=3600");
	$db_mobile->execute("SET wait_timeout=3600");
	
	ini_set("memory_limit", "-1");		
	
	try
	{
		$sdate = "2019-01-02";
		$edate = "2019-03-07";
		
		while($sdate < $edate)
		{
			$today = $sdate;
			$yesterday = date('Y-m-d', strtotime($sdate.' - 1 day'));
			
			// retention
			$sql = "SELECT rtidx, platform, useridx, adflag, createdate, writedate
					FROM tbl_user_retention_mobile_log
					WHERE writedate >= '$yesterday 00:00:00' AND writedate < '$today 23:59:59'
					AND (adflag LIKE '%_int' OR adflag LIKE 'Facebook%' OR adflag LIKE 'amazon')";
			$retention_list = $db_main2->gettotallist($sql);
			
			$update_cnt = 0;
			
			for($i=0; $i<sizeof($retention_list); $i++)
			{
				$rtidx = $retention_list[$i]["rtidx"];
				$platform = $retention_list[$i]["platform"];
				$useridx = $retention_list[$i]["useridx"];
				$media_source = $retention_list[$i]["adflag"];
				$createdate = $retention_list[$i]["createdate"];
				$writedate = $retention_list[$i]["writedate"];
				$site_id = "";
				
				//appsflyer
				$sql = "SELECT appsflyerid, IF(os_type = 1, advid, deviceid) AS deviceid
						FROM tbl_user_mobile_appsflyer_new
						WHERE useridx = $useridx AND os_type = $platform
						ORDER BY writedate DESC
						LIMIT 1";
				$user_info = $db_main2->getarray($sql);
				
				$appsflyerid = $user_info["appsflyerid"];
				$device_id = $user_info["deviceid"];
							
				if($appsflyerid != "")
				{
					$sql = "SELECT site_id
							FROM tbl_appsflyer_install 
							WHERE appsflyer_device_id = '$appsflyerid' AND platform = $platform AND 0 <= DATEDIFF('$writedate', install_time) AND DATEDIFF('$writedate', install_time) <= 2
							ORDER BY logidx DESC
							LIMIT 1";
					$apps_info = $db_main2->getarray($sql);
					
					$site_id = $apps_info["site_id"];
					
					if($site_id != "")
					{
						$sql = "UPDATE tbl_user_retention_mobile_log SET site_id = '$site_id' WHERE rtidx = $rtidx";						
						$db_main2->execute($sql);
						
						$update_cnt++;
					}
				}
			}
			
			sleep(1);
			
			write_log("$yesterday ~ $today site_id update cnt:".$update_cnt);
			
			$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();
	$db_mobile->end();
?>