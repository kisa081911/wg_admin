<?
	include("../common/common_include.inc.php");
// 	include("../common/facebook_new.inc.php");
// 	include "./monitor/Sendmail.php";
	
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/vip_nopay90_request_scheduler") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/vip_nopay90_request_scheduler") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("vip_nopay90_request_scheduler Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$slave_main = new CDatabase_Slave_Main();
	$db_main2 = new CDatabase_Main2();
	$db_otherdb = new CDatabase_Other();
	
	$slave_main->execute("SET wait_timeout=72000");
	$db_main2->execute("SET wait_timeout=72000");
	$db_otherdb->execute("SET wait_timeout=72000");	
	
	$yesterday = date("Y-m-d",strtotime("-1 days"));
	$today = date("Y-m-d");
	
	function utf8_bytes($cp)
	{
		if ($cp > 0x10000){
			# 4 bytes
			return	chr(0xF0 | (($cp & 0x1C0000) >> 18)).
			chr(0x80 | (($cp & 0x3F000) >> 12)).
			chr(0x80 | (($cp & 0xFC0) >> 6)).
			chr(0x80 | ($cp & 0x3F));
		}else if ($cp > 0x800){
			# 3 bytes
			return	chr(0xE0 | (($cp & 0xF000) >> 12)).
			chr(0x80 | (($cp & 0xFC0) >> 6)).
			chr(0x80 | ($cp & 0x3F));
		}else if ($cp > 0x80){
			# 2 bytes
			return	chr(0xC0 | (($cp & 0x7C0) >> 6)).
			chr(0x80 | ($cp & 0x3F));
		}else{
			# 1 byte
			return chr($cp);
		}
	}
	
	try
	{
		write_log("100% Coupon Noti Send Test Start");
		

		
		
		// Type 1 : VIP($499이상 결제) 사용자 중 이탈기간이 7일 이하 && 결제이탈이 21일을 초과한 경우 VIP오퍼를 제공한다
		// isvip = 1 남긴 이유는, playstat_daily 데이터가 너무 많아서 결제자 필터링용으로 넣음. 
		$sql = "SELECT useridx, SUM(n) AS n, SUM(facebook) AS facebook, SUM(ios) AS ios, SUM(android) AS android, SUM(amazon) AS amazon
				FROM
				(
					SELECT useridx, COUNT(useridx) AS n, 1 AS facebook, 0 AS ios, 0 AS android, 0 AS amazon
					FROM tbl_user_playstat_daily
					WHERE today >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND vip_level > 0 
					GROUP BY useridx
				
					UNION ALL
				
					SELECT useridx, COUNT(useridx) AS n, 0 AS facebook, 1 AS ios, 0 AS android, 0 AS amazon
					FROM tbl_user_playstat_daily_ios
					WHERE today >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND vip_level > 0 
					GROUP BY useridx
				
					UNION ALL
				
					SELECT useridx, COUNT(useridx) AS n, 0 AS facebook, 0 AS ios, 1 AS android, 0 AS amazon
					FROM tbl_user_playstat_daily_android
					WHERE today >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND vip_level > 0 
					GROUP BY useridx
				
					UNION ALL
				
					SELECT useridx, COUNT(useridx) AS n, 0 AS facebook, 0 AS ios, 0 AS android, 1 AS amazon
					FROM tbl_user_playstat_daily_amazon
					WHERE today >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND vip_level > 0 
					GROUP BY useridx
				) a
				GROUP BY useridx
				HAVING n >= 2";
		$vip_userlist = $db_otherdb->gettotallist($sql);
		
		$insert_sql = "";
		$type1_noti_cnt = 0;
		$type1_insert_cnt = 0;
		
		for($i=0; $i<sizeof($vip_userlist); $i++)
		{
			$useridx = $vip_userlist[$i]["useridx"];
			$is_facebook = $vip_userlist[$i]["facebook"];
			$is_ios = $vip_userlist[$i]["ios"];
			$is_android = $vip_userlist[$i]["android"];
			$is_amazon = $vip_userlist[$i]["amazon"];
			
			// 최근 49일 이내 VIP 100% 쿠폰 사용내역이 있는 경우 발급 제한
			$sql = "SELECT COUNT(*) FROM tbl_nopay_vip_coupon WHERE useridx = $useridx AND is_buy = 1 AND buydate >= DATE_SUB(NOW(), INTERVAL 49 DAY)";
			$already_buy = $db_main2->getvalue($sql);
			
			if($already_buy == 0)
			{
				$sql = "SELECT
						(SELECT COUNT(*) FROM tbl_product_order WHERE useridx = $useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 2 DAY) LIMIT 1) +
						(SELECT COUNT(*) FROM tbl_product_order_mobile WHERE useridx = $useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 2 DAY) LIMIT 1) AS purchase_count";
				$purchase_count = $slave_main->getvalue($sql);
				
				if($purchase_count == 0)
				{
					$sql = "SELECT *
							FROM (
								SELECT t1.useridx, MAX(writedate) AS recent_buydate, vip_point
								FROM (
									SELECT useridx, MAX(writedate) AS writedate FROM tbl_product_order WHERE STATUS = 1 AND useridx = $useridx
									UNION ALL
									SELECT useridx, MAX(writedate) AS writedate FROM tbl_product_order_mobile WHERE STATUS = 1 AND useridx = $useridx
								) t1 JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx
							) t3
							WHERE recent_buydate <= DATE_SUB(NOW(), INTERVAL 21 DAY)";
					$payment_info = $slave_main->getarray($sql);
					
					$recent_buydate = $payment_info["recent_buydate"];
					$vip_point = $payment_info["vip_point"];
					
					if($vip_point >= 499)
					{
					    if($is_facebook == "1")
						{
							$sql = "SELECT userid FROM tbl_user WHERE useridx = $useridx";
							$facebookid = $slave_main->getvalue($sql);
								
							try
							{
								$facebook = new Facebook(array(
										'appId'  => FACEBOOK_APP_ID,
										'secret' => FACEBOOK_SECRET_KEY,
										'cookie' => true,
								));
									
								$session = $facebook->getUser();
									
								$template = "A 100% More Chips Coupon has been issued! Don't miss this great opportunity!";
									
 								$args = array('template' => "$template",
 										'href' => "?adflag=vip_coupon",
 										'ref' => "group_vip_coupon");
							
 								$info = $facebook->api("/$facebookid/notifications", "POST", $args);
									
								$type1_noti_cnt++;
									
							}
							catch (FacebookApiException $e)
							{
								if($e->getMessage() == "Unsupported operation" || $e->getMessage() == "(#200) Cannot send notifications to a user who has not installed the app")
								{
								}
							}
						}
				
						if($insert_sql == "")
						{
							$insert_sql = "INSERT INTO tbl_nopay_vip_coupon(useridx, TYPE, web, ios, android, amazon, is_buy, latest_buydate, buydate, writedate) ".
											"VALUES($useridx, 1, $is_facebook, $is_ios, $is_android, $is_amazon, 0, 0, '0000-00-00 00:00:00', NOW()) ";
						}
						else
							$insert_sql .= ", ($useridx, 1, $is_facebook, $is_ios, $is_android, $is_amazon, 0, 0, '0000-00-00 00:00:00', NOW())";
					
						$type1_insert_cnt++;
					
						if($i%500 == 0)
						{
							if($insert_sql != "")
							{
								$insert_sql .= "ON DUPLICATE KEY UPDATE TYPE = VALUES(TYPE), web = VALUES(web), ios = VALUES(ios), android = VALUES(android), amazon = VALUES(amazon), ".
												"is_buy = VALUES(is_buy), latest_buydate = VALUES(latest_buydate), buydate = VALUES(buydate), writedate = VALUES(writedate)";
								
 								$db_main2->execute($insert_sql);
								$insert_sql = "";
							}
						}
					}
				}
			}
		}
		
		if($insert_sql != "")
		{
			$insert_sql .= "ON DUPLICATE KEY UPDATE TYPE = VALUES(TYPE), web = VALUES(web), ios = VALUES(ios), android = VALUES(android), amazon = VALUES(amazon), ".
							"is_buy = VALUES(is_buy), latest_buydate = VALUES(latest_buydate), buydate = VALUES(buydate), writedate = VALUES(writedate)";
 			$db_main2->execute($insert_sql);
		}
		
		$sql = "INSERT INTO tbl_nopay_vip_coupon_stat VALUES('$today', 1, $type1_noti_cnt, $type1_insert_cnt)";
		$db_main2->execute($sql);
		
		write_log("100% Coupon Noti Send Count [Type1] : ".$type1_noti_cnt);
		write_log("100% Coupon Insert Count [Type1] : ".$type1_insert_cnt);
		
		// Type 2 : VIP($499이상 결제) 사용자 중 이탈기간이 7일 초과인 경우 VIP 오퍼를 제공한다
		$sql = "SELECT t1.useridx, userid
				FROM tbl_user t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx
				WHERE logindate <= DATE_SUB(NOW(), INTERVAL 7 DAY) AND vip_point >= 499";
		$vip_userlist = $slave_main->gettotallist($sql);
		
		$insert_sql = "";
		$type2_noti_cnt = 0;
		$type2_insert_cnt = 0;
		
		for($i=0; $i<sizeof($vip_userlist); $i++)
		{
			$useridx = $vip_userlist[$i]["useridx"];
			$facebookid = $vip_userlist[$i]["userid"];
				
			// 과거 49일 이내 VIP 100% 쿠폰 사용내역이 있는 경우 발급 제한
			$sql = "SELECT COUNT(*) FROM tbl_nopay_vip_coupon WHERE useridx = $useridx AND is_buy = 1 AND buydate >= DATE_SUB(NOW(), INTERVAL 49 DAY)";
			
			$already_buy = $db_main2->getvalue($sql);
			
			if($already_buy == 0)
			{
				$sql = "SELECT SUM(n) AS n, SUM(facebook) AS facebook, SUM(ios) AS ios, SUM(android) AS android, SUM(amazon) AS amazon
						FROM
						(
							SELECT COUNT(useridx) AS n, 1 AS facebook, 0 AS ios, 0 AS android, 0 AS amazon
							FROM tbl_user_playstat_daily
							WHERE DATE_SUB(NOW(), INTERVAL 7 DAY) >= today AND today >= DATE_SUB(NOW(), INTERVAL 31 DAY) AND vip_level > 0 
							AND useridx = $useridx
								
							UNION ALL
								
							SELECT COUNT(useridx) AS n, 0 AS facebook, 1 AS ios, 0 AS android, 0 AS amazon
							FROM tbl_user_playstat_daily_ios
							WHERE DATE_SUB(NOW(), INTERVAL 7 DAY) >= today AND today >= DATE_SUB(NOW(), INTERVAL 31 DAY) AND vip_level > 0 
							AND useridx = $useridx
								
							UNION ALL
								
							SELECT COUNT(useridx) AS n, 0 AS facebook, 0 AS ios, 1 AS android, 0 AS amazon
							FROM tbl_user_playstat_daily_android
							WHERE DATE_SUB(NOW(), INTERVAL 7 DAY) >= today AND today >= DATE_SUB(NOW(), INTERVAL 31 DAY) AND vip_level > 0 
							AND useridx = $useridx
								
							UNION ALL
								
							SELECT COUNT(useridx) AS n, 0 AS facebook, 0 AS ios, 0 AS android, 1 AS amazon
							FROM tbl_user_playstat_daily_amazon
							WHERE DATE_SUB(NOW(), INTERVAL 7 DAY) >= today AND today >= DATE_SUB(NOW(), INTERVAL 31 DAY) AND vip_level > 0 
							AND useridx = $useridx
							
						) t1";
				$is_platform_info = $db_otherdb->getarray($sql);
					
				$is_facebook = $is_platform_info["facebook"];
				$is_ios = $is_platform_info["ios"];
				$is_android = $is_platform_info["android"];
				$is_amazon = $is_platform_info["amazon"];
				
				if($is_facebook == "1")
				{
					try
					{
						$facebook = new Facebook(array(
								'appId'  => FACEBOOK_APP_ID,
								'secret' => FACEBOOK_SECRET_KEY,
								'cookie' => true,
						));
	
						$session = $facebook->getUser();
	
						$template = "A 100% More Chips Coupon has been issued! Don't miss this great opportunity!";
	
 						$args = array('template' => "$template",
 								'href' => "?adflag=vip_coupon",
 								'ref' => "group_vip_coupon");
	
 						$info = $facebook->api("/$facebookid/notifications", "POST", $args);
	
						$type2_noti_cnt++;
	
					}
					catch (FacebookApiException $e)
					{
						if($e->getMessage() == "Unsupported operation" || $e->getMessage() == "(#200) Cannot send notifications to a user who has not installed the app")
						{
						}
					}
				}
	
				if($insert_sql == "")
				{
					$insert_sql = "INSERT INTO tbl_nopay_vip_coupon(useridx, TYPE, web, ios, android, amazon, is_buy, latest_buydate, buydate, writedate) ".
									"VALUES($useridx, 2, $is_facebook, $is_ios, $is_android, $is_amazon,  0, 0, '0000-00-00 00:00:00', NOW()) ";
				}
				else
					$insert_sql .= ", ($useridx, 2, $is_facebook, $is_ios, $is_android, $is_amazon,  0, 0, '0000-00-00 00:00:00', NOW())";
				
				$type2_insert_cnt++;
	
				if($i%500 == 0)
				{
					if($insert_sql != "")
					{
						$insert_sql .= "ON DUPLICATE KEY UPDATE TYPE = VALUES(TYPE), web = VALUES(web), ios = VALUES(ios), android = VALUES(android), amazon = VALUES(amazon), ".
										"is_buy = VALUES(is_buy), latest_buydate = VALUES(latest_buydate), buydate = VALUES(buydate), writedate = VALUES(writedate)";
						
 						$db_main2->execute($insert_sql);
						$insert_sql = "";
					}
				}
			}
		}
		
		if($insert_sql != "")
		{
			$insert_sql .= "ON DUPLICATE KEY UPDATE TYPE = VALUES(TYPE), web = VALUES(web), ios = VALUES(ios), android = VALUES(android), amazon = VALUES(amazon), ".
							"is_buy = VALUES(is_buy), latest_buydate = VALUES(latest_buydate), buydate = VALUES(buydate), writedate = VALUES(writedate)";
			
 			$db_main2->execute($insert_sql);
		}
		
		
		$sql = "INSERT INTO tbl_nopay_vip_coupon_stat VALUES('$today', 2, $type2_noti_cnt, $type2_insert_cnt)";
		$db_main2->execute($sql);
		
		write_log("100% Coupon Noti Send Count [Type2] : ".$type2_noti_cnt);
		write_log("100% Coupon Insert Count [Type2] : ".$type2_insert_cnt);
		
// 		$config=array(
// 				'host'=>'10.0.0.211',
// 				'debug'=>1,
// 				'charset'=>'utf-8',
// 				'ctype'=>'text/html'
// 		);
		
// 		$sendmail = new Sendmail($config);
		
// 		$to = "minewat@afewgoodsoft.com";
// 		$from = "report@doubleucasino.com";
		
// 		$title = "[DUC] $today - 100% 쿠폰 발급 대상자 확인";
		
// 		$sql = "SELECT * FROM tbl_nopay_vip_coupon_stat WHERE today >= '$yesterday' ORDER BY today ASC, type ASC";
// 		$vip_coupon_stat = $db_main2->gettotallist($sql);
		
// 		$content .= "<table border='1' style='width:700px; border-collapse:collapse; border:1px gray solid;'>".
// 					"<thead>".
// 						"<th>today</th>".
// 						"<th>type</th>".
// 						"<th>noti_cnt</th>".
// 						"<th>coupon_cnt</th>".
// 					"</thead>".
// 					"<tbody>";
// 		for($i=0; $i<sizeof($vip_coupon_stat); $i++)
// 		{
// 			$today = $vip_coupon_stat[$i]["today"];
// 			$type = $vip_coupon_stat[$i]["type"];
// 			$noti_cnt = $vip_coupon_stat[$i]["noti_cnt"];
// 			$coupon_cnt = $vip_coupon_stat[$i]["coupon_cnt"];
				
// 			$content .= "<tr>".
// 							"<td style='text-align:center'>".$today."</td>".
// 							"<td style='text-align:center'>".$type."</td>".
// 							"<td style='text-align:center'>".$noti_cnt."</td>".
// 							"<td style='text-align:center'>".$coupon_cnt."</td>".
// 						"</tr>";
// 		}
		
// 		$content .= "</tbody>".
// 					"</table>";
		
// 		$sendmail->send_mail($to, $from, $title, $content);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$slave_main->end();
	$db_main2->end();
	$db_otherdb->end();
?>
