<?
    include("../common/common_include.inc.php");
    
	$db_main = new CDatabase_Main();
	$db_other = new CDatabase_Other();
	
	$db_main->execute("SET wait_timeout=36000");
	$db_other->execute("SET wait_timeout=36000");
	
	$str_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/month_join_purchase_stats") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/month_join_purchase_stats") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("month_join_purchase_stats Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	
	try
	{
		$sdate = date('Y-m', strtotime($sdate.' - 4 month'));
		$edate = date('Y-m', strtotime($sdate.' + 5 month'));
		
		$sdate = date('Y-m-d', strtotime($sdate."-01"));
		$edate = date('Y-m-d', strtotime($edate."-01"));

		while($sdate < $edate)
		{
			$temp_date = date('Y-m-d', strtotime($sdate.' + 1 month'));
			
			//Web
			$sql = 	"SELECT DATE_FORMAT(createdate, '%Y-%m') AS join_date, ROUND(SUM(facebookcredit), 1) AS total_credit, COUNT(*) AS pay_count, COUNT(DISTINCT t1.useridx) AS user_count ".
					"FROM ( ".
					"	SELECT useridx, facebookcredit/10 AS facebookcredit, writedate FROM tbl_product_order WHERE useridx>$str_useridx AND '$sdate 00:00:00' <= writedate AND writedate < '$temp_date 00:00:00' AND STATUS IN (1,3) ".
					"	UNION ALL ".
					"	SELECT useridx, money AS facebookcredit, writedate AS facebookcredit FROM tbl_product_order_mobile WHERE useridx>$str_useridx AND '$sdate 00:00:00' <= writedate AND writedate < '$temp_date 00:00:00' AND STATUS=1 ".
					") t1 LEFT JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx ".
					" WHERE t1.useridx > $str_useridx AND platform = 0 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m');";			
			$list = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($list); $i++)
			{
				$join_date = $list[$i]["join_date"];
				$pay_date = date('Y-m', strtotime($sdate));
				$total_credit = $list[$i]["total_credit"];
				$pay_count = $list[$i]["pay_count"];
				$user_count = $list[$i]["user_count"];
			
				$sql = "INSERT INTO tbl_user_month_purchases_statics(platform, join_month, purchase_month, total_credit, pay_count, user_count) VALUES(0, '$join_date', '$pay_date', '$total_credit', '$pay_count', '$user_count') ".
						"ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), pay_count = VALUES(pay_count), user_count = VALUES(user_count);";
				$db_other->execute($sql);
			}
			
			//Ios
			$sql = 	"SELECT DATE_FORMAT(createdate, '%Y-%m') AS join_date, ROUND(SUM(facebookcredit), 1) AS total_credit, COUNT(*) AS pay_count, COUNT(DISTINCT t1.useridx) AS user_count ".
					"FROM ( ".
					"	SELECT useridx, facebookcredit/10 AS facebookcredit, writedate FROM tbl_product_order WHERE useridx>$str_useridx AND '$sdate 00:00:00' <= writedate AND writedate < '$temp_date 00:00:00' AND STATUS IN (1, 3) ".
					"	UNION ALL ".
					"	SELECT useridx, money AS facebookcredit, writedate AS facebookcredit FROM tbl_product_order_mobile WHERE useridx>$str_useridx AND '$sdate 00:00:00' <= writedate AND writedate < '$temp_date 00:00:00' AND STATUS=1 ".
					") t1 LEFT JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx ".
					" WHERE t1.useridx > $str_useridx AND platform = 1 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m');";			
			$list = $db_main->gettotallist($sql);
				
			for($i=0; $i<sizeof($list); $i++)
			{
				$join_date = $list[$i]["join_date"];
				$pay_date = date('Y-m', strtotime($sdate));
				$total_credit = $list[$i]["total_credit"];
				$pay_count = $list[$i]["pay_count"];
				$user_count = $list[$i]["user_count"];
		
				$sql = "INSERT INTO tbl_user_month_purchases_statics(platform, join_month, purchase_month, total_credit, pay_count, user_count) VALUES(1, '$join_date', '$pay_date', '$total_credit', '$pay_count', '$user_count') ".
						"ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), pay_count = VALUES(pay_count), user_count = VALUES(user_count);";
				$db_other->execute($sql);
			}
			
			//Android
			$sql = 	"SELECT DATE_FORMAT(createdate, '%Y-%m') AS join_date, ROUND(SUM(facebookcredit), 1) AS total_credit, COUNT(*) AS pay_count, COUNT(DISTINCT t1.useridx) AS user_count ".
					"FROM ( ".
					"	SELECT useridx, facebookcredit/10 AS facebookcredit, writedate FROM tbl_product_order WHERE useridx>$str_useridx AND '$sdate 00:00:00' <= writedate AND writedate < '$temp_date 00:00:00' AND STATUS IN (1, 3) ".
					"	UNION ALL ".
					"	SELECT useridx, money AS facebookcredit, writedate AS facebookcredit FROM tbl_product_order_mobile WHERE useridx>$str_useridx AND '$sdate 00:00:00' <= writedate AND writedate < '$temp_date 00:00:00' AND STATUS=1 ".
					") t1 LEFT JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx ".
					" WHERE t1.useridx > $str_useridx AND platform = 2 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m');";			
			$list = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($list); $i++)
			{
				$join_date = $list[$i]["join_date"];
				$pay_date = date('Y-m', strtotime($sdate));
				$total_credit = $list[$i]["total_credit"];
				$pay_count = $list[$i]["pay_count"];
				$user_count = $list[$i]["user_count"];
			
				$sql = "INSERT INTO tbl_user_month_purchases_statics(platform, join_month, purchase_month, total_credit, pay_count, user_count) VALUES(2, '$join_date', '$pay_date', '$total_credit', '$pay_count', '$user_count') ".
						"ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), pay_count = VALUES(pay_count), user_count = VALUES(user_count);";
				$db_other->execute($sql);
			}
			
			//Amazon
			$sql = 	"SELECT DATE_FORMAT(createdate, '%Y-%m') AS join_date, ROUND(SUM(facebookcredit), 1) AS total_credit, COUNT(*) AS pay_count, COUNT(DISTINCT t1.useridx) AS user_count ".
					"FROM ( ".
					"	SELECT useridx, facebookcredit/10 AS facebookcredit, writedate FROM tbl_product_order WHERE useridx>$str_useridx AND '$sdate 00:00:00' <= writedate AND writedate < '$temp_date 00:00:00' AND STATUS IN (1, 3) ".
					"	UNION ALL ".
					"	SELECT useridx, money AS facebookcredit, writedate AS facebookcredit FROM tbl_product_order_mobile WHERE useridx>$str_useridx AND '$sdate 00:00:00' <= writedate AND writedate < '$temp_date 00:00:00' AND STATUS=1 ".
					") t1 LEFT JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx ".
					" WHERE t1.useridx > $str_useridx AND platform = 3 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m');";	;
			$list = $db_main->gettotallist($sql);
				
			for($i=0; $i<sizeof($list); $i++)
			{
				$join_date = $list[$i]["join_date"];
				$pay_date = date('Y-m', strtotime($sdate));
				$total_credit = $list[$i]["total_credit"];
				$pay_count = $list[$i]["pay_count"];
				$user_count = $list[$i]["user_count"];
		
				$sql = "INSERT INTO tbl_user_month_purchases_statics(platform, join_month, purchase_month, total_credit, pay_count, user_count) VALUES(3, '$join_date', '$pay_date', '$total_credit', '$pay_count', '$user_count') ".
						"ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), pay_count = VALUES(pay_count), user_count = VALUES(user_count);";
				$db_other->execute($sql);
			}
				
			$sdate = $temp_date;	
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		$sdate = date('Y-m-d', strtotime('2015-11-01'));
	
		$edate = date('Y-m')."-01";
		$edate = date('Y-m-d', strtotime($edate.' + 1 month'));
		
		$purchase_date = date('Y-m')."-01";
		$purchase_date = date('Y-m-d', strtotime($purchase_date.' -4 month'));
	
		while($sdate < $edate)
		{
			$temp_date = date('Y-m-d', strtotime($sdate.' + 1 month'));
				
			//Web
			$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m') AS pay_date, ROUND(SUM(facebookcredit), 1) AS total_credit, COUNT(*) AS pay_count, COUNT(DISTINCT useridx) AS user_count ".
					"FROM ( ".
					"	SELECT facebookcredit/10 AS facebookcredit, t1.useridx, writedate, createdate ".
					"	FROM ( ".
					"		SELECT useridx, createdate ".
					"		FROM tbl_user_ext ".
					"		WHERE '$sdate 00:00:00' <= createdate AND createdate < '$temp_date 00:00:00' AND platform = 0 ".
					"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
					"	WHERE STATUS IN (1, 3) AND '$purchase_date 00:00:00' <= writedate ".
					"	UNION ALL ".
					"	SELECT money AS facebookcredit, t1.useridx, writedate, createdate ".
					"	FROM ( ".
					"		SELECT useridx, createdate ".
					"		FROM tbl_user_ext ".
					"		WHERE '$sdate 00:00:00' <= createdate AND createdate < '$temp_date 00:00:00' AND platform = 0 ".
					"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
					"	WHERE STATUS = 1  AND '$purchase_date 00:00:00' <= writedate ".
					") t3 ".
					"WHERE useridx > $str_useridx ".
					"GROUP BY DATE_FORMAT(writedate, '%Y-%m')";
		
			$list = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($list); $i++)
			{
				$pay_date = $list[$i]["pay_date"];
				$join_date = date('Y-m', strtotime($sdate));
				$total_credit = $list[$i]["total_credit"];
				$pay_count = $list[$i]["pay_count"];
				$user_count = $list[$i]["user_count"];
						
				$sql = "INSERT INTO tbl_user_month_join_statics(platform,join_month, purchase_month, total_credit, pay_count, user_count) VALUES(0,'$join_date', '$pay_date', '$total_credit', '$pay_count', '$user_count') ".
						"ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), pay_count = VALUES(pay_count), user_count = VALUES(user_count);";
				$db_other->execute($sql);
			}
			
			//Ios
			$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m') AS pay_date, ROUND(SUM(facebookcredit), 1) AS total_credit, COUNT(*) AS pay_count, COUNT(DISTINCT useridx) AS user_count ".
					"FROM ( ".
					"	SELECT facebookcredit/10 AS facebookcredit, t1.useridx, writedate, createdate ".
					"	FROM ( ".
					"		SELECT useridx, createdate ".
					"		FROM tbl_user_ext ".
					"		WHERE '$sdate 00:00:00' <= createdate AND createdate < '$temp_date 00:00:00' AND platform = 1 ".
					"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
					"	WHERE STATUS IN (1, 3) AND '$purchase_date 00:00:00' <= writedate ".
					"	UNION ALL ".
					"	SELECT money AS facebookcredit, t1.useridx, writedate, createdate ".
					"	FROM ( ".
					"		SELECT useridx, createdate ".
					"		FROM tbl_user_ext ".
					"		WHERE '$sdate 00:00:00' <= createdate AND createdate < '$temp_date 00:00:00' AND platform = 1 ".
					"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
					"	WHERE STATUS = 1 AND '$purchase_date 00:00:00' <= writedate ".
					") t3 ".
					"WHERE useridx > $str_useridx ".
					"GROUP BY DATE_FORMAT(writedate, '%Y-%m')";
				
			$list = $db_main->gettotallist($sql);
				
			for($i=0; $i<sizeof($list); $i++)
			{
				$pay_date = $list[$i]["pay_date"];
				$join_date = date('Y-m', strtotime($sdate));
				$total_credit = $list[$i]["total_credit"];
				$pay_count = $list[$i]["pay_count"];
				$user_count = $list[$i]["user_count"];
			
				$sql = "INSERT INTO tbl_user_month_join_statics(platform,join_month, purchase_month, total_credit, pay_count, user_count) VALUES(1,'$join_date', '$pay_date', '$total_credit', '$pay_count', '$user_count') ".
						"ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), pay_count = VALUES(pay_count), user_count = VALUES(user_count);";			
				$db_other->execute($sql);
			}
						
			//Android
			$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m') AS pay_date, ROUND(SUM(facebookcredit), 1) AS total_credit, COUNT(*) AS pay_count, COUNT(DISTINCT useridx) AS user_count ".
					"FROM ( ".
					"	SELECT facebookcredit/10 AS facebookcredit, t1.useridx, writedate, createdate ".
					"	FROM ( ".
					"		SELECT useridx, createdate ".
					"		FROM tbl_user_ext ".
					"		WHERE '$sdate 00:00:00' <= createdate AND createdate < '$temp_date 00:00:00' AND platform = 2 ".
					"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
					"	WHERE STATUS IN (1, 3) AND '$purchase_date 00:00:00' <= writedate ".
					"	UNION ALL ".
					"	SELECT money AS facebookcredit, t1.useridx, writedate, createdate ".
					"	FROM ( ".
					"		SELECT useridx, createdate ".
					"		FROM tbl_user_ext ".
					"		WHERE '$sdate 00:00:00' <= createdate AND createdate < '$temp_date 00:00:00' AND platform = 2 ".
					"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
					"	WHERE STATUS = 1 AND '$purchase_date 00:00:00' <= writedate ".
					") t3 ".
					"WHERE useridx > $str_useridx ".
					"GROUP BY DATE_FORMAT(writedate, '%Y-%m')";
				
			$list = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($list); $i++)
			{
				$pay_date = $list[$i]["pay_date"];
				$join_date = date('Y-m', strtotime($sdate));
				$total_credit = $list[$i]["total_credit"];
				$pay_count = $list[$i]["pay_count"];
				$user_count = $list[$i]["user_count"];
			
				$sql = "INSERT INTO tbl_user_month_join_statics(platform,join_month, purchase_month, total_credit, pay_count, user_count) VALUES(2,'$join_date', '$pay_date', '$total_credit', '$pay_count', '$user_count') ".
						"ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), pay_count = VALUES(pay_count), user_count = VALUES(user_count);";			
				$db_other->execute($sql);
			}
			
			//Amazon
			$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m') AS pay_date, ROUND(SUM(facebookcredit), 1) AS total_credit, COUNT(*) AS pay_count, COUNT(DISTINCT useridx) AS user_count ".
					"FROM ( ".
					"	SELECT facebookcredit/10 AS facebookcredit, t1.useridx, writedate, createdate ".
					"	FROM ( ".
					"		SELECT useridx, createdate ".
					"		FROM tbl_user_ext ".
					"		WHERE '$sdate 00:00:00' <= createdate AND createdate < '$temp_date 00:00:00' AND platform = 3 ".
					"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
					"	WHERE STATUS IN (1, 3) AND '$purchase_date 00:00:00' <= writedate ".
					"	UNION ALL ".
					"	SELECT money AS facebookcredit, t1.useridx, writedate, createdate ".
					"	FROM ( ".
					"		SELECT useridx, createdate ".
					"		FROM tbl_user_ext ".
					"		WHERE '$sdate 00:00:00' <= createdate AND createdate < '$temp_date 00:00:00' AND platform = 3 ".
					"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
					"	WHERE STATUS = 1 AND '$purchase_date 00:00:00' <= writedate ".
					") t3 ".
					"WHERE useridx > $str_useridx ".
					"GROUP BY DATE_FORMAT(writedate, '%Y-%m')";
				
			$list = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($list); $i++)
			{
				$pay_date = $list[$i]["pay_date"];
				$join_date = date('Y-m', strtotime($sdate));
				$total_credit = $list[$i]["total_credit"];
				$pay_count = $list[$i]["pay_count"];
				$user_count = $list[$i]["user_count"];
			
				$sql = "INSERT INTO tbl_user_month_join_statics(platform,join_month, purchase_month, total_credit, pay_count, user_count) VALUES(3,'$join_date', '$pay_date', '$total_credit', '$pay_count', '$user_count') ".
						"ON DUPLICATE KEY UPDATE total_credit = VALUES(total_credit), pay_count = VALUES(pay_count), user_count = VALUES(user_count);";
				$db_other->execute($sql);
			}
	
			$sdate = $temp_date;
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	
	$db_main->end();
	$db_other->end();
?>