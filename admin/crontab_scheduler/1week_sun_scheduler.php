<?
	include("../common/common_include.inc.php");
	
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_friend = new CDatabase_Friend();
	$db_inbox = new CDatabase_Inbox();
	
	$db_main2->execute("SET wait_timeout=7200");
	$db_analysis->execute("SET wait_timeout=7200");
	$db_friend->execute("SET wait_timeout=7200");
	$db_inbox->execute("SET wait_timeout=7200");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$port = "";
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
		$port = ":8081";
	
	try
	{
		$sql = "INSERT INTO tbl_scheduler_log(today, type, status, writedate) VALUES('$today', 501, 0, now());";
		$db_analysis->execute($sql);
		$sql = "SELECT LAST_INSERT_ID();";
		$logidx = $db_analysis->getvalue($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/1week_sun_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/1week_sun_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("1week_sun_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}

	try
	{
		
		
		$sql = "SELECT  @ROWNUM := @ROWNUM+1 AS rank, t1.* 
                FROM
                (   
                 SELECT useridx, userid, nickname, weekly, honor_point ".
				"FROM tbl_user_weekly_point ".
				"WHERE weekly = YEARWEEK(NOW()) ".
				"ORDER BY honor_point DESC ".
				"LIMIT 1000 ) t1, (SELECT @ROWNUM := 0) R;";
		
		$week_list = $db_main2->gettotallist($sql);
		
		$insertcount = 0;
		$insert_sql = "";

		for($i=0; $i<sizeof($week_list); $i++)
		{
			$rank = $week_list[$i]["rank"];
			$useridx = $week_list[$i]["useridx"];
			$userid = $week_list[$i]["userid"];
			$nickname = encode_db_field($week_list[$i]["nickname"]);
			$weekly = $week_list[$i]["weekly"];
			$honor_point = $week_list[$i]["honor_point"];
			$prize = 0;
			
			if($rank == 1)
				$prize = 1000000;
			else if($rank == 2)
				$prize = 700000;
			else if($rank == 3)
				$prize = 500000;
			else if(4 <= $rank && $rank <= 50)
				$prize = 300000;
			else if(51 <= $rank && $rank <= 200)
				$prize = 100000;
			else if(201 <= $rank && $rank <= 500)
				$prize = 50000;
			else
				$prize = 30000;
			
			if($useridx > 10000)
			{
				if($insertcount == 0)
				{
					$insert_sql .= "INSERT INTO tbl_fame_weekly_log(weekly, rank, useridx, userid, nickname, honor_point, prize, status, writedate) ".
							"VALUES($weekly, $rank, $useridx, $userid, '$nickname', $honor_point, $prize, 0, NOW())";
					$insertcount++;
				}
				else if ($insertcount < 100)
				{
					$insert_sql .= ",($weekly, $rank, $useridx, $userid, '$nickname', $honor_point, $prize, 0, NOW())";
					$insertcount++;
				}
				else
				{
					$insert_sql .= ",($weekly, $rank, $useridx, $userid, '$nickname', $honor_point, $prize, 0, NOW()) ON DUPLICATE KEY UPDATE writedate=VALUES(writedate);";
					$insertcount = 0;
				}
			}
		}
		
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE writedate=VALUES(writedate);";
			$db_main2->execute($insert_sql);
		}
		
		if($i % 100 == 0 && $i > 0)
			sleep(1);
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		$sql = "UPDATE tbl_scheduler_log SET status = $issuccess, writedate = now() WHERE logidx = $logidx;";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
	$db_analysis->end();
	$db_friend->end();
	$db_inbox->end();
?>