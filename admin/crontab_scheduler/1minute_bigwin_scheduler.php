<?
	include("../common/common_include.inc.php");
	
	$db_main2 = new CDatabase_Main2();
		
	$db_main2->execute("SET wait_timeout=60");
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/1minute_bigwin_scheduler") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/1minute_bigwin_scheduler") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("1minute_bigwin_scheduler Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	try
	{
		$sql = "SELECT idx FROM tbl_bigwin_event WHERE startdate <= NOW() AND NOW() <= enddate;";
		$bigwin_event_idx = $db_main2->getvalue($sql);
		
		if($bigwin_event_idx > 0)
		{
			$sql = "SELECT amount_1-SUM(ROUND(IF(amount >= 20000000000, 10000000000, amount/2), 0)) AS amount2_cost  FROM tbl_bigwin_event t1 JOIN `tbl_bigwin_event_win_user` t2 ON t1.idx = t2.eventidx WHERE t1.idx = $bigwin_event_idx";
			$amount2_cost = $db_main2->getvalue($sql);
		
			if($amount2_cost < 0)
				$amount2_cost = 0;
		
			$sql = "UPDATE tbl_bigwin_event SET amount_2=$amount2_cost WHERE idx = $bigwin_event_idx";
			$db_main2->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();	
?>