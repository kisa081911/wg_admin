<?
	include("../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$db_livestats = new CDatabase_Livestats();
	
	$db_main->execute("SET wait_timeout=72000");;
	$db_livestats->execute("SET wait_timeout=72000");
	
	$today = date("Y-m-d");
	
	$port = "";
	
	$str_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/user_current_credit_update") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/user_current_credit_update") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("daily_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}

	try
	{
		$yesterday = date("Y-m-d",strtotime("-1 days"));
		
		for($i=0; $i<10; $i++)
		{
			$sql = "SELECT useridx FROM tbl_user_login_log_$i WHERE today = '$yesterday'";
			$login_user_list = $db_livestats->gettotallist($sql);
			
			for($j=0; $j<sizeof($login_user_list); $j++)
			{
				$useridx = $login_user_list[$j]["useridx"];
				
				$sql = "SELECT coin FROM tbl_user WHERE useridx = $useridx";
				$user_coin = $db_main->getvalue($sql);
				
				$sql = "UPDATE tbl_user_login_log_$i SET aftercoin=$user_coin WHERE today = '$yesterday' AND useridx = $useridx";
				$db_livestats->execute($sql);

				if($j%10000 == 0 && $j > 0)
					sleep(1);
			}
			
		}		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_livestats->end();
?>