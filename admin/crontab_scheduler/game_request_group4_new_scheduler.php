<?
	include("../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_inbox = new CDatabase_Inbox();
	
	$db_main->execute("SET wait_timeout=7200");
	$db_main2->execute("SET wait_timeout=7200");
	$db_inbox->execute("SET wait_timeout=7200");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	$str_useridx = 20000;	
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/game_request_group4_new_scheduler") !== false)
		{
			$count++;
		}
	}
	
	function utf8_bytes($cp)
	{
	    if ($cp > 0x10000){
	        # 4 bytes
	        return	chr(0xF0 | (($cp & 0x1C0000) >> 18)).
	        chr(0x80 | (($cp & 0x3F000) >> 12)).
	        chr(0x80 | (($cp & 0xFC0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else if ($cp > 0x800){
	        # 3 bytes
	        return	chr(0xE0 | (($cp & 0xF000) >> 12)).
	        chr(0x80 | (($cp & 0xFC0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else if ($cp > 0x80){
	        # 2 bytes
	        return	chr(0xC0 | (($cp & 0x7C0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else{
	        # 1 byte
	        return chr($cp);
	    }
	}
	
	if ($count > 1)
		exit();
	
	try
	{
		$sql = "SELECT useridx, userid, platform, bonus_coin, leftcount FROM `tbl_user_firstbuy_gift` WHERE useridx > $str_useridx AND leftcount > 0 AND sendmode = 1 ".
				"AND (senddate < DATE_SUB(NOW(), INTERVAL 1 DAY) OR senddate = '0000-00-00 00:00:00')";		
		$req_user = $db_main2->gettotallist($sql);
		
		for($i=0; $i<sizeof($req_user); $i++)
		{
			$useridx = $req_user[$i]["useridx"];
			$facebookid = $req_user[$i]["userid"];
			$platform = $req_user[$i]["platform"];
			$bonus_coin = $req_user[$i]["bonus_coin"];
			$leftcount = $req_user[$i]["leftcount"];
			
			try
			{
				$sql = "SELECT COUNT(*) FROM tbl_user_delete WHERE useridx = $useridx AND status = 1";
				$delete_user_check = $db_main->getvalue($sql);
				
				if($delete_user_check == 0)
				{				
					$sql = "INSERT INTO tbl_user_firstbuy_gift_log(useridx, bonus_coin, type, platform, senddate) VALUES($useridx, $bonus_coin, 1, $platform, NOW())";
					$db_main2->execute($sql);
					
					$sql = "SELECT LAST_INSERT_ID()";
					$notiidx = $db_main2->getvalue($sql);
					
					if($platform == 0)
					{
						$facebook = new Facebook(array(
								'appId'  => FACEBOOK_APP_ID,
								'secret' => FACEBOOK_SECRET_KEY,
								'cookie' => true,
						));
					
						$session = $facebook->getUser();
					
						$day1 = utf8_bytes(0x1F38A)."What a surprise! Extra Daily Bonus is now available! Check out your Gift Box!".utf8_bytes(0x1F381);
						$day2 = utf8_bytes(0x1F4EE)."Extra Daily Bonus has been sent to your Gift Box!".utf8_bytes(0x1F381)." Collect the Bonus and PLAY NOW!";
						$day3 = utf8_bytes(0x1F389)."No time to wait! Collect your Extra Daily Bonus and let it spin!".utf8_bytes(0x1F3B0);
						$daily_template = array($day1, $day2, $day3);
						
						$date_flag = rand(0, 2);
						
						$template = $daily_template[$date_flag];
						
						$args = array('template' => "$template",
										'href' => "?adflag=firstbuy_noti_click&notiidx=$notiidx",
										'ref' => "game_group4");
							
						$info = $facebook->api("/$facebookid/notifications", "POST", $args);						

						sleep(1);
					}
				}
				else
				{
					$sql = "UPDATE tbl_user_firstbuy_gift SET leftcount = 0 WHERE useridx = $useridx";
					$db_main2->execute($sql);
				}
			}
			catch (FacebookApiException $e)
			{
				if($e->getMessage() == "Unsupported operation" || $e->getMessage() == "(#200) Cannot send notifications to a user who has not installed the app")
				{
					write_log("Group 4 |".$e->getMessage());
				}
			}
		}		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();
	$db_inbox->end();	
?>