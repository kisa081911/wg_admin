<?
	include("../common/common_include.inc.php");

	$db_analysis = new CDatabase_Analysis();

	$db_analysis->execute("SET wait_timeout=72000");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");	
	
	try
	{
			$online_today = '2016-08-06';
			
			$sql = "SELECT gameslottype, 0 AS mode, SUM(totalcount) AS sum_totalcount ".
					"FROM ".
					"(	".
					"	SELECT gameslottype, objectidx, SUM(totalcount) AS totalcount ".
					"	FROM user_online_game_log ".
					"	WHERE objectidx < 1000000 AND writedate BETWEEN '$online_today 00:00:00' AND '$online_today 23:59:59' ".
					"	GROUP BY gameslottype, objectidx ".
					") t1 GROUP BY gameslottype";
			$normal_data_list = $db_analysis->gettotallist($sql);
		
			for ($i=0; $i<sizeof($normal_data_list); $i++)
			{
				$gameslottype = $normal_data_list[$i]["gameslottype"];
				$mode = $normal_data_list[$i]["mode"];
				$sum_totalcount = $normal_data_list[$i]["sum_totalcount"];
		
				$sql = "INSERT INTO user_online_game_log_daily(today, mode, gameslottype, totalcount) VALUES ('$online_today', $mode, $gameslottype, $sum_totalcount)  ON DUPLICATE KEY UPDATE totalcount=VALUES(totalcount);";
				$db_analysis->execute($sql);
			}
		
			$sql = "SELECT gameslottype, 1 AS mode, SUM(totalcount) AS sum_totalcount ".
					"FROM ".
					"(	".
					"	SELECT gameslottype, objectidx, SUM(totalcount) AS totalcount ".
					"	FROM user_online_game_log ".
					"	WHERE objectidx >= 1000000 AND writedate BETWEEN '$online_today 00:00:00' AND '$online_today 23:59:59' ".
					"	GROUP BY gameslottype, objectidx ".
					") t1 GROUP BY gameslottype";
			$highroller_data_list = $db_analysis->gettotallist($sql);
		
			for ($i=0; $i<sizeof($highroller_data_list); $i++)
			{
				$gameslottype = $highroller_data_list[$i]["gameslottype"];
				$mode = $highroller_data_list[$i]["mode"];
				$sum_totalcount = $highroller_data_list[$i]["sum_totalcount"];
					
				$sql = "INSERT INTO user_online_game_log_daily(today, mode, gameslottype, totalcount) VALUES ('$online_today', $mode, $gameslottype, $sum_totalcount)  ON DUPLICATE KEY UPDATE totalcount=VALUES(totalcount);";
				$db_analysis->execute($sql);
			}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	$db_analysis->end();
?>