<?
    include("../../../common/common_include.inc.php");

    ini_set("memory_limit", "-1");
    
    $db_main = new CDatabase_Main();

    for($i=50; $i<53; $i++)
    {
    	$s_interval = $i;
    	$e_interval = $i+1;
    	
    	$sql = "SELECT ROUND(SUM(rev), 2) AS rev ".
		      	"FROM ( ".
		      	"	SELECT SUM(facebookcredit)/10 AS rev ".
		      	"	FROM tbl_product_order a JOIN tbl_user_ext b ON a.useridx = b.useridx ".
		      	"	WHERE STATUS = 1 AND a.useridx > 20000 ".
		      	"		AND platform = 2 ".
		      	"		AND writedate >= DATE_ADD('2015-11-15', INTERVAL $s_interval*28 DAY) AND writedate < DATE_ADD('2015-11-15', INTERVAL $e_interval*28 DAY) ".
		      	"	UNION ALL ".
		      	"	SELECT SUM(money) AS rev ".
		      	"	FROM tbl_product_order_mobile a JOIN tbl_user_ext b ON a.useridx = b.useridx ".
		      	"	WHERE STATUS = 1 AND a.useridx > 20000 ".
		      	"		AND platform = 2 ".
		      	"		AND writedate >= DATE_ADD('2015-11-15', INTERVAL $s_interval*28 DAY) AND writedate < DATE_ADD('2015-11-15', INTERVAL $e_interval*28 DAY) ".
		      	") a";
    	$revenue = $db_main->getvalue($sql);
    	
    	write_log($i.": ".$revenue);
    }
    
    
    
    $db_main->end();
?>