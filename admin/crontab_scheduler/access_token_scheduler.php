<?
	include_once("../common/common_include.inc.php");

	$db_analysis = new CDatabase_Analysis();
	
	try
	{
		$sql = "SELECT accesstoken FROM page_accesstoken WHERE userid = 0";
		$access_token = $db_analysis->getvalue($sql);
		$app_id = FACEBOOK_APP_ID;
		$app_secret_key = FACEBOOK_SECRET_KEY;
	
		//새로운 access_token 발급
		$facebook_json = file_get_contents('https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id='.$app_id.'&client_secret='.$app_secret_key.'&fb_exchange_token='.$access_token);
		$facebook_objs = json_decode($facebook_json);
		$new_access_token = $facebook_objs->{"access_token"};
		write_log($new_access_token);
		//새로운 access_token 저장
		$sql = "UPDATE page_accesstoken SET accesstoken='$new_access_token ', writedate=NOW() WHERE userid=0";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_analysis->end();
?>