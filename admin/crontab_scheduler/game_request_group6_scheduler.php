<?
	include("../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_inbox = new CDatabase_Inbox();
	
	$db_main->execute("SET wait_timeout=7200");
	$db_inbox->execute("SET wait_timeout=7200");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	$str_useridx = 20000;	
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/game_request_group6_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
		exit();
	
	try
	{
		//비결제자 3일 리텐션(72시간 이내 접속자, 3백만 코인 이하, 10만 리워드)
		$sql = "SELECT t1.userid, t2.useridx, DATE_FORMAT(t1.logindate, '%H') AS login_hour_min ".
				"FROM tbl_user t1 JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx	". 
				"WHERE t1.useridx > $str_useridx AND daylogincount >= 8	". 
				"AND t1.coin <= 3000000	".
				"AND t1.logindate > DATE_SUB(NOW(), INTERVAL 72 HOUR)	". 
				"AND t1.logindate <  DATE_SUB(NOW(), INTERVAL 24 HOUR)	".
				"AND isguest = 0 ".
				"AND DATE_FORMAT(t1.logindate, '%H') = DATE_FORMAT(NOW(), '%H') ".
				"AND NOT EXISTS (SELECT * FROM tbl_product_order WHERE useridx = t1.useridx AND status = 1)	".
				"AND NOT EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx = t1.useridx AND status = 1)	".
				"AND NOT EXISTS (SELECT * FROM tbl_user_delete WHERE useridx = t1.useridx AND status = 1) ".
				"AND NOT EXISTS (SELECT * FROM tbl_notification_group_log WHERE useridx = t1.useridx AND group_no=6 AND senddate >= DATE_SUB(NOW(), INTERVAL 1 DAY)) ".
				"UNION ALL ".
				"SELECT userid, useridx, DATE_FORMAT(logindate, '%H') AS login_hour_min  FROM tbl_user WHERE useridx IN (10002,10071,10004) ";
		$req_user = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($req_user); $i++)
		{
			$useridx = $req_user[$i]["useridx"];
			$facebookid = $req_user[$i]["userid"];
			$user_login_hour_min = $req_user[$i]["login_hour_min"];
			$bonus_coin = 100000;
			
			if($user_login_hour_min == date('H') || $useridx == 10002 ||  $useridx == 10004)
			{
				try
				{	
					$sql = "INSERT INTO tbl_notification_group_log(useridx, platform, group_no, type, bonus_coin, senddate) VALUES($useridx, 0, 6, 1, $bonus_coin, NOW());";
					$db_main->execute($sql);
					
					$sql = "SELECT LAST_INSERT_ID()";
					$notiidx = $db_main->getvalue($sql);				
					
					$facebook = new Facebook(array(
							'appId'  => FACEBOOK_APP_ID,
							'secret' => FACEBOOK_SECRET_KEY,
							'cookie' => true,
					));
				
					$session = $facebook->getUser();
				
					$template = "Make a wish for jackpots! Collect lucky 100,000 Coins now.";
						
					$args = array('template' => "$template",
									'href' => "?adflag=gamenotifygroup6&notiidx=$notiidx",
									'ref' => "game_group6");
						
					$info = $facebook->api("/$facebookid/notifications", "POST", $args);
					
					$title = "Here is your 'Make A Wish for Jackpots' Coins.";				
					
					$sql = "INSERT INTO tbl_user_inbox_".($useridx%20)." (`useridx`, `sender_useridx`, `sender_facebookid`, `sender_name`, `category`, `coin`, `multiple`, `title`, `writedate`) ".
							"VALUES('$useridx','0','0','Jessie Moore','108','$bonus_coin','1','".encode_db_field($title)."',NOW());";				
					$db_inbox->execute($sql);
					
					$sql = "SELECT LAST_INSERT_ID()";
					$inbox_idx = $db_inbox->getvalue($sql);				
					
					$sql = "UPDATE tbl_notification_group_log SET inboxidx='$inbox_idx' WHERE useridx = '$useridx' AND logidx = '$notiidx' AND group_no = 6;";
					$db_main->execute($sql);
					
					sleep(1);
				}
				catch (FacebookApiException $e)
				{					
					if($e->getMessage() == "Unsupported operation" || $e->getMessage() == "(#200) Cannot send notifications to a user who has not installed the app")
					{
						//write_log($e->getMessage());
					}
				}
			}
		}		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_inbox->end();	
?>