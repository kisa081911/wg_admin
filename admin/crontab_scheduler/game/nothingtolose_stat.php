<?      
	include("../../common/common_include.inc.php");

	$db_main2 = new CDatabase_Main2();
	$db_slave_main2 = new CDatabase_Slave_Main2();

	$db_main2->execute("SET wait_timeout=7200");
	$db_slave_main2->execute("SET wait_timeout=7200");
	
	try
	{
    	    
	    try
	    {
	        $sql = "SELECT DATE_FORMAT(writedate,'%Y-%m-%d') AS today ". 
   	                " ,(SELECT COUNT(useridx) FROM tbl_cashback_user_info WHERE DATE_FORMAT(purchase_date,'%Y-%m-%d') = DATE_FORMAT(tbl_cashback_user_log.writedate,'%Y-%m-%d')) AS payuser_count ".
                    " ,(COUNT(useridx)+(SELECT COUNT(useridx) FROM tbl_cashback_user_info WHERE STATUS IN (1,2,3) AND DATE_FORMAT(writedate,'%Y-%m-%d') = DATE_FORMAT(tbl_cashback_user_log.writedate,'%Y-%m-%d'))) AS target_user ". 
                    " ,(COUNT(useridx)+(SELECT COUNT(useridx) FROM tbl_cashback_user_info WHERE STATUS IN (3) AND DATE_FORMAT(writedate,'%Y-%m-%d') = DATE_FORMAT(tbl_cashback_user_log.writedate,'%Y-%m-%d'))) AS end3hour_user ".
                    " ,(SUM(saveamount)+IFNULL((SELECT SUM(save_amount) FROM tbl_cashback_user_info WHERE STATUS = 3 AND DATE_FORMAT(writedate,'%Y-%m-%d') = DATE_FORMAT(tbl_cashback_user_log.writedate,'%Y-%m-%d')),0)) AS saveamount ".
                    " , SUM(IF(inboxidx!='',saveamount,0)) AS give_saveamount ".
                    " , SUM(IF(collect_date!='',1,0))  AS take_user_count ".
                    " FROM tbl_cashback_user_log  GROUP BY DATE_FORMAT(writedate,'%Y-%m-%d')";
	        $date = $db_slave_main2->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date); $i++)
	        {
	            $today = $date[$i]["today"];
	            $payuser_count = $date[$i]["payuser_count"];
	            $target_user = $date[$i]["target_user"];
	        	$end3hour_user = $date[$i]["end3hour_user"];
	        	$saveamount  = $date[$i]["saveamount"];
	        	$give_saveamount = $date[$i]["give_saveamount"];
	        	$take_user_count = $date[$i]["take_user_count"];
	            
	            $sql ="INSERT INTO tbl_nothingtolose_stat (today, target_user, payuser_count, end3hour_user, saveamount, give_saveamount, take_user_count) ".
	   	            " VALUES('$today', '$target_user', '$payuser_count', '$end3hour_user', '$saveamount', '$give_saveamount', '$take_user_count') ".
                      " ON DUPLICATE KEY UPDATE end3hour_user=VALUES(end3hour_user), saveamount=VALUES(saveamount), give_saveamount=VALUES(give_saveamount), take_user_count=VALUES(take_user_count);";
	            $db_main2->execute($sql);
	                
	        }
	    }
	    catch(Exception $e)
	    {
	        write_log($e->getMessage());
	    }
	    
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	$db_main2->end();
	$db_slave_main2->end();
?>
