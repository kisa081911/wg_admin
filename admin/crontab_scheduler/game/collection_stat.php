<?      
	include("../../common/common_include.inc.php");

	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();

	$db_main2->execute("SET wait_timeout=21600");
	$db_analysis->execute("SET wait_timeout=21600");
	
	try
	{
		$today = date("Y-m-d");
	    
	    try
	    {
			//컬렉션 - 테마
	        $sql = "SELECT event_idx
					,DATE_FORMAT(writedate,'%Y-%m-%d') as today
					,SUM(IF(theme_type = 1,1,0)) AS theme1_count
					,SUM(IF(theme_type = 2,1,0)) AS theme2_count
					,SUM(IF(theme_type = 3,1,0)) AS theme3_count
					,SUM(IF(theme_type = 100,1,0)) AS themeall_count 
					FROM `tbl_collection_event_theme_log` WHERE writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' GROUP BY event_idx; ";
	        $date = $db_main2->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date); $i++)
	        {
	            $today = $date[$i]["today"];
				$event_idx = $date[$i]["event_idx"];
				$theme1_count = $date[$i]["theme1_count"];
				$theme2_count = $date[$i]["theme2_count"];
				$theme3_count = $date[$i]["theme3_count"];
				$themeall_count = $date[$i]["themeall_count"];
				$reward_coin = $db_analysis->getvalue("SELECT ROUND(SUM(freeamount)/SUM(freecount))  FROM `tbl_user_freecoin_daily` WHERE today >= '2019-12-12' AND TYPE = 109");
	            
	            $sql ="INSERT INTO tbl_collection_stat_daily (today, eventidx, theme1_count, theme2_count, theme3_count, themeall_count, reward_coin) ".
	   	            " VALUES( '$today', '$event_idx', '$theme1_count', '$theme2_count','$theme3_count', '$themeall_count', '$reward_coin') ".
                        " ON DUPLICATE KEY UPDATE theme1_count=VALUES(theme1_count), theme2_count=VALUES(theme2_count),theme3_count=VALUES(theme3_count), themeall_count=VALUES(themeall_count), reward_coin=VALUES(reward_coin);";
	            $db_analysis->execute($sql);
	                
			}

			//컬렉션 - 테마 리워드
	        $sql = "SELECT event_idx, theme_type, DATE_FORMAT(writedate,'%Y-%m-%d') AS today, MAX(totalmoney_in) AS max_amount, ROUND(AVG(totalmoney_in),2) AS avg_amount 
					FROM `tbl_collection_event_theme_log`
					WHERE writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' GROUP BY  event_idx, theme_type  ";
	        $date = $db_main2->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date); $i++)
	        {
	            $today = $date[$i]["today"];
				$event_idx = $date[$i]["event_idx"];
				$theme_type = $date[$i]["theme_type"];
				$max_amount = $date[$i]["max_amount"];
				$avg_amount = $date[$i]["avg_amount"];
				
				if($theme_type == 100)
					$theme_type = "all";
					
				$sql = 	"INSERT INTO tbl_collection_stat_daily (today, eventidx,  theme".$theme_type."_max_amount, theme".$theme_type."_avg_amount) ".
					   	" VALUES( '$today', '$event_idx', '$max_amount', '$avg_amount') ".
						 " ON DUPLICATE KEY UPDATE theme".$theme_type."_max_amount = VALUES(theme".$theme_type."_max_amount),theme".$theme_type."_avg_amount = VALUES(theme".$theme_type."_avg_amount)";

		 $db_analysis->execute($sql);
	                
			}
			
		    //컬렉션 - 아이템 획득
			$sql = "SELECT event_idx
					, DATE_FORMAT(writedate,'%Y-%m-%d') as today
					,SUM(star_token1_count) AS star_token1_count
					,SUM(star_token2_count) AS star_token2_count
					,SUM(star_token3_count) AS star_token3_count
		 			FROM `tbl_collection_useritem_info` WHERE writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' GROUP BY event_idx;";
	        $date = $db_main2->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date); $i++)
	        {
	            $today = $date[$i]["today"];
				$event_idx = $date[$i]["event_idx"];
				$star_token1_count = $date[$i]["star_token1_count"];
				$star_token2_count = $date[$i]["star_token2_count"];
				$star_token3_count= $date[$i]["star_token3_count"];

	            $sql ="INSERT INTO tbl_collection_stat_daily (today, eventidx, star_token1_count, star_token2_count, star_token3_count) ".
	   	            " VALUES( '$today', '$event_idx', '$star_token1_count', '$star_token2_count', '$star_token3_count') ".
                        " ON DUPLICATE KEY UPDATE star_token1_count=VALUES(star_token1_count), star_token2_count=VALUES(star_token2_count),star_token3_count=VALUES(star_token3_count)";
	            $db_analysis->execute($sql);
	                
			}

			//미션
			$sql = "SELECT event_idx, DATE_FORMAT(writedate,'%Y-%m-%d') as today, current_stage_idx as current_stage_idx, COUNT(DISTINCT useridx) as count
					FROM `tbl_collection_event_theme_step_log` WHERE writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' GROUP BY event_idx,current_stage_idx;";
	        $date = $db_main2->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date); $i++)
	        {
	            $today = $date[$i]["today"];
				$event_idx = $date[$i]["event_idx"];
				$current_stage_idx = $date[$i]["current_stage_idx"];
				$count = $date[$i]["count"];
				
		    if($current_stage_idx == 0)
			$current_stage_idx = 5;
			
	            $sql ="INSERT INTO tbl_collection_stat_daily (today, eventidx, step".$current_stage_idx.") ".
	   	            " VALUES( '$today', '$event_idx', '$count') ".
                        " ON DUPLICATE KEY UPDATE step".$current_stage_idx."=VALUES(step".$current_stage_idx.")";
	            $db_analysis->execute($sql);
	                
			}
			
			//미션 스탭 리워드
			$sql = "SELECT event_idx, current_stage_idx, DATE_FORMAT(writedate,'%Y-%m-%d') AS today, MAX(reward_amount) AS max_amount, ROUND(AVG(reward_amount),2) AS avg_amount 
					FROM `tbl_collection_event_theme_step_log`  WHERE writedate >= '$today 00:00:00' AND writedate <= '$today 23:59:59' AND current_stage_idx != 0 GROUP BY event_idx, current_stage_idx";
	        $date = $db_main2->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date); $i++)
	        {
	            $today = $date[$i]["today"];
				$event_idx = $date[$i]["event_idx"];
				$current_stage_idx = $date[$i]["current_stage_idx"];
				$max_amount = $date[$i]["max_amount"];
				$avg_amount = $date[$i]["avg_amount"];

	            $sql ="INSERT INTO tbl_collection_stat_daily (today, eventidx, step".$current_stage_idx."_max_amount,step".$current_stage_idx."_avg_amount) ".
	   	            " VALUES( '$today', '$event_idx', '$max_amount', '$avg_amount') ".
                        " ON DUPLICATE KEY UPDATE step".$current_stage_idx."_max_amount = VALUES(step".$current_stage_idx."_max_amount),step".$current_stage_idx."_avg_amount = VALUES(step".$current_stage_idx."_avg_amount)";
	            $db_analysis->execute($sql);
	                
	        }
	    }
	    catch(Exception $e)
	    {
	        write_log($e->getMessage());
	    }
	    
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	$db_main2->end();
	$db_analysis->end();
?>
