<?      
	include("../../common/common_include.inc.php");

	$db_main2 = new CDatabase_Main2();
	$db_slave_main2 = new CDatabase_Slave_Main2();

	$db_main2->execute("SET wait_timeout=7200");
	$db_slave_main2->execute("SET wait_timeout=7200");
	
	try
	{
    	    
	    try
	    {
	        $sql = "SELECT
                     DATE_FORMAT(write_date,'%Y-%m-%d') AS today
                    ,event_idx
                    ,reward_group
                    , os_type 
                    , SUM(IF(quest1 = 3, 1,0)) AS quest1_count
                    , SUM(IF(quest2 = 400, 1,0))  AS quest2_count
                    , SUM(IF(quest1 = 3 AND quest2 = 400, 1,0)) AS questall_count
                    , SUM(amount) AS reward_amount
                    , SUM(money_in) AS money_in
                    , SUM(IF(amount > 0, 1,0)) AS amount_count
                    FROM (
                    	SELECT d1.*, IFNULL(t2.amount,0) AS amount FROM (
                    		SELECT
                    		t1.event_idx 
                    		,t1.useridx
                    		,write_date
                    		,quest_idx
                    		,quest1
                    		,quest2
                    		,IFNULL((SELECT collect_date FROM tbl_user_event_quest_collect_log WHERE useridx = t1.useridx AND quest_idx = t1.quest_idx AND event_idx = t1.event_idx ),0) AS collect_date
                    		,IFNULL((SELECT reward_group FROM tbl_user_event_quest_collect_log WHERE useridx = t1.useridx AND quest_idx = t1.quest_idx AND event_idx = t1.event_idx ),0) AS reward_group
                    		,IFNULL((SELECT os_type FROM tbl_user_event_quest_collect_log WHERE useridx = t1.useridx AND quest_idx = t1.quest_idx  AND event_idx = t1.event_idx ORDER BY write_date DESC LIMIT 1),0) AS os_type	,money_in
                    		FROM (	
                    			SELECT * FROM `tbl_user_event_quest_info_0` WHERE event_idx = 3
                    			UNION ALL
                    			SELECT * FROM `tbl_user_event_quest_info_1` WHERE event_idx = 3
                    			UNION ALL
                    			SELECT * FROM `tbl_user_event_quest_info_2` WHERE event_idx = 3
                    			UNION ALL
                    			SELECT * FROM `tbl_user_event_quest_info_3` WHERE event_idx = 3
                    			UNION ALL
                    			SELECT * FROM `tbl_user_event_quest_info_4` WHERE event_idx = 3
                    			UNION ALL
                    			SELECT * FROM `tbl_user_event_quest_info_5` WHERE event_idx = 3
                    			UNION ALL
                    			SELECT * FROM `tbl_user_event_quest_info_6` WHERE event_idx = 3
                    			UNION ALL
                    			SELECT * FROM `tbl_user_event_quest_info_7` WHERE event_idx = 3
                    			UNION ALL
                    			SELECT * FROM `tbl_user_event_quest_info_8` WHERE event_idx = 3
                    			UNION ALL
                    			SELECT * FROM `tbl_user_event_quest_info_9` WHERE event_idx = 3
                    		)  t1 
                    	) d1
                    	LEFT JOIN 
                    	(
                    		SELECT * FROM `tbl_user_freecoin_log_0` WHERE TYPE = 137
                    		UNION ALL
                    		SELECT * FROM `tbl_user_freecoin_log_1` WHERE TYPE = 137
                    		UNION ALL
                    		SELECT * FROM `tbl_user_freecoin_log_2` WHERE TYPE = 137
                    		UNION ALL
                    		SELECT * FROM `tbl_user_freecoin_log_3` WHERE TYPE = 137
                    		UNION ALL
                    		SELECT * FROM `tbl_user_freecoin_log_4` WHERE TYPE = 137
                    		UNION ALL
                    		SELECT * FROM `tbl_user_freecoin_log_5` WHERE TYPE = 137
                    		UNION ALL
                    		SELECT * FROM `tbl_user_freecoin_log_6` WHERE TYPE = 137
                    		UNION ALL
                    		SELECT * FROM `tbl_user_freecoin_log_7` WHERE TYPE = 137
                    		UNION ALL
                    		SELECT * FROM `tbl_user_freecoin_log_8` WHERE TYPE = 137
                    		UNION ALL
                    		SELECT * FROM `tbl_user_freecoin_log_9` WHERE TYPE = 137
                    	) t2
                    	ON d1.useridx = t2.useridx
                    	AND DATE_FORMAT(t2.writedate,'%Y-%m-%d') = DATE_FORMAT(d1.collect_date,'%Y-%m-%d')
                    ) t3
                    GROUP BY DATE_FORMAT(write_date,'%Y-%m-%d'),reward_group,os_type";
	        $date = $db_slave_main2->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date); $i++)
	        {
	            $today = $date[$i]["today"];
	        	$event_idx = $date[$i]["event_idx"];
	            $reward_group = $date[$i]["reward_group"];
	            $os_type = $date[$i]["os_type"];
	            $quest1_count = $date[$i]["quest1_count"];
	            $quest2_count = $date[$i]["quest2_count"];
	            $questall_count = $date[$i]["questall_count"];
	            $reward_amount = $date[$i]["reward_amount"];
	            $money_in = $date[$i]["money_in"];
	            $amount_count = $date[$i]["amount_count"];
	            
	            $sql ="INSERT INTO tbl_user_event_quest_stat (event_idx, today, reward_group, os_type, quest1_count, quest2_count, questall_count, money_in, reward_count, reward_amount) ".
                        " VALUES('$event_idx', '$today', '$reward_group', '$os_type', '$quest1_count', '$quest2_count', '$questall_count', '$money_in',  '$amount_count','$reward_amount') ".
                        " ON DUPLICATE KEY UPDATE quest1_count=VALUES(quest1_count), quest2_count=VALUES(quest2_count), questall_count=VALUES(questall_count), money_in=VALUES(money_in), reward_count=VALUES(reward_count), reward_amount=VALUES(reward_amount);";
	            $db_main2->execute($sql);
	                
	        }
	    }
	    catch(Exception $e)
	    {
	        write_log($e->getMessage());
	    }
	    
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	$db_main2->end();
	$db_slave_main2->end();
?>
