<?
	include("../../common/common_include.inc.php");

	$slave_main = new CDatabase_Slave_Main();
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();

	$slave_main->execute("SET wait_timeout=7200");
	$db_main2->execute("SET wait_timeout=7200");
	$db_other->execute("SET wait_timeout=7200");
	
	//$sql = "SELECT DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(), INTERVAL 1 DAY), INTERVAL WEEKDAY(DATE_SUB(NOW(), INTERVAL 1 DAY)) DAY), '%Y-%m-%d 00:00:00') AS week_start, //DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d 23:59:59') AS week_end";	
	//$date_info = $slave_main->getarray($sql);
	
	$week_start = '2016-12-26';
	$week_end = '2017-01-01';

	// 일별 슬롯 전체 
	// Web
	$sql = "SELECT ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR)) ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)) AS today, ".
			"MIN(writedate) AS s_date, MAX(writedate) AS e_date, ".
			"SUM(moneyin) AS money_in, SUM(moneyout) AS money_out, SUM(treatamount) AS treatamount, SUM(playcount) AS playcount ".
			"FROM tbl_game_cash_stats_daily2 ".
			"WHERE '$week_start' <= writedate AND writedate <= '$week_end' AND MODE NOT IN (9, 26) ".
			"GROUP BY ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR))	".
			"ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)) ".
			"ORDER BY today ASC";

	$slot_stat_list = $db_main2->gettotallist($sql);
	
	for($i=0; $i<sizeof($slot_stat_list); $i++)
	{
		$today = $slot_stat_list[$i]["today"];
		$s_date = $slot_stat_list[$i]["s_date"];
		$e_date = $slot_stat_list[$i]["e_date"];
		$money_in = $slot_stat_list[$i]["money_in"];
		$money_out = $slot_stat_list[$i]["money_out"];
		$treatamount = $slot_stat_list[$i]["treatamount"];
		$playcount = $slot_stat_list[$i]["playcount"];
		
		$sql = "INSERT INTO tbl_slot_stat_week(today, os_type, s_date, e_date, money_in, money_out, treatamount, playcount) VALUES($today, 0,'$s_date', '$e_date', $money_in, $money_out, $treatamount, $playcount) ".
				"ON DUPLICATE KEY UPDATE s_date=VALUES(s_date), e_date=VALUES(e_date), money_in=VALUES(money_in), money_out=VALUES(money_out), treatamount=VALUES(treatamount), playcount=VALUES(playcount);";
		$db_other->execute($sql);
	}
	
	// ios
	$sql = "SELECT ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR)) ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)) AS today, ".
			"MIN(writedate) AS s_date, MAX(writedate) AS e_date, ".
			"SUM(moneyin) AS money_in, SUM(moneyout) AS money_out, SUM(treatamount) AS treatamount, SUM(playcount) AS playcount ".
			"FROM tbl_game_cash_stats_ios_daily2 ".
			"WHERE '$week_start' <= writedate AND writedate <= '$week_end' AND MODE NOT IN (9, 26) ".
			"GROUP BY ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR))	".
			"ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)) ".
			"ORDER BY today ASC";
	
	$slot_stat_list = $db_main2->gettotallist($sql);
	
	for($i=0; $i<sizeof($slot_stat_list); $i++)
	{
		$today = $slot_stat_list[$i]["today"];
		$s_date = $slot_stat_list[$i]["s_date"];
		$e_date = $slot_stat_list[$i]["e_date"];
		$money_in = $slot_stat_list[$i]["money_in"];
		$money_out = $slot_stat_list[$i]["money_out"];
		$treatamount = $slot_stat_list[$i]["treatamount"];
		$playcount = $slot_stat_list[$i]["playcount"];

		$sql = "INSERT INTO tbl_slot_stat_week(today, os_type, s_date, e_date, money_in, money_out, treatamount, playcount) VALUES($today, 1,'$s_date', '$e_date', $money_in, $money_out, $treatamount, $playcount) ".
		"ON DUPLICATE KEY UPDATE s_date=VALUES(s_date), e_date=VALUES(e_date), money_in=VALUES(money_in), money_out=VALUES(money_out), treatamount=VALUES(treatamount), playcount=VALUES(playcount);";
		$db_other->execute($sql);
	}
	
	// android
	$sql = "SELECT ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR)) ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)) AS today, ".
			"MIN(writedate) AS s_date, MAX(writedate) AS e_date, ".
			"SUM(moneyin) AS money_in, SUM(moneyout) AS money_out, SUM(treatamount) AS treatamount, SUM(playcount) AS playcount ".
			"FROM tbl_game_cash_stats_android_daily2 ".
			"WHERE '$week_start' <= writedate AND writedate <= '$week_end' AND MODE NOT IN (9, 26) ".
			"GROUP BY ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR))	".
			"ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)) ".
			"ORDER BY today ASC";
	
	$slot_stat_list = $db_main2->gettotallist($sql);
	
	for($i=0; $i<sizeof($slot_stat_list); $i++)
	{
		$today = $slot_stat_list[$i]["today"];
		$s_date = $slot_stat_list[$i]["s_date"];
		$e_date = $slot_stat_list[$i]["e_date"];
		$money_in = $slot_stat_list[$i]["money_in"];
		$money_out = $slot_stat_list[$i]["money_out"];
		$treatamount = $slot_stat_list[$i]["treatamount"];
		$playcount = $slot_stat_list[$i]["playcount"];
	
		$sql = "INSERT INTO tbl_slot_stat_week(today, os_type, s_date, e_date, money_in, money_out, treatamount, playcount) VALUES($today, 2,'$s_date', '$e_date', $money_in, $money_out, $treatamount, $playcount) ".
				"ON DUPLICATE KEY UPDATE s_date=VALUES(s_date), e_date=VALUES(e_date), money_in=VALUES(money_in), money_out=VALUES(money_out), treatamount=VALUES(treatamount), playcount=VALUES(playcount);";
		$db_other->execute($sql);
	}
	
	// amazon
	$sql = "SELECT ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR)) ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)) AS today, ".
			"MIN(writedate) AS s_date, MAX(writedate) AS e_date, ".
			"SUM(moneyin) AS money_in, SUM(moneyout) AS money_out, SUM(treatamount) AS treatamount, SUM(playcount) AS playcount ".
			"FROM tbl_game_cash_stats_amazon_daily2 ".
			"WHERE '$week_start' <= writedate AND writedate <= '$week_end' AND MODE NOT IN (9, 26) ".
			"GROUP BY ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR))	".
			"ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)) ".
			"ORDER BY today ASC";
	
	$slot_stat_list = $db_main2->gettotallist($sql);
	
	for($i=0; $i<sizeof($slot_stat_list); $i++)
	{
		$today = $slot_stat_list[$i]["today"];
		$s_date = $slot_stat_list[$i]["s_date"];
		$e_date = $slot_stat_list[$i]["e_date"];
		$money_in = $slot_stat_list[$i]["money_in"];
		$money_out = $slot_stat_list[$i]["money_out"];
		$treatamount = $slot_stat_list[$i]["treatamount"];
		$playcount = $slot_stat_list[$i]["playcount"];

		$sql = "INSERT INTO tbl_slot_stat_week(today, os_type, s_date, e_date, money_in, money_out, treatamount, playcount) VALUES($today, 3,'$s_date', '$e_date', $money_in, $money_out, $treatamount, $playcount) ".
				"ON DUPLICATE KEY UPDATE s_date=VALUES(s_date), e_date=VALUES(e_date), money_in=VALUES(money_in), money_out=VALUES(money_out), treatamount=VALUES(treatamount), playcount=VALUES(playcount);";
		$db_other->execute($sql);
	}
	
	//jackpot
	$sql = "SELECT ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR)) ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)) AS today, ".
			"SUM(amount + payout_amount) AS jackpot_amount, COUNT(jackpotidx) AS jackpot_count, devicetype ".
			"FROM `tbl_jackpot_log` ".
			"WHERE '$week_start' <= writedate AND writedate <= '$week_end' AND useridx > 20000 ".
			"GROUP BY ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR))	".
			"ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)), devicetype;";
	
	$jackpot_list = $slave_main->gettotallist($sql);
	
	for($i=0; $i<sizeof($jackpot_list); $i++)
	{
		$today = $jackpot_list[$i]["today"];
		$devicetype = $jackpot_list[$i]["devicetype"];
		$jackpot_amount = $jackpot_list[$i]["jackpot_amount"];
		$jackpot_count = $jackpot_list[$i]["jackpot_count"];
	
		$sql = "INSERT INTO tbl_slot_stat_week(today, os_type, jackpot, jackpotcount) VALUES($today, $devicetype, $jackpot_amount, $jackpot_count) ".
				"ON DUPLICATE KEY UPDATE jackpot=VALUES(jackpot), jackpotcount=VALUES(jackpotcount);";
		$db_other->execute($sql);
	}
	
	$sql = "SELECT ((CASE WHEN WEEKOFYEAR(purchasedate) >= 52 && MONTH(purchasedate) = 1 THEN YEAR(DATE_SUB(purchasedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(purchasedate) = 12 && WEEKOFYEAR(purchasedate) = 1 THEN YEAR(DATE_ADD(purchasedate, INTERVAL 1 YEAR)) ELSE YEAR(purchasedate) END) * 100 + WEEKOFYEAR(purchasedate)) AS today, ".
			"SUM(money) AS pay_amount, COUNT(logidx) AS pay_count, buy_platform ".
			"FROM `tbl_user_pay_pattern_log` ".
			"WHERE last_slotidx > 0 AND useridx > 20000 AND '$week_start' <= purchasedate AND purchasedate <= '$week_end' ".
			"GROUP BY ((CASE WHEN WEEKOFYEAR(purchasedate) >= 52 && MONTH(purchasedate) = 1 THEN YEAR(DATE_SUB(purchasedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(purchasedate) = 12 && WEEKOFYEAR(purchasedate) = 1 THEN YEAR(DATE_ADD(purchasedate, INTERVAL 1 YEAR))	".
			"ELSE YEAR(purchasedate) END) * 100 + WEEKOFYEAR(purchasedate)), buy_platform;";
	
	$pay_list = $db_other->gettotallist($sql);
	
	for($i=0; $i<sizeof($pay_list); $i++)
	{
		$today = $pay_list[$i]["today"];
		$platform = $pay_list[$i]["buy_platform"];
		$pay_amount = $pay_list[$i]["pay_amount"];
		$pay_count = $pay_list[$i]["pay_count"];
		
		$sql = "INSERT INTO tbl_slot_stat_week(today, os_type, pay_amount, pay_count) VALUES($today, $platform, $pay_amount, $pay_count) ".
				"ON DUPLICATE KEY UPDATE pay_amount=VALUES(pay_amount), pay_count=VALUES(pay_count);";
		$db_other->execute($sql);
	}

	// 일별 슬롯별
	//web
	$sql = "SELECT ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR)) ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)) AS today, ".
			" slottype, SUM(moneyin) AS money_in, SUM(moneyout) AS money_out, SUM(treatamount) AS treatamount, SUM(playcount) AS playcount ".
			"FROM tbl_game_cash_stats_daily2 ".
			"WHERE MODE NOT IN (9, 26) AND '$week_start' <= writedate AND writedate <= '$week_end' ".
			"GROUP BY ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR))	".
			"ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)), slottype ".
			"ORDER BY today ASC, slottype ASC";
	
	$slot_detail_stat_list = $db_main2->gettotallist($sql);
	
	for($i=0; $i<sizeof($slot_detail_stat_list); $i++)
	{
		$today = $slot_detail_stat_list[$i]["today"];
		$slottype = $slot_detail_stat_list[$i]["slottype"];
		$money_in = $slot_detail_stat_list[$i]["money_in"];
		$money_out = $slot_detail_stat_list[$i]["money_out"];
		$treatamount = $slot_detail_stat_list[$i]["treatamount"];
		$playcount = $slot_detail_stat_list[$i]["playcount"];
	
		$sql = "INSERT INTO tbl_slot_rank_week(today, os_type, slottype, money_in, money_out, treatamount, playcount) VALUES($today, 0, $slottype, $money_in, $money_out, $treatamount, $playcount) ".
				"ON DUPLICATE KEY UPDATE money_in=VALUES(money_in), money_out=VALUES(money_out), treatamount=VALUES(treatamount), playcount=VALUES(playcount);";
		$db_other->execute($sql);
	}
	
	// ios
	$sql = "SELECT ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR)) ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)) AS today, ".
			" slottype, SUM(moneyin) AS money_in, SUM(moneyout) AS money_out, SUM(treatamount) AS treatamount, SUM(playcount) AS playcount ".
			"FROM tbl_game_cash_stats_ios_daily2 ".
			"WHERE MODE NOT IN (9, 26) AND '$week_start' <= writedate AND writedate <= '$week_end' ".
			"GROUP BY ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR))	".
			"ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)), slottype ".
			"ORDER BY today ASC, slottype ASC";
	
	$slot_detail_stat_list = $db_main2->gettotallist($sql);
	
	for($i=0; $i<sizeof($slot_detail_stat_list); $i++)
	{
		$today = $slot_detail_stat_list[$i]["today"];
		$slottype = $slot_detail_stat_list[$i]["slottype"];
		$money_in = $slot_detail_stat_list[$i]["money_in"];
		$money_out = $slot_detail_stat_list[$i]["money_out"];
		$treatamount = $slot_detail_stat_list[$i]["treatamount"];
		$playcount = $slot_detail_stat_list[$i]["playcount"];

		$sql = "INSERT INTO tbl_slot_rank_week(today, os_type, slottype, money_in, money_out, treatamount, playcount) VALUES($today, 1, $slottype, $money_in, $money_out, $treatamount, $playcount) ".
				"ON DUPLICATE KEY UPDATE money_in=VALUES(money_in), money_out=VALUES(money_out), treatamount=VALUES(treatamount), playcount=VALUES(playcount);";
		$db_other->execute($sql);
	}
	
	// android
	$sql = "SELECT ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR)) ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)) AS today, ".
			" slottype, SUM(moneyin) AS money_in, SUM(moneyout) AS money_out, SUM(treatamount) AS treatamount, SUM(playcount) AS playcount ".
			"FROM tbl_game_cash_stats_android_daily2 ".
			"WHERE MODE NOT IN (9, 26) AND '$week_start' <= writedate AND writedate <= '$week_end' ".
			"GROUP BY ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR))	".
			"ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)), slottype ".
			"ORDER BY today ASC, slottype ASC";
	
	$slot_detail_stat_list = $db_main2->gettotallist($sql);
	
	for($i=0; $i<sizeof($slot_detail_stat_list); $i++)
	{
		$today = $slot_detail_stat_list[$i]["today"];
		$slottype = $slot_detail_stat_list[$i]["slottype"];
		$money_in = $slot_detail_stat_list[$i]["money_in"];
		$money_out = $slot_detail_stat_list[$i]["money_out"];
		$treatamount = $slot_detail_stat_list[$i]["treatamount"];
		$playcount = $slot_detail_stat_list[$i]["playcount"];

		$sql = "INSERT INTO tbl_slot_rank_week(today, os_type, slottype, money_in, money_out, treatamount, playcount) VALUES($today, 2, $slottype, $money_in, $money_out, $treatamount, $playcount) ".
				"ON DUPLICATE KEY UPDATE money_in=VALUES(money_in), money_out=VALUES(money_out), treatamount=VALUES(treatamount), playcount=VALUES(playcount);";
		$db_other->execute($sql);
	}
	
	// amazon
	$sql = "SELECT ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR)) ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)) AS today, ".
			" slottype, SUM(moneyin) AS money_in, SUM(moneyout) AS money_out, SUM(treatamount) AS treatamount, SUM(playcount) AS playcount ".
			"FROM tbl_game_cash_stats_amazon_daily2 ".
			"WHERE MODE NOT IN (9, 26) AND '$week_start' <= writedate AND writedate <= '$week_end' ".
			"GROUP BY ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR))	".
			"ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)), slottype ".
			"ORDER BY today ASC, slottype ASC";
	
	$slot_detail_stat_list = $db_main2->gettotallist($sql);
	
	for($i=0; $i<sizeof($slot_detail_stat_list); $i++)
	{
		$today = $slot_detail_stat_list[$i]["today"];
		$slottype = $slot_detail_stat_list[$i]["slottype"];
		$money_in = $slot_detail_stat_list[$i]["money_in"];
		$money_out = $slot_detail_stat_list[$i]["money_out"];
		$treatamount = $slot_detail_stat_list[$i]["treatamount"];
		$playcount = $slot_detail_stat_list[$i]["playcount"];
	
		$sql = "INSERT INTO tbl_slot_rank_week(today, os_type, slottype, money_in, money_out, treatamount, playcount) VALUES($today, 3, $slottype, $money_in, $money_out, $treatamount, $playcount) ".
				"ON DUPLICATE KEY UPDATE money_in=VALUES(money_in), money_out=VALUES(money_out), treatamount=VALUES(treatamount), playcount=VALUES(playcount);";
		$db_other->execute($sql);
	}
	
	//jackpot
	$sql = "SELECT ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR)) ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)) AS today, ". 
			"slottype, SUM(amount + payout_amount) AS jackpot_amount, COUNT(DISTINCT jackpothallidx) AS jackpot_count, devicetype ".
			"FROM `tbl_jackpot_log` ".
			"WHERE useridx > 20000 AND '$week_start' <= writedate AND writedate <= '$week_end' ".
			"GROUP BY ((CASE WHEN WEEKOFYEAR(writedate) >= 52 && MONTH(writedate) = 1 THEN YEAR(DATE_SUB(writedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(writedate) = 12 && WEEKOFYEAR(writedate) = 1 THEN YEAR(DATE_ADD(writedate, INTERVAL 1 YEAR))	".
			"ELSE YEAR(writedate) END) * 100 + WEEKOFYEAR(writedate)), slottype, devicetype";
	
	$jackpot_detail_list = $slave_main->gettotallist($sql);
	
	for($i=0; $i<sizeof($jackpot_detail_list); $i++)
	{
		$today = $jackpot_detail_list[$i]["today"];
		$slottype = $jackpot_detail_list[$i]["slottype"];
		$platform = $jackpot_detail_list[$i]["devicetype"];
		$jackpot_amount = $jackpot_detail_list[$i]["jackpot_amount"];
		$jackpot_count = $jackpot_detail_list[$i]["jackpot_count"];
	
		$sql = "INSERT INTO tbl_slot_rank_week(today, os_type, slottype, jackpot, jackpotcount) VALUES($today, $platform, $slottype, $jackpot_amount, $jackpot_count) ".
				"ON DUPLICATE KEY UPDATE jackpot=VALUES(jackpot), jackpotcount=VALUES(jackpotcount);";
		$db_other->execute($sql);
	}
	
	$sql = "SELECT ((CASE WHEN WEEKOFYEAR(purchasedate) >= 52 && MONTH(purchasedate) = 1 THEN YEAR(DATE_SUB(purchasedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(purchasedate) = 12 && WEEKOFYEAR(purchasedate) = 1 THEN YEAR(DATE_ADD(purchasedate, INTERVAL 1 YEAR)) ELSE YEAR(purchasedate) END) * 100 + WEEKOFYEAR(purchasedate)) AS today, ".
			"last_slotidx, SUM(money) AS pay_amount, COUNT(logidx) AS pay_count, buy_platform ".
			"FROM `tbl_user_pay_pattern_log` ".
			"WHERE last_slotidx > 0 AND useridx > 20000 AND '$week_start' <= purchasedate AND purchasedate <= '$week_end' ".
			"GROUP BY ((CASE WHEN WEEKOFYEAR(purchasedate) >= 52 && MONTH(purchasedate) = 1 THEN YEAR(DATE_SUB(purchasedate, INTERVAL 1 YEAR)) ".
			"WHEN MONTH(purchasedate) = 12 && WEEKOFYEAR(purchasedate) = 1 THEN YEAR(DATE_ADD(purchasedate, INTERVAL 1 YEAR))	".
			"ELSE YEAR(purchasedate) END) * 100 + WEEKOFYEAR(purchasedate)), buy_platform, last_slotidx";
	
	$pay_list = $db_other->gettotallist($sql);
	
	for($i=0; $i<sizeof($pay_list); $i++)
	{
		$today = $pay_list[$i]["today"];
		$platform = $pay_list[$i]["buy_platform"];
		$slottype = $pay_list[$i]["last_slotidx"];
		$pay_amount = $pay_list[$i]["pay_amount"];
		$pay_count = $pay_list[$i]["pay_count"];
	
		$sql = "INSERT INTO tbl_slot_rank_week(today, os_type, slottype, pay_amount, pay_count) VALUES($today, $platform, $slottype, $pay_amount, $pay_count) ".
				"ON DUPLICATE KEY UPDATE pay_amount=VALUES(pay_amount), pay_count=VALUES(pay_count);";
		$db_other->execute($sql);
	}
	
	$sql = "SELECT MAX(today) FROM tbl_slot_rank_week";
	$nowweekyear = $db_other->getvalue($sql);
	
	// ratio 추가
	$sql = "UPDATE `tbl_slot_rank_week` ".
			"SET play_rate = ROUND(playcount/(SELECT playcount FROM tbl_slot_stat_week WHERE today = tbl_slot_rank_week.today AND os_type=tbl_slot_rank_week.os_type)*100, 2), ".
    		"	pay_rate = IFNULL(ROUND(pay_amount/(SELECT pay_amount FROM tbl_slot_stat_week WHERE today = tbl_slot_rank_week.today AND os_type=tbl_slot_rank_week.os_type)*100, 2), 0) ".
    		"WHERE today = $nowweekyear;";
	
	$db_other->execute($sql);
	
	
	// ranking 산정
	// web
	$sql = "SELECT today, os_type, slottype, ROUND(money_in * play_rate * (IF(today < 201613, 1, pay_rate))) AS total_point ".
			"FROM `tbl_slot_rank_week` ".
			"WHERE os_type = 0 AND today = $nowweekyear ".
			"ORDER BY today ASC, total_point DESC";
	
	$point_list = $db_other->gettotallist($sql);
	
	$select_today = "";
	$rank = 0;
	
	for($i=0; $i<sizeof($point_list); $i++)
	{
		$today = $point_list[$i]["today"];
		$platform = $point_list[$i]["os_type"];
		$slottype = $point_list[$i]["slottype"];
		$total_point = $point_list[$i]["total_point"];
		
		if($today != $select_today)
		{
			$select_today = $today;
			$rank = 1;
		}
		else
		{
			$rank += 1;
		}
		
		$sql = "UPDATE tbl_slot_rank_week SET rank = $rank WHERE today = $today AND os_type = $platform AND slottype = $slottype";
		$db_other->execute($sql);
	}
	
	// ios
	$sql = "SELECT today, os_type, slottype, ROUND(money_in * play_rate * (IF(today < 201613, 1, pay_rate))) AS total_point ".
			"FROM `tbl_slot_rank_week` ".
			"WHERE os_type = 1 AND today = $nowweekyear ".
			"ORDER BY today ASC, total_point DESC";
	
	$point_list = $db_other->gettotallist($sql);
	
	$select_today = "";
	$rank = 0;
	
	for($i=0; $i<sizeof($point_list); $i++)
	{
		$today = $point_list[$i]["today"];
		$platform = $point_list[$i]["os_type"];
		$slottype = $point_list[$i]["slottype"];
		$total_point = $point_list[$i]["total_point"];
	
		if($today != $select_today)
		{
			$select_today = $today;
			$rank = 1;
		}
		else
		{
			$rank += 1;
		}
	
		$sql = "UPDATE tbl_slot_rank_week SET rank = $rank WHERE today = $today AND os_type = $platform AND slottype = $slottype";
		$db_other->execute($sql);
	}
	
	// android
	$sql = "SELECT today, os_type, slottype, ROUND(money_in * play_rate * (IF(today < 201613, 1, pay_rate))) AS total_point ".
			"FROM `tbl_slot_rank_week` ".
			"WHERE os_type = 2 AND today = $nowweekyear ".
			"ORDER BY today ASC, total_point DESC";
	
	$point_list = $db_other->gettotallist($sql);
	
	$select_today = "";
	$rank = 0;
	
	for($i=0; $i<sizeof($point_list); $i++)
	{
		$today = $point_list[$i]["today"];
		$platform = $point_list[$i]["os_type"];
		$slottype = $point_list[$i]["slottype"];
		$total_point = $point_list[$i]["total_point"];
	
		if($today != $select_today)
		{
			$select_today = $today;
			$rank = 1;
		}
		else
		{
			$rank += 1;
		}
	
		$sql = "UPDATE tbl_slot_rank_week SET rank = $rank WHERE today = $today AND os_type = $platform AND slottype = $slottype";
		$db_other->execute($sql);
	}
	
	// amazon
	$sql = "SELECT today, os_type, slottype, ROUND(money_in * play_rate * (IF(today < 201613, 1, pay_rate))) AS total_point ".
			"FROM `tbl_slot_rank_week` ".
			"WHERE os_type = 3 AND today = $nowweekyear ".
			"ORDER BY today ASC, total_point DESC";
	
	$point_list = $db_other->gettotallist($sql);
	
	$select_today = "";
	$rank = 0;
	
	for($i=0; $i<sizeof($point_list); $i++)
	{
		$today = $point_list[$i]["today"];
		$platform = $point_list[$i]["os_type"];
		$slottype = $point_list[$i]["slottype"];
		$total_point = $point_list[$i]["total_point"];
	
		if($today != $select_today)
		{
			$select_today = $today;
			$rank = 1;
		}
		else
		{
			$rank += 1;
		}
	
		$sql = "UPDATE tbl_slot_rank_week SET rank = $rank WHERE today = $today AND os_type = $platform AND slottype = $slottype";
		$db_other->execute($sql);
	}
	
	// pre_ranking
	$sql = "SELECT today, os_type, slottype ".
			"FROM tbl_slot_rank_week AS t1 ".
			"WHERE today = $nowweekyear";
	
	$pre_rank_list = $db_other->gettotallist($sql);
	
	for($i=0; $i<sizeof($pre_rank_list); $i++)
	{
		$today = $pre_rank_list[$i]["today"];
		$platform = $pre_rank_list[$i]["os_type"];
		$slottype = $pre_rank_list[$i]["slottype"];		
		
		$sql = "SELECT IFNULL(rank, 0) FROM tbl_slot_rank_week WHERE today < $today AND slottype=$slottype AND os_type=$platform ORDER BY today DESC LIMIT 1";
		$pre_rank = $db_other->getvalue($sql);
		
		if($pre_rank == "")
			$pre_rank = 0;
		
		$sql = "UPDATE tbl_slot_rank_week SET pre_rank=$pre_rank WHERE today=$today AND os_type=$platform AND slottype=$slottype";
		$db_other->execute($sql);
	}
	
	$slave_main->end();
	$db_main2->end();
	$db_other->end();
?>
