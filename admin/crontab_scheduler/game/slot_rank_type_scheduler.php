<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");

	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/game/slot_rank_type_scheduler") !== false)
		{
			$count++;
		}
	}
	
	ini_set("memory_limit", "-1");
	
	$issuccess = "1";
	
	if ($count > 1)
		exit();
		
	$db_redshift = new CDatabase_Redshift();
	$db_other = new CDatabase_Other();

	$db_other->execute("SET wait_timeout=72000");
	
	$sdate = date("Y-m-d", time() - 24 * 60 * 60);
	$edate = date("Y-m-d", time() - 24 * 60 * 60);
		
	while($sdate <= $edate)
	{
		try
		{
			//
			// 최근 1주일 결제자 결제액을 최근 2주 슬롯별 머니인 기준 배분
			//
			write_log("slot_rank_type0_web: ".$sdate);
			// web
			// 최근 1주일 결제자 결제액을 최근 2주 슬롯별 머니인 기준 배분
			$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ".
					"from ( ".
  					"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
  					"	from ( ".
  					"		select useridx, round(SUM(money), 2) as money ".
  					"		from ( ".
  					"			select useridx, round(facebookcredit::float/10::float, 2) as money ".
  					"			from t5_product_order ".
  					"			where useridx > 20000 and status = 1 and dateadd(day,(-(7*1)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
    				"		) t1 ".
    				"		group by useridx ".
  					"	) t1 join t5_user_gamelog t2 on t1.useridx = t2.useridx ".
  					"	where dateadd(day,(-(7*2)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ".
  					"	group by t1.useridx, slottype ".
					") t1 join t5_user t2 on t1.useridx = t2.useridx";
			
			$play_list = $db_redshift->gettotallist($sql);
		
			$insert_sql = "";
		
			for($i=0; $i<sizeof($play_list); $i++)
			{
				$type = 0;
				$platform = 0;
				$useridx = $play_list[$i]["useridx"];
				$slottype = $play_list[$i]["slottype"];
				$money = $play_list[$i]["money"];
				$money_in = $play_list[$i]["money_in"];
				$dayafterinstall = $play_list[$i]["dayafterinstall"];
					
				if($insert_sql == "")
					$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
				else
					$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
		
				$insert_cnt++;

				if($i > 0 && $i % 500 == 0)
				{
					$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
					$db_other->execute($insert_sql);
					$insert_sql = "";
				}
			}
		
			if($insert_sql != "")
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
			}
			
			write_log("slot_rank_type0_ios: ".$sdate);
			// ios
			// 최근 1주일 결제자 결제액을 최근 2주 슬롯별 머니인 기준 배분
			$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ".
					"from ( ".
  					"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
  					"	from ( ".
  					"		select useridx, round(SUM(money), 2) as money ".
  					"		from ( ".
  					"			select useridx, money ".
  					"			from t5_product_order_mobile ".
  					"			where useridx > 20000 and os_type = 1 and status = 1 and dateadd(day,(-(7*1)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
  					"		) t1 ".
  					"		group by useridx ".
  					"	) t1 join t5_user_gamelog_ios t2 on t1.useridx = t2.useridx ".
  					"	where dateadd(day,(-(7*2)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ".
  					"	group by t1.useridx, slottype ".
					") t1 join t5_user t2 on t1.useridx = t2.useridx";
			$play_list = $db_redshift->gettotallist($sql);
			
			$insert_sql = "";
			
			for($i=0; $i<sizeof($play_list); $i++)
			{
				$type = 0;
				$platform = 1;
				$useridx = $play_list[$i]["useridx"];
				$slottype = $play_list[$i]["slottype"];
				$money = $play_list[$i]["money"];
				$money_in = $play_list[$i]["money_in"];
				$dayafterinstall = $play_list[$i]["dayafterinstall"];
					
				if($insert_sql == "")
					$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
				else
					$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
		
					$insert_cnt++;
		
				if($i > 0 && $i % 500 == 0)
				{
					$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
					$db_other->execute($insert_sql);
					$insert_sql = "";
				}
			}
			
			if($insert_sql != "")
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
			}
				
			write_log("slot_rank_type0_android: ".$sdate);
			// android
			$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ".
					"from ( ".
					"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
					"	from ( ".
					"		select useridx, round(SUM(money), 2) as money ".
					"		from ( ".
					"			select useridx, money ".
					"			from t5_product_order_mobile ".
					"			where useridx > 20000 and os_type = 2 and status = 1 and dateadd(day,(-(7*1)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
					"		) t1 ".
					"		group by useridx ".
					"	) t1 join t5_user_gamelog_android t2 on t1.useridx = t2.useridx ".
					"	where dateadd(day,(-(7*2)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ".
					"	group by t1.useridx, slottype ".
					") t1 join t5_user t2 on t1.useridx = t2.useridx";
			$play_list = $db_redshift->gettotallist($sql);
				
			$insert_sql = "";
				
			for($i=0; $i<sizeof($play_list); $i++)
			{
				$type = 0;
				$platform = 2;
				$useridx = $play_list[$i]["useridx"];
				$slottype = $play_list[$i]["slottype"];
				$money = $play_list[$i]["money"];
				$money_in = $play_list[$i]["money_in"];
				$dayafterinstall = $play_list[$i]["dayafterinstall"];
					
				if($insert_sql == "")
					$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
				else
					$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
			
				$insert_cnt++;
		
				if($i > 0 && $i % 500 == 0)
				{
					$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
					$db_other->execute($insert_sql);
					$insert_sql = "";
				}
			}
				
			if($insert_sql != "")
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
			}
				
			write_log("slot_rank_type0_amazon: ".$sdate);
			// amazon
			// 최근 1주일 결제자 결제액을 최근 2주 슬롯별 머니인 기준 배분
			$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ".
					"from ( ".
					"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
					"	from ( ".
					"		select useridx, round(SUM(money), 2) as money ".
					"		from ( ".
					"			select useridx, money ".
					"			from t5_product_order_mobile ".
					"			where useridx > 20000 and os_type = 3 and status = 1 and dateadd(day,(-(7*1)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
					"		) t1 ".
					"		group by useridx ".
					"	) t1 join t5_user_gamelog_amazon t2 on t1.useridx = t2.useridx ".
					"	where dateadd(day,(-(7*2)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ".
					"	group by t1.useridx, slottype ".
					") t1 join t5_user t2 on t1.useridx = t2.useridx";
			$play_list = $db_redshift->gettotallist($sql);
			
			$insert_sql = "";
			
			for($i=0; $i<sizeof($play_list); $i++)
			{
				$type = 0;
				$platform = 3;
				$useridx = $play_list[$i]["useridx"];
				$slottype = $play_list[$i]["slottype"];
				$money = $play_list[$i]["money"];
				$money_in = $play_list[$i]["money_in"];
				$dayafterinstall = $play_list[$i]["dayafterinstall"];
					
				if($insert_sql == "")
					$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
				else
					$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
						
				$insert_cnt++;
		
				if($i > 0 && $i % 500 == 0)
				{
					$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
					$db_other->execute($insert_sql);
					$insert_sql = "";
				}
			}
			
			if($insert_sql != "")
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
			}
		
			$sql = "SELECT today, type, platform, useridx, SUM(moneyin) AS total_moneyin ".
					"FROM `tbl_slot_info_raw_log` ".
					"WHERE today = '$sdate' AND type = $type ".
					"GROUP BY today, type, platform, useridx";
			$total_list = $db_other->gettotallist($sql);
			
			$sql = "SELECT SUM(money) AS total_money ".
					"FROM ( ".
					"	SELECT today, TYPE, platform, useridx, ROUND(AVG(money), 2) AS money ".
					"	FROM `tbl_slot_info_raw_log` ".
					"	WHERE today = '$sdate' AND TYPE = $type ".
					"	GROUP BY today, TYPE, platform, useridx ".
					") t1";
			$total_money = $db_other->getvalue($sql);
			
			for($i=0; $i<sizeof($total_list); $i++)
			{
				$today = $total_list[$i]["today"];
				$type = $total_list[$i]["type"];
				$platform = $total_list[$i]["platform"];
				$useridx = $total_list[$i]["useridx"];
				$total_moneyin = $total_list[$i]["total_moneyin"];
					
				$sql = "UPDATE tbl_slot_info_raw_log SET total_money = $total_money, total_moneyin = $total_moneyin WHERE today = '$today' AND type = $type AND platform = $platform AND useridx=$useridx AND total_moneyin = 0";
				$db_other->execute($sql);
			}
			
			
			
			for($j=0; $j<4; $j++)
			{
				// 최근 1주일 결제자 결제액을 최근 2주 슬롯별 머니인 기준 배분 - Daily Rank
				$sql = "SELECT today, type, platform, t1.slottype, user_cnt, money, dayafterinstall ".
						"FROM ( ".
						"	SELECT today, type, platform, slottype, COUNT(useridx) AS user_cnt, ROUND(SUM(money*moneyin / total_moneyin), 2) AS money ".
						"	FROM tbl_slot_info_raw_log ".
						"	WHERE today = '$sdate' AND type = 0 AND platform = $j ".
						"	GROUP BY today, type, platform, slottype ".
						"	HAVING money > 0 ".
						") t1 JOIN ( ".
						"	SELECT slottype, ROUND(SUM(money/total_money*dayafterinstall)) AS dayafterinstall ".
						"	FROM ( ".
						"		SELECT useridx, slottype, ROUND(AVG(money), 2) AS money, ROUND(AVG(total_money), 2) AS total_money, ROUND(AVG(dayafterinstall)) AS dayafterinstall ".
						"		FROM tbl_slot_info_raw_log ".
						"		WHERE today = '$sdate' AND type = 0 AND platform = $j ".
						"		GROUP BY slottype, useridx ".
						"	) t1 ".
						"	GROUP BY slottype ".
						") t2 ON t1.slottype = t2.slottype ".
						"ORDER BY money DESC";
				$day_rank_list = $db_other->gettotallist($sql);
				
				for($i=0; $i<sizeof($day_rank_list); $i++)
				{
					$today = $day_rank_list[$i]["today"];
					$type = $day_rank_list[$i]["type"];
					$platform = $day_rank_list[$i]["platform"];
					$slottype = $day_rank_list[$i]["slottype"];
					$user_cnt = $day_rank_list[$i]["user_cnt"];
					$money = $day_rank_list[$i]["money"];
					$dayafterinstall = $day_rank_list[$i]["dayafterinstall"];
					$rank = $i+1;
					

					$yesterday = date('Y-m-d', strtotime($sdate.' - 1 day'));
					$sql = "SELECT rank FROM tbl_slot_rank_daily WHERE today='$yesterday' AND type=$type AND platform=$platform AND slottype=$slottype";
					$pre_rank = $db_other->getvalue($sql);
					$pre_rank = ($pre_rank == "") ? 0 : $pre_rank;
			
					$sql = "INSERT INTO tbl_slot_rank_daily(today, type, platform, slottype, user_cnt, money, dayafterinstall, pre_rank, rank) ".
							"VALUES('$today', $type, $platform, $slottype, $user_cnt, $money, $dayafterinstall, $pre_rank, $rank) ON DUPLICATE KEY UPDATE user_cnt=VALUES(user_cnt), money=VALUES(money), dayafterinstall=VALUES(dayafterinstall), pre_rank=VALUES(pre_rank), rank=VALUES(rank);";
					$db_other->execute($sql);
				}
			
				$type = 0;
			
				$sql = "SELECT IFNULL(SUM(money), 0) ".
						"FROM `tbl_slot_rank_daily` ".
						"WHERE today = '$sdate' AND type = $type AND platform=$j  ";
				$total_money = $db_other->getvalue($sql);
			
				if($total_money == 0)
					$sql = "UPDATE tbl_slot_rank_daily SET share_rate = 0 WHERE today = '$sdate' AND type = $type AND platform=$j";
				else
					$sql = "UPDATE tbl_slot_rank_daily SET share_rate = ROUND(money/$total_money*100, 2) WHERE today = '$sdate' AND type = $type AND platform=$j";
				
				$db_other->execute($sql);
			}
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
		

		//
		// 최근 4주이내 가입자의 결제액을 최근 4주 슬롯별 머니인 기준 배분
		//
		write_log("slot_rank_type1_web: ".$sdate);
		// web
		// 최근 4주이내 가입자의 결제액을 최근 4주 슬롯별 머니인 기준 배분
		$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ".
				"from ( ".
  				"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
  				"	from ( ".
  				"		select t1.useridx, SUM(facebookcredit::float/10::float) as money ".
  				"		from t5_user t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
  				"		where t1.useridx > 20000 and platform = 0 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= createdate AND createdate < '$sdate 00:00:00' ".
  				"			and status = 1 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
  				"		group by t1.useridx ".
  				"	) t1 join t5_user_gamelog t2 on t1.useridx = t2.useridx and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ".
  				"	group by t1.useridx, slottype ".
				") t1 join t5_user t2 on t1.useridx = t2.useridx";
		$play_list = $db_redshift->gettotallist($sql);
		
		$insert_sql = "";
		
		for($i=0; $i<sizeof($play_list); $i++)
		{
			$type = 1;
			$platform = 0;
			$useridx = $play_list[$i]["useridx"];
			$slottype = $play_list[$i]["slottype"];
			$money = $play_list[$i]["money"];
			$money_in = $play_list[$i]["money_in"];
			$dayafterinstall = $play_list[$i]["dayafterinstall"];
				
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
			else
				$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
		
			$insert_cnt++;
	
			if($i > 0 && $i % 500 == 0)
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
				$insert_sql = "";
			}
		}
		
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall)";
			$db_other->execute($insert_sql);
		}
		
		
		write_log("slot_rank_type1_ios: ".$sdate);
		// ios
		// 최근 4주이내 가입자의 결제액을 최근 4주 슬롯별 머니인 기준 배분
		$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ".
				"from ( ".
  				"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
  				"	from ( ".
  				"		select t1.useridx, SUM(money) as money ".
  				"		from t5_user t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
  				"		where t1.useridx > 20000 and platform = 1 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= createdate AND createdate < '$sdate 00:00:00' ".
  				"			and os_type = 1 and status = 1 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
  				"		group by t1.useridx ".
  				"	) t1 join t5_user_gamelog_ios t2 on t1.useridx = t2.useridx and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ". 
  				"	group by t1.useridx, slottype ".
				") t1 join t5_user t2 on t1.useridx = t2.useridx";
		$play_list = $db_redshift->gettotallist($sql);
			
		$insert_sql = "";
			
		for($i=0; $i<sizeof($play_list); $i++)
		{
			$type = 1;
			$platform = 1;
			$useridx = $play_list[$i]["useridx"];
			$slottype = $play_list[$i]["slottype"];
			$money = $play_list[$i]["money"];
			$dayafterinstall = $play_list[$i]["dayafterinstall"];
			
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
			else
				$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
		
			$insert_cnt++;
	
			if($i > 0 && $i % 500 == 0)
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
				$insert_sql = "";
			}
		}
			
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall)";
			$db_other->execute($insert_sql);
		}
		
		write_log("slot_rank_type1_android: ".$sdate);
		// android
		// 최근 4주이내 가입자의 결제액을 최근 4주 슬롯별 머니인 기준 배분
		$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ".
				"from ( ".
				"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
				"	from ( ".
				"		select t1.useridx, SUM(money) as money ".
				"		from t5_user t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
				"		where t1.useridx > 20000 and platform = 2 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= createdate AND createdate < '$sdate 00:00:00' ".
				"			and os_type = 2 and status = 1 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
				"		group by t1.useridx ".
				"	) t1 join t5_user_gamelog_android t2 on t1.useridx = t2.useridx and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ".
				"	group by t1.useridx, slottype ".
				") t1 join t5_user t2 on t1.useridx = t2.useridx";
		$play_list = $db_redshift->gettotallist($sql);
		
		$insert_sql = "";
		
		for($i=0; $i<sizeof($play_list); $i++)
		{
			$type = 1;
			$platform = 2;
			$useridx = $play_list[$i]["useridx"];
			$slottype = $play_list[$i]["slottype"];
			$money = $play_list[$i]["money"];
			$money_in = $play_list[$i]["money_in"];
			$dayafterinstall = $play_list[$i]["dayafterinstall"];
				
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
			else
				$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
		
			$insert_cnt++;
	
			if($i > 0 && $i % 500 == 0)
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
				$insert_sql = "";
			}
		}
		
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall)";
			$db_other->execute($insert_sql);
		}
		
		write_log("slot_rank_type1_amazon: ".$sdate);
		// amazon
		// 최근 4주이내 가입자의 결제액을 최근 4주 슬롯별 머니인 기준 배분
		$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ".
				"from ( ".
				"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
				"	from ( ".
				"		select t1.useridx, SUM(money) as money ".
				"		from t5_user t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
				"		where t1.useridx > 20000 and platform = 3 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= createdate AND createdate < '$sdate 00:00:00' ".
				"			and os_type = 3 and status = 1 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
				"		group by t1.useridx ".
				"	) t1 join t5_user_gamelog_amazon t2 on t1.useridx = t2.useridx and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ".
				"	group by t1.useridx, slottype ".
				") t1 join t5_user t2 on t1.useridx = t2.useridx";
		$play_list = $db_redshift->gettotallist($sql);
			
		$insert_sql = "";
			
		for($i=0; $i<sizeof($play_list); $i++)
		{
			$type = 1;
			$platform = 3;
			$useridx = $play_list[$i]["useridx"];
			$slottype = $play_list[$i]["slottype"];
			$money = $play_list[$i]["money"];
			$dayafterinstall = $play_list[$i]["dayafterinstall"];
			
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
			else
				$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
			
			$insert_cnt++;
			
			if($i > 0 && $i % 500 == 0)
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
				$insert_sql = "";
			}
		}
			
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
			$db_other->execute($insert_sql);
		}
		
		$type = 1;
					
		$sql = "SELECT today, type, platform, useridx, SUM(moneyin) AS total_moneyin ".
				"FROM `tbl_slot_info_raw_log` ".
				"WHERE today = '$sdate' AND type = $type ".
				"GROUP BY today, type, platform, useridx";
		$total_list = $db_other->gettotallist($sql);
			
		$sql = "SELECT SUM(money) AS total_money ".
				"FROM ( ".
				"	SELECT today, TYPE, platform, useridx, ROUND(AVG(money), 2) AS money ".
				"	FROM `tbl_slot_info_raw_log` ".
				"	WHERE today = '$sdate' AND TYPE = $type ".
				"	GROUP BY today, TYPE, platform, useridx ".
				") t1";
		$total_money = $db_other->getvalue($sql);
			
		for($i=0; $i<sizeof($total_list); $i++)
		{
			$today = $total_list[$i]["today"];
			$type = $total_list[$i]["type"];
			$platform = $total_list[$i]["platform"];
			$useridx = $total_list[$i]["useridx"];
			$total_moneyin = $total_list[$i]["total_moneyin"];
				
			$sql = "UPDATE tbl_slot_info_raw_log SET total_money = $total_money, total_moneyin = $total_moneyin WHERE today = '$today' AND type = $type AND platform = $platform AND useridx=$useridx AND total_moneyin = 0";
			$db_other->execute($sql);
		}
			
		
		
		for($j=0; $j<4; $j++)
		{
			// 최근 4주이내 가입자의 결제액을 최근 4주 슬롯별 머니인 기준 배분 - Daily Rank
			$sql = "SELECT today, type, platform, t1.slottype, user_cnt, money, dayafterinstall ".
					"FROM ( ".
					"	SELECT today, type, platform, slottype, COUNT(useridx) AS user_cnt, ROUND(SUM(money*moneyin / total_moneyin), 2) AS money ".
					"	FROM tbl_slot_info_raw_log ".
					"	WHERE today = '$sdate' AND type = 1 AND platform = $j ".
					"	GROUP BY today, type, platform, slottype ".
					"	HAVING money > 0 ".
					") t1 JOIN ( ".
					"	SELECT slottype, ROUND(SUM(money/total_money*dayafterinstall)) AS dayafterinstall ".
					"	FROM ( ".
					"		SELECT useridx, slottype, ROUND(AVG(money), 2) AS money, ROUND(AVG(total_money), 2) AS total_money, ROUND(AVG(dayafterinstall)) AS dayafterinstall ".
					"		FROM tbl_slot_info_raw_log ".
					"		WHERE today = '$sdate' AND type = 1 AND platform = $j ".
					"		GROUP BY slottype, useridx ".
					"	) t1 ".
					"	GROUP BY slottype ".
					") t2 ON t1.slottype = t2.slottype ".
					"ORDER BY money DESC";
			$day_rank_list = $db_other->gettotallist($sql);
		
			for($i=0; $i<sizeof($day_rank_list); $i++)
			{
				$today = $day_rank_list[$i]["today"];
				$type = $day_rank_list[$i]["type"];
				$platform = $day_rank_list[$i]["platform"];
				$slottype = $day_rank_list[$i]["slottype"];
				$user_cnt = $day_rank_list[$i]["user_cnt"];
				$money = $day_rank_list[$i]["money"];
				$dayafterinstall = $day_rank_list[$i]["dayafterinstall"];
				$rank = $i+1;
					
		
				$yesterday = date('Y-m-d', strtotime($sdate.' - 1 day'));
				$sql = "SELECT rank FROM tbl_slot_rank_daily WHERE today='$yesterday' AND type=$type AND platform=$platform AND slottype=$slottype";
				$pre_rank = $db_other->getvalue($sql);
				$pre_rank = ($pre_rank == "") ? 0 : $pre_rank;
					
				$sql = "INSERT INTO tbl_slot_rank_daily(today, type, platform, slottype, user_cnt, money, dayafterinstall, pre_rank, rank) ".
						"VALUES('$today', $type, $platform, $slottype, $user_cnt, $money, $dayafterinstall, $pre_rank, $rank) ON DUPLICATE KEY UPDATE user_cnt=VALUES(user_cnt), money=VALUES(money), dayafterinstall=VALUES(dayafterinstall), pre_rank=VALUES(pre_rank), rank=VALUES(rank);";
				$db_other->execute($sql);
			}
				
			$type = 1;
				
			$sql = "SELECT IFNULL(SUM(money), 0) ".
					"FROM `tbl_slot_rank_daily` ".
					"WHERE today = '$sdate' AND type = $type AND platform=$j  ";
			$total_money = $db_other->getvalue($sql);
				
			if($total_money == 0)
				$sql = "UPDATE tbl_slot_rank_daily SET share_rate = 0 WHERE today = '$sdate' AND type = $type AND platform=$j";
			else
				$sql = "UPDATE tbl_slot_rank_daily SET share_rate = ROUND(money/$total_money*100, 2) WHERE today = '$sdate' AND type = $type AND platform=$j";
		
			$db_other->execute($sql);
		}
		
		

		//
		// 최근 4주이내 복귀자(D28이상)의 결제액을 최근 4주 슬롯별 머니인 기준 배분
		//
		write_log("slot_rank_type2_web: ".$sdate);
		// web
		// 최근 4주이내 복귀자(D28이상)의 결제액을 최근 4주 슬롯별 머니인 기준 배분
		$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ".
				"from ( ".
  				"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
  				"	from ( ".
  				"		select t1.useridx, SUM(facebookcredit::float/10::float) as money ".
  				"		from ( ".
  				"			select useridx ".
  				"			from t5_user_retention_log ".
  				"			where leavedays >= 28 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
  				"		) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
  				"		where t1.useridx > 20000 and status = 1 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
  				"		group by t1.useridx ".
  				"	) t1 join t5_user_gamelog t2 on t1.useridx = t2.useridx and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ".
  				"	group by t1.useridx, slottype ".
  				"	having SUM(money_in) > 0 ".
				") t1 join t5_user t2 on t1.useridx = t2.useridx";
		$play_list = $db_redshift->gettotallist($sql);
		
		$insert_sql = "";
		
		for($i=0; $i<sizeof($play_list); $i++)
		{
			$type = 2;
			$platform = 0;
			$useridx = $play_list[$i]["useridx"];
			$slottype = $play_list[$i]["slottype"];
			$money = $play_list[$i]["money"];
			$money_in = $play_list[$i]["money_in"];
			$dayafterinstall = $play_list[$i]["dayafterinstall"];
			
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
			else
				$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
		
			$insert_cnt++;
	
			if($i > 0 && $i % 500 == 0)
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
				$insert_sql = "";
			}
		}
		
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
			$db_other->execute($insert_sql);
		}
		
		
		write_log("slot_rank_type2_ios: ".$sdate);
		// ios
		// 최근 4주이내 가입자의 결제액을 최근 4주 슬롯별 머니인 기준 배분
		$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ". 
				"from ( ".
  				"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
  				"	from ( ".
  				"		select t1.useridx, SUM(money) as money ".
  				"		from ( ".
  				"			select useridx, platform ".
  				"			from t5_user_retention_mobile_log ".
  				"			where platform = 1 and leavedays >= 28 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
  				"		) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx and t1.platform = t2.os_type ".
  				"		where t1.useridx > 20000 and status = 1 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
  				"		group by t1.useridx ".
  				"	) t1 join t5_user_gamelog_ios t2 on t1.useridx = t2.useridx and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ". 
  				"	group by t1.useridx, slottype ".
  				"	having SUM(money_in) > 0 ".
				") t1 join t5_user t2 on t1.useridx = t2.useridx";
		$play_list = $db_redshift->gettotallist($sql);
			
		$insert_sql = "";
			
		for($i=0; $i<sizeof($play_list); $i++)
		{
			$type = 2;
			$platform = 1;
			$useridx = $play_list[$i]["useridx"];
			$slottype = $play_list[$i]["slottype"];
			$money = $play_list[$i]["money"];
			$money_in = $play_list[$i]["money_in"];
			$dayafterinstall = $play_list[$i]["dayafterinstall"];
			
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
			else
				$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
		
			$insert_cnt++;
	
			if($i > 0 && $i % 500 == 0)
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
				$insert_sql = "";
			}
		}
			
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
			$db_other->execute($insert_sql);
		}
		
		write_log("slot_rank_type2_android: ".$sdate);
		// android
		// 최근 4주이내 복귀자(D28이상)의 결제액을 최근 4주 슬롯별 머니인 기준 배분
		$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ".
				"from ( ".
				"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
				"	from ( ".
				"		select t1.useridx, SUM(money) as money ".
				"		from ( ".
				"			select useridx, platform ".
				"			from t5_user_retention_mobile_log ".
				"			where platform = 2 and leavedays >= 28 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
				"		) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx and t1.platform = t2.os_type ".
				"		where t1.useridx > 20000 and status = 1 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
				"		group by t1.useridx ".
				"	) t1 join t5_user_gamelog_android t2 on t1.useridx = t2.useridx and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ".
				"	group by t1.useridx, slottype ".
				"	having SUM(money_in) > 0 ".
				") t1 join t5_user t2 on t1.useridx = t2.useridx";
		$play_list = $db_redshift->gettotallist($sql);
		
		$insert_sql = "";
		
		for($i=0; $i<sizeof($play_list); $i++)
		{
			$type = 2;
			$platform = 2;
			$useridx = $play_list[$i]["useridx"];
			$slottype = $play_list[$i]["slottype"];
			$money = $play_list[$i]["money"];
			$money_in = $play_list[$i]["money_in"];
			$dayafterinstall = $play_list[$i]["dayafterinstall"];
				
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
			else
				$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
		
			$insert_cnt++;
	
			if($i > 0 && $i % 500 == 0)
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
				$insert_sql = "";
			}
		}
		
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
			$db_other->execute($insert_sql);
		}
		
		write_log("slot_rank_type2_amazon: ".$sdate);
		// amazon
		// 최근 4주이내 복귀자(D28이상)의 결제액을 최근 4주 슬롯별 머니인 기준 배분
		$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ".
				"from ( ".
				"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
				"	from ( ".
				"		select t1.useridx, SUM(money) as money ".
				"		from ( ".
				"			select useridx, platform ".
				"			from t5_user_retention_mobile_log ".
				"			where platform = 3 and leavedays >= 28 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
				"		) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx and t1.platform = t2.os_type ".
				"		where t1.useridx > 20000 and status = 1 and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' ".
				"		group by t1.useridx ".
				"	) t1 join t5_user_gamelog_amazon t2 on t1.useridx = t2.useridx and dateadd(day,(-(7*4)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ".
				"	group by t1.useridx, slottype ".
				"	having SUM(money_in) > 0 ".
				") t1 join t5_user t2 on t1.useridx = t2.useridx";
		$play_list = $db_redshift->gettotallist($sql);
			
		$insert_sql = "";
			
		for($i=0; $i<sizeof($play_list); $i++)
		{
			$type = 2;
			$platform = 3;
			$useridx = $play_list[$i]["useridx"];
			$slottype = $play_list[$i]["slottype"];
			$money = $play_list[$i]["money"];
			$money_in = $play_list[$i]["money_in"];
			$dayafterinstall = $play_list[$i]["dayafterinstall"];
			
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
			else
				$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
		
			$insert_cnt++;
	
			if($i > 0 && $i % 500 == 0)
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
				$insert_sql = "";
			}
		}
			
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
			$db_other->execute($insert_sql);
		}
		
		$type = 2;
			
		$sql = "SELECT today, type, platform, useridx, SUM(moneyin) AS total_moneyin ".
				"FROM `tbl_slot_info_raw_log` ".
				"WHERE today = '$sdate' AND type = $type ".
				"GROUP BY today, type, platform, useridx";
		$total_list = $db_other->gettotallist($sql);
			
		$sql = "SELECT SUM(money) AS total_money ".
				"FROM ( ".
				"	SELECT today, TYPE, platform, useridx, ROUND(AVG(money), 2) AS money ".
				"	FROM `tbl_slot_info_raw_log` ".
				"	WHERE today = '$sdate' AND TYPE = $type ".
				"	GROUP BY today, TYPE, platform, useridx ".
				") t1";
		$total_money = $db_other->getvalue($sql);
			
		for($i=0; $i<sizeof($total_list); $i++)
		{
			$today = $total_list[$i]["today"];
			$type = $total_list[$i]["type"];
			$platform = $total_list[$i]["platform"];
			$useridx = $total_list[$i]["useridx"];
			$total_moneyin = $total_list[$i]["total_moneyin"];
		
			$sql = "UPDATE tbl_slot_info_raw_log SET total_money = $total_money, total_moneyin = $total_moneyin WHERE today = '$today' AND type = $type AND platform = $platform AND useridx=$useridx AND total_moneyin = 0";
			$db_other->execute($sql);
		}
			
		
		
		for($j=0; $j<4; $j++)
		{
			// 최근 4주이내 복귀자(D28이상)의 결제액을 최근 4주 슬롯별 머니인 기준 배분 - Daily Rank
			$sql = "SELECT today, type, platform, t1.slottype, user_cnt, money, dayafterinstall ".
					"FROM ( ".
					"	SELECT today, type, platform, slottype, COUNT(useridx) AS user_cnt, ROUND(SUM(money*moneyin / total_moneyin), 2) AS money ".
					"	FROM tbl_slot_info_raw_log ".
					"	WHERE today = '$sdate' AND type = 2 AND platform = $j ".
					"	GROUP BY today, type, platform, slottype ".
					"	HAVING money > 0 ".
					") t1 JOIN ( ".
					"	SELECT slottype, ROUND(SUM(money/total_money*dayafterinstall)) AS dayafterinstall ".
					"	FROM ( ".
					"		SELECT useridx, slottype, ROUND(AVG(money), 2) AS money, ROUND(AVG(total_money), 2) AS total_money, ROUND(AVG(dayafterinstall)) AS dayafterinstall ".
					"		FROM tbl_slot_info_raw_log ".
					"		WHERE today = '$sdate' AND type = 2 AND platform = $j ".
					"		GROUP BY slottype, useridx ".
					"	) t1 ".
					"	GROUP BY slottype ".
					") t2 ON t1.slottype = t2.slottype ".
					"ORDER BY money DESC";
			$day_rank_list = $db_other->gettotallist($sql);
		
			for($i=0; $i<sizeof($day_rank_list); $i++)
			{
				$today = $day_rank_list[$i]["today"];
				$type = $day_rank_list[$i]["type"];
				$platform = $day_rank_list[$i]["platform"];
				$slottype = $day_rank_list[$i]["slottype"];
				$user_cnt = $day_rank_list[$i]["user_cnt"];
				$money = $day_rank_list[$i]["money"];
				$dayafterinstall = $day_rank_list[$i]["dayafterinstall"];
				$rank = $i+1;
					
				$yesterday = date('Y-m-d', strtotime($sdate.' - 1 day'));
				$sql = "SELECT rank FROM tbl_slot_rank_daily WHERE today='$yesterday' AND type=$type AND platform=$platform AND slottype=$slottype";
				$pre_rank = $db_other->getvalue($sql);
				$pre_rank = ($pre_rank == "") ? 0 : $pre_rank;
					
				$sql = "INSERT INTO tbl_slot_rank_daily(today, type, platform, slottype, user_cnt, money, dayafterinstall, pre_rank, rank) ".
						"VALUES('$today', $type, $platform, $slottype, $user_cnt, $money, $dayafterinstall, $pre_rank, $rank) ON DUPLICATE KEY UPDATE user_cnt=VALUES(user_cnt), money=VALUES(money), dayafterinstall=VALUES(dayafterinstall), pre_rank=VALUES(pre_rank), rank=VALUES(rank);";
				$db_other->execute($sql);
			}
		
			$type = 2;
		
			$sql = "SELECT IFNULL(SUM(money), 0) ".
					"FROM `tbl_slot_rank_daily` ".
					"WHERE today = '$sdate' AND type = $type AND platform=$j  ";
			$total_money = $db_other->getvalue($sql);
		
			if($total_money == 0)
				$sql = "UPDATE tbl_slot_rank_daily SET share_rate = 0 WHERE today = '$sdate' AND type = $type AND platform=$j";
			else
				$sql = "UPDATE tbl_slot_rank_daily SET share_rate = ROUND(money/$total_money*100, 2) WHERE today = '$sdate' AND type = $type AND platform=$j";
		
			$db_other->execute($sql);
		}
		
		
		
		//
		// 최근 1주 이내 결제복귀자(D28이상)의 결제액은 결제복귀시점 1주 슬롯별 머니인 기준 배분
		//
		write_log("slot_rank_type3_web: ".$sdate);
		// web
		// 최근 1주 이내 결제복귀자(D28이상)의 결제액은 결제복귀시점 1주 슬롯별 머니인 기준 배분
		$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ". 
				"from ( ".
  				"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
  				"	from ( ".
  				"		select t1.useridx, SUM(facebookcredit::float/10::float) as money ".
  				"		from ( ".
  				"			select useridx, platform, purchasedate ".
  				"			from t5_user_28day_leave_purchase_log ".
  				"			where platform = 0 and leavedays >= 28 and dateadd(day,(-(7*1)), '$sdate 00:00:00') <= purchasedate AND purchasedate < '$sdate 00:00:00' ".
  				"		) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
  				"		where t1.useridx > 20000 and status = 1 and datediff(day, purchasedate, writedate) <= 1 ".
  				"			and dateadd(day,(-(7*1)), '$sdate 00:00:00') <= writedate ".
  				"		group by t1.useridx ".
  				"	) t1 join t5_user_gamelog t2 on t1.useridx = t2.useridx and dateadd(day,(-(7*1)), '$sdate 00:00:00') <= writedate AND writedate < '$sdate 00:00:00' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ".
  				"	group by t1.useridx, slottype ".
  				"	having SUM(money_in) > 0 ".
				") t1 join t5_user t2 on t1.useridx = t2.useridx";
		$play_list = $db_redshift->gettotallist($sql);
		
		$insert_sql = "";
		
		for($i=0; $i<sizeof($play_list); $i++)
		{
			$type = 3;
			$platform = 0;
			$useridx = $play_list[$i]["useridx"];
			$slottype = $play_list[$i]["slottype"];
			$money = $play_list[$i]["money"];
			$money_in = $play_list[$i]["money_in"];
			$dayafterinstall = $play_list[$i]["dayafterinstall"];
			
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
			else
				$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
		
			$insert_cnt++;
	
			if($i > 0 && $i % 500 == 0)
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
				$insert_sql = "";
			}
		}
		
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
			$db_other->execute($insert_sql);
		}
		
		write_log("slot_rank_type3_ios: ".$sdate);
		// ios
		// 최근 1주 이내 결제복귀자(D28이상)의 결제액은 결제복귀시점 1주 슬롯별 머니인 기준 배분
		$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ".
				" from ( ".
  				"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
  				"	from ( ".
  				"		select t1.useridx, SUM(money) as money ".
  				"		from ( ".
  				"			select useridx, platform, purchasedate ".
  				"			from t5_user_28day_leave_purchase_log ".
  				"			where platform = 1 and leavedays >= 28 and dateadd(day,(-(7*1)), '$sdate 00:00:00') <= purchasedate AND purchasedate < '$sdate' ".
  				"		) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
  				"		where t1.useridx > 20000 and status = 1 and datediff(day, purchasedate, writedate) <= 1 ".
  				"			and dateadd(day,(-(7*1)), '$sdate') <= writedate ".
  				"		group by t1.useridx ".
  				"	) t1 join t5_user_gamelog_ios t2 on t1.useridx = t2.useridx and dateadd(day,(-(7*1)), '$sdate') <= writedate AND writedate < '$sdate' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ". 
  				"	group by t1.useridx, slottype ".
  				"	having SUM(money_in) > 0 ".
				") t1 join t5_user t2 on t1.useridx = t2.useridx";
		$play_list = $db_redshift->gettotallist($sql);
			
		$insert_sql = "";
			
		for($i=0; $i<sizeof($play_list); $i++)
		{
			$type = 3;
			$platform = 1;
			$useridx = $play_list[$i]["useridx"];
			$slottype = $play_list[$i]["slottype"];
			$money = $play_list[$i]["money"];
			$money_in = $play_list[$i]["money_in"];
			$dayafterinstall = $play_list[$i]["dayafterinstall"];
				
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
			else
				$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
		
			$insert_cnt++;
	
			if($i > 0 && $i % 500 == 0)
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
				$insert_sql = "";
			}
		}
			
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
			$db_other->execute($insert_sql);
		}
		
		write_log("slot_rank_type3_android: ".$sdate);
		// android
		// 최근 1주 이내 결제복귀자(D28이상)의 결제액은 결제복귀시점 1주 슬롯별 머니인 기준 배분
		$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ".
				" from ( ".
				"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
				"	from ( ".
				"		select t1.useridx, SUM(money) as money ".
				"		from ( ".
				"			select useridx, platform, purchasedate ".
				"			from t5_user_28day_leave_purchase_log ".
				"			where platform = 2 and leavedays >= 28 and dateadd(day,(-(7*1)), '$sdate 00:00:00') <= purchasedate AND purchasedate < '$sdate' ".
				"		) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
				"		where t1.useridx > 20000 and status = 1 and datediff(day, purchasedate, writedate) <= 1 ".
				"			and dateadd(day,(-(7*1)), '$sdate') <= writedate ".
				"		group by t1.useridx ".
				"	) t1 join t5_user_gamelog_android t2 on t1.useridx = t2.useridx and dateadd(day,(-(7*1)), '$sdate') <= writedate AND writedate < '$sdate' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ".
				"	group by t1.useridx, slottype ".
				"	having SUM(money_in) > 0 ".
				") t1 join t5_user t2 on t1.useridx = t2.useridx";
		$play_list = $db_redshift->gettotallist($sql);
		
		$insert_sql = "";
		
		for($i=0; $i<sizeof($play_list); $i++)
		{
			$type = 3;
			$platform = 2;
			$useridx = $play_list[$i]["useridx"];
			$slottype = $play_list[$i]["slottype"];
			$money = $play_list[$i]["money"];
			$money_in = $play_list[$i]["money_in"];
			$dayafterinstall = $play_list[$i]["dayafterinstall"];
			
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
			else
				$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
		
			$insert_cnt++;
	
			if($i > 0 && $i % 500 == 0)
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
				$insert_sql = "";
			}
		}
		
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
			$db_other->execute($insert_sql);
		}
		
		write_log("slot_rank_type3_amazon: ".$sdate);
		// amazon
		// 최근 1주 이내 결제복귀자(D28이상)의 결제액은 결제복귀시점 1주 슬롯별 머니인 기준 배분
		$sql = "select t1.useridx, slottype, money, money_in, datediff(day, createdate, '$sdate 00:00:00') as dayafterinstall ".
				" from ( ".
				"	select t1.useridx, slottype, ROUND(AVG(money), 2) as money, SUM(money_in) as money_in ".
				"	from ( ".
				"		select t1.useridx, SUM(money) as money ".
				"		from ( ".
				"			select useridx, platform, purchasedate ".
				"			from t5_user_28day_leave_purchase_log ".
				"			where platform = 3 and leavedays >= 28 and dateadd(day,(-(7*1)), '$sdate 00:00:00') <= purchasedate AND purchasedate < '$sdate' ".
				"		) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
				"		where t1.useridx > 20000 and status = 1 and datediff(day, purchasedate, writedate) <= 1 ".
				"			and dateadd(day,(-(7*1)), '$sdate') <= writedate ".
				"		group by t1.useridx ".
				"	) t1 join t5_user_gamelog_amazon t2 on t1.useridx = t2.useridx and dateadd(day,(-(7*1)), '$sdate') <= writedate AND writedate < '$sdate' AND mode NOT IN (4, 5, 9, 26, 29) AND money_in > 0 ".
				"	group by t1.useridx, slottype ".
				"	having SUM(money_in) > 0 ".
				") t1 join t5_user t2 on t1.useridx = t2.useridx";
		$play_list = $db_redshift->gettotallist($sql);
			
		$insert_sql = "";
			
		for($i=0; $i<sizeof($play_list); $i++)
		{
			$type = 3;
			$platform = 3;
			$useridx = $play_list[$i]["useridx"];
			$slottype = $play_list[$i]["slottype"];
			$money = $play_list[$i]["money"];
			$money_in = $play_list[$i]["money_in"];
			$dayafterinstall = $play_list[$i]["dayafterinstall"];
				
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_slot_info_raw_log(today, type, platform, useridx, slottype, money, moneyin, total_moneyin, dayafterinstall) VALUES('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
			else
				$insert_sql .= ", ('$sdate', $type, $platform, $useridx, $slottype, $money, $money_in, 0, $dayafterinstall)";
		
			$insert_cnt++;
	
			if($i > 0 && $i % 500 == 0)
			{
				$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
				$db_other->execute($insert_sql);
				$insert_sql = "";
			}
		}
			
		if($insert_sql != "")
		{
			$insert_sql .= " ON DUPLICATE KEY UPDATE moneyin = VALUES(moneyin), dayafterinstall = VALUES(dayafterinstall) ";
			$db_other->execute($insert_sql);
		}
		
		$type = 3;
			
		$sql = "SELECT today, type, platform, useridx, SUM(moneyin) AS total_moneyin ".
				"FROM `tbl_slot_info_raw_log` ".
				"WHERE today = '$sdate' AND type = $type ".
				"GROUP BY today, type, platform, useridx";
		$total_list = $db_other->gettotallist($sql);
			
		$sql = "SELECT SUM(money) AS total_money ".
				"FROM ( ".
				"	SELECT today, TYPE, platform, useridx, ROUND(AVG(money), 2) AS money ".
				"	FROM `tbl_slot_info_raw_log` ".
				"	WHERE today = '$sdate' AND TYPE = $type ".
				"	GROUP BY today, TYPE, platform, useridx ".
				") t1";
		$total_money = $db_other->getvalue($sql);
			
		for($i=0; $i<sizeof($total_list); $i++)
		{
			$today = $total_list[$i]["today"];
			$type = $total_list[$i]["type"];
			$platform = $total_list[$i]["platform"];
			$useridx = $total_list[$i]["useridx"];
			$total_moneyin = $total_list[$i]["total_moneyin"];
		
			$sql = "UPDATE tbl_slot_info_raw_log SET total_money = $total_money, total_moneyin = $total_moneyin WHERE today = '$today' AND type = $type AND platform = $platform AND useridx=$useridx AND total_moneyin = 0";
			$db_other->execute($sql);
		}
			
			

		for($j=0; $j<4; $j++)
		{
			// 최근 1주 이내 결제복귀자(D28이상)의 결제액은 결제복귀시점 1주 슬롯별 머니인 기준 배분 - Daily Rank
			$sql = "SELECT today, type, platform, t1.slottype, user_cnt, money, dayafterinstall ".
					"FROM ( ".
					"	SELECT today, type, platform, slottype, COUNT(useridx) AS user_cnt, ROUND(SUM(money*moneyin / total_moneyin), 2) AS money ".
					"	FROM tbl_slot_info_raw_log ".
					"	WHERE today = '$sdate' AND type = 3 AND platform = $j ".
					"	GROUP BY today, type, platform, slottype ".
					"	HAVING money > 0 ".
					") t1 JOIN ( ".
					"	SELECT slottype, ROUND(SUM(money/total_money*dayafterinstall)) AS dayafterinstall ".
					"	FROM ( ".
					"		SELECT useridx, slottype, ROUND(AVG(money), 2) AS money, ROUND(AVG(total_money), 2) AS total_money, ROUND(AVG(dayafterinstall)) AS dayafterinstall ".
					"		FROM tbl_slot_info_raw_log ".
					"		WHERE today = '$sdate' AND type = 3 AND platform = $j ".
					"		GROUP BY slottype, useridx ".
					"	) t1 ".
					"	GROUP BY slottype ".
					") t2 ON t1.slottype = t2.slottype ".
					"ORDER BY money DESC";
			$day_rank_list = $db_other->gettotallist($sql);
		
			for($i=0; $i<sizeof($day_rank_list); $i++)
			{
				$today = $day_rank_list[$i]["today"];
				$type = $day_rank_list[$i]["type"];
				$platform = $day_rank_list[$i]["platform"];
				$slottype = $day_rank_list[$i]["slottype"];
				$user_cnt = $day_rank_list[$i]["user_cnt"];
				$money = $day_rank_list[$i]["money"];
				$dayafterinstall = $day_rank_list[$i]["dayafterinstall"];
				$rank = $i+1;
					
				$yesterday = date('Y-m-d', strtotime($sdate.' - 1 day'));
				$sql = "SELECT rank FROM tbl_slot_rank_daily WHERE today='$yesterday' AND type=$type AND platform=$platform AND slottype=$slottype";
				$pre_rank = $db_other->getvalue($sql);
				$pre_rank = ($pre_rank == "") ? 0 : $pre_rank;
					
				$sql = "INSERT INTO tbl_slot_rank_daily(today, type, platform, slottype, user_cnt, money, dayafterinstall, pre_rank, rank) ".
						"VALUES('$today', $type, $platform, $slottype, $user_cnt, $money, $dayafterinstall, $pre_rank, $rank) ON DUPLICATE KEY UPDATE user_cnt=VALUES(user_cnt), money=VALUES(money), dayafterinstall=VALUES(dayafterinstall), pre_rank=VALUES(pre_rank), rank=VALUES(rank);";
				$db_other->execute($sql);
			}
		
			$type = 3;
		
			$sql = "SELECT IFNULL(SUM(money), 0) ".
					"FROM `tbl_slot_rank_daily` ".
					"WHERE today = '$sdate' AND type = $type AND platform=$j  ";
			$total_money = $db_other->getvalue($sql);
		
			if($total_money == 0)
				$sql = "UPDATE tbl_slot_rank_daily SET share_rate = 0 WHERE today = '$sdate' AND type = $type AND platform=$j";
			else
				$sql = "UPDATE tbl_slot_rank_daily SET share_rate = ROUND(money/$total_money*100, 2) WHERE today = '$sdate' AND type = $type AND platform=$j";
	
			$db_other->execute($sql);
		}
		
		try
		{
			// 최근 1주일 결제자 결제액을 최근 2주 슬롯별 머니인 기준 배분 - Daily Rank
			$sql = "SELECT today, type, t1.slottype, user_cnt, money, dayafterinstall ".
					"FROM ( ".
					"	SELECT today, type, slottype, COUNT(useridx) AS user_cnt, ROUND(SUM(money*moneyin / total_moneyin), 2) AS money ".
					"	FROM tbl_slot_info_raw_log ".
					"	WHERE today = '$sdate' AND type = 0 ".
					"	GROUP BY today, type, slottype ".
					"	HAVING money > 0 ".
					") t1 JOIN ( ".
					"	SELECT slottype, ROUND(SUM(money/total_money*dayafterinstall)) AS dayafterinstall ".
					"	FROM ( ".
					"		SELECT useridx, slottype, ROUND(AVG(money), 2) AS money, ROUND(AVG(total_money), 2) AS total_money, ROUND(AVG(dayafterinstall)) AS dayafterinstall ".
					"		FROM tbl_slot_info_raw_log ".
					"		WHERE today = '$sdate' AND type = 0 ".
					"		GROUP BY slottype, useridx ".
					"	) t1 ".
					"	GROUP BY slottype ".
					") t2 ON t1.slottype = t2.slottype ".
					"ORDER BY money DESC";
			$day_rank_list = $db_other->gettotallist($sql);
		
			for($i=0; $i<sizeof($day_rank_list); $i++)
			{
				$today = $day_rank_list[$i]["today"];
				$type = $day_rank_list[$i]["type"];
				$slottype = $day_rank_list[$i]["slottype"];
				$user_cnt = $day_rank_list[$i]["user_cnt"];
				$money = $day_rank_list[$i]["money"];
				$dayafterinstall = $day_rank_list[$i]["dayafterinstall"];
				$rank = $i+1;
					
				$yesterday = date('Y-m-d', strtotime($sdate.' - 1 day'));
				$sql = "SELECT rank FROM tbl_slot_rank_daily WHERE today='$yesterday' AND type=$type AND platform=4 AND slottype=$slottype";
				$pre_rank = $db_other->getvalue($sql);
				$pre_rank = ($pre_rank == "") ? 0 : $pre_rank;
					
				$sql = "INSERT INTO tbl_slot_rank_daily(today, type, platform, slottype, user_cnt, money, dayafterinstall, pre_rank, rank) ".
						"VALUES('$today', $type, 4, $slottype, $user_cnt, $money, $dayafterinstall, $pre_rank, $rank) ON DUPLICATE KEY UPDATE user_cnt=VALUES(user_cnt), money=VALUES(money), dayafterinstall=VALUES(dayafterinstall), pre_rank=VALUES(pre_rank), rank=VALUES(rank);";
				$db_other->execute($sql);
			}
		
			$type = 0;
		
			$sql = "SELECT IFNULL(SUM(money), 0) ".
					"FROM `tbl_slot_rank_daily` ".
					"WHERE today = '$sdate' AND type = $type AND platform = 4 ";
			$total_money = $db_other->getvalue($sql);
		
			if($total_money == 0)
				$sql = "UPDATE tbl_slot_rank_daily SET share_rate = 0 WHERE today = '$sdate' AND type = $type AND platform=4";
				else
					$sql = "UPDATE tbl_slot_rank_daily SET share_rate = ROUND(money/$total_money*100, 2) WHERE today = '$sdate' AND type = $type AND platform=4";
		
					$db_other->execute($sql);
		
					// 최근 4주이내 가입자의 결제액을 최근 4주 슬롯별 머니인 기준 배분 - Daily Rank
					$sql = "SELECT today, type, t1.slottype, user_cnt, money, dayafterinstall ".
							"FROM ( ".
							"	SELECT today, type, slottype, COUNT(useridx) AS user_cnt, ROUND(SUM(money*moneyin / total_moneyin), 2) AS money ".
							"	FROM tbl_slot_info_raw_log ".
							"	WHERE today = '$sdate' AND type = 1 ".
							"	GROUP BY today, type, slottype ".
							"	HAVING money > 0 ".
							") t1 JOIN ( ".
							"	SELECT slottype, ROUND(SUM(money/total_money*dayafterinstall)) AS dayafterinstall ".
							"	FROM ( ".
							"		SELECT useridx, slottype, ROUND(AVG(money), 2) AS money, ROUND(AVG(total_money), 2) AS total_money, ROUND(AVG(dayafterinstall)) AS dayafterinstall ".
							"		FROM tbl_slot_info_raw_log ".
							"		WHERE today = '$sdate' AND type = 1 ".
							"		GROUP BY slottype, useridx ".
							"	) t1 ".
							"	GROUP BY slottype ".
							") t2 ON t1.slottype = t2.slottype ".
							"ORDER BY money DESC";
					$day_rank_list = $db_other->gettotallist($sql);
		
					for($i=0; $i<sizeof($day_rank_list); $i++)
					{
						$today = $day_rank_list[$i]["today"];
						$type = $day_rank_list[$i]["type"];
						$slottype = $day_rank_list[$i]["slottype"];
						$user_cnt = $day_rank_list[$i]["user_cnt"];
						$money = $day_rank_list[$i]["money"];
						$dayafterinstall = $day_rank_list[$i]["dayafterinstall"];
						$rank = $i+1;
							
						$yesterday = date('Y-m-d', strtotime($sdate.' - 1 day'));
						$sql = "SELECT rank FROM tbl_slot_rank_daily WHERE today='$yesterday' AND type=$type AND platform=4 AND slottype=$slottype";
						$pre_rank = $db_other->getvalue($sql);
						$pre_rank = ($pre_rank == "") ? 0 : $pre_rank;
							
						$sql = "INSERT INTO tbl_slot_rank_daily(today, type, platform, slottype, user_cnt, money, dayafterinstall, pre_rank, rank) ".
								"VALUES('$today', $type, 4, $slottype, $user_cnt, $money, $dayafterinstall, $pre_rank, $rank) ON DUPLICATE KEY UPDATE user_cnt=VALUES(user_cnt), money=VALUES(money), dayafterinstall=VALUES(dayafterinstall), pre_rank=VALUES(pre_rank), rank=VALUES(rank);";
						$db_other->execute($sql);
					}
		
					$type = 1;
		
					$sql = "SELECT IFNULL(SUM(money), 0) ".
							"FROM `tbl_slot_rank_daily` ".
							"WHERE today = '$sdate' AND type = $type AND platform = 4 ";
					$total_money = $db_other->getvalue($sql);
		
					if($total_money == 0)
						$sql = "UPDATE tbl_slot_rank_daily SET share_rate = 0 WHERE today = '$sdate' AND type = $type AND platform=4";
						else
							$sql = "UPDATE tbl_slot_rank_daily SET share_rate = ROUND(money/$total_money*100, 2) WHERE today = '$sdate' AND type = $type AND platform=4";
		
							$db_other->execute($sql);
		
							// 최근 4주이내 복귀자(D28이상)의 결제액을 최근 4주 슬롯별 머니인 기준 배분 - Daily Rank
							$sql = "SELECT today, type, t1.slottype, user_cnt, money, dayafterinstall ".
									"FROM ( ".
									"	SELECT today, type, slottype, COUNT(useridx) AS user_cnt, ROUND(SUM(money*moneyin / total_moneyin), 2) AS money ".
									"	FROM tbl_slot_info_raw_log ".
									"	WHERE today = '$sdate' AND type = 2 ".
									"	GROUP BY today, type, slottype ".
									"	HAVING money > 0 ".
									") t1 JOIN ( ".
									"	SELECT slottype, ROUND(SUM(money/total_money*dayafterinstall)) AS dayafterinstall ".
									"	FROM ( ".
									"		SELECT useridx, slottype, ROUND(AVG(money), 2) AS money, ROUND(AVG(total_money), 2) AS total_money, ROUND(AVG(dayafterinstall)) AS dayafterinstall ".
									"		FROM tbl_slot_info_raw_log ".
									"		WHERE today = '$sdate' AND type = 2 ".
									"		GROUP BY slottype, useridx ".
									"	) t1 ".
									"	GROUP BY slottype ".
									") t2 ON t1.slottype = t2.slottype ".
									"ORDER BY money DESC";
							$day_rank_list = $db_other->gettotallist($sql);
		
							for($i=0; $i<sizeof($day_rank_list); $i++)
							{
								$today = $day_rank_list[$i]["today"];
								$type = $day_rank_list[$i]["type"];
								$slottype = $day_rank_list[$i]["slottype"];
								$user_cnt = $day_rank_list[$i]["user_cnt"];
								$money = $day_rank_list[$i]["money"];
								$dayafterinstall = $day_rank_list[$i]["dayafterinstall"];
								$rank = $i+1;
									
								$yesterday = date('Y-m-d', strtotime($sdate.' - 1 day'));
								$sql = "SELECT rank FROM tbl_slot_rank_daily WHERE today='$yesterday' AND type=$type AND platform=4 AND slottype=$slottype";
								$pre_rank = $db_other->getvalue($sql);
								$pre_rank = ($pre_rank == "") ? 0 : $pre_rank;
									
								$sql = "INSERT INTO tbl_slot_rank_daily(today, type, platform, slottype, user_cnt, money, dayafterinstall, pre_rank, rank) ".
										"VALUES('$today', $type, 4, $slottype, $user_cnt, $money, $dayafterinstall, $pre_rank, $rank) ON DUPLICATE KEY UPDATE user_cnt=VALUES(user_cnt), money=VALUES(money), dayafterinstall=VALUES(dayafterinstall), pre_rank=VALUES(pre_rank), rank=VALUES(rank);";
								$db_other->execute($sql);
							}
		
							$type = 2;
		
							$sql = "SELECT IFNULL(SUM(money), 0) ".
									"FROM `tbl_slot_rank_daily` ".
									"WHERE today = '$sdate' AND type = $type AND platform = 4 ";
							$total_money = $db_other->getvalue($sql);
		
							if($total_money == 0)
								$sql = "UPDATE tbl_slot_rank_daily SET share_rate = 0 WHERE today = '$sdate' AND type = $type AND platform=4";
								else
									$sql = "UPDATE tbl_slot_rank_daily SET share_rate = ROUND(money/$total_money*100, 2) WHERE today = '$sdate' AND type = $type AND platform=4";
		
									$db_other->execute($sql);
		
									// 최근 1주 이내 결제복귀자(D28이상)의 결제액은 결제복귀시점 1주 슬롯별 머니인 기준 배분 - Daily Rank
									$sql = "SELECT today, type, t1.slottype, user_cnt, money, dayafterinstall ".
											"FROM ( ".
											"	SELECT today, type, slottype, COUNT(useridx) AS user_cnt, ROUND(SUM(money*moneyin / total_moneyin), 2) AS money ".
											"	FROM tbl_slot_info_raw_log ".
											"	WHERE today = '$sdate' AND type = 3 ".
											"	GROUP BY today, type, slottype ".
											"	HAVING money > 0 ".
											") t1 JOIN ( ".
											"	SELECT slottype, ROUND(SUM(money/total_money*dayafterinstall)) AS dayafterinstall ".
											"	FROM ( ".
											"		SELECT useridx, slottype, ROUND(AVG(money), 2) AS money, ROUND(AVG(total_money), 2) AS total_money, ROUND(AVG(dayafterinstall)) AS dayafterinstall ".
											"		FROM tbl_slot_info_raw_log ".
											"		WHERE today = '$sdate' AND type = 3 ".
											"		GROUP BY slottype, useridx ".
											"	) t1 ".
											"	GROUP BY slottype ".
											") t2 ON t1.slottype = t2.slottype ".
											"ORDER BY money DESC";
		
									$day_rank_list = $db_other->gettotallist($sql);
		
									for($i=0; $i<sizeof($day_rank_list); $i++)
									{
										$today = $day_rank_list[$i]["today"];
										$type = $day_rank_list[$i]["type"];
										$slottype = $day_rank_list[$i]["slottype"];
										$user_cnt = $day_rank_list[$i]["user_cnt"];
										$money = $day_rank_list[$i]["money"];
										$dayafterinstall = $day_rank_list[$i]["dayafterinstall"];
										$rank = $i+1;
											
										$yesterday = date('Y-m-d', strtotime($sdate.' - 1 day'));
										$sql = "SELECT rank FROM tbl_slot_rank_daily WHERE today='$yesterday' AND type=$type AND platform=4 AND slottype=$slottype";
										$pre_rank = $db_other->getvalue($sql);
										$pre_rank = ($pre_rank == "") ? 0 : $pre_rank;
											
										$sql = "INSERT INTO tbl_slot_rank_daily(today, type, platform, slottype, user_cnt, money, dayafterinstall, pre_rank, rank) ".
												"VALUES('$today', $type, 4, $slottype, $user_cnt, $money, $dayafterinstall, $pre_rank, $rank) ON DUPLICATE KEY UPDATE user_cnt=VALUES(user_cnt), money=VALUES(money), dayafterinstall=VALUES(dayafterinstall), pre_rank=VALUES(pre_rank), rank=VALUES(rank);";
										$db_other->execute($sql);
									}
		
									$type = 3;
		
									$sql = "SELECT IFNULL(SUM(money), 0) ".
											"FROM `tbl_slot_rank_daily` ".
											"WHERE today = '$sdate' AND type = $type AND platform = 4 ";
									$total_money = $db_other->getvalue($sql);
		
									if($total_money == 0)
										$sql = "UPDATE tbl_slot_rank_daily SET share_rate = 0 WHERE today = '$sdate' AND type = $type AND platform=4";
										else
											$sql = "UPDATE tbl_slot_rank_daily SET share_rate = ROUND(money/$total_money*100, 2) WHERE today = '$sdate' AND type = $type AND platform=4";
		
											$db_other->execute($sql);
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}

		$sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));
	}
	
	$db_redshift->end();
	$db_other->end();
?>
