<?      
	include("../../common/common_include.inc.php");

	$db_main2 = new CDatabase_Main2();
	$db_slave_main2 = new CDatabase_Slave_Main2();

	$db_main2->execute("SET wait_timeout=7200");
	$db_slave_main2->execute("SET wait_timeout=7200");
	
	try
	{
	    $event_idx = 6;
	    try
	    {
	        $sql = "SELECT today
                        	,collection_cnt
                        	,os_type
                        	,COUNT(DISTINCT useridx)  AS usercnt
                        	,SUM(reward) AS reward
                        FROM (
                        	SELECT 
                        	DATE_FORMAT(write_date,'%Y-%m-%d') AS today
                        	, useridx
                        	, collection_cnt
                        	, IFNULL((SELECT SUM(collection_coin) FROM tbl_user_collection_log WHERE useridx = t1.useridx AND event_idx = t1.event_idx AND collect_type = 0),0) AS reward
                        	, IFNULL((SELECT os_type FROM tbl_user_collection_log WHERE useridx = t1.useridx AND event_idx = t1.event_idx AND collect_type = 0 ORDER BY write_date DESC LIMIT 1),0) AS os_type
                        	FROM(
                        		SELECT * FROM `tbl_user_collection_info_0` WHERE event_idx = $event_idx AND collection_list != ''
                        		UNION ALL
                        		SELECT * FROM `tbl_user_collection_info_1` WHERE event_idx = $event_idx AND collection_list != ''
                        		UNION ALL
                        		SELECT * FROM `tbl_user_collection_info_2` WHERE event_idx = $event_idx AND collection_list != ''
                        		UNION ALL
                        		SELECT * FROM `tbl_user_collection_info_3` WHERE event_idx = $event_idx AND collection_list != '' 
                        		UNION ALL
                        		SELECT * FROM `tbl_user_collection_info_4` WHERE event_idx = $event_idx AND collection_list != ''
                        		UNION ALL
                        		SELECT * FROM `tbl_user_collection_info_5` WHERE event_idx = $event_idx AND collection_list != ''
                        		UNION ALL
                        		SELECT * FROM `tbl_user_collection_info_6` WHERE event_idx = $event_idx AND collection_list != ''
                        		UNION ALL
                        		SELECT * FROM `tbl_user_collection_info_7` WHERE event_idx = $event_idx AND collection_list != ''
                        		UNION ALL
                        		SELECT * FROM `tbl_user_collection_info_8` WHERE event_idx = $event_idx AND collection_list != ''
                        		UNION ALL
                        		SELECT * FROM `tbl_user_collection_info_9` WHERE event_idx = $event_idx AND collection_list != ''
                        	)t1
                        )d1
                        GROUP BY today,os_type,collection_cnt";
	        $date = $db_slave_main2->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date); $i++)
	        {
	            $today = $date[$i]["today"];
	            $collection_cnt = $date[$i]["collection_cnt"];
	            $os_type = $date[$i]["os_type"];
	            $usercnt = $date[$i]["usercnt"];
	            $reward = $date[$i]["reward"];
	            
	            $sql ="INSERT INTO tbl_user_collection_stat_daily (today, collection_cnt, os_type, usercount, reward) ".
	   	            " VALUES( '$today', '$collection_cnt', '$os_type', '$usercnt', '$reward') ".
                        " ON DUPLICATE KEY UPDATE usercount=VALUES(usercount), reward=VALUES(reward);";
	            $db_main2->execute($sql);
	                
	        }
	    }
	    catch(Exception $e)
	    {
	        write_log($e->getMessage());
	    }
	    
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	}
	
	$db_main2->end();
	$db_slave_main2->end();
?>
