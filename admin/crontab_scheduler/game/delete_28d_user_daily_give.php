<?      
	include("../../common/common_include.inc.php");

	$slave_main = new CDatabase_Slave_Main();
	$db_main2 = new CDatabase_Main2();
	
	ini_set("memory_limit", "-1");
	
	try
	{
		
		$sql = "SELECT MAX(useridx) FROM tmp_tbl_28d_delete_user";
		$max_useridx = $db_main2->getvalue($sql);
		
		$sql = "SELECT useridx FROM tbl_user WHERE useridx > $max_useridx AND logindate < DATE_SUB(NOW(), INTERVAL 29 DAY) LIMIT 1000000;";
		$user_list = $slave_main->gettotallist($sql);
		
		$insert_cnt = 0;
		$insert_useridx = "";
		
		for($i=0; $i<sizeof($user_list); $i++)
		{
			$useridx = $user_list[$i]["useridx"];
			
			if($insert_cnt == 0)
			{
				$insert_useridx = "INSERT INTO tmp_tbl_28d_delete_user(useridx, flag, writedate) VALUES ($useridx, 1, NOW())";
				$insert_cnt++;
			}
			else if($insert_cnt < 500)
			{
				$insert_useridx .= ",($useridx, 1, NOW())";
				$insert_cnt++;
			}
			else 
			{
				$insert_useridx .= " ON DUPLICATE KEY UPDATE flag = VALUES(flag), writedate = VALUES(writedate);";
				$db_main2->execute($insert_useridx);
				
				$insert_useridx = "";
				$insert_cnt = 0;
			}				
		}
		
		if($insert_useridx != "")
		{
			$insert_useridx .= " ON DUPLICATE KEY UPDATE flag = VALUES(flag), writedate = VALUES(writedate);";
			$db_main2->execute($insert_useridx);
			
			$insert_useridx = "";
			$insert_cnt = 0;
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$slave_main->end();
	$db_main2->end();
?>