<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");

	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/game/user_play_stat_week_summary") !== false)
		{
			$count++;
		}
	}
	
	ini_set("memory_limit", "-1");
	
	$issuccess = "1";
	
	if ($count > 1)
		exit();
		
	$db_redshift = new CDatabase_Redshift();
	$db_other = new CDatabase_Other();

	$db_other->execute("SET wait_timeout=72000");
	
	$current_date = date("Y-m-d");
	
	try
	{
		$sql = "TRUNCATE TABLE t5_user_playstat_week_summary;";
		$db_redshift->execute($sql);
		
		$sql = "INSERT INTO t5_user_playstat_week_summary ".
				"SELECT useridx, (CASE WHEN SUM(money_in) > 0 THEN ROUND(SUM(money_out)::FLOAT/SUM(money_in)::FLOAT*100::FLOAT) ELSE 0 END) AS winrate, SUM(playcount) AS playcount, ". 
				"SUM(money_in) AS week_moneyin,  '$current_date' AS writedate	".		
				"FROM (	".		 
				"	SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out,  (playcount + h_playcount) as playcount ".		 
				"	FROM t5_user_playstat_daily		 ".
				"	WHERE today >= dateadd(DAY, -8, '$current_date'::date) ".		
				"	UNION ALL		 ".
				"	SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out,  (playcount + h_playcount) as playcount ".
				"	FROM t5_user_playstat_daily_ios		".
				"	WHERE today >= dateadd(DAY, -8, '$current_date'::date) ".		
				"	UNION ALL		 ".
				"	SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out,  (playcount + h_playcount) as playcount ".		 
				"	FROM t5_user_playstat_daily_android		 ".
				"	WHERE today >= dateadd(DAY, -8, '$current_date'::date) ".		
				"	UNION ALL		 ".
				"	SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out,  (playcount + h_playcount) as playcount ".		 
				"	FROM t5_user_playstat_daily_amazon		 ".
				"WHERE today >= dateadd(DAY, -8, '$current_date'::date)	".	
				") a		".
				"GROUP BY useridx;";
		$db_redshift->execute($sql);
		
		$insertsql = " INSERT INTO tbl_user_playstat_week_summary ".
        		  		" SELECT useridx, (CASE WHEN SUM(money_in) > 0 THEN ROUND(SUM(money_out)/SUM(money_in)*100) ELSE 0 END) AS winrate, SUM(playcount) AS playcount, ".
        		  		"  SUM(money_in) AS week_moneyin,  NOW() AS writedate ".
        		  		"  FROM ( ".
        		  		"     	SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out,  (playcount + h_playcount) AS playcount ".
        		  		"     	FROM tbl_user_playstat_daily ".
        		  		"     	WHERE today >= DATE_SUB('$current_date', INTERVAL 8 DAY) ".
        		  		"     	UNION ALL ".
        		  		"     	SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out,  (playcount + h_playcount) AS playcount ".
        		  		"     	FROM tbl_user_playstat_daily_ios ".
        		  		"     	WHERE today >= DATE_SUB('$current_date', INTERVAL 8 DAY) ".
        		  		"     	UNION ALL ".
        		  		"     	SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out,  (playcount + h_playcount) AS playcount ".
        		  		"     	FROM tbl_user_playstat_daily_android ".
        		  		"     	WHERE today >= DATE_SUB('$current_date', INTERVAL 8 DAY) ".
        		  		"     	UNION ALL ".
        		  		"     	SELECT useridx, (moneyin + h_moneyin) AS money_in, (moneyout + h_moneyout + jackpot) AS money_out,  (playcount + h_playcount) AS playcount ".
        		  		"     	FROM tbl_user_playstat_daily_amazon	".
        		  		"        WHERE today >= DATE_SUB('$current_date', INTERVAL 8 DAY) ".
        		  		" ) a ".
        		  		" GROUP BY useridx ON DUPLICATE KEY UPDATE winrate=VALUES(winrate), playcount=VALUES(playcount), week_moneyin=VALUES(week_moneyin), writedate=VALUES(writedate);";
		$db_other->execute($insertsql);
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
		
	$db_redshift->end();
	$db_other->end();
	
?>