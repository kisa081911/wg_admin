<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/pay_pattern_log") !== false)
		{
			$count++;
		}
	}

	ini_set("memory_limit", "-1");
	
	$issuccess = "1";
	
	if ($count > 1)
		exit();
	
	$db_slave_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();
 
	try
	{
		// 웹
		$today = date("Y-m-d");
		
		$sdate = date("Y-m-d", time() - 24 * 60 * 60);
		$edate = $today;		

		while($sdate < $edate)
		{
			$temp_date = date('Y-m-d', strtotime($sdate.' + 1 day'));
			
			$sql = "SELECT orderidx, useridx, productidx, usercoin, coin, gift_coin, ROUND(facebookcredit/10, 2) AS money, couponidx, special_discount, special_more, writedate ".
					"FROM tbl_product_order ".
					"WHERE status = 1 AND '$sdate 00:00:00' <= writedate AND writedate < '$temp_date 00:00:00'";
			$web_pay_list = $db_slave_main->gettotallist($sql);

			for($i=0; $i<sizeof($web_pay_list); $i++)
			{
				$platform = 0;
				$orderidx = $web_pay_list[$i]["orderidx"];
				$useridx = $web_pay_list[$i]["useridx"];
				$productidx = $web_pay_list[$i]["productidx"];
				$usercoin = $web_pay_list[$i]["usercoin"];
				$coin = $web_pay_list[$i]["coin"];
				$gift_coin = $web_pay_list[$i]["gift_coin"];
				$buy_coin = $coin + $gift_coin;
				$money = $web_pay_list[$i]["money"];
				$couponidx = $web_pay_list[$i]["couponidx"];
				$special_discount = $web_pay_list[$i]["special_discount"];
				$special_more = $web_pay_list[$i]["special_more"];
				$writedate = $web_pay_list[$i]["writedate"];
				
				if($useridx > 20000)
				{
					$sql = "SELECT adflag, createdate FROM tbl_user_ext WHERE useridx=$useridx";
					$user_info = $db_slave_main->getarray($sql);
					
					$adflag = $user_info["adflag"];
					$createdate = $user_info["createdate"];

					if($couponidx > 0)
					{
						$sql = "SELECT coupon_more FROM tbl_coupon WHERE useridx = $useridx AND couponidx = $couponidx";
						$coupon_extra = $db_main2->getvalue($sql);
						$special_more = $coupon_extra;
					}
					
					$special_more = ($special_more == "") ? 0 : $special_more;
					
					$sql = "SELECT IFNULL(MAX(writedate), '0000-00-00 00:00:00') ".
							"FROM ( ".
							"	SELECT MAX(writedate) AS writedate FROM tbl_product_order WHERE useridx = $useridx AND writedate < '$writedate' ".
 							"	UNION ALL ".
 							"	SELECT MAX(writedate) AS writedate FROM tbl_product_order_mobile WHERE useridx = $useridx AND writedate < '$writedate' ".
							") t1";
					$last_buydate = $db_slave_main->getvalue($sql);
					
					if($last_buydate == "0000-00-00 00:00:00")
						$last_buydate_tmp = $createdate;
					else
						$last_buydate_tmp = $last_buydate;

					$sql = "SELECT IFNULL(SUM(moneyin), 0) AS money_in, IFNULL(SUM(moneyout), 0) AS money_out ".
							"FROM ( ".
							"	SELECT * FROM `tbl_user_gamelog` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ". 
 							"	UNION ALL ".
 							"	SELECT * FROM `tbl_user_gamelog_ios` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ". 
 							"	UNION ALL ".
 							"	SELECT * FROM `tbl_user_gamelog_android` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ". 
 							"	UNION ALL ".
 							"	SELECT * FROM `tbl_user_gamelog_amazon` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ".
							") t1";
					$game_info = $db_other->getarray($sql);

					$money_in = $game_info["money_in"];
					$money_out = $game_info["money_out"];

					$sql = "SELECT IFNULL(slottype, 0) ".
							"FROM ( ".
							"	SELECT * FROM `tbl_user_gamelog` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ". 
 							"	UNION ALL ".
 							"	SELECT * FROM `tbl_user_gamelog_ios` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ". 
 							"	UNION ALL ".
 							"	SELECT * FROM `tbl_user_gamelog_android` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ". 
 							"	UNION ALL ".
 							"	SELECT * FROM `tbl_user_gamelog_amazon` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ". 
							") t1 ".
							"ORDER BY writedate DESC ".
							"LIMIT 1";
					
					$last_slottype = $db_other->getvalue($sql);
					
					$last_slottype = ($last_slottype == "") ? 0 : $last_slottype;

					$sql = "INSERT INTO tbl_user_pay_pattern_log(useridx, adflag, createdate, buy_platform, productidx, usercoin, buycoin, money, special_more, special_discount, last_buydate, purchasedate, money_in, money_out, last_slotidx) ".
							"VALUES($useridx, '$adflag', '$createdate', $platform, $productidx, $usercoin, $buy_coin, $money, $special_more, $special_discount, '$last_buydate', '$writedate', $money_in, $money_out, $last_slottype)";

					$db_other->execute($sql);
				}
			}
			
			$sdate = $temp_date;
		}

		// 모바일
 		$today = date("Y-m-d");

 		$sdate = date("Y-m-d", time() - 24 * 60 * 60);
 		$edate = $today;

		
 		while($sdate < $edate)
 		{
 			$temp_date = date('Y-m-d', strtotime($sdate.' + 1 day'));
				
 			$sql = "SELECT orderidx, os_type, useridx, productidx, usercoin, coin, gift_coin, money, 0 AS couponidx, 0 AS special_discount, special_more, writedate ".
 					"FROM tbl_product_order_mobile ".
 					"WHERE status = 1 AND '$sdate 00:00:00' <= writedate AND writedate < '$temp_date 00:00:00'";
 			$web_pay_list = $db_slave_main->gettotallist($sql);
		
 			for($i=0; $i<sizeof($web_pay_list); $i++)
 			{
 				$platform = $web_pay_list[$i]["os_type"];
 				$orderidx = $web_pay_list[$i]["orderidx"];
 				$useridx = $web_pay_list[$i]["useridx"];
 				$productidx = $web_pay_list[$i]["productidx"];
 				$usercoin = $web_pay_list[$i]["usercoin"];
				$coin = $web_pay_list[$i]["coin"];
				$gift_coin = $web_pay_list[$i]["gift_coin"];
				$buy_coin = $coin + $gift_coin;
				$money = $web_pay_list[$i]["money"];
				$couponidx = $web_pay_list[$i]["couponidx"];
				$special_discount = $web_pay_list[$i]["special_discount"];
				$special_extracoin = $web_pay_list[$i]["special_more"];
				$writedate = $web_pay_list[$i]["writedate"];
		
				if($useridx > 20000)
				{
					$sql = "SELECT adflag, createdate FROM tbl_user_ext WHERE useridx=$useridx";
					$user_info = $db_slave_main->getarray($sql);
						
					$adflag = $user_info["adflag"];
					$createdate = $user_info["createdate"];
		
					if($couponidx > 0)
					{						
						$sql = "SELECT coupon_more FROM tbl_coupon WHERE useridx = $useridx AND couponidx = $couponidx";
						$coupon_extra = $db_main2->getvalue($sql);
						$special_more = $coupon_extra;
					}
						
					$special_more = ($special_more == "") ? 0 : $special_more;
						
					$sql = "SELECT IFNULL(MAX(writedate), '0000-00-00 00:00:00') ".
							"FROM ( ".
							"	SELECT MAX(writedate) AS writedate FROM tbl_product_order WHERE useridx = $useridx AND writedate < '$writedate' ".
							"	UNION ALL ".
							"	SELECT MAX(writedate) AS writedate FROM tbl_product_order_mobile WHERE useridx = $useridx AND writedate < '$writedate' ".
							") t1";
					$last_buydate = $db_slave_main->getvalue($sql);					
					
					if($last_buydate == "0000-00-00 00:00:00")
						$last_buydate_tmp = $createdate;
					else
						$last_buydate_tmp = $last_buydate;
					
					$last_buydate_tmp = ($last_buydate_tmp == "") ? '0000-00-00 00:00:00' : $last_buydate_tmp;
		
					$sql = "SELECT IFNULL(SUM(moneyin), 0) AS money_in, IFNULL(SUM(moneyout), 0) AS money_out ".
							"FROM ( ".
							"	SELECT * FROM `tbl_user_gamelog` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ".
							"	UNION ALL ".
							"	SELECT * FROM `tbl_user_gamelog_ios` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ".
							"	UNION ALL ".
							"	SELECT * FROM `tbl_user_gamelog_android` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ".
							"	UNION ALL ".
							"	SELECT * FROM `tbl_user_gamelog_amazon` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ".
							") t1";
					$game_info = $db_other->getarray($sql);
		
					$money_in = $game_info["money_in"];
					$money_out = $game_info["money_out"];
		
					$sql = "SELECT IFNULL(slottype, 0) ".
							"FROM ( ".
							"	SELECT * FROM `tbl_user_gamelog` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ".
							"	UNION ALL ".
							"	SELECT * FROM `tbl_user_gamelog_ios` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ".
							"	UNION ALL ".
							"	SELECT * FROM `tbl_user_gamelog_android` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ".
							"	UNION ALL ".
							"	SELECT * FROM `tbl_user_gamelog_amazon` WHERE useridx = $useridx AND '$last_buydate_tmp' < writedate AND writedate < '$writedate' ".
							") t1 ".
							"ORDER BY writedate DESC ".
							"LIMIT 1";
						
					$last_slottype = $db_other->getvalue($sql);
						
					$last_slottype = ($last_slottype == "") ? 0 : $last_slottype;
		
					$sql = "INSERT INTO tbl_user_pay_pattern_log(useridx, adflag, createdate, buy_platform, productidx, usercoin, buycoin, money, special_more, special_discount, last_buydate, purchasedate, money_in, money_out, last_slotidx) ".
							"VALUES($useridx, '$adflag', '$createdate', $platform, $productidx, $usercoin, $buy_coin, $money, $special_extracoin, $special_discount, '$last_buydate', '$writedate', $money_in, $money_out, $last_slottype)";
		
					$db_other->execute($sql);
				}
			}
				
			$sdate = $temp_date;
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	$db_slave_main->end();
	$db_main2->end();
	$db_other->end();
?>
