<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/betting_status") !== false)
		{
			$count++;
		}
	}

	ini_set("memory_limit", "-1");
	
	$issuccess = "1";

	if ($count > 1)
		exit();
	
	ini_set("memory_limit", "-1");
	$current_date = date("Y-m-d");
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	try 
	{
		$sql = "SELECT COUNT(*) FROM tbl_vip_nopurchase_tmp";
		$row_count = $db_main2->getvalue($sql);
		
		while($row_count > 0)
		{
			$sql = "DELETE FROM tbl_vip_nopurchase_tmp LIMIT 5000;";
			$db_main2->execute($sql);
		
			$row_count -= 5000;
		}
		
		// VIP 유저 중에서 최근 28일 동안 결제하지 않은 유저
		$sql = "SELECT useridx, ROUND(SUM(money)) AS rev, max(vip_level) AS vip_level, max(coin) AS coin ".
				"FROM (	". 
  				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money, vip_level, t2.coin, writedate	".
  				"	FROM t5_product_order t1 JOIN t5_user t2 ON t1.useridx = t2.useridx	".
  				"	WHERE t1.useridx > 20000 AND vip_level >= 4 AND t2.coin >= 50000000 AND logindate >= DATEADD(day, -7, '$current_date'::date)	". 
				"	UNION ALL ".
  				"	SELECT t1.useridx, money, vip_level, t2.coin, writedate ". 
  				"	FROM t5_product_order_mobile t1 JOIN t5_user t2 ON t1.useridx = t2.useridx ".
  				"	WHERE t1.useridx > 20000 AND vip_level >= 4 AND t2.coin >= 50000000 AND logindate >= DATEADD(day, -7, '$current_date'::date) ".
				") t3 group by useridx	".
				"HAVING MAX(writedate) < DATEADD(day, -28, '$current_date'::date)";		
		
		$vip_nopay_list = $db_redshift->gettotallist($sql);
		
		$sql = "";
		$insertcount = 0;
			
		for($i=0; $i<sizeof($vip_nopay_list); $i++)
		{
			$useridx = $vip_nopay_list[$i]["useridx"];
			$rev = $vip_nopay_list[$i]["rev"];
			$vip_level = $vip_nopay_list[$i]["vip_level"];
			$coin = $vip_nopay_list[$i]["coin"];
		
			if ($insertcount == 0)
			{
				$sql = "INSERT INTO tbl_vip_nopurchase_tmp(useridx, rev, vip_level, coin, writedate) VALUES($useridx, $rev, $vip_level, $coin, NOW())";

				$insertcount++;
			}
			else if ($insertcount < 500)
			{
				$sql .= ", ($useridx, $rev, $vip_level, $coin, NOW())";
				$insertcount++;
			}
			else
			{
				$sql .= ", ($useridx, $rev, $vip_level, $coin, NOW());";
				$insertcount = 0;

				$db_main2->execute($sql);

				$sql = "";
			}
		}
		
		if($sql != "")
		{
			$db_main2->execute($sql);
		}
		
		// Redshift 동기화 시간에 따른 결제자 체크
		$sql = "SELECT useridx FROM tbl_vip_nopurchase_tmp";
		$vip_nopay_tmp_list = $db_main2->gettotallist($sql);
		
		$delete_useridx = "";
		
		for($i=0; $i<sizeof($vip_nopay_tmp_list); $i++)
		{
			$tmp_useridx = $vip_nopay_tmp_list[$i]["useridx"];
							
			$sql = "SELECT ".
					"(SELECT COUNT(*) FROM tbl_product_order WHERE useridx = $tmp_useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 4 WEEK) LIMIT 1) + ".
					"(SELECT COUNT(*) FROM tbl_product_order_mobile WHERE useridx = $tmp_useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 4 WEEK) LIMIT 1) AS purchase_count";		
			$pay_count = $db_main->getvalue($sql);
			
			if($pay_count > 0)
			{
				if($delete_useridx == "")
					$delete_useridx .= "$tmp_useridx";
				else
					$delete_useridx .= ", $tmp_useridx";
			}
		}
		
		if($delete_useridx != "")
		{
			$sql = "DELETE FROM tbl_vip_nopurchase_tmp WHERE useridx IN ($delete_useridx);";
			$db_main2->execute($sql);
		}
		// End
		
		$sql = "UPDATE tbl_vip_nopurchase ".
				"SET is_buy = 1, writedate = NOW() ".
				"WHERE NOT EXISTS (SELECT * FROM tbl_vip_nopurchase_tmp WHERE useridx=tbl_vip_nopurchase.useridx)";
		$db_main2->execute($sql);
		
		$sql = "INSERT INTO tbl_vip_nopurchase(useridx, rev, vip_level, is_buy, writedate) ".
				"SELECT useridx, rev, vip_level, 0 AS is_buy, writedate ".
				"FROM tbl_vip_nopurchase_tmp ".
				"ON DUPLICATE KEY UPDATE rev = VALUES(rev), is_buy = VALUES(is_buy), writedate = VALUES(writedate) ";
		$db_main2->execute($sql);
		
		$sql = "INSERT INTO tbl_vip_nopurchase_history(useridx, rev, vip_level, coin, writedate) ".
				"SELECT useridx, rev, vip_level, coin, NOW() ".
				"FROM tbl_vip_nopurchase_tmp";
		$db_main2->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$sql = "SELECT COUNT(*) FROM tbl_vip_purchase_tmp";
		$row_count = $db_main2->getvalue($sql);
	
		while($row_count > 0)
		{
			$sql = "DELETE FROM tbl_vip_purchase_tmp LIMIT 5000;";
			$db_main2->execute($sql);
	
			$row_count -= 5000;
		}
	
		// VIP 유저 중에서 최근 28일 동안 결제 한 유저		
		$sql = "SELECT useridx, ROUND(SUM(money)) AS rev, MAX(coin) AS coin ". 
				"FROM (	".						
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money, t2.coin, writedate	". 
				"	FROM tbl_product_order t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx	". 
				"	WHERE t1.useridx > 20000 AND status = 1 AND logindate >= DATE_SUB(NOW(), INTERVAL 7 DAY)	".
				"	UNION ALL	". 
				"	SELECT t1.useridx, money, t2.coin, writedate	". 
				"	FROM tbl_product_order_mobile t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx	". 
				"	WHERE t1.useridx > 20000 AND status = 1 AND logindate >= DATE_SUB(NOW(), INTERVAL 7 DAY)	".
				") t3	". 
				"GROUP BY useridx	". 
				"HAVING MAX(writedate) >= DATE_SUB(NOW(), INTERVAL 28 DAY)";	
		$vip_nopay_list = $db_main->gettotallist($sql);
	
		$sql = "";
		$insertcount = 0;
			
		for($i=0; $i<sizeof($vip_nopay_list); $i++)
		{
			$useridx = $vip_nopay_list[$i]["useridx"];
			$coin = $vip_nopay_list[$i]["coin"];
	
			if ($insertcount == 0)
			{
				$sql = "INSERT INTO tbl_vip_purchase_tmp(useridx, coin, writedate) VALUES($useridx, $coin, NOW())";
	
				$insertcount++;
			}
			else if ($insertcount < 500)
			{
				$sql .= ", ($useridx, $coin, NOW())";
				$insertcount++;
			}
			else
			{
				$sql .= ", ($useridx, $coin, NOW());";
				$insertcount = 0;
	
				$db_main2->execute($sql);
	
				$sql = "";
			}
		}
	
		if($sql != "")
		{
			$db_main2->execute($sql);
		}
			
		//결제 가중
		$sql = "SELECT useridx FROM tbl_vip_purchase_tmp";
		$vip_nopay_tmp_list = $db_main2->gettotallist($sql);
	
		$update_sql = "";
		$updatecount = 0;

		$today = date("Y-m-d");
	
		for($i=0; $i<sizeof($vip_nopay_tmp_list); $i++)
		{
			$tmp_useridx = $vip_nopay_tmp_list[$i]["useridx"];
				
			$sql = "SELECT IFNULL(ROUND(SUM(usercoin * money)/SUM(money)),0) AS coin_p ".
					"FROM	".
					"(	".
					"	SELECT usercoin, ROUND(facebookcredit/10, 2) AS money, writedate FROM tbl_product_order WHERE writedate >= DATE_SUB(NOW(), INTERVAL 4 WEEK) AND status = 1 AND useridx = $tmp_useridx	".
					"	UNION ALL	".
					"	SELECT usercoin, money, writedate FROM tbl_product_order_mobile WHERE writedate >= DATE_SUB(NOW(), INTERVAL 4 WEEK) AND status = 1 AND useridx = $tmp_useridx	".
					") t1";
			$coin_p = $db_main->getvalue($sql);
	
			if ($updatecount == 0)
			{
				$update_sql = "INSERT INTO tbl_vip_purchase_tmp(useridx, coin_p) VALUES($tmp_useridx, $coin_p)";
	
				$updatecount++;
			}
			else if ($updatecount < 50)
			{
				$update_sql .= ", ($tmp_useridx, $coin_p)";
				$updatecount++;
			}
			else
			{
				$update_sql .= ", ($tmp_useridx, $coin_p)";
				$update_sql .= " ON DUPLICATE KEY UPDATE coin_p=VALUES(coin_p);";
				$updatecount = 0;
	
				$db_main2->execute($update_sql);
	
				$update_sql = "";
			}
		}			
	
		if($update_sql != "")
		{
			$update_sql .= " ON DUPLICATE KEY UPDATE coin_p=VALUES(coin_p);";
			$db_main2->execute($update_sql);
		}
		
		// 결제자 체크
		for($j=0; $j<10; $j++)
		{
			$sql = "SELECT useridx FROM tbl_vip_purchase_tmp WHERE writedate >= '$today 00:00:00' AND useridx % 10 = $j";
			$status_tmp_list = $db_main2->gettotallist($sql);
		
			for($i=0; $i<sizeof($status_tmp_list); $i++)
			{
				$tmp_useridx = $status_tmp_list[$i]["useridx"];
			
				$sql = "SELECT ".
						"(SELECT COUNT(*) FROM tbl_product_order WHERE useridx = $tmp_useridx AND STATUS = 1 AND facebookcredit >= 50 AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY) LIMIT 1) + ".
						"(SELECT COUNT(*) FROM tbl_product_order_mobile WHERE useridx = $tmp_useridx AND STATUS = 1 ANd money >= 5 AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY) LIMIT 1) AS purchase_count";
				$pay_count = $db_main->getvalue($sql);
			
				if($pay_count > 0)
				{
					$sql = "DELETE FROM tbl_vip_purchase_tmp WHERE useridx = $tmp_useridx;";
					$db_main2->execute($sql);
				}
			}
		}
	
		$sql = "UPDATE tbl_vip_purchase ".
				"SET is_buy = 1, writedate = NOW() ".
				"WHERE NOT EXISTS (SELECT * FROM tbl_vip_purchase_tmp WHERE useridx=tbl_vip_purchase.useridx)";
		$db_main2->execute($sql);
	
		$sql = "INSERT INTO tbl_vip_purchase(useridx, coin_p, is_buy, writedate) ".
				"SELECT useridx, coin_p, 0 AS is_buy, writedate ".
				"FROM tbl_vip_purchase_tmp ".
				"ON DUPLICATE KEY UPDATE coin_p = VALUES(coin_p), is_buy = VALUES(is_buy), writedate = VALUES(writedate) ";
		$db_main2->execute($sql);
	
		$sql = "INSERT INTO tbl_vip_purchase_history(useridx, coin_p, coin, writedate) ".
				"SELECT useridx, coin_p, coin, NOW() ".
				"FROM tbl_vip_purchase_tmp";
		$db_main2->execute($sql);
	}
	catch(Exception $e)
	{
			write_log($e->getMessage());
	}
		
	try
	{
		$today = date("Y-m-d", time());
		$yesterday = date("Y-m-d", time() - 60 * 60 * 24);
	
		$sql = "SELECT COUNT(*) FROM tbl_user_betting_status_tmp";
		$row_count = $db_main2->getvalue($sql);

		while($row_count > 0)
		{
			$sql = "DELETE FROM tbl_user_betting_status_tmp LIMIT 5000;";
			$db_main2->execute($sql);
	
			$row_count -= 5000;
		}
	
		// 예상 소진일 12일 이상인 유저, 3일 이내 $5이상 결제 하지 않은 유저, 보유코인 $19 상품 코인(950만코인)이상 유저
		$sql = "select t3.useridx AS useridx, coin, ROUND((sum_money_in)/sum_playcount) AS avg_bet, ROUND(coin/((avg_money_in)*0.03)) AS expected_days, sum_playcount	".
				"from	".
				"(	".   
     			"	select useridx, avg(money_in) as avg_money_in,  sum(money_in) as sum_money_in, sum(playcount) as sum_playcount	".
        		"	from	".
        		"	(	".
          		"		select today, useridx, sum(moneyin + h_moneyin) as money_in, sum(playcount+h_playcount) as playcount	".
          		"		from	".
          		"		(	".
            	"			select * from t5_user_playstat_daily WHERE today > DATEADD(day,-8, '$current_date'::date) AND playcount >= 50	".
            	"			union all	".
            	"			select * from t5_user_playstat_daily_ios WHERE today > DATEADD(day,-8, '$current_date'::date) AND playcount >= 50	".
            	"			union all	".
            	"			select * from t5_user_playstat_daily_android WHERE today > DATEADD(day,-8, '$current_date'::date) AND playcount >= 50	".
            	"			union all	".
            	"			select * from t5_user_playstat_daily_amazon WHERE today > DATEADD(day,-8, '$current_date'::date) AND playcount >= 50	".
          		"		) t1 where  (	".
              	"			NOT EXISTS	".
              	"			(	".
				"				SELECT useridx FROM t5_product_order WHERE writedate > DATEADD(hour,-72, '$current_date'::date) AND facebookcredit >= 50 AND status = 1 AND useridx = t1.useridx	".
          		"			)	". 
          		"			AND	".
          		"			NOT EXISTS	".
          		"			(	".
              	"				SELECT useridx FROM t5_product_order_mobile WHERE writedate > DATEADD(hour,-72, '$current_date'::date) AND money >= 5 AND status = 1 AND useridx = t1.useridx	".
          		"			)	".
          		"		) group by today, useridx	".
         		"	) t2 group by useridx	".
				") t3 join t5_user t4 on t3.useridx = t4.useridx where coin > 9500000 AND date_diff('day', createdate, '$current_date'::date) > 3  AND coin/(avg_money_in*0.03) >= 12;";
		$use_list = $db_redshift->gettotallist($sql);
		
		$sql = "";
		$insertcount = 0;
			
		for($i=0; $i<sizeof($use_list); $i++)
		{
			$useridx = $use_list[$i]["useridx"];
			$currentcoin = $use_list[$i]["coin"];
			$avg_bet = $use_list[$i]["avg_bet"];
			$expected_days = $use_list[$i]["expected_days"];
			$playcount = $use_list[$i]["sum_playcount"];
		
		
			if ($insertcount == 0)
			{
				$sql = "INSERT INTO tbl_user_betting_status_tmp(useridx, platform, currentcoin, avg_bet, expected_days, sum_playcount, is_buy, today, writedate) VALUES($useridx, 0, $currentcoin, $avg_bet, $expected_days, $playcount, 0, '$today', NOW())";
		
				$insertcount++;
			}
			else if ($insertcount < 500)
			{
				$sql .= ", ($useridx, 0, $currentcoin, $avg_bet, $expected_days, $playcount, 0, '$today', NOW())";
				$insertcount++;
			}
			else
			{
				$sql .= ", ($useridx, 0, $currentcoin, $avg_bet, $expected_days, $playcount, 0, '$today', NOW());";
				$insertcount = 0;
		
				$db_main2->execute($sql);
		
				$sql = "";
			}
		}
		
		if($sql != "")
		{
			$db_main2->execute($sql);
		}
		
		// Redshift 동기화 시간에 따른 결제자 체크
		for($j=0; $j<10; $j++)
		{
			$sql = "SELECT useridx FROM tbl_user_betting_status_tmp WHERE today = '$today' AND useridx % 10 = $j";
			$status_tmp_list = $db_main2->gettotallist($sql);
		
			for($i=0; $i<sizeof($status_tmp_list); $i++)
			{
				$tmp_useridx = $status_tmp_list[$i]["useridx"];
				
				$sql = "SELECT ".
						"(SELECT COUNT(*) FROM tbl_product_order WHERE useridx = $tmp_useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY) LIMIT 1) + ".
						"(SELECT COUNT(*) FROM tbl_product_order_mobile WHERE useridx = $tmp_useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY) LIMIT 1) AS purchase_count";
						
				$pay_count = $db_main->getvalue($sql);
				
				if($pay_count > 0)
				{
					$sql = "DELETE FROM tbl_user_betting_status_tmp WHERE useridx = $tmp_useridx;";
					$db_main2->execute($sql);
				}
			}
		}
	
		$sql = "UPDATE tbl_user_betting_status SET is_buy = 1, writedate = NOW() ".
				"WHERE is_buy = 0 AND NOT EXISTS (SELECT * FROM tbl_user_betting_status_tmp WHERE useridx = tbl_user_betting_status.useridx)";
		$db_main2->execute($sql);
	
		$sql = "INSERT INTO tbl_user_betting_status(useridx, platform, currentcoin, avg_bet, expected_days, sum_playcount, is_buy, writedate, register_date) ".
				"SELECT useridx, platform, currentcoin, avg_bet, expected_days, sum_playcount, is_buy, writedate, today ".
				"FROM tbl_user_betting_status_tmp ".
				"	ON DUPLICATE KEY UPDATE platform = VALUES(platform), currentcoin = VALUES(currentcoin), avg_bet = VALUES(avg_bet), expected_days = VALUES(expected_days), sum_playcount = VALUES(sum_playcount), is_buy = VALUES(is_buy), writedate = VALUES(writedate), register_date = VALUES(register_date)";
		$db_main2->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	$db_main->end();
	$db_main2->end();
	$db_other->end();
	$db_redshift->end();
?>
