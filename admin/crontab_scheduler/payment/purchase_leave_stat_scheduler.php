<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/purchase_leave_stat_scheduler") !== false)
		{
			$count++;
		}
	}	
	
	if ($count > 1)
		exit();
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();	
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	$current_date = date("Y-m-d");
	
	$db_main->execute("SET wait_timeout=72000");
	$db_main2->execute("SET wait_timeout=72000");
	$db_other->execute("SET wait_timeout=72000");
	
	write_log("Purchase Leave Stat Scheduler Start");
	
	$product_arr = array(
			array("key"=>"08_009_30ac59d", "money"=>"9"),
			array("key"=>"08_019_3580ca6", "money"=>"19"),
			array("key"=>"08_039_40cf647", "money"=>"39"),
			array("key"=>"08_019_3033b91", "money"=>"19"),
			array("key"=>"08_039_35ef324", "money"=>"39"),
			array("key"=>"08_059_4065dd8", "money"=>"59"),
			array("key"=>"08_039_309ab19", "money"=>"39"),
			array("key"=>"08_059_3592dbc", "money"=>"59"),
			array("key"=>"08_099_40eb5f6", "money"=>"99"),
			array("key"=>"08_059_30973dc", "money"=>"59"),
			array("key"=>"08_099_3532d70", "money"=>"99"),
			array("key"=>"08_199_401d689", "money"=>"199"),
			array("key"=>"08_099_30b617b", "money"=>"99"),
			array("key"=>"08_199_35065fd", "money"=>"199"),
			array("key"=>"08_299_40698bf", "money"=>"299"),
			array("key"=>"08_199_3082b3d", "money"=>"199"),
			array("key"=>"08_299_35cc54e", "money"=>"299"),
			array("key"=>"08_499_40e9d9d", "money"=>"499"),
			array("key"=>"09_009_20a814b", "money"=>"9"),
			array("key"=>"09_019_255d0e8", "money"=>"19"),
			array("key"=>"09_039_30a5136", "money"=>"39"),
			array("key"=>"09_019_2069520", "money"=>"19"),
			array("key"=>"09_039_250c6fc", "money"=>"39"),
			array("key"=>"09_059_30bacb4", "money"=>"59"),
			array("key"=>"09_039_2098ca8", "money"=>"39"),
			array("key"=>"09_059_25ed757", "money"=>"59"),
			array("key"=>"09_099_30195d1", "money"=>"99"),
			array("key"=>"09_059_20e1e49", "money"=>"59"),
			array("key"=>"09_099_25c8ae9", "money"=>"99"),
			array("key"=>"09_199_30c32a1", "money"=>"199"),
			array("key"=>"09_099_209dbb3", "money"=>"99"),
			array("key"=>"09_199_25d1a8c", "money"=>"199"),
			array("key"=>"09_299_3046160", "money"=>"299"),
			array("key"=>"09_199_206922a", "money"=>"199"),
			array("key"=>"09_299_25b8e7a", "money"=>"299"),
			array("key"=>"09_499_3079eb8", "money"=>"499")
	);
	
	$morechip_arr = array(
			array("key"=>"08_009_30ac59d", "more"=>"30"),
			array("key"=>"08_019_3580ca6", "more"=>"35"),
			array("key"=>"08_039_40cf647", "more"=>"40"),
			array("key"=>"08_019_3033b91", "more"=>"30"),
			array("key"=>"08_039_35ef324", "more"=>"35"),
			array("key"=>"08_059_4065dd8", "more"=>"40"),
			array("key"=>"08_039_309ab19", "more"=>"30"),
			array("key"=>"08_059_3592dbc", "more"=>"35"),
			array("key"=>"08_099_40eb5f6", "more"=>"40"),
			array("key"=>"08_059_30973dc", "more"=>"30"),
			array("key"=>"08_099_3532d70", "more"=>"35"),
			array("key"=>"08_199_401d689", "more"=>"40"),
			array("key"=>"08_099_30b617b", "more"=>"30"),
			array("key"=>"08_199_35065fd", "more"=>"35"),
			array("key"=>"08_299_40698bf", "more"=>"40"),
			array("key"=>"08_199_3082b3d", "more"=>"30"),
			array("key"=>"08_299_35cc54e", "more"=>"35"),
			array("key"=>"08_499_40e9d9d", "more"=>"40"),			
			array("key"=>"09_009_20a814b", "more"=>"20"),
			array("key"=>"09_019_255d0e8", "more"=>"25"),
			array("key"=>"09_039_30a5136", "more"=>"30"),
			array("key"=>"09_019_2069520", "more"=>"20"),
			array("key"=>"09_039_250c6fc", "more"=>"25"),
			array("key"=>"09_059_30bacb4", "more"=>"30"),
			array("key"=>"09_039_2098ca8", "more"=>"20"),
			array("key"=>"09_059_25ed757", "more"=>"25"),
			array("key"=>"09_099_30195d1", "more"=>"30"),
			array("key"=>"09_059_20e1e49", "more"=>"20"),
			array("key"=>"09_099_25c8ae9", "more"=>"25"),
			array("key"=>"09_199_30c32a1", "more"=>"30"),
			array("key"=>"09_099_209dbb3", "more"=>"20"),
			array("key"=>"09_199_25d1a8c", "more"=>"25"),
			array("key"=>"09_299_3046160", "more"=>"30"),
			array("key"=>"09_199_206922a", "more"=>"20"),
			array("key"=>"09_299_25b8e7a", "more"=>"25"),
			array("key"=>"09_499_3079eb8", "more"=>"30")
	);
	
	$is_seasonoffer = -1;
	
	$sql = "SELECT seasonsaleidx FROM `tbl_season_sale_offer_setting` WHERE startdate<=NOW() AND enddate>=NOW() AND STATUS=1 ORDER BY seasonsaleidx DESC LIMIT 1";
	$is_seasonoffer = ($db_main2->getvalue($sql) == "") ? "0" : "1";
	
	$today = date("Y-m-d");

	$sql = "select ".
    		"	t3.useridx, recent_avg_product_coin, avg_product_coin, current_coin, recent_avg_buy_money, recent_avg_rebuyday, recent_avg_buy_more, recent_buy_count, ".
    		"	avg_buy_money, avg_rebuyday, avg_buy_more, buy_count, datediff(day, recent_writedate, '$current_date'::date) as elapsed_purchase_date, elapsed_loginday, mobile_version, platform, ".
    		"	(select 0 as platform from t5_product_order where useridx = t3.useridx and status = 1 and writedate =  t4.recent_writedate) as web_category, ".
    		"	(select os_type from t5_product_order_mobile where useridx = t3.useridx and status = 1 and writedate =  t4.recent_writedate) as mobile_category, recent_writedate as recent_writetime  ".
			"from ( ".
    		"	select t1.useridx, coin as current_coin, recent_avg_product_coin, recent_avg_buy_money, recent_avg_rebuyday, recent_buy_count, recent_avg_buy_more, avg_product_coin, avg_buy_money, avg_rebuyday, buy_count, avg_buy_more, elapsed_loginday, mobile_version, platform ".
    		"	from ( ".
            "		select useridx, coin, datediff(day, logindate, '$current_date'::date) as elapsed_loginday, mobile_version, platform from t5_user where useridx > 20000 and logindate > dateadd(w, -2, '$current_date'::date) ".
    		"	) t1 join t5_user_purchase_gamelog_type t2 on t1.useridx = t2.useridx ".
    		"	where t2.type = 2 and writedate between '$today 00:00:00' and '$today 23:59:59' ".
			") t3 join ( ".
    		"	select useridx, max(recent_writedate) as recent_writedate ". 
    		"	from ( ".
        	"		select useridx, max(writedate) as recent_writedate, 0 as os_type from t5_product_order where useridx > 20000 and status = 1 group by useridx ".
        	"		union all ".
        	"		select useridx, max(writedate) as recent_writedate, os_type from t5_product_order_mobile where useridx > 20000 and status = 1 group by useridx, os_type ". 
    		"	) sub ".
    		"	group by useridx ".
			") t4 on t3.useridx = t4.useridx ".
			"group by t3.useridx, recent_avg_product_coin, current_coin, recent_avg_buy_money, recent_avg_rebuyday, recent_avg_buy_more, recent_buy_count, avg_product_coin, avg_buy_money, avg_rebuyday, avg_buy_more, buy_count, t4.recent_writedate, datediff(day, recent_writedate, '$current_date'::date), elapsed_loginday, mobile_version, platform";
	
	$data_list = $db_redshift->gettotallist($sql);
	
	$insert_cnt = 0;
	$insert_sql = "";
	
	for($i=0; $i<sizeof($data_list); $i++)
	{
		$useridx = $data_list[$i]["useridx"];
		$recent_avg_product_coin = $data_list[$i]["recent_avg_product_coin"];
		$recent_avg_buy_money = $data_list[$i]["recent_avg_buy_money"];
		$recent_avg_rebuyday = $data_list[$i]["recent_avg_rebuyday"];
		$recent_avg_buy_more = $data_list[$i]["recent_avg_buy_more"];
		$recent_buy_count = $data_list[$i]["recent_buy_count"];
		$avg_product_coin = $data_list[$i]["avg_product_coin"];
		$current_coin = $data_list[$i]["current_coin"];
		$avg_buy_money = $data_list[$i]["avg_buy_money"];
		$avg_rebuyday = $data_list[$i]["avg_rebuyday"];
		$avg_buy_more = $data_list[$i]["avg_buy_more"];
		$buy_count = $data_list[$i]["buy_count"];
		$elapsed_purchase_date = $data_list[$i]["elapsed_purchase_date"];
		$elapsed_loginday = $data_list[$i]["elapsed_loginday"];
		$web_category = $data_list[$i]["web_category"];
		$mobile_category = $data_list[$i]["mobile_category"];
		$mobile_version = $data_list[$i]["mobile_version"];
		$platform = $data_list[$i]["platform"];
		$recent_writetime = $data_list[$i]["recent_writetime"];
		
		$basic_coupon_remainday = -1;
		$promotion_coupon2_remainday = -1;
		$promotion_coupon3_remainday = -1;		
		
		$coupon_1 = -1;
		$coupon_2 = -1;
		$coupon_3 = -1;

		$season_product_1 = -1;
		$season_product_2 = -1;
		$season_product_3 = -1;
		
		$season_product_more_1 = -1;
		$season_product_more_2 = -1;
		$season_product_more_3 = -1;
		
		$mobile_season_product_1 = -1;
		$mobile_season_product_2 = -1;
		$mobile_season_product_3 = -1;
		
		$mobile_season_product_more_1 = -1;
		$mobile_season_product_more_2 = -1;
		$mobile_season_product_more_3 = -1;
		
		$whale_product = -1;
		
		$whale_product_more = -1;
		
		$threshold_product_1 = -1;
		$threshold_product_2 = -1;
		
		$threshold_product_more_1 = -1;
		$threshold_product_more_2 = -1;
		
		$piggypot_stat = -1;
		
		$lastest_login_platform = -1;

		// Web, Mobile Coupon 재발급까지 남은 일자(쿠폰 동일하게 사용)
		$sql = "SELECT DATEDIFF(IF(STATUS = 0, expiredate, usedate), NOW()) AS coupon_remain FROM tbl_coupon WHERE useridx = $useridx AND category = 0 ORDER BY writedate DESC LIMIT 1";
		$basic_coupon_remainday = $db_main2->getvalue($sql);
		
		if($basic_coupon_remainday == "" || $basic_coupon_remainday < 0)
			$basic_coupon_remainday = -1;
		
		$sql = "SELECT DATEDIFF(expiredate, NOW()) AS coupon_remain FROM tbl_coupon WHERE useridx = $useridx AND category = 2 ORDER BY writedate DESC LIMIT 1";
		$promotion_coupon2_remainday = $db_main2->getvalue($sql);
		
		if($promotion_coupon2_remainday == "" || $basic_coupon_remainday < 0)
			$promotion_coupon2_remainday = -1;
		
		$sql = "SELECT DATEDIFF(expiredate, NOW()) AS coupon_remain FROM tbl_coupon WHERE useridx = $useridx AND category = 3 ORDER BY writedate DESC LIMIT 1";
		$promotion_coupon3_remainday = $db_main2->getvalue($sql);
		
		if($promotion_coupon3_remainday == "" || $basic_coupon_remainday < 0)
			$promotion_coupon3_remainday = -1;
		
		// Web, Mobile 보유 쿠폰
		$sql = "SELECT coupon_more FROM tbl_coupon WHERE useridx = $useridx AND status = 0";
		$web_coupon_list = $db_main2->gettotallist($sql);
		
		if($web_coupon_list != "")
		{
			if($web_coupon_list[0]["coupon_more"] != "")
				$coupon_1 = $web_coupon_list[0]["coupon_more"];
			
			if($web_coupon_list[1]["coupon_more"] != "")
				$coupon_2 = $web_coupon_list[1]["coupon_more"];
				
			if($web_coupon_list[2]["coupon_more"] != "")
				$coupon_3 = $web_coupon_list[2]["coupon_more"];
		}		
		
		// 시즌오퍼 상품 및 More (tbl_user_season_offer)
		if($is_seasonoffer == 1)
		{			
			$sql = "SELECT productkey1, productkey2, productkey3 FROM tbl_season_offer_user WHERE useridx = $useridx";
			$web_season_offer_data = $db_main2->getarray($sql);
			
			$productkey1 = $web_season_offer_data["productkey1"];
			$productkey2= $web_season_offer_data["productkey2"];
			$productkey3 = $web_season_offer_data["productkey3"];

			for($k=0; $k<sizeof($product_arr); $k++)
			{
				if($product_arr[$k]["key"] == $productkey1)
					$season_product_1 = $product_arr[$k]["money"];
			}
				
			for($k=0; $k<sizeof($product_arr); $k++)
			{
				if($product_arr[$k]["key"] == $productkey2)
					$season_product_2 = $product_arr[$k]["money"];
			}
				
			for($k=0; $k<sizeof($product_arr); $k++)
			{
				if($product_arr[$k]["key"] == $productkey3)
					$season_product_3 = $product_arr[$k]["money"];
			}
				
			for($k=0; $k<sizeof($morechip_arr); $k++)
			{
				if($morechip_arr[$k]["key"] == $productkey1)
					$season_product_more_1 = $morechip_arr[$k]["more"];
			}
		
			for($k=0; $k<sizeof($morechip_arr); $k++)
			{
				if($morechip_arr[$k]["key"] == $productkey2)
					$season_product_more_2 = $morechip_arr[$k]["more"];
			}
		
			for($k=0; $k<sizeof($morechip_arr); $k++)
			{
				if($morechip_arr[$k]["key"] == $productkey3)
					$season_product_more_3 = $morechip_arr[$k]["more"];
			}
			
			if($season_product_1 == "")
				$season_product_1 = -1;
			
			if($season_product_2 == "")
				$season_product_2 = -1;
			
			if($season_product_3 == "")
				$season_product_3 = -1;
			
			if($season_product_more_1 == "")
				$season_product_more_1 = -1;
			
			if($season_product_more_2 == "")
				$season_product_more_2 = -1;
			
			if($season_product_more_3 == "")
				$season_product_more_3 = -1;			
			
			// Mobile 사용자만 조회
			if($platform > 0)
			{
				$sql = "SELECT season_group FROM tbl_season_offer_user_mobile WHERE useridx = $useridx ORDER BY writedate DESC LIMIT 1";
				$mobile_season_group = $db_main2->getvalue($sql);

				if($mobile_season_group == 1)
					$start_limit = 0;
				else if($mobile_season_group == 2)
					$start_limit = 3;
				else if($mobile_season_group == 3)
					$start_limit = 6;
				else if($mobile_season_group == 4)
					$start_limit = 9;
					
	
				$sql = "SELECT money, special_more FROM tbl_product_mobile WHERE os_type = $platform AND category = 5 LIMIT $start_limit, 3;";
				$mobile_season_data =  $db_main->gettotallist($sql);
				
				$mobile_season_product_1 = $mobile_season_data[0]["money"];
				$mobile_season_product_2 = $mobile_season_data[1]["money"];
				$mobile_season_product_3 = $mobile_season_data[2]["money"];
				
				$mobile_season_product_more_1 = $mobile_season_data[0]["special_more"];
				$mobile_season_product_more_2 = $mobile_season_data[1]["special_more"];
				$mobile_season_product_more_3  = $mobile_season_data[2]["special_more"];
			}
		}
		
		// Whale 이탈자
		$sql = "SELECT target_credit FROM tbl_user_special_offer WHERE useridx = $useridx AND buyable = 1 AND type = 2";
		$whale_user_offer_exist = $db_main->getvalue($sql);
		
		if(target_credit > 0)
		{			
			$whale_product = target_credit;		

			if($platform == 1 && target_credit >= 199)
				$whale_product = 99;
			
			if($platform == 2 && target_credit >= 299)
				$whale_product = 199;
			
			if($platform == 3 && target_credit >= 299)
				$whale_product = 199;
			
			$whale_product_more = 50;
		}
		
		// Threshold(mode 19 2차)
		$sql = "SELECT target_credit FROM tbl_user_special_offer WHERE useridx = $useridx AND buyable = 1 AND type = 1";
		$threshold_credit = $db_main->getvalue($sql);
		
		if($threshold_credit > 0)
		{
			if($threshold_credit == 19)
			{
				$threshold_product_1 = 9;
				$threshold_product_2 = 19;
			}
			else if($threshold_credit == 39)
			{
				$threshold_product_1 = 19;
				$threshold_product_2 = 39;
			}
			else if($threshold_credit == 59)
			{
				$threshold_product_1 = 39;
				$threshold_product_2 = 59;
			}
			else if($threshold_credit == 99)
			{
				$threshold_product_1 = 59;
				$threshold_product_2 = 99;
			}
			else if($threshold_credit == 199)
			{
				$threshold_product_1 = 99;
				$threshold_product_2 = 199;
				
				if($plaform == 1)
				{
					$threshold_product_1 = 59;
					$threshold_product_2 = 99;
				}
			}
			else if($threshold_credit == 299)
			{
				$threshold_product_1 = 199;
				$threshold_product_2 = 299;
				
				if($plaform == 1)
				{
					$threshold_product_1 = 59;
					$threshold_product_2 = 99;
				}
				
				if($plaform == 2 || $plaform == 3)
				{
					$threshold_product_1 = 99;
					$threshold_product_2 = 199;
				}
			}
			else if($threshold_credit == 499)
			{
				$threshold_product_1 = 299;
				$threshold_product_2 = 499;
				
				if($plaform == 1)
				{
					$threshold_product_1 = 59;
					$threshold_product_2 = 99;
				}
				
				if($plaform == 2 || $plaform == 3)
				{
					$threshold_product_1 = 99;
					$threshold_product_2 = 199;
				}
			}
			
			$threshold_product_more_1 = 25;
			$threshold_product_more_2 = 30;
		}
		
		// 피기팟 현황
		$sql = "SELECT ROUND(current_spin/1000 * 100, 0) AS stat, DATEDIFF(NOW(), collectdate) AS diffdate FROM tbl_user_piggy_pot_".($useridx%10)." WHERE useridx = $useridx";
		$piggypot_data = $db_main2->getarray($sql);
		
		if($piggypot_data != "")
		{
			$stat = $piggypot_data["stat"];
			$cooltime = $piggypot_data["diffdate"];
			
			if($cooltime > 0)
				$piggypot_stat = $stat;
		}
		
		// 마지막 로그인 플랫폼
		if($platform == 0 && $mobile_version == "")
		{
			$lastest_login_platform = 0;
		}
		else
		{
			$sql = "SELECT platform ".
					"FROM ( ".
					"	SELECT 0 AS platform, web AS logindate FROM tbl_user_platform_logindate WHERE useridx = $useridx ".
					"	UNION ALL ".
					"	SELECT 1 AS platform, ios AS logindate FROM tbl_user_platform_logindate WHERE useridx = $useridx ".
					"	UNION ALL ".
					"	SELECT 2 AS platform, android AS logindate FROM tbl_user_platform_logindate WHERE useridx = $useridx ".
					"	UNION ALL ".
					"	SELECT 3 AS platform, amazon AS logindate FROM tbl_user_platform_logindate WHERE useridx = $useridx ".
					") sub ".
					"ORDER BY logindate DESC LIMIT 1 ";
			
			$lastest_login_platform = $db_main->getvalue($sql);
			
			if($lastest_login_platform == "")
				$lastest_login_platform = -1;
		}
		
		//최근 12주 coin_p
		$sql = "SELECT IFNULL(ROUND(SUM(coin_p)/SUM(credit)),0) ".
				"FROM ( ".
				"	SELECT SUM((facebookcredit*usercoin)) AS coin_p, SUM(facebookcredit) AS credit FROM tbl_product_order WHERE useridx = $useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 12 WEEK)".
				"	UNION ALL ".
				"	SELECT SUM((money*10*usercoin)) AS coin_p, SUM((money*10)) AS credit FROM tbl_product_order_mobile WHERE useridx = $useridx AND status = 1 AND writedate > DATE_SUB(NOW(), INTERVAL 12 WEEK) ".
				") t1; ";
		$coin_p = $db_main->getvalue($sql);
		
		if($coin_p == "")
			$coin_p = 0;
		
		//최근 4주 Coin_p
		$recent_coin_p = 0;
		
		if($recent_buy_count >= 10)
		{
			$sql = "SELECT ROUND(SUM(coin_p)/SUM(credit)) ".
					"FROM ( ".
					"	SELECT (facebookcredit*usercoin) AS coin_p, facebookcredit AS credit, writedate FROM tbl_product_order WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 40 AND writedate > DATE_SUB(NOW(), INTERVAL 4 WEEK) ".
					"	UNION ALL ".
					"	SELECT (money*10*usercoin) AS coin_p, (money*10) AS credit, writedate FROM tbl_product_order_mobile WHERE useridx = $useridx AND STATUS = 1 AND money >= 4 AND writedate > DATE_SUB(NOW(), INTERVAL 4 WEEK) ".
					") t1 ".
					"ORDER BY writedate DESC LIMIT 10 ";	
			
			$recent_coin_p = $db_main->getvalue($sql);
			
		}
		else if(0 < $recent_buy_count && $recent_buy_count < 10)
		{
			$sql = "SELECT ROUND(SUM(coin_p)/SUM(credit)) ".
					"FROM ( ".
					"	SELECT (facebookcredit*usercoin) AS coin_p, facebookcredit AS credit, writedate FROM tbl_product_order WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 40 AND writedate > DATE_SUB(NOW(), INTERVAL 4 WEEK) ".
					"	UNION ALL ".
					"	SELECT (money*10*usercoin) AS coin_p, (money*10) AS credit, writedate FROM tbl_product_order_mobile WHERE useridx = $useridx AND STATUS = 1 AND money >= 4 AND writedate > DATE_SUB(NOW(), INTERVAL 4 WEEK) ".
					") t1 ".
					"ORDER BY writedate DESC";
			
			$recent_coin_p = $db_main->getvalue($sql);
			
		}
		
		if($recent_coin_p == "")
			$recent_coin_p = 0;
		
		$today = date("Y-m-d");
		
		$last_buy_platform = -1;
		
		if($web_category != "")
			$last_buy_platform = $web_category;
		else
			$last_buy_platform = $mobile_category;
		
		// Insert 
		if($insert_cnt == 0)
		{
			$insert_sql = "INSERT INTO t5_user_recent_paytime(useridx, recent_writedate) VALUES($useridx, '$recent_writetime')";

			$insert_other_sql =	"INSERT INTO tbl_user_buyer_leave_stat(today, useridx, last_buy_platform, last_login_platform, recent_avg_buy_coin, recent_avg_buy_money, recent_avg_rebuyday, recent_avg_buy_more, recent_buy_count, recent_coin_p, avg_buy_coin, avg_buy_money, current_coin, avg_rebuyday, avg_buy_more, buy_count, buy_leave_day, login_leave_day, basic_coupon_remainday, promotion_coupon2_remainday, promotion_coupon3_remainday, coupon1, coupon2, coupon3, ".
							"web_season_product1, web_season_product2, web_season_product3, web_season_product_more1, web_season_product_more2, web_season_product_more3, mobile_season_product1, mobile_season_product2, mobile_season_product3, mobile_season_product_more1, mobile_season_product_more2, mobile_season_product_more3, ".
							"whale_product, whale_product_more, threshold_product1, threshold_product2, threshold_product_more1, threshold_product_more2, piggypot_stat, coin_p) ".
								"VALUES('$today', $useridx, $last_buy_platform, $lastest_login_platform, $recent_avg_product_coin, $recent_avg_buy_money, $recent_avg_rebuyday, $recent_avg_buy_more, $recent_buy_count, $recent_coin_p, $avg_product_coin, $avg_buy_money, $current_coin, $avg_rebuyday, $avg_buy_more, $buy_count, $elapsed_purchase_date, $elapsed_loginday, $basic_coupon_remainday, $promotion_coupon2_remainday, $promotion_coupon3_remainday, $coupon_1, $coupon_2, $coupon_3, ".
								"$season_product_1, $season_product_2, $season_product_3, $season_product_more_1, $season_product_more_2, $season_product_more_3, $mobile_season_product_1, $mobile_season_product_2, $mobile_season_product_3, $mobile_season_product_more_1, $mobile_season_product_more_2, $mobile_season_product_more_3, ".
								"$whale_product, $whale_product_more, $threshold_product_1, $threshold_product_2, $threshold_product_more_1, $threshold_product_more_2, $piggypot_stat, $coin_p) ";
		
			$insert_cnt++;
		}
		else if($insert_cnt < 1000)
		{
			$insert_sql .= ",($useridx, '$recent_writetime')";
			
			$insert_other_sql .= ",('$today', $useridx, $last_buy_platform, $lastest_login_platform, $recent_avg_product_coin, $recent_avg_buy_money, $recent_avg_rebuyday, $recent_avg_buy_more, $recent_buy_count, $recent_coin_p, $avg_product_coin, $avg_buy_money, $current_coin, $avg_rebuyday, $avg_buy_more,  $buy_count, $elapsed_purchase_date, $elapsed_loginday, $basic_coupon_remainday, $promotion_coupon2_remainday, $promotion_coupon3_remainday, $coupon_1, $coupon_2, $coupon_3, ".
								"$season_product_1, $season_product_2, $season_product_3, $season_product_more_1, $season_product_more_2, $season_product_more_3, $mobile_season_product_1, $mobile_season_product_2, $mobile_season_product_3, $mobile_season_product_more_1, $mobile_season_product_more_2, $mobile_season_product_more_3, ".
								"$whale_product, $whale_product_more, $threshold_product_1, $threshold_product_2, $threshold_product_more_1, $threshold_product_more_2, $piggypot_stat, $coin_p) ";
		
			$insert_cnt++;
		}
		else
		{
			$insert_sql .= ",($useridx, '$recent_writetime')";
			
			$insert_other_sql .= ",('$today', $useridx, $last_buy_platform, $lastest_login_platform, $recent_avg_product_coin, $recent_avg_buy_money, $recent_avg_rebuyday, $recent_avg_buy_more, $recent_buy_count, $recent_coin_p, $avg_product_coin, $avg_buy_money, $current_coin, $avg_rebuyday, $avg_buy_more, $buy_count, $elapsed_purchase_date, $elapsed_loginday, $basic_coupon_remainday, $promotion_coupon2_remainday, $promotion_coupon3_remainday, $coupon_1, $coupon_2, $coupon_3, ".
								"$season_product_1, $season_product_2, $season_product_3, $season_product_more_1, $season_product_more_2, $season_product_more_3, $mobile_season_product_1, $mobile_season_product_2, $mobile_season_product_3, $mobile_season_product_more_1, $mobile_season_product_more_2, $mobile_season_product_more_3, ".
								"$whale_product, $whale_product_more, $threshold_product_1, $threshold_product_2, $threshold_product_more_1, $threshold_product_more_2, $piggypot_stat, $coin_p) ";
			
			$db_redshift->execute($insert_sql);
			$db_other->execute($insert_other_sql);
		
			sleep(1);
		
			$insert_cnt = 0;
			$insert_sql = "";
			$insert_other_sql = "";
		}
	}
	
	if($insert_sql != "")
		$db_redshift->execute($insert_sql);
	
	if($insert_other_sql != "")
		$db_other->execute($insert_other_sql);
	
	// Money In,out Playcount
	$sql = "select useridx, sum(money_in) as money_in, sum(money_out) as money_out, sum(playcount) as playcount ".
			"from ( ".
			"	select t1.useridx, sum(money_in) as money_in, sum(money_out) as money_out, sum(playcount) as playcount from t5_user_gamelog t1 join t5_user_recent_paytime t2 on t1.useridx = t2.useridx ".
			"	where mode not in (4, 9, 26, 29) and t1.useridx = t2.useridx and writedate >= t2.recent_writedate group by t1.useridx ".
			"	union all ".
			"	select t1.useridx, sum(money_in) as money_in, sum(money_out) as money_out, sum(playcount) as playcount from t5_user_gamelog_ios t1 join t5_user_recent_paytime t2 on t1.useridx = t2.useridx ".
			"	where mode not in (4, 9, 26, 29) and t1.useridx = t2.useridx and writedate >= t2.recent_writedate group by t1.useridx ".
			"	union all ".
			"	select t1.useridx, sum(money_in) as money_in, sum(money_out) as money_out, sum(playcount) as playcount from t5_user_gamelog_android t1 join t5_user_recent_paytime t2 on t1.useridx = t2.useridx ".
			"	where mode not in (4, 9, 26, 29) and t1.useridx = t2.useridx and writedate >= t2.recent_writedate group by t1.useridx ".
			"	union all ".
			"	select t1.useridx, sum(money_in) as money_in, sum(money_out) as money_out, sum(playcount) as playcount from t5_user_gamelog_amazon t1 join t5_user_recent_paytime t2 on t1.useridx = t2.useridx ".
			"	where mode not in (4, 9, 26, 29) and t1.useridx = t2.useridx and writedate >= t2.recent_writedate group by t1.useridx ".
			") sub ".
			"group by useridx";	
	$gamelog_list = $db_redshift->gettotallist($sql);
	
	$today = date("Y-m-d");
	
	$insert_cnt = 0;
	$insert_sql = "";
	
	for($i=0; $i<sizeof($gamelog_list); $i++)
	{
		$useridx = $gamelog_list[$i]["useridx"];
		$money_in = $gamelog_list[$i]["money_in"];
		$money_out = $gamelog_list[$i]["money_out"];
		$playcount = $gamelog_list[$i]["playcount"];
	
		// Insert
		if($insert_cnt == 0)
		{
			$insert_sql = "INSERT INTO tbl_user_buyer_leave_stat(today, useridx, money_in, money_out, playcount) VALUES('$today', $useridx, $money_in, $money_out, $playcount)";

			$insert_cnt++;
		}
		else if($insert_cnt < 1000)
		{
			$insert_sql .= ",('$today', $useridx, $money_in, $money_out, $playcount)";

			$insert_cnt++;
		}
		else
		{
			$insert_sql .= ",('$today', $useridx, $money_in, $money_out, $playcount)";
			$insert_sql .= " ON DUPLICATE KEY UPDATE money_in = VALUES(money_in), money_out = VALUES(money_out), playcount = VALUES(playcount);";

			$db_other->execute($insert_sql);

			sleep(1);

			$insert_cnt = 0;
			$insert_sql = "";
		}
	}
	
	if($insert_sql != "")
	{
		$insert_sql .= " ON DUPLICATE KEY UPDATE money_in = VALUES(money_in), money_out = VALUES(money_out), playcount = VALUES(playcount);";
		$db_other->execute($insert_sql);
	}
	
	// duc_user_recent_paytime 초기화
	$truncate_sql = "truncate table t5_user_recent_paytime";
	$db_redshift->execute($truncate_sql);
	
	write_log("Purchase Leave Stat Scheduler End");
	
	$db_main->end();
	$db_main2->end();
	$db_other->end();
	$db_redshift->end();
?>