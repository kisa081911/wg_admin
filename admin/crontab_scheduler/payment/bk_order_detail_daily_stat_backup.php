<?	
    include("../../common/common_include.inc.php");

	$db_main_slave = new CDatabase_Main();
	$db_other = new CDatabase_Other();
	
	$db_main_slave->execute("SET wait_timeout=21600");
	$db_other->execute("SET wait_timeout=21600");	
	
	try
	{
		$str_useridx = 20000;
		
		//$sdate = "2015-11-17";
		//$edate = "2017-01-01";	
		
		$sdate = "2018-12-05";
		$edate = "2018-12-07";
		
		while($sdate < $edate)
		{
			$temp_date = date('Y-m-d', strtotime($sdate.' + 1 day'));
			
			write_log("order_detail_update : $sdate ~ $temp_date");
			
			$sql = "SELECT orderidx, platform,  useridx, money, basecoin,  coin, special_more, special_discount, usercoin, product_type, coupon_type, status, canceldate, writedate, ".
					  	"	IF(coupon_type IN (2, 3, 4), 1, 0) AS is_sale, DAYOFWEEK(writedate) AS today_week, WEEK(writedate) AS weeks, DATE_FORMAT(writedate, '%Y-%m-%d') AS ymd,   ".
						"	(SELECT createdate FROM tbl_user WHERE useridx = tt.useridx) AS user_createdate, ".
						"	DATEDIFF(tt.writedate, (SELECT createdate FROM tbl_user WHERE useridx = tt.useridx)) AS dayafterinstall,	".
						"	IFNULL((	".
						"		SELECT SUM(facebookcredit/10)FROM tbl_product_order WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate < DATE_FORMAT(tt.writedate, '%Y-%m-%d 00:00:00')	".
						"	), 0) AS web_accrue_money,	".
						"	(SELECT COUNT(*) FROM tbl_product_order	WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate < DATE_FORMAT(tt.writedate, '%Y-%m-%d 00:00:00')) AS web_accrue_buycnt,	".
						"	IFNULL((	".
						"		SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx = tt.useridx AND STATUS = 1 AND writedate < DATE_FORMAT(tt.writedate, '%Y-%m-%d 00:00:00')	".
						"	), 0) AS mobile_accrue_money,	".
						"	(SELECT COUNT(*) FROM tbl_product_order_mobile	WHERE useridx = tt.useridx AND STATUS = 1 AND writedate < DATE_FORMAT(tt.writedate, '%Y-%m-%d 00:00:00')) AS mobile_accrue_buycnt,	".
						"	IFNULL((	".
						"		SELECT SUM(facebookcredit/10) FROM tbl_product_order WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate <= tt.writedate	".
						"	), 0) AS web_total_money,	".
						"	IFNULL((	".
						"		SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx = tt.useridx AND STATUS = 1 AND writedate <= tt.writedate	".
						"	), 0) AS mobile_total_money,	".
						"	IFNULL((	".
						"		SELECT SUM(vip_point) FROM tbl_product_order WHERE useridx = tt.useridx AND STATUS IN (1, 3) AND writedate <= tt.writedate	".
						"	), 0) AS web_total_vip_point,	".
						"	IFNULL((	".
						"		SELECT SUM(vip_point) FROM tbl_product_order_mobile WHERE useridx = tt.useridx AND STATUS = 1 AND writedate <= tt.writedate	".
						"	), 0) AS mobile_total_vip_point	".
						"FROM	".
						"(	".
						"	SELECT orderidx, 0 AS platform, useridx, (facebookcredit/10) AS money, basecoin,  coin, special_more, special_discount, usercoin, vip_point,  product_type, coupon_type, STATUS, canceldate, writedate	".
						"	FROM tbl_product_order t1	".
						"	WHERE useridx > $str_useridx AND STATUS IN (1, 3) AND '$sdate 00:00:00' <= writedate AND writedate < '$temp_date 00:00:00'	". 
						"	UNION ALL	".
						"	SELECT orderidx, os_type AS platform, useridx, money, basecoin,  coin, special_more, special_discount, usercoin, vip_point,  product_type, coupon_type, STATUS, canceldate, writedate	".
						"	FROM tbl_product_order_mobile t2	".
						"	WHERE useridx > $str_useridx AND STATUS = 1 AND '$sdate 00:00:00' <= writedate AND writedate < '$temp_date 00:00:00'	". 
						") tt";
			   $order_list = $db_main_slave->gettotallist($sql);
			   
			   $insert_cnt = 0;
			   $insert_sql = "";
			    
			   for($i=0; $i<sizeof($order_list); $i++)
			   {
			   		$orderidx = $order_list[$i]["orderidx"];
			   		$platform = $order_list[$i]["platform"];
			   		$useridx = $order_list[$i]["useridx"];
			   		$money = $order_list[$i]["money"];
			   		$basecoin = $order_list[$i]["basecoin"];
			   		$coin = $order_list[$i]["coin"];
			   		$special_more = $order_list[$i]["special_more"];
			   		$special_discount = $order_list[$i]["special_discount"];
			   		$usercoin = $order_list[$i]["usercoin"];
			   		$product_type = $order_list[$i]["product_type"];
			   		$coupon_type = $order_list[$i]["coupon_type"];
			   		$status = $order_list[$i]["status"];
			   		$canceldate = $order_list[$i]["canceldate"];
			   		$writedate = $order_list[$i]["writedate"];
			   		$is_sale = $order_list[$i]["is_sale"];
			   		$dayweek = $order_list[$i]["today_week"];
			   		$weeks = $order_list[$i]["weeks"];
			   		$ymd = $order_list[$i]["ymd"];	   		
			   		$dayafterinstall = $order_list[$i]["dayafterinstall"];
			   		
			   		$web_accrue_money = $order_list[$i]["web_accrue_money"];
			   		$web_accrue_buycnt = $order_list[$i]["web_accrue_buycnt"];
			   		
			   		$mobile_accrue_money = $order_list[$i]["mobile_accrue_money"];
			   		$mobile_accrue_buycnt = $order_list[$i]["mobile_accrue_buycnt"];
			   		
			   		$web_total_money = $order_list[$i]["web_total_money"];
			   		$mobile_total_money = $order_list[$i]["mobile_total_money"];
			   		
			   		$web_total_vip_point = $order_list[$i]["web_total_vip_point"];
			   		$mobile_total_vip_point = $order_list[$i]["mobile_total_vip_point"];		   			
			   			
			   		$accrue_money = $web_accrue_money + $mobile_accrue_money;
			   		$accrue_buycnt = $web_accrue_buycnt + $mobile_accrue_buycnt;
			   		
			   		$vip_point = $web_total_vip_point+$mobile_total_vip_point;
			   		
			   		$total_money = $web_total_money + $mobile_total_money;	   		
			   		
			   		if($vip_point >= 9999)
			   			$vip_level = 9;
			   		else if($vip_point >= 7999)
			   			$vip_level = 8;
			   		else if($vip_point >= 5999)
			   			$vip_level = 7;
			   		else if($vip_point >= 2999)
			   			$vip_level = 6;
			   		else if($vip_point >= 1999)
			   			$vip_level = 5;
			   		else if($vip_point >= 999)
			   			$vip_level = 4;
			   		else if($vip_point >= 499)
			   			$vip_level = 3;
			   		else if($vip_point >= 99)
			   			$vip_level = 2;
			   		else if($vip_point >= 39)
			   			$vip_level = 1;
			   		else 
			   			$vip_level = 0;
			   		
			   		$dayafterpurchase = "0000-00-00 00:00:00";
			   		
			   		// 이전 직전 결제와 현재 결제 diff
			   		if($writedate != "")
			   		{
				   		$sql = "SELECT DATEDIFF('$writedate', writedate) ".
								"FROM	".
								"(	".
								"	SELECT writedate	".
								"	FROM tbl_product_order t1	".
								"	WHERE useridx = $useridx AND STATUS IN (1, 3) AND writedate < '$writedate'	". 
								"	UNION ALL	".
								"	SELECT writedate	".
								"	FROM tbl_product_order_mobile t2	".
								"	WHERE useridx = $useridx AND STATUS = 1 AND writedate < '$writedate'	". 
								") tt ORDER BY writedate DESC LIMIT 1";
				   		$dayafterpurchase = $db_main_slave->getvalue($sql);
					}
					
					if($dayafterpurchase == "")
						$dayafterpurchase = 0;
					
					$recent_4w_money = 0;
					$recent_4w_buycnt = 0;
					
					//현재 결제 기준을 00:00:00로 변경 후 최근 4week(28day) 기준
					if($ymd != "")
					{
				   		$sql = "SELECT IFNULL(SUM(money),0) AS recent_4week_money, COUNT(*) AS recent_4week_cnt ".
								"FROM	".
								"(	".
								"	SELECT (facebookcredit/10) AS money, writedate	".
								"	FROM tbl_product_order t1	".
								"	WHERE useridx = $useridx AND STATUS IN (1, 3) AND writedate >= DATE_SUB('$ymd 00:00:00', INTERVAL 4 WEEK) AND writedate < '$ymd 00:00:00'	".
								"	UNION ALL	".
								"	SELECT money, writedate	".
								"	FROM tbl_product_order_mobile t2	".
								"	WHERE useridx = $useridx AND STATUS = 1 AND writedate >= DATE_SUB('$ymd 00:00:00', INTERVAL 4 WEEK) AND writedate < '$ymd 00:00:00'	".
								") tt";		   		
				   		$recent_4week_info = $db_main_slave->getarray($sql);
				   		
				   		$recent_4w_money = $recent_4week_info["recent_4week_money"];
				   		$recent_4w_buycnt = $recent_4week_info["recent_4week_cnt"];		   		
					}
					
					if($insert_cnt == 0)
					{
			   			$insert_sql = "INSERT INTO tbl_product_order_detail(orderidx, platform, useridx, money, basecoin, coin, special_more, special_discount, usercoin, vip_level, vip_point, product_type, coupon_type, status, canceldate, writedate, is_sale, weeks, dayweek, dayafterpurchase, dayafterinstall, recent_4w_money, recent_4w_buycnt, accrue_money, accrue_buycnt) VALUES ".
					   					" ('$orderidx', '$platform', '$useridx', '$money', '$basecoin', '$coin', '$special_more', '$special_discount', '$usercoin', '$vip_level', '$vip_point', '$product_type', '$coupon_type', '$status', '$canceldate', '$writedate', '$is_sale', '$weeks', '$dayweek', '$dayafterpurchase', '$dayafterinstall', '$recent_4w_money', '$recent_4w_buycnt', '$accrue_money', '$accrue_buycnt') ";
			   			$insert_cnt++;
					}
					else if($insert_cnt < 2000)
					{
						$insert_sql .= ", ('$orderidx', '$platform', '$useridx', '$money', '$basecoin', '$coin', '$special_more', '$special_discount', '$usercoin', '$vip_level', '$vip_point', '$product_type', '$coupon_type', '$status', '$canceldate', '$writedate', '$is_sale', '$weeks', '$dayweek', '$dayafterpurchase', '$dayafterinstall', '$recent_4w_money', '$recent_4w_buycnt', '$accrue_money', '$accrue_buycnt') ";
						
						$insert_cnt++;
					}
					else 
					{
						$insert_sql .= ", ('$orderidx', '$platform', '$useridx', '$money', '$basecoin', '$coin', '$special_more', '$special_discount', '$usercoin', '$vip_level', '$vip_point', '$product_type', '$coupon_type', '$status', '$canceldate', '$writedate', '$is_sale', '$weeks', '$dayweek', '$dayafterpurchase', '$dayafterinstall', '$recent_4w_money', '$recent_4w_buycnt', '$accrue_money', '$accrue_buycnt') ";
						
						$insert_sql .= " ON DUPLICATE KEY UPDATE useridx=VALUES(useridx), money=VALUES(money), basecoin=VALUES(basecoin), coin=VALUES(coin), special_more=VALUES(special_more), special_discount=VALUES(special_discount), usercoin=VALUES(usercoin), vip_level=VALUES(vip_level), vip_point=VALUES(vip_point), product_type=VALUES(product_type), coupon_type=VALUES(coupon_type), status=VALUES(status), canceldate=VALUES(canceldate), writedate=VALUES(writedate), ".
						" is_sale=VALUES(is_sale), weeks=VALUES(weeks), dayweek=VALUES(dayweek), dayafterpurchase=VALUES(dayafterpurchase), dayafterinstall=VALUES(dayafterinstall), recent_4w_money=VALUES(recent_4w_money), recent_4w_buycnt=VALUES(recent_4w_buycnt), accrue_money=VALUES(accrue_money), accrue_buycnt=VALUES(accrue_buycnt);";
						$db_other->execute($insert_sql);
						
						$insert_sql = "";
						$insert_cnt = 0;
					}
			   }
			   
			   if($insert_sql != "")
			   {
				   	$insert_sql .= " ON DUPLICATE KEY UPDATE useridx=VALUES(useridx), money=VALUES(money), basecoin=VALUES(basecoin), coin=VALUES(coin), special_more=VALUES(special_more), special_discount=VALUES(special_discount), usercoin=VALUES(usercoin), vip_level=VALUES(vip_level), vip_point=VALUES(vip_point), product_type=VALUES(product_type), coupon_type=VALUES(coupon_type), status=VALUES(status), canceldate=VALUES(canceldate), writedate=VALUES(writedate), ".
				   			" is_sale=VALUES(is_sale), weeks=VALUES(weeks), dayweek=VALUES(dayweek), dayafterpurchase=VALUES(dayafterpurchase), dayafterinstall=VALUES(dayafterinstall), recent_4w_money=VALUES(recent_4w_money), recent_4w_buycnt=VALUES(recent_4w_buycnt), accrue_money=VALUES(accrue_money), accrue_buycnt=VALUES(accrue_buycnt);";
				   	
				   	$db_other->execute($insert_sql);
			   }
			   
			   $sdate = date('Y-m-d', strtotime($sdate.' + 1 day'));
			   
			   sleep(1);
		}
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	}	
	
	$db_main_slave->end();
	$db_other->end();
?>