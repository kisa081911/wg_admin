<?
    include("../../common/common_include.inc.php");
	
    $db_main = new CDatabase_Main();
    $db_analysis = new CDatabase_Analysis();
	
    $db_main->execute("SET wait_timeout=7200");
	$db_analysis->execute("SET wait_timeout=7200");
	
    $write_sdate  = date("Y-m-d");
	
	// Facebook
	$sql = "SELECT DATE_FORMAT(MAX(writedate), '%Y-%m-%d') today, 0 AS platform, SUM(coin) AS total_coin, SUM(basecoin) AS total_basecoin
			FROM tbl_product_order
			WHERE useridx > 20000 AND status = 1 AND writedate >= DATE_SUB('$write_sdate', INTERVAL 4 WEEK) AND writedate < '$write_sdate'";
	$web_list = $db_main->gettotallist($sql);
	
	for($i=0; $i<sizeof($web_list); $i++)
	{
		$today = $web_list[$i]["today"];
		$platform = $web_list[$i]["platform"];
		$total_coin = $web_list[$i]["total_coin"];
		$total_basecoin = $web_list[$i]["total_basecoin"];
		
		$sql = "INSERT INTO tbl_order_4week_daily(today, platform, total_coin, total_basecoin) VALUES('$today', $platform, $total_coin, $total_basecoin) ".
				"ON DUPLICATE KEY UPDATE total_coin=VALUES(total_coin), total_basecoin=VALUES(total_basecoin);";
		$db_analysis->execute($sql);
	}
		
	// Mobile
	$sql = "SELECT DATE_FORMAT(MAX(writedate), '%Y-%m-%d') today, os_type AS platform, SUM(coin) AS total_coin, SUM(basecoin) AS total_basecoin
			FROM tbl_product_order_mobile
			WHERE useridx > 20000 AND status = 1 AND writedate >= DATE_SUB('$write_sdate', INTERVAL 4 WEEK) AND writedate < '$write_sdate'
			GROUP BY os_type";
	$mobile_list = $db_main->gettotallist($sql);
	
	for($i=0; $i<sizeof($mobile_list); $i++)
	{
		$today = $mobile_list[$i]["today"];
		$platform = $mobile_list[$i]["platform"];
		$total_coin = $mobile_list[$i]["total_coin"];
		$total_basecoin = $mobile_list[$i]["total_basecoin"];
		
		$sql = "INSERT INTO tbl_order_4week_daily(today, platform, total_coin, total_basecoin) VALUES('$today', $platform, $total_coin, $total_basecoin) ".
				"ON DUPLICATE KEY UPDATE total_coin=VALUES(total_coin), total_basecoin=VALUES(total_basecoin);";
		$db_analysis->execute($sql);
	}
	
	$db_main->end();
	$db_analysis->end();
?>