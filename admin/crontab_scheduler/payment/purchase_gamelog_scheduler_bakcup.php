<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/purchase_gamelog_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
		exit();
	
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	$db_other->execute("SET wait_timeout=72000");
	
	// 과거 데이터 Insert	
	$startdate = '2016-06-30';
	$enddate = '2016-07-31';		
	
	while($startdate < $enddate)
	{
		$temp_date = date('Y-m-d', strtotime($startdate.' + 1 day'));
	
		write_log($temp_date);	
	
		$sql = "SELECT logidx, useridx, writedate, rebuydate ".
				"FROM tbl_user_purchase_log WHERE rebuydate BETWEEN '$temp_date 00:00:00' AND '$temp_date 23:59:59' ORDER BY logidx ASC";
		write_log($sql);
		$purchase_data_list = $db_other->gettotallist($sql);
		
		write_log("Purchase Gamelog Scheduler Start");
		write_log("Purchase Gamelog Scheduler Start Time : ".date('Y-m-d H:i:s'));
		write_log("Purchase Result today : ".$temp_date);		
		write_log("Purchase Result Size : ".sizeof($purchase_data_list));
		
		for($i=0; $i<sizeof($purchase_data_list); $i++)
		{	
			$payment_logidx = $purchase_data_list[$i]["logidx"];
			$useridx = $purchase_data_list[$i]["useridx"];
			$writedate = $purchase_data_list[$i]["writedate"];
			$rebuydate = $purchase_data_list[$i]["rebuydate"];
			
			$sql = "insert into t5_user_purchase_gamelog(pidx, useridx, platform, slottype, betlevel, mode, money_in, money_out, playcount, writedate, rebuydate) ".
					"(select $payment_logidx as pidx, useridx, platform, slottype, betlevel, mode, money_in, money_out, playcount, writedate, '$rebuydate' AS rebuydate ".
					"from ( ".   
					"  select useridx, 0 as platform, slottype, betlevel, mode, money_in, money_out, playcount, writedate ".   
					"  from t5_user_gamelog where writedate between '$writedate' and '$rebuydate' and useridx = $useridx ".   
					"  union all ".
					"  select useridx, 1 as platform, slottype, betlevel, mode, money_in, money_out, playcount, writedate ".   
					"  from t5_user_gamelog_ios where writedate between '$writedate' and '$rebuydate' and useridx = $useridx ".   
					"  union all ".
					"  select useridx, 2 as platform, slottype, betlevel, mode, money_in, money_out, playcount, writedate ".   
					"  from t5_user_gamelog_android where writedate between '$writedate' and '$rebuydate' and useridx = $useridx ".   
					"  union all ".
					"  select useridx, 3 as platform, slottype, betlevel, mode, money_in, money_out, playcount, writedate ".   
					"  from t5_user_gamelog_amazon where writedate between '$writedate' and '$rebuydate' and useridx = $useridx ". 
					") t1 order by writedate asc) ";
			
			$db_redshift->execute($sql);
		}
		
		$startdate = $temp_date;
		
		write_log("Purchase Gamelog Scheduler End Time : ".date('Y-m-d H:i:s'));
		write_log("Purchase Gamelog Scheduler End");
	}
	
	$db_other->end();
	$db_redshift->end();
?>