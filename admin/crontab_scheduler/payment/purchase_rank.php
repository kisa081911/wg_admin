<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/purchase_rank") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/purchase_rank") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("payment/purchase_rank Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	$db_redshift = new CDatabase_Redshift();
	$db_other = new CDatabase_Other();
	
	$db_other->execute("SET wait_timeout=72000");
	
	$having_tail = "";
	
	$today = date("Y-m-d", strtotime("-1 days"));
	
	for($i=0; $i<5; $i++)
	{
		if($i == 0)
			$having_tail = "having  SUM(money) < 9 ";
		else if($i == 1)
			$having_tail = "having  SUM(money) >= 9 and SUM(money) < 59 ";
		else if($i == 2)
			$having_tail = "having  SUM(money) >= 59 and SUM(money) < 199 ";
		else if($i == 3)
			$having_tail = "having  SUM(money) >= 199 and SUM(money) < 499 ";
		else if($i == 4)
			$having_tail = "having  SUM(money) >= 499 ";
								
		// 유저 추출
		$sql = "select useridx, SUM(money) as money ".
				"from ( ".
				"	select useridx, (facebookcredit::float/10::float) as money ".
				"	from t5_product_order ".
				"	where useridx > 20000 and status = 1 and 0 <= datediff(day, writedate, '$today 00:00:00') and datediff(day, writedate, '$today 00:00:00') <= 28 ".
				"	union all ".
				"	select useridx, money ".
				"	from t5_product_order_mobile ".
				"	where useridx > 20000 and status = 1 and 0 <= datediff(day, writedate, '$today 00:00:00') and datediff(day, writedate, '$today 00:00:00') <= 28 ".
				") t1 ".
				"group by useridx ".
				"$having_tail";

		$data_list = $db_redshift->gettotallist($sql);

		$userlist = "";

		$usercount = sizeof($data_list);

		for($j=0; $j<sizeof($data_list); $j++)
		{
			$useridx = $data_list[$j]["useridx"];

			if($userlist == "")
				$userlist = $useridx;
			else
				$userlist .= ",".$useridx;
		}

		if($usercount > 0)
		{
			// Platform 결제액
			$sql = "select os_type, SUM(money) as money ".
				"from ( ".
				"	select useridx, os_type, SUM(money) as money ".
				"	from ( ".
				"		select useridx, 0 AS os_type, (facebookcredit::float/10::float) as money ".
				"		from t5_product_order ".
				"		where useridx > 20000 and status = 1 and 0 <= datediff(day, writedate, '$today 00:00:00') and datediff(day, writedate, '$today 00:00:00') <= 28 and useridx in ($userlist) ".
				"		union all ".
				"		select useridx, os_type, money ".
				"		from t5_product_order_mobile ".
				"		where useridx > 20000 and status = 1 and 0 <= datediff(day, writedate, '$today 00:00:00') and datediff(day, writedate, '$today 00:00:00') <= 28 and useridx in ($userlist)".
				"	) t1 ".
				"	group by useridx, os_type ".
				") t2 ".
				"group by os_type order by os_type asc ";
			
			$purchase_list = $db_redshift->gettotallist($sql);

			$money = $purchase_list[0]["money"];
			$ios_money = $purchase_list[1]["money"];
			$and_money = $purchase_list[2]["money"];
			$ama_money = $purchase_list[3]["money"];
			
			if($money == "") 
				$money = 0;
			
			if($ios_money == "") 
				$ios_money = 0;
			
			if($and_money == "") 
				$and_money = 0;	
			
			if($ama_money == "") 
				$ama_money = 0;
		}
		else
		{
			$usercount = 0;
			$money = 0;
			$ios_money = 0;
			$and_money = 0;
			$ama_money = 0;
		}



		$insert_sql = "INSERT INTO tbl_buyer_grade_stat VALUES('$today', $i, $usercount, $money, $ios_money, $and_money, $ama_money)";
		$db_other->execute($insert_sql);
	}
	
	$db_other->end();
	$db_redshift->end();
?>
