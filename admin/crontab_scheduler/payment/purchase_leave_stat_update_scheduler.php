<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/purchase_leave_stat_update_scheduler") !== false)
		{
			$count++;
		}
	}
	
	ini_set("memory_limit", "-1");
	
	$issuccess = "1";
	
	if ($count > 1)
		exit();
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();
	
	$db_main->execute("SET wait_timeout=21600");
	$db_main2->execute("SET wait_timeout=21600");
	$db_other->execute("SET wait_timeout=21600");
	
	write_log("Purchase Leave Stat Scheduler Update Start");
	
	$today = date("Y-m-d");
	
	$sql = "SELECT MAX(today) FROM tbl_user_buyer_leave_stat";
	$leave_stat_today = $db_other->getvalue($sql);
	
	$sql = "SELECT * FROM tbl_user_buyer_leave_stat WHERE today = '$leave_stat_today' AND ".
			"(LEAST(recent_avg_rebuyday, avg_rebuyday) >= 5 OR buy_leave_day >= 7) AND buy_leave_day - LEAST(recent_avg_rebuyday, avg_rebuyday) >= 3 AND recent_avg_buy_money >= 19 AND recent_buy_count >= 3";	
	$data_list = $db_other->gettotallist($sql);
	
	$insert_cnt = 0;
	$insert_sql = "";
	$insert_other_sql = "";
	
	for($i=0; $i<sizeof($data_list); $i++)
	{
		$useridx = $data_list[$i]["useridx"];
		$last_buy_platform = $data_list[$i]["last_buy_platform"];
		$last_login_platform = $data_list[$i]["last_login_platform"];
		
		$recent_avg_buy_coin = $data_list[$i]["recent_avg_buy_coin"];
		$recent_avg_buy_money = $data_list[$i]["recent_avg_buy_money"];
		$recent_avg_rebuyday = $data_list[$i]["recent_avg_rebuyday"];
		$recent_avg_buy_more = $data_list[$i]["recent_avg_buy_more"];
		$recent_buy_count = $data_list[$i]["recent_buy_count"];
		$recent_coin_p = $data_list[$i]["recent_coin_p"];
		
		$avg_buy_coin = $data_list[$i]["avg_buy_coin"];
		$avg_buy_money = $data_list[$i]["avg_buy_money"];
		$current_coin = $data_list[$i]["current_coin"];
		
		$avg_rebuyday = $data_list[$i]["avg_rebuyday"];
		$avg_buy_more = $data_list[$i]["avg_buy_more"];
		$buy_count = $data_list[$i]["buy_count"];
		
		$buy_leave_day = $data_list[$i]["buy_leave_day"];
		$login_leave_day = $data_list[$i]["login_leave_day"];
		
		$money_in = $data_list[$i]["money_in"];
		$money_out = $data_list[$i]["money_out"];
		$playcount = $data_list[$i]["playcount"];		
		$coin_p = $data_list[$i]["coin_p"];
		
		$basic_coupon_remainday = $data_list[$i]["basic_coupon_remainday"];
		$promotion_coupon2_remainday = $data_list[$i]["promotion_coupon2_remainday"];
		$promotion_coupon3_remainday = $data_list[$i]["promotion_coupon3_remainday"];
		
		$coupon1 = $data_list[$i]["coupon1"];
		$coupon2 = $data_list[$i]["coupon2"];
		$coupon3 = $data_list[$i]["coupon3"];
		
		$web_season_product1 = $data_list[$i]["web_season_product1"];
		$web_season_product2 = $data_list[$i]["web_season_product2"];
		$web_season_product3 = $data_list[$i]["web_season_product3"];
		
		$web_season_product_more1 = $data_list[$i]["web_season_product_more1"];
		$web_season_product_more2 = $data_list[$i]["web_season_product_more2"];
		$web_season_product_more3 = $data_list[$i]["web_season_product_more3"];
		
		$mobile_season_product1 = $data_list[$i]["mobile_season_product1"];
		$mobile_season_product2 = $data_list[$i]["mobile_season_product2"];
		$mobile_season_product3 = $data_list[$i]["mobile_season_product3"];
		
		$mobile_season_product_more1 = $data_list[$i]["mobile_season_product_more1"];
		$mobile_season_product_more2 = $data_list[$i]["mobile_season_product_more2"];
		$mobile_season_product_more3 = $data_list[$i]["mobile_season_product_more3"];
		
		$whale_product = $data_list[$i]["whale_product"];	
		$whale_product_more = $data_list[$i]["whale_product_more"];		
		
		$threshold_product1 = $data_list[$i]["threshold_product1"];
		$threshold_product2 = $data_list[$i]["threshold_product2"];
		
		$threshold_product_more1 = $data_list[$i]["threshold_product_more1"];
		$threshold_product_more2 = $data_list[$i]["threshold_product_more2"];
		
		$piggypot_stat = $data_list[$i]["piggypot_stat"];
		
		$sql = "SELECT ".
				"	(SELECT COUNT(*) FROM tbl_product_order WHERE useridx = $useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 2 DAY) LIMIT 1) + ".
				"	(SELECT COUNT(*) FROM tbl_product_order_mobile WHERE useridx = $useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 2 DAY) LIMIT 1) AS purchase_count ";
		$purchase_count = $db_main->getvalue($sql);
		
		if($purchase_count == 0)
		{
			// Insert
			if($insert_cnt == 0)
			{		
				$insert_sql = "INSERT INTO tbl_user_buyer_leave_stat(useridx, last_buy_platform, last_login_platform, recent_avg_buy_coin, recent_avg_buy_money, recent_avg_rebuyday, recent_avg_buy_more, recent_buy_count, recent_coin_p, avg_buy_coin, avg_buy_money, current_coin, avg_rebuyday, avg_buy_more, buy_count, buy_leave_day, login_leave_day, money_in, money_out, playcount, coin_p, basic_coupon_remainday, promotion_coupon2_remainday, promotion_coupon3_remainday, coupon1, coupon2, coupon3, ".
						"web_season_product1, web_season_product2, web_season_product3, web_season_product_more1, web_season_product_more2, web_season_product_more3, mobile_season_product1, mobile_season_product2, mobile_season_product3, mobile_season_product_more1, mobile_season_product_more2, mobile_season_product_more3, ".
						"whale_product, whale_product_more, threshold_product1, threshold_product2, threshold_product_more1, threshold_product_more2, piggypot_stat, is_buy, writedate) ".
						"VALUES($useridx, $last_buy_platform, $last_login_platform, $recent_avg_buy_coin, $recent_avg_buy_money, $recent_avg_rebuyday, $recent_avg_buy_more, $recent_buy_count, $recent_coin_p, $avg_buy_coin, $avg_buy_money, $current_coin, $avg_rebuyday, $avg_buy_more, $buy_count, $buy_leave_day, $login_leave_day, $money_in, $money_out, $playcount, $coin_p, $basic_coupon_remainday, $promotion_coupon2_remainday, $promotion_coupon3_remainday, $coupon1, $coupon2, $coupon3, ".
						"$web_season_product1, $web_season_product2, $web_season_product3, $web_season_product_more1, $web_season_product_more2, $web_season_product_more3, $mobile_season_product1, $mobile_season_product2, $mobile_season_product3, $mobile_season_product_more1, $mobile_season_product_more2, $mobile_season_product_more3, ".
						"$whale_product, $whale_product_more, $threshold_product1, $threshold_product2, $threshold_product_more1, $threshold_product_more2, $piggypot_stat, 0, NOW()) ";
			
				$insert_cnt++;
			}
			else if($insert_cnt < 1000)
			{					
				$insert_sql .= ",($useridx, $last_buy_platform, $last_login_platform, $recent_avg_buy_coin, $recent_avg_buy_money, $recent_avg_rebuyday, $recent_avg_buy_more, $recent_buy_count, $recent_coin_p, $avg_buy_coin, $avg_buy_money, $current_coin, $avg_rebuyday, $avg_buy_more, $buy_count, $buy_leave_day, $login_leave_day, $money_in, $money_out, $playcount, $coin_p, $basic_coupon_remainday, $promotion_coupon2_remainday, $promotion_coupon3_remainday, $coupon1, $coupon2, $coupon3, ".
								"$web_season_product1, $web_season_product2, $web_season_product3, $web_season_product_more1, $web_season_product_more2, $web_season_product_more3, $mobile_season_product1, $mobile_season_product2, $mobile_season_product3, $mobile_season_product_more1, $mobile_season_product_more2, $mobile_season_product_more3, ".
								"$whale_product, $whale_product_more, $threshold_product1, $threshold_product2, $threshold_product_more1, $threshold_product_more2, $piggypot_stat, 0, NOW()) ";
			
				$insert_cnt++;
			}
			else
			{		
				$insert_sql .= ",($useridx, $last_buy_platform, $last_login_platform, $recent_avg_buy_coin, $recent_avg_buy_money, $recent_avg_rebuyday, $recent_avg_buy_more, $recent_buy_count, $recent_coin_p, $avg_buy_coin, $avg_buy_money, $current_coin, $avg_rebuyday, $avg_buy_more, $buy_count, $buy_leave_day, $login_leave_day, $money_in, $money_out, $playcount, $coin_p, $basic_coupon_remainday, $promotion_coupon2_remainday, $promotion_coupon3_remainday, $coupon1, $coupon2, $coupon3, ".
								"$web_season_product1, $web_season_product2, $web_season_product3, $web_season_product_more1, $web_season_product_more2, $web_season_product_more3, $mobile_season_product1, $mobile_season_product2, $mobile_season_product3, $mobile_season_product_more1, $mobile_season_product_more2, $mobile_season_product_more3, ".
								"$whale_product, $whale_product_more, $threshold_product1, $threshold_product2, $threshold_product_more1, $threshold_product_more2, $piggypot_stat, 0, NOW()) ";
				
				$insert_sql .= "ON DUPLICATE KEY UPDATE last_buy_platform = VALUES(last_buy_platform), last_login_platform = VALUES(last_login_platform), recent_avg_buy_coin = VALUES(recent_avg_buy_coin), recent_avg_buy_money = VALUES(recent_avg_buy_money), recent_avg_rebuyday = VALUES(recent_avg_rebuyday), recent_avg_buy_more = VALUES(recent_avg_buy_more), recent_buy_count = VALUES(recent_buy_count), recent_coin_p = VALUES(recent_coin_p), avg_buy_coin = VALUES(avg_buy_coin), avg_buy_money = VALUES(avg_buy_money), current_coin = VALUES(current_coin), avg_rebuyday = VALUES(avg_rebuyday), avg_buy_more = VALUES(avg_buy_more), buy_count = VALUES(buy_count), buy_leave_day = VALUES(buy_leave_day), login_leave_day = VALUES(login_leave_day), ".
								"money_in = VALUES(money_in), money_out = VALUES(money_out), playcount = VALUES(playcount), coin_p = VALUES(coin_p), basic_coupon_remainday = VALUES(basic_coupon_remainday), promotion_coupon2_remainday = VALUES(promotion_coupon2_remainday), promotion_coupon3_remainday = VALUES(promotion_coupon3_remainday), coupon1 = VALUES(coupon1), coupon2 = VALUES(coupon2), coupon3 = VALUES(coupon3), ".
								"web_season_product1 = VALUES(web_season_product1), web_season_product2 = VALUES(web_season_product2), web_season_product3 = VALUES(web_season_product3), web_season_product_more1 = VALUES(web_season_product_more1), web_season_product_more2 = VALUES(web_season_product_more2), web_season_product_more3 = VALUES(web_season_product_more3), ".
								"mobile_season_product1 = VALUES(mobile_season_product1), mobile_season_product2 = VALUES(mobile_season_product2), mobile_season_product3 = VALUES(mobile_season_product3), mobile_season_product_more1 = VALUES(mobile_season_product_more1), mobile_season_product_more2 = VALUES(mobile_season_product_more2), mobile_season_product_more3 = VALUES(mobile_season_product_more3), whale_product = VALUES(whale_product), whale_product_more = VALUES(whale_product_more), ".
								"threshold_product1 = VALUES(threshold_product1), threshold_product2 = VALUES(threshold_product2), threshold_product_more1 = VALUES(threshold_product_more1), threshold_product_more2 = VALUES(threshold_product_more2), piggypot_stat = VALUES(piggypot_stat), is_buy = VALUES(is_buy), writedate = VALUES(writedate);";
				
				$db_main2->execute($insert_sql);
			
				sleep(1);
			
				$insert_cnt = 0;
				$insert_sql = "";
			}
		}
	}
	
	if($insert_sql != "")
	{
		$insert_sql .= "ON DUPLICATE KEY UPDATE last_buy_platform = VALUES(last_buy_platform), last_login_platform = VALUES(last_login_platform), recent_avg_buy_coin = VALUES(recent_avg_buy_coin), recent_avg_buy_money = VALUES(recent_avg_buy_money), recent_avg_rebuyday = VALUES(recent_avg_rebuyday), recent_avg_buy_more = VALUES(recent_avg_buy_more), recent_buy_count = VALUES(recent_buy_count), recent_coin_p = VALUES(recent_coin_p), avg_buy_coin = VALUES(avg_buy_coin), avg_buy_money = VALUES(avg_buy_money), current_coin = VALUES(current_coin), avg_rebuyday = VALUES(avg_rebuyday), avg_buy_more = VALUES(avg_buy_more), buy_count = VALUES(buy_count), buy_leave_day = VALUES(buy_leave_day), login_leave_day = VALUES(login_leave_day), ".
								"money_in = VALUES(money_in), money_out = VALUES(money_out), playcount = VALUES(playcount), coin_p = VALUES(coin_p), basic_coupon_remainday = VALUES(basic_coupon_remainday), promotion_coupon2_remainday = VALUES(promotion_coupon2_remainday), promotion_coupon3_remainday = VALUES(promotion_coupon3_remainday), coupon1 = VALUES(coupon1), coupon2 = VALUES(coupon2), coupon3 = VALUES(coupon3), ".
								"web_season_product1 = VALUES(web_season_product1), web_season_product2 = VALUES(web_season_product2), web_season_product3 = VALUES(web_season_product3), web_season_product_more1 = VALUES(web_season_product_more1), web_season_product_more2 = VALUES(web_season_product_more2), web_season_product_more3 = VALUES(web_season_product_more3), ".
								"mobile_season_product1 = VALUES(mobile_season_product1), mobile_season_product2 = VALUES(mobile_season_product2), mobile_season_product3 = VALUES(mobile_season_product3), mobile_season_product_more1 = VALUES(mobile_season_product_more1), mobile_season_product_more2 = VALUES(mobile_season_product_more2), mobile_season_product_more3 = VALUES(mobile_season_product_more3), whale_product = VALUES(whale_product), whale_product_more = VALUES(whale_product_more), ".
								"threshold_product1 = VALUES(threshold_product1), threshold_product2 = VALUES(threshold_product2), threshold_product_more1 = VALUES(threshold_product_more1), threshold_product_more2 = VALUES(threshold_product_more2), piggypot_stat = VALUES(piggypot_stat), is_buy = VALUES(is_buy), writedate = VALUES(writedate); ";
		
		$db_main2->execute($insert_sql);
	}
	
	write_log("Purchase Leave Stat Scheduler Update End");
	
	$db_main->end();
	$db_main2->end();
	$db_other->end();
?>