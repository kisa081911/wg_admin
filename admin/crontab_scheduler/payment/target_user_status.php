<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/target_user_status") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/target_user_status") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("target_user_status Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	ini_set("memory_limit", "-1");	
	$current_date = date("Y-m-d");
	
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	$db_main2->execute("SET wait_timeout=7200");
	$db_other->execute("SET wait_timeout=7200");
	
	try
	{
		$sql = "SELECT COUNT(*) FROM tbl_target_user_tmp";
		$row_count = $db_main2->getvalue($sql);
		
		while($row_count > 0)
		{
			$sql = "DELETE FROM tbl_target_user_tmp LIMIT 5000;";
			$db_main2->execute($sql);
		
			$row_count -= 5000;
		}
		
		write_log("target_user_1");
		//
		$sql = "select useridx, (sum(moneyout)::float/sum(moneyin)::float*100::float) as winrate, avg(moneyin) as avg_money_in ".
				"from	". 
				"(	".
				"	select useridx, moneyin + h_moneyin AS moneyin, moneyout + h_moneyout + jackpot AS moneyout, (playcount+h_playcount) AS playcount". 
				"	from t5_user_playstat_daily	". 
				"	where dateadd(DAY, -8,  '$current_date') <= today and today <= dateadd(DAY, -1,  '$current_date')	".
				"	UNION ALL	".
				"	select useridx, moneyin + h_moneyin AS moneyin, moneyout + h_moneyout + jackpot  AS moneyout, (playcount+h_playcount) AS playcount ". 
				"	from t5_user_playstat_daily_ios	". 
				"	where dateadd(DAY, -8,  '$current_date') <= today and today <= dateadd(DAY, -1,  '$current_date')	".
				"	UNION ALL	".
				"	SELECT useridx, moneyin + h_moneyin AS moneyin, moneyout + h_moneyout + jackpot  AS moneyout, (playcount+h_playcount) AS playcount ". 
				"	FROM t5_user_playstat_daily_android	".
				"	WHERE dateadd(DAY, -8,  '$current_date') <= today AND today <= dateadd(DAY, -1,  '$current_date')	".
				"	UNION ALL	".
				"	SELECT useridx, moneyin + h_moneyin AS moneyin, moneyout + h_moneyout + jackpot  AS moneyout, (playcount+h_playcount) AS playcount ".  
				"	FROM t5_user_playstat_daily_amazon	". 
				"	WHERE dateadd(DAY, -8,  '$current_date') <= today AND today <= dateadd(DAY, -1,  '$current_date')	".
				"	) t1	". 
				"group by useridx having sum(playcount) >= 350 and case when sum(moneyin)::float =0 then 0 else sum(moneyout)::float/sum(moneyin)::float*100::float end >= 87";	
		$target_user_list = $db_redshift->gettotallist($sql);
		
		$sql = "";
		$insertcount = 0;
			
		for($i=0; $i<sizeof($target_user_list); $i++)
		{
			$useridx = $target_user_list[$i]["useridx"];
			$winrate = $target_user_list[$i]["winrate"];
			$avg_money_in = $target_user_list[$i]["avg_money_in"];
		
			if ($insertcount == 0)
			{
				$sql = "INSERT INTO tbl_target_user_tmp(useridx, mode, week_avg_moneyin, week_winrate, writedate) VALUES($useridx, 0, $avg_money_in, $winrate, NOW())";
		
				$insertcount++;
			}
			else if ($insertcount < 500)
			{
				$sql .= ", ($useridx, 0, $avg_money_in, $winrate, NOW())";
				$insertcount++;
			}
			else
			{
				$sql .= ", ($useridx, 0, $avg_money_in, $winrate, NOW());";
				$insertcount = 0;
		
				$db_main2->execute($sql);

				$sql = "";
			}
		}
		
		if($sql != "")
		{
			$db_main2->execute($sql);
		}
		
		//
		$sql = "SELECT useridx FROM tbl_target_user_tmp";
		$vip_nopay_tmp_list = $db_main2->gettotallist($sql);
		
		$update_sql = "";
		$updatecount = 0;
		
		$today = date("Y-m-d");
		
		write_log("target_user_2");
		
		$sql = "SELECT MAX(today) FROM tbl_user_buyer_leave_stat";
		$max_today_buyer_leave_stat = $db_other->getvalue($sql);
		
		for($i=0; $i<sizeof($vip_nopay_tmp_list); $i++)
		{
			$tmp_useridx = $vip_nopay_tmp_list[$i]["useridx"];
			
			$target_user_mode = 0;
								
			$sql = "SELECT buy_leave_day - LEAST(recent_avg_rebuyday, avg_rebuyday) AS check_buy_leave_mode FROM tbl_user_buyer_leave_stat WHERE today = '$max_today_buyer_leave_stat' AND useridx = $tmp_useridx";
			$buy_leave_value = $db_other->getvalue($sql);
			
			$sql = "SELECT recent_coin_p, recent_avg_buy_coin FROM tbl_user_buyer_leave_stat WHERE today = '$max_today_buyer_leave_stat' AND useridx = $tmp_useridx";
			$recent_info = $db_other->getarray($sql);
			
			$recent_coin_p = $recent_info["recent_coin_p"];
			$recent_avg_buy_coin = $recent_info["recent_avg_buy_coin"];	
			
			if($buy_leave_value == "")
				$buy_leave_value = 0;
			
			if($recent_coin_p == "")
				$recent_coin_p = 0;
			
			if($recent_avg_buy_coin == "")
				$recent_avg_buy_coin = 0;
							
			if ($updatecount == 0)
			{
				$update_sql = "INSERT INTO tbl_target_user_tmp(useridx, mode, buy_leave_value, coin_p, avg_buy_coin) ".
								"VALUES($tmp_useridx, $target_user_mode, $buy_leave_value, $recent_coin_p, $recent_avg_buy_coin)";

				$updatecount++;
			}
			else if ($updatecount < 50)
			{
				$update_sql .= ", ($tmp_useridx, $target_user_mode, $buy_leave_value, $recent_coin_p, $recent_avg_buy_coin)";
				$updatecount++;
			}
			else
			{
				$update_sql .= ", ($tmp_useridx, $target_user_mode, $buy_leave_value, $recent_coin_p, $recent_avg_buy_coin)";
				$update_sql .= " ON DUPLICATE KEY UPDATE mode = VALUES(mode), buy_leave_value=VALUES(buy_leave_value), coin_p=VALUES(coin_p), avg_buy_coin=VALUES(avg_buy_coin);";
				$updatecount = 0;
		
				$db_main2->execute($update_sql);
		
				$update_sql = "";
			}
		}
		
		if($update_sql != "")
		{
			$update_sql .= " ON DUPLICATE KEY UPDATE mode = VALUES(mode), buy_leave_value=VALUES(buy_leave_value), coin_p=VALUES(coin_p), avg_buy_coin=VALUES(avg_buy_coin);";
			
			$db_main2->execute($update_sql);
		}
		
		write_log("target_user_3");
		
		// Redshift 동기화 시간에 따른 결제자 체크		
		$sql = "UPDATE tbl_target_user ".
				"SET is_buy = 1, writedate = NOW() ".
				"WHERE NOT EXISTS (SELECT * FROM tbl_target_user_tmp WHERE useridx=tbl_target_user.useridx)";
		$db_main2->execute($sql);
	
		$sql = "INSERT INTO tbl_target_user(useridx, mode, week_avg_moneyin, week_winrate, coin_p, avg_buy_coin, buy_leave_value, is_buy, writedate) ".
				"SELECT useridx, mode, week_avg_moneyin, week_winrate, coin_p, avg_buy_coin, buy_leave_value, 0 AS is_buy, writedate ".
				"FROM tbl_target_user_tmp ".
				"ON DUPLICATE KEY UPDATE mode = VALUES(mode), week_avg_moneyin = VALUES(week_avg_moneyin), week_winrate = VALUES(week_winrate), week_winrate = VALUES(week_winrate), ".
				"coin_p = VALUES(coin_p), avg_buy_coin = VALUES(avg_buy_coin), buy_leave_value = VALUES(buy_leave_value), is_buy = VALUES(is_buy), writedate=VALUES(writedate);";
		$db_main2->execute($sql);
		
		$sql = "INSERT INTO tbl_target_user_history(useridx, mode, week_avg_moneyin, week_winrate, coin_p, avg_buy_coin, buy_leave_value, writedate) ".
				"SELECT useridx, mode, week_avg_moneyin, week_winrate, coin_p, avg_buy_coin, buy_leave_value, NOW() ".
				"FROM tbl_target_user_tmp";
		$db_main2->execute($sql);

	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
	$db_other->end();
	$db_redshift->end();
?>