<?
    include("../../common/common_include.inc.php");
	
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    $db_analysis = new CDatabase_Analysis();


	$db_main->execute("SET wait_timeout=43200");
	$db_main2->execute("SET wait_timeout=43200");
	$db_analysis->execute("SET wait_timeout=43200");
	
	ini_set("memory_limit", "-1");
	
	try
	{
	    $sdate = date("Y-m-d", time() - 24 * 60 * 60 * 5);
	    $edate = date("Y-m-d", time() + 24 * 60 * 60);

	    while($sdate < $edate)
	    {
	        $temp_date = date('Y-m-d', strtotime($sdate.' + 1 day'));
	        
	        $join_sdate = "$sdate";
	        $join_edate = "$temp_date";
	        
	        $write_sdate = $join_sdate." 00:00:00";
	        $write_edate = $join_edate." 00:00:00";

	        $sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, platform, DAYOFWEEK(writedate) AS today_week, ". 
                    "   (CASE WHEN product_type = 1 AND coupon_type = -1 THEN 'basic' ". // 기본 상품, 쿠폰 X
    	   	        "   WHEN product_type = 12 AND parent_coupon_more = 0 THEN 'basic' ". // 프라임 딜(쿠폰 X)
                    "   WHEN product_type = 13 AND parent_coupon_more = 0 THEN 'basic' ". // 프라임 딜 v2(쿠폰 X)
                    "   WHEN product_type = 1 AND coupon_type = 0 THEN 'coupon' ". // 기본 상품, 정기 쿠폰
                    "   WHEN product_type = 1 AND coupon_type = 1 THEN 'coupon' ". // 기본 상품, 100% 쿠폰
                    "   WHEN product_type = 1 AND coupon_type IN (2,3,4) THEN 'coupon' ". // 기본 상품, 시즌 쿠폰 
                    "   WHEN product_type = 1 AND coupon_type = 5 THEN 'coupon' ". // 기본 상품, 모바일 유도 쿠폰
                    "   WHEN product_type = 1 AND coupon_type IN (10, 11, 12) THEN 'coupon' ". // 기본 상품, 인센티브 쿠폰 
                    "   WHEN product_type = 12 AND parent_coupon_more > 0 THEN 'coupon' ". // 프라임 딜(쿠폰 O) 
                    "   WHEN product_type = 13 AND parent_coupon_more > 0 THEN 'coupon' ". // 프라임 딜 v2(쿠폰 O)
                    "   WHEN product_type = 3 AND coupon_type = -1 THEN 'offer' ". // TH 오퍼 
                    "   WHEN product_type = 6 AND coupon_type = -1 THEN 'offer' ". // 첫 결제 오퍼
                    "   WHEN product_type = 7 AND coupon_type = -1 THEN 'offer' ". // 럭키 오퍼 
                    "   WHEN product_type = 9 THEN 'offer' ". // 피기 팟 
                    "   WHEN product_type = 10 AND coupon_type = -1 THEN 'offer' ". // 결제 이탈자 오퍼 
                    "   WHEN product_type = 14 AND coupon_type = -1 THEN 'offer' ". // 기간제 오퍼
                    "   WHEN product_type = 15 AND coupon_type = -1 THEN 'offer' ". // 하이롤러 언락오퍼
                    "   WHEN product_type = 16 AND coupon_type = -1 THEN 'offer' ". // 어메이징 딜
                    "   WHEN product_type = 17 AND coupon_type = -1 THEN 'offer' ". // 기간제 오퍼(비결제자))
                    "   ELSE 'unknown' END) AS category_name, ROUND(SUM(money), 2) AS total_money, COUNT(DISTINCT useridx) AS payer_cnt, ".
                    "   COUNT(*) AS pay_cnt, ". 
                    "   ROUND(SUM(coin)/SUM(basecoin)*100, 2) AS more_rate, ".
                    "   SUM(basecoin) AS base_coin, ".
                    "   SUM(coin) AS buy_coin ".
                    "FROM ( ".
                    "   SELECT 0 AS platform, useridx, (facebookcredit/10) AS money, couponidx, special_more, special_discount, basecoin, coin, product_type, coupon_type, 0 AS parent_coupon_more, writedate ".
                    "   FROM tbl_product_order ".
                    "   WHERE useridx > 20000 AND STATUS IN (1, 3) AND '$write_sdate' <= writedate AND writedate < '$write_edate' ". 
                    "   UNION ALL ".
                    "   SELECT os_type AS platform, useridx, money, couponidx, special_more, special_discount, basecoin, coin, product_type, coupon_type, parent_coupon_more, writedate ".
                    "   FROM tbl_product_order_mobile ".
                    "   WHERE useridx > 20000 AND STATUS = 1 AND '$write_sdate' <= writedate AND writedate < '$write_edate' ". 
                    ") t1 ".
                    "GROUP BY platform, category_name, today ASC";
	        $order_list = $db_main->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($order_list); $i++)
	        {
	            $today = $order_list[$i]["today"];
	            $platform = $order_list[$i]["platform"];
	            $category = 0;
	            $category_name = $order_list[$i]["category_name"];
	            $today_week = $order_list[$i]["today_week"];
	            $total_money = $order_list[$i]["total_money"];
	            $payer_cnt = $order_list[$i]["payer_cnt"];
	            $pay_cnt = $order_list[$i]["pay_cnt"];
	            $base_coin = $order_list[$i]["base_coin"];
	            $buy_coin = $order_list[$i]["buy_coin"];
	            $more_rate = $order_list[$i]["more_rate"];
	            
	            $sql = "SELECT COUNT(*) FROM `tbl_coupon_promotion_setting` WHERE DATE(startdate) <= '$today' AND '$today' <= DATE(enddate)";
	            $is_sale = $db_main2->getvalue($sql);
	            
	            $sql = "INSERT INTO tbl_order_daily_stat(today, platform, category, category_name, today_week, is_sale, total_money, payer_cnt, pay_cnt, base_coin, buy_coin, more_rate) ".
                        "VALUES('$today', $platform, $category, '$category_name', $today_week, $is_sale, $total_money, $payer_cnt, $pay_cnt, $base_coin, $buy_coin, $more_rate) ".
                        "ON DUPLICATE KEY UPDATE is_sale=VALUES(is_sale), total_money=VALUES(total_money), payer_cnt=VALUES(payer_cnt), pay_cnt=VALUES(pay_cnt), base_coin=VALUES(base_coin), buy_coin=VALUES(buy_coin), more_rate=VALUES(more_rate)";
	            $db_analysis->execute($sql);
	        }
	        	        
	        $sdate = $temp_date;
	    }
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	}

	$db_main->end();
	$db_main2->end();
	$db_analysis->end();
?>