<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/purchase_stat_daily") !== false)
		{
			$count++;
		}
	}

	ini_set("memory_limit", "-1");
	
	$issuccess = "1";
	
	if ($count > 1)
		exit();
	
	$db_slave_main = new CDatabase_Slave_Main();
	$db_other = new CDatabase_Other();
	
	$db_slave_main->execute("SET wait_timeout=72000");
	$db_other->execute("SET wait_timeout=72000");
	
	function get_groupcode($freq, $pa)
	{
		if($freq < 3)
			$groupcode = 1;
		else if($freq < 10)
			$groupcode = 2;
		else
			$groupcode = 3;
	
		if($pa < 9) // $9미만
			$groupcode += 10;
		else if($pa < 39) // $39미만
			$groupcode += 20;
		else // $39이상
			$groupcode += 30;
	
		return $groupcode;
	}
	
	try
	{
		$today = date("Y-m-d");
		
		$sql = "SELECT t1.useridx, createdate, logindate, DATEDIFF(NOW(), createdate) AS dayafterinstall ".
				"FROM ( ".
				"	SELECT DISTINCT useridx ".
				"	FROM tbl_product_order ".
				"	WHERE useridx > 20000 AND status = 1 AND facebookcredit >= 50 ".
				"	UNION ALL ".
				"	SELECT DISTINCT useridx ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE useridx > 20000 AND STATUS = 1 AND money >= 4.99 ".
				") t1 JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx";
		
		$user_list = $db_slave_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($user_list); $i++)
		{
			$useridx = $user_list[$i]["useridx"];
			$createdate = $user_list[$i]["createdate"];
			$logindate = $user_list[$i]["logindate"];
			$dayafterinstall = ($user_list[$i]["dayafterinstall"]=="")?0:$user_list[$i]["dayafterinstall"];
			
			$sql = "SELECT coin FROM tbl_user WHERE useridx = $useridx";
			$user_coin = $db_slave_main->getvalue($sql);
			$user_coin = ($user_coin == "")? 0 : $user_coin;
			
			// LifeTime			
			$sql = "SELECT SUM(pa) AS pa, SUM(freq) AS freq, SUM(threshold) AS threshold, SUM(cnt_pay) AS cnt_pay  ".
					"FROM ".
					"(	 ".
					"	SELECT IFNULL(ROUND(AVG(money/10), 2), 0) AS pa, FLOOR(COUNT(orderidx)*28/$dayafterinstall) AS freq, 0 AS threshold, COUNT(orderidx) AS cnt_pay  ".
					"	FROM ".
					"	( ".
					"		SELECT facebookcredit AS money, orderidx ".
					"		FROM tbl_product_order ".
					"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 ".
					"		UNION ALL ".
					"		SELECT ROUND(money*10) AS money, orderidx ".
					"		FROM tbl_product_order_mobile ".
					"		WHERE useridx = $useridx AND STATUS = 1 AND money >= 4.99 ".
					"	) t1 ".
					"	UNION ALL ".
					"	SELECT 0 AS pa, 0 AS freq, IFNULL(ROUND(AVG(usercoin/coin)*100), 10) AS threshold, 0 AS cnt_pay ".
					"	FROM ".
					"	( ".
					"		SELECT usercoin, coin ".
					"		FROM tbl_product_order ".
					"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND usercoin < coin ".
					"		UNION ALL ".
					"		SELECT usercoin, coin ".
					"		FROM tbl_product_order_mobile ".
					"		WHERE useridx = $useridx AND STATUS = 1 AND money >= 4.99 AND usercoin < coin ".
					"	) t2 ".
					") t3";
			$pay_info_lt = $db_slave_main->getarray($sql);
			
			$pa_lt = $pay_info_lt["pa"];
			$freq_lt = $pay_info_lt["freq"];
			$threshold_lt = $pay_info_lt["threshold"];
			$groupcode_lt = get_groupcode($freq_lt, $pa_lt);
			
			$stat_type = 1;
				
			$sql = "INSERT IGNORE INTO tbl_payer_stat_daily(today, useridx, type, pa, freq, threshold, groupcode, coin, createdate, logindate) ".
					"VALUES('$today', $useridx, $stat_type, $pa_lt, $freq_lt, $threshold_lt, $groupcode_lt, $user_coin, '$createdate', '$logindate')";
			$db_other->execute($sql);
			
			// 4 week			
			$sql = "SELECT SUM(pa) AS pa, SUM(freq) AS freq, SUM(threshold) AS threshold ".
					"FROM ".
					"(	 ".
					"	SELECT IFNULL(ROUND(AVG(money/10), 2), 0) AS pa, COUNT(orderidx) AS freq, 0 AS threshold ".
					"	FROM ".
					"	( ".
					"		SELECT facebookcredit AS money, orderidx ".
					"		FROM tbl_product_order ".
					"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND DATE_SUB(NOW(), INTERVAL 28 DAY) <= writedate ".
					"		UNION ALL ".
					"		SELECT ROUND(money*10) AS money, orderidx ".
					"		FROM tbl_product_order_mobile ".
					"		WHERE useridx = $useridx AND STATUS = 1 AND money >= 4.99 AND DATE_SUB(NOW(), INTERVAL 28 DAY) <= writedate ".
					"	) t1 ".
					"	UNION ALL ".
					"	SELECT 0 AS pa, 0 AS freq, IFNULL(ROUND(AVG(usercoin/coin)*100), 10) AS threshold ".
					"	FROM ".
					"	( ".
					"		SELECT usercoin, coin ".
					"		FROM tbl_product_order ".
					"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND DATE_SUB(NOW(), INTERVAL 28 DAY) <= writedate AND usercoin < coin ".
					"		UNION ALL ".
					"		SELECT usercoin, coin ".
					"		FROM tbl_product_order_mobile ".
					"		WHERE useridx = $useridx AND STATUS = 1 AND money >= 4.99 AND DATE_SUB(NOW(), INTERVAL 28 DAY) <= writedate AND usercoin < coin ".
					"	) t2 ".
					") t3";
			$pay_info_4w = $db_slave_main->getarray($sql);
			
			$pa_4w = $pay_info_4w["pa"];
			$freq_4w = $pay_info_4w["freq"];
			$threshold_4w = $pay_info_4w["threshold"];
			$groupcode_4w = get_groupcode($freq_4w, $pa_4w);
			
			$stat_type = 2;
			
			$sql = "INSERT IGNORE INTO tbl_payer_stat_daily(today, useridx, type, pa, freq, threshold, groupcode, coin, createdate, logindate) ".
					"VALUES('$today', $useridx, $stat_type, $pa_4w, $freq_4w, $threshold_4w, $groupcode_4w, $user_coin, '$createdate', '$logindate')";
			$db_other->execute($sql);
			
			// 1 week
			$sql = "SELECT SUM(pa) AS pa, SUM(freq) AS freq, SUM(threshold) AS threshold ".
					"FROM ".
					"(	 ".
					"	SELECT IFNULL(ROUND(AVG(money/10), 2), 0) AS pa, COUNT(orderidx)*4 AS freq, 0 AS threshold ".
					"	FROM ".
					"	( ".
					"		SELECT facebookcredit AS money, orderidx ".
					"		FROM tbl_product_order ".
					"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND DATE_SUB(NOW(), INTERVAL 7 DAY) <= writedate ".
					"		UNION ALL ".
					"		SELECT ROUND(money*10) AS money, orderidx ".
					"		FROM tbl_product_order_mobile ".
					"		WHERE useridx = $useridx AND STATUS = 1 AND money >= 4.99 AND DATE_SUB(NOW(), INTERVAL 7 DAY) <= writedate ".
					"	) t1 ".
					"	UNION ALL ".
					"	SELECT 0 AS pa, 0 AS freq, IFNULL(ROUND(AVG(usercoin/coin)*100), 10) AS threshold ".
					"	FROM ".
					"	( ".
					"		SELECT usercoin, coin ".
					"		FROM tbl_product_order ".
					"		WHERE useridx = $useridx AND STATUS = 1 AND facebookcredit >= 50 AND  DATE_SUB(NOW(), INTERVAL 7 DAY) <= writedate AND usercoin < coin ".
					"		UNION ALL ".
					"		SELECT usercoin, coin ".
					"		FROM tbl_product_order_mobile ".
					"		WHERE useridx = $useridx AND STATUS = 1 AND money >= 4.99 AND  DATE_SUB(NOW(), INTERVAL 7 DAY) <= writedate AND usercoin < coin ".
					"	) t2 ".
					") t3";
			$pay_info_1w = $db_slave_main->getarray($sql);
			$pay_info_1w = $db_slave_main->getarray($sql);
			
			$pa_1w = $pay_info_1w["pa"];
			$freq_1w = $pay_info_1w["freq"];
			$threshold_1w = $pay_info_1w["threshold"];
			$groupcode_1w = get_groupcode($freq_1w, $pa_1w);
			
			$stat_type = 3;
			
			$sql = "INSERT IGNORE INTO tbl_payer_stat_daily(today, useridx, type, pa, freq, threshold, groupcode, coin, createdate, logindate) ".
					"VALUES('$today', $useridx, $stat_type, $pa_1w, $freq_1w, $threshold_1w, $groupcode_1w, $user_coin, '$createdate', '$logindate')";
			$db_other->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	$db_slave_main->end();
	$db_other->end();
?>
