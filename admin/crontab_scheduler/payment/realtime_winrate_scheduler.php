<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/realtime_winrate_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
		exit();

	$db_main2 = new CDatabase_Main2();
	
	try 
	{
		$to = "plat@doubleugames.com,chobh1244@doubleugames.com,kgun@afewgoodsoft.com,jong1kwan@afewgoodsoft.com";
		$from = "plat@doubleugames.com";
		
		$title = "[Take5] 승률 급등 슬롯";
		$contents = "";
		
		//조건 : 승률 130%이상, 게임횟수 : 10만초과
		//4:tutorial, 9:Ultra Freespin, 26:multi jackpot, 29:Daily Freespin, 31:부양중스킵(Push Freespin)
		//Web
		$sql = "SELECT slottype, (SELECT slotname FROM tbl_slot_list WHERE slottype = t1.slottype) AS slotname, ".
				"SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, SUM(playcount) AS playcount, ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS winrate ".
				"FROM tbl_game_cash_stats_daily t1 ".
				"WHERE writedate > DATE_SUB(NOW(), INTERVAL 2 DAY) AND mode NOT IN (4, 5, 9, 26, 29, 31) and slottype NOT IN (35,44,43)  GROUP BY slottype HAVING winrate >= 130 AND playcount > 200000";
		$web_list = $db_main2->gettotallist($sql);
		
		//ios
		$sql = "SELECT slottype, (SELECT slotname FROM tbl_slot_list WHERE slottype = t1.slottype) AS slotname, ".
				"SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, SUM(playcount) AS playcount, ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS winrate ".
				"FROM tbl_game_cash_stats_ios_daily t1 ".
				"WHERE writedate > DATE_SUB(NOW(), INTERVAL 2 DAY) AND mode NOT IN (4, 5, 9, 26, 29, 31) and slottype NOT IN (35,44,43)  GROUP BY slottype HAVING winrate >= 130 AND playcount > 200000";
		$ios_list = $db_main2->gettotallist($sql);
		
		//android
		$sql = "SELECT slottype, (SELECT slotname FROM tbl_slot_list WHERE slottype = t1.slottype) AS slotname, ".
				"SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, SUM(playcount) AS playcount, ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS winrate ".
				"FROM tbl_game_cash_stats_android_daily t1 ".
				"WHERE writedate > DATE_SUB(NOW(), INTERVAL 2 DAY) AND mode NOT IN (4, 5, 9, 26, 29, 31) and slottype NOT IN (35,44,43)  GROUP BY slottype HAVING winrate >= 130 AND playcount > 200000";
		$android_list = $db_main2->gettotallist($sql);
		
		//amazon
		$sql = "SELECT slottype, (SELECT slotname FROM tbl_slot_list WHERE slottype = t1.slottype) AS slotname, ".
				"SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, SUM(playcount) AS playcount, ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS winrate ".
				"FROM tbl_game_cash_stats_amazon_daily t1 ".
				"WHERE writedate > DATE_SUB(NOW(), INTERVAL 2 DAY) AND mode NOT IN (4, 5, 9, 26, 29, 31) and slottype NOT IN (35,44,43)  GROUP BY slottype HAVING winrate >= 130 AND playcount > 100000";
		$amazon_list = $db_main2->gettotallist($sql);
		
		
		// web
		if(sizeof($web_list) > 0)
		{
		    $contents .= "[Web]<br/>";
		    
		    for($i=0;$i<sizeof($web_list);$i++)
		    {
		        $slotname = $web_list[$i]["slotname"];
		        $moneyin = $web_list[$i]["moneyin"];
		        $moneyout = $web_list[$i]["moneyout"];
		        $playcount = $web_list[$i]["playcount"];
		        $winrate = $web_list[$i]["winrate"];
		        $slottype = $web_list[$i]["slottype"];
		        
		        $contents .= $slotname." winrate : ".$winrate."%<br/>";
		        
		        
		        $sql="	SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, SUM(playcount) AS playcount,
							SUM(moneyout) - SUM(moneyin) AS winamount, ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS winrate
						FROM tbl_game_cash_stats_daily
						WHERE  writedate > DATE_SUB(NOW(), INTERVAL 2 DAY) AND mode NOT IN (4, 5, 9, 26, 29, 31)
						AND slottype = $slottype
						GROUP BY today";
		        $cash_stats_info = $db_main2->gettotallist($sql);
		        
		        for($j=0; $j<sizeof($cash_stats_info); $j++)
		        {
		            $today = $cash_stats_info[$j]["today"];
		            $moneyin = $cash_stats_info[$j]["moneyin"];
		            $moneyout = $cash_stats_info[$j]["moneyout"];
		            $playcount = $cash_stats_info[$j]["playcount"];
		            $winamount = $cash_stats_info[$j]["winamount"];
		            $winrate = $cash_stats_info[$j]["winrate"];
		            
		            $contents .= "Today : ".$today." Slottype : ".number_format($slottype)." Slotname : ".$slotname." moneyin : ".number_format($moneyin)." moneyout : ".number_format($moneyout)
		            ." playcount :".number_format($playcount)." winamount : ".number_format($winamount)." winrate : ".$winrate."%<br/>";
		            if($j == sizeof($cash_stats_info) - 1)
		            {
		                $contents .= 	"<br/><br/>";
		            }
		        }
		        
		    }
		}
		
		// ios
		if(sizeof($ios_list) > 0)
		{
		    $contents .= "[IOS]<br/>";
		    
		    for($i=0;$i<sizeof($ios_list);$i++)
		    {
		        $slotname = $ios_list[$i]["slotname"];
		        $moneyin = $ios_list[$i]["moneyin"];
		        $moneyout = $ios_list[$i]["moneyout"];
		        $playcount = $ios_list[$i]["playcount"];
		        $winrate = $ios_list[$i]["winrate"];
		        $slottype = $ios_list[$i]["slottype"];
		        
		        $contents .= $slotname." winrate : ".$winrate."%<br/>";
		        
		        
		        $sql="	SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, SUM(playcount) AS playcount,
							SUM(moneyout) - SUM(moneyin) AS winamount, ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS winrate
						FROM tbl_game_cash_stats_ios_daily
						WHERE  writedate > DATE_SUB(NOW(), INTERVAL 2 DAY) AND mode NOT IN (4, 5, 9, 26, 29, 31)
						AND slottype = $slottype
						GROUP BY today";
		        $cash_stats_info = $db_main2->gettotallist($sql);
		        
		        for($j=0; $j<sizeof($cash_stats_info); $j++)
		        {
		            $today = $cash_stats_info[$j]["today"];
		            $moneyin = $cash_stats_info[$j]["moneyin"];
		            $moneyout = $cash_stats_info[$j]["moneyout"];
		            $playcount = $cash_stats_info[$j]["playcount"];
		            $winamount = $cash_stats_info[$j]["winamount"];
		            $winrate = $cash_stats_info[$j]["winrate"];
		            
		            $contents .= "Today : ".$today." Slottype : ".number_format($slottype)." Slotname : ".$slotname." moneyin : ".number_format($moneyin)." moneyout : ".number_format($moneyout)
		            ." playcount :".number_format($playcount)." winamount : ".number_format($winamount)." winrate : ".$winrate."%<br/>";
		            if($j == sizeof($cash_stats_info) - 1)
		            {
		                $contents .= 	"<br/><br/>";
		            }
		        }
		        
		    }
		}
		
		// android
		if(sizeof($android_list) > 0)
		{
		    $contents .= "[Android]<br/>";
		    
		    for($i=0;$i<sizeof($android_list);$i++)
		    {
		        $slotname = $android_list[$i]["slotname"];
		        $moneyin = $android_list[$i]["moneyin"];
		        $moneyout = $android_list[$i]["moneyout"];
		        $playcount = $android_list[$i]["playcount"];
		        $winrate = $android_list[$i]["winrate"];
		        $slottype = $android_list[$i]["slottype"];
		        
		        $contents .= $slotname." winrate : ".$winrate."%<br/>";
		        
		        
		        $sql="	SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, SUM(playcount) AS playcount,
							SUM(moneyout) - SUM(moneyin) AS winamount, ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS winrate
						FROM tbl_game_cash_stats_android_daily
						WHERE  writedate > DATE_SUB(NOW(), INTERVAL 2 DAY) AND mode NOT IN (4, 5, 9, 26, 29, 31)
						AND slottype = $slottype
						GROUP BY today";
		        $cash_stats_info = $db_main2->gettotallist($sql);
		        
		        for($j=0; $j<sizeof($cash_stats_info); $j++)
		        {
		            $today = $cash_stats_info[$j]["today"];
		            $moneyin = $cash_stats_info[$j]["moneyin"];
		            $moneyout = $cash_stats_info[$j]["moneyout"];
		            $playcount = $cash_stats_info[$j]["playcount"];
		            $winamount = $cash_stats_info[$j]["winamount"];
		            $winrate = $cash_stats_info[$j]["winrate"];
		            
		            $contents .= "Today : ".$today." Slottype : ".number_format($slottype)." Slotname : ".$slotname." moneyin : ".number_format($moneyin)." moneyout : ".number_format($moneyout)
		            ." playcount :".number_format($playcount)." winamount : ".number_format($winamount)." winrate : ".$winrate."%<br/>";
		            if($j == sizeof($cash_stats_info) - 1)
		            {
		                $contents .= 	"<br/><br/>";
		            }
		        }
		        
		    }
		}
		
		// amazon
		if(sizeof($amazon_list) > 0)
		{
		    $contents .= "[Amazon]<br/>";
		    
		    for($i=0;$i<sizeof($amazon_list);$i++)
		    {
		        $slotname = $amazon_list[$i]["slotname"];
		        $moneyin = $amazon_list[$i]["moneyin"];
		        $moneyout = $amazon_list[$i]["moneyout"];
		        $playcount = $amazon_list[$i]["playcount"];
		        $winrate = $amazon_list[$i]["winrate"];
		        $slottype = $amazon_list[$i]["slottype"];
		        
		        $contents .= $slotname." winrate : ".$winrate."%<br/>";
		        
		        
		        $sql="	SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, SUM(playcount) AS playcount,
							SUM(moneyout) - SUM(moneyin) AS winamount, ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS winrate
						FROM tbl_game_cash_stats_amazon_daily
						WHERE  writedate > DATE_SUB(NOW(), INTERVAL 2 DAY) AND mode NOT IN (4, 5, 9, 26, 29, 31)
						AND slottype = $slottype
						GROUP BY today";
		        $cash_stats_info = $db_main2->gettotallist($sql);
		        
		        for($j=0; $j<sizeof($cash_stats_info); $j++)
		        {
		            $today = $cash_stats_info[$j]["today"];
		            $moneyin = $cash_stats_info[$j]["moneyin"];
		            $moneyout = $cash_stats_info[$j]["moneyout"];
		            $playcount = $cash_stats_info[$j]["playcount"];
		            $winamount = $cash_stats_info[$j]["winamount"];
		            $winrate = $cash_stats_info[$j]["winrate"];
		            
		            $contents .= "Today : ".$today." Slottype : ".number_format($slottype)." Slotname : ".$slotname." moneyin : ".number_format($moneyin)." moneyout : ".number_format($moneyout)
		            ." playcount :".number_format($playcount)." winamount : ".number_format($winamount)." winrate : ".$winrate."%<br/>";
		            if($j == sizeof($cash_stats_info) - 1)
		            {
		                $contents .= 	"<br/><br/>";
		            }
		        }
		    }
		}
		
		if($web_list != null || $ios_list != null || $and_list != null || $ama_list != null)
			sendmail($to, $from, $title, $contents);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
?>
