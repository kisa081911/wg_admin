<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/user_week_pay_log") !== false)
		{
			$count++;
		}
	}

	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/user_week_pay_log") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/user_week_pay_log") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("payment/user_week_pay_log Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	
	$issuccess = "1";
	
	if ($count > 1)
		exit();
	
	$db_slave_main = new CDatabase_Slave_Main();
	$db_livestats = new CDatabase_Livestats();
	$db_other = new CDatabase_Other();
	
	// 유저별 결제 데이터 저장 
	try
	{
		$today = date("Y-m-d");
		
		$sdate = get_past_date($today,91,"d");
		$edate = $today;

		while($sdate < $edate)
		{
			$temp_date = date('Y-m-d', strtotime($sdate.' + 1 day'));

			$sql = "DELETE FROM tbl_user_week_pay_log WHERE '$sdate 00:00:00' <= purchasedate AND purchasedate < '$temp_date 00:00:00'";
			$db_livestats->execute($sql);
			
			$sql = "SELECT purchasedate, t2.useridx, platform, adflag, DATE_FORMAT(createdate, '%Y-%m-%d') AS createdate, money ".
					"FROM ( ".
					"	SELECT useridx, purchasedate, ROUND(SUM(money), 2) AS money ".
					"	FROM ( ".
					"		SELECT useridx, SUM(facebookcredit)/10 AS money, DATE_FORMAT(writedate, '%Y-%m-%d') AS purchasedate ".
					"		FROM tbl_product_order ".
					"		WHERE useridx > 20000 AND status IN (1, 3) AND '$sdate 00:00:00' <= writedate AND writedate < '$temp_date 00:00:00' ".
					"		GROUP BY useridx ".
					"		UNION ALL ".
					"		SELECT useridx, money, DATE_FORMAT(writedate, '%Y-%m-%d') AS purchasedate ".
					"		FROM tbl_product_order_mobile ".
					"		WHERE useridx > 20000 AND status = 1 AND '$sdate 00:00:00' <= writedate AND writedate < '$temp_date 00:00:00' ".
					"		GROUP BY useridx ".
					"	) t1 ".
					"GROUP BY useridx, purchasedate ".
					") t2 JOIN tbl_user_ext t3 ON t2.useridx = t3.useridx ".
					"WHERE DATEDIFF('$sdate 00:00:00', createdate) <= 365";
			
			$week_pay_list = $db_slave_main->gettotallist($sql);
			
			$sql = "";
			$insertcount = 0;
			
			for($i=0; $i<sizeof($week_pay_list); $i++)
			{
				$purchasedate = $week_pay_list[$i]["purchasedate"];
				$useridx = $week_pay_list[$i]["useridx"];
				$platform = $week_pay_list[$i]["platform"];
				$adflag = $week_pay_list[$i]["adflag"];
				$createdate = $week_pay_list[$i]["createdate"];
				$money = $week_pay_list[$i]["money"];
					
				if ($insertcount == 0)
				{
					$sql = "INSERT INTO tbl_user_week_pay_log(purchasedate, useridx, category, adflag, createdate, money) VALUES('$purchasedate', $useridx, $platform, '$adflag', '$createdate', $money)";
						
					$insertcount++;
				}
				else if ($insertcount < 200)
				{
					$sql .= ", ('$purchasedate', $useridx, $platform, '$adflag', '$createdate', $money)";
					$insertcount++;
				}
				else
				{
					$sql .= ", ('$purchasedate', $useridx, $platform, '$adflag', '$createdate', $money) ON DUPLICATE KEY UPDATE money=VALUES(money);";
					$insertcount = 0;
						
					$db_livestats->execute($sql);
						
					$sql = "";
				}
			}
			
			if($sql != "")
			{
				$sql .= " ON DUPLICATE KEY UPDATE money=VALUES(money);";
				$db_livestats->execute($sql);
			}

			$sdate = $temp_date;
		}

		// 주별 결제  통계
		$sql = "SELECT createdate, category, adflag, FLOOR(DATEDIFF(purchasedate, createdate)/7) AS week_after_install, SUM(money) AS money ".
				"FROM tbl_user_week_pay_log ".
				"WHERE purchasedate >= DATE_SUB(NOW(), INTERVAL 1 YEAR) ".
				"GROUP BY createdate, category, adflag, week_after_install";
		
		$daily_stat_list = $db_livestats->gettotallist($sql);
		
		$sql = "";
		$insertcount = 0;
		
		for($i=0; $i<sizeof($daily_stat_list); $i++)
		{
			$createdate = $daily_stat_list[$i]["createdate"];
			$category = $daily_stat_list[$i]["category"];
			$adflag = $daily_stat_list[$i]["adflag"];
			$money = $daily_stat_list[$i]["money"];
			$week_after_install = $daily_stat_list[$i]["week_after_install"];

			if ($insertcount == 0)
			{
				$sql = "INSERT INTO tbl_user_week_pay_log_stat(createdate, category, adflag, week_after_install, money) VALUES('$createdate', $category, '$adflag', $week_after_install, $money) ";
				$insertcount++;
			}
			else if ($insertcount < 200)
			{
				$sql .= ", ('$createdate', $category, '$adflag', $week_after_install, $money)";
				$insertcount++;
			}
			else
			{
				$sql .= ", ('$createdate', $category, '$adflag', $week_after_install, $money) ON DUPLICATE KEY UPDATE money=VALUES(money);";
				$insertcount = 0;
			
				$db_livestats->execute($sql);
			
				$sql = "";
			}
		}
		
		if($sql != "")
		{
			$sql .= " ON DUPLICATE KEY UPDATE money=VALUES(money);";
			$db_livestats->execute($sql);
		}

		$sql = "SELECT createdate, category, adflag, week_after_install, SUM(money) AS money ".
				"FROM tbl_user_week_pay_log_stat ".
				"WHERE createdate >= DATE_SUB(NOW(), INTERVAL 1 YEAR) AND 0 <= week_after_install AND week_after_install <= 51 ".
				"GROUP BY createdate, category, adflag, week_after_install";
		
		$week_stat_list = $db_livestats->gettotallist($sql);
		
		for($i=0; $i<sizeof($week_stat_list); $i++)
		{
			$createdate = $week_stat_list[$i]["createdate"];
			$category = $week_stat_list[$i]["category"];
			$adflag = $week_stat_list[$i]["adflag"];
			$money = $week_stat_list[$i]["money"];
			$week_after_install = $week_stat_list[$i]["week_after_install"];
				
			$sql = "INSERT INTO tbl_user_week_pay_daily(createdate, category, adflag, week_$week_after_install) ".
					"VALUES('$createdate', $category, '$adflag', $money) ".
					"ON DUPLICATE KEY UPDATE week_$week_after_install=VALUES(week_$week_after_install)";
	
			$db_other->execute($sql);
		}

		$yesterday = get_past_date($today,1,"d");
		
		$sql = "SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS today, platform, adflag, COUNT(useridx) AS cnt ".
				"FROM tbl_user_ext ".
				"WHERE createdate >= '$yesterday 00:00:00' AND createdate < '$today 00:00:00' ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m-%d'), platform, adflag";

		$join_list = $db_slave_main->gettotallist($sql);
			
		for($i=0; $i<sizeof($join_list); $i++)
		{
			$join_date = $join_list[$i]["today"];
			$join_platform = $join_list[$i]["platform"];
			$join_adflag = $join_list[$i]["adflag"];
			$join_cnt = $join_list[$i]["cnt"];
		
			$sql = "INSERT INTO tbl_user_week_pay_daily(createdate, category, adflag, join_count) VALUES('$join_date', $join_platform, '$join_adflag', $join_cnt) ON DUPLICATE KEY UPDATE join_count=VALUES(join_count);";
			$db_other->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	$db_slave_main->end();
	$db_livestats->end();
	$db_other->end();
?>
