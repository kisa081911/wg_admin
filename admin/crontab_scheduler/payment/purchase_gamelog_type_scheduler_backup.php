<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/purchase_gamelog_type_scheduler") !== false)
		{
			$count++;
		}
	}	
	
	$issuccess = "1";
	
	if ($count > 1)
		exit();
	
	$db_redshift = new CDatabase_Redshift();
	
	$today = date("Y-m-d");
	$before_4week = date("Y-m-d", time() - 60 * 60 * 24 * 7 * 4);
	$before_12week = date("Y-m-d", time() - 60 * 60 * 24 * 7 * 12);
	$before_24week = date("Y-m-d", time() - 60 * 60 * 24 * 7 * 24);
	$before_52week = date("Y-m-d", time() - 60 * 60 * 24 * 7 * 52);
	
	for($i=1; $i<3; $i++)
	{	
		$type = $i;
		
		write_log("Purchase_Gamelog_Type ".$type." Scheduler Start ( Redshift )");
		
		$pay_sql = "";
		$writedate_sql = "";
	
		if($i==1)
		{
			$pay_sql = "where t2.writedate >= '$before_4week 00:00:00'";
			$writedate_sql = "1=1 and writedate >= '$before_4week 00:00:00'";
		}
		else if($i == 2)
		{
			$pay_sql = "where t2.writedate >= '$before_12week 00:00:00'";
			$writedate_sql = "1=1 and writedate >= '$before_12week 00:00:00'";
		}
		else if($i == 3)
		{
			$pay_sql = "where t2.writedate >= '$before_24week 00:00:00'";
			$writedate_sql = "1=1 and writedate >= '$before_24week 00:00:00'";
		}
		else if($i == 4)
		{
			$pay_sql = "where t2.writedate >= '$before_52week 00:00:00'";
			$writedate_sql = "1=1 and writedate >= '$before_52week 00:00:00'";
		}
		else if($i == 5)
		{
			$pay_sql = "";
			$writedate_sql = "1=1";
		}
	
		// 어제 데이터 쌓기
		$sql = "insert into t5_user_purchase_gamelog_type ".
				"(useridx, type, avg_user_coin, avg_product_coin, total_buy_coin, total_money, avg_buy_money, avg_buy_more, avg_rebuyday, couponbuy_rate, basicbuy_rate, buy_count, money_in, money_out, playcount, web_purchase_rate, ios_purchase_rate, android_purchase_rate, amazon_purchase_rate, writedate) ".
	    		"select ".
	          	"	t4.useridx, type, weight_avg_user_coin, weight_avg_product_coin, total_product_coin, total_money, avg_buy_money, weight_avg_more_rate, weight_avg_rebuydate, coupon_buy_rate, basic_buy_rate, ". 
	          	"	buy_count, nvl(money_in, 0) as money_in, nvl(money_out, 0) as money_out, nvl(playcount, 0) as playcount, web_purchase_rate, ios_purchase_rate, android_purchase_rate, amazon_purchase_rate, trunc(getdate()) ".
	    		"from ( ".
	        	"	select ". 
	            "		t3.useridx, type, ".
	            "		(sum(weight_avg_user_coin)::float/sum(money)::float) as weight_avg_user_coin, ".
	            "		avg(product_coin) as weight_avg_product_coin, ".
	            "		sum(product_coin) as total_product_coin, ". 
	            "		sum(money) as total_money, ".
	            "		avg(money) as avg_buy_money, ".
	            "		(case when sum(weight_avg_more_rate) = 0 then 0 else (sum(weight_avg_more_rate)::float/sum(money)::float) end) as weight_avg_more_rate, ".
	            "		(case when sum(weight_avg_rebuydate) = 0 then 0 else (sum(weight_avg_rebuydate)::float/sum(money)::float) end) as weight_avg_rebuydate, ".
	            "		(case when sum(coupon_product_coin) = 0 then 0 else (sum(coupon_product_coin)::float/sum(product_coin)::float)*100 end) as coupon_buy_rate, ".
	            "		(case when sum(basic_buy_coin) = 0 then 0 else (sum(basic_buy_coin)::float/sum(product_coin)::float)*100 end) as basic_buy_rate, ".
	            "		count(money) as buy_count, ".
	            "		(case when sum(web_money) = 0 then 0 else ((sum(web_money)::float/sum(money)::float)*100) end) as web_purchase_rate, ". 
	            "		(case when sum(ios_money) = 0 then 0 else ((sum(ios_money)::float/sum(money)::float)*100) end) as ios_purchase_rate, ". 
	            "		(case when sum(android_money) = 0 then 0 else ((sum(android_money)::float/sum(money)::float)*100) end) as android_purchase_rate, ". 
	            "		(case when sum(amazon_money) = 0 then 0 else ((sum(amazon_money)::float/sum(money)::float)*100) end) as amazon_purchase_rate ".
	        	"	from ( ".
	            "		select t1.useridx, $type as type, t2.platform, t2.user_coin, t2.product_coin, money, is_coupon, more_rate, ".
	            "			(money * t2.product_coin) as weight_avg_product_coin, ".
	            "			(money * t2.user_coin) as weight_avg_user_coin, ".
	            "			(money * more_rate) as weight_avg_more_rate, ".
	            "			(case when datediff(day, writedate, rebuydate) < 0 then 0 else (money * datediff(day, writedate, rebuydate)) end) as weight_avg_rebuydate, ".       
	            "			(case when datediff(day, writedate, rebuydate) < 0 then 0 else datediff(day, writedate, rebuydate) end) as rebuydate, ".
	            "			(case when is_coupon = 1 then product_coin else 0 end) as coupon_product_coin, ".
	            "			(case when base_coin = product_coin then product_coin else 0 end) as basic_buy_coin, ".
	            "			(case when platform = 0  then money else 0 end) as web_money, ".
	            "			(case when platform = 1  then money else 0 end) as ios_money, ".
	            "			(case when platform = 2  then money else 0 end) as android_money, ".
	            "			(case when platform = 3  then money else 0 end) as amazon_money ".
	            "		from ( ".
	            "			select useridx from t5_user where useridx > 20000 and logindate >= '$before_4week 00:00:00'  ".
	           	"		) t1 join t5_user_purchase_log t2 on t1.useridx = t2.useridx ".
	            "		$pay_sql ".
	        	"	) t3 ".
	        	"	group by t3.useridx, type ".
	    		") t4 left join ( ".
	        	"	select useridx, sum(money_in) as money_in, sum(money_out) as money_out, sum(playcount) as playcount ".
	        	"	from ( ".
	            "		select useridx, slottype, money_in, money_out, playcount from t5_user_gamelog where mode not in (4, 9, 26) AND $writedate_sql ".
	            "		union all ".
	            "		select useridx, slottype, money_in, money_out, playcount from t5_user_gamelog_ios where mode not in (4, 9, 26) AND  $writedate_sql ".
	            "		union all ".
	            "		select useridx, slottype, money_in, money_out, playcount from t5_user_gamelog_android where mode not in (4, 9, 26) AND  $writedate_sql ".
	            "		union all ".
	            "		select useridx, slottype, money_in, money_out, playcount from t5_user_gamelog_amazon where mode not in (4, 9, 26) AND  $writedate_sql ".
	        	"	) sub ".
	        	"	group by useridx ".
	    		") t5 on t4.useridx = t5.useridx ".
	    		"group by t4.useridx, type, weight_avg_user_coin, weight_avg_product_coin, total_product_coin, total_money, avg_buy_money, weight_avg_more_rate, weight_avg_rebuydate, coupon_buy_rate, basic_buy_rate, buy_count, ". 
	            "money_in, money_out, playcount, web_purchase_rate, ios_purchase_rate, android_purchase_rate, amazon_purchase_rate ";
		
		$db_redshift->execute($sql);
		
		write_log("Purchase_Gamelog_Type ".$type." Scheduler End ( Redshift )");
	
	}
	
	$db_otherdb = new CDatabase_Other();
	
	$db_otherdb->execute("SET wait_timeout=72000");
	
	$sql = "TRUNCATE TABLE tbl_user_purchase_gamelog_type";
	$db_otherdb->execute($sql);
	
	for($i=1; $i<3; $i++)
	{
		$type = $i;
	
		write_log("Purchase_Gamelog_Type ".$type." Scheduler Start ( OtherDB )");
	
		$pay_sql = "";
		$writedate_sql = "";
	
		if($i==1)
		{
			$pay_sql = "where t2.writedate >= '$before_4week 00:00:00'";
			$writedate_sql = "1=1 and writedate >= '$before_4week 00:00:00'";
		}
		else if($i == 2)
		{
			$pay_sql = "where t2.writedate >= '$before_12week 00:00:00'";
			$writedate_sql = "1=1 and writedate >= '$before_12week 00:00:00'";
		}
		else if($i == 3)
		{
			$pay_sql = "where t2.writedate >= '$before_24week 00:00:00'";
			$writedate_sql = "1=1 and writedate >= '$before_24week 00:00:00'";
		}
		else if($i == 4)
		{
			$pay_sql = "where t2.writedate >= '$before_52week 00:00:00'";
			$writedate_sql = "1=1 and writedate >= '$before_52week 00:00:00'";
		}
		else if($i == 5)
		{
			$pay_sql = "";
			$writedate_sql = "1=1 ";
		}
	
		// 어제 데이터 쌓기
		$sql = 	"select ".
	          	"	t4.useridx, type, weight_avg_user_coin, total_product_coin, total_money, avg_buy_money, weight_avg_more_rate, weight_avg_rebuydate, coupon_buy_rate, basic_buy_rate, ". 
	          	"	buy_count, nvl(money_in, 0) as money_in, nvl(money_out, 0) as money_out, nvl(playcount, 0) as playcount, web_purchase_rate, ios_purchase_rate, android_purchase_rate, amazon_purchase_rate ".
	    		"from ( ".
	        	"	select ". 
	            "		t3.useridx, type, ".
	            "		(sum(weight_avg_user_coin)::float/sum(money)::float) as weight_avg_user_coin, ".
	            "		sum(product_coin) as total_product_coin, ". 
	            "		sum(money) as total_money, ".
	            "		avg(money) as avg_buy_money, ".
	            "		(case when sum(weight_avg_more_rate) = 0 then 0 else (sum(weight_avg_more_rate)::float/sum(money)::float) end) as weight_avg_more_rate, ".
	            "		(case when sum(weight_avg_rebuydate) = 0 then 0 else (sum(weight_avg_rebuydate)::float/sum(money)::float) end) as weight_avg_rebuydate, ".
	            "		(case when sum(coupon_product_coin) = 0 then 0 else (sum(coupon_product_coin)::float/sum(product_coin)::float)*100 end) as coupon_buy_rate, ".
	            "		(case when sum(basic_buy_coin) = 0 then 0 else (sum(basic_buy_coin)::float/sum(product_coin)::float)*100 end) as basic_buy_rate, ".
	            "		count(money) as buy_count, ".
	            "		(case when sum(web_money) = 0 then 0 else ((sum(web_money)::float/sum(money)::float)*100) end) as web_purchase_rate, ". 
	            "		(case when sum(ios_money) = 0 then 0 else ((sum(ios_money)::float/sum(money)::float)*100) end) as ios_purchase_rate, ". 
	            "		(case when sum(android_money) = 0 then 0 else ((sum(android_money)::float/sum(money)::float)*100) end) as android_purchase_rate, ". 
	            "		(case when sum(amazon_money) = 0 then 0 else ((sum(amazon_money)::float/sum(money)::float)*100) end) as amazon_purchase_rate ".
	        	"	from ( ".
	            "		select t1.useridx, $type as type, t2.platform, t2.user_coin, t2.product_coin, money, is_coupon, more_rate, ".
	            "			(money * t2.product_coin) as weight_avg_product_coin, ".
	            "			(money * t2.user_coin) as weight_avg_user_coin, ".
	            "			(money * more_rate) as weight_avg_more_rate, ".
	            "			(case when datediff(day, writedate, rebuydate) < 0 then 0 else (money * datediff(day, writedate, rebuydate)) end) as weight_avg_rebuydate, ".       
	            "			(case when datediff(day, writedate, rebuydate) < 0 then 0 else datediff(day, writedate, rebuydate) end) as rebuydate, ".
	            "			(case when is_coupon = 1 then product_coin else 0 end) as coupon_product_coin, ".
	            "			(case when base_coin = product_coin then product_coin else 0 end) as basic_buy_coin, ".
	            "			(case when platform = 0  then money else 0 end) as web_money, ".
	            "			(case when platform = 1  then money else 0 end) as ios_money, ".
	            "			(case when platform = 2  then money else 0 end) as android_money, ".
	            "			(case when platform = 3  then money else 0 end) as amazon_money ".
	            "		from ( ".
	            "			select useridx from t5_user where useridx > 20000 and logindate >= '$before_4week 00:00:00'  ".
	           	"		) t1 join t5_user_purchase_log t2 on t1.useridx = t2.useridx ".
	            "		$pay_sql ".
	        	"	) t3 ".
	        	"	group by t3.useridx, type ".
	    		") t4 left join ( ".
	        	"	select useridx, sum(money_in) as money_in, sum(money_out) as money_out, sum(playcount) as playcount ".
	        	"	from ( ".
	            "		select useridx, slottype, money_in, money_out, playcount from t5_user_gamelog where mode not in (4, 9, 26) AND  $writedate_sql ".
	            "		union all ".
	            "		select useridx, slottype, money_in, money_out, playcount from t5_user_gamelog_ios where mode not in (4, 9, 26) AND  $writedate_sql ".
	            "		union all ".
	            "		select useridx, slottype, money_in, money_out, playcount from t5_user_gamelog_android where mode not in (4, 9, 26) AND  $writedate_sql ".
	            "		union all ".
	            "		select useridx, slottype, money_in, money_out, playcount from t5_user_gamelog_amazon where mode not in (4, 9, 26) AND  $writedate_sql ".
	        	"	) sub ".
	        	"	group by useridx ".
	    		") t5 on t4.useridx = t5.useridx ".
	    		"group by t4.useridx, type, weight_avg_user_coin, total_product_coin, total_money, avg_buy_money, weight_avg_more_rate, weight_avg_rebuydate, coupon_buy_rate, basic_buy_rate, buy_count, ". 
	            "money_in, money_out, playcount, web_purchase_rate, ios_purchase_rate, android_purchase_rate, amazon_purchase_rate ";
		$data_list = $db_redshift->gettotallist($sql);
		
		$insert_cnt = 0;
		$insert_sql = "";
		
		for($j=0; $j<sizeof($data_list); $j++)
		{
			$useridx = $data_list[$j]["useridx"];
			$type = $data_list[$j]["type"];
			$weight_avg_user_coin = $data_list[$j]["weight_avg_user_coin"];
			$total_product_coin = $data_list[$j]["total_product_coin"];
			$total_money = $data_list[$j]["total_money"];
			$avg_buy_money = $data_list[$j]["avg_buy_money"];
			$weight_avg_more_rate = $data_list[$j]["weight_avg_more_rate"];
			$weight_avg_rebuydate = $data_list[$j]["weight_avg_rebuydate"];
			$coupon_buy_rate = $data_list[$j]["coupon_buy_rate"];
			$basic_buy_rate = $data_list[$j]["basic_buy_rate"];
			$buy_count = $data_list[$j]["buy_count"];
			$money_in = $data_list[$j]["money_in"];
			$money_out = $data_list[$j]["money_out"];
			$playcount = $data_list[$j]["playcount"];
			$web_purchase_rate = $data_list[$j]["web_purchase_rate"];
			$ios_purchase_rate = $data_list[$j]["ios_purchase_rate"];
			$android_purchase_rate = $data_list[$j]["android_purchase_rate"];
			$amazon_purchase_rate = $data_list[$j]["amazon_purchase_rate"];
			
			if($insert_cnt == 0)
			{
				$insert_sql = "INSERT INTO tbl_user_purchase_gamelog_type VALUES($useridx, $type, $weight_avg_user_coin, $total_product_coin, $total_money, $avg_buy_money, $weight_avg_more_rate, $weight_avg_rebuydate, $coupon_buy_rate, ".
								"$basic_buy_rate, $buy_count, $money_in, $money_out, $playcount, $web_purchase_rate, $ios_purchase_rate, $android_purchase_rate, $amazon_purchase_rate)";
				
				$insert_cnt++;
			}
			else if($insert_cnt < 1000)
			{
				$insert_sql .= ",($useridx, $type, $weight_avg_user_coin, $total_product_coin, $total_money, $avg_buy_money, $weight_avg_more_rate, $weight_avg_rebuydate, $coupon_buy_rate, ".
								"$basic_buy_rate, $buy_count, $money_in, $money_out, $playcount, $web_purchase_rate, $ios_purchase_rate, $android_purchase_rate, $amazon_purchase_rate)";
				
				$insert_cnt++;
			}
			else
			{
				$insert_sql .= ",($useridx, $type, $weight_avg_user_coin, $total_product_coin, $total_money, $avg_buy_money, $weight_avg_more_rate, $weight_avg_rebuydate, $coupon_buy_rate, ".
						"$basic_buy_rate, $buy_count, $money_in, $money_out, $playcount, $web_purchase_rate, $ios_purchase_rate, $android_purchase_rate, $amazon_purchase_rate)";
				
				$db_otherdb->execute($insert_sql);
				
				sleep(1);
				
				$insert_cnt = 0;
				$insert_sql = "";
			}
		}
		
		if($insert_sql != "")
			$db_otherdb->execute($insert_sql);
		
		write_log("Purchase_Gamelog_Type ".$type." Scheduler End ( OtherDB )");
	
	}
	
	$db_otherdb->end();
	$db_redshift->end();
?>