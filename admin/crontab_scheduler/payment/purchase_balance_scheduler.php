<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/purchase_balance_scheduler") !== false)
		{
			$count++;
		}
	}
	
	$issuccess = "1";
	
	if ($count > 1)
		exit();
	
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	$db_other->execute("SET wait_timeout=7200");
	
	write_log("Purchase_Balance_Scheduler Start");		
	
	$today = date("Y-m-d", strtotime("-1 days"));
				
	$sql = "SELECT t3.useridx, t3.platform, ROUND(t3.money::float, 2) AS money, t3.usercoin, t3.purchase_coin, t3.basecoin, t3.user_level, t4.vip_level, t3.product_type, t3.is_coupon, 
			t3.more_rate, ROUND(t3.web_total_credit + t3.mobile_total_credit, 2) AS before_total_credit, t4.dayafterinstall, t3.writedate 
			FROM ( 
				SELECT 
					useridx, productidx, 0 AS platform, (facebookcredit/10::float) AS money, usercoin, userlevel AS user_level, coin AS purchase_coin, basecoin, 
					(SELECT product_type FROM t5_product_type WHERE productidx = t1.productidx) AS product_type, 
					(CASE WHEN couponidx <> 0 THEN 1 WHEN couponidx = 0 THEN 0 END) AS is_coupon, 
					(coin::FLOAT/basecoin::FLOAT)*100 - 100  AS more_rate, 
					(SELECT nvl(ROUND(SUM(facebookcredit/10::float), 2),0) FROM t5_product_order WHERE useridx = t1.useridx AND writedate < t1.writedate AND STATUS = 1) AS web_total_credit, 
					(SELECT nvl(ROUND(SUM(money), 2),0) FROM t5_product_order_mobile WHERE useridx = t1.useridx AND writedate < t1.writedate AND STATUS = 1) AS mobile_total_credit, 
				writedate 
				FROM t5_product_order t1 
				WHERE writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND STATUS = 1 
				UNION ALL 
				SELECT 
				useridx, productidx, os_type AS platform, money, usercoin, userlevel AS user_level, coin AS purchase_coin, basecoin, 
				(SELECT product_type FROM t5_product_type_mobile WHERE productidx = t2.productidx) AS product_type, 
				(CASE WHEN couponidx <> 0 THEN 1 WHEN couponidx = 0 THEN 0 END) AS is_coupon, 
				(coin::FLOAT/basecoin::FLOAT)*100 - 100  AS more_rate, 
				(SELECT nvl(ROUND(SUM(facebookcredit/10::float), 2),0) FROM t5_product_order WHERE useridx = t2.useridx AND writedate < t2.writedate AND STATUS = 1) AS web_total_credit, 
				(SELECT nvl(ROUND(SUM(money), 2),0) FROM t5_product_order_mobile WHERE useridx = t2.useridx AND writedate < t2.writedate AND STATUS = 1) AS mobile_total_credit, 
				writedate 
				FROM t5_product_order_mobile t2 
				WHERE writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND STATUS = 1 
			) t3 JOIN ( 
				SELECT useridx, MAX(vip_level) AS vip_level, MAX(days_after_install) AS dayafterinstall 
				FROM ( 
					SELECT useridx, vip_level, days_after_install, today FROM t5_user_playstat_daily WHERE today = '$today' 
					UNION ALL 
					SELECT useridx, vip_level, days_after_install, today FROM t5_user_playstat_daily_ios WHERE today = '$today' 
					UNION ALL 
					SELECT useridx, vip_level, days_after_install, today FROM t5_user_playstat_daily_android WHERE today = '$today' 
					UNION ALL 
					SELECT useridx, vip_level, days_after_install, today FROM t5_user_playstat_daily_amazon WHERE today = '$today' 
				) sub 
				GROUP BY useridx 
			) t4 ON t3.useridx = t4.useridx ORDER BY writedate ASC;";
		
	$purchase_list = $db_redshift->gettotallist($sql);
	
	$insert_cnt = 0;
	$update_cnt = 0;
	
	for($i=0; $i<sizeof($purchase_list); $i++)
	{
		$useridx = $purchase_list[$i]["useridx"];
		$platform = $purchase_list[$i]["platform"];
		$money = $purchase_list[$i]["money"];
		$usercoin = $purchase_list[$i]["usercoin"];
		$purchase_coin = $purchase_list[$i]["purchase_coin"];
		$basecoin = $purchase_list[$i]["basecoin"];
		$user_level = $purchase_list[$i]["user_level"];
		$vip_level = $purchase_list[$i]["vip_level"];
		$product_type = $purchase_list[$i]["product_type"];
		$is_coupon = $purchase_list[$i]["is_coupon"];
		$more_rate = $purchase_list[$i]["more_rate"];
		$is_first = 0;
		$before_total_credit = $purchase_list[$i]["before_total_credit"];
		$dayafterinstall = $purchase_list[$i]["dayafterinstall"];
		$writedate = $purchase_list[$i]["writedate"];
		$rebuydate = "";
	
		if($before_total_credit == 0)
			$is_first = 1;
	
		if($is_first == 0)
		{
			// rebuydate Update
			$sql = "SELECT logidx FROM tbl_user_purchase_log WHERE useridx = $useridx AND writedate < '$writedate' ORDER BY writedate DESC LIMIT 1;";
			$logidx = $db_other->getvalue($sql);
				
			if($logidx != "")
			{		
				$update_sql = "UPDATE tbl_user_purchase_log SET rebuydate = '$writedate' WHERE logidx = $logidx;";
				$db_other->execute($update_sql);
	
				$update_cnt++;
			}
		}
	
		$insert_sql = "INSERT INTO tbl_user_purchase_log(useridx, platform, money, user_coin, product_coin, base_coin, user_level, vip_level, product_type, is_coupon, more_rate, is_first, total_buy_amount, dayafterinstall, writedate, rebuydate) ".
				"VALUES($useridx, $platform, $money, $usercoin, $purchase_coin, $basecoin, $user_level, $vip_level, '$product_type', $is_coupon, $more_rate, $is_first, $before_total_credit, $dayafterinstall, '$writedate', '$rebuydate');";
		$db_other->execute($insert_sql);
	
		$insert_cnt++;
	
		if($insert_cnt%500 == 0)
			write_log("Insert Cnt ".$insert_cnt);		
	}	
	write_log("Purchase_Balance_Scheduler End");

	$db_other->end();
	$db_redshift->end();
?>