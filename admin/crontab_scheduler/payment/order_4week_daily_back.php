<?
	include("../../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();
	
	$db_main->execute("SET wait_timeout=7200");
	$db_analysis->execute("SET wait_timeout=7200");
	
	ini_set("memory_limit", "-1");
				
	//$sdate = date("Y-m-d", time() - 24 * 60 * 60 * 14);
	$sdate = date("Y-m-d", time() - 24 * 60 * 60 * 14);
	//$sdate = "2015-11-17";
	$edate = date("Y-m-d");
		
	while($sdate < $edate)
	{
		$temp_date = date('Y-m-d', strtotime($sdate));
		$today_date = date('Y-m-d', strtotime($sdate.' - 1 day'));
		
		$join_sdate = "$sdate";
		$join_edate = "$temp_date";
		
		$write_sdate = $join_sdate." 00:00:00";
		$write_edate = $join_edate." 00:00:00";
		
		$sql = "SELECT DATE_FORMAT(MAX(writedate), '%Y-%m-%d') today, 0 AS platform, SUM(facebookcredit) / 10 AS total_money  ".
				"FROM tbl_product_order ".
				"WHERE useridx > 20000 and status = 1 AND writedate >= DATE_SUB('$write_sdate', INTERVAL 4 WEEK) AND writedate < '$write_edate' ";
		$web_list = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($web_list); $i++)
		{
			$today = $web_list[$i]["today"];
			$platform = $web_list[$i]["platform"];
			$total_money = $web_list[$i]["total_money"];
			
			$sql = "INSERT INTO tbl_order_4week_daily(today, platform, total_money) VALUES('$today', $platform, $total_money) ON DUPLICATE KEY UPDATE total_money=VALUES(total_money);";
			$db_analysis->execute($sql);
		}
		
// 		$sql = "SELECT DATE_FORMAT(MAX(writedate), '%Y-%m-%d') today, os_type AS platform, SUM(money) AS total_money   ".
// 				"FROM tbl_product_order_mobile ".
// 				"WHERE useridx > 20000 and status = 1 AND writedate >= DATE_SUB('$write_sdate', INTERVAL 4 WEEK) AND writedate < '$write_edate' ".
// 				"GROUP BY os_type";		
		$sql = "SELECT DATE_FORMAT(MAX(writedate), '%Y-%m-%d') today, os_type AS platform, SUM(money) AS total_money   
				FROM
				(				
					SELECT os_type, money, writedate
					FROM tbl_product_order_mobile 
					WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$write_sdate', INTERVAL 4 WEEK) AND writedate < '$write_sdate' 
					UNION ALL
					SELECT os_type, money, writedate
					FROM  `tbl_product_order_ironsource`
					WHERE STATUS = 1 AND writedate >= DATE_SUB('$write_sdate', INTERVAL 4 WEEK) AND writedate < '$write_sdate' 
				) tt
				GROUP BY os_type";
		write_log($sql);
		$mobile_list = $db_main->gettotallist($sql);
			
		for($i=0; $i<sizeof($mobile_list); $i++)
		{
			$today = $mobile_list[$i]["today"];
			$platform = $mobile_list[$i]["platform"];
			$total_money = $mobile_list[$i]["total_money"];
			$today_str = $today;
									
			$sql = "INSERT INTO tbl_order_4week_daily(today, platform, total_money) VALUES('$today', $platform, $total_money) ON DUPLICATE KEY UPDATE total_money=VALUES(total_money);";
			$db_analysis->execute($sql);
		}
		
		write_log("$write_sdate ~ $write_edate order 4 week insert");
		
		$sdate = date('Y-m-d', strtotime($temp_date.' + 1 day'));
	}
	
	$db_main->end();
	$db_analysis->end();
?>