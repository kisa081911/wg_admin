<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/payment/betting_status") !== false)
		{
			$count++;
		}
	}

	ini_set("memory_limit", "-1");
	
	$issuccess = "1";

	if ($count > 1)
		exit();
	
	ini_set("memory_limit", "-1");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	try 
	{
		$sql = "SELECT COUNT(*) FROM tbl_vip_nopurchase_tmp";
		$row_count = $db_main2->getvalue($sql);
		
		while($row_count > 0)
		{
			$sql = "DELETE FROM tbl_vip_nopurchase_tmp LIMIT 5000;";
			$db_main2->execute($sql);
		
			$row_count -= 5000;
		}
		
		// VIP 유저 중에서 최근 28일 동안 결제하지 않은 유저
		$sql = "SELECT useridx, ROUND(SUM(money)) AS rev, max(vip_level) AS vip_level, max(coin) AS coin ".
				"FROM (	". 
  				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money, vip_level, t2.coin, writedate	".
  				"	FROM t5_product_order t1 JOIN t5_user t2 ON t1.useridx = t2.useridx	".
  				"	WHERE t1.useridx > 20000 AND vip_level >= 4 AND t2.coin >= 50000000 AND logindate >= DATEADD(day, -7, CURRENT_DATE)	". 
				"	UNION ALL ".
  				"	SELECT t1.useridx, money, vip_level, t2.coin, writedate ". 
  				"	FROM t5_product_order_mobile t1 JOIN t5_user t2 ON t1.useridx = t2.useridx ".
  				"	WHERE t1.useridx > 20000 AND vip_level >= 4 AND t2.coin >= 50000000 AND logindate >= DATEADD(day, -7, CURRENT_DATE) ".
				") t3 group by useridx	".
				"HAVING MAX(writedate) < DATEADD(day, -28, CURRENT_DATE)";		
		
		$vip_nopay_list = $db_redshift->gettotallist($sql);
		
		$sql = "";
		$insertcount = 0;
			
		for($i=0; $i<sizeof($vip_nopay_list); $i++)
		{
			$useridx = $vip_nopay_list[$i]["useridx"];
			$rev = $vip_nopay_list[$i]["rev"];
			$vip_level = $vip_nopay_list[$i]["vip_level"];
			$coin = $vip_nopay_list[$i]["coin"];
		
			if ($insertcount == 0)
			{
				$sql = "INSERT INTO tbl_vip_nopurchase_tmp(useridx, rev, vip_level, coin, writedate) VALUES($useridx, $rev, $vip_level, $coin, NOW())";

				$insertcount++;
			}
			else if ($insertcount < 500)
			{
				$sql .= ", ($useridx, $rev, $vip_level, $coin, NOW())";
				$insertcount++;
			}
			else
			{
				$sql .= ", ($useridx, $rev, $vip_level, $coin, NOW());";
				$insertcount = 0;

				$db_main2->execute($sql);

				$sql = "";
			}
		}
		
		if($sql != "")
		{
			$db_main2->execute($sql);
		}
		
		// Redshift 동기화 시간에 따른 결제자 체크
		$sql = "SELECT useridx FROM tbl_vip_nopurchase_tmp";
		$vip_nopay_tmp_list = $db_main2->gettotallist($sql);
		
		$delete_useridx = "";
		
		for($i=0; $i<sizeof($vip_nopay_tmp_list); $i++)
		{
			$tmp_useridx = $vip_nopay_tmp_list[$i]["useridx"];
							
			$sql = "SELECT ".
					"(SELECT COUNT(*) FROM tbl_product_order WHERE useridx = $tmp_useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 4 WEEK) LIMIT 1) + ".
					"(SELECT COUNT(*) FROM tbl_product_order_mobile WHERE useridx = $tmp_useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 4 WEEK) LIMIT 1) AS purchase_count";		
			$pay_count = $db_main->getvalue($sql);
			
			if($pay_count > 0)
			{
				if($delete_useridx == "")
					$delete_useridx .= "$tmp_useridx";
				else
					$delete_useridx .= ", $tmp_useridx";
			}
		}
		
		if($delete_useridx != "")
		{
			$sql = "DELETE FROM tbl_vip_nopurchase_tmp WHERE useridx IN ($delete_useridx);";
			$db_main2->execute($sql);
		}
		// End
		
		$sql = "UPDATE tbl_vip_nopurchase ".
				"SET is_buy = 1 ".
				"WHERE NOT EXISTS (SELECT * FROM tbl_vip_nopurchase_tmp WHERE useridx=tbl_vip_nopurchase.useridx)";
		$db_main2->execute($sql);
		
		$sql = "INSERT INTO tbl_vip_nopurchase(useridx, rev, vip_level, is_buy, writedate) ".
				"SELECT useridx, rev, vip_level, 0 AS is_buy, writedate ".
				"FROM tbl_vip_nopurchase_tmp ".
				"ON DUPLICATE KEY UPDATE rev = VALUES(rev), is_buy = VALUES(is_buy), writedate = VALUES(writedate) ";
		$db_main2->execute($sql);
		
		$sql = "INSERT INTO tbl_vip_nopurchase_history(useridx, rev, vip_level, coin, writedate) ".
				"SELECT useridx, rev, vip_level, coin, NOW() ".
				"FROM tbl_vip_nopurchase_tmp";
		$db_main2->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$sql = "SELECT COUNT(*) FROM tbl_vip_purchase_tmp";
		$row_count = $db_main2->getvalue($sql);
	
		while($row_count > 0)
		{
			$sql = "DELETE FROM tbl_vip_purchase_tmp LIMIT 5000;";
			$db_main2->execute($sql);
	
			$row_count -= 5000;
		}
	
		// VIP 유저 중에서 최근 28일 동안 결제 한 유저, 보유 코인 1억 이상
		$sql = "SELECT useridx, ROUND(SUM(money)) AS rev, max(coin) AS coin ".
				"FROM ( ".
				"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money, t2.coin, writedate ".
				"	FROM t5_product_order t1 JOIN t5_user t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND vip_level >= 4 AND t2.coin >= 50000000 AND logindate >= DATEADD(day, -7, CURRENT_DATE) ".
				"	UNION ALL ".
				"	SELECT t1.useridx, money, t2.coin, writedate ".
				"	FROM t5_product_order_mobile t1 JOIN t5_user t2 ON t1.useridx = t2.useridx ".
				"	WHERE t1.useridx > 20000 AND vip_level >= 4 AND t2.coin >= 50000000 AND logindate >= DATEADD(day, -7, CURRENT_DATE) ".
				") t3 ".
				"GROUP BY useridx ".
				"HAVING MAX(writedate) >= DATEADD(day,-28, CURRENT_DATE)";
	
		$vip_nopay_list = $db_redshift->gettotallist($sql);
	
		$sql = "";
		$insertcount = 0;
			
		for($i=0; $i<sizeof($vip_nopay_list); $i++)
		{
			$useridx = $vip_nopay_list[$i]["useridx"];
			$coin = $vip_nopay_list[$i]["coin"];
	
			if ($insertcount == 0)
			{
				$sql = "INSERT INTO tbl_vip_purchase_tmp(useridx, coin, writedate) VALUES($useridx, $coin, NOW())";
	
				$insertcount++;
			}
			else if ($insertcount < 500)
			{
				$sql .= ", ($useridx, $coin, NOW())";
				$insertcount++;
			}
			else
			{
				$sql .= ", ($useridx, $coin, NOW());";
				$insertcount = 0;
	
				$db_main2->execute($sql);
	
				$sql = "";
			}
		}
	
		if($sql != "")
		{
			$db_main2->execute($sql);
		}
	
		// Redshift 동기화 시간에 따른 결제자 체크
		$sql = "SELECT useridx FROM tbl_vip_purchase_tmp";
		$vip_nopay_tmp_list = $db_main2->gettotallist($sql);
	
		for($i=0; $i<sizeof($vip_nopay_tmp_list); $i++)
		{
			$tmp_useridx = $vip_nopay_tmp_list[$i]["useridx"];
				
			$sql = "SELECT ".
					"(SELECT COUNT(*) FROM tbl_product_order WHERE useridx = $tmp_useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 2 DAY) LIMIT 1) + ".
					"(SELECT COUNT(*) FROM tbl_product_order_mobile WHERE useridx = $tmp_useridx AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 2 DAY) LIMIT 1) AS purchase_count";
						
			$pay_count = $db_main->getvalue($sql);
						
			if($pay_count > 0)
			{
				$sql = "DELETE FROM tbl_vip_purchase_tmp WHERE useridx = $tmp_useridx;";
				$db_main2->execute($sql);
			}
		}
	
		//결제 가중
		$sql = "SELECT useridx FROM tbl_vip_purchase_tmp";
		$vip_nopay_tmp_list = $db_main2->gettotallist($sql);
	
		$update_sql = "";
		$updatecount = 0;
	
		for($i=0; $i<sizeof($vip_nopay_tmp_list); $i++)
		{
			$tmp_useridx = $vip_nopay_tmp_list[$i]["useridx"];
				
			$sql = "SELECT IFNULL(ROUND(SUM(usercoin * money)/SUM(money)),0) AS coin_p ".
					"FROM	".
					"(	".
					"	SELECT usercoin, ROUND(facebookcredit/10, 2) AS money, writedate FROM tbl_product_order WHERE writedate >= DATE_SUB(NOW(), INTERVAL 4 WEEK) AND status = 1 AND useridx = $tmp_useridx	".
					"	UNION ALL	".
					"	SELECT usercoin, money, writedate FROM tbl_product_order_mobile WHERE writedate >= DATE_SUB(NOW(), INTERVAL 4 WEEK) AND status = 1 AND useridx = $tmp_useridx	".
					") t1";
					$coin_p = $db_main->getvalue($sql);
				
			if ($updatecount == 0)
			{
				$update_sql = "INSERT INTO tbl_vip_purchase_tmp(useridx, coin_p) VALUES($tmp_useridx, $coin_p)";
	
				$updatecount++;
			}
			else if ($updatecount < 50)
			{
				$update_sql .= ", ($tmp_useridx, $coin_p)";
				$updatecount++;
			}
			else
			{
				$update_sql .= ", ($tmp_useridx, $coin_p)";
				$update_sql .= " ON DUPLICATE KEY UPDATE coin_p=VALUES(coin_p);";
				$updatecount = 0;
	
				$db_main2->execute($update_sql);
	
				$update_sql = "";
			}
		}
	
	
		if($update_sql != "")
		{
			$update_sql .= " ON DUPLICATE KEY UPDATE coin_p=VALUES(coin_p);";
			$db_main2->execute($update_sql);
		}
	
		$sql = "UPDATE tbl_vip_purchase ".
				"SET is_buy = 1 ".
				"WHERE NOT EXISTS (SELECT * FROM tbl_vip_purchase_tmp WHERE useridx=tbl_vip_purchase.useridx)";
		$db_main2->execute($sql);
	
		$sql = "INSERT INTO tbl_vip_purchase(useridx, coin_p, is_buy, writedate) ".
				"SELECT useridx, coin_p, 0 AS is_buy, writedate ".
				"FROM tbl_vip_purchase_tmp ".
							"ON DUPLICATE KEY UPDATE coin_p = VALUES(coin_p), is_buy = VALUES(is_buy), writedate = VALUES(writedate) ";
		$db_main2->execute($sql);
	
		$sql = "INSERT INTO tbl_vip_purchase_history(useridx, coin_p, coin, writedate) ".
				"SELECT useridx, coin_p, coin, NOW() ".
				"FROM tbl_vip_purchase_tmp";
		$db_main2->execute($sql);
	}
	catch(Exception $e)
	{
			write_log($e->getMessage());
	}
		
	try
	{
		$today = date("Y-m-d", time());
		$yesterday = date("Y-m-d", time() - 60 * 60 * 24);
	
		$sql = "SELECT COUNT(*) FROM tbl_user_betting_status_tmp";
		$row_count = $db_main2->getvalue($sql);

		while($row_count > 0)
		{
			$sql = "DELETE FROM tbl_user_betting_status_tmp LIMIT 5000;";
			$db_main2->execute($sql);
	
			$row_count -= 5000;
		}
	
		// 예상 소진일 12일 이상인 유저, 3일 이내 $5이상 결제 하지 않은 유저, 보유코인 $19 상품 코인(950만코인)이상 유저
		//web
		$sql = "SELECT useridx, currentcoin, ROUND((moneyin + h_moneyin)/(playcount + h_playcount)) AS avg_bet, ROUND(currentcoin/((moneyin+h_moneyin)*0.03)) AS expected_days, playcount + h_playcount AS sum_playcount, today ".
				"FROM t5_user_playstat_daily ".
				"WHERE EXISTS ( ".
				"	SELECT statidx ".
				"	FROM ( ".
				"		SELECT useridx, MAX(statidx) AS statidx ".
				"		FROM t5_user_playstat_daily ".
				"		WHERE today > DATEADD(day, -8, CURRENT_DATE) AND (playcount + h_playcount) >= 50 AND currentcoin/((moneyin+h_moneyin)*0.03) >= 12 AND (moneyin+h_moneyin) > 0 ".
      			"			AND ( ".
        		"				NOT EXISTS( ".
          		"					SELECT useridx FROM t5_product_order WHERE writedate > DATEADD(h, -36, CURRENT_DATE) AND facebookcredit >= 50 AND status = 1 AND useridx = t5_user_playstat_daily.useridx ".
        		"				) AND ".
        		"				NOT EXISTS( ".
          		"					SELECT useridx FROM t5_product_order_mobile WHERE writedate > DATEADD(h, -36, CURRENT_DATE) AND money >= 5 AND status = 1 AND useridx = t5_user_playstat_daily.useridx ".
         		"				) ".
      			"			) ".
				"		GROUP BY useridx ".
				"	) t1 ".
				"	WHERE statidx = t5_user_playstat_daily.statidx ".
				") AND currentcoin > 9500000 AND days_after_install > 3";
		$use_list = $db_redshift->gettotallist($sql);
			
		$sql = "";
		$insertcount = 0;
			
		for($i=0; $i<sizeof($use_list); $i++)
		{
			$useridx = $use_list[$i]["useridx"];
			$currentcoin = $use_list[$i]["currentcoin"];
			$avg_bet = $use_list[$i]["avg_bet"];
			$expected_days = $use_list[$i]["expected_days"];
			$sum_playcount = $use_list[$i]["sum_playcount"];
			$today = $use_list[$i]["today"];
		
			if ($insertcount == 0)
			{
				$sql = "INSERT INTO tbl_user_betting_status_tmp(useridx, platform, currentcoin, avg_bet, expected_days, sum_playcount, is_buy, today, writedate) VALUES($useridx, 0, $currentcoin, $avg_bet, $expected_days, $sum_playcount, 0, '$today', NOW())";
			
				$insertcount++;
			}
			else if ($insertcount < 500)
			{
				$sql .= ", ($useridx, 0, $currentcoin, $avg_bet, $expected_days, $sum_playcount, 0, '$today', NOW())";
				$insertcount++;
			}
			else
			{
				$sql .= ", ($useridx, 0, $currentcoin, $avg_bet, $expected_days, $sum_playcount, 0, '$today', NOW());";
				$insertcount = 0;
			
				$db_main2->execute($sql);
			
				$sql = "";
			}
		}
	
		if($sql != "")
		{			
			$db_main2->execute($sql);
		}
		
		// 예상 소진일 12일 이상인 유저, 3일 이내 $5이상 결제 하지 않은 유저, 보유코인 $19 상품 코인(950만코인)이상 유저
		//ios
		$sql = "SELECT useridx, currentcoin, ROUND((moneyin + h_moneyin)/(playcount + h_playcount)) AS avg_bet, ROUND(currentcoin/((moneyin+h_moneyin)*0.03)) AS expected_days, playcount + h_playcount AS sum_playcount, today ".
				"FROM t5_user_playstat_daily_ios ".
				"WHERE EXISTS ( ".
				"	SELECT statidx ".
				"	FROM ( ".
				"		SELECT useridx, MAX(statidx) AS statidx ".
				"		FROM t5_user_playstat_daily_ios ".
				"		WHERE today > DATEADD(day, -8, CURRENT_DATE) AND (playcount + h_playcount) >= 50 AND currentcoin/((moneyin+h_moneyin)*0.03) >= 12 AND (moneyin+h_moneyin) > 0 ".
				"			AND ( ".
				"				NOT EXISTS( ".
				"					SELECT useridx FROM t5_product_order WHERE writedate > DATEADD(h, -36, CURRENT_DATE) AND facebookcredit >= 50 AND status = 1 AND useridx = t5_user_playstat_daily_ios.useridx ".
				"				) AND ".
				"				NOT EXISTS( ".
				"					SELECT useridx FROM t5_product_order_mobile WHERE writedate > DATEADD(h, -36, CURRENT_DATE) AND money >= 5 AND status = 1 AND useridx = t5_user_playstat_daily_ios.useridx ".
				"				) ".
				"			) ".
				"		GROUP BY useridx ".
				"	) t1 ".
				"	WHERE statidx = t5_user_playstat_daily_ios.statidx ".
				") AND currentcoin > 9500000 AND days_after_install > 3";
		$use_list = $db_redshift->gettotallist($sql);
			
		$sql = "";
		$insertcount = 0;
			
		for($i=0; $i<sizeof($use_list); $i++)
		{
			$useridx = $use_list[$i]["useridx"];
			$currentcoin = $use_list[$i]["currentcoin"];
			$avg_bet = $use_list[$i]["avg_bet"];
			$expected_days = $use_list[$i]["expected_days"];
			$sum_playcount = $use_list[$i]["sum_playcount"];
			$today = $use_list[$i]["today"];
		
			if ($insertcount == 0)
			{
				$sql = "INSERT INTO tbl_user_betting_status_tmp(useridx, platform, currentcoin, avg_bet, expected_days, sum_playcount, is_buy, today, writedate) VALUES($useridx, 1, $currentcoin, $avg_bet, $expected_days, $sum_playcount, 0, '$today', NOW())";
							
				$insertcount++;
			}
			else if ($insertcount < 500)
			{
				$sql .= ", ($useridx, 1, $currentcoin, $avg_bet, $expected_days, $sum_playcount, 0, '$today', NOW())";
				$insertcount++;
			}
			else
			{
				$sql .= ", ($useridx, 1, $currentcoin, $avg_bet, $expected_days, $sum_playcount, 0, '$today', NOW());";
				$insertcount = 0;
			
				$db_main2->execute($sql);
			
				$sql = "";
			}
		}
		
		if($sql != "")
		{
			$db_main2->execute($sql);
		}
		
		// 예상 소진일 12일 이상인 유저, 3일 이내 $5이상 결제 하지 않은 유저, 보유코인 $19 상품 코인(950만코인)이상 유저
		// Android
		$sql = "SELECT useridx, currentcoin, ROUND((moneyin + h_moneyin)/(playcount + h_playcount)) AS avg_bet, ROUND(currentcoin/((moneyin+h_moneyin)*0.03)) AS expected_days, playcount + h_playcount AS sum_playcount, today ".
				"FROM t5_user_playstat_daily_android ".
				"WHERE EXISTS ( ".
				"	SELECT statidx ".
				"	FROM ( ".
				"		SELECT useridx, MAX(statidx) AS statidx ".
				"		FROM t5_user_playstat_daily_android ".
				"		WHERE today > DATEADD(day, -8, CURRENT_DATE) AND (playcount + h_playcount) >= 50 AND currentcoin/((moneyin+h_moneyin)*0.03) >= 12 AND (moneyin+h_moneyin) > 0 ".
				"			AND ( ".
				"				NOT EXISTS( ".
				"					SELECT useridx FROM t5_product_order WHERE writedate > DATEADD(h, -36, CURRENT_DATE) AND facebookcredit >= 50 AND status = 1 AND useridx = t5_user_playstat_daily_android.useridx ".
				"				) AND ".
				"				NOT EXISTS( ".
				"					SELECT useridx FROM t5_product_order_mobile WHERE writedate > DATEADD(h, -36, CURRENT_DATE) AND money >= 5 AND status = 1 AND useridx = t5_user_playstat_daily_android.useridx ".
				"				) ".
				"			) ".
				"		GROUP BY useridx ".
				"	) t1 ".
				"	WHERE statidx = t5_user_playstat_daily_android.statidx ".
				") AND currentcoin > 9500000 AND days_after_install > 3";
		$use_list = $db_redshift->gettotallist($sql);
			
		$sql = "";
		$insertcount = 0;
			
		for($i=0; $i<sizeof($use_list); $i++)
		{
			$useridx = $use_list[$i]["useridx"];
			$currentcoin = $use_list[$i]["currentcoin"];
			$avg_bet = $use_list[$i]["avg_bet"];
			$expected_days = $use_list[$i]["expected_days"];
			$sum_playcount = $use_list[$i]["sum_playcount"];
			$today = $use_list[$i]["today"];
			
			if ($insertcount == 0)
			{
				$sql = "INSERT INTO tbl_user_betting_status_tmp(useridx, platform, currentcoin, avg_bet, expected_days, sum_playcount, is_buy, today, writedate) VALUES($useridx, 2, $currentcoin, $avg_bet, $expected_days, $sum_playcount, 0, '$today', NOW())";
									
				$insertcount++;
			}
			else if ($insertcount < 500)
			{
				$sql .= ", ($useridx, 2, $currentcoin, $avg_bet, $expected_days, $sum_playcount, 0, '$today', NOW())";
				$insertcount++;
			}
			else
			{
				$sql .= ", ($useridx, 2, $currentcoin, $avg_bet, $expected_days, $sum_playcount, 0, '$today', NOW());";
				$insertcount = 0;
					
				$db_main2->execute($sql);
					
				$sql = "";
			}
		}
		
		// 예상 소진일 12일 이상인 유저, 3일 이내 $5이상 결제 하지 않은 유저, 보유코인 $19 상품 코인(950만코인)이상 유저
		// Amazon
		$sql = "SELECT useridx, currentcoin, ROUND((moneyin + h_moneyin)/(playcount + h_playcount)) AS avg_bet, ROUND(currentcoin/((moneyin+h_moneyin)*0.03)) AS expected_days, playcount + h_playcount AS sum_playcount, today ".
				"FROM t5_user_playstat_daily_amazon ".
				"WHERE EXISTS ( ".
				"	SELECT statidx ".
				"	FROM ( ".
				"		SELECT useridx, MAX(statidx) AS statidx ".
				"		FROM t5_user_playstat_daily_amazon ".
				"		WHERE today > DATEADD(day, -8, CURRENT_DATE) AND (playcount + h_playcount) >= 50 AND currentcoin/((moneyin+h_moneyin)*0.03) >= 12 AND (moneyin+h_moneyin) > 0 ".
				"			AND ( ".
				"				NOT EXISTS( ".
				"					SELECT useridx FROM t5_product_order WHERE writedate > DATEADD(h, -36, CURRENT_DATE) AND facebookcredit >= 50 AND status = 1 AND useridx = t5_user_playstat_daily_amazon.useridx ".
				"				) AND ".
				"				NOT EXISTS( ".
				"					SELECT useridx FROM t5_product_order_mobile WHERE writedate > DATEADD(h, -36, CURRENT_DATE) AND money >= 5 AND status = 1 AND useridx = t5_user_playstat_daily_amazon.useridx ".
				"				) ".
				"			) ".
				"		GROUP BY useridx ".
				"	) t1 ".
				"	WHERE statidx = t5_user_playstat_daily_amazon.statidx ".
				") AND currentcoin > 9500000 AND days_after_install > 3";
		$use_list = $db_redshift->gettotallist($sql);
			
		$sql = "";
		$insertcount = 0;
			
		for($i=0; $i<sizeof($use_list); $i++)
		{
			$useridx = $use_list[$i]["useridx"];
			$currentcoin = $use_list[$i]["currentcoin"];
			$avg_bet = $use_list[$i]["avg_bet"];
			$expected_days = $use_list[$i]["expected_days"];
			$sum_playcount = $use_list[$i]["sum_playcount"];
			$today = $use_list[$i]["today"];
								
			if ($insertcount == 0)
			{
				$sql = "INSERT INTO tbl_user_betting_status_tmp(useridx, platform, currentcoin, avg_bet, expected_days, sum_playcount, is_buy, today, writedate) VALUES($useridx, 3, $currentcoin, $avg_bet, $expected_days, $sum_playcount, 0, '$today', NOW())";
					
				$insertcount++;
			}
			else if ($insertcount < 500)
			{
				$sql .= ", ($useridx, 3, $currentcoin, $avg_bet, $expected_days, $sum_playcount, 0, '$today', NOW())";
				$insertcount++;
			}
			else
			{
				$sql .= ", ($useridx, 3, $currentcoin, $avg_bet, $expected_days, $sum_playcount, 0, '$today', NOW());";
				$insertcount = 0;
					
				$db_main2->execute($sql);
					
				$sql = "";
			}
		}
		
		if($sql != "")
		{
			$db_main2->execute($sql);
		}
		
		// 플랫폼별 중복 제거
		$sql = "DELETE ".
				"FROM tbl_user_betting_status_tmp ".
				"WHERE EXISTS ( ".
				"	SELECT useridx, sum_playcount ".
				"	FROM ( ".
				"		SELECT useridx, COUNT(*) AS tt, MAX(sum_playcount) AS sum_playcount ".
				"		FROM tbl_user_betting_status_tmp ".
				"		GROUP BY useridx ".
				"		HAVING tt > 1 ".
				"	) t1 ".
				"	WHERE useridx = tbl_user_betting_status_tmp.useridx AND sum_playcount != tbl_user_betting_status_tmp.sum_playcount ".
				")";
		$db_main2->execute($sql);
		
		$sql = "DELETE FROM tbl_user_betting_status_tmp ".
				" WHERE EXISTS ( ".
				"	SELECT * ".
				"	FROM ( ".
				"		SELECT useridx, COUNT(*) AS tt, MAX(expected_days) AS expected_days ".
				"		FROM tbl_user_betting_status_tmp ".
				"		GROUP BY useridx ".
				"		HAVING tt > 1 ".
				"	) t1 ".
				"	WHERE useridx = tbl_user_betting_status_tmp.useridx AND expected_days != tbl_user_betting_status_tmp.expected_days ".
				")";
		$db_main2->execute($sql);
		
		$sql = "";
		
		$sql = "SELECT useridx FROM tbl_user_betting_status_tmp WHERE today >= '$yesterday'";
		$status_tmp_list = $db_main2->gettotallist($sql);
		
		$delete_useridx = "";
		
		for($i = 0; $i < sizeof($status_tmp_list); $i++)
		{
			$status_tmp_useridx = $status_tmp_list[$i]["useridx"];
			
			$sql = "SELECT ".
					"(SELECT COUNT(*) FROM tbl_product_order WHERE useridx = '$status_tmp_useridx' AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY) LIMIT 1) + ".
					"(SELECT COUNT(*) FROM tbl_product_order_mobile WHERE useridx = '$status_tmp_useridx' AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 3 DAY) LIMIT 1) AS purchase_count";
			$pay_count = $db_main->getvalue($sql);
			
			if($pay_count > 0)
			{
				if($delete_useridx == "")
					$delete_useridx .= "$status_tmp_useridx";
				else 
					$delete_useridx .= ", $status_tmp_useridx";					
			}			
		}
		
		if($delete_useridx != "")
		{
			$sql = "DELETE FROM tbl_user_betting_status_tmp WHERE useridx IN ($delete_useridx);";
			$db_main2->execute($sql);
		}
	
		$sql = "UPDATE tbl_user_betting_status SET is_buy = 1 WHERE is_buy = 0 AND NOT EXISTS (SELECT * FROM tbl_user_betting_status_tmp WHERE useridx = tbl_user_betting_status.useridx)";
		$db_main2->execute($sql);
	
		$sql = "INSERT INTO tbl_user_betting_status(useridx, platform, currentcoin, avg_bet, expected_days, sum_playcount, is_buy, writedate, register_date) ".
				"SELECT useridx, platform, currentcoin, avg_bet, expected_days, sum_playcount, is_buy, writedate, today ".
				"FROM tbl_user_betting_status_tmp ".
				"	ON DUPLICATE KEY UPDATE platform = VALUES(platform), currentcoin = VALUES(currentcoin), avg_bet = VALUES(avg_bet), expected_days = VALUES(expected_days), sum_playcount = VALUES(sum_playcount), is_buy = VALUES(is_buy), writedate = VALUES(writedate), register_date = VALUES(register_date)";
		$db_main2->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	$db_main->end();
	$db_main2->end();
	$db_other->end();
	$db_redshift->end();
?>
