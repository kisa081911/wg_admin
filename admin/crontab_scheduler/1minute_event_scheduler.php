<?
	include("../common/common_include.inc.php");
	
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	
	$db_main2->execute("SET wait_timeout=60");
	$db_analysis->execute("SET wait_timeout=60");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	try
	{
		$sql = "INSERT INTO tbl_scheduler_log(today, type, status, writedate) VALUES('$today', 102, 0, NOW());";
		$db_analysis->execute($sql);
		$sql = "SELECT LAST_INSERT_ID();";
		$logidx = $db_analysis->getvalue($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/1minute_event_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) 
		{
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/1minute_event_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
		
				$content .= "kill -9 ".$process_list[0]."\n";
		
				write_log("1minute_event_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	try
	{
		// 이벤트 게시 (팬 페이지)
		$sql = "SELECT eventidx,reservation_imagepath,reservation_contents,shorturl FROM tbl_event WHERE NOW()>reservation_date AND reservation_fanpage=1 AND reservation_fanpage_status=0";
		$list = $db_main2->gettotallist($sql);
	
		if (sizeof($list) > 0)
		{
			session_start();
	
			$facebook = new Facebook(array(
					'appId'  => FACEBOOK_APP_ID,
					'secret' => FACEBOOK_SECRET_KEY,
					'cookie' => true,
			));
	
			$session = $facebook->getUser();
	
			$sql = "SELECT accesstoken FROM page_accesstoken WHERE userid='1015660465162708' ORDER BY writedate DESC LIMIT 1";
			$accesstoken = $db_analysis->getvalue($sql);
	
			if ($accesstoken != "")
			{
				$facebook->manual_accesstoken = $accesstoken;
	
				$page_id = 1514417478856074;
					
				for ($i=0; $i<sizeof($list); $i++)
				{
					$eventidx = $list[$i]["eventidx"];
					$imagepath = $list[$i]["reservation_imagepath"];
					$contents = $list[$i]["reservation_contents"];
					$shorturl = $list[$i]["shorturl"];
	
					$contents = str_replace("[URL]", $shorturl, $contents);

					$args = array('name' => $contents, 'url' => $imagepath);
	
					$id = "";
	
					try
					{
						$result = $facebook->api("/$page_id/photos", "post", $args);
						$id = $result["id"];
					}
					catch (FacebookApiException $e)
					{
						print_r($e);
						write_log("Main Photo Upload - ".$e->getMessage());
						$issuccess = "0";
					}
	
					if ($id != "")
					{
						$sql = "UPDATE tbl_event SET reservation_fanpage_status=1,reservation_fanpage_date=NOW(),writedate=NOW(),fbid='$id' WHERE eventidx=$eventidx";
						$db_main2->execute($sql);
					}
				}
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		// 임시 ( 동영상 게시물 예외처리)
		$sql = "SELECT eventidx,shorturl FROM tbl_event WHERE NOW()>reservation_date AND title LIKE '%Video%' AND fbid = '' AND writedate > '2016-08-18 00:00:00' ";
		$list = $db_main2->gettotallist($sql);
	
		if(sizeof($list) > 0)
		{
			session_start();
	
			$facebook = new Facebook(array(
					'appId'  => FACEBOOK_MNG_APP_ID,
					'secret' => FACEBOOK_MNG_SECRET_KEY,
					'cookie' => true,
			));
	
			$session = $facebook->getUser();
	
			$sql = "SELECT accesstoken FROM page_accesstoken WHERE userid='1015660465162708' ORDER BY writedate DESC LIMIT 1";
			$accesstoken = $db_analysis->getvalue($sql);
	
			if ($accesstoken != "")
			{
				$facebook->manual_accesstoken = $accesstoken;
					
				$page_id = 1514417478856074;				
					
				for ($i=0; $i<sizeof($list); $i++)
				{
					$eventidx = $list[$i]["eventidx"];
					$shorturl = $list[$i]["shorturl"];
		
					$id = "";
		
					$result = $facebook->api("/$page_id/videos?limit=10");
					$video_list = $result["data"];
		
					for($i=0; $i<sizeof($video_list); $i++)
					{
						$data = $video_list[$i];
		
						$description = $data["description"];
						$fbid = $data["id"];
		
						if(strpos($description, $shorturl) > -1)
						{
							$id = $fbid;
						}
					}
		
					if ($id != "")
					{
						$sql = "UPDATE tbl_event SET reservation_fanpage_status=1,reservation_fanpage_date=NOW(),writedate=NOW(),fbid='$id' WHERE eventidx=$eventidx";
						$db_main2->execute($sql);
					}
				}
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		$sql = "UPDATE tbl_scheduler_log SET status = $issuccess, writedate = NOW() WHERE logidx = $logidx;";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
	$db_analysis->end();
?>