<?
	include("../../common/common_include.inc.php");
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/mobile/mobile_push_stat") !== false)
		{
			$count++;
		}
	}

	$issuccess = "1";
	
	if ($count > 1)
		exit();
	
	$db_mobile = new CDatabase_Mobile();
	$db_other = new CDatabase_Other();

	// 모바일 push 현황 3번 통계
	try
	{
	
		$push_stat_today = date("Y-m-d", time() - 24 * 60 * 60);
		$push_stat_tomorrow = date("Y-m-d");
	
		$sql = "SELECT  DATE_FORMAT(createdate, '%Y-%m-%d') AS today, os_type, COUNT(os_type) AS join_count, DATEDIFF(DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d'), DATE_FORMAT(createdate, '%Y-%m-%d'))  AS after_day, SUM(IF(push_enabled=1, 1, 0)) AS enable_count ".
				"FROM tbl_mobile ". 
				"WHERE  DATE_SUB('$push_stat_tomorrow 00:00:00', INTERVAL 13 DAY) <= createdate AND createdate < '$push_stat_tomorrow 00:00:00'	". 
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m-%d'), os_type";
		$push_enable_list = $db_mobile->gettotallist($sql);
	
		for($i=0; $i<sizeof($push_enable_list); $i++)
		{
			$push_enable_today = $push_enable_list[$i]["today"];
			$push_enable_platform = $push_enable_list[$i]["os_type"];
			$push_enable_join_count = $push_enable_list[$i]["join_count"];
			$push_enable_after_day = $push_enable_list[$i]["after_day"];
			$push_enable_enable_count = $push_enable_list[$i]["enable_count"];
				
			$sql = "INSERT INTO tbl_push_enable_stat(today, platform, join_count, day_$push_enable_after_day) VALUES('$push_enable_today', $push_enable_platform, $push_enable_join_count, $push_enable_enable_count) ".
					"ON DUPLICATE KEY UPDATE join_count=VALUES(join_count), day_$push_enable_after_day=VALUES(day_$push_enable_after_day);";
			$db_other->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
// 	try
// 	{
// 		// 모바일 리워드 유저 수 통계 4번 통계
// 		$push_today = date("Y-m-d", time() - 24 * 60 * 60);
// 		$push_tomorrow = date("Y-m-d");
		
// 		$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, platform, COUNT(useridx) AS user_cnt ".
// 				"FROM `tbl_user_push_reward` ".
// 				"WHERE '$push_today 00:00:00' <= writedate AND writedate < '$push_tomorrow 00:00:00' ".
// 				"GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), platform";
		
// 		$push_reward_stat = $db_mobile->gettotallist($sql);
		
// 		for($i=0; $i<sizeof($push_reward_stat); $i++)
// 		{
// 			$today = $push_reward_stat[$i]["today"];
// 			$platform = $push_reward_stat[$i]["platform"];
// 			$user_cnt = $push_reward_stat[$i]["user_cnt"];
			
// 			$sql = "INSERT INTO tbl_push_reward_cancel_stat(today, platform, user_count) VALUES('$today', $platform, $user_cnt) ON DUPLICATE KEY UPDATE user_count=VALUES(user_count);";
// 			$db_other->execute($sql);
// 		}
		
// 		// D1 Push Cancel 유저 통계
// 		$d1_today = date("Y-m-d", time() - 24 * 60 * 60 * 2);
// 		$d1_tomorrow = date("Y-m-d", time() - 24 * 60 * 60 * 1);
		
// 		$sql = "SELECT platform, COUNT(t1.useridx) AS d1_cancel ".
// 				"FROM ( ".
// 				"	SELECT useridx, platform ".
// 				"	FROM `tbl_user_push_reward` ".
// 				"	WHERE '$d1_today 00:00:00' <= writedate AND writedate < '$d1_tomorrow 00:00:00' ".
// 				") t1 JOIN tbl_user_mobile t2 ON t1.useridx= t2.useridx ".
// 				"WHERE push_enabled = 0 ".
// 				"GROUP BY platform";
		
// 		$push_d1_stat = $db_mobile->gettotallist($sql);
		
// 		for($i=0; $i<sizeof($push_d1_stat); $i++)
// 		{
// 			$platform = $push_d1_stat[$i]["platform"];
// 			$d1_cancel = $push_d1_stat[$i]["d1_cancel"];
				
// 			$sql = "INSERT INTO tbl_push_reward_cancel_stat(today, platform, d1_cancel) VALUES('$d1_today', $platform, $d1_cancel) ON DUPLICATE KEY UPDATE d1_cancel=VALUES(d1_cancel);";
// 			$db_other->execute($sql);
// 		}
		
// 		// D7 Push Cancel 유저 통계
// 		$d7_today = date("Y-m-d", time() - 24 * 60 * 60 * 8);
// 		$d7_tomorrow = date("Y-m-d", time() - 24 * 60 * 60 * 7);
		
// 		$sql = "SELECT platform, COUNT(t1.useridx) AS d7_cancel ".
// 				"FROM ( ".
// 				"	SELECT useridx, platform ".
// 				"	FROM `tbl_user_push_reward` ".
// 				"	WHERE '$d7_today 00:00:00' <= writedate AND writedate < '$d7_tomorrow 00:00:00' ".
// 				") t1 JOIN tbl_user_mobile t2 ON t1.useridx= t2.useridx ".
// 				"WHERE push_enabled = 0 ".
// 				"GROUP BY platform";
		
// 		$push_d7_stat = $db_mobile->gettotallist($sql);
		
// 		for($i=0; $i<sizeof($push_d7_stat); $i++)
// 		{
// 			$platform = $push_d7_stat[$i]["platform"];
// 			$d7_cancel = $push_d7_stat[$i]["d7_cancel"];
		
// 			$sql = "INSERT INTO tbl_push_reward_cancel_stat(today, platform, d7_cancel) VALUES('$d7_today', $platform, $d7_cancel) ON DUPLICATE KEY UPDATE d7_cancel=VALUES(d7_cancel);";
// 			$db_other->execute($sql);
// 		}
// 	}
// 	catch(Exception $e)
// 	{
// 		write_log($e->getMessage());
// 	}
	
	$db_mobile->end();
	$db_other->end();
?>