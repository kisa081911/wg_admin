<?
    include("../../../common/common_include.inc.php");
    include("../../../common/db_util_redshift.inc.php");
  
    ini_set("memory_limit", "-1");
    
    $db_otherdb = new CDatabase_Other();
    $db_redshift = new CDatabase_Redshift();

    // platform, useridx별 (오픈일 - 첫 구매일), (첫구매일 - 가입일)
    for($platform=0; $platform<3; $platform++)
    {
    	$sql = "select useridx, cd_interval, np_interval ".
				"from ( ".
  				"	select t1.useridx, round((datediff(day, '2015-11-15', MIN(createdate)) - datediff(day, '2015-11-15', MIN(createdate)) % 28) / 28) as cd_interval, ".
    			"		round((datediff(day, MIN(createdate), MIN(writedate)) - datediff(day, MIN(createdate), MIN(writedate)) % 28) / 28) AS np_interval ".
  				"	from t5_product_order t1 join t5_user t2 on t1.useridx = t2.useridx ".
  				"	where status = 1 and t1.useridx > 20000 and t2.platform = $platform AND createdate >= '2015-11-15' AND createdate < '2019-01-06' ".
  				"	group by t1.useridx ".
  				"	union all ".
  				"	select t1.useridx, round((datediff(day, '2015-11-15', MIN(createdate)) - datediff(day, '2015-11-15', MIN(createdate)) % 28) / 28) as cd_interval, ".
    			"		round((datediff(day, MIN(createdate), MIN(writedate)) - datediff(day, MIN(createdate), MIN(writedate)) % 28) / 28) AS np_interval ".
  				"	from t5_product_order_mobile t1 join t5_user t2 on t1.useridx = t2.useridx ".
  				"	where status = 1 and t1.useridx > 20000 and t2.platform = $platform AND createdate >= '2015-11-15' AND createdate < '2019-01-06' ".
  				"	group by t1.useridx ".
				") tt ".
				"group by useridx, cd_interval, np_interval";
    	
    	$user_list = $db_redshift->gettotallist($sql);
    	
    	$insertcount = 0;
    	$insert_sql = "";
    	
    	for($i=0; $i<sizeof($user_list); $i++)
    	{
    		$useridx = $user_list[$i]["useridx"];
    		$cd_interval = $user_list[$i]["cd_interval"];
    		$np_interval = $user_list[$i]["np_interval"];
    		
    		if($insertcount == 0)
    		{
    			$insert_sql .= "INSERT INTO _tbl_purchase_ltv_take5_2019(platform, useridx, cd_interval, np_interval) ".
      			"VALUES($platform, $useridx, $cd_interval, $np_interval)";
    			$insertcount++;
    		}
    		else if ($insertcount < 200)
    		{
    			$insert_sql .= ",($platform, $useridx, $cd_interval, $np_interval)";
    			$insertcount++;
    		}
    		else
    		{
    			$insert_sql .= ",($platform, $useridx, $cd_interval, $np_interval) ON DUPLICATE KEY UPDATE cd_interval=VALUES(cd_interval), np_interval=VALUES(np_interval);";
    			$db_otherdb->execute($insert_sql);
    			
    			$insert_sql = "";
    			$insertcount = 0;
    		}
    	}
    	
    	if($insert_sql != "")
    	{
    		$insert_sql .= " ON DUPLICATE KEY UPDATE cd_interval=VALUES(cd_interval), np_interval=VALUES(np_interval);";
    		$db_otherdb->execute($insert_sql);
    		
    		$insert_sql = "";
    	}
    }

    $start_list[0] = 0;
    $start_list[1] = 13;
    $start_list[2] = 13;
    
    $end_4week_section = 54;
/*    
    write_log("tbl_purchase_ltv_take5_2019 start");
    for($platform=0; $platform<3; $platform++)
    {
    	for($cd_interval=$start_list[$platform]; $cd_interval<$end_4week_section; $cd_interval++)
    	{
    		for($np_interval=0; $np_interval<$end_4week_section-$cd_interval; $np_interval++)
    		{
    			$sql = "SELECT useridx FROM _tbl_purchase_ltv_take5_2019 WHERE platform = $platform AND cd_interval = $cd_interval AND np_interval = $np_interval";
    			$useridx_list = $db_otherdb->gettotallist($sql);
    			
    			$useridx_in = "";
    			
    			for($i=0; $i<sizeof($useridx_list); $i++)
    			{
    				$useridx_str = $useridx_list[$i]["useridx"];
    				
    				if($i == 0)
    				{
    					$useridx_in = "$useridx_str";
    				}
    				else
    				{
    					$useridx_in .= ",$useridx_str";
    				}
    			}
    			
    			// 가입일 기준 28일 단위 결제금액 입력
    			if(sizeof($useridx_list) > 0)
    			{
    				$sql = "SELECT p_interval, SUM(rev) as rev ".
							"FROM ( ".
  							"	SELECT ROUND((datediff(day, dateadd(day, (28*$cd_interval), '2015-11-15'), writedate) - datediff(day, dateadd(day, (28*$cd_interval), '2015-11-15'), writedate) % 28) / 28) AS p_interval, ROUND(SUM(facebookcredit::float/10::float)) AS rev ". 
  							"	FROM t5_product_order t1 JOIN t5_user t2 ON t1.useridx = t2.useridx ". 
  							"	WHERE status = 1 AND t1.useridx IN ($useridx_in) ". 
  							"	GROUP BY p_interval ".
  							"	UNION ALL ".
  							"	SELECT ROUND((datediff(day, dateadd(day, (28*$cd_interval), '2015-11-15'), writedate) - datediff(day, dateadd(day, (28*$cd_interval), '2015-11-15'), writedate) % 28) / 28) AS p_interval, ROUND(SUM(money)) AS rev ". 
  							"	FROM t5_product_order_mobile t1 JOIN t5_user t2 ON t1.useridx = t2.useridx ". 
  							"	WHERE status = 1 AND t1.useridx IN ($useridx_in) ". 
  							"	GROUP BY p_interval ".
							") t1 ".
							"GROUP BY p_interval";
    				
    				$ltv_list = $db_redshift->gettotallist($sql);
    				
    				$insertcount = 0;
    				$insert_sql = "";
    				
    				for($i=0; $i<sizeof($ltv_list); $i++)
    				{
    					$p_interval = $ltv_list[$i]["p_interval"];
    					$rev = $ltv_list[$i]["rev"];
    					
    					if($insertcount == 0)
    					{
    						$insert_sql .= "INSERT INTO _tbl_ltv_take5_2019(platform, cd_interval, np_interval, p_interval, revenue) ".
      						"VALUES($platform, $cd_interval, $np_interval, $p_interval, $rev)";
    						$insertcount++;
    					}
    					else if ($insertcount < 200)
    					{
    						$insert_sql .= ",($platform, $cd_interval, $np_interval, $p_interval, $rev)";
    						$insertcount++;
    					}
    					else
    					{
    						$insert_sql .= ",($platform, $cd_interval, $np_interval, $p_interval, $rev) ON DUPLICATE KEY UPDATE revenue=VALUES(revenue);";
    						$db_otherdb->execute($insert_sql);
    						
    						$insert_sql = "";
    						$insertcount = 0;
    					}
    				}
    				
    				if($insert_sql != "")
    				{
    					$insert_sql .= " ON DUPLICATE KEY UPDATE revenue=VALUES(revenue);";
    					$db_otherdb->execute($insert_sql);
    					
    					$insert_sql = "";
    				}
    			}
    		}
    	}
    }
    write_log("tbl_purchase_ltv_take5_2019 end");
*/
/*
    $seasonal_weight[0] = 102;
    $seasonal_weight[1] = 104;
    $seasonal_weight[2] = 100;
    $seasonal_weight[3] = 98;
    $seasonal_weight[4] = 101;
    $seasonal_weight[5] = 100;
    $seasonal_weight[6] = 101;
    $seasonal_weight[7] = 101;
    $seasonal_weight[8] = 101;
    $seasonal_weight[9] = 98;
    $seasonal_weight[10] = 98;
    $seasonal_weight[11] = 99;
    $seasonal_weight[12] = 100;
    
    for($platform=0; $platform<3; $platform++)
    {
        for($i=0; $i<200; $i++)
        {
            $cd_interval[$i] = 0;
            $rev[$i] = 0;
            $rev_exp[$i] = 0;
            
            $pivot_np[$i] = 0;
            $pivot_fp[$i] = 0;
        }
        
        if($platform == 0)
        {
        	$interval_weight_np[0] = 132;
        	$interval_weight_np[1] = 90;
        	$interval_weight_np[2] = 70;
        	$interval_weight_np[3] = 70;
        	
        	$interval_weight_fp[0] = 140;
        	$interval_weight_fp[1] = 100+7;
        	$interval_weight_fp[2] = 70;
        	$interval_weight_fp[3] = 70;
        }
        else if($platform == 1)
        {
        	$interval_weight_np[0] = 100;
        	$interval_weight_np[1] = 120;
        	$interval_weight_np[2] = 99;
        	$interval_weight_np[3] = 100;
        	
        	$interval_weight_fp[0] = 100;
        	$interval_weight_fp[1] = 100;
        	$interval_weight_fp[2] = 90;
        	$interval_weight_fp[3] = 100;
        }
        else if($platform == 2)
        {
        	$interval_weight_np[0] = 100;
        	$interval_weight_np[1] = 113;
        	$interval_weight_np[2] = 70;
        	$interval_weight_np[3] = 110;
        	
        	$interval_weight_fp[0] = 100;
        	$interval_weight_fp[1] = 100;
        	$interval_weight_fp[2] = 60;
        	$interval_weight_fp[3] = 110;
        }

        $predict_interval = 41-$start_list[$platform];
        
    	$sql = "SELECT cd_interval, revenue FROM _tbl_ltv_take5_2018 WHERE platform = $platform AND cd_interval >= $start_list[$platform] and cd_interval < $end_4week_section and np_interval = 0 AND p_interval = 0";
    	$ltv_list = $db_otherdb->gettotallist($sql);

    	for($i=0; $i<sizeof($ltv_list); $i++)
    	{
    	    $cd_interval[$i] =  $ltv_list[$i]["cd_interval"];
    	    $rev[$i] = $ltv_list[$i]["revenue"];
    	}
    	
    	//$ios_temp = 6000+600;
        //$and_temp = 3500+600;
    	
    	for($i=sizeof($ltv_list); $i<100; $i++)
    	{
    	    if($platform == 0)
    	    {
    	        $rev[$i] = 1200;
    	        
    	        if($i >= 36)
    	        {
    	            $rev[$i] = 2500;
    	        }
    	    }
	        else if($platform == 1)
	        {
	            $rev[$i] = 30000;
	            
	            if($i >= 23)
	            {
	                $rev[$i] = 35000;
	            }
	        }
            else if($platform == 2)
            {
                $rev[$i] = 30000;
                
                if($i >= 23)
                {
                    $rev[$i] = 35000;
                }
            }
    	}
    	
    	$sql = "select rate from _tbl_ltv_pivot_np_take5_2018 where platform = $platform order by r_interval limit 200";
    	$pivot_np_list = $db_otherdb->gettotallist($sql);
    	
    	for($i=0; $i<sizeof($pivot_np_list); $i++)
    	{
    	    $pivot_np[$i] =  $pivot_np_list[$i]["rate"];
    	}
    	
    	$sql = "select rate from _tbl_ltv_pivot_fp_take5_2018 where platform = $platform order by r_interval limit 200";
    	$pivot_fp_list = $db_otherdb->gettotallist($sql);
    	
    	for($i=0; $i<sizeof($pivot_fp_list); $i++)
    	{
    	    $pivot_fp[$i] =  $pivot_fp_list[$i]["rate"];
    	}

    	for ($i=0; $i<$predict_interval; $i++)
    	{
    	    $rev_add = 0;
    	    $w = 0;

    	    //신규 가입자에서 발생한 매출 계산
    	    $rev_exp[$i] += $rev[$i];

    	    $rev96_add = 0;
    	    
    	    for($j=0; $j<$i; $j++)
    	    {
    	        // 이전 결제자에서 넘어오는 매출 계산
    	        if($j+$start_list[$platform] < 2)
    	        {
    	        	$w = 0;
    	        }
    	        else if(2 <= $j+$start_list[$platform] && $j+$start_list[$platform] < 15)
    	        {
    	        	$w = 1;
    	        }
    	        else if(15 <= $j+$start_list[$platform] && $j+$start_list[$platform] < 28)
    	        {
    	            $w = 2;
    	        }
    	        else 
    	        {
    	        	$w = 3;
    	        }
    	        
    	        //$w = (($j+$start_list[$platform] % 13)-($j+$start_list[$platform]%13)%13)/13;
    	        //if ($w > 4) $w = 4;
    	        
    	        $exp_weight = 1.0;
    	        //if (j+start_interval > 66) exp_weight = 0.9;
    	        
    	        if($platform == 0)
    	        {
    	            if($j+$start_list[$platform] >= 6 && $j+$start_list[$platform] < 7)
    	            {
    	                $exp_weight = 1.6;
    	            }
    	            if($j+$start_list[$platform] >= 9 && $j+$start_list[$platform] < 12)
    	            {
    	                $exp_weight = 1.4;
    	            }
    	            else if($j+$start_list[$platform] >= 25 && $j+$start_list[$platform] < 28)
    	            {
    	                $exp_weight = 4.5;
    	            }
    	        }
    	        else if($platform == 1)
    	        {
    	            if($j+$start_list[$platform] >= 16 && $j+$start_list[$platform] < 22)
    	            {
    	                $exp_weight = 1.20;
    	            }
    	            else if($j+$start_list[$platform] >= 22)
    	            {
    	                $exp_weight = 1.00;
    	            }
    	        }
    	        else if($platform == 2)
    	        {
    	            if($j+$start_list[$platform] >= 15 && $j+$start_list[$platform] < 25)
    	            {
    	                $exp_weight = 1.30;
    	            }
    	            else if($j+$start_list[$platform] >= 33)
    	            {
    	                $exp_weight = 0.85;
    	            }
    	        }
    	        
    	        // 게임 개선 10% 증가
    	        if($j+$start_list[$platform] >= 35)
    	        {
    	        	$exp_weight += 0.10;
    	        }
    	        
    	        for($k=0; $k<$i-$j; $k++)
    	        {
    	            $w2 = $w;
    	            if ($k == 0) $w2 = 0;	// 가입달 매출은 원형을 유지해야 함
    	            $rev_add += $rev[$j]*($pivot_np[$k]*$interval_weight_np[$w2]*$exp_weight / 100) / 10000 *($pivot_fp[$i-$j-$k]*$interval_weight_fp[$w] * $exp_weight / 100) / 10000;
    	            
    	            //if ($j+$start_list[$platform] >= 28)
    	            //{
    	            //    $rev96_add += $rev[$j]*($pivot_np[$k]*$interval_weight_np[$w2]*$exp_weight / 100) / 10000 * ($pivot_fp[$i-$j-$k]*$interval_weight_fp[$w] * $exp_weight / 100) / 10000;
    	            //}
    	        }
    	        
    	        //이전 가입자에서  신규로 발생한 매출 계산
    	        $rev_add += $rev[$j]*($pivot_np[$i-$j] * $interval_weight_np[$w] * $exp_weight / 100) / 10000;
    	        
    	        //if ($j+$start_list[$platform] >= 28)
    	        //{
    	        //    $rev96_add += $rev[$j] * ($pivot_np[$i-$j] * $interval_weight_np[$w] * $exp_weight / 100) / 10000;
    	        //}
    	    }

    	    // 시즌효과
    	    //$s_w = (($i+$start_list[$platform])*28+180) % 365;
    	    //$s_w = ($s_w - $s_w % 91 ) / 91;
    	    //if ($s_w > 3) $s_w = 3;
    	    //$rev_add = $rev_add * $seasonal_weight[$s_w] / 100;
    	    
    	    // 시즌 효과 월별로 수정
    	    $s_w = ($i+$start_list[$platform]) % 13;

    	    $rev_add = $rev_add * $seasonal_weight[$s_w] / 100;
    	    
    	    $rev96_add *= $seasonal_weight[$s_w] / 100;
    	    
    	    $rev96 += $rev96_add;
    	    if ($i == 64-$start_list[$platform]+13)
    	    {
    	        //printf("rev96_1Y = %.1f\n", rev96);
    	    }

    	    $rev_exp[$i] += $rev_add;
    	    
    	    $sql = "INSERT INTO _tbl_ltv_sim_result_take5_2018(platform, r_interval, revenue) VALUES($platform, $i+$start_list[$platform], $rev_exp[$i]) ON DUPLICATE KEY UPDATE revenue=VALUES(revenue);";
    	    $db_otherdb->execute($sql);
    	}
    }
*/
    $db_otherdb->end();
    $db_redshift->end();
?>