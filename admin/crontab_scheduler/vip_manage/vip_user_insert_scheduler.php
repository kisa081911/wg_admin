<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	$db_redshift = new CDatabase_Redshift();
	$db_main_slave = new CDatabase_Slave_Main();
	$db_main2 = new CDatabase_Main2();
	$db_mobile = new CDatabase_Mobile();

	$db_main_slave->execute("SET wait_timeout=7200");
	$db_main2->execute("SET wait_timeout=7200");
	$db_mobile->execute("SET wait_timeout=7200");
	
	ini_set("memory_limit", "-1");
	
	$today = date("Y-m-d", time() - 24 * 60 * 60);
	$std_leavedays = 14;
	$std_total_money = 499;
	
	try
	{
	    // 누적 결제금액 $499이상, 14일 이상 이탈자
		$sql = "select t1.useridx, total_money, logindate, datediff('day', logindate, '$today 00:00:00') as leavedays, isblock, (case when is_guest = 1 then '0' else userid end) as fb_id, platform, coin,  email, is_guest ".
                "from ( ".
                "   select useridx, SUM(money) as total_money ".
                "   from t5_product_order_all ".
                "   where useridx > 20000 and status = 1 ".
                "   group by useridx ".
                "   having SUM(money) > $std_total_money ".
                ") t1 join t5_user t2 on t1.useridx = t2.useridx ".
                "where datediff('day', logindate, '$today 00:00:00') >= $std_leavedays";
		$vip_list = $db_redshift->gettotallist($sql);
		
		$exsists_useridx = "";
		
		for($i=0; $i<sizeof($vip_list); $i++)
		{
		    $useridx = $vip_list[$i]["useridx"];
		    $is_guest = $vip_list[$i]["is_guest"];
		    
		    if($i == 0)
		    	$exsists_useridx = $useridx;
		    else
		    	$exsists_useridx .= ",".$useridx;

		    $sql = "SELECT t1.useridx, nickname, coin, isblock, userid, DATEDIFF(NOW(), IFNULL(t1.logindate, t1.createdate)) AS leavedays, t1.logindate, t1.createdate ".
                    "FROM ( ". 
                    "   SELECT t1.useridx, t1.nickname, coin, isblock, userid, DATEDIFF(NOW(), IFNULL(t1.logindate, t1.createdate)) AS leavedays, t1.logindate, t1.createdate ".
                    "   FROM tbl_user t1 JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx ".
                    "   WHERE t1.useridx=$useridx ".
                    ") t1;"; 
		    $user_info = $db_main_slave->getarray($sql);
		    
		    $nickname = encode_db_field($user_info["nickname"]);
		    $coin = $user_info["coin"];
		    $isblock = $user_info["isblock"];
		    $fb_id = $user_info["userid"];
		    
		    if(is_number($fb_id))
		    {
		        if(strlen($fb_id) >= 18)
		        {
		            $fb_id = 0;
		        }
		        
		        $leavedays = $user_info["leavedays"];
		        $logindate = $user_info["logindate"];
		        $createdate = $user_info["createdate"];
		        $welcome_coin = 0;
		        
		        if($leavedays >= 14)
		        {
		            $sql = "SELECT IFNULL(ROUND(SUM(money), 2), 0) AS total_money ".
                            "FROM ( ".
                            "   SELECT SUM(facebookcredit/10) AS money FROM tbl_product_order WHERE useridx = $useridx AND STATUS = 1 ".
                            "   UNION ALL ".
                            "   SELECT SUM(money) AS money FROM tbl_product_order_mobile WHERE useridx = $useridx AND STATUS = 1 ".
                            ") t1";
		            $total_money = $db_main_slave->getvalue($sql);
		            
		            $sql = "SELECT email, unsubscribe FROM `tbl_user_email` WHERE useridx = $useridx";
		            $email_info = $db_main2->getarray($sql);
		            
		            $sql = "SELECT ".
							"(SELECT COUNT(*) FROM tbl_user_gdpr_info WHERE useridx = $useridx AND is_agree = 1) +	".
							"(SELECT COUNT(*) FROM tbl_user_gdpr_info_mobile WHERE useridx = $useridx AND is_agree = 1)";
		            $email_gdpr_info = $db_main_slave->getvalue($sql);
		            
		            $email = $email_info["email"];		            
	            	$email_disable = ($email_info["unsubscribe"] == "" ? 0 : $email_info["unsubscribe"]);
	            	
	            	if($email_gdpr_info == 0)
	            		$email_disable = 1;
		            
		            // A2U 최근 발송 일자
		            if($fb_id > 0)
		            {
		                $sql = "SELECT MAX(senddate) FROM `tbl_notification_group_log_new` WHERE useridx = $useridx";
		                $last_a2u_send_date = $db_main_slave->getvalue($sql);
		                
		                if($last_a2u_send_date == "")
		                {
		                    $last_a2u_send_date = "0000-00-00 00:00:00";
		                }
		                
		                $sql = "SELECT COUNT(*) FROM `tbl_user_delete` WHERE useridx = $useridx AND STATUS = 1 ";
		                $is_app_delete = $db_main_slave->getvalue($sql);
		            }		            
		            
		            //푸시 발송 유무
		            $sql = "SELECT IF(COUNT(*) = 0, 1, 0) AS push_disable, os_type FROM tbl_mobile WHERE device_id = (SELECT device_id FROM tbl_user_mobile_connection_log ".
		            		"WHERE useridx = $useridx ORDER BY logindate DESC LIMIT 1) AND push_enabled = 1 AND UUID != ''; ";		            
		            $push_info = $db_mobile->getarray($sql);
		            
		            $push_disable = $push_info["push_disable"];          
		            
		            $push_platform = 0;
		            
		            if($push_disable == 0)
		            {
		            	// 푸시 최근 발송 일자
		                $sql = "SELECT MAX(writedate) FROM tbl_push_event_result WHERE useridx=$useridx";
		                $last_push_send_date = $db_main2->getvalue($sql);
		                
		                if($last_push_send_date == "")
		                {
		                    $last_push_send_date = "0000-00-00 00:00:00";
		                }
		                
		               
		                $push_platform = $push_info["os_type"];
		            }
		            
		            if($coin >= 1000000000) // 10억 이상
		            {
		                $welcome_coin = 100000000;
		            }
		            else if($coin >= 100000000) // 1억 이상
		            {
		                $welcome_coin = max(floor($coin/10), 20000000);
		            }
		            else if($coin >= 10000000) // 1천만 이상
		            {
		                $welcome_coin = max(floor($coin/20), 3000000);
		            }
		            else if($coin >= 2000000) // 1천만 이상
		            {
		                $welcome_coin = max(floor($coin/30), 1000000);
		            }
		            else
		            {
		                $welcome_coin = 1000000;
		            }
		            
		            $status = 0;
		            
		            $sql = "INSERT INTO tbl_vip_leave_user(useridx, nickname, total_money, isblock, fb_id, email, is_guest, coin, is_app_delete, is_email_disable, is_push_disable, push_platform, leavedays, logindate, createdate, welcome_coin, last_a2u_send_date, last_push_send_date, status, update_date, register_date) ".
		  		            "VALUES($useridx, '$nickname', $total_money, $isblock, '$fb_id', '$email', $is_guest, $coin, $is_app_delete, $email_disable, $push_disable, $push_platform, $leavedays, '$logindate', '$createdate', $welcome_coin, '$last_a2u_send_date', '$last_push_send_date', $status, NOW(), NOW()) ".
		  		            "ON DUPLICATE KEY UPDATE nickname=VALUES(nickname), total_money=VALUES(total_money), isblock=VALUES(isblock), fb_id=VALUES(fb_id), email=VALUES(email), is_guest=VALUES(is_guest), coin=VALUES(coin), ".
                            "is_app_delete=VALUES(is_app_delete), is_email_disable=VALUES(is_email_disable), is_push_disable=VALUES(is_push_disable), push_platform=VALUES(push_platform), ".
		  		            "leavedays=VALUES(leavedays), welcome_coin=VALUES(welcome_coin), logindate=VALUES(logindate), last_a2u_send_date=VALUES(last_a2u_send_date), last_push_send_date=VALUES(last_push_send_date), status=VALUES(status), update_date=VALUES(update_date);";
		            $db_main2->execute($sql);
		        }
		        else
		        {
		            if($leavedays >= 7 && $leavedays < 14)
		            {
		                $welcome_coin = floor(min($leavedays, 14) * 200000 / 14);
		            }
		            
		            // A2U 최근 발송 일자
		            if($fb_id > 0)
		            {
		                $sql = "SELECT MAX(senddate) FROM `tbl_notification_group_log_new` WHERE useridx = $useridx";
		                $last_a2u_send_date = $db_main_slave->getvalue($sql);
		            }
		            
		            $sql = "UPDATE tbl_vip_leave_user SET status = 1, logindate='$logindate', leavedays=$leavedays, welcome_coin=$welcome_coin, last_a2u_send_date='$last_a2u_send_date' WHERE useridx=$useridx";
		            $db_main2->execute($sql);
		        }
		    }		   
		}
		
		if($exsists_useridx != "")
		{
			$sql = "SELECT * FROM tbl_vip_leave_user WHERE status = 0 AND useridx NOT IN ($exsists_useridx);";
			$vip_list = $db_main2->gettotallist($sql);
			
			for($i=0; $i<sizeof($vip_list); $i++)
			{
				$useridx = $vip_list[$i]["useridx"];
				
				$sql = "SELECT t1.useridx, nickname, coin, isblock, userid, DATEDIFF(NOW(), IFNULL(t1.logindate, t1.createdate)) AS leavedays, t1.logindate, t1.createdate ".
                    "FROM ( ". 
                    "   SELECT t1.useridx, t1.nickname, coin, isblock, userid, DATEDIFF(NOW(), IFNULL(t1.logindate, t1.createdate)) AS leavedays, t1.logindate, t1.createdate ".
                    "   FROM tbl_user t1 JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx ".
                    "   WHERE t1.useridx=$useridx ".
                    ") t1;"; 
		    	$user_info = $db_main_slave->getarray($sql);
		    	
		    	$leavedays = $user_info["leavedays"];
		        $logindate = $user_info["logindate"];		        
		        $welcome_coin = 0;
				
				if($leavedays >= 7 && $leavedays < 14)
				{
					$welcome_coin = floor(min($leavedays, 14) * 200000 / 14);
				}
				
				// A2U 최근 발송 일자
				if($fb_id > 0)
				{
					$sql = "SELECT MAX(senddate) FROM `tbl_notification_group_log_new` WHERE useridx = $useridx";
					$last_a2u_send_date = $db_main_slave->getvalue($sql);
				}
				
				$sql = "UPDATE tbl_vip_leave_user SET status = 1, logindate='$logindate', leavedays=$leavedays, welcome_coin=$welcome_coin, last_a2u_send_date='$last_a2u_send_date' WHERE useridx=$useridx";
				$db_main2->execute($sql);
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	$db_redshift->end();
	$db_main_slave->end();
	$db_main2->end();
	$db_mobile->end();
?>