<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	ini_set("memory_limit", "-1");
	
	$db_main2 = new CDatabase_Main2();
	$db_redshift = new CDatabase_Redshift();	
	
	$db_main2->execute("SET wait_timeout=72000");	
	
	
	$today = date("Y-m-d h:i:s", time());
	$before_14day = date("Y-m-d h:i:s", time() - 60 * 60 * 24 * 14);
	$before_21day = date("Y-m-d h:i:s", time() - 60 * 60 * 24 * 21);
	$before_35day = date("Y-m-d h:i:s", time() - 60 * 60 * 24 * 35);
	
	$std_useridx = 20000;
	
	try
	{
		$sql = "SELECT promoidx FROM tbl_promotion_setting WHERE status = 1 AND startdate <= NOW() AND NOW() <= enddate;";
		$promoidx = $db_main2->getvalue($sql);
		
		if($promoidx > 0)
		{
			//nopay_21, 신규 cr_take5_nopay_21
			$sql = "SELECT t1.useridx AS useridx ".
					"FROM t5_user t1 ".
					"WHERE t1.useridx > $std_useridx  AND createdate <= '$before_21day' AND t1.fb_token <> '' ".
					"AND NOT EXISTS (SELECT * FROM t5_product_order WHERE useridx = t1.useridx AND STATUS = 1)	".
					"AND NOT EXISTS (SELECT * FROM duc_user WHERE fb_token = t1.fb_token) ".
					"AND NOT EXISTS (SELECT * FROM t5_user_promotion WHERE useridx = t1.useridx AND adflag = 'cr_t5_nopay_21');";
			$nopay_21_info = $db_redshift->gettotallist($sql);
			
			$promotion_nopay_21_redshift = "";
			$promotion_nopay_21_sql = "";

			$insertcount = 0;
			
			for($i = 0; $i < sizeof($nopay_21_info); $i++)
			{
				$nopay_21_useridx = $nopay_21_info[$i]['useridx'];

				if($insertcount == 0)
				{
					$promotion_nopay_21_redshift = "INSERT INTO t5_user_promotion(useridx, adflag, writedate) VALUES($nopay_21_useridx, 'cr_t5_nopay_21', '$today')";
					$promotion_nopay_21_sql = "INSERT INTO tbl_promotion_user(promoidx, useridx, type, status, writedate) VALUES($promoidx, $nopay_21_useridx, 'cr_t5_nopay_21', 0, NOW())";
					
					$insertcount++;
				}
				else if ($insertcount < 5000)
				{
					$promotion_nopay_21_redshift .= ",($nopay_21_useridx, 'cr_t5_nopay_21', '$today')";
					$promotion_nopay_21_sql .= ",($promoidx, $nopay_21_useridx, 'cr_t5_nopay_21', 0, NOW())";
					
					$insertcount++;
				}
				else 
				{	
					$promotion_nopay_21_redshift .= ",($nopay_21_useridx, 'cr_t5_nopay_21', '$today')";
					$db_redshift->execute($promotion_nopay_21_redshift);
					
					$promotion_nopay_21_sql .= ",($promoidx, $nopay_21_useridx, 'cr_t5_nopay_21', 0, NOW()) ON DUPLICATE KEY UPDATE writedate = VALUES(writedate);";
					$db_main2->execute($promotion_nopay_21_sql);
					
					sleep(1);
					
					$insertcount = 0;
					$promotion_nopay_21_redshift = "";
					$promotion_nopay_21_sql = "";
				}
			}
			
			if($promotion_nopay_21_redshift != '')
				$db_redshift->execute($promotion_nopay_21_redshift);
			
			if($promotion_nopay_21_sql != '')
			{
				$promotion_nopay_21_sql .= "ON DUPLICATE KEY UPDATE writedate = VALUES(writedate);";
				$db_main2->execute($promotion_nopay_21_sql);
			}		
			
			//nopay_21_re, 2주 이탈자 cr_take5_nopay_21_re
			$sql = "SELECT t1.useridx AS useridx ".
					"FROM t5_user t1	".
					"WHERE t1.useridx > 20000  AND createdate <= '$before_21day'  AND t1.fb_token <> '' ".
					"AND NOT EXISTS (SELECT * FROM t5_product_order WHERE useridx = t1.useridx AND STATUS = 1)	".
					"AND EXISTS (SELECT *  FROM duc_user WHERE logindate <= '$before_14day' AND fb_token = t1.fb_token) ".
					"AND NOT EXISTS (SELECT * FROM t5_user_promotion WHERE useridx = t1.useridx AND adflag = 'cr_t5_nopay_21_re');";
			$nopay_21_re_info = $db_redshift->gettotallist($sql);
			
			$promotion_nopay_21_re_redshift = "";
			$promotion_nopay_21_re_sql = "";
			
			$insertcount = 0;
				
			for($i = 0; $i < sizeof($nopay_21_re_info); $i++)
			{
				$nopay_21_re_useridx = $nopay_21_re_info[$i]['useridx'];				
				
				if($insertcount == 0)
				{
					$promotion_nopay_21_re_redshift = "INSERT INTO t5_user_promotion(useridx, adflag, writedate) VALUES($nopay_21_re_useridx, 'cr_t5_nopay_21_re', '$today')";
					$promotion_nopay_21_re_sql = "INSERT INTO tbl_promotion_user(promoidx, useridx, type, status, writedate) VALUES($promoidx, $nopay_21_re_useridx, 'cr_t5_nopay_21_re', 0, NOW())";
						
					$insertcount++;
				}
				else if ($insertcount < 5000)
				{
					$promotion_nopay_21_re_redshift .= ",($nopay_21_re_useridx, 'cr_t5_nopay_21_re', '$today')";
					$promotion_nopay_21_re_sql .= ",($promoidx, $nopay_21_re_useridx, 'cr_t5_nopay_21_re', 0, NOW())";
						
					$insertcount++;
				}
				else
				{
					$promotion_nopay_21_re_redshift .= ",($nopay_21_re_useridx, 'cr_t5_nopay_21_re', '$today')";
					$db_redshift->execute($promotion_nopay_21_re_redshift);
						
					$promotion_nopay_21_re_sql .= ",($promoidx, $nopay_21_re_useridx, 'cr_t5_nopay_21_re', 0, NOW()) ON DUPLICATE KEY UPDATE writedate = VALUES(writedate);";
					$db_main2->execute($promotion_nopay_21_re_sql);
						
					sleep(1);
						
					$insertcount = 0;
					$promotion_nopay_21_re_redshift = "";
					$promotion_nopay_21_re_sql = "";
				}
			}
			
			if($promotion_nopay_21_re_redshift != '')
				$db_redshift->execute($promotion_nopay_21_re_redshift);
			
			if($promotion_nopay_21_re_sql != '')
			{
				$promotion_nopay_21_re_sql .= "ON DUPLICATE KEY UPDATE writedate = VALUES(writedate);";			
				$db_main2->execute($promotion_nopay_21_re_sql);
			}	
			
			//pay_35, 신규 cr_take5_pay_35
			$sql = "SELECT DISTINCT t1.useridx AS useridx ".
					"FROM t5_product_order t1  JOIN t5_user_fb_token t2 ON t1.useridx = t2.useridx ". 
					"WHERE t1.useridx > 20000 AND t1.status = 1  AND t1.writedate <= '$before_35day' AND t2.fb_token <> '' ".
					"AND NOT EXISTS (SELECT * FROM t5_product_order WHERE useridx > 20000 AND STATUS = 1 AND useridx = t1.useridx AND writedate > '$before_35day')	".
					"AND NOT EXISTS (SELECT * FROM duc_user WHERE fb_token = t2.fb_token) ".
					"AND NOT EXISTS (SELECT * FROM t5_user_promotion WHERE useridx = t1.useridx AND adflag = 'cr_t5_pay_35');";
			$pay_35_info = $db_redshift->gettotallist($sql);
			
			$promotion_pay_35_redshift = "";
			$promotion_pay_35_sql = "";
			
			$insertcount = 0;
			
			for($i = 0; $i < sizeof($pay_35_info); $i++)
			{
				$pay_35_useridx = $pay_35_info[$i]['useridx'];
				
				if($insertcount == 0)
				{
					$promotion_pay_35_redshift = "INSERT INTO t5_user_promotion(useridx, adflag, writedate) VALUES($pay_35_useridx, 'cr_t5_pay_35', '$today')";
					$promotion_pay_35_sql = "INSERT INTO tbl_promotion_user(promoidx, useridx, type, status, writedate) VALUES($promoidx, $pay_35_useridx, 'cr_t5_pay_35', 0, NOW())";
				
					$insertcount++;
				}
				else if ($insertcount < 5000)
				{
					$promotion_pay_35_redshift .= ",($pay_35_useridx, 'cr_t5_pay_35', '$today')";
					$promotion_pay_35_sql .= ",($promoidx, $pay_35_useridx, 'cr_t5_pay_35', 0, NOW())";
				
					$insertcount++;
				}
				else
				{
					$promotion_pay_35_redshift .= ",($pay_35_useridx, 'cr_t5_pay_35', '$today')";
					$db_redshift->execute($promotion_pay_35_redshift);
				
					$promotion_pay_35_sql .= ",($promoidx, $pay_35_useridx, 'cr_t5_pay_35', 0, NOW()) ON DUPLICATE KEY UPDATE writedate = VALUES(writedate);";
					$db_main2->execute($promotion_pay_35_sql);
				
					sleep(1);
				
					$insertcount = 0;
					$promotion_pay_35_redshift = "";
					$promotion_pay_35_sql = "";
				}
			}
			
			if($promotion_pay_35_redshift != '')
				$db_redshift->execute($promotion_pay_35_redshift);
			
			if($promotion_pay_35_sql != '')
			{
				$promotion_pay_35_sql .= "ON DUPLICATE KEY UPDATE writedate = VALUES(writedate);";			
				$db_main2->execute($promotion_pay_35_sql);
			}
			
			//pay_35_re, 2주 이탈자 cr_take5_pay_35_re
			$sql = "SELECT DISTINCT t1.useridx AS useridx ".
					"FROM t5_product_order t1  JOIN t5_user_fb_token t2 ON t1.useridx = t2.useridx ". 
					"WHERE t1.useridx > 20000 AND t1.status = 1  AND t1.writedate <= '$before_35day' AND t2.fb_token <> '' ".
					"AND NOT EXISTS(SELECT * FROM t5_product_order WHERE useridx > 20000 AND STATUS = 1 AND useridx = t1.useridx AND writedate > '$before_35day')	".
					"AND EXISTS (SELECT *  FROM duc_user WHERE logindate <= '$before_14day' AND fb_token = t2.fb_token) ".
					"AND NOT EXISTS (SELECT * FROM t5_user_promotion WHERE useridx = t1.useridx AND adflag = 'cr_t5_pay_35_re');";
			$pay_35_re_info = $db_redshift->gettotallist($sql);
			
			$promotion_pay_35_re_redshift = "";
			$promotion_pay_35_re_sql = "";
			
			$insertcount = 0;
			
			for($i = 0; $i < sizeof($pay_35_re_info); $i++)
			{
				$pay_35_re_useridx = $pay_35_re_info[$i]['useridx'];
				
				if($insertcount == 0)
				{
					$promotion_pay_35_re_redshift = "INSERT INTO t5_user_promotion(useridx, adflag, writedate) VALUES($pay_35_re_useridx, 'cr_t5_pay_35_re', '$today')";
					$promotion_pay_35_re_sql = "INSERT INTO tbl_promotion_user(promoidx, useridx, type, status, writedate) VALUES($promoidx, $pay_35_re_useridx, 'cr_t5_pay_35_re', 0, NOW())";
				
					$insertcount++;
				}
				else if ($insertcount < 5000)
				{
					$promotion_pay_35_re_redshift .= ",($pay_35_re_useridx, 'cr_t5_pay_35_re', '$today')";
					$promotion_pay_35_re_sql .= ",($promoidx, $pay_35_re_useridx, 'cr_t5_pay_35_re', 0, NOW())";
				
					$insertcount++;
				}
				else
				{
					$promotion_pay_35_re_redshift .= ",($pay_35_re_useridx, 'cr_t5_pay_35_re', '$today')";
					$db_redshift->execute($promotion_pay_35_re_redshift);
				
					$promotion_pay_35_re_sql .= ",($promoidx, $pay_35_re_useridx, 'cr_t5_pay_35_re', 0, NOW()) ON DUPLICATE KEY UPDATE writedate = VALUES(writedate);";
					$db_main2->execute($promotion_pay_35_re_sql);
				
					sleep(1);
				
					$insertcount = 0;
					$promotion_pay_35_redshift = "";
					$promotion_pay_35_sql = "";
				}
			}
			
			if($promotion_pay_35_re_redshift != '')
				$db_redshift->execute($promotion_pay_35_re_redshift);
			
			if($promotion_pay_35_re_sql != '')
			{
				$promotion_pay_35_re_sql .= "ON DUPLICATE KEY UPDATE writedate = VALUES(writedate);";
				$db_main2->execute($promotion_pay_35_re_sql);
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main2->end();
	$db_redshift->end();
?>