<?
	include("../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();	

	$db_main->execute("SET wait_timeout=7200");
	$db_analysis->execute("SET wait_timeout=7200");	
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/game_request_group2_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
			$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
					$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);

		if (!$fp) 
		{
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/game_request_group2_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("game request group2 scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	
	try
	{
		$sql = 	" SELECT t1.useridx AS useridx, (SELECT userid FROM tbl_user WHERE useridx = t1.useridx) AS userid ".
				" FROM tbl_user_ext t1 JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx ". 
				" WHERE tutorial >= 3 AND t1.useridx > 766367 AND t1.logindate < DATE_SUB(NOW(), INTERVAL 27 HOUR) AND daylogincount = 1 ORDER BY t1.useridx DESC";		
		$noti_new_user_list = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($noti_new_user_list); $i++)
		{
			$noti_user_useridx = $noti_new_user_list[$i]["useridx"];
			$noti_user_fbid = $noti_new_user_list[$i]["userid"];	
			
			$sql = "SELECT COUNT(useridx) FROM notification_retention_log WHERE useridx = $noti_user_useridx AND status > 0;";
			$check_user = $db_analysis->getvalue($sql);
			
			$sql = "SELECT COUNT(*) FROM notification_retention_log WHERE status > 0 AND writedate > DATE_SUB(NOW(),INTERVAL 1 WEEK)";
			$noti_cnt = $db_analysis->getvalue($sql);
			
			if($check_user == 0)
			{
				try
				{
					if($noti_cnt < 15000)
					{
						$sql = "INSERT INTO notification_retention_log(useridx, status, writedate) VALUES($noti_user_useridx, 0, NOW()) ON DUPLICATE KEY UPDATE status=0;";
						$db_analysis->execute($sql);
					}
					else
					{
						break;
					}
						
					$facebook = new Facebook(array(
							'appId'  => FACEBOOK_APP_ID,
							'secret' => FACEBOOK_SECRET_KEY,
							'cookie' => true,
					));
			
					$session = $facebook->getUser();
			
					$template="@[$noti_user_fbid] It's time to have fun spinning on Take 5 Free Slots with your friends! Come join now!";
					$adflag = "gamenotifygroup2";
			
					$args = array('template' => "$template",
							'href' => "?adflag=$adflag",
							'ref' => "game_group2");
			
					$info = $facebook->api("/$noti_user_fbid/notifications", "POST", $args);
			
					$sql = "UPDATE notification_retention_log SET status = 1, writedate = NOW() WHERE useridx=$noti_user_useridx";
					$db_analysis->execute($sql);
					
					sleep(1);
				}
				catch (FacebookApiException $e)
				{
					
					$sql = "UPDATE notification_retention_log SET status = 3, writedate=NOW() WHERE useridx=$noti_user_useridx";
					$db_analysis->execute($sql);
				}
				
			}	
		}	
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();	
	$db_analysis->end();
?>