<?
	include("../../common/common_include.inc.php");

	$slave_main = new CDatabase_Main();
	$db_other = new CDatabase_Other();
	
	$slave_main->execute("SET wait_timeout=3600");
	$db_other->execute("SET wait_timeout=3600");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/total_product_order") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/backup/total_product_order") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("backup/total_product_order Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	
	try
	{
		$yesterday = date("Y-m-d",strtotime("-1 days"));
		$today = date("Y-m-d");
		
		$sql = "SELECT orderidx AS org_orderidx, useridx, productidx, os_type, usercoin, userlevel, coin, orderno, receipt, signature, ROUND(money,2) AS money, vip_point, status, gift_status, gift_coin, couponidx, special_discount, special_more, basecoin, canceldate, cancelleftcoin, disputed, errcount, writedate,coupon_type,product_type  
				FROM `tbl_product_order_mobile` WHERE os_type = 4";	
		$product_order = $slave_main->gettotallist($sql);
		
		$insert_sql = "";
		$insert_cnt = 0;
		
		for($i=0; $i<sizeof($product_order); $i++)
		{
			$org_orderidx = $product_order[$i]['org_orderidx'];
			$useridx = $product_order[$i]['useridx'];
			$productidx = $product_order[$i]['productidx'];
			$os_type = $product_order[$i]['os_type'];
			$usercoin = $product_order[$i]['usercoin'];
			$userlevel = $product_order[$i]['userlevel'];
			$coin = $product_order[$i]['coin'];
			$orderno = $product_order[$i]['orderno'];
			$receipt = $product_order[$i]['receipt'];
			$signature = $product_order[$i]['signature'];
			$money = $product_order[$i]['money'];
			$vip_point = $product_order[$i]['vip_point'];
			$status = $product_order[$i]['status'];
			$gift_status = $product_order[$i]['gift_status'];
			$gift_coin = $product_order[$i]['gift_coin'];
			$couponidx = $product_order[$i]['couponidx'];
			$special_discount = $product_order[$i]['special_discount'];
			$special_more = $product_order[$i]['special_more'];
			$basecoin = $product_order[$i]['basecoin'];
			$canceldate = $product_order[$i]['canceldate'];
			$cancelleftcoin = $product_order[$i]['cancelleftcoin'];
			$disputed = $product_order[$i]['disputed'];
			$errcount = $product_order[$i]['errcount'];
			$writedate = $product_order[$i]['writedate'];
			$coupon_type = $product_order[$i]['coupon_type'];
			$product_type = $product_order[$i]['product_type'];
			
			if($insert_sql == "")
			{
				$insert_sql = "INSERT INTO tbl_product_order_all(org_orderidx, useridx, productidx, os_type, usercoin, userlevel, coin, orderno, receipt, signature, money, vip_point, status, gift_status, gift_coin, couponidx, special_discount, special_more, basecoin, canceldate, cancelleftcoin, disputed, errcount,  writedate,coupon_type,product_type) ".
								" VALUES ($org_orderidx, $useridx, $productidx, $os_type, $usercoin, $userlevel, $coin, '$orderno', '$receipt', '$signature', '$money', $vip_point, $status, $gift_status, $gift_coin, $couponidx, $special_discount, $special_more, $basecoin, '$canceldate', $cancelleftcoin, $disputed, $errcount, '$writedate',$coupon_type,$product_type)";		
			}
			else  
				$insert_sql .= ",($org_orderidx, $useridx, $productidx, $os_type, $usercoin, $userlevel, $coin, '$orderno', '$receipt', '$signature', '$money', $vip_point, $status, $gift_status, $gift_coin, $couponidx, $special_discount, $special_more, $basecoin, '$canceldate', $cancelleftcoin, $disputed, $errcount, '$writedate',$coupon_type,$product_type)";
			
			$insert_cnt++;
			
			if($insert_cnt == 500)
			{
				$db_other->execute($insert_sql);
				
				$insert_sql = "";
				$insert_cnt = 0;					
			}
		}
		
		if($insert_sql != "")
			$db_other->execute($insert_sql);
			
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try 
	{
		$sql = "SELECT * FROM 
				(
					SELECT orderidx AS org_orderidx, useridx, 0 AS os_type, status, canceldate, cancelleftcoin, disputed
					FROM `tbl_product_order` WHERE useridx > 20000 AND DATE_SUB(NOW(), INTERVAL 2 WEEK) <=  canceldate  AND STATUS = 2
					UNION ALL
					SELECT orderidx AS org_orderidx, useridx, os_type, status, canceldate, cancelleftcoin, disputed
					FROM `tbl_product_order_mobile` WHERE useridx > 20000 AND DATE_SUB(NOW(), INTERVAL 2 WEEK) <=  canceldate  AND STATUS = 2
				) t1 ORDER BY canceldate ASC";
		
		$product_order_update = $slave_main->gettotallist($sql);
		
		$update_cnt = 0;
		
		for($i=0; $i<sizeof($product_order_update); $i++)
		{
			$org_orderidx = $product_order_update[$i]['org_orderidx'];
			$useridx = $product_order_update[$i]['useridx'];
			$os_type = $product_order_update[$i]['os_type'];			
			$status = $product_order_update[$i]['status'];
			$canceldate = $product_order_update[$i]['canceldate'];
			$cancelleftcoin = $product_order_update[$i]['cancelleftcoin'];
			$disputed = $product_order_update[$i]['disputed'];
			
			$update_sql = "UPDATE tbl_product_order_all SET canceldate = '$canceldate', cancelleftcoin = $cancelleftcoin, disputed = $disputed WHERE os_type = $os_type AND useridx = $useridx AND org_orderidx = $org_orderidx;";
			$db_other->execute($update_sql);
				
			$update_cnt++;
				
			if($update_cnt == 100)
				sleep(1);

		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try 
	{
		$sql = "SELECT * FROM 
				(
					SELECT orderidx AS org_orderidx, useridx, 0 AS os_type, status, canceldate, cancelleftcoin, disputed
					FROM `tbl_product_order` WHERE useridx > 20000 AND DATE_SUB(NOW(), INTERVAL 4 WEEK) <=  canceldate AND STATUS = 3
					UNION ALL
					SELECT orderidx AS org_orderidx, useridx, os_type, status, canceldate, cancelleftcoin, disputed
					FROM `tbl_product_order_mobile` WHERE useridx > 20000 AND DATE_SUB(NOW(), INTERVAL 4 WEEK) <=  canceldate AND STATUS = 3
				) t1 ORDER BY canceldate ASC";
		
		$product_order_update = $slave_main->gettotallist($sql);
		
		$update_cnt = 0;
		
		for($i=0; $i<sizeof($product_order_update); $i++)
		{
			$org_orderidx = $product_order_update[$i]['org_orderidx'];
			$useridx = $product_order_update[$i]['useridx'];
			$os_type = $product_order_update[$i]['os_type'];			
			$status = $product_order_update[$i]['status'];
			$canceldate = $product_order_update[$i]['canceldate'];
			$cancelleftcoin = $product_order_update[$i]['cancelleftcoin'];
			$disputed = $product_order_update[$i]['disputed'];
			
			$update_sql = "UPDATE tbl_product_order_all SET status = $status , canceldate = '$canceldate', cancelleftcoin = $cancelleftcoin, disputed = $disputed WHERE os_type = $os_type AND useridx = $useridx AND org_orderidx = $org_orderidx;";
			$db_other->execute($update_sql);
				
			$update_cnt++;
				
			if($update_cnt == 100)
				sleep(1);

		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	$slave_main->end();
        $db_other->end();	
	
?>
