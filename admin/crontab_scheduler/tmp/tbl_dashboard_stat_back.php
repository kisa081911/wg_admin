<?
	include("../../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();
	
	$db_main->execute("SET wait_timeout=7200");
	$db_analysis->execute("SET wait_timeout=7200");
	
	ini_set("memory_limit", "-1");
				
	//$sdate = date("Y-m-d", time() - 24 * 60 * 60 * 14);
	$sdate = date("Y-m-d", time() - 24 * 60 * 60 * 14);
	//$sdate = "2015-11-17";
	$edate = date("Y-m-d");
		
	while($sdate < $edate)
	{
		$temp_date = date('Y-m-d', strtotime($sdate));
		$today_date = date('Y-m-d', strtotime($sdate.' - 1 day'));
		
		$join_sdate = "$sdate";
		$join_edate = "$temp_date";
		
		$write_sdate = $join_sdate." 00:00:00";
		$write_edate = $join_edate." 00:00:00";
		
		$dashstat_where = "WHERE useridx > $str_useridx ";
		
		// 기간별 비할인 구매율 계산
		$sql = 	"SELECT SUM(facebookcredit)/10 FROM tbl_product_order ".
		  		"WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND couponidx = 0 AND special_discount = 0 AND special_more = 0";
		$normalpurchase_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(facebookcredit)/10 FROM tbl_product_order ".
		  		"WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND date_sub(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0";
		$normalpurchase_7days = $db_main->getvalue($sql);
		
		$sql = 	"	SELECT SUM(facebookcredit)/10 FROM tbl_product_order ".
		  		"	WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND date_sub(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0";
		$normalpurchase_28days = $db_main->getvalue($sql);
		
		$sql = 	"SELECT SUM(facebookcredit)/10 ".
		  		" FROM ( ".
		  		"	SELECT facebookcredit FROM tbl_product_order  WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND couponidx = 0 AND special_discount = 0 AND special_more = 0 ".
		  		"	UNION ALL ".
		  		"	SELECT facebookcredit FROM tbl_product_order_mobile  WHERE STATUS = 1 AND useridx > $str_useridx AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND couponidx = 0 AND special_discount = 0 AND special_more = 0 ".
		  		" ) t1";
		$total_normalpurchase_yesterday = $db_main->getvalue($sql);
		
		
		$sql = 	"	SELECT SUM(facebookcredit)/10 ".
		  		" FROM ( ".
		  		"    SELECT facebookcredit FROM tbl_product_order ".
		  		"    WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 ".
		  		"    UNION ALL ".
		  		"    SELECT facebookcredit FROM tbl_product_order_mobile ".
		  		"    WHERE STATUS = 1 AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 ".
		  		" ) t1";
		$total_normalpurchase_7days = $db_main->getvalue($sql);
		
		
		$sql = 	"	SELECT SUM(facebookcredit)/10 ".
		  		" FROM ( ".
		  		"    SELECT facebookcredit FROM tbl_product_order ".
		  		"    WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 ".
		  		"    UNION ALL ".
		  		"    SELECT facebookcredit FROM tbl_product_order_mobile ".
		  		"    WHERE STATUS = 1 AND useridx > $str_useridx AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 ".
		  		"    UNION ALL ".
		  		"    SELECT ROUND(IFNULL(SUM(money),0), 1) AS facebookcredit FROM tbl_product_order_ironsource ".
		  		"    WHERE DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1";
		$total_normalpurchase_28days = $db_main->getvalue($sql);
		
		//type 3 : Web
		//subtype
		//1 : 웹 최근 28일 비할인 구매율 , 2 : 웹 최근 7일 비할인 구매율, 3 : 웹 어제 비할인 구매율
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(3, 1, '$normalpurchase_28days', 'Pay(normalpurchase_28days)', now()) ON DUPLICATE KEY UPDATE value='$normalpurchase_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(3, 2, '$normalpurchase_7days', 'Pay(normalpurchase_7days)', now()) ON DUPLICATE KEY UPDATE value='$normalpurchase_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(3, 3, '$normalpurchase_yesterday', 'Pay(normalpurchase_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$normalpurchase_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		//type 403 : Total
		//subtype
		//1 : 전체 최근 28일 비할인 구매율 , 2 : 전체 최근 7일 비할인 구매율, 3 : 전체 어제 비할인 구매율
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(403, 1, '$total_normalpurchase_28days', 'Pay(total_normalpurchase_28days)', now()) ON DUPLICATE KEY UPDATE value='$total_normalpurchase_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(403, 2, '$total_normalpurchase_7days', 'Pay(total_normalpurchase_7days)', now()) ON DUPLICATE KEY UPDATE value='$total_normalpurchase_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(403, 3, '$total_normalpurchase_yesterday', 'Pay(total_normalpurchase_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$total_normalpurchase_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// IOS - Start
		$today = date("Y-m-d");
		$yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));
		
		// 최근 28일 결제 통계
		$sql = "SELECT SUM(money) FROM  ".
		  		" ( ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" UNION ALL  ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_ironsource WHERE os_type = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1";
		$ios_order_28day = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile_cancel WHERE useridx > $str_useridx AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < canceldate AND canceldate < NOW()";
		$ios_order_cancel_28day = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(cnt) AS cnt FROM ".
		  		" ( ".
		  		" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" UNION ALL ".
		  		" SELECT usercnt FROM tbl_product_order_ironsource WHERE os_type = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1";
		$ios_count_28day = $db_main->getvalue($sql);
		
		// 최근 7일 결제 통계
		$sql = "SELECT SUM(money) FROM  ".
		  		" ( ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" UNION ALL  ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_ironsource WHERE os_type = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1";
		$ios_order_7day = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile_cancel WHERE useridx > $str_useridx AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < canceldate AND canceldate < NOW()";
		$ios_order_cancel_7day = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(cnt) AS cnt FROM ".
		  		" ( ".
		  		" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" UNION ALL ".
		  		" SELECT usercnt FROM tbl_product_order_ironsource WHERE os_type = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1";
		$ios_count_7day = $db_main->getvalue($sql);
		
		// 어제 결제 통계
		$sql = "SELECT SUM(money) FROM  ".
		  		" ( ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" UNION ALL  ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_ironsource WHERE os_type = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" ) t1";
		$ios_order_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile_cancel WHERE useridx > $str_useridx AND os_type = 1 AND canceldate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$ios_order_cancel_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(cnt) AS cnt FROM ".
		  		" ( ".
		  		" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" UNION ALL ".
		  		" SELECT usercnt FROM tbl_product_order_ironsource WHERE os_type = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" ) t1";
		$ios_count_yesterday = $db_main->getvalue($sql);
		
		//type 101 : IOS
		//subtype
		//1 : 최근 28일 결제 금액, 2 : 최근 28일 결제 취소 금액, 3 : 최근 28일 결제 수
		//4 : 최근 7일 결제 금액, 5 : 최근 7일 결제 취소 금액, 6 : 최근 7일 결제 수
		//7 : 어제 결제 금액, 8 : 어제 결제 취소 금액, 9 : 어제 결제 수
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 1, '$ios_order_28day', 'IOS - Pay(order_28day)', now()) ON DUPLICATE KEY UPDATE value='$ios_order_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 2, '$ios_order_cancel_28day', 'IOS - Pay(order_cancel_28day)', now()) ON DUPLICATE KEY UPDATE value='$ios_order_cancel_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 3, '$ios_count_28day', 'IOS - Pay(count_28day)', now()) ON DUPLICATE KEY UPDATE value='$ios_count_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 4, '$ios_order_7day', 'IOS - Pay(order_7day)', now()) ON DUPLICATE KEY UPDATE value='$ios_order_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 5, '$ios_order_cancel_7day', 'IOS - Pay(order_cancel_7day)', now()) ON DUPLICATE KEY UPDATE value='$ios_order_cancel_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 6, '$ios_count_7day', 'IOS - Pay(count_7day)', now()) ON DUPLICATE KEY UPDATE value='$ios_count_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 7, '$ios_order_yesterday', 'IOS - Pay(order_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$ios_order_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 8, '$ios_order_cancel_yesterday', 'IOS - Pay(order_cancel_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$ios_order_cancel_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(101, 9, '$ios_count_yesterday', 'IOS - Pay(count_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$ios_count_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// IOS 기간별 비할인 구매율 계산
		$sql = "SELECT SUM(money) FROM ".
		  		" ( ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_mobile t1 ".
		  		" WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND couponidx = 0 AND special_discount = 0 AND special_more = 0 AND coin=basecoin ".
		  		" UNION ALL ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_ironsource t1 ".
		  		" WHERE os_type = 1 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" ) t1;";
		$ios_normalpurchase_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(money) FROM ".
		  		" ( ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_mobile t1 ".
		  		" WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 AND coin=basecoin ".
		  		" UNION ALL ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_ironsource t1 ".
		  		" WHERE os_type = 1 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1;";
		$ios_normalpurchase_7days = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(money) FROM ".
		  		" ( ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_mobile t1 ".
		  		" WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 AND coin=basecoin ".
		  		" UNION ALL ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_ironsource t1 ".
		  		" WHERE os_type = 1 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1;";
		$ios_normalpurchase_28days = $db_main->getvalue($sql);
		
		//type 103 : IOS
		//subtype
		//1 : IOS 최근 28일 비할인 구매율 , 2 : IOS 최근 7일 비할인 구매율, 3 : IOS 어제 비할인 구매율
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(103, 1, '$ios_normalpurchase_28days', 'IOS - Pay(normalpurchase_28days)', now()) ON DUPLICATE KEY UPDATE value='$ios_normalpurchase_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(103, 2, '$ios_normalpurchase_7days', 'IOS - Pay(normalpurchase_7days)', now()) ON DUPLICATE KEY UPDATE value='$ios_normalpurchase_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(103, 3, '$ios_normalpurchase_yesterday', 'IOS - Pay(normalpurchase_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$ios_normalpurchase_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// IOS - End
		
		// Android - Start
		$today = date("Y-m-d");
		$yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));
		
		// 최근 28일 결제 통계
		$sql = "SELECT SUM(money) FROM  ".
		  		" ( ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" UNION ALL  ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_ironsource WHERE os_type = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1";
		$android_order_28day = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile_cancel WHERE useridx > $str_useridx AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < canceldate AND canceldate < NOW()";
		$android_order_cancel_28day = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(cnt) AS cnt FROM ".
		  		" ( ".
		  		" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" UNION ALL ".
		  		" SELECT usercnt FROM tbl_product_order_ironsource WHERE os_type = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1";
		$android_count_28day = $db_main->getvalue($sql);
		
		// 최근 7일 결제 통계
		$sql = "SELECT SUM(money) FROM  ".
		  		" ( ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" UNION ALL  ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_ironsource WHERE os_type = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1";
		$android_order_7day = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile_cancel WHERE useridx > $str_useridx AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < canceldate AND canceldate < NOW()";
		$android_order_cancel_7day = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(cnt) AS cnt FROM ".
		  		" ( ".
		  		" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" UNION ALL ".
		  		" SELECT usercnt FROM tbl_product_order_ironsource WHERE os_type = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1";
		$android_count_7day = $db_main->getvalue($sql);
		
		// 어제 결제 통계
		$sql = "SELECT SUM(money) FROM  ".
		  		" ( ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 2 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" UNION ALL  ".
		  		" SELECT ROUND(IFNULL(SUM(money),0), 1) AS money FROM tbl_product_order_ironsource WHERE os_type = 2 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" ) t1";
		$android_order_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT ROUND(IFNULL(SUM(money),0), 1) FROM tbl_product_order_mobile_cancel WHERE useridx > $str_useridx AND os_type = 2 AND canceldate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
		$android_order_cancel_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(cnt) AS cnt FROM ".
		  		" ( ".
		  		" SELECT COUNT(*) AS cnt FROM tbl_product_order_mobile WHERE useridx > $str_useridx AND STATUS=1 AND os_type = 2 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" UNION ALL ".
		  		" SELECT usercnt FROM tbl_product_order_ironsource WHERE os_type = 2 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" ) t1";
		$android_count_yesterday = $db_main->getvalue($sql);
		
		//type 201 : Android
		//subtype
		//1 : 최근 28일 결제 금액, 2 : 최근 28일 결제 취소 금액, 3 : 최근 28일 결제 수
		//4 : 최근 7일 결제 금액, 5 : 최근 7일 결제 취소 금액, 6 : 최근 7일 결제 수
		//7 : 어제 결제 금액, 8 : 어제 결제 취소 금액, 9 : 어제 결제 수
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 1, '$android_order_28day', 'Android - Pay(order_28day)', now()) ON DUPLICATE KEY UPDATE value='$android_order_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 2, '$android_order_cancel_28day', 'Android - Pay(order_cancel_28day)', now()) ON DUPLICATE KEY UPDATE value='$android_order_cancel_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 3, '$android_count_28day', 'Android - Pay(count_28day)', now()) ON DUPLICATE KEY UPDATE value='$android_count_28day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 4, '$android_order_7day', 'Android - Pay(order_7day)', now()) ON DUPLICATE KEY UPDATE value='$android_order_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 5, '$android_order_cancel_7day', 'Android - Pay(order_cancel_7day)', now()) ON DUPLICATE KEY UPDATE value='$android_order_cancel_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 6, '$android_count_7day', 'Android - Pay(count_7day)', now()) ON DUPLICATE KEY UPDATE value='$android_count_7day', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 7, '$android_order_yesterday', 'Android - Pay(order_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$android_order_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 8, '$android_order_cancel_yesterday', 'Android - Pay(order_cancel_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$android_order_cancel_yesterday', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(201, 9, '$android_count_yesterday', 'Android - Pay(count_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$android_count_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		
		// Android 기간별 비할인 구매율 계산
		$sql = "SELECT SUM(money) FROM ".
		  		" ( ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_mobile t1 ".
		  		" WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 2 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND couponidx = 0 AND special_discount = 0 AND special_more = 0 AND coin=basecoin ".
		  		" UNION ALL ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_ironsource t1 ".
		  		" WHERE os_type = 2 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' ".
		  		" ) t1;";
		$android_normalpurchase_yesterday = $db_main->getvalue($sql);
		
		$sql = "SELECT SUM(money) FROM ".
		  		" ( ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_mobile t1 ".
		  		" WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 AND coin=basecoin ".
		  		" UNION ALL ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_ironsource t1 ".
		  		" WHERE os_type = 2 AND DATE_SUB(NOW(), INTERVAL 7 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1;";
		$android_normalpurchase_7days = $db_main->getvalue($sql);
		
		
		$sql = "SELECT SUM(money) FROM ".
		  		" ( ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_mobile t1 ".
		  		" WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() AND couponidx = 0 AND special_discount = 0 AND special_more = 0 AND coin=basecoin ".
		  		" UNION ALL ".
		  		" SELECT SUM(money) AS money FROM tbl_product_order_ironsource t1 ".
		  		" WHERE os_type = 2 AND DATE_SUB(NOW(), INTERVAL 28 DAY) < writedate AND writedate < NOW() ".
		  		" ) t1;";
		$android_normalpurchase_28days = $db_main->getvalue($sql);
		
		//type 203 : Android
		//subtype
		//1 : Android 최근 28일 비할인 구매율 , 2 : Android 최근 7일 비할인 구매율, 3 : Android 어제 비할인 구매율
		$sql = "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(203, 1, '$android_normalpurchase_28days', 'Android - Pay(normalpurchase_28days)', now()) ON DUPLICATE KEY UPDATE value='$android_normalpurchase_28days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(203, 2, '$android_normalpurchase_7days', 'Android - Pay(normalpurchase_7days)', now()) ON DUPLICATE KEY UPDATE value='$android_normalpurchase_7days', writedate=now();";
		$sql .= "INSERT INTO tbl_dashboard_stat(type, subtype, value, description, writedate) VALUES(203, 3, '$android_normalpurchase_yesterday', 'Android - Pay(normalpurchase_yesterday)', now()) ON DUPLICATE KEY UPDATE value='$android_normalpurchase_yesterday', writedate=now();";
		$db_analysis->execute($sql);
		
		// Android - End
		
		write_log("$write_sdate ~ $write_edate order 4 week insert");
		
		$sdate = date('Y-m-d', strtotime($temp_date.' + 1 day'));
	}
	
	$db_main->end();
	$db_analysis->end();
?>