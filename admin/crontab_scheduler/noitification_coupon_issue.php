<?
	include("../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_slave_main2 = new CDatabase_Slave_Main2();

	$db_main->execute("SET wait_timeout=7200");
	$db_main2->execute("SET wait_timeout=7200");
	$db_slave_main2->execute("SET wait_timeout=7200");	

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/noitification_coupon_issue") !== false)
		{
			$count++;
		}
	}
	
	function utf8_bytes($cp)
	{
	    if ($cp > 0x10000){
	        # 4 bytes
	        return	chr(0xF0 | (($cp & 0x1C0000) >> 18)).
	        chr(0x80 | (($cp & 0x3F000) >> 12)).
	        chr(0x80 | (($cp & 0xFC0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else if ($cp > 0x800){
	        # 3 bytes
	        return	chr(0xE0 | (($cp & 0xF000) >> 12)).
	        chr(0x80 | (($cp & 0xFC0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else if ($cp > 0x80){
	        # 2 bytes
	        return	chr(0xC0 | (($cp & 0x7C0) >> 6)).
	        chr(0x80 | ($cp & 0x3F));
	    }else{
	        # 1 byte
	        return chr($cp);
	    }
	}
	
	if ($count > 1)
		exit();	
	
	try
	{

		//프로모션 기간 체크
	    // 시즌 서프라이즈 쿠폰
	    // 시즌 기간 확인
	    $sql = "SELECT offeridx , TYPE  FROM tbl_coupon_promotion_setting WHERE startdate <= NOW() AND NOW() <= enddate AND status = 1;";
	    $check_coupon_promotion_array = $db_slave_main2->getarray($sql);
	    $check_coupon_promotion_offeridx = $check_coupon_promotion_array["offeridx"];
	    $check_coupon_promotion_type = $check_coupon_promotion_array["type"];
	                
	    if($check_coupon_promotion_offeridx == 0 || $check_coupon_promotion_offeridx == "")
		{
 			$sql = "SELECT useridx, userid FROM tbl_coupon_noti WHERE useridx > 20000 AND senddate <= NOW();";
			$noti_user_coupon_list = $db_slave_main2->gettotallist($sql);
			
			for($i=0; $i<sizeof($noti_user_coupon_list); $i++)
			{
				$noti_user_useridx = $noti_user_coupon_list[$i]["useridx"];
				$noti_user_fbid = $noti_user_coupon_list[$i]["userid"];
				
				$sql = "SELECT vip_level FROM tbl_user_detail t1 JOIN tbl_user_ext t2 ON t1.useridx = t2.useridx WHERE platform = 0 AND t1.useridx = $noti_user_useridx ".
						"AND NOT EXISTS (SELECT * FROM tbl_user_delete WHERE useridx = t1.useridx AND status = 1)";
				$noti_user_vip_level = $db_main->getvalue($sql);
				
				$sql="SELECT COUNT(*) FROM tbl_coupon WHERE category IN (2,3,4,10,11,12) AND useridx = $noti_user_useridx AND expiredate > NOW()";
				$check_coupon = $db_slave_main2->getvalue($sql); 
				
				$sql="SELECT count(*) FROM tbl_unlimited_coupon_target_user WHERE useridx=$noti_user_useridx";
				$check_seasonuser = $db_slave_main2->getvalue($sql);
				
				//특정유저 시즌 확인
				if($check_coupon_promotion_offeridx > 0 && $check_coupon_promotion_type < 2)
				    $check_promotion = 1;
			    else if ($check_coupon_promotion_offeridx > 0 && $check_coupon_promotion_type == 2 && $check_seasonuser > 0)
			    {
			        if($noti_user_useridx%1000 < 500)
			            $check_promotion  = 1;
			        else
			            $check_promotion  = 0;
			    }
			    else
			        $check_promotion  = 0;
				
			    if($noti_user_vip_level >= 4 && $check_coupon == 0 && $check_promotion == 0)
				{
					$day1 = "@[$noti_user_fbid]! ".utf8_bytes(0x1F609)." We've sent you VIP Coupons for an Amazing Deal! Check them out NOW! ".utf8_bytes(0x2705);
					$day2 = "@[$noti_user_fbid]!".utf8_bytes(0x1F603)." Don't miss these VIP Coupons for a special offer on Coins! ".utf8_bytes(0x1F4B2);
					$day3 = "@[$noti_user_fbid]! VIP Coupons have arrived! ".utf8_bytes(0x1F4E8)." Click here Now! ".utf8_bytes(0x1F446);
					$day4 = utf8_bytes(0x1F603)." Don't forget to use these Coupons for More Coins! ".utf8_bytes(0x1F4B0);
					$day5 = "@[$noti_user_fbid]! Waiting for VIP Coupons? ".utf8_bytes(0x1F603)." The Wait is Over! Click NOW! ".utf8_bytes(0x2705);
					
					$daily_template = array($day1, $day2, $day3, $day4, $day5);
					
					$date_flag = rand(0, 4);
					
					$template = $daily_template[$date_flag];
					
					$adflag = "couponnotify";
					
					$sql = "INSERT INTO tbl_coupon_noti_log(useridx, status, senddate) VALUES($noti_user_useridx, 0, NOW());";
					$db_main2->execute($sql);
					
					$sql = "SELECT LAST_INSERT_ID()";
					$notiidx = $db_main2->getvalue($sql);
					
					try
					{
						$facebook = new Facebook(array(
								'appId'  => FACEBOOK_APP_ID,
								'secret' => FACEBOOK_SECRET_KEY,
								'cookie' => true,
						));
					
						$session = $facebook->getUser();
					
						$args = array('template' => "$template",
								'href' => "?adflag=$adflag&notiidx=$notiidx",
								'ref' => "coupon_group1");
					
						$info = $facebook->api("/$noti_user_fbid/notifications", "POST", $args);
					
						$sql = "UPDATE tbl_coupon_noti_log SET status = 1, senddate = NOW() WHERE useridx=$noti_user_useridx AND notiidx = $notiidx";
						$db_main2->execute($sql);
											
						$sql = "DELETE FROM tbl_coupon_noti WHERE useridx=$noti_user_useridx";
						$db_main2->execute($sql);
						
				
					}
					catch (FacebookApiException $e)
					{					
						$sql = "UPDATE tbl_coupon_noti_log SET status = 3, senddate=NOW() WHERE useridx=$noti_user_useridx AND notiidx = $notiidx";
						$db_main2->execute($sql);
					}
				}
			}
		}		
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();	
	$db_slave_main2->end();
?>
