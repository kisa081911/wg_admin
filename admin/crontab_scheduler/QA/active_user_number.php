<?
	include("../../common/common_include.inc.php");
	
	$today = date("Y-m-d");
	$before_day = get_past_date($today,15,"d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	

	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT LEFT(today,10) AS today, todayjoincount, todayactivecount, todayfacebookactivecount, todayiosactivecount, todayandroidactivecount, todayamazonactivecount  ".
			"FROM user_join_log WHERE today >= '$startdate 00:00:00' AND today <= '$enddate 23:59:59'  GROUP BY LEFT(today,10) ORDER BY today DESC";	
	$joinlog_list = $db_analysis->gettotallist($sql);
?>
<style>  
.mytable { border-collapse:collapse; }  
.mytable th, .mytable td { border:1px solid black; }
.mytable th, .mytable td { text-align:center; }
</style>

        <div id="tab_content_1">
    		<table class="mytable">
    			<colgroup>
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    			</colgroup>
    			<thead>
    				<tr>
    					<th class="tdc">날짜</th>
    					<th class="tdc">활동회원수</th>
    					<th class="tdc">Facebook활동회원수</th>
    					<th class="tdc">IOS활동회원수</th>
    					<th class="tdc">Android활동회원수</th>
    					<th class="tdc">Amazon활동회원수</th>
    					<th class="tdc">재방문회원수</th>
    				</tr>
    			</thead>
    			<tbody>  
<?
    			for($i=0; $i<sizeof($joinlog_list); $i++)
    			{
    				$today = $joinlog_list[$i]["today"];
    				$todayjoincount = $joinlog_list[$i]["todayjoincount"];
    				$todayactivecount = $joinlog_list[$i]["todayactivecount"];
    				$todayfacebookactivecount = $joinlog_list[$i]["todayfacebookactivecount"];
    				$todayiosactivecount = $joinlog_list[$i]["todayiosactivecount"];
    				$todayandroidactivecount = $joinlog_list[$i]["todayandroidactivecount"];
    				$todayamazonactivecount = $joinlog_list[$i]["todayamazonactivecount"];
    				
    				$revisitcount = $todayactivecount - $todayjoincount;
?>
					<tr>
    					<td class="tdc point_title"><?= $today?></td>
    					<td class="tdc"><?= number_format($todayactivecount) ?></td>
    					<td class="tdc"><?= number_format($todayfacebookactivecount) ?></td>
    					<td class="tdc"><?= number_format($todayiosactivecount) ?></td>
    					<td class="tdc"><?= number_format($todayandroidactivecount) ?></td>
    					<td class="tdc"><?= number_format($todayamazonactivecount) ?></td>
    					<td class="tdc"><?= number_format($revisitcount) ?></td>
    				</tr>	
<?
				}
?>
    			</tbody>
    		</table>
    	</div>

	
	
<?
	$db_analysis->end();
?>