<?
	include("../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_other = new CDatabase_Other();
	
	$db_main->execute("SET wait_timeout=72000");
	$db_analysis->execute("SET wait_timeout=72000");
	$db_main2->execute("SET wait_timeout=72000");
	$db_other->execute("SET wait_timeout=72000");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$port = "";
	
	$str_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}
	
	// 사용자 유지현황(광고)
	try
	{
		// 과거 데이터 Insert
		$startdate = "2017-02-19";
		$enddate = "2017-02-19";
		
		while($startdate <= $enddate)
		{
			$today = $startdate;
			
			$tempdate = date('Y-m-d', strtotime($startdate.' + 1 day'));
		
			write_log("mobile marketing Scheduler Start : ".$today);
			
			// 사용자 유지 현황 - 월별 신규회원수 조회(Ios)
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS joincount ".
					"FROM tbl_user_ext ".
					"WHERE '2017-01-19 00:00:00' <= createdate AND createdate < '$tempdate 00:00:00'".
					"	AND platform = 1 AND adflag NOT LIKE 'share%' ".
					"	AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			$reguser_list = $db_main->gettotallist($sql);
			
			for ($i=0; $i<sizeof($reguser_list); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 1;
				$regmonth = $reguser_list[$i]["regmonth"];
				$joincnt = $reguser_list[$i]["joincount"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount) VALUES('$today', $type, '$regmonth', $joincnt) ON DUPLICATE KEY UPDATE joincount=$joincnt";
				$db_analysis->execute($sql);
			}
			
			// Ios 활동유저수
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS activecnt ".
					"FROM tbl_user_ext ".
					"WHERE '2017-01-19 00:00:00' <= createdate AND createdate < '$tempdate 00:00:00' ".
					"	AND platform = 1 AND adflag NOT LIKE 'share%' ".
					"	AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					"	AND logindate > date_sub(DATE_FORMAT('$today', '%Y-%m-%d'), INTERVAL 2 WEEK) AND logindate < '$tempdate 00:00:00' ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			
			$webactivelist = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($webactivelist); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 1;
				$regmonth = $reguser_list[$i]["regmonth"];
				$activecnt = $webactivelist[$i]["activecnt"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
						"VALUES('$today', $type, '$regmonth', 0, $activecnt, 0, 0, 0, 0) ON DUPLICATE KEY UPDATE activecount=$activecnt";
			
				$db_analysis->execute($sql);
			}
			
			// Ios 가입자 누적 웹 결제
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
					"FROM ( ".
					"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate < '$tempdate 00:00:00'), 0 ) AS totalcredit ".
					"	FROM tbl_user_ext ".
					"	WHERE '2017-01-19 00:00:00' <= createdate AND createdate < '$tempdate 00:00:00' ".
					"		AND platform = 1 AND adflag NOT LIKE 'share%' ".
					"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					") t1 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			
			$webtotalcreditlist = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($webtotalcreditlist); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 1;
				$regmonth = $reguser_list[$i]["regmonth"];
				$webcredit = $webtotalcreditlist[$i]["totalcredit"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
						"VALUES('$today', $type, '$regmonth', 0, 0, $webcredit, 0, 0, 0) ON DUPLICATE KEY UPDATE webcredit=$webcredit";
			
				$db_analysis->execute($sql);
			}
			
			// Ios 가입자 누적 모바일 결제
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
					"FROM ( ".
					"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate < '$tempdate 00:00:00'), 0 ) AS totalcredit ".
					"	FROM tbl_user_ext ".
					"	WHERE '2017-01-19 00:00:00' <= createdate AND createdate < '$tempdate 00:00:00' ".
					"		AND platform = 1 AND adflag NOT LIKE 'share%' ".
					"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					") t1 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			
			$mobiletotalcreditlist = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 1;
				$regmonth = $reguser_list[$i]["regmonth"];
				$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
						"VALUES('$today', $type, '$regmonth', 0, 0, 0, $mobilecredit, 0, 0) ON DUPLICATE KEY UPDATE mobilecredit=$mobilecredit";
			
				$db_analysis->execute($sql);
			}
			
			// Ios 가입자 최근2주 웹 결제
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
					"FROM ( ".
					"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB('$today', INTERVAL 2 WEEK) AND writedate < '$tempdate 00:00:00'), 0 ) AS totalcredit ".
					"	FROM tbl_user_ext ".
					"	WHERE '2017-01-19 00:00:00' <= createdate AND createdate < '$tempdate 00:00:00' ".
					"		AND platform = 1 AND adflag NOT LIKE 'share%' ".
					"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					") t1 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			
			$webtotalcreditlist = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($webtotalcreditlist); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 1;
				$regmonth = $reguser_list[$i]["regmonth"];
				$webcredit = $webtotalcreditlist[$i]["totalcredit"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
						"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, $webcredit, 0) ON DUPLICATE KEY UPDATE webcredit2week=$webcredit";
			
				$db_analysis->execute($sql);
			}
			
			// Ios 가입자 최근2주 모바일 결제
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
					"FROM ( ".
					"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB('$today', INTERVAL 2 WEEK) AND writedate < '$tempdate 00:00:00'), 0 ) AS totalcredit ".
					"	FROM tbl_user_ext ".
					"	WHERE '2017-01-19 00:00:00' <= createdate AND createdate < '$tempdate 00:00:00' ".
					"		AND platform = 1 AND adflag NOT LIKE 'share%' ".
					"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					") t1 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			
			$mobiletotalcreditlist = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 1;
				$regmonth = $reguser_list[$i]["regmonth"];
				$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
						"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, 0, $mobilecredit) ON DUPLICATE KEY UPDATE mobilecredit2week=$mobilecredit";
			
				$db_analysis->execute($sql);
			}
			
			// 사용자 유지 현황 - 월별 신규회원수 조회(Android)
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS joincount ".
					"FROM tbl_user_ext ".
					"WHERE '2017-01-19 00:00:00' <= createdate  AND createdate < '$tempdate 00:00:00' ".
					"	AND platform = 2 AND adflag NOT LIKE 'share%' ".
					"	AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			$reguser_list = $db_main->gettotallist($sql);
			
			for ($i=0; $i<sizeof($reguser_list); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 2;
				$regmonth = $reguser_list[$i]["regmonth"];
				$joincnt = $reguser_list[$i]["joincount"];
									
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount) VALUES('$today', $type, '$regmonth', $joincnt) ON DUPLICATE KEY UPDATE joincount=$joincnt";
				$db_analysis->execute($sql);
			}
			
			// Android 활동유저수
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS activecnt ".
					"FROM tbl_user_ext ".
					"WHERE '2017-01-19 00:00:00' <= createdate  AND createdate < '$tempdate 00:00:00' ".
					"	AND platform = 2 AND adflag NOT LIKE 'share%' ".
					"	AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					"	AND logindate > date_sub(DATE_FORMAT('$today', '%Y-%m-%d'), INTERVAL 2 WEEK) AND logindate < '$tempdate 00:00:00' ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			
			$webactivelist = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($webactivelist); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 2;
				$regmonth = $reguser_list[$i]["regmonth"];
				$activecnt = $webactivelist[$i]["activecnt"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
						"VALUES('$today', $type, '$regmonth', 0, $activecnt, 0, 0, 0, 0) ON DUPLICATE KEY UPDATE activecount=$activecnt";
			
				$db_analysis->execute($sql);
			}
			
			// Android 가입자 누적 웹 결제
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
					"FROM ( ".
					"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate < '$tempdate 00:00:00'), 0 ) AS totalcredit ".
					"	FROM tbl_user_ext ".
					"	WHERE '2017-01-19 00:00:00' <= createdate  AND createdate < '$tempdate 00:00:00' ".
					"		AND platform = 2 AND adflag NOT LIKE 'share%' ".
					"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					") t1 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			
			$webtotalcreditlist = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($webtotalcreditlist); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 2;
				$regmonth = $reguser_list[$i]["regmonth"];
				$webcredit = $webtotalcreditlist[$i]["totalcredit"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
						"VALUES('$today', $type, '$regmonth', 0, 0, $webcredit, 0, 0, 0) ON DUPLICATE KEY UPDATE webcredit=$webcredit";
			
				$db_analysis->execute($sql);
			}
			
			// Android 가입자 누적 모바일 결제		
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
					"FROM ( ".
					"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate < '$tempdate 00:00:00'), 0 ) AS totalcredit ".
					"	FROM tbl_user_ext ".
					"	WHERE '2017-01-19 00:00:00' <= createdate AND createdate < '$tempdate 00:00:00' ".
					"		AND platform = 2 AND adflag NOT LIKE 'share%' ".
					"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					") t1 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			
			$mobiletotalcreditlist = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 2;
				$regmonth = $reguser_list[$i]["regmonth"];
				$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
						"VALUES('$today', $type, '$regmonth', 0, 0, 0, $mobilecredit, 0, 0) ON DUPLICATE KEY UPDATE mobilecredit=$mobilecredit";
			
				$db_analysis->execute($sql);
			}
			
			// Android 가입자 최근2주 웹 결제
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
					"FROM ( ".
					"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB('$today', INTERVAL 2 WEEK) AND writedate < '$tempdate 00:00:00'), 0 ) AS totalcredit ".
					"	FROM tbl_user_ext ".
					"	WHERE '2017-01-19 00:00:00' <= createdate AND createdate < '$tempdate 00:00:00' ".
					"		AND platform = 2 AND adflag NOT LIKE 'share%' ".
					"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					") t1 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			
			$webtotalcreditlist = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($webtotalcreditlist); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 2;
				$regmonth = $reguser_list[$i]["regmonth"];
				$webcredit = $webtotalcreditlist[$i]["totalcredit"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
						"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, $webcredit, 0) ON DUPLICATE KEY UPDATE webcredit2week=$webcredit";
			
				$db_analysis->execute($sql);
			}
			
			// Android 가입자 최근2주 모바일 결제
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
					"FROM ( ".
					"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB('$today', INTERVAL 2 WEEK) AND writedate < '$tempdate 00:00:00'), 0 ) AS totalcredit ".
					"	FROM tbl_user_ext ".
					"	WHERE '2017-01-19 00:00:00' <= createdate AND createdate < '$tempdate 00:00:00' ".
					"		AND platform = 2 AND adflag NOT LIKE 'share%' ".
					"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					") t1 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			
			$mobiletotalcreditlist = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 2;
				$regmonth = $reguser_list[$i]["regmonth"];
				$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
						"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, 0, $mobilecredit) ON DUPLICATE KEY UPDATE mobilecredit2week=$mobilecredit";
			
				$db_analysis->execute($sql);
			}
			
			// 사용자 유지 현황 - 월별 신규회원수 조회(Amazon)
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS joincount ".
					"FROM tbl_user_ext ".
					"WHERE '2017-01-19 00:00:00' <= createdate AND createdate < '$tempdate 00:00:00' ".
					"	AND platform = 3 AND adflag NOT LIKE 'share%' ".
					"	AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			$reguser_list = $db_main->gettotallist($sql);
			
			for ($i=0; $i<sizeof($reguser_list); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 3;
				$regmonth = $reguser_list[$i]["regmonth"];
				$joincnt = $reguser_list[$i]["joincount"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount) VALUES('$today', $type, '$regmonth', $joincnt) ON DUPLICATE KEY UPDATE joincount=$joincnt";
				$db_analysis->execute($sql);
			}
			
			// Amazon 활동유저수
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, COUNT(useridx) AS activecnt ".
					"FROM tbl_user_ext ".
					"WHERE '2017-01-19 00:00:00' <= createdate AND createdate < '$tempdate 00:00:00' ".
					"	AND platform = 3 AND adflag NOT LIKE 'share%' ".
					"	AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					"	AND logindate > date_sub(DATE_FORMAT('$today', '%Y-%m-%d'), INTERVAL 2 WEEK) AND logindate < '$tempdate 00:00:00' ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			
			$webactivelist = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($webactivelist); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 3;
				$regmonth = $reguser_list[$i]["regmonth"];
				$activecnt = $webactivelist[$i]["activecnt"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
						"VALUES('$today', $type, '$regmonth', 0, $activecnt, 0, 0, 0, 0) ON DUPLICATE KEY UPDATE activecount=$activecnt";
			
				$db_analysis->execute($sql);
			}
			
			// Amazon 가입자 누적 웹 결제
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
					"FROM ( ".
					"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate < '$tempdate 00:00:00'), 0 ) AS totalcredit ".
					"	FROM tbl_user_ext ".
					"	WHERE '2017-01-19 00:00:00' <= createdate AND createdate < '$tempdate 00:00:00' ".
					"		AND platform = 3 AND adflag NOT LIKE 'share%' ".
					"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					") t1 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			
			$webtotalcreditlist = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($webtotalcreditlist); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 3;
				$regmonth = $reguser_list[$i]["regmonth"];
				$webcredit = $webtotalcreditlist[$i]["totalcredit"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
						"VALUES('$today', $type, '$regmonth', 0, 0, $webcredit, 0, 0, 0) ON DUPLICATE KEY UPDATE webcredit=$webcredit";
			
				$db_analysis->execute($sql);
			}
			
			// Amazon 가입자 누적 모바일 결제
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
					"FROM ( ".
					"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate < '$tempdate 00:00:00'), 0 ) AS totalcredit ".
					"	FROM tbl_user_ext ".
					"	WHERE '2017-01-19 00:00:00' <= createdate AND createdate < '$tempdate 00:00:00' ".
					"		AND platform = 3 AND adflag NOT LIKE 'share%' ".
					"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					") t1 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			
			$mobiletotalcreditlist = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 3;
				$regmonth = $reguser_list[$i]["regmonth"];
				$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
						"VALUES('$today', $type, '$regmonth', 0, 0, 0, $mobilecredit, 0, 0) ON DUPLICATE KEY UPDATE mobilecredit=$mobilecredit";
			
				$db_analysis->execute($sql);
			}
			
			// Amazon 가입자 최근2주 웹 결제
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit)/10, 2) AS totalcredit ".
					"FROM ( ".
					"	SELECT useridx, createdate, IFNULL((SELECT SUM(facebookcredit) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB('$today', INTERVAL 2 WEEK) AND writedate < '$tempdate 00:00:00'), 0 ) AS totalcredit ".
					"	FROM tbl_user_ext ".
					"	WHERE '2017-01-19 00:00:00' <= createdate AND createdate < '$tempdate 00:00:00' ".
					"		AND platform = 3 AND adflag NOT LIKE 'share%' ".
					"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					") t1 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			
			$webtotalcreditlist = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($webtotalcreditlist); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 3;
				$regmonth = $reguser_list[$i]["regmonth"];
				$webcredit = $webtotalcreditlist[$i]["totalcredit"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
						"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, $webcredit, 0) ON DUPLICATE KEY UPDATE webcredit2week=$webcredit";
			
				$db_analysis->execute($sql);
			}
			
			// Amazon 가입자 최근2주 모바일 결제
			$sql = "SELECT date_format('$today', '%Y-%m-%d') AS today, DATE_FORMAT(createdate, '%Y-%m') AS regmonth, ROUND(SUM(totalcredit), 2) AS totalcredit ".
					"FROM ( ".
					"	SELECT useridx, createdate, IFNULL((SELECT SUM(money) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status = 1 AND writedate > DATE_SUB('$today', INTERVAL 2 WEEK) AND writedate < '$tempdate 00:00:00'), 0 ) AS totalcredit ".
					"	FROM tbl_user_ext ".
					"	WHERE '2017-01-19 00:00:00' <= createdate AND createdate < '$tempdate 00:00:00' ".
					"		AND platform = 3 AND adflag NOT LIKE 'share%' ".
					"		AND (adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Facebook%' OR adflag LIKE 'nanigans%' OR adflag LIKE 'Twitter%' OR adflag LIKE 'amazon%') ".
					") t1 ".
					"GROUP BY DATE_FORMAT(createdate, '%Y-%m')";
			
			$mobiletotalcreditlist = $db_main->gettotallist($sql);
			
			for($i=0; $i<sizeof($mobiletotalcreditlist); $i++)
			{
				$today = $reguser_list[$i]["today"];
				$type = 3;
				$regmonth = $reguser_list[$i]["regmonth"];
				$mobilecredit = $mobiletotalcreditlist[$i]["totalcredit"];
			
				$sql = "INSERT INTO _tmp_tbl_user_inflowroute_daily_marketing(today, type, regmonth, joincount, activecount, webcredit, mobilecredit, webcredit2week, mobilecredit2week) ".
						"VALUES('$today', $type, '$regmonth', 0, 0, 0, 0, 0, $mobilecredit) ON DUPLICATE KEY UPDATE mobilecredit2week=$mobilecredit";
			
				$db_analysis->execute($sql);
			}
			
			write_log("mobile marketing Scheduler End : ".$today);
			
			$startdate = date('Y-m-d', strtotime($startdate.' + 1 day'));
		}	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}

	$db_main->end();
	$db_main2->end();
	$db_analysis->end();
	$db_other->end();
?>