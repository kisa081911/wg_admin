<?
	include("../common/common_include.inc.php");

    $db_main = new CDatabase_Main();
    $db_analysis = new CDatabase_Analysis();
    $db_livestat = new CDatabase_Livestats();

    $db_main->execute("SET wait_timeout=72000");
    $db_analysis->execute("SET wait_timeout=72000");
    $db_livestat->execute("SET wait_timeout=72000");

    $issuccess = "1";

    $today = date("Y-m-d");

    $port = "";

    $str_useridx = 20000;

    if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
    {
        $port = ":8081";
        $str_useridx = 10000;
    }

    try
    {
        $sql = "INSERT INTO tbl_scheduler_log(today, type, status, writedate) VALUES('$today', 404, 0, NOW());";
        $db_analysis->execute($sql);
        $sql = "SELECT LAST_INSERT_ID();";
        $logidx = $db_analysis->getvalue($sql);
    }
    catch(Exception $e)
    {
        write_log($e->getMessage());
    }

    $result = array();
    exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);

    $count = 0;

    for ($i=0; $i<sizeof($result); $i++)
    {
        if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/daily_userleave_scheduler") !== false)
        {
            $count++;
        }
    }

    if ($count > 1)
    {
        $count = 0;

        $killcontents = "#!/bin/bash\n";

        $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
        $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];

        $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');

        flock($fp, LOCK_EX);

        if (!$fp) {
            echo "Fail";
            exit;
        }
        else
        {
            echo "OK";
        }

        flock($fp, LOCK_UN);

        $content = "#!/bin/bash\n";

        for ($i=0; $i<sizeof($result); $i++)
        {
            if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/daily_userleave_scheduler") !== false)
            {
                $process_list = explode("|", $result[$i]);

                $content .= "kill -9 ".$process_list[0]."\n";

                write_log("daily_scheduler Dead Lock Kill!");
            }
        }

        fwrite($fp, $content, strlen($content));
        fclose($fp);

        exit();
    }

    try
    {
        $twoWeekAgo = get_past_date(date("Y-m-d"), 14, 'd');
        
        $sql = "SELECT ". 
				"IFNULL(SUM(@is_vip:=(CASE WHEN vip_point >=99 THEN 1 ELSE 0 END)), 0) AS leave_vip,	". 
				"IFNULL(SUM(@is_continuous_order :=(CASE WHEN @is_vip = 0 AND buy_count >= 5 THEN 1 ELSE 0 END)), 0) AS leave_continuous_order,	". 
				"IFNULL(SUM(@is_first_order :=(CASE WHEN @is_vip = 0 AND @is_continuous_order = 0 AND buy_count >= 1 AND buy_count < 5 THEN 1 ELSE 0 END)), 0) AS leave_first_order,	". 
				"IFNULL(SUM(@is_continuous_play :=(CASE WHEN @is_vip = 0 AND @is_continuous_order = 0 AND @is_first_order=0 AND daylogincount>6 THEN 1 ELSE 0 END)), 0) AS leave_continuous_play,	". 
				"IFNULL(SUM(CASE WHEN @is_vip = 0 AND @is_continuous_order = 0 AND @is_first_order=0 AND @is_continuous_play=0 THEN 1 ELSE 0 END), 0) AS leave_normal	".
				"FROM	".
				"(	".
				"	SELECT ". 
				"	t1.useridx AS useridx, t2.vip_point AS vip_point, ". 
				"	(SELECT daylogincount FROM tbl_user_ext WHERE useridx = t1.useridx) AS daylogincount, ".
				"	(SELECT COUNT(*) FROM tbl_product_order WHERE STATUS = 1 AND useridx = t1.useridx) AS buy_count ".
				"	FROM tbl_user t1 JOIN tbl_user_detail t2  ON t1.useridx = t2.useridx WHERE t1.useridx > $str_useridx ". 
				"	AND '$twoWeekAgo 00:00:00' <= logindate AND logindate <= '$twoWeekAgo 23:59:59' ".
				") t3";
        $userLeaveDaily = $db_main->getarray($sql);

        $leave_normal = $userLeaveDaily["leave_normal"];
        $leave_continuous_play = $userLeaveDaily["leave_continuous_play"];
        $leave_first_order = $userLeaveDaily["leave_first_order"];
        $leave_continuous_order = $userLeaveDaily["leave_continuous_order"];
        $leave_vip = $userLeaveDaily["leave_vip"];

        $sql = "INSERT INTO tbl_user_leave_daily (today, leave_normal, leave_continuous_play, leave_first_order, leave_continuous_order, leave_vip) ".
            	" VALUES('$today', $leave_normal, $leave_continuous_play, $leave_first_order, $leave_continuous_order,  $leave_vip) " .
            	" ON DUPLICATE KEY UPDATE leave_normal=VALUES(leave_normal), leave_continuous_play=VALUES(leave_continuous_play), ".
            	" leave_first_order=VALUES(leave_first_order), leave_continuous_order=VALUES(leave_continuous_order), leave_vip=VALUES(leave_vip)";
        $db_livestat->execute($sql);
    }
    catch(Exception $e)
    {
        write_log($e->getMessage());
        $issuccess = "0";
    }

    try
    {
        $sql = "UPDATE tbl_scheduler_log SET status = $issuccess, writedate = NOW() WHERE logidx = $logidx;";
        $db_analysis->execute($sql);
    }
    catch(Exception $e)
    {
        write_log($e->getMessage());
    }

    $db_main->end();
    $db_analysis->end();
    $db_livestat->end();
?>