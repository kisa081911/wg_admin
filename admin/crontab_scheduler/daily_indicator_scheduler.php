<?
	include("../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();
	
	$db_main->execute("SET wait_timeout=72000");
	$db_analysis->execute("SET wait_timeout=72000");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}

	try
	{
		$sql = "INSERT INTO tbl_scheduler_log(today, type, status, writedate) VALUES('$today', 501, 0, NOW());";
		$db_analysis->execute($sql);
		$sql = "SELECT LAST_INSERT_ID();";
		$logidx = $db_analysis->getvalue($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/daily_indicator_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/daily_indicator_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("daily_indicator_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	try
	{
		$indicator_today = date("Y-m-d", time() - 60 * 60 * 24 * 1);
		$indicator_yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 2);
		$indicator_week = date("Y-m-d", time() - 60 * 60 * 24 * 8);
		$indicator_month = date("Y-m-d", time() - 60 * 60 * 24 * 31);
		$indicator_quarter = date("Y-m-d", time() - 60 * 60 * 24 * 85);		
		
		// 1.신규회원가입수
		// (1) 전체
		// 당일
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_today 00:00:00' <= createdate AND createdate <= '$indicator_today 23:59:59'";
		$today_all_join = $db_main->getvalue($sql);
		
		// 전일
		//$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_yesterday 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59'";
		//$yesterday_all_join = $db_main->getvalue($sql);
		$yesterday_all_join = 0;
			
		// 전주
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_week 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' ".
			   "AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(createdate)";
		$week_all_join = $db_main->getvalue($sql);
			
		// 전월
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_month 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(createdate)";
		$month_all_join = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_quarter 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(createdate)";		
		$quarter_all_join = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 10, $today_all_join, $yesterday_all_join, $week_all_join, $month_all_join, $quarter_all_join) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month) , quarter = VALUES(quarter)";
			
		$db_analysis->execute($sql);

		// (2) facebook
		// 당일
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_today 00:00:00' <= createdate AND createdate <= '$indicator_today 23:59:59' AND platform NOT IN (1, 2, 3)";
		$today_facebook_join = $db_main->getvalue($sql);
		
		// 전일
		//$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_yesterday 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND platform NOT IN (1, 2, 3)";
		//$yesterday_facebook_join = $db_main->getvalue($sql);
		$yesterday_facebook_join = 0;
			
		// 전주
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_week 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND platform NOT IN (1, 2, 3) ".
			   "AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(createdate)";
		$week_facebook_join = $db_main->getvalue($sql);
			
		// 전월
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_month 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND platform NOT IN (1, 2, 3) ".
			   "AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(createdate)";
		$month_facebook_join = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_quarter 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND platform NOT IN (1, 2, 3) ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(createdate)";
		$quarter_facebook_join = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 11, $today_facebook_join, $yesterday_facebook_join, $week_facebook_join, $month_facebook_join, $quarter_facebook_join) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
			
		$db_analysis->execute($sql);
		
		// (3) Apple
		// 당일
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_today 00:00:00' <= createdate AND createdate <= '$indicator_today 23:59:59' AND platform = 1";
		$today_apple_join = $db_main->getvalue($sql);
		
		// 전일
		//$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_yesterday 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND adflag = 'mobile'";
		//$yesterday_apple_join = $db_main->getvalue($sql);
		$yesterday_apple_join = 0;
			
		// 전주
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_week 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND platform = 1 ".
			   "AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(createdate)";
		$week_apple_join = $db_main->getvalue($sql);
			
		// 전월
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_month 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND platform = 1 " .
			   "AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(createdate)";
		$month_apple_join = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_quarter 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND platform = 1 " .
			   "AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(createdate)";
		$quarter_apple_join = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 12, $today_apple_join, $yesterday_apple_join, $week_apple_join, $month_apple_join, $quarter_apple_join) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
		
		$db_analysis->execute($sql);
		
		// (4) Google
		// 당일
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_today 00:00:00' <= createdate AND createdate <= '$indicator_today 23:59:59' AND platform = 2";
		$today_google_join = $db_main->getvalue($sql);
		
		// 전일
		//$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_yesterday 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND adflag = 'mobile_ad'";
		//$yesterday_google_join = $db_main->getvalue($sql);
		$yesterday_google_join = 0;
			
		// 전주
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_week 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND platform = 2 ".
		 	   "AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(createdate)";
		$week_google_join = $db_main->getvalue($sql);
			
		// 전월
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_month 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND platform = 2 ".
			   "AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(createdate)";
		$month_google_join = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_quarter 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND platform = 2 ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(createdate)";
		$quarter_google_join = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 13, $today_google_join, $yesterday_google_join, $week_google_join, $month_google_join, $quarter_google_join) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
		
		$db_analysis->execute($sql);

		// (5) Amazon
		// 당일
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_today 00:00:00' <= createdate AND createdate <= '$indicator_today 23:59:59' AND platform = 3 ";
		$today_amazon_join = $db_main->getvalue($sql);
		
		// 전일
		//$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_yesterday 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND adflag = 'mobile_kindle'";
		//$yesterday_amazon_join = $db_main->getvalue($sql);
		$yesterday_amazon_join = 0;
			
		// 전주
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_week 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND platform = 3 ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(createdate)";
		$week_amazon_join = $db_main->getvalue($sql);
			
		// 전월
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_month 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND platform = 3 ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(createdate)";
		$month_amazon_join = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT COUNT(useridx) FROM tbl_user_ext WHERE '$indicator_quarter 00:00:00' <= createdate AND createdate <= '$indicator_yesterday 23:59:59' AND platform = 3 ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(createdate)";
		$quarter_amazon_join = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 14, $today_amazon_join, $yesterday_amazon_join, $week_amazon_join, $month_amazon_join, $quarter_amazon_join) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";

		$db_analysis->execute($sql);
		
		// 2.활동유저수
		// (1) 전체
		// 당일
		$sql = "SELECT todayactivecount FROM user_join_log WHERE today = '$indicator_today'";
		$today_all_active = $db_analysis->getvalue($sql);
		
		// 전일
		//$sql = "SELECT todayactivecount FROM user_join_log WHERE today = '$indicator_yesterday'";
		//$yesterday_all_active = $db_analysis->getvalue($sql);
		$yesterday_all_active = 0;
			
		// 전주
		$sql = "SELECT IFNULL(SUM(todayactivecount), 0) FROM user_join_log WHERE today BETWEEN '$indicator_week' AND '$indicator_yesterday' ".
			   "AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
		$week_all_active = $db_analysis->getvalue($sql);
			
		// 전월
		$sql = "SELECT IFNULL(SUM(todayactivecount), 0) FROM user_join_log WHERE today BETWEEN '$indicator_month' AND '$indicator_yesterday' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
		$month_all_active = $db_analysis->getvalue($sql);
		
		// 전분기
		$sql = "SELECT IFNULL(SUM(todayactivecount), 0) FROM user_join_log WHERE today BETWEEN '$indicator_quarter' AND '$indicator_yesterday' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
		$quarter_all_active = $db_analysis->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 20, $today_all_active, $yesterday_all_active, $week_all_active, $month_all_active, $quarter_all_active) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter=VALUES(quarter)";
			
		$db_analysis->execute($sql);

		// (2) Facebook
		// 당일
		$sql = "SELECT IFNULL(SUM(todayactivecount - todayiosactivecount - todayandroidactivecount - todayamazonactivecount), 0) FROM user_join_log WHERE today = '$indicator_today'";
		$today_facebook_active = $db_analysis->getvalue($sql);
		
		// 전일
		//$sql = "SELECT todayactivecount - todayiosactivecount - todayandroidactivecount - todayamazonactivecount FROM user_join_log WHERE today = '$indicator_yesterday'";
		//$yesterday_facebook_active = $db_analysis->getvalue($sql);
		$yesterday_facebook_active = 0;
			
		// 전주
		$sql = "SELECT IFNULL(SUM(todayactivecount - todayiosactivecount - todayandroidactivecount - todayamazonactivecount), 0) FROM user_join_log WHERE today BETWEEN '$indicator_week' AND '$indicator_yesterday' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
		$week_facebook_active = $db_analysis->getvalue($sql);
			
		// 전월
		$sql = "SELECT IFNULL(SUM(todayactivecount - todayiosactivecount - todayandroidactivecount - todayamazonactivecount), 0) FROM user_join_log WHERE today BETWEEN '$indicator_month' AND '$indicator_yesterday' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
		$month_facebook_active = $db_analysis->getvalue($sql);
		
		// 전분기
		$sql = "SELECT IFNULL(SUM(todayactivecount - todayiosactivecount - todayandroidactivecount - todayamazonactivecount), 0) FROM user_join_log WHERE today BETWEEN '$indicator_quarter' AND '$indicator_yesterday' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
		$quarter_facebook_active = $db_analysis->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 21, $today_facebook_active, $yesterday_facebook_active, $week_facebook_active, $month_facebook_active, $quarter_facebook_active) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
			
		$db_analysis->execute($sql);
		
		// (3) Apple
		// 당일
		$sql = "SELECT IFNULL(SUM(todayiosactivecount), 0) FROM user_join_log WHERE today = '$indicator_today'";
		$today_apple_active = $db_analysis->getvalue($sql);
		
		// 전일
		//$sql = "SELECT todayiosactivecount FROM user_join_log WHERE today = '$indicator_yesterday'";
		//$yesterday_apple_active = $db_analysis->getvalue($sql);
		$yesterday_apple_active = 0;
			
		// 전주
		$sql = "SELECT IFNULL(SUM(todayiosactivecount), 0) FROM user_join_log WHERE today BETWEEN '$indicator_week' AND '$indicator_yesterday' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
		$week_apple_active = $db_analysis->getvalue($sql);
			
		// 전월
		$sql = "SELECT IFNULL(SUM(todayiosactivecount), 0) FROM user_join_log WHERE today BETWEEN '$indicator_month' AND '$indicator_yesterday' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
		$month_apple_active = $db_analysis->getvalue($sql);
		
		// 전분기
		$sql = "SELECT IFNULL(SUM(todayiosactivecount), 0) FROM user_join_log WHERE today BETWEEN '$indicator_quarter' AND '$indicator_yesterday' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
		$quarter_apple_active = $db_analysis->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 22, $today_apple_active, $yesterday_apple_active, $week_apple_active, $month_apple_active, $quarter_apple_active) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
			
		$db_analysis->execute($sql);
		
		// (4) Google
		// 당일
		$sql = "SELECT IFNULL(SUM(todayandroidactivecount), 0) FROM user_join_log WHERE today = '$indicator_today'";
		$today_google_active = $db_analysis->getvalue($sql);
		
		// 전일
		//$sql = "SELECT todayandroidactivecount FROM user_join_log WHERE today = '$indicator_yesterday'";
		//$yesterday_google_active = $db_analysis->getvalue($sql);
		$yesterday_google_active = 0;
			
		// 전주
		$sql = "SELECT IFNULL(SUM(todayandroidactivecount), 0) FROM user_join_log WHERE today BETWEEN '$indicator_week' AND '$indicator_yesterday' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
		$week_google_active = $db_analysis->getvalue($sql);
			
		// 전월
		$sql = "SELECT IFNULL(SUM(todayandroidactivecount), 0) FROM user_join_log WHERE today BETWEEN '$indicator_month' AND '$indicator_yesterday' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
		$month_google_active = $db_analysis->getvalue($sql);
		
		// 전분기
		$sql = "SELECT IFNULL(SUM(todayandroidactivecount), 0) FROM user_join_log WHERE today BETWEEN '$indicator_quarter' AND '$indicator_yesterday' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
		$quarter_google_active = $db_analysis->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 23, $today_google_active, $yesterday_google_active, $week_google_active, $month_google_active, $quarter_google_active) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
			
		$db_analysis->execute($sql);

		// (5) Amazon
		// 당일
		$sql = "SELECT IFNULL(SUM(todayamazonactivecount), 0) FROM user_join_log WHERE today = '$indicator_today'";
		$today_amazon_active = $db_analysis->getvalue($sql);
		
		// 전일
		//$sql = "SELECT todayamazonactivecount FROM user_join_log WHERE today = '$indicator_yesterday'";
		//$yesterday_amazon_active = $db_analysis->getvalue($sql);
		$yesterday_amazon_active = 0;
			
		// 전주
		$sql = "SELECT IFNULL(SUM(todayamazonactivecount), 0) FROM user_join_log WHERE today BETWEEN '$indicator_week' AND '$indicator_yesterday' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
		$week_amazon_active = $db_analysis->getvalue($sql);
			
		// 전월
		$sql = "SELECT IFNULL(SUM(todayamazonactivecount), 0) FROM user_join_log WHERE today BETWEEN '$indicator_month' AND '$indicator_yesterday' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
		$month_amazon_active = $db_analysis->getvalue($sql);
		
		// 전분기
		$sql = "SELECT IFNULL(SUM(todayamazonactivecount), 0) FROM user_join_log WHERE today BETWEEN '$indicator_quarter' AND '$indicator_yesterday' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
		$quarter_amazon_active = $db_analysis->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 24, $today_amazon_active, $yesterday_amazon_active, $week_amazon_active, $month_amazon_active, $quarter_amazon_active) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
			
		$db_analysis->execute($sql);

		// 3.결제유저수
		// (1) 전체
		// 당일
		$sql = "SELECT COUNT(DISTINCT useridx) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59' ".
				"	UNION ALL ".
				"	SELECT useridx FROM tbl_product_order_earn WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59' ".
				"	UNION ALL ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59' ".
				") t1";
		$today_purchase_all_user = $db_main->getvalue($sql);
		
		// 전일
//		$sql = "SELECT COUNT(DISTINCT useridx) ".
//				"FROM ( ".
//				"	SELECT useridx FROM tbl_product_order WHERE useridx > 20000 AND STATUS IN (1, 3) AND '$indicator_yesterday 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
//				"	UNION ALL ".
//				"	SELECT useridx FROM tbl_product_order_earn WHERE useridx > 20000 AND STATUS = 1 AND '$indicator_yesterday 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
//				"	UNION ALL ".
//				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > 20000 AND STATUS = 1 AND '$indicator_yesterday 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
//				") t1";
//		$yesterday_purchase_all_user = $db_main->getvalue($sql);
		$yesterday_purchase_all_user = 0;
			
		// 전주
		$sql = "SELECT COUNT(DISTINCT useridx) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"	AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"	UNION ALL ".
				"	SELECT useridx FROM tbl_product_order_earn WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"	AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"	UNION ALL ".
				"	SELECT useridx FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"	AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				") t1";

		$week_purchase_all_user = $db_main->getvalue($sql);
			
		// 전월
		$sql = "SELECT IFNULL(ROUND(AVG(day_cnt) * 4), 0) AS total_cnt ".
				"FROM ( ".
				"	SELECT writedate, COUNT(DISTINCT useridx) AS day_cnt ".
				"	FROM ". 
				"	( ". 	
				"		SELECT useridx, DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate ".
				"		FROM tbl_product_order ". 
				"		WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"		AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"		UNION ALL ".
				"		SELECT useridx, DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate ".
				"		FROM tbl_product_order_earn ".
				"		WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"		AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"		UNION ALL ". 	
				"		SELECT useridx, DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate ".
				"		FROM tbl_product_order_mobile ". 
				"		WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"		AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"	) t1 ".
				"	GROUP BY writedate ".
				") t2";

		$month_purchase_all_user = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT IFNULL(ROUND(AVG(day_cnt) * 12), 0) AS total_cnt ".
				"FROM ( ".
				"	SELECT writedate, COUNT(DISTINCT useridx) AS day_cnt ".
				"	FROM ".
				"	( ".
				"		SELECT useridx, DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate ".
				"		FROM tbl_product_order ".
				"		WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"		AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"		UNION ALL ".
				"		SELECT useridx, DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate ".
				"		FROM tbl_product_order_earn ".
				"		WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"		AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"		UNION ALL ".
				"		SELECT useridx, DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate ".
				"		FROM tbl_product_order_mobile ".
				"		WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"		AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"	) t1 ".
				"	GROUP BY writedate ".
				") t2";
		
		$quarter_purchase_all_user = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 30, $today_purchase_all_user, $yesterday_purchase_all_user, $week_purchase_all_user, $month_purchase_all_user, $quarter_purchase_all_user) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
			
		$db_analysis->execute($sql);

		// (2) facebook
		// 당일		
		$sql = "SELECT COUNT(DISTINCT useridx) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order WHERE useridx > $std_useridx AND status IN (1, 3) AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59' ".
				"	UNION ALL ".
				"	SELECT useridx FROM tbl_product_order_earn WHERE useridx > $std_useridx AND status = 1 AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59' ".
				") t1";
		$today_purchase_facebook_user = $db_main->getvalue($sql);
		
		// 전일
		//$sql = "SELECT COUNT(DISTINCT useridx) FROM tbl_product_order WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND '$indicator_yesterday 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59'";
		//$yesterday_purchase_facebook_user = $db_main->getvalue($sql);
		$yesterday_purchase_facebook_user = 0;		

		// 전주
		$sql = "SELECT COUNT(DISTINCT useridx) ".
				"FROM ( ".
				"	SELECT useridx FROM tbl_product_order WHERE useridx > $std_useridx AND status IN (1, 3) AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"		AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"	UNION ALL ".
				"	SELECT useridx FROM tbl_product_order_earn WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"		AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				") t1";		
		$week_purchase_facebook_user = $db_main->getvalue($sql);

		
		// 전월
		$sql = "SELECT IFNULL(ROUND(AVG(day_cnt) * 4), 0) AS total_cnt ".
				"FROM ( ".
				"	SELECT writedate, COUNT(DISTINCT useridx) AS day_cnt ".
				"	FROM ".
				"	( ".
				"		SELECT useridx, DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate ".
				"		FROM tbl_product_order ".
				"		WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"			AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"		UNION ALL ".
				"		SELECT useridx, DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate ".
				"		FROM tbl_product_order_earn ".
				"		WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"			AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"	) t1 ".
				"	GROUP BY writedate ".
				") t2";
		$month_purchase_facebook_user = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT IFNULL(ROUND(AVG(day_cnt) * 12), 0) AS total_cnt ".
				"FROM ( ".
				"	SELECT writedate, COUNT(DISTINCT useridx) AS day_cnt ".
				"	FROM ".
				"	( ".
				"		SELECT useridx, DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate ".
				"		FROM tbl_product_order ".
				"		WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"			AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"		UNION ALL ".
				"		SELECT useridx, DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate ".
				"		FROM tbl_product_order_earn ".
				"		WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"			AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"	) t1 ".
				"	GROUP BY writedate ".
				") t2";
		
		$quarter_purchase_facebook_user = $db_main->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 31, $today_purchase_facebook_user, $yesterday_purchase_facebook_user, $week_purchase_facebook_user, $month_purchase_facebook_user, $quarter_purchase_facebook_user) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
			
		$db_analysis->execute($sql);
		
		// (3) Apple
		// 당일
		$sql = "SELECT COUNT(DISTINCT useridx) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 1 AND STATUS = 1 AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59'";
		$today_purchase_apple_user = $db_main->getvalue($sql);
		
		// 전일
		//$sql = "SELECT COUNT(DISTINCT useridx) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 1 AND STATUS = 1 AND '$indicator_yesterday 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59'";
		//$yesterday_purchase_apple_user = $db_main->getvalue($sql);
		$yesterday_purchase_apple_user = 0;
		
		// 전주
		$sql = "SELECT COUNT(DISTINCT useridx) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 1 AND STATUS = 1 AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$week_purchase_apple_user = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(ROUND(AVG(day_cnt) * 4), 0) AS total_cnt ".
				"FROM ( ".
				"	SELECT writedate, COUNT(DISTINCT useridx) AS day_cnt ".
				"	FROM ( ".
				"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, useridx ".
				"		FROM tbl_product_order_mobile ".
				"		WHERE useridx > $std_useridx AND os_type = 1 AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"		AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"	) t1 ".
				"	GROUP BY writedate ".
				") t2";
		$month_purchase_apple_user = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT IFNULL(ROUND(AVG(day_cnt) * 12), 0) AS total_cnt ".
				"FROM ( ".
				"	SELECT writedate, COUNT(DISTINCT useridx) AS day_cnt ".
				"	FROM ( ".
				"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, useridx ".
				"		FROM tbl_product_order_mobile ".
				"		WHERE useridx > $std_useridx AND os_type = 1 AND STATUS = 1 AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"		AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"	) t1 ".
				"	GROUP BY writedate ".
				") t2";
		$quarter_purchase_apple_user = $db_main->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 32, $today_purchase_apple_user, $yesterday_purchase_apple_user, $week_purchase_apple_user, $month_purchase_apple_user, $quarter_purchase_apple_user) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
							
		$db_analysis->execute($sql);
		
		// (4) Google
		// 당일
		$sql = "SELECT COUNT(DISTINCT useridx) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 2 AND STATUS = 1 AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59'";
		$today_purchase_google_user = $db_main->getvalue($sql);
		
		// 전일
		//$sql = "SELECT COUNT(DISTINCT useridx) FROM tbl_product_order_mobile WHERE useridx > 10090 AND category = 2 AND STATUS = 1 AND '$indicator_yesterday 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59'";
		//$yesterday_purchase_google_user = $db_main->getvalue($sql);
		$yesterday_purchase_google_user = 0;

		// 전주
		$sql = "SELECT COUNT(DISTINCT useridx) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 2 AND STATUS = 1 AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";		
		$week_purchase_google_user = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(ROUND(AVG(day_cnt) * 4), 0) AS total_cnt ".
				"FROM ( ".
				"	SELECT writedate, COUNT(DISTINCT useridx) AS day_cnt ".
				"	FROM ( ".
				"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, useridx ".
				"		FROM tbl_product_order_mobile ".
				"		WHERE useridx > $std_useridx AND os_type = 2 AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"		AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"	) t1 ".
				"	GROUP BY writedate ".
				") t2";
		$month_purchase_google_user = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT IFNULL(ROUND(AVG(day_cnt) * 12), 0) AS total_cnt ".
				"FROM ( ".
				"	SELECT writedate, COUNT(DISTINCT useridx) AS day_cnt ".
				"	FROM ( ".
				"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, useridx ".
				"		FROM tbl_product_order_mobile ".
				"		WHERE useridx > $std_useridx AND os_type = 2 AND STATUS = 1 AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"		AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"	) t1 ".
				"	GROUP BY writedate ".
				") t2";
		$quarter_purchase_google_user = $db_main->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 33, $today_purchase_google_user, $yesterday_purchase_google_user, $week_purchase_google_user, $month_purchase_google_user, $quarter_purchase_google_user) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
			
		$db_analysis->execute($sql);

		// (5) Amazon
		// 당일
		$sql = "SELECT COUNT(DISTINCT useridx) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 3 AND STATUS = 1 AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59'";
		$today_purchase_amazon_user = $db_main->getvalue($sql);
		
		// 전일
		//$sql = "SELECT COUNT(DISTINCT useridx) FROM tbl_product_order_mobile WHERE useridx > 10090 AND category = 3 AND STATUS = 1 AND '$indicator_yesterday 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59'";
		//$yesterday_purchase_amazon_user = $db_main->getvalue($sql);
		$yesterday_purchase_amazon_user = 0;
		
		// 전주
		$sql = "SELECT COUNT(DISTINCT useridx) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 3 AND STATUS = 1 AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$week_purchase_amazon_user = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(ROUND(AVG(day_cnt) * 4), 0) AS total_cnt ".
				"FROM ( ".
				"	SELECT writedate, COUNT(DISTINCT useridx) AS day_cnt ".
				"	FROM ( ".
				"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, useridx ".
				"		FROM tbl_product_order_mobile ".
				"		WHERE useridx > $std_useridx AND os_type = 3 AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"		AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"	) t1 ".
				"	GROUP BY writedate ".
				") t2";
		$month_purchase_amazon_user = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT IFNULL(ROUND(AVG(day_cnt) * 12), 0) AS total_cnt ".
				"FROM ( ".
				"	SELECT writedate, COUNT(DISTINCT useridx) AS day_cnt ".
				"	FROM ( ".
				"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, useridx ".
				"		FROM tbl_product_order_mobile ".
				"		WHERE useridx > $std_useridx AND os_type = 3 AND STATUS = 1 AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"		AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				"	) t1 ".
				"	GROUP BY writedate ".
				") t2";
		$quarter_purchase_amazon_user = $db_main->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 34, $today_purchase_amazon_user, $yesterday_purchase_amazon_user, $week_purchase_amazon_user, $month_purchase_amazon_user, $quarter_purchase_amazon_user) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
			
		$db_analysis->execute($sql);

		// 4.결제금액
		// (1) 젠체
		// 당일
		$sql = "SELECT ( ".
				" 	SELECT IFNULL(SUM(facebookcredit), 0) ". 
				"	FROM tbl_product_order ". 
				"	WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59' ".				
				") + ( ".
				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
				"	FROM tbl_product_order_earn ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59' ".
				") + ( ".
				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59' ".
				") AS total_amount";

		$today_purchase_all_amount = $db_main->getvalue($sql);
		
		// 전일
//		$sql = "SELECT ( ".
//				" 	SELECT IFNULL(SUM(facebookcredit), 0) ".
//				"	FROM tbl_product_order ".
//				"	WHERE useridx > 20000 AND STATUS = 1 AND '$indicator_yesterday 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
//				") + ( ".
//				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
//				"	FROM tbl_product_order_earn ".
//				"	WHERE useridx > 20000 AND STATUS = 1 AND '$indicator_yesterday 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
//				") + ( ".
//				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
//				"	FROM tbl_product_order_mobile ".
//				"	WHERE useridx > 20000 AND STATUS = 1 AND '$indicator_yesterday 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
//				") AS total_amount";
//		$yesterday_purchase_all_amount = $db_main->getvalue($sql);
		$yesterday_purchase_all_amount = 0;
			
		// 전주
		$sql = "SELECT ( ".
				" 	SELECT IFNULL(SUM(facebookcredit), 0) ".
				"	FROM tbl_product_order ".
				"	WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"	AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				") + ( ".
				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
				"	FROM tbl_product_order_earn ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"	AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				") + ( ".
				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"	AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				") AS total_amount";
		$week_purchase_all_amount = $db_main->getvalue($sql);
			
		// 전월
		$sql = "SELECT ( ".
				" 	SELECT IFNULL(SUM(facebookcredit), 0) ".
				"	FROM tbl_product_order ".
				"	WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"	AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				") + ( ".
				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
				"	FROM tbl_product_order_earn ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"	AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				") + ( ".
				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"	AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				") AS total_amount";
		$month_purchase_all_amount = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT ( ".
				" 	SELECT IFNULL(SUM(facebookcredit), 0) ".
				"	FROM tbl_product_order ".
				"	WHERE useridx > $std_useridx AND STATUS IN (1, 3) AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"	AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				") + ( ".
				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
				"	FROM tbl_product_order_earn ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"	AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				") + ( ".
				"	SELECT IFNULL(ROUND(SUM(money*10)), 0) ".
				"	FROM tbl_product_order_mobile ".
				"	WHERE useridx > $std_useridx AND STATUS = 1 AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"	AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				") AS total_amount";
		$quarter_purchase_all_amount = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 40, $today_purchase_all_amount, $yesterday_purchase_all_amount, $week_purchase_all_amount, $month_purchase_all_amount, $quarter_purchase_all_amount) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";

		$db_analysis->execute($sql);

		// (2) facebook
		// 당일
		$sql = "SELECT IFNULL(SUM(facebookcredit), 0) FROM tbl_product_order WHERE useridx > $std_useridx AND status IN (1, 3) AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59'";
		$today_purchase_facebook_amount = $db_main->getvalue($sql);
		
		// 전일
//		$sql = "SELECT IFNULL(SUM(facebookcredit), 0) FROM tbl_product_order WHERE useridx > $std_useridx AND status IN (1, 3) AND '$indicator_yesterday 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59'";
//		$yesterday_purchase_facebook_amount = $db_main->getvalue($sql);
		$yesterday_purchase_facebook_amount = 0;
			
		// 전주
		$sql = "SELECT IFNULL(SUM(facebookcredit), 0) FROM tbl_product_order WHERE useridx > $std_useridx AND status IN (1, 3) AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59'".
			   "AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$week_purchase_facebook_amount = $db_main->getvalue($sql);
			
		// 전월
		$sql = "SELECT IFNULL(SUM(facebookcredit), 0) FROM tbl_product_order WHERE useridx > $std_useridx AND status IN (1, 3) AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59'".
			   "AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$month_purchase_facebook_amount = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT IFNULL(SUM(facebookcredit), 0) FROM tbl_product_order WHERE useridx > $std_useridx AND status IN (1, 3) AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59'".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$quarter_purchase_facebook_amount = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 41, $today_purchase_facebook_amount, $yesterday_purchase_facebook_amount, $week_purchase_facebook_amount, $month_purchase_facebook_amount, $quarter_purchase_facebook_amount) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
			
		$db_analysis->execute($sql);

		// (3) Apple
		// 당일
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 1 AND status = 1 AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59'";
		$today_purchase_apple_amount = $db_main->getvalue($sql);
		
		// 전일
		//$sql = "SELECT ROUND(SUM(money*10)) FROM tbl_product_order_mobile WHERE useridx > 10090 AND category = 1 AND status = 1 AND '$indicator_yesterday 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59'";
		//$yesterday_purchase_apple_amount = $db_main->getvalue($sql);
		$yesterday_purchase_apple_amount = 0;
			
		// 전주
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 1 AND status = 1 AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$week_purchase_apple_amount = $db_main->getvalue($sql);
			
		// 전월
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 1 AND status = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$month_purchase_apple_amount = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 1 AND status = 1 AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$quarter_purchase_apple_amount = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 42, $today_purchase_apple_amount, $yesterday_purchase_apple_amount, $week_purchase_apple_amount, $month_purchase_apple_amount, $quarter_purchase_apple_amount) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
			
		$db_analysis->execute($sql);
		
		// (4) Google
		// 당일
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 2 AND status = 1 AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59'";
		$today_purchase_google_amount = $db_main->getvalue($sql);
		
		// 전일
		//$sql = "SELECT ROUND(SUM(money*10)) FROM tbl_product_order_mobile WHERE useridx > 10090 AND category = 2 AND status = 1 AND '$indicator_yesterday 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59'";
		//$yesterday_purchase_google_amount = $db_main->getvalue($sql);
		$yesterday_purchase_google_amount = 0;
			
		// 전주
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 2 AND status = 1 AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$week_purchase_google_amount = $db_main->getvalue($sql);
			
		// 전월
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 2 AND status = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$month_purchase_google_amount = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 2 AND status = 1 AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$quarter_purchase_google_amount = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 43, $today_purchase_google_amount, $yesterday_purchase_google_amount, $week_purchase_google_amount, $month_purchase_google_amount, $quarter_purchase_google_amount) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
			
		$db_analysis->execute($sql);
		
		// (5) Amazon
		// 당일
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 3 AND status = 1 AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59'";
		$today_purchase_amazon_amount = $db_main->getvalue($sql);
		
		// 전일
		//$sql = "SELECT ROUND(SUM(money*10)) FROM tbl_product_order_mobile WHERE useridx > 10090 AND category = 3 AND status = 1 AND '$indicator_yesterday 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59'";
		//$yesterday_purchase_amazon_amount = $db_main->getvalue($sql);
		$yesterday_purchase_amazon_amount = 0;
			
		// 전주
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 3 AND status = 1 AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$week_purchase_amazon_amount = $db_main->getvalue($sql);
			
		// 전월
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 3 AND status = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$month_purchase_amazon_amount = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT IFNULL(ROUND(SUM(money*10)), 0) FROM tbl_product_order_mobile WHERE useridx > $std_useridx AND os_type = 3 AND status = 1 AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
				"AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$quarter_purchase_amazon_amount = $db_main->getvalue($sql);
			
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 44, $today_purchase_amazon_amount, $yesterday_purchase_amazon_amount, $week_purchase_amazon_amount, $month_purchase_amazon_amount, $quarter_purchase_amazon_amount) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
			
		$db_analysis->execute($sql);

		// (5) trialpay
		// 당일
		$sql = "SELECT IFNULL(SUM(money*10), 0) FROM tbl_product_order_earn WHERE useridx > $std_useridx AND status = 1 AND '$indicator_today 00:00:00' <= writedate AND writedate <= '$indicator_today 23:59:59'";
		$today_purchase_trialpay_amount = $db_main->getvalue($sql);
		
		// 전일
		//$sql = "SELECT IFNULL(SUM(money*10), 0) FROM tbl_product_order_earn WHERE useridx > 20000 AND status = 1 AND '$indicator_yesterday 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59'";
		//$yesterday_purchase_trialpay_amount = $db_main->getvalue($sql);
		$yesterday_purchase_trialpay_amount = 0;
		
		// 전주
		$sql = "SELECT IFNULL(SUM(money*10), 0) FROM tbl_product_order_earn WHERE useridx > $std_useridx AND status = 1 AND '$indicator_week 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
			   "AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$week_purchase_trialpay_amount = $db_main->getvalue($sql);
		
		// 전월
		$sql = "SELECT IFNULL(SUM(money*10), 0) FROM tbl_product_order_earn WHERE useridx > $std_useridx AND status = 1 AND '$indicator_month 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
			   "AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$month_purchase_trialpay_amount = $db_main->getvalue($sql);
		
		// 전분기
		$sql = "SELECT IFNULL(SUM(money*10), 0) FROM tbl_product_order_earn WHERE useridx > $std_useridx AND status = 1 AND '$indicator_quarter 00:00:00' <= writedate AND writedate <= '$indicator_yesterday 23:59:59' ".
			   "AND DAYOFWEEK(DATE_SUB(DATE_FORMAT(NOW(), '%Y-%m-%d'), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
		$quarter_purchase_trialpay_amount = $db_main->getvalue($sql);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 45, $today_purchase_trialpay_amount, $yesterday_purchase_trialpay_amount, $week_purchase_trialpay_amount, $month_purchase_trialpay_amount, $quarter_purchase_trialpay_amount) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
		
		$db_analysis->execute($sql);

		// 5-1.ARPU
		// (1) 전체
		// 당일
		$today_all_arpu = ($today_all_active == 0) ? 0 : round($today_purchase_all_amount / $today_all_active * 100);
		
		// 전일
		//$yesterday_all_arpu = ($yesterday_all_active == 0) ? 0 : round($yesterday_purchase_all_amount / $yesterday_all_active * 100);
		$yesterday_all_arpu = 0;
		
		// 전주
		$week_all_active_avg = round($week_all_active);
		$week_purchase_all_amount_avg = round($week_purchase_all_amount);
		$week_all_arpu = ($week_all_active_avg == 0) ? 0 : round($week_purchase_all_amount_avg / $week_all_active_avg * 100);
		
		// 전월
		$month_all_active_avg = round($month_all_active / 4);
		$month_purchase_all_amount_avg = round($month_purchase_all_amount / 4);
		$month_all_arpu = ($month_all_active_avg == 0) ? 0 : round($month_purchase_all_amount_avg / $month_all_active_avg * 100);
		
		// 전분기
		$quarter_all_active_avg = round($quarter_all_active / 12);
		$quarter_purchase_all_amount_avg = round($quarter_purchase_all_amount / 12);
		$quarter_all_arpu = ($quarter_all_active_avg == 0) ? 0 : round($quarter_purchase_all_amount_avg / $quarter_all_active_avg * 100);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 50, $today_all_arpu, $yesterday_all_arpu, $week_all_arpu, $month_all_arpu, $quarter_all_arpu) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
		
		$db_analysis->execute($sql);

		// (2) facebook
		// 당일
		$today_facebook_arpu = ($today_facebook_active == 0) ? 0 : round(($today_purchase_facebook_amount + $today_purchase_trialpay_amount) / $today_facebook_active * 100);
		
		// 전일
		//$yesterday_facebook_arpu = ($yesterday_facebook_active == 0) ? 0 : round(($yesterday_purchase_facebook_amount + $yesterday_purchase_trialpay_amount) / $yesterday_facebook_active * 100);
		$yesterday_facebook_arpu = 0;
		
		// 전주
		$week_facebook_active_avg = round($week_facebook_active);
		$week_purchase_facebook_amount_avg = round($week_purchase_facebook_amount);
		$week_facebook_arpu = ($week_facebook_active_avg == 0) ? 0 : round($week_purchase_facebook_amount_avg / $week_facebook_active_avg * 100);
		
		// 전월
		$month_facebook_active_avg = round($month_facebook_active / 4);
		$month_purchase_facebook_amount_avg = round($month_purchase_facebook_amount / 4);
		$month_facebook_arpu = ($month_facebook_active_avg == 0) ? 0 : round($month_purchase_facebook_amount_avg / $month_facebook_active_avg * 100);
		
		// 전분기
		$quarter_facebook_active_avg = round($quarter_facebook_active / 12);
		$quarter_purchase_facebook_amount_avg = round($quarter_purchase_facebook_amount / 12);
		$quarter_facebook_arpu = ($quarter_facebook_active_avg == 0) ? 0 : round($quarter_purchase_facebook_amount_avg / $quarter_facebook_active_avg * 100);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 51, $today_facebook_arpu, $yesterday_facebook_arpu, $week_facebook_arpu, $month_facebook_arpu, $quarter_facebook_arpu) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
		
		$db_analysis->execute($sql);
		
		// (3) Apple
		// 당일
		$today_apple_arpu = ($today_apple_active == 0) ? 0 : round($today_purchase_apple_amount / $today_apple_active * 100);
		
		// 전일
		//$yesterday_apple_arpu = ($yesterday_apple_active == 0) ? 0 : round($yesterday_purchase_apple_amount / $yesterday_apple_active * 100);
		$yesterday_apple_arpu = 0;
		
		// 전주
		$week_apple_active_avg = round($week_apple_active);
		$week_purchase_apple_amount_avg = round($week_purchase_apple_amount);
		$week_apple_arpu = ($week_apple_active_avg == 0) ? 0 : round($week_purchase_apple_amount_avg / $week_apple_active_avg * 100);
		
		// 전월
		$month_apple_active_avg = round($month_apple_active / 4);
		$month_purchase_apple_amount_avg = round($month_purchase_apple_amount / 4);
		$month_apple_arpu = ($month_apple_active_avg == 0) ? 0 : round($month_purchase_apple_amount_avg / $month_apple_active_avg * 100);
		
		// 전분기
		$quarter_apple_active_avg = round($quarter_apple_active / 12);
		$quarter_purchase_apple_amount_avg = round($quarter_purchase_apple_amount / 12);
		$quarter_apple_arpu = ($quarter_apple_active_avg == 0) ? 0 : round($quarter_purchase_apple_amount_avg / $quarter_apple_active_avg * 100);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 52, $today_apple_arpu, $yesterday_apple_arpu, $week_apple_arpu, $month_apple_arpu, $quarter_apple_arpu) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
		
		$db_analysis->execute($sql);
		
		// (4) Google
		// 당일
		$today_google_arpu = ($today_google_active == 0) ? 0 : round($today_purchase_google_amount / $today_google_active * 100);
		
		// 전일
		//$yesterday_google_arpu = ($yesterday_google_active == 0) ? 0 : round($yesterday_purchase_google_amount / $yesterday_google_active * 100);
		$yesterday_google_arpu = 0;
		
		// 전주
		$week_google_active_avg = round($week_google_active);
		$week_purchase_google_amount_avg = round($week_purchase_google_amount);
		$week_google_arpu = ($week_google_active_avg == 0) ? 0 : round($week_purchase_google_amount_avg / $week_google_active_avg * 100);
		
		// 전월
		$month_google_active_avg = round($month_google_active / 4);
		$month_purchase_google_amount_avg = round($month_purchase_google_amount / 4);
		$month_google_arpu = ($month_google_active_avg == 0) ? 0 : round($month_purchase_google_amount_avg / $month_google_active_avg * 100);
		
		// 전분기
		$quarter_google_active_avg = round($quarter_google_active / 12);
		$quarter_purchase_google_amount_avg = round($quarter_purchase_google_amount / 12);
		$quarter_google_arpu = ($quarter_google_active_avg == 0) ? 0 : round($quarter_purchase_google_amount_avg / $quarter_google_active_avg * 100);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 53, $today_google_arpu, $yesterday_google_arpu, $week_google_arpu, $month_google_arpu, $quarter_google_arpu) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
		
		$db_analysis->execute($sql);

		// (5) Amazon
		// 당일
		$today_amazon_arpu = ($today_amazon_active == 0) ? 0 : round($today_purchase_amazon_amount / $today_amazon_active * 100);
		
		// 전일
		//$yesterday_amazon_arpu = ($yesterday_amazon_active == 0) ? 0 : round($yesterday_purchase_amazon_amount / $yesterday_amazon_active * 100);
		$yesterday_amazon_arpu = 0;
		
		// 전주
		$week_amazon_active_avg = round($week_amazon_active);
		$week_purchase_amazon_amount_avg = round($week_purchase_amazon_amount);
		$week_amazon_arpu = ($week_amazon_active_avg == 0) ? 0 : round($week_purchase_amazon_amount_avg / $week_amazon_active_avg * 100);
		
		// 전월
		$month_amazon_active_avg = round($month_amazon_active / 4);
		$month_purchase_amazon_amount_avg = round($month_purchase_amazon_amount / 4);
		$month_amazon_arpu = ($month_amazon_active_avg == 0) ? 0 : round($month_purchase_amazon_amount_avg / $month_amazon_active_avg * 100);
		
		// 전분기
		$quarter_amazon_active_avg = round($quarter_amazon_active / 12);
		$quarter_purchase_amazon_amount_avg = round($quarter_purchase_amazon_amount / 12);
		$quarter_amazon_arpu = ($quarter_amazon_active_avg == 0) ? 0 : round($quarter_purchase_amazon_amount_avg / $quarter_amazon_active_avg * 100);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 54, $today_amazon_arpu, $yesterday_amazon_arpu, $week_amazon_arpu, $month_amazon_arpu, $quarter_amazon_arpu) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
		
		$db_analysis->execute($sql);

		// 6.ARPPU
		// (1) 전체
		// 당일
		$today_all_arppu = ($today_purchase_all_user == 0) ? 0 : round($today_purchase_all_amount / $today_purchase_all_user * 100);
		
		// 전일
		//$yesterday_all_arppu = ($yesterday_purchase_all_user == 0) ? 0 : round($yesterday_purchase_all_amount / $yesterday_purchase_all_user * 100);
		$yesterday_all_arppu = 0;
		
		// 전주
		$week_all_arppu = ($week_purchase_all_user == 0) ? 0 : round($week_purchase_all_amount / $week_purchase_all_user * 100);
		
		// 전월
		$month_all_arppu = ($month_purchase_all_user == 0) ? 0 : round($month_purchase_all_amount / $month_purchase_all_user * 100);
		
		// 전분기
		$quarter_all_arppu = ($quarter_purchase_all_user == 0) ? 0 : round($quarter_purchase_all_amount / $quarter_purchase_all_user * 100);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 60, $today_all_arppu, $yesterday_all_arppu, $week_all_arppu, $month_all_arppu, $quarter_all_arppu) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
		
		$db_analysis->execute($sql);

		// (2) Facebook
		// 당일
		$today_facebook_arppu = ($today_purchase_facebook_user == 0) ? 0 : round(($today_purchase_facebook_amount + $today_purchase_trialpay_amount) / $today_purchase_facebook_user * 100);

		// 전일
		//$yesterday_facebook_arppu = ($yesterday_purchase_facebook_user == 0) ? 0 : round(($yesterday_purchase_facebook_amount + $yesterday_purchase_trialpay_amount) / $yesterday_purchase_facebook_user * 100);
		$yesterday_facebook_arppu = 0;

		// 전주
		$week_facebook_arppu = ($week_purchase_facebook_user == 0) ? 0 : round(($week_purchase_facebook_amount + $week_purchase_trialpay_amount) / $week_purchase_facebook_user * 100);

		// 전월
		$month_facebook_arppu = ($month_purchase_facebook_user == 0) ? 0 : round(($month_purchase_facebook_amount + $month_purchase_trialpay_amount) / $month_purchase_facebook_user * 100);
		
		// 전분기
		$quarter_facebook_arppu = ($quarter_purchase_facebook_user == 0) ? 0 : round(($quarter_purchase_facebook_amount + $quarter_purchase_trialpay_amount) / $quarter_purchase_facebook_user * 100);

		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 61, $today_facebook_arppu, $yesterday_facebook_arppu, $week_facebook_arppu, $month_facebook_arppu, $quarter_facebook_arppu) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
		
		$db_analysis->execute($sql);
		
		// (3) Apple
		// 당일
		$today_apple_arppu = ($today_purchase_apple_user == 0) ? 0 : round($today_purchase_apple_amount / $today_purchase_apple_user * 100);
		
		// 전일
		//$yesterday_apple_arppu = ($yesterday_purchase_apple_user == 0) ? 0 : round($yesterday_purchase_apple_amount / $yesterday_purchase_apple_user * 100);
		$yesterday_apple_arppu = 0;
		
		// 전주
		$week_apple_arppu = ($week_purchase_apple_user == 0) ? 0 : round($week_purchase_apple_amount / $week_purchase_apple_user * 100);
		
		// 전월
		$month_apple_arppu = ($month_purchase_apple_user == 0) ? 0 : round($month_purchase_apple_amount / $month_purchase_apple_user * 100);
		
		// 전분기
		$quarter_apple_arppu = ($quarter_purchase_apple_user == 0) ? 0 : round($quarter_purchase_apple_amount / $quarter_purchase_apple_user * 100);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 62, $today_apple_arppu, $yesterday_apple_arppu, $week_apple_arppu, $month_apple_arppu, $quarter_apple_arppu) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
		
		$db_analysis->execute($sql);
		
		// (4) Google
		// 당일
		$today_google_arppu = ($today_purchase_google_user == 0) ? 0 : round($today_purchase_google_amount / $today_purchase_google_user * 100);
		
		// 전일
		//$yesterday_google_arppu = ($yesterday_purchase_google_user == 0) ? 0 : round($yesterday_purchase_google_amount / $yesterday_purchase_google_user * 100);
		$yesterday_google_arppu = 0;
		
		// 전주
		$week_google_arppu = ($week_purchase_google_user == 0) ? 0 : round($week_purchase_google_amount / $week_purchase_google_user * 100);
		
		// 전월
		$month_google_arppu = ($month_purchase_google_user == 0) ? 0 : round($month_purchase_google_amount / $month_purchase_google_user * 100);
		
		// 전분기
		$quarter_google_arppu = ($quarter_purchase_google_user == 0) ? 0 : round($quarter_purchase_google_amount / $quarter_purchase_google_user * 100);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 63, $today_google_arppu, $yesterday_google_arppu, $week_google_arppu, $month_google_arppu, $quarter_google_arppu) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
		
		$db_analysis->execute($sql);

		// (5) Amazon
		// 당일
		$today_amazon_arppu = ($today_purchase_amazon_user == 0) ? 0 : round($today_purchase_amazon_amount / $today_purchase_amazon_user * 100);
		
		// 전일
		//$yesterday_amazon_arppu = ($yesterday_purchase_amazon_user == 0) ? 0 : round($yesterday_purchase_amazon_amount / $yesterday_purchase_amazon_user * 100);
		$yesterday_amazon_arppu = 0;
		
		// 전주
		$week_amazon_arppu = ($week_purchase_amazon_user == 0) ? 0 : round($week_purchase_amazon_amount / $week_purchase_amazon_user * 100);
		
		// 전월
		$month_amazon_arppu = ($month_purchase_amazon_user == 0) ? 0 : round($month_purchase_amazon_amount / $month_purchase_amazon_user * 100);
		
		// 전분기
		$quarter_amazon_arppu = ($quarter_purchase_amazon_user == 0) ? 0 : round($quarter_purchase_amazon_amount / $quarter_purchase_amazon_user * 100);
		
		$sql = "INSERT INTO game_indicator_daily(writedate, typeid, today, yesterday, week, month, quarter) ".
				"VALUES('$indicator_today', 64, $today_amazon_arppu, $yesterday_amazon_arppu, $week_amazon_arppu, $month_amazon_arppu, $quarter_amazon_arppu) ".
				"ON DUPLICATE KEY UPDATE today = VALUES(today), yesterday = VALUES(yesterday), week = VALUES(week), month = VALUES(month), quarter = VALUES(quarter)";
		
		$db_analysis->execute($sql);

	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$sql = "UPDATE tbl_scheduler_log SET status = $issuccess, writedate = NOW() WHERE logidx = $logidx;";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_analysis->end();
?>