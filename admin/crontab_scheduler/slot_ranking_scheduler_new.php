<?
	include("../common/common_include.inc.php");
	include("../common/dbconnect/db_util_redshift.inc.php");

	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/slot_ranking_scheduler_new") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/slot_ranking_scheduler_new") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("slot_ranking_scheduler_new Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$db_main2 = new CDatabase_Main2();
	$db_slave_main2 = new CDatabase_Slave_Main2();
	$db_redshift = new CDatabase_Redshift();
	$db_game = new CDatabase_Game();
	
	$db_main2->execute("SET wait_timeout=7200");
	$db_slave_main2->execute("SET wait_timeout=7200");
	$db_game->execute("SET wait_timeout=7200");
	
	write_log("Slot Ranking Scheduler Start");
	
	$before_14day = date("Y-m-d", time() - 60 * 60 * 24 * 14);
	$before_60day = date("Y-m-d", time() - 60 * 60 * 24 * 60);
	
	$ods_list = "25,26,27,29,30";
	
	$web_slot_str="";
	$sql="SELECT slottype FROM tbl_slot_list WHERE web = 1 AND is_ods != 1";
	$web_slot_list = $db_slave_main2->gettotallist($sql);
	
	for($i=0; $i<sizeof($web_slot_list); $i++)
	{
		$slottype = $web_slot_list[$i]["slottype"];
	
		if($web_slot_str == "")
			$web_slot_str = $slottype;
		else
			$web_slot_str .= ",".$slottype;
	}
	
	$mobile_slot_str="";
	$sql="SELECT slottype FROM tbl_slot_list WHERE android = 1 AND is_ods != 1";
	$mobile_slot_list = $db_slave_main2->gettotallist($sql);
	
	for($i=0; $i<sizeof($mobile_slot_list); $i++)
	{
		$slottype = $mobile_slot_list[$i]["slottype"];
	
		if($mobile_slot_str == "")
			$mobile_slot_str = $slottype;
		else
			$mobile_slot_str .= ",".$slottype;
	}
	
	try
	{
		// Web - ODS
		$sql = "SELECT 25 AS slottype, AVG(total) AS total ".
				"FROM ( ".
				"	SELECT slottype, SUM((playcount * multi)) AS total ".
				"	FROM ( ".
				"		SELECT slottype, money_in, money_out, playcount, ROUND(money_in/playcount), ".
				"  		(CASE WHEN ROUND(money_in/playcount) > 100000000 THEN 4000 ".
  				"		WHEN ROUND(money_in/playcount) > 50000000 THEN 2000 ".
  				"		WHEN ROUND(money_in/playcount) > 10000000 THEN 1000 ". 
				"		WHEN ROUND(money_in/playcount) > 5000000 THEN 500 ".
				"		WHEN ROUND(money_in/playcount) > 2000000 THEN 200 ".
				"		WHEN ROUND(money_in/playcount) > 1000000 THEN 100 ".
				"		WHEN ROUND(money_in/playcount) > 500000 THEN 50 ".
				"		WHEN ROUND(money_in/playcount) > 200000 THEN 20 ".
				"		WHEN ROUND(money_in/playcount) > 100000 THEN 10 ".
				"		WHEN ROUND(money_in/playcount) > 50000 THEN 5 ".
				"		WHEN ROUND(money_in/playcount) > 25000 THEN 3 ".
				"		ELSE 1 END) AS multi ".
				"		FROM t5_user_gamelog ".
				"		WHERE useridx > 20000  AND slottype IN ($ods_list) AND writedate >= '$before_14day 00:00:00' AND money_in > 0 AND mode NOT IN (4, 5, 9, 26) ".
				"	) t1 ".
				"GROUP BY slottype ".
				"ORDER BY total DESC LIMIT 2 ".
				") t2";
		
		$slot_info_step1 = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i<sizeof($slot_info_step1); $i++)
		{
			$step1_slottype = $slot_info_step1[$i]["slottype"];
			$step1_amount = $slot_info_step1[$i]["total"];
			
			$insert_sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, step_1) VALUES(0, $step1_slottype, $step1_amount) ".
			     		 	"ON DUPLICATE KEY UPDATE step_1=VALUES(step_1);";
			$db_main2->execute($insert_sql);
		}
		
		// Web - ODS 제외
		$sql = "SELECT slottype, SUM((playcount * multi)) AS total ".
				"FROM ( ".
				"	SELECT slottype, money_in, money_out, playcount, ROUND(money_in/playcount), ".
				"  		(CASE WHEN ROUND(money_in/playcount) > 100000000 THEN 4000 ".
  				"		WHEN ROUND(money_in/playcount) > 50000000 THEN 2000 ".
  				"		WHEN ROUND(money_in/playcount) > 10000000 THEN 1000 ".
				"		WHEN ROUND(money_in/playcount) > 5000000 THEN 500 ".
				"		WHEN ROUND(money_in/playcount) > 2000000 THEN 200 ".
				"		WHEN ROUND(money_in/playcount) > 1000000 THEN 100 ".
				"		WHEN ROUND(money_in/playcount) > 500000 THEN 50 ".
				"		WHEN ROUND(money_in/playcount) > 200000 THEN 20 ".
				"		WHEN ROUND(money_in/playcount) > 100000 THEN 10 ".
				"		WHEN ROUND(money_in/playcount) > 50000 THEN 5 ".
				"		WHEN ROUND(money_in/playcount) > 25000 THEN 3 ".
				"		ELSE 1 END) AS multi ".
				"	FROM t5_user_gamelog ".
				"	WHERE useridx > 20000  AND slottype IN ($web_slot_str) AND money_in > 0 AND mode NOT IN (4, 5, 9, 26) AND writedate >= '$before_14day 00:00:00' ".
				") t1 ".
				"GROUP BY slottype ".
				"ORDER BY total DESC";
		
		$slot_info_step1 = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i<sizeof($slot_info_step1); $i++)
		{
			$step1_slottype = $slot_info_step1[$i]["slottype"];
			$step1_amount = $slot_info_step1[$i]["total"];
				
			$insert_sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, step_1) VALUES(0, $step1_slottype, $step1_amount) ".
					"ON DUPLICATE KEY UPDATE step_1=VALUES(step_1);";
			$db_main2->execute($insert_sql);
		}
		
		// Android - ODS
		$sql = "SELECT 25 AS slottype, AVG(total) AS total ".
				"FROM ( ".
				"	SELECT slottype, SUM((playcount * multi)) AS total ".
				"	FROM ( ".
				"		SELECT slottype, money_in, money_out, playcount, ROUND(money_in/playcount), ".
				"  		(CASE WHEN ROUND(money_in/playcount) > 100000000 THEN 4000 ".
  				"		WHEN ROUND(money_in/playcount) > 50000000 THEN 2000 ".
  				"		WHEN ROUND(money_in/playcount) > 10000000 THEN 1000 ".
				"		WHEN ROUND(money_in/playcount) > 5000000 THEN 500 ".
				"		WHEN ROUND(money_in/playcount) > 2000000 THEN 200 ".
				"		WHEN ROUND(money_in/playcount) > 1000000 THEN 100 ".
				"		WHEN ROUND(money_in/playcount) > 500000 THEN 50 ".
				"		WHEN ROUND(money_in/playcount) > 200000 THEN 20 ".
				"		WHEN ROUND(money_in/playcount) > 100000 THEN 10 ".
				"		WHEN ROUND(money_in/playcount) > 50000 THEN 5 ".
				"		WHEN ROUND(money_in/playcount) > 25000 THEN 3 ".
				"		ELSE 1 END) AS multi ".
				"		FROM t5_user_gamelog_android ".
				"		WHERE useridx > 20000  AND slottype IN ($ods_list) AND writedate >= '$before_14day 00:00:00' AND money_in > 0 AND mode NOT IN (4, 5, 9, 26) ".
				"	) t1 ".
				"GROUP BY slottype ".
				"ORDER BY total DESC LIMIT 2 ".
				") t2";
		
		$slot_info_step1 = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i<sizeof($slot_info_step1); $i++)
		{
			$step1_slottype = $slot_info_step1[$i]["slottype"];
			$step1_amount = $slot_info_step1[$i]["total"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, step_1) VALUES(2, $step1_slottype, $step1_amount) ".
					"ON DUPLICATE KEY UPDATE step_1=VALUES(step_1);";
			$db_main2->execute($insert_sql);
		}
		
		// Android - ODS 제외
		$sql = "SELECT slottype, SUM((playcount * multi)) AS total ".
				"FROM ( ".
				"	SELECT slottype, money_in, money_out, playcount, ROUND(money_in/playcount), ".
				"  		(CASE WHEN ROUND(money_in/playcount) > 100000000 THEN 4000 ".
  				"		WHEN ROUND(money_in/playcount) > 50000000 THEN 2000 ".
  				"		WHEN ROUND(money_in/playcount) > 10000000 THEN 1000 ".
				"		WHEN ROUND(money_in/playcount) > 5000000 THEN 500 ".
				"		WHEN ROUND(money_in/playcount) > 2000000 THEN 200 ".
				"		WHEN ROUND(money_in/playcount) > 1000000 THEN 100 ".
				"		WHEN ROUND(money_in/playcount) > 500000 THEN 50 ".
				"		WHEN ROUND(money_in/playcount) > 200000 THEN 20 ".
				"		WHEN ROUND(money_in/playcount) > 100000 THEN 10 ".
				"		WHEN ROUND(money_in/playcount) > 50000 THEN 5 ".
				"		WHEN ROUND(money_in/playcount) > 25000 THEN 3 ".
				"		ELSE 1 END) AS multi ".
				"	FROM t5_user_gamelog_android ".
				"	WHERE useridx > 20000  AND slottype IN ($mobile_slot_str) AND money_in > 0 AND mode NOT IN (4, 5, 9, 26) AND writedate >= '$before_14day 00:00:00' ".
				") t1 ".
				"GROUP BY slottype ".
				"ORDER BY total DESC";
		
		$slot_info_step1 = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i<sizeof($slot_info_step1); $i++)
		{
			$step1_slottype = $slot_info_step1[$i]["slottype"];
			$step1_amount = $slot_info_step1[$i]["total"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, step_1) VALUES(2, $step1_slottype, $step1_amount) ".
					"ON DUPLICATE KEY UPDATE step_1=VALUES(step_1);";
			$db_main2->execute($insert_sql);
		}
		
		// Web 결제  - ODS 
		$sql = "SELECT 25 AS slottype, AVG(step_2) AS step_2 ".
				"FROM ( ".
				"	SELECT slottype, SUM(playcount*(multi)) AS step_2 ". 
				"	FROM ( ". 
				"		SELECT DISTINCT useridx ".
				"		FROM ( ".
				"			SELECT DISTINCT useridx ". 
				"			FROM t5_product_order ". 
				"			WHERE useridx > 20000 AND status = 1 AND writedate >= '$before_60day 00:00:00' ".
				"			UNION ALL ".
				"			SELECT DISTINCT useridx ".
				"			FROM t5_product_order_mobile ".
				"			WHERE useridx > 20000 AND status = 1 AND writedate >= '$before_60day 00:00:00' ".
				"		) sub ".
				"	) t1 JOIN ( ".
				"	SELECT useridx, slottype, money_in, money_out, playcount, ROUND(money_in/playcount), ".
				"  		(CASE WHEN ROUND(money_in/playcount) > 100000000 THEN 4000 ".
  				"		WHEN ROUND(money_in/playcount) > 50000000 THEN 2000 ".
  				"		WHEN ROUND(money_in/playcount) > 10000000 THEN 1000 ".
				"		WHEN ROUND(money_in/playcount) > 5000000 THEN 500 ".
				"		WHEN ROUND(money_in/playcount) > 2000000 THEN 200 ".
				"		WHEN ROUND(money_in/playcount) > 1000000 THEN 100 ".
				"		WHEN ROUND(money_in/playcount) > 500000 THEN 50 ".
				"		WHEN ROUND(money_in/playcount) > 200000 THEN 20 ".
				"		WHEN ROUND(money_in/playcount) > 100000 THEN 10 ".
				"		WHEN ROUND(money_in/playcount) > 50000 THEN 5 ".
				"		WHEN ROUND(money_in/playcount) > 25000 THEN 3 ".
				"		ELSE 1 END) AS multi, writedate ".
				"		FROM t5_user_gamelog ".
				"		WHERE useridx > 20000  AND slottype IN ($ods_list) AND writedate >= '$before_14day 00:00:00' AND money_in > 0 AND mode NOT IN (4, 5, 9, 26) ".
				"	) t2 ON t1.useridx = t2.useridx ".
				"	GROUP BY slottype ".
				"	ORDER BY step_2 DESC LIMIT 2 ".
				") t3";
		
		$slot_info_step2 = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i<sizeof($slot_info_step2); $i++)
		{
			$step2_slottype = $slot_info_step2[$i]["slottype"];
			$step2_amount = $slot_info_step2[$i]["step_2"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, step_2) VALUES(0, $step2_slottype, $step2_amount) ".
			     		 	"ON DUPLICATE KEY UPDATE step_2=VALUES(step_2);";
			$db_main2->execute($insert_sql);
		}
		
		
		// Web 결제  - ODS 제외
		$sql = 	"SELECT slottype, SUM(playcount*(multi)) AS step_2 ".
				"FROM ( ".
				"	SELECT DISTINCT useridx ".
				"	FROM ( ".
				"		SELECT DISTINCT useridx ".
				"		FROM t5_product_order ".
				"		WHERE useridx > 20000 AND status = 1 AND writedate >= '$before_60day 00:00:00' ".
				"		UNION ALL ".
				"		SELECT DISTINCT useridx ".
				"		FROM t5_product_order_mobile ".
				"		WHERE useridx > 20000 AND status = 1 AND writedate >= '$before_60day 00:00:00' ".
				"	) sub ".
				") t1 JOIN ( ".
				"	SELECT useridx, slottype, money_in, money_out, playcount, ROUND(money_in/playcount), ".
				"  		(CASE WHEN ROUND(money_in/playcount) > 100000000 THEN 4000 ".
  				"		WHEN ROUND(money_in/playcount) > 50000000 THEN 2000 ".
  				"		WHEN ROUND(money_in/playcount) > 10000000 THEN 1000 ".
				"		WHEN ROUND(money_in/playcount) > 5000000 THEN 500 ".
				"		WHEN ROUND(money_in/playcount) > 2000000 THEN 200 ".
				"		WHEN ROUND(money_in/playcount) > 1000000 THEN 100 ".
				"		WHEN ROUND(money_in/playcount) > 500000 THEN 50 ".
				"		WHEN ROUND(money_in/playcount) > 200000 THEN 20 ".
				"		WHEN ROUND(money_in/playcount) > 100000 THEN 10 ".
				"		WHEN ROUND(money_in/playcount) > 50000 THEN 5 ".
				"		WHEN ROUND(money_in/playcount) > 25000 THEN 3 ".
				"		ELSE 1 END) AS multi, writedate ".
				"		FROM t5_user_gamelog ".
				"		WHERE useridx > 20000  AND slottype IN ($web_slot_str) AND writedate >= '$before_14day 00:00:00' AND money_in > 0 AND mode NOT IN (4, 5, 9, 26) ".
				") t2 ON t1.useridx = t2.useridx ".
				"GROUP BY slottype ".
				"ORDER BY step_2 DESC ";
		
		$slot_info_step2 = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i<sizeof($slot_info_step2); $i++)
		{
			$step2_slottype = $slot_info_step2[$i]["slottype"];
			$step2_amount = $slot_info_step2[$i]["step_2"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, step_2) VALUES(0, $step2_slottype, $step2_amount) ".
					"ON DUPLICATE KEY UPDATE step_2=VALUES(step_2);";
			$db_main2->execute($insert_sql);
		}
		// Android 결제  - ODS
		$sql = "SELECT 25 AS slottype, AVG(step_2) AS step_2 ".
				"FROM ( ".
				"	SELECT slottype, SUM(playcount*(multi)) AS step_2 ".
				"	FROM ( ".
				"		SELECT DISTINCT useridx ".
				"		FROM ( ".
				"			SELECT DISTINCT useridx ".
				"			FROM t5_product_order ".
				"			WHERE useridx > 20000 AND status = 1 AND writedate >= '$before_60day 00:00:00' ".
				"			UNION ALL ".
				"			SELECT DISTINCT useridx ".
				"			FROM t5_product_order_mobile ".
				"			WHERE useridx > 20000 AND status = 1 AND writedate >= '$before_60day 00:00:00' ".
				"		) sub ".
				"	) t1 JOIN ( ".
				"	SELECT useridx, slottype, money_in, money_out, playcount, ROUND(money_in/playcount), ".
				"  		(CASE WHEN ROUND(money_in/playcount) > 100000000 THEN 4000 ".
  				"		WHEN ROUND(money_in/playcount) > 50000000 THEN 2000 ".
  				"		WHEN ROUND(money_in/playcount) > 10000000 THEN 1000 ".
				"		WHEN ROUND(money_in/playcount) > 5000000 THEN 500 ".
				"		WHEN ROUND(money_in/playcount) > 2000000 THEN 200 ".
				"		WHEN ROUND(money_in/playcount) > 1000000 THEN 100 ".
				"		WHEN ROUND(money_in/playcount) > 500000 THEN 50 ".
				"		WHEN ROUND(money_in/playcount) > 200000 THEN 20 ".
				"		WHEN ROUND(money_in/playcount) > 100000 THEN 10 ".
				"		WHEN ROUND(money_in/playcount) > 50000 THEN 5 ".
				"		WHEN ROUND(money_in/playcount) > 25000 THEN 3 ".
				"		ELSE 1 END) AS multi, writedate ".
				"		FROM t5_user_gamelog_android ".
				"		WHERE useridx > 20000  AND slottype IN ($ods_list) AND writedate >= '$before_14day 00:00:00' AND money_in > 0 AND mode NOT IN (4, 5, 9, 26) ".
				"	) t2 ON t1.useridx = t2.useridx ".
				"	GROUP BY slottype ".
				"	ORDER BY step_2 DESC LIMIT 2 ".
				") t3";
		
		$slot_info_step2 = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i<sizeof($slot_info_step2); $i++)
		{
			$step2_slottype = $slot_info_step2[$i]["slottype"];
			$step2_amount = $slot_info_step2[$i]["step_2"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, step_2) VALUES(2, $step2_slottype, $step2_amount) ".
					"ON DUPLICATE KEY UPDATE step_2=VALUES(step_2);";
			$db_main2->execute($insert_sql);
		}
		
		// Android 결제  - ODS 제외
		$sql = 	"SELECT slottype, SUM(playcount*(multi)) AS step_2 ".
				"FROM ( ".
				"	SELECT DISTINCT useridx ".
				"	FROM ( ".
				"		SELECT DISTINCT useridx ".
				"		FROM t5_product_order ".
				"		WHERE useridx > 20000 AND status = 1 AND writedate >= '$before_60day 00:00:00' ".
				"		UNION ALL ".
				"		SELECT DISTINCT useridx ".
				"		FROM t5_product_order_mobile ".
				"		WHERE useridx > 20000 AND status = 1 AND writedate >= '$before_60day 00:00:00' ".
				"	) sub ".
				") t1 JOIN ( ".
				"	SELECT useridx, slottype, money_in, money_out, playcount, ROUND(money_in/playcount), ".
				"  		(CASE WHEN ROUND(money_in/playcount) > 100000000 THEN 4000 ".
  				"		WHEN ROUND(money_in/playcount) > 50000000 THEN 2000 ".
  				"		WHEN ROUND(money_in/playcount) > 10000000 THEN 1000 ".
				"		WHEN ROUND(money_in/playcount) > 5000000 THEN 500 ".
				"		WHEN ROUND(money_in/playcount) > 2000000 THEN 200 ".
				"		WHEN ROUND(money_in/playcount) > 1000000 THEN 100 ".
				"		WHEN ROUND(money_in/playcount) > 500000 THEN 50 ".
				"		WHEN ROUND(money_in/playcount) > 200000 THEN 20 ".
				"		WHEN ROUND(money_in/playcount) > 100000 THEN 10 ".
				"		WHEN ROUND(money_in/playcount) > 50000 THEN 5 ".
				"		WHEN ROUND(money_in/playcount) > 25000 THEN 3 ".
				"		ELSE 1 END) AS multi, writedate ".
				"		FROM t5_user_gamelog_android ".
				"		WHERE useridx > 20000  AND slottype IN ($mobile_slot_str) AND writedate >= '$before_14day 00:00:00' AND money_in > 0 AND mode NOT IN (4, 5, 9, 26) ".
				") t2 ON t1.useridx = t2.useridx ".
				"GROUP BY slottype ".
				"ORDER BY step_2 DESC ";
		
		$slot_info_step2 = $db_redshift->gettotallist($sql);
		
		for($i = 0; $i<sizeof($slot_info_step2); $i++)
		{
			$step2_slottype = $slot_info_step2[$i]["slottype"];
			$step2_amount = $slot_info_step2[$i]["step_2"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, step_2) VALUES(2, $step2_slottype, $step2_amount) ".
					"ON DUPLICATE KEY UPDATE step_2=VALUES(step_2);";
			$db_main2->execute($insert_sql);
		}

		// MAX(전체), MAX(결제) 일치하도록 Normalize - Web
		$sql = "SELECT ROUND(MAX(step_1)/MAX(step_2),0) AS multiple FROM tbl_slot_ranking_info WHERE platform = 0 ";
		$ranking_multiple = $db_slave_main2->getvalue($sql);
		
		$sql = "SELECT slottype, step_2 * $ranking_multiple AS step2_balance FROM tbl_slot_ranking_info WHERE platform = 0 ";
		$ranking_step2_balance = $db_slave_main2->gettotallist($sql);
		
		for($i = 0; $i<sizeof($ranking_step2_balance); $i++)
		{
			$step2_balance_slottype = $ranking_step2_balance[$i]["slottype"];
			$step2_balance_amount = $ranking_step2_balance[$i]["step2_balance"];
			
			$insert_sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, step_2) VALUES(0, $step2_balance_slottype, $step2_balance_amount) ".
			     		 	"ON DUPLICATE KEY UPDATE step_2=VALUES(step_2);";
			$db_main2->execute($insert_sql);
		}
		
		
		// MAX(전체), MAX(결제) 일치하도록 Normalize - Android
		$sql = "SELECT ROUND(MAX(step_1)/MAX(step_2),0) AS multiple FROM tbl_slot_ranking_info WHERE platform = 2 ";
		$ranking_multiple = $db_slave_main2->getvalue($sql);
		
		$sql = "SELECT slottype, step_2 * $ranking_multiple AS step2_balance FROM tbl_slot_ranking_info WHERE platform = 2 ";
		$ranking_step2_balance = $db_slave_main2->gettotallist($sql);
		
		for($i = 0; $i<sizeof($ranking_step2_balance); $i++)
		{
			$step2_balance_slottype = $ranking_step2_balance[$i]["slottype"];
			$step2_balance_amount = $ranking_step2_balance[$i]["step2_balance"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, step_2) VALUES(2, $step2_balance_slottype, $step2_balance_amount) ".
					"ON DUPLICATE KEY UPDATE step_2=VALUES(step_2);";
			$db_main2->execute($insert_sql);
		}
		
		
		//전체 X 4 + 결제 X 6 하여 최종 순위값 도출 - Web
		$sql = "SELECT slottype, ((step_1*4) +  (step_2*6)) AS ranking FROM tbl_slot_ranking_info WHERE platform = 0";
		$ranking_info = $db_slave_main2->gettotallist($sql);
		
		for($i = 0; $i<sizeof($ranking_info); $i++)
		{
			$ranking_slottype = $ranking_info[$i]["slottype"];
			$ranking_amount = $ranking_info[$i]["ranking"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, ranking) VALUES(0, $ranking_slottype, $ranking_amount) ".
			     		 	"ON DUPLICATE KEY UPDATE ranking=VALUES(ranking);";
			$db_main2->execute($insert_sql);
		}
		
		//전체 X 4 + 결제 X 6 하여 최종 순위값 도출 - Android
		$sql = "SELECT slottype, ((step_1*4) +  (step_2*6)) AS ranking FROM tbl_slot_ranking_info WHERE platform = 2";
		$ranking_info = $db_slave_main2->gettotallist($sql);
		
		for($i = 0; $i<sizeof($ranking_info); $i++)
		{
			$ranking_slottype = $ranking_info[$i]["slottype"];
			$ranking_amount = $ranking_info[$i]["ranking"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, ranking) VALUES(2, $ranking_slottype, $ranking_amount) ".
					"ON DUPLICATE KEY UPDATE ranking=VALUES(ranking);";
			$db_main2->execute($insert_sql);
		}
		
		// 최종 순위 계산 - Web
		$sql = "SELECT  @ROWNUM := @ROWNUM+1 AS ROWNUM, t1.* ".
				"FROM ".
				"(	".
				"	SELECT slottype FROM tbl_slot_ranking_info WHERE platform = 0 ORDER BY ranking DESC ".
				") t1, (SELECT @ROWNUM := 0) R";
		$ranking_num_info = $db_slave_main2->gettotallist($sql);
		
		for($i = 0; $i<sizeof($ranking_num_info); $i++)
		{
			$ranking_num_slottype = $ranking_num_info[$i]["slottype"];
			$rownum = $ranking_num_info[$i]["ROWNUM"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, ranking_num) VALUES(0, $ranking_num_slottype, $rownum) ".
							"ON DUPLICATE KEY UPDATE ranking_num=VALUES(ranking_num);";
			$db_main2->execute($insert_sql);
		}
		
		// is_new 슬롯 상위권 순위 고정
		$sql = "SELECT slottype FROM tbl_slot_list WHERE open_date > DATE_SUB(NOW(),INTERVAL 2 WEEK) AND web = 1 ORDER BY open_date DESC";
		$newslot_list = $db_slave_main2->gettotallist($sql);
		
		if(sizeof($newslot_list) > 0)
		{
			$newslot_cnt = 0;

			for($i=0; $i<sizeof($newslot_list); $i++)
			{
				$newslot_type = $newslot_list[$i]["slottype"];
				write_log("web".$newslot_type);
				$sql = "SELECT ranking_num, slottype FROM tbl_slot_ranking_info WHERE slottype = $newslot_type AND platform = 0;";
				$newslot_rank_info = $db_slave_main2->getarray($sql);
				
				if($newslot_rank_info != "")
				{
					$newslot_rank = $newslot_rank_info["ranking_num"];
					$newslot_type = $newslot_rank_info["slottype"];
				}
				else
				{
					$newslot_rank = $newslot_type;
					
					$sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, ranking_num) VALUES(0, $newslot_type, $newslot_rank)";
					$db_main2->execute($sql);
				}
				
				$fix_rank = 1 + $newslot_cnt;
				
				if($newslot_rank > $fix_rank)
				{
					$insert_sql = "UPDATE tbl_slot_ranking_info SET ranking_num = ranking_num+1 WHERE slottype!=$newslot_type AND ranking_num >= $fix_rank AND platform = 0;";
					$insert_sql .= "UPDATE tbl_slot_ranking_info SET ranking_num = $fix_rank WHERE slottype=$newslot_type AND platform = 0;";
					$insert_sql .= "UPDATE tbl_slot_ranking_info SET ranking_num = ranking_num-1 WHERE ranking_num > $newslot_rank AND platform = 0;";
					
					$db_main2->execute($insert_sql);
				}
				
				$newslot_cnt++;
			}
		}
		
		// 최종 순위 계산 - Android
		$sql = "SELECT  @ROWNUM := @ROWNUM+1 AS ROWNUM, t1.* ".
				"FROM ".
				"(	".
				"	SELECT slottype FROM tbl_slot_ranking_info WHERE platform = 2 AND slottype ORDER BY ranking DESC ".
				") t1, (SELECT @ROWNUM := 0) R";
		$ranking_num_info = $db_slave_main2->gettotallist($sql);
		
		for($i = 0; $i<sizeof($ranking_num_info); $i++)
		{
			$ranking_num_slottype = $ranking_num_info[$i]["slottype"];
			$rownum = $ranking_num_info[$i]["ROWNUM"];
		
			$insert_sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, ranking_num) VALUES(2, $ranking_num_slottype, $rownum) ".
					"ON DUPLICATE KEY UPDATE ranking_num=VALUES(ranking_num);";
			$db_main2->execute($insert_sql);
		}
		
		// is_new 슬롯 상위권 순위 고정
		$sql = "SELECT slottype FROM tbl_slot_list WHERE open_date_mobile > DATE_SUB(NOW(),INTERVAL 2 WEEK) AND android = 1 ORDER BY open_date_mobile DESC";
		$newslot_list = $db_slave_main2->gettotallist($sql);
		
		if(sizeof($newslot_list) > 0)
		{
			$newslot_cnt = 0;
			
			for($i=0; $i<sizeof($newslot_list); $i++)
			{
				$newslot_type = $newslot_list[$i]["slottype"];
				write_log("android".$newslot_type);
				$sql = "SELECT ranking_num, slottype FROM tbl_slot_ranking_info WHERE slottype = $newslot_type AND platform = 2;";
				$newslot_rank_info = $db_slave_main2->getarray($sql);
				
				if($newslot_rank_info != "")
				{
					$newslot_rank = $newslot_rank_info["ranking_num"];
					$newslot_type = $newslot_rank_info["slottype"];
				}
				else
				{
					$newslot_rank = $newslot_type;
				
					$sql = "INSERT INTO tbl_slot_ranking_info(platform, slottype, ranking_num) VALUES(2, $newslot_type, $newslot_rank)";
					$db_main2->execute($sql);
				}
		
				$fix_rank = 1 + $newslot_cnt;
		
				if($newslot_rank > $fix_rank)
				{
					$insert_sql = "UPDATE tbl_slot_ranking_info SET ranking_num = ranking_num+1 WHERE slottype!=$newslot_type AND ranking_num >= $fix_rank AND platform = 2;";
					$insert_sql .= "UPDATE tbl_slot_ranking_info SET ranking_num = $fix_rank WHERE slottype=$newslot_type AND platform = 2;";
					$insert_sql .= "UPDATE tbl_slot_ranking_info SET ranking_num = ranking_num-1 WHERE ranking_num > $newslot_rank AND platform = 2;";
						
					$db_main2->execute($insert_sql);
				}
		
				$newslot_cnt++;
			}
		}
		
// 		//web 부활절 이스타 보난자
// 		$sql = "SELECT ranking_num  FROM `tbl_slot_ranking_info` WHERE platform = 0  AND slottype = 17";
// 		$easter_bonanza_ranking_num = $db_main2->getvalue($sql);
		
// 		$sql = " UPDATE tbl_slot_ranking_info SET ranking_num = 2 WHERE platform = 0 AND slottype = 17;".
// 				" UPDATE tbl_slot_ranking_info SET ranking_num = ranking_num+1 WHERE platform = 0 AND  ranking_num > 1 AND ranking_num < $easter_bonanza_ranking_num AND slottype != 17; ";
// 		$db_main2->execute($sql);
		
		//Web align 적용		
		$sql="SELECT slottype, ranking_num FROM `tbl_slot_ranking_info` WHERE platform = 0";
		$web_align_list = $db_slave_main2 -> gettotallist($sql);
	
		$web_align_sql = "";
		for($i=0; $i<sizeof($web_align_list); $i++)
		{
			$slottype = $web_align_list[$i]["slottype"];
			$alignidx = $web_align_list[$i]["ranking_num"];
			$web_align_sql .="UPDATE tbl_normal_bet_info SET alignidx =$alignidx	WHERE slotidx =$slottype;";
			
		}
		$db_game->execute($web_align_sql);
		

// 		//mobile 부활절 이스타 보난자
// 		$sql = "SELECT ranking_num  FROM `tbl_slot_ranking_info` WHERE platform = 2  AND slottype = 17";
// 		$easter_bonanza_ranking_num = $db_main2->getvalue($sql);
		
// 		$sql = " UPDATE tbl_slot_ranking_info SET ranking_num = 2 WHERE platform = 2 AND slottype = 17;".
// 				" UPDATE tbl_slot_ranking_info SET ranking_num = ranking_num+1 WHERE platform = 2 AND  ranking_num > 1 AND ranking_num < $easter_bonanza_ranking_num AND slottype != 17; ";
// 		$db_main2->execute($sql);
		
		//Mobile align 적용
		$sql="SELECT slottype, ranking_num FROM `tbl_slot_ranking_info` WHERE platform = 2";
		$mobile_align_list = $db_slave_main2 -> gettotallist($sql);
		
		$mobile_align_sql = "";
		for($i=0; $i<sizeof($mobile_align_list); $i++)
		{
			$slottype = $mobile_align_list[$i]["slottype"];
			$alignidx = $mobile_align_list[$i]["ranking_num"];
			$mobile_align_sql .="UPDATE tbl_normal_bet_info SET alignidx_mobile =$alignidx	WHERE slotidx =$slottype;";
			
		}
		$db_game->execute($mobile_align_sql);

	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	write_log("Slot Ranking Scheduler End");
	
	$db_main2->end();
	$db_slave_main2->end();
	$db_game->end();
	$db_redshift->end();
?>