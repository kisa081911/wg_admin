<?
	include("../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();
	
	$port = "";
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
		$port = ":8081";
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/fanpage_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
		
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/fanpage_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
		
				$content .= "kill -9 ".$process_list[0]."\n";
		
				write_log("fanpage_scheduler Dead Lock Kill!");
			}
		}
		
		fwrite($fp, $content, strlen($content));
		fclose($fp);
		
		exit();
	}
	
	try
	{
	    $accessToken = $db_main->getvalue('SELECT accesstoken FROM page_accesstoken WHERE userid = 0');
	    
	    $appsecret_proof = hash_hmac('sha256', $accessToken, FACEBOOK_SECRET_KEY);
	    
		$facebook = new Facebook(array(
				'appId'  => FACEBOOK_APP_ID,
				'secret' => FACEBOOK_SECRET_KEY,
		        'appsecret_proof' => $appsecret_proof,
				'cookie' => true,
		));
		
		$session = $facebook->getUser();
		
		$access_token = $db_analysis->getvalue("SELECT accesstoken FROM `page_accesstoken` WHERE userid = 0");
		$facebook->setAccessToken($access_token);
		
		$feedurl = "/take5freeslots/feed?appsecret_proof=$appsecret_proof";
		$articlecount = 0;
		
		while ($feedurl != "")
		{
			$result = $facebook->api($feedurl);
			$list = $result["data"];
			for ($i=0; $i<sizeof($list); $i++)
			{
				$data = $list[$i];
					
				$articleid = $data["id"];
				$message = $data["message"];
				
				
				$likeurl = "/$articleid/likes?summary=true&appsecret_proof=$appsecret_proof";
				$like_result = $facebook->api($likeurl);
				$likes = $like_result["summary"]["total_count"];
				
				$commentsurl = "/$articleid/comments?summary=true&appsecret_proof=$appsecret_proof";
				$comments_result = $facebook->api($commentsurl);
				$comments = $comments_result["summary"]["total_count"];
				
				$writedate = str_replace("T", " ", substr($data["created_time"], 0, 19));
				
				if ($message == "")
					continue;
				
				$message = addslashes($message);
				
				$sql = "DELETE FROM fanpage_article WHERE articleid='$articleid'\n".
						"INSERT INTO fanpage_article(articleid,message,shares,likes,comments,writedate) ".
						"VALUES('$articleid','$message','0','$likes','$comments','$writedate')";
				
				$db_analysis->execute($sql);
				
				$articlecount++;
				
				$commenturl = "/$articleid/comments?limit=1000&appsecret_proof=$appsecret_proof";
				
				while ($commenturl != "")
				{
					sleep(3);
					$comments_result = $facebook->api($commenturl);
					$commentlist = $comments_result["data"];
				
					for ($j=0; $j<sizeof($commentlist); $j++)
					{
						$data = $commentlist[$j];
						
						$commentid = $data["id"];
						$facebookid = $data["from"]["id"];
						$message = $data["message"];
						$likes = $data["like_count"];
						$writedate = str_replace("T", " ", substr($data["created_time"], 0, 19));
						
						$message = addslashes($message);
				
 						$sql = "SELECT COUNT(*) FROM fanpage_article_comment WHERE commentid='$commentid'";
										
 						if ($db_analysis->getvalue($sql) == 0)
 						{
							$sql = "SELECT useridx FROM tbl_user WHERE userid='$facebookid'";
							$useridx = $db_main->getvalue($sql);
				
							if ($useridx == "")
								$useridx = "0";
				
							$sql = "INSERT INTO fanpage_article_comment(commentid,articleid,facebookid,useridx,message,likes,writedate) ".
									"VALUES('$commentid','$articleid','$facebookid',$useridx,'$message','$likes','$writedate') ON DUPLICATE KEY UPDATE facebookid=VALUES(facebookid), useridx=VALUES(useridx)";
							$db_analysis->execute($sql);
 						}
					}
				
					$commenturl = str_replace("https://graph.facebook.com/v3.3", "", $comments_result["paging"]["next"]);
				}
				
				$likeurl = "/$articleid/likes?limit=1000&appsecret_proof=$appsecret_proof";
				
				while ($likeurl != "")
				{
					sleep(3);
					$likes_result = $facebook->api($likeurl);
					$likelist = $likes_result["data"];
				
					for ($j=0; $j<sizeof($likelist); $j++)
					{
						$data = $likelist[$j];
				
						$facebookid = $data["id"];
				
						$sql = "SELECT COUNT(*) FROM fanpage_article_like WHERE articleid='$articleid' AND facebookid=$facebookid";
							
						if ($db_analysis->getvalue($sql) == 0)
						{
							$sql = "SELECT useridx FROM tbl_user WHERE userid=$facebookid";
							$useridx = $db_main->getvalue($sql);
						 
							if ($useridx == "")
								$useridx = "0";
				
							$sql = "INSERT INTO fanpage_article_like(articleid,facebookid,useridx,writedate) ".
									"VALUES('$articleid','$facebookid',$useridx,NOW())";
						 
							$db_analysis->execute($sql);
						}
					}
				
					$likeurl = str_replace("https://graph.facebook.com/v3.3", "", $likes_result["paging"]["next"]);
				}
				
				if ($articlecount >= 3)
					break;
			}
			
			if ($articlecount >= 3)
				break;
			
			$feedurl = str_replace("https://graph.facebook.com/v3.3", "", $result["paging"]["next"]);
		}
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	$db_main->end();
	$db_analysis->end();
?>