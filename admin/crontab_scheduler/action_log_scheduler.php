<?
	include("../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/action_log_scheduler") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/action_log_scheduler") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("action_log_scheduler Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	//오늘 날짜 정보
	$now_time = date("H");
	
	if ($now_time == "00")
		$startdate = date("Y-m-d", mktime(0,0,0,date("m"),date("d")-1,date("Y")));
	else
		$startdate = date("Y-m-d", mktime(0,0,0,date("m"),date("d"),date("Y")));
	
	$tail = "WHERE status=1 AND useridx > $std_useridx AND writedate >= '$startdate 00:00:00' AND writedate <= '$startdate 23:59:59' ";
	
	$sql = "SELECT useridx,(SELECT userid FROM tbl_user WHERE useridx=tbl_product_order.useridx) AS facebookid,facebookcredit,writedate FROM tbl_product_order $tail";
	$order_list = $db_main->gettotallist($sql);
	
	$temp = $db_main->getarray("SELECT COUNT(*) AS total_count, IFNULL(SUM(facebookcredit),0) AS total_credit FROM tbl_product_order $tail");
	
	$total_count = $temp["total_count"];
	$total_credit = $temp["total_credit"];
	
	$action_list = array();
	
	foreach ($order_list as $orders)
	{
		$useridx = $orders["useridx"];
		$facebookid = $orders["facebookid"];
		$facebookcredit = $orders["facebookcredit"];
		$writedate = $orders["writedate"];
	
		$sql = "SELECT category,category_info,action,writedate,CONCAT(category,' (',category_info, ')' ) AS detail_category FROM user_action_log_".($facebookid % 10)." ".
				"WHERE facebookid=$facebookid AND writedate < '$writedate' AND category NOT IN ('PURCHASE', 'PURCHASE(realtime)', 'LOADING', 'LOADING End', 'LOGIN_COMPLETE', 'Resource Error') ORDER BY actionidx DESC LIMIT 0,5";
		$action_before_log_list = $db_analysis->gettotallist($sql);
	
		$sql = "SELECT category,category_info,action,writedate,CONCAT(category,' (',category_info, ')' ) AS detail_category FROM user_action_log_".($facebookid % 10)." ".
				"WHERE facebookid=$facebookid AND writedate > '$writedate' AND category NOT IN ('PURCHASE', 'PURCHASE(realtime)', 'LOADING', 'LOADING End', 'LOGIN_COMPLETE', 'Resource Error') ORDER BY actionidx LIMIT 0,5";
		$action_after_log_list = $db_analysis->gettotallist($sql);
	
		$point = 5;
		$sum_orderprice_point = 0;
		$sum_ordercount_point = 0;
		$sum_frequency = 0;
		foreach ($action_before_log_list as $actions)
		{
			$category = $actions["category"];
			$detail_category = $actions["detail_category"];
	
			if (!array_key_exists($detail_category, $action_list))
			{
				$info = array("orderprice_point"=>($point*$facebookcredit),"ordercount_point"=>$point, "frequency"=>1, "category"=>$category, "detail_category"=>$detail_category);
				$temp = array($detail_category=>$info);
				$action_list = array_merge($action_list,$temp);
			}
			else
			{
				$info = $action_list[$detail_category];
	
				$sum_orderprice_point = $info["orderprice_point"] + ($point*$facebookcredit);
				$info["orderprice_point"] = $sum_orderprice_point;
	
				$sum_ordercount_point = $info["ordercount_point"] + $point;
				$info["ordercount_point"] = $sum_ordercount_point;
	
				$sum_frequency = $info["frequency"] + 1;
				$info["frequency"] = $sum_frequency;
	
				$action_list[$detail_category] = $info;
			}
			 
			$point--;
		}
	
		$point = 5;
		$sum_orderprice_point = 0;
		$sum_ordercount_point = 0;
		$sum_frequency = 0;
		
		foreach ($action_after_log_list as $actions)
		{
			$category = $actions["category"];
			$detail_category = $actions["detail_category"];
	
			if (!array_key_exists($detail_category, $action_list))
			{
				$info = array("orderprice_point"=>($point*$facebookcredit),"ordercount_point"=>$point, "frequency"=>1, "category"=>$category, "detail_category"=>$detail_category);
				$temp = array($detail_category=>$info);
				$action_list = array_merge($action_list,$temp);
			}
			else
			{
				$info = $action_list[$detail_category];
	
				$sum_orderprice_point = $info["orderprice_point"] + ($point*$facebookcredit);
				$info["orderprice_point"] = $sum_orderprice_point;
	
				$sum_ordercount_point = $info["ordercount_point"] + $point;
				$info["ordercount_point"] = $sum_ordercount_point;
	
				$sum_frequency = $info["frequency"] + 1;
				$info["frequency"] = $sum_frequency;
	
				$action_list[$detail_category] = $info;
			}
			 
			$point--;
		}
	}
	
	$total_ordercount_point = 0;
	$total_orderprice_point = 0;
	
	foreach ($action_list as $key=>$value)
	{
		$total_ordercount_point += $value["ordercount_point"];
		$total_orderprice_point += $value["orderprice_point"];
	}
	
	$sql = "DELETE FROM user_order_factor_log WHERE writedate='$startdate'";
	$db_analysis->execute($sql);
	
	foreach ($action_list as $key=>$value)
	{
		$category = $value["category"];
		$detail_category = $value["detail_category"];
		$frequency = $value["frequency"];
		$ordercount_point = $value["ordercount_point"];
		$orderprice_point = $value["orderprice_point"];
		$rate1 = $ordercount_point / $total_ordercount_point;
		$rate2 = $orderprice_point / $total_orderprice_point;
	
		$amount_count = $total_count * $rate1;
		$amount_price = $total_credit * $rate2;
	
		$sql = "INSERT INTO user_order_factor_log (category,detail_category,frequency,ordercount_point,orderprice_point,amount_count,amount_price,writedate) ".
				"VALUES ('$category','$detail_category',$frequency,'$ordercount_point','$orderprice_point','$amount_count','$amount_price','$startdate')";
		$db_analysis->execute($sql);
	}
	
	$db_main->end();
	$db_analysis->end();
?>
