<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/email/email_retention_stat_scheduler") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/email/email_retention_stat_scheduler") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("email/email_retention_stat_scheduler Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 11);
	
	$db_main2 = new CDatabase_Main2();
	$db_redshift = new CDatabase_Redshift();
	
	$db_main2->execute("SET wait_timeout=21600");
	
	$sql = "SELECT t2.eventidx, title, t2.category, start_eventdate, end_eventdate, COUNT(*) AS user_cnt, COUNT(IF(isreturn=1,1,NULL)) AS nopay_return_cnt, COUNT(IF(ordercredit > 0,1,NULL)) AS pay_return_cnt
			FROM tbl_event_result t1 JOIN (
				SELECT * FROM tbl_event WHERE category IN (7) AND '$yesterday' BETWEEN start_eventdate AND end_eventdate
			) t2 ON t1.eventidx = t2.eventidx
			WHERE t1.writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'
			GROUP BY t2.eventidx, t2.category
			ORDER BY category ASC, eventidx ASC";
	$email_retention_event_stat = $db_main2->gettotallist($sql);
	
	$insert_sql = "";
	
	for($i=0; $i<sizeof($email_retention_event_stat); $i++)
	{
		$eventidx = $email_retention_event_stat[$i]["eventidx"];
		$title = encode_db_field($email_retention_event_stat[$i]["title"]);
		$category = $email_retention_event_stat[$i]["category"];
		$start_eventdate = $email_retention_event_stat[$i]["start_eventdate"];
		$end_eventdate = $email_retention_event_stat[$i]["end_eventdate"];
		
		$user_cnt = $email_retention_event_stat[$i]["user_cnt"];
		$nopay_return_cnt = $email_retention_event_stat[$i]["nopay_return_cnt"];
		$pay_return_cnt = $email_retention_event_stat[$i]["pay_return_cnt"];
		
		$sql = "SELECT agencyidx, sendidx, target_type FROM tbl_email_retention_setting WHERE eventidx = $eventidx";
		$email_retention_setting_info = $db_main2->getarray($sql);
		
		$agencyidx = $email_retention_setting_info["agencyidx"];
		$sendidx = $email_retention_setting_info["sendidx"];
		$target_type = $email_retention_setting_info["target_type"];
		
		if($category == 7)
			$table = "t5_user_exclude_useridx";
		
		$target_email_retention_useridx = "";
		$target_email_retention_user_cnt = 0;
		$target_other_retention_user_cnt = 0;
		$target_email_active_user_cnt = 0;
		$target_other_active_user_cnt = 0;
		
		$sql = "SELECT useridx,
					(CASE WHEN web_adflag is not null THEN web_adflag ELSE mobile_adflag END) AS adflag,
					(CASE WHEN web_leavedays is not null THEN web_leavedays ELSE mobile_leavedays END) AS leavedays
				FROM (
					SELECT t2.useridx,
						(SELECT adflag FROM t5_user_retention_log WHERE writedate = t2.recent_writedate AND useridx = t2.useridx) AS web_adflag,
						(SELECT leavedays FROM t5_user_retention_log WHERE writedate = t2.recent_writedate AND useridx = t2.useridx) AS web_leavedays,
						(SELECT adflag FROM t5_user_retention_mobile_log WHERE writedate = t2.recent_writedate AND useridx = t2.useridx) AS mobile_adflag,
						(SELECT leavedays FROM t5_user_retention_mobile_log WHERE writedate = t2.recent_writedate AND useridx = t2.useridx) AS mobile_leavedays,
						recent_writedate
					FROM (
						SELECT useridx, MIN(writedate) AS recent_writedate
						FROM (
							SELECT useridx, adflag, leavedays, writedate FROM t5_user_retention_log WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'
							AND EXISTS (SELECT useridx FROM $table WHERE sendidx = $sendidx AND useridx = t5_user_retention_log.useridx AND agencyidx = $agencyidx)
							AND EXISTS (SELECT useridx FROM t5_event_result WHERE eventidx = $eventidx AND useridx = t5_user_retention_log.useridx AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59')
							UNION ALL
							SELECT useridx, adflag, leavedays, writedate FROM t5_user_retention_mobile_log WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'
							AND EXISTS (SELECT useridx FROM $table WHERE sendidx = $sendidx AND useridx = t5_user_retention_mobile_log.useridx AND agencyidx = $agencyidx)
							AND EXISTS (SELECT useridx FROM t5_event_result WHERE eventidx = $eventidx AND useridx = t5_user_retention_mobile_log.useridx AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59')
						) t1
						GROUP BY useridx 
					) t2
					group by useridx,recent_writedate
				) t3";
		$target_retention_list = $db_redshift->gettotallist($sql);
		
		for($j=0; $j<sizeof($target_retention_list); $j++)
		{
			$useridx = $target_retention_list[$j]["useridx"];
			$adflag = $target_retention_list[$j]["adflag"];
			$leavedays = $target_retention_list[$j]["leavedays"];
			
			if($adflag == "email_retention" && $leavedays >= 7)
			{
				if($target_email_retention_useridx == "")
					$target_email_retention_useridx = $useridx;
				else
					$target_email_retention_useridx .= ",".$useridx;
				
				$target_email_retention_user_cnt++;
			}
			else if($adflag != "email_retention" && $leavedays >= 7)
				$target_other_retention_user_cnt++;
			else if($adflag == "email_retention" && $leavedays < 7)
				$target_email_active_user_cnt++;
			else if($adflag != "email_retention" && $leavedays < 7)
				$target_other_active_user_cnt++;
		}
		
		$non_target_email_retention_useridx = "";
		$non_target_email_retention_user_cnt = 0;
		$non_target_other_retention_user_cnt = 0;
		$non_target_email_active_user_cnt = 0;
		$non_target_other_active_user_cnt = 0;
		
		$sql = "SELECT useridx,
					(CASE WHEN web_adflag is not null THEN web_adflag ELSE mobile_adflag END) AS adflag,
					(CASE WHEN web_leavedays is not null THEN web_leavedays ELSE mobile_leavedays END) AS leavedays
				FROM (
					SELECT t2.useridx,
						(SELECT adflag FROM t5_user_retention_log WHERE writedate = t2.recent_writedate AND useridx = t2.useridx) AS web_adflag,
						(SELECT leavedays FROM t5_user_retention_log WHERE writedate = t2.recent_writedate AND useridx = t2.useridx) AS web_leavedays,
						(SELECT adflag FROM t5_user_retention_mobile_log WHERE writedate = t2.recent_writedate AND useridx = t2.useridx) AS mobile_adflag,
						(SELECT leavedays FROM t5_user_retention_mobile_log WHERE writedate = t2.recent_writedate AND useridx = t2.useridx) AS mobile_leavedays,
						recent_writedate
					FROM (
						SELECT useridx, MIN(writedate) AS recent_writedate
						FROM (
							SELECT useridx, adflag, leavedays, writedate FROM t5_user_retention_log WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'
							AND NOT EXISTS (SELECT useridx FROM $table WHERE sendidx = $sendidx AND useridx = t5_user_retention_log.useridx AND agencyidx = $agencyidx)
							AND EXISTS (SELECT useridx FROM t5_event_result WHERE eventidx = $eventidx AND useridx = t5_user_retention_log.useridx AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59')
							UNION ALL
							SELECT useridx, adflag, leavedays, writedate FROM t5_user_retention_mobile_log WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'
							AND NOT EXISTS (SELECT useridx FROM $table WHERE sendidx = $sendidx AND useridx = t5_user_retention_mobile_log.useridx AND agencyidx = $agencyidx)
							AND EXISTS (SELECT useridx FROM t5_event_result WHERE eventidx = $eventidx AND useridx = t5_user_retention_mobile_log.useridx AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59')
						) t1
						GROUP BY useridx
					) t2
					GROUP BY useridx ,recent_writedate
				) t3";
		$non_target_retention_list = $db_redshift->gettotallist($sql);
		
		for($j=0; $j<sizeof($non_target_retention_list); $j++)
		{
			$useridx = $non_target_retention_list[$j]["useridx"];
			$adflag = $non_target_retention_list[$j]["adflag"];
			$leavedays = $non_target_retention_list[$j]["leavedays"];
				
			if($adflag == "email_retention" && $leavedays >= 7)
			{
				if($non_target_email_retention_useridx == "")
					$non_target_email_retention_useridx = $useridx;
				else
					$non_target_email_retention_useridx .= ",".$useridx;
		
				$non_target_email_retention_user_cnt++;
			}
			else if($adflag != "email_retention" && $leavedays >= 7)
				$non_target_other_retention_user_cnt++;
			else if($adflag == "email_retention" && $leavedays < 7)
				$non_target_email_active_user_cnt++;
			else if($adflag != "email_retention" && $leavedays < 7)
				$non_target_other_active_user_cnt++;
		}
		
		if($target_email_retention_useridx == "")
		{
			$money = 0;
			$send_cnt = $db_redshift->getvalue("SELECT COUNT(*) FROM $table WHERE sendidx = $sendidx AND agencyidx = $agencyidx");
			write_log("SELECT COUNT(*) FROM $table WHERE sendidx = $sendidx AND agencyidx = $agencyidx");
		}
		else
		{
			$sql = "SELECT nvl(SUM(money), 0) AS money, (SELECT COUNT(*) FROM $table WHERE sendidx = $sendidx AND agencyidx = $agencyidx) AS send_cnt
					FROM (
						SELECT useridx, (facebookcredit / 10) AS money FROM t5_product_order WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND status = 1 AND useridx IN ($target_email_retention_useridx)
						UNION ALL
						SELECT useridx, money FROM t5_product_order_mobile WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND status = 1 AND useridx IN ($target_email_retention_useridx)
					) t1";
			write_log($sql);
			$send_info = $db_redshift->getarray($sql);
			
			$money = $send_info["money"];
			$send_cnt = $send_info["send_cnt"];
		}
		if($insert_sql == "")
			$insert_sql = "INSERT INTO tbl_email_retention_stat VALUES('$yesterday', $sendidx, $eventidx, '$title', $category, $agencyidx, $target_type, '$start_eventdate', '$end_eventdate', $send_cnt, $money, $user_cnt, $nopay_return_cnt, $pay_return_cnt, ".
							"$target_email_retention_user_cnt, $target_other_retention_user_cnt, $target_email_active_user_cnt, $target_other_active_user_cnt, $non_target_email_retention_user_cnt, $non_target_other_retention_user_cnt, $non_target_email_active_user_cnt, $non_target_other_active_user_cnt) ";
		else
			$insert_sql .= ",('$yesterday', $sendidx, $eventidx, '$title', $category, $agencyidx, $target_type, '$start_eventdate', '$end_eventdate', $send_cnt, $money, $user_cnt, $nopay_return_cnt, $pay_return_cnt, ".
							"$target_email_retention_user_cnt, $target_other_retention_user_cnt, $target_email_active_user_cnt, $target_other_active_user_cnt, $non_target_email_retention_user_cnt, $non_target_other_retention_user_cnt, $non_target_email_active_user_cnt, $non_target_other_active_user_cnt) ";
	}
	
	if($insert_sql != "")
	{
		$insert_sql .= "ON DUPLICATE KEY UPDATE send_cnt = VALUES(send_cnt), money = VALUES(money), user_cnt = VALUES(user_cnt), nopay_return_cnt = VALUES(nopay_return_cnt), pay_return_cnt = VALUES(pay_return_cnt), ".
						"target_email_retention_user_cnt = VALUES(target_email_retention_user_cnt), target_other_retention_user_cnt = VALUES(target_other_retention_user_cnt), target_email_active_user_cnt = VALUES(target_email_active_user_cnt), target_other_active_user_cnt = VALUES(target_other_active_user_cnt), ".
						"non_target_email_retention_user_cnt = VALUES(non_target_email_retention_user_cnt), non_target_other_retention_user_cnt = VALUES(non_target_other_retention_user_cnt), non_target_email_active_user_cnt = VALUES(non_target_email_active_user_cnt), non_target_other_active_user_cnt = VALUES(non_target_other_active_user_cnt);";
		$db_main2->execute($insert_sql);
	}
	
	// Unsubscribe Email Update
	$sql = "SELECT email FROM tbl_user_email WHERE unsubscribe_date BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND unsubscribe = 1";
	$unsubscribe_list = $db_main2->gettotallist($sql);
	
	$insert_sql = "";
	
	for($i=0; $i<sizeof($unsubscribe_list); $i++)
	{
		$email = $unsubscribe_list[$i]["email"];
	
		if($insert_sql == "")
			$insert_sql = "INSERT INTO t5_user_exclude_email VALUES('$email')";
		else
			$insert_sql .= ",('$email')";
	}
	
	if($insert_sql != "")
		$db_redshift->execute($insert_sql);
	
	$db_main2->end();
	$db_redshift->end();
?>