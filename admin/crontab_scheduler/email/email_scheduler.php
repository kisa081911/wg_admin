<?
	include "Sendmail.php";
	include("../../common/common_include.inc.php");	
	
// 	$config=array(
// 		'host'=>'10.0.0.211',
// 		'debug'=>1,
// 		'charset'=>'utf-8',
// 		'ctype'=>'text/html'
// 	);
	
// 	$sendmail = new Sendmail($config);
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/email/email_scheduler") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/email/email_scheduler") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("email/email_scheduler Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	
	$db_main = new CDatabase_Main();
	
	$sql = "SELECT useridx, email, auth_code FROM tbl_temp_email WHERE is_auth = 0 AND is_send = 0 AND writedate >= DATE_SUB(NOW(), INTERVAL 1 MINUTE)";
	$email_list = $db_main->gettotallist($sql);
	
	for($i=0; $i<sizeof($email_list); $i++)
	{
		$email = $email_list[$i]["email"];
		$auth_code = $email_list[$i]["auth_code"];
		$useridx = $email_list[$i]["useridx"];
		
		try
		{
			// Web
			$title = "Take 5 Slots - Verify Your Email!";
			$to = $email;
			$from = "support@take5slots.com";
			
			$contents = '<table cellspacing="0" cellpadding="0" width="750" align="center" style="border-style:none;">
							<tr>
								<td style="text-align: center;">
									<table cellspacing="0" cellpadding="0" width="100%">
										<tr style="height:87px;">
											<td>
												<img style="margin-left:18px; alt="Take5Slots" src="https://d3ue0f6hyynyam.cloudfront.net/facebook/images/email/take5_logo.png"/>
											</td>
											<td style="text-align: right;">
												<span style="margin-right:40px;color:#888888;font:bold 13px Tahoma Bold;">Is this E-mail not displaying correctly? <a href="https://take5slots.com/facebook/etc/email/auth_email.php?auth_code='.$auth_code.'" target="_blank" style="color:#237bed;">View it</a> in your browser.</span><br/>
												<span style="margin-right:40px;color:#ec7831;font:bold 16px Tahoma Bold;">Please verify within 24 hours.</span>
											</td>
										</tr>
									</table>
						
									<table cellspacing="0" cellpadding="0" width="100%" style="background:url(https://d3ue0f6hyynyam.cloudfront.net/facebook/images/email/auth_bg_new.jpg);height:500px;">
										<tr>
											<td style="height:400px;" colspan="3"></td>
										</tr>
										<tr>
											<td style="width:179px;"></td>
											<td style="width:179px;height:67px;">
											  <div style="margin-left:12px;color:#fff389;font:bold 50px DroidSans Bold;">'.$auth_code.'</div>
											</td>
											<td></td>
										</tr>
										<tr>
											<td style="height:32px;" colspan="3"></td>
										</tr>
									</table>
						      
						      <table cellspacing="0" cellpadding="0" width="100%" >
										<tr>
											<td style="height:20px;text-align:center;" colspan="4">
											</td>
										</tr>
										<tr style="height:70px;text-align:center;">
											<td colspan="4">
												<div>
													<span style="color:#aeafb3;font:12px Tahoma Regular;">
															We are take5slots.com, Inc, the creator of Take5 Slots. Your ultimate Vegas experience online! You received this e-mail<br/>
															because you agree to login with your email account in Take5 Slots Mobile. If you don’t want to receive these e-mails,<br/>
															<a href="http://take5slots.com/facebook/etc/email_unsubscribe.php" target="_blank" style="color:#002aff;">Unsubscribe</a> here or contact us at our <a href="https://take5slots.com/facebook/support/appstore_support.php" target="_blank" style="color:#237bed;" >Support Center.</a> Refer to our <a href="https://take5slots.com/facebook/support/privacy_policy.php" target="_blank" style="color:#00a1e9;">Private Policy.</a><br/>
													</span>
												</div>
											</td>
										</tr>
										<tr>
											<td style="height:20px;text-align:center;" colspan="4">
											</td>
										</tr>	  		
										<tr>
											<td style="background:url(https://d3ue0f6hyynyam.cloudfront.net/facebook/images/email/auth_bg_footer.png);height:35px;"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>';			
			
			//'Auth Code : '.$auth_code;
			
			//$sendmail->send_mail($to, $from, $title, $contents);
			
			sendmail($to, $from, $title, $contents);
			
			$update_sql = "UPDATE tbl_temp_email SET is_send = 1 WHERE useridx=$useridx";
			$db_main->execute($update_sql);
			
		}
		catch(Exception $e)
		{
			write_log($e->getMessage());
		}
	}
	
	$db_main->end();
	
?>