<?
	include("../common/common_include.inc.php");
	
	$db_main = new CDatabase_Main();

	$db_main->execute("SET wait_timeout=7200");	
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	$str_useridx = 20000;	
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/game_request_group3_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
			$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
					$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);

		if (!$fp) 
		{
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/game_request_group3_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("game request group3 scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	
	try
	{
		$today = date("Y-m-d");
		
 		$sql = 	"SELECT t1.useridx AS useridx, t1.userid AS userid, t2.daylogincount, t1.logindate, DATE_FORMAT(t1.logindate, '%H:%i') AS login_hour_min, ".
 				"IF ((SELECT COUNT(*) FROM tbl_product_order WHERE useridx = t1.useridx AND STATUS = 1), 1, 0) AS pay_check ".
 				"FROM tbl_user t1 JOIN tbl_user_ext t2 ". 
 				"ON t1.useridx = t2.useridx ".
 				"WHERE t1.useridx > $str_useridx ".
 				"		AND t1.coin < 500000 ".
 				"		AND t1.logindate < DATE_SUB(NOW(), INTERVAL 2 WEEK) ".
 				"		AND DAYOFWEEK(t1.logindate) = DAYOFWEEK(NOW()) ".
 				"		AND DATE_FORMAT(t1.logindate, '%H:%i') = DATE_FORMAT(NOW(), '%H:%i') ".
 				"		AND NOT EXISTS (SELECT * FROM tbl_notification_group3_log WHERE useridx = t1.useridx) ".
 				"ORDER BY pay_check DESC, t2.daylogincount DESC;";
		$noti_user_list = $db_main->gettotallist($sql);
		
		for($i=0; $i<sizeof($noti_user_list); $i++)
		{
			$noti_user_useridx = $noti_user_list[$i]["useridx"];
			$noti_user_fbid = $noti_user_list[$i]["userid"];
			$noti_user_login_hour_min = $noti_user_list[$i]["login_hour_min"];
			
			if($noti_user_login_hour_min == date('H:i'))
			{			
				$sql = "SELECT COUNT(*) FROM tbl_notification_group3_log WHERE useridx = $noti_user_useridx";
				$isexist = $db_main->getvalue($sql);
				
				$sql = " SELECT COUNT(*) FROM tbl_notification_group3_log WHERE status > 0 AND '$today 00:00:00' <= writedate AND writedate <= '$today 23:59:59' ";
				$noti_cnt = $db_main->getvalue($sql);
				
				if($isexist == 0)
				{
					try
					{
						if($noti_cnt < 4500)
						{
							$sql = "INSERT INTO tbl_notification_group3_log(useridx, category, reward, status, writedate) VALUES($noti_user_useridx, 0, 0, 0, NOW()) ON DUPLICATE KEY UPDATE status=0;";
							$db_main->execute($sql);
							
							$sql = "SELECT LAST_INSERT_ID()";
							$notiidx = $db_main->getvalue($sql);
						}
						else
						{
							break;
						}
							
						$facebook = new Facebook(array(
								'appId'  => FACEBOOK_APP_ID,
								'secret' => FACEBOOK_SECRET_KEY,
								'cookie' => true,
						));
						
						$session = $facebook->getUser();
				
						$template="Don't miss your chance to win up to 1,000,000 Coins! Click to check now!";
						$adflag = "gamenotifygroup3";
				
						$args = array('template' => "$template",
								'href' => "?adflag=$adflag&notiidx=$notiidx",
								'ref' => "game_group3");
				
						$info = $facebook->api("/$noti_user_fbid/notifications", "POST", $args);
				
						$sql = "UPDATE tbl_notification_group3_log SET status = 1, writedate = NOW() WHERE useridx=$noti_user_useridx";
						$db_main->execute($sql);
						
						sleep(1);
					}
					catch (FacebookApiException $e)
					{
						
						$sql = "UPDATE tbl_notification_group3_log SET status = 3, writedate=NOW() WHERE useridx=$noti_user_useridx";
						$db_main->execute($sql);
					}
					
				}
			}	
		}	
	}
	catch (Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
?>