<?
	include("../common/common_include.inc.php");
	include("../common/dbconnect/db_util_redshift.inc.php");

	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/monitoring_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
		exit();
		
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_redshift = new CDatabase_Redshift();
	
	$db_main2->execute("SET wait_timeout=600");
	$db_analysis->execute("SET wait_timeout=600");
	
	//스케줄러 모니터링 알람 메일 발송	
	$sql = "SELECT COUNT( logidx ) ".
			"FROM tbl_scheduler_log ".
			"WHERE type = 101 AND status = 0 AND (now() - INTERVAL 10 MINUTE) <= writedate AND writedate <= (now() - INTERVAL 2 MINUTE) ";
	
	$oneminfail = $db_analysis->getvalue($sql);
	
	$sql = "SELECT COUNT( logidx ) ".
			"FROM tbl_scheduler_log ".
			"WHERE type = 102 AND status = 0 AND (now() - INTERVAL 10 MINUTE) <= writedate AND writedate <= (now() - INTERVAL 2 MINUTE) ";
	
	$onemineventfail = $db_analysis->getvalue($sql);
	
	$sql = "SELECT COUNT( logidx ) ".
			"FROM tbl_scheduler_log ".
			"WHERE type = 201 AND status = 0 AND (now() - INTERVAL 15 MINUTE) <= writedate AND writedate <= (now() - INTERVAL 5 MINUTE) ";
	
 	$fiveminfail = $db_analysis->getvalue($sql);
	
 	$sql = "SELECT COUNT( logidx ) ".
 			"FROM tbl_scheduler_log ".
 			"WHERE type = 301 AND status = 0 AND (now() - INTERVAL 75 MINUTE) <= writedate AND writedate <= (now() - INTERVAL 1 HOUR) ";
	
 	$onehourfail = $db_analysis->getvalue($sql);

 	$sql = "SELECT COUNT( logidx ) ".
 			"FROM tbl_scheduler_log ".
 			"WHERE type = 401 AND status = 0 AND (now() - INTERVAL 1 DAY) <= writedate AND writedate <= now() ";
 	
 	$onedayfail = $db_analysis->getvalue($sql);
	
	$to = "clickman21@doubleugames.com,minewat@doubleugames.com,kisa0819@doubleugames.com,yhjeong@doubleugames.com,seo0882@doubleugames.com";
	$from = "report@take5slots.com";
	$contents = "";
		
	if($oneminfail > 0)
	{
		$title = "[Take5 - Scheduler] ".date("Y-m-d")." 1minute Scheduler Fail";
		
		$result = array();
		exec("tail /vol/logs/LOG_".date("Y-m-d").".txt", $result);
		
		for($i=0;$i<sizeof($result);$i++)
		{
			$contents .= $result[$i]."<br/>";
		}
		
		sendmail($to, $from, $title, $contents);
	}
	
	if($onemineventfail > 0)
	{
		$title = "[Take5 - Scheduler] ".date("Y-m-d")." 1minute Event Scheduler Fail";
	
		$result = array();
		exec("tail /vol/logs/LOG_".date("Y-m-d").".txt", $result);
	
		for($i=0;$i<sizeof($result);$i++)
		{
			$contents .= $result[$i]."<br/>";
		}
	
		sendmail($to, $from, $title, $contents);
	}
	
	if($fiveminfail > 0)
	{
		$title = "[Take5 - Scheduler] ".date("Y-m-d")." 5minute Scheduler Fail";
		
		$result = array();
		exec("tail /vol/logs/LOG_".date("Y-m-d").".txt", $result);
		
		for($i=0;$i<sizeof($result);$i++)
		{
			$contents .= $result[$i]."<br/>";
		}
		
		sendmail($to, $from, $title, $contents);
	}
	
	if($onehourfail > 1)
	{
		$title = "[Take5 - Scheduler] ".date("Y-m-d")." 1hour Scheduler Fail";
		
		$result = array();
		exec("tail /vol/logs/LOG_".date("Y-m-d").".txt", $result);
		
		for($i=0;$i<sizeof($result);$i++)
		{
			$contents .= $result[$i]."<br/>";
		}
		
		sendmail($to, $from, $title, $contents);
	}
	
	if($onedayfail == 1 && date("Y-m-d H:i:s") > date("Y-m-d")." 14:00:00")
	{
		$title = "[Take5 - Scheduler] ".date("Y-m-d")." 1day Scheduler Fail";
	
		$result = array();
		exec("tail /vol/logs/LOG_".date("Y-m-d").".txt", $result);
	
		for($i=0;$i<sizeof($result);$i++)
		{
			$contents .= $result[$i]."<br/>";
		}
	
		sendmail($to, $from, $title, $contents);
	}
	
	$sql = "SELECT servername,serverip,cpu,diskuse,memtotal,memfree,vmem,established,timewait,closewait,sync_recv,UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(writedate) AS seconds FROM tbl_server_status ORDER BY servername";
	$server_list = $db_main2->gettotallist($sql);
	
	$contents = "";
	
	$title = "[Take5 - System Stat] Error";
	$to = "clickman21@doubleugames.com,minewat@doubleugames.com,kisa0819@doubleugames.com,yhjeong@doubleugames.com,seo0882@doubleugames.com";
	$from = "report@take5slots.com";
	
	for ($i=0; $i<sizeof($server_list); $i++)
	{
		$servername = $server_list[$i]["servername"];
		$serverip = $server_list[$i]["serverip"];
		$cpu = $server_list[$i]["cpu"];
		$diskuse = $server_list[$i]["diskuse"];
		$memtotal = $server_list[$i]["memtotal"];
		$memfree = $server_list[$i]["memfree"];
		$vmem = $server_list[$i]["vmem"];
		$established = $server_list[$i]["established"];
		$timewait = $server_list[$i]["timewait"];
		$closewait = $server_list[$i]["closewait"];
		$sync_recv = $server_list[$i]["sync_recv"];
		$mem_rate = round($memfree / $memtotal * 10000) / 100;
		$minutes = round($server_list[$i]["seconds"]/60);

	
		if ($diskuse >= "90")
		{
			$contents .= $serverip."($servername)"." Disk Used $diskuse%<br/>";
		}
	
		if ($cpu >= "80")
		{
			$contents .= $serverip."($servername)"." CPU Used $cpu%<br/>";
		}
	
		if ($minutes > 3)
		{
			$contents .= $serverip."($servername)"." Data Update $minutes"."minutes<br/>";
		}
		
		if ($mem_rate <= 5 && $servername == "InboxDB")
		{
			$contents .= $serverip."($servername)"." Memory Free $mem_rate"."%<br/>";
		}
		else if($mem_rate <= 7 && $servername == "FriendDB")
		{
			$contents .= $serverip."($servername)"." Memory Free $mem_rate"."%<br/>";
		}
		else if($mem_rate < 5 && $servername != "OtherDB")
                {
                        $contents .= $serverip."($servername)"." Memory Free $mem_rate"."%<br/>";
                }
		else if($mem_rate < 5)
		{
			$contents .= $serverip."($servername)"." Memory Free $mem_rate"."%<br/>";
		}
		
// 		if ($mem_rate < 5 && ($servername == "CommonServer1" || $servername == "CommonServer2" || $servername != "CommonServer3"))
// 		{
// 			$contents .= $serverip."($servername)"." Memory Free $mem_rate"."%<br/>";
// 		}
	}
	
	if($contents != "")
	{
		sendmail($to, $from, $title, $contents);
		
		$sql = "SELECT * FROM tbl_server_status";
		$server_status_list = $db_main2->gettotallist($sql);
		
		$insert_sql = "";
		
		for($i=0; $i<sizeof($server_status_list); $i++)
		{
			$serverip = $server_status_list[$i]["serverip"];
			$servername = $server_status_list[$i]["servername"];
			$diskuse = $server_status_list[$i]["diskuse"];
			$cpu = $server_status_list[$i]["cpu"];
			$memtotal = $server_status_list[$i]["memtotal"];
			$memfree = $server_status_list[$i]["memfree"];
			$vmem = $server_status_list[$i]["vmem"];
			$cached = $server_status_list[$i]["cached"];
			$buffers = $server_status_list[$i]["buffers"];
			$established = $server_status_list[$i]["established"];
			$timewait = $server_status_list[$i]["timewait"];
			$closewait = $server_status_list[$i]["closewait"];
			$sync_recv = $server_status_list[$i]["sync_recv"];
			$writedate = $server_status_list[$i]["writedate"];
		
			$insert_sql = "INSERT INTO tbl_server_status_log(serverip, servername, diskuse, cpu, memtotal, memfree, vmem, cached, buffers, established, timewait, closewait, sync_recv, writedate, is_error)".
					" VALUES('$serverip','$servername','$diskuse', '$cpu', '$memtotal', '$memfree', '$vmem', '$cached', '$buffers', '$established', '$timewait', '$closewait', '$sync_recv', '$writedate', 1)";
			$db_analysis->execute($insert_sql);
		}
	}	
	
	$yesterday = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$sql = " SELECT COUNT(*) FROM stl_load_errors WHERE strpos(filename, '/t5/') > 0 AND date(starttime) = '$yesterday' ";
	$error_cnt = $db_redshift->getvalue($sql);
	
	if($error_cnt > 0 && date("Y-m-d H:i") == date("Y-m-d")." 01:30")
	{
		$title = "[Take5 - redshift] Error";
		$to = "clickman21@doubleugames.com,minewat@doubleugames.com,seric85@afewgoodsoft.com,kisa0819@doubleugames.com,yhjeong@doubleugames.com,seo0882@doubleugames.com";
		$from = "report@take5slots.com";
	
		$contents = " redshift Data Backup Error Count $error_cnt"."<br/>";
	
		sendmail($to, $from, $title, $contents);
	
	}
	
	$db_main2->end();
	$db_analysis->end();
	$db_redshift->end();
?>
