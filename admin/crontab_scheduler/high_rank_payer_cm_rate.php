<?
	include("../common/common_include.inc.php");

	$db_slave_main = new CDatabase_Slave_Main();	
	$db_analysis = new CDatabase_Analysis();
	
	
	$db_slave_main->execute("SET wait_timeout=7200");	
	$db_analysis->execute("SET wait_timeout=7200");
	
	try 
	{
		$sql = "SELECT t1.useridx AS useridx, sum_credit, IFNULL(SUM(freecoin), 0) AS sum_freecoin ".
				"FROM ".
				"( ".	
				"	SELECT useridx, SUM(facebookcredit) AS sum_credit ". 
				"	FROM `tbl_product_order` ". 
				"	WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 4 WEEK) GROUP BY useridx ORDER BY sum_credit DESC LIMIT 200 ".
				") t1 LEFT JOIN tbl_freecoin_admin_log t2 ON t1.useridx = t2.useridx GROUP BY useridx";
		$data_list = $db_slave_main->gettotallist($sql);
		
		for($i = 0; $i < sizeof($data_list); $i++)
		{
			$useridx = $data_list[$i]["useridx"];
			$sum_credit = $data_list[$i]["sum_credit"];
			$sum_freecoin = $data_list[$i]["sum_freecoin"];
			
			$insert_sql = "INSERT INTO tbl_high_rank_200_payer(useridx, sum_facebookcredit, sum_freecoin, writedate) VALUES('$useridx', '$sum_credit', '$sum_freecoin', NOW()) ".
							"ON DUPLICATE KEY UPDATE sum_facebookcredit=VALUES(sum_facebookcredit), sum_freecoin=VALUES(sum_freecoin), writedate = NOW();";
			$db_analysis->execute($insert_sql);
		}
		
		$sql = "SELECT t1.useridx, COUNT(qaidx) AS cnt_qa ".
				"FROM tbl_high_rank_200_payer t1 LEFT JOIN `support_qa` t2 ON t1.useridx = t2.useridx WHERE t2.writedate >= DATE_SUB(NOW(), INTERVAL 4 WEEK) GROUP BY t1.useridx";
		$data_list = $db_analysis->gettotallist($sql);
		
		for($i = 0; $i < sizeof($data_list); $i++)
		{
			$useridx = $data_list[$i]["useridx"];
			$cnt_qa = $data_list[$i]["cnt_qa"];			
									
			$insert_sql = "INSERT INTO tbl_high_rank_200_payer(useridx, cnt_qa) VALUES('$useridx', '$cnt_qa') ".
							"ON DUPLICATE KEY UPDATE cnt_qa=VALUES(cnt_qa);";
			$db_analysis->execute($insert_sql);
		}
		
		
	}
	catch(Exception $e)
	{
		
	}
	
	$db_slave_main->end();
	$db_analysis->end();
?>