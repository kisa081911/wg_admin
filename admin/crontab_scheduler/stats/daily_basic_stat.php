<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/stats/daily_basic_stat") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/stats/daily_basic_stat") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("stats/daily_basic_stat Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	
	$db_slave = new CDatabase_Slave_Main();
	$db2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_other = new CDatabase_Other();
	$db_game = new CDatabase_Game();
	$db_redshift = new CDatabase_Redshift();
	
	$today = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$current_date = date("Y-m-d");
	// 1.매출
	// 최근 52주
	$sql = "SELECT ROUND(AVG(money), 2) ".
			" FROM ( ".
				" SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, ROUND(SUM(facebookcredit)/10, 2) AS money ".
				" FROM tbl_product_order ".
				" WHERE useridx > 20000 AND STATUS = 1 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
			" ) t1 ";
	$pay_web_52week = $db_slave->getvalue($sql);
	
	$sql = "SELECT os_type, IFNULL(ROUND(AVG(money), 2),0) as total_amount ".
			" FROM ( ".
				" SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, os_type, SUM(money) AS money ".
				" FROM tbl_product_order_mobile ".
				" WHERE useridx > 20000 AND STATUS = 1 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), os_type ".
			" ) t1 ".
			" GROUP BY os_type ";
	$pay_data_52week = $db_slave->gettotallist($sql);
	
	$pay_ios_52week = $pay_data_52week[0]["total_amount"];
	$pay_and_52week = $pay_data_52week[1]["total_amount"];
	$pay_ama_52week = $pay_data_52week[2]["total_amount"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 1, 1, $pay_web_52week, $pay_ios_52week, $pay_and_52week, $pay_ama_52week) ". 
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	$sql = "SELECT ROUND(AVG(money), 2) ".
			" FROM ( ".
				" SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, ROUND(SUM(facebookcredit)/10, 2) AS money ".
				" FROM tbl_product_order ".
				" WHERE useridx > 20000 AND STATUS = 1 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
			" ) t1 ";
	$pay_web_13week = $db_slave->getvalue($sql);
	
	$sql = "SELECT os_type, IFNULL(ROUND(AVG(money), 2),0) as total_amount ".
			" FROM ( ".
				" SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, os_type, SUM(money) AS money ".
				" FROM tbl_product_order_mobile ".
				" WHERE useridx > 20000 AND STATUS = 1 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), os_type ".
			" ) t1 ".
			" GROUP BY os_type ";
	$pay_data_13week = $db_slave->gettotallist($sql);
	
	$pay_ios_13week = $pay_data_13week[0]["total_amount"];
	$pay_and_13week = $pay_data_13week[1]["total_amount"];
	$pay_ama_13week = $pay_data_13week[2]["total_amount"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 1, 2, $pay_web_13week, $pay_ios_13week, $pay_and_13week, $pay_ama_13week) ". 
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주
	$sql = "SELECT ROUND(AVG(money), 2) ".
			" FROM ( ".
				" SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, ROUND(SUM(facebookcredit)/10, 2) AS money ".
				" FROM tbl_product_order ".
				" WHERE useridx > 20000 AND STATUS = 1 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
			" ) t1 ";
	$pay_web_4week = $db_slave->getvalue($sql);
	
	$sql = "SELECT os_type, IFNULL(ROUND(AVG(money), 2),0) as total_amount ".
			" FROM ( ".
				" SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, os_type, SUM(money) AS money ".
				" FROM tbl_product_order_mobile ".
				" WHERE useridx > 20000 AND STATUS = 1 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), os_type ".
			" ) t1 ".
			" GROUP BY os_type ";
	$pay_data_4week = $db_slave->gettotallist($sql);
	
	$pay_ios_4week = $pay_data_4week[0]["total_amount"];
	$pay_and_4week = $pay_data_4week[1]["total_amount"];
	$pay_ama_4week = $pay_data_4week[2]["total_amount"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 1, 3, $pay_web_4week, $pay_ios_4week, $pay_and_4week, $pay_ama_4week) ". 
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 1주
	$sql = "SELECT ROUND(AVG(money), 2) ".
			" FROM ( ".
				" SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, ROUND(SUM(facebookcredit)/10, 2) AS money ".
				" FROM tbl_product_order ".
				" WHERE useridx > 20000 AND STATUS = 1 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
			" ) t1 ";
	$pay_web_1week = $db_slave->getvalue($sql);
	
	$sql = "SELECT os_type, IFNULL(ROUND(AVG(money), 2),0) as total_amount ".
			" FROM ( ".
				" SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, os_type, SUM(money) AS money ".
				" FROM tbl_product_order_mobile ".
				" WHERE useridx > 20000 AND STATUS = 1 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), os_type ".
			" ) t1 ".
			" GROUP BY os_type ";
	$pay_data_1week = $db_slave->gettotallist($sql);
	
	$pay_ios_1week = $pay_data_1week[0]["total_amount"];
	$pay_and_1week = $pay_data_1week[1]["total_amount"];
	$pay_ama_1week = $pay_data_1week[2]["total_amount"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 1, 4, $pay_web_1week, $pay_ios_1week, $pay_and_1week, $pay_ama_1week) ". 
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 어제
	$sql = "SELECT ROUND(AVG(money), 2) ".
			" FROM ( ".
				" SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, ROUND(SUM(facebookcredit)/10, 2) AS money ".
				" FROM tbl_product_order ".
				" WHERE useridx > 20000 AND STATUS = 1 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
			" ) t1 ";
				
	$pay_web_1day = $db_slave->getvalue($sql);
	$pay_web_1day = ($pay_web_1day=="")?0:$pay_web_1day;
	
	$sql = "SELECT os_type, ROUND(AVG(money), 2) as total_amount ".
			" FROM ( ".
				" SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, os_type, SUM(money) AS money ".
				" FROM tbl_product_order_mobile ".
				" WHERE useridx > 20000 AND STATUS = 1 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), os_type ".
			" ) t1 ".
			" GROUP BY os_type ";
	
	$pay_data_1day = $db_slave->gettotallist($sql);
	
	$pay_ios_1day = ($pay_data_1day[0]["total_amount"]=="")?0:$pay_data_1day[0]["total_amount"];
	$pay_and_1day = ($pay_data_1day[1]["total_amount"]=="")?0:$pay_data_1day[1]["total_amount"];
	$pay_ama_1day = ($pay_data_1day[2]["total_amount"]=="")?0:$pay_data_1day[2]["total_amount"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 1, 5, $pay_web_1day, $pay_ios_1day, $pay_and_1day, $pay_ama_1day) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	$sql = "SELECT ROUND(AVG(money), 2) ". 
			" FROM (  ".
				" SELECT ROUND(SUM(facebookcredit)/10, 2) AS money ". 
				" FROM tbl_product_order  ".
				" WHERE useridx > 20000 AND STATUS = 1 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d 23:59:59') ". 
				" AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
			" ) t1 ";
				
	$pay_web_dayweek = $db_slave->getvalue($sql);
	
	$sql = "SELECT os_type, ROUND(AVG(money), 2) AS total_amount ".
			" FROM (  ".
				" SELECT os_type, SUM(money) AS money ". 
				" FROM tbl_product_order_mobile  ".
				" WHERE useridx > 20000 AND STATUS = 1 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d 23:59:59') ". 
				" AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), os_type ".
			" ) t1 GROUP BY os_type ";
	
	$pay_data_dayweek = $db_slave->gettotallist($sql);
	
	$pay_ios_dayweek = $pay_data_dayweek[0]["total_amount"];
	$pay_and_dayweek = $pay_data_dayweek[1]["total_amount"];
	$pay_ama_dayweek= $pay_data_dayweek[2]["total_amount"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 1, 6, $pay_web_dayweek, $pay_ios_dayweek, $pay_and_dayweek, $pay_ama_dayweek) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 2.할인율
	// 최근 52주
	$sql = "SELECT ROUND(((SUM(coin)/SUM(basecoin))-1)*100, 2) AS salerate ".
			" FROM tbl_product_order ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND useridx > 20000 AND STATUS = 1";
	$salerate_web_52week = $db_slave->getvalue($sql);
	
	$sql = "SELECT os_type, ROUND(((SUM(coin)/SUM(basecoin))-1)*100, 2) AS salerate ".
			" FROM tbl_product_order_mobile ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND useridx > 20000 AND STATUS = 1 GROUP BY os_type ";
	$salerate_mobile_52week_array = $db_slave->gettotallist($sql);
	
	$salerate_ios_52week = 0;
	$salerate_android_52week = 0;
	$salerate_amazon_52week = 0;
	foreach ($salerate_mobile_52week_array as $salerate_mobile_52weekObj)
	{
		$os_type = $salerate_mobile_52weekObj['os_type'];
		if($os_type =='1')
			$salerate_ios_52week = $salerate_mobile_52weekObj['salerate'];
		else if ($os_type =='2')
			$salerate_android_52week = $salerate_mobile_52weekObj['salerate'];
		else if ($os_type =='3')
			$salerate_amazon_52week = $salerate_mobile_52weekObj['salerate'];
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 2, 1, $salerate_web_52week, $salerate_ios_52week, $salerate_android_52week, $salerate_amazon_52week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	$sql = "SELECT ROUND(((SUM(coin)/SUM(basecoin))-1)*100, 2) AS salerate ".
			" FROM tbl_product_order ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND useridx > 20000 AND STATUS = 1";
	$salerate_web_13week = $db_slave->getvalue($sql);
	
	$sql = "SELECT os_type, ROUND(((SUM(coin)/SUM(basecoin))-1)*100, 2) AS salerate ".
			" FROM tbl_product_order_mobile ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND useridx > 20000 AND STATUS = 1 GROUP BY os_type";
	$salerate_mobile_13week_array = $db_slave->gettotallist($sql);
	
	$salerate_ios_13week = 0;
	$salerate_android_13week = 0;
	$salerate_amazon_13week = 0;
	foreach ($salerate_mobile_13week_array as $salerate_mobile_13weekObj)
	{
		$os_type = $salerate_mobile_13weekObj['os_type'];
		if($os_type =='1')
			$salerate_ios_13week = $salerate_mobile_13weekObj['salerate'];
		else if ($os_type =='2')
			$salerate_android_13week = $salerate_mobile_13weekObj['salerate'];
		else if ($os_type =='3')
			$salerate_amazon_13week = $salerate_mobile_13weekObj['salerate'];
	}

	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 2, 2, $salerate_web_13week, $salerate_ios_13week, $salerate_android_13week, $salerate_amazon_13week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	// 최근 4주
	$sql = "SELECT ROUND(((SUM(coin)/SUM(basecoin))-1)*100, 2) AS salerate ".
			" FROM tbl_product_order ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND useridx > 20000 AND STATUS = 1";
	$salerate_web_4week = $db_slave->getvalue($sql);
	
	$sql = "SELECT os_type, ROUND(((SUM(coin)/SUM(basecoin))-1)*100, 2) AS salerate ".
			" FROM tbl_product_order_mobile ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND useridx > 20000 AND STATUS = 1 GROUP BY os_type";
	$salerate_mobile_4week_array = $db_slave->gettotallist($sql);
	
	$salerate_ios_4week = 0;
	$salerate_android_4week = 0;
	$salerate_amazon_4week = 0;
	foreach ($salerate_mobile_4week_array as $salerate_mobile_4weekObj)
	{
		$os_type = $salerate_mobile_4weekObj['os_type'];
		if($os_type =='1')
			$salerate_ios_4week = $salerate_mobile_4weekObj['salerate'];
		else if ($os_type =='2')
			$salerate_android_4week = $salerate_mobile_4weekObj['salerate'];
		else if ($os_type =='3')
			$salerate_amazon_4week = $salerate_mobile_4weekObj['salerate'];
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 2, 3, $salerate_web_4week, $salerate_ios_4week, $salerate_android_4week, $salerate_amazon_4week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 1주
	$sql = "SELECT ROUND(((SUM(coin)/SUM(basecoin))-1)*100, 2) AS salerate ".
			" FROM tbl_product_order ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND useridx > 20000 AND STATUS = 1";
	$salerate_web_1week = $db_slave->getvalue($sql);
	
	$sql = "SELECT os_type, ROUND(((SUM(coin)/SUM(basecoin))-1)*100, 2) AS salerate ".
			" FROM tbl_product_order_mobile ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND useridx > 20000 AND STATUS = 1 GROUP BY os_type";
	$salerate_mobile_1week_array = $db_slave->gettotallist($sql);
	
	$salerate_ios_1week = 0;
	$salerate_android_1week = 0;
	$salerate_amazon_1week = 0;
	foreach ($salerate_mobile_1week_array as $salerate_mobile_1weekObj)
	{
		$os_type = $salerate_mobile_1weekObj['os_type'];
		if($os_type =='1')
			$salerate_ios_1week = $salerate_mobile_1weekObj['salerate'];
		else if ($os_type =='2')
			$salerate_android_1week = $salerate_mobile_1weekObj['salerate'];
		else if ($os_type =='3')
			$salerate_amazon_1week = $salerate_mobile_1weekObj['salerate'];
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 2, 4, $salerate_web_1week, $salerate_ios_1week, $salerate_android_1week, $salerate_amazon_1week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 어제
	$sql = "SELECT ROUND(((SUM(coin)/SUM(basecoin))-1)*100, 2) AS salerate ".
			" FROM tbl_product_order ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND useridx > 20000 AND STATUS = 1";
	$salerate_web_day = $db_slave->getvalue($sql);
	$salerate_web_day = ($salerate_web_day=="")?0:$salerate_web_day;
	
	$sql = "SELECT os_type, ROUND(((SUM(coin)/SUM(basecoin))-1)*100, 2) AS salerate ".
			" FROM tbl_product_order_mobile ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') AND useridx > 20000 AND STATUS = 1 ". 
			" GROUP BY os_type ";
	$salerate_mobile_day_array = $db_slave->gettotallist($sql);
	
	$salerate_ios_day = 0;
	$salerate_android_day = 0;
	$salerate_amazon_day = 0;
	foreach ($salerate_mobile_day_array as $salerate_mobile_dayObj)
	{
		$os_type = $salerate_mobile_dayObj['os_type'];
		if($os_type =='1')
			$salerate_ios_day = $salerate_mobile_dayObj['salerate'];
		else if ($os_type =='2')
			$salerate_android_day = $salerate_mobile_dayObj['salerate'];
		else if ($os_type =='3')
			$salerate_amazon_day = $salerate_mobile_dayObj['salerate'];
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 2, 5, $salerate_web_day, $salerate_ios_day, $salerate_android_day, $salerate_amazon_day) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일통계
	$sql = "SELECT ROUND(((SUM(coin)/SUM(basecoin))-1)*100, 2) AS salerate ". 
			" FROM tbl_product_order  ".
			" WHERE useridx > 20000 AND STATUS = 1 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d 23:59:59') AND STATUS = 1 ".
			" AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)";
	$salerate_web_dayweek = $db_slave->getvalue($sql);
	
	$sql = "SELECT os_type, ROUND(((SUM(coin)/SUM(basecoin))-1)*100, 2) AS salerate ". 
			" FROM tbl_product_order_mobile ".
			" WHERE useridx > 20000 AND STATUS = 1 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d 23:59:59') and STATUS = 1 ".
			" AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
			" GROUP BY os_type";
	$salerate_mobile_dayweek_array = $db_slave->gettotallist($sql);
	
	$salerate_ios_dayweek = 0;
	$salerate_android_dayweek = 0;
	$salerate_amazon_dayweek = 0;
	foreach ($salerate_mobile_dayweek_array as $salerate_mobile_dayObj)
	{
		$os_type = $salerate_mobile_dayObj['os_type'];
		if($os_type =='1')
			$salerate_ios_dayweek = $salerate_mobile_dayObj['salerate'];
		else if ($os_type =='2')
			$salerate_android_dayweek = $salerate_mobile_dayObj['salerate'];
		else if ($os_type =='3')
			$salerate_amazon_dayweek = $salerate_mobile_dayObj['salerate'];
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 2, 6, $salerate_web_dayweek, $salerate_ios_dayweek, $salerate_android_dayweek, $salerate_amazon_dayweek) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 3.가입자수
	// 최근 52주
	$sql = "SELECT platform, ROUND(AVG(join_count)) as reg_count ".
			" FROM ( ".
				" SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS today, platform, COUNT(*) AS join_count ".
				" FROM tbl_user_ext ".
				" WHERE useridx > 20000 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d 00:00:00') <= createdate AND createdate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(createdate, '%Y-%m-%d'), platform ".
			" ) t1 ".
			" GROUP BY platform ";
			
	$join_web_52week_list = $db_slave->gettotallist($sql);

	$join_web_52week = 0;
	$join_ios_52week = 0;
	$join_and_52week = 0;
	$join_ama_52week = 0;
	
	foreach ($join_web_52week_list as $join_web_52weekObj)
	{
		$platform = $join_web_52weekObj['platform'];
		
		if($platform ==0)
			$join_web_52week = $join_web_52weekObj["reg_count"];
		else if($platform ==1)
			$join_ios_52week = $join_web_52weekObj["reg_count"];
		else if($platform ==2)
			$join_and_52week = $join_web_52weekObj["reg_count"];
		else if($platform ==3)
			$join_ama_52week = $join_web_52weekObj["reg_count"];
	}
	
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 3, 1, $join_web_52week, $join_ios_52week, $join_and_52week, $join_ama_52week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	$sql = "SELECT platform, ROUND(AVG(join_count)) as reg_count ".
			" FROM ( ".
				" SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS today, platform, COUNT(*) AS join_count ".
				" FROM tbl_user_ext ".
				" WHERE useridx > 20000 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d 00:00:00') <= createdate AND createdate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(createdate, '%Y-%m-%d'), platform ".
			" ) t1 ".
			" GROUP BY platform ";
			
	$join_web_13week_list = $db_slave->gettotallist($sql);
	
	$join_web_13week = 0;
	$join_ios_13week = 0;
	$join_and_13week = 0;
	$join_ama_13week = 0;
	
	foreach ($join_web_13week_list as $join_web_13weekObj)
	{
		$platform = $join_web_13weekObj['platform'];
		
		if($platform ==0)
			$join_web_13week = $join_web_13weekObj["reg_count"];
		else if($platform ==1)
			$join_ios_13week = $join_web_13weekObj["reg_count"];
		else if($platform ==2)
			$join_and_13week = $join_web_13weekObj["reg_count"];
		else if($platform ==3)
			$join_ama_13week = $join_web_13weekObj["reg_count"];
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 3, 2, $join_web_13week, $join_ios_13week, $join_and_13week, $join_ama_13week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주
	$sql = "SELECT platform, ROUND(AVG(join_count)) as reg_count ".
			" FROM ( ".
				" SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS today, platform, COUNT(*) AS join_count ".
				" FROM tbl_user_ext ".
				" WHERE useridx > 20000 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d 00:00:00') <= createdate AND createdate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(createdate, '%Y-%m-%d'), platform ".
			" ) t1 ".
			" GROUP BY platform ";
			
	$join_web_4week_list = $db_slave->gettotallist($sql);
	
	$join_web_4week = 0;
	$join_ios_4week = 0;
	$join_and_4week = 0;
	$join_ama_4week = 0;
	
	foreach ($join_web_4week_list as $join_web_4weekObj)
	{
		$platform = $join_web_4weekObj['platform'];
		
		if($platform ==0)
			$join_web_4week = $join_web_4weekObj["reg_count"];
		else if($platform ==1)
			$join_ios_4week = $join_web_4weekObj["reg_count"];
		else if($platform ==2)
			$join_and_4week = $join_web_4weekObj["reg_count"];
		else if($platform ==3)
			$join_ama_4week = $join_web_4weekObj["reg_count"];
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 3, 3, $join_web_4week, $join_ios_4week, $join_and_4week, $join_ama_4week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 1주
	$sql = "SELECT platform, ROUND(AVG(join_count)) as reg_count ".
			" FROM ( ".
				" SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS today, platform, COUNT(*) AS join_count ".
				" FROM tbl_user_ext ".
				" WHERE useridx > 20000 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d 00:00:00') <= createdate AND createdate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(createdate, '%Y-%m-%d'), platform ".
			" ) t1 ".
			" GROUP BY platform ";
			
	$join_web_1week_list = $db_slave->gettotallist($sql);
	
	$join_web_1week = 0;
	$join_ios_1week = 0;
	$join_and_1week = 0;
	$join_ama_1week = 0;
	
	foreach ($join_web_1week_list as $join_web_1weekObj)
	{
		$platform = $join_web_1weekObj['platform'];
		
		if($platform ==0)
			$join_web_1week = $join_web_1weekObj["reg_count"];
		else if($platform ==1)
			$join_ios_1week = $join_web_1weekObj["reg_count"];
		else if($platform ==2)
			$join_and_1week = $join_web_1weekObj["reg_count"];
		else if($platform ==3)
			$join_ama_1week = $join_web_1weekObj["reg_count"];
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 3, 4, $join_web_1week, $join_ios_1week, $join_and_1week, $join_ama_1week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 어제
	$sql = "SELECT platform, ROUND(AVG(join_count)) as reg_count ".
			" FROM ( ".
				" SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS today, platform, COUNT(*) AS join_count ".
				" FROM tbl_user_ext ".
				" WHERE useridx > 20000 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d 00:00:00') <= createdate AND createdate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(createdate, '%Y-%m-%d'), platform ".
			" ) t1 ".
			" GROUP BY platform ";
			
	$join_web_day_list = $db_slave->gettotallist($sql);
	
	$join_web_day = 0;
	$join_ios_day = 0;
	$join_and_day = 0;
	$join_ama_day = 0;
	
	foreach ($join_web_day_list as $join_web_dayObj)
	{
		$platform = $join_web_dayObj['platform'];
		
		if($platform ==0)
			$join_web_day = $join_web_dayObj["reg_count"];
		else if($platform ==1)
			$join_ios_day = $join_web_dayObj["reg_count"];
		else if($platform ==2)
			$join_and_day = $join_web_dayObj["reg_count"];
		else if($platform ==3)
			$join_ama_day = $join_web_dayObj["reg_count"];
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 3, 5, $join_web_day, $join_ios_day, $join_and_day, $join_ama_day) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	$sql = "SELECT platform, ROUND(AVG(join_count)) AS reg_count ". 
			" FROM ( ".
				" SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS today, platform, COUNT(*) AS join_count ". 
				" FROM tbl_user_ext  ".
				" WHERE useridx > 20000 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d 00:00:00') <= createdate AND createdate < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d 23:59:59') ".
				" AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(createdate) ".
				" GROUP BY DATE_FORMAT(createdate, '%Y-%m-%d'), platform  ".
			 " ) t1  ".
			 " GROUP BY platform ";
			
	$join_web_dayweek_list = $db_slave->gettotallist($sql);
	
	$join_web_dayweek = 0;
	$join_ios_dayweek = 0;
	$join_and_dayweek = 0;
	$join_ama_dayweek = 0;
	
	foreach ($join_web_dayweek_list as $join_web_dayweekObj)
	{
		$platform = $join_web_dayweekObj['platform'];
		
		if($platform ==0)
			$join_web_dayweek = $join_web_dayweekObj["reg_count"];
		else if($platform ==1)
			$join_ios_dayweek = $join_web_dayweekObj["reg_count"];
		else if($platform ==2)
			$join_and_dayweek = $join_web_dayweekObj["reg_count"];
		else if($platform ==3)
			$join_ama_dayweek = $join_web_dayweekObj["reg_count"];
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 3, 6, $join_web_dayweek, $join_ios_dayweek, $join_and_dayweek, $join_ama_dayweek) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 4.DAU
	// 최근 52주
	$sql = "SELECT ROUND(AVG(todayfacebookactivecount)) AS web, ROUND(AVG(todayiosactivecount)) AS ios, ROUND(AVG(todayandroidactivecount)) AS android, ROUND(AVG(todayamazonactivecount)) AS amazon ".
			" FROM `user_join_log` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$dau_data_52week = $db_analysis->getarray($sql);
	
	$dau_web_52week = ($dau_data_52week["web"]=='')?0:$dau_data_52week["web"];
	$dau_ios_52week = ($dau_data_52week["ios"]=='')?0:$dau_data_52week["ios"];
	$dau_and_52week = ($dau_data_52week["android"]=='')?0:$dau_data_52week["android"];
	$dau_ama_52week = ($dau_data_52week["amazon"]=='')?0:$dau_data_52week["amazon"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 4, 1, $dau_web_52week, $dau_ios_52week, $dau_and_52week, $dau_ama_52week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	$sql = "SELECT ROUND(AVG(todayfacebookactivecount)) AS web, ROUND(AVG(todayiosactivecount)) AS ios, ROUND(AVG(todayandroidactivecount)) AS android, ROUND(AVG(todayamazonactivecount)) AS amazon ".
			" FROM `user_join_log` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$dau_data_13week = $db_analysis->getarray($sql);
	
	$dau_web_13week = ($dau_data_13week["web"]=='')?0:$dau_data_13week["web"];
	$dau_ios_13week = ($dau_data_13week["ios"]=='')?0:$dau_data_13week["ios"];
	$dau_and_13week = ($dau_data_13week["android"]=='')?0:$dau_data_13week["android"];
	$dau_ama_13week = ($dau_data_13week["amazon"]=='')?0:$dau_data_13week["amazon"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 4, 2, $dau_web_13week, $dau_ios_13week, $dau_and_13week, $dau_ama_13week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주
	$sql = "SELECT ROUND(AVG(todayfacebookactivecount)) AS web, ROUND(AVG(todayiosactivecount)) AS ios, ROUND(AVG(todayandroidactivecount)) AS android, ROUND(AVG(todayamazonactivecount)) AS amazon ".
			" FROM `user_join_log` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$dau_data_4week = $db_analysis->getarray($sql);
	
	$dau_web_4week = ($dau_data_4week["web"]=='')?0:$dau_data_4week["web"];
	$dau_ios_4week = ($dau_data_4week["ios"]=='')?0:$dau_data_4week["ios"];
	$dau_and_4week = ($dau_data_4week["android"]=='')?0:$dau_data_4week["android"];
	$dau_ama_4week = ($dau_data_4week["amazon"]=='')?0:$dau_data_4week["amazon"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 4, 3, $dau_web_4week, $dau_ios_4week, $dau_and_4week, $dau_ama_4week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 1주
	$sql = "SELECT ROUND(AVG(todayfacebookactivecount)) AS web, ROUND(AVG(todayiosactivecount)) AS ios, ROUND(AVG(todayandroidactivecount)) AS android, ROUND(AVG(todayamazonactivecount)) AS amazon ".
			" FROM `user_join_log` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$dau_data_1week = $db_analysis->getarray($sql);
	
	$dau_web_1week = ($dau_data_1week["web"]=='')?0:$dau_data_1week["web"];
	$dau_ios_1week = ($dau_data_1week["ios"]=='')?0:$dau_data_1week["ios"];
	$dau_and_1week = ($dau_data_1week["android"]=='')?0:$dau_data_1week["android"];
	$dau_ama_1week = ($dau_data_1week["amazon"]=='')?0:$dau_data_1week["amazon"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 4, 4, $dau_web_1week, $dau_ios_1week, $dau_and_1week, $dau_ama_1week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 어제
	$sql = "SELECT ROUND(AVG(todayfacebookactivecount)) AS web, ROUND(AVG(todayiosactivecount)) AS ios, ROUND(AVG(todayandroidactivecount)) AS android, ROUND(AVG(todayamazonactivecount)) AS amazon ".
			" FROM `user_join_log` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$dau_data_day = $db_analysis->getarray($sql);
	
	$dau_web_day = ($dau_data_day["web"]=='')?0:$dau_data_day["web"];
	$dau_ios_day = ($dau_data_day["ios"]=='')?0:$dau_data_day["ios"];
	$dau_and_day = ($dau_data_day["android"]=='')?0:$dau_data_day["android"];
	$dau_ama_day = ($dau_data_day["amazon"]=='')?0:$dau_data_day["amazon"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 4, 5, $dau_web_day, $dau_ios_day, $dau_and_day, $dau_ama_day) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	$sql = "SELECT ROUND(AVG(todayfacebookactivecount)) AS web, ROUND(AVG(todayiosactivecount)) AS ios, ROUND(AVG(todayandroidactivecount)) AS android, ROUND(AVG(todayamazonactivecount)) AS amazon ". 
			 " FROM `user_join_log` ".
			 " WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
			 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
	$dau_data_dayweek = $db_analysis->getarray($sql);
	
	$dau_web_dayweek = ($dau_data_dayweek["web"]=='')?0:$dau_data_dayweek["web"];
	$dau_ios_dayweek = ($dau_data_dayweek["ios"]=='')?0:$dau_data_dayweek["ios"];
	$dau_and_dayweek = ($dau_data_dayweek["android"]=='')?0:$dau_data_dayweek["android"];
	$dau_ama_dayweek = ($dau_data_dayweek["amazon"]=='')?0:$dau_data_dayweek["amazon"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 4, 6, $dau_web_dayweek, $dau_ios_dayweek, $dau_and_dayweek, $dau_ama_dayweek) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 5.ARPDAU
	// 최근 52주
	$arpdau_web_52week = ($dau_web_52week == 0) ? 0 : round($pay_web_52week/$dau_web_52week, 2);
	
	$sql = "SELECT ROUND((SUM(guest_ios_money)+SUM(ios_money))/(SUM(guest_ios_logincount)+SUM(ios_logincount)), 2) AS ios_arpu ".
			" , ROUND((SUM(guest_and_money)+SUM(and_money))/(SUM(guest_and_logincount)+SUM(and_logincount)), 2) AS and_arpu ".
			" , ROUND((SUM(guest_ama_money)+SUM(ama_money))/(SUM(guest_ama_logincount)+SUM(ama_logincount)), 2) AS ama_arpu ".
			" FROM `tbl_user_guest_stat_daily` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') AND today >= '2016-11-14'"; 
	$arpdau_data_52week = $db_other->getarray($sql);
	
	$arpdau_ios_52week = ($arpdau_data_52week["ios_arpu"]=='')?0:$arpdau_data_52week["ios_arpu"];
	$arpdau_and_52week = ($arpdau_data_52week["and_arpu"]=='')?0:$arpdau_data_52week["and_arpu"];
	$arpdau_ama_52week = ($arpdau_data_52week["ama_arpu"]=='')?0:$arpdau_data_52week["ama_arpu"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 5, 1, $arpdau_web_52week, $arpdau_ios_52week, $arpdau_and_52week, $arpdau_ama_52week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	$arpdau_web_13week = ($dau_web_13week == 0) ? 0 : round($pay_web_13week/$dau_web_13week, 2);
	
	$sql = "SELECT ROUND((SUM(guest_ios_money)+SUM(ios_money))/(SUM(guest_ios_logincount)+SUM(ios_logincount)), 2) AS ios_arpu ".
			" , ROUND((SUM(guest_and_money)+SUM(and_money))/(SUM(guest_and_logincount)+SUM(and_logincount)), 2) AS and_arpu ".
			" , ROUND((SUM(guest_ama_money)+SUM(ama_money))/(SUM(guest_ama_logincount)+SUM(ama_logincount)), 2) AS ama_arpu ".
			" FROM `tbl_user_guest_stat_daily` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') AND today >= '2016-11-14'"; 
	$arpdau_data_13week = $db_other->getarray($sql);
	
	$arpdau_ios_13week = ($arpdau_data_13week["ios_arpu"]=='')?0:$arpdau_data_13week["ios_arpu"];
	$arpdau_and_13week = ($arpdau_data_13week["and_arpu"]=='')?0:$arpdau_data_13week["and_arpu"];
	$arpdau_ama_13week = ($arpdau_data_13week["ama_arpu"]=='')?0:$arpdau_data_13week["ama_arpu"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 5, 2, $arpdau_web_13week, $arpdau_ios_13week, $arpdau_and_13week, $arpdau_ama_13week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주
	$arpdau_web_4week = ($dau_web_4week == 0) ? 0 : round($pay_web_4week/$dau_web_4week, 2);
	
	$sql = "SELECT ROUND((SUM(guest_ios_money)+SUM(ios_money))/(SUM(guest_ios_logincount)+SUM(ios_logincount)), 2) AS ios_arpu ".
			" , ROUND((SUM(guest_and_money)+SUM(and_money))/(SUM(guest_and_logincount)+SUM(and_logincount)), 2) AS and_arpu ".
			" , ROUND((SUM(guest_ama_money)+SUM(ama_money))/(SUM(guest_ama_logincount)+SUM(ama_logincount)), 2) AS ama_arpu ".
			" FROM `tbl_user_guest_stat_daily` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') AND today >= '2016-11-14'"; 
	$arpdau_data_4week = $db_other->getarray($sql);
	
	$arpdau_ios_4week = ($arpdau_data_4week["ios_arpu"]=='')?0:$arpdau_data_4week["ios_arpu"];
	$arpdau_and_4week = ($arpdau_data_4week["and_arpu"]=='')?0:$arpdau_data_4week["and_arpu"];
	$arpdau_ama_4week = ($arpdau_data_4week["ama_arpu"]=='')?0:$arpdau_data_4week["ama_arpu"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 5, 3, $arpdau_web_4week, $arpdau_ios_4week, $arpdau_and_4week, $arpdau_ama_4week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 1주
	$arpdau_web_1week = ($dau_web_1week == 0) ? 0 : round($pay_web_1week/$dau_web_1week, 2);
	
	$sql = "SELECT ROUND((SUM(guest_ios_money)+SUM(ios_money))/(SUM(guest_ios_logincount)+SUM(ios_logincount)), 2) AS ios_arpu ".
			" , ROUND((SUM(guest_and_money)+SUM(and_money))/(SUM(guest_and_logincount)+SUM(and_logincount)), 2) AS and_arpu ".
			" , ROUND((SUM(guest_ama_money)+SUM(ama_money))/(SUM(guest_ama_logincount)+SUM(ama_logincount)), 2) AS ama_arpu ".
			" FROM `tbl_user_guest_stat_daily` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') AND today >= '2016-11-14'"; 
	$arpdau_data_1week = $db_other->getarray($sql);
	
	$arpdau_ios_1week = ($arpdau_data_1week["ios_arpu"]=='')?0:$arpdau_data_1week["ios_arpu"];
	$arpdau_and_1week = ($arpdau_data_1week["and_arpu"]=='')?0:$arpdau_data_1week["and_arpu"];
	$arpdau_ama_1week = ($arpdau_data_1week["ama_arpu"]=='')?0:$arpdau_data_1week["ama_arpu"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 5, 4, $arpdau_web_1week, $arpdau_ios_1week, $arpdau_and_1week, $arpdau_ama_1week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 어제
	$arpdau_web_day = ($dau_web_day == 0) ? 0 : round($pay_web_1day/$dau_web_day, 2);
	
	$sql = "SELECT ROUND((SUM(guest_ios_money)+SUM(ios_money))/(SUM(guest_ios_logincount)+SUM(ios_logincount)), 2) AS ios_arpu ".
			" , ROUND((SUM(guest_and_money)+SUM(and_money))/(SUM(guest_and_logincount)+SUM(and_logincount)), 2) AS and_arpu ".
			" , ROUND((SUM(guest_ama_money)+SUM(ama_money))/(SUM(guest_ama_logincount)+SUM(ama_logincount)), 2) AS ama_arpu ".
			" FROM `tbl_user_guest_stat_daily` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') AND today >= '2016-11-14'"; 
	$arpdau_data_day = $db_other->getarray($sql);
	
	$arpdau_ios_day = ($arpdau_data_day["ios_arpu"]=='')?0:$arpdau_data_day["ios_arpu"];
	$arpdau_and_day = ($arpdau_data_day["and_arpu"]=='')?0:$arpdau_data_day["and_arpu"];
	$arpdau_ama_day = ($arpdau_data_day["ama_arpu"]=='')?0:$arpdau_data_day["ama_arpu"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 5, 5, $arpdau_web_day, $arpdau_ios_day, $arpdau_and_day, $arpdau_ama_day) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	$arpdau_web_dayweek = ($dau_web_dayweek == 0) ? 0 : round($pay_web_dayweek/$dau_web_dayweek, 2);
	
	$sql = " SELECT ROUND((SUM(guest_ios_money)+SUM(ios_money))/(SUM(guest_ios_logincount)+SUM(ios_logincount)), 2) AS ios_arpu ". 
			 " , ROUND((SUM(guest_and_money)+SUM(and_money))/(SUM(guest_and_logincount)+SUM(and_logincount)), 2) AS and_arpu ".
			 " , ROUND((SUM(guest_ama_money)+SUM(ama_money))/(SUM(guest_ama_logincount)+SUM(ama_logincount)), 2) AS ama_arpu ".
			 " FROM `tbl_user_guest_stat_daily` ".
			 " WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ". 
			 " AND today >= '2016-11-14' ".
			 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today)"; 
	$arpdau_data_dayweek = $db_other->getarray($sql);
	
	$arpdau_ios_dayweek = ($arpdau_data_dayweek["ios_arpu"]=='')?0:$arpdau_data_dayweek["ios_arpu"];
	$arpdau_and_dayweek = ($arpdau_data_dayweek["and_arpu"]=='')?0:$arpdau_data_dayweek["and_arpu"];
	$arpdau_ama_dayweek = ($arpdau_data_dayweek["ama_arpu"]=='')?0:$arpdau_data_dayweek["ama_arpu"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 5, 6, $arpdau_web_dayweek, $arpdau_ios_dayweek, $arpdau_and_dayweek, $arpdau_ama_dayweek) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 6.결제경험자
	// 최근 52주
	$sql = "SELECT ROUND(AVG(payer_logincount)) AS payer_logincount, ROUND(AVG(ios_payer_logincount)) AS ios_payer_logincount, ". 
			" ROUND(AVG(and_payer_logincount)) AS and_payer_logincount, ROUND(AVG(ama_payer_logincount)) AS ama_payer_logincount ".
			" FROM tbl_user_payer_stat_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')"; 
	$payer_log_data_52week = $db_other->getarray($sql);
	
	$payer_log_web_52week = $payer_log_data_52week["payer_logincount"];
	$payer_log_ios_52week = $payer_log_data_52week["ios_payer_logincount"];
	$payer_log_and_52week = $payer_log_data_52week["and_payer_logincount"];
	$payer_log_ama_52week = $payer_log_data_52week["ama_payer_logincount"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 6, 1, $payer_log_web_52week, $payer_log_ios_52week, $payer_log_and_52week, $payer_log_ama_52week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	$sql = "SELECT ROUND(AVG(payer_logincount)) AS payer_logincount, ROUND(AVG(ios_payer_logincount)) AS ios_payer_logincount, ". 
			" ROUND(AVG(and_payer_logincount)) AS and_payer_logincount, ROUND(AVG(ama_payer_logincount)) AS ama_payer_logincount ".
			" FROM tbl_user_payer_stat_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')"; 
	$payer_log_data_13week = $db_other->getarray($sql);
	
	$payer_log_web_13week = $payer_log_data_13week["payer_logincount"];
	$payer_log_ios_13week = $payer_log_data_13week["ios_payer_logincount"];
	$payer_log_and_13week = $payer_log_data_13week["and_payer_logincount"];
	$payer_log_ama_13week = $payer_log_data_13week["ama_payer_logincount"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 6, 2, $payer_log_web_13week, $payer_log_ios_13week, $payer_log_and_13week, $payer_log_ama_13week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주
	$sql = "SELECT ROUND(AVG(payer_logincount)) AS payer_logincount, ROUND(AVG(ios_payer_logincount)) AS ios_payer_logincount, ". 
			" ROUND(AVG(and_payer_logincount)) AS and_payer_logincount, ROUND(AVG(ama_payer_logincount)) AS ama_payer_logincount ".
			" FROM tbl_user_payer_stat_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')"; 
	$payer_log_data_4week = $db_other->getarray($sql);
	
	$payer_log_web_4week = $payer_log_data_4week["payer_logincount"];
	$payer_log_ios_4week = $payer_log_data_4week["ios_payer_logincount"];
	$payer_log_and_4week = $payer_log_data_4week["and_payer_logincount"];
	$payer_log_ama_4week = $payer_log_data_4week["ama_payer_logincount"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 6, 3, $payer_log_web_4week, $payer_log_ios_4week, $payer_log_and_4week, $payer_log_ama_4week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 1주
	$sql = "SELECT ROUND(AVG(payer_logincount)) AS payer_logincount, ROUND(AVG(ios_payer_logincount)) AS ios_payer_logincount, ". 
			" ROUND(AVG(and_payer_logincount)) AS and_payer_logincount, ROUND(AVG(ama_payer_logincount)) AS ama_payer_logincount ".
			" FROM tbl_user_payer_stat_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')"; 
	$payer_log_data_1week = $db_other->getarray($sql);
	
	$payer_log_web_1week = $payer_log_data_1week["payer_logincount"];
	$payer_log_ios_1week = $payer_log_data_1week["ios_payer_logincount"];
	$payer_log_and_1week = $payer_log_data_1week["and_payer_logincount"];
	$payer_log_ama_1week = $payer_log_data_1week["ama_payer_logincount"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 6, 4, $payer_log_web_1week, $payer_log_ios_1week, $payer_log_and_1week, $payer_log_ama_1week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 어제
	$sql = "SELECT ROUND(AVG(payer_logincount)) AS payer_logincount, ROUND(AVG(ios_payer_logincount)) AS ios_payer_logincount, ". 
			" ROUND(AVG(and_payer_logincount)) AS and_payer_logincount, ROUND(AVG(ama_payer_logincount)) AS ama_payer_logincount ".
			" FROM tbl_user_payer_stat_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')"; 
	$payer_log_data_day = $db_other->getarray($sql);
	
	$payer_log_web_day = $payer_log_data_day["payer_logincount"];
	$payer_log_ios_day = $payer_log_data_day["ios_payer_logincount"];
	$payer_log_and_day = $payer_log_data_day["and_payer_logincount"];
	$payer_log_ama_day = $payer_log_data_day["ama_payer_logincount"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 6, 5, $payer_log_web_day, $payer_log_ios_day, $payer_log_and_day, $payer_log_ama_day) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	$sql = "SELECT ROUND(AVG(payer_logincount)) AS payer_logincount, ROUND(AVG(ios_payer_logincount)) AS ios_payer_logincount, ". 
			 " ROUND(AVG(and_payer_logincount)) AS and_payer_logincount, ROUND(AVG(ama_payer_logincount)) AS ama_payer_logincount ". 
			 " FROM tbl_user_payer_stat_daily  ".
			 " WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
			 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today)";  
	$payer_log_data_dayweek = $db_other->getarray($sql);
	
	$payer_log_web_dayweek = $payer_log_data_dayweek["payer_logincount"];
	$payer_log_ios_dayweek = $payer_log_data_dayweek["ios_payer_logincount"];
	$payer_log_and_dayweek = $payer_log_data_dayweek["and_payer_logincount"];
	$payer_log_ama_dayweek = $payer_log_data_dayweek["ama_payer_logincount"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 6, 6, $payer_log_web_dayweek, $payer_log_ios_dayweek, $payer_log_and_dayweek, $payer_log_ama_dayweek) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 7.결제자
	// 최근 52주
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count, ROUND(AVG(ios_payer_count)) AS ios_payer_count, ROUND(AVG(and_payer_count)) AS and_payer_count, ROUND(AVG(ama_payer_count)) AS ama_payer_count ".
			" FROM tbl_user_payer_stat_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ";
	$payer_data_52week = $db_other->getarray($sql);
	
	$payer_web_52week = $payer_data_52week["payer_count"];
	$payer_ios_52week = $payer_data_52week["ios_payer_count"];
	$payer_and_52week = $payer_data_52week["and_payer_count"];
	$payer_ama_52week = $payer_data_52week["ama_payer_count"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 7, 1, $payer_web_52week, $payer_ios_52week, $payer_and_52week, $payer_ama_52week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count, ROUND(AVG(ios_payer_count)) AS ios_payer_count, ROUND(AVG(and_payer_count)) AS and_payer_count, ROUND(AVG(ama_payer_count)) AS ama_payer_count ".
			" FROM tbl_user_payer_stat_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ";
	$payer_data_13week = $db_other->getarray($sql);
	
	$payer_web_13week = $payer_data_13week["payer_count"];
	$payer_ios_13week = $payer_data_13week["ios_payer_count"];
	$payer_and_13week = $payer_data_13week["and_payer_count"];
	$payer_ama_13week = $payer_data_13week["ama_payer_count"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 7, 2, $payer_web_13week, $payer_ios_13week, $payer_and_13week, $payer_ama_13week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count, ROUND(AVG(ios_payer_count)) AS ios_payer_count, ROUND(AVG(and_payer_count)) AS and_payer_count, ROUND(AVG(ama_payer_count)) AS ama_payer_count ".
			" FROM tbl_user_payer_stat_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ";
	$payer_data_4week = $db_other->getarray($sql);
	
	$payer_web_4week = $payer_data_4week["payer_count"];
	$payer_ios_4week = $payer_data_4week["ios_payer_count"];
	$payer_and_4week = $payer_data_4week["and_payer_count"];
	$payer_ama_4week = $payer_data_4week["ama_payer_count"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 7, 3, $payer_web_4week, $payer_ios_4week, $payer_and_4week, $payer_ama_4week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 1주
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count, ROUND(AVG(ios_payer_count)) AS ios_payer_count, ROUND(AVG(and_payer_count)) AS and_payer_count, ROUND(AVG(ama_payer_count)) AS ama_payer_count ".
			" FROM tbl_user_payer_stat_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ";
	$payer_data_1week = $db_other->getarray($sql);
	
	$payer_web_1week = $payer_data_1week["payer_count"];
	$payer_ios_1week = $payer_data_1week["ios_payer_count"];
	$payer_and_1week = $payer_data_1week["and_payer_count"];
	$payer_ama_1week = $payer_data_1week["ama_payer_count"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 7, 4, $payer_web_1week, $payer_ios_1week, $payer_and_1week, $payer_ama_1week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 어제
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count, ROUND(AVG(ios_payer_count)) AS ios_payer_count, ROUND(AVG(and_payer_count)) AS and_payer_count, ROUND(AVG(ama_payer_count)) AS ama_payer_count ".
			" FROM tbl_user_payer_stat_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ";
	$payer_data_day = $db_other->getarray($sql);
	
	$payer_web_day = $payer_data_day["payer_count"];
	$payer_ios_day = $payer_data_day["ios_payer_count"];
	$payer_and_day = $payer_data_day["and_payer_count"];
	$payer_ama_day = $payer_data_day["ama_payer_count"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 7, 5, $payer_web_day, $payer_ios_day, $payer_and_day, $payer_ama_day) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count, ROUND(AVG(ios_payer_count)) AS ios_payer_count, ROUND(AVG(and_payer_count)) AS and_payer_count, ROUND(AVG(ama_payer_count)) AS ama_payer_count ". 
			 " FROM tbl_user_payer_stat_daily ".
			 " WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
			 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today)"; 
	$payer_data_dayweek = $db_other->getarray($sql);
	
	$payer_web_dayweek = $payer_data_dayweek["payer_count"];
	$payer_ios_dayweek = $payer_data_dayweek["ios_payer_count"];
	$payer_and_dayweek = $payer_data_dayweek["and_payer_count"];
	$payer_ama_dayweek = $payer_data_dayweek["ama_payer_count"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, ios, android, amazon) VALUES('$today', 7, 6, $payer_web_dayweek, $payer_ios_dayweek, $payer_and_dayweek, $payer_ama_dayweek) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);

	// 8.게임 플레이
	// 최근 52주
	//web
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7*52), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7*52), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_web_data_52week = $db2->getarray($sql);
	
	$game_web_moneyin_52week = $game_web_data_52week["moneyin"];
	$game_web_moneyout_52week = $game_web_data_52week["moneyout"];
	$game_web_unitrate_52week = $game_web_data_52week["unitrate"];
	
	//ios
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7*52), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7*52), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_ios_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_ios_data_52week = $db2->getarray($sql);
	
	$game_ios_moneyin_52week = $game_ios_data_52week["moneyin"];
	$game_ios_moneyout_52week = $game_ios_data_52week["moneyout"];
	$game_ios_unitrate_52week = $game_ios_data_52week["unitrate"];
	
	//android
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7*52), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7*52), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_android_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_android_data_52week = $db2->getarray($sql);
	
	$game_android_moneyin_52week = $game_android_data_52week["moneyin"];
	$game_android_moneyout_52week = $game_android_data_52week["moneyout"];
	$game_android_unitrate_52week = $game_android_data_52week["unitrate"];
	
	//amazon
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7*52), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7*52), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_amazon_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_amazon_data_52week = $db2->getarray($sql);
	
	$game_amazon_moneyin_52week = $game_amazon_data_52week["moneyin"];
	$game_amazon_moneyout_52week = $game_amazon_data_52week["moneyout"];
	$game_amazon_unitrate_52week = $game_amazon_data_52week["unitrate"];

	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, web_sub1, web_sub2, ios, ios_sub1, ios_sub2, android, android_sub1, android_sub2, amazon, amazon_sub1, amazon_sub2) ".
			"VALUES('$today', 8, 1, $game_web_moneyin_52week, $game_web_moneyout_52week, $game_web_unitrate_52week, $game_ios_moneyin_52week, $game_ios_moneyout_52week, $game_ios_unitrate_52week, ".
			"		$game_android_moneyin_52week, $game_android_moneyout_52week, $game_android_unitrate_52week, $game_amazon_moneyin_52week, $game_amazon_moneyout_52week, $game_amazon_unitrate_52week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ios_sub2=VALUES(ios_sub2), ".
			"		android=VALUES(android), android_sub1=VALUES(android_sub1), android_sub2=VALUES(android_sub2), amazon=VALUES(amazon), amazon_sub1=VALUES(amazon_sub1), amazon_sub2=VALUES(amazon_sub2);";
	$db_other->execute($sql);
	
	// 최근 13주
	//web
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7*13), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7*13), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_web_data_13week = $db2->getarray($sql);
	
	$game_web_moneyin_13week = $game_web_data_13week["moneyin"];
	$game_web_moneyout_13week = $game_web_data_13week["moneyout"];
	$game_web_unitrate_13week = $game_web_data_13week["unitrate"];
	
	//ios
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7*13), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7*13), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_ios_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_ios_data_13week = $db2->getarray($sql);
	
	$game_ios_moneyin_13week = $game_ios_data_13week["moneyin"];
	$game_ios_moneyout_13week = $game_ios_data_13week["moneyout"];
	$game_ios_unitrate_13week = $game_ios_data_13week["unitrate"];
	
	//android
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7*13), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7*13), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_android_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_android_data_13week = $db2->getarray($sql);
	
	$game_android_moneyin_13week = $game_android_data_13week["moneyin"];
	$game_android_moneyout_13week = $game_android_data_13week["moneyout"];
	$game_android_unitrate_13week = $game_android_data_13week["unitrate"];
	
	//amazon
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7*13), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7*13), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_amazon_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_amazon_data_13week = $db2->getarray($sql);
	
	$game_amazon_moneyin_13week = $game_amazon_data_13week["moneyin"];
	$game_amazon_moneyout_13week = $game_amazon_data_13week["moneyout"];
	$game_amazon_unitrate_13week = $game_amazon_data_13week["unitrate"];

	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, web_sub1, web_sub2, ios, ios_sub1, ios_sub2, android, android_sub1, android_sub2, amazon, amazon_sub1, amazon_sub2) ".
			"VALUES('$today', 8, 2, $game_web_moneyin_13week, $game_web_moneyout_13week, $game_web_unitrate_13week, $game_ios_moneyin_13week, $game_ios_moneyout_13week, $game_ios_unitrate_13week, ".
			"		$game_android_moneyin_13week, $game_android_moneyout_13week, $game_android_unitrate_13week, $game_amazon_moneyin_13week, $game_amazon_moneyout_13week, $game_amazon_unitrate_13week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ios_sub2=VALUES(ios_sub2), ".
			"		android=VALUES(android), android_sub1=VALUES(android_sub1), android_sub2=VALUES(android_sub2), amazon=VALUES(amazon), amazon_sub1=VALUES(amazon_sub1), amazon_sub2=VALUES(amazon_sub2);";
	$db_other->execute($sql);
	
	// 최근 4주
	//web
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7*4), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7*4), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_web_data_4week = $db2->getarray($sql);
	
	$game_web_moneyin_4week = $game_web_data_4week["moneyin"];
	$game_web_moneyout_4week = $game_web_data_4week["moneyout"];
	$game_web_unitrate_4week = $game_web_data_4week["unitrate"];
	
	//ios
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7*4), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7*4), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_ios_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_ios_data_4week = $db2->getarray($sql);
	
	$game_ios_moneyin_4week = $game_ios_data_4week["moneyin"];
	$game_ios_moneyout_4week = $game_ios_data_4week["moneyout"];
	$game_ios_unitrate_4week = $game_ios_data_4week["unitrate"];
	
	//android
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7*4), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7*4), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_android_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_android_data_4week = $db2->getarray($sql);
	
	$game_android_moneyin_4week = $game_android_data_4week["moneyin"];
	$game_android_moneyout_4week = $game_android_data_4week["moneyout"];
	$game_android_unitrate_4week = $game_android_data_4week["unitrate"];
	
	//amazon
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7*4), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7*4), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_amazon_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_amazon_data_4week = $db2->getarray($sql);
	
	$game_amazon_moneyin_4week = $game_amazon_data_4week["moneyin"];
	$game_amazon_moneyout_4week = $game_amazon_data_4week["moneyout"];
	$game_amazon_unitrate_4week = $game_amazon_data_4week["unitrate"];
	
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, web_sub1, web_sub2, ios, ios_sub1, ios_sub2, android, android_sub1, android_sub2, amazon, amazon_sub1, amazon_sub2) ".
			"VALUES('$today', 8, 3, $game_web_moneyin_4week, $game_web_moneyout_4week, $game_web_unitrate_4week, $game_ios_moneyin_4week, $game_ios_moneyout_4week, $game_ios_unitrate_4week, ".
			"		$game_android_moneyin_4week, $game_android_moneyout_4week, $game_android_unitrate_4week, $game_amazon_moneyin_4week, $game_amazon_moneyout_4week, $game_amazon_unitrate_4week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ios_sub2=VALUES(ios_sub2), ".
			"		android=VALUES(android), android_sub1=VALUES(android_sub1), android_sub2=VALUES(android_sub2), amazon=VALUES(amazon), amazon_sub1=VALUES(amazon_sub1), amazon_sub2=VALUES(amazon_sub2);";
	$db_other->execute($sql);
	
	// 최근 1주
	//web
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_web_data_1week = $db2->getarray($sql);
	
	$game_web_moneyin_1week = $game_web_data_1week["moneyin"];
	$game_web_moneyout_1week = $game_web_data_1week["moneyout"];
	$game_web_unitrate_1week = $game_web_data_1week["unitrate"];
	
	//ios
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_ios_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_ios_data_1week = $db2->getarray($sql);
	
	$game_ios_moneyin_1week = $game_ios_data_1week["moneyin"];
	$game_ios_moneyout_1week = $game_ios_data_1week["moneyout"];
	$game_ios_unitrate_1week = $game_ios_data_1week["unitrate"];
	
	//android
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_android_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_android_data_1week = $db2->getarray($sql);
	
	$game_android_moneyin_1week = $game_android_data_1week["moneyin"];
	$game_android_moneyout_1week = $game_android_data_1week["moneyout"];
	$game_android_unitrate_1week = $game_android_data_1week["unitrate"];
	
	//amazon
	$sql = "SELECT ROUND(SUM(moneyin)/500000/(7), 2) AS moneyin,ROUND(SUM(moneyout)/500000/(7), 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_amazon_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_amazon_data_1week = $db2->getarray($sql);
	
	$game_amazon_moneyin_1week = $game_amazon_data_1week["moneyin"];
	$game_amazon_moneyout_1week = $game_amazon_data_1week["moneyout"];
	$game_amazon_unitrate_1week = $game_amazon_data_1week["unitrate"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, web_sub1, web_sub2, ios, ios_sub1, ios_sub2, android, android_sub1, android_sub2, amazon, amazon_sub1, amazon_sub2) ".
			"VALUES('$today', 8, 4, $game_web_moneyin_1week, $game_web_moneyout_1week, $game_web_unitrate_1week, $game_ios_moneyin_1week, $game_ios_moneyout_1week, $game_ios_unitrate_1week, ".
			"		$game_android_moneyin_1week, $game_android_moneyout_1week, $game_android_unitrate_1week, $game_amazon_moneyin_1week, $game_amazon_moneyout_1week, $game_amazon_unitrate_1week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ios_sub2=VALUES(ios_sub2), ".
			"		android=VALUES(android), android_sub1=VALUES(android_sub1), android_sub2=VALUES(android_sub2), amazon=VALUES(amazon), amazon_sub1=VALUES(amazon_sub1), amazon_sub2=VALUES(amazon_sub2);";
	$db_other->execute($sql);

	// 어제
	//web
	$sql = "SELECT ROUND(SUM(moneyin)/500000, 2) AS moneyin,ROUND(SUM(moneyout)/500000, 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_web_data_day = $db2->getarray($sql);
	
	$game_web_moneyin_day = $game_web_data_day["moneyin"];
	$game_web_moneyout_day = $game_web_data_day["moneyout"];
	$game_web_unitrate_day = $game_web_data_day["unitrate"];
	
	//ios
	$sql = "SELECT ROUND(SUM(moneyin)/500000, 2) AS moneyin,ROUND(SUM(moneyout)/500000, 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_ios_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_ios_data_day = $db2->getarray($sql);
	
	$game_ios_moneyin_day = $game_ios_data_day["moneyin"];
	$game_ios_moneyout_day = $game_ios_data_day["moneyout"];
	$game_ios_unitrate_day = $game_ios_data_day["unitrate"];
	
	//android
	$sql = "SELECT ROUND(SUM(moneyin)/500000, 2) AS moneyin,ROUND(SUM(moneyout)/500000, 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_android_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_android_data_day = $db2->getarray($sql);
	
	$game_android_moneyin_day = $game_android_data_day["moneyin"];
	$game_android_moneyout_day = $game_android_data_day["moneyout"];
	$game_android_unitrate_day = $game_android_data_day["unitrate"];
	
	//amazon
	$sql = "SELECT ROUND(SUM(moneyin)/500000, 2) AS moneyin,ROUND(SUM(moneyout)/500000, 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			 "FROM  ".
			 " ( ". 
				"  SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
				"  FROM tbl_game_cash_stats_amazon_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				"  WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d') AND MODE NOT IN (4, 9, 26, 29) ". 
				"  GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_amazon_data_day = $db2->getarray($sql);
	
	$game_amazon_moneyin_day = $game_amazon_data_day["moneyin"];
	$game_amazon_moneyout_day = $game_amazon_data_day["moneyout"];
	$game_amazon_unitrate_day = $game_amazon_data_day["unitrate"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, web_sub1, web_sub2, ios, ios_sub1, ios_sub2, android, android_sub1, android_sub2, amazon, amazon_sub1, amazon_sub2) ".
			"VALUES('$today', 8, 5, $game_web_moneyin_day, $game_web_moneyout_day, $game_web_unitrate_day, $game_ios_moneyin_day, $game_ios_moneyout_day, $game_ios_unitrate_day, ".
			"		$game_android_moneyin_day, $game_android_moneyout_day, $game_android_unitrate_day, $game_amazon_moneyin_day, $game_amazon_moneyout_day, $game_amazon_unitrate_day) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ios_sub2=VALUES(ios_sub2), ".
			"		android=VALUES(android), android_sub1=VALUES(android_sub1), android_sub2=VALUES(android_sub2), amazon=VALUES(amazon), amazon_sub1=VALUES(amazon_sub1), amazon_sub2=VALUES(amazon_sub2);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	//web
	$sql = "SELECT ROUND(SUM(moneyin)/500000/4, 2) AS moneyin,ROUND(SUM(moneyout)/500000/4, 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ". 
			" FROM  ".
			 " ( ".
			 " SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ". 
			 " FROM tbl_game_cash_stats_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ".
			 " WHERE 1=1 AND 0 <= t1.betlevel ".
			 " AND t1.betlevel <= 16 ".
			 " AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= writedate ". 
			 " AND writedate < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
			 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
			 " AND MODE NOT IN (4, 9, 26, 29) ".
			 " GROUP BY slottype, betlevel ".
			 " ) t3";
	$game_web_data_dayweek = $db2->getarray($sql);
	
	$game_web_moneyin_dayweek = $game_web_data_dayweek["moneyin"];
	$game_web_moneyout_dayweek = $game_web_data_dayweek["moneyout"];
	$game_web_unitrate_dayweek = $game_web_data_dayweek["unitrate"];
	
	//ios
	$sql = "SELECT ROUND(SUM(moneyin)/500000/4, 2) AS moneyin,ROUND(SUM(moneyout)/500000/4, 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ".  
			" FROM  ".
			 " (  ".
				 " SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ".  
				 " FROM tbl_game_cash_stats_ios_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel ".
				 " WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 ".
				 " AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= writedate ". 
				 " AND writedate < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
				 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(writedate)  ".
				 " AND MODE NOT IN (4, 9, 26, 29) ". 
				 " GROUP BY slottype, betlevel ". 
			  " ) t3";
	$game_ios_data_dayweek = $db2->getarray($sql);
	
	$game_ios_moneyin_dayweek = $game_ios_data_dayweek["moneyin"];
	$game_ios_moneyout_dayweek = $game_ios_data_dayweek["moneyout"];
	$game_ios_unitrate_dayweek = $game_ios_data_dayweek["unitrate"];
	
	//android
	$sql = "SELECT ROUND(SUM(moneyin)/500000/4, 2) AS moneyin,ROUND(SUM(moneyout)/500000/4, 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ".  
			 " FROM  ".
			  " (  ".
				 " SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ".  
				 " FROM tbl_game_cash_stats_android_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel  ". 
				 " WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 ".
				 " AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= writedate ". 
				 " AND writedate < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
				 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				 " AND MODE NOT IN (4, 9, 26, 29) ". 
				 " GROUP BY slottype, betlevel ". 
			  " ) t3";
	$game_android_data_dayweek = $db2->getarray($sql);
	
	$game_android_moneyin_dayweek = $game_android_data_dayweek["moneyin"];
	$game_android_moneyout_dayweek = $game_android_data_dayweek["moneyout"];
	$game_android_unitrate_dayweek = $game_android_data_dayweek["unitrate"];
	
	//amazon
	$sql = "SELECT ROUND(SUM(moneyin)/500000/4, 2) AS moneyin,ROUND(SUM(moneyout)/500000/4, 2) AS moneyout, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unitrate ".  
			" FROM  ".
			 " (  ".
				  " SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout ".  
				  " FROM tbl_game_cash_stats_amazon_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel ".   
				  " WHERE 1=1 AND 0 <= t1.betlevel AND t1.betlevel <= 16 ".
				  " AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= writedate ". 
				  " AND writedate < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
				  " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				  " AND MODE NOT IN (4, 9, 26, 29) ". 
				  " GROUP BY slottype, betlevel ". 
			 " ) t3";
	$game_amazon_data_dayweek = $db2->getarray($sql);
	
	$game_amazon_moneyin_dayweek = $game_amazon_data_dayweek["moneyin"];
	$game_amazon_moneyout_dayweek = $game_amazon_data_dayweek["moneyout"];
	$game_amazon_unitrate_dayweek = $game_amazon_data_dayweek["unitrate"];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, web_sub1, web_sub2, ios, ios_sub1, ios_sub2, android, android_sub1, android_sub2, amazon, amazon_sub1, amazon_sub2) ".
			"VALUES('$today', 8, 6, $game_web_moneyin_dayweek, $game_web_moneyout_dayweek, $game_web_unitrate_dayweek, $game_ios_moneyin_dayweek, $game_ios_moneyout_dayweek, $game_ios_unitrate_dayweek, ".
			"		$game_android_moneyin_dayweek, $game_android_moneyout_dayweek, $game_android_unitrate_dayweek, $game_amazon_moneyin_dayweek, $game_amazon_moneyout_dayweek, $game_amazon_unitrate_dayweek) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ios_sub2=VALUES(ios_sub2), ".
			"		android=VALUES(android), android_sub1=VALUES(android_sub1), android_sub2=VALUES(android_sub2), amazon=VALUES(amazon), amazon_sub1=VALUES(amazon_sub1), amazon_sub2=VALUES(amazon_sub2);";
	$db_other->execute($sql);

	// 9.잭팟
	// 최근 52주
	$sql = "SELECT devicetype, ROUND(SUM(jackpot_count)/(7*52), 2) AS jackpot_count, ROUND(SUM(jackpot_amount/500000/(7*52))) AS jackpot_amount ".
			" FROM ( ".
				" SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, devicetype, COUNT(DISTINCT jackpothallidx) AS jackpot_count, ROUND(SUM(amount), 2) AS jackpot_amount ".
				" FROM `tbl_jackpot_log` ".
				" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), devicetype ".
			" ) t1 ".
			" GROUP BY devicetype ";
	
	$jackpot_data_52week = $db_slave->gettotallist($sql);

	$jackpot_cnt_web_52week = 0;
	$jackpot_amount_web_52week = 0;
	$jackpot_cnt_ios_52week = 0;
	$jackpot_amount_ios_52week = 0;
	$jackpot_cnt_android_52week = 0;
	$jackpot_amount_android_52week = 0;
	$jackpot_cnt_amazon_52week = 0;
	$jackpot_amount_amazon_52week = 0;
	
	foreach ($jackpot_data_52week as $jackpot_data_52weekObj)
	{
		$devicetype = $jackpot_data_52weekObj['devicetype'];
		if($devicetype == 0)
		{
			$jackpot_cnt_web_52week = $jackpot_data_52weekObj["jackpot_count"];
			$jackpot_amount_web_52week = $jackpot_data_52weekObj["jackpot_amount"];
		}
		else if($devicetype == 1)
		{
			$jackpot_cnt_ios_52week = $jackpot_data_52weekObj["jackpot_count"];
			$jackpot_amount_ios_52week = $jackpot_data_52weekObj["jackpot_amount"];
		}
		else if($devicetype == 2)
		{
			$jackpot_cnt_android_52week = $jackpot_data_52weekObj["jackpot_count"];
			$jackpot_amount_android_52week = $jackpot_data_52weekObj["jackpot_amount"];
		}
		else if($devicetype == 3)
		{
			$jackpot_cnt_amazon_52week = $jackpot_data_52weekObj["jackpot_count"];
			$jackpot_amount_amazon_52week = $jackpot_data_52weekObj["jackpot_amount"];
		}
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, web_sub1, ios, ios_sub1, android, android_sub1, amazon, amazon_sub1) ".
			"VALUES('$today', 9, 1, $jackpot_cnt_web_52week, $jackpot_amount_web_52week, $jackpot_cnt_ios_52week, $jackpot_amount_ios_52week, $jackpot_cnt_android_52week,$jackpot_amount_android_52week, ".
			"		$jackpot_cnt_amazon_52week, $jackpot_amount_amazon_52week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ".
			"		android=VALUES(android), android_sub1=VALUES(android_sub1), amazon=VALUES(amazon), amazon_sub1=VALUES(amazon_sub1);";
	$db_other->execute($sql);
	
	// 최근 13주
	$sql = "SELECT devicetype, ROUND(SUM(jackpot_count)/(7*13), 2) AS jackpot_count, ROUND(SUM(jackpot_amount/500000/(7*13))) AS jackpot_amount ".
			" FROM ( ".
				" SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, devicetype, COUNT(DISTINCT jackpothallidx) AS jackpot_count, ROUND(SUM(amount), 2) AS jackpot_amount ".
				" FROM `tbl_jackpot_log` ".
				" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), devicetype ".
			" ) t1 ".
			" GROUP BY devicetype ";
	
	$jackpot_data_13week = $db_slave->gettotallist($sql);

	$jackpot_cnt_web_13week = 0;
	$jackpot_amount_web_13week = 0;
	$jackpot_cnt_ios_13week = 0;
	$jackpot_amount_ios_13week = 0;
	$jackpot_cnt_android_13week = 0;
	$jackpot_amount_android_13week = 0;
	$jackpot_cnt_amazon_13week = 0;
	$jackpot_amount_amazon_13week = 0;
	
	foreach ($jackpot_data_13week as $jackpot_data_13weekObj)
	{
		$devicetype = $jackpot_data_13weekObj['devicetype'];
		if($devicetype == 0)
		{
			$jackpot_cnt_web_13week = $jackpot_data_13weekObj["jackpot_count"];
			$jackpot_amount_web_13week = $jackpot_data_13weekObj["jackpot_amount"];
		}
		else if($devicetype == 1)
		{
			$jackpot_cnt_ios_13week = $jackpot_data_13weekObj["jackpot_count"];
			$jackpot_amount_ios_13week = $jackpot_data_13weekObj["jackpot_amount"];
		}
		else if($devicetype == 2)
		{
			$jackpot_cnt_android_13week = $jackpot_data_13weekObj["jackpot_count"];
			$jackpot_amount_android_13week = $jackpot_data_13weekObj["jackpot_amount"];
		}
		else if($devicetype == 3)
		{
			$jackpot_cnt_amazon_13week = $jackpot_data_13weekObj["jackpot_count"];
			$jackpot_amount_amazon_13week = $jackpot_data_13weekObj["jackpot_amount"];
		}
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, web_sub1, ios, ios_sub1, android, android_sub1, amazon, amazon_sub1) ".
			"VALUES('$today', 9, 2, $jackpot_cnt_web_13week, $jackpot_amount_web_13week, $jackpot_cnt_ios_13week, $jackpot_amount_ios_13week, $jackpot_cnt_android_13week,$jackpot_amount_android_13week, ".
			"		$jackpot_cnt_amazon_13week, $jackpot_amount_amazon_13week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ".
			"		android=VALUES(android), android_sub1=VALUES(android_sub1), amazon=VALUES(amazon), amazon_sub1=VALUES(amazon_sub1);";
	$db_other->execute($sql);
	
	
	// 최근 4주
	$sql = "SELECT devicetype, ROUND(SUM(jackpot_count)/(7*4), 2) AS jackpot_count, ROUND(SUM(jackpot_amount/500000/(7*4))) AS jackpot_amount ".
			" FROM ( ".
				" SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, devicetype, COUNT(DISTINCT jackpothallidx) AS jackpot_count, ROUND(SUM(amount), 2) AS jackpot_amount ".
				" FROM `tbl_jackpot_log` ".
				" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), devicetype ".
			" ) t1 ".
			" GROUP BY devicetype ";
	
	$jackpot_data_4week = $db_slave->gettotallist($sql);

	$jackpot_cnt_web_4week = 0;
	$jackpot_amount_web_4week = 0;
	$jackpot_cnt_ios_4week = 0;
	$jackpot_amount_ios_4week = 0;
	$jackpot_cnt_android_4week = 0;
	$jackpot_amount_android_4week = 0;
	$jackpot_cnt_amazon_4week = 0;
	$jackpot_amount_amazon_4week = 0;
	
	foreach ($jackpot_data_4week as $jackpot_data_4weekObj)
	{
		$devicetype = $jackpot_data_4weekObj['devicetype'];
		if($devicetype == 0)
		{
			$jackpot_cnt_web_4week = $jackpot_data_4weekObj["jackpot_count"];
			$jackpot_amount_web_4week = $jackpot_data_4weekObj["jackpot_amount"];
		}
		else if($devicetype == 1)
		{
			$jackpot_cnt_ios_4week = $jackpot_data_4weekObj["jackpot_count"];
			$jackpot_amount_ios_4week = $jackpot_data_4weekObj["jackpot_amount"];
		}
		else if($devicetype == 2)
		{
			$jackpot_cnt_android_4week = $jackpot_data_4weekObj["jackpot_count"];
			$jackpot_amount_android_4week = $jackpot_data_4weekObj["jackpot_amount"];
		}
		else if($devicetype == 3)
		{
			$jackpot_cnt_amazon_4week = $jackpot_data_4weekObj["jackpot_count"];
			$jackpot_amount_amazon_4week = $jackpot_data_4weekObj["jackpot_amount"];
		}
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, web_sub1, ios, ios_sub1, android, android_sub1, amazon, amazon_sub1) ".
			"VALUES('$today', 9, 3, $jackpot_cnt_web_4week, $jackpot_amount_web_4week, $jackpot_cnt_ios_4week, $jackpot_amount_ios_4week, $jackpot_cnt_android_4week,$jackpot_amount_android_4week, ".
			"		$jackpot_cnt_amazon_4week, $jackpot_amount_amazon_4week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ".
			"		android=VALUES(android), android_sub1=VALUES(android_sub1), amazon=VALUES(amazon), amazon_sub1=VALUES(amazon_sub1);";
	$db_other->execute($sql);
	
	// 최근 1주
	$sql = "SELECT devicetype, ROUND(SUM(jackpot_count)/(7), 2) AS jackpot_count, ROUND(SUM(jackpot_amount/500000/(7))) AS jackpot_amount ".
			" FROM ( ".
				" SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, devicetype, COUNT(DISTINCT jackpothallidx) AS jackpot_count, ROUND(SUM(amount), 2) AS jackpot_amount ".
				" FROM `tbl_jackpot_log` ".
				" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), devicetype ".
			" ) t1 ".
			" GROUP BY devicetype ";
	
	$jackpot_data_1week = $db_slave->gettotallist($sql);

	$jackpot_cnt_web_1week = 0;
	$jackpot_amount_web_1week = 0;
	$jackpot_cnt_ios_1week = 0;
	$jackpot_amount_ios_1week = 0;
	$jackpot_cnt_android_1week = 0;
	$jackpot_amount_android_1week = 0;
	$jackpot_cnt_amazon_1week = 0;
	$jackpot_amount_amazon_1week = 0;
	
	foreach ($jackpot_data_1week as $jackpot_data_1weekObj)
	{
		$devicetype = $jackpot_data_1weekObj['devicetype'];
		if($devicetype == 0)
		{
			$jackpot_cnt_web_1week = $jackpot_data_1weekObj["jackpot_count"];
			$jackpot_amount_web_1week = $jackpot_data_1weekObj["jackpot_amount"];
		}
		else if($devicetype == 1)
		{
			$jackpot_cnt_ios_1week = $jackpot_data_1weekObj["jackpot_count"];
			$jackpot_amount_ios_1week = $jackpot_data_1weekObj["jackpot_amount"];
		}
		else if($devicetype == 2)
		{
			$jackpot_cnt_android_1week = $jackpot_data_1weekObj["jackpot_count"];
			$jackpot_amount_android_1week = $jackpot_data_1weekObj["jackpot_amount"];
		}
		else if($devicetype == 3)
		{
			$jackpot_cnt_amazon_1week = $jackpot_data_1weekObj["jackpot_count"];
			$jackpot_amount_amazon_1week = $jackpot_data_1weekObj["jackpot_amount"];
		}
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, web_sub1, ios, ios_sub1, android, android_sub1, amazon, amazon_sub1) ".
			"VALUES('$today', 9, 4, $jackpot_cnt_web_1week, $jackpot_amount_web_1week, $jackpot_cnt_ios_1week, $jackpot_amount_ios_1week, $jackpot_cnt_android_1week,$jackpot_amount_android_1week, ".
			"		$jackpot_cnt_amazon_1week, $jackpot_amount_amazon_1week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ".
			"		android=VALUES(android), android_sub1=VALUES(android_sub1), amazon=VALUES(amazon), amazon_sub1=VALUES(amazon_sub1);";
	$db_other->execute($sql);
	
	// 어제
	$sql = "SELECT devicetype, ROUND(SUM(jackpot_count), 2) AS jackpot_count, ROUND(SUM(jackpot_amount/500000)) AS jackpot_amount ".
			" FROM ( ".
				" SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, devicetype, COUNT(DISTINCT jackpothallidx) AS jackpot_count, ROUND(SUM(amount), 2) AS jackpot_amount ".
				" FROM `tbl_jackpot_log` ".
				" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d 00:00:00') <= writedate AND writedate < DATE_FORMAT(NOW(), '%Y-%m-%d 00:00:00') ".
				" GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), devicetype ".
			" ) t1 ".
			" GROUP BY devicetype ";
	
	$jackpot_data_day = $db_slave->gettotallist($sql);

	$jackpot_cnt_web_day = 0;
	$jackpot_amount_web_day = 0;
	$jackpot_cnt_ios_day = 0;
	$jackpot_amount_ios_day = 0;
	$jackpot_cnt_android_day = 0;
	$jackpot_amount_android_day = 0;
	$jackpot_cnt_amazon_day = 0;
	$jackpot_amount_amazon_day = 0;
	
	foreach ($jackpot_data_day as $jackpot_data_dayObj)
	{
		$devicetype = $jackpot_data_dayObj['devicetype'];
		if($devicetype == 0)
		{
			$jackpot_cnt_web_day = $jackpot_data_dayObj["jackpot_count"];
			$jackpot_amount_web_day = $jackpot_data_dayObj["jackpot_amount"];
		}
		else if($devicetype == 1)
		{
			$jackpot_cnt_ios_day = $jackpot_data_dayObj["jackpot_count"];
			$jackpot_amount_ios_day = $jackpot_data_dayObj["jackpot_amount"];
		}
		else if($devicetype == 2)
		{
			$jackpot_cnt_android_day = $jackpot_data_dayObj["jackpot_count"];
			$jackpot_amount_android_day = $jackpot_data_dayObj["jackpot_amount"];
		}
		else if($devicetype == 3)
		{
			$jackpot_cnt_amazon_day = $jackpot_data_dayObj["jackpot_count"];
			$jackpot_amount_amazon_day = $jackpot_data_dayObj["jackpot_amount"];
		}
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, web_sub1, ios, ios_sub1, android, android_sub1, amazon, amazon_sub1) ".
			"VALUES('$today', 9, 5, $jackpot_cnt_web_day, $jackpot_amount_web_day, $jackpot_cnt_ios_day, $jackpot_amount_ios_day, $jackpot_cnt_android_day,$jackpot_amount_android_day, ".
			"		$jackpot_cnt_amazon_day, $jackpot_amount_amazon_day) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ".
			"		android=VALUES(android), android_sub1=VALUES(android_sub1), amazon=VALUES(amazon), amazon_sub1=VALUES(amazon_sub1);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	$sql = "SELECT devicetype, ROUND(SUM(jackpot_count), 2) AS jackpot_count, ROUND(SUM(jackpot_amount/500000/(7*4))) AS jackpot_amount ". 
			 " FROM ( ".
				 " SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, devicetype, COUNT(DISTINCT jackpothallidx) AS jackpot_count, ROUND(SUM(amount), 2) AS jackpot_amount ". 
				 " FROM `tbl_jackpot_log` ".
				 " WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d 00:00:00') <= writedate ". 
				 " AND writedate < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d 23:59:59') ".
				 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(writedate) ".
				 " GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), devicetype ".
			 " ) t1 ".
			 " GROUP BY devicetype";
	
	$jackpot_data_dayweek = $db_slave->gettotallist($sql);

	$jackpot_cnt_web_dayweek = 0;
	$jackpot_amount_web_dayweek = 0;
	$jackpot_cnt_ios_dayweek = 0;
	$jackpot_amount_ios_dayweek = 0;
	$jackpot_cnt_android_dayweek = 0;
	$jackpot_amount_android_dayweek = 0;
	$jackpot_cnt_amazon_dayweek = 0;
	$jackpot_amount_amazon_dayweek = 0;
	
	foreach ($jackpot_data_dayweek as $jackpot_data_dayweekObj)
	{
		$devicetype = $jackpot_data_dayweekObj['devicetype'];
		if($devicetype == 0)
		{
			$jackpot_cnt_web_dayweek = $jackpot_data_dayweekObj["jackpot_count"];
			$jackpot_amount_web_dayweek = $jackpot_data_dayweekObj["jackpot_amount"];
		}
		else if($devicetype == 1)
		{
			$jackpot_cnt_ios_dayweek = $jackpot_data_dayweekObj["jackpot_count"];
			$jackpot_amount_ios_dayweek = $jackpot_data_dayweekObj["jackpot_amount"];
		}
		else if($devicetype == 2)
		{
			$jackpot_cnt_android_dayweek = $jackpot_data_dayweekObj["jackpot_count"];
			$jackpot_amount_android_dayweek = $jackpot_data_dayweekObj["jackpot_amount"];
		}
		else if($devicetype == 3)
		{
			$jackpot_cnt_amazon_dayweek = $jackpot_data_dayweekObj["jackpot_count"];
			$jackpot_amount_amazon_dayweek = $jackpot_data_dayweekObj["jackpot_amount"];
		}
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type,  subtype, web, web_sub1, ios, ios_sub1, android, android_sub1, amazon, amazon_sub1) ".
			"VALUES('$today', 9, 6, $jackpot_cnt_web_dayweek, $jackpot_amount_web_dayweek, $jackpot_cnt_ios_dayweek, $jackpot_amount_ios_dayweek, $jackpot_cnt_android_dayweek, $jackpot_amount_android_dayweek, ".
			"		$jackpot_cnt_amazon_dayweek, $jackpot_amount_amazon_dayweek) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ".
			"		android=VALUES(android), android_sub1=VALUES(android_sub1), amazon=VALUES(amazon), amazon_sub1=VALUES(amazon_sub1);";
	$db_other->execute($sql);
	
	// 10.보유코인 전체
	// 최근 52주
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_web_52week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_ios` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_ios_52week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_android` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_and_52week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_amazon` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_amazon_52week = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 10, 1, $coin_web_52week, $coin_ios_52week, $coin_and_52week, $coin_amazon_52week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_web_13week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_ios` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_ios_13week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_android` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_and_13week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_amazon` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_amazon_13week = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 10, 2, $coin_web_13week, $coin_ios_13week, $coin_and_13week, $coin_amazon_13week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_web_4week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_ios` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_ios_4week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_android` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_and_4week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_amazon` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_amazon_4week = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 10, 3, $coin_web_4week, $coin_ios_4week, $coin_and_4week, $coin_amazon_4week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 1주
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_web_1week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_ios` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_ios_1week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_android` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_and_1week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_amazon` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_amazon_1week = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 10, 4, $coin_web_1week, $coin_ios_1week, $coin_and_1week, $coin_amazon_1week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 어제
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_web_day = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_ios` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_ios_day = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_android` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_and_day = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_amazon` ".
			" WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$coin_amazon_day = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 10, 5, $coin_web_day, $coin_ios_day, $coin_and_day, $coin_amazon_day) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ". 
			 " FROM `tbl_user_playstat_gather_daily` ". 
			 " WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
			 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
	$coin_web_dayweek = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ". 
			 " FROM `tbl_user_playstat_gather_daily_ios` ". 
			 " WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
			 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
	$coin_ios_dayweek = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ". 
			 " FROM `tbl_user_playstat_gather_daily_android` ". 
			 " WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
			 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
	$coin_and_dayweek = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ". 
			 " FROM `tbl_user_playstat_gather_daily_amazon` ". 
			 " WHERE statcode = 101 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
			 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today)"; 
	$coin_amazon_dayweek = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 10, 6, $coin_web_dayweek, $coin_ios_dayweek, $coin_and_dayweek, $coin_amazon_dayweek) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 11.보유코인 VIP
	// 최근 52주
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_web_52week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_ios` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_ios_52week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_android` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_and_52week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_amazon` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_amazon_52week = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 11, 1, $vip_coin_web_52week, $vip_coin_ios_52week, $vip_coin_and_52week, $vip_coin_amazon_52week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_web_13week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_ios` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_ios_13week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_android` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_and_13week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_amazon` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_amazon_13week = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 11, 2, $vip_coin_web_13week, $vip_coin_ios_13week, $vip_coin_and_13week, $vip_coin_amazon_13week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_web_4week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_ios` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_ios_4week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_android` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_and_4week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_amazon` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_amazon_4week = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 11, 3, $vip_coin_web_4week, $vip_coin_ios_4week, $vip_coin_and_4week, $vip_coin_amazon_4week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 1주
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_web_1week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_ios` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_ios_1week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_android` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_and_1week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_amazon` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_amazon_1week = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 11, 4, $vip_coin_web_1week, $vip_coin_ios_1week, $vip_coin_and_1week, $vip_coin_amazon_1week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 어제
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_web_day = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_ios` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_ios_day = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_android` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_and_day = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ".
			" FROM `tbl_user_playstat_gather_daily_amazon` ".
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d')";
	$vip_coin_amazon_day = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 11, 5, $vip_coin_web_day, $vip_coin_ios_day, $vip_coin_and_day, $vip_coin_amazon_day) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일통계
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ". 
			" FROM `tbl_user_playstat_gather_daily` ". 
			" WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
			" AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
	$vip_coin_web_dayweek = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ". 
			 " FROM `tbl_user_playstat_gather_daily_ios` ". 
			 " WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
			 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
	$vip_coin_ios_dayweek = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ". 
			 " FROM `tbl_user_playstat_gather_daily_android` ". 
			 " WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
			 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
	$vip_coin_and_dayweek = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(currentcoin)/50) ". 
			 " FROM `tbl_user_playstat_gather_daily_amazon` ". 
			 " WHERE statcode IN (106, 107, 108, 109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
			 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today)";
	$vip_coin_amazon_dayweek = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 11, 6, $vip_coin_web_dayweek, $vip_coin_ios_dayweek, $vip_coin_and_dayweek, $vip_coin_amazon_dayweek) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 12.잔존율 
	// 최근 52주
	$sql = "SELECT platform, IFNULL(ROUND(SUM(day_28)/SUM(reg_count)*100, 2), 0) AS day_28 ".
			" FROM `tbl_user_retention_daily` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL (1+7*4+7*52) DAY), '%Y-%m-%d') <= reg_date AND reg_date < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL (1+7*4) DAY), '%Y-%m-%d') ". 
			" GROUP BY platform ";
	
	$day_28_data_52week = $db_other->gettotallist($sql);

	$day_28_52week = 0;
	$day_28_52week_ios = 0;
	$day_28_52week_android = 0;
	$day_28_52week_amazon = 0;
	
	foreach ($day_28_data_52week as $day_28_data_52weekObj)
	{
		$devicetype = $day_28_data_52weekObj['platform'];
		if($devicetype == 0)
			$day_28_52week = $day_28_data_52weekObj["day_28"];
		else if($devicetype == 1)
			$day_28_52week_ios = $day_28_data_52weekObj["day_28"];
		else if($devicetype == 2)
			$day_28_52week_android = $day_28_data_52weekObj["day_28"];
		else if($devicetype == 3)
			$day_28_52week_amazon = $day_28_data_52weekObj["day_28"];
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 12, 1, $day_28_52week, $day_28_52week_ios, $day_28_52week_android, $day_28_52week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	
	$sql = "SELECT platform, IFNULL(ROUND(SUM(day_28)/SUM(reg_count)*100, 2), 0) AS day_28 ".
			" FROM `tbl_user_retention_daily` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL (1+7*4+7*13) DAY), '%Y-%m-%d') <= reg_date AND reg_date < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL (1+7*4) DAY), '%Y-%m-%d') ". 
			" GROUP BY platform ";
	
	$day_28_data_13week = $db_other->gettotallist($sql);

	$day_28_13week = 0;
	$day_28_13week_ios = 0;
	$day_28_13week_android = 0;
	$day_28_13week_amazon = 0;
	
	foreach ($day_28_data_13week as $day_28_data_13weekObj)
	{
		$devicetype = $day_28_data_13weekObj['platform'];
		if($devicetype == 0)
			$day_28_13week = $day_28_data_13weekObj["day_28"];
		else if($devicetype == 1)
			$day_28_13week_ios = $day_28_data_13weekObj["day_28"];
		else if($devicetype == 2)
			$day_28_13week_android = $day_28_data_13weekObj["day_28"];
		else if($devicetype == 3)
			$day_28_13week_amazon = $day_28_data_13weekObj["day_28"];
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 12, 2, $day_28_13week, $day_28_13week_ios, $day_28_13week_android, $day_28_13week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주
	$sql = "SELECT platform, IFNULL(ROUND(SUM(day_28)/SUM(reg_count)*100, 2), 0) AS day_28 ".
			" FROM `tbl_user_retention_daily` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL (1+7*4+7*4) DAY), '%Y-%m-%d') <= reg_date AND reg_date < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL (1+7*4) DAY), '%Y-%m-%d') ".
			" GROUP BY platform ";
	
	$day_28_data_4week = $db_other->gettotallist($sql);
	
	$day_28_4week = 0;
	$day_28_4week_ios = 0;
	$day_28_4week_android = 0;
	$day_28_4week_amazon = 0;
	
	foreach ($day_28_data_4week as $day_28_data_4weekObj)
	{
		$devicetype = $day_28_data_4weekObj['platform'];
		if($devicetype == 0)
			$day_28_4week = $day_28_data_4weekObj["day_28"];
		else if($devicetype == 1)
			$day_28_4week_ios = $day_28_data_4weekObj["day_28"];
		else if($devicetype == 2)
			$day_28_4week_android = $day_28_data_4weekObj["day_28"];
		else if($devicetype == 3)
			$day_28_4week_amazon = $day_28_data_4weekObj["day_28"];
	}
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 12, 3, $day_28_4week, $day_28_4week_ios, $day_28_4week_android, $day_28_4week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);

	// 최근 1주
	
	$sql = "SELECT platform, ROUND(SUM(day_28)/SUM(reg_count)*100, 2) AS day_28 ".
			" FROM `tbl_user_retention_daily` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL (1+7*4+7*1) DAY), '%Y-%m-%d') <= reg_date AND reg_date < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL (1+7*4) DAY), '%Y-%m-%d') ".
			" GROUP BY platform ";
	
	$day_28_data_1week = $db_other->gettotallist($sql);
	
	$day_28_1week = 0;
	$day_28_1week_ios = 0;
	$day_28_1week_android = 0;
	$day_28_1week_amazon = 0;
	
	foreach ($day_28_data_1week as $day_28_data_1weekObj)
	{
		$devicetype = $day_28_data_1weekObj['platform'];
		if($devicetype == 0)
			$day_28_1week = $day_28_data_1weekObj["day_28"];
		else if($devicetype == 1)
			$day_28_1week_ios = $day_28_data_1weekObj["day_28"];
		else if($devicetype == 2)
			$day_28_1week_android = $day_28_data_1weekObj["day_28"];
		else if($devicetype == 3)
			$day_28_1week_amazon = $day_28_data_1weekObj["day_28"];
	}
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 12, 4, $day_28_1week, $day_28_1week_ios, $day_28_1week_android, $day_28_1week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	$sql = "SELECT platform, ROUND(SUM(day_28)/SUM(reg_count)*100, 2) AS day_28 ". 
			 " FROM `tbl_user_retention_daily`  ".
			 " WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL (1+7*4+7*5) DAY), '%Y-%m-%d') <= reg_date AND reg_date < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL (1+7*4+7*1) DAY), '%Y-%m-%d') ".
			 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(reg_date) ".
			 " GROUP BY platform ";
	
	$day_28_data_dayweek = $db_other->gettotallist($sql);
	
	$day_28_dayweek = 0;
	$day_28_dayweek_ios = 0;
	$day_28_dayweek_android = 0;
	$day_28_dayweek_amazon = 0;
	
	foreach ($day_28_data_dayweek as $day_28_data_dayweekObj)
	{
		$devicetype = $day_28_data_dayweekObj['platform'];
		if($devicetype == 0)
			$day_28_dayweek = $day_28_data_dayweekObj["day_28"];
		else if($devicetype == 1)
			$day_28_dayweek_ios = $day_28_data_dayweekObj["day_28"];
		else if($devicetype == 2)
			$day_28_dayweek_android = $day_28_data_dayweekObj["day_28"];
		else if($devicetype == 3)
			$day_28_dayweek_amazon = $day_28_data_dayweekObj["day_28"];
	}
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 12, 5, $day_28_dayweek, $day_28_dayweek_ios, $day_28_dayweek_android, $day_28_dayweek_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 13.슬롯순위
	$sql = "SELECT platform, slottype, rank, IF(pre_rank=0, 'new', (pre_rank-rank)) AS updownpoint 
			FROM `tbl_slot_rank_daily` 
			WHERE EXISTS ( 
				SELECT * 
				FROM ( 
					SELECT MAX(today) AS today FROM `tbl_slot_rank_daily`  WHERE TYPE=0 
				) t1 
				WHERE today = tbl_slot_rank_daily.today
				AND TYPE=0 
			) AND rank <= 5 
			AND TYPE = 0 
			GROUP BY platform, slottype 
			ORDER BY platform ASC, rank ASC";
	$slotRankList = $db_other->gettotallist($sql);
	$insert_col="";
	foreach ($slotRankList as $slotRankObj){
		$rank = $slotRankObj['rank'];
		$slottype = $slotRankObj['slottype'];
		$os_type = $slotRankObj['platform'];
		$updownpoint = $slotRankObj['updownpoint'];
		$slotname = $db_game->getvalue("SELECT slotname FROM tbl_slot_list WHERE slottype =".$slottype );
		
		if($updownpoint == 0)
		{
			$updownstr = "-";
		} 
		else if($updownpoint> 0)
		{
			$updownstr = "▲".$updownpoint;
		}
		else if($updownpoint< 0)
		{
			$updownpoint = $updownpoint*-1;
			$updownstr = "▼".$updownpoint;
		}
		else if($updownpoint == 'new')
		{
			$updownstr = "new";
		}
		
		if($os_type == 0)
			$insert_col = "web_sub3";
		else if($os_type == 1)
			$insert_col = "ios_sub3";
		else if($os_type == 2)
			$insert_col = "android_sub3";
		else if($os_type == 3)
			$insert_col = "amazon_sub3";
		
		$insert_str = $slotname."(".$updownstr.")";
		
		$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, $insert_col) ".
				"VALUES('$today', 13, $rank,'$insert_str') ".
				"ON DUPLICATE KEY UPDATE $insert_col=VALUES($insert_col);";
		$db_other->execute($sql);
	}
	
	// 14.VIP 방문자수 (Level 5~7)
	// 최근 52주
	$sql = "SELECT ROUND(AVG(usercount)) AS usercount ".
			" FROM ( ".
				" SELECT today, SUM(usercount) AS usercount ". 
				" FROM `tbl_user_playstat_gather_daily` ".
				" WHERE statcode IN (106, 107, 108) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
				" GROUP BY today ".
			" ) t1"; 
	$vip57_user_cnt_52week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(usercount)) AS usercount ".
			" FROM ( ".
				" SELECT today, SUM(usercount) AS usercount ". 
				" FROM `tbl_user_playstat_gather_daily_ios` ".
				" WHERE statcode IN (106, 107, 108) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
				" GROUP BY today ".
			" ) t1"; 
	$vip57_user_cnt_52week_ios = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(usercount)) AS usercount ".
			" FROM ( ".
				" SELECT today, SUM(usercount) AS usercount ". 
				" FROM `tbl_user_playstat_gather_daily_android` ".
				" WHERE statcode IN (106, 107, 108) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
				" GROUP BY today ".
			" ) t1"; 
	$vip57_user_cnt_52week_android = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(usercount)) AS usercount ".
			" FROM ( ".
				" SELECT today, SUM(usercount) AS usercount ". 
				" FROM `tbl_user_playstat_gather_daily_amazon` ".
				" WHERE statcode IN (106, 107, 108) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
				" GROUP BY today ".
			" ) t1"; 
	$vip57_user_cnt_52week_amazon = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 14, 1, $vip57_user_cnt_52week, $vip57_user_cnt_52week_ios, $vip57_user_cnt_52week_android, $vip57_user_cnt_52week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
					" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
					" FROM `tbl_user_playstat_daily` ".
					" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
					" GROUP BY today ".
					" ) t1"; 
	$vip57_user_cnt_13week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_ios` ".
			" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip57_user_cnt_13week_ios = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_android` ".
			" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip57_user_cnt_13week_android = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_amazon` ".
			" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip57_user_cnt_13week_amazon = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 14, 2, $vip57_user_cnt_13week, $vip57_user_cnt_13week_ios, $vip57_user_cnt_13week_android, $vip57_user_cnt_13week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
					" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
					" FROM `tbl_user_playstat_daily` ".
					" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
					" GROUP BY today ".
					" ) t1"; 
	$vip57_user_cnt_4week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_ios` ".
			" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip57_user_cnt_4week_ios = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_android` ".
			" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip57_user_cnt_4week_android = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_amazon` ".
			" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip57_user_cnt_4week_amazon = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 14, 3, $vip57_user_cnt_4week, $vip57_user_cnt_4week_ios, $vip57_user_cnt_4week_android, $vip57_user_cnt_4week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 1주
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
					" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
					" FROM `tbl_user_playstat_daily` ".
					" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
					" GROUP BY today ".
					" ) t1"; 
	$vip57_user_cnt_1week = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_ios` ".
			" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip57_user_cnt_1week_ios = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_android` ".
			" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip57_user_cnt_1week_android = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_amazon` ".
			" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip57_user_cnt_1week_amazon = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 14, 4, $vip57_user_cnt_1week, $vip57_user_cnt_1week_ios, $vip57_user_cnt_1week_android, $vip57_user_cnt_1week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 어제
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
					" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
					" FROM `tbl_user_playstat_daily` ".
					" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
					" GROUP BY today ".
					" ) t1"; 
	$vip57_user_cnt_day = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_ios` ".
			" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip57_user_cnt_day_ios = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_android` ".
			" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip57_user_cnt_day_android = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_amazon` ".
			" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip57_user_cnt_day_amazon = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 14, 5, $vip57_user_cnt_day, $vip57_user_cnt_day_ios, $vip57_user_cnt_day_android, $vip57_user_cnt_day_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ". 
			 " FROM (  ".
				" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
				" FROM `tbl_user_playstat_daily`  ".
				" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
				" AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today) ".
				" GROUP BY today ".
			   " ) t1"; 
	$vip57_user_cnt_dayweek = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ". 
			 " FROM (  ".
				" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ". 
				" FROM `tbl_user_playstat_daily_ios` ".
				" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
				" AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today) ".
				" GROUP BY today ".
			 " ) t1";
	$vip57_user_cnt_dayweek_ios = $db_other->getvalue($sql);
	
	$sql = "SELECT ROUND(AVG(user_cnt)) AS user_cnt ". 
			 " FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ". 
				" FROM `tbl_user_playstat_daily_android` ".
				" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
				" AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today) ".
				" GROUP BY today ".
			 " ) t1";
	$vip57_user_cnt_dayweek_android = $db_other->getvalue($sql);
	
	$sql = " SELECT ROUND(AVG(user_cnt)) AS user_cnt ". 
			 " FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ". 
				" FROM `tbl_user_playstat_daily_amazon` ".
				" WHERE vip_level IN (5,6,7) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
				" AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today) ".
				" GROUP BY today ".
			 " ) t1";
	$vip57_user_cnt_dayweek_amazon = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 14, 6, $vip57_user_cnt_dayweek, $vip57_user_cnt_dayweek_ios, $vip57_user_cnt_dayweek_android, $vip57_user_cnt_dayweek_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	

	// 15.VIP 방문자수 (Level 8~10)
	// 최근 52주
	$sql = "SELECT IFNULL(ROUND(AVG(usercount)),0) AS usercount ".
			" FROM ( ".
				" SELECT today, SUM(usercount) AS usercount ". 
				" FROM `tbl_user_playstat_gather_daily` ".
				" WHERE statcode IN (109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
				" GROUP BY today ".
			" ) t1"; 
	$vip810_user_cnt_52week = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(usercount)),0) AS usercount ".
			" FROM ( ".
				" SELECT today, SUM(usercount) AS usercount ". 
				" FROM `tbl_user_playstat_gather_daily_ios` ".
				" WHERE statcode IN (109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
				" GROUP BY today ".
			" ) t1"; 
	$vip810_user_cnt_52week_ios = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(usercount)),0) AS usercount ".
			" FROM ( ".
				" SELECT today, SUM(usercount) AS usercount ". 
				" FROM `tbl_user_playstat_gather_daily_android` ".
				" WHERE statcode IN (109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
				" GROUP BY today ".
			" ) t1"; 
	$vip810_user_cnt_52week_android = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(usercount)),0) AS usercount ".
			" FROM ( ".
				" SELECT today, SUM(usercount) AS usercount ". 
				" FROM `tbl_user_playstat_gather_daily_amazon` ".
				" WHERE statcode IN (109, 110, 111) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
				" GROUP BY today ".
			" ) t1"; 
	$vip810_user_cnt_52week_amazon = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 15, 1, $vip810_user_cnt_52week, $vip810_user_cnt_52week_ios, $vip810_user_cnt_52week_android, $vip810_user_cnt_52week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
					" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
					" FROM `tbl_user_playstat_daily` ".
					" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
					" GROUP BY today ".
					" ) t1"; 
	$vip810_user_cnt_13week = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_ios` ".
			" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip810_user_cnt_13week_ios = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_android` ".
			" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip810_user_cnt_13week_android = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_amazon` ".
			" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip810_user_cnt_13week_amazon = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 15, 2, $vip810_user_cnt_13week, $vip810_user_cnt_13week_ios, $vip810_user_cnt_13week_android, $vip810_user_cnt_13week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
					" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
					" FROM `tbl_user_playstat_daily` ".
					" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
					" GROUP BY today ".
					" ) t1"; 
	$vip810_user_cnt_4week = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_ios` ".
			" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip810_user_cnt_4week_ios = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_android` ".
			" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip810_user_cnt_4week_android = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_amazon` ".
			" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip810_user_cnt_4week_amazon = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 15, 3, $vip810_user_cnt_4week, $vip810_user_cnt_4week_ios, $vip810_user_cnt_4week_android, $vip810_user_cnt_4week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 1주
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
					" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
					" FROM `tbl_user_playstat_daily` ".
					" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
					" GROUP BY today ".
					" ) t1"; 
	$vip810_user_cnt_1week = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_ios` ".
			" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip810_user_cnt_1week_ios = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_android` ".
			" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip810_user_cnt_1week_android = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_amazon` ".
			" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip810_user_cnt_1week_amazon = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 15, 4, $vip810_user_cnt_1week, $vip810_user_cnt_1week_ios, $vip810_user_cnt_1week_android, $vip810_user_cnt_1week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 어제
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
					" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
					" FROM `tbl_user_playstat_daily` ".
					" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
					" GROUP BY today ".
					" ) t1"; 
	$vip810_user_cnt_day = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_ios` ".
			" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip810_user_cnt_day_ios = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_android` ".
			" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip810_user_cnt_day_android = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ".
			" FROM `tbl_user_playstat_daily_amazon` ".
			" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$vip810_user_cnt_day_amazon = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 15, 5, $vip810_user_cnt_day, $vip810_user_cnt_day_ios, $vip810_user_cnt_day_android, $vip810_user_cnt_day_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일통계
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ". 
			 " FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ". 
				" FROM `tbl_user_playstat_daily` ".
				" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
				" AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today) ".
				" GROUP BY today ".
			  " ) t1"; 
	$vip810_user_cnt_dayweek = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ". 
			 " FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ". 
				" FROM `tbl_user_playstat_daily_ios` ".
				" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
				" AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today) ".
				" GROUP BY today ".
			 " ) t1";
	$vip810_user_cnt_dayweek_ios = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ". 
			 " FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ". 
				" FROM `tbl_user_playstat_daily_android` ".
				" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
				" AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today) ".
				" GROUP BY today ".
			 " ) t1 ";
	$vip810_user_cnt_dayweek_android = $db_other->getvalue($sql);
	
	$sql = "SELECT IFNULL(ROUND(AVG(user_cnt)),0) AS user_cnt ". 
			 " FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS user_cnt ". 
				" FROM `tbl_user_playstat_daily_amazon` ".
				" WHERE vip_level IN (8,9,10) AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
				" AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today) ".
				" GROUP BY today ".
			 " ) t1 ";
	$vip810_user_cnt_dayweek_amazon = $db_other->getvalue($sql);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 15, 6, $vip810_user_cnt_dayweek, $vip810_user_cnt_dayweek_ios, $vip810_user_cnt_dayweek_android, $vip810_user_cnt_dayweek_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 16.VIP 결제자수 (Level 5~7)
	// 최근 52주
	//web
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(week,-52,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip57_payer_count_52week = $db_redshift->getvalue($sql);
	$vip57_payer_count_52week = ($vip57_payer_count_52week == '') ? 0 : $vip57_payer_count_52week;
	
	//ios
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-52,'$current_date'))  <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_52week_ios = $db_redshift->getvalue($sql);
	$vip57_payer_count_52week_ios = ($vip57_payer_count_52week_ios == '') ? 0 : $vip57_payer_count_52week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-52,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_52week_android = $db_redshift->getvalue($sql);
	$vip57_payer_count_52week_android = ($vip57_payer_count_52week_android == '') ? 0 : $vip57_payer_count_52week_android;
	
	//amazon
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-52,'$current_date'))  <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_52week_amazon = $db_redshift->getvalue($sql);
	$vip57_payer_count_52week_amazon = ($vip57_payer_count_52week_amazon == '') ? 0 : $vip57_payer_count_52week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 16, 1, $vip57_payer_count_52week, $vip57_payer_count_52week_ios, $vip57_payer_count_52week_android, $vip57_payer_count_52week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	
	//web
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(week,-13,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip57_payer_count_13week = $db_redshift->getvalue($sql);
	$vip57_payer_count_13week = ($vip57_payer_count_13week == '') ? 0 : $vip57_payer_count_13week;
	
	//ios
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-13,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_13week_ios = $db_redshift->getvalue($sql);
	$vip57_payer_count_13week_ios = ($vip57_payer_count_13week_ios == '') ? 0 : $vip57_payer_count_13week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-13,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_13week_android = $db_redshift->getvalue($sql);
	$vip57_payer_count_13week_android = ($vip57_payer_count_13week_android == '') ? 0 : $vip57_payer_count_13week_android;
	
	//amazon
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-13,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_13week_amazon = $db_redshift->getvalue($sql);
	$vip57_payer_count_13week_amazon = ($vip57_payer_count_13week_amazon == '') ? 0 : $vip57_payer_count_13week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 16, 2, $vip57_payer_count_13week, $vip57_payer_count_13week_ios, $vip57_payer_count_13week_android, $vip57_payer_count_13week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주
	//web
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily ".
			" WHERE date(dateadd(week,-4,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_4week = $db_redshift->getvalue($sql);
	$vip57_payer_count_4week = ($vip57_payer_count_4week == '') ? 0 : $vip57_payer_count_4week;
	
	//ios
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-4,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_4week_ios = $db_redshift->getvalue($sql);
	$vip57_payer_count_4week_ios = ($vip57_payer_count_4week_ios == '') ? 0 : $vip57_payer_count_4week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-4,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_4week_android = $db_redshift->getvalue($sql);
	$vip57_payer_count_4week_android = ($vip57_payer_count_4week_android == '') ? 0 : $vip57_payer_count_4week_android;
	
	//amazon
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-4,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_4week_amazon = $db_redshift->getvalue($sql);
	$vip57_payer_count_4week_amazon = ($vip57_payer_count_4week_amazon == '') ? 0 : $vip57_payer_count_4week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 16, 3, $vip57_payer_count_4week, $vip57_payer_count_4week_ios, $vip57_payer_count_4week_android, $vip57_payer_count_4week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 1주
	//web
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily ".
			" WHERE date(dateadd(week,-1,'$current_date'))  <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_1week = $db_redshift->getvalue($sql);
	$vip57_payer_count_1week = ($vip57_payer_count_1week == '') ? 0 : $vip57_payer_count_1week;
	
	//ios
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_1week_ios = $db_redshift->getvalue($sql);
	$vip57_payer_count_1week_ios = ($vip57_payer_count_1week_ios == '') ? 0 : $vip57_payer_count_1week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_1week_android = $db_redshift->getvalue($sql);
	$vip57_payer_count_1week_android = ($vip57_payer_count_1week_android == '') ? 0 : $vip57_payer_count_1week_android;
	
	//amazon
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-1,'$current_date'))  <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_1week_amazon = $db_redshift->getvalue($sql);
	$vip57_payer_count_1week_amazon = ($vip57_payer_count_1week_amazon == '') ? 0 : $vip57_payer_count_1week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 16, 4, $vip57_payer_count_1week, $vip57_payer_count_1week_ios, $vip57_payer_count_1week_android, $vip57_payer_count_1week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 어제
	//web
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily ".
			" WHERE date(dateadd(day,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_day = $db_redshift->getvalue($sql);
	$vip57_payer_count_day = ($vip57_payer_count_day == '') ? 0 : $vip57_payer_count_day;
	
	//ios
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(day,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_day_ios = $db_redshift->getvalue($sql);
	$vip57_payer_count_day_ios = ($vip57_payer_count_day_ios == '') ? 0 : $vip57_payer_count_day_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(day,-1,'$current_date'))  <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_day_android = $db_redshift->getvalue($sql);
	$vip57_payer_count_day_android = ($vip57_payer_count_day_android == '') ? 0 : $vip57_payer_count_day_android;
	
	//amazon
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(day,-1,'$current_date'))  <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_day_amazon = $db_redshift->getvalue($sql);
	$vip57_payer_count_day_amazon = ($vip57_payer_count_day_amazon == '') ? 0 : $vip57_payer_count_day_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 16, 5, $vip57_payer_count_day, $vip57_payer_count_day_ios, $vip57_payer_count_day_android, $vip57_payer_count_day_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일통계
	// web
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
					" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
					" FROM t5_user_playstat_daily ".
					" WHERE date(dateadd(week,-5,'$current_date'))  <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (5, 6, 7) ". 
					" AND date_part(dow, '$today') = date_part(dow, today) ".
					" AND purchasecount > 0 ".
					" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_dayweek = $db_redshift->getvalue($sql);
	$vip57_payer_count_dayweek = ($vip57_payer_count_dayweek == '') ? 0 : $vip57_payer_count_dayweek;
	
	//ios
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
					" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
					" FROM t5_user_playstat_daily_ios ".
					" WHERE date(dateadd(week,-5,'$current_date')) <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (5, 6, 7) ". 
					" AND date_part(dow, '$today') = date_part(dow, today) ".
					" AND purchasecount > 0 ".
					" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_dayweek_ios = $db_redshift->getvalue($sql);
	$vip57_payer_count_dayweek_ios = ($vip57_payer_count_dayweek_ios == '') ? 0 : $vip57_payer_count_dayweek_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
					" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
					" FROM t5_user_playstat_daily_android ".
					" WHERE date(dateadd(week,-5,'$current_date'))  <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (5, 6, 7) ". 
					" AND date_part(dow, '$today') = date_part(dow, today) ".
					" AND purchasecount > 0 ".
					" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_dayweek_android = $db_redshift->getvalue($sql);
	$vip57_payer_count_dayweek_android = ($vip57_payer_count_dayweek_android == '') ? 0 : $vip57_payer_count_dayweek_android;
	
	//amazon
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
					" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
					" FROM t5_user_playstat_daily_amazon ".
					" WHERE date(dateadd(week,-5,'$current_date')) <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (5, 6, 7) ". 
					" AND date_part(dow, '$today') = date_part(dow, today) ".
					" AND purchasecount > 0 ".
					" GROUP BY today ".
			" ) t1";
	$vip57_payer_count_dayweek_amazon = $db_redshift->getvalue($sql);
	$vip57_payer_count_dayweek_amazon = ($vip57_payer_count_dayweek_amazon == '') ? 0 : $vip57_payer_count_dayweek_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 16, 6, $vip57_payer_count_dayweek, $vip57_payer_count_dayweek_ios, $vip57_payer_count_dayweek_android, $vip57_payer_count_dayweek_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	
	// 17.VIP 결제자수 (Level 8~10)
	// 최근 52주
	
	// 최근 52주
	//web
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(week,-52,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip810_payer_count_52week = $db_redshift->getvalue($sql);
	$vip810_payer_count_52week = ($vip810_payer_count_52week == '') ? 0 : $vip810_payer_count_52week;
	
	//ios
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-52,'$current_date'))  <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_52week_ios = $db_redshift->getvalue($sql);
	$vip810_payer_count_52week_ios = ($vip810_payer_count_52week_ios == '') ? 0 : $vip810_payer_count_52week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-52,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_52week_android = $db_redshift->getvalue($sql);
	$vip810_payer_count_52week_android = ($vip810_payer_count_52week_android == '') ? 0 : $vip810_payer_count_52week_android;
	
	//amazon
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-52,'$current_date'))  <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_52week_amazon = $db_redshift->getvalue($sql);
	$vip810_payer_count_52week_amazon = ($vip810_payer_count_52week_amazon == '') ? 0 : $vip810_payer_count_52week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 17, 1, $vip810_payer_count_52week, $vip810_payer_count_52week_ios, $vip810_payer_count_52week_android, $vip810_payer_count_52week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	
	//web
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(week,-13,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip810_payer_count_13week = $db_redshift->getvalue($sql);
	$vip810_payer_count_13week = ($vip810_payer_count_13week == '') ? 0 : $vip810_payer_count_13week;
	
	//ios
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-13,'$current_date'))  <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_13week_ios = $db_redshift->getvalue($sql);
	$vip810_payer_count_13week_ios = ($vip810_payer_count_13week_ios == '') ? 0 : $vip810_payer_count_13week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-13,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_13week_android = $db_redshift->getvalue($sql);
	$vip810_payer_count_13week_android = ($vip810_payer_count_13week_android == '') ? 0 : $vip810_payer_count_13week_android;
	
	//amazon
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-13,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_13week_amazon = $db_redshift->getvalue($sql);
	$vip810_payer_count_13week_amazon = ($vip810_payer_count_13week_amazon == '') ? 0 : $vip810_payer_count_13week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 17, 2, $vip810_payer_count_13week, $vip810_payer_count_13week_ios, $vip810_payer_count_13week_android, $vip810_payer_count_13week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주
	
	//web
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(week,-4,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip810_payer_count_4week = $db_redshift->getvalue($sql);
	$vip810_payer_count_4week = ($vip810_payer_count_4week == '') ? 0 : $vip810_payer_count_4week;
	
	//ios
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-4,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_4week_ios = $db_redshift->getvalue($sql);
	$vip810_payer_count_4week_ios = ($vip810_payer_count_4week_ios == '') ? 0 : $vip810_payer_count_4week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-4,'$current_date'))  <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_4week_android = $db_redshift->getvalue($sql);
	$vip810_payer_count_4week_android = ($vip810_payer_count_4week_android == '') ? 0 : $vip810_payer_count_4week_android;
	
	//amazon
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-4,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_4week_amazon = $db_redshift->getvalue($sql);
	$vip810_payer_count_4week_amazon = ($vip810_payer_count_4week_amazon == '') ? 0 : $vip810_payer_count_4week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 17, 3, $vip810_payer_count_4week, $vip810_payer_count_4week_ios, $vip810_payer_count_4week_android, $vip810_payer_count_4week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 1주
	
	//web
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(week,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip810_payer_count_1week = $db_redshift->getvalue($sql);
	$vip810_payer_count_1week = ($vip810_payer_count_1week == '') ? 0 : $vip810_payer_count_1week;
	
	//ios
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_1week_ios = $db_redshift->getvalue($sql);
	$vip810_payer_count_1week_ios = ($vip810_payer_count_1week_ios == '') ? 0 : $vip810_payer_count_1week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_1week_android = $db_redshift->getvalue($sql);
	$vip810_payer_count_1week_android = ($vip810_payer_count_1week_android == '') ? 0 : $vip810_payer_count_1week_android;
	
	//amazon
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_1week_amazon = $db_redshift->getvalue($sql);
	$vip810_payer_count_1week_amazon = ($vip810_payer_count_1week_amazon == '') ? 0 : $vip810_payer_count_1week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 17, 4, $vip810_payer_count_1week, $vip810_payer_count_1week_ios, $vip810_payer_count_1week_android, $vip810_payer_count_1week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 어제
	//web
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily ".
			" WHERE date(dateadd(day,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_day = $db_redshift->getvalue($sql);
	$vip810_payer_count_day = ($vip810_payer_count_day == '') ? 0 : $vip810_payer_count_day;
	
	//ios
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(day,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_day_ios = $db_redshift->getvalue($sql);
	$vip810_payer_count_day_ios = ($vip810_payer_count_day_ios == '') ? 0 : $vip810_payer_count_day_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(day,-1,'$current_date'))  <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_day_android = $db_redshift->getvalue($sql);
	$vip810_payer_count_day_android = ($vip810_payer_count_day_android == '') ? 0 : $vip810_payer_count_day_android;
	
	//amazon
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(day,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_payer_count_day_amazon = $db_redshift->getvalue($sql);
	$vip810_payer_count_day_amazon = ($vip810_payer_count_day_amazon == '') ? 0 : $vip810_payer_count_day_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 17, 5, $vip810_payer_count_day, $vip810_payer_count_day_ios, $vip810_payer_count_day_android, $vip810_payer_count_day_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	//web
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ". 
			" FROM ( ".
			    	" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
			    	" FROM t5_user_playstat_daily  ".
			    	" WHERE date(dateadd(week,-5,'$current_date')) <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (8, 9, 10) ". 
			    	" AND date_part(dow, '$today') = date_part(dow, today) ".
			    	" AND purchasecount > 0 ".
			    	" GROUP BY today  ".
			" ) t1";
	$vip810_payer_count_dayweek = $db_redshift->getvalue($sql);
	$vip810_payer_count_dayweek = ($vip810_payer_count_dayweek == '') ? 0 : $vip810_payer_count_dayweek;
	
	//ios
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ". 
			" FROM ( ".
			    	" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
			    	" FROM t5_user_playstat_daily_ios  ".
			    	" WHERE date(dateadd(week,-5,'$current_date')) <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (8, 9, 10) ". 
			    	" AND date_part(dow, '$today') = date_part(dow, today) ".
			    	" AND purchasecount > 0 ".
			    	" GROUP BY today  ".
			" ) t1";
	$vip810_payer_count_dayweek_ios = $db_redshift->getvalue($sql);
	$vip810_payer_count_dayweek_ios = ($vip810_payer_count_dayweek_ios == '') ? 0 : $vip810_payer_count_dayweek_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ". 
			" FROM ( ".
			    	" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
			    	" FROM t5_user_playstat_daily_android  ".
			    	" WHERE date(dateadd(week,-5,'$current_date')) <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (8, 9, 10) ". 
			    	" AND date_part(dow, '$today') = date_part(dow, today) ".
			    	" AND purchasecount > 0 ".
			    	" GROUP BY today  ".
			" ) t1";
	$vip810_payer_count_dayweek_android = $db_redshift->getvalue($sql);
	$vip810_payer_count_dayweek_android = ($vip810_payer_count_dayweek_android == '') ? 0 : $vip810_payer_count_dayweek_android;
	
	//amazon
	$sql = "SELECT ROUND(AVG(payer_count)) AS payer_count ". 
			" FROM ( ".
			    	" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
			    	" FROM t5_user_playstat_daily_amazon ".
			    	" WHERE date(dateadd(week,-5,'$current_date'))  <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (8, 9, 10) ". 
			    	" AND date_part(dow, '$today') = date_part(dow, today) ".
			    	" AND purchasecount > 0 ".
			    	" GROUP BY today  ".
			" ) t1";
	$vip810_payer_count_dayweek_amazon = $db_redshift->getvalue($sql);
	$vip810_payer_count_dayweek_amazon = ($vip810_payer_count_dayweek_amazon == '') ? 0 : $vip810_payer_count_dayweek_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 17, 6, $vip810_payer_count_dayweek, $vip810_payer_count_dayweek_ios, $vip810_payer_count_dayweek_android, $vip810_payer_count_dayweek_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 18.VIP 결제율 (Level 5~7)
	// 최근 52주
	$vip57_payer_rate_52week = ($vip57_user_cnt_52week==0)?0:round($vip57_payer_count_52week/$vip57_user_cnt_52week*100,2);
	$vip57_payer_rate_52week_ios = ($vip57_user_cnt_52week_ios==0)?0:round($vip57_payer_count_52week_ios/$vip57_user_cnt_52week_ios*100,2);
	$vip57_payer_rate_52week_android = ($vip57_user_cnt_52week_android==0)?0:round($vip57_payer_count_52week_android/$vip57_user_cnt_52week_android*100,2);
	$vip57_payer_rate_52week_amazon = ($vip57_user_cnt_52week_amazon==0)?0:round($vip57_payer_count_52week_amazon/$vip57_user_cnt_52week_amazon*100,2);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 18, 1, $vip57_payer_rate_52week, $vip57_payer_rate_52week_ios, $vip57_payer_rate_52week_android, $vip57_payer_rate_52week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	// 최근 13주
	$vip57_payer_rate_13week = ($vip57_user_cnt_13week==0)?0:round($vip57_payer_count_13week/$vip57_user_cnt_13week*100,2);
	$vip57_payer_rate_13week_ios = ($vip57_user_cnt_13week_ios==0)?0:round($vip57_payer_count_13week_ios/$vip57_user_cnt_13week_ios*100,2);
	$vip57_payer_rate_13week_android = ($vip57_user_cnt_13week_android==0)?0:round($vip57_payer_count_13week_android/$vip57_user_cnt_13week_android*100,2);
	$vip57_payer_rate_13week_amazon = ($vip57_user_cnt_13week_amazon==0)?0:round($vip57_payer_count_13week_amazon/$vip57_user_cnt_13week_amazon*100,2);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 18, 2, $vip57_payer_rate_13week, $vip57_payer_rate_13week_ios, $vip57_payer_rate_13week_android, $vip57_payer_rate_13week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	// 최근 4주
	$vip57_payer_rate_4week = ($vip57_user_cnt_4week==0)?0:round($vip57_payer_count_4week/$vip57_user_cnt_4week*100,2);
	$vip57_payer_rate_4week_ios = ($vip57_user_cnt_4week_ios==0)?0:round($vip57_payer_count_4week_ios/$vip57_user_cnt_4week_ios*100,2);
	$vip57_payer_rate_4week_android = ($vip57_user_cnt_4week_android==0)?0:round($vip57_payer_count_4week_android/$vip57_user_cnt_4week_android*100,2);
	$vip57_payer_rate_4week_amazon = ($vip57_user_cnt_4week_amazon==0)?0:round($vip57_payer_count_4week_amazon/$vip57_user_cnt_4week_amazon*100,2);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 18, 3, $vip57_payer_rate_4week, $vip57_payer_rate_4week_ios, $vip57_payer_rate_4week_android, $vip57_payer_rate_4week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	// 최근 1주
	$vip57_payer_rate_1week = ($vip57_user_cnt_1week==0)?0:round($vip57_payer_count_1week/$vip57_user_cnt_1week*100,2);
	$vip57_payer_rate_1week_ios = ($vip57_user_cnt_1week_ios==0)?0:round($vip57_payer_count_1week_ios/$vip57_user_cnt_1week_ios*100,2);
	$vip57_payer_rate_1week_android = ($vip57_user_cnt_1week_android==0)?0:round($vip57_payer_count_1week_android/$vip57_user_cnt_1week_android*100,2);
	$vip57_payer_rate_1week_amazon = ($vip57_user_cnt_1week_amazon==0)?0:round($vip57_payer_count_1week_amazon/$vip57_user_cnt_1week_amazon*100,2);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 18, 4, $vip57_payer_rate_1week, $vip57_payer_rate_1week_ios, $vip57_payer_rate_1week_android, $vip57_payer_rate_1week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	// 어제
	$vip57_payer_rate_day = ($vip57_user_cnt_day==0)?0:round($vip57_payer_count_day/$vip57_user_cnt_day*100,2);
	$vip57_payer_rate_day_ios = ($vip57_user_cnt_day_ios==0)?0:round($vip57_payer_count_day_ios/$vip57_user_cnt_day_ios*100,2);
	$vip57_payer_rate_day_android = ($vip57_user_cnt_day_android==0)?0:round($vip57_payer_count_day_android/$vip57_user_cnt_day_android*100,2);
	$vip57_payer_rate_day_amazon = ($vip57_user_cnt_day_amazon==0)?0:round($vip57_payer_count_day_amazon/$vip57_user_cnt_day_amazon*100,2);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 18, 5, $vip57_payer_rate_day, $vip57_payer_rate_day_ios, $vip57_payer_rate_day_android, $vip57_payer_rate_day_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	$vip57_payer_rate_dayweek = ($vip57_user_cnt_dayweek==0)?0:round($vip57_payer_count_dayweek/$vip57_user_cnt_dayweek*100,2);
	$vip57_payer_rate_dayweek_ios = ($vip57_user_cnt_dayweek_ios==0)?0:round($vip57_payer_count_dayweek_ios/$vip57_user_cnt_dayweek_ios*100,2);
	$vip57_payer_rate_dayweek_android = ($vip57_user_cnt_dayweek_android==0)?0:round($vip57_payer_count_dayweek_android/$vip57_user_cnt_dayweek_android*100,2);
	$vip57_payer_rate_dayweek_amazon = ($vip57_user_cnt_dayweek_amazon==0)?0:round($vip57_payer_count_dayweek_amazon/$vip57_user_cnt_dayweek_amazon*100,2);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 18, 6, $vip57_payer_rate_dayweek, $vip57_payer_rate_dayweek_ios, $vip57_payer_rate_dayweek_android, $vip57_payer_rate_dayweek_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 19.VIP 결제율 (Level 8~10)
	$vip810_payer_rate_52week = ($vip810_user_cnt_52week==0)?0:round($vip810_payer_count_52week/$vip810_user_cnt_52week*100,2);
	$vip810_payer_rate_52week_ios = ($vip810_user_cnt_52week_ios==0)?0:round($vip810_payer_count_52week_ios/$vip810_user_cnt_52week_ios*100,2);
	$vip810_payer_rate_52week_android = ($vip810_user_cnt_52week_android==0)?0:round($vip810_payer_count_52week_android/$vip810_user_cnt_52week_android*100,2);
	$vip810_payer_rate_52week_amazon = ($vip810_user_cnt_52week_amazon==0)?0:round($vip810_payer_count_52week_amazon/$vip810_user_cnt_52week_amazon*100,2);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 19, 1, $vip810_payer_rate_52week, $vip810_payer_rate_52week_ios, $vip810_payer_rate_52week_android, $vip810_payer_rate_52week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	// 최근 13주
	$vip810_payer_rate_13week = ($vip810_user_cnt_13week==0)?0:round($vip810_payer_count_13week/$vip810_user_cnt_13week*100,2);
	$vip810_payer_rate_13week_ios = ($vip810_user_cnt_13week_ios==0)?0:round($vip810_payer_count_13week_ios/$vip810_user_cnt_13week_ios*100,2);
	$vip810_payer_rate_13week_android = ($vip810_user_cnt_13week_android==0)?0:round($vip810_payer_count_13week_android/$vip810_user_cnt_13week_android*100,2);
	$vip810_payer_rate_13week_amazon = ($vip810_user_cnt_13week_amazon==0)?0:round($vip810_payer_count_13week_amazon/$vip810_user_cnt_13week_amazon*100,2);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 19, 2, $vip810_payer_rate_13week, $vip810_payer_rate_13week_ios, $vip810_payer_rate_13week_android, $vip810_payer_rate_13week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	// 최근 4주
	$vip810_payer_rate_4week = ($vip810_user_cnt_4week==0)?0:round($vip810_payer_count_4week/$vip810_user_cnt_4week*100,2);
	$vip810_payer_rate_4week_ios = ($vip810_user_cnt_4week_ios==0)?0:round($vip810_payer_count_4week_ios/$vip810_user_cnt_4week_ios*100,2);
	$vip810_payer_rate_4week_android = ($vip810_user_cnt_4week_android==0)?0:round($vip810_payer_count_4week_android/$vip810_user_cnt_4week_android*100,2);
	$vip810_payer_rate_4week_amazon = ($vip810_user_cnt_4week_amazon==0)?0:round($vip810_payer_count_4week_amazon/$vip810_user_cnt_4week_amazon*100,2);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 19, 3, $vip810_payer_rate_4week, $vip810_payer_rate_4week_ios, $vip810_payer_rate_4week_android, $vip810_payer_rate_4week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	// 최근 1주
	$vip810_payer_rate_1week = ($vip810_user_cnt_1week==0)?0:round($vip810_payer_count_1week/$vip810_user_cnt_1week*100,2);
	$vip810_payer_rate_1week_ios = ($vip810_user_cnt_1week_ios==0)?0:round($vip810_payer_count_1week_ios/$vip810_user_cnt_1week_ios*100,2);
	$vip810_payer_rate_1week_android = ($vip810_user_cnt_1week_android==0)?0:round($vip810_payer_count_1week_android/$vip810_user_cnt_1week_android*100,2);
	$vip810_payer_rate_1week_amazon = ($vip810_user_cnt_1week_amazon==0)?0:round($vip810_payer_count_1week_amazon/$vip810_user_cnt_1week_amazon*100,2);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 19, 4, $vip810_payer_rate_1week, $vip810_payer_rate_1week_ios, $vip810_payer_rate_1week_android, $vip810_payer_rate_1week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 어제
	$vip810_payer_rate_day = ($vip810_user_cnt_day==0)?0:round($vip810_payer_count_day/$vip810_user_cnt_day*100,2);
	$vip810_payer_rate_day_ios = ($vip810_user_cnt_day_ios==0)?0:round($vip810_payer_count_day_ios/$vip810_user_cnt_day_ios*100,2);
	$vip810_payer_rate_day_android = ($vip810_user_cnt_day_android==0)?0:round($vip810_payer_count_day_android/$vip810_user_cnt_day_android*100,2);
	$vip810_payer_rate_day_amazon = ($vip810_user_cnt_day_amazon==0)?0:round($vip810_payer_count_day_amazon/$vip810_user_cnt_day_amazon*100,2);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 19, 5, $vip810_payer_rate_day, $vip810_payer_rate_day_ios, $vip810_payer_rate_day_android, $vip810_payer_rate_day_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	$vip810_payer_rate_dayweek = ($vip810_user_cnt_dayweek ==0)?0:round($vip810_payer_count_dayweek/$vip810_user_cnt_dayweek*100,2);
	$vip810_payer_rate_dayweek_ios = ($vip810_user_cnt_dayweek_ios==0)?0:round($vip810_payer_count_dayweek_ios/$vip810_user_cnt_dayweek_ios*100,2);
	$vip810_payer_rate_dayweek_android = ($vip810_user_cnt_dayweek_android==0)?0:round($vip810_payer_count_dayweek_android/$vip810_user_cnt_dayweek_android*100,2);
	$vip810_payer_rate_dayweek_amazon = ($vip810_user_cnt_dayweek_amazon==0)?0:round($vip810_payer_count_dayweek_amazon/$vip810_user_cnt_dayweek_amazon*100,2);
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 19, 6, $vip810_payer_rate_dayweek, $vip810_payer_rate_dayweek_ios, $vip810_payer_rate_dayweek_android, $vip810_payer_rate_dayweek_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 20.VIP 평균 결제 금액 (Level 5~7)
	// 최근 52주
	//web
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(week,-52,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip57_avg_money_52week = $db_redshift->getvalue($sql);
	$vip57_avg_money_52week = ($vip57_avg_money_52week == '') ? 0 : $vip57_avg_money_52week;
	
	//ios
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-52,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_avg_money_52week_ios = $db_redshift->getvalue($sql);
	$vip57_avg_money_52week_ios = ($vip57_avg_money_52week_ios == '') ? 0 : $vip57_avg_money_52week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-52,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_avg_money_52week_android = $db_redshift->getvalue($sql);
	$vip57_avg_money_52week_android = ($vip57_avg_money_52week_android == '') ? 0 : $vip57_avg_money_52week_android;
	//amazon
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-52,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_avg_money_52week_amazon = $db_redshift->getvalue($sql);
	$vip57_avg_money_52week_amazon = ($vip57_avg_money_52week_amazon == '') ? 0 : $vip57_avg_money_52week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 20, 1, $vip57_avg_money_52week, $vip57_avg_money_52week_ios, $vip57_avg_money_52week_android, $vip57_avg_money_52week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	//web
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(week,-13,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip57_avg_money_13week = $db_redshift->getvalue($sql);
	$vip57_avg_money_13week = ($vip57_avg_money_13week == '') ? 0 : $vip57_avg_money_13week;
	
	//ios
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-13,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_avg_money_13week_ios = $db_redshift->getvalue($sql);
	$vip57_avg_money_13week_ios = ($vip57_avg_money_13week_ios == '') ? 0 : $vip57_avg_money_13week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-13,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_avg_money_13week_android = $db_redshift->getvalue($sql);
	$vip57_avg_money_13week_android = ($vip57_avg_money_13week_android == '') ? 0 : $vip57_avg_money_13week_android;
	//amazon
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-13,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_avg_money_13week_amazon = $db_redshift->getvalue($sql);
	$vip57_avg_money_13week_amazon = ($vip57_avg_money_13week_amazon == '') ? 0 : $vip57_avg_money_13week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 20, 2, $vip57_avg_money_13week, $vip57_avg_money_13week_ios, $vip57_avg_money_13week_android, $vip57_avg_money_13week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	
	// 최근 4주
	//web
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(week,-4,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip57_avg_money_4week = $db_redshift->getvalue($sql);
	$vip57_avg_money_4week = ($vip57_avg_money_4week == '') ? 0 : $vip57_avg_money_4week;
	
	//ios
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-4,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_avg_money_4week_ios = $db_redshift->getvalue($sql);
	$vip57_avg_money_4week_ios = ($vip57_avg_money_4week_ios == '') ? 0 : $vip57_avg_money_4week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-4,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_avg_money_4week_android = $db_redshift->getvalue($sql);
	$vip57_avg_money_4week_android = ($vip57_avg_money_4week_android == '') ? 0 : $vip57_avg_money_4week_android;
	//amazon
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-4,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_avg_money_4week_amazon = $db_redshift->getvalue($sql);
	$vip57_avg_money_4week_amazon = ($vip57_avg_money_4week_amazon == '') ? 0 : $vip57_avg_money_4week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 20, 3, $vip57_avg_money_4week, $vip57_avg_money_4week_ios, $vip57_avg_money_4week_android, $vip57_avg_money_4week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	
	// 최근 1주
	//web
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(week,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip57_avg_money_1week = $db_redshift->getvalue($sql);
	$vip57_avg_money_1week = ($vip57_avg_money_1week == '') ? 0 : $vip57_avg_money_1week;
	
	//ios
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_avg_money_1week_ios = $db_redshift->getvalue($sql);
	$vip57_avg_money_1week_ios = ($vip57_avg_money_1week_ios == '') ? 0 : $vip57_avg_money_1week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_avg_money_1week_android = $db_redshift->getvalue($sql);
	$vip57_avg_money_1week_android = ($vip57_avg_money_1week_android == '') ? 0 : $vip57_avg_money_1week_android;
	//amazon
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_avg_money_1week_amazon = $db_redshift->getvalue($sql);
	$vip57_avg_money_1week_amazon = ($vip57_avg_money_1week_amazon == '') ? 0 : $vip57_avg_money_1week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 20, 4, $vip57_avg_money_1week, $vip57_avg_money_1week_ios, $vip57_avg_money_1week_android, $vip57_avg_money_1week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	
	// 어제
	//web
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(day,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip57_avg_money_day = $db_redshift->getvalue($sql);
	$vip57_avg_money_day = ($vip57_avg_money_day == '') ? 0 : $vip57_avg_money_day;
	
	//ios
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(day,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_avg_money_day_ios = $db_redshift->getvalue($sql);
	$vip57_avg_money_day_ios  = ($vip57_avg_money_day_ios  == '') ? 0 : $vip57_avg_money_day_ios ;
	
	//android
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(day,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_avg_money_day_android = $db_redshift->getvalue($sql);
	$vip57_avg_money_day_android  = ($vip57_avg_money_day_android  == '') ? 0 : $vip57_avg_money_day_android ;
	//amazon
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(day,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (5, 6, 7) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip57_avg_money_day_amazon = $db_redshift->getvalue($sql);
	$vip57_avg_money_day_amazon = ($vip57_avg_money_day_amazon == '') ? 0 : $vip57_avg_money_day_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 20, 5, $vip57_avg_money_day, $vip57_avg_money_day_ios, $vip57_avg_money_day_android, $vip57_avg_money_day_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	//web
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ". 
			" FROM ( ".
      				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
      				" FROM t5_user_playstat_daily ".
      				" WHERE date(dateadd(week,-5,'$current_date')) <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (5, 6, 7) ". 
             		" AND date_part(dow, '$today') = date_part(dow, today) ".
      				" AND purchasecount > 0 ".
      				" GROUP BY today  ".
			" ) t1";
	$vip57_avg_money_dayweek = $db_redshift->getvalue($sql);
	$vip57_avg_money_dayweek = ($vip57_avg_money_dayweek == '') ? 0 : $vip57_avg_money_dayweek;
	
	//ios
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ". 
			" FROM ( ".
      				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
      				" FROM t5_user_playstat_daily_ios ".
      				" WHERE date(dateadd(week,-5,'$current_date')) <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (5, 6, 7) ". 
             		" AND date_part(dow, '$today') = date_part(dow, today) ".
      				" AND purchasecount > 0 ".
      				" GROUP BY today  ".
			" ) t1";
	$vip57_avg_money_dayweek_ios = $db_redshift->getvalue($sql);
	$vip57_avg_money_dayweek_ios  = ($vip57_avg_money_dayweek_ios  == '') ? 0 : $vip57_avg_money_dayweek_ios ;
	
	//android
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ". 
			" FROM ( ".
      				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
      				" FROM t5_user_playstat_daily_android ".
      				" WHERE date(dateadd(week,-5,'$current_date')) <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (5, 6, 7) ". 
             		" AND date_part(dow, '$today') = date_part(dow, today) ".
      				" AND purchasecount > 0 ".
      				" GROUP BY today  ".
			" ) t1";
	$vip57_avg_money_dayweek_android = $db_redshift->getvalue($sql);
	$vip57_avg_money_dayweek_android  = ($vip57_avg_money_dayweek_android  == '') ? 0 : $vip57_avg_money_dayweek_android ;
	
	//amazon
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ". 
			" FROM ( ".
      				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
      				" FROM t5_user_playstat_daily_amazon ".
      				" WHERE date(dateadd(week,-5,'$current_date')) <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (5, 6, 7) ". 
             		" AND date_part(dow, '$today') = date_part(dow, today) ".
      				" AND purchasecount > 0 ".
      				" GROUP BY today  ".
			" ) t1";
	$vip57_avg_money_dayweek_amazon = $db_redshift->getvalue($sql);
	$vip57_avg_money_dayweek_amazon = ($vip57_avg_money_dayweek_amazon == '') ? 0 : $vip57_avg_money_dayweek_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 20, 6, $vip57_avg_money_dayweek, $vip57_avg_money_dayweek_ios, $vip57_avg_money_dayweek_android, $vip57_avg_money_dayweek_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	
	// 21.VIP 평균 결제 금액 (Level 8~10)
	// 최근 52주
	//web
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(week,-52,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip810_avg_money_52week = $db_redshift->getvalue($sql);
	$vip810_avg_money_52week = ($vip810_avg_money_52week == '') ? 0 : $vip810_avg_money_52week;
	
	//ios
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-52,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_avg_money_52week_ios = $db_redshift->getvalue($sql);
	$vip810_avg_money_52week_ios = ($vip810_avg_money_52week_ios == '') ? 0 : $vip810_avg_money_52week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-52,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_avg_money_52week_android = $db_redshift->getvalue($sql);
	$vip810_avg_money_52week_android = ($vip810_avg_money_52week_android == '') ? 0 : $vip810_avg_money_52week_android;
	//amazon
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-52,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_avg_money_52week_amazon = $db_redshift->getvalue($sql);
	$vip810_avg_money_52week_amazon = ($vip810_avg_money_52week_amazon == '') ? 0 : $vip810_avg_money_52week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 21, 1, $vip810_avg_money_52week, $vip810_avg_money_52week_ios, $vip810_avg_money_52week_android, $vip810_avg_money_52week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 13주
	//web
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(week,-13,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip810_avg_money_13week = $db_redshift->getvalue($sql);
	$vip810_avg_money_13week  = ($vip810_avg_money_13week  == '') ? 0 : $vip810_avg_money_13week ;
	
	//ios
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-13,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_avg_money_13week_ios = $db_redshift->getvalue($sql);
	$vip810_avg_money_13week_ios = ($vip810_avg_money_13week_ios == '') ? 0 : $vip810_avg_money_13week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-13,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_avg_money_13week_android = $db_redshift->getvalue($sql);
	$vip810_avg_money_13week_android = ($vip810_avg_money_13week_android == '') ? 0 : $vip810_avg_money_13week_android;
	//amazon
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-13,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_avg_money_13week_amazon = $db_redshift->getvalue($sql);
	$vip810_avg_money_13week_amazon = ($vip810_avg_money_13week_amazon == '') ? 0 : $vip810_avg_money_13week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 21, 2, $vip810_avg_money_13week , $vip810_avg_money_13week_ios, $vip810_avg_money_13week_android, $vip810_avg_money_13week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	
	// 최근 4주
	//web
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(week,-4,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip810_avg_money_4week = $db_redshift->getvalue($sql);
	$vip810_avg_money_4week  = ($vip810_avg_money_4week  == '') ? 0 : $vip810_avg_money_4week ;
	
	//ios
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-4,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_avg_money_4week_ios = $db_redshift->getvalue($sql);
	$vip810_avg_money_4week_ios = ($vip810_avg_money_4week_ios == '') ? 0 : $vip810_avg_money_4week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-4,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_avg_money_4week_android = $db_redshift->getvalue($sql);
	$vip810_avg_money_4week_android = ($vip810_avg_money_4week_android == '') ? 0 : $vip810_avg_money_4week_android;
	//amazon
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-4,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_avg_money_4week_amazon = $db_redshift->getvalue($sql);
	$vip810_avg_money_4week_amazon = ($vip810_avg_money_4week_amazon == '') ? 0 : $vip810_avg_money_4week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 21, 3, $vip810_avg_money_4week, $vip810_avg_money_4week_ios, $vip810_avg_money_4week_android, $vip810_avg_money_4week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	
	// 최근 1주
	//web
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(week,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip810_avg_money_1week = $db_redshift->getvalue($sql);
	$vip810_avg_money_1week  = ($vip810_avg_money_1week  == '') ? 0 : $vip810_avg_money_1week ;
	
	//ios
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(week,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_avg_money_1week_ios = $db_redshift->getvalue($sql);
	$vip810_avg_money_1week_ios = ($vip810_avg_money_1week_ios == '') ? 0 : $vip810_avg_money_1week_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(week,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_avg_money_1week_android = $db_redshift->getvalue($sql);
	$vip810_avg_money_1week_android = ($vip810_avg_money_1week_android == '') ? 0 : $vip810_avg_money_1week_android;
	//amazon
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(week,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_avg_money_1week_amazon = $db_redshift->getvalue($sql);
	$vip810_avg_money_1week_amazon = ($vip810_avg_money_1week_amazon == '') ? 0 : $vip810_avg_money_1week_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 21, 4, $vip810_avg_money_1week, $vip810_avg_money_1week_ios, $vip810_avg_money_1week_android, $vip810_avg_money_1week_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	
	// 어제
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
				" FROM t5_user_playstat_daily ".
				" WHERE date(dateadd(day,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
				" AND purchasecount > 0 ".
				" GROUP BY today ". 
			" ) t1";
	$vip810_avg_money_day = $db_redshift->getvalue($sql);
	$vip810_avg_money_day  = ($vip810_avg_money_day  == '') ? 0 : $vip810_avg_money_day ;
	
	//ios
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_ios ".
			" WHERE date(dateadd(day,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_avg_money_day_ios = $db_redshift->getvalue($sql);
	$vip810_avg_money_day_ios = ($vip810_avg_money_day_ios == '') ? 0 : $vip810_avg_money_day_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_android ".
			" WHERE date(dateadd(day,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_avg_money_day_android = $db_redshift->getvalue($sql);
	$vip810_avg_money_day_android = ($vip810_avg_money_day_android == '') ? 0 : $vip810_avg_money_day_android;
	//amazon
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ".
			" FROM ( ".
			" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ".
			" FROM t5_user_playstat_daily_amazon ".
			" WHERE date(dateadd(day,-1,'$current_date')) <= today AND today < '$current_date' AND vip_level IN (8, 9, 10) ".
			" AND purchasecount > 0 ".
			" GROUP BY today ".
			" ) t1";
	$vip810_avg_money_day_amazon = $db_redshift->getvalue($sql);
	$vip810_avg_money_day_amazon = ($vip810_avg_money_day_amazon == '') ? 0 : $vip810_avg_money_day_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 21, 5, $vip810_avg_money_day, $vip810_avg_money_day_ios, $vip810_avg_money_day_android, $vip810_avg_money_day_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ". 
			" FROM ( ".
      				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
      				" FROM t5_user_playstat_daily ".
      				" WHERE date(dateadd(week,-5,'$current_date')) <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (8, 9, 10) ".
             		" AND date_part(dow, '$today') = date_part(dow, today) ".
      				" AND purchasecount > 0 ".
      				" GROUP BY today  ".
			" ) t1";
	$vip810_avg_money_dayweek = $db_redshift->getvalue($sql);
	$vip810_avg_money_dayweek  = ($vip810_avg_money_dayweek  == '') ? 0 : $vip810_avg_money_dayweek ;
	
	//ios
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ". 
			" FROM ( ".
      				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
      				" FROM t5_user_playstat_daily_ios ".
      				" WHERE date(dateadd(week,-5,'$current_date'))  <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (8, 9, 10) ".
             		" AND date_part(dow, '$today') = date_part(dow, today) ".
      				" AND purchasecount > 0 ".
      				" GROUP BY today  ".
			" ) t1";
	$vip810_avg_money_dayweek_ios = $db_redshift->getvalue($sql);
	$vip810_avg_money_dayweek_ios = ($vip810_avg_money_dayweek_ios == '') ? 0 : $vip810_avg_money_dayweek_ios;
	
	//android
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ". 
			" FROM ( ".
      				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
      				" FROM t5_user_playstat_daily_android ".
      				" WHERE date(dateadd(week,-5,'$current_date')) <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (8, 9, 10) ".
             		" AND date_part(dow, '$today') = date_part(dow, today) ".
      				" AND purchasecount > 0 ".
      				" GROUP BY today  ".
			" ) t1";
	$vip810_avg_money_dayweek_android = $db_redshift->getvalue($sql);
	$vip810_avg_money_dayweek_android = ($vip810_avg_money_dayweek_android == '') ? 0 : $vip810_avg_money_dayweek_android;
	
	//amazon
	$sql = "SELECT ROUND(AVG(avg_money), 2) AS avg_money ". 
			" FROM ( ".
      				" SELECT today, COUNT(DISTINCT useridx) AS payer_count, SUM(purchaseamount/10)::FLOAT/COUNT(DISTINCT useridx)::FLOAT AS avg_money ". 
      				" FROM t5_user_playstat_daily_amazon ".
      				" WHERE date(dateadd(week,-5,'$current_date')) <= today AND today < date(dateadd(week,-1,'$current_date')) AND vip_level IN (8, 9, 10) ".
             		" AND date_part(dow, '$today') = date_part(dow, today) ".
      				" AND purchasecount > 0 ".
      				" GROUP BY today  ".
			" ) t1";
	$vip810_avg_money_dayweek_amazon = $db_redshift->getvalue($sql);
	$vip810_avg_money_dayweek_amazon = ($vip810_avg_money_dayweek_amazon == '') ? 0 : $vip810_avg_money_dayweek_amazon;
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, ios, android, amazon) ".
			"VALUES('$today', 21, 6, $vip810_avg_money_dayweek, $vip810_avg_money_dayweek_ios, $vip810_avg_money_dayweek_android, $vip810_avg_money_dayweek_amazon) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), ios=VALUES(ios), android=VALUES(android), amazon=VALUES(amazon);";
	$db_other->execute($sql);
	
	
	// 22.전체 VIP 유저수 
	// 최근 52주
	$sql = "SELECT ROUND(AVG(vip_level56)) AS vip_level56, ROUND(AVG(vip_level78)) AS vip_level78, ROUND(AVG(vip_level910)) AS vip_level910 ".
			" FROM ( ".
				" SELECT today, SUM(vip_level_5 + vip_level_6) AS vip_level56,SUM(vip_level_7 + vip_level_8) AS vip_level78,SUM(vip_level_9 + vip_level_10) AS vip_level910 ".
				" FROM `tbl_vip_level_stat_log` ".
				" WHERE TYPE = 0 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 52 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
				" GROUP BY today ".
			" ) t1";
	$total_vip_count_52week = $db_analysis->getarray($sql);
	
	$total_vip56_count_52week = $total_vip_count_52week['vip_level56'];
	$total_vip78_count_52week = $total_vip_count_52week['vip_level78'];
	$total_vip910_count_52week = $total_vip_count_52week['vip_level910'];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, web_sub1, web_sub2) ".
			"VALUES('$today', 22, 1, $total_vip56_count_52week, $total_vip78_count_52week, $total_vip910_count_52week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2);";
	$db_other->execute($sql);
	// 최근 13주
	$sql = "SELECT ROUND(AVG(vip_level56)) AS vip_level56, ROUND(AVG(vip_level78)) AS vip_level78, ROUND(AVG(vip_level910)) AS vip_level910 ".
			" FROM ( ".
			" SELECT today, SUM(vip_level_5 + vip_level_6) AS vip_level56,SUM(vip_level_7 + vip_level_8) AS vip_level78,SUM(vip_level_9 + vip_level_10) AS vip_level910 ".
			" FROM `tbl_vip_level_stat_log` ".
			" WHERE TYPE = 0 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 13 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$total_vip_count_13week = $db_analysis->getarray($sql);
	
	$total_vip56_count_13week = $total_vip_count_13week['vip_level56'];
	$total_vip78_count_13week = $total_vip_count_13week['vip_level78'];
	$total_vip910_count_13week = $total_vip_count_13week['vip_level910'];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, web_sub1, web_sub2) ".
			"VALUES('$today', 22, 2, $total_vip56_count_13week, $total_vip78_count_13week, $total_vip910_count_13week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2);";
	$db_other->execute($sql);
	// 최근 4주
	$sql = "SELECT ROUND(AVG(vip_level56)) AS vip_level56, ROUND(AVG(vip_level78)) AS vip_level78, ROUND(AVG(vip_level910)) AS vip_level910 ".
			" FROM ( ".
			" SELECT today, SUM(vip_level_5 + vip_level_6) AS vip_level56,SUM(vip_level_7 + vip_level_8) AS vip_level78,SUM(vip_level_9 + vip_level_10) AS vip_level910 ".
			" FROM `tbl_vip_level_stat_log` ".
			" WHERE TYPE = 0 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 4 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$total_vip_count_4week = $db_analysis->getarray($sql);
	
	$total_vip56_count_4week = $total_vip_count_4week['vip_level56'];
	$total_vip78_count_4week = $total_vip_count_4week['vip_level78'];
	$total_vip910_count_4week = $total_vip_count_4week['vip_level910'];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, web_sub1, web_sub2) ".
			"VALUES('$today', 22, 3, $total_vip56_count_4week, $total_vip78_count_4week, $total_vip910_count_4week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2);";
	$db_other->execute($sql);
	// 최근 1주
	$sql = "SELECT ROUND(AVG(vip_level56)) AS vip_level56, ROUND(AVG(vip_level78)) AS vip_level78, ROUND(AVG(vip_level910)) AS vip_level910 ".
			" FROM ( ".
			" SELECT today, SUM(vip_level_5 + vip_level_6) AS vip_level56,SUM(vip_level_7 + vip_level_8) AS vip_level78,SUM(vip_level_9 + vip_level_10) AS vip_level910 ".
			" FROM `tbl_vip_level_stat_log` ".
			" WHERE TYPE = 0 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$total_vip_count_1week = $db_analysis->getarray($sql);
	
	$total_vip56_count_1week = $total_vip_count_1week['vip_level56'];
	$total_vip78_count_1week = $total_vip_count_1week['vip_level78'];
	$total_vip910_count_1week = $total_vip_count_1week['vip_level910'];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, web_sub1, web_sub2) ".
			"VALUES('$today', 22, 4, $total_vip56_count_1week, $total_vip78_count_1week, $total_vip910_count_1week) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2);";
	$db_other->execute($sql);
	// 어제
	$sql = "SELECT ROUND(AVG(vip_level56)) AS vip_level56, ROUND(AVG(vip_level78)) AS vip_level78, ROUND(AVG(vip_level910)) AS vip_level910 ".
			" FROM ( ".
			" SELECT today, SUM(vip_level_5 + vip_level_6) AS vip_level56,SUM(vip_level_7 + vip_level_8) AS vip_level78,SUM(vip_level_9 + vip_level_10) AS vip_level910 ".
			" FROM `tbl_vip_level_stat_log` ".
			" WHERE TYPE = 0 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(NOW(), '%Y-%m-%d') ".
			" GROUP BY today ".
			" ) t1";
	$total_vip_count_day = $db_analysis->getarray($sql);
	
	$total_vip56_count_day = $total_vip_count_day['vip_level56'];
	$total_vip78_count_day = $total_vip_count_day['vip_level78'];
	$total_vip910_count_day = $total_vip_count_day['vip_level910'];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, web_sub1, web_sub2) ".
			"VALUES('$today', 22, 5, $total_vip56_count_day, $total_vip78_count_day, $total_vip910_count_day) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2);";
	$db_other->execute($sql);
	
	// 최근 4주 요일 통계
	$sql = "SELECT ROUND(AVG(vip_level56)) AS vip_level56, ROUND(AVG(vip_level78)) AS vip_level78, ROUND(AVG(vip_level910)) AS vip_level910 ". 
			 " FROM ( ".
				 " SELECT today, SUM(vip_level_5 + vip_level_6) AS vip_level56,SUM(vip_level_7 + vip_level_8) AS vip_level78,SUM(vip_level_9 + vip_level_10) AS vip_level910 ". 
				 " FROM `tbl_vip_level_stat_log` ". 
				 " WHERE TYPE = 0 AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 5 WEEK), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 WEEK), '%Y-%m-%d') ".
				 " AND DAYOFWEEK(DATE_SUB(NOW(), INTERVAL 1 DAY)) = DAYOFWEEK(today) ".
				 " GROUP BY today ".
			 " ) t1";
	$total_vip_count_dayweek = $db_analysis->getarray($sql);
	
	$total_vip56_count_dayweek = $total_vip_count_dayweek['vip_level56'];
	$total_vip78_count_dayweek = $total_vip_count_dayweek['vip_level78'];
	$total_vip910_count_dayweek = $total_vip_count_dayweek['vip_level910'];
	
	$sql = "INSERT INTO tbl_daily_basic_stat(today, type, subtype, web, web_sub1, web_sub2) ".
			"VALUES('$today', 22, 6, $total_vip56_count_dayweek, $total_vip78_count_dayweek, $total_vip910_count_dayweek) ".
			"ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2);";
	$db_other->execute($sql);
	
	$db_slave->end();
	$db2->end();
	$db_analysis->end();
	$db_other->end();
	$db_redshift->end();
	$db_game->end();
?>