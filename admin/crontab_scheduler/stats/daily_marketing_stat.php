<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/stats/daily_marketing_stat") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/stats/daily_marketing_stat") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("stats/daily_marketing_stat Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	$db_slave = new CDatabase_Slave_Main();
	$db2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_other = new CDatabase_Other();
	$db_game = new CDatabase_Game();
	$db_redshift = new CDatabase_Redshift();
	
	$today = date("Y-m-d", time() - 60 * 60 * 24 * 1);
	$current_date = date("Y-m-d");
	
	// 1.ARPI
	// 최근 1주
	$sql = "SELECT count(useridx) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 ".
			" AND (adflag like 'fbself%' OR adflag like 'nanigans%') "; 
	$web_arpi_1week = $db_redshift->getvalue($sql);
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM tbl_ad_stats ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*1) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') ".
			" ) t1"; 
	$web_cost_1week = $db2->getvalue($sql);
	$web_cost_1week = ($web_cost_1week=="")?0:$web_cost_1week;
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 ". 
			"      and (adflag like 'fbself%' OR adflag like 'nanigans%')  ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 ".
			"      and (adflag like 'fbself%' OR adflag like 'nanigans%')  ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$web_sales_1week = $db_redshift->getvalue($sql);
	$web_sales_1week = ($web_sales_1week=="")?0:$web_sales_1week;
	
	$sql = " SELECT count(distinct deviceid) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			" AND (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%')"; 
	$ios_arpi_1week = $db_redshift->getvalue($sql);
	$ios_arpi_1week = ($ios_arpi_1week=="")?0:$ios_arpi_1week;
	
	$sql = "SELECT SUM(spend) AS spend ".
			"FROM ( ".
			" SELECT SUM(spend) AS spend ".
			" FROM tbl_agency_spend_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*1) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 1 AND agencyname NOT LIKE 'Face%' ".
			" UNION ALL ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM tbl_ad_stats_mobile ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*1) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 1  ".
			" ) t1"; 
	$ios_cost_1week = $db2->getvalue($sql);
	$ios_cost_1week = ($ios_cost_1week=="")?0:$ios_cost_1week;
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user ".
			"    WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user ".
			"    WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"  where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$ios_sales_1week = $db_redshift->getvalue($sql);
	$ios_sales_1week = ($ios_sales_1week=="")?0:$ios_sales_1week;
	
	$sql = "SELECT count(distinct deviceid) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"  and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%')"; 
	$and_arpi_1week = $db_redshift->getvalue($sql);
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT SUM(spend) AS spend ".
			" FROM tbl_agency_spend_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*1) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 2 AND agencyname NOT LIKE 'Face%' ".
			" UNION ALL ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM tbl_ad_stats_mobile ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*1) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 2 ".
			" ) t1"; 
	$and_cost_1week = $db2->getvalue($sql);
	$and_cost_1week = ($and_cost_1week=="")?0:$and_cost_1week;
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$and_sales_1week = $db_redshift->getvalue($sql);
	$and_sales_1week = ($and_sales_1week=="")?0:$and_sales_1week;
	
	$sql = " INSERT INTO tbl_daily_marketing_stat(today, type,  subtype, web, web_sub1, web_sub2, ios, ios_sub1, ios_sub2, android, android_sub1, android_sub2) ". 
			" VALUES('$today', 1, 1, $web_arpi_1week, $web_cost_1week, $web_sales_1week, $ios_arpi_1week, $ios_cost_1week,$ios_sales_1week, $and_arpi_1week, $and_cost_1week, $and_sales_1week) ". 
			" ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ios_sub2=VALUES(ios_sub2) ".
			" , android=VALUES(android), android_sub1=VALUES(android_sub1), android_sub2=VALUES(android_sub2);";
	$db_other->execute($sql);
	
	// 최근 4주
	$sql = "SELECT count(useridx) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 ".
			" AND (adflag like 'fbself%' OR adflag like 'nanigans%') "; 
	$web_arpi_4week = $db_redshift->getvalue($sql);
	$web_arpi_4week = ($web_arpi_4week=="")?0:$web_arpi_4week;
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM tbl_ad_stats ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*4) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') ".
			" ) t1";
	$web_cost_4week = $db2->getvalue($sql);
	$web_cost_4week = ($web_cost_4week=="")?0:$web_cost_4week;
	
	$sql = "select SUM(money) as money ".
		" from ( ".
		"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
		"  FROM ( ".
		"    SELECT useridx, createdate ".
		"    FROM t5_user  ".
		"    WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 ". 
		"      and (adflag like 'fbself%' OR adflag like 'nanigans%') ".
		"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
		"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
		"  union all ".
		"  SELECT round(SUM(money), 2) as money ".
		"  FROM ( ".
		"    SELECT useridx, createdate ".
		"    FROM t5_user  ".
		"    WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 ".
		"      and (adflag like 'fbself%' OR adflag like 'nanigans%') ".
		"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
		"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
		" ) t1";
	$web_sales_4week = $db_redshift->getvalue($sql);
	$web_sales_4week = ($web_sales_4week=="")?0:$web_sales_4week;
	
	
	$sql = " SELECT count(distinct deviceid) ".
		   " FROM t5_user ".
		   " WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
		   " and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%')";
	$ios_arpi_4week = $db_redshift->getvalue($sql);
	$ios_arpi_4week = ($ios_arpi_4week=="")?0:$ios_arpi_4week;
	
	$sql = " SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT SUM(spend) AS spend ".
			" FROM tbl_agency_spend_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*4) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			" AND platform = 1 AND agencyname NOT LIKE 'Face%' ".
			" UNION ALL ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM tbl_ad_stats_mobile ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*4) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 1 ".
			" ) t1";
	$ios_cost_4week = $db2->getvalue($sql);
	$ios_cost_4week = ($ios_cost_4week=="")?0:$ios_cost_4week;
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user ".
			"    WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$ios_sales_4week = $db_redshift->getvalue($sql);
	$ios_sales_4week = ($ios_sales_4week=="")?0:$ios_sales_4week;
	
	$sql = "SELECT count(distinct deviceid) ".
		   " FROM t5_user ".
		   " WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
		   " AND (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%')";
	$and_arpi_4week = $db_redshift->getvalue($sql);
	$and_arpi_4week = ($and_arpi_4week=="")?0:$and_arpi_4week;
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT SUM(spend) AS spend ".
			" FROM tbl_agency_spend_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*4) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 2 AND agencyname NOT LIKE 'Face%' ".
			" UNION ALL ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM tbl_ad_stats_mobile ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*4) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 2 ". 
			" ) t1";
	$and_cost_4week = $db2->getvalue($sql);
	$and_cost_4week = ($and_cost_4week=="")?0:$and_cost_4week;
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$and_sales_4week = $db_redshift->getvalue($sql);
	$and_sales_4week = ($and_sales_4week=="")?0:$and_sales_4week;
	
	$sql = " INSERT INTO tbl_daily_marketing_stat(today, type,  subtype, web, web_sub1, web_sub2, ios, ios_sub1, ios_sub2, android, android_sub1, android_sub2) ".
			" VALUES('$today', 1, 2, $web_arpi_4week, $web_cost_4week, $web_sales_4week, $ios_arpi_4week, $ios_cost_4week,$ios_sales_4week, $and_arpi_4week, $and_cost_4week, $and_sales_4week) ".
			" ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ios_sub2=VALUES(ios_sub2) ".
			" , android=VALUES(android), android_sub1=VALUES(android_sub1), android_sub2=VALUES(android_sub2);";
	$db_other->execute($sql);
	
	// 최근 13주
	$sql = "SELECT count(useridx) ".
		   " FROM t5_user ".
		   " WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 ".
		   " AND (adflag like 'fbself%' OR adflag like 'nanigans%')"; 
	$web_arpi_13week = $db_redshift->getvalue($sql);
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM tbl_ad_stats ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*13) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') ".
			" ) t1";
	$web_cost_13week = $db2->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 ". 
			"      and (adflag like 'fbself%' OR adflag like 'nanigans%') ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user ".
			"    WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 ".
			"      and (adflag like 'fbself%' OR adflag like 'nanigans%') ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$web_sales_13week = $db_redshift->getvalue($sql);
	
	$sql = " SELECT count(distinct deviceid) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			" and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%')"; 
	$ios_arpi_13week = $db_redshift->getvalue($sql);
	
	$sql = " SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT SUM(spend) AS spend ".
			" FROM tbl_agency_spend_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*13) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 1 AND agencyname NOT LIKE 'Face%' ".
			" UNION ALL ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM tbl_ad_stats_mobile ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*13) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 1 ".
			" ) t1";
	$ios_cost_13week = $db2->getvalue($sql);
	
	$sql = " select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$ios_sales_13week = $db_redshift->getvalue($sql);
	
	$sql = "SELECT count(distinct deviceid) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			" AND (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%')";
	$and_arpi_13week = $db_redshift->getvalue($sql);
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT SUM(spend) AS spend ".
			" FROM tbl_agency_spend_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*13) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 2 AND agencyname NOT LIKE 'Face%' ".
			" UNION ALL ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM tbl_ad_stats_mobile ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*13) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 2 ".
			" ) t1";
	$and_cost_13week = $db2->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$and_sales_13week = $db_redshift->getvalue($sql);
	
	$web_sales_13week = ($web_sales_13week=="")?0:$web_sales_13week;
	$web_cost_13week = ($web_cost_13week=="")?0:$web_cost_13week;
	$ios_arpi_13week = ($ios_arpi_13week=="")?0:$ios_arpi_13week;
	
	$sql = " INSERT INTO tbl_daily_marketing_stat(today, type,  subtype, web, web_sub1, web_sub2, ios, ios_sub1, ios_sub2, android, android_sub1, android_sub2) ".
			" VALUES('$today', 1, 3, $web_arpi_13week, $web_cost_13week, $web_sales_13week, $ios_arpi_13week, $ios_cost_13week,$ios_sales_13week, $and_arpi_13week, $and_cost_13week, $and_sales_13week) ".
			" ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ios_sub2=VALUES(ios_sub2) ".
			" , android=VALUES(android), android_sub1=VALUES(android_sub1), android_sub2=VALUES(android_sub2);";
	$db_other->execute($sql);
	
	// 최근 52주
	$sql = "SELECT count(useridx) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 ".
			" AND (adflag like 'fbself%' OR adflag like 'nanigans%')"; 
	$web_arpi_52week = $db_redshift->getvalue($sql);
	$web_arpi_52week = ($web_arpi_52week=="")?0:$web_arpi_52week;
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM tbl_ad_stats ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*52) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') ".
			" ) t1";
	$web_cost_52week = $db2->getvalue($sql);
	$web_cost_52week = ($web_cost_52week=="")?0:$web_cost_52week;
	
	$sql = " select SUM(money) as money ".
				" from ( ".
				"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
				"  FROM ( ".
				"    SELECT useridx, createdate ".
				"    FROM t5_user  ".
				"    WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 ". 
				"      and (adflag like 'fbself%' OR adflag like 'nanigans%') ".
				"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
				"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
				"  union all ".
				"  SELECT round(SUM(money), 2) as money ".
				"  FROM ( ".
				"    SELECT useridx, createdate ".
				"    FROM t5_user  ".
				"    WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 ".
				"      and (adflag like 'fbself%' OR adflag like 'nanigans%') ".
				"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
				"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
				" ) t1";
	$web_sales_52week = $db_redshift->getvalue($sql);
	$web_sales_52week = ($web_sales_52week=="")?0:$web_sales_52week;
	
	$sql = "  SELECT count(distinct deviceid) ".
				" FROM t5_user ".
				" WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
				" AND (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%')"; 
	$ios_arpi_52week = $db_redshift->getvalue($sql);
	$ios_arpi_52week  = ($ios_arpi_52week =="")?0:$ios_arpi_52week ;
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT SUM(spend) AS spend ".
			" FROM tbl_agency_spend_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*52) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 1 AND agencyname NOT LIKE 'Face%' ".
			" UNION ALL ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM tbl_ad_stats_mobile ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*52) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 1 ".
			" ) t1";
	$ios_cost_52week = $db2->getvalue($sql);
	$ios_cost_52week  = ($ios_cost_52week =="")?0:$ios_cost_52week ;
	
	$sql = " select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user ".
			"    WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$ios_sales_52week = $db_redshift->getvalue($sql);
	$ios_sales_52week  = ($ios_sales_52week =="")?0:$ios_sales_52week;
	
	$sql = "SELECT count(distinct deviceid) ".
		   " FROM t5_user ".
		   " WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
		   " AND (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%')"; 
	$and_arpi_52week = $db_redshift->getvalue($sql);
	$and_arpi_52week  = ($and_arpi_52week =="")?0:$and_arpi_52week;
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT SUM(spend) AS spend ".
			" FROM tbl_agency_spend_daily ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*52) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 2 AND agencyname NOT LIKE 'Face%' ".
			" UNION ALL ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM tbl_ad_stats_mobile ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6+(7*52) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 6 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 2 ". 
			" ) t1";
	$and_cost_52week = $db2->getvalue($sql);
	$and_cost_52week  = ($and_cost_52week =="")?0:$and_cost_52week;
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$and_sales_52week = $db_redshift->getvalue($sql);
	$and_sales_52week  = ($and_sales_52week =="")?0:$and_sales_52week;
	
	$sql = " INSERT INTO tbl_daily_marketing_stat(today, type,  subtype, web, web_sub1, web_sub2, ios, ios_sub1, ios_sub2, android, android_sub1, android_sub2) ".
			" VALUES('$today', 1, 4, $web_arpi_52week, $web_cost_52week, $web_sales_52week, $ios_arpi_52week, $ios_cost_52week,$ios_sales_52week, $and_arpi_52week, $and_cost_52week, $and_sales_52week) ".
			" ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ios_sub2=VALUES(ios_sub2) ".
			" , android=VALUES(android), android_sub1=VALUES(android_sub1), android_sub2=VALUES(android_sub2);";
	$db_other->execute($sql);
	
	// 2.28일 - ARPI
	
	// 최근 4주
	$sql = "SELECT count(useridx) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 0 ".
			" AND (adflag like 'fbself%' OR adflag like 'nanigans%')"; 
	$web_28day_arpi_4week = $db_redshift->getvalue($sql);
	$web_28day_arpi_4week = ($web_28day_arpi_4week=="")?0:$web_28day_arpi_4week;
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM `tbl_ad_stats` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1+(7*4) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1 DAY), '%Y-%m-%d') ".
			" ) t1"; 
	$web_28day_cost_4week = $db2->getvalue($sql);
	$web_28day_cost_4week = ($web_28day_cost_4week=="")?0:$web_28day_cost_4week;
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 0 ". 
			"      and (adflag like 'fbself%' OR adflag like 'nanigans%')  ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 0 ".
			"      and (adflag like 'fbself%' OR adflag like 'nanigans%') ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			" ) t1";
	$web_28day_sales_4week = $db_redshift->getvalue($sql);
	$web_28day_sales_4week =($web_28day_sales_4week=="")?0:$web_28day_sales_4week;
	
	$sql = "SELECT count(distinct deviceid) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			" AND (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%')"; 
	$ios_28day_arpi_4week = $db_redshift->getvalue($sql);
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT SUM(spend) AS spend ".
			" FROM `tbl_agency_spend_daily` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1+(7*4) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 1 AND agencyname NOT LIKE 'Face%' ".
			" UNION ALL ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM `tbl_ad_stats_mobile` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1+(7*4) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 1 ".
			" ) t1"; 
	$ios_28day_cost_4week = $db2->getvalue($sql);
	$ios_28day_cost_4week = ($ios_28day_cost_4week=="")?0:$ios_28day_cost_4week;
	
	$sql = "select SUM(money) as money ".
			 " from ( ".
			 " SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			 " FROM ( ".
			 "   SELECT useridx, createdate ".
			 "   FROM t5_user  ".
			 "   WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			 "     and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			 "  ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			 "  where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			 " union all ".
			 " SELECT round(SUM(money), 2) as money ".
			 "  FROM ( ".
			 " SELECT useridx, createdate ".
			 " FROM t5_user  ".
			 " WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			 "   and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			 " ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			 " where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 )";
	$ios_28day_sales_4week = $db_redshift->getvalue($sql);
	$ios_28day_sales_4week = ($ios_28day_sales_4week=="")?0:$ios_28day_sales_4week;
	
	$sql = "SELECT count(distinct deviceid) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			" AND (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%')"; 
	$and_28day_arpi_4week = $db_redshift->getvalue($sql);
	$and_28day_arpi_4week = ($and_28day_arpi_4week=="")?0:$and_28day_arpi_4week;
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT SUM(spend) AS spend ".
			" FROM `tbl_agency_spend_daily` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1+(7*4) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 2 AND agencyname NOT LIKE 'Face%' ".
			" UNION ALL ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM `tbl_ad_stats_mobile` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1+(7*4) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 2 ".
			" ) t1"; 
	$and_28day_cost_4week = $db2->getvalue($sql);
	$and_28day_cost_4week = ($and_28day_cost_4week=="")?0:$and_28day_cost_4week;
	
	$sql = "select SUM(money) as money ".
				" from ( ".
				"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
				"  FROM ( ".
				"    SELECT useridx, createdate ".
				"    FROM t5_user  ".
				"    WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
				"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
				"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
				"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
				"  union all ".
				"  SELECT round(SUM(money), 2) as money ".
				"  FROM ( ".
				"    SELECT useridx, createdate  ".
				"    FROM t5_user  ".
				"    WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
				"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
				"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
				"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
				" ) t1";
	$and_28day_sales_4week = $db_redshift->getvalue($sql);
	$and_28day_sales_4week = ($and_28day_sales_4week=="")?0:$and_28day_sales_4week;
	
	$sql = " INSERT INTO tbl_daily_marketing_stat(today, type,  subtype, web, web_sub1, web_sub2, ios, ios_sub1, ios_sub2, android, android_sub1, android_sub2) ". 
			" VALUES('$today', 2, 1, $web_28day_arpi_4week, $web_28day_cost_4week, $web_28day_sales_4week, $ios_28day_arpi_4week, $ios_28day_cost_4week,$ios_28day_sales_4week, $and_28day_arpi_4week, $and_28day_cost_4week, $and_28day_sales_4week) ". 
			" ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ios_sub2=VALUES(ios_sub2) ".
			" , android=VALUES(android), android_sub1=VALUES(android_sub1), android_sub2=VALUES(android_sub2);";
	$db_other->execute($sql);
	
	// 최근 13주
	$sql = "SELECT count(useridx) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 0 ".
			" AND (adflag like 'fbself%' OR adflag like 'nanigans%')"; 
	$web_28day_arpi_13week = $db_redshift->getvalue($sql);
	$web_28day_arpi_13week =($web_28day_arpi_13week =="" )? 0 : $web_28day_arpi_13week;
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
				" SELECT ROUND(SUM(spend), 2) AS spend ".
				" FROM `tbl_ad_stats` ".
				" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1+(7*13) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1 DAY), '%Y-%m-%d') ".
			" ) t1";
	$web_28day_cost_13week = $db2->getvalue($sql);
	$web_28day_cost_13week =($web_28day_cost_13week =="" )? 0 : $web_28day_cost_13week;
	
	$sql = "select SUM(money) as money ".
				" from ( ".
				"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
				"  FROM ( ".
				"    SELECT useridx, createdate ".
				"    FROM t5_user  ".
				"    WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 0 ". 
				"      and (adflag like 'fbself%' OR adflag like 'nanigans%') ".
				"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
				"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
				"  union all ".
				"  SELECT round(SUM(money), 2) as money ".
				"  FROM ( ".
				"    SELECT useridx, createdate ".
				"    FROM t5_user  ".
				"    WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 0 ".
				"      and (adflag like 'fbself%' OR adflag like 'nanigans%')  ".
				"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
				"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
				" ) t1";
	$web_28day_sales_13week = $db_redshift->getvalue($sql);
	$web_28day_sales_13week =($web_28day_sales_13week =="" )? 0 : $web_28day_sales_13week;
	
	$sql = " SELECT count(distinct deviceid) ".
				"FROM t5_user ".
				" WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
				" AND (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%')"; 
	$ios_28day_arpi_13week = $db_redshift->getvalue($sql);
	$ios_28day_arpi_13week =($ios_28day_arpi_13week =="" )? 0 : $ios_28day_arpi_13week;
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT SUM(spend) AS spend ".
			" FROM `tbl_agency_spend_daily` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1+(7*13) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 1 AND agencyname NOT LIKE 'Face%' ".
			" UNION ALL ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM `tbl_ad_stats_mobile` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1+(7*13) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 1 ".
			" ) t1";
	$ios_28day_cost_13week = $db2->getvalue($sql);
	$ios_28day_cost_13week =($ios_28day_cost_13week =="" )? 0 : $ios_28day_cost_13week;
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			 " SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			 " FROM ( ".
			 "   SELECT useridx, createdate ".
			 "   FROM t5_user  ".
			 "   WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			 "     and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			 "  ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			 "  where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			 " union all ".
			 " SELECT round(SUM(money), 2) as money ".
			 " FROM ( ".
			   " SELECT useridx, createdate ".
			   " FROM t5_user  ".
			   " WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			   "   and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			   " ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			   " where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 )";
	$ios_28day_sales_13week = $db_redshift->getvalue($sql);
	$ios_28day_sales_13week =($ios_28day_sales_13week =="" )? 0 : $ios_28day_sales_13week;
	
	$sql = "SELECT count(distinct deviceid) ".
				" FROM t5_user ".
				" WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
				" AND (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%')";
	$and_28day_arpi_13week = $db_redshift->getvalue($sql);
	$and_28day_arpi_13week =($and_28day_arpi_13week =="" )? 0 : $and_28day_arpi_13week;
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			 " SELECT SUM(spend) AS spend ".
			 " FROM `tbl_agency_spend_daily` ".
			 " WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1+(7*13) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			 " AND platform = 2 AND agencyname NOT LIKE 'Face%' ".
			 " UNION ALL ".
			 " SELECT ROUND(SUM(spend), 2) AS spend ".
			 " FROM `tbl_ad_stats_mobile` ".
			 " WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1+(7*13) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			 " AND platform = 2  ".
			" ) t1";
	$and_28day_cost_13week = $db2->getvalue($sql);
	$and_28day_cost_13week =($and_28day_cost_13week =="" )? 0 : $and_28day_cost_13week;
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			" ) t1";
	$and_28day_sales_13week = $db_redshift->getvalue($sql);
	
	$and_28day_sales_13week =($and_28day_sales_13week =="" )? 0 : $and_28day_sales_13week;
	
	$sql = " INSERT INTO tbl_daily_marketing_stat(today, type,  subtype, web, web_sub1, web_sub2, ios, ios_sub1, ios_sub2, android, android_sub1, android_sub2) ".
			" VALUES('$today', 2, 2, $web_28day_arpi_13week, $web_28day_cost_13week, $web_28day_sales_13week, $ios_28day_arpi_13week, $ios_28day_cost_13week,$ios_28day_sales_13week, $and_28day_arpi_13week, $and_28day_cost_13week, $and_28day_sales_13week) ".
			" ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ios_sub2=VALUES(ios_sub2) ".
			" , android=VALUES(android), android_sub1=VALUES(android_sub1), android_sub2=VALUES(android_sub2);";
	$db_other->execute($sql);
	
	// 최근 13주
	$sql = "SELECT count(useridx) ".
			 " FROM t5_user ".
			 " WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 0 ".
			 " AND (adflag like 'fbself%' OR adflag like 'nanigans%')"; 
	$web_28day_arpi_52week = $db_redshift->getvalue($sql);
	
	$web_28day_arpi_52week = ($web_28day_arpi_52week == "")? 0 : $web_28day_arpi_52week;
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM `tbl_ad_stats` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1+(7*52) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1 DAY), '%Y-%m-%d') ".
			" ) t1";
	$web_28day_cost_52week = $db2->getvalue($sql);
	$web_28day_cost_52week = ($web_28day_cost_52week == "")? 0 : $web_28day_cost_52week;
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user ".
			"    WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 0 ". 
			"      and (adflag like 'fbself%' OR adflag like 'nanigans%') ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 0 ".
			"      and (adflag like 'fbself%' OR adflag like 'nanigans%')  ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			" ) t1";
	$web_28day_sales_52week = $db_redshift->getvalue($sql);
	$web_28day_sales_52week = ($web_28day_sales_52week == "")? 0 : $web_28day_sales_52week;
	
	$sql = "SELECT count(distinct deviceid) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			" AND (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%')"; 
	$ios_28day_arpi_52week = $db_redshift->getvalue($sql);
	$ios_28day_arpi_52week = ($ios_28day_arpi_52week == "")? 0 : $ios_28day_arpi_52week;
	
	$sql = "SELECT SUM(spend) AS spend ".
			" FROM ( ".
			" SELECT SUM(spend) AS spend ".
			" FROM `tbl_agency_spend_daily` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1+(7*52) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 1 AND agencyname NOT LIKE 'Face%' ".
			" UNION ALL ".
			" SELECT ROUND(SUM(spend), 2) AS spend ".
			" FROM `tbl_ad_stats_mobile` ".
			" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1+(7*52) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
			"  AND platform = 1 ".
			" ) t1";
	$ios_28day_cost_52week = $db2->getvalue($sql);
	$ios_28day_cost_52week = ($ios_28day_cost_52week == "")? 0 : $ios_28day_cost_52week;
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			  " SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			  " FROM ( ".
			  "  SELECT useridx, createdate ".
			  "  FROM t5_user  ".
			  "  WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			  "    and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			  " ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			  "  where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			  " union all ".
			  " SELECT round(SUM(money), 2) as money ".
			  " FROM ( ".
			  "  SELECT useridx, createdate ".
			  "  FROM t5_user  ".
			  "  WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			  "    and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			  " ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			  " where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 )";
	$ios_28day_sales_52week = $db_redshift->getvalue($sql);
	$ios_28day_sales_52week = ($ios_28day_sales_52week == "")? 0 : $ios_28day_sales_52week;
	
	
	$sql = "SELECT count(distinct deviceid) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			" AND (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%')";
	$and_28day_arpi_52week = $db_redshift->getvalue($sql);
	$and_28day_arpi_52week = ($and_28day_arpi_52week == "")? 0 : $and_28day_arpi_52week;
	
	$sql = "SELECT SUM(spend) AS spend ".
				" FROM ( ".
				" SELECT SUM(spend) AS spend ".
				" FROM `tbl_agency_spend_daily` ".
				" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1+(7*52) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
				"  AND platform = 2 AND agencyname NOT LIKE 'Face%' ".
				" UNION ALL ".
				" SELECT ROUND(SUM(spend), 2) AS spend ".
				" FROM `tbl_ad_stats_mobile` ".
				" WHERE DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1+(7*52) DAY), '%Y-%m-%d') <= today AND today < DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7*4-1 DAY), '%Y-%m-%d') AND today >= '2016-09-01' ".
				"  AND platform = 2 ".
				" ) t1";
	$and_28day_cost_52week = $db2->getvalue($sql);
	$and_28day_cost_52week = ($and_28day_cost_52week == "")? 0 : $and_28day_cost_52week;
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ". 
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user ". 
			"    WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user ".
			"    WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"      and (adflag like '%_int' OR adflag like '%_int_viral' OR adflag like 'nanigans%' OR adflag like 'amazon%' OR adflag like 'Facebook Ads%') ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			" ) t1";
	$and_28day_sales_52week = $db_redshift->getvalue($sql);
	$and_28day_sales_52week = ($and_28day_sales_52week == "")? 0 : $and_28day_sales_52week;
	
	$sql = " INSERT INTO tbl_daily_marketing_stat(today, type,  subtype, web, web_sub1, web_sub2, ios, ios_sub1, ios_sub2, android, android_sub1, android_sub2) ".
			" VALUES('$today', 2, 3, $web_28day_arpi_52week, $web_28day_cost_52week, $web_28day_sales_52week, $ios_28day_arpi_52week, $ios_28day_cost_52week,$ios_28day_sales_52week, $and_28day_arpi_52week, $and_28day_cost_52week, $and_28day_sales_52week) ".
			" ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), web_sub2=VALUES(web_sub2), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1), ios_sub2=VALUES(ios_sub2) ".
			" , android=VALUES(android), android_sub1=VALUES(android_sub1), android_sub2=VALUES(android_sub2);";
	$db_other->execute($sql);
	
	// 3.organic - ARPI
	// 최근 1주
	$sql = "select count(useridx) ".
			" from t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 and adflag = ''";
	$web_organic_user_1week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 0 ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 0 ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$web_organic_sales_1week = $db_redshift->getvalue($sql);
	$web_organic_sales_1week = ($web_organic_sales_1week == "")?0:$web_organic_sales_1week;
	
	$sql = "select count(distinct deviceid) ".
			" from t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and adflag = '' and createdate >= '2016-09-01 00:00:00'"; 
	$ios_organic_user_1week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user ".   
			"    WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$ios_organic_sales_1week = $db_redshift->getvalue($sql);
	
	$sql = "select count(distinct deviceid) ".
			" from t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and adflag = '' and createdate >= '2016-09-01 00:00:00'"; 
	$and_organic_user_1week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*1)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"  ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$and_organic_sales_1week = $db_redshift->getvalue($sql);
	
	$sql = " INSERT INTO tbl_daily_marketing_stat(today, type,  subtype, web, web_sub1, ios, ios_sub1,  android, android_sub1) ".
			" VALUES('$today', 3, 1, $web_organic_user_1week, $web_organic_sales_1week, $ios_organic_user_1week, $ios_organic_sales_1week, $and_organic_user_1week, $and_organic_sales_1week) ".
			" ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1) ".
			" , android=VALUES(android), android_sub1=VALUES(android_sub1);";
	$db_other->execute($sql);
	
	// 최근 4주
	$sql = "select count(useridx) ".
			" from t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 and adflag = ''"; 
	$web_organic_user_4week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 0 ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ". 
			"    WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 0 ".
			"  ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$web_organic_sales_4week = $db_redshift->getvalue($sql);
	
	$sql = "select count(distinct deviceid) ".
			" from t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and adflag = '' and createdate >= '2016-09-01 00:00:00'"; 
	$ios_organic_user_4week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ". 
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user ".
			"    WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$ios_organic_sales_4week = $db_redshift->getvalue($sql);
	
	$sql = " select count(distinct deviceid) ".
			" from t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and adflag = '' and createdate >= '2016-09-01 00:00:00'"; 
	$and_organic_user_4week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user ".  
			"    WHERE dateadd(day,((-7*1+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$and_organic_sales_4week = $db_redshift->getvalue($sql);
	
	$sql = " INSERT INTO tbl_daily_marketing_stat(today, type,  subtype, web, web_sub1, ios, ios_sub1,  android, android_sub1) ".
			" VALUES('$today', 3, 2, $web_organic_user_4week, $web_organic_sales_4week, $ios_organic_user_4week, $ios_organic_sales_4week, $and_organic_user_4week, $and_organic_sales_4week) ".
			" ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1) ".
			" , android=VALUES(android), android_sub1=VALUES(android_sub1);";
	$db_other->execute($sql);
	
	// 최근 13주
	$sql = "select count(useridx) ".
			" from t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 and adflag = ''";  
	$web_organic_user_13week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 0 ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 0 ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$web_organic_sales_13week = $db_redshift->getvalue($sql);
	
	$sql = "select count(distinct deviceid) ".
			" from t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and adflag = '' and createdate >= '2016-09-01 00:00:00'"; 
	$ios_organic_user_13week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"  ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$ios_organic_sales_13week = $db_redshift->getvalue($sql);
	
	$sql = " select count(distinct deviceid) ".
			 " from t5_user ".
			 " WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and adflag = '' and createdate >= '2016-09-01 00:00:00'"; 
	$and_organic_user_13week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$and_organic_sales_13week = $db_redshift->getvalue($sql);
	
	$sql = " INSERT INTO tbl_daily_marketing_stat(today, type,  subtype, web, web_sub1, ios, ios_sub1,  android, android_sub1) ".
			" VALUES('$today', 3, 3, $web_organic_user_13week, $web_organic_sales_13week, $ios_organic_user_13week, $ios_organic_sales_13week, $and_organic_user_13week, $and_organic_sales_13week) ".
			" ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1) ".
			" , android=VALUES(android), android_sub1=VALUES(android_sub1);";
	$db_other->execute($sql);
	
	// 최근 52주
	$sql = "select count(useridx) ".
			" from t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 0 and adflag = ''";  
	$web_organic_user_52week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 0 ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 0 ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$web_organic_sales_52week = $db_redshift->getvalue($sql);
	
	$sql = "select count(distinct deviceid) ".
			" from t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 1 and adflag = '' and createdate >= '2016-09-01 00:00:00'"; 
	$ios_organic_user_52week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user ". 
			"    WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$ios_organic_sales_52week = $db_redshift->getvalue($sql);
	
	$sql = "select count(distinct deviceid) ".
			" from t5_user ".
			" WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and platform = 2 and adflag = '' and createdate >= '2016-09-01 00:00:00'";  
	$and_organic_user_52week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user ".
			"    WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*1+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*1+1),'$current_date') and adflag  = '' and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 7 ".
			" ) t1";
	$and_organic_sales_52week = $db_redshift->getvalue($sql);
	
	$sql = " INSERT INTO tbl_daily_marketing_stat(today, type,  subtype, web, web_sub1, ios, ios_sub1,  android, android_sub1) ".
			" VALUES('$today', 3, 4, $web_organic_user_52week, $web_organic_sales_52week, $ios_organic_user_52week, $ios_organic_sales_52week, $and_organic_user_52week, $and_organic_sales_52week) ".
			" ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1) ".
			" , android=VALUES(android), android_sub1=VALUES(android_sub1);";
	$db_other->execute($sql);
	
	// 4.organic 28일
	// 최근 4주
	$sql = "SELECT count(useridx) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 0";
	$web_28dayorganic_user_4week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 0 ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 0 ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			" ) t1";
	$web_28dayorganic_sales_4week = $db_redshift->getvalue($sql);
	
	$sql = "SELECT count(distinct deviceid) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 1 and adflag = '' and createdate >= '2016-09-01 00:00:00'";
	$ios_28dayorganic_user_4week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			"from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ". 
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			" ) t1";
	$ios_28dayorganic_sales_4week = $db_redshift->getvalue($sql);
	
	$sql = "SELECT count(distinct deviceid) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 2 and adflag = '' and createdate >= '2016-09-01 00:00:00'";
	$and_28dayorganic_user_4week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*4)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			" ) t1";
	$and_28dayorganic_sales_4week = $db_redshift->getvalue($sql);
	
	$sql = " INSERT INTO tbl_daily_marketing_stat(today, type,  subtype, web, web_sub1, ios, ios_sub1,  android, android_sub1) ".
			" VALUES('$today', 4, 1, $web_28dayorganic_user_4week, $web_28dayorganic_sales_4week, $ios_28dayorganic_user_4week, $ios_28dayorganic_sales_4week, $and_28dayorganic_user_4week, $and_28dayorganic_sales_4week) ".
			" ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1) ".
			" , android=VALUES(android), android_sub1=VALUES(android_sub1);";
	$db_other->execute($sql);
	
	// 최근13주
	$sql = "SELECT count(useridx) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 0";
	$web_28dayorganic_user_13week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
				" from ( ".
				"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
				"  FROM ( ".
				"    SELECT useridx, createdate ".
				"    FROM t5_user  ".
				"    WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 0 ".
				"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
				"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
				"  union all ".
				"  SELECT round(SUM(money), 2) as money ".
				"  FROM ( ".
				"    SELECT useridx, createdate ".
				"    FROM t5_user  ".
				"    WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 0 ".
				"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
				"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
				" ) t1";
	$web_28dayorganic_sales_13week = $db_redshift->getvalue($sql);
	
	$sql = "SELECT count(distinct deviceid) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 1 and adflag = '' and createdate >= '2016-09-01 00:00:00'"; 
	$ios_28dayorganic_user_13week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			" ) t1";
	$ios_28dayorganic_sales_13week = $db_redshift->getvalue($sql);
	
	$sql = "SELECT count(distinct deviceid) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 2 and adflag = '' and createdate >= '2016-09-01 00:00:00'";
	$and_28dayorganic_user_13week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			 " SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			 " FROM ( ".
			 "   SELECT useridx, createdate ".
			 "   FROM t5_user  ".
			 "   WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			 "  ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			 "  where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			 " union all ".
			 " SELECT round(SUM(money), 2) as money ".
			 " FROM ( ".
			 "   SELECT useridx, createdate ".
			 "   FROM t5_user  ".
			 "   WHERE dateadd(day,((-7*4+1)-(7*13)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			 "  ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			 "  where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			" ) t1";
	$and_28dayorganic_sales_13week = $db_redshift->getvalue($sql);
	
	$sql = " INSERT INTO tbl_daily_marketing_stat(today, type,  subtype, web, web_sub1, ios, ios_sub1,  android, android_sub1) ".
			" VALUES('$today', 4, 2, $web_28dayorganic_user_13week, $web_28dayorganic_sales_13week, $ios_28dayorganic_user_13week, $ios_28dayorganic_sales_13week, $and_28dayorganic_user_13week, $and_28dayorganic_sales_13week) ".
			" ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1) ".
			" , android=VALUES(android), android_sub1=VALUES(android_sub1);";
	$db_other->execute($sql);
	
	// 최근52주
	$sql = "SELECT count(useridx) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 0";
	$web_28dayorganic_user_52week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 0 ".
			"  ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 0 ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			" ) t1";
	$web_28dayorganic_sales_52week = $db_redshift->getvalue($sql);
	
	$sql = "SELECT count(distinct deviceid) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 1 and adflag = '' and createdate >= '2016-09-01 00:00:00'"; 
	$ios_28dayorganic_user_52week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 1 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			" ) t1";
	$ios_28dayorganic_sales_52week = $db_redshift->getvalue($sql);
	
	$sql = "SELECT count(distinct deviceid) ".
			" FROM t5_user ".
			" WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and platform = 2 and adflag = '' and createdate >= '2016-09-01 00:00:00'";
	$and_28dayorganic_user_52week = $db_redshift->getvalue($sql);
	
	$sql = "select SUM(money) as money ".
			" from ( ".
			"  SELECT round(SUM(facebookcredit::float/10::float), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			"  union all ".
			"  SELECT round(SUM(money), 2) as money ".
			"  FROM ( ".
			"    SELECT useridx, createdate ".
			"    FROM t5_user  ".
			"    WHERE dateadd(day,((-7*4+1)-(7*52)),'$current_date') <= createdate AND createdate < dateadd(day,(-7*4+1),'$current_date') and adflag  = '' and platform = 2 and createdate >= '2016-09-01 00:00:00' ".
			"   ) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx ".
			"   where status = 1 and t1.createdate <= t2.writedate ANd datediff(day, createdate, writedate) <= 28 ".
			" ) t1"; 
	$and_28dayorganic_sales_52week = $db_redshift->getvalue($sql);
	
	$sql = " INSERT INTO tbl_daily_marketing_stat(today, type,  subtype, web, web_sub1, ios, ios_sub1,  android, android_sub1) ".
			" VALUES('$today', 4, 3, $web_28dayorganic_user_52week, $web_28dayorganic_sales_52week, $ios_28dayorganic_user_52week, $ios_28dayorganic_sales_52week, $and_28dayorganic_user_52week, $and_28dayorganic_sales_52week) ".
			" ON DUPLICATE KEY UPDATE web=VALUES(web), web_sub1=VALUES(web_sub1), ios=VALUES(ios), ios_sub1=VALUES(ios_sub1) ".
			" , android=VALUES(android), android_sub1=VALUES(android_sub1);";
	$db_other->execute($sql);
	
	$db_slave->end();
	$db2->end();
	$db_analysis->end();
	$db_other->end();
	$db_redshift->end();
	$db_game->end();
?>