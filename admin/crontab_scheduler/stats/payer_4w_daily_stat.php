<?
	include("../../common/common_include.inc.php");
	
	$db_main_slave = new CDatabase_Slave_Main();
	$db_analysis = new CDatabase_Analysis();
	
	$db_main_slave->execute("SET wait_timeout=43200");
	$db_analysis->execute("SET wait_timeout=43200");
	
	ini_set("memory_limit", "-1");

	try
	{
	    $sdate = date("Y-m-d", time() - 24 * 60 * 60 * 7);
	    $edate = date("Y-m-d", time() + 24 * 60 * 60);
	    
	    while($sdate < $edate)
	    {
	        $temp_date = date('Y-m-d', strtotime($sdate.' + 1 day'));

	        // 전체 - 결제 유지자
	        $sql = "SELECT platform, COUNT(useridx_4_0w) AS payer_cnt, SUM(web_cnt) AS web_cnt, ROUND(SUM(web_money), 2) AS web_money, SUM(web_coin) AS web_coin, SUM(web_basecoin) AS web_basecoin,
                    	SUM(ios_cnt) AS ios_cnt, ROUND(SUM(ios_money), 2) AS ios_money, SUM(ios_coin) AS ios_coin, SUM(ios_basecoin) AS ios_basecoin,
                    	SUM(and_cnt) AS and_cnt, ROUND(SUM(and_money), 2) AS and_money, SUM(and_coin) AS and_coin, SUM(and_basecoin) AS and_basecoin,
                    	SUM(ama_cnt) AS ama_cnt, ROUND(SUM(ama_money), 2) AS ama_money, SUM(ama_coin) AS ama_coin, SUM(ama_basecoin) AS ama_basecoin
                    FROM (
                    	SELECT useridx_4_0w, web_cnt, web_money, web_coin, web_basecoin, ios_cnt, ios_money, ios_coin, ios_basecoin, and_cnt, and_money, and_coin, and_basecoin, ama_cnt, ama_money, ama_coin, ama_basecoin
                    	FROM (
                    		SELECT useridx AS useridx_8_4w
                    		FROM (
                    			SELECT useridx, 0 AS platform, ROUND(facebookcredit/10, 2) AS money, coin, basecoin, special_more, writedate
                    			FROM tbl_product_order
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 8 WEEK) AND writedate < DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK)
                    			UNION ALL
                    			SELECT useridx, os_type AS platform, money, coin, basecoin, special_more, writedate
                    			FROM tbl_product_order_mobile
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 8 WEEK) AND writedate < DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK)
                    		) tt
                    		GROUP BY useridx
                    	) t1 LEFT JOIN (
                    		SELECT useridx AS useridx_4_0w, SUM(web_cnt) AS web_cnt, SUM(web_money) AS web_money, SUM(web_coin) AS web_coin, SUM(web_basecoin) AS web_basecoin,
                    			SUM(ios_cnt) AS ios_cnt, SUM(ios_money) AS ios_money, SUM(ios_coin) AS ios_coin, SUM(ios_basecoin) AS ios_basecoin,
                    			SUM(and_cnt) AS and_cnt, SUM(and_money) AS and_money, SUM(and_coin) AS and_coin, SUM(and_basecoin) AS and_basecoin,
                    			SUM(ama_cnt) AS ama_cnt, SUM(ama_money) AS ama_money, SUM(ama_coin) AS ama_coin, SUM(ama_basecoin) AS ama_basecoin
                    		FROM (
                    			SELECT useridx, 1 AS web_cnt, ROUND(facebookcredit/10, 2) AS web_money, coin AS web_coin, basecoin AS web_basecoin,
                    				0 AS ios_cnt, 0 AS ios_money, 0 AS ios_coin, 0 AS ios_basecoin,
                    				0 AS and_cnt, 0 AS and_money, 0 AS and_coin, 0 AS and_basecoin,
                    				0 AS ama_cnt, 0 AS ama_money, 0 AS ama_coin, 0 AS ama_basecoin
                    			FROM tbl_product_order
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK) AND writedate < '$sdate 00:00:00'
                    			UNION ALL
                    			SELECT useridx, 0 AS web_cnt, 0 AS web_money, 0 AS web_coin, 0 AS web_basecoin, 
                    				IF(os_type = 1, 1, 0) AS ios_cnt, IF(os_type = 1, money, 0) AS ios_money, IF(os_type = 1, coin, 0) AS ios_coin, IF(os_type = 1, basecoin, 0) AS ios_basecoin,
                    				IF(os_type = 2, 1, 0) AS and_cnt, IF(os_type = 2, money, 0) AS and_money, IF(os_type = 2, coin, 0) AS and_coin, IF(os_type = 2, basecoin, 0) AS and_basecoin,
                    				IF(os_type = 3, 1, 0) AS ama_cnt, IF(os_type = 3, money, 0) AS ama_money, IF(os_type = 3, coin, 0) AS ama_coin, IF(os_type = 3, basecoin, 0) AS ama_basecoin
                    			FROM tbl_product_order_mobile
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK) AND writedate < '$sdate 00:00:00'
                    		) tt
                    		GROUP BY useridx
                    	) t2 ON t1.useridx_8_4w = t2.useridx_4_0w
                    	WHERE useridx_4_0w IS NOT NULL
                    ) t1 JOIN tbl_user_ext t2 ON t1.useridx_4_0w = t2.useridx
                    GROUP BY platform";
	        $date_list = $db_main_slave->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = $date_list[$i]["platform"];
	            $type = 1;
	            $payer_cnt = $date_list[$i]["payer_cnt"];
	            $web_cnt = $date_list[$i]["web_cnt"];
	            $web_money = $date_list[$i]["web_money"];
	            $web_coin = $date_list[$i]["web_coin"];
	            $web_basecoin = $date_list[$i]["web_basecoin"];
	            $ios_cnt = $date_list[$i]["ios_cnt"];
	            $ios_money = $date_list[$i]["ios_money"];
	            $ios_coin = $date_list[$i]["ios_coin"];
	            $ios_basecoin = $date_list[$i]["ios_basecoin"];
	            $and_cnt = $date_list[$i]["and_cnt"];
	            $and_money = $date_list[$i]["and_money"];
	            $and_coin = $date_list[$i]["and_coin"];
	            $and_basecoin = $date_list[$i]["and_basecoin"];
	            $ama_cnt = $date_list[$i]["ama_cnt"];
	            $ama_money = $date_list[$i]["ama_money"];
	            $ama_coin = $date_list[$i]["ama_coin"];
	            $ama_basecoin = $date_list[$i]["ama_basecoin"];
	            
	            $sql = "INSERT INTO tbl_payer_4w_daily_stat(today, type, platform, payer_cnt, web_cnt, web_money, web_coin, web_basecoin, ios_cnt, ios_money, ios_coin, ios_basecoin, and_cnt, and_money, and_coin, and_basecoin, ama_cnt, ama_money, ama_coin, ama_basecoin) ".
    	   	            "VALUES('$sdate', $type, $platform, $payer_cnt, $web_cnt, $web_money, $web_coin, $web_basecoin, $ios_cnt, $ios_money, $ios_coin, $ios_basecoin, $and_cnt, $and_money, $and_coin, $and_basecoin, $ama_cnt, $ama_money, $ama_coin, $ama_basecoin) ".
    	   	            "ON DUPLICATE KEY UPDATE payer_cnt=VALUES(payer_cnt), web_cnt=VALUES(web_cnt), web_money=VALUES(web_money), web_coin=VALUES(web_coin), web_basecoin=VALUES(web_basecoin), ios_cnt=VALUES(ios_cnt), ios_money=VALUES(ios_money), ios_coin=VALUES(ios_coin), ios_basecoin=VALUES(ios_basecoin), and_cnt=VALUES(and_cnt), and_money=VALUES(and_money), and_coin=VALUES(and_coin), and_basecoin=VALUES(and_basecoin), ama_cnt=VALUES(ama_cnt), ama_money=VALUES(ama_money), ama_coin=VALUES(ama_coin), ama_basecoin=VALUES(ama_basecoin);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 전체 - 첫 결제자
	        $sql = "SELECT platform, COUNT(useridx_4_0w) AS payer_cnt, SUM(web_cnt) AS web_cnt, ROUND(SUM(web_money), 2) AS web_money, SUM(web_coin) AS web_coin, SUM(web_basecoin) AS web_basecoin,
                    	SUM(ios_cnt) AS ios_cnt, ROUND(SUM(ios_money), 2) AS ios_money, SUM(ios_coin) AS ios_coin, SUM(ios_basecoin) AS ios_basecoin,
                    	SUM(and_cnt) AS and_cnt, ROUND(SUM(and_money), 2) AS and_money, SUM(and_coin) AS and_coin, SUM(and_basecoin) AS and_basecoin,
                    	SUM(ama_cnt) AS ama_cnt, ROUND(SUM(ama_money), 2) AS ama_money, SUM(ama_coin) AS ama_coin, SUM(ama_basecoin) AS ama_basecoin
                    FROM (
                    	SELECT useridx_4_0w, web_cnt, web_money, web_coin, web_basecoin, ios_cnt, ios_money, ios_coin, ios_basecoin, and_cnt, and_money, and_coin, and_basecoin, ama_cnt, ama_money, ama_coin, ama_basecoin
                    	FROM (
                    		SELECT useridx AS useridx_8_4w
                    		FROM (
                    			SELECT useridx, 0 AS platform, ROUND(facebookcredit/10, 2) AS money, coin, basecoin, special_more, writedate
                    			FROM tbl_product_order
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 8 WEEK) AND writedate < DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK)
                    			UNION ALL
                    			SELECT useridx, os_type AS platform, money, coin, basecoin, special_more, writedate
                    			FROM tbl_product_order_mobile
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 8 WEEK) AND writedate < DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK)
                    		) tt
                    		GROUP BY useridx
                    	) t1 RIGHT JOIN (
                    		SELECT useridx AS useridx_4_0w, SUM(web_cnt) AS web_cnt, SUM(web_money) AS web_money, SUM(web_coin) AS web_coin, SUM(web_basecoin) AS web_basecoin,
                    			SUM(ios_cnt) AS ios_cnt, SUM(ios_money) AS ios_money, SUM(ios_coin) AS ios_coin, SUM(ios_basecoin) AS ios_basecoin,
                    			SUM(and_cnt) AS and_cnt, SUM(and_money) AS and_money, SUM(and_coin) AS and_coin, SUM(and_basecoin) AS and_basecoin,
                    			SUM(ama_cnt) AS ama_cnt, SUM(ama_money) AS ama_money, SUM(ama_coin) AS ama_coin, SUM(ama_basecoin) AS ama_basecoin
                    		FROM (
                    			SELECT useridx, 1 AS web_cnt, ROUND(facebookcredit/10, 2) AS web_money, coin AS web_coin, basecoin AS web_basecoin,
                    				0 AS ios_cnt, 0 AS ios_money, 0 AS ios_coin, 0 AS ios_basecoin,
                    				0 AS and_cnt, 0 AS and_money, 0 AS and_coin, 0 AS and_basecoin,
                    				0 AS ama_cnt, 0 AS ama_money, 0 AS ama_coin, 0 AS ama_basecoin
                    			FROM tbl_product_order
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK) AND writedate < '$sdate 00:00:00'
                    			UNION ALL
                    			SELECT useridx, 0 AS web_cnt, 0 AS web_money, 0 AS web_coin, 0 AS web_basecoin, 
                    				IF(os_type = 1, 1, 0) AS ios_cnt, IF(os_type = 1, money, 0) AS ios_money, IF(os_type = 1, coin, 0) AS ios_coin, IF(os_type = 1, basecoin, 0) AS ios_basecoin,
                    				IF(os_type = 2, 1, 0) AS and_cnt, IF(os_type = 2, money, 0) AS and_money, IF(os_type = 2, coin, 0) AS and_coin, IF(os_type = 2, basecoin, 0) AS and_basecoin,
                    				IF(os_type = 3, 1, 0) AS ama_cnt, IF(os_type = 3, money, 0) AS ama_money, IF(os_type = 3, coin, 0) AS ama_coin, IF(os_type = 3, basecoin, 0) AS ama_basecoin
                    			FROM tbl_product_order_mobile
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK) AND writedate < '$sdate 00:00:00'
                    		) tt
                    		GROUP BY useridx
                    	) t2 ON t1.useridx_8_4w = t2.useridx_4_0w
                    	WHERE useridx_8_4w IS NULL AND EXISTS (SELECT * FROM `tbl_user_first_buy` WHERE useridx = useridx_4_0w AND buywritedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK) AND buywritedate < '$sdate 00:00:00')
                    ) t1 JOIN tbl_user_ext t2 ON t1.useridx_4_0w = t2.useridx
                    GROUP BY platform";
	        $date_list = $db_main_slave->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = $date_list[$i]["platform"];
	            $type = 2;
	            $payer_cnt = $date_list[$i]["payer_cnt"];
	            $web_cnt = $date_list[$i]["web_cnt"];
	            $web_money = $date_list[$i]["web_money"];
	            $web_coin = $date_list[$i]["web_coin"];
	            $web_basecoin = $date_list[$i]["web_basecoin"];
	            $ios_cnt = $date_list[$i]["ios_cnt"];
	            $ios_money = $date_list[$i]["ios_money"];
	            $ios_coin = $date_list[$i]["ios_coin"];
	            $ios_basecoin = $date_list[$i]["ios_basecoin"];
	            $and_cnt = $date_list[$i]["and_cnt"];
	            $and_money = $date_list[$i]["and_money"];
	            $and_coin = $date_list[$i]["and_coin"];
	            $and_basecoin = $date_list[$i]["and_basecoin"];
	            $ama_cnt = $date_list[$i]["ama_cnt"];
	            $ama_money = $date_list[$i]["ama_money"];
	            $ama_coin = $date_list[$i]["ama_coin"];
	            $ama_basecoin = $date_list[$i]["ama_basecoin"];
	            
	            $sql = "INSERT INTO tbl_payer_4w_daily_stat(today, type, platform, payer_cnt, web_cnt, web_money, web_coin, web_basecoin, ios_cnt, ios_money, ios_coin, ios_basecoin, and_cnt, and_money, and_coin, and_basecoin, ama_cnt, ama_money, ama_coin, ama_basecoin) ".
    	   	            "VALUES('$sdate', $type, $platform, $payer_cnt, $web_cnt, $web_money, $web_coin, $web_basecoin, $ios_cnt, $ios_money, $ios_coin, $ios_basecoin, $and_cnt, $and_money, $and_coin, $and_basecoin, $ama_cnt, $ama_money, $ama_coin, $ama_basecoin) ".
    	   	            "ON DUPLICATE KEY UPDATE payer_cnt=VALUES(payer_cnt), web_cnt=VALUES(web_cnt), web_money=VALUES(web_money), web_coin=VALUES(web_coin), web_basecoin=VALUES(web_basecoin), ios_cnt=VALUES(ios_cnt), ios_money=VALUES(ios_money), ios_coin=VALUES(ios_coin), ios_basecoin=VALUES(ios_basecoin), and_cnt=VALUES(and_cnt), and_money=VALUES(and_money), and_coin=VALUES(and_coin), and_basecoin=VALUES(and_basecoin), ama_cnt=VALUES(ama_cnt), ama_money=VALUES(ama_money), ama_coin=VALUES(ama_coin), ama_basecoin=VALUES(ama_basecoin);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 전체 - 이탈복귀 결제자
	        $sql = "SELECT platform, COUNT(useridx_4_0w) AS payer_cnt, SUM(web_cnt) AS web_cnt, ROUND(SUM(web_money), 2) AS web_money, SUM(web_coin) AS web_coin, SUM(web_basecoin) AS web_basecoin,
                    	SUM(ios_cnt) AS ios_cnt, ROUND(SUM(ios_money), 2) AS ios_money, SUM(ios_coin) AS ios_coin, SUM(ios_basecoin) AS ios_basecoin,
                    	SUM(and_cnt) AS and_cnt, ROUND(SUM(and_money), 2) AS and_money, SUM(and_coin) AS and_coin, SUM(and_basecoin) AS and_basecoin,
                    	SUM(ama_cnt) AS ama_cnt, ROUND(SUM(ama_money), 2) AS ama_money, SUM(ama_coin) AS ama_coin, SUM(ama_basecoin) AS ama_basecoin
                    FROM (
                    	SELECT useridx_4_0w, web_cnt, web_money, web_coin, web_basecoin, ios_cnt, ios_money, ios_coin, ios_basecoin, and_cnt, and_money, and_coin, and_basecoin, ama_cnt, ama_money, ama_coin, ama_basecoin
                    	FROM (
                    		SELECT useridx AS useridx_8_4w
                    		FROM (
                    			SELECT useridx, 0 AS platform, ROUND(facebookcredit/10, 2) AS money, coin, basecoin, special_more, writedate
                    			FROM tbl_product_order
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 8 WEEK) AND writedate < DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK)
                    			UNION ALL
                    			SELECT useridx, os_type AS platform, money, coin, basecoin, special_more, writedate
                    			FROM tbl_product_order_mobile
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 8 WEEK) AND writedate < DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK)
                    		) tt
                    		GROUP BY useridx
                    	) t1 RIGHT JOIN (
                    		SELECT useridx AS useridx_4_0w, SUM(web_cnt) AS web_cnt, SUM(web_money) AS web_money, SUM(web_coin) AS web_coin, SUM(web_basecoin) AS web_basecoin,
                    			SUM(ios_cnt) AS ios_cnt, SUM(ios_money) AS ios_money, SUM(ios_coin) AS ios_coin, SUM(ios_basecoin) AS ios_basecoin,
                    			SUM(and_cnt) AS and_cnt, SUM(and_money) AS and_money, SUM(and_coin) AS and_coin, SUM(and_basecoin) AS and_basecoin,
                    			SUM(ama_cnt) AS ama_cnt, SUM(ama_money) AS ama_money, SUM(ama_coin) AS ama_coin, SUM(ama_basecoin) AS ama_basecoin
                    		FROM (
                    			SELECT useridx, 1 AS web_cnt, ROUND(facebookcredit/10, 2) AS web_money, coin AS web_coin, basecoin AS web_basecoin,
                    				0 AS ios_cnt, 0 AS ios_money, 0 AS ios_coin, 0 AS ios_basecoin,
                    				0 AS and_cnt, 0 AS and_money, 0 AS and_coin, 0 AS and_basecoin,
                    				0 AS ama_cnt, 0 AS ama_money, 0 AS ama_coin, 0 AS ama_basecoin
                    			FROM tbl_product_order
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK) AND writedate < '$sdate 00:00:00'
                    			UNION ALL
                    			SELECT useridx, 0 AS web_cnt, 0 AS web_money, 0 AS web_coin, 0 AS web_basecoin,
                    				IF(os_type = 1, 1, 0) AS ios_cnt, IF(os_type = 1, money, 0) AS ios_money, IF(os_type = 1, coin, 0) AS ios_coin, IF(os_type = 1, basecoin, 0) AS ios_basecoin,
                    				IF(os_type = 2, 1, 0) AS and_cnt, IF(os_type = 2, money, 0) AS and_money, IF(os_type = 2, coin, 0) AS and_coin, IF(os_type = 2, basecoin, 0) AS and_basecoin,
                    				IF(os_type = 3, 1, 0) AS ama_cnt, IF(os_type = 3, money, 0) AS ama_money, IF(os_type = 3, coin, 0) AS ama_coin, IF(os_type = 3, basecoin, 0) AS ama_basecoin
                    			FROM tbl_product_order_mobile
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK) AND writedate < '$sdate 00:00:00'
                    		) tt
                    		GROUP BY useridx
                    	) t2 ON t1.useridx_8_4w = t2.useridx_4_0w
                    	WHERE useridx_8_4w IS NULL AND NOT EXISTS (SELECT * FROM `tbl_user_first_buy` WHERE useridx = useridx_4_0w AND buywritedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK) AND buywritedate < '$sdate 00:00:00')
                    ) t1 JOIN tbl_user_ext t2 ON t1.useridx_4_0w = t2.useridx
                    GROUP BY platform";
	        $date_list = $db_main_slave->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = $date_list[$i]["platform"];
	            $type = 3;
	            $payer_cnt = $date_list[$i]["payer_cnt"];
	            $web_cnt = $date_list[$i]["web_cnt"];
	            $web_money = $date_list[$i]["web_money"];
	            $web_coin = $date_list[$i]["web_coin"];
	            $web_basecoin = $date_list[$i]["web_basecoin"];
	            $ios_cnt = $date_list[$i]["ios_cnt"];
	            $ios_money = $date_list[$i]["ios_money"];
	            $ios_coin = $date_list[$i]["ios_coin"];
	            $ios_basecoin = $date_list[$i]["ios_basecoin"];
	            $and_cnt = $date_list[$i]["and_cnt"];
	            $and_money = $date_list[$i]["and_money"];
	            $and_coin = $date_list[$i]["and_coin"];
	            $and_basecoin = $date_list[$i]["and_basecoin"];
	            $ama_cnt = $date_list[$i]["ama_cnt"];
	            $ama_money = $date_list[$i]["ama_money"];
	            $ama_coin = $date_list[$i]["ama_coin"];
	            $ama_basecoin = $date_list[$i]["ama_basecoin"];
	            
	            $sql = "INSERT INTO tbl_payer_4w_daily_stat(today, type, platform, payer_cnt, web_cnt, web_money, web_coin, web_basecoin, ios_cnt, ios_money, ios_coin, ios_basecoin, and_cnt, and_money, and_coin, and_basecoin, ama_cnt, ama_money, ama_coin, ama_basecoin) ".
    	   	            "VALUES('$sdate', $type, $platform, $payer_cnt, $web_cnt, $web_money, $web_coin, $web_basecoin, $ios_cnt, $ios_money, $ios_coin, $ios_basecoin, $and_cnt, $and_money, $and_coin, $and_basecoin, $ama_cnt, $ama_money, $ama_coin, $ama_basecoin) ".
    	   	            "ON DUPLICATE KEY UPDATE payer_cnt=VALUES(payer_cnt), web_cnt=VALUES(web_cnt), web_money=VALUES(web_money), web_coin=VALUES(web_coin), web_basecoin=VALUES(web_basecoin), ios_cnt=VALUES(ios_cnt), ios_money=VALUES(ios_money), ios_coin=VALUES(ios_coin), ios_basecoin=VALUES(ios_basecoin), and_cnt=VALUES(and_cnt), and_money=VALUES(and_money), and_coin=VALUES(and_coin), and_basecoin=VALUES(and_basecoin), ama_cnt=VALUES(ama_cnt), ama_money=VALUES(ama_money), ama_coin=VALUES(ama_coin), ama_basecoin=VALUES(ama_basecoin);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 전체 - 결제 이탈자
	        $sql = "SELECT platform, COUNT(useridx_8_4w) AS payer_cnt
                    FROM (
                    	SELECT useridx_8_4w 
                    	FROM (
                    		SELECT useridx AS useridx_8_4w
                    		FROM (
                    			SELECT useridx, 0 AS platform, ROUND(facebookcredit/10, 2) AS money, coin, basecoin, special_more, writedate
                    			FROM tbl_product_order
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 8 WEEK) AND writedate < DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK)
                    			UNION ALL
                    			SELECT useridx, os_type AS platform, money, coin, basecoin, special_more, writedate
                    			FROM tbl_product_order_mobile
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 8 WEEK) AND writedate < DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK)
                    		) tt
                    		GROUP BY useridx
                    	) t1 LEFT JOIN (
                    		SELECT useridx AS useridx_4_0w, COUNT(*) AS pay_cnt, ROUND(SUM(money), 2) AS total_money, SUM(coin) AS total_coin, SUM(basecoin) AS total_basecoin
                    		FROM (
                    			SELECT useridx, 0 AS platform, ROUND(facebookcredit/10, 2) AS money, coin, basecoin, special_more, writedate
                    			FROM tbl_product_order
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK) AND writedate < '$sdate 00:00:00'
                    			UNION ALL
                    			SELECT useridx, os_type AS platform, money, coin, basecoin, special_more, writedate
                    			FROM tbl_product_order_mobile
                    			WHERE useridx > 20000 AND STATUS = 1 AND writedate >= DATE_SUB('$sdate 00:00:00', INTERVAL 4 WEEK) AND writedate < '$sdate 00:00:00'
                    		) tt
                    		GROUP BY useridx
                    	) t2 ON t1.useridx_8_4w = t2.useridx_4_0w
                    	WHERE useridx_4_0w IS NULL
                    ) t1 JOIN tbl_user_ext t2 ON t1.useridx_8_4w = t2.useridx
                    GROUP BY platform";
	        $date_list = $db_main_slave->gettotallist($sql);

	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = $date_list[$i]["platform"];
	            $type = 4;
	            $payer_cnt = $date_list[$i]["payer_cnt"];
	            $web_cnt = 0;
	            $web_money = 0;
	            $web_coin = 0;
	            $web_basecoin = 0;
	            $ios_cnt = 0;
	            $ios_money = 0;
	            $ios_coin = 0;
	            $ios_basecoin = 0;
	            $and_cnt = 0;
	            $and_money = 0;
	            $and_coin = 0;
	            $and_basecoin = 0;
	            $ama_cnt = 0;
	            $ama_money = 0;
	            $ama_coin = 0;
	            $ama_basecoin = 0;
	            
	            $sql = "INSERT INTO tbl_payer_4w_daily_stat(today, type, platform, payer_cnt, web_cnt, web_money, web_coin, web_basecoin, ios_cnt, ios_money, ios_coin, ios_basecoin, and_cnt, and_money, and_coin, and_basecoin, ama_cnt, ama_money, ama_coin, ama_basecoin) ".
    	   	            "VALUES('$sdate', $type, $platform, $payer_cnt, $web_cnt, $web_money, $web_coin, $web_basecoin, $ios_cnt, $ios_money, $ios_coin, $ios_basecoin, $and_cnt, $and_money, $and_coin, $and_basecoin, $ama_cnt, $ama_money, $ama_coin, $ama_basecoin) ".
    	   	            "ON DUPLICATE KEY UPDATE payer_cnt=VALUES(payer_cnt), web_cnt=VALUES(web_cnt), web_money=VALUES(web_money), web_coin=VALUES(web_coin), web_basecoin=VALUES(web_basecoin), ios_cnt=VALUES(ios_cnt), ios_money=VALUES(ios_money), ios_coin=VALUES(ios_coin), ios_basecoin=VALUES(ios_basecoin), and_cnt=VALUES(and_cnt), and_money=VALUES(and_money), and_coin=VALUES(and_coin), and_basecoin=VALUES(and_basecoin), ama_cnt=VALUES(ama_cnt), ama_money=VALUES(ama_money), ama_coin=VALUES(ama_coin), ama_basecoin=VALUES(ama_basecoin);";
	            $db_analysis->execute($sql);
	        }
	        
	        $sdate = $temp_date;
	    }
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	}
	
	$db_main_slave->end();
	$db_analysis->end();
?>