<?
	include("../../common/common_include.inc.php");
	
	$db_other = new CDatabase_Other();
	
	$db_other->execute("SET wait_timeout=7200");
	
	$today = date("Y-m-d");
	
	$start_searchweek = date("Y-m-d"  ,strtotime("-8 day"));
	$end_searchweek = date("Y-m-d"  ,strtotime("-1 day"));
	
	//신규 사용자 패턴 
	try
	
	{
		// Web
		//1 week
		$sql = " SELECT DATE_SUB('$end_searchweek',INTERVAL 1 WEEK) AS createdate, 0 AS platform, AVG(moneyin+h_moneyin) AS  moneyin, AVG(moneyout+h_moneyout) AS moneyout, AVG(playcount+h_playcount) AS playcount, ".
						" (AVG(moneyout+h_moneyout) / AVG(moneyin+h_moneyin)) * 100 AS winrate, AVG(playcount+h_playcount) AS playcount, ROUND(AVG(playcount+h_playcount)*5/60,2) AS playminute, ". 
						" AVG(currentcoin) AS currentcoin, AVG(freecoin) AS freecoin , SUM(IF(purchasecount > 0, 1, 0)) AS purchasecount, ". 
						" SUM(purchaseamount)/10 AS purchaseamount ".
				" FROM `tbl_user_playstat_daily` t1 WHERE today BETWEEN '$start_searchweek' AND '$end_searchweek' ".
				" AND EXISTS (SELECT * FROM `tbl_user_playstat_daily` WHERE today = DATE_SUB('$end_searchweek',INTERVAL 1 WEEK) AND useridx=t1.useridx AND days_after_install = 1) AND playcount+h_playcount >= 50;";
		
		$newuser_retention_1week = $db_other->getarray($sql);
		
		$createdate_1week = $newuser_retention_1week['createdate'];
		$platform_1week = $newuser_retention_1week['platform'];
		$moneyin_1week = $newuser_retention_1week['moneyin'];
		$moneyout_1week = $newuser_retention_1week['moneyout'];
		$winrate_1week = $newuser_retention_1week['winrate'];
		$playcount_1week = $newuser_retention_1week['playcount'];
		$playminute_1week = $newuser_retention_1week['playminute'];
		$currentcoin_1week = $newuser_retention_1week['currentcoin'];
		$freecoin_1week = $newuser_retention_1week['freecoin'];
		$purchasecount_1week = $newuser_retention_1week['purchasecount'];
		$purchaseamount_1week = $newuser_retention_1week['purchaseamount'];

		$sql = "INSERT INTO tbl_new_user_retention_week_stats(today, createdate, platform, moneyin, moneyout, winrate, playcount, playminute, currentcoin, freecoin, purchasecount, purchaseamount)".
				" VALUES('$today','$createdate_1week', $platform_1week, $moneyin_1week, $moneyout_1week, $winrate_1week ,$playcount_1week, $playminute_1week, $freecoin_1week, $currentcoin_1week, $purchasecount_1week, $purchaseamount_1week) ".
				" ON DUPLICATE KEY UPDATE  moneyin = VALUES(moneyin), moneyout= VALUES(moneyout), winrate= VALUES(winrate), playcount= VALUES(playcount), playminute= VALUES(playminute), currentcoin= VALUES(currentcoin), freecoin= VALUES(freecoin), purchasecount= VALUES(purchasecount), purchaseamount= VALUES(purchaseamount);";
		$db_other->execute($sql);
		
		//2 week		
		$sql = " SELECT DATE_SUB('$end_searchweek',INTERVAL 2 WEEK) AS createdate, 0 AS platform, AVG(moneyin+h_moneyin) AS  moneyin, AVG(moneyout+h_moneyout) AS moneyout, AVG(playcount+h_playcount) AS playcount, ".
						" (AVG(moneyout+h_moneyout) / AVG(moneyin+h_moneyin)) * 100 AS winrate, AVG(playcount+h_playcount) AS playcount, ROUND(AVG(playcount+h_playcount)*5/60,2) AS playminute, ". 
						" AVG(currentcoin) AS currentcoin, AVG(freecoin) AS freecoin , SUM(IF(purchasecount > 0, 1, 0)) AS purchasecount, ". 
						" SUM(purchaseamount)/10 AS purchaseamount ".
				" FROM `tbl_user_playstat_daily` t1 WHERE today BETWEEN '$start_searchweek' AND '$end_searchweek' ".
				" AND EXISTS (SELECT * FROM `tbl_user_playstat_daily` WHERE today = DATE_SUB('$end_searchweek',INTERVAL 2 WEEK) AND useridx=t1.useridx AND days_after_install = 1) AND playcount+h_playcount >= 50;";
		
		$newuser_retention_2week = $db_other->getarray($sql);
		
		$createdate_2week = $newuser_retention_2week['createdate'];
		$platform_2week = $newuser_retention_2week['platform'];
		$moneyin_2week = $newuser_retention_2week['moneyin'];
		$moneyout_2week = $newuser_retention_2week['moneyout'];
		$winrate_2week = $newuser_retention_2week['winrate'];
		$playcount_2week = $newuser_retention_2week['playcount'];
		$playminute_2week = $newuser_retention_2week['playminute'];
		$currentcoin_2week = $newuser_retention_2week['currentcoin'];
		$freecoin_2week = $newuser_retention_2week['freecoin'];
		$purchasecount_2week = $newuser_retention_2week['purchasecount'];
		$purchaseamount_2week = $newuser_retention_2week['purchaseamount'];

		$sql = "INSERT INTO tbl_new_user_retention_week_stats(today, createdate, platform, moneyin, moneyout, winrate, playcount, playminute, currentcoin, freecoin, purchasecount, purchaseamount)".
				" VALUES('$today','$createdate_2week', $platform_2week, $moneyin_2week, $moneyout_2week, $winrate_2week ,$playcount_2week, $playminute_2week, $freecoin_2week, $currentcoin_2week, $purchasecount_2week, $purchaseamount_2week) ".
				" ON DUPLICATE KEY UPDATE  moneyin = VALUES(moneyin), moneyout= VALUES(moneyout), winrate= VALUES(winrate), playcount= VALUES(playcount), playminute= VALUES(playminute), currentcoin= VALUES(currentcoin), freecoin= VALUES(freecoin), purchasecount= VALUES(purchasecount), purchaseamount= VALUES(purchaseamount);";
		$db_other->execute($sql);
		
		//3 week		
		$sql = " SELECT DATE_SUB('$end_searchweek',INTERVAL 3 WEEK) AS createdate, 0 AS platform,  AVG(moneyin+h_moneyin) AS  moneyin, AVG(moneyout+h_moneyout) AS moneyout, AVG(playcount+h_playcount) AS playcount, ".
						" (AVG(moneyout+h_moneyout) / AVG(moneyin+h_moneyin)) * 100 AS winrate, AVG(playcount+h_playcount) AS playcount, ROUND(AVG(playcount+h_playcount)*5/60,2) AS playminute, ". 
						" AVG(currentcoin) AS currentcoin, AVG(freecoin) AS freecoin , SUM(IF(purchasecount > 0, 1, 0)) AS purchasecount, ". 
						" SUM(purchaseamount)/10 AS purchaseamount ".
				" FROM `tbl_user_playstat_daily` t1 WHERE today BETWEEN '$start_searchweek' AND '$end_searchweek' ".
				" AND EXISTS (SELECT * FROM `tbl_user_playstat_daily` WHERE today = DATE_SUB('$end_searchweek',INTERVAL 3 WEEK) AND useridx=t1.useridx AND days_after_install = 1) AND playcount+h_playcount >= 50;";
		
		$newuser_retention_3week = $db_other->getarray($sql);
		
		$createdate_3week = $newuser_retention_3week['createdate'];
		$platform_3week = $newuser_retention_3week['platform'];
		$moneyin_3week = $newuser_retention_3week['moneyin'];
		$moneyout_3week = $newuser_retention_3week['moneyout'];
		$winrate_3week = $newuser_retention_3week['winrate'];
		$playcount_3week = $newuser_retention_3week['playcount'];
		$playminute_3week = $newuser_retention_3week['playminute'];
		$currentcoin_3week = $newuser_retention_3week['currentcoin'];
		$freecoin_3week = $newuser_retention_3week['freecoin'];
		$purchasecount_3week = $newuser_retention_3week['purchasecount'];
		$purchaseamount_3week = $newuser_retention_3week['purchaseamount'];

		$sql = "INSERT INTO tbl_new_user_retention_week_stats(today, createdate, platform, moneyin, moneyout, winrate, playcount, playminute, currentcoin, freecoin, purchasecount, purchaseamount)".
				" VALUES('$today','$createdate_3week', $platform_3week, $moneyin_3week, $moneyout_3week, $winrate_3week ,$playcount_3week, $playminute_3week, $freecoin_3week, $currentcoin_3week, $purchasecount_3week, $purchaseamount_3week) ".
				" ON DUPLICATE KEY UPDATE  moneyin = VALUES(moneyin), moneyout= VALUES(moneyout), winrate= VALUES(winrate), playcount= VALUES(playcount), playminute= VALUES(playminute), currentcoin= VALUES(currentcoin), freecoin= VALUES(freecoin), purchasecount= VALUES(purchasecount), purchaseamount= VALUES(purchaseamount);";
		$db_other->execute($sql);
		
		// Ios
		//1 week
		$sql = " SELECT DATE_SUB('$end_searchweek',INTERVAL 1 WEEK) AS createdate, 1 AS platform, AVG(moneyin+h_moneyin) AS  moneyin, AVG(moneyout+h_moneyout) AS moneyout, AVG(playcount+h_playcount) AS playcount, ".
				" (AVG(moneyout+h_moneyout) / AVG(moneyin+h_moneyin)) * 100 AS winrate, AVG(playcount+h_playcount) AS playcount, ROUND(AVG(playcount+h_playcount)*5/60,2) AS playminute, ".
				" AVG(currentcoin) AS currentcoin, AVG(freecoin) AS freecoin , SUM(IF(purchasecount > 0, 1, 0)) AS purchasecount, ".
				" SUM(purchaseamount)/10 AS purchaseamount ".
				" FROM `tbl_user_playstat_daily_ios` t1 WHERE today BETWEEN '$start_searchweek' AND '$end_searchweek' ".
				" AND EXISTS (SELECT * FROM `tbl_user_playstat_daily_ios` WHERE today = DATE_SUB('$end_searchweek',INTERVAL 1 WEEK) AND useridx=t1.useridx AND days_after_install = 1) AND playcount+h_playcount >= 50;";
		
		$newuser_retention_1week = $db_other->getarray($sql);
		
		$createdate_1week = $newuser_retention_1week['createdate'];
		$platform_1week = $newuser_retention_1week['platform'];
		$moneyin_1week = $newuser_retention_1week['moneyin'];
		$moneyout_1week = $newuser_retention_1week['moneyout'];
		$winrate_1week = $newuser_retention_1week['winrate'];
		$playcount_1week = $newuser_retention_1week['playcount'];
		$playminute_1week = $newuser_retention_1week['playminute'];
		$currentcoin_1week = $newuser_retention_1week['currentcoin'];
		$freecoin_1week = $newuser_retention_1week['freecoin'];
		$purchasecount_1week = $newuser_retention_1week['purchasecount'];
		$purchaseamount_1week = $newuser_retention_1week['purchaseamount'];
		
		$sql = "INSERT INTO tbl_new_user_retention_week_stats(today, createdate, platform, moneyin, moneyout, winrate, playcount, playminute, currentcoin, freecoin, purchasecount, purchaseamount)".
				" VALUES('$today','$createdate_1week', $platform_1week, $moneyin_1week, $moneyout_1week, $winrate_1week ,$playcount_1week, $playminute_1week, $freecoin_1week, $currentcoin_1week, $purchasecount_1week, $purchaseamount_1week) ".
				" ON DUPLICATE KEY UPDATE  moneyin = VALUES(moneyin), moneyout= VALUES(moneyout), winrate= VALUES(winrate), playcount= VALUES(playcount), playminute= VALUES(playminute), currentcoin= VALUES(currentcoin), freecoin= VALUES(freecoin), purchasecount= VALUES(purchasecount), purchaseamount= VALUES(purchaseamount);";
		$db_other->execute($sql);
		
		//2 week
		$sql = " SELECT DATE_SUB('$end_searchweek',INTERVAL 2 WEEK) AS createdate, 1 AS platform, AVG(moneyin+h_moneyin) AS  moneyin, AVG(moneyout+h_moneyout) AS moneyout, AVG(playcount+h_playcount) AS playcount, ".
				" (AVG(moneyout+h_moneyout) / AVG(moneyin+h_moneyin)) * 100 AS winrate, AVG(playcount+h_playcount) AS playcount, ROUND(AVG(playcount+h_playcount)*5/60,2) AS playminute, ".
				" AVG(currentcoin) AS currentcoin, AVG(freecoin) AS freecoin , SUM(IF(purchasecount > 0, 1, 0)) AS purchasecount, ".
				" SUM(purchaseamount)/10 AS purchaseamount ".
				" FROM `tbl_user_playstat_daily_ios` t1 WHERE today BETWEEN '$start_searchweek' AND '$end_searchweek' ".
				" AND EXISTS (SELECT * FROM `tbl_user_playstat_daily_ios` WHERE today = DATE_SUB('$end_searchweek',INTERVAL 2 WEEK) AND useridx=t1.useridx AND days_after_install = 1) AND playcount+h_playcount >= 50;";
		
		$newuser_retention_2week = $db_other->getarray($sql);
		
		$createdate_2week = $newuser_retention_2week['createdate'];
		$platform_2week = $newuser_retention_2week['platform'];
		$moneyin_2week = $newuser_retention_2week['moneyin'];
		$moneyout_2week = $newuser_retention_2week['moneyout'];
		$winrate_2week = $newuser_retention_2week['winrate'];
		$playcount_2week = $newuser_retention_2week['playcount'];
		$playminute_2week = $newuser_retention_2week['playminute'];
		$currentcoin_2week = $newuser_retention_2week['currentcoin'];
		$freecoin_2week = $newuser_retention_2week['freecoin'];
		$purchasecount_2week = $newuser_retention_2week['purchasecount'];
		$purchaseamount_2week = $newuser_retention_2week['purchaseamount'];
		
		$sql = "INSERT INTO tbl_new_user_retention_week_stats(today, createdate, platform, moneyin, moneyout, winrate, playcount, playminute, currentcoin, freecoin, purchasecount, purchaseamount)".
				" VALUES('$today','$createdate_2week', $platform_2week, $moneyin_2week, $moneyout_2week, $winrate_2week ,$playcount_2week, $playminute_2week, $freecoin_2week, $currentcoin_2week, $purchasecount_2week, $purchaseamount_2week) ".
				" ON DUPLICATE KEY UPDATE  moneyin = VALUES(moneyin), moneyout= VALUES(moneyout), winrate= VALUES(winrate), playcount= VALUES(playcount), playminute= VALUES(playminute), currentcoin= VALUES(currentcoin), freecoin= VALUES(freecoin), purchasecount= VALUES(purchasecount), purchaseamount= VALUES(purchaseamount);";
		$db_other->execute($sql);
		
		//3 week
		$sql = " SELECT DATE_SUB('$end_searchweek',INTERVAL 3 WEEK) AS createdate, 1 AS platform,  AVG(moneyin+h_moneyin) AS  moneyin, AVG(moneyout+h_moneyout) AS moneyout, AVG(playcount+h_playcount) AS playcount, ".
				" (AVG(moneyout+h_moneyout) / AVG(moneyin+h_moneyin)) * 100 AS winrate, AVG(playcount+h_playcount) AS playcount, ROUND(AVG(playcount+h_playcount)*5/60,2) AS playminute, ".
				" AVG(currentcoin) AS currentcoin, AVG(freecoin) AS freecoin , SUM(IF(purchasecount > 0, 1, 0)) AS purchasecount, ".
				" SUM(purchaseamount)/10 AS purchaseamount ".
				" FROM `tbl_user_playstat_daily_ios` t1 WHERE today BETWEEN '$start_searchweek' AND '$end_searchweek' ".
				" AND EXISTS (SELECT * FROM `tbl_user_playstat_daily_ios` WHERE today = DATE_SUB('$end_searchweek',INTERVAL 3 WEEK) AND useridx=t1.useridx AND days_after_install = 1) AND playcount+h_playcount >= 50;";
		
		$newuser_retention_3week = $db_other->getarray($sql);
		
		$createdate_3week = $newuser_retention_3week['createdate'];
		$platform_3week = $newuser_retention_3week['platform'];
		$moneyin_3week = $newuser_retention_3week['moneyin'];
		$moneyout_3week = $newuser_retention_3week['moneyout'];
		$winrate_3week = $newuser_retention_3week['winrate'];
		$playcount_3week = $newuser_retention_3week['playcount'];
		$playminute_3week = $newuser_retention_3week['playminute'];
		$currentcoin_3week = $newuser_retention_3week['currentcoin'];
		$freecoin_3week = $newuser_retention_3week['freecoin'];
		$purchasecount_3week = $newuser_retention_3week['purchasecount'];
		$purchaseamount_3week = $newuser_retention_3week['purchaseamount'];
		
		$sql = "INSERT INTO tbl_new_user_retention_week_stats(today, createdate, platform, moneyin, moneyout, winrate, playcount, playminute, currentcoin, freecoin, purchasecount, purchaseamount)".
				" VALUES('$today','$createdate_3week', $platform_3week, $moneyin_3week, $moneyout_3week, $winrate_3week ,$playcount_3week, $playminute_3week, $freecoin_3week, $currentcoin_3week, $purchasecount_3week, $purchaseamount_3week) ".
				" ON DUPLICATE KEY UPDATE  moneyin = VALUES(moneyin), moneyout= VALUES(moneyout), winrate= VALUES(winrate), playcount= VALUES(playcount), playminute= VALUES(playminute), currentcoin= VALUES(currentcoin), freecoin= VALUES(freecoin), purchasecount= VALUES(purchasecount), purchaseamount= VALUES(purchaseamount);";
		$db_other->execute($sql);
		
		// Android
		//1 week
		$sql = " SELECT DATE_SUB('$end_searchweek',INTERVAL 1 WEEK) AS createdate, 2 AS platform, AVG(moneyin+h_moneyin) AS  moneyin, AVG(moneyout+h_moneyout) AS moneyout, AVG(playcount+h_playcount) AS playcount, ".
				" (AVG(moneyout+h_moneyout) / AVG(moneyin+h_moneyin)) * 100 AS winrate, AVG(playcount+h_playcount) AS playcount, ROUND(AVG(playcount+h_playcount)*5/60,2) AS playminute, ".
				" AVG(currentcoin) AS currentcoin, AVG(freecoin) AS freecoin , SUM(IF(purchasecount > 0, 1, 0)) AS purchasecount, ".
				" SUM(purchaseamount)/10 AS purchaseamount ".
				" FROM `tbl_user_playstat_daily_android` t1 WHERE today BETWEEN '$start_searchweek' AND '$end_searchweek' ".
				" AND EXISTS (SELECT * FROM `tbl_user_playstat_daily_android` WHERE today = DATE_SUB('$end_searchweek',INTERVAL 1 WEEK) AND useridx=t1.useridx AND days_after_install = 1) AND playcount+h_playcount >= 50;";
		
		$newuser_retention_1week = $db_other->getarray($sql);
		
		$createdate_1week = $newuser_retention_1week['createdate'];
		$platform_1week = $newuser_retention_1week['platform'];
		$moneyin_1week = $newuser_retention_1week['moneyin'];
		$moneyout_1week = $newuser_retention_1week['moneyout'];
		$winrate_1week = $newuser_retention_1week['winrate'];
		$playcount_1week = $newuser_retention_1week['playcount'];
		$playminute_1week = $newuser_retention_1week['playminute'];
		$currentcoin_1week = $newuser_retention_1week['currentcoin'];
		$freecoin_1week = $newuser_retention_1week['freecoin'];
		$purchasecount_1week = $newuser_retention_1week['purchasecount'];
		$purchaseamount_1week = $newuser_retention_1week['purchaseamount'];
		
		$sql = "INSERT INTO tbl_new_user_retention_week_stats(today, createdate, platform, moneyin, moneyout, winrate, playcount, playminute, currentcoin, freecoin, purchasecount, purchaseamount)".
				" VALUES('$today','$createdate_1week', $platform_1week, $moneyin_1week, $moneyout_1week, $winrate_1week ,$playcount_1week, $playminute_1week, $freecoin_1week, $currentcoin_1week, $purchasecount_1week, $purchaseamount_1week) ".
				" ON DUPLICATE KEY UPDATE  moneyin = VALUES(moneyin), moneyout= VALUES(moneyout), winrate= VALUES(winrate), playcount= VALUES(playcount), playminute= VALUES(playminute), currentcoin= VALUES(currentcoin), freecoin= VALUES(freecoin), purchasecount= VALUES(purchasecount), purchaseamount= VALUES(purchaseamount);";
		$db_other->execute($sql);
		
		//2 week
		$sql = " SELECT DATE_SUB('$end_searchweek',INTERVAL 2 WEEK) AS createdate, 2 AS platform, AVG(moneyin+h_moneyin) AS  moneyin, AVG(moneyout+h_moneyout) AS moneyout, AVG(playcount+h_playcount) AS playcount, ".
				" (AVG(moneyout+h_moneyout) / AVG(moneyin+h_moneyin)) * 100 AS winrate, AVG(playcount+h_playcount) AS playcount, ROUND(AVG(playcount+h_playcount)*5/60,2) AS playminute, ".
				" AVG(currentcoin) AS currentcoin, AVG(freecoin) AS freecoin , SUM(IF(purchasecount > 0, 1, 0)) AS purchasecount, ".
				" SUM(purchaseamount)/10 AS purchaseamount ".
				" FROM `tbl_user_playstat_daily_android` t1 WHERE today BETWEEN '$start_searchweek' AND '$end_searchweek' ".
				" AND EXISTS (SELECT * FROM `tbl_user_playstat_daily_android` WHERE today = DATE_SUB('$end_searchweek',INTERVAL 2 WEEK) AND useridx=t1.useridx AND days_after_install = 1) AND playcount+h_playcount >= 50;";
		
		$newuser_retention_2week = $db_other->getarray($sql);
		
		$createdate_2week = $newuser_retention_2week['createdate'];
		$platform_2week = $newuser_retention_2week['platform'];
		$moneyin_2week = $newuser_retention_2week['moneyin'];
		$moneyout_2week = $newuser_retention_2week['moneyout'];
		$winrate_2week = $newuser_retention_2week['winrate'];
		$playcount_2week = $newuser_retention_2week['playcount'];
		$playminute_2week = $newuser_retention_2week['playminute'];
		$currentcoin_2week = $newuser_retention_2week['currentcoin'];
		$freecoin_2week = $newuser_retention_2week['freecoin'];
		$purchasecount_2week = $newuser_retention_2week['purchasecount'];
		$purchaseamount_2week = $newuser_retention_2week['purchaseamount'];
		
		$sql = "INSERT INTO tbl_new_user_retention_week_stats(today, createdate, platform, moneyin, moneyout, winrate, playcount, playminute, currentcoin, freecoin, purchasecount, purchaseamount)".
				" VALUES('$today','$createdate_2week', $platform_2week, $moneyin_2week, $moneyout_2week, $winrate_2week ,$playcount_2week, $playminute_2week, $freecoin_2week, $currentcoin_2week, $purchasecount_2week, $purchaseamount_2week) ".
				" ON DUPLICATE KEY UPDATE  moneyin = VALUES(moneyin), moneyout= VALUES(moneyout), winrate= VALUES(winrate), playcount= VALUES(playcount), playminute= VALUES(playminute), currentcoin= VALUES(currentcoin), freecoin= VALUES(freecoin), purchasecount= VALUES(purchasecount), purchaseamount= VALUES(purchaseamount);";
		$db_other->execute($sql);
		
		//3 week
		$sql = " SELECT DATE_SUB('$end_searchweek',INTERVAL 3 WEEK) AS createdate, 2 AS platform,  AVG(moneyin+h_moneyin) AS  moneyin, AVG(moneyout+h_moneyout) AS moneyout, AVG(playcount+h_playcount) AS playcount, ".
				" (AVG(moneyout+h_moneyout) / AVG(moneyin+h_moneyin)) * 100 AS winrate, AVG(playcount+h_playcount) AS playcount, ROUND(AVG(playcount+h_playcount)*5/60,2) AS playminute, ".
				" AVG(currentcoin) AS currentcoin, AVG(freecoin) AS freecoin , SUM(IF(purchasecount > 0, 1, 0)) AS purchasecount, ".
				" SUM(purchaseamount)/10 AS purchaseamount ".
				" FROM `tbl_user_playstat_daily_android` t1 WHERE today BETWEEN '$start_searchweek' AND '$end_searchweek' ".
				" AND EXISTS (SELECT * FROM `tbl_user_playstat_daily_android` WHERE today = DATE_SUB('$end_searchweek',INTERVAL 3 WEEK) AND useridx=t1.useridx AND days_after_install = 1) AND playcount+h_playcount >= 50;";
		
		$newuser_retention_3week = $db_other->getarray($sql);
		
		$createdate_3week = $newuser_retention_3week['createdate'];
		$platform_3week = $newuser_retention_3week['platform'];
		$moneyin_3week = $newuser_retention_3week['moneyin'];
		$moneyout_3week = $newuser_retention_3week['moneyout'];
		$winrate_3week = $newuser_retention_3week['winrate'];
		$playcount_3week = $newuser_retention_3week['playcount'];
		$playminute_3week = $newuser_retention_3week['playminute'];
		$currentcoin_3week = $newuser_retention_3week['currentcoin'];
		$freecoin_3week = $newuser_retention_3week['freecoin'];
		$purchasecount_3week = $newuser_retention_3week['purchasecount'];
		$purchaseamount_3week = $newuser_retention_3week['purchaseamount'];
		
		$sql = "INSERT INTO tbl_new_user_retention_week_stats(today, createdate, platform, moneyin, moneyout, winrate, playcount, playminute, currentcoin, freecoin, purchasecount, purchaseamount)".
				" VALUES('$today','$createdate_3week', $platform_3week, $moneyin_3week, $moneyout_3week, $winrate_3week ,$playcount_3week, $playminute_3week, $freecoin_3week, $currentcoin_3week, $purchasecount_3week, $purchaseamount_3week) ".
				" ON DUPLICATE KEY UPDATE  moneyin = VALUES(moneyin), moneyout= VALUES(moneyout), winrate= VALUES(winrate), playcount= VALUES(playcount), playminute= VALUES(playminute), currentcoin= VALUES(currentcoin), freecoin= VALUES(freecoin), purchasecount= VALUES(purchasecount), purchaseamount= VALUES(purchaseamount);";
		$db_other->execute($sql);
		
		// Amazon
		//1 week
		$sql = " SELECT DATE_SUB('$end_searchweek',INTERVAL 1 WEEK) AS createdate, 3 AS platform, AVG(moneyin+h_moneyin) AS  moneyin, AVG(moneyout+h_moneyout) AS moneyout, AVG(playcount+h_playcount) AS playcount, ".
				" (AVG(moneyout+h_moneyout) / AVG(moneyin+h_moneyin)) * 100 AS winrate, AVG(playcount+h_playcount) AS playcount, ROUND(AVG(playcount+h_playcount)*5/60,2) AS playminute, ".
				" AVG(currentcoin) AS currentcoin, AVG(freecoin) AS freecoin , SUM(IF(purchasecount > 0, 1, 0)) AS purchasecount, ".
				" SUM(purchaseamount)/10 AS purchaseamount ".
				" FROM `tbl_user_playstat_daily_amazon` t1 WHERE today BETWEEN '$start_searchweek' AND '$end_searchweek' ".
				" AND EXISTS (SELECT * FROM `tbl_user_playstat_daily_amazon` WHERE today = DATE_SUB('$end_searchweek',INTERVAL 1 WEEK) AND useridx=t1.useridx AND days_after_install = 1) AND playcount+h_playcount >= 50;";
		
		$newuser_retention_1week = $db_other->getarray($sql);
		
		$createdate_1week = $newuser_retention_1week['createdate'];
		$platform_1week = $newuser_retention_1week['platform'];
		$moneyin_1week = $newuser_retention_1week['moneyin'];
		$moneyout_1week = $newuser_retention_1week['moneyout'];
		$winrate_1week = $newuser_retention_1week['winrate'];
		$playcount_1week = $newuser_retention_1week['playcount'];
		$playminute_1week = $newuser_retention_1week['playminute'];
		$currentcoin_1week = $newuser_retention_1week['currentcoin'];
		$freecoin_1week = $newuser_retention_1week['freecoin'];
		$purchasecount_1week = $newuser_retention_1week['purchasecount'];
		$purchaseamount_1week = $newuser_retention_1week['purchaseamount'];
		
		$sql = "INSERT INTO tbl_new_user_retention_week_stats(today, createdate, platform, moneyin, moneyout, winrate, playcount, playminute, currentcoin, freecoin, purchasecount, purchaseamount)".
				" VALUES('$today','$createdate_1week', $platform_1week, $moneyin_1week, $moneyout_1week, $winrate_1week ,$playcount_1week, $playminute_1week, $freecoin_1week, $currentcoin_1week, $purchasecount_1week, $purchaseamount_1week) ".
				" ON DUPLICATE KEY UPDATE  moneyin = VALUES(moneyin), moneyout= VALUES(moneyout), winrate= VALUES(winrate), playcount= VALUES(playcount), playminute= VALUES(playminute), currentcoin= VALUES(currentcoin), freecoin= VALUES(freecoin), purchasecount= VALUES(purchasecount), purchaseamount= VALUES(purchaseamount);";
		$db_other->execute($sql);
		
		//2 week
		$sql = " SELECT DATE_SUB('$end_searchweek',INTERVAL 2 WEEK) AS createdate, 3 AS platform, AVG(moneyin+h_moneyin) AS  moneyin, AVG(moneyout+h_moneyout) AS moneyout, AVG(playcount+h_playcount) AS playcount, ".
				" (AVG(moneyout+h_moneyout) / AVG(moneyin+h_moneyin)) * 100 AS winrate, AVG(playcount+h_playcount) AS playcount, ROUND(AVG(playcount+h_playcount)*5/60,2) AS playminute, ".
				" AVG(currentcoin) AS currentcoin, AVG(freecoin) AS freecoin , SUM(IF(purchasecount > 0, 1, 0)) AS purchasecount, ".
				" SUM(purchaseamount)/10 AS purchaseamount ".
				" FROM `tbl_user_playstat_daily_amazon` t1 WHERE today BETWEEN '$start_searchweek' AND '$end_searchweek' ".
				" AND EXISTS (SELECT * FROM `tbl_user_playstat_daily_amazon` WHERE today = DATE_SUB('$end_searchweek',INTERVAL 2 WEEK) AND useridx=t1.useridx AND days_after_install = 1) AND playcount+h_playcount >= 50;";
		
		$newuser_retention_2week = $db_other->getarray($sql);
		
		$createdate_2week = $newuser_retention_2week['createdate'];
		$platform_2week = $newuser_retention_2week['platform'];
		$moneyin_2week = $newuser_retention_2week['moneyin'];
		$moneyout_2week = $newuser_retention_2week['moneyout'];
		$winrate_2week = $newuser_retention_2week['winrate'];
		$playcount_2week = $newuser_retention_2week['playcount'];
		$playminute_2week = $newuser_retention_2week['playminute'];
		$currentcoin_2week = $newuser_retention_2week['currentcoin'];
		$freecoin_2week = $newuser_retention_2week['freecoin'];
		$purchasecount_2week = $newuser_retention_2week['purchasecount'];
		$purchaseamount_2week = $newuser_retention_2week['purchaseamount'];
		
		$sql = "INSERT INTO tbl_new_user_retention_week_stats(today, createdate, platform, moneyin, moneyout, winrate, playcount, playminute, currentcoin, freecoin, purchasecount, purchaseamount)".
				" VALUES('$today','$createdate_2week', $platform_2week, $moneyin_2week, $moneyout_2week, $winrate_2week ,$playcount_2week, $playminute_2week, $freecoin_2week, $currentcoin_2week, $purchasecount_2week, $purchaseamount_2week) ".
				" ON DUPLICATE KEY UPDATE  moneyin = VALUES(moneyin), moneyout= VALUES(moneyout), winrate= VALUES(winrate), playcount= VALUES(playcount), playminute= VALUES(playminute), currentcoin= VALUES(currentcoin), freecoin= VALUES(freecoin), purchasecount= VALUES(purchasecount), purchaseamount= VALUES(purchaseamount);";
		$db_other->execute($sql);
		
		//3 week
		$sql = " SELECT DATE_SUB('$end_searchweek',INTERVAL 3 WEEK) AS createdate, 3 AS platform,  AVG(moneyin+h_moneyin) AS  moneyin, AVG(moneyout+h_moneyout) AS moneyout, AVG(playcount+h_playcount) AS playcount, ".
				" (AVG(moneyout+h_moneyout) / AVG(moneyin+h_moneyin)) * 100 AS winrate, AVG(playcount+h_playcount) AS playcount, ROUND(AVG(playcount+h_playcount)*5/60,2) AS playminute, ".
				" AVG(currentcoin) AS currentcoin, AVG(freecoin) AS freecoin , SUM(IF(purchasecount > 0, 1, 0)) AS purchasecount, ".
				" SUM(purchaseamount)/10 AS purchaseamount ".
				" FROM `tbl_user_playstat_daily_amazon` t1 WHERE today BETWEEN '$start_searchweek' AND '$end_searchweek' ".
				" AND EXISTS (SELECT * FROM `tbl_user_playstat_daily_amazon` WHERE today = DATE_SUB('$end_searchweek',INTERVAL 3 WEEK) AND useridx=t1.useridx AND days_after_install = 1) AND playcount+h_playcount >= 50;";
		
		$newuser_retention_3week = $db_other->getarray($sql);
		
		$createdate_3week = $newuser_retention_3week['createdate'];
		$platform_3week = $newuser_retention_3week['platform'];
		$moneyin_3week = ($newuser_retention_3week['moneyin'] == "")?0:$newuser_retention_3week['moneyin'];
		$moneyout_3week = ($newuser_retention_3week['moneyout'] == "")?0:$newuser_retention_3week['moneyout'];
		$winrate_3week = ($newuser_retention_3week['winrate'] == "")?0:$newuser_retention_3week['winrate'];
		$playcount_3week = ($newuser_retention_3week['playcount'] == "")?0:$newuser_retention_3week['playcount'];
		$playminute_3week = ($newuser_retention_3week['playminute'] == "")?0:$newuser_retention_3week['playminute'];
		$currentcoin_3week = ($newuser_retention_3week['currentcoin'] == "")?0:$newuser_retention_3week['currentcoin'];
		$freecoin_3week = ($newuser_retention_3week['freecoin'] == "")?0:$newuser_retention_3week['freecoin'];
		$purchasecount_3week = ($newuser_retention_3week['purchasecount'] == "")?0:$newuser_retention_3week['purchasecount'];
		$purchaseamount_3week = ($newuser_retention_3week['purchaseamount'] == "")?0:$newuser_retention_3week['purchaseamount'];
		
		$sql = "INSERT INTO tbl_new_user_retention_week_stats(today, createdate, platform, moneyin, moneyout, winrate, playcount, playminute, currentcoin, freecoin, purchasecount, purchaseamount)".
				" VALUES('$today','$createdate_3week', $platform_3week, $moneyin_3week, $moneyout_3week, $winrate_3week ,$playcount_3week, $playminute_3week, $freecoin_3week, $currentcoin_3week, $purchasecount_3week, $purchaseamount_3week) ".
				" ON DUPLICATE KEY UPDATE  moneyin = VALUES(moneyin), moneyout= VALUES(moneyout), winrate= VALUES(winrate), playcount= VALUES(playcount), playminute= VALUES(playminute), currentcoin= VALUES(currentcoin), freecoin= VALUES(freecoin), purchasecount= VALUES(purchasecount), purchaseamount= VALUES(purchaseamount);";
		$db_other->execute($sql);	
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	$db_other->end();
?>
