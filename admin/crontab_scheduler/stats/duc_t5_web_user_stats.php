<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	ini_set("memory_limit", "-1");
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	    if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/stats/duc_t5_web_user_stats") !== false)
	    {
	        $count++;
	    }
	}
	
	if ($count > 1)
	{
	    $count = 0;
	    
	    $killcontents = "#!/bin/bash\n";
	    
	    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	    
	    $fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	    
	    flock($fp, LOCK_EX);
	    
	    if (!$fp) {
	        echo "Fail";
	        exit;
	    }
	    else
	    {
	        echo "OK";
	    }
	    
	    flock($fp, LOCK_UN);
	    
	    $content = "#!/bin/bash\n";
	    
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	        if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/stats/duc_t5_web_user_stats") !== false)
	        {
	            $process_list = explode("|", $result[$i]);
	            
	            $content .= "kill -9 ".$process_list[0]."\n";
	            
	            write_log("stats/duc_t5_web_user_stats Dead Lock Kill!");
	        }
	    }
	    
	    fwrite($fp, $content, strlen($content));
	    fclose($fp);
	    
	    exit();
	}
	$db_other = new CDatabase_Other();
	$db_redshift = new CDatabase_Redshift();
	
	$db_other->execute("SET wait_timeout=7200");	
	
	$today = date("Y-m-d", time() - 60 * 60 * 24);
	$tomorrow = date("Y-m-d", time() + 60 * 60);
	
	try 
	{
		// Duc Game 
		$sql = "select count(distinct duc_useridx) AS cnt_duc_user, SUM(a2.playcount) AS duc_playcount, sum(a2.money_in) AS duc_money_in, sum(a2.money_out) AS duc_money_out ".
				"from (	".
  				"	select t1.useridx as duc_useridx, t2.useridx as t5_useridx	".
  				"	from (	".
    			"		select *	". 
    			"		from duc_user	".
    			"		where logindate >= '$today 00:00:00' and fb_token != ''	".
  				"	) t1 join (	".
    			"		select *	". 
    			"		from t5_user	".
    			"		where logindate >= '$today 00:00:00' and fb_token != ''	".
  				"	) t2 on t1.fb_token = t2.fb_token	".
				") a1 join duc_user_gamelog a2 on a1.duc_useridx = a2.useridx and a2.writedate >= '$today 00:00:00' and a2.writedate < '$tomorrow 00:00:00'";
		$duc_game_info = $db_redshift->getarray($sql);
		
		$cnt_duc_user = ($duc_game_info["cnt_duc_user"]=="")? 0 : $duc_game_info["cnt_duc_user"];
		$duc_playcount =($duc_game_info["duc_playcount"]=="")? 0 : $duc_game_info["duc_playcount"];
		$duc_money_in = ($duc_game_info["duc_money_in"]=="")? 0 : $duc_game_info["duc_money_in"];
		$duc_money_out = ($duc_game_info["duc_money_out"]=="")? 0 : $duc_game_info["duc_money_out"];
		
		// T5 Game
		$sql = "select count(distinct t5_useridx) AS cnt_t5_user, SUM(a2.playcount) AS t5_playcount, sum(a2.money_in) AS t5_money_in, sum(a2.money_out)  AS t5_money_out ".
				"from (	".
  				"	select t1.useridx as duc_useridx, t2.useridx as t5_useridx	".
  				"	from (	".
    			"		select *	". 
    			"		from duc_user	".
    			"		where logindate >= '$today 00:00:00' and fb_token != ''	".
  				"	) t1 join (	".
    			"		select *	". 
    			"		from t5_user	".
    			"		where logindate >= '$today 00:00:00' and fb_token != ''	".
  				"	) t2 on t1.fb_token = t2.fb_token	".
				") a1 join t5_user_gamelog a2 on a1.t5_useridx = a2.useridx and a2.writedate >= '$today 00:00:00' and a2.writedate < '$tomorrow 00:00:00'";
		$t5_game_info = $db_redshift->getarray($sql);
		
		$cnt_t5_user = ($t5_game_info["cnt_t5_user"]=="")?0:$t5_game_info["cnt_t5_user"];
		$t5_playcount = ($t5_game_info["t5_playcount"]=="")?0:$t5_game_info["t5_playcount"];
		$t5_money_in = ($t5_game_info["t5_money_in"]=="")?0:$t5_game_info["t5_money_in"];
		$t5_money_out = ($t5_game_info["t5_money_out"]=="")?0:$t5_game_info["t5_money_out"];
		
		//Duc Pay
		$sql = "select count(distinct duc_useridx) AS cnt_duc_pay_user, count(orderidx) AS duc_pay, round(SUM(a2.facebookcredit/10)) AS duc_sum_money ".
				"from (	".
  				"	select t1.useridx as duc_useridx, t2.useridx as t5_useridx	".
  				"	from (	".
    			"		select *	". 
    			"		from duc_user	".
    			"		where logindate >= '$today 00:00:00' and fb_token != ''	".
  				"	) t1 join (	".
    			"		select *	". 
    			"		from t5_user	".
    			"		where logindate >= '$today 00:00:00' and fb_token != ''	".
  				"	) t2 on t1.fb_token = t2.fb_token	".
				") a1 join duc_product_order a2 on a1.duc_useridx = a2.useridx and a2.writedate >= '$today 00:00:00' and a2.writedate < '$tomorrow 00:00:00' and a2.status = 1 and a2.useridx > 10090";		
		$duc_pay_info = $db_redshift->getarray($sql);
		
		$cnt_duc_pay_user = ($duc_pay_info["cnt_duc_pay_user"]=="")?0:$duc_pay_info["cnt_duc_pay_user"];
		$duc_pay = ($duc_pay_info["duc_pay"]=="")?0:$duc_pay_info["duc_pay"];
		$duc_sum_money = ($duc_pay_info["duc_sum_money"]=="")?0:$duc_pay_info["duc_sum_money"];
		
		//T5 Pay
		$sql = "select count(distinct t5_useridx) AS cnt_t5_pay_user, count(orderidx) AS t5_pay, round(SUM(a2.facebookcredit/10)) AS t5_sum_money ".
				"from (	".
  				"	select t1.useridx as duc_useridx, t2.useridx as t5_useridx	".
  				"	from (	".
    			"		select *	". 
    			"		from duc_user	".
    			"		where logindate >= '$today 00:00:00' and fb_token != ''	".
  				"	) t1 join (	".
    			"		select *	". 
    			"		from t5_user	".
    			"		where logindate >= '$today 00:00:00' and fb_token != ''	".
  				"	) t2 on t1.fb_token = t2.fb_token	".
				") a1 join t5_product_order a2 on a1.t5_useridx = a2.useridx and a2.writedate >= '$today 00:00:00' and a2.writedate < '$tomorrow 00:00:00' and a2.status = 1 and a2.useridx > 20000";		
		$t5_pay_info = $db_redshift->getarray($sql);
		
		$cnt_t5_pay_user = ($t5_pay_info["cnt_t5_pay_user"]=="")?0:$t5_pay_info["cnt_t5_pay_user"];
		$t5_pay = ($t5_pay_info["t5_pay"]=="")?0:$t5_pay_info["t5_pay"];
		$t5_sum_money = ($t5_pay_info["t5_sum_money"]=="")?0:$t5_pay_info["t5_sum_money"];
		
		// Duc Dua
		$sql = "select count(*) from duc_user where logindate >= '$today 00:00:00' AND logindate < '$tomorrow 00:00:00'";
		$duc_dau = $db_redshift->getvalue($sql);
		$duc_dau = ($duc_dau=="")?0:$duc_dau;
		// T5 Dua
		$sql = "select count(*) from t5_user where logindate >= '$today 00:00:00' AND logindate < '$tomorrow 00:00:00'";
		$t5_dau = $db_redshift->getvalue($sql);
		$t5_dau = ($t5_dau=="")?0:$t5_dau;
		
		
		$sql = "INSERT INTO tbl_duc_t5_web_user_daily(today, duc_usercount, duc_playcount, duc_moneyin, duc_moneyout, duc_dau, duc_payusercount, duc_paycount, duc_money, ".
				" t5_usercount, t5_playcount, t5_moneyin, t5_moneyout, t5_dau, t5_payusercount, t5_paycount, t5_money) ".
				"VALUES('$today', $cnt_duc_user, $duc_playcount, $duc_money_in, $duc_money_out, $duc_dau, $cnt_duc_pay_user, $duc_pay, $duc_sum_money, ".
				" $cnt_t5_user, $t5_playcount, $t5_money_in, $t5_money_out, $t5_dau, $cnt_t5_pay_user, $t5_pay, $t5_sum_money);";
		$db_other->execute($sql);
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_other->end();	
	$db_redshift->end();
	
?>