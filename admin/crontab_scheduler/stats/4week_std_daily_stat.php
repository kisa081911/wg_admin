<?
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	$db_redshift = new CDatabase_Redshift();
	
	$db_main->execute("SET wait_timeout=43200");
	$db_main2->execute("SET wait_timeout=43200");
	$db_analysis->execute("SET wait_timeout=43200");
	
	ini_set("memory_limit", "-1");

	try
	{
	    $sdate = date("Y-m-d", time() - 24 * 60 * 60 * 5);
	    $edate = date("Y-m-d", time() + 24 * 60 * 60);
	    
	    
	    $sdate = "2018-11-01";
	    
	    while($sdate < $edate)
	    {
	        $temp_date = date('Y-m-d', strtotime($sdate.' + 1 day'));
	        
	        $join_sdate = "$sdate";
	        $join_edate = "$temp_date";
	        
	        $write_sdate = $join_sdate." 00:00:00";
	        $write_edate = $join_edate." 00:00:00";
	        
	        // 28일 결제액
	        // 전체
	        $sql = "SELECT DATE_FORMAT(MAX(writedate), '%Y-%m-%d') AS today, 100 AS platform, ROUND(SUM(money), 2) AS total_money, COUNT(DISTINCT useridx) AS payer_cnt ".
                    "FROM ( ".
                    "   SELECT 0 AS platform, useridx, (facebookcredit/10) AS money, writedate ".
                    "   FROM tbl_product_order ".
                    "   WHERE useridx > 20000 AND STATUS = 1 AND DATE_SUB('$write_sdate', INTERVAL 28 DAY) <= writedate AND writedate < '$write_sdate' ".
                    "   UNION ALL ".
                    "   SELECT os_type AS platform, useridx, money, writedate ".
                    "   FROM tbl_product_order_mobile ".
                    "   WHERE useridx > 20000 AND STATUS = 1 AND DATE_SUB('$write_sdate', INTERVAL 28 DAY) <= writedate AND writedate < '$write_sdate' ".
                    ") t1";
	        $date_list = $db_main->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $today = $date_list[$i]["today"];
	            $platform = $date_list[$i]["platform"];
	            $total_money = $date_list[$i]["total_money"];
	            $payer_cnt = $date_list[$i]["payer_cnt"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, money_4week, payer_cnt_4week) ".
    	   	            "VALUES('$join_sdate', $platform, $total_money, $payer_cnt) ".
    	   	            "ON DUPLICATE KEY UPDATE money_4week=VALUES(money_4week), payer_cnt_4week=VALUES(payer_cnt_4week);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 플랫폼별
	        $sql = "SELECT DATE_FORMAT(MAX(writedate), '%Y-%m-%d') AS today, platform, ROUND(SUM(money), 2) AS total_money, COUNT(DISTINCT useridx) AS payer_cnt ".
    	   	        "FROM ( ".
    	   	        "   SELECT 0 AS platform, useridx, (facebookcredit/10) AS money, writedate ".
    	   	        "   FROM tbl_product_order ".
    	   	        "   WHERE useridx > 20000 AND STATUS = 1 AND DATE_SUB('$write_sdate', INTERVAL 28 DAY) <= writedate AND writedate < '$write_sdate' ".
    	   	        "   UNION ALL ".
    	   	        "   SELECT os_type AS platform, useridx, money, writedate ".
    	   	        "   FROM tbl_product_order_mobile ".
    	   	        "   WHERE useridx > 20000 AND STATUS = 1 AND DATE_SUB('$write_sdate', INTERVAL 28 DAY) <= writedate AND writedate < '$write_sdate' ".
    	   	        ") t1 ".
    	   	        "GROUP BY platform";
	        $date_list = $db_main->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $today = $date_list[$i]["today"];
	            $platform = $date_list[$i]["platform"];
	            $total_money = $date_list[$i]["total_money"];
	            $payer_cnt = $date_list[$i]["payer_cnt"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, money_4week, payer_cnt_4week) ".
    	   	            "VALUES('$join_sdate', $platform, $total_money, $payer_cnt) ".
    	   	            "ON DUPLICATE KEY UPDATE money_4week=VALUES(money_4week), payer_cnt_4week=VALUES(payer_cnt_4week);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 28일 활동 유저수
	        // 전체
	        $sql = "select count(distinct useridx) as user_cnt ".
    	   	        "from ( ".
    	   	        "   select 0 as platform, useridx, writedate ".
    	   	        "   from t5_user_gamelog ".
    	   	        "   where writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate' ".
    	   	        "   union all ".
    	   	        "   select 1 as platform, useridx, writedate ".
    	   	        "   from t5_user_gamelog_ios ".
    	   	        "   where writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate' ".
    	   	        "   union all ".
    	   	        "   select 2 as platform, useridx, writedate ".
    	   	        "   from t5_user_gamelog_android ".
    	   	        "   where writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate' ".
    	   	        "   union all ".
    	   	        "   select 3 as platform, useridx, writedate ".
    	   	        "   from t5_user_gamelog_amazon ".
    	   	        "   where writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate' ".
    	   	        ") t1";
	        $date_list = $db_redshift->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $user_cnt = $date_list[$i]["user_cnt"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, active_user_4week) ".
    	   	            "VALUES('$join_sdate', 100, $user_cnt) ".
    	   	            "ON DUPLICATE KEY UPDATE active_user_4week=VALUES(active_user_4week);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 플랫폼별
	        $sql = "select platform, count(distinct useridx) as user_cnt ".
    	   	        "from ( ".
    	   	        "   select 0 as platform, useridx, writedate ".
    	   	        "   from t5_user_gamelog ".
    	   	        "   where writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate' ".
    	   	        "   union all ".
    	   	        "   select 1 as platform, useridx, writedate ".
    	   	        "   from t5_user_gamelog_ios ".
    	   	        "   where writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate' ".
    	   	        "   union all ".
    	   	        "   select 2 as platform, useridx, writedate ".
    	   	        "   from t5_user_gamelog_android ".
    	   	        "   where writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate' ".
    	   	        "   union all ".
    	   	        "   select 3 as platform, useridx, writedate ".
    	   	        "   from t5_user_gamelog_amazon ".
    	   	        "   where writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate' ".
    	   	        ") t1 ".
    	   	        "group by platform ".
    	   	        "order by platform asc";
	        $date_list = $db_redshift->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = $date_list[$i]["platform"];
	            $user_cnt = $date_list[$i]["user_cnt"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, active_user_4week) ".
    	   	            "VALUES('$join_sdate', $platform, $user_cnt) ".
    	   	            "ON DUPLICATE KEY UPDATE active_user_4week=VALUES(active_user_4week);";
	            $db_analysis->execute($sql);
	        }

	        // vip별 결제금액
	        // 전체
	        $sql = "select vip_type, count(useridx) as user_cnt, sum(four_week_money) as d28_money ".
                    "from ( ".
                    "   select t1.useridx, four_week_money, total_money, (case when total_money >= 499 and total_money < 4000 then 'vip' when total_money >= 4000 then 'vvip' else 'nonvip' end) as vip_type ".
                    "   from ( ".
                    "       select useridx, sum(money) as four_week_money ".
                    "       from t5_product_order_all ".
                    "       where useridx > 20000 and status = 1 and writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate' ".
                    "       group by useridx ".
                    "   ) t1 join ( ".
                    "       select useridx, sum(money) as total_money ".
                    "       from t5_product_order_all ".
                    "       where useridx > 20000 and status = 1 and writedate < '$write_sdate' ".
                    "       group by useridx ".
                    "   ) t2 on t1.useridx = t2.useridx ".
                    ") total ".
                    "group by vip_type";
	        $date_list = $db_redshift->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = 100;
	            $vip_type = $date_list[$i]["vip_type"];
	            $user_cnt = $date_list[$i]["user_cnt"];
	            $d28_money = $date_list[$i]["d28_money"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, ".$vip_type."_cnt_4week, ".$vip_type."_money_4week) ".
    	   	            "VALUES('$join_sdate', $platform, $user_cnt, $d28_money) ".
    	   	            "ON DUPLICATE KEY UPDATE ".$vip_type."_cnt_4week=VALUES(".$vip_type."_cnt_4week), ".$vip_type."_money_4week=VALUES(".$vip_type."_money_4week);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 플랫폼별
	        $sql = "select os_type, vip_type, count(useridx) as user_cnt, sum(four_week_money) as d28_money ".
                    "from ( ".
                    "   select t1.os_type, t1.useridx, four_week_money, total_money, (case when total_money >= 499 and total_money < 4000 then 'vip' when total_money >= 4000 then 'vvip' else 'nonvip' end) as vip_type ".
                    "   from ( ".
                    "       select os_type, useridx, sum(money) as four_week_money ".
                    "       from t5_product_order_all ".
                    "       where useridx > 20000 and status = 1 and writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate' ".
                    "       group by os_type, useridx ".
                    "   ) t1 join ( ".
                    "       select os_type, useridx, sum(money) as total_money ".
                    "       from t5_product_order_all ".
                    "       where useridx > 20000 and status = 1 and writedate < '$write_sdate' ".
                    "       group by os_type, useridx ".
                    "   ) t2 on t1.os_type = t2.os_type and t1.useridx = t2.useridx ".
                    ") total ".
                    "group by os_type, vip_type";
	        $date_list = $db_redshift->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = $date_list[$i]["os_type"];
	            $vip_type = $date_list[$i]["vip_type"];
	            $user_cnt = $date_list[$i]["user_cnt"];
	            $d28_money = $date_list[$i]["d28_money"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, ".$vip_type."_cnt_4week, ".$vip_type."_money_4week) ".
    	   	            "VALUES('$join_sdate', $platform, $user_cnt, $d28_money) ".
    	   	            "ON DUPLICATE KEY UPDATE ".$vip_type."_cnt_4week=VALUES(".$vip_type."_cnt_4week), ".$vip_type."_money_4week=VALUES(".$vip_type."_money_4week);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 신규 가입자수
	        // 전체
	        $sql = "select count(useridx) AS user_cnt ".
    	   	        "from t5_user ".
    	   	        "where useridx > 20000 and createdate >= dateadd(day,(-7*4),'$write_sdate') and createdate < '$write_sdate'";
	        $date_list = $db_redshift->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = 100;
	            $user_cnt = $date_list[$i]["user_cnt"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, new_user_cnt_4week) ".
    	   	            "VALUES('$join_sdate', $platform, $user_cnt) ".
    	   	            "ON DUPLICATE KEY UPDATE new_user_cnt_4week=VALUES(new_user_cnt_4week);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 플랫폼별
	        $sql = "select platform, count(useridx) as user_cnt ".
    	   	        "from t5_user ".
    	   	        "where useridx > 20000 and createdate >= dateadd(day,(-7*4),'$write_sdate') and createdate < '$write_sdate' ".
    	   	        "group by platform";
	        $date_list = $db_redshift->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = $date_list[$i]["platform"];
	            $user_cnt = $date_list[$i]["user_cnt"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, new_user_cnt_4week) ".
    	   	            "VALUES('$join_sdate', $platform, $user_cnt) ".
    	   	            "ON DUPLICATE KEY UPDATE new_user_cnt_4week=VALUES(new_user_cnt_4week);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 신규 가입자 중 결제자
	        // 전체
	        $sql = "select count(distinct t1.useridx) user_cnt, round(sum(money), 2) as total_money ".
    	   	        "from ( ".
    	   	        "   select useridx ".
    	   	        "   from t5_user ".
    	   	        "   where useridx > 20000 and createdate >= dateadd(day,(-7*4),'$write_sdate') and createdate < '$write_sdate' ".
    	   	        ") t1 join t5_product_order_all t2 on t1.useridx = t2.useridx ".
    	   	        "where status = 1 and writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate'";
	        $date_list = $db_redshift->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = 100;
	            $user_cnt = $date_list[$i]["user_cnt"];
	            $total_money = $date_list[$i]["total_money"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, new_user_payer_4week, new_user_money_4week) ".
    	   	            "VALUES('$join_sdate', $platform, $user_cnt, $total_money) ".
    	   	            "ON DUPLICATE KEY UPDATE new_user_payer_4week=VALUES(new_user_payer_4week), new_user_money_4week=VALUES(new_user_money_4week);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 플랫폼별
	        $sql = "select platform, count(distinct t1.useridx) user_cnt, round(sum(money), 2) as total_money ".
	   	        "from ( ".
	   	        "   select platform, useridx ".
	   	        "   from t5_user ".
	   	        "   where useridx > 20000 and createdate >= dateadd(day,(-7*4),'$write_sdate') and createdate < '$write_sdate' ".
	   	        ") t1 join t5_product_order_all t2 on t1.useridx = t2.useridx ".
	   	        "where status = 1 and writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate' ".
	   	        "group by platform";
	        $date_list = $db_redshift->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = $date_list[$i]["platform"];
	            $user_cnt = $date_list[$i]["user_cnt"];
	            $total_money = $date_list[$i]["total_money"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, new_user_payer_4week, new_user_money_4week) ".
    	   	            "VALUES('$join_sdate', $platform, $user_cnt, $total_money) ".
    	   	            "ON DUPLICATE KEY UPDATE new_user_payer_4week=VALUES(new_user_payer_4week), new_user_money_4week=VALUES(new_user_money_4week);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 기존 가입자 중 최근 28일 첫 결제자
	        // 전체
	        $sql = "select count(distinct tt1.useridx) as user_cnt, sum(sum_money) as total_money ".
    	   	        "from ( ".
    	   	        "   select t1.useridx, SUM(money) as sum_money ".
    	   	        "   from ( ".
    	   	        "       select useridx, min(writedate) as min_writedate ".
    	   	        "       from t5_product_order_all ".
    	   	        "       where useridx > 20000 and status = 1 ".
    	   	        "       group by useridx ".
    	   	        "       having min_writedate >= dateadd(day,(-7*4),'$write_sdate') and min_writedate < '$write_sdate' ".
    	   	        "   ) t1 join t5_product_order_all t2 on t1.useridx = t2.useridx ".
    	   	        "   where status = 1 and writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate' ".
    	   	        "   group by t1.useridx ".
    	   	        ") tt1 join t5_user tt2 on tt1.useridx = tt2.useridx ".
    	   	        "where createdate < dateadd(day,(-7*4),'$write_sdate')";
	        $date_list = $db_redshift->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = 100;
	            $user_cnt = $date_list[$i]["user_cnt"];
	            $total_money = $date_list[$i]["total_money"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, olduser_new_payer_4week, olduser_new_money_4week) ".
    	   	            "VALUES('$join_sdate', $platform, $user_cnt, $total_money) ".
    	   	            "ON DUPLICATE KEY UPDATE olduser_new_payer_4week=VALUES(olduser_new_payer_4week), olduser_new_money_4week=VALUES(olduser_new_money_4week);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 플랫폼별
	        $sql = "select platform, count(distinct tt1.useridx) as user_cnt, sum(sum_money) as total_money ".
    	   	        "from ( ".
    	   	        "   select t1.useridx, SUM(money) as sum_money ".
    	   	        "   from ( ".
    	   	        "       select useridx, min(writedate) as min_writedate ".
    	   	        "       from t5_product_order_all ".
    	   	        "       where useridx > 20000 and status = 1 ".
    	   	        "       group by useridx ".
    	   	        "       having min_writedate >= dateadd(day,(-7*4),'$write_sdate') and min_writedate < '$write_sdate' ".
    	   	        "   ) t1 join t5_product_order_all t2 on t1.useridx = t2.useridx ".
    	   	        "   where status = 1 and writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate' ".
    	   	        "   group by t1.useridx ".
    	   	        ") tt1 join t5_user tt2 on tt1.useridx = tt2.useridx ".
    	   	        "where createdate < dateadd(day,(-7*4),'$write_sdate') ".
    	   	        "group by platform";
	        $date_list = $db_redshift->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = $date_list[$i]["platform"];
	            $user_cnt = $date_list[$i]["user_cnt"];
	            $total_money = $date_list[$i]["total_money"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, olduser_new_payer_4week, olduser_new_money_4week) ".
    	   	            "VALUES('$join_sdate', $platform, $user_cnt, $total_money) ".
    	   	            "ON DUPLICATE KEY UPDATE olduser_new_payer_4week=VALUES(olduser_new_payer_4week), olduser_new_money_4week=VALUES(olduser_new_money_4week);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 기존 가입자 중 최근 28일 기존 결제자
	        // 전체
	        $sql = "select count(distinct tt1.useridx) as user_cnt, sum(sum_money) as total_money ".
    	   	        "from ( ".
    	   	        "   select t1.useridx, SUM(money) as sum_money ".
    	   	        "   from ( ".
    	   	        "       select useridx, min(writedate) as min_writedate ".
    	   	        "       from t5_product_order_all ".
    	   	        "       where useridx > 20000 and status = 1 ".
    	   	        "       group by useridx ".
    	   	        "       having min_writedate < dateadd(day,(-7*4),'$write_sdate') ".
    	   	        "   ) t1 join t5_product_order_all t2 on t1.useridx = t2.useridx ".
    	   	        "   where status = 1 and writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate' ".
    	   	        "   group by t1.useridx ".
    	   	        ") tt1 join t5_user tt2 on tt1.useridx = tt2.useridx ".
    	   	        "where createdate < dateadd(day,(-7*4),'$write_sdate')";
	        $date_list = $db_redshift->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = 100;
	            $user_cnt = $date_list[$i]["user_cnt"];
	            $total_money = $date_list[$i]["total_money"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, olduser_re_payer_4week, olduser_re_money_4week) ".
    	   	            "VALUES('$join_sdate', $platform, $user_cnt, $total_money) ".
    	   	            "ON DUPLICATE KEY UPDATE olduser_re_payer_4week=VALUES(olduser_re_payer_4week), olduser_re_money_4week=VALUES(olduser_re_money_4week);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 플랫폼별
	        $sql = "select platform, count(distinct tt1.useridx) as user_cnt, sum(sum_money) as total_money ".
    	   	        "from ( ".
    	   	        "   select t1.useridx, SUM(money) as sum_money ".
    	   	        "   from ( ".
    	   	        "       select useridx, min(writedate) as min_writedate ".
    	   	        "       from t5_product_order_all ".
    	   	        "       where useridx > 20000 and status = 1 ".
    	   	        "       group by useridx ".
    	   	        "       having min_writedate < dateadd(day,(-7*4),'$write_sdate') ".
    	   	        "   ) t1 join t5_product_order_all t2 on t1.useridx = t2.useridx ".
    	   	        "   where status = 1 and writedate >= dateadd(day,(-7*4),'$write_sdate') and writedate < '$write_sdate' ".
    	   	        "   group by t1.useridx ".
    	   	        ") tt1 join t5_user tt2 on tt1.useridx = tt2.useridx ".
    	   	        "where createdate < dateadd(day,(-7*4),'$write_sdate') ".
    	   	        "group by platform";
	        $date_list = $db_redshift->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = $date_list[$i]["platform"];
	            $user_cnt = $date_list[$i]["user_cnt"];
	            $total_money = $date_list[$i]["total_money"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, olduser_re_payer_4week, olduser_re_money_4week) ".
	   	            "VALUES('$join_sdate', $platform, $user_cnt, $total_money) ".
	   	            "ON DUPLICATE KEY UPDATE olduser_re_payer_4week=VALUES(olduser_re_payer_4week), olduser_re_money_4week=VALUES(olduser_re_money_4week);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 신규 마케팅 비용
	        // 전체
	        $sql = "SELECT IFNULL(SUM(spend), 0) AS spend ".
                    "FROM ( ".
                    "   SELECT 0 AS platform, today, ROUND(SUM(spend), 2) AS spend ".
                    "   FROM ( ".
                    "       SELECT today, ROUND(SUM(spend), 2) AS spend ".
                    "       FROM `tbl_ad_stats` ".
                    "       WHERE today < '$join_sdate' AND DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today ".
                    "       GROUP BY today ".
                    "       UNION ALL ".
                    "       SELECT today, ROUND(SUM(spend), 2) AS spend ".
                    "       FROM tbl_agency_spend_daily ".
                    "       WHERE agencyidx < 1000 AND platform = 0 AND today >= '$join_sdate' AND DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today ".
                    "       GROUP BY today ".
                    "   ) t1 ".
                    "   WHERE DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today AND today < '2018-11-01' ".
                    "   GROUP BY today ".
                    "   UNION ALL ".
                    "   SELECT platform, today, SUM(spend) AS spend ".
                    "   FROM tbl_agency_spend_daily ".
                    "   WHERE agencyidx < 1000 AND platform > 0 AND DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today AND today < '$join_sdate' ".
                    "   GROUP BY platform, today ".
                    ") total";
	        $date_list = $db_main2->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = 100;
	            $spend = $date_list[$i]["spend"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, marketing_spend) ".
    	   	            "VALUES('$join_sdate', $platform, $spend) ".
    	   	            "ON DUPLICATE KEY UPDATE marketing_spend=VALUES(marketing_spend);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 플랫폼별
	        $sql = "SELECT platform, IFNULL(SUM(spend), 0) AS spend ".
    	   	        "FROM ( ".
    	   	        "   SELECT 0 AS platform, today, ROUND(SUM(spend), 2) AS spend ".
    	   	        "   FROM ( ".
    	   	        "       SELECT today, ROUND(SUM(spend), 2) AS spend ".
    	   	        "       FROM `tbl_ad_stats` ".
    	   	        "       WHERE today < '$join_sdate' AND DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today ".
    	   	        "       GROUP BY today ".
    	   	        "       UNION ALL ".
    	   	        "       SELECT today, ROUND(SUM(spend), 2) AS spend ".
    	   	        "       FROM tbl_agency_spend_daily ".
    	   	        "       WHERE agencyidx < 1000 AND platform = 0 AND today >= '$join_sdate' AND DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today ".
    	   	        "       GROUP BY today ".
    	   	        "   ) t1 ".
    	   	        "   WHERE DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today AND today < '2018-11-01' ".
    	   	        "   GROUP BY today ".
    	   	        "   UNION ALL ".
    	   	        "   SELECT platform, today, SUM(spend) AS spend ".
    	   	        "   FROM tbl_agency_spend_daily ".
    	   	        "   WHERE agencyidx < 1000 AND platform > 0 AND DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today AND today < '$join_sdate' ".
    	   	        "   GROUP BY platform, today ".
    	   	        ") total ".
	                "GROUP BY platform";
	        $date_list = $db_main2->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = $date_list[$i]["platform"];
	            $spend = $date_list[$i]["spend"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, marketing_spend) ".
    	   	            "VALUES('$join_sdate', $platform, $spend) ".
    	   	            "ON DUPLICATE KEY UPDATE marketing_spend=VALUES(marketing_spend);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 리텐션 마케팅 비용
	        // 전체
	        $sql = "SELECT IFNULL(SUM(spend), 0) AS spend ".
    	   	        "FROM ( ".
    	   	        "   SELECT 0 AS platform, today, ROUND(SUM(spend), 2) AS spend ".
    	   	        "   FROM `tbl_ad_retention_stats` ".
    	   	        "   WHERE DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today AND today < '$join_sdate' ".
    	   	        "   GROUP BY today ".
    	   	        "   UNION ALL ".
    	   	        "   SELECT platform, today, ROUND(SUM(spend), 2) AS spend ".
    	   	        "   FROM `tbl_ad_retention_stats_mobile` ".
    	   	        "   WHERE DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today AND today < '$join_sdate' ".
    	   	        "   GROUP BY platform, today ".
    	   	        "   UNION ALL ".
    	   	        "   SELECT platform, today, ROUND(SUM(spend), 2) AS spend ".
    	   	        "   FROM `tbl_agency_spend_daily` ".
    	   	        "   WHERE DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today AND today < '$join_sdate' AND agencyidx >= 1000 ".
    	   	        "   GROUP BY platform, today ".
    	   	        ") t1 ";
	        $date_list = $db_main2->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = 100;
	            $spend = $date_list[$i]["spend"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, re_marketing_spend) ".
    	   	            "VALUES('$join_sdate', $platform, $spend) ".
    	   	            "ON DUPLICATE KEY UPDATE re_marketing_spend=VALUES(re_marketing_spend);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 플랫폼별
	        $sql = "SELECT platform, IFNULL(SUM(spend), 0) AS spend ".
    	   	        "FROM ( ".
    	   	        "   SELECT 0 AS platform, today, ROUND(SUM(spend), 2) AS spend ".
    	   	        "   FROM `tbl_ad_retention_stats` ".
    	   	        "   WHERE DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today AND today < '$join_sdate' ".
    	   	        "   GROUP BY today ".
    	   	        "   UNION ALL ".
    	   	        "   SELECT platform, today, ROUND(SUM(spend), 2) AS spend ".
    	   	        "   FROM `tbl_ad_retention_stats_mobile` ".
    	   	        "   WHERE DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today AND today < '$join_sdate' ".
    	   	        "   GROUP BY platform, today ".
    	   	        "   UNION ALL ".
    	   	        "   SELECT platform, today, ROUND(SUM(spend), 2) AS spend ".
    	   	        "   FROM `tbl_agency_spend_daily` ".
    	   	        "   WHERE DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today AND today < '$join_sdate' AND agencyidx >= 1000 ".
    	   	        "   GROUP BY platform, today ".
    	   	        ") t1 ".
    	   	        "GROUP BY platform";
	        $date_list = $db_main2->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = $date_list[$i]["platform"];
	            $spend = $date_list[$i]["spend"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, re_marketing_spend) ".
    	   	            "VALUES('$join_sdate', $platform, $spend) ".
    	   	            "ON DUPLICATE KEY UPDATE re_marketing_spend=VALUES(re_marketing_spend);";
	            $db_analysis->execute($sql);
	        }
	        
	        // DAU 일평균
	        // 전체
	        $sql = "SELECT 100 AS platform, ROUND(AVG(todayactivecount)) AS activecount ".
                    "FROM `user_join_log` ".
                    "WHERE DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today AND today < '$join_sdate' ".
                    "UNION ALL ".
                    "SELECT 0 AS platform, ROUND(AVG(todayfacebookactivecount)) AS activecount ".
                    "FROM `user_join_log` ".
                    "WHERE DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today AND today < '$join_sdate' ".
                    "UNION ALL ".
                    "SELECT 1 AS platform, ROUND(AVG(todayiosactivecount)) AS activecount ".
                    "FROM `user_join_log` ".
                    "WHERE DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today AND today < '$join_sdate' ".
                    "UNION ALL ".
                    "SELECT 2 AS platform, ROUND(AVG(todayandroidactivecount)) AS activecount ".
                    "FROM `user_join_log` ".
                    "WHERE DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today AND today < '$join_sdate' ".
                    "UNION ALL ".
                    "SELECT 3 AS platform, ROUND(AVG(todayamazonactivecount)) AS activecount ".
                    "FROM `user_join_log` ".
                    "WHERE DATE_SUB('$join_sdate', INTERVAL 28 DAY) <= today AND today < '$join_sdate'";
	        $date_list = $db_analysis->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = $date_list[$i]["platform"];
	            $activecount = $date_list[$i]["activecount"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, dau_avg) ".
    	   	            "VALUES('$join_sdate', $platform, $activecount) ".
    	   	            "ON DUPLICATE KEY UPDATE dau_avg=VALUES(dau_avg);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 일 평균 결제액
	        // 전체
	        $sql = "select round(avg(total_money), 2) as money_avg ".
	   	        "from ( ".
	   	        "   select date(writedate) as today, SUM(money) as total_money ".
	   	        "   from t5_product_order_all ".
	   	        "   where useridx > 20000 and status = 1 and writedate >= dateadd(day,(-7*4), '$write_sdate') and writedate < '$write_sdate' ".
	   	        "   group by date(writedate) ".
	   	        ") t1";
	        $date_list = $db_redshift->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = 100;
	            $money_avg = $date_list[$i]["money_avg"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, day_money_avg) ".
    	   	            "VALUES('$join_sdate', $platform, $money_avg) ".
    	   	            "ON DUPLICATE KEY UPDATE day_money_avg=VALUES(day_money_avg);";
	            $db_analysis->execute($sql);
	        }
	        
	        // 플랫폼별
	        $sql = "select os_type AS platform, round(avg(total_money), 2) as money_avg ".
    	   	        "from ( ".
    	   	        "   select os_type, date(writedate) as today, SUM(money) as total_money ".
    	   	        "   from t5_product_order_all ".
    	   	        "   where useridx > 20000 and status = 1 and writedate >= dateadd(day,(-7*4), '$write_sdate') and writedate < '$write_sdate' ".
    	   	        "   group by os_type, date(writedate) ".
    	   	        ") t1 ".
    	   	        "group by os_type";
	        $date_list = $db_redshift->gettotallist($sql);
	        
	        for($i=0; $i<sizeof($date_list); $i++)
	        {
	            $platform = $date_list[$i]["platform"];
	            $money_avg = $date_list[$i]["money_avg"];
	            
	            $sql = "INSERT INTO tbl_4week_std_daily_stat(today, platform, day_money_avg) ".
    	   	            "VALUES('$join_sdate', $platform, $money_avg) ".
    	   	            "ON DUPLICATE KEY UPDATE day_money_avg=VALUES(day_money_avg);";
	            $db_analysis->execute($sql);
	        }
	        
	        $sdate = $temp_date;
	    } 
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_main2->end();
	$db_analysis->end();
	$db_redshift->end();
?>