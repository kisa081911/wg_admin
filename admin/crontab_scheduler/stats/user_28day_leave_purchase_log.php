<?	
	include("../../common/common_include.inc.php");
	include("../../common/dbconnect/db_util_redshift.inc.php");
	require '../../common/aws_sdk/vendor/autoload.php';
	
	use Aws\Common\Aws;
	use Aws\S3\S3Client;
	
	ini_set("memory_limit", "-1");	
	
	$result = array();
	exec("ps -ef | grep wget", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/stats/user_28day_leave_purchase_log") !== false)
		{
			$count++;
		}
	}
	
	$issuccess = "1";
	
	if ($count > 1)
		exit();
	
	
	$yesterday = date("Y-m-d", time() - 60 * 60);
	$today = date("Y-m-d", time() + 60 * 60);
	
	$db_main = new CDatabase_Slave_Main();
	$db_other = new CDatabase_Other();
	
	$db_main->execute("SET wait_timeout=7200");
	$db_other->execute("SET wait_timeout=7200");
	
	try 
	{		
		$sql = "SELECT useridx, os_type, ROUND(money, 2) AS money, DATEDIFF(writedate, IF(web > mobile, web, mobile)) AS last_date,  writedate ".
				"FROM	".
				"(	".
  				"	SELECT useridx, os_type, money, MIN(writedate) AS writedate,	".
  				"	IFNULL((SELECT MAX(writedate) FROM tbl_product_order WHERE useridx = t1.useridx AND status = 1 AND writedate < MIN(t1.writedate)), '0000-00-00 00:00:00') AS web,	".
  				"	IFNULL((SELECT MAX(writedate) FROM tbl_product_order_mobile WHERE useridx = t1.useridx AND status = 1 AND writedate < MIN(t1.writedate)), '0000-00-00 00:00:00') AS mobile	".
  				"	FROM	".
  				"(	".
    			"	SELECT useridx, 0 AS os_type, (facebookcredit/10) AS money, writedate FROM tbl_product_order WHERE useridx > 20000 AND STATUS = 1 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'	".
    			"	UNION ALL	".
    			"	SELECT useridx, os_type, money, writedate FROM tbl_product_order_mobile WHERE useridx > 20000 AND STATUS = 1 AND '$yesterday 00:00:00' <= writedate AND writedate < '$today 00:00:00'	".
   				") t1 GROUP BY useridx	".
				") t2 WHERE web != '0000-00-00 00:00:00' OR mobile != '0000-00-00 00:00:00'	".
				"GROUP BY useridx	".
				"HAVING last_date >= 28";
		$user_list = $db_main->gettotallist($sql);
		
		$insert_sql = "";
		$insert_cnt = 0;		
		
		for($i=0; $i<sizeof($user_list); $i++)
		{
			$useridx = $user_list[$i]["useridx"];
			$os_type = $user_list[$i]["os_type"];
			$money = $user_list[$i]["money"];
			$last_date = $user_list[$i]["last_date"];
			$writedate= $user_list[$i]["writedate"];
			
			if($insert_sql == "")
				$insert_sql = "INSERT INTO tbl_user_28day_leave_purchase_log(useridx, platform, money, leavedays, purchasedate) VALUES($useridx, $os_type, '$money', $last_date, '$writedate')";
			else
				$insert_sql .= ",($useridx, $os_type, '$money', $last_date, '$writedate')";
			
			$insert_cnt++;
			
			if($insert_cnt == 50)
			{
				$db_other->execute($insert_sql);
				
				$insert_sql = "";
				$insert_cnt = 0;
			}				
		}
		
		if($insert_sql != "")
			$db_other->execute($insert_sql);		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());		
	}
	
	try 
	{
		$db_redshift = new CDatabase_Redshift();
		
		$sql = "SELECT logidx, useridx, platform, money, leavedays, purchasedate FROM tbl_user_28day_leave_purchase_log WHERE '$yesterday 00:00:00' <= purchasedate AND purchasedate < '$today 00:00:00';";
		$leave_purchase_log_list = $db_other->gettotallist($sql);
		
		$insert_sql = "";
		$insert_cnt = 0;
		
		for($i=0; $i<sizeof($leave_purchase_log_list); $i++)
		{
			$logidx = $leave_purchase_log_list[$i]["logidx"];
			$useridx = $leave_purchase_log_list[$i]["useridx"];
			$platform = $leave_purchase_log_list[$i]["platform"];
			$money = $leave_purchase_log_list[$i]["money"];
			$leavedays = $leave_purchase_log_list[$i]["leavedays"];
			$purchasedate = $leave_purchase_log_list[$i]["purchasedate"];
									
			if($insert_sql == "")
				$insert_sql = "INSERT INTO t5_user_28day_leave_purchase_log(logidx, useridx, platform, money, leavedays, purchasedate) VALUES($logidx, $useridx, $platform, '$money', $leavedays, '$purchasedate')";
			else
				$insert_sql .= ",($logidx, $useridx, $platform, '$money', $leavedays, '$purchasedate')";
									
			$insert_cnt++;
									
			if($insert_cnt == 50)
			{
				$db_redshift->execute($insert_sql);

				$insert_sql = "";
				$insert_cnt = 0;
			}
		}
		
		if($insert_sql != "")
			$db_redshift->execute($insert_sql);
		
		$db_redshift->end();
		
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}	
	
	$db_main->end();
	$db_other->end();	
?>