<?
	include("../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();
	$db_livestats = new CDatabase_Livestats();
	$db_other = new CDatabase_Other();
	
	$db_main->execute("SET wait_timeout=72000");
	$db_analysis->execute("SET wait_timeout=72000");
	$db_livestats->execute("SET wait_timeout=72000");
	$db_other->execute("SET wait_timeout=72000");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$port = "";
	
	$str_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}

	try
	{
		$sql = "INSERT INTO tbl_scheduler_log(today, type, status, writedate) VALUES('$today', 402, 0, NOW());";
		$db_analysis->execute($sql);
		$sql = "SELECT LAST_INSERT_ID();";
		$logidx = $db_analysis->getvalue($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/retention_engage_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1".$port."/crontab_scheduler/retention_engage_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("retention_engage_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}

	// engage 정보 업데이트
	try
	{
		$today = date("Y-m-d", time() - 60 * 60 * 24);
		
		for($i=0; $i<10; $i++)
		{
			$sql = "SELECT useridx FROM tbl_user_retention_log_$i WHERE today='$today' AND after_day_install = 0 AND engage = -1;";
			$user_list = $db_livestats->gettotallist($sql);
			
			for($j=0; $j<sizeof($user_list); $j++)
			{
				$useridx = $user_list[$j]["useridx"];
		
				$sql = "SELECT experience FROM tbl_user WHERE useridx = $useridx;";
		
				$experience = $db_main->getvalue($sql);
		
				$isengage = 0;
		
				if($experience > 0)
					$isengage = 1;
		
				$sql = "UPDATE tbl_user_retention_log_$i SET engage = $isengage WHERE today='$today' AND useridx=$useridx AND after_day_install = 0";
				$db_livestats->execute($sql);
		
				if($j%100 == 0 && $j > 0)
					sleep(1);
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	// 일별 통계
	try 
	{
		$today = date("Y-m-d", time() - 60 * 60 * 24);

		$sql = "SELECT createdate, category, adflag, after_day_install, SUM(total_cnt) AS total_cnt ".
				"FROM ( ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_0 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 3, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_1 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 3, 7, 14, 28, 60, 90) ". 
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_2 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 3, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_3 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 3, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_4 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 3, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_5 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 3, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_6 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 3, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_7 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 3, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_8 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 3, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_9 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install IN (0, 1, 3, 7, 14, 28, 60, 90) ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				") t1 ".
				"GROUP BY createdate, category, adflag, after_day_install ".
				"ORDER BY createdate ASC, after_day_install ASC, category ASC, adflag ASC";

		$retention_stat_list = $db_livestats->gettotallist($sql);
		
		for($i=0; $i<sizeof($retention_stat_list); $i++)
		{
			$reg_date = $retention_stat_list[$i]["createdate"];
			$category = $retention_stat_list[$i]["category"];
			$adflag = $retention_stat_list[$i]["adflag"];
			$after_day_install = $retention_stat_list[$i]["after_day_install"];
			$total_cnt = $retention_stat_list[$i]["total_cnt"];
			
			if($after_day_install == 0)
			{
				$sql = "INSERT INTO tbl_user_retention_daily(reg_date, platform, adflag, reg_count) ".
						"VALUES('$reg_date', $category, '$adflag', $total_cnt) ON DUPLICATE KEY UPDATE reg_count=VALUES(reg_count);";
				$db_other->execute($sql);
			}
			else 
			{
				$sql = "INSERT INTO tbl_user_retention_daily(reg_date, platform, adflag, day_$after_day_install) ".
						"VALUES('$reg_date', $category, '$adflag', $total_cnt) ON DUPLICATE KEY UPDATE day_$after_day_install=VALUES(day_$after_day_install);";
				$db_other->execute($sql);
			}
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$sql = "SELECT createdate, category, adflag, after_day_install, SUM(total_cnt) AS total_cnt ".
				"FROM ( ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_0 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install = 0 AND engage = 1 ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_1 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install = 0 AND engage = 1 ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_2 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install = 0 AND engage = 1 ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_3 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install = 0 AND engage = 1 ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_4 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install = 0 AND engage = 1 ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_5 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install = 0 AND engage = 1 ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_6 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install = 0 AND engage = 1 ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_7 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install = 0 AND engage = 1 ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_8 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install = 0 AND engage = 1 ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				"	UNION ALL ".
				"	SELECT createdate, category, adflag, after_day_install, COUNT(useridx) AS total_cnt ".
				"	FROM tbl_user_retention_log_9 ".
				"	WHERE createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND today = '$today' AND after_day_install = 0 AND engage = 1 ".
				"	GROUP BY createdate, category, adflag, after_day_install ".
				") t1 ".
				"GROUP BY createdate, category, adflag, after_day_install ".
				"ORDER BY createdate ASC, after_day_install ASC, category ASC, adflag ASC";
	
		$retention_day0_stat_list = $db_livestats->gettotallist($sql);
	
		for($i=0; $i<sizeof($retention_day0_stat_list); $i++)
		{
			$reg_date = $retention_day0_stat_list[$i]["createdate"];
			$category = $retention_day0_stat_list[$i]["category"];
			$adflag = $retention_day0_stat_list[$i]["adflag"];
			$after_day_install = $retention_day0_stat_list[$i]["after_day_install"];
			$total_cnt = $retention_day0_stat_list[$i]["total_cnt"];
	
			$sql = "INSERT INTO tbl_user_retention_daily(reg_date, platform, adflag, day_0) ".
					"VALUES('$reg_date', $category, '$adflag', $total_cnt) ON DUPLICATE KEY UPDATE day_0=VALUES(day_0);";
			$db_other->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$today = date("Y-m-d", time() - 60 * 60 * 24);
		
		$sql = "SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS createdate, platform, adflag, COUNT(useridx) AS total_cnt ".
				"FROM tbl_user_ext ".
				"WHERE useridx > $str_useridx AND createdate >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 91 DAY), '%Y-%m-%d') AND createdate <= '$today 23:59:59' ".
				"	AND logincount >= 3 ".
				"GROUP BY DATE_FORMAT(createdate, '%Y-%m-%d'), platform, adflag";
	
		$retention_loyal_stat_list = $db_main->gettotallist($sql);
	
		for($i=0; $i<sizeof($retention_loyal_stat_list); $i++)
		{
			$reg_date = $retention_loyal_stat_list[$i]["createdate"];
			$platform = $retention_loyal_stat_list[$i]["platform"];
			$adflag = $retention_loyal_stat_list[$i]["adflag"];
			$total_cnt = $retention_loyal_stat_list[$i]["total_cnt"];
	
			$sql = "INSERT INTO tbl_user_retention_daily(reg_date, platform, adflag, loyal_count) ".
					"VALUES('$reg_date', $platform, '$adflag', $total_cnt) ON DUPLICATE KEY UPDATE loyal_count=VALUES(loyal_count);";
	
			$db_other->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	try
	{
		$sql = "UPDATE tbl_scheduler_log SET status = $issuccess, writedate = NOW() WHERE logidx = $logidx;";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_analysis->end();
	$db_livestats->end();
	$db_other->end();
?>