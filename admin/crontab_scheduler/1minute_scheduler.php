<?
	include("../common/common_include.inc.php");

	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();
	$db_friend = new CDatabase_Friend();
	
	$db_main->execute("SET wait_timeout=60");
	$db_analysis->execute("SET wait_timeout=60");
	$db_friend->execute("SET wait_timeout=60");
	
	$issuccess = "1";
	
	$today = date("Y-m-d");
	
	$port = "";
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}
	
	try
	{
 		$sql = "INSERT INTO tbl_scheduler_log(today, type, status, writedate) VALUES('$today', 101, 0, NOW());";
 		$db_analysis->execute($sql);
 		$sql = "SELECT LAST_INSERT_ID();";
 		$logidx = $db_analysis->getvalue($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$result = array();
	exec("ps -ef | grep wget | grep -v grep | awk '{print $2 \"|\" $13}'", $result);
	
	$count = 0;
	
	for ($i=0; $i<sizeof($result); $i++)
	{
		if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/1minute_scheduler") !== false)
		{
			$count++;
		}
	}
	
	if ($count > 1)
	{
		$count = 0;
	
		$killcontents = "#!/bin/bash\n";
	
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	
		$fp = fopen("$DOCUMENT_ROOT/crontab_scheduler/kill_scheduler.sh", 'w+');
	
		flock($fp, LOCK_EX);
	
		if (!$fp) {
			echo "Fail";
			exit;
		}
		else
		{
			echo "OK";
		}
	
		flock($fp, LOCK_UN);
	
		$content = "#!/bin/bash\n";
	
		for ($i=0; $i<sizeof($result); $i++)
		{
			if (strpos($result[$i], "http://127.0.0.1/crontab_scheduler/1minute_scheduler") !== false)
			{
				$process_list = explode("|", $result[$i]);
	
				$content .= "kill -9 ".$process_list[0]."\n";
	
				write_log("1minute_scheduler Dead Lock Kill!");
			}
		}
	
		fwrite($fp, $content, strlen($content));
		fclose($fp);
	
		exit();
	}
	
	try
	{
		// online 사용자 sync
		for ($i=0; $i<10; $i++)
		{
			$sql = "INSERT INTO tbl_user_online_sync2(useridx,serveridx,cur_objectidx,slottype,heartbeatdate,userid,nickname,sex,honor_level,crowntype,os_type,is_v2) ". 
					"SELECT useridx,serveridx,cur_objectidx,slottype,heartbeatdate,userid,nickname,sex,honor_level,crowntype,os_type,is_v2 FROM tbl_user_online_$i ON DUPLICATE KEY ". 
					"UPDATE serveridx=VALUES(serveridx),cur_objectidx=VALUES(cur_objectidx),slottype=VALUES(slottype),heartbeatdate=VALUES(heartbeatdate),userid=VALUES(userid),nickname=VALUES(nickname),sex=VALUES(sex),honor_level=VALUES(honor_level),crowntype=VALUES(crowntype),os_type=VALUES(os_type),is_v2=VALUES(is_v2);".
					"DELETE FROM tbl_user_online_sync2 WHERE useridx%10=$i AND NOT EXISTS (SELECT useridx FROM tbl_user_online_$i WHERE useridx=tbl_user_online_sync2.useridx)";

			$db_friend->execute($sql);
		}
		
		$sql = "INSERT INTO tbl_user_online_sync2(useridx, serveridx, cur_objectidx, slottype, heartbeatdate, userid, nickname, sex, honor_level, crowntype) ".
				"SELECT useridx, 1, 0, 0, NOW(), facebookid, nickname, sex, honor_level, crowntype ". 
				"FROM tbl_user_gamedata ".
				"WHERE useridx = ".HELPER_TAKE5_ID." ".
				"ON DUPLICATE KEY UPDATE heartbeatdate = VALUES(heartbeatdate), honor_level = VALUES(honor_level), crowntype = VALUES(crowntype)";
		$db_friend->execute($sql);
		
		$sql = "INSERT INTO tbl_user_online_sync2(useridx, serveridx, cur_objectidx, slottype, heartbeatdate, userid, nickname, sex, honor_level, crowntype) ".
				"VALUES(".HELPER_TAKE5_ID.", 1, 0, 0, NOW(), HELPER_FB_ID, '".HELPER_NAME."', 2)";
	
		$sql = "INSERT INTO tbl_user_online_sync1(useridx) SELECT DISTINCT useridx FROM tbl_user_online_sync2 WHERE NOT EXISTS (SELECT useridx FROM tbl_user_online_sync1 WHERE useridx=tbl_user_online_sync2.useridx);".
				"DELETE FROM tbl_user_online_sync1 WHERE NOT EXISTS (SELECT useridx FROM tbl_user_online_sync2 WHERE useridx=tbl_user_online_sync1.useridx);";
	
		$db_friend->execute($sql);

		$sql = "SELECT * FROM tbl_user_online_sync2";
		$sync_list = $db_friend->gettotallist($sql);
		
		$insertcount = 0;
		$insert_sql = "DELETE FROM tbl_user_online_sync2;";
		
		for ($i=0; $i<sizeof($sync_list); $i++)
		{
			$useridx = $sync_list[$i]["useridx"];
			$serveridx = $sync_list[$i]["serveridx"];
			$cur_objectidx = $sync_list[$i]["cur_objectidx"];
			$slottype = $sync_list[$i]["slottype"];
			$heartbeatdate = $sync_list[$i]["heartbeatdate"];
			$userid = $sync_list[$i]["userid"];
			$nickname = addslashes($sync_list[$i]["nickname"]);
			$sex = $sync_list[$i]["sex"];
			$honor_level = $sync_list[$i]["honor_level"];
			$crowntype = $sync_list[$i]["crowntype"];
			$os_type = $sync_list[$i]["os_type"];
			$is_v2 = $sync_list[$i]["is_v2"];
			
			if($insertcount == 0)
			{
				$insert_sql .= "INSERT INTO tbl_user_online_sync2(useridx, serveridx, cur_objectidx, slottype, heartbeatdate, userid, nickname, sex, honor_level, crowntype, os_type, is_v2) VALUES($useridx, $serveridx, $cur_objectidx, $slottype, '$heartbeatdate', $userid, '$nickname', $sex, $honor_level, $crowntype, $os_type, $is_v2)";
				$insertcount++;
			}
			else if ($insertcount < 100)
			{
				$insert_sql .= ",($useridx, $serveridx, $cur_objectidx, $slottype, '$heartbeatdate', $userid, '$nickname', $sex, $honor_level, $crowntype, $os_type, $is_v2)";
				$insertcount++;
			}
			else
			{
				$insert_sql .= ",($useridx, $serveridx, $cur_objectidx, $slottype, '$heartbeatdate', $userid, '$nickname', $sex, $honor_level, $crowntype, $os_type, $is_v2);";
				$insertcount = 0;
			}
		}
		
		$db_main->execute($insert_sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
		
	try 
	{
		// online 접속자 통계 삽입 - web
		$sql = "SELECT COUNT(*) AS totalcount,serveridx,slottype, CASE WHEN t1.useridx<=10000 THEN 1 ELSE 0 END AS isnpc, ".
				"SUM(CASE WHEN vip_level > 0 AND vip_level <= 3 THEN 1 ELSE 0 END) AS group1, ".
				"SUM(CASE WHEN vip_level > 3 AND vip_level <= 6 THEN 1 ELSE 0 END) AS group2, ".
				"SUM(CASE WHEN vip_level > 6 THEN 1 ELSE 0 END) AS group3, is_v2 ".
				"FROM tbl_user_online_sync2 t1 LEFT JOIN (SELECT useridx, vip_level FROM tbl_user_detail WHERE useridx > 10000) t2 ON t1.useridx = t2.useridx ".
				"WHERE t1.useridx > $std_useridx AND os_type = 0 ".
				"GROUP BY serveridx,slottype,isnpc,is_v2";
		
		$list = $db_friend->gettotallist($sql);
		
		$insert_sql = "";
		
		$logkey = uniqid("");
		
		for ($i=0; $i<sizeof($list); $i++)
		{
			$totalcount = $list[$i]["totalcount"];
			$group1 = $list[$i]["group1"];
			$group2 = $list[$i]["group2"];
			$group3 = $list[$i]["group3"];
			$serveridx = $list[$i]["serveridx"];
			$slottype = $list[$i]["slottype"];
			$isnpc = $list[$i]["isnpc"];
			$is_v2 = $list[$i]["is_v2"];
		
			$insert_sql .= "INSERT INTO user_online_log(logkey,isnpc,serveridx,slottype,totalcount,group1,group2,group3,is_v2,writedate) VALUES('$logkey',$isnpc,$serveridx,$slottype,$totalcount,$group1,$group2,$group3,$is_v2,NOW());";
		}
		
		if ($insert_sql != "")
			$db_analysis->execute($insert_sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try 
	{
		// online 접속자 통계 삽입 - mobile
		$sql = "SELECT COUNT(*) AS totalcount,serveridx,slottype, CASE WHEN t1.useridx<=10000 THEN 1 ELSE 0 END AS isnpc, os_type, ".
				"SUM(CASE WHEN vip_level > 0 AND vip_level <= 3 THEN 1 ELSE 0 END) AS group1, ".
				"SUM(CASE WHEN vip_level > 3 AND vip_level <= 6 THEN 1 ELSE 0 END) AS group2, ".
				"SUM(CASE WHEN vip_level > 6 THEN 1 ELSE 0 END) AS group3 ".
				"FROM tbl_user_online_sync2 t1 LEFT JOIN (SELECT useridx, vip_level FROM tbl_user_detail WHERE useridx > 10000) t2 ON t1.useridx = t2.useridx ".
				"WHERE t1.useridx > $std_useridx AND os_type > 0 ".
				"GROUP BY serveridx,slottype,isnpc,os_type";
		
		$list = $db_friend->gettotallist($sql);
		
		$insert_sql = "";
		
		$logkey = uniqid("");
		
		for ($i=0; $i<sizeof($list); $i++)
		{
			$totalcount = $list[$i]["totalcount"];
			$os_type = $list[$i]["os_type"];
			$group1 = $list[$i]["group1"];
			$group2 = $list[$i]["group2"];
			$group3 = $list[$i]["group3"];
			$serveridx = $list[$i]["serveridx"];
			$slottype = $list[$i]["slottype"];
			$isnpc = $list[$i]["isnpc"];
		
			$insert_sql .= "INSERT INTO user_online_mobile_log(logkey,os_type,isnpc,serveridx,slottype,totalcount,group1,group2,group3,writedate) VALUES('$logkey',$os_type,$isnpc,$serveridx,$slottype,$totalcount,$group1,$group2,$group3,NOW());";
		}
		
		if ($insert_sql != "")
			$db_analysis->execute($insert_sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		// online 접속자 통계 삽입 (각 게임별) - 웹
		$sql = "SELECT COUNT(*) AS totalcount, slottype, cur_objectidx, is_v2 ".
				"FROM tbl_user_online_sync2 ".
				"WHERE useridx>10000 AND os_type=0 AND slottype > 0 ".
				"GROUP BY slottype, cur_objectidx, is_v2 ". 
				"ORDER BY totalcount DESC";
			
		$list = $db_main->gettotallist($sql);
	
		$sql = "";

		for ($i=0; $i<sizeof($list); $i++)
		{
			$gameslottype = $list[$i]["slottype"];
			$objectidx = $list[$i]["cur_objectidx"];
			$totalcount = $list[$i]["totalcount"];
			$is_v2 = $list[$i]["is_v2"];
	
			$sql .= "INSERT INTO user_online_game_log(gameslottype,objectidx,totalcount,is_v2,writedate) VALUES($gameslottype,$objectidx,$totalcount,$is_v2,NOW());";
		}
	
		if ($sql != "")
		{
			$db_analysis->execute($sql);
		}
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		// online 접속자 통계 삽입 - 모바일(IOS)		
		$sql = "SELECT COUNT(*) AS totalcount, slottype, cur_objectidx ".
				"FROM tbl_user_online_sync2 ".
				"WHERE useridx>10000 AND os_type = 1  AND slottype > 0 ".
				"GROUP BY slottype, cur_objectidx ".
				"ORDER BY totalcount DESC";
	
		$list = $db_friend->gettotallist($sql);
	
		$sql = "";
	
		for ($i=0; $i<sizeof($list); $i++)
		{
			$gameslottype = $list[$i]["slottype"];
			$objectidx = $list[$i]["cur_objectidx"];
			$totalcount = $list[$i]["totalcount"];
	
			$sql .= "INSERT INTO user_online_game_mobile_log(os_type, gameslottype,objectidx,totalcount,writedate) VALUES(1, $gameslottype,$objectidx,$totalcount,NOW());";
		}
	
		if ($sql != "")
			$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		// online 접속자 통계 삽입 - 모바일(Android)
		$sql = "SELECT COUNT(*) AS totalcount, slottype, cur_objectidx ".
				"FROM tbl_user_online_sync2 ".
				"WHERE useridx>10000 AND os_type = 2 AND slottype > 0  ".
				"GROUP BY slottype, cur_objectidx ".
				"ORDER BY totalcount DESC";
	
		$list = $db_friend->gettotallist($sql);
	
		$sql = "";
	
		for ($i=0; $i<sizeof($list); $i++)
		{
			$gameslottype = $list[$i]["slottype"];
			$objectidx = $list[$i]["cur_objectidx"];
			$totalcount = $list[$i]["totalcount"];
	
			$sql .= "INSERT INTO user_online_game_mobile_log(os_type, gameslottype,objectidx,totalcount,writedate) VALUES(2, $gameslottype,$objectidx,$totalcount,NOW());";
		}
	
		if ($sql != "")
			$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
		// online 접속자 통계 삽입 - 모바일(Amazon)
		$sql = "SELECT COUNT(*) AS totalcount, slottype, cur_objectidx ".
				"FROM tbl_user_online_sync2 ".
				"WHERE useridx>10000 AND os_type = 3 AND slottype > 0 ".
				"GROUP BY slottype, cur_objectidx ".
				"ORDER BY totalcount DESC";
	
		$list = $db_friend->gettotallist($sql);
	
		$sql = "";
	
		for ($i=0; $i<sizeof($list); $i++)
		{
			$gameslottype = $list[$i]["slottype"];
			$objectidx = $list[$i]["cur_objectidx"];
			$totalcount = $list[$i]["totalcount"];
	
			$sql .= "INSERT INTO user_online_game_mobile_log(os_type, gameslottype,objectidx,totalcount,writedate) VALUES(3, $gameslottype,$objectidx,$totalcount,NOW());";
		}
	
		if ($sql != "")
			$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
		$issuccess = "0";
	}
	
	try
	{
	    for ($i=0; $i<10; $i++)
	    {
	        $sql = "DELETE FROM tbl_user_online_$i WHERE useridx >= 10000 AND heartbeatdate < DATE_SUB(NOW(), INTERVAL 5 MINUTE)";
	        $db_friend->execute($sql);
	    }
	}
	catch(Exception $e)
	{
	    write_log($e->getMessage());
	    $issuccess = "0";
	}
	
	try
	{
		$sql = "UPDATE tbl_scheduler_log SET status = $issuccess, writedate = NOW() WHERE logidx = $logidx;";
		$db_analysis->execute($sql);
	}
	catch(Exception $e)
	{
		write_log($e->getMessage());
	}
	
	$db_main->end();
	$db_analysis->end();
	$db_friend->end();
?>