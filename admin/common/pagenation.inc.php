<div class="pagenation">
<?
	$firstpage = ((int)(($page-1) / 10)) * 10 + 1;
	$endpage = $firstpage + 9;
	$finalpage = (int)(($totalcount-1) / $listcount + 1);
	
	if ($endpage > $finalpage)
		$endpage = $finalpage;

	if ($firstpage > 1)
	{
?>
<A href="<?= $pagename ?>?page=1&<?= $pagefield ?>" class="pprev" onmouseover="className='pprev_over'" onmouseout="className='pprev'"></A>
<A href="<?= $pagename ?>?page=<?= $firstpage-1 ?>&<?= $pagefield ?>" class="prev" onmouseover="className='prev_over'" onmouseout="className='prev'"></A>
<?
	}
	
	for ($i=$firstpage; $i<=$endpage; $i++)
	{
		if ($i == $page)
		{
?>
			<a href="<?= $pagename ?>?page=<?= $i ?>&<?= $pagefield ?>" class="select"><?= $i ?></a>
<?
		}
		else
		{
?>
			<a href="<?= $pagename ?>?page=<?= $i ?>&<?= $pagefield ?>" onmouseover="className='over'" onmouseout="className=''"><?= $i ?></a>
<?
		}
	}

	if ($endpage < $finalpage)
	{
?>
<A href="<?= $pagename ?>?page=<?= $endpage+1 ?>&<?= $pagefield ?>" class="next" onmouseover="className='next_over'" onmouseout="className='next'"></A>
<A href="<?= $pagename ?>?page=<?= $finalpage ?>&<?= $pagefield ?>" class="nnext" onmouseover="className='nnext_over'" onmouseout="className='nnext'"></A>
<?
	}

	if ($finalpage == "0")
	{
?>
		<div style="padding:20px 0">검색결과가 없습니다.</div>
<? 
	}
?>
</div>
