<?
/**
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.
 * It is also available through the world-wide-web at this URL:
 * http://code.google.com/p/android-market-license-verification/source/browse/trunk/LICENSE
 */
 
/**
 * A representation of the data returned by the licensing service
 *
 * @category   AndroidMarket
 * @package    AndroidMarket_Licensing
 */
 
class AndroidMarket_Licensing_ResponseData
{
    const LICENSED                   = 0x0;
    const NOT_LICENSED               = 0x1;
    const LICENSED_OLD_KEY           = 0x2;
    const ERROR_NOT_MARKET_MANAGED   = 0x3;
    const ERROR_SERVER_FAILURE       = 0x4;
    const ERROR_OVER_QUOTA           = 0x5;
 
    const ERROR_CONTACTING_SERVER    = 0x101;
    const ERROR_INVALID_PACKAGE_NAME = 0x102;
    const ERROR_NON_MATCHING_UID     = 0x103;   
 
    /**
     * @var string
     */
 
    protected $_orderId;
    /**
     * @var string
     */
 
    protected $_packageName;
    /**
     * @var string
     */
 
    protected $_productId;
    /**
     * @var integer
     */
 
    protected $_purchaseTime;
    /**
     * @var integer
     */
 
    protected $_purchaseState;
    /**
     * @var integer
     */
 
    protected $_purchaseToken;
    /**
     * @param string $responseData
     */
 
    public function  __construct($responseData)
    {
        if (!is_string($responseData)) 
        {
        	write_log("Invalid response data, expected string");
        }

        $response_data = json_decode($responseData, true);
        
        $this->_orderId = $response_data["orderId"];
        $this->_packageName = $response_data["packageName"];
        $this->_productId = $response_data["productId"];
        $this->_purchaseTime = $response_data["purchaseTime"];
        $this->_purchaseState = $response_data["purchaseState"];
        $this->_purchaseToken = $response_data["purchaseToken"];
    }
 
    /**
     * Get the OrderID
     *
     * @return string
     */
 
    public function getOrderID()
    {
        return $this->_orderId;
    }
    
    /**
     * Get the application package name
     * 
     * @return string
     */
 
    public function getPackageName()
    {
        return $this->_packageName;
    }

    /**
     * Get the Product ID
     *
     * @return string
     */
 
    public function getProductID()
    {
        return $this->_productId;
    }
 
    /**
     * Get the Purchase Time
     *
     * @return int
     */
 
    public function getPurchaseTime()
    {
        return (int)$this->_purchaseTime;
    }
 
    /**
     * Get the Purchase State
     *
     * @return int
     */
 
    public function getPurchaseState()
    {
        return (int)$this->_purchaseState;
    }
 
    /**
     * Get the response timestamp
     *
     * @return integer
     */
 
    public function getPurchaseToken()
    {
        return (int)$this->_purchaseToken;
    }
 
    /**
     * If server response was licensed
     *
     * @return bool
     */
 
    public function isLicensed()
    {
        return (self::LICENSED == $this->_purchaseState
               || self::LICENSED_OLD_KEY == $this->_purchaseState);
    }
}