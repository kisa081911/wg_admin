<?
	if (!defined ("__db_util_redshift_inc_"))
	{
		define("__db_util_redshift_inc_", 1);
	
		header("expires: -2000");
		
		define("_db_redshift_host", "host=wg-dw-instance.cnk70f3ejrhq.us-east-1.redshift.amazonaws.com");
	    define("_db_redshift_name", "dbname=wgdb");
	    define("_db_redshift_port", "port=5439");
	    define("_db_redshift_user", "user=wplat");
	    define("_db_redshift_passwd", "password=!Plat1227");

		// 고정된 mysql database를 다루는 클래스
		class CDatabase_Redshift
		{
			var $conn;
			var $result = Array();
			
			function error($reason)
			{
				$this->end();
				
                write_log($reason);
				exit();
				
				if ($this->conn)
					echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />Database Error - $reason, ".pg_last_error($this->conn));
				else
					echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />Database Error - $reason");
				
				exit();
			}	
			
			function connect()
			{
				$this->conn = pg_connect(_db_redshift_host." "._db_redshift_port." "._db_redshift_name." "._db_redshift_user." "._db_redshift_passwd);

			    if (!$this->conn || pg_last_error($this->conn)) 
				{
					sleep(rand(4, 6));
					
					$this->conn = pg_connect(_db_redshift_host." "._db_redshift_port." "._db_redshift_name." "._db_redshift_user." "._db_redshift_passwd);
					
				    if (!$this->conn || pg_last_error($this->conn)) 
    					$this->error("Can`t connect to the database.");
  				}

			  	pg_exec($this->conn, "SET NAMES 'UTF8'");
				
			    return $this->conn;
			}
		
			function end()
			{
				if ($this->conn)
					pg_close($this->conn);
			}

			function execute($sql, $resultnum = -1)
			{
				$sql_array = array();
				$sql .= ";";
				
				$tempsql = "";
				$isstring = false;
				
				for ($i=0; $i<strlen($sql); $i++)
				{
					$char = substr($sql, $i, 1);
					$char2 = substr($sql, $i, 2);
					
					if ($isstring)
					{
						if ($char2 == "''" || $char2 == "\\'" || $char2 == "\\\\")
						{
							$tempsql .= $char2;
							$i++;
						}
						else if ($char == "'")
						{
							$isstring = false;
							$tempsql .= $char;
						}
						else
							$tempsql .= $char;
					}
					else
					{
						if ($char == ";" || $char == "\n")
						{
							if (trim($tempsql) != "")
							{
								$sql_array[sizeof($sql_array)] = trim($tempsql);
							}

							$tempsql = "";
						}
						else if ($char == "'")
						{
							$isstring = true;
							$tempsql .= $char;
						}
						else
							$tempsql .= $char;
					}
				}
				
				if (sizeof($sql_array) == 1)
					$this->query($sql, $resultnum);
				else
				{
					$this->query("BEGIN", $resultnum);

					for ($i=0; $i<sizeof($sql_array); $i++)
						$this->query($sql_array[$i], $resultnum);
						
					$this->query("COMMIT", $resultnum);
				}
			}
			
			function query($sql, $resultnum = 0)
			{
				if (!$this->conn)
					$this->connect();
					
				for ($i=0; $i<3; $i++)
				{
					$this->result[$resultnum] = pg_exec($this->conn, $sql);
				
					if (!$this->result[$resultnum])
					{
						if ($i != 2)
						{
							write_log("SQL Query Error. STEP-$i : $sql");
							write_log(pg_last_error());
							sleep(1);
						}
						else
						{
							$this->error("SQL Query Error. STEP-$i: $sql");
              				write_log(pg_last_error());
						}
					}
					else 
						break;
				}
					
				return $this->result[$resultnum];
			}

			function getcount($resultnum = 0)
			{
				return pg_numrows($this->result[$resultnum]);
			}			

			function getvalue($sql, $resultnum = -1)
			{
				$this->query($sql, $resultnum);
				$result_array = $this->fetch_array($resultnum);
				return $result_array[0];
			}

			function getarray($sql, $resultnum = -1)
			{
				$this->query($sql, $resultnum);
				$result_array = $this->fetch_array($resultnum);
				return $result_array;
			}
			
			function gettotallist($sql, $resultnum = -1)
			{
				$this->query($sql, $resultnum);
				
				$list = array();
				
				while(($data = $this->fetch_array($resultnum)) != NULL)
				{
					$list[sizeof($list)] = $data;
				}
				
				return $list;
			}
			
			function fetch_array($resultnum = 0)
			{
				$result_row = pg_fetch_array($this->result[$resultnum]);
				return $result_row;
			}
		}
	}
?>