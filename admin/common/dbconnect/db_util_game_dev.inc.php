<?
	if (!defined ("__db_util_game_inc_dev_"))
	{
		define("__db_util_game_inc_dev_", 1);
	
		header("expires: -2000");
		
		define("_db_game_dev_host", "218.38.136.51");
        define("_db_game_dev_name", "takefive_game");
        define("_db_game_dev_user", "w_game");
    	define("_db_game_dev_passwd", "Troqkftjqj@0420");

		// 고정된 mysql database를 다루는 클래스
		class CDatabase_Game_Dev
		{
			var $conn;
			var $result = Array();
			
			function error($reason)
			{
				$this->end();
				
                write_log($reason);
				exit();
				
				if ($this->conn)
					echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />Database Error - $reason, ".mysql_error($this->conn));
				else
					echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />Database Error - $reason");
				
				exit();
			}	
			
			function connect()
			{
			    $this->conn = mysql_connect(_db_game_dev_host, _db_game_dev_user, _db_game_dev_passwd);

			    if (!$this->conn || mysql_error($this->conn)) 
				{
					sleep(rand(4, 6));
					
					$this->conn = mysql_connect(_db_game_dev_host, _db_game_dev_user, _db_game_dev_passwd);
					
				    if (!$this->conn || mysql_error($this->conn)) 
    					$this->error("Can`t connect to the database.");
  				}

  				if (mysql_select_db(_db_game_dev_name, $this->conn) != true) 
					$this->error("Can`t connect to the database.");

				mysql_query("SET NAMES 'UTF8'", $this->conn);
				
			    return $this->conn;
			}
		
			function end()
			{
				if ($this->conn)
					mysql_close($this->conn);
			}

			function execute($sql, $resultnum = -1)
			{
				$sql_array = array();
				$sql .= ";";
				
				$tempsql = "";
				$isstring = false;
				
				for ($i=0; $i<strlen($sql); $i++)
				{
					$char = substr($sql, $i, 1);
					$char2 = substr($sql, $i, 2);
					
					if ($isstring)
					{
						if ($char2 == "''" || $char2 == "\\'" || $char2 == "\\\\")
						{
							$tempsql .= $char2;
							$i++;
						}
						else if ($char == "'")
						{
							$isstring = false;
							$tempsql .= $char;
						}
						else
							$tempsql .= $char;
					}
					else
					{
						if ($char == ";" || $char == "\n")
						{
							if (trim($tempsql) != "")
							{
								$sql_array[sizeof($sql_array)] = trim($tempsql);
							}

							$tempsql = "";
						}
						else if ($char == "'")
						{
							$isstring = true;
							$tempsql .= $char;
						}
						else
							$tempsql .= $char;
					}
				}
				
				if (sizeof($sql_array) == 1)
					$this->query($sql, $resultnum);
				else
				{
					$this->query("BEGIN", $resultnum);

					for ($i=0; $i<sizeof($sql_array); $i++)
						$this->query($sql_array[$i], $resultnum);
						
					$this->query("COMMIT", $resultnum);
				}
			}
			
			function query($sql, $resultnum = 0)
			{
				if (!$this->conn)
					$this->connect();

				for ($i=0; $i<3; $i++)
				{
					$this->result[$resultnum] = mysql_query($sql, $this->conn);
				
					if (!$this->result[$resultnum])
					{
						if ($i != 2)
						{
							write_log("SQL Query Error. STEP-$i : $sql");
							write_log(mysql_error());
							sleep(1);
						}
						else
						{
							$this->error("SQL Query Error. STEP-$i: $sql");
                            write_log(mysql_error());
						}
					}
					else 
						break;
				}
					
				return $this->result[$resultnum];
			}

			function getcount($resultnum = 0)
			{
				return mysql_num_rows($this->result[$resultnum]);
			}			

			function getvalue($sql, $resultnum = -1)
			{
				$this->query($sql, $resultnum);
				$result_array = $this->fetch_array($resultnum);
				return $result_array[0];
			}

			function getarray($sql, $resultnum = -1)
			{
				$this->query($sql, $resultnum);
				$result_array = $this->fetch_array($resultnum);
				return $result_array;
			}
			
			function gettotallist($sql, $resultnum = -1)
			{
				$this->query($sql, $resultnum);
				
				$list = array();
				
				while(($data = $this->fetch_array($resultnum)) != NULL)
				{
					$list[sizeof($list)] = $data;
				}
				
				return $list;
			}
			
			function fetch_array($resultnum = 0)
			{
				$result_row = mysql_fetch_array($this->result[$resultnum]);
				return $result_row;
			}
		}
	}
?>
