<?	
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	$adminid = $_POST["adminid"];
	$password = $_POST["password"];
	$saveid = $_POST["saveid"];
	$current_url = $_POST["currenturl"];
	
	if ($adminid == "" || $password == "")
		error_back("잘못된 접근입니다.");
	
	$password = encrypt($password);

	$db_analysis = new CDatabase_Analysis();
	$db_main2 = new CDatabase_Main2();
	
	$client_accesstoken= $db_main2->getvalue("SELECT accesstoken FROM tbl_client_accesstoken");
	
	$sql = "SELECT adminidx,adminid,adminname,menu,status,dbaccesstype,iscustomerservice FROM tbl_admin WHERE adminid='$adminid' AND password='$password' AND status=1";
	
	$admin = $db_analysis->getarray($sql);	
	
	$adminidx = $admin["adminidx"];
	$adminname = $admin["adminname"];
    $adminid = $admin["adminid"];
	$status = $admin["status"];
	$dbaccesstype = $admin["dbaccesstype"];
    $menu = $admin["menu"];
    $iscustomerservice = $admin["iscustomerservice"];
    
	if ($adminidx == "")
		error_back("아이디 혹은 비밀번호가 일치하지 않습니다.");
			
	$db_analysis->end();
	$db_main2->end();
	
	$_SESSION["adminidx"] = $adminidx;
	$_SESSION["adminid"] = $adminid;
	$_SESSION["adminname"] = $adminname;
    $_SESSION["adminmenu"] = $menu;
    $_SESSION["admincustomerservice"] = $iscustomerservice;
    $_SESSION["dbaccesstype"] = $dbaccesstype;
    $_SESSION["client_accesstoken"] = $client_accesstoken;
	
	// 아이디 저장 (5일)
	if ($saveid == "1")
	{
		setcookie("saved_userid", $adminid, time() + 86400 * 365, "/");
	}
	else
	{
		setcookie("saved_userid", "",  time()-86400 * 365, "/");
	}
	
	if($current_url != NULL)
		just_go($current_url);
	
	if (strpos($menu, "[product]") !== FALSE)
		just_go("/m1_product/order_mng.php?status=1");
	else if (strpos($menu, "[dashboard]") !== FALSE)
		just_go("/m_dashboard/dashboard_all.php");
	else if (strpos($menu, "[user]") !== FALSE)
		just_go("/m2_user/user_mng.php");
	else if (strpos($menu, "[user_static]") !== FALSE)
		just_go("/m3_user_static/user_stats.php");
	else if (strpos($menu, "[pay_static]") !== FALSE)
		just_go("/m4_pay_static/pay_static.php");
	else if (strpos($menu, "[game_stats]") !== FALSE)
		just_go("/m5_game_stats/game_total_stats.php");
	else if (strpos($menu, "[game]") !== FALSE)
		just_go("/m6_game/event.php");
	else if (strpos($menu, "[customer]") !== FALSE)
		just_go("/m7_customer/qa_dashboard.php");
	else if (strpos($menu, "[monitoring]") !== FALSE)
		just_go("/m8_monitoring/total_stats.php");
	else if (strpos($menu, "[management]") !== FALSE)
		just_go("/m10_management/daily_game_indicator.php");
	else if (strpos($menu, "[admin]") !== FALSE)
		just_go("/m11_admin/admin_mng.php");
	else if (strpos($menu, "[marketing]") !== FALSE)
		just_go("/m12_marketing/marketing_dashboard.php");
?>