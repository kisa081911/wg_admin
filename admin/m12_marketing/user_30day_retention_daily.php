<?
    $top_menu = "marketing";
    $sub_menu = "user_30day_retention_daily";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $startdate = $_GET["startdate"];
    $enddate = $_GET["enddate"];
    
    $search_type = ($_GET["search_type"] == "") ? 0 : $_GET["search_type"];
    $search_adflag = ($_GET["search_adflag"] == "") ? 0 : $_GET["search_adflag"];
    $search_view = ($_GET["search_view"] == "") ? 0 : $_GET["search_view"];
    $os_type = ($_GET["os_type"] == "") ? 0 : $_GET["os_type"];
    
    //오늘 날짜 정보
    $today = get_past_date(date("Y-m-d"),1,"d");
    $before_day = "2016-03-10";
    $startdate = ($startdate == "") ? $before_day : $startdate;
    $enddate = ($enddate == "") ? $today : $enddate;
    
    $pagename = "user_30day_retention_daily.php";
    
    $db_other = new CDatabase_Other();
    
    $std_count_sql = "reg_count";
    
    if($search_type == 0)
    	$type_str = " type = 1 ";
    else if($search_type == 1)
    	$type_str = " type = 2 ";
    
    $sql = "SELECT retention_adflag FROM tbl_user_30day_retention_daily2 ".
    		"WHERE '$startdate' <= user_logindate AND user_logindate <= '$enddate' GROUP BY retention_adflag";    
    $adflag_list = $db_other->gettotallist($sql);
    
    if($search_view == 0)
    {
    	$stamp_sql = "DATE_FORMAT(user_logindate, '%Y-%m')";
    	$user_logindate_sql = "DATE_FORMAT(user_logindate, '%Y-%m')";
    	
    	$day_0_sql = "SUM(day_0)";
    	
    	if($search_type == 0)
    		$day_0_sql = "SUM(IF(user_logindate='$enddate',0,day_0))";
    }
    else
    {
    	$stamp_sql = "DATE_FORMAT(user_logindate, '%Y-%m-%d')";
    	$user_logindate_sql = "DATE_FORMAT(user_logindate, '%Y-%m-%d')";
    	$day_0_sql = "SUM(day_0)";
    }
    
    if($search_adflag == "0")
    {
    	if($search_view == 0)
    	{
    		if($os_type == 0)
    		{
	    		//web
	    		$sql = "SELECT DATE_FORMAT(user_logindate, '%Y-%m-%d') AS stamp, DATE_FORMAT(user_logindate, '%Y-%m') AS user_logindate,  DATEDIFF('$today', user_logindate) AS std_day, ".
	    				"	SUM(day_0) AS std_data, $day_0_sql AS day_0 ".
	    				"FROM tbl_user_30day_retention_daily2 ".
	    				"WHERE $type_str AND '$before_day' <= user_logindate AND '$startdate' <= user_logindate AND user_logindate <= '$enddate' ".
	    				"AND category = 0 ".
	    				"GROUP BY stamp ".
		    			"ORDER BY stamp ASC, user_logindate ASC";    	
	    		$day_reg_count_list = array(array("name" => "Web", "data" => $db_other->gettotallist($sql)));
    		}
    		else if($os_type == 2)
    		{
	    		//android
	    		$sql = "SELECT DATE_FORMAT(user_logindate, '%Y-%m-%d') AS stamp, DATE_FORMAT(user_logindate, '%Y-%m') AS user_logindate,  DATEDIFF('$today', user_logindate) AS std_day, ".
	    				"	SUM(day_0) AS std_data, $day_0_sql AS day_0 ".
	    				"FROM tbl_user_30day_retention_daily2 ".
	    				"WHERE $type_str AND '$before_day' <= user_logindate AND '$startdate' <= user_logindate AND user_logindate <= '$enddate' ".
	    				"AND category = 2 ".
	    				"GROUP BY stamp ".
	    				"ORDER BY stamp ASC, user_logindate ASC";
	    		$day_reg_count_list = array(array("name" => "Android", "data" => $db_other->gettotallist($sql)));
    		}
    		else if($os_type == 1)
    		{
	    		//ios
	    		$sql = "SELECT DATE_FORMAT(user_logindate, '%Y-%m-%d') AS stamp, DATE_FORMAT(user_logindate, '%Y-%m') AS user_logindate,  DATEDIFF('$today', user_logindate) AS std_day, ".
	    				"	SUM(day_0) AS std_data, $day_0_sql AS day_0 ".
	    				"FROM tbl_user_30day_retention_daily2 ".
	    				"WHERE $type_str AND '$before_day' <= user_logindate AND '$startdate' <= user_logindate AND user_logindate <= '$enddate' ".
	    				"AND category = 1 ".
	    				"GROUP BY stamp ".
	    				"ORDER BY stamp ASC, user_logindate ASC";
	    		$day_reg_count_list = array(array("name" => "IOS", "data" => $db_other->gettotallist($sql)));
    		}
    		else if($os_type == 3)
    		{
	    		//amazon
	    		$sql = "SELECT DATE_FORMAT(user_logindate, '%Y-%m-%d') AS stamp, DATE_FORMAT(user_logindate, '%Y-%m') AS user_logindate,  DATEDIFF('$today', user_logindate) AS std_day, ".
	    				"	SUM(day_0) AS std_data, $day_0_sql AS day_0 ".
	    				"FROM tbl_user_30day_retention_daily2 ".
	    				"WHERE $type_str AND '$before_day' <= user_logindate AND '$startdate' <= user_logindate AND user_logindate <= '$enddate' ".
	    				"AND category = 3 ".
	    				"GROUP BY stamp ".
	    				"ORDER BY stamp ASC, user_logindate ASC";
	    		$day_reg_count_list = array(array("name" => "Amazon", "data" => $db_other->gettotallist($sql)));
    		}
    	}
    	
    	if($os_type == 0)
    	{
    		$platform_name = "WEB";
	    	//Web
	    	$sql = "SELECT $stamp_sql AS stamp, $user_logindate_sql AS user_logindate, ".
	      			"	DATEDIFF('$today', user_logindate) AS std_day, SUM(pay_count) AS pay_count, ".
	    			"	SUM(day_0) AS std_data, $day_0_sql AS day_0, SUM(day_1) AS day_1, SUM(day_2) AS day_2,". 
	    			"	SUM(day_3) AS day_3, SUM(day_4) AS day_4, SUM(day_5) AS day_5, SUM(day_6) AS day_6,	". 
	    			"	SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
	    			"FROM tbl_user_30day_retention_daily2 ".
	    			"WHERE $type_str AND '$before_day' <= user_logindate AND '$startdate' <= user_logindate AND user_logindate <= '$enddate' ".
	    			"AND  category = 0 ".
	    			"GROUP BY stamp ".
					"ORDER BY stamp ASC, user_logindate ASC";
	    	$retention_list = array(array("name" => "Web", "data" => $db_other->gettotallist($sql)));
    	}
    	else if($os_type == 2)
    	{
    		$platform_name = "Android";
	    	//Android
	    	$sql = "SELECT $stamp_sql AS stamp, $user_logindate_sql AS user_logindate, ".
	    			"	DATEDIFF('$today', user_logindate) AS std_day, SUM(pay_count) AS pay_count, ".
	    			"	SUM(day_0) AS std_data, $day_0_sql AS day_0, SUM(day_1) AS day_1, SUM(day_2) AS day_2,".
	    			"	SUM(day_3) AS day_3, SUM(day_4) AS day_4, SUM(day_5) AS day_5, SUM(day_6) AS day_6,	".
	    			"	SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
	    			"FROM tbl_user_30day_retention_daily2 ".
	    			"WHERE $type_str AND '$before_day' <= user_logindate AND '$startdate' <= user_logindate AND user_logindate <= '$enddate' ".
	    			"AND  category = 2 ".
	    			"GROUP BY stamp ".
					"ORDER BY stamp ASC, user_logindate ASC";
	    	$retention_list = array(array("name" => "Android", "data" => $db_other->gettotallist($sql)));
    	}
    	else if($os_type == 1)
    	{
    		$platform_name = "IOS";
	    	//IOS
	    	$sql = "SELECT $stamp_sql AS stamp, $user_logindate_sql AS user_logindate, ".
	    			"	DATEDIFF('$today', user_logindate) AS std_day, SUM(pay_count) AS pay_count, ".
	    			"	SUM(day_0) AS std_data, $day_0_sql AS day_0, SUM(day_1) AS day_1, SUM(day_2) AS day_2,".
	    			"	SUM(day_3) AS day_3, SUM(day_4) AS day_4, SUM(day_5) AS day_5, SUM(day_6) AS day_6,	".
	    			"	SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
	    			"FROM tbl_user_30day_retention_daily2 ".
	    			"WHERE $type_str AND '$before_day' <= user_logindate AND '$startdate' <= user_logindate AND user_logindate <= '$enddate' ".
	    			"AND  category = 1 ".
	    			"GROUP BY stamp ".
					"ORDER BY stamp ASC, user_logindate ASC";
	    	$retention_list = array(array("name" => "IOS", "data" => $db_other->gettotallist($sql)));
    	}
    	else if($os_type == 3)
    	{
    		$platform_name = "Amazon";
	    	//Amazon
	    	$sql = "SELECT $stamp_sql AS stamp, $user_logindate_sql AS user_logindate, ".
	    			"	DATEDIFF('$today', user_logindate) AS std_day, SUM(pay_count) AS pay_count, ".
	    			"	SUM(day_0) AS std_data, $day_0_sql AS day_0, SUM(day_1) AS day_1, SUM(day_2) AS day_2,".
	    			"	SUM(day_3) AS day_3, SUM(day_4) AS day_4, SUM(day_5) AS day_5, SUM(day_6) AS day_6,	".
	    			"	SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
	    			"FROM tbl_user_30day_retention_daily2 ".
	    			"WHERE $type_str AND '$before_day' <= user_logindate AND '$startdate' <= user_logindate AND user_logindate <= '$enddate' ".
	    			"AND  category = 3 ".
	    			"GROUP BY stamp ".
					"ORDER BY stamp ASC, user_logindate ASC";
	    	$retention_list = array(array("name" => "Amazon", "data" => $db_other->gettotallist($sql)));
    	}
    }
    else
    {
    	if($search_view == 0)
    	{
    		$sql = "SELECT DATE_FORMAT(user_logindate, '%Y-%m-%d') AS stamp, DATE_FORMAT(user_logindate, '%Y-%m') AS user_logindate,  DATEDIFF('$today', user_logindate) AS std_day, ".
    				"	SUM(day_0) AS std_data, $day_0_sql AS day_0 ".
    				" FROM tbl_user_30day_retention_daily2 ".
    				" WHERE $type_str AND '$before_day' <= user_logindate AND '$startdate' <= user_logindate AND user_logindate <= '$enddate' ".
    				" AND retention_adflag = '".($search_adflag == "viral" ? "" : $search_adflag)."' ".
    				" AND category = $os_type ".
    				" GROUP BY stamp ".
    				" ORDER BY stamp ASC, user_logindate ASC";    		 
    		$day_reg_count_list = $db_other->gettotallist($sql);
    	}
    	
    	$sql = "SELECT $stamp_sql AS stamp, $user_logindate_sql AS user_logindate, ".
    			"	DATEDIFF('$today', user_logindate) AS std_day, ".
    			"	SUM(day_0) AS std_data, $day_0_sql AS day_0, SUM(day_1) AS day_1, SUM(day_2) AS day_2,".
    			"	SUM(day_3) AS day_3, SUM(day_4) AS day_4, SUM(day_5) AS day_5, SUM(day_6) AS day_6, ".
    			"	SUM(day_7) AS day_7, SUM(day_14) AS day_14, SUM(day_28) AS day_28, SUM(day_60) AS day_60, SUM(day_90) AS day_90 ".
    			" FROM tbl_user_30day_retention_daily2 ".
    			" WHERE $type_str AND '$before_day' <= user_logindate AND '$startdate' <= user_logindate AND user_logindate <= '$enddate' ".
    			" AND retention_adflag = '".($search_adflag == "viral" ? "" : $search_adflag)."' ".
    			" AND category = $os_type ".
    			" GROUP BY stamp ".
				" ORDER BY stamp ASC, user_logindate ASC";
    	$retention_list = array(array("name" => "$search_adflag", "data" => $db_other->gettotallist($sql)));    	
    }   
    
    $db_other->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#startdate").datepicker({ });
	});
	
	$(function() {
		$("#enddate").datepicker({ });
	});
	
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.startdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    } 
	
	    if (search_form.enddate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    } 
	
	    search_form.submit();
	}

	function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; <?=$platform_name?> 30Day 이탈자 Retention</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
			
				<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />
        		<div style="margin-left:500px">
	        		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
						<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
						<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
						<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
						<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
					</span>
				</div>
				</br>
				<span class="search_lbl">보기&nbsp;&nbsp;&nbsp;</span>
				<select name="search_type" id="search_type">
					<option value="0" <?= ($search_type == "0") ? "selected" : "" ?>>복귀자</option>
					<option value="1" <?= ($search_type == "1") ? "selected" : "" ?>>결제액</option>
				</select>&nbsp;&nbsp;
				<span class="search_lbl ml20">구분&nbsp;</span>
				<select name="search_adflag" id="search_adflag">
					<option value="0" <?= ($search_adflag == "0") ? "selected" : "" ?>>전체</option>
<?
				for($i=0; $i<sizeof($adflag_list); $i++)
				{
					$adflag = $adflag_list[$i]["retention_adflag"];
					
					$adflag = ($adflag == "") ? "viral" : $adflag;
?>
					<option value="<?= $adflag ?>" <?= ("$search_adflag" == "$adflag") ? "selected" : "" ?>><?= $adflag ?></option>
<?
				}
?>
				</select>
				<span class="search_lbl ml20">기간&nbsp;&nbsp;&nbsp;</span>
				<select name="search_view" id="search_view">
					<option value="0" <?= ($search_view == "0") ? "selected" : "" ?>>월</option>
					<option value="1" <?= ($search_view == "1") ? "selected" : "" ?>>일</option>
				</select>
				<span class="search_lbl ml20">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" /> ~
				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
<?
	for($i=0; $i<sizeof($retention_list); $i++)
	{
		if($i > 0)
		{
?>
		<br/><br/>
<?
		}
?>
		<div class="search_result"><?= $retention_list[$i]["name"]; ?></div>
		<div class="search_result">
			<span><?= $startdate ?> ~ <?= $enddate ?></span> 통계입니다
		</div>
	
		<table class="tbl_list_basic1">
		<colgroup>
			<col width="80">
			<col width="70">
			<col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
			<col width="70">
            <col width="70">
<?
	if($search_type == 1)
	{
?>
			 <col width="70">
<?
	}
?>
		</colgroup>
        <thead>
         	<th>복귀일</th>
<?
	if($search_type == 0)
	{
?>
			<th>복귀자수</th>
<?
	}
	else
	{
?>
			<th>결제수</th>
			<th>D+0</th>
<?
	}
?>
			<th>D+1</th>
			<th>D+2</th>
			<th>D+3</th>
			<th>D+4</th>
			<th>D+5</th>
			<th>D+6</th>
			<th>D+7</th>
			<th>D+14</th>
			<th>D+28</th>
			<th>D+60</th>
			<th>D+90</th>
        </thead>
        <tbody>
<?
		$retention_detail = $retention_list[$i]["data"];
		$day_reg_count_detail = $day_reg_count_list[$i]["data"];
		
		$sum_pay_count = 0;
		
		$sum_day_0 = 0;
		$sum_day_0_reg_count = 0;
		$day_0_row_count = 0;
		$sum_day_1 = 0;
		$sum_day_1_reg_count = 0;
		$day_1_row_count = 0;
		$sum_day_2 = 0;
		$sum_day_2_reg_count = 0;
		$day_2_row_count = 0;
		$sum_day_3 = 0;
		$sum_day_3_reg_count = 0;
		$day_3_row_count = 0;
		
		$sum_day_4 = 0;
		$sum_day_4_reg_count = 0;
		$day_4_row_count = 0;
		$sum_day_5 = 0;
		$sum_day_5_reg_count = 0;
		$day_5_row_count = 0;
		$sum_day_6 = 0;
		$sum_day_6_reg_count = 0;
		$day_6_row_count = 0;
		
		$sum_day_7 = 0;
		$sum_day_7_reg_count = 0;
		$day_7_row_count = 0;
		$sum_day_14 = 0;
		$sum_day_14_reg_count = 0;
		$day_14_row_count = 0;
		$sum_day_28 = 0;
		$sum_day_28_reg_count = 0;
		$day_28_row_count = 0;
		$sum_day_60 = 0;
		$sum_day_60_reg_count = 0;
		$day_60_row_count = 0;
		$sum_day_90 = 0;
		$sum_day_90_reg_count = 0;
		$day_90_row_count = 0;
		
		$day_90_reg_count = array();
		$day_60_reg_count = array();
		$day_28_reg_count = array();
		$day_14_reg_count = array();
		$day_7_reg_count = array();
		$day_6_reg_count = array();
		$day_5_reg_count = array();
		$day_4_reg_count = array();
		$day_3_reg_count = array();
		$day_2_reg_count = array();
		$day_1_reg_count = array();
		
		$sum_row_day_90_reg_count = 0;
		$sum_row_day_60_reg_count = 0;
		$sum_row_day_28_reg_count = 0;
		$sum_row_day_14_reg_count = 0;
		$sum_row_day_7_reg_count = 0;
		$sum_row_day_6_reg_count = 0;
		$sum_row_day_5_reg_count = 0;
		$sum_row_day_4_reg_count = 0;
		$sum_row_day_3_reg_count = 0;
		$sum_row_day_2_reg_count = 0;
		$sum_row_day_1_reg_count = 0;		
		
		if($search_view == 0)
		{
			for($k = 0; $k < sizeof($day_reg_count_detail); $k++)
			{
				$user_logindate = $day_reg_count_detail[$k]["user_logindate"];
				$std_day = $day_reg_count_detail[$k]["std_day"];
				$std_data = $day_reg_count_detail[$k]["std_data"];
				$join_count = $day_reg_count_detail[$k]["day_0"];				
		
				if($std_day >= 90)
					$day_90_reg_count[$user_logindate] += $join_count;
		
				if($std_day >= 60)
					$day_60_reg_count[$user_logindate] += $join_count;
		
				if($std_day >= 28)
					$day_28_reg_count[$user_logindate] += $join_count;
		
				if($std_day >= 14)
					$day_14_reg_count[$user_logindate] += $join_count;
		
				if($std_day >= 7)
					$day_7_reg_count[$user_logindate] += $join_count;

				if($std_day >= 6)
					$day_6_reg_count[$user_logindate] += $join_count;

				if($std_day >= 5)
					$day_5_reg_count[$user_logindate] += $join_count;

				if($std_day >= 4)
					$day_4_reg_count[$user_logindate] += $join_count;

				if($std_day >= 3)
					$day_3_reg_count[$user_logindate] += $join_count;

				if($std_day >= 2)
					$day_2_reg_count[$user_logindate] += $join_count;

				if($std_day >= 1)
					$day_1_reg_count[$user_logindate] += $join_count;


				if($std_day >= 90)
					$sum_row_day_90_reg_count += $join_count;

				if($std_day >= 60)
					$sum_row_day_60_reg_count += $join_count;

				if($std_day >= 28)
					$sum_row_day_28_reg_count += $join_count;

				if($std_day >= 14)
					$sum_row_day_14_reg_count += $join_count;

				if($std_day >= 7)
					$sum_row_day_7_reg_count += $join_count;

				if($std_day >= 6)
					$sum_row_day_6_reg_count += $join_count;

				if($std_day >= 5)
					$sum_row_day_5_reg_count += $join_count;

				if($std_day >= 4)
					$sum_row_day_4_reg_count += $join_count;

				if($std_day >= 3)
					$sum_row_day_3_reg_count += $join_count;

				if($std_day >= 2)
					$sum_row_day_2_reg_count += $join_count;

				if($std_day >= 1)
					$sum_row_day_1_reg_count += $join_count;
			}
		}

		for($j=0; $j<sizeof($retention_detail); $j++)
		{
			$user_logindate = $retention_detail[$j]["user_logindate"];
			$std_day = $retention_detail[$j]["std_day"];
			$std_data = $retention_detail[$j]["std_data"];
			$join_count = $retention_detail[$j]["day_0"]; 
			$pay_count = $retention_detail[$j]["pay_count"];
			
			$day_0 = $retention_detail[$j]["day_0"];
			$day_1 = $retention_detail[$j]["day_1"];
			$day_2 = $retention_detail[$j]["day_2"];
			$day_3 = $retention_detail[$j]["day_3"];
			$day_4 = $retention_detail[$j]["day_4"];
			$day_5 = $retention_detail[$j]["day_5"];
			$day_6 = $retention_detail[$j]["day_6"];
			$day_7 = $retention_detail[$j]["day_7"];
			$day_14 = $retention_detail[$j]["day_14"];
			$day_28 = $retention_detail[$j]["day_28"];
			$day_60 = $retention_detail[$j]["day_60"];
			$day_90 = $retention_detail[$j]["day_90"];
			
			$sum_pay_count += $pay_count;
			$sum_day_0 += $day_0;
			$sum_day_1 += $day_1;
			$sum_day_2 += $day_2;
			$sum_day_3 += $day_3;
			$sum_day_4 += $day_4;
			$sum_day_5 += $day_5;
			$sum_day_6 += $day_6;
			$sum_day_7 += $day_7;
			$sum_day_14 += $day_14;
			$sum_day_28 += $day_28;
			$sum_day_60 += $day_60;
			$sum_day_90 += $day_90;
			
			if($day_0 > 0)
				$day_0_row_count++;
			
			if($day_1 > 0)
				$day_1_row_count++;
			
			if($day_2 > 0)
				$day_2_row_count++;
			
			if($day_3 > 0)
				$day_3_row_count++;
			
			if($day_4 > 0)
				$day_4_row_count++;
					
			if($day_5 > 0)
				$day_5_row_count++;
						
			if($day_6 > 0)
				$day_6_row_count++;
			
			if($day_7 > 0)
				$day_7_row_count++;
			
			if($day_14 > 0)
				$day_14_row_count++;
			
			if($day_28 > 0)
				$day_28_row_count++;
			
			if($day_60 > 0)
				$day_60_row_count++;
			
			if($day_90 > 0)
				$day_90_row_count++;
			
			if($search_view == 0)
			{
				if($std_day >= 90)
					$sum_day_90_reg_count = $sum_row_day_90_reg_count;
					
				if($std_day >= 60)
					$sum_day_60_reg_count = $sum_row_day_60_reg_count;
					
				if($std_day >= 28)
					$sum_day_28_reg_count = $sum_row_day_28_reg_count;
					
				if($std_day >= 14)
					$sum_day_14_reg_count = $sum_row_day_14_reg_count;
					
				if($std_day >= 7)
					$sum_day_7_reg_count = $sum_row_day_7_reg_count;
					
				if($std_day >= 6)
					$sum_day_6_reg_count = $sum_row_day_6_reg_count;
					
				if($std_day >= 5)
					$sum_day_5_reg_count = $sum_row_day_5_reg_count;
					
				if($std_day >= 4)
					$sum_day_4_reg_count = $sum_row_day_4_reg_count;
					
				if($std_day >= 3)
					$sum_day_3_reg_count = $sum_row_day_3_reg_count;
					
				if($std_day >= 2)
					$sum_day_2_reg_count = $sum_row_day_2_reg_count;
					
				if($std_day >= 1)
					$sum_day_1_reg_count = $sum_row_day_1_reg_count;
			
			
				$sum_day_0_reg_count += $std_data;
			}
			else
			{			
				if($std_day >= 90)
					$sum_day_90_reg_count += $join_count;
				
				if($std_day >= 60)
					$sum_day_60_reg_count += $join_count;
				
				if($std_day >= 28)
					$sum_day_28_reg_count += $join_count;
				
				if($std_day >= 14)
					$sum_day_14_reg_count += $join_count;
				
				if($std_day >= 7)
					$sum_day_7_reg_count += $join_count;
				
				if($std_day >= 6)
					$sum_day_6_reg_count += $join_count;
				
				if($std_day >= 5)
					$sum_day_5_reg_count += $join_count;
				
				if($std_day >= 4)
					$sum_day_4_reg_count += $join_count;
				
				if($std_day >= 3)
					$sum_day_3_reg_count += $join_count;
				
				if($std_day >= 2)
					$sum_day_2_reg_count += $join_count;
				
				if($std_day >= 1)
					$sum_day_1_reg_count += $join_count;
				
				$sum_day_0_reg_count += $std_data;
			}
			
			$add_per = "";
			$add_point = 0;
			
			if($search_data == 0)
			{
				$add_per = "%";
				$add_point = 1;
				
				if($search_type == 0)
				{
					if($search_view == 0)
					{
						$day_0 = $join_count;
						$day_1 = ($day_1_reg_count[$user_logindate] == 0) ? 0 : round($day_1/$day_1_reg_count[$user_logindate]*100, $add_point);
						$day_2 = ($day_2_reg_count[$user_logindate] == 0) ? 0 : round($day_2/$day_2_reg_count[$user_logindate]*100, $add_point);
						$day_3 = ($day_3_reg_count[$user_logindate] == 0) ? 0 : round($day_3/$day_3_reg_count[$user_logindate]*100, $add_point);
						$day_4 = ($day_4_reg_count[$user_logindate] == 0) ? 0 : round($day_4/$day_4_reg_count[$user_logindate]*100, $add_point);
						$day_5 = ($day_5_reg_count[$user_logindate] == 0) ? 0 : round($day_5/$day_5_reg_count[$user_logindate]*100, $add_point);
						$day_6 = ($day_6_reg_count[$user_logindate] == 0) ? 0 : round($day_6/$day_6_reg_count[$user_logindate]*100, $add_point);
						$day_7 = ($day_7_reg_count[$user_logindate] == 0) ? 0 : round($day_7/$day_7_reg_count[$user_logindate]*100, $add_point);
						$day_14 = ($day_14_reg_count[$user_logindate] == 0) ? 0 : round($day_14/$day_14_reg_count[$user_logindate]*100, $add_point);
						$day_28 = ($day_28_reg_count[$user_logindate] == 0) ? 0 : round($day_28/$day_28_reg_count[$user_logindate]*100, $add_point);
						$day_60 = ($day_60_reg_count[$user_logindate] == 0) ? 0 : round($day_60/$day_60_reg_count[$user_logindate]*100, $add_point);
						$day_90 = ($day_90_reg_count[$user_logindate] == 0) ? 0 : round($day_90/$day_90_reg_count[$user_logindate]*100, $add_point);
					}
					else
					{
						$day_0 = $join_count;
						$day_1 = ($join_count == 0) ? 0 : round($day_1/$join_count*100, $add_point);
						$day_2 = ($join_count == 0) ? 0 : round($day_2/$join_count*100, $add_point);
						$day_3 = ($join_count == 0) ? 0 : round($day_3/$join_count*100, $add_point);
						$day_4 = ($join_count == 0) ? 0 : round($day_4/$join_count*100, $add_point);
						$day_5 = ($join_count == 0) ? 0 : round($day_5/$join_count*100, $add_point);
						$day_6 = ($join_count == 0) ? 0 : round($day_6/$join_count*100, $add_point);
						$day_7 = ($join_count == 0) ? 0 : round($day_7/$join_count*100, $add_point);
						$day_14 = ($join_count == 0) ? 0 : round($day_14/$join_count*100, $add_point);
						$day_28 = ($join_count == 0) ? 0 : round($day_28/$join_count*100, $add_point);
						$day_60 = ($join_count == 0) ? 0 : round($day_60/$join_count*100, $add_point);
						$day_90 = ($join_count == 0) ? 0 : round($day_90/$join_count*100, $add_point);
					}
				}
			}
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
	            <td class="tdc"><?= $user_logindate ?></td>
<?
			if($search_type == "1")
			{
?>
				<td class="tdc"><?=	number_format($pay_count) ?></td>
<?
			}
?>
	            <td class="tdc"><?=	($search_type == 0) ? number_format($std_data): "$".number_format($day_0, $add_point) ?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format($day_1, $add_point)." $add_per" : "$".number_format($day_1, $add_point) ?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format($day_2, $add_point)." $add_per" : "$".number_format($day_2, $add_point) ?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format($day_3, $add_point)." $add_per" : "$".number_format($day_3, $add_point) ?></td>
	            <td class="tdc"><?=	($search_type == 0) ? number_format($day_4, $add_point)." $add_per" : "$".number_format($day_4, $add_point) ?></td>
	            <td class="tdc"><?=	($search_type == 0) ? number_format($day_5, $add_point)." $add_per" : "$".number_format($day_5, $add_point) ?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format($day_6, $add_point)." $add_per" : "$".number_format($day_6, $add_point) ?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format($day_7, $add_point)." $add_per" : "$".number_format($day_7, $add_point) ?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format($day_14, $add_point)." $add_per" : "$".number_format($day_14, $add_point) ?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format($day_28, $add_point)." $add_per" : "$".number_format($day_28, $add_point) ?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format($day_60, $add_point)." $add_per" : "$".number_format($day_60, $add_point) ?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format($day_90, $add_point)." $add_per" : "$".number_format($day_90, $add_point) ?></td>
	        </tr>
<?
	}
	
    if(sizeof($retention_detail) == 0)
    {
?>
   			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   				<td class="tdc" colspan="13">검색 결과가 없습니다.</td>
   			</tr>
<?
   	}
   	else
   	{
   		if($search_data == 0)
   		{
   			if($search_type == 0)
   			{
	   			$sum_day_0 = $sum_day_0_reg_count;
	   			$sum_day_1 = ($sum_day_1_reg_count == 0) ? 0 : round($sum_day_1/$sum_day_1_reg_count*100, $add_point);
	   			$sum_day_2 = ($sum_day_2_reg_count == 0) ? 0 : round($sum_day_2/$sum_day_2_reg_count*100, $add_point);
	   			$sum_day_3 = ($sum_day_3_reg_count == 0) ? 0 : round($sum_day_3/$sum_day_3_reg_count*100, $add_point);
	   			$sum_day_4 = ($sum_day_4_reg_count == 0) ? 0 : round($sum_day_4/$sum_day_4_reg_count*100, $add_point);
	   			$sum_day_5 = ($sum_day_5_reg_count == 0) ? 0 : round($sum_day_5/$sum_day_5_reg_count*100, $add_point);
	   			$sum_day_6 = ($sum_day_6_reg_count == 0) ? 0 : round($sum_day_6/$sum_day_6_reg_count*100, $add_point);
	   			$sum_day_7 = ($sum_day_7_reg_count == 0) ? 0 : round($sum_day_7/$sum_day_7_reg_count*100, $add_point);
	   			$sum_day_14 = ($sum_day_14_reg_count == 0) ? 0 : round($sum_day_14/$sum_day_14_reg_count*100, $add_point);
	   			$sum_day_28 = ($sum_day_28_reg_count == 0) ? 0 : round($sum_day_28/$sum_day_28_reg_count*100, $add_point);
	   			$sum_day_60 = ($sum_day_60_reg_count == 0) ? 0 : round($sum_day_60/$sum_day_60_reg_count*100, $add_point);
	   			$sum_day_90 = ($sum_day_90_reg_count == 0) ? 0 : round($sum_day_90/$sum_day_90_reg_count*100, $add_point);
   			}
   		}
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point_title">평균</td>	            
<?
			if($search_type == "1")
			{
?>
				<td class="tdc point"><?= number_format($sum_pay_count) ?></td>
<?
			}
?>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format($sum_day_0): "$".number_format($sum_day_0, $add_point) ?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format($sum_day_1, $add_point)." $add_per" : "$".number_format($sum_day_1, $add_point) ?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format($sum_day_2, $add_point)." $add_per" : "$".number_format($sum_day_2, $add_point) ?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format($sum_day_3, $add_point)." $add_per" : "$".number_format($sum_day_3, $add_point) ?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format($sum_day_4, $add_point)." $add_per" : "$".number_format($sum_day_4, $add_point) ?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format($sum_day_5, $add_point)." $add_per" : "$".number_format($sum_day_5, $add_point) ?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format($sum_day_6, $add_point)." $add_per" : "$".number_format($sum_day_6, $add_point) ?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format($sum_day_7, $add_point)." $add_per" : "$".number_format($sum_day_7, $add_point) ?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format($sum_day_14, $add_point)." $add_per" : "$".number_format($sum_day_14, $add_point) ?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format($sum_day_28, $add_point)." $add_per" : "$".number_format($sum_day_28, $add_point) ?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format($sum_day_60, $add_point)." $add_per" : "$".number_format($sum_day_60, $add_point) ?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format($sum_day_90, $add_point)." $add_per" : "$".number_format($sum_day_90, $add_point) ?></td>
	        </tr>
<?
   	}
?>
        </tbody>
	</table>
<?
	}
?>
</div>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>