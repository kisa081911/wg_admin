<?
	include("../common/common_include.inc.php");		
 	ini_set("memory_limit", "-1");
 	check_login();
 	
 	$search_start_createdate = ($_GET["start_createdate"] == "") ? date("Y-m-d", strtotime("-14 day")) : $_GET["start_createdate"];
    $search_end_createdate = ($_GET["end_createdate"] == "") ? date("Y-m-d") : $_GET["end_createdate"];
	$search_platform = ($_GET["search_platform"] == "") ? "1" : $_GET["search_platform"];
	$search_total_user_cnt = ($_GET["search_total_user_cnt"] == "") ? "50" : $_GET["search_total_user_cnt"];
	$search_agency = ($_GET["search_agency"] == "") ? "" : $_GET["search_agency"];
	$search_order_by = ($_GET["search_order_by"] == "") ? "2" : $_GET["search_order_by"];
	
	$pagefield = "search_platform=$search_platform&search_total_user_cnt=$search_total_user_cnt&search_agency=$search_agency&search_order_by=$search_order_by&start_createdate=$search_start_createdate&end_createdate=$search_end_createdate&search_order_by=$search_order_by";
	
	$tail = " AND t1.createdate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59' ";
	
 	if($search_platform == "ALL")
		$tail .= " AND platform IN (1, 2) ";
	else if($search_platform == "1")
		$tail .= " AND platform = 1 ";
	else if($search_platform == "2")
		$tail .= " AND platform = 2 ";
	
	if($search_agency != "")
		$agency_tail = " WHERE agency = '$search_agency' ";
	
	if($search_order_by == "")
		$order_by_tail = "ORDER BY login_cnt_1_user_cnt DESC";
	else if($search_order_by == "1") // 총 가입자수
		$order_by_tail = "ORDER BY total_user_cnt ASC";
	else if($search_order_by == "2")
		$order_by_tail = "ORDER BY total_user_cnt DESC";
	else if($search_order_by == "3") // 미게임율
		$order_by_tail = "ORDER BY noplay_user_per ASC";
	else if($search_order_by == "4")
		$order_by_tail = "ORDER BY noplay_user_per DESC";
	else if($search_order_by == "5") // 경험치 0 비중
		$order_by_tail = "ORDER BY noexperience_user_per ASC";
	else if($search_order_by == "6")
		$order_by_tail = "ORDER BY noexperience_user_per DESC";
	else if($search_order_by == "7") // 로그인 10회이상 비중
		$order_by_tail = "ORDER BY login_cnt_10_user_per ASC";
	else if($search_order_by == "8")
		$order_by_tail = "ORDER BY login_cnt_10_user_per DESC";
	
	$db_slave_main = new CDatabase_Slave_Main();
	
	$sql = "SELECT platform, agency, ifcontext, total_user_cnt, noexperience_user_cnt, experience_5under_user_cnt,
			noplay_user_cnt, login_cnt_1_user_cnt, login_cnt_2_user_cnt, login_cnt_3_user_cnt, login_cnt_10_user_cnt,
			
			ROUND((noexperience_user_cnt / total_user_cnt) * 100, 2) AS noexperience_user_per,
			ROUND((experience_5under_user_cnt / total_user_cnt) * 100, 2) AS experience_5under_user_per,
			ROUND((noplay_user_cnt / total_user_cnt) * 100, 2) AS noplay_user_per,
			
			ROUND((login_cnt_1_user_cnt / total_user_cnt) * 100, 2) AS login_cnt_1_user_per,
			ROUND((login_cnt_2_user_cnt / total_user_cnt) * 100, 2) AS login_cnt_2_user_per,
			ROUND((login_cnt_3_user_cnt / total_user_cnt) * 100, 2) AS login_cnt_3_user_per,
			ROUND((login_cnt_10_user_cnt / total_user_cnt) * 100, 2) AS login_cnt_10_user_per
			FROM (
				SELECT CASE platform 
                 WHEN 1 THEN 'iOS' 
                 WHEN 2 THEN 'Android' 
                 WHEN 3 THEN 'Amazon' 
                 ELSE 'Web'  END AS platform, agency, ifcontext,
				COUNT(useridx) AS total_user_cnt, SUM(IF(honor_point = 0, 1, 0)) AS noexperience_user_cnt, SUM(IF(honor_point < 5, 1, 0)) AS experience_5under_user_cnt,
				SUM(IF(coin = 1000000, 1, 0)) AS noplay_user_cnt, SUM(IF(logincount < 2, 1, 0)) AS login_cnt_1_user_cnt, SUM(IF(logincount = 2, 1, 0)) AS login_cnt_2_user_cnt, SUM(IF(logincount = 3, 1, 0)) AS login_cnt_3_user_cnt, SUM(IF(logincount >= 10, 1, 0)) AS login_cnt_10_user_cnt
				FROM (
					SELECT t1.useridx, adflag, REPLACE(adflag, '_int', '') AS agency, ifcontext, coin, logincount, honor_point, LOWER(email) AS country, t1.logindate, platform, t1.createdate
					FROM tbl_user_ext t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx
					WHERE REPLACE(adflag, '_int', '') != '' $tail
				) total
				$agency_tail
				GROUP BY agency, ifcontext
				HAVING total_user_cnt >= $search_total_user_cnt
			) t1
			$order_by_tail";
	
// 	$sql = "SELECT CASE adflag WHEN 'mobile' THEN 'iOS' WHEN 'mobile_ad' THEN 'Android' WHEN 'mobile_kindle' THEN 'Amazon' ELSE 'Web' END AS platform, agency, ifcontext,
// 					COUNT(useridx) AS total_user_cnt, SUM(IF(honor_point = 0, 1, 0)) AS noexperience_user_cnt, SUM(IF(honor_point < 5, 1, 0)) AS experience_5under_user_cnt, SUM(IF(coin = 1000000, 1, 0)) AS noplay_user_cnt,
// 					SUM(IF(logincount < 2, 1, 0)) AS login_cnt_1_user_cnt, SUM(IF(logincount = 2, 1, 0)) AS login_cnt_2_user_cnt, SUM(IF(logincount = 3, 1, 0)) AS login_cnt_3_user_cnt, SUM(IF(logincount >= 10, 1, 0)) AS login_cnt_10_user_cnt
// 			FROM
// 			(
// 				SELECT t1.useridx, adflag, REPLACE(fbsource, '_viral', '') AS agency, ifcontext, coin, logincount, honor_point, LOWER(email) AS country, t1.logindate, t1.createdate
// 				FROM tbl_user_ext t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx
// 				WHERE REPLACE(fbsource, '_viral', '') != '' $tail
// 			) total
// 			$agency_tail
// 			GROUP BY agency, ifcontext
// 			HAVING total_user_cnt >= $search_total_user_cnt
// 			ORDER BY login_cnt_1_user_cnt DESC";
	$marketing_fraud_check_list = $db_slave_main->gettotallist($sql);
// 	write_log($sql);

	$db_slave_main->end();
	
	if (sizeof($marketing_fraud_check_list) == 0)
		error_go("저장할 데이터가 없습니다.", "marketing_fraud_check.php?$pagefield");
	
// 	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
// 			"<table border=1>".
// 			"	<tr>".
// 			"		<td style='font-weight:bold;'>useridx</td>".
// 			"		<td style='font-weight:bold;'>platform</td>".
// 			"		<td style='font-weight:bold;'>install_time</td>".
// 			"		<td style='font-weight:bold;'>agency</td>".
// 			"		<td style='font-weight:bold;'>media_source</td>".
// 			"		<td style='font-weight:bold;'>campaign</td>".
// 			"		<td style='font-weight:bold;'>fb_campaign_name</td>".
// 			"		<td style='font-weight:bold;'>fb_adset_name</td>".
// 			"		<td style='font-weight:bold;'>fb_adgroup_name</td>".
// 			"		<td style='font-weight:bold;'>keyword</td>".
// 			"		<td style='font-weight:bold;'>site_id</td>".
// 			"		<td style='font-weight:bold;'>sub1</td>".
// 			"		<td style='font-weight:bold;'>country_code</td>".
// 			"		<td style='font-weight:bold;'>city</td>".
// 			"		<td style='font-weight:bold;'>device_id</td>".
// 			"		<td style='font-weight:bold;'>device_type</td>".
// 			"	</tr>";
	
	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
						"<table border=1>".
						"<tr>".
			            "	<th style='font-weight:bold;' rowspan='3'>플랫폼</th>".
			            "    <th style='font-weight:bold;' rowspan='3'>agency</th>".
			            "    <th style='font-weight:bold; border-right: double 1px;' rowspan='3'>siteid</th>".
			            "    <th style='font-weight:bold; border-right: double 1px;' colspan='8'>기본</th>".
			            "    <th style='font-weight:bold;' colspan='7'>비중</th>".
			            "</tr>".
			            "<tr>".
			            "    <th style='font-weight:bold;' rowspan='2'>총 가입자수</th>".
			            "    <th style='font-weight:bold;' rowspan='2'>경험치 0 유저</th>".
			            "    <th style='font-weight:bold;' rowspan='2'>경험치 5미만</th>".
			            "    <th style='font-weight:bold;' rowspan='2'>미게임유저</th>".
			            "    <th style='font-weight:bold; border-right: double 1px;' colspan='4'>로그인 카운트 횟수별 유저</th>".
			            "    <th style='font-weight:bold;' rowspan='2'>경험치 0 유저</th>".
			            "    <th style='font-weight:bold;' rowspan='2'>경험치 5미만</th>".
			            "    <th style='font-weight:bold;' rowspan='2'>미게임유저</th>".
			            "    <th style='font-weight:bold;' colspan='4'>로그인 카운트 횟수별 유저</th>".
			            "</tr>".
			            "<tr>".
			            "	<th style='font-weight:bold;'>1회</th>".
			            "    <th style='font-weight:bold;'>2회</th>".
			            "    <th style='font-weight:bold;'>3회</th>".
			            "    <th style='font-weight:bold; border-right: double 1px;'>10회</th>".
			            "    <th style='font-weight:bold;'>1회</th>".
			            "    <th style='font-weight:bold;'>2회</th>".
			            "    <th style='font-weight:bold;'>3회</th>".              
			            "    <th style='font-weight:bold;'>10회</th>".
			            "</tr>";
		
	for($i=0; $i<sizeof($marketing_fraud_check_list); $i++)
	{
		$platform = $marketing_fraud_check_list[$i]["platform"];
		$agency = $marketing_fraud_check_list[$i]["agency"];
		$ifcontext = $marketing_fraud_check_list[$i]["ifcontext"];
		
		$total_user_cnt = $marketing_fraud_check_list[$i]["total_user_cnt"];
		$noexperience_user_cnt = $marketing_fraud_check_list[$i]["noexperience_user_cnt"];
		$experience_5under_user_cnt = $marketing_fraud_check_list[$i]["experience_5under_user_cnt"];
		$noplay_user_cnt = $marketing_fraud_check_list[$i]["noplay_user_cnt"];
		
		$login_cnt_1_user_cnt = $marketing_fraud_check_list[$i]["login_cnt_1_user_cnt"];
		$login_cnt_2_user_cnt = $marketing_fraud_check_list[$i]["login_cnt_2_user_cnt"];
		$login_cnt_3_user_cnt = $marketing_fraud_check_list[$i]["login_cnt_3_user_cnt"];
		$login_cnt_10_user_cnt = $marketing_fraud_check_list[$i]["login_cnt_10_user_cnt"];
		
		$noexperience_user_per = $marketing_fraud_check_list[$i]["noexperience_user_per"];
		$experience_5under_user_per = $marketing_fraud_check_list[$i]["experience_5under_user_per"];
		$noplay_user_per = $marketing_fraud_check_list[$i]["noplay_user_per"];
		
		$login_cnt_1_user_per = $marketing_fraud_check_list[$i]["login_cnt_1_user_per"];
		$login_cnt_2_user_per = $marketing_fraud_check_list[$i]["login_cnt_2_user_per"];
		$login_cnt_3_user_per = $marketing_fraud_check_list[$i]["login_cnt_3_user_per"];
		$login_cnt_10_user_per = $marketing_fraud_check_list[$i]["login_cnt_10_user_per"];
	
		$excel_contents .= "<tr>".
								"<td>".$platform."</td>".
								"<td>".$agency."</td>".
								"<td>".$ifcontext."</td>".
								
								"<td>".(($total_user_cnt == 0) ? "-" : number_format($total_user_cnt))."</td>".
								"<td>".(($noexperience_user_cnt == 0) ? "-" : number_format($noexperience_user_cnt))."</td>".
								"<td>".(($experience_5under_user_cnt == 0) ? "-" : number_format($experience_5under_user_cnt))."</td>".
								"<td>".(($noplay_user_cnt == 0) ? "-" : number_format($noplay_user_cnt))."</td>".
								
								"<td>".(($login_cnt_1_user_cnt == 0) ? "-" : number_format($login_cnt_1_user_cnt))."</td>".
								"<td>".(($login_cnt_2_user_cnt == 0) ? "-" : number_format($login_cnt_2_user_cnt))."</td>".
								"<td>".(($login_cnt_3_user_cnt == 0) ? "-" : number_format($login_cnt_3_user_cnt))."</td>".
								"<td>".(($login_cnt_10_user_cnt == 0) ? "-" : number_format($login_cnt_10_user_cnt))."</td>".
								
								"<td>".(($noexperience_user_per == 0) ? "-" : number_format($noexperience_user_per, 2)."%")."</td>".
								"<td>".(($experience_5under_user_per == 0) ? "-" : number_format($experience_5under_user_per, 2)."%")."</td>".
								"<td>".(($noplay_user_per == 0) ? "-" : number_format($noplay_user_per, 2)."%")."</td>".
								
								"<td>".(($login_cnt_1_user_per == 0) ? "-" : number_format($login_cnt_1_user_per, 2)."%")."</td>".
								"<td>".(($login_cnt_2_user_per == 0) ? "-" : number_format($login_cnt_2_user_per, 2)."%")."</td>".
								"<td>".(($login_cnt_3_user_per == 0) ? "-" : number_format($login_cnt_3_user_per, 2)."%")."</td>".
								"<td>".(($login_cnt_10_user_per == 0) ? "-" : number_format($login_cnt_10_user_per, 2)."%")."</td>".
							"</tr>";
	}
	$excel_contents .= "</table>";
	 	
	Header("Content-type: application/x-msdownload");
	Header("Content-type: application/x-msexcel");
	Header("Content-Disposition: attachment; filename=$filename");
	
	echo($excel_contents);
?>