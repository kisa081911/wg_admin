<?
	$top_menu = "marketing";
	$sub_menu = "marketing_roi_factor_week_stat";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$db_other = new CDatabase_Other();
	
	$max_today = $db_other->getvalue("SELECT MAX(today) FROM tbl_marketing_week_stat");
	
	$today = ($_GET["today"] == "") ? $max_today : $_GET["today"];

	$type_term = ($_GET["type_term"] == "") ? "1" : $_GET["type_term"];
	$os_term = ($_GET["term"] == "") ? "0" : $_GET["term"];
	$grade_term = ($_GET["grade_term"] == "") ? "3" : $_GET["grade_term"];
	$week_term = ($_GET["week_term"] == "") ? "4" : $_GET["week_term"];

	if ($os_term != "ALL" && $os_term != "0" && $os_term != "1" && $os_term != "2" && $os_term != "3" 
			&& $week_term != "1" && $week_term != "2" && $week_term != "3" && $week_term != "4"
			&& $grade_term != "1" && $grade_term != "2" && $grade_term != "3")
		error_back("잘못된 접근입니다.");
	
	$platform = $os_term;
	
	$table = "tbl_roi_factor";
	
	$sql = "SELECT *, '',
					(kw_money * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 5)) +
					(wh_money * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 4)) +
					(sl_money * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 3)) +
					(dp_money * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 2)) +
					(mn_money * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 1)) AS pay_money,
				
					((SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 0 AND platform = $platform) - 1) *
					((kw_money * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 5)) +
					(wh_money * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 4)) +
					(sl_money * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 3)) +
					(dp_money * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 2)) +
					(mn_money * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 1))) AS nopay_money
			FROM tbl_marketing_week_stat
			WHERE category = $grade_term AND platform = $platform AND week_type = $week_term AND today = '$today'
			GROUP BY adflag";
	$marketing_week_stats = $db_other->gettotallist($sql);
	
	$sql = "SELECT COUNT(DISTINCT adflag)
			FROM tbl_marketing_week_stat
			WHERE category = $grade_term AND platform = $platform AND week_type = $week_term AND today = '$today'";
	$rowspan = $db_other->getvalue($sql);
	
	$sql = "SELECT today, category, platform, 'ALL' AS adflag, week_type, SUM(user_cnt) AS user_cnt, SUM(spend) AS spend,
	
					SUM(kw_money) AS kw_money, SUM(kw_cnt) AS kw_cnt,
					SUM(wh_money) AS wh_money, SUM(wh_cnt) AS wh_cnt,
					SUM(sl_money) AS sl_money, SUM(sl_cnt) AS sl_cnt,
					SUM(dp_money) AS dp_money, SUM(dp_cnt) AS dp_cnt,
					SUM(mn_money) AS mn_money, SUM(mn_cnt) AS mn_cnt,
					
					(SUM(kw_money) * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 5)) +
					(SUM(wh_money) * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 4)) +
					(SUM(sl_money) * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 3)) +
					(SUM(dp_money) * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 2)) +
					(SUM(mn_money) * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 1)) AS pay_money,
				
					((SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 0 AND platform = $platform) - 1) *
					((SUM(kw_money) * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 5)) +
					(SUM(wh_money) * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 4)) +
					(SUM(sl_money) * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 3)) +
					(SUM(dp_money) * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 2)) +
					(SUM(mn_money) * (SELECT ROUND(multiple/divd, 15) AS multiple FROM $table WHERE type = $type_term AND today = '$today' AND category = $grade_term AND is_payer = 1 AND platform = $platform AND grade = 1))) AS nopay_money
			FROM tbl_marketing_week_stat
			WHERE category = $grade_term AND platform = $platform AND week_type = $week_term AND today = '$today'";
	$total_marketing_week_stats = $db_other->getarray($sql);
	
	$db_other->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	function tab_change(tab)
	{
		var search_form = document.search_form;
		search_form.tab.value = tab;
		search_form.submit();
	}
	
	function change_term(term)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("term_web");
		var ios = document.getElementById("term_ios");
		var android = document.getElementById("term_android");
		var amazon = document.getElementById("term_amazon");
		
		search_form.term.value = term;
		
		if (term == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (term == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
	
	function change_type_term(term)
	{
		var search_form = document.search_form;
		
		var term_1 = document.getElementById("type_term_1");
		var term_2 = document.getElementById("type_term_2");
		
		search_form.type_term.value = term;
		
		if (term == "1")
		{
			term_1.className="btn_schedule_select";
			term_2.className="btn_schedule";
		}
		else if (term == "2")
		{
			term_1.className="btn_schedule";
			term_2.className="btn_schedule_select";
		}
		
		search_form.submit();
	}
	
	function change_grade_term(term)
	{
		var search_form = document.search_form;
		
		var term_1 = document.getElementById("grade_term_1");
		var term_2 = document.getElementById("grade_term_2");
		var term_3 = document.getElementById("grade_term_3");
		
		search_form.grade_term.value = term;
		
		if (term == "1")
		{
			term_1.className="btn_schedule_select";
			term_2.className="btn_schedule";
			term_3.className="btn_schedule";
		}
		else if (term == "2")
		{
			term_1.className="btn_schedule";
			term_2.className="btn_schedule_select";
			term_3.className="btn_schedule";
		}
		else if (term == "3")
		{
			term_1.className="btn_schedule";
			term_2.className="btn_schedule";
			term_3.className="btn_schedule_select";
		}
		
		search_form.submit();
	}
	
	function change_week_term(term)
	{
		var search_form = document.search_form;
		
		var term_1 = document.getElementById("term_1");
		var term_2 = document.getElementById("term_2");
		var term_3 = document.getElementById("term_3");
		var term_4 = document.getElementById("term_4");
		
		search_form.week_term.value = term;
		
		if (term == "1")
		{
			term_1.className="btn_schedule_select";
			term_2.className="btn_schedule";
			term_3.className="btn_schedule";
			term_4.className="btn_schedule";
		}
		else if (term == "2")
		{
			term_1.className="btn_schedule";
			term_2.className="btn_schedule_select";
			term_3.className="btn_schedule";
			term_4.className="btn_schedule";
		}
		else if (term == "3")
		{
			term_1.className="btn_schedule";
			term_2.className="btn_schedule";
			term_3.className="btn_schedule_select";
			term_4.className="btn_schedule";
		}
		else if (term == "4")
		{
			term_1.className="btn_schedule";
			term_2.className="btn_schedule";
			term_3.className="btn_schedule";
			term_4.className="btn_schedule_select";
		}
	
		search_form.submit();
	}

	$(function() {
		$("#today").datepicker({});
		$("#startdate").datepicker({});
		$("#enddate").datepicker({});
	});

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
		
	function search()
	{
	    var search_form = document.search_form;

	    if (search_form.today.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.today.focus();
	        return;
	    }

	    search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 마케팅 ROI 예측
			<input type="button" class="<?= ($os_term == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="term_web" onclick="change_term('0')" />
			<input type="button" class="<?= ($os_term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_term('1')" />
			<input type="button" class="<?= ($os_term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_term('2')"    />
			<input type="button" class="<?= ($os_term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_term('3')"    />
		</div>
		
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<input type="hidden" name="type_term" id="type_term" value="<?= $type_term ?>" />
			<input type="hidden" name="term" id="term" value="<?= $os_term ?>" />
			<input type="hidden" name="grade_term" id="grade_term" value="<?= $grade_term ?>" />
			<input type="hidden" name="week_term" id="week_term" value="<?= $week_term ?>" />
			<div class="search_box"> 
				<input type="button" class="<?= ($type_term == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="D+360일" id="type_term_1" onclick="change_type_term('0')" />
				<input type="button" class="<?= ($type_term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="D+540일" id="type_term_2" onclick="change_type_term('1')" />
				<br/><br/>
				Grade : 
				<input type="button" class="<?= ($grade_term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="7일" id="grade_term_1" onclick="change_grade_term('1')" />
				<input type="button" class="<?= ($grade_term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="14일" id="grade_term_2" onclick="change_grade_term('2')" />
				<input type="button" class="<?= ($grade_term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="28일" id="grade_term_3" onclick="change_grade_term('3')" />
				<br/><br/>
				Week : 
				<input type="button" class="<?= ($week_term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="1주" id="term_1" onclick="change_week_term('1')" />
				<input type="button" class="<?= ($week_term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="2주" id="term_2" onclick="change_week_term('2')" />
				<input type="button" class="<?= ($week_term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="3주" id="term_3" onclick="change_week_term('3')" />
				<input type="button" class="<?= ($week_term == "4") ? "btn_schedule_select" : "btn_schedule" ?>" value="4주" id="term_4" onclick="change_week_term('4')" />
				<br/><br/>
				날짜 : &nbsp;&nbsp;<input type="text" class="search_text" id="today" name="today" value="<?= $today ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	
	<!-- Web -->
	<table class="tbl_list_basic1" style="width:1300px">
		<colgroup>
			<col width="100">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			
			<col width="">
			<col width="">
			
			<col width="55">
        </colgroup>
        
        <thead>
        	<tr>
        		<th class="tdc" style="border-right: 1px solid #dbdbdb;" rowspan="2">날짜</th>
        		<th class="tdc" rowspan="2">Agency </th>
        		<th class="tdr" rowspan="2">광고비용 </th>
        		<th class="tdr" rowspan="2">유입수</th>
        		<th class="tdr" style="border-right: 1px solid #dbdbdb;" rowspan="2">CPI </th>
<?
	if($grade_term == 1)
		$grade_str = "7";
	else if($grade_term == 2)
		$grade_str = "14";
	else if($grade_term == 3)
		$grade_str = "28";
	
		if($type_term == 0)
			$type_str = 360;
		else
			$type_str = 540;
?>
        		<th class="tdc" style="border-right: 1px solid #dbdbdb;" colspan="5">D+<?= $grade_str ?> 결제자수</th>
        		<th class="tdc" style="border-right: 1px solid #dbdbdb;" colspan="7">D+<?= $grade_str ?> 결제금액</th>
        		<th class="tdc" style="border-right: 1px solid #dbdbdb;" colspan="2">D+<?= $type_str ?> 예상</th>
        		<th class="tdc" style="border-right: 1px solid #dbdbdb;" rowspan="2">기준ROI</th>
        	</tr>
        	
        	<tr>
            	<th class="tdr">KW</th>
            	<th class="tdr">WH</th>
            	<th class="tdr">SL</th>
            	<th class="tdr">DP</th>
            	<th class="tdr" style="border-right: 1px solid #dbdbdb;">MN</th>
               	
               	<th class="tdr">KW</th>
            	<th class="tdr">WH</th>
            	<th class="tdr">SL</th>
            	<th class="tdr">DP</th>
            	<th class="tdr">MN</th>
               	<th class="tdr">총 결제금액</th>
               	<th class="tdr" style="border-right: 1px solid #dbdbdb;">ROI</th>
               	
               	<th class="tdr">결제금액</th>
               	<th class="tdr" style="border-right: 1px solid #dbdbdb;">ROI</th>
         	</tr>
        </thead>
        
        <tbody>
<?
	for($i=0; $i<sizeof($marketing_week_stats); $i++)
	{
		$today = $marketing_week_stats[$i]["today"];
		$adflag = $marketing_week_stats[$i]["adflag"];
		$spend = $marketing_week_stats[$i]["spend"];
		$user_cnt = $marketing_week_stats[$i]["user_cnt"];
		$cpi = ($user_cnt == 0) ? 0 : number_format($spend / $user_cnt, 1);
		
		$kw_cnt = $marketing_week_stats[$i]["kw_cnt"];
		$wh_cnt = $marketing_week_stats[$i]["wh_cnt"];
		$sl_cnt = $marketing_week_stats[$i]["sl_cnt"];
		$dp_cnt = $marketing_week_stats[$i]["dp_cnt"];
		$mn_cnt = $marketing_week_stats[$i]["mn_cnt"];
		
		$kw_money = $marketing_week_stats[$i]["kw_money"];
		$wh_money = $marketing_week_stats[$i]["wh_money"];
		$sl_money = $marketing_week_stats[$i]["sl_money"];
		$dp_money = $marketing_week_stats[$i]["dp_money"];
		$mn_money = $marketing_week_stats[$i]["mn_money"];
		$grade_sum_money = $kw_money + $wh_money + $sl_money + $dp_money + $mn_money;  
		$grade_roi = ($spend == 0) ? 0 : number_format((($grade_sum_money) / $spend) * 100, 2);
		
		$pay_money = $marketing_week_stats[$i]["pay_money"];
		$nopay_money = $marketing_week_stats[$i]["nopay_money"];
		

			$d_sum_money = $pay_money + $nopay_money;

			
		$d_roi = ($spend == 0) ? 0 : number_format((($d_sum_money) / $spend) * 100, 2);
		
?>
			<tr>
<?
		if($i == 0)
		{
?>
				<td class="tdc point" style="font-size: 10pt; border-right: 1px solid #dbdbdb;" rowspan="<?= $rowspan ?>"><?= $today ?></td>
<?
		}
?>
				<td class="tdc"><?= $adflag ?></td>
				<td class="tdr"><?= ($spend == 0) ? "-" : "$ ".number_format($spend) ?></td>
				<td class="tdr"><?= ($user_cnt == 0) ? "-" : number_format($user_cnt) ?></td>
				<td class="tdr" style="border-right: 1px solid #dbdbdb;"><?= ($cpi == 0) ? "-" : "$ ".$cpi ?></td>
				
				<td class="tdr"><?= ($kw_cnt == 0) ? "-" : number_format($kw_cnt) ?></td>
				<td class="tdr"><?= ($wh_cnt == 0) ? "-" : number_format($wh_cnt) ?></td>
				<td class="tdr"><?= ($sl_cnt == 0) ? "-" : number_format($sl_cnt) ?></td>
				<td class="tdr"><?= ($dp_cnt == 0) ? "-" : number_format($dp_cnt) ?></td>
				<td class="tdr" style="border-right: 1px solid #dbdbdb;"><?= ($mn_cnt == 0) ? "-" : number_format($mn_cnt) ?></td>
				
				<td class="tdr"><?= ($kw_money == 0) ? "-" : "$ ".number_format($kw_money) ?></td>
				<td class="tdr"><?= ($wh_money == 0) ? "-" : "$ ".number_format($wh_money) ?></td>
				<td class="tdr"><?= ($sl_money == 0) ? "-" : "$ ".number_format($sl_money) ?></td>
				<td class="tdr"><?= ($dp_money == 0) ? "-" : "$ ".number_format($dp_money) ?></td>
				<td class="tdr"><?= ($mn_money == 0) ? "-" : "$ ".number_format($mn_money) ?></td>
				<td class="tdr"><?= ($grade_sum_money == 0) ? "-" : "$ ".number_format($grade_sum_money) ?></td>
				<td class="tdr" style="border-right: 1px solid #dbdbdb;"><?= ($grade_roi == 0) ? "-" : number_format($grade_roi, 1)."%" ?></td>

				<td class="tdr"> <?= ($d_sum_money == 0) ? "-" : "$ ".number_format($d_sum_money) ?></td>
				<td class="tdr" style="border-right: 1px solid #dbdbdb;"><?= ($d_roi == 0) ? "-" : number_format($d_roi, 1)."%" ?></td>
				
				<td style="border-right: 1px solid #dbdbdb;"></td>
			</tr>
<?
	}
	
	if(sizeof($total_marketing_week_stats) != 0)
	{
		$total_spend = $total_marketing_week_stats["spend"];
		$total_user_cnt = $total_marketing_week_stats["user_cnt"];
		$total_cpi = ($total_user_cnt == 0) ? 0 : number_format($total_spend / $total_user_cnt, 1);
		
		$total_kw_cnt = $total_marketing_week_stats["kw_cnt"];
		$total_wh_cnt = $total_marketing_week_stats["wh_cnt"];
		$total_sl_cnt = $total_marketing_week_stats["sl_cnt"];
		$total_dp_cnt = $total_marketing_week_stats["dp_cnt"];
		$total_mn_cnt = $total_marketing_week_stats["mn_cnt"];
		
		$total_kw_money = $total_marketing_week_stats["kw_money"];
		$total_wh_money = $total_marketing_week_stats["wh_money"];
		$total_sl_money = $total_marketing_week_stats["sl_money"];
		$total_dp_money = $total_marketing_week_stats["dp_money"];
		$total_mn_money = $total_marketing_week_stats["mn_money"];
		$total_grade_sum_money = $total_kw_money + $total_wh_money + $total_sl_money + $total_dp_money + $total_mn_money;
		$total_grade_roi = ($total_spend == 0) ? 0 : number_format((($total_grade_sum_money) / $total_spend) * 100, 2);
		
		$total_pay_money = $total_marketing_week_stats["pay_money"];
		$total_nopay_money = $total_marketing_week_stats["nopay_money"];
		
	
			$total_d_sum_money = $total_pay_money + $total_nopay_money;
	
		
		$total_d_roi = ($total_spend == 0) ? 0 : number_format((($total_d_sum_money) / $total_spend) * 100, 2);
?>
			<tr style="border-top:1px double;">
				<td class="tdc point" colspan="2">Total</td>
				<td class="tdr"><?= ($spend == 0) ? "-" : "$ ".number_format($total_spend) ?></td>
				<td class="tdr"><?= ($total_user_cnt == 0) ? "-" : number_format($total_user_cnt) ?></td>
				<td class="tdr" style="border-right: 1px solid #dbdbdb;"><?= ($total_cpi == 0) ? "-" : "$ ".$total_cpi ?></td>
				
				<td class="tdr"><?= ($total_kw_cnt == 0) ? "-" : number_format($total_kw_cnt) ?></td>
				<td class="tdr"><?= ($total_wh_cnt == 0) ? "-" : number_format($total_wh_cnt) ?></td>
				<td class="tdr"><?= ($total_sl_cnt == 0) ? "-" : number_format($total_sl_cnt) ?></td>
				<td class="tdr"><?= ($total_dp_cnt == 0) ? "-" : number_format($total_dp_cnt) ?></td>
				<td class="tdr" style="border-right: 1px solid #dbdbdb;"><?= ($total_mn_cnt == 0) ? "-" : number_format($total_mn_cnt) ?></td>
				
				<td class="tdr"><?= ($total_kw_money == 0) ? "-" : "$ ".number_format($total_kw_money) ?></td>
				<td class="tdr"><?= ($total_wh_money == 0) ? "-" : "$ ".number_format($total_wh_money) ?></td>
				<td class="tdr"><?= ($total_sl_money == 0) ? "-" : "$ ".number_format($total_sl_money) ?></td>
				<td class="tdr"><?= ($total_dp_money == 0) ? "-" : "$ ".number_format($total_dp_money) ?></td>
				<td class="tdr"><?= ($total_mn_money == 0) ? "-" : "$ ".number_format($total_mn_money) ?></td>
				<td class="tdr"><?= ($total_grade_sum_money == 0) ? "-" : "$ ".number_format($total_grade_sum_money) ?></td>
				<td class="tdr" style="border-right: 1px solid #dbdbdb;"><?= ($total_grade_roi == 0) ? "-" : number_format($total_grade_roi, 1)."%" ?></td>

				<td class="tdr"> <?= ($total_d_sum_money == 0) ? "-" : "$ ".number_format($total_d_sum_money) ?></td>
				<td class="tdr" style="border-right: 1px solid #dbdbdb;"><?= ($total_d_roi == 0) ? "-" : number_format($total_d_roi, 1)."%" ?></td>
				
				<td style="border-right: 1px solid #dbdbdb;"></td>
			</tr>
<?
	}
?>
        </tbody>
	</table>
</div>
        
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
