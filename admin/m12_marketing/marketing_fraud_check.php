<?
    $top_menu = "marketing";
    $sub_menu = "marketing_fraud_check";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $search_start_createdate = ($_GET["start_createdate"] == "") ? date("Y-m-d", strtotime("-14 day")) : $_GET["start_createdate"];
    $search_end_createdate = ($_GET["end_createdate"] == "") ? date("Y-m-d") : $_GET["end_createdate"];
	$search_platform = ($_GET["search_platform"] == "") ? "1" : $_GET["search_platform"];
	$search_total_user_cnt = ($_GET["search_total_user_cnt"] == "") ? "50" : $_GET["search_total_user_cnt"];
	$search_agency = ($_GET["search_agency"] == "") ? "" : $_GET["search_agency"];
	$search_order_by = ($_GET["search_order_by"] == "") ? "2" : $_GET["search_order_by"];
	
	$pagefield = "search_platform=$search_platform&search_total_user_cnt=$search_total_user_cnt&search_agency=$search_agency&search_order_by=$search_order_by&start_createdate=$search_start_createdate&end_createdate=$search_end_createdate&search_order_by=$search_order_by";
	
	$tail = " AND t1.createdate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59' ";
	
	if($search_platform == "ALL")
		$tail .= " AND platform IN (1, 2) ";
	else if($search_platform == "1")
		$tail .= " AND platform = 1 ";
	else if($search_platform == "2")
		$tail .= " AND platform = 2 ";
	
	if($search_agency != "")
		$agency_tail = " WHERE agency = '$search_agency' ";
	
	if($search_order_by == "")
		$order_by_tail = "ORDER BY login_cnt_1_user_cnt DESC";
	else if($search_order_by == "1") // 총 가입자수
		$order_by_tail = "ORDER BY total_user_cnt ASC";
	else if($search_order_by == "2")
		$order_by_tail = "ORDER BY total_user_cnt DESC";
	else if($search_order_by == "3") // 미게임율
		$order_by_tail = "ORDER BY noplay_user_per ASC";
	else if($search_order_by == "4")
		$order_by_tail = "ORDER BY noplay_user_per DESC";
	else if($search_order_by == "5") // 경험치 0 비중
		$order_by_tail = "ORDER BY noexperience_user_per ASC";
	else if($search_order_by == "6")
		$order_by_tail = "ORDER BY noexperience_user_per DESC";
	else if($search_order_by == "7") // 로그인 10회이상 비중
		$order_by_tail = "ORDER BY login_cnt_10_user_per ASC";
	else if($search_order_by == "8")
		$order_by_tail = "ORDER BY login_cnt_10_user_per DESC";
		
	$db_slave_main = new CDatabase_Slave_Main();
	
	$sql = "SELECT platform, agency, ifcontext, total_user_cnt, noexperience_user_cnt, experience_5under_user_cnt,
			noplay_user_cnt, login_cnt_1_user_cnt, login_cnt_2_user_cnt, login_cnt_3_user_cnt, login_cnt_10_user_cnt,
			
			ROUND((noexperience_user_cnt / total_user_cnt) * 100, 2) AS noexperience_user_per,
			ROUND((experience_5under_user_cnt / total_user_cnt) * 100, 2) AS experience_5under_user_per,
			ROUND((noplay_user_cnt / total_user_cnt) * 100, 2) AS noplay_user_per,
			
			ROUND((login_cnt_1_user_cnt / total_user_cnt) * 100, 2) AS login_cnt_1_user_per,
			ROUND((login_cnt_2_user_cnt / total_user_cnt) * 100, 2) AS login_cnt_2_user_per,
			ROUND((login_cnt_3_user_cnt / total_user_cnt) * 100, 2) AS login_cnt_3_user_per,
			ROUND((login_cnt_10_user_cnt / total_user_cnt) * 100, 2) AS login_cnt_10_user_per
			FROM (
				SELECT CASE platform 
                 WHEN 1 THEN 'iOS' 
                 WHEN 2 THEN 'Android' 
                 WHEN 3 THEN 'Amazon' 
                 ELSE 'Web'  END AS platform, agency, ifcontext,
				COUNT(useridx) AS total_user_cnt, SUM(IF(honor_point = 0, 1, 0)) AS noexperience_user_cnt, SUM(IF(honor_point < 5, 1, 0)) AS experience_5under_user_cnt,
				SUM(IF(coin = 1000000, 1, 0)) AS noplay_user_cnt, SUM(IF(logincount < 2, 1, 0)) AS login_cnt_1_user_cnt, SUM(IF(logincount = 2, 1, 0)) AS login_cnt_2_user_cnt, SUM(IF(logincount = 3, 1, 0)) AS login_cnt_3_user_cnt, SUM(IF(logincount >= 10, 1, 0)) AS login_cnt_10_user_cnt
				FROM (
					SELECT t1.useridx, adflag, REPLACE(adflag, '_int', '') AS agency, ifcontext, coin, logincount, honor_point, LOWER(email) AS country, t1.logindate, platform, t1.createdate
					FROM tbl_user_ext t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx
					WHERE REPLACE(adflag, '_int', '') != '' $tail
				) total
				$agency_tail
				GROUP BY agency, ifcontext
				HAVING total_user_cnt >= $search_total_user_cnt
			) t1
			$order_by_tail";
	
// 	$sql = "SELECT CASE adflag WHEN 'mobile' THEN 'iOS' WHEN 'mobile_ad' THEN 'Android' WHEN 'mobile_kindle' THEN 'Amazon' ELSE 'Web' END AS platform, agency, ifcontext,
// 					COUNT(useridx) AS total_user_cnt, SUM(IF(honor_point = 0, 1, 0)) AS noexperience_user_cnt, SUM(IF(honor_point < 5, 1, 0)) AS experience_5under_user_cnt, SUM(IF(coin = 1000000, 1, 0)) AS noplay_user_cnt,
// 					SUM(IF(logincount < 2, 1, 0)) AS login_cnt_1_user_cnt, SUM(IF(logincount = 2, 1, 0)) AS login_cnt_2_user_cnt, SUM(IF(logincount = 3, 1, 0)) AS login_cnt_3_user_cnt, SUM(IF(logincount >= 10, 1, 0)) AS login_cnt_10_user_cnt
// 			FROM
// 			(
// 				SELECT t1.useridx, adflag, REPLACE(fbsource, '_viral', '') AS agency, ifcontext, coin, logincount, honor_point, LOWER(email) AS country, t1.logindate, t1.createdate
// 				FROM tbl_user_ext t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx
// 				WHERE REPLACE(fbsource, '_viral', '') != '' $tail
// 			) total
// 			$agency_tail
// 			GROUP BY agency, ifcontext
// 			HAVING total_user_cnt >= $search_total_user_cnt
// 			ORDER BY login_cnt_1_user_cnt DESC";
	$marketing_fraud_check_list = $db_slave_main->gettotallist($sql);
// 	echo($sql);
	
	$sql = "SELECT DISTINCT agency
			FROM
			(
				SELECT t1.useridx, adflag, REPLACE(adflag, '_int', '') AS agency, ifcontext, coin, logincount, honor_point, LOWER(email) AS country, t1.logindate, t1.createdate
				FROM tbl_user_ext t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx
				WHERE REPLACE(adflag, '_int', '') != '' $tail
			) total
			GROUP BY agency, ifcontext
			HAVING COUNT(useridx) > $search_total_user_cnt
			ORDER BY agency ASC";
	$agency_list = $db_slave_main->gettotallist($sql);
			
	$db_slave_main->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	function change_term(term)
	{
		var search_form = document.search_form;
		
		var ios = document.getElementById("term_ios");
		var android = document.getElementById("term_android");
		
		search_form.search_platform.value = term;
		
		if (term == "ALL")
		{
			ios.className="btn_schedule";
			android.className="btn_schedule";
		}		
		else if (term == "1")
		{
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
		}
		else if (term == "2")
		{
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
	
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>
        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
        <form name="search_form" id="search_form"  method="get" action="marketing_fraud_check.php">
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 마케팅 Fraud 체크</div>
                <div class="search_box">
                	<input type="hidden" name="search_platform" id="search_platform" value="<?= $search_platform ?>" />
					<input type="button" class="<?= ($search_platform == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_term('1')" />
					<input type="button" class="<?= ($search_platform == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_term('2')" />
					
					<br/><br/>
					
                	가입유저수 : 
                	<select name="search_total_user_cnt" id="search_total_user_cnt">
						<option value="10" <?= ($search_total_user_cnt == "10") ? "selected" : "" ?>>10명 이상</option>
						<option value="20" <?= ($search_total_user_cnt == "20") ? "selected" : "" ?>>20명 이상</option>
						<option value="50" <?= ($search_total_user_cnt == "50") ? "selected" : "" ?>>50명 이상</option>
						<option value="100" <?= ($search_total_user_cnt == "100") ? "selected" : "" ?>>100명 이상</option>
					</select>
					
					<br/><br/>
					
					Agency : 
                	<select name="search_agency" id="search_agency">
                		<option value="" <?= ($search_agency == "") ? "selected" : "" ?>>전체</option>
<?
			for($i=0; $i<sizeof($agency_list); $i++)
			{
				$agency_name = $agency_list[$i]["agency"];
?>
						<option value="<?= $agency_name ?>" <?= ($agency_name == $search_agency) ? "selected" : "" ?>><?= $agency_name ?></option>
<?
			}
?>
					</select>
					
					<br/><br/>
					
                	정렬기준
                	<select name="search_order_by" id="search_order_by">
						<option value="1" <?= ($search_order_by == "1") ? "selected" : "" ?>>총가입자수 ▲</option>
						<option value="2" <?= ($search_order_by == "2") ? "selected" : "" ?>>총가입자수 ▼</option>
						
						<option value="3" <?= ($search_order_by == "3") ? "selected" : "" ?>>미게임율 ▲</option>
						<option value="4" <?= ($search_order_by == "4") ? "selected" : "" ?>>미게임율 ▼</option>
						
						<option value="5" <?= ($search_order_by == "5") ? "selected" : "" ?>>경험치 0비중 ▲</option>
						<option value="6" <?= ($search_order_by == "6") ? "selected" : "" ?>>경험치 0비중 ▼</option>
						
						<option value="7" <?= ($search_order_by == "7") ? "selected" : "" ?>>로그인 10회이상 비중 ▲</option>
						<option value="8" <?= ($search_order_by == "8") ? "selected" : "" ?>>로그인 10회이상 비중 ▼</option>
					</select>
					
					<br/><br/>
										
					가입 날짜 :
					<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
                    <input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
                     <input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
                </div>
            </div>
            <!-- //title_warp -->
            
            <div class="search_result">
                <span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
                <input type="button" class="btn_03" value="Excel 다운로드" onclick="window.location.href='marketing_fraud_check_excel.php?<?= $pagefield ?>'" />
            </div>
            
            <div id="tab_content_1">
            <table class="tbl_list_basic1" style="width:1400px">
            <colgroup>
                <col width=""> 
                <col width="150">
                <col width="100">
                 
                <col width="80">
                <col width="90">
                <col width="85">
                <col width="80">
                
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                
                <col width="90">
                <col width="85">
                <col width="80">
                
                <col width="">
                <col width="">
                <col width="">
                <col width="">
            </colgroup>
            <thead>
            <tr>
            	<th class="tdc" rowspan="3">플랫폼</th>
                <th class="tdc" rowspan="3">agency</th>
                <th class="tdc" rowspan="3" style="border-right: double 1px">siteid</th>
                <th class="tdc" colspan="8" style="border-right: double 1px">기본</th>
                <th class="tdc" colspan="7">비중</th>
            </tr>
            
            <tr>
                <th class="tdr" rowspan="2">총 가입자수</th>
                <th class="tdr" rowspan="2">경험치 0 유저</th>
                <th class="tdr" rowspan="2">경험치 5미만</th>
                <th class="tdr" rowspan="2">미게임유저</th>
                
                <th class="tdc" colspan="4" style="border-right: double 1px">로그인 카운트 횟수별 유저</th>
                
                <th class="tdr" rowspan="2">경험치 0 유저</th>
                <th class="tdr" rowspan="2">경험치 5미만</th>
                <th class="tdr" rowspan="2">미게임유저</th>
                
                <th class="tdc" colspan="4">로그인 카운트 횟수별 유저</th>
                
                
            </tr>
            
            <tr>
            	<th class="tdr">1회</th>
                <th class="tdr">2회</th>
                <th class="tdr">3회</th>
                <th class="tdr" style="border-right: double 1px">10회</th>
                
                <th class="tdr">1회</th>
                <th class="tdr">2회</th>
                <th class="tdr">3회</th>                
                <th class="tdr">10회</th>
            </tr>
            </thead>
            <tbody>
<?
			for($i=0; $i<sizeof($marketing_fraud_check_list); $i++)
			{
				$platform = $marketing_fraud_check_list[$i]["platform"];
				$agency = $marketing_fraud_check_list[$i]["agency"];
				$ifcontext = $marketing_fraud_check_list[$i]["ifcontext"];
				
				$total_user_cnt = $marketing_fraud_check_list[$i]["total_user_cnt"];
				$noexperience_user_cnt = $marketing_fraud_check_list[$i]["noexperience_user_cnt"];
				$experience_5under_user_cnt = $marketing_fraud_check_list[$i]["experience_5under_user_cnt"];
				$noplay_user_cnt = $marketing_fraud_check_list[$i]["noplay_user_cnt"];
				
				$login_cnt_1_user_cnt = $marketing_fraud_check_list[$i]["login_cnt_1_user_cnt"];
				$login_cnt_2_user_cnt = $marketing_fraud_check_list[$i]["login_cnt_2_user_cnt"];
				$login_cnt_3_user_cnt = $marketing_fraud_check_list[$i]["login_cnt_3_user_cnt"];
				$login_cnt_10_user_cnt = $marketing_fraud_check_list[$i]["login_cnt_10_user_cnt"];
				
				$noexperience_user_per = $marketing_fraud_check_list[$i]["noexperience_user_per"];
				$experience_5under_user_per = $marketing_fraud_check_list[$i]["experience_5under_user_per"];
				$noplay_user_per = $marketing_fraud_check_list[$i]["noplay_user_per"];
				
				$login_cnt_1_user_per = $marketing_fraud_check_list[$i]["login_cnt_1_user_per"];
				$login_cnt_2_user_per = $marketing_fraud_check_list[$i]["login_cnt_2_user_per"];
				$login_cnt_3_user_per = $marketing_fraud_check_list[$i]["login_cnt_3_user_per"];
				$login_cnt_10_user_per = $marketing_fraud_check_list[$i]["login_cnt_10_user_per"];

// 				$noexperience_percent = $noexperience_user_cnt / $total_user_cnt * 100;
// 				$noexperience_5under_percent = $noexperience_5under_user_cnt / $total_user_cnt * 100;
// 				$noplay_user_percent = $noplay_user_cnt / $total_user_cnt * 100;
				
// 				$login_cnt_1_user_percent = $login_cnt_1_user_cnt / $total_user_cnt * 100;
// 				$login_cnt_2_user_percent = $login_cnt_2_user_cnt / $total_user_cnt * 100;
// 				$login_cnt_3_user_percent = $login_cnt_3_user_cnt / $total_user_cnt * 100;
// 				$login_cnt_10_user_percent = $login_cnt_10_user_cnt / $total_user_cnt * 100;
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
					<td class="tdc"><?= $platform ?></td>
					<td class="tdc"><?= $agency ?></td>
					<td class="tdc" style="border-right: double 1px"><?= $ifcontext ?></td>
					
					<td class="tdr"><?= ($total_user_cnt == 0) ? "-" : number_format($total_user_cnt) ?></td>
					<td class="tdr"><?= ($noexperience_user_cnt == 0) ? "-" : number_format($noexperience_user_cnt) ?></td>
					<td class="tdr"><?= ($experience_5under_user_cnt == 0) ? "-" : number_format($experience_5under_user_cnt) ?></td>
					<td class="tdr"><?= ($noplay_user_cnt == 0) ? "-" : number_format($noplay_user_cnt) ?></td>
					
					<td class="tdr"><?= ($login_cnt_1_user_cnt == 0) ? "-" : number_format($login_cnt_1_user_cnt) ?></td>
					<td class="tdr"><?= ($login_cnt_2_user_cnt == 0) ? "-" : number_format($login_cnt_2_user_cnt) ?></td>
					<td class="tdr"><?= ($login_cnt_3_user_cnt == 0) ? "-" : number_format($login_cnt_3_user_cnt) ?></td>
					<td class="tdr" style="border-right: double 1px"><?= ($login_cnt_10_user_cnt == 0) ? "-" : number_format($login_cnt_10_user_cnt) ?></td>
					
					<td class="tdr"><?= ($noexperience_user_per == 0) ? "-" : number_format($noexperience_user_per, 2)."%" ?></td>
					<td class="tdr"><?= ($experience_5under_user_per == 0) ? "-" : number_format($experience_5under_user_per, 2)."%" ?></td>
					<td class="tdr"><?= ($noplay_user_per == 0) ? "-" : number_format($noplay_user_per, 2)."%" ?></td>
					
					<td class="tdr"><?= ($login_cnt_1_user_per == 0) ? "-" : number_format($login_cnt_1_user_per, 2)."%" ?></td>
					<td class="tdr"><?= ($login_cnt_2_user_per == 0) ? "-" : number_format($login_cnt_2_user_per, 2)."%" ?></td>
					<td class="tdr"><?= ($login_cnt_3_user_per == 0) ? "-" : number_format($login_cnt_3_user_per, 2)."%" ?></td>
					<td class="tdr"><?= ($login_cnt_10_user_per == 0) ? "-" : number_format($login_cnt_10_user_per, 2)."%" ?></td>
				</tr>
<?
			}
?>
            </tbody>
            </table>
        </div>
        
        </form>
</div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>