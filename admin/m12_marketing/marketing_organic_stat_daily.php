<?
	$top_menu = "marketing";
	$sub_menu = "marketing_organic_stat_daily";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$db_main2 = new CDatabase_Main2();
	
	$max_today = $db_main2->getvalue("SELECT MAX(today) FROM tbl_organic_stat_daily");
	
	$today = ($_GET["today"] == "") ? $today = $max_today : $_GET["today"];
	$startdate = ($_GET["startdate"] == "") ? date("Y-m-d", strtotime("-29 day")) : $_GET["startdate"];
	$enddate = ($_GET["enddate"] == "") ? $max_today : $_GET["enddate"];
	
	$chart_height = "170";
	
	// Web
	
	$sql = "SELECT * FROM `tbl_organic_stat_daily` WHERE today = '$today' AND platform = 0 ORDER BY subtype ASC";
	$web_organic_week_stats = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 1 AND platform = 0
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$web_organic_graph_stats_1week = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 2 AND platform = 0
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$web_organic_graph_stats_2week = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 3 AND platform = 0
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$web_organic_graph_stats_3week = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 4 AND platform = 0
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$web_organic_graph_stats_4week = $db_main2->gettotallist($sql);
	
	// iOS
	
	$sql = "SELECT * FROM `tbl_organic_stat_daily` WHERE today = '$today' AND platform = 1 ORDER BY subtype ASC";
	$ios_organic_week_stats = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 1 AND platform = 1
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$ios_organic_graph_stats_1week = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 2 AND platform = 1
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$ios_organic_graph_stats_2week = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 3 AND platform = 1
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$ios_organic_graph_stats_3week = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 4 AND platform = 1
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$ios_organic_graph_stats_4week = $db_main2->gettotallist($sql);
	
	// Android
	
	$sql = "SELECT * FROM `tbl_organic_stat_daily` WHERE today = '$today' AND platform = 2 ORDER BY subtype ASC";
	$and_organic_week_stats = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 1 AND platform = 2
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$and_organic_graph_stats_1week = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 2 AND platform = 2
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$and_organic_graph_stats_2week = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 3 AND platform = 2
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$and_organic_graph_stats_3week = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 4 AND platform = 2
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$and_organic_graph_stats_4week = $db_main2->gettotallist($sql);
	
	// Amazon
	
	$sql = "SELECT * FROM `tbl_organic_stat_daily` WHERE today = '$today' AND platform = 3 ORDER BY subtype ASC";
	$ama_organic_week_stats = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 1 AND platform = 3
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$ama_organic_graph_stats_1week = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 2 AND platform = 3
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$ama_organic_graph_stats_2week = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 3 AND platform = 3
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$ama_organic_graph_stats_3week = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, subtype, SUM(newuser_payer) AS newuser_payer, IFNULL(SUM(newuser_money) / SUM(newuser), 0) AS arpu
			FROM tbl_organic_stat_daily
			WHERE subtype = 4 AND platform = 3
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$ama_organic_graph_stats_4week = $db_main2->gettotallist($sql);
	
	
	
	$db_main2->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">	
	$(function() {
		$("#today").datepicker({});
		$("#startdate").datepicker({});
		$("#enddate").datepicker({});
	});

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
		
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.today.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.today.focus();
	        return;
	    }
	
	    search_form.submit();
	}

	google.load("visualization", "1", {packages:["corechart"]});

	function drawChart() 
	{
		var data1 = new google.visualization.DataTable();
        data1.addColumn('string', '날짜');
        data1.addColumn('number', '최근 1주');
        data1.addColumn('number', '최근 2주');
        data1.addColumn('number', '최근 3주');
        data1.addColumn('number', '최근 4주');
        data1.addRows([
<?
		    for ($i=0; $i<sizeof($web_organic_graph_stats_1week); $i++)
		    {
		        $_today = $web_organic_graph_stats_1week[$i]["today"];
		        $_newuser_payer_1week = $web_organic_graph_stats_1week[$i]["newuser_payer"];
		        $_newuser_payer_2week = $web_organic_graph_stats_2week[$i]["newuser_payer"];
		        $_newuser_payer_3week = $web_organic_graph_stats_3week[$i]["newuser_payer"];
		        $_newuser_payer_4week = $web_organic_graph_stats_4week[$i]["newuser_payer"];
		
		        echo("['".$_today."'");
		
				if ($_newuser_payer_1week != "")
					echo(",{v:".$_newuser_payer_1week.",f:'".number_format($_newuser_payer_1week)."'}");
				else
					echo(",0");
		
				if ($_newuser_payer_2week != "")
					echo(",{v:".$_newuser_payer_2week.",f:'".number_format($_newuser_payer_2week)."'}");
				else
					echo(",0");
		
				if ($_newuser_payer_3week != "")
					echo(",{v:".$_newuser_payer_3week.",f:'".number_format($_newuser_payer_3week)."'}");
				else
					echo(",0");
		
				if ($_newuser_payer_4week != "")
					echo(",{v:".$_newuser_payer_4week.",f:'".number_format($_newuser_payer_4week)."'}]");
				else
					echo(",0]");
		        
		        if ($i < sizeof($web_organic_graph_stats_1week))
		        	echo(",");
		        
		    }
?>
		]);
		
		var data2 = new google.visualization.DataTable();
        data2.addColumn('string', '날짜');
        data2.addColumn('number', '최근 1주');
        data2.addColumn('number', '최근 2주');
        data2.addColumn('number', '최근 3주');
        data2.addColumn('number', '최근 4주');
        data2.addRows([
<?
		    for ($i=0; $i<sizeof($ios_organic_graph_stats_1week); $i++)
		    {
		        $_today = $ios_organic_graph_stats_1week[$i]["today"];
		        $_newuser_payer_1week = $ios_organic_graph_stats_1week[$i]["newuser_payer"];
		        $_newuser_payer_2week = $ios_organic_graph_stats_2week[$i]["newuser_payer"];
		        $_newuser_payer_3week = $ios_organic_graph_stats_3week[$i]["newuser_payer"];
		        $_newuser_payer_4week = $ios_organic_graph_stats_4week[$i]["newuser_payer"];
		
		        echo("['".$_today."'");
		
				if ($_newuser_payer_1week != "")
					echo(",{v:".$_newuser_payer_1week.",f:'".number_format($_newuser_payer_1week)."'}");
				else
					echo(",0");
		
				if ($_newuser_payer_2week != "")
					echo(",{v:".$_newuser_payer_2week.",f:'".number_format($_newuser_payer_2week)."'}");
				else
					echo(",0");
		
				if ($_newuser_payer_3week != "")
					echo(",{v:".$_newuser_payer_3week.",f:'".number_format($_newuser_payer_3week)."'}");
				else
					echo(",0");
		
				if ($_newuser_payer_4week != "")
					echo(",{v:".$_newuser_payer_4week.",f:'".number_format($_newuser_payer_4week)."'}]");
				else
					echo(",0]");
		        
		        if ($i < sizeof($ios_organic_graph_stats_1week))
		        	echo(",");
		        
		    }
?>
		]);
		
		var data3 = new google.visualization.DataTable();
        data3.addColumn('string', '날짜');
        data3.addColumn('number', '최근 1주');
        data3.addColumn('number', '최근 2주');
        data3.addColumn('number', '최근 3주');
        data3.addColumn('number', '최근 4주');
        data3.addRows([
<?
		    for ($i=0; $i<sizeof($and_organic_graph_stats_1week); $i++)
		    {
		        $_today = $and_organic_graph_stats_1week[$i]["today"];
		        $_newuser_payer_1week = $and_organic_graph_stats_1week[$i]["newuser_payer"];
		        $_newuser_payer_2week = $and_organic_graph_stats_2week[$i]["newuser_payer"];
		        $_newuser_payer_3week = $and_organic_graph_stats_3week[$i]["newuser_payer"];
		        $_newuser_payer_4week = $and_organic_graph_stats_4week[$i]["newuser_payer"];
		
		        echo("['".$_today."'");
		
				if ($_newuser_payer_1week != "")
					echo(",{v:".$_newuser_payer_1week.",f:'".number_format($_newuser_payer_1week)."'}");
				else
					echo(",0");
		
				if ($_newuser_payer_2week != "")
					echo(",{v:".$_newuser_payer_2week.",f:'".number_format($_newuser_payer_2week)."'}");
				else
					echo(",0");
		
				if ($_newuser_payer_3week != "")
					echo(",{v:".$_newuser_payer_3week.",f:'".number_format($_newuser_payer_3week)."'}");
				else
					echo(",0");
		
				if ($_newuser_payer_4week != "")
					echo(",{v:".$_newuser_payer_4week.",f:'".number_format($_newuser_payer_4week)."'}]");
				else
					echo(",0]");
		        
		        if ($i < sizeof($and_organic_graph_stats_1week))
		        	echo(",");
		        
		    }
?>
		]);
		
		var data4 = new google.visualization.DataTable();
        data4.addColumn('string', '날짜');
        data4.addColumn('number', '최근 1주');
        data4.addColumn('number', '최근 2주');
        data4.addColumn('number', '최근 3주');
        data4.addColumn('number', '최근 4주');
        data4.addRows([
<?
		    for ($i=0; $i<sizeof($ama_organic_graph_stats_1week); $i++)
		    {
		        $_today = $ama_organic_graph_stats_1week[$i]["today"];
		        $_newuser_payer_1week = $ama_organic_graph_stats_1week[$i]["newuser_payer"];
		        $_newuser_payer_2week = $ama_organic_graph_stats_2week[$i]["newuser_payer"];
		        $_newuser_payer_3week = $ama_organic_graph_stats_3week[$i]["newuser_payer"];
		        $_newuser_payer_4week = $ama_organic_graph_stats_4week[$i]["newuser_payer"];
		
		        echo("['".$_today."'");
		
				if ($_newuser_payer_1week != "")
					echo(",{v:".$_newuser_payer_1week.",f:'".number_format($_newuser_payer_1week)."'}");
				else
					echo(",0");
		
				if ($_newuser_payer_2week != "")
					echo(",{v:".$_newuser_payer_2week.",f:'".number_format($_newuser_payer_2week)."'}");
				else
					echo(",0");
		
				if ($_newuser_payer_3week != "")
					echo(",{v:".$_newuser_payer_3week.",f:'".number_format($_newuser_payer_3week)."'}");
				else
					echo(",0");
		
				if ($_newuser_payer_4week != "")
					echo(",{v:".$_newuser_payer_4week.",f:'".number_format($_newuser_payer_4week)."'}]");
				else
					echo(",0]");
		        
		        if ($i < sizeof($web_organic_graph_stats_1week))
		        	echo(",");
		        
		    }
?>
		]);
		
		var data5 = new google.visualization.DataTable();
        data5.addColumn('string', '날짜');
        data5.addColumn('number', '최근 1주');
        data5.addColumn('number', '최근 2주');
        data5.addColumn('number', '최근 3주');
        data5.addColumn('number', '최근 4주');
        data5.addRows([
<?
		    for ($i=0; $i<sizeof($web_organic_graph_stats_1week); $i++)
		    {
		        $_today = $web_organic_graph_stats_1week[$i]["today"];
		        $_arpu_1week = $web_organic_graph_stats_1week[$i]["arpu"];
		        $_arpu_2week = $web_organic_graph_stats_2week[$i]["arpu"];
		        $_arpu_3week = $web_organic_graph_stats_3week[$i]["arpu"];
		        $_arpu_4week = $web_organic_graph_stats_4week[$i]["arpu"];
		
		        echo("['".$_today."'");
		
				if ($_arpu_1week != "")
					echo(",{v:".$_arpu_1week.",f:'".number_format($_arpu_1week, 2)."'}");
				else
					echo(",0");
		
				if ($_arpu_2week != "")
					echo(",{v:".$_arpu_2week.",f:'".number_format($_arpu_2week, 2)."'}");
				else
					echo(",0");
		
				if ($_arpu_3week != "")
					echo(",{v:".$_arpu_3week.",f:'".number_format($_arpu_3week, 2)."'}");
				else
					echo(",0");
		
				if ($_arpu_4week != "")
					echo(",{v:".$_arpu_4week.",f:'".number_format($_arpu_4week, 2)."'}]");
				else
					echo(",0]");
		        
		        if ($i < sizeof($web_organic_graph_stats_1week))
		        	echo(",");
		        
		    }
?>
		]);
		
		var data6 = new google.visualization.DataTable();
        data6.addColumn('string', '날짜');
        data6.addColumn('number', '최근 1주');
        data6.addColumn('number', '최근 2주');
        data6.addColumn('number', '최근 3주');
        data6.addColumn('number', '최근 4주');
        data6.addRows([
<?
		    for ($i=0; $i<sizeof($ios_organic_graph_stats_1week); $i++)
		    {
		        $_today = $ios_organic_graph_stats_1week[$i]["today"];
		        $_arpu_1week = $ios_organic_graph_stats_1week[$i]["arpu"];
		        $_arpu_2week = $ios_organic_graph_stats_2week[$i]["arpu"];
		        $_arpu_3week = $ios_organic_graph_stats_3week[$i]["arpu"];
		        $_arpu_4week = $ios_organic_graph_stats_4week[$i]["arpu"];
		
		        echo("['".$_today."'");
		
				if ($_arpu_1week != "")
					echo(",{v:".$_arpu_1week.",f:'".number_format($_arpu_1week, 2)."'}");
				else
					echo(",0");
		
				if ($_arpu_2week != "")
					echo(",{v:".$_arpu_2week.",f:'".number_format($_arpu_2week, 2)."'}");
				else
					echo(",0");
		
				if ($_arpu_3week != "")
					echo(",{v:".$_arpu_3week.",f:'".number_format($_arpu_3week, 2)."'}");
				else
					echo(",0");
		
				if ($_arpu_4week != "")
					echo(",{v:".$_arpu_4week.",f:'".number_format($_arpu_4week, 2)."'}]");
				else
					echo(",0]");
		        
		        if ($i < sizeof($ios_organic_graph_stats_1week))
		        	echo(",");
		        
		    }
?>
		]);
		
		var data7 = new google.visualization.DataTable();
        data7.addColumn('string', '날짜');
        data7.addColumn('number', '최근 1주');
        data7.addColumn('number', '최근 2주');
        data7.addColumn('number', '최근 3주');
        data7.addColumn('number', '최근 4주');
        data7.addRows([
<?
		    for ($i=0; $i<sizeof($and_organic_graph_stats_1week); $i++)
		    {
		        $_today = $and_organic_graph_stats_1week[$i]["today"];
		        $_arpu_1week = $and_organic_graph_stats_1week[$i]["arpu"];
		        $_arpu_2week = $and_organic_graph_stats_2week[$i]["arpu"];
		        $_arpu_3week = $and_organic_graph_stats_3week[$i]["arpu"];
		        $_arpu_4week = $and_organic_graph_stats_4week[$i]["arpu"];
		
		        echo("['".$_today."'");
		
				if ($_arpu_1week != "")
					echo(",{v:".$_arpu_1week.",f:'".number_format($_arpu_1week, 2)."'}");
				else
					echo(",0");
		
				if ($_arpu_2week != "")
					echo(",{v:".$_arpu_2week.",f:'".number_format($_arpu_2week, 2)."'}");
				else
					echo(",0");
		
				if ($_arpu_3week != "")
					echo(",{v:".$_arpu_3week.",f:'".number_format($_arpu_3week, 2)."'}");
				else
					echo(",0");
		
				if ($_arpu_4week != "")
					echo(",{v:".$_arpu_4week.",f:'".number_format($_arpu_4week, 2)."'}]");
				else
					echo(",0]");
		        
		        if ($i < sizeof($and_organic_graph_stats_1week))
		        	echo(",");
		        
		    }
?>
		]);
		
		var data8 = new google.visualization.DataTable();
        data8.addColumn('string', '날짜');
        data8.addColumn('number', '최근 1주');
        data8.addColumn('number', '최근 2주');
        data8.addColumn('number', '최근 3주');
        data8.addColumn('number', '최근 4주');
        data8.addRows([
<?
		    for ($i=0; $i<sizeof($ama_organic_graph_stats_1week); $i++)
		    {
		        $_today = $ama_organic_graph_stats_1week[$i]["today"];
		        $_arpu_1week = $ama_organic_graph_stats_1week[$i]["arpu"];
		        $_arpu_2week = $ama_organic_graph_stats_2week[$i]["arpu"];
		        $_arpu_3week = $ama_organic_graph_stats_3week[$i]["arpu"];
		        $_arpu_4week = $ama_organic_graph_stats_4week[$i]["arpu"];
		
		        echo("['".$_today."'");
		
				if ($_arpu_1week != "")
					echo(",{v:".$_arpu_1week.",f:'".number_format($_arpu_1week, 2)."'}");
				else
					echo(",0");
		
				if ($_arpu_2week != "")
					echo(",{v:".$_arpu_2week.",f:'".number_format($_arpu_2week, 2)."'}");
				else
					echo(",0");
		
				if ($_arpu_3week != "")
					echo(",{v:".$_arpu_3week.",f:'".number_format($_arpu_3week, 2)."'}");
				else
					echo(",0");
		
				if ($_arpu_4week != "")
					echo(",{v:".$_arpu_4week.",f:'".number_format($_arpu_4week, 2)."'}]");
				else
					echo(",0]");
		        
		        if ($i < sizeof($web_organic_graph_stats_1week))
		        	echo(",");
		        
		    }
?>
		]);

        var options = {
    	        title:'',                                                
    	        width:650,
    	        height:170,
    	        axisTitlesPosition:'in',
    	        curveType:'none',
    	        focusTarget:'category',
    	        interpolateNulls:'true',
    	        legend:'top',
    	        fontSize : 12,
    	        tickValues:0,
    	        chartArea:{left:100,top:20,width:650,height:100}
    	    };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
	    chart.draw(data1, options);
	    
        var chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
	    chart.draw(data2, options);
	    
        var chart = new google.visualization.LineChart(document.getElementById('chart_div3'));
	    chart.draw(data3, options);
	    
        var chart = new google.visualization.LineChart(document.getElementById('chart_div4'));
	    chart.draw(data4, options);

        var chart = new google.visualization.LineChart(document.getElementById('chart_div5'));
	    chart.draw(data5, options);
	    
        var chart = new google.visualization.LineChart(document.getElementById('chart_div6'));
	    chart.draw(data6, options);
	    
        var chart = new google.visualization.LineChart(document.getElementById('chart_div7'));
	    chart.draw(data7, options);
	    
        var chart = new google.visualization.LineChart(document.getElementById('chart_div8'));
	    chart.draw(data8, options);
	}

	google.setOnLoadCallback(drawChart);
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap" style="width:1270px;">
		<div class="title"><?= $top_menu_txt ?> &gt; Organic 일일 현황 </div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<div style="float: right; margin-right: 2px">
				날짜 : <input type="text" class="search_text" id="today" name="today" value="<?= $today ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" />
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
				</div>
				<br/>
				
				<div style="float: right; margin-right: 2px">
					그래프 날짜 : &nbsp;&nbsp;
					<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" />~
					<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" />
					<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
				</div>
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	
	<div style="width:1300px;">
	
	<div style="min-width:620px;float:left;margin-top:15px;margin-left:5px;margin-right:20px;"><font style="font:12px;color:#000;font-weight:bold;cursor:ponter;">Web</font></div>
    <div style="min-width:620px;float:left;margin-top:15px;margin-left:35px;"><font style="font:12px;color:#000;font-weight:bold;cursor:ponter;">iOS</font></div>
	
	<!-- Web -->
	<table class="tbl_list_basic1" style="width:620px; float:left; margin-left:5px;margin-right:20px;">
		<colgroup>
			<col width="100">
			<col width="70">
			<col width="90">
			<col width="80">
			<col width="80">
            <col width="100">
            <col width="80">
            <col width="80">
        </colgroup>
        
        <thead>
        	<tr>
        		<th class="tdc">기간</th>
            	<th class="tdr">가입자수</th>
            	<th class="tdc">미게임율</th>
            	<th class="tdr">결제자수</th>
            	<th class="tdr">PUR</th>
             	<th class="tdr">매출</th>
               	<th class="tdr">ARPU</th>
               	<th class="tdr">ARPPU</th>
         	</tr>
        </thead>
        
        <tbody>
<?
	for($i=0; $i<sizeof($web_organic_week_stats); $i++)
	{
		$subtype = $web_organic_week_stats[$i]["subtype"];
		$platform = $web_organic_week_stats[$i]["platform"];
		$newuser = $web_organic_week_stats[$i]["newuser"];
		$newuser_noplay = $web_organic_week_stats[$i]["newuser_noplay"];
		$noplay_rate = ($newuser == 0) ? 0 : number_format(($newuser_noplay / $newuser) * 100, 2);
		$newuser_payer = $web_organic_week_stats[$i]["newuser_payer"];
		$newuser_money = $web_organic_week_stats[$i]["newuser_money"];
		$pur = ($newuser_payer == 0) ? 0 : number_format(($newuser_payer / $newuser) * 100, 2);
		
		$arpu = ($newuser == 0) ? 0 : $newuser_money / $newuser;
		$arppu = ($newuser_payer == 0) ? 0 : $newuser_money / $newuser_payer;
		
?>
			<tr>
				<td class="tdc point">최근 <?= $subtype ?>주</td>
				<td class="tdr"><?= number_format($newuser) ?></td>
				<td class="tdc"><?= $noplay_rate ?> %</td>
				<td class="tdr"><?= number_format($newuser_payer) ?></td>
				<td class="tdr"><?= $pur ?> %</td>
				<td class="tdr">$ <?= number_format($newuser_money) ?></td>
				<td class="tdr"><?= number_format($arpu, 2) ?></td>
				<td class="tdr"><?= number_format($arppu, 2) ?></td>
			</tr>
<?
	}
?>
        </tbody>
	</table>
	
	<!-- iOS -->
	<table class="tbl_list_basic1" style="width:620px; float:left; margin-left:5px;">
		<colgroup>
			<col width="100">
			<col width="70">
			<col width="90">
			<col width="80">
			<col width="80">
            <col width="100">
            <col width="80">
            <col width="80">
        </colgroup>
        
        <thead>
        	<tr>
        		<th class="tdc">기간</th>
            	<th class="tdr">가입자수</th>
            	<th class="tdc">미게임율</th>
            	<th class="tdr">결제자수</th>
            	<th class="tdr">PUR</th>
             	<th class="tdr">매출</th>
               	<th class="tdr">ARPU</th>
               	<th class="tdr">ARPPU</th>
         	</tr>
        </thead>
        
        <tbody>
<?
	for($i=0; $i<sizeof($ios_organic_week_stats); $i++)
	{
		$subtype = $ios_organic_week_stats[$i]["subtype"];
		$platform = $ios_organic_week_stats[$i]["platform"];
		$newuser = $ios_organic_week_stats[$i]["newuser"];
		$newuser_noplay = $ios_organic_week_stats[$i]["newuser_noplay"];
		$noplay_rate = ($newuser == 0) ? 0 : number_format(($newuser_noplay / $newuser) * 100, 2);
		$newuser_payer = $ios_organic_week_stats[$i]["newuser_payer"];
		$newuser_money = $ios_organic_week_stats[$i]["newuser_money"];
		$pur = ($newuser_payer == 0) ? 0 : number_format(($newuser_payer / $newuser) * 100, 2);
		
		$arpu = ($newuser == 0) ? 0 : $newuser_money / $newuser;
		$arppu = ($newuser_payer == 0) ? 0 : $newuser_money / $newuser_payer;
		
?>
			<tr>
				<td class="tdc point">최근 <?= $subtype ?>주</td>
				<td class="tdr"><?= number_format($newuser) ?></td>
				<td class="tdc"><?= $noplay_rate ?> %</td>
				<td class="tdr"><?= number_format($newuser_payer) ?></td>
				<td class="tdr"><?= $pur ?> %</td>
				<td class="tdr">$ <?= number_format($newuser_money) ?></td>
				<td class="tdr"><?= number_format($arpu, 2) ?></td>
				<td class="tdr"><?= number_format($arppu, 2) ?></td>
			</tr>
<?
	}
?>
        </tbody>
	</table>
	
	<div style="min-width:620px;float:left;margin-top:20px;margin-left:5px;margin-right:20px;"><font style="font:12px;color:#000;font-weight:bold;cursor:ponter;">Android</font></div>
    <div style="min-width:620px;float:left;margin-top:20px;margin-left:35px;"><font style="font:12px;color:#000;font-weight:bold;cursor:ponter;">Amazon</font></div>
	
	<!-- Android -->
	<table class="tbl_list_basic1" style="width:620px; float:left; margin-left:5px;margin-right:20px;">
		<colgroup>
			<col width="100">
			<col width="70">
			<col width="90">
			<col width="80">
			<col width="80">
            <col width="100">
            <col width="80">
            <col width="80">
        </colgroup>
        
        <thead>
        	<tr>
        		<th class="tdc">기간</th>
            	<th class="tdr">가입자수</th>
            	<th class="tdc">미게임율</th>
            	<th class="tdr">결제자수</th>
            	<th class="tdr">PUR</th>
             	<th class="tdr">매출</th>
               	<th class="tdr">ARPU</th>
               	<th class="tdr">ARPPU</th>
         	</tr>
        </thead>
        
        <tbody>
<?
	for($i=0; $i<sizeof($and_organic_week_stats); $i++)
	{
		$subtype = $and_organic_week_stats[$i]["subtype"];
		$platform = $and_organic_week_stats[$i]["platform"];
		$newuser = $and_organic_week_stats[$i]["newuser"];
		$newuser_noplay = $and_organic_week_stats[$i]["newuser_noplay"];
		$noplay_rate = ($newuser == 0) ? 0 : number_format(($newuser_noplay / $newuser) * 100, 2);
		$newuser_payer = $and_organic_week_stats[$i]["newuser_payer"];
		$newuser_money = $and_organic_week_stats[$i]["newuser_money"];
		$pur = ($newuser_payer == 0) ? 0 : number_format(($newuser_payer / $newuser) * 100, 2);
		
		$arpu = ($newuser == 0) ? 0 : $newuser_money / $newuser;
		$arppu = ($newuser_payer == 0) ? 0 : $newuser_money / $newuser_payer;
		
?>
			<tr>
				<td class="tdc point">최근 <?= $subtype ?>주</td>
				<td class="tdr"><?= number_format($newuser) ?></td>
				<td class="tdc"><?= $noplay_rate ?> %</td>
				<td class="tdr"><?= number_format($newuser_payer) ?></td>
				<td class="tdr"><?= $pur ?> %</td>
				<td class="tdr">$ <?= number_format($newuser_money) ?></td>
				<td class="tdr"><?= number_format($arpu, 2) ?></td>
				<td class="tdr"><?= number_format($arppu, 2) ?></td>
			</tr>
<?
	}
?>
        </tbody>
	</table>
	
	<!-- Amazon -->
	<table class="tbl_list_basic1" style="width:620px; float:left; margin-left:5px;">
		<colgroup>
			<col width="100">
			<col width="70">
			<col width="90">
			<col width="80">
			<col width="80">
            <col width="100">
            <col width="80">
            <col width="80">
        </colgroup>
        
        <thead>
        	<tr>
        		<th class="tdc">기간</th>
            	<th class="tdr">가입자수</th>
            	<th class="tdc">미게임율</th>
            	<th class="tdr">결제자수</th>
            	<th class="tdr">PUR</th>
             	<th class="tdr">매출</th>
               	<th class="tdr">ARPU</th>
               	<th class="tdr">ARPPU</th>
         	</tr>
        </thead>
        
        <tbody>
<?
	for($i=0; $i<sizeof($ama_organic_week_stats); $i++)
	{
		$subtype = $ama_organic_week_stats[$i]["subtype"];
		$platform = $ama_organic_week_stats[$i]["platform"];
		$newuser = $ama_organic_week_stats[$i]["newuser"];
		$newuser_noplay = $ama_organic_week_stats[$i]["newuser_noplay"];
		$noplay_rate = ($newuser == 0) ? 0 : number_format(($newuser_noplay / $newuser) * 100, 2);
		$newuser_payer = $ama_organic_week_stats[$i]["newuser_payer"];
		$newuser_money = $ama_organic_week_stats[$i]["newuser_money"];
		$pur = ($newuser_payer == 0) ? 0 : number_format(($newuser_payer / $newuser) * 100, 2);
		
		$arpu = ($newuser == 0) ? 0 : $newuser_money / $newuser;
		$arppu = ($newuser_payer == 0) ? 0 : $newuser_money / $newuser_payer;
		
?>
			<tr>
				<td class="tdc point">최근 <?= $subtype ?>주</td>
				<td class="tdr"><?= number_format($newuser) ?></td>
				<td class="tdc"><?= $noplay_rate ?> %</td>
				<td class="tdr"><?= number_format($newuser_payer) ?></td>
				<td class="tdr"><?= $pur ?> %</td>
				<td class="tdr">$ <?= number_format($newuser_money) ?></td>
				<td class="tdr"><?= number_format($arpu, 2) ?></td>
				<td class="tdr"><?= number_format($arppu, 2) ?></td>
			</tr>
<?
	}
?>
        </tbody>
	</table>
	
	<div style="min-width:900px;height:20px;"/>
	<div style="min-width:900px;"/>
	<div style="min-width:900px;"/>
	
	<div style="min-width:700px;float:left;font:bold 15px 'Malgun Gothic', dotum; color:#888; margin-top:25px; margin-bottom:10px;" name="div_normal">
		결제자수
	</div>
	<div style="min-width:500px;float:left;font:bold 15px 'Malgun Gothic', dotum; color:#888; margin-top:25px; margin-bottom:10px;" name="div_normal">
		ARPU
	</div>
	
	<div style="min-width:1300px;margin-left:5px;float:left;"><font style="font:12px;color:#000;font-weight:bold;cursor:ponter;">Web</font></div>
	<div id="chart_div1" name="div_normal" style="margin-bottom:10px;height:<?= $chart_height ?>px; min-width: 620px;float:left;"></div>
	<div id="chart_div5" name="div_normal" style="margin-bottom:10px;height:<?= $chart_height ?>px; min-width: 620px;float:left;"></div>
	
	
	
    <div style="min-width:1300px;margin-left:5px;float:left;"><font style="font:12px;color:#000;font-weight:bold;cursor:ponter;">iOS</font></div>
    <div id="chart_div2" name="div_normal" style="height:<?= $chart_height ?>px; min-width: 620px;float:left;"></div>
    <div id="chart_div6" name="div_normal" style="height:<?= $chart_height ?>px; min-width: 620px;float:left;"></div>
    
    <div style="min-width:1300px;margin-left:5px;float:left;"><font style="font:12px;color:#000;font-weight:bold;cursor:ponter;">Android</font></div>
    <div id="chart_div3" name="div_normal" style="height:<?= $chart_height ?>px; min-width: 620px;float:left;"></div>
    <div id="chart_div7" name="div_normal" style="height:<?= $chart_height ?>px; min-width: 620px;float:left;"></div>
    
    <div style="min-width:1300px;margin-left:5px;float:left;"><font style="font:12px;color:#000;font-weight:bold;cursor:ponter;">Amazon</font></div>
    <div id="chart_div4" name="div_normal" style="height:<?= $chart_height ?>px; min-width: 620px;float:left;"></div>
    <div id="chart_div8" name="div_normal" style="height:<?= $chart_height ?>px; min-width: 620px;float:left;"></div>
    </div>
</div>
        
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>