<?
	$top_menu = "marketing";
	$sub_menu = "marketing_newuser_stat_daily";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$db_main2 = new CDatabase_Main2();
	
	$max_today = $db_main2->getvalue("SELECT MAX(today) FROM tbl_marketing_stat_daily");
	
	$today = ($_GET["today"] == "") ? $max_today : $_GET["today"];
	$startdate = ($_GET["startdate"] == "") ? date("Y-m-d", strtotime("-29 day")) : $_GET["startdate"];
	$enddate = ($_GET["enddate"] == "") ? $max_today : $_GET["enddate"];
	
	$appsflyer_startdate = ($_GET["appsflyer_startdate"] == "") ? date("Y-m-d", strtotime("-14 day")) : $_GET["appsflyer_startdate"];
	$appsflyer_enddate = ($_GET["appsflyer_enddate"] == "") ? $max_today : $_GET["appsflyer_enddate"];
	
	$t5_startdate = ($_GET["t5_startdate"] == "") ? date("Y-m-d", strtotime("-14 day")) : $_GET["t5_startdate"];
	$t5_enddate = ($_GET["t5_enddate"] == "") ? $max_today : $_GET["t5_enddate"];
	
	$term = ($_GET["term"] == "") ? "0" : $_GET["term"];
	$week_term = ($_GET["week_term"] == "") ? "1" : $_GET["week_term"];
	$search_adflag = ($_GET["adflag"] == "") ? "" : $_GET["adflag"];
	
	$user_type = $_GET["user_type"];
	
	if ($term != "ALL" && $term != "0" && $term != "1" && $term != "2" && $term != "3")
		error_back("잘못된 접근입니다.");
	
	$platform = $term;
	
	if($search_adflag != "")
		$adflag_tail = " AND REPLACE(adflag, '_viral', '') = '$search_adflag' ";
		
	$sql = "SELECT today, adflag, type, subtype, platform, SUM(spend) AS spend,
			SUM(newuser) AS newuser, SUM(newuser_payer) AS newuser_payer, SUM(newuser_money) AS newuser_money, SUM(newuser_noplay) AS newuser_noplay,
			SUM(reuser) AS reuser, AVG(reuser_leavedays) AS reuser_leavedays, SUM(reuser_payer) AS reuser_payer, AVG(reuser_payer_leavedays) AS reuser_payer_leavedays, SUM(reuser_money) AS reuser_money
			FROM tbl_marketing_stat_daily
			WHERE today = '$today' AND type = 1 AND platform = $platform $adflag_tail
			GROUP BY subtype
			ORDER BY subtype ASC";
	$newuser_week_stats = $db_main2->gettotallist($sql);
	
	$sql = "SELECT DISTINCT REPLACE(adflag, '_viral', '') AS adflag
			FROM tbl_marketing_stat_daily
			WHERE today = '$today' AND type = 1 AND platform = $platform
			ORDER BY adflag ASC";
	$adflag_list = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, TYPE, subtype, SUM(spend) AS spend, SUM(newuser) AS newuser, ROUND(((IFNULL(SUM(newuser_money), 0) + IFNULL(SUM(reuser_money), 0)) / SUM(spend)) * 100, 2) AS roi,
			SUM(newuser_money) AS newuser_money, IFNULL(SUM(spend) / SUM(newuser), 0) AS cpi
			FROM tbl_marketing_stat_daily
			WHERE TYPE = 1 AND subtype = 1 AND platform = $platform $adflag_tail
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$newuser_graph_stats_1week = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, TYPE, subtype, SUM(spend) AS spend, SUM(newuser) AS newuser, ROUND(((IFNULL(SUM(newuser_money), 0) + IFNULL(SUM(reuser_money), 0)) / SUM(spend)) * 100, 2) AS roi,
			SUM(newuser_money) AS newuser_money, IFNULL(SUM(spend) / SUM(newuser), 0) AS cpi
			FROM tbl_marketing_stat_daily
			WHERE TYPE = 1 AND subtype = 2 AND platform = $platform $adflag_tail
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$newuser_graph_stats_2week = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, TYPE, subtype, SUM(spend) AS spend, SUM(newuser) AS newuser, ROUND(((IFNULL(SUM(newuser_money), 0) + IFNULL(SUM(reuser_money), 0)) / SUM(spend)) * 100, 2) AS roi,
			SUM(newuser_money) AS newuser_money, IFNULL(SUM(spend) / SUM(newuser), 0) AS cpi
			FROM tbl_marketing_stat_daily
			WHERE TYPE = 1 AND subtype = 3 AND platform = $platform $adflag_tail
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$newuser_graph_stats_3week = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, TYPE, subtype, SUM(spend) AS spend, SUM(newuser) AS newuser, ROUND(((IFNULL(SUM(newuser_money), 0) + IFNULL(SUM(reuser_money), 0)) / SUM(spend)) * 100, 2) AS roi,
			SUM(newuser_money) AS newuser_money, IFNULL(SUM(spend) / SUM(newuser), 0) AS cpi
			FROM tbl_marketing_stat_daily
			WHERE TYPE = 1 AND subtype = 4 AND platform = $platform $adflag_tail
			AND today BETWEEN '$startdate' AND '$enddate'
			GROUP BY today";
	$newuser_graph_stats_4week = $db_main2->gettotallist($sql);
	
	$date_pointer = $enddate;
	
	for($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
	{
		$date_list[$i] = $date_pointer;
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	$db_main2->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	function tab_change(tab)
	{
		var search_form = document.search_form;
		search_form.tab.value = tab;
		search_form.submit();
	}
	
	function change_term(term)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("term_web");
		var ios = document.getElementById("term_ios");
		var android = document.getElementById("term_android");
		var amazon = document.getElementById("term_amazon");
		
		search_form.term.value = term;
		search_form.adflag.value = '';
		
		if (term == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (term == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
	
	function change_week_term(term)
	{
		var search_form = document.search_form;
		
		var term_1 = document.getElementById("term_1");
		var term_2 = document.getElementById("term_2");
		var term_3 = document.getElementById("term_3");
		var term_4 = document.getElementById("term_4");
		
		search_form.week_term.value = term;
		
		if (term == "1")
		{
			term_1.className="btn_schedule_select";
			term_2.className="btn_schedule";
			term_3.className="btn_schedule";
			term_4.className="btn_schedule";
		}
		else if (term == "2")
		{
			term_1.className="btn_schedule";
			term_2.className="btn_schedule_select";
			term_3.className="btn_schedule";
			term_4.className="btn_schedule";
		}
		else if (term == "3")
		{
			term_1.className="btn_schedule";
			term_2.className="btn_schedule";
			term_3.className="btn_schedule_select";
			term_4.className="btn_schedule";
		}
		else if (term == "4")
		{
			term_1.className="btn_schedule";
			term_2.className="btn_schedule";
			term_3.className="btn_schedule";
			term_4.className="btn_schedule_select";
		}
	
		search_form.submit();
	}

	$(function() {
		$("#today").datepicker({});
		$("#startdate").datepicker({});
		$("#enddate").datepicker({});
		$("#appsflyer_startdate").datepicker({});
		$("#appsflyer_enddate").datepicker({});
		$("#t5_startdate").datepicker({});
		$("#t5_enddate").datepicker({});
	});

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
		
	function search()
	{
	    var search_form = document.search_form;
	    
	    var appsflyer_startdate = document.getElementById("appsflyer_startdate");
	    var appsflyer_enddate = document.getElementById("appsflyer_enddate");
	    
	    var t5_startdate = document.getElementById("t5_startdate");
	    var t5_enddate = document.getElementById("t5_enddate");
	    
	    var user_type = document.getElementById("user_type");
	    
	    search_form.appendChild(appsflyer_startdate);
	    search_form.appendChild(appsflyer_enddate);
	    
	    search_form.appendChild(t5_startdate);
	    search_form.appendChild(t5_enddate);

	    search_form.appendChild(user_type);

	    if (search_form.today.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.today.focus();
	        return;
	    }

	    if (search_form.appsflyer_startdate.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.appsflyer_startdate.focus();
	        return;
	    }

	    if (search_form.appsflyer_enddate.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.appsflyer_enddate.focus();
	        return;
	    }

	    if (search_form.t5_startdate.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.t5_startdate.focus();
	        return;
	    }

	    if (search_form.t5_enddate.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.t5_enddate.focus();
	        return;
	    }

	    search_form.submit();
	}

	function pop_newuser_stat(today, term, subtype, adflag)
	{
		window.open('/m12_marketing/pop_marketing_newuser_stat_daily.php?today='+today+'&term='+term+'&subtype='+subtype, '', 'width=1450,height=800,toolbar=no,menubar=no,scrollbars=yes,resizable=no');
	}
	
	function excelDownload(data_type, download_type)
	{
		
		var appsflyer_startdate = document.getElementById("appsflyer_startdate").value;
	    var appsflyer_enddate = document.getElementById("appsflyer_enddate").value;
	    
		var t5_startdate = document.getElementById("t5_startdate").value;
	    var t5_enddate = document.getElementById("t5_enddate").value;

	    var user_type = document.getElementById("user_type").value;
	    
		if(data_type == 1)
			window.location.href = 'marketing_newuser_stat_daily_excel.php?data_type=' + data_type + '&download_type=' + download_type + '&user_type=' + user_type + '&startdate=<?= $startdate ?>&enddate=<?= $enddate ?>&appsflyer_startdate=' + appsflyer_startdate + '&appsflyer_enddate=' + appsflyer_enddate + '&platform=<?= $platform ?>&adflag=<?= $search_adflag ?>&subtype=<?= $subtype ?>'
		else
			window.location.href = 'marketing_newuser_stat_daily_excel.php?data_type=' + data_type + '&download_type=' + download_type + '&user_type=' + user_type + '&startdate=<?= $startdate ?>&enddate=<?= $enddate ?>&t5_startdate=' + t5_startdate + '&t5_enddate=' + t5_enddate + '&platform=<?= $platform ?>&adflag=<?= $search_adflag ?>&subtype=<?= $subtype ?>'
	}

	google.load("visualization", "1", {packages:["corechart"]});

	function drawChart() 
	{
		var data1 = new google.visualization.DataTable();
        data1.addColumn('string', '날짜');
        data1.addColumn('number', '최근 1주');
        data1.addColumn('number', '최근 2주');
        data1.addColumn('number', '최근 3주');
        data1.addColumn('number', '최근 4주');
        data1.addRows([
<?
		    $index1 = 0;
		    $index2 = 0;
		    $index3 = 0;
		    $index4 = 0;
		    
		    for ($j=sizeof($date_list); $j>0; $j--)
		    {
		    	$_date = $date_list[$j-1];
		    	
		    	$is_graph_1week = 0;
		    	$is_graph_2week = 0;
		    	$is_graph_3week = 0;
		    	$is_graph_4week = 0;
		    	
		        $_today1 = $newuser_graph_stats_1week[$index1]["today"];
		        $_today2 = $newuser_graph_stats_2week[$index2]["today"];
		        $_today3 = $newuser_graph_stats_3week[$index3]["today"];
		        $_today4 = $newuser_graph_stats_4week[$index4]["today"];
		        
		        if($_date == $_today1)
		        	$is_graph_1week = 1;
		        if($_date == $_today2)
		        	$is_graph_2week = 1;
		        if($_date == $_today3)
		        	$is_graph_3week = 1;
		        if($_date == $_today4)
		        	$is_graph_4week = 1;
		        	
		        $_spend_1week = ($is_graph_1week == 1) ? $newuser_graph_stats_1week[$index1++]["spend"] : "0";
		        $_spend_2week = ($is_graph_2week == 1) ? $newuser_graph_stats_2week[$index2++]["spend"] : "0";
		        $_spend_3week = ($is_graph_3week == 1) ? $newuser_graph_stats_3week[$index3++]["spend"] : "0";
		        $_spend_4week = ($is_graph_4week == 1) ? $newuser_graph_stats_4week[$index4++]["spend"] : "0";
		
		        echo("['".$_date."'");
		
				if ($_spend_1week != "")
					echo(",{v:".$_spend_1week.",f:'".number_format($_spend_1week, 2)."'}");
				else
					echo(",0");
		
				if ($_spend_2week != "")
					echo(",{v:".$_spend_2week.",f:'".number_format($_spend_2week, 2)."'}");
				else
					echo(",0");
		
				if ($_spend_3week != "")
					echo(",{v:".$_spend_3week.",f:'".number_format($_spend_3week, 2)."'}");
				else
					echo(",0");
		
				if ($_spend_4week != "")
					echo(",{v:".$_spend_4week.",f:'".number_format($_spend_4week, 2)."'}]");
				else
					echo(",0]");
		        
		        if ($j > 1)
					echo(",");
		        
		    }
?>
		]);
		
		var data2 = new google.visualization.DataTable();
        data2.addColumn('string', '날짜');
        data2.addColumn('number', '최근 1주');
        data2.addColumn('number', '최근 2주');
        data2.addColumn('number', '최근 3주');
        data2.addColumn('number', '최근 4주');
        data2.addRows([
<?
		    $index1 = 0;
		    $index2 = 0;
		    $index3 = 0;
		    $index4 = 0;
		    
		    for ($j=sizeof($date_list); $j>0; $j--)
		    {
		    	$_date = $date_list[$j-1];
		    	
		    	$is_graph_1week = 0;
		    	$is_graph_2week = 0;
		    	$is_graph_3week = 0;
		    	$is_graph_4week = 0;
		    	
		        $_today1 = $newuser_graph_stats_1week[$index1]["today"];
		        $_today2 = $newuser_graph_stats_2week[$index2]["today"];
		        $_today3 = $newuser_graph_stats_3week[$index3]["today"];
		        $_today4 = $newuser_graph_stats_4week[$index4]["today"];
		        
		        if($_date == $_today1)
		        	$is_graph_1week = 1;
		        if($_date == $_today2)
		        	$is_graph_2week = 1;
		        if($_date == $_today3)
		        	$is_graph_3week = 1;
		        if($_date == $_today4)
		        	$is_graph_4week = 1;
		        	
		        $_newuser_week1 = ($is_graph_1week == 1) ? $newuser_graph_stats_1week[$index1++]["newuser"] : "0";
		        $_newuser_week2 = ($is_graph_2week == 1) ? $newuser_graph_stats_2week[$index2++]["newuser"] : "0";
		        $_newuser_week3 = ($is_graph_3week == 1) ? $newuser_graph_stats_3week[$index3++]["newuser"] : "0";
		        $_newuser_week4 = ($is_graph_4week == 1) ? $newuser_graph_stats_4week[$index4++]["newuser"] : "0";
		        
		        echo("['".$_date."'");
		
				if ($_newuser_week1 != "")
					echo(",{v:".$_newuser_week1.",f:'".number_format($_newuser_week1)."'}");
				else
					echo(",0");
		
				if ($_newuser_week2 != "")
					echo(",{v:".$_newuser_week2.",f:'".number_format($_newuser_week2)."'}");
				else
					echo(",0");
		
				if ($_newuser_week3 != "")
					echo(",{v:".$_newuser_week3.",f:'".number_format($_newuser_week3)."'}");
				else
					echo(",0");
		
				if ($_newuser_week4 != "")
					echo(",{v:".$_newuser_week4.",f:'".number_format($_newuser_week4)."'}]");
				else
					echo(",0]");
		        
		        if ($j > 1)
					echo(",");
		        
		    }
?>
		]);
		
		var data3 = new google.visualization.DataTable();
        data3.addColumn('string', '날짜');
        data3.addColumn('number', '최근 1주');
        data3.addColumn('number', '최근 2주');
        data3.addColumn('number', '최근 3주');
        data3.addColumn('number', '최근 4주');
        data3.addRows([
<?
		    $index1 = 0;
		    $index2 = 0;
		    $index3 = 0;
		    $index4 = 0;
		    
		    for ($j=sizeof($date_list); $j>0; $j--)
		    {
		    	$_date = $date_list[$j-1];
		    	
		    	$is_graph_1week = 0;
		    	$is_graph_2week = 0;
		    	$is_graph_3week = 0;
		    	$is_graph_4week = 0;
		    	
		        $_today1 = $newuser_graph_stats_1week[$index1]["today"];
		        $_today2 = $newuser_graph_stats_2week[$index2]["today"];
		        $_today3 = $newuser_graph_stats_3week[$index3]["today"];
		        $_today4 = $newuser_graph_stats_4week[$index4]["today"];
		        
		        if($_date == $_today1)
		        	$is_graph_1week = 1;
		        if($_date == $_today2)
		        	$is_graph_2week = 1;
		        if($_date == $_today3)
		        	$is_graph_3week = 1;
		        if($_date == $_today4)
		        	$is_graph_4week = 1;
		        	
		        $_roi_1week = ($is_graph_1week == 1) ? $newuser_graph_stats_1week[$index1++]["roi"] : "0";
		        $_roi_2week = ($is_graph_2week == 1) ? $newuser_graph_stats_2week[$index2++]["roi"] : "0";
		        $_roi_3week = ($is_graph_3week == 1) ? $newuser_graph_stats_3week[$index3++]["roi"] : "0";
		        $_roi_4week = ($is_graph_4week == 1) ? $newuser_graph_stats_4week[$index4++]["roi"] : "0";
		        
		        echo("['".$_date."'");
		        
				if ($_roi_1week != "")
					echo(",{v:".$_roi_1week.",f:'".number_format($_roi_1week, 2)."'}");
				else
					echo(",0");
		
				if ($_roi_2week != "")
					echo(",{v:".$_roi_2week.",f:'".number_format($_roi_2week, 2)."'}");
				else
					echo(",0");
		
				if ($_roi_3week != "")
					echo(",{v:".$_roi_3week.",f:'".number_format($_roi_3week, 2)."'}");
				else
					echo(",0");
		
				if ($_roi_4week != "")
					echo(",{v:".$_roi_4week.",f:'".number_format($_roi_4week, 2)."'}]");
				else
					echo(",0]");
		        
		        if ($j > 1)
					echo(",");
		        
		    }
?>
		]);
		
		var data4 = new google.visualization.DataTable();
        data4.addColumn('string', '날짜');
        data4.addColumn('number', '최근 1주');
        data4.addColumn('number', '최근 2주');
        data4.addColumn('number', '최근 3주');
        data4.addColumn('number', '최근 4주');
        data4.addRows([
<?
		$index1 = 0;
		    $index2 = 0;
		    $index3 = 0;
		    $index4 = 0;
		    
		    for ($j=sizeof($date_list); $j>0; $j--)
		    {
		    	$_date = $date_list[$j-1];
		    	
		    	$is_graph_1week = 0;
		    	$is_graph_2week = 0;
		    	$is_graph_3week = 0;
		    	$is_graph_4week = 0;
		    	
		        $_today1 = $newuser_graph_stats_1week[$index1]["today"];
		        $_today2 = $newuser_graph_stats_2week[$index2]["today"];
		        $_today3 = $newuser_graph_stats_3week[$index3]["today"];
		        $_today4 = $newuser_graph_stats_4week[$index4]["today"];
		        
		        if($_date == $_today1)
		        	$is_graph_1week = 1;
		        if($_date == $_today2)
		        	$is_graph_2week = 1;
		        if($_date == $_today3)
		        	$is_graph_3week = 1;
		        if($_date == $_today4)
		        	$is_graph_4week = 1;
		        	
		        $_cpi_1week = ($is_graph_1week == 1) ? $newuser_graph_stats_1week[$index1++]["cpi"] : "0";
		        $_cpi_2week = ($is_graph_2week == 1) ? $newuser_graph_stats_2week[$index2++]["cpi"] : "0";
		        $_cpi_3week = ($is_graph_3week == 1) ? $newuser_graph_stats_3week[$index3++]["cpi"] : "0";
		        $_cpi_4week = ($is_graph_4week == 1) ? $newuser_graph_stats_4week[$index4++]["cpi"] : "0";
		        
		        echo("['".$_date."'");
		
				if ($_cpi_1week != "")
					echo(",{v:".$_cpi_1week.",f:'".number_format($_cpi_1week, 2)."'}");
				else
					echo(",0");
		
				if ($_cpi_2week != "")
					echo(",{v:".$_cpi_2week.",f:'".number_format($_cpi_2week, 2)."'}");
				else
					echo(",0");
		
				if ($_cpi_3week != "")
					echo(",{v:".$_cpi_3week.",f:'".number_format($_cpi_3week, 2)."'}");
				else
					echo(",0");
		
				if ($_cpi_4week != "")
					echo(",{v:".$_cpi_4week.",f:'".number_format($_cpi_4week, 2)."'}]");
				else
					echo(",0]");
		        
		        if ($j > 1)
					echo(",");
		        
		    }
?>
		]);
		
		var data5 = new google.visualization.DataTable();
        data5.addColumn('string', '날짜');
        data5.addColumn('number', '최근 1주');
        data5.addColumn('number', '최근 2주');
        data5.addColumn('number', '최근 3주');
        data5.addColumn('number', '최근 4주');
        data5.addRows([
<?

			$index1 = 0;
			$index2 = 0;
			$index3 = 0;
			$index4 = 0;
			
		    for ($j=sizeof($date_list); $j>0; $j--)
		    {
		        $_date = $date_list[$j-1];
		        
		        $is_graph_1week = 0;
		    	$is_graph_2week = 0;
		    	$is_graph_3week = 0;
		    	$is_graph_4week = 0;
		        
		    	$_today1 = $newuser_graph_stats_1week[$index1]["today"];
		    	$_today2 = $newuser_graph_stats_2week[$index2]["today"];
		    	$_today3 = $newuser_graph_stats_3week[$index3]["today"];
		    	$_today4 = $newuser_graph_stats_4week[$index4]["today"];
		    	
		    	if($_date == $_today1)
		    		$is_graph_1week = 1;
		    	if($_date == $_today2)
		    		$is_graph_2week = 1;
		    	if($_date == $_today3)
		    		$is_graph_3week = 1;
		    	if($_date == $_today4)
		    		$is_graph_4week = 1;
		    	
		    	$_newuser_money_1week = ($is_graph_1week == 1) ? $newuser_graph_stats_1week[$index1++]["newuser_money"] : "0";
		    	$_newuser_money_2week = ($is_graph_2week == 1) ? $newuser_graph_stats_2week[$index2++]["newuser_money"] : "0";
		    	$_newuser_money_3week = ($is_graph_3week == 1) ? $newuser_graph_stats_3week[$index3++]["newuser_money"] : "0";
		    	$_newuser_money_4week = ($is_graph_4week == 1) ? $newuser_graph_stats_4week[$index4++]["newuser_money"] : "0";
		    		
		        echo("['".$_date."'");
		
				if ($_newuser_money_1week != "")
					echo(",{v:".$_newuser_money_1week.",f:'".number_format($_newuser_money_1week, 2)."'}");
				else
					echo(",0");
		
				if ($_newuser_money_2week != "")
					echo(",{v:".$_newuser_money_2week.",f:'".number_format($_newuser_money_2week, 2)."'}");
				else
					echo(",0");
		
				if ($_newuser_money_3week != "")
					echo(",{v:".$_newuser_money_3week.",f:'".number_format($_newuser_money_3week, 2)."'}");
				else
					echo(",0");
		
				if ($_newuser_money_4week != "")
					echo(",{v:".$_newuser_money_4week.",f:'".number_format($_newuser_money_4week, 2)."'}]");
				else
					echo(",0]");
		        
		        if ($j > 1)
					echo(",");
		        
		    }
?>
		]);
		
        var options = {
    	        title:'',                                                
    	        width:1100,                         
    	        height:200,
    	        axisTitlesPosition:'in',
    	        curveType:'none',
    	        focusTarget:'category',
    	        interpolateNulls:'true',
    	        legend:'top',
    	        fontSize : 12,
    	        chartArea:{left:100,top:40,width:1100,height:130}
    	    };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
	    chart.draw(data1, options);
	    
        chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
	    chart.draw(data2, options);
	    
        chart = new google.visualization.LineChart(document.getElementById('chart_div3'));
	    chart.draw(data3, options);
	    
        chart = new google.visualization.LineChart(document.getElementById('chart_div4'));
	    chart.draw(data4, options);
	    
        chart = new google.visualization.LineChart(document.getElementById('chart_div5'));
	    chart.draw(data5, options);
	}

	google.setOnLoadCallback(drawChart);
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 신규 마케팅 일일 현황 </div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<input type="hidden" name="term" id="term" value="<?= $term ?>" />
			<input type="hidden" name="week_term" id="week_term" value="<?= $week_term ?>" />
			<div class="search_box">
				Adflag : 
				<select name="adflag" id="adflag">
					<option value="" <?= ($search_adflag == "" || $search_adflag == "전체") ? "selected" : "" ?>>전체</option>
<?
	for($i=0; $i<sizeof($adflag_list); $i++)
	{
		$adflag = $adflag_list[$i]["adflag"];
?>
					<option value="<?= $adflag ?>" <?= ($search_adflag == $adflag) ? "selected" : "" ?>><?= $adflag ?></option>
<?
	}
?>
			    </select>
			    &nbsp;&nbsp;
				<input type="button" class="<?= ($term == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="term_web" onclick="change_term('0')" />
				<input type="button" class="<?= ($term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_term('1')" />
				<input type="button" class="<?= ($term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_term('2')"    />
				<input type="button" class="<?= ($term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_term('3')"    />
				&nbsp;&nbsp;
				날짜 : &nbsp;&nbsp;<input type="text" class="search_text" id="today" name="today" value="<?= $today ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
				
				<br/>
				
				<div style="float: right; margin-right: 2px">
					그래프 날짜 : &nbsp;&nbsp;
					<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" />~
					<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" />
					<input type="button" class="btn_search" value="검색" onclick="search()" />
				</div>
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	
	<!-- Web -->
	<table class="tbl_list_basic1" style="width:1250px">
		<colgroup>
			<col width="300">
			<col width="70">
			
			<col width="55">
			<col width="60">
			<col width="80">
			<col width="55">
			<col width="65">
			<col width="50">
			<col width="85">
			
			<col width="65">
			<col width="85">
			<col width="70">
			<col width="90">
			<col width="70">

			<col width="65">
        </colgroup>
        
        <thead>
        	<tr>
        		<th class="tdc" style="border-right: 1px solid #dbdbdb;" rowspan="2">기간</th>
        		<th class="tdc" style="border-right: 1px solid #dbdbdb;" rowspan="2">광고비용 </th>
        		<th class="tdc" style="border-right: 1px solid #dbdbdb;" colspan="7">신규</th>
        		<th class="tdc" style="border-right: 1px solid #dbdbdb;" colspan="5">복귀</th>
        		<th class="tdc" style="border-right: 1px solid #dbdbdb;" rowspan="2">ROI</th>
        	</tr>
        	
        	<tr>
            	<th class="tdr">유입수</th>
            	<th class="tdr">CPI</th>
            	<th class="tdc">미게임율</th>
            	<th class="tdr">결제자수</th>
            	<th class="tdc">PUR</th>
            	<th class="tdr">ARPU</th>
             	<th class="tdr" style="border-right: 1px solid #dbdbdb;">매출</th>
               	
               	<th class="tdr">복귀자수</th>
               	<th class="tdr">평균복귀일</th>
               	<th class="tdr">결제자수</th>
               	<th class="tdr">결제자<br/>평균 복귀일</th>
               	<th class="tdr" style="border-right: 1px solid #dbdbdb;">매출</th>
         	</tr>
        </thead>
        
        <tbody>
<?
	for($i=0; $i<sizeof($newuser_week_stats); $i++)
	{
		$subtype = $newuser_week_stats[$i]["subtype"];
		$spend = $newuser_week_stats[$i]["spend"];
		
		$newuser = $newuser_week_stats[$i]["newuser"];
		$newuser_money = $newuser_week_stats[$i]["newuser_money"];
		$newuser_noplay = $newuser_week_stats[$i]["newuser_noplay"];
		$noplay_rate = ($newuser == 0) ? 0 : number_format(($newuser_noplay / $newuser) * 100, 2);
		$newuser_payer = $newuser_week_stats[$i]["newuser_payer"];
		$pur = ($newuser_payer == 0) ? 0 : number_format(($newuser_payer / $newuser) * 100, 2);
		$arpu = ($newuser_money == 0) ? 0 : number_format(($newuser_money / $newuser), 2);
		$cpi = ($newuser == 0) ? 0 : number_format($spend / $newuser, 2);
		
		$reuser = $newuser_week_stats[$i]["reuser"];
		$reuser_leavedays = $newuser_week_stats[$i]["reuser_leavedays"];
		$reuser_payer = $newuser_week_stats[$i]["reuser_payer"];
		$reuser_payer_leavedays = $newuser_week_stats[$i]["reuser_payer_leavedays"];
		$reuser_money = $newuser_week_stats[$i]["reuser_money"];
		$roi = ($spend == 0) ? 0 : number_format((($newuser_money + $reuser_money) / $spend) * 100, 2);

		$week = date("Y-m-d", strtotime($today."-".((7 * $subtype) - 1)." days"))." ~ ".$today;
?>
			<tr>
				<td class="tdc point" style="font-size: 10pt; border-right: 1px solid #dbdbdb;">
				<input type="button" style="border:1px solid #4daad2; color:#fff; background-color: #4daad2; cursor: pointer;" value="+" onclick="pop_newuser_stat('<?= $today ?>',<?= $term ?>,<?= $subtype ?>)" />
				최근 <?= $subtype ?>주 (<?= $week ?>)</td>
				<td class="tdc" style="border-right: 1px solid #dbdbdb;"><?= ($spend == 0) ? "-" : "$ ".number_format($spend) ?></td>
				<td class="tdr"><?= number_format($newuser) ?></td>
				<td class="tdr">$ <?= $cpi ?></td>
				<td class="tdc"><?= $noplay_rate ?> %</td>
				<td class="tdr"><?= number_format($newuser_payer) ?></td>
				<td class="tdr"><?= $pur ?> %</td>
				<td class="tdr"><?= $arpu ?></td>
				<td class="tdr" style="border-right: 1px solid #dbdbdb;">$ <?= number_format($newuser_money) ?></td>
				
				<td class="tdr"><?= number_format($reuser) ?></td>
				<td class="tdr"><?= number_format($reuser_leavedays) ?> 일</td>
				<td class="tdr"><?= number_format($reuser_payer) ?></td>
				<td class="tdr"><?= number_format($reuser_payer_leavedays) ?> 일</td>
				<td class="tdr" style="border-right: 1px solid #dbdbdb;">$ <?= number_format($reuser_money) ?></td>

				<td class="tdc" style="border-right: 1px solid #dbdbdb;"><?= $roi ?> %</td>
			</tr>
<?
	}
?>
        </tbody>
	</table>
	
	<table class="tbl_list_basic1" style="width:750px;">
		<colgroup>
			<col width="227">
			<col width="230">
			<col width="65">
			<col width="65">
			<col width="">
		</colgroup>
		
		<tbody>
			<tr>
				<td class="tdc point_title">Appsflyer Raw Data Download</td>
				<td colspan="4">
					날짜 : <input type="text" class="search_text" id="appsflyer_startdate" name="appsflyer_startdate" value="<?= $appsflyer_startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" />~
					<input type="text" class="search_text" id="appsflyer_enddate" name="appsflyer_enddate" value="<?= $appsflyer_enddate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" />
				</td>
				<td class="tdc"><input style="width:60px;" type="button" class="btn_03" value="Install" onclick="excelDownload(1, 1);"/></td>
				<td class="tdc" style="border-right: 1px solid #dbdbdb;"><input style="width:60px;" type="button" class="btn_03" value="IAP" onclick="excelDownload(1, 2);"/></td>
			</tr>
			<tr>
				<td class="tdc point_title">내부DB Raw Data Download</td>
				<td colspan="4">
					날짜 : <input type="text" class="search_text" id="t5_startdate" name="t5_startdate" value="<?= $t5_startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" />~
					<input type="text" class="search_text" id="t5_enddate" name="t5_enddate" value="<?= $t5_enddate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" />
				</td>
				<td class="tdc"><input style="width:60px;" type="button" class="btn_03" value="Install" onclick="excelDownload(2, 1);"/></td>
				<td class="tdc"><input style="width:60px;" type="button" class="btn_03" value="IAP" onclick="excelDownload(2, 2);"/></td>
				<td class="tdc" style="border-right: 1px solid #dbdbdb; border-top: 1px solid #dbdbdb;">
					<select name="user_type" id="user_type">
						<option value="1" <?= ($user_type == 1) ? "selected" : "" ?>>신규유저</option>
						<option value="2" <?= ($user_type == 2) ? "selected" : "" ?>>복귀유저</option>
					</select>
				</td>
			</tr>
		</tbody>
	</table>
	
	<div class="h2_title mt30" name="div_normal">
		신규유입수
	</div>
	<div id="chart_div2" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title mt30" name="div_normal">
		신규유입 매출
	</div>
	<div id="chart_div5" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title mt30" name="div_normal">
		ROI
	</div>
	<div id="chart_div3" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title mt30" name="div_normal">
		광고비용
	</div>
	<div id="chart_div1" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title mt30" name="div_normal">
		CPI
	</div>
	<div id="chart_div4" name="div_normal" style="height:230px; min-width: 500px"></div>
</div>
        
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>