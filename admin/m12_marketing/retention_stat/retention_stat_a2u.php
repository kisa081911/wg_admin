<?
	$top_menu = "marketing";
	$sub_menu = "app_performance";
	
	include("../../common/dbconnect/db_util_redshift.inc.php");
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	ini_set("memory_limit", "-1");
	
	$platform = ($_GET["platform"] == "") ? "ALL" : $_GET["platform"];
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
	$search_leavedays = ($_GET["leavedays"] == "") ? "7" : $_GET["leavedays"];
	$ispayer = ($_GET["ispayer"] == "") ? "0" : $_GET["ispayer"];
	$viewmode = ($_GET["viewmode"] == "") ? "0" : $_GET["viewmode"];
	
	check_xss($platform);
	check_xss($startdate.$enddate.$search_leavedays);
	
	$today = date("Y-m-d");
	$before_day = get_past_date($today, 14, "d");
	
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db_redshift = new CDatabase_Redshift();
	
	$ispayer_sql = "";
	
	if($ispayer != 0)
	{
	    $ispayer_sql = "AND is_payer = $ispayer ";
	}
	
	$orderby_sql = "";
	
	if($viewmode == 0)
	{
	    $orderby_sql = "ASC";
	}
	else
	{
	    
	    $orderby_sql = "DESC";
	}
	
    $sql = "select today,
              SUM(CASE WHEN platform = 0 THEN user_cnt ELSE 0 END) as web_count,
              SUM(CASE WHEN platform = 1 THEN user_cnt ELSE 0 END) as ios_count,
              SUM(CASE WHEN platform = 2 THEN user_cnt ELSE 0 END) as android_count,
              SUM(CASE WHEN platform = 3 THEN user_cnt ELSE 0 END) as amazon_count
            from (
              select date(writedate) as today, 0 as platform, adflag, count(*) as user_cnt
              from t5_user_retention_log
              where writedate >= '$startdate 00:00:00' and writedate <= '$enddate 23:59:59' and leavedays >= $search_leavedays $ispayer_sql 
                and (adflag like 'gamenotify%' or adflag = 'vip_coupon' or adflag = 'couponnotify')
              group by 1, 2, 3
              union all
              select date(writedate) as today, platform, adflag, count(*) as user_cnt
              from t5_user_retention_mobile_log
              where writedate >= '$startdate 00:00:00' and writedate <= '$enddate 23:59:59' and leavedays >= $search_leavedays $ispayer_sql 
                and (adflag like 'gamenotify%' or adflag = 'vip_coupon' or adflag = 'couponnotify')
              group by 1, 2, 3
            ) t1
            group by today
            order by today $orderby_sql";
    $retention_list = $db_redshift->gettotallist($sql);

    $sql = "SELECT today, adflag, sum(user_cnt) as user_cnt
            FROM (
              SELECT date(writedate) as today, 0 as platform, adflag, count(*) as user_cnt
              FROM t5_user_retention_log
              WHERE writedate >= '$startdate 00:00:00' and writedate <= '$enddate 23:59:59' and leavedays >= $search_leavedays $ispayer_sql 
                and (adflag like 'gamenotify%' or adflag = 'vip_coupon' or adflag = 'couponnotify')
              GROUP BY 1, 2, 3
            ) t1
            GROUP BY today, adflag
            ORDER BY today DESC, user_cnt DESC";
    $detail_retention_list = $db_redshift->gettotallist($sql);
    
    $sql = "SELECT adflag, sum(user_cnt) as user_cnt
            FROM (
              SELECT date(writedate) as today, 0 as platform, adflag, count(*) as user_cnt
              FROM t5_user_retention_log
              WHERE writedate >= '$startdate 00:00:00' and writedate <= '$enddate 23:59:59' and leavedays >= $search_leavedays $ispayer_sql 
                and (adflag like 'gamenotify%' or adflag = 'vip_coupon' or adflag = 'couponnotify')
              GROUP BY 1, 2, 3
            ) t1
            GROUP BY adflag
            ORDER BY user_cnt DESC";
    $detail_total_retention_list = $db_redshift->gettotallist($sql);
    
	$db_redshift->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>

<script type="text/javascript">
    $(function() {
        $("#startdate").datepicker({ });
        $("#enddate").datepicker({ });
    });

    function tab_change(tab)
    {
		window.location.href = "/m12_marketing/retention_stat/retention_stat_" + tab + ".php";
    }

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;
		
		search_form.submit();
	}

	google.load("visualization", "1", {packages:["corechart"]});

    function drawChart() 
    {
    	var data0 = new google.visualization.DataTable();
        data0.addColumn('string', '날짜');
        data0.addColumn('number', 'web 복귀자(명)');
        data0.addRows([
<?
    for ($i=0; $i<sizeof($retention_list); $i++)
	{
	    $today = $retention_list[$i]["today"];
	    $web_count = $retention_list[$i]["web_count"];
		
	    echo("['".$today."',".$web_count."]");
		
	    if ($i != sizeof($retention_list) - 1)
			echo(",");
	}
?>
        ]);

    	var options1 = {
            width:1050,                         
            height:470,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:300}
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div0'));
        chart.draw(data0, options1);
    }
        
	google.setOnLoadCallback(drawChart);
</script>
		<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; A2U 복귀 유입 현황</div>
	        </div>
	    	<!-- //title_warp -->
	    	<ul class="tab">
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('all')">전체</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('retention')">리텐션 광고</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('new')">신규 광고</li>
				<li class="select" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('a2u')">A2U</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('push')">Push</li>
			</ul>
	    	<form name="search_form" id="search_form"  method="get" action="retention_stat_a2u.php">
	    		<input type="hidden" name="platform" id="platform" value="<?= $platform ?>" />
	    		<div class="detail_search_wrap">
					<input type="text" class="search_text" id="leavedays" name="leavedays" style="width:30px" value="<?= $search_leavedays ?>" onkeypress="search_press(event); return checknum();" /><span class="search_lbl">일 이상 이탈자</span>
					<span class="search_lb2 ml10">결제경험 여부</span>
	    			<select name="ispayer" id="ispayer">										
						<option value="0" <?= ($ispayer=="0") ? "selected" : "" ?>>전체</option>
						<option value="1" <?= ($ispayer=="1") ? "selected" : "" ?>>결제경험자만</option>                       				
					</select>
					<span class="search_lb2 ml10">View</span>
	    			<select name="viewmode" id="viewmode">										
						<option value="0" <?= ($viewmode=="0") ? "selected" : "" ?>>그래프</option>
						<option value="1" <?= ($viewmode=="1") ? "selected" : "" ?>>리스트(세부)</option>  				
					</select>
	    			<span class="search_lb2 ml10">날짜</span>
	    			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
		            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
					<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
				</div>
    		</form>
<?
	if($viewmode == 0)
	{
?>
	    	<div class="h2_title">[A2U 복귀자수]</div>
	    	<div id="chart_div0" style="height:480px; min-width: 500px"></div>
    	</div>
<?          
	}
	else if($viewmode == 1)
	{
?>    	
			<div class="h2_title">[전체 복귀자수]</div>
            <div id="tab_content_1">
        		<table class="tbl_list_basic1">
        			<colgroup>
        				<col width="">
        				<col width="">
        			</colgroup>
        			<thead>
        				<tr>
        					<th class="tdc">날짜</th>
        					<th class="tdc">web</th>
        				</tr>
        			</thead>
        			<tbody>    				
<?
                    $total_count = 0;
                    
                    for($i=0; $i<sizeof($retention_list); $i++)
        			{
        			    $today = $retention_list[$i]["today"];
        			    $web_count = $retention_list[$i]["web_count"];
        			    
        			    $total_count += $web_count;
?>
    					<tr>
        					<td class="tdc point_title"><?= $today?></td>
        					<td class="tdc"><?= number_format($web_count) ?></td>
        				</tr>
<?
    			     }
?>
						<tr>
							<td class="tdc point_title">합계</td>
        					<td class="tdc"><?= number_format($total_count) ?></td>
						</tr>
        			</tbody>
        		</table>
        		<br/><br/>
        		<div class="h2_title">[세부 복귀자수]</div>
        		<table class="tbl_list_basic1">
        			<colgroup>
        				<col width="">
        				<col width="">
        				<col width="">
        			</colgroup>
        			<thead>
        				<tr>
        					<th class="tdc">날짜</th>
        					<th class="tdc">adflag</th>
        					<th class="tdc">복귀자수</th>
        				</tr>
        			</thead>
        			<tbody>    				
<?
                    $tmp_today = "";
                    
                    for($i=0; $i<sizeof($detail_retention_list); $i++)
        			{
        			    $today = $detail_retention_list[$i]["today"];
        			    $adflag = $detail_retention_list[$i]["adflag"];
        			    $user_cnt = $detail_retention_list[$i]["user_cnt"];
?>
    					<tr>
<?
                        if($tmp_today != $today)
                        {
                            $rowcnt = 0;
                            
                            for($j=0; $j<sizeof($detail_retention_list); $j++)
                            {
                                if($today == $detail_retention_list[$j]["today"])
                                {
                                    $rowcnt++;
                                }
                            }
?>
							<td class="tdc point_title" rowspan="<?= $rowcnt?>"><?= $today?></td>
<?
                            $tmp_today = $today;
                        }
?>
        					<td class="tdc"><?= $adflag ?></td>
        					<td class="tdc"><?= number_format($user_cnt) ?></td>
        				</tr>
<?
    			     }
    			     for($i=0; $i<sizeof($detail_total_retention_list); $i++)
    			     {
    			         $total_adflag = $detail_total_retention_list[$i]["adflag"];
    			         $total_user_cnt = $detail_total_retention_list[$i]["user_cnt"];
    			         ?>
						<tr>
<?
                        if($i == 0)
                        {
?>
							<td class="tdc point_title" rowspan="<?= sizeof($detail_total_retention_list)?>"><?= 합계?></td>
							<td class="tdc"><?= $total_adflag ?></td>
        					<td class="tdc"><?= number_format($total_user_cnt) ?></td>
<?
                        }
                        else
                        {
?>
							<td class="tdc"><?= $total_adflag ?></td>
        					<td class="tdc"><?= number_format($total_user_cnt) ?></td>
<?
                        }   
?>
						</tr>
<?
    			     }
?>
        			</tbody>
<?
   }
?>
        		</table>
        	</div>						
    	</div>
    	<!--  //CONTENTS WRAP -->
    	
    	<div class="clear"></div>
    </div>
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>