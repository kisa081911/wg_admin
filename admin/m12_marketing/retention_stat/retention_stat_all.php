<?
    $top_menu = "marketing";
    $sub_menu = "retention_stat";
    
    include("../../common/dbconnect/db_util_redshift.inc.php");
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    ini_set("memory_limit", "-1");
    
    $platform = ($_GET["platform"] == "") ? "ALL" : $_GET["platform"];
    
    $startdate = $_GET["startdate"];
    $enddate = $_GET["enddate"];
    $search_leavedays = ($_GET["leavedays"] == "") ? "7" : $_GET["leavedays"];
    $ispayer = ($_GET["ispayer"] == "") ? "0" : $_GET["ispayer"];
    
    check_xss($platform);
    check_xss($startdate.$enddate.$search_leavedays);
    
    $today = date("Y-m-d");
    $before_day = get_past_date($today, 14, "d");
    
    $startdate = ($startdate == "") ? $before_day : $startdate;
    $enddate = ($enddate == "") ? $today : $enddate;
    
    $db_redshift = new CDatabase_Redshift();
    
    $platform_sql = "";
    
    if($platform != "ALL")
    {
        $platform_sql = "AND platform = $platform ";
    }
    
    $ispayer_sql = "";
    
    if($ispayer != 0)
    {
        $ispayer_sql = "AND is_payer = $ispayer ";
    }
    
    $sql = "select today, SUM(retention_ad) as retention_ad, SUM(new_ad) as new_ad, SUM(share) as share, SUM(a2u) as a2u, SUM(wakeup) as wakeup, SUM(cross_promotion) as cross_promotion, SUM(email_retention) as email_retention,
                    SUM(event) as event, SUM(push_event) as push_event, SUM(etc) as etc, SUM(organic) as organic
            from (
              select today, platform,
                (case when adflag_group = 'retention_ad' then user_cnt else 0 end) as retention_ad,
                (case when adflag_group = 'new_ad' then user_cnt else 0 end) as new_ad,
                (case when adflag_group = 'share' then user_cnt else 0 end) as share,
                (case when adflag_group = 'a2u' then user_cnt else 0 end) as a2u,
                (case when adflag_group = 'wakeup' then user_cnt else 0 end) as wakeup,
                (case when adflag_group = 'cross_promotion' then user_cnt else 0 end) as cross_promotion,
                (case when adflag_group = 'email_retention' then user_cnt else 0 end) as email_retention,
                (case when adflag_group = 'event' then user_cnt else 0 end) as event,
                (case when adflag_group = 'push_event' then user_cnt else 0 end) as push_event,
                (case when adflag_group = 'etc' then user_cnt else 0 end) as etc,
                (case when adflag_group = '' then user_cnt else 0 end) as organic
              from (
                select date(writedate) as today, 0 as platform, (case when (adflag like 'm_retention%' or adflag like 'retention%') then 'retention_ad'
                    when (adflag like 'fbself%' or adflag like '%_int' or adflag = 'Facebook Ads' or adflag = 'Apple Search Ads' or adflag like 'amazon%' or adflag like 'm_d%') then 'new_ad'
                    when (adflag like 'share_%' or adflag like 'og_%') then 'share'
                    when (adflag like 'gamenotify%' or adflag = 'vip_coupon' or adflag = 'couponnotify') then 'a2u'
                    when (adflag like 'wakeup_request%') then 'wakeup'
                    when (adflag like 'cr_%' or adflag = 'wbingo_bottom' or adflag like 'wcasino_%' or adflag like 'duc_%' or adflag like 'wbingo_%') then 'cross_promotion'
                    when (adflag = 'email_retention') then 'email_retention'
                    when (adflag = 'event') then 'event'
                    when (adflag = 'm_push') then 'push_event'
                    when adflag = '' then ''
                    else 'etc' end) as adflag_group,
                    count(*) as user_cnt, avg(leavedays) as avg_leavedays
                from t5_user_retention_log
                where writedate >= '$startdate 00:00:00' and writedate <= '$enddate 23:59:59' and leavedays >= $search_leavedays $ispayer_sql
                group by 1, 2, 3
                union all
                select date(writedate) as today, platform, (case when (adflag like 'm_retention%' or adflag like 'retention%') then 'retention_ad'
                    when (adflag like 'fbself%' or adflag like '%_int' or adflag = 'Facebook Ads' or adflag = 'Apple Search Ads' or adflag like 'amazon%' or adflag like 'm_d%') then 'new_ad'
                    when (adflag like 'share_%' or adflag like 'og_%') then 'share'
                    when (adflag like 'gamenotify%' or adflag = 'vip_coupon' or adflag = 'couponnotify') then 'a2u'
                    when (adflag like 'wakeup_request%') then 'wakeup'
                    when (adflag like 'cr_%' or adflag = 'wbingo_bottom' or adflag like 'wcasino_%' or adflag like 'duc_%' or adflag like 'wbingo_%') then 'cross_promotion'
                    when (adflag = 'email_retention') then 'email_retention'
                    when (adflag = 'event') then 'event'
                    when (adflag = 'm_push') then 'push_event'
                    when adflag = '' then ''
                    else 'etc' end) as adflag_group,
                    count(*) as user_cnt, avg(leavedays) as avg_leavedays
                from t5_user_retention_mobile_log
                where writedate >= '$startdate 00:00:00' and writedate <= '$enddate 23:59:59' and leavedays >= $search_leavedays $ispayer_sql
                group by 1, 2, 3
              ) t1
            ) total
            WHERE 1 = 1 $platform_sql
            group by 1
            order by 1 desc";
    $retention_list = $db_redshift->gettotallist($sql);
    
    $db_redshift->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>

<script type="text/javascript">
    $(function() {
        $("#startdate").datepicker({ });
        $("#enddate").datepicker({ });
    });

    function tab_change(tab)
    {
		window.location.href = "/m12_marketing/retention_stat/retention_stat_" + tab + ".php";
    }

	function change_platform(term)
	{
		var search_form = document.search_form;
		
		var term_all = document.getElementById("term_all");
		var term_fb = document.getElementById("term_fb");
		var term_ios = document.getElementById("term_ios");
		var term_and = document.getElementById("term_and");
		var term_ama = document.getElementById("term_ama");

		document.search_form.platform.value = term;
		
		if (term == "ALL")
		{
			term_all.className="btn_schedule_select";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 0)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule_select";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 1)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule_select";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 2)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule_select";
			term_ama.className="btn_schedule";
		}
		else if (term == 3)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule_select";
		}

		search_form.submit();
	}

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;
		
		search_form.submit();
	}
</script>
		<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; Retention 유입 현황(Organic 제외)</div>
	        </div>
	    	<!-- //title_warp -->
	    	<ul class="tab">
				<li class="select" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('all')">전체</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('retention')">리텐션 광고</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('new')">신규 광고</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('a2u')">A2U</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('push')">Push</li>
			</ul>
	    	<form name="search_form" id="search_form"  method="get" action="retention_stat_all.php">
	    		<input type="hidden" name="platform" id="platform" value="<?= $platform ?>" />
	    		<div class="detail_search_wrap">
	    			<input type="button" class="<?= ($platform == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_all" onclick="change_platform('ALL')" value="통합" />
	    			<input type="button" class="<?= ($platform == "0") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_fb" onclick="change_platform('0')" value="Facebook" />
	    			<input type="button" class="<?= ($platform == "1") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ios" onclick="change_platform('1')" value="iOS" />
	    			<input type="button" class="<?= ($platform == "2") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_and" onclick="change_platform('2')" value="Android" />
	    			<input type="button" class="<?= ($platform == "3") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ama" onclick="change_platform('3')" value="Amazon" />
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" class="search_text" id="leavedays" name="leavedays" style="width:30px" value="<?= $search_leavedays ?>" onkeypress="search_press(event); return checknum();" /><span class="search_lbl">일 이상 이탈자</span>
					<span class="search_lb2 ml10">결제경험 여부</span>
	    			<select name="ispayer" id="ispayer">										
						<option value="0" <?= ($ispayer=="0") ? "selected" : "" ?>>전체</option>
						<option value="1" <?= ($ispayer=="1") ? "selected" : "" ?>>결제경험자만</option>                       				
					</select>
	    			<span class="search_lb2 ml10">날짜</span>
	    			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
		            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
					<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
				</div>
    		</form>

	    	<div id="tab_content_1">
    		<table class="tbl_list_basic1">
    			<colgroup>
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    			</colgroup>
    			<thead>
    				<tr>
    					<th class="tdc">날짜</th>
    					<th class="tdc">전체 복귀자</th>
    					<th class="tdc">리텐션 광고</th>
    					<th class="tdc">신규 광고</th>
						<th class="tdc">쉐어</th>
						<th class="tdc">A2U</th>
    					<th class="tdc">WakeUp</th>
    					<th class="tdc">크로스프로모션</th>
						<th class="tdc">이메일</th>
						<th class="tdc">팬페이지</th>
    					<th class="tdc">Push</th>
    					<th class="tdc">Etc</th>
    				</tr>
    			</thead>
    			<tbody>    				
<?
                for($i=0; $i<sizeof($retention_list); $i++)
    			{
    			    $today = $retention_list[$i]["today"];
    			    $retention_ad = $retention_list[$i]["retention_ad"];
    			    $new_ad = $retention_list[$i]["new_ad"];
    			    $share = $retention_list[$i]["share"];
    			    $a2u = $retention_list[$i]["a2u"];
    			    $wakeup = $retention_list[$i]["wakeup"];
    			    $cross_promotion = $retention_list[$i]["cross_promotion"];
    			    $email_retention = $retention_list[$i]["email_retention"];
    			    $event = $retention_list[$i]["event"];
    			    $push_event = $retention_list[$i]["push_event"];
    			    $etc = $retention_list[$i]["etc"];
    			    
    			    $total_count = $retention_ad + $new_ad + $share + $a2u + $wakeup + $cross_promotion + $email_retention + $event + $push_event + $etc
?>
					<tr>
    					<td class="tdc point_title"><?= $today?></td>
    					<td class="tdc"><?= number_format($total_count) ?></td>
    					<td class="tdc"><?= number_format($retention_ad) ?></td>
    					<td class="tdc"><?= number_format($new_ad) ?></td>
    					<td class="tdc"><?= number_format($share) ?></td>
    					<td class="tdc"><?= number_format($a2u) ?></td>
    					<td class="tdc"><?= number_format($wakeup) ?></td>
    					<td class="tdc"><?= number_format($cross_promotion) ?></td>
    					<td class="tdc"><?= number_format($email_retention) ?></td>
    					<td class="tdc"><?= number_format($event) ?></td>
    					<td class="tdc"><?= number_format($push_event) ?></td>
    					<td class="tdc"><?= number_format($etc) ?></td>
    				</tr>
<?
			}
?>
    			</tbody>
    		</table>
    	</div>	
    	</div>
    	<!--  //CONTENTS WRAP -->
    	
    	<div class="clear"></div>
    </div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>