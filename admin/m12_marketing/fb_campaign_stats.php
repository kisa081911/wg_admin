<?
	$top_menu = "marketing";
	$sub_menu = "fb_campaign_web";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_startdate = $_GET["start_searchdate"];
	$search_enddate = $_GET["end_searchdate"];
	$search_start_orderdate = $_GET["start_orderdate"];
	$search_end_orderdate = $_GET["end_orderdate"];
	$search_payday = $_GET["payday"];
	
	$category = ($_GET["category"] == "") ? "1":$_GET["category"];

	$today = date("Y-m-d");
	
	if($search_startdate == "")
		$search_startdate = date("Y-m-d", strtotime("-15 day"));
	
	if($search_enddate == "")
		$search_enddate = $today;
	
	$ad_stats_table="tbl_ad_stats";

	$db_main2 = new CDatabase_main2();
	
	if(($search_start_orderdate != "" && $search_end_orderdate != ""))
	{
		$add_sql = "	WHERE sub2.writedate BETWEEN '$search_start_orderdate 00:00:00' AND '$search_end_orderdate 23:59:59' ";
	}
	
	if($search_payday != "")
	{
		if($add_sql == "")
			$add_sql = "WHERE sub2.dayafterinstall <= $search_payday ";
		else
			$add_sql .= "AND sub2.dayafterinstall <= $search_payday ";
	}
	
	$sql = "SELECT t1.campaign_id, campaign_name, app_install_result, reach, cpi, spend, STATUS, IFNULL(money, 0) AS money, IFNULL(buy_user_cnt, 0) AS buy_user_cnt ".
			"FROM ( ".
			"	SELECT campaign_id, (SELECT campaign_name FROM $ad_stats_table WHERE campaign_id = a.campaign_id ORDER BY today DESC LIMIT 1) AS campaign_name, ".			
			"		SUM(app_install_result) AS app_install_result, MAX(campaign_reach) AS reach, IFNULL(SUM(spend) / SUM(app_install_result),0) AS cpi, ". 
			"		SUM(spend) AS spend, STATUS, updated_time ".
			"	FROM $ad_stats_table a ". 
			"	WHERE today BETWEEN '$search_startdate' AND '$search_enddate' ".
			"	GROUP BY campaign_id ".
			") t1 LEFT JOIN ( ".
			"	SELECT campaign_id, IFNULL(SUM(money), 0) AS money, COUNT(DISTINCT useridx) AS buy_user_cnt ".
			"	FROM ( ".
			"		SELECT campaign_id, ad_id ".
			"		FROM $ad_stats_table ".
			"		WHERE today BETWEEN '$search_startdate' AND '$search_enddate' ". 
			"		GROUP BY campaign_id, ad_id ".
			"	) sub1 JOIN tbl_user_marketing_order sub2 ON sub1.ad_id = sub2.fb_ad_id ".
			$add_sql.
			"	GROUP BY campaign_id ".
			") t2 ON t1.campaign_id = t2.campaign_id ".
			"ORDER BY updated_time DESC ";
	$contents_list = $db_main2->gettotallist($sql);
	
	$sql = "SELECT campaign_id, SUM(join_count) AS sum_join_count ". 
			"FROM ".
			"( ".	
			"	SELECT campaign_id, ad_id, (SELECT COUNT(useridx) FROM tbl_user_marketing WHERE fb_ad_id = a.ad_id AND createdate BETWEEN '2016-05-26' AND '2016-06-10') AS join_count ".
			"	FROM $ad_stats_table a ". 	
			"	WHERE today BETWEEN '$search_startdate' AND '$search_enddate' ". 
			"	GROUP BY campaign_id, ad_id ".
			") b GROUP BY campaign_id ";
	$sum_join_count_list = $db_main2->gettotallist($sql);
	
	for($i = 0; $i < sizeof($sum_join_count_list); $i++)
	{
		$join_campaign_id = $sum_join_count_list[$i]["campaign_id"];
		$join_count = $sum_join_count_list[$i]["sum_join_count"];
		 
		 $sum_join_count_array[$join_campaign_id] = $join_count;
	}
	
 	$sql = " SELECT sum(spend) as spend ,today ". 
		   " FROM $ad_stats_table ".
		   " WHERE '$today' >= today ".
		   " AND today >= DATE_SUB('$today', INTERVAL 6 day)".
		   " GROUP BY today ORDER BY today ASC ";
	$spend_list = $db_main2->gettotallist($sql);

	$db_main2->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<link type="text/css" rel="stylesheet" href="../css/marketing.css" />
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
<?
	if(sizeof($spend_list)>0){
?>
	google.setOnLoadCallback(drawChart);
<?
	}
?>
	
	function drawChart() 
	{                                          		
		var data1 = google.visualization.arrayToDataTable([
			['날짜', '지출'],
<?	
	foreach ($spend_list as $spend)
	{
		$toal_spend = $spend['spend'];
		$chart_date = date("m-d", strtotime($spend['today']));
		
		$sum_toal_spend += $toal_spend;
		if($chart_date != date("m-d", strtotime($today)))
		{
			echo "['".$chart_date."',  {v:".$toal_spend." ,f:'".number_format($toal_spend,2)."'}],";
		}
		else
		{
			echo "['오늘',  {v:".$toal_spend." ,f:'".number_format($toal_spend,2)."'}]";
		}
	}
?>
		]);
	
		var options1 = {
			    title: '지난 7일간 지출: <?="$".number_format($sum_toal_spend,2)?>',
				legend: { position: "none" }
			  };
			  
		var chart1 = new google.visualization.ColumnChart(document.getElementById('chart1_div'));
		chart1.draw(data1, options1);
	}
	
	$(function() {
		$("#start_searchedate").datepicker({});
		$("#end_searchdate").datepicker({});
		$("#start_orderdate").datepicker({});
		$("#end_orderdate").datepicker({});
	});
		
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.start_date.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.start_date.focus();
	        return;
	    } 
	
	    search_form.submit();
	}

</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Facebook 캠페인 현황 - Web</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<input type="text" class="search_text" id="start_searchedate" name="start_searchdate" value="<?= $search_startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_searchdate" name="end_searchdate" value="<?= $search_enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
				&nbsp;&nbsp;&nbsp;&nbsp;결제액 기준 : 가입 후 <input type="text" class="search_text" id="payday" name="payday" style="width:60px" value="<?= $search_payday ?>" onkeypress="search_press(event); return checknum();" />일 이내 결제
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
			
			<div class="clear"></div>
                
			<div class="search_box">
				<input type="text" class="search_text" id="start_orderdate" name="start_orderdate" value="<?= $search_start_orderdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_orderdate" name="end_orderdate" value="<?= $search_end_orderdate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
				&nbsp;&nbsp;&nbsp;&nbsp;기간내 결제
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $search_startdate ?></span> ~ <span><?= $search_enddate ?></span> 통계입니다
	</div>
<?
	if(sizeof($spend_list)>0){
?>
	<div id="chart1_div" style="width: 800px; height: 100px; margin-left:150px;"></div>
<?	
	} else {
?>
	<div id="chart1_div" style="width: 800px; height: 100px; margin-left:10px;"><h3>지난 7일간 지출 결과가 없습니다.</h3></div>
<?
	}
?>
	<br/>
	<br/>
	<div id="js_1_layer" style="width: 220px;margin-left: 392px;margin-top: 32px;position: absolute; display:none;">
		<div class="io1" style="margin-left: 183px;">&nbsp;</div>
		<div class="_53il" style="border:0;border-radius:2px;box-shadow:0 0 0 1px rgba(0, 0, 0, .1), 0 1px 10px rgba(0, 0, 0, .35);">
			<div class="_53ij">
				<div data-reactroot="">
					<div class="_2ny1">
						<div class="_2ny2">결과</div>
						<div class="_f-m _2ny3">
							<div class="_if">
								<p class="_3p8">광고로 인해 발생한 결과. 이 결과는 선택한 목표를 기준으로 합니다.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="js_2_layer" style="width: 220px;margin-left: 503px;margin-top: 32px;position: absolute; display:none;">
		<div class="io1" style="margin-left: 183px;">&nbsp;</div>
		<div class="_53il" style="border:0;border-radius:2px;box-shadow:0 0 0 1px rgba(0, 0, 0, .1), 0 1px 10px rgba(0, 0, 0, .35);">
			<div class="_53ij">
				<div data-reactroot="">
					<div class="_2ny1">
						<div class="_2ny2">도달</div>
						<div class="_f-m _2ny3">
							<div class="_if">
								<p class="_3p8">광고가 게재된 사람 수입니다.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="js_3_layer" style="width: 220px;margin-left: 616px;margin-top: 32px;position: absolute; display:none;">
		<div class="io1" style="margin-left: 183px;">&nbsp;</div>
		<div class="_53il" style="border:0;border-radius:2px;box-shadow:0 0 0 1px rgba(0, 0, 0, .1), 0 1px 10px rgba(0, 0, 0, .35);">
			<div class="_53ij">
				<div data-reactroot="">
					<div class="_2ny1">
						<div class="_2ny2">비용</div>
						<div class="_f-m _2ny3">
							<div class="_if">
								<p class="_3p8">목표와 관련되어 발생한 각 행동당 평균 지출 비용.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="js_4_layer" style="width: 220px;margin-left: 808px;margin-top: 32px;position: absolute; display:none;">
		<div class="io1" style="margin-left: 183px;">&nbsp;</div>
		<div class="_53il" style="border:0;border-radius:2px;box-shadow:0 0 0 1px rgba(0, 0, 0, .1), 0 1px 10px rgba(0, 0, 0, .35);">
			<div class="_53ij">
				<div data-reactroot="">
					<div class="_2ny1">
						<div class="_2ny2">지출금액</div>
						<div class="_f-m _2ny3">
							<div class="_if">
								<p class="_3p8">광고 관리자에서 선택한 기간 동안 지출한 총 금액입니다.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<table class="tbl_list_basic_matket1" >
		<colgroup>
			<col width="5">
			<col width="180">
			<col width="50">
			<col width="50">
            <col width="60">
            <col width="60">
            <col width="60">
            <col width="60">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="5">
		</colgroup>
        <thead>
        	<tr>
        		<th class="tdl">&nbsp;</th>
        		<th class="tdl">캠페인 이름</th>
             	<th class="tdr">결과 <i class="_2ny7 img sp_rHAvPENsMSL sx_ff2fa9" aria-owns="js_24" aria-haspopup="true" tabindex="0" id="js_1" onmouseover="javascript:$('#js_1_layer').css('display','block');$('#js_1').css('opacity','1');" onmouseout="javascript:$('#js_1_layer').css('display','none');$('#js_1').css('opacity','.6');"></i></th>             	
               	<th class="tdr">도달수 <i class="_2ny7 img sp_rHAvPENsMSL sx_ff2fa9" aria-owns="js_24" aria-haspopup="true" tabindex="0" id="js_2" onmouseover="javascript:$('#js_2_layer').css('display','block');$('#js_2').css('opacity','1');" onmouseout="javascript:$('#js_2_layer').css('display','none');$('#js_2').css('opacity','.6');"></i></th>
               	<th class="tdr">비용 <i class="_2ny7 img sp_rHAvPENsMSL sx_ff2fa9" aria-owns="js_24" aria-haspopup="true" tabindex="0" id="js_3" onmouseover="javascript:$('#js_3_layer').css('display','block');$('#js_3').css('opacity','1');" onmouseout="javascript:$('#js_3_layer').css('display','none');$('#js_3').css('opacity','.6');"></i></th>
               	<th class="tdr">지출금액 <i class="_2ny7 img sp_rHAvPENsMSL sx_ff2fa9" aria-owns="js_24" aria-haspopup="true" tabindex="0" id="js_4" onmouseover="javascript:$('#js_4_layer').css('display','block');$('#js_4').css('opacity','1');" onmouseout="javascript:$('#js_4_layer').css('display','none');$('#js_4').css('opacity','.6');"></i></th>               	
               	<th class="tdr">신규 가입<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_5"></i></th>
               	<th class="tdr">매출<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_6"></i></th>
               	<th class="tdr">결제자수<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_7"></i></th>
               	<th class="tdr">PUR<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_8"></i></th>
               	<th class="tdr">ARPU<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_9"></i></th>
               	<th class="tdr">ARPPU<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_10"></i></th>
               	<th class="tdr">ROI<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_11"></i></th>
               	<th class="tdl">&nbsp;</th>
         	</tr>
        </thead>
		<tbody>
<?
	foreach ($contents_list as $contents)
	{
		$campaign_id = $contents['campaign_id'];
		$campaign_name = $contents['campaign_name'];
		$app_install_result = $contents['app_install_result'];
		$join_count = $sum_join_count_array[$campaign_id];
		$reach = $contents['reach'];
		$cpi = $contents['cpi'];
		$spend = $contents['spend'];
		$money = $contents['money'];
		$buy_user_cnt = $contents['buy_user_cnt'];
		
		$pur = ($buy_user_cnt == 0) ? 0 : round(($buy_user_cnt/$app_install_result)*100, 3);
		$arpu = ($app_install_result == 0) ? 0 : round($money/$app_install_result, 3);
		$arppu = ($money == 0) ? 0 : round($money/$buy_user_cnt, 3);
		$roi = ($spend == 0) ? 0 : round($money/$spend*100, 2);
		
		$total_money += $money;
		$total_buy_user_cnt += $buy_user_cnt;
		
		$total_app_install_result += $app_install_result ;
		$total_join_count_result += $join_count ;
		$total_reach += $reach;
		$total_spend +=$spend;
?>    
			<tr>
				<td class="tdl">&nbsp;</td>
        		<td class="tdl point_title"><a href="./fb_adsets_stats.php?ids=<?=$campaign_id?>&select_type=web&start_searchdate=<?= $search_startdate ?>&end_searchdate=<?= $search_enddate ?>&start_orderdate=<?= $search_start_orderdate ?>&end_orderdate=<?= $search_end_orderdate ?>"><?=$campaign_name?></a></td>
             	<td class="tdr "><?=number_format($app_install_result)?><br/><span >앱 설치</span></td>             	
               	<td class="tdr "><?=number_format($reach)?></td>
               	<td class="tdr ">$<?=number_format($cpi,2)?><br/><span >앱 설치 당</span></td>
               	<td class="tdr ">$<?=number_format($spend,2)?></td>
               	<td class="tdr "><?=number_format($join_count)?><br/><span >신규 가입</span></td>
               	<td class="tdr ">$<?=number_format($money,2)?></td>
               	<td class="tdr "><?=number_format($buy_user_cnt)?></td>
               	<td class="tdr "><?=number_format($pur, 2)?>%</td>
               	<td class="tdr ">$<?=number_format($arpu, 2)?></td>
               	<td class="tdr ">$<?=number_format($arppu, 1)?></td>
               	<td class="tdr "><?=number_format($roi, 2)?>%</td>
               	<td class="tdl ">&nbsp;</td>
         	</tr>
<?
	}
	
	$total_arpu = ($total_app_install_result == 0) ? 0 : round($total_money/$total_app_install_result, 3);
	$total_roi = ($total_spend == 0) ? 0 : round($total_money/$total_spend*100, 2);
	$total_pur = ($total_buy_user_cnt == 0) ? 0 : round(($total_buy_user_cnt/$total_app_install_result)*100, 3);
	$total_arppu = ($total_money == 0) ? 0 : round($total_money/$total_buy_user_cnt, 3);
?>
			<tr>
         		<td class="tdl">&nbsp;</td>
        		<td class="tdl point_title">캠페인 <?=number_format(sizeof($contents_list))?>개 결과</td>
            	<td class="tdc "><?=number_format($total_app_install_result)?><br/><span >앱 설치</span></td>
             	<td class="tdr ">&nbsp;</td>
               	<td class="tdr ">$<?=number_format($total_spend/$total_app_install_result,2)?><br/><span >앱 설치 당</span></td>
               	<td class="tdr ">$<?=number_format($total_spend,2) ?><br/><span >총 지출</span></td>
               	<td class="tdr "><?=number_format($total_join_count_result)?><br/><span >총 신규 가입</span></td>
               	<td class="tdr ">$<?=number_format($total_money,2)?><br/><span >총 매출</span></td>
               	<td class="tdr "><?=number_format($total_buy_user_cnt)?><br/><span >총 결제자수</span></td>
               	<td class="tdr "><?=number_format($total_pur, 2)?>%<br/><span >총 PUR</span></td>
               	<td class="tdr ">$<?=number_format($total_arpu, 2)?><br/><span >총 ARPU</span></td>
               	<td class="tdr ">$<?=number_format($total_arppu, 1)?><br/><span >총 ARPPU</span></td>               	
               	<td class="tdr "><?=number_format($total_roi, 2)?>%<br/><span >총 ROI</span></td>
               	<td class="tdl">&nbsp;</td>
         	</tr>
        </tbody>
	</table>
	
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>