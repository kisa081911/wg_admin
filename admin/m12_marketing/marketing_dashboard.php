<?
	$top_menu = "marketing";
	$sub_menu = "dashboard";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$db_main2 = new CDatabase_main2();
	$db_analysis = new CDatabase_Analysis();
	$db_slave_main = new CDatabase_Slave_Main();
	
	$sql = "SELECT subtype, value FROM tbl_dashboard_stat WHERE type = 1001 AND subtype < 100";
	$stat_list = $db_analysis->gettotallist($sql);
	
	// 30day
	$join_count_30day = $stat_list[0]["value"];
	$spend_30day = $stat_list[1]["value"];
	$cpi_30day = ($join_count_30day == 0) ? 0 : round($spend_30day/$join_count_30day, 2);
	$nogame_30day = $stat_list[2]["value"];
	$nogame_per_30day = ($join_count_30day == 0) ? 0 : round(($nogame_30day/$join_count_30day*100), 2);
	$pay_30day = $stat_list[3]["value"];
	$pay_user_cnt_30day = $stat_list[4]["value"];
	$roi_30day = ($spend_30day == 0) ? 0 : round($pay_30day/$spend_30day*100, 2);
	$pur_30day = ($join_count_30day == 0) ? 0 : round($pay_user_cnt_30day/$join_count_30day*100, 2);
	$arppu_30day = ($pay_user_cnt_30day == 0) ? 0 : round($pay_30day/$pay_user_cnt_30day, 2);
	$arpi_30day = ($join_count_30day == 0) ? 0 : round($pay_30day/$join_count_30day, 3);
	
	// 14day
	$join_count_14day = $stat_list[5]["value"];
	$spend_14day = $stat_list[6]["value"];
	$cpi_14day = ($join_count_14day == 0) ? 0 : round($spend_14day/$join_count_14day, 2);
	$nogame_14day = $stat_list[7]["value"];
	$nogame_per_14day = ($join_count_14day == 0) ? 0 : round(($nogame_14day/$join_count_14day*100), 2);
	$pay_14day = $stat_list[8]["value"];
	$pay_user_cnt_14day = $stat_list[9]["value"];
	$roi_14day = ($spend_14day == 0) ? 0 : round($pay_14day/$spend_14day*100, 2);
	$pur_14day = ($join_count_14day == 0) ? 0 : round($pay_user_cnt_14day/$join_count_14day*100, 2);
	$arppu_14day = ($pay_user_cnt_14day == 0) ? 0 : round($pay_14day/$pay_user_cnt_14day, 2);
	$arpi_14day = ($join_count_14day == 0) ? 0 : round($pay_14day/$join_count_14day, 3);
	
	// 7day
	$join_count_7day = $stat_list[10]["value"];
	$spend_7day = $stat_list[11]["value"];
	$cpi_7day = ($join_count_7day == 0) ? 0 : round($spend_7day/$join_count_7day, 2);
	$nogame_7day = $stat_list[12]["value"];
	$nogame_per_7day = ($join_count_7day == 0) ? 0 : round(($nogame_7day/$join_count_7day*100), 2);
	$pay_7day = $stat_list[13]["value"];
	$pay_user_cnt_7day = $stat_list[14]["value"];
	$roi_7day = ($spend_7day == 0) ? 0 : round($pay_7day/$spend_7day*100, 2);
	$pur_7day = ($join_count_7day == 0) ? 0 : round($pay_user_cnt_7day/$join_count_7day*100, 2);
	$arppu_7day = ($pay_user_cnt_7day == 0) ? 0 : round($pay_7day/$pay_user_cnt_7day, 2);
	$arpi_7day = ($join_count_7day == 0) ? 0 : round($pay_7day/$join_count_7day, 3);
	
	// yesterday
	$join_count_yesterday = $stat_list[15]["value"];
	$spend_yesterday = $stat_list[16]["value"];
	$cpi_yesterday = ($join_count_yesterday == 0) ? 0 : round($spend_yesterday/$join_count_yesterday, 2);
	$nogame_yesterday = $stat_list[17]["value"];
	$nogame_per_yesterday = ($join_count_yesterday == 0) ? 0 : round(($nogame_yesterday/$join_count_yesterday*100), 2);
	$pay_yesterday = $stat_list[18]["value"];
	$pay_user_cnt_yesterday = $stat_list[19]["value"];
	$roi_yesterday = ($spend_yesterday == 0) ? 0 : round($pay_yesterday/$spend_yesterday*100, 2);
	$pur_yesterday = ($join_count_yesterday == 0) ? 0 : round($pay_user_cnt_yesterday/$join_count_yesterday*100, 2);
	$arppu_yesterday = ($pay_user_cnt_yesterday == 0) ? 0 : round($pay_yesterday/$pay_user_cnt_yesterday, 2);
	$arpi_yesterday = ($join_count_yesterday == 0) ? 0 : round($pay_yesterday/$join_count_yesterday, 3);
	
	// today
	// web
	$sql = "SELECT COUNT(useridx) ". 
			"FROM tbl_user_ext ".
			"WHERE createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND platform = 0 AND (adflag LIKE 'fbself%')";
	$join_count_today = $db_slave_main->getvalue($sql);
	
	$sql = "SELECT ROUND(IFNULL(SUM(spend), 0), 2) ".
			"FROM tbl_ad_stats ".
			"WHERE today = '$today' ";			
	$spend_today = $db_main2->getvalue($sql);
	
	$cpi_today = ($join_count_today == 0) ? 0 : round($spend_today/$join_count_today, 2);
	
	$sql = "SELECT IFNULL( ".
			"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"	,0) AS join_nogame_today ".
			"FROM tbl_user_ext ".
			"WHERE platform = 0 AND (adflag LIKE 'fbself%' OR adflag = 'nanigans') AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
	$nogame_today = $db_slave_main->getvalue($sql);
	
	$nogame_per_today = ($join_count_today == 0) ? 0 : round(($nogame_today/$join_count_today*100), 2);
	
	$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
			"FROM ( ".
			"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
			"	FROM ( ".
			"		SELECT useridx ".
			"		FROM tbl_user_ext ".
			"		WHERE platform = 0 AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (adflag LIKE 'fbself%') ".
			") t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
			"WHERE t1.useridx > 20000 AND STATUS = 1 ".
			"UNION ALL ".
			"SELECT t1.useridx, money ".
			"FROM ( ".
			"	SELECT useridx ".
			"	FROM tbl_user_ext ".
			"	WHERE platform = 0 AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (adflag LIKE 'fbself%') ".
			") t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
			"WHERE t1.useridx > 20000 AND STATUS = 1 ".
			") total";
	$pay_info_today = $db_slave_main->getarray($sql);
		
	$pay_today = $pay_info_today["money"];
	$pay_user_cnt_today = $pay_info_today["user_cnt"];
	
	$roi_today = ($spend_today == 0) ? 0 : round($pay_today/$spend_today*100, 2);
	$pur_today = ($join_count_today == 0) ? 0 : round($pay_user_cnt_today/$join_count_today*100, 2);
	$arppu_today = ($pay_user_cnt_today == 0) ? 0 : round($pay_today/$pay_user_cnt_today, 2);
	$arpi_today = ($join_count_today == 0) ? 0 : round($pay_today/$join_count_today, 3);
	
	// ios - fbself
	$sql = "SELECT subtype, value FROM tbl_dashboard_stat WHERE type = 1001 AND 100 < subtype AND subtype < 200";
	$stat_list = $db_analysis->gettotallist($sql);
	
	// 30day
	$ios_fbself_join_count_30day = $stat_list[0]["value"];
	$ios_fbself_spend_30day = $stat_list[1]["value"];
	$ios_fbself_cpi_30day = ($ios_fbself_join_count_30day == 0) ? 0 : round($ios_fbself_spend_30day/$ios_fbself_join_count_30day, 2);
	$ios_fbself_nogame_30day = $stat_list[2]["value"];
	$ios_fbself_nogame_per_30day = ($ios_fbself_join_count_30day == 0) ? 0 : round(($ios_fbself_nogame_30day/$ios_fbself_join_count_30day*100), 2);
	$ios_fbself_pay_30day = $stat_list[3]["value"];
	$ios_fbself_pay_user_cnt_30day = $stat_list[4]["value"];
	$ios_fbself_roi_30day = ($ios_fbself_spend_30day == 0) ? 0 : round($ios_fbself_pay_30day/$ios_fbself_spend_30day*100, 2);
	$ios_fbself_pur_30day = ($ios_fbself_join_count_30day == 0) ? 0 : round($ios_fbself_pay_user_cnt_30day/$ios_fbself_join_count_30day*100, 2);
	$ios_fbself_arppu_30day = ($ios_fbself_pay_user_cnt_30day == 0) ? 0 : round($ios_fbself_pay_30day/$ios_fbself_pay_user_cnt_30day, 2);
	$ios_fbself_arpi_30day = ($ios_fbself_join_count_30day == 0) ? 0 : round($ios_fbself_pay_30day/$ios_fbself_join_count_30day, 3);
	
	// 14day
	$ios_fbself_join_count_14day = $stat_list[5]["value"];
	$ios_fbself_spend_14day = $stat_list[6]["value"];
	$ios_fbself_cpi_14day = ($ios_fbself_join_count_14day == 0) ? 0 : round($ios_fbself_spend_14day/$ios_fbself_join_count_14day, 2);
	$ios_fbself_nogame_14day = $stat_list[7]["value"];
	$ios_fbself_nogame_per_14day = ($ios_fbself_join_count_14day == 0) ? 0 : round(($ios_fbself_nogame_14day/$ios_fbself_join_count_14day*100), 2);
	$ios_fbself_pay_14day = $stat_list[8]["value"];
	$ios_fbself_pay_user_cnt_14day = $stat_list[9]["value"];
	$ios_fbself_roi_14day = ($ios_fbself_spend_14day == 0) ? 0 : round($ios_fbself_pay_14day/$ios_fbself_spend_14day*100, 2);
	$ios_fbself_pur_14day = ($ios_fbself_join_count_14day == 0) ? 0 : round($ios_fbself_pay_user_cnt_14day/$ios_fbself_join_count_14day*100, 2);
	$ios_fbself_arppu_14day = ($ios_fbself_pay_user_cnt_14day == 0) ? 0 : round($ios_fbself_pay_14day/$ios_fbself_pay_user_cnt_14day, 2);
	$ios_fbself_arpi_14day = ($ios_fbself_join_count_14day == 0) ? 0 : round($ios_fbself_pay_14day/$ios_fbself_join_count_14day, 3);
	
	// 7day
	$ios_fbself_join_count_7day = $stat_list[10]["value"];
	$ios_fbself_spend_7day = $stat_list[11]["value"];
	$ios_fbself_cpi_7day = ($ios_fbself_join_count_7day == 0) ? 0 : round($ios_fbself_spend_7day/$ios_fbself_join_count_7day, 2);
	$ios_fbself_nogame_7day = $stat_list[12]["value"];
	$ios_fbself_nogame_per_7day = ($ios_fbself_join_count_7day == 0) ? 0 : round(($ios_fbself_nogame_7day/$ios_fbself_join_count_7day*100), 2);
	$ios_fbself_pay_7day = $stat_list[13]["value"];
	$ios_fbself_pay_user_cnt_7day = $stat_list[14]["value"];
	$ios_fbself_roi_7day = ($ios_fbself_spend_7day == 0) ? 0 : round($ios_fbself_pay_7day/$ios_fbself_spend_7day*100, 2);
	$ios_fbself_pur_7day = ($ios_fbself_join_count_7day == 0) ? 0 : round($ios_fbself_pay_user_cnt_7day/$ios_fbself_join_count_7day*100, 2);
	$ios_fbself_arppu_7day = ($ios_fbself_pay_user_cnt_7day == 0) ? 0 : round($ios_fbself_pay_7day/$ios_fbself_pay_user_cnt_7day, 2);
	$ios_fbself_arpi_7day = ($ios_fbself_join_count_7day == 0) ? 0 : round($ios_fbself_pay_7day/$ios_fbself_join_count_7day, 3);
	
	// yesterday
	$ios_fbself_join_count_yesterday = $stat_list[15]["value"];
	$ios_fbself_spend_yesterday = $stat_list[16]["value"];
	$ios_fbself_cpi_yesterday = ($ios_fbself_join_count_yesterday == 0) ? 0 : round($ios_fbself_spend_yesterday/$ios_fbself_join_count_yesterday, 2);
	$ios_fbself_nogame_yesterday = $stat_list[17]["value"];
	$ios_fbself_nogame_per_yesterday = ($ios_fbself_join_count_yesterday == 0) ? 0 : round(($ios_fbself_nogame_yesterday/$ios_fbself_join_count_yesterday*100), 2);
	$ios_fbself_pay_yesterday = $stat_list[18]["value"];
	$ios_fbself_pay_user_cnt_yesterday = $stat_list[19]["value"];
	$ios_fbself_roi_yesterday = ($ios_fbself_spend_yesterday == 0) ? 0 : round($ios_fbself_pay_yesterday/$ios_fbself_spend_yesterday*100, 2);
	$ios_fbself_pur_yesterday = ($ios_fbself_join_count_yesterday == 0) ? 0 : round($ios_fbself_pay_user_cnt_yesterday/$ios_fbself_join_count_yesterday*100, 2);
	$ios_fbself_arppu_yesterday = ($ios_fbself_pay_user_cnt_yesterday == 0) ? 0 : round($ios_fbself_pay_yesterday/$ios_fbself_pay_user_cnt_yesterday, 2);
	$ios_fbself_arpi_yesterday = ($ios_fbself_join_count_yesterday == 0) ? 0 : round($ios_fbself_pay_yesterday/$ios_fbself_join_count_yesterday, 3);
	
	$sql = "SELECT COUNT(useridx) ".
			"FROM tbl_user_ext ".
			"WHERE createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND platform = 1";
	$ios_fbself_join_count_today = $db_slave_main->getvalue($sql);
	
	$sql = "SELECT ROUND(IFNULL(SUM(spend), 0), 2) ".
			"FROM tbl_ad_stats_mobile ".
			"WHERE today = '$today' AND platform = 1 ";
	$ios_fbself_spend_today = $db_main2->getvalue($sql);
	
	$ios_fbself_cpi_today = ($ios_fbself_join_count_today == 0) ? 0 : round($ios_fbself_spend_today/$ios_fbself_join_count_today, 2);
	
	$sql = "SELECT IFNULL( ".
			"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"	,0) AS join_nogame_today ".
			"FROM tbl_user_ext ".
			"WHERE platform = 1 AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
	$ios_fbself_nogame_today = $db_slave_main->getvalue($sql);
	
	$ios_fbself_nogame_per_today = ($ios_fbself_join_count_today == 0) ? 0 : round(($ios_fbself_nogame_today/$ios_fbself_join_count_today*100), 2);
	
	$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
			"FROM ( ".
			"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
			"	FROM ( ".
			"		SELECT useridx ".
			"		FROM tbl_user_ext ".
			"		WHERE platform = 1 AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
			"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ". 
			"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
			"	UNION ALL ".
			"	SELECT t1.useridx, money ".
			"	FROM ( ".
			"		SELECT useridx ".
			"		FROM tbl_user_ext ".
			"		WHERE platform = 1 AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
			"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
			"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
			") total ";
	$ios_fbself_pay_info_today = $db_slave_main->getarray($sql);
	
	$ios_fbself_pay_today = $ios_fbself_pay_info_today["money"];
	$ios_fbself_pay_user_cnt_today = $ios_fbself_pay_info_today["user_cnt"];
	
	$ios_fbself_roi_today = ($ios_fbself_spend_today == 0) ? 0 : round($ios_fbself_pay_today/$ios_fbself_spend_today*100, 2);
	$ios_fbself_pur_today = ($ios_fbself_join_count_today == 0) ? 0 : round($ios_fbself_pay_user_cnt_today/$ios_fbself_join_count_today*100, 2);
	$ios_fbself_arppu_today = ($ios_fbself_pay_user_cnt_today == 0) ? 0 : round($ios_fbself_pay_today/$ios_fbself_pay_user_cnt_today, 2);
	$ios_fbself_arpi_today = ($ios_fbself_join_count_today == 0) ? 0 : round($ios_fbself_pay_today/$ios_fbself_join_count_today, 3);
	
	// ios - agency
	$sql = "SELECT subtype, value FROM tbl_dashboard_stat WHERE type = 1001 AND 400 < subtype AND subtype < 500";
	$stat_list = $db_analysis->gettotallist($sql);
	
	// 30day
	$ios_agency_join_count_30day = $stat_list[0]["value"];
	$ios_agency_spend_30day = $stat_list[1]["value"];
	$ios_agency_cpi_30day = ($ios_agency_join_count_30day == 0) ? 0 : round($ios_agency_spend_30day/$ios_agency_join_count_30day, 2);
	$ios_agency_nogame_30day = $stat_list[2]["value"];
	$ios_agency_nogame_per_30day = ($ios_agency_join_count_30day == 0) ? 0 : round(($ios_agency_nogame_30day/$ios_agency_join_count_30day*100), 2);
	$ios_agency_pay_30day = $stat_list[3]["value"];
	$ios_agency_pay_user_cnt_30day = $stat_list[4]["value"];
	$ios_agency_roi_30day = ($ios_agency_spend_30day == 0) ? 0 : round($ios_agency_pay_30day/$ios_agency_spend_30day*100, 2);
	$ios_agency_pur_30day = ($ios_agency_join_count_30day == 0) ? 0 : round($ios_agency_pay_user_cnt_30day/$ios_agency_join_count_30day*100, 2);
	$ios_agency_arppu_30day = ($ios_agency_pay_user_cnt_30day == 0) ? 0 : round($ios_agency_pay_30day/$ios_agency_pay_user_cnt_30day, 2);
	$ios_agency_arpi_30day = ($ios_agency_join_count_30day == 0) ? 0 : round($ios_agency_pay_30day/$ios_agency_join_count_30day, 3);
	
	// 14day
	$ios_agency_join_count_14day = $stat_list[5]["value"];
	$ios_agency_spend_14day = $stat_list[6]["value"];
	$ios_agency_cpi_14day = ($ios_agency_join_count_14day == 0) ? 0 : round($ios_agency_spend_14day/$ios_agency_join_count_14day, 2);
	$ios_agency_nogame_14day = $stat_list[7]["value"];
	$ios_agency_nogame_per_14day = ($ios_agency_join_count_14day == 0) ? 0 : round(($ios_agency_nogame_14day/$ios_agency_join_count_14day*100), 2);
	$ios_agency_pay_14day = $stat_list[8]["value"];
	$ios_agency_pay_user_cnt_14day = $stat_list[9]["value"];
	$ios_agency_roi_14day = ($ios_agency_spend_14day == 0) ? 0 : round($ios_agency_pay_14day/$ios_agency_spend_14day*100, 2);
	$ios_agency_pur_14day = ($ios_agency_join_count_14day == 0) ? 0 : round($ios_agency_pay_user_cnt_14day/$ios_agency_join_count_14day*100, 2);
	$ios_agency_arppu_14day = ($ios_agency_pay_user_cnt_14day == 0) ? 0 : round($ios_agency_pay_14day/$ios_agency_pay_user_cnt_14day, 2);
	$ios_agency_arpi_14day = ($ios_agency_join_count_14day == 0) ? 0 : round($ios_agency_pay_14day/$ios_agency_join_count_14day, 3);
	
	// 7day
	$ios_agency_join_count_7day = $stat_list[10]["value"];
	$ios_agency_spend_7day = $stat_list[11]["value"];
	$ios_agency_cpi_7day = ($ios_agency_join_count_7day == 0) ? 0 : round($ios_agency_spend_7day/$ios_agency_join_count_7day, 2);
	$ios_agency_nogame_7day = $stat_list[12]["value"];
	$ios_agency_nogame_per_7day = ($ios_agency_join_count_7day == 0) ? 0 : round(($ios_agency_nogame_7day/$ios_agency_join_count_7day*100), 2);
	$ios_agency_pay_7day = $stat_list[13]["value"];
	$ios_agency_pay_user_cnt_7day = $stat_list[14]["value"];
	$ios_agency_roi_7day = ($ios_agency_spend_7day == 0) ? 0 : round($ios_agency_pay_7day/$ios_agency_spend_7day*100, 2);
	$ios_agency_pur_7day = ($ios_agency_join_count_7day == 0) ? 0 : round($ios_agency_pay_user_cnt_7day/$ios_agency_join_count_7day*100, 2);
	$ios_agency_arppu_7day = ($ios_agency_pay_user_cnt_7day == 0) ? 0 : round($ios_agency_pay_7day/$ios_agency_pay_user_cnt_7day, 2);
	$ios_agency_arpi_7day = ($ios_agency_join_count_7day == 0) ? 0 : round($ios_agency_pay_7day/$ios_agency_join_count_7day, 3);
	
	// yesterday
	$ios_agency_join_count_yesterday = $stat_list[15]["value"];
	$ios_agency_spend_yesterday = $stat_list[16]["value"];
	$ios_agency_cpi_yesterday = ($ios_agency_join_count_yesterday == 0) ? 0 : round($ios_agency_spend_yesterday/$ios_agency_join_count_yesterday, 2);
	$ios_agency_nogame_yesterday = $stat_list[17]["value"];
	$ios_agency_nogame_per_yesterday = ($ios_agency_join_count_yesterday == 0) ? 0 : round(($ios_agency_nogame_yesterday/$ios_agency_join_count_yesterday*100), 2);
	$ios_agency_pay_yesterday = $stat_list[18]["value"];
	$ios_agency_pay_user_cnt_yesterday = $stat_list[19]["value"];
	$ios_agency_roi_yesterday = ($ios_agency_spend_yesterday == 0) ? 0 : round($ios_agency_pay_yesterday/$ios_agency_spend_yesterday*100, 2);
	$ios_agency_pur_yesterday = ($ios_agency_join_count_yesterday == 0) ? 0 : round($ios_agency_pay_user_cnt_yesterday/$ios_agency_join_count_yesterday*100, 2);
	$ios_agency_arppu_yesterday = ($ios_agency_pay_user_cnt_yesterday == 0) ? 0 : round($ios_agency_pay_yesterday/$ios_agency_pay_user_cnt_yesterday, 2);
	$ios_agency_arpi_yesterday = ($ios_agency_join_count_yesterday == 0) ? 0 : round($ios_agency_pay_yesterday/$ios_agency_join_count_yesterday, 3);
	
	$sql = "SELECT COUNT(useridx) ".
			"FROM tbl_user_ext ".
			"WHERE createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND platform = 1 AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ";
	$ios_agency_join_count_today = $db_slave_main->getvalue($sql);
	
	$sql = "SELECT IFNULL(SUM(spend), 0) ".
			"FROM tbl_agency_spend_daily ".
			"WHERE today = '$today' AND platform = 1 AND (agencyname NOT LIKE 'Face%') ";
	$ios_agency_spend_today = $db_main2->getvalue($sql);
	
	$ios_agency_cpi_today = ($ios_agency_join_count_today == 0) ? 0 : round($ios_agency_spend_today/$ios_agency_join_count_today, 2);
	
	$sql = "SELECT IFNULL( ".
			"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"	,0) AS join_nogame_today ".
			"FROM tbl_user_ext ".
			"WHERE platform = 1 AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
	$ios_agency_nogame_today = $db_slave_main->getvalue($sql);
	
	$ios_agency_nogame_per_today = ($ios_agency_join_count_today == 0) ? 0 : round(($ios_agency_nogame_today/$ios_agency_join_count_today*100), 2);
	
	$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
			"FROM ( ".
			"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
			"	FROM ( ".
			"		SELECT useridx ".
			"		FROM tbl_user_ext ".
			"		WHERE platform = 1 AND createdate BETWEEN 'today 00:00:00' AND 'today 23:59:59' AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
			"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
			"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
			"	UNION ALL ".
			"	SELECT t1.useridx, money ".
			"	FROM ( ".
			"		SELECT useridx ".
			"		FROM tbl_user_ext ".
			"		WHERE platform = 1 AND createdate BETWEEN 'today 00:00:00' AND 'today 23:59:59' AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
			"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
			"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
			") total ";
	$ios_agency_pay_info_today = $db_slave_main->getarray($sql);
	
	$ios_agency_pay_today = $ios_agency_pay_info_today["money"];
	$ios_agency_pay_user_cnt_today = $ios_agency_pay_info_today["user_cnt"];
	
	$ios_agency_roi_today = ($ios_agency_spend_today == 0) ? 0 : round($ios_agency_pay_today/$ios_agency_spend_today*100, 2);
	$ios_agency_pur_today = ($ios_agency_join_count_today == 0) ? 0 : round($ios_agency_pay_user_cnt_today/$ios_agency_join_count_today*100, 2);
	$ios_agency_arppu_today = ($ios_agency_pay_user_cnt_today == 0) ? 0 : round($ios_agency_pay_today/$ios_agency_pay_user_cnt_today, 2);
	$ios_agency_arpi_today = ($ios_agency_join_count_today == 0) ? 0 : round($ios_agency_pay_today/$ios_agency_join_count_today, 3);

	// android - fbself
	$sql = "SELECT subtype, value FROM tbl_dashboard_stat WHERE  type = 1001 AND 200 < subtype AND subtype < 300 ";
	$stat_list = $db_analysis->gettotallist($sql);
	
	// 30day
	$and_fbself_join_count_30day = $stat_list[0]["value"];
	$and_fbself_spend_30day = $stat_list[1]["value"];
	$and_fbself_cpi_30day = ($and_fbself_join_count_30day == 0) ? 0 : round($and_fbself_spend_30day/$and_fbself_join_count_30day, 2);
	$and_fbself_nogame_30day = $stat_list[2]["value"];
	$and_fbself_nogame_per_30day = ($and_fbself_join_count_30day == 0) ? 0 : round(($and_fbself_nogame_30day/$and_fbself_join_count_30day*100), 2);
	$and_fbself_pay_30day = $stat_list[3]["value"];
	$and_fbself_pay_user_cnt_30day = $stat_list[4]["value"];
	$and_fbself_roi_30day = ($and_fbself_spend_30day == 0) ? 0 : round($and_fbself_pay_30day/$and_fbself_spend_30day*100, 2);
	$and_fbself_pur_30day = ($and_fbself_join_count_30day == 0) ? 0 : round($and_fbself_pay_user_cnt_30day/$and_fbself_join_count_30day*100, 2);
	$and_fbself_arppu_30day = ($and_fbself_pay_user_cnt_30day == 0) ? 0 : round($and_fbself_pay_30day/$and_fbself_pay_user_cnt_30day, 2);
	$and_fbself_arpi_30day = ($and_fbself_join_count_30day == 0) ? 0 : round($and_fbself_pay_30day/$and_fbself_join_count_30day, 3);
	
	// 14day
	$and_fbself_join_count_14day = $stat_list[5]["value"];
	$and_fbself_spend_14day = $stat_list[6]["value"];
	$and_fbself_cpi_14day = ($and_fbself_join_count_14day == 0) ? 0 : round($and_fbself_spend_14day/$and_fbself_join_count_14day, 2);
	$and_fbself_nogame_14day = $stat_list[7]["value"];
	$and_fbself_nogame_per_14day = ($and_fbself_join_count_14day == 0) ? 0 : round(($and_fbself_nogame_14day/$and_fbself_join_count_14day*100), 2);
	$and_fbself_pay_14day = $stat_list[8]["value"];
	$and_fbself_pay_user_cnt_14day = $stat_list[9]["value"];
	$and_fbself_roi_14day = ($and_fbself_spend_14day == 0) ? 0 : round($and_fbself_pay_14day/$and_fbself_spend_14day*100, 2);
	$and_fbself_pur_14day = ($and_fbself_join_count_14day == 0) ? 0 : round($and_fbself_pay_user_cnt_14day/$and_fbself_join_count_14day*100, 2);
	$and_fbself_arppu_14day = ($and_fbself_pay_user_cnt_14day == 0) ? 0 : round($and_fbself_pay_14day/$and_fbself_pay_user_cnt_14day, 2);
	$and_fbself_arpi_14day = ($and_fbself_join_count_14day == 0) ? 0 : round($and_fbself_pay_14day/$and_fbself_join_count_14day, 3);
	
	// 7day
	$and_fbself_join_count_7day = $stat_list[10]["value"];
	$and_fbself_spend_7day = $stat_list[11]["value"];
	$and_fbself_cpi_7day = ($and_fbself_join_count_7day == 0) ? 0 : round($and_fbself_spend_7day/$and_fbself_join_count_7day, 2);
	$and_fbself_nogame_7day = $stat_list[12]["value"];
	$and_fbself_nogame_per_7day = ($and_fbself_join_count_7day == 0) ? 0 : round(($and_fbself_nogame_7day/$and_fbself_join_count_7day*100), 2);
	$and_fbself_pay_7day = $stat_list[13]["value"];
	$and_fbself_pay_user_cnt_7day = $stat_list[14]["value"];
	$and_fbself_roi_7day = ($and_fbself_spend_7day == 0) ? 0 : round($and_fbself_pay_7day/$and_fbself_spend_7day*100, 2);
	$and_fbself_pur_7day = ($and_fbself_join_count_7day == 0) ? 0 : round($and_fbself_pay_user_cnt_7day/$and_fbself_join_count_7day*100, 2);
	$and_fbself_arppu_7day = ($and_fbself_pay_user_cnt_7day == 0) ? 0 : round($and_fbself_pay_7day/$and_fbself_pay_user_cnt_7day, 2);
	$and_fbself_arpi_7day = ($and_fbself_join_count_7day == 0) ? 0 : round($and_fbself_pay_7day/$and_fbself_join_count_7day, 3);
	
	// yesterday
	$and_fbself_join_count_yesterday = $stat_list[15]["value"];
	$and_fbself_spend_yesterday = $stat_list[16]["value"];
	$and_fbself_cpi_yesterday = ($and_fbself_join_count_yesterday == 0) ? 0 : round($and_fbself_spend_yesterday/$and_fbself_join_count_yesterday, 2);
	$and_fbself_nogame_yesterday = $stat_list[17]["value"];
	$and_fbself_nogame_per_yesterday = ($and_fbself_join_count_yesterday == 0) ? 0 : round(($and_fbself_nogame_yesterday/$and_fbself_join_count_yesterday*100), 2);
	$and_fbself_pay_yesterday = $stat_list[18]["value"];
	$and_fbself_pay_user_cnt_yesterday = $stat_list[19]["value"];
	$and_fbself_roi_yesterday = ($and_fbself_spend_yesterday == 0) ? 0 : round($and_fbself_pay_yesterday/$and_fbself_spend_yesterday*100, 2);
	$and_fbself_pur_yesterday = ($and_fbself_join_count_yesterday == 0) ? 0 : round($and_fbself_pay_user_cnt_yesterday/$and_fbself_join_count_yesterday*100, 2);
	$and_fbself_arppu_yesterday = ($and_fbself_pay_user_cnt_yesterday == 0) ? 0 : round($and_fbself_pay_yesterday/$and_fbself_pay_user_cnt_yesterday, 2);
	$and_fbself_arpi_yesterday = ($and_fbself_join_count_yesterday == 0) ? 0 : round($and_fbself_pay_yesterday/$and_fbself_join_count_yesterday, 3);
	
	$sql = "SELECT COUNT(useridx) ".
			"FROM tbl_user_ext ".
			"WHERE createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND platform = 2 AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%')";
	$and_fbself_join_count_today = $db_slave_main->getvalue($sql);
	
	$sql = "SELECT ROUND(IFNULL(SUM(spend), 0), 2) ".
			"FROM tbl_ad_stats_mobile ".
			"WHERE today = '$today' AND platform = 2 ";
	$and_fbself_spend_today = $db_main2->getvalue($sql);
	
	$and_fbself_cpi_today = ($and_fbself_join_count_today == 0) ? 0 : round($and_fbself_spend_today/$and_fbself_join_count_today, 2);
	
	$sql = "SELECT IFNULL( ".
			"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"	,0) AS join_nogame_today ".
			"FROM tbl_user_ext ".
			"WHERE platform = 2 AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
	$and_fbself_nogame_today = $db_slave_main->getvalue($sql);
	
	$and_fbself_nogame_per_today = ($and_fbself_join_count_today == 0) ? 0 : round(($and_fbself_nogame_today/$and_fbself_join_count_today*100), 2);
	
	$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
			"FROM ( ".
			"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
			"	FROM ( ".
			"		SELECT useridx ".
			"		FROM tbl_user_ext ".
			"		WHERE platform = 2 AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
			"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
			"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
			"	UNION ALL ".
			"	SELECT t1.useridx, money ".
			"	FROM ( ".
			"		SELECT useridx ".
			"		FROM tbl_user_ext ".
			"		WHERE platform = 2 AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (adflag LIKE 'Face%' OR adflag LIKE 'm_d%') ".
			"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
			"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
			") total";
	$and_fbself_pay_info_today = $db_slave_main->getarray($sql);
	
	$and_fbself_pay_today = $and_fbself_pay_info_today["money"];
	$and_fbself_pay_user_cnt_today = $and_fbself_pay_info_today["user_cnt"];
	
	$and_fbself_roi_today = ($and_fbself_spend_today == 0) ? 0 : round($and_fbself_pay_today/$and_fbself_spend_today*100, 2);
	$and_fbself_pur_today = ($and_fbself_join_count_today == 0) ? 0 : round($and_fbself_pay_user_cnt_today/$and_fbself_join_count_today*100, 2);
	$and_fbself_arppu_today = ($and_fbself_pay_user_cnt_today == 0) ? 0 : round($and_fbself_pay_today/$and_fbself_pay_user_cnt_today, 2);
	$and_fbself_arpi_today = ($and_fbself_join_count_today == 0) ? 0 : round($and_fbself_pay_today/$and_fbself_join_count_today, 3);
	
	// android - agency
	$sql = "SELECT subtype, value FROM tbl_dashboard_stat WHERE type = 1001 AND 500 < subtype AND subtype < 600";
	$stat_list = $db_analysis->gettotallist($sql);
	
	// 30day
	$and_agency_join_count_30day = $stat_list[0]["value"];
	$and_agency_spend_30day = $stat_list[1]["value"];
	$and_agency_cpi_30day = ($and_agency_join_count_30day == 0) ? 0 : round($and_agency_spend_30day/$and_agency_join_count_30day, 2);
	$and_agency_nogame_30day = $stat_list[2]["value"];
	$and_agency_nogame_per_30day = ($and_agency_join_count_30day == 0) ? 0 : round(($and_agency_nogame_30day/$and_agency_join_count_30day*100), 2);
	$and_agency_pay_30day = $stat_list[3]["value"];
	$and_agency_pay_user_cnt_30day = $stat_list[4]["value"];
	$and_agency_roi_30day = ($and_agency_spend_30day == 0) ? 0 : round($and_agency_pay_30day/$and_agency_spend_30day*100, 2);
	$and_agency_pur_30day = ($and_agency_join_count_30day == 0) ? 0 : round($and_agency_pay_user_cnt_30day/$and_agency_join_count_30day*100, 2);
	$and_agency_arppu_30day = ($and_agency_pay_user_cnt_30day == 0) ? 0 : round($and_agency_pay_30day/$and_agency_pay_user_cnt_30day, 2);
	$and_agency_arpi_30day = ($and_agency_join_count_30day == 0) ? 0 : round($and_agency_pay_30day/$and_agency_join_count_30day, 3);
	
	// 14day
	$and_agency_join_count_14day = $stat_list[5]["value"];
	$and_agency_spend_14day = $stat_list[6]["value"];
	$and_agency_cpi_14day = ($and_agency_join_count_14day == 0) ? 0 : round($and_agency_spend_14day/$and_agency_join_count_14day, 2);
	$and_agency_nogame_14day = $stat_list[7]["value"];
	$and_agency_nogame_per_14day = ($and_agency_join_count_14day == 0) ? 0 : round(($and_agency_nogame_14day/$and_agency_join_count_14day*100), 2);
	$and_agency_pay_14day = $stat_list[8]["value"];
	$and_agency_pay_user_cnt_14day = $stat_list[9]["value"];
	$and_agency_roi_14day = ($and_agency_spend_14day == 0) ? 0 : round($and_agency_pay_14day/$and_agency_spend_14day*100, 2);
	$and_agency_pur_14day = ($and_agency_join_count_14day == 0) ? 0 : round($and_agency_pay_user_cnt_14day/$and_agency_join_count_14day*100, 2);
	$and_agency_arppu_14day = ($and_agency_pay_user_cnt_14day == 0) ? 0 : round($and_agency_pay_14day/$and_agency_pay_user_cnt_14day, 2);
	$and_agency_arpi_14day = ($and_agency_join_count_14day == 0) ? 0 : round($and_agency_pay_14day/$and_agency_join_count_14day, 3);
	
	// 7day
	$and_agency_join_count_7day = $stat_list[10]["value"];
	$and_agency_spend_7day = $stat_list[11]["value"];
	$and_agency_cpi_7day = ($and_agency_join_count_7day == 0) ? 0 : round($and_agency_spend_7day/$and_agency_join_count_7day, 2);
	$and_agency_nogame_7day = $stat_list[12]["value"];
	$and_agency_nogame_per_7day = ($and_agency_join_count_7day == 0) ? 0 : round(($and_agency_nogame_7day/$and_agency_join_count_7day*100), 2);
	$and_agency_pay_7day = $stat_list[13]["value"];
	$and_agency_pay_user_cnt_7day = $stat_list[14]["value"];
	$and_agency_roi_7day = ($and_agency_spend_7day == 0) ? 0 : round($and_agency_pay_7day/$and_agency_spend_7day*100, 2);
	$and_agency_pur_7day = ($and_agency_join_count_7day == 0) ? 0 : round($and_agency_pay_user_cnt_7day/$and_agency_join_count_7day*100, 2);
	$and_agency_arppu_7day = ($and_agency_pay_user_cnt_7day == 0) ? 0 : round($and_agency_pay_7day/$and_agency_pay_user_cnt_7day, 2);
	$and_agency_arpi_7day = ($and_agency_join_count_7day == 0) ? 0 : round($and_agency_pay_7day/$and_agency_join_count_7day, 3);
	
	// yesterday
	$and_agency_join_count_yesterday = $stat_list[15]["value"];
	$and_agency_spend_yesterday = $stat_list[16]["value"];
	$and_agency_cpi_yesterday = ($and_agency_join_count_yesterday == 0) ? 0 : round($and_agency_spend_yesterday/$and_agency_join_count_yesterday, 2);
	$and_agency_nogame_yesterday = $stat_list[17]["value"];
	$and_agency_nogame_per_yesterday = ($and_agency_join_count_yesterday == 0) ? 0 : round(($and_agency_nogame_yesterday/$and_agency_join_count_yesterday*100), 2);
	$and_agency_pay_yesterday = $stat_list[18]["value"];
	$and_agency_pay_user_cnt_yesterday = $stat_list[19]["value"];
	$and_agency_roi_yesterday = ($and_agency_spend_yesterday == 0) ? 0 : round($and_agency_pay_yesterday/$and_agency_spend_yesterday*100, 2);
	$and_agency_pur_yesterday = ($and_agency_join_count_yesterday == 0) ? 0 : round($and_agency_pay_user_cnt_yesterday/$and_agency_join_count_yesterday*100, 2);
	$and_agency_arppu_yesterday = ($and_agency_pay_user_cnt_yesterday == 0) ? 0 : round($and_agency_pay_yesterday/$and_agency_pay_user_cnt_yesterday, 2);
	$and_agency_arpi_yesterday = ($and_agency_join_count_yesterday == 0) ? 0 : round($and_agency_pay_yesterday/$and_agency_join_count_yesterday, 3);
	
	$sql = "SELECT COUNT(useridx) ".
			"FROM tbl_user_ext ".
			"WHERE createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND platform = 2 AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ";
	$and_agency_join_count_today = $db_slave_main->getvalue($sql);
	
	$sql = "SELECT IFNULL(SUM(spend), 0) ".
			"FROM tbl_agency_spend_daily ".
			"WHERE today = '$today' AND platform = 2 AND (agencyname LIKE 'Face%')";
	$and_agency_spend_today = $db_main2->getvalue($sql);
	
	$and_agency_cpi_today = ($and_agency_join_count_today == 0) ? 0 : round($and_agency_spend_today/$and_agency_join_count_today, 2);
	
	$sql = "SELECT IFNULL( ".
			"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"	,0) AS join_nogame_today ".
			"FROM tbl_user_ext ".
			"WHERE platform = 2 AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
	$and_agency_nogame_today = $db_slave_main->getvalue($sql);
	
	$and_agency_nogame_per_today = ($and_agency_join_count_today == 0) ? 0 : round(($and_agency_nogame_today/$and_agency_join_count_today*100), 2);
	
	$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
			"FROM ( ".
			"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
			"	FROM ( ".
			"		SELECT useridx ".
			"		FROM tbl_user_ext ".
			"		WHERE platform = 2 AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
			"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
			"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
			"	UNION ALL ".
			"	SELECT t1.useridx, money ".
			"	FROM ( ".
			"		SELECT useridx ".
			"		FROM tbl_user_ext ".
			"		WHERE platform = 2 AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (adflag LIKE '%_int' OR adflag LIKE '%_int_viral') AND adflag NOT LIKE 'm_d%' ".
			"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
			"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
			") total";
	$and_agency_pay_info_today = $db_slave_main->getarray($sql);
	
	$and_agency_pay_today = $and_agency_pay_info_today["money"];
	$and_agency_pay_user_cnt_today = $and_agency_pay_info_today["user_cnt"];
	
	$and_agency_roi_today = ($and_agency_spend_today == 0) ? 0 : round($and_agency_pay_today/$and_agency_spend_today*100, 2);
	$and_agency_pur_today = ($and_agency_join_count_today == 0) ? 0 : round($and_agency_pay_user_cnt_today/$and_agency_join_count_today*100, 2);
	$and_agency_arppu_today = ($and_agency_pay_user_cnt_today == 0) ? 0 : round($and_agency_pay_today/$and_agency_pay_user_cnt_today, 2);
	$and_agency_arpi_today = ($and_agency_join_count_today == 0) ? 0 : round($and_agency_pay_today/$and_agency_join_count_today, 3);
	
 	// amazon
	$sql = "SELECT subtype, value FROM tbl_dashboard_stat WHERE type = 1001 AND 300 < subtype AND subtype < 400";
	$stat_list = $db_analysis->gettotallist($sql);
	
	// 30day
	$amazon_fbself_join_count_30day = $stat_list[0]["value"];
	$amazon_fbself_spend_30day = $stat_list[1]["value"];
	$amazon_fbself_cpi_30day = ($amazon_fbself_join_count_30day == 0) ? 0 : round($amazon_fbself_spend_30day/$amazon_fbself_join_count_30day, 2);
	$amazon_fbself_nogame_30day = $stat_list[2]["value"];
	$amazon_fbself_nogame_per_30day = ($amazon_fbself_join_count_30day == 0) ? 0 : round(($amazon_fbself_nogame_30day/$amazon_fbself_join_count_30day*100), 2);
	$amazon_fbself_pay_30day = $stat_list[3]["value"];
	$amazon_fbself_pay_user_cnt_30day = $stat_list[4]["value"];
	$amazon_fbself_roi_30day = ($amazon_fbself_spend_30day == 0) ? 0 : round($amazon_fbself_pay_30day/$amazon_fbself_spend_30day*100, 2);
	$amazon_fbself_pur_30day = ($amazon_fbself_join_count_30day == 0) ? 0 : round($amazon_fbself_pay_user_cnt_30day/$amazon_fbself_join_count_30day*100, 2);
	$amazon_fbself_arppu_30day = ($amazon_fbself_pay_user_cnt_30day == 0) ? 0 : round($amazon_fbself_pay_30day/$amazon_fbself_pay_user_cnt_30day, 2);
	$amazon_fbself_arpi_30day = ($amazon_fbself_join_count_30day == 0) ? 0 : round($amazon_fbself_pay_30day/$amazon_fbself_join_count_30day, 3);
	
	// 14day
	$amazon_fbself_join_count_14day = $stat_list[5]["value"];
	$amazon_fbself_spend_14day = $stat_list[6]["value"];
	$amazon_fbself_cpi_14day = ($amazon_fbself_join_count_14day == 0) ? 0 : round($amazon_fbself_spend_14day/$amazon_fbself_join_count_14day, 2);
	$amazon_fbself_nogame_14day = $stat_list[7]["value"];
	$amazon_fbself_nogame_per_14day = ($amazon_fbself_join_count_14day == 0) ? 0 : round(($amazon_fbself_nogame_14day/$amazon_fbself_join_count_14day*100), 2);
	$amazon_fbself_pay_14day = $stat_list[8]["value"];
	$amazon_fbself_pay_user_cnt_14day = $stat_list[9]["value"];
	$amazon_fbself_roi_14day = ($amazon_fbself_spend_14day == 0) ? 0 : round($amazon_fbself_pay_14day/$amazon_fbself_spend_14day*100, 2);
	$amazon_fbself_pur_14day = ($amazon_fbself_join_count_14day == 0) ? 0 : round($amazon_fbself_pay_user_cnt_14day/$amazon_fbself_join_count_14day*100, 2);
	$amazon_fbself_arppu_14day = ($amazon_fbself_pay_user_cnt_14day == 0) ? 0 : round($amazon_fbself_pay_14day/$amazon_fbself_pay_user_cnt_14day, 2);
	$amazon_fbself_arpi_14day = ($amazon_fbself_join_count_14day == 0) ? 0 : round($amazon_fbself_pay_14day/$amazon_fbself_join_count_14day, 3);
	
	// 7day
	$amazon_fbself_join_count_7day = $stat_list[10]["value"];
	$amazon_fbself_spend_7day = $stat_list[11]["value"];
	$amazon_fbself_cpi_7day = ($amazon_fbself_join_count_7day == 0) ? 0 : round($amazon_fbself_spend_7day/$amazon_fbself_join_count_7day, 2);
	$amazon_fbself_nogame_7day = $stat_list[12]["value"];
	$amazon_fbself_nogame_per_7day = ($amazon_fbself_join_count_7day == 0) ? 0 : round(($amazon_fbself_nogame_7day/$amazon_fbself_join_count_7day*100), 2);
	$amazon_fbself_pay_7day = $stat_list[13]["value"];
	$amazon_fbself_pay_user_cnt_7day = $stat_list[14]["value"];
	$amazon_fbself_roi_7day = ($amazon_fbself_spend_7day == 0) ? 0 : round($amazon_fbself_pay_7day/$amazon_fbself_spend_7day*100, 2);
	$amazon_fbself_pur_7day = ($amazon_fbself_join_count_7day == 0) ? 0 : round($amazon_fbself_pay_user_cnt_7day/$amazon_fbself_join_count_7day*100, 2);
	$amazon_fbself_arppu_7day = ($amazon_fbself_pay_user_cnt_7day == 0) ? 0 : round($amazon_fbself_pay_7day/$amazon_fbself_pay_user_cnt_7day, 2);
	$amazon_fbself_arpi_7day = ($amazon_fbself_join_count_7day == 0) ? 0 : round($amazon_fbself_pay_7day/$amazon_fbself_join_count_7day, 3);
	
	// yesterday
	$amazon_fbself_join_count_yesterday = $stat_list[15]["value"];
	$amazon_fbself_spend_yesterday = $stat_list[16]["value"];
	$amazon_fbself_cpi_yesterday = ($amazon_fbself_join_count_yesterday == 0) ? 0 : round($amazon_fbself_spend_yesterday/$amazon_fbself_join_count_yesterday, 2);
	$amazon_fbself_nogame_yesterday = $stat_list[17]["value"];
	$amazon_fbself_nogame_per_yesterday = ($amazon_fbself_join_count_yesterday == 0) ? 0 : round(($amazon_fbself_nogame_yesterday/$amazon_fbself_join_count_yesterday*100), 2);
	$amazon_fbself_pay_yesterday = $stat_list[18]["value"];
	$amazon_fbself_pay_user_cnt_yesterday = $stat_list[19]["value"];
	$amazon_fbself_roi_yesterday = ($amazon_fbself_spend_yesterday == 0) ? 0 : round($amazon_fbself_pay_yesterday/$amazon_fbself_spend_yesterday*100, 2);
	$amazon_fbself_pur_yesterday = ($amazon_fbself_join_count_yesterday == 0) ? 0 : round($amazon_fbself_pay_user_cnt_yesterday/$amazon_fbself_join_count_yesterday*100, 2);
	$amazon_fbself_arppu_yesterday = ($amazon_fbself_pay_user_cnt_yesterday == 0) ? 0 : round($amazon_fbself_pay_yesterday/$amazon_fbself_pay_user_cnt_yesterday, 2);
	$amazon_fbself_arpi_yesterday = ($amazon_fbself_join_count_yesterday == 0) ? 0 : round($amazon_fbself_pay_yesterday/$amazon_fbself_join_count_yesterday, 3);
	
	$sql = "SELECT COUNT(useridx) ".
			"FROM tbl_user_ext ".
			"WHERE createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND platform = 3 AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%')";
	$amazon_fbself_join_count_today = $db_slave_main->getvalue($sql);

	$sql = "SELECT ROUND(IFNULL(SUM(spend), 0), 2) ".
			"FROM tbl_agency_spend_daily ".
			"WHERE today = '$today' AND platform = 3 ";
	$amazon_fbself_spend_today = $db_main2->getvalue($sql);
	
	$amazon_fbself_cpi_today = ($amazon_fbself_join_count_today == 0) ? 0 : round($amazon_fbself_spend_today/$amazon_fbself_join_count_today, 2);
	
	$sql = "SELECT IFNULL( ".
			"	SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"		- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0) ".
			"	,0) AS join_nogame_today ".
			"FROM tbl_user_ext ".
			"WHERE platform = 3 AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
	
	$amazon_fbself_nogame_today = $db_slave_main->getvalue($sql);
	$amazon_fbself_nogame_per_today = ($amazon_fbself_join_count_today == 0) ? 0 : round(($amazon_fbself_nogame_today/$amazon_fbself_join_count_today*100), 2);
	
	$sql = "SELECT COUNT(DISTINCT useridx) AS user_cnt, IFNULL(ROUND(SUM(money), 2), 0) AS money ".
			"FROM ( ".
			"	SELECT t1.useridx, ROUND(facebookcredit/10, 2) AS money ".
			"	FROM ( ".
			"		SELECT useridx ".
			"		FROM tbl_user_ext ".
			"		WHERE platform = 3 AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') ".
			"	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
			"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
			"	UNION ALL ".
			"	SELECT t1.useridx, money ".
			"	FROM ( ".
			"		SELECT useridx ".
			"		FROM tbl_user_ext ".
			"		WHERE platform = 3 AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') ".
			"	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
			"	WHERE t1.useridx > 20000 AND STATUS = 1 ".
			") total";
	
	$amazon_fbself_pay_info_today = $db_slave_main->getarray($sql);
	
	$amazon_fbself_pay_today = $amazon_fbself_pay_info_today["money"];
	$amazon_fbself_pay_user_cnt_today = $amazon_fbself_pay_info_today["user_cnt"];
	
	$amazon_fbself_roi_today = ($amazon_fbself_spend_today == 0) ? 0 : round($amazon_fbself_pay_today/$amazon_fbself_spend_today*100, 2);
	$amazon_fbself_pur_today = ($amazon_fbself_join_count_today == 0) ? 0 : round($amazon_fbself_pay_user_cnt_today/$amazon_fbself_join_count_today*100, 2);
	$amazon_fbself_arppu_today = ($amazon_fbself_pay_user_cnt_today == 0) ? 0 : round($amazon_fbself_pay_today/$amazon_fbself_pay_user_cnt_today, 2);
	$amazon_fbself_arpi_today = ($amazon_fbself_join_count_today == 0) ? 0 : round($amazon_fbself_pay_today/$amazon_fbself_join_count_today, 3);
	
	$db_main2->end();
	$db_analysis->end();
	$db_slave_main->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="/js/ui/ui.core.js"></script>
<script type="text/javascript" src="/js/ui/ui.datepicker.js"></script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title" style="padding-right:10px;">Marketing Dash Board</div>
	</div>
	<!-- //title_warp -->
	
	<a style="font:12px;color:#000;font-weight:bold;cursor:ponter;">Web</a>
	<table class="tbl_list_basic1" style="margin-top:5px">
		<colgroup>
			<col width="60">
			<col width="50">
			<col width="100">
			<col width="80">
			<col width="100">
			<col width="80">
			<col width="80">
			<col width="90">
			<col width="80">
			<col width="80">
			<col width="80">         
		</colgroup>
		<thead>
			<tr>
				<th class="tdc">기간</th>
				<th class="tdr">회원가입</th>
				<th class="tdr">비용</th>
				<th class="tdr">CPI</th>
				<th class="tdr">미게임건수(%)</th>
				<th class="tdr">결제자수</th>
				<th class="tdr">결제비율</th>
				<th class="tdr">결제금액</th>
				<th class="tdr">ROI</th>
				<th class="tdr">ARPPU</th>
				<th class="tdr">ARPI</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="tdc point">최근30일</td>
				<td class="tdr"><?= number_format($join_count_30day) ?></td>
				<td class="tdr">$<?= number_format($spend_30day, 1) ?></td>
				<td class="tdr">$<?= number_format($cpi_30day, 2) ?></td>
				<td class="tdr"><?= number_format($nogame_30day)?> (<?= number_format($nogame_per_30day, 2)?>%)</td>
				<td class="tdr"><?= number_format($pay_user_cnt_30day)?></td>
				<td class="tdr"><?= number_format($pur_30day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($pay_30day, 1)?></td>
				<td class="tdr"><?= number_format($roi_30day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($arppu_30day, 2) ?></td>
				<td class="tdr">$<?= number_format($arpi_30day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">최근14일</td>
				<td class="tdr"><?= number_format($join_count_14day) ?></td>
				<td class="tdr">$<?= number_format($spend_14day, 1) ?></td>
				<td class="tdr">$<?= number_format($cpi_14day, 2) ?></td>
				<td class="tdr"><?= number_format($nogame_14day)?> (<?= number_format($nogame_per_14day, 2)?>%)</td>
				<td class="tdr"><?= number_format($pay_user_cnt_14day) ?></td>
				<td class="tdr"><?= number_format($pur_14day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($pay_14day, 1)?></td>
				<td class="tdr"><?= number_format($roi_14day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($arppu_14day, 2) ?></td>
				<td class="tdr">$<?= number_format($arpi_14day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">최근7일</td>
				<td class="tdr"><?= number_format($join_count_7day) ?></td>
				<td class="tdr">$<?= number_format($spend_7day, 1) ?></td>
				<td class="tdr">$<?= number_format($cpi_7day, 2) ?></td>
				<td class="tdr"><?= number_format($nogame_7day)?> (<?= number_format($nogame_per_7day, 2)?>%)</td>
				<td class="tdr"><?= number_format($pay_user_cnt_7day)?></td>
				<td class="tdr"><?= number_format($pur_7day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($pay_7day, 1)?></td>
				<td class="tdr"><?= number_format($roi_7day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($arppu_7day, 2) ?></td>
				<td class="tdr">$<?= number_format($arpi_7day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">어제</td>
				<td class="tdr"><?= number_format($join_count_yesterday) ?></td>
				<td class="tdr">$<?= number_format($spend_yesterday, 1) ?></td>
				<td class="tdr">$<?= number_format($cpi_yesterday, 2) ?></td>
				<td class="tdr"><?= number_format($nogame_yesterday)?> (<?= number_format($nogame_per_yesterday, 2)?>%)</td>
				<td class="tdr"><?= number_format($pay_user_cnt_yesterday)?></td>
				<td class="tdr"><?= number_format($pur_yesterday, 2) ?>%</td>
				<td class="tdr">$<?= number_format($pay_yesterday, 1)?></td>
				<td class="tdr"><?= number_format($roi_yesterday, 2) ?>%</td>
				<td class="tdr">$<?= number_format($arppu_yesterday, 2) ?></td>
				<td class="tdr">$<?= number_format($arpi_yesterday, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point_title">오늘</td>
				<td class="tdr point_title"><?= number_format($join_count_today) ?></td>
				<td class="tdr point_title">$<?= number_format($spend_today, 1) ?></td>
				<td class="tdr point_title">$<?= number_format($cpi_today, 2) ?></td>
				<td class="tdr point_title"><?= number_format($nogame_today)?> (<?= number_format($nogame_per_today, 2)?>%)</td>
				<td class="tdr"><?= number_format($pay_user_cnt_today)?></td>
				<td class="tdr"><?= number_format($pur_today, 2) ?>%</td>
				<td class="tdr">$<?= number_format($pay_today, 1)?></td>
				<td class="tdr"><?= number_format($roi_today, 2) ?>%</td>
				<td class="tdr">$<?= number_format($arppu_today, 2) ?></td>
				<td class="tdr">$<?= number_format($arpi_today, 3) ?></td>
			</tr>
		</tbody>
	</table>            

	<div class="clear"></div>
	<br/><br/>
	<a style="font:12px;color:#000;font-weight:bold;cursor:ponter;">IOS - fbself</a>
	<table class="tbl_list_basic1" style="margin-top:5px">
		<colgroup>
			<col width="60">
			<col width="50">
			<col width="100">
			<col width="80">
			<col width="100">
			<col width="80">
			<col width="80">
			<col width="90">
			<col width="80">
			<col width="80">
			<col width="80">         
		</colgroup>
		<thead>
			<tr>
				<th class="tdc">기간</th>
				<th class="tdr">회원가입</th>
				<th class="tdr">비용</th>
				<th class="tdr">CPI</th>
				<th class="tdr">미게임건수(%)</th>
				<th class="tdr">결제자수</th>
				<th class="tdr">결제비율</th>
				<th class="tdr">결제금액</th>
				<th class="tdr">ROI</th>
				<th class="tdr">ARPPU</th>
				<th class="tdr">ARPI</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="tdc point">최근30일</td>
				<td class="tdr"><?= number_format($ios_fbself_join_count_30day) ?></td>
				<td class="tdr">$<?= number_format($ios_fbself_spend_30day, 1) ?></td>
				<td class="tdr">$<?= number_format($ios_fbself_cpi_30day, 2) ?></td>
				<td class="tdr"><?= number_format($ios_fbself_nogame_30day)?> (<?= number_format($ios_fbself_nogame_per_30day, 2)?>%)</td>
				<td class="tdr"><?= number_format($ios_fbself_pay_user_cnt_30day)?></td>
				<td class="tdr"><?= number_format($ios_fbself_pur_30day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_fbself_pay_30day, 1)?></td>
				<td class="tdr"><?= number_format($ios_fbself_roi_30day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_fbself_arppu_30day, 2) ?></td>
				<td class="tdr">$<?= number_format($ios_fbself_arpi_30day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">최근14일</td>
				<td class="tdr"><?= number_format($ios_fbself_join_count_14day) ?></td>
				<td class="tdr">$<?= number_format($ios_fbself_spend_14day, 1) ?></td>
				<td class="tdr">$<?= number_format($ios_fbself_cpi_14day, 2) ?></td>
				<td class="tdr"><?= number_format($ios_fbself_nogame_14day)?> (<?= number_format($ios_fbself_nogame_per_14day, 2)?>%)</td>
				<td class="tdr"><?= number_format($ios_fbself_pay_user_cnt_14day)?></td>
				<td class="tdr"><?= number_format($ios_fbself_pur_14day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_fbself_pay_14day, 1)?></td>
				<td class="tdr"><?= number_format($ios_fbself_roi_14day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_fbself_arppu_14day, 2) ?></td>
				<td class="tdr">$<?= number_format($ios_fbself_arpi_14day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">최근7일</td>
				<td class="tdr"><?= number_format($ios_fbself_join_count_7day) ?></td>
				<td class="tdr">$<?= number_format($ios_fbself_spend_7day, 1) ?></td>
				<td class="tdr">$<?= number_format($ios_fbself_cpi_7day, 2) ?></td>
				<td class="tdr"><?= number_format($ios_fbself_nogame_7day)?> (<?= number_format($ios_fbself_nogame_per_7day, 2)?>%)</td>
				<td class="tdr"><?= number_format($ios_fbself_pay_user_cnt_7day)?></td>
				<td class="tdr"><?= number_format($ios_fbself_pur_7day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_fbself_pay_7day, 1)?></td>
				<td class="tdr"><?= number_format($ios_fbself_roi_7day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_fbself_arppu_7day, 2) ?></td>
				<td class="tdr">$<?= number_format($ios_fbself_arpi_7day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">어제</td>
				<td class="tdr"><?= number_format($ios_fbself_join_count_yesterday) ?></td>
				<td class="tdr">$<?= number_format($ios_fbself_spend_yesterday, 1) ?></td>
				<td class="tdr">$<?= number_format($ios_fbself_cpi_yesterday, 2) ?></td>
				<td class="tdr"><?= number_format($ios_fbself_nogame_yesterday)?> (<?= number_format($ios_fbself_nogame_per_yesterday, 2)?>%)</td>
				<td class="tdr"><?= number_format($ios_fbself_pay_user_cnt_yesterday)?></td>
				<td class="tdr"><?= number_format($ios_fbself_pur_yesterday, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_fbself_pay_yesterday, 1)?></td>
				<td class="tdr"><?= number_format($ios_fbself_roi_yesterday, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_fbself_arppu_yesterday, 2) ?></td>
				<td class="tdr">$<?= number_format($ios_fbself_arpi_yesterday, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point_title">오늘</td>
				<td class="tdr point_title"><?= number_format($ios_fbself_join_count_today) ?></td>
				<td class="tdr point_title">$<?= number_format($ios_fbself_spend_today, 1) ?></td>
				<td class="tdr point_title">$<?= number_format($ios_fbself_cpi_today, 2) ?></td>
				<td class="tdr point_title"><?= number_format($ios_fbself_nogame_today)?> (<?= number_format($ios_fbself_nogame_per_today, 2)?>%)</td>
				<td class="tdr"><?= number_format($ios_fbself_pay_user_cnt_today)?></td>
				<td class="tdr"><?= number_format($ios_fbself_pur_today, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_fbself_pay_today, 1)?></td>
				<td class="tdr"><?= number_format($ios_fbself_roi_today, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_fbself_arppu_today, 2) ?></td>
				<td class="tdr">$<?= number_format($ios_fbself_arpi_today, 3) ?></td>
			</tr>
		</tbody>
	</table>
	
	<div class="clear"></div>
	<br/><br/>
	<a style="font:12px;color:#000;font-weight:bold;cursor:ponter;">IOS - agency</a>
	<table class="tbl_list_basic1" style="margin-top:5px">
		<colgroup>
			<col width="60">
			<col width="50">
			<col width="100">
			<col width="80">
			<col width="100">
			<col width="80">
			<col width="80">
			<col width="90">
			<col width="80">
			<col width="80">
			<col width="80">         
		</colgroup>
		<thead>
			<tr>
				<th class="tdc">기간</th>
				<th class="tdr">회원가입</th>
				<th class="tdr">비용</th>
				<th class="tdr">CPI</th>
				<th class="tdr">미게임건수(%)</th>
				<th class="tdr">결제자수</th>
				<th class="tdr">결제비율</th>
				<th class="tdr">결제금액</th>
				<th class="tdr">ROI</th>
				<th class="tdr">ARPPU</th>
				<th class="tdr">ARPI</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="tdc point">최근30일</td>
				<td class="tdr"><?= number_format($ios_agency_join_count_30day) ?></td>
				<td class="tdr">$<?= number_format($ios_agency_spend_30day, 1) ?></td>
				<td class="tdr">$<?= number_format($ios_agency_cpi_30day, 2) ?></td>
				<td class="tdr"><?= number_format($ios_agency_nogame_30day)?> (<?= number_format($ios_agency_nogame_per_30day, 2)?>%)</td>
				<td class="tdr"><?= number_format($ios_agency_pay_user_cnt_30day)?></td>
				<td class="tdr"><?= number_format($ios_agency_pur_30day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_agency_pay_30day, 1)?></td>
				<td class="tdr"><?= number_format($ios_agency_roi_30day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_agency_arppu_30day, 2) ?></td>
				<td class="tdr">$<?= number_format($ios_agency_arpi_30day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">최근14일</td>
				<td class="tdr"><?= number_format($ios_agency_join_count_14day) ?></td>
				<td class="tdr">$<?= number_format($ios_agency_spend_14day, 1) ?></td>
				<td class="tdr">$<?= number_format($ios_agency_cpi_14day, 2) ?></td>
				<td class="tdr"><?= number_format($ios_agency_nogame_14day)?> (<?= number_format($ios_agency_nogame_per_14day, 2)?>%)</td>
				<td class="tdr"><?= number_format($ios_agency_pay_user_cnt_14day)?></td>
				<td class="tdr"><?= number_format($ios_agency_pur_14day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_agency_pay_14day, 1)?></td>
				<td class="tdr"><?= number_format($ios_agency_roi_14day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_agency_arppu_14day, 2) ?></td>
				<td class="tdr">$<?= number_format($ios_agency_arpi_14day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">최근7일</td>
				<td class="tdr"><?= number_format($ios_agency_join_count_7day) ?></td>
				<td class="tdr">$<?= number_format($ios_agency_spend_7day, 1) ?></td>
				<td class="tdr">$<?= number_format($ios_agency_cpi_7day, 2) ?></td>
				<td class="tdr"><?= number_format($ios_agency_nogame_7day)?> (<?= number_format($ios_agency_nogame_per_7day, 2)?>%)</td>
				<td class="tdr"><?= number_format($ios_agency_pay_user_cnt_7day)?></td>
				<td class="tdr"><?= number_format($ios_agency_pur_7day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_agency_pay_7day, 1)?></td>
				<td class="tdr"><?= number_format($ios_agency_roi_7day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_agency_arppu_7day, 2) ?></td>
				<td class="tdr">$<?= number_format($ios_agency_arpi_7day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">어제</td>
				<td class="tdr"><?= number_format($ios_agency_join_count_yesterday) ?></td>
				<td class="tdr">$<?= number_format($ios_agency_spend_yesterday, 1) ?></td>
				<td class="tdr">$<?= number_format($ios_agency_cpi_yesterday, 2) ?></td>
				<td class="tdr"><?= number_format($ios_agency_nogame_yesterday)?> (<?= number_format($ios_agency_nogame_per_yesterday, 2)?>%)</td>
				<td class="tdr"><?= number_format($ios_agency_pay_user_cnt_yesterday)?></td>
				<td class="tdr"><?= number_format($ios_agency_pur_yesterday, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_agency_pay_yesterday, 1)?></td>
				<td class="tdr"><?= number_format($ios_agency_roi_yesterday, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_agency_arppu_yesterday, 2) ?></td>
				<td class="tdr">$<?= number_format($ios_agency_arpi_yesterday, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point_title">오늘</td>
				<td class="tdr point_title"><?= number_format($ios_agency_join_count_today) ?></td>
				<td class="tdr point_title">$<?= number_format($ios_agency_spend_today, 1) ?></td>
				<td class="tdr point_title">$<?= number_format($ios_agency_cpi_today, 2) ?></td>
				<td class="tdr point_title"><?= number_format($ios_agency_nogame_today)?> (<?= number_format($ios_agency_nogame_per_today, 2)?>%)</td>
				<td class="tdr"><?= number_format($ios_agency_pay_user_cnt_today)?></td>
				<td class="tdr"><?= number_format($ios_agency_pur_today, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_agency_pay_today, 1)?></td>
				<td class="tdr"><?= number_format($ios_agency_roi_today, 2) ?>%</td>
				<td class="tdr">$<?= number_format($ios_agency_arppu_today, 2) ?></td>
				<td class="tdr">$<?= number_format($ios_agency_arpi_today, 3) ?></td>
			</tr>
		</tbody>
	</table> 
	
	<div class="clear"></div>
	<br/><br/>
	<a style="font:12px;color:#000;font-weight:bold;cursor:ponter;">Android - fbself</a>
	<table class="tbl_list_basic1" style="margin-top:5px">
		<colgroup>
			<col width="60">
			<col width="50">
			<col width="100">
			<col width="80">
			<col width="100">
			<col width="80">
			<col width="80">
			<col width="90">
			<col width="80">
			<col width="80">
			<col width="80">         
		</colgroup>
		<thead>
			<tr>
				<th class="tdc">기간</th>
				<th class="tdr">회원가입</th>
				<th class="tdr">비용</th>
				<th class="tdr">CPI</th>
				<th class="tdr">미게임건수(%)</th>
				<th class="tdr">결제자수</th>
				<th class="tdr">결제비율</th>
				<th class="tdr">결제금액</th>
				<th class="tdr">ROI</th>
				<th class="tdr">ARPPU</th>
				<th class="tdr">ARPI</th>
			</tr>
		</thead>
		</thead>
		<tbody>
			<tr>
				<td class="tdc point">최근30일</td>
				<td class="tdr"><?= number_format($and_fbself_join_count_30day) ?></td>
				<td class="tdr">$<?= number_format($and_fbself_spend_30day, 1) ?></td>
				<td class="tdr">$<?= number_format($and_fbself_cpi_30day, 2) ?></td>
				<td class="tdr"><?= number_format($and_fbself_nogame_30day)?> (<?= number_format($and_fbself_nogame_per_30day, 2)?>%)</td>
				<td class="tdr"><?= number_format($and_fbself_pay_user_cnt_30day)?></td>
				<td class="tdr"><?= number_format($and_fbself_pur_30day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_fbself_pay_30day, 1)?></td>
				<td class="tdr"><?= number_format($and_fbself_roi_30day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_fbself_arppu_30day, 2) ?></td>
				<td class="tdr">$<?= number_format($and_fbself_arpi_30day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">최근14일</td>
				<td class="tdr"><?= number_format($and_fbself_join_count_14day) ?></td>
				<td class="tdr">$<?= number_format($and_fbself_spend_14day, 1) ?></td>
				<td class="tdr">$<?= number_format($and_fbself_cpi_14day, 2) ?></td>
				<td class="tdr"><?= number_format($and_fbself_nogame_14day)?> (<?= number_format($and_fbself_nogame_per_14day, 2)?>%)</td>
				<td class="tdr"><?= number_format($and_fbself_pay_user_cnt_14day)?></td>
				<td class="tdr"><?= number_format($and_fbself_pur_14day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_fbself_pay_14day, 1)?></td>
				<td class="tdr"><?= number_format($and_fbself_roi_14day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_fbself_arppu_14day, 2) ?></td>
				<td class="tdr">$<?= number_format($and_fbself_arpi_14day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">최근7일</td>
				<td class="tdr"><?= number_format($and_fbself_join_count_7day) ?></td>
				<td class="tdr">$<?= number_format($and_fbself_spend_7day, 1) ?></td>
				<td class="tdr">$<?= number_format($and_fbself_cpi_7day, 2) ?></td>
				<td class="tdr"><?= number_format($and_fbself_nogame_7day)?> (<?= number_format($and_fbself_nogame_per_7day, 2)?>%)</td>
				<td class="tdr"><?= number_format($and_fbself_pay_user_cnt_7day)?></td>
				<td class="tdr"><?= number_format($and_fbself_pur_7day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_fbself_pay_7day, 1)?></td>
				<td class="tdr"><?= number_format($and_fbself_roi_7day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_fbself_arppu_7day, 2) ?></td>
				<td class="tdr">$<?= number_format($and_fbself_arpi_7day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">어제</td>
				<td class="tdr"><?= number_format($and_fbself_join_count_yesterday) ?></td>
				<td class="tdr">$<?= number_format($and_fbself_spend_yesterday, 1) ?></td>
				<td class="tdr">$<?= number_format($and_fbself_cpi_yesterday, 2) ?></td>
				<td class="tdr"><?= number_format($and_fbself_nogame_yesterday)?> (<?= number_format($and_fbself_nogame_per_yesterday, 2)?>%)</td>
				<td class="tdr"><?= number_format($and_fbself_pay_user_cnt_yesterday)?></td>
				<td class="tdr"><?= number_format($and_fbself_pur_yesterday, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_fbself_pay_yesterday, 1)?></td>
				<td class="tdr"><?= number_format($and_fbself_roi_yesterday, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_fbself_arppu_yesterday, 2) ?></td>
				<td class="tdr">$<?= number_format($and_fbself_arpi_yesterday, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point_title">오늘</td>
				<td class="tdr point_title"><?= number_format($and_fbself_join_count_today) ?></td>
				<td class="tdr point_title">$<?= number_format($and_fbself_spend_today, 1) ?></td>
				<td class="tdr point_title">$<?= number_format($and_fbself_cpi_today, 2) ?></td>
				<td class="tdr point_title"><?= number_format($and_fbself_nogame_today)?> (<?= number_format($and_fbself_nogame_per_today, 2)?>%)</td>
				<td class="tdr"><?= number_format($and_fbself_pay_user_cnt_today)?></td>
				<td class="tdr"><?= number_format($and_fbself_pur_today, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_fbself_pay_today, 1)?></td>
				<td class="tdr"><?= number_format($and_fbself_roi_today, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_fbself_arppu_today, 2) ?></td>
				<td class="tdr">$<?= number_format($and_fbself_arpi_today, 3) ?></td>
			</tr>
		</tbody>
	</table>
	
	<div class="clear"></div>
	<br/><br/>
	<a style="font:12px;color:#000;font-weight:bold;cursor:ponter;">Android - agency</a>
	<table class="tbl_list_basic1" style="margin-top:5px">
		<colgroup>
			<col width="60">
			<col width="50">
			<col width="100">
			<col width="80">
			<col width="100">
			<col width="80">
			<col width="80">
			<col width="90">
			<col width="80">
			<col width="80">
			<col width="80">         
		</colgroup>
		<thead>
			<tr>
				<th class="tdc">기간</th>
				<th class="tdr">회원가입</th>
				<th class="tdr">비용</th>
				<th class="tdr">CPI</th>
				<th class="tdr">미게임건수(%)</th>
				<th class="tdr">결제자수</th>
				<th class="tdr">결제비율</th>
				<th class="tdr">결제금액</th>
				<th class="tdr">ROI</th>
				<th class="tdr">ARPPU</th>
				<th class="tdr">ARPI</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="tdc point">최근30일</td>
				<td class="tdr"><?= number_format($and_agency_join_count_30day) ?></td>
				<td class="tdr">$<?= number_format($and_agency_spend_30day, 1) ?></td>
				<td class="tdr">$<?= number_format($and_agency_cpi_30day, 2) ?></td>
				<td class="tdr"><?= number_format($and_agency_nogame_30day)?> (<?= number_format($and_agency_nogame_per_30day, 2)?>%)</td>
				<td class="tdr"><?= number_format($and_agency_pay_user_cnt_30day)?></td>
				<td class="tdr"><?= number_format($and_agency_pur_30day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_agency_pay_30day, 1)?></td>
				<td class="tdr"><?= number_format($and_agency_roi_30day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_agency_arppu_30day, 2) ?></td>
				<td class="tdr">$<?= number_format($and_agency_arpi_30day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">최근14일</td>
				<td class="tdr"><?= number_format($and_agency_join_count_14day) ?></td>
				<td class="tdr">$<?= number_format($and_agency_spend_14day, 1) ?></td>
				<td class="tdr">$<?= number_format($and_agency_cpi_14day, 2) ?></td>
				<td class="tdr"><?= number_format($and_agency_nogame_14day)?> (<?= number_format($and_agency_nogame_per_14day, 2)?>%)</td>
				<td class="tdr"><?= number_format($and_agency_pay_user_cnt_14day)?></td>
				<td class="tdr"><?= number_format($and_agency_pur_14day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_agency_pay_14day, 1)?></td>
				<td class="tdr"><?= number_format($and_agency_roi_14day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_agency_arppu_14day, 2) ?></td>
				<td class="tdr">$<?= number_format($and_agency_arpi_14day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">최근7일</td>
				<td class="tdr"><?= number_format($and_agency_join_count_7day) ?></td>
				<td class="tdr">$<?= number_format($and_agency_spend_7day, 1) ?></td>
				<td class="tdr">$<?= number_format($and_agency_cpi_7day, 2) ?></td>
				<td class="tdr"><?= number_format($and_agency_nogame_7day)?> (<?= number_format($and_agency_nogame_per_7day, 2)?>%)</td>
				<td class="tdr"><?= number_format($and_agency_pay_user_cnt_7day)?></td>
				<td class="tdr"><?= number_format($and_agency_pur_7day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_agency_pay_7day, 1)?></td>
				<td class="tdr"><?= number_format($and_agency_roi_7day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_agency_arppu_7day, 2) ?></td>
				<td class="tdr">$<?= number_format($and_agency_arpi_7day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">어제</td>
				<td class="tdr"><?= number_format($and_agency_join_count_yesterday) ?></td>
				<td class="tdr">$<?= number_format($and_agency_spend_yesterday, 1) ?></td>
				<td class="tdr">$<?= number_format($and_agency_cpi_yesterday, 2) ?></td>
				<td class="tdr"><?= number_format($and_agency_nogame_yesterday)?> (<?= number_format($and_agency_nogame_per_yesterday, 2)?>%)</td>
				<td class="tdr"><?= number_format($and_agency_pay_user_cnt_yesterday)?></td>
				<td class="tdr"><?= number_format($and_agency_pur_yesterday, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_agency_pay_yesterday, 1)?></td>
				<td class="tdr"><?= number_format($and_agency_roi_yesterday, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_agency_arppu_yesterday, 2) ?></td>
				<td class="tdr">$<?= number_format($and_agency_arpi_yesterday, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point_title">오늘</td>
				<td class="tdr point_title"><?= number_format($and_agency_join_count_today) ?></td>
				<td class="tdr point_title">$<?= number_format($and_agency_spend_today, 1) ?></td>
				<td class="tdr point_title">$<?= number_format($and_agency_cpi_today, 2) ?></td>
				<td class="tdr point_title"><?= number_format($and_agency_nogame_today)?> (<?= number_format($and_agency_nogame_per_today, 2)?>%)</td>
				<td class="tdr"><?= number_format($and_agency_pay_user_cnt_today)?></td>
				<td class="tdr"><?= number_format($and_agency_pur_today, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_agency_pay_today, 1)?></td>
				<td class="tdr"><?= number_format($and_agency_roi_today, 2) ?>%</td>
				<td class="tdr">$<?= number_format($and_agency_arppu_today, 2) ?></td>
				<td class="tdr">$<?= number_format($and_agency_arpi_today, 3) ?></td>
			</tr>
		</tbody>
	</table> 
	
	<br/><br/>
	<a style="font:12px;color:#000;font-weight:bold;cursor:ponter;">Amazon</a>
	<table class="tbl_list_basic1" style="margin-top:5px">
		<colgroup>
			<col width="60">
			<col width="50">
			<col width="100">
			<col width="80">
			<col width="100">
			<col width="80">
			<col width="80">
			<col width="90">
			<col width="80">
			<col width="80">
			<col width="80">         
		</colgroup>
		<thead>
			<tr>
				<th class="tdc">기간</th>
				<th class="tdr">회원가입</th>
				<th class="tdr">비용</th>
				<th class="tdr">CPI</th>
				<th class="tdr">미게임건수(%)</th>
				<th class="tdr">결제자수</th>
				<th class="tdr">결제비율</th>
				<th class="tdr">결제금액</th>
				<th class="tdr">ROI</th>
				<th class="tdr">ARPPU</th>
				<th class="tdr">ARPI</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="tdc point">최근30일</td>
				<td class="tdr"><?= number_format($amazon_fbself_join_count_30day) ?></td>
				<td class="tdr">$<?= number_format($amazon_fbself_spend_30day, 1) ?></td>
				<td class="tdr">$<?= number_format($amazon_fbself_cpi_30day, 2) ?></td>
				<td class="tdr"><?= number_format($amazon_fbself_nogame_30day)?> (<?= number_format($amazon_fbself_nogame_per_30day, 2)?>%)</td>
				<td class="tdr"><?= number_format($amazon_fbself_pay_user_cnt_30day)?></td>
				<td class="tdr"><?= number_format($amazon_fbself_pur_30day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($amazon_fbself_pay_30day, 1)?></td>
				<td class="tdr"><?= number_format($amazon_fbself_roi_30day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($amazon_fbself_arppu_30day, 2) ?></td>
				<td class="tdr">$<?= number_format($amazon_fbself_arpi_30day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">최근14일</td>
				<td class="tdr"><?= number_format($amazon_fbself_join_count_14day) ?></td>
				<td class="tdr">$<?= number_format($amazon_fbself_spend_14day, 1) ?></td>
				<td class="tdr">$<?= number_format($amazon_fbself_cpi_14day, 2) ?></td>
				<td class="tdr"><?= number_format($amazon_fbself_nogame_14day)?> (<?= number_format($amazon_fbself_nogame_per_14day, 2)?>%)</td>
				<td class="tdr"><?= number_format($amazon_fbself_pay_user_cnt_14day)?></td>
				<td class="tdr"><?= number_format($amazon_fbself_pur_14day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($amazon_fbself_pay_14day, 1)?></td>
				<td class="tdr"><?= number_format($amazon_fbself_roi_14day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($amazon_fbself_arppu_14day, 2) ?></td>
				<td class="tdr">$<?= number_format($amazon_fbself_arpi_14day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">최근7일</td>
				<td class="tdr"><?= number_format($amazon_fbself_join_count_7day) ?></td>
				<td class="tdr">$<?= number_format($amazon_fbself_spend_7day, 1) ?></td>
				<td class="tdr">$<?= number_format($amazon_fbself_cpi_7day, 2) ?></td>
				<td class="tdr"><?= number_format($amazon_fbself_nogame_7day)?> (<?= number_format($amazon_fbself_nogame_per_7day, 2)?>%)</td>
				<td class="tdr"><?= number_format($amazon_fbself_pay_user_cnt_7day)?></td>
				<td class="tdr"><?= number_format($amazon_fbself_pur_7day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($amazon_fbself_pay_7day, 1)?></td>
				<td class="tdr"><?= number_format($amazon_fbself_roi_7day, 2) ?>%</td>
				<td class="tdr">$<?= number_format($amazon_fbself_arppu_7day, 2) ?></td>
				<td class="tdr">$<?= number_format($amazon_fbself_arpi_7day, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point">어제</td>
				<td class="tdr"><?= number_format($amazon_fbself_join_count_yesterday) ?></td>
				<td class="tdr">$<?= number_format($amazon_fbself_spend_yesterday, 1) ?></td>
				<td class="tdr">$<?= number_format($amazon_fbself_cpi_yesterday, 2) ?></td>
				<td class="tdr"><?= number_format($amazon_fbself_nogame_yesterday)?> (<?= number_format($amazon_fbself_nogame_per_yesterday, 2)?>%)</td>
				<td class="tdr"><?= number_format($amazon_fbself_pay_user_cnt_yesterday)?></td>
				<td class="tdr"><?= number_format($amazon_fbself_pur_yesterday, 2) ?>%</td>
				<td class="tdr">$<?= number_format($amazon_fbself_pay_yesterday, 1)?></td>
				<td class="tdr"><?= number_format($amazon_fbself_roi_yesterday, 2) ?>%</td>
				<td class="tdr">$<?= number_format($amazon_fbself_arppu_yesterday, 2) ?></td>
				<td class="tdr">$<?= number_format($amazon_fbself_arpi_yesterday, 3) ?></td>
			</tr>
			<tr>
				<td class="tdc point_title">오늘</td>
				<td class="tdr point_title"><?= number_format($amazon_fbself_join_count_today) ?></td>
				<td class="tdr point_title">$<?= number_format($amazon_fbself_spend_today, 1) ?></td>
				<td class="tdr point_title">$<?= number_format($amazon_fbself_cpi_today, 2) ?></td>
				<td class="tdr point_title"><?= number_format($amazon_fbself_nogame_today)?> (<?= number_format($amazon_fbself_nogame_per_today, 2)?>%)</td>
				<td class="tdr"><?= number_format($amazon_fbself_pay_user_cnt_today)?></td>
				<td class="tdr"><?= number_format($amazon_fbself_pur_today, 2) ?>%</td>
				<td class="tdr">$<?= number_format($amazon_fbself_pay_today, 1)?></td>
				<td class="tdr"><?= number_format($amazon_fbself_roi_today, 2) ?>%</td>
				<td class="tdr">$<?= number_format($amazon_fbself_arppu_today, 2) ?></td>
				<td class="tdr">$<?= number_format($amazon_fbself_arpi_today, 3) ?></td>
			</tr>
		</tbody>
	</table>
	
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>