<?
	$top_menu = "marketing";
	$sub_menu = "fb_campaign_week_web";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_startdate = $_GET["start_searchdate"];
	$search_enddate = $_GET["end_searchdate"];
	
	$pay_startdate = $_GET["start_paydate"];
	$pay_enddate = $_GET["end_paydate"];
	
	$search_campaign = $_GET["search_campaign"];
	$search_adset = $_GET["search_adset"];

	$today = date("Y-m-d");
	
	if($search_startdate == "")
		$search_startdate = date("Y-m-d", strtotime("-30 day"));
	
	if($search_enddate == "")
		$search_enddate = $today;
	
	if($pay_startdate == "")
		$pay_startdate = date("Y-m-d", strtotime("-30 day"));
	
	if($pay_enddate == "")
		$pay_enddate = $today;
	
	$pagefield = "sdate=$search_startdate&edate=$search_enddate&pay_sdate=$pay_startdate&pay_edate=$pay_enddate&campaign_id=$search_campaign&adset_id=$search_adset";
	
	$ad_stats_table="tbl_ad_stats";
	
	$search_sql = "";
	
	if($search_campaign != "")
		$search_sql .= " AND campaign_id = $search_campaign ";
	
	if($search_adset != "")
		$search_sql .= " AND adset_id = $search_adset ";

	$db_main2 = new CDatabase_main2();
	
	$sql = "SELECT t3.week_count, start_date, end_date, app_install, spend, IFNULL(pay_user_count, 0) AS pay_user_count, IFNULL(pay_count, 0) AS pay_count, IFNULL(money, 0) AS money ".
			"FROM ( ".
			"	SELECT WEEKOFYEAR(today) AS week_count, MIN(today) AS start_date, MAX(today) AS end_date, SUM(app_install_result) AS app_install, ROUND(SUM(spend), 2) AS spend ".
			"	FROM $ad_stats_table ".
			"	WHERE today BETWEEN '$search_startdate' AND '$search_enddate' $search_sql ".
			"	GROUP BY WEEKOFYEAR(today) ".
			") t3 LEFT JOIN ( ".
			"	SELECT WEEKOFYEAR(DATE_SUB(writedate, INTERVAL dayafterinstall DAY)) AS week_count, SUM(money) AS money, COUNT(DISTINCT useridx) AS pay_user_count, COUNT(orderidx) AS pay_count ".
			"	FROM tbl_user_marketing_order ".
			"	WHERE EXISTS ( ".
			"		SELECT useridx, fb_ad_id, createdate ".
			"		FROM ( ".
			"			SELECT ad_id FROM $ad_stats_table ".
			"			WHERE today BETWEEN '$search_startdate' AND '$search_enddate' $search_sql ". 
			"		) t1 JOIN ( ".
			"			SELECT fb_ad_id, useridx, createdate FROM tbl_user_marketing ".
			"			WHERE '$search_startdate 00:00:00' <= createdate AND createdate <= '$search_enddate 23:59:59' ".
			"		) t2 ON t1.ad_id = t2.fb_ad_id ".
			"		WHERE useridx = tbl_user_marketing_order.useridx ".
			"	) AND '$pay_startdate 00:00:00' <= writedate AND writedate <= '$pay_enddate 23:59:59' ".
			"	GROUP BY WEEKOFYEAR(DATE_SUB(writedate, INTERVAL dayafterinstall DAY)) ".
			") t4 ON t3.week_count = t4.week_count";
	$contents_list = $db_main2->gettotallist($sql);
	
 	$sql = "SELECT campaign_id, campaign_name FROM $ad_stats_table WHERE today BETWEEN '$search_startdate' AND '$search_enddate' GROUP BY campaign_id";
	$campaign_list = $db_main2->gettotallist($sql);
	
	$adset_sql = "";
	
	if($search_campaign != "")
	{
		$sql = "SELECT adset_id, adset_name FROM `$ad_stats_table` WHERE campaign_id = $search_campaign GROUP BY adset_id";
		$adset_list = $db_main2->gettotallist($sql);
	}

	$db_main2->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<link type="text/css" rel="stylesheet" href="../css/marketing.css" />
<script type="text/javascript">	
	$(function() {
		$("#start_searchdate").datepicker({});
		$("#end_searchdate").datepicker({});
		$("#start_paydate").datepicker({});
		$("#end_paydate").datepicker({});
	});
		
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.start_searchdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.start_searchdate.focus();
	        return;
	    } 

	    if (search_form.end_searchdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.end_searchdate.focus();
	        return;
	    } 
	
	    search_form.submit();
	}

	function change_campaign(select_id)
    {
        var param = {};
		param.campaign_id = select_id;		

        if(select_id == "")
        {
			var Obj = "<option value=''>전체</option>";
            $('#search_adset').html(Obj);
        }
        else
        	WG_ajax_list("game/get_fb_adset_info", param, change_campaign_callback, true);
    }

    function change_campaign_callback(result, reason, totalcount, list)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
		{ 
			var optionObj = "";

			optionObj = "<option value=''>전체</option>";
			
			for (var i=0; i<list.length; i++)
			{
				var adset_id = list[i].adset_id;
				var adset_name = list[i].adset_name;

				var Obj = "<option value='" + adset_id + "'>" + adset_name + "</option>";
			
				optionObj += Obj; 
            }

            $('#search_adset').html(optionObj);
		}
    }
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 캠페인 누적 퍼포먼스 - Web</div>
	</div>
	<!-- //title_warp -->
	
	<form name="search_form" id="search_form"  method="get">
		<div class="detail_search_wrap">
			<span class="search_lbl" style="padding-left:270px;">캠페인</span>
			<span class="search_lbl ml10">
				<select id="search_campaign" name="search_campaign" onchange="change_campaign(this.value);" style="width:250px;">
					<option value="">전체</option>
<?
				for($i=0; $i<sizeof($campaign_list); $i++)
				{
					$campaign_id = $campaign_list[$i]["campaign_id"];
					$campaign_name = $campaign_list[$i]["campaign_name"];
?>
					<option value="<?= $campaign_id?>" <?= ($search_campaign==$campaign_id) ? "selected" : "" ?>><?= $campaign_name?></option>
<?
				}
?>
				</select>
			</span>

			<span class="search_lbl">ad set</span>
			<span class="search_lbl ml10">
				<select id="search_adset" name="search_adset" style="width:320px;">
				<option value="">전체</option>
<?
				for($i=0; $i<sizeof($adset_list); $i++)
				{
					$adset_id = $adset_list[$i]["adset_id"];
					$adset_name = $adset_list[$i]["adset_name"];
?>
					<option value="<?= $adset_id?>" <?= ($search_adset==$adset_id) ? "selected" : "" ?>><?= $adset_name?></option>
<?
				}

?>
				</select>
			</span>

			<div class="clear" style="height:5px;"></div>
			
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
			
			<span class="search_lbl" style="padding-left:270px;">기준일</span>
			<span class="search_lbl ml10">
				<input type="text" class="search_text" id="start_searchdate" name="start_searchdate" value="<?= $search_startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_searchdate" name="end_searchdate" value="<?= $search_enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			</span>
			
			<span class="search_lbl" style="padding-left: 57px;">결제일</span>
			<span class="search_lbl ml10">
				<input type="text" class="search_text" id="start_paydate" name="start_paydate" value="<?= $pay_startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_paydate" name="end_paydate" value="<?= $pay_enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			</span>
		</div>
	</form>

	<div class="search_result">
		<span><?= $search_startdate ?></span> ~ <span><?= $search_enddate ?></span> 통계입니다
		<input type="button" class="btn_03" value="Excel 다운로드" onclick="window.location.href='fb_campaign_week_stats_excel.php?<?= $pagefield ?>'">
	</div>
	
	<table class="tbl_list_basic1" style="width:1200px;">
		<colgroup>
			<col width="130">
			<col width="50">
			<col width="40">
            <col width="50">
            <col width="50">
            <col width="40">
            <col width="45">
            <col width="50">
            <col width="40">
            <col width="60">
            <col width="60">
            <col width="40">
            <col width="40">
		</colgroup>
        <thead>
        	<tr>
        		<th class="tdc">구간</th>
            	<th class="tdc">회원가입수</th>
             	<th class="tdc">CPI</th>
               	<th class="tdc">비용</th>
               	<th class="tdc">총결제 </th>
               	<th class="tdc">ROI</th>
               	<th class="tdc">결제자수 </th>
               	<th class="tdc">결제자 비율</th>
               	<th class="tdc">결제건수</th>
               	<th class="tdc">평균 결제 횟수</th>
               	<th class="tdc">평균 결제 금액</th>
               	<th class="tdc">ARPU</th>
               	<th class="tdc">ARPPU</th>
         	</tr>
        </thead>
		<tbody>
<?
	$total_install = 0;
	$total_spend = 0;
	$total_cpi = 0;
	$total_money = 0;
	$total_roi = 0;
	$total_pay_user_count = 0;
	$total_pay_count = 0;
	$total_payer_rate = 0;
	$total_avg_pay_count = 0;
	$total_avg_pay_money = 0;
	$total_arpu = 0;
	$total_arppu = 0;

	for($i=0; $i<sizeof($contents_list); $i++)
	{
		$week_count = $contents_list[$i]["week_count"];
		$start_date = $contents_list[$i]["start_date"];
		$end_date = $contents_list[$i]["end_date"];
		$app_install = $contents_list[$i]["app_install"];
		$spend = $contents_list[$i]["spend"];
		$cpi = ($app_install == 0) ? 0 : round($spend/$app_install, 2);
		$money = $contents_list[$i]["money"];
		$roi = ($spend == 0) ? 0 : round($money/$spend*100, 2);
		$pay_user_count = $contents_list[$i]["pay_user_count"];
		$payer_rate = ($app_install == 0) ? 0 : round($pay_user_count/$app_install*100, 2);
		$pay_count = $contents_list[$i]["pay_count"];
		$avg_pay_count = ($pay_user_count == 0) ? 0 : round($pay_count/$pay_user_count, 2);
		$avg_pay_money = ($pay_count == 0) ? 0 : round($money/$pay_count, 2);
		$arpu = ($app_install == 0) ? 0 : round($money/$app_install, 2);
		$arppu = ($pay_user_count == 0) ? 0 : round($money/$pay_user_count, 2);
		
		$total_install += $app_install;
		$total_spend += $spend;
		$total_money += $money;
		$total_pay_user_count += $pay_user_count;
		$total_pay_count += $pay_count;
?>    
			<tr>
        		<td class="tdc point">구간 <?= $week_count ?> - <?= $start_date?> ~ <?= $end_date?></td>
            	<td class="tdc"><?= number_format($app_install) ?></td>
             	<td class="tdc">$<?=number_format($cpi, 2)?></td>
               	<td class="tdc">$<?=number_format($spend, 2)?></td>
               	<td class="tdc">$<?=number_format($money, 2)?></td>
               	<td class="tdc"><?=number_format($roi, 2)?>%</td>
               	<td class="tdc"><?=number_format($pay_user_count)?></td>
               	<td class="tdc"><?=number_format($payer_rate,2)?>%</td>
               	<td class="tdc"><?=number_format($pay_count)?></td>
               	<td class="tdc"><?=number_format($avg_pay_count, 2)?></td>
               	<td class="tdc">$<?=number_format($avg_pay_money, 2)?></td>
               	<td class="tdc"><?=number_format($arpu, 2)?></td>
               	<td class="tdc"><?=number_format($arppu, 2)?></td>
         	</tr>
<?
	}
	
	$total_cpi = ($total_install == 0) ? 0 : round($total_spend/$total_install, 2);
	$total_roi = ($total_spend == 0) ? 0 : round($total_money/$total_spend*100, 2);
	$total_payer_rate = ($total_install == 0) ? 0 : round($total_pay_user_count/$total_install*100, 2);
	$total_avg_pay_count = ($total_pay_user_count == 0) ? 0 : round($total_pay_count/$total_pay_user_count, 2);
	$total_avg_pay_money = ($total_pay_count == 0) ? 0 : round($total_money/$total_pay_count, 2);
	$total_arpu = ($total_install == 0) ? 0 : round($total_money/$total_install, 2);
	$total_arppu = ($total_pay_user_count == 0) ? 0 : round($total_money/$total_pay_user_count, 2);
?>
			<tr>
        		<td class="tdc point_title">합계</td>
            	<td class="tdc point"><?= number_format($total_install) ?></td>
             	<td class="tdc point">$<?= number_format($total_cpi, 2) ?></td>
               	<td class="tdc point">$<?= number_format($total_spend, 2) ?></td>
               	<td class="tdc point">$<?= number_format($total_money, 2) ?></td>
               	<td class="tdc point"><?= number_format($total_roi, 2) ?>%</td>
               	<td class="tdc point"><?= number_format($total_pay_user_count) ?></td>
               	<td class="tdc point"><?= number_format($total_payer_rate, 2) ?>%</td>
               	<td class="tdc point"><?=number_format($total_pay_count)?></td>
               	<td class="tdc point"><?=number_format($total_avg_pay_count, 2)?></td>
               	<td class="tdc point">$<?=number_format($total_avg_pay_money, 2)?></td>
               	<td class="tdc point"><?=number_format($total_arpu, 2)?></td>
               	<td class="tdc point"><?=number_format($total_arppu, 2)?></td>
         	</tr>
        </tbody>
	</table>
	
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>