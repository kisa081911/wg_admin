<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");		
 	include_once("../common/dbconnect/db_util_redshift.inc.php");
 	ini_set("memory_limit", "-1");
 	check_login();
 	
 	$data_type = $_GET["data_type"];
 	$download_type = $_GET["download_type"];
 	$user_type = $_GET["user_type"];
 	
 	$search_today = $_GET["today"];
 	$search_startdate = $_GET["startdate"];
 	$search_enddate = $_GET["enddate"];
 	
 	$appsflyer_startdate = $_GET["appsflyer_startdate"];
 	$appsflyer_enddate = $_GET["appsflyer_enddate"];
 	
 	$t5_startdate = $_GET["t5_startdate"];
 	$t5_enddate = $_GET["t5_enddate"];
 	
 	$search_platform = $_GET["platform"];
 	$search_adflag = $_GET["adflag"];
 	$search_subtype = $_GET["subtype"];

 	if($search_platform == 0)
 	{
 		$title_platform = "Web";
 	}
 	else if($search_platform == 1)
 	{
 		$title_platform = "iOS";
 	}
 	else
 	{
 		if($search_platform == 2)
 			$title_platform = "Android";
 		else if($search_platform == 3)
 			$title_platform = "Amazon";
 	}
 	
 	if($search_adflag != "")
 	{
 	    if($data_type == 1)
 	    {
 	        if($user_type == 1)
 	            $adflag_tail = " AND media_source LIKE '$search_adflag%' ";
 	            else if($user_type == 2)
 	                $adflag_tail = " AND media_source LIKE '$search_adflag%' ";
 	    }
 	    else
 	    {
 	        if($user_type == 1)
 	            $adflag_tail = " AND adflag LIKE '$search_adflag%' ";
            else if($user_type == 2)
                $adflag_tail = " AND adflag LIKE '$search_adflag%' ";
 	    }
 	}
 	else
 	{
 	    $search_adflag = "전체";
 	    
 	    if($data_type == 2)
 	    {
 	        if($download_type == 1) // duc_user
 	        {
 	            if($user_type == 1) // 신규
 	            {
 	                if($search_platform == 0)
 	                {
 	                    $adflag_tail = " AND (adflag LIKE 'fbself20%' OR adflag LIKE 'fbself30%')";
 	                }
 	                else
 	                {
 	                    $adflag_tail = " AND (adflag LIKE '%_int' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple Search Ads%')";
 	                }
 	            }
 	            else
 	            {
 	                if($search_platform == 0)
 	                {
 	                    $adflag_tail = " AND (adflag LIKE 'fbself20%' OR adflag LIKE 'fbself30%')";
 	                }
 	                else
 	                {
 	                    $adflag_tail = " AND (adflag LIKE '%_int' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple Search Ads%')";
 	                }
 	            }
 	        }
 	        else
 	        {
 	            if($user_type == 1) // 신규
 	            {
 	                if($search_platform == 0)
 	                {
 	                    $adflag_tail = " AND (adflag LIKE 'fbself20%' OR adflag LIKE 'fbself30%')";
 	                }
 	                else
 	                {
 	                    $adflag_tail = " AND (adflag LIKE '%_int' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple Search Ads%')";
 	                }
 	            }
 	            else
 	            {
 	                if($search_platform == 0)
 	                {
 	                    $adflag_tail = " AND (adflag LIKE 'fbself20%' OR adflag LIKE 'fbself30%')";
 	                }
 	                else
 	                {
 	                    $adflag_tail = " AND (adflag LIKE '%_int' OR adflag LIKE 'Facebook Ad%' OR adflag LIKE 'amazon%' OR adflag LIKE 'Apple Search Ads%')";
 	                }
 	            }
 	        }
 	    }
 	    else
 	    {
 	        $adflag_tail = "";
 	    }
 	}
 	
 	$platform_tail = " AND platform = $search_platform ";
 	
 	$db_redshift = new CDatabase_Redshift();
 	
 	if($data_type == 1) // Appsflyer
 	{
	 	if($download_type == 1) // Appsflyer Install Data
	 	{
	 		$filename = "Install_Raw_Data_".$appsflyer_startdate." ~ ".$appsflyer_enddate."_".$search_adflag."(".$title_platform.").xls";
	 		
	 		$sql = "select platform, appsflyer_device_id, install_time, agency, media_source, campaign, fb_campaign_name, fb_adset_name, fb_adgroup_name, keyword, site_id, sub1, country_code, city, device_type, install_useridx, retention_logidx
                    from t5_appsflyer_install
                    where install_time BETWEEN '$appsflyer_startdate 00:00:00' and '$appsflyer_enddate 23:59:59' $adflag_tail $platform_tail
                    order by logidx asc";
			$install_data_list = $db_redshift->gettotallist($sql);
			
			if (sizeof($install_data_list) == 0)
				error_go("저장할 데이터가 없습니다.", "marketing_newuser_stat_daily.php?today=$search_today&startdate=$search_startdate&enddate=$search_enddate&appsflyer_startdate=$appsflyer_startdate&appsflyer_enddate=$appsflyer_enddate&t5_startdate=$t5_startdate&t5_enddate=$t5_enddate&term=$search_platform&week_term=$search_subtype&adflag=$search_adflag");
			
			$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
								"<table border=1>".
								"	<tr>".
								"		<td style='font-weight:bold;'>platform</td>".
								"		<td style='font-weight:bold;'>appsflyerid</td>".
								"		<td style='font-weight:bold;'>install_time</td>".
								"		<td style='font-weight:bold;'>agency</td>".
								"		<td style='font-weight:bold;'>media_source</td>".
								"		<td style='font-weight:bold;'>campaign</td>".
								"		<td style='font-weight:bold;'>fb_campaign_name</td>".
								"		<td style='font-weight:bold;'>fb_adset_name</td>".
								"		<td style='font-weight:bold;'>fb_adgroup_name</td>".
								"		<td style='font-weight:bold;'>keyword</td>".
								"		<td style='font-weight:bold;'>site_id</td>".
								"		<td style='font-weight:bold;'>sub1</td>".
								"		<td style='font-weight:bold;'>country_code</td>".
								"		<td style='font-weight:bold;'>city</td>".
								"		<td style='font-weight:bold;'>device_type</td>".
								"		<td style='font-weight:bold;'>install_useridx</td>".
								"		<td style='font-weight:bold;'>retention_logidx</td>".
								"	</tr>";
			
			for ($i=0; $i < sizeof($install_data_list); $i++)
			{
			    $platform = $install_data_list[$i]["platform"];
			    $appsflyer_device_id = $install_data_list[$i]["appsflyer_device_id"];
			    $install_time = $install_data_list[$i]["install_time"];
			    $agency = $install_data_list[$i]["agency"];
			    $media_source = $install_data_list[$i]["media_source"];
			    $campaign = $install_data_list[$i]["campaign"];
			    $fb_campaign_name = $install_data_list[$i]["fb_campaign_name"];
			    $fb_adset_name = $install_data_list[$i]["fb_adset_name"];
			    $fb_adgroup_name = $install_data_list[$i]["fb_adgroup_name"];
			    $keyword = $install_data_list[$i]["keyword"];
			    $site_id = $install_data_list[$i]["site_id"];
			    $sub1 = $install_data_list[$i]["sub1"];
			    $country_code = $install_data_list[$i]["country_code"];
			    $city = $install_data_list[$i]["city"];
			    $device_type = $install_data_list[$i]["device_type"];
			    $install_useridx = $install_data_list[$i]["install_useridx"];
			    $retention_logidx = $install_data_list[$i]["retention_logidx"];
			
			    $excel_contents .= "<tr>".
			 			    "<td>".$platform."</td>".
			 			    "<td>".$appsflyer_device_id."</td>".
			 			    "<td>".$install_time."</td>".
			 			    "<td>".$agency."</td>".
			 			    "<td>".$media_source."</td>".
			 			    "<td>".$campaign."</td>".
			 			    "<td>".$fb_campaign_name."</td>".
			 			    "<td>".$fb_adset_name."</td>".
			 			    "<td>".$fb_adgroup_name."</td>".
			 			    "<td>".$keyword."</td>".
			 			    "<td>".$site_id."</td>".
			 			    "<td>".$sub1."</td>".
			 			    "<td>".$country_code."</td>".
			 			    "<td>".$city."</td>".
			 			    "<td>".$device_type."</td>".
			 			    "<td>".$install_useridx."</td>".
			 			    "<td>".$retention_logidx."</td>".
			 			    "</tr>";
			}
			
			$excel_contents .= "</table>";
	 	}
	 	else if($download_type == 2) // Appsflyer In-App-Event
	 	{
	 		$filename = "InAppEvent_Raw_Data_".$appsflyer_startdate." ~ ".$appsflyer_enddate."_".$search_adflag."(".$title_platform.").xls";
	 		
	 		$sql = "select useridx, platform, appsflyer_device_id, install_time, event_time, event_value, agency, media_source, campaign, fb_campaign_name, fb_adset_name, fb_adgroup_name, keyword, site_id, sub1, country_code, city, device_type
                    from t5_appsflyer_inappevent
                    WHERE install_time BETWEEN '$appsflyer_startdate 00:00:00' and '$appsflyer_enddate 23:59:59' $adflag_tail $platform_tail
                    order by logidx asc";
	 		$inappevent_list = $db_redshift->gettotallist($sql);

	 		if (sizeof($inappevent_list) == 0)
	 			error_go("저장할 데이터가 없습니다.", "marketing_newuser_stat_daily.php?today=$search_today&startdate=$search_startdate&enddate=$search_enddate&appsflyer_startdate=$appsflyer_startdate&appsflyer_enddate=$appsflyer_enddate&t5_startdate=$t5_startdate&t5_enddate=$t5_enddate&term=$search_platform&week_term=$search_subtype&adflag=$search_adflag");
	 		
 			$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
			 					"<table border=1>".
			 					"	<tr>".
			 					"		<td style='font-weight:bold;'>useridx</td>".
			 					"		<td style='font-weight:bold;'>platform</td>".
			 					"		<td style='font-weight:bold;'>appsflyerid</td>".
			 					"		<td style='font-weight:bold;'>install_time</td>".
			 					"		<td style='font-weight:bold;'>event_time</td>".
			 					"		<td style='font-weight:bold;'>event_value</td>".
			 					"		<td style='font-weight:bold;'>agency</td>".
			 					"		<td style='font-weight:bold;'>media_source</td>".
			 					"		<td style='font-weight:bold;'>campaign</td>".
			 					"		<td style='font-weight:bold;'>fb_campaign_name</td>".
			 					"		<td style='font-weight:bold;'>fb_adset_name</td>".
			 					"		<td style='font-weight:bold;'>fb_adgroup_name</td>".
			 					"		<td style='font-weight:bold;'>keyword</td>".
			 					"		<td style='font-weight:bold;'>site_id</td>".
			 					"		<td style='font-weight:bold;'>sub1</td>".
			 					"		<td style='font-weight:bold;'>country_code</td>".
			 					"		<td style='font-weight:bold;'>city</td>".
			 					"		<td style='font-weight:bold;'>device_type</td>".
			 					"	</tr>";
 		
 			for ($i=0; $i < sizeof($inappevent_list); $i++)
 			{
 			    $useridx = $inappevent_list[$i]["useridx"];
 			    $platform = $inappevent_list[$i]["platform"];
 			    $appsflyer_device_id = $inappevent_list[$i]["appsflyer_device_id"];
 			    $install_time = $inappevent_list[$i]["install_time"];
 			    $event_time = $inappevent_list[$i]["event_time"];
 			    $event_value = $inappevent_list[$i]["event_value"];
 			    $agency = $inappevent_list[$i]["agency"];
 			    $media_source = $inappevent_list[$i]["media_source"];
 			    $campaign = $inappevent_list[$i]["campaign"];
 			    $fb_campaign_name = $inappevent_list[$i]["fb_campaign_name"];
 			    $fb_adset_name = $inappevent_list[$i]["fb_adset_name"];
 			    $fb_adgroup_name = $inappevent_list[$i]["fb_adgroup_name"];
 			    $keyword = $inappevent_list[$i]["keyword"];
 			    $site_id = $inappevent_list[$i]["site_id"];
 			    $sub1 = $inappevent_list[$i]["sub1"];
 			    $country_code = $inappevent_list[$i]["country_code"];
 			    $city = $inappevent_list[$i]["city"];
 			    $device_type = $inappevent_list[$i]["device_type"];
 		
 			    $excel_contents .= "<tr>".
 			 			    "<td>".$useridx."</td>".
 			 			    "<td>".$platform."</td>".
 			 			    "<td>".$appsflyer_device_id."</td>".
 			 			    "<td>".$install_time."</td>".
 			 			    "<td>".$event_time."</td>".
 			 			    "<td>".$event_value."</td>".
 			 			    "<td>".$agency."</td>".
 			 			    "<td>".$media_source."</td>".
 			 			    "<td>".$campaign."</td>".
 			 			    "<td>".$fb_campaign_name."</td>".
 			 			    "<td>".$fb_adset_name."</td>".
 			 			    "<td>".$fb_adgroup_name."</td>".
 			 			    "<td>".$keyword."</td>".
 			 			    "<td>".$site_id."</td>".
 			 			    "<td>".$sub1."</td>".
 			 			    "<td>".$country_code."</td>".
 			 			    "<td>".$city."</td>".
 			 			    "<td>".$device_type."</td>".
 			 			    "</tr>";
 			}
 		
 			$excel_contents .= "</table>";
	 	}
 	}
 	else if($data_type == 2) // DUC 내부 DB
 	{
 		if($download_type == 1) // t5_user
 		{
 			if($user_type == 1) // 신규
 			{
	 			$filename = "t5_user_".$t5_startdate." ~ ".$t5_enddate."_".$search_adflag."(".$title_platform.").xls";
	 			
	 			$sql = "SELECT platform, useridx, adflag, coin, sex, level, experience, isblock, vip_level, mobile_version, os_version, country, logindate, createdate
                        FROM t5_user
                        WHERE useridx > 20000 $adflag_tail $platform_tail
                          AND createdate BETWEEN '$t5_startdate 00:00:00' and '$t5_enddate 23:59:59'
                        ORDER BY useridx ASC";
	 			$t5_user_list = $db_redshift->gettotallist($sql);

	 			if (sizeof($t5_user_list) == 0)
	 				error_go("저장할 데이터가 없습니다.", "marketing_newuser_stat_daily.php?today=$search_today&startdate=$search_startdate&enddate=$search_enddate&appsflyer_startdate=$appsflyer_startdate&appsflyer_enddate=$appsflyer_enddate&t5_startdate=$t5_startdate&t5_enddate=$t5_enddate&term=$search_platform&week_term=$search_subtype&adflag=$search_adflag");
	 					
	 			$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
				 					"<table border=1>".
				 					"	<tr>".
				 					"		<td style='font-weight:bold;'>platform</td>".
				 					"		<td style='font-weight:bold;'>useridx</td>".
				 					"		<td style='font-weight:bold;'>adflag</td>".
				 					"		<td style='font-weight:bold;'>coin</td>".
				 					"		<td style='font-weight:bold;'>sex</td>".
				 					"		<td style='font-weight:bold;'>level</td>".
				 					"		<td style='font-weight:bold;'>experience</td>".
				 					"		<td style='font-weight:bold;'>isblock</td>".
				 					"		<td style='font-weight:bold;'>vip_level</td>".
				 					"		<td style='font-weight:bold;'>mobile_version</td>".
				 					"		<td style='font-weight:bold;'>os_version</td>".
				 					"		<td style='font-weight:bold;'>country</td>".
				 					"		<td style='font-weight:bold;'>logindate</td>".
				 					"		<td style='font-weight:bold;'>createdate</td>".
				 					"	</tr>";
	 				
	 			for ($i=0; $i < sizeof($t5_user_list); $i++)
	 			{
	 			    $platform = $t5_user_list[$i]["platform"];
	 			    $useridx = $t5_user_list[$i]["useridx"];
	 			    $adflag = $t5_user_list[$i]["adflag"];
	 			    $coin = $t5_user_list[$i]["coin"];
	 			    $sex = $t5_user_list[$i]["sex"];
	 			    $level = $t5_user_list[$i]["level"];
	 			    $experience = $t5_user_list[$i]["experience"];
	 			    $isblock = $t5_user_list[$i]["isblock"];
	 			    $vip_level = $t5_user_list[$i]["vip_level"];
	 			    $mobile_version = $t5_user_list[$i]["mobile_version"];
	 			    $os_version = $t5_user_list[$i]["os_version"];
	 			    $country = $t5_user_list[$i]["country"];
	 			    $logindate = $t5_user_list[$i]["logindate"];
	 			    $createdate = $t5_user_list[$i]["createdate"];
	 					
	 			    $excel_contents .= "<tr>".
	 			 			    "<td>".$platform."</td>".
	 			 			    "<td>".$useridx."</td>".
	 			 			    "<td>".$adflag."</td>".
	 			 			    "<td>".$coin."</td>".
	 			 			    "<td>".$sex."</td>".
	 			 			    "<td>".$level."</td>".
	 			 			    "<td>".$experience."</td>".
	 			 			    "<td>".$isblock."</td>".
	 			 			    "<td>".$vip_level."</td>".
	 			 			    "<td>".$mobile_version."</td>".
	 			 			    "<td>".$os_version."</td>".
	 			 			    "<td>".$country."</td>".
	 			 			    "<td>".$logindate."</td>".
	 			 			    "<td>".$createdate."</td>".
	 			 			    "</tr>";
	 			}
	 				
	 			$excel_contents .= "</table>";
 			}
 			else if($user_type == 2) // 복귀일 28일 이상
 			{
 				$filename = "t5_user_".$t5_startdate." ~ ".$t5_enddate."_".$search_adflag."(".$title_platform.").xls";
	 		
 				if($search_platform == 0)
 				{
 				    $sql = "SELECT rtidx, 0 as platform, t1.useridx, adflag, fbsource as site_id, is_payer, leavedays, createdate, writedate
                            FROM
                            (
                              SELECT useridx, min(rtidx) as min_rtidx, max(leavedays) as max_leavedays
                              FROM t5_user_retention_log
                              WHERE useridx > 20000 AND writedate BETWEEN '$t5_startdate 00:00:00' and '$t5_enddate 23:59:59' $adflag_tail
                              GROUP BY useridx
                            ) t1 join t5_user_retention_log t2 on t1.min_rtidx = t2.rtidx
                            order by writedate asc";
 				}
 				else
 				{
 				    $sql = "SELECT rtidx, platform, t1.useridx, adflag, site_id, is_payer, leavedays, createdate, writedate
                            FROM
                            (
                              SELECT useridx, min(rtidx) as min_rtidx, max(leavedays) as max_leavedays
                              FROM t5_user_retention_mobile_log
                              WHERE useridx > 20000 AND writedate BETWEEN '$t5_startdate 00:00:00' and '$t5_enddate 23:59:59' $platform_tail $adflag_tail
                              GROUP BY useridx
                            ) t1 join t5_user_retention_mobile_log t2 on t1.min_rtidx = t2.rtidx
                            order by writedate asc";
 				}
	 			$t5_user_list = $db_redshift->gettotallist($sql);
	 			
	 			if (sizeof($t5_user_list) == 0)
	 				error_go("저장할 데이터가 없습니다.", "marketing_newuser_stat_daily.php?today=$search_today&startdate=$search_startdate&enddate=$search_enddate&appsflyer_startdate=$appsflyer_startdate&appsflyer_enddate=$appsflyer_enddate&t5_startdate=$t5_startdate&t5_enddate=$t5_enddate&term=$search_platform&week_term=$search_subtype&adflag=$search_adflag");
	 					
	 			$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
				 					"<table border=1>".
				 					"	<tr>".
				 					"		<td style='font-weight:bold;'>rtidx</td>".
				 					"		<td style='font-weight:bold;'>platform</td>".
				 					"		<td style='font-weight:bold;'>useridx</td>".
				 					"		<td style='font-weight:bold;'>adflag</td>".
				 					"		<td style='font-weight:bold;'>site_id</td>".
				 					"		<td style='font-weight:bold;'>is_payer</td>".
				 					"		<td style='font-weight:bold;'>leavedays</td>".
				 					"		<td style='font-weight:bold;'>createdate</td>".
				 					"		<td style='font-weight:bold;'>writedate</td>".
				 					"	</tr>";
	 				
	 			for ($i=0; $i < sizeof($t5_user_list); $i++)
	 			{
	 				$rtidx = $t5_user_list[$i]["rtidx"];
	 				$platform = $t5_user_list[$i]["platform"];
	 				$useridx = $t5_user_list[$i]["useridx"];
	 				$adflag = $t5_user_list[$i]["adflag"];
	 				$site_id = $t5_user_list[$i]["site_id"];
	 				$is_payer = $t5_user_list[$i]["is_payer"];
	 				$leavedays = $t5_user_list[$i]["leavedays"];
	 				$createdate = $t5_user_list[$i]["createdate"];
	 				$writedate = $t5_user_list[$i]["writedate"];
	 				
	 				$excel_contents .= "<tr>".
 			 				"<td>".$rtidx."</td>".
 			 				"<td>".$platform."</td>".
 			 				"<td>".$useridx."</td>".
 			 				"<td>".$adflag."</td>".
 			 				"<td>".$site_id."</td>".
 			 				"<td>".$is_payer."</td>".
 			 				"<td>".$leavedays."</td>".
 			 				"<td>".$createdate."</td>".
 			 				"<td>".$writedate."</td>".
 			 				"</tr>";
	 			}
	 				
	 			$excel_contents .= "</table>";
 			}
 		}
 		else if($download_type == 2) // t5_product_order
 		{
 			$filename = "t5_product_order_Raw_Data_".$t5_startdate." ~ ".$t5_enddate."_".$search_adflag."(".$title_platform.").xls";
 			
 			if($user_type == 1) // 신규
 			{
 			    $sql = "SELECT t1.platform, t1.useridx, adflag, sex, level, experience, vip_level, mobile_version, os_version, country, logindate, createdate, money, writedate as paydate
                        FROM (
                          SELECT platform, useridx, adflag, coin, sex, level, experience, isblock, vip_level, mobile_version, os_version, country, locale, logindate, createdate
                          FROM t5_user
                          WHERE useridx > 20000  $adflag_tail $platform_tail
                            AND createdate BETWEEN '$t5_startdate 00:00:00' and '$t5_enddate 23:59:59'
                        ) t1 JOIN t5_product_order_all t2 ON t1.useridx = t2.useridx and status = 1 AND datediff(day, createdate, writedate) < 28 AND writedate <= '$t5_enddate 23:59:59'
                        ORDER BY createdate asc, paydate asc";	
 			    write_log($sql);
 			    $t5_product_order_list = $db_redshift->gettotallist($sql);
 			    
 			    if (sizeof($t5_product_order_list) == 0)
 			        error_go("저장할 데이터가 없습니다.", "marketing_newuser_stat_daily.php?today=$search_today&startdate=$search_startdate&enddate=$search_enddate&appsflyer_startdate=$appsflyer_startdate&appsflyer_enddate=$appsflyer_enddate&t5_startdate=$t5_startdate&t5_enddate=$t5_enddate&term=$search_platform&week_term=$search_subtype&adflag=$search_adflag");
 			        
 			        $excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
 			 			        "<table border=1>".
 			 			        "	<tr>".
 			 			        "		<td style='font-weight:bold;'>platform</td>".
 			 			        "		<td style='font-weight:bold;'>useridx</td>".
 			 			        "		<td style='font-weight:bold;'>adflag</td>".
 			 			        "		<td style='font-weight:bold;'>sex</td>".
 			 			        "		<td style='font-weight:bold;'>level</td>".
 			 			        "		<td style='font-weight:bold;'>vip_level</td>".
 			 			        "		<td style='font-weight:bold;'>mobile_version</td>".
 			 			        "		<td style='font-weight:bold;'>os_version</td>".
 			 			        "		<td style='font-weight:bold;'>country</td>".
 			 			        "		<td style='font-weight:bold;'>logindate</td>".
 			 			        "		<td style='font-weight:bold;'>createdate</td>".
 			 			        "		<td style='font-weight:bold;'>money</td>".
 			 			        "		<td style='font-weight:bold;'>paydate</td>".
 			 			        "	</tr>";
 			        
 			        for ($i=0; $i < sizeof($t5_product_order_list); $i++)
 			        {
 			            $platform = $t5_product_order_list[$i]["platform"];
 			            $useridx = $t5_product_order_list[$i]["useridx"];
 			            $adflag = $t5_product_order_list[$i]["adflag"];
 			            $sex = $t5_product_order_list[$i]["sex"];
 			            $level = $t5_product_order_list[$i]["level"];
 			            $vip_level = $t5_product_order_list[$i]["vip_level"];
 			            $mobile_version = $t5_product_order_list[$i]["mobile_version"];
 			            $os_version = $t5_product_order_list[$i]["os_version"];
 			            $country = $t5_product_order_list[$i]["country"];
 			            $logindate = $t5_product_order_list[$i]["logindate"];
 			            $createdate = $t5_product_order_list[$i]["createdate"];
 			            $money = $t5_product_order_list[$i]["money"];
 			            $paydate = $t5_product_order_list[$i]["paydate"];
 			            
 			            $excel_contents .= "<tr>".
 			 			            "<td>".$platform."</td>".
 			 			            "<td>".$useridx."</td>".
 			 			            "<td>".$adflag."</td>".
 			 			            "<td>".$sex."</td>".
 			 			            "<td>".$level."</td>".
 			 			            "<td>".$vip_level."</td>".
 			 			            "<td>".$mobile_version."</td>".
 			 			            "<td>".$os_version."</td>".
 			 			            "<td>".$country."</td>".
 			 			            "<td>".$logindate."</td>".
 			 			            "<td>".$createdate."</td>".
 			 			            "<td>".$money."</td>".
 			 			            "<td>".$paydate."</td>".
 			 			            "</tr>";
 			        }
 			        
 			        $excel_contents .= "</table>";
 			}
 			else if($user_type == 2) // 복귀일 28일 이상
 			{
 			    if($search_platform == 0)
 			    {
 			        $sql = "SELECT t3.platform, t3.useridx, adflag, site_id, is_payer, leavedays, ROUND(money * multi_value, 2) as total_money, createdate, t3.writedate as retentiondate, t4.writedate as paydate
                            FROM (
                              SELECT t1.platform, t1.useridx, adflag, fbsource as site_id, is_payer, leavedays, createdate, writedate,
                                (case when (leavedays) = 0 then 0.000346 when (leavedays) = 1 then 0.011076 when (leavedays) = 2 then 0.045827 when (leavedays) = 3 then 0.082923 when (leavedays) = 4 then 0.100089
                                      when (leavedays) = 5 then 0.117452 when (leavedays) = 6 then 0.151834 when (leavedays) < 14 then 0.17545 when (leavedays) < 28 then 0.252959 when (leavedays) < 56 then 0.33947
                                      when (leavedays) < 112 then 0.399101 when (leavedays) < 365 then 0.513731 else 0.596342 end) as multi_value
                              FROM (
                                SELECT 0 as platform, useridx, min(rtidx) as min_rtidx, max(leavedays) as max_leavedays
                                FROM t5_user_retention_log
                                WHERE useridx > 20000 $adflag_tail
                                  AND '$t5_startdate 00:00:00'  <= writedate AND writedate < '$t5_enddate 23:59:59'
                                group by platform, useridx
                              ) t1 join t5_user_retention_log t2 on t1.min_rtidx = t2.rtidx
                            ) t3 join t5_product_order_all t4 on t3.useridx = t4.useridx and t3.writedate <= t4.writedate and status = 1 AND datediff(day, t3.writedate, t4.writedate) < 28 AND t4.writedate <= '$t5_enddate 23:59:59'
                            ORDER BY retentiondate asc, paydate asc";
 			    }
 			    else
 			    {
 			        $sql = "SELECT t3.platform, t3.useridx, adflag, site_id, is_payer, leavedays, ROUND(money * multi_value, 2) as total_money, createdate, t3.writedate as retentiondate, t4.writedate as paydate
                            FROM (
                              SELECT t1.platform, t1.useridx, adflag, site_id, is_payer, leavedays, createdate, writedate,
                                (case when (leavedays) = 0 then 0.000346 when (leavedays) = 1 then 0.011076 when (leavedays) = 2 then 0.045827 when (leavedays) = 3 then 0.082923 when (leavedays) = 4 then 0.100089
                                      when (leavedays) = 5 then 0.117452 when (leavedays) = 6 then 0.151834 when (leavedays) < 14 then 0.17545 when (leavedays) < 28 then 0.252959 when (leavedays) < 56 then 0.33947
                                      when (leavedays) < 112 then 0.399101 when (leavedays) < 365 then 0.513731 else 0.596342 end) as multi_value
                              FROM (
                                SELECT platform, useridx, min(rtidx) as min_rtidx, max(leavedays) as max_leavedays
                                FROM t5_user_retention_mobile_log
                                WHERE useridx > 20000 $platform_tail $adflag_tail
                                  AND '$t5_startdate 00:00:00' <= writedate AND writedate < '$t5_enddate 23:59:59'
                                group by platform, useridx
                              ) t1 join t5_user_retention_mobile_log t2 on t1.min_rtidx = t2.rtidx
                            ) t3 join t5_product_order_all t4 on t3.useridx = t4.useridx and t3.writedate <= t4.writedate and status = 1 AND datediff(day, t3.writedate, t4.writedate) < 28 AND t4.writedate <= '$t5_enddate 23:59:59'
                            ORDER BY retentiondate asc, paydate asc";
 			    }
 			    $t5_product_order_list = $db_redshift->gettotallist($sql);
 			
     			if (sizeof($t5_product_order_list) == 0)
     				error_go("저장할 데이터가 없습니다.", "marketing_newuser_stat_daily.php?today=$search_today&startdate=$search_startdate&enddate=$search_enddate&appsflyer_startdate=$appsflyer_startdate&appsflyer_enddate=$appsflyer_enddate&t5_startdate=$t5_startdate&t5_enddate=$t5_enddate&term=$search_platform&week_term=$search_subtype&adflag=$search_adflag");
     		
     			$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
    			 					"<table border=1>".
    			 					"	<tr>".
    			 					"		<td style='font-weight:bold;'>platform</td>".
    			 					"		<td style='font-weight:bold;'>useridx</td>".
    			 					"		<td style='font-weight:bold;'>adflag</td>".
    			 					"		<td style='font-weight:bold;'>site_id</td>".
    			 					"		<td style='font-weight:bold;'>is_payer</td>".
    			 					"		<td style='font-weight:bold;'>vip_point</td>".
    			 					"		<td style='font-weight:bold;'>leavedays</td>".
    			 					"		<td style='font-weight:bold;'>money</td>".
    			 					"		<td style='font-weight:bold;'>createdate</td>".
    			 					"		<td style='font-weight:bold;'>retentiondate</td>".
    			 					"		<td style='font-weight:bold;'>paydate</td>".
    			 					"	</tr>";
     		
     			for ($i=0; $i < sizeof($t5_product_order_list); $i++)
     			{
     			    $platform = $t5_product_order_list[$i]["platform"];
     			    $useridx = $t5_product_order_list[$i]["useridx"];
     			    $adflag = $t5_product_order_list[$i]["adflag"];
     			    $site_id = $t5_product_order_list[$i]["site_id"];
     			    $is_payer = $t5_product_order_list[$i]["is_payer"];
     			    $vip_point = $t5_product_order_list[$i]["vip_point"];
     			    $leavedays = $t5_product_order_list[$i]["leavedays"];
     			    $money = $t5_product_order_list[$i]["total_money"];
     			    $createdate = $t5_product_order_list[$i]["createdate"];
     			    $retentiondate = $t5_product_order_list[$i]["retentiondate"];
     			    $paydate = $t5_product_order_list[$i]["paydate"];
     		
     			    $excel_contents .= "<tr>".
     			 			    "<td>".$platform."</td>".
     			 			    "<td>".$useridx."</td>".
     			 			    "<td>".$adflag."</td>".
     			 			    "<td>".$site_id."</td>".
     			 			    "<td>".$is_payer."</td>".
     			 			    "<td>".$vip_point."</td>".
     			 			    "<td>".$leavedays."</td>".
     			 			    "<td>".$money."</td>".
     			 			    "<td>".$createdate."</td>".
     			 			    "<td>".$retentiondate."</td>".
     			 			    "<td>".$paydate."</td>".
     			 			    "</tr>";
     			}
 		
 			    $excel_contents .= "</table>";
 			}
 		}
 	}
	Header("Content-type: application/x-msdownload");
	Header("Content-type: application/x-msexcel");
	Header("Content-Disposition: attachment; filename=$filename");
	
	echo($excel_contents);
	
 	$db_redshift->end();
?>