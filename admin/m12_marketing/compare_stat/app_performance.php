<?
	$top_menu = "marketing";
	$sub_menu = "app_performance";
	
	include("../../common/dbconnect/db_util_redshift.inc.php");
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$platform = ($_GET["platform"] == "") ? "ALL" : $_GET["platform"];
	
	$a_startdate = $_GET["a_startdate"];
	$b_startdate = $_GET["b_startdate"];
	$a_enddate = $_GET["a_enddate"];
	$b_enddate = $_GET["b_enddate"];
	
	$a_user_type = ($_GET["a_user_type"] == "") ? "ALL" : $_GET["a_user_type"];
	$b_user_type = ($_GET["b_user_type"] == "") ? "ALL" : $_GET["b_user_type"];
	$a_play_type = ($_GET["a_play_type"] == "") ? "ALL" : $_GET["a_play_type"];
	$b_play_type = ($_GET["b_play_type"] == "") ? "ALL" : $_GET["b_play_type"];
	$result_date = ($_GET["result_date"] == "") ? "28" : $_GET["result_date"];
	
	//$a_enable = ($_GET["a_enable"] == "") ? "" : $_GET["a_enable"];
	//$b_enable = ($_GET["b_enable"] == "") ? "" : $_GET["b_enable"];
	
	$a_enable = ($_GET["a_enable"] == "a") ? "1" : "";
	$b_enable = ($_GET["b_enable"] == "b") ? "1" : "";
	
	$a_std1 = ($_GET["a_std1"] == "") ? "" : $_GET["a_std1"];
	$a_std2 = ($_GET["a_std2"] == "") ? "" : $_GET["a_std2"];
	$a_equl = ($_GET["a_equl"] == "") ? "<" : $_GET["a_equl"];
	$b_std1 = ($_GET["b_std1"] == "") ? "" : $_GET["b_std1"];
	$b_std2 = ($_GET["b_std2"] == "") ? "" : $_GET["b_std2"];
	$b_equl = ($_GET["b_equl"] == "") ? "<" : $_GET["b_equl"];
	
	check_xss($platform);
	check_xss($a_startdate.$b_startdate);
	check_xss($a_enddate.$b_enddate);
	check_xss($a_user_type.$b_user_type.$a_play_type.$b_play_type);
	check_xss($a_enable.$b_enable);
	check_xss($a_std1.$a_std2.$a_equl.$b_std1.$b_std2.$b_equl);
	
	//오늘 날짜 정보
	$a_today = get_past_date(date("Y-m-d"), 57, "d");
	$a_before_day = get_past_date($a_today, 29, "d");
	
	$b_today = get_past_date(date("Y-m-d"), 29, "d");
	$b_before_day = get_past_date($b_today, 29, "d");
	
	$a_startdate = ($a_startdate == "") ? $a_before_day : $a_startdate;
	$b_startdate = ($b_startdate == "") ? $b_before_day : $b_startdate;
	$a_enddate = ($a_enddate == "") ? $a_today : $a_enddate;
	$b_enddate = ($b_enddate == "") ? $b_today : $b_enddate;
	
	$db_redshift = new CDatabase_Redshift();
	
	$platform_sql = "";
	
	if($platform != "ALL")
	{
	    $platform_sql = "AND platform = $platform ";
	}
	
	$a_user_sql = "";
	
	if($a_user_type != "ALL")
	{
	    if($a_user_type == "organic")
	    {
	        $a_user_sql = " AND adflag = '' ";
	    }
	    else if($a_user_type == "ad_all")
	    {
	        $a_user_sql = " AND (adflag LIKE 'fbself%' OR adflag LIKE 'Facebook%' OR adflag LIKE 'amazon%' OR adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'm_d%') ";
	    }
	    else
	    {
	        $a_user_sql = " AND adflag LIKE '".$a_user_type."%' ";
	    }
	}
	
	$a_detail_sql = "";
	
	if($a_enable == "1")
	{
	    $a_detail_sql = " AND (useridx % $a_std1) $a_equl $a_std2 ";
	}
	
	$b_user_sql = "";
	
	if($b_user_type != "ALL")
	{
	    if($b_user_type == "organic")
	    {
	        $b_user_sql = " AND adflag = '' ";
	    }
	    else if($b_user_type == "ad_all")
	    {
	        $b_user_sql = " AND (adflag LIKE 'fbself%' OR adflag LIKE 'Facebook%' OR adflag LIKE 'amazon%' OR adflag LIKE '%_int' OR adflag LIKE '%_int_viral' OR adflag LIKE 'm_d%') ";
	    }
	    else
	    {
	        $b_user_sql = " AND adflag LIKE '".$b_user_type."%' ";
	    }
	}
	
	$b_detail_sql = "";
	
	if($b_enable == "1")
	{
	    $b_detail_sql = " AND (useridx % $b_std1) $b_equl $b_std2 ";
	}
	
	$a_play_sql = "";
	
	if($a_play_type != "ALL")
	{
	    $a_play_sql = " AND totalspin >= $a_play_type";
	}

	$b_play_sql = "";
	
	if($b_play_type != "ALL")
	{
	    $b_play_sql = " AND totalspin >= $b_play_type";
	}
		
	$sql = "select count(useridx) as user_cnt ".
            "from t5_user ".
            "where '$a_startdate 00:00:00' <= createdate and createdate <= '$a_enddate 23:59:59' $platform_sql $a_user_sql $a_detail_sql $a_play_sql";
	$a_user_cnt = $db_redshift->getvalue($sql);
	
	$sql = "select num, nvl(total_money, 0) as total_money ".
            "from d_day_origin a1 left join ( ".
            "   select datediff('day', createdate, writedate) as today, SUM(money) AS total_money, count(distinct t1.useridx) as payer_cnt ".
            "   from ( ".
            "       select useridx, createdate ". 
            "       from t5_user ".
            "       where '$a_startdate 00:00:00' <= createdate and createdate <= '$a_enddate 23:59:59' $platform_sql $a_user_sql $a_detail_sql $a_play_sql".
            "   ) t1 join t5_product_order_all t2 on t1.useridx = t2.useridx and t2.status = 1 ".
            "   where datediff('day', createdate, writedate) < $result_date ".
            "   group by datediff('day', createdate, writedate) ".
            ") a2 on a1.num = a2.today ".
            "where num < $result_date ".
            "order by num asc";
	$a_order_list = $db_redshift->gettotallist($sql);
	
	$sql = "select num, nvl(retention_cnt, 0) as retention_cnt ".
            "from d_day_origin a1 left join ( ".
            "   select datediff('day', createdate, writedate) as today, count(distinct useridx) as retention_cnt ".
            "   from ( ".
            "       select t1.useridx, t1.createdate, writedate ".
            "       from ( ".
            "           select useridx, createdate ". 
            "           from t5_user ".
            "           where '$a_startdate 00:00:00' <= createdate and createdate <= '$a_enddate 23:59:59' $platform_sql $a_user_sql $a_detail_sql $a_play_sql".
            "       ) t1 join t5_user_retention_log t2 on t1.useridx = t2.useridx ".
            "       where datediff('day', t1.createdate, writedate) <= $result_date ".
            "       union all ".
            "       select t1.useridx, t1.createdate, writedate ".
            "       from ( ".
            "           select useridx, createdate ". 
            "           from t5_user ".
            "           where '$a_startdate 00:00:00' <= createdate and createdate <= '$a_enddate 23:59:59' $platform_sql $a_user_sql $a_detail_sql $a_play_sql".
            "       ) t1 join t5_user_retention_mobile_log t2 on t1.useridx = t2.useridx ".
            "       where datediff('day', t1.createdate, writedate) <= $result_date ".
            "   ) total ".
            "   where datediff('day', createdate, writedate) > 0 ".
            "   group by datediff('day', createdate, writedate) ".
            ") a2 on a1.num = a2.today ".
            "where 0 < num and num <= $result_date ".
            "order by num asc";
	$a_retentnion_list = $db_redshift->gettotallist($sql);
	
	$sql = "select num, nvl(pur_cnt, 0) as pur_cnt ".
            "from d_day_origin a1 left join ( ".
            "   select today, count(useridx) as pur_cnt ".
            "   from ( ".
            "       select t1.useridx, min(datediff('day', createdate, writedate)) as today ".
            "       from ( ".
            "           select useridx, createdate ". 
            "           from t5_user ".
            "           where '$a_startdate 00:00:00' <= createdate and createdate <= '$a_enddate 23:59:59' $platform_sql $a_user_sql $a_detail_sql $b_play_sql".
            "       ) t1 join t5_product_order_all t2 on t1.useridx = t2.useridx and t2.status = 1 ".
            "       where datediff('day', createdate, writedate) < $result_date ".
            "       group by t1.useridx ".
            "   ) total ".
            "   group by today ".
            ") a2 on a1.num = a2.today ".
            "where num < $result_date ".
            "order by num asc";
	$a_pur_list = $db_redshift->gettotallist($sql);
	
	$sql = "select count(useridx) as user_cnt ".
	   	"from t5_user ".
	   	"where '$b_startdate 00:00:00' <= createdate and createdate <= '$b_enddate 23:59:59' $platform_sql $b_user_sql $b_detail_sql $b_play_sql";
	$b_user_cnt = $db_redshift->getvalue($sql);
	
	$sql = "select num, nvl(total_money, 0) as total_money ".
	   	"from d_day_origin a1 left join ( ".
	   	"   select datediff('day', createdate, writedate) as today, SUM(money) AS total_money, count(distinct t1.useridx) as payer_cnt ".
	   	"   from ( ".
	   	"       select useridx, createdate ".
	   	"       from t5_user ".
	   	"       where '$b_startdate 00:00:00' <= createdate and createdate <= '$b_enddate 23:59:59' $platform_sql $b_user_sql $b_detail_sql $b_play_sql".
	   	"   ) t1 join t5_product_order_all t2 on t1.useridx = t2.useridx and t2.status = 1 ".
	   	"   where datediff('day', createdate, writedate) < $result_date ".
	   	"   group by datediff('day', createdate, writedate) ".
	   	") a2 on a1.num = a2.today ".
	   	"where num < $result_date ".
	   	"order by num asc";
	$b_order_list = $db_redshift->gettotallist($sql);

	$sql = "select num, nvl(retention_cnt, 0) as retention_cnt ".
	   	"from d_day_origin a1 left join ( ".
	   	"   select datediff('day', createdate, writedate) as today, count(distinct useridx) as retention_cnt ".
	   	"   from ( ".
	   	"       select t1.useridx, t1.createdate, writedate ".
	   	"       from ( ".
	   	"           select useridx, createdate ".
	   	"           from t5_user ".
	   	"           where '$b_startdate 00:00:00' <= createdate and createdate <= '$b_enddate 23:59:59' $platform_sql $b_user_sql $b_detail_sql $b_play_sql".
	   	"       ) t1 join t5_user_retention_log t2 on t1.useridx = t2.useridx ".
	   	"       where datediff('day', t1.createdate, writedate) <= $result_date ".
	   	"       union all ".
	   	"       select t1.useridx, t1.createdate, writedate ".
	   	"       from ( ".
	   	"           select useridx, createdate ".
	   	"           from t5_user ".
	   	"           where '$b_startdate 00:00:00' <= createdate and createdate <= '$b_enddate 23:59:59' $platform_sql $b_user_sql $b_detail_sql $b_play_sql".
	   	"       ) t1 join t5_user_retention_mobile_log t2 on t1.useridx = t2.useridx ".
	   	"       where datediff('day', t1.createdate, writedate) <= $result_date ".
	   	"   ) total ".
	   	"   where datediff('day', createdate, writedate) > 0 ".
	   	"   group by datediff('day', createdate, writedate) ".
	   	") a2 on a1.num = a2.today ".
	   	"where 0 < num and num <= $result_date ".
	   	"order by num asc";
	$b_retentnion_list = $db_redshift->gettotallist($sql);
	
	$sql = "select num, nvl(pur_cnt, 0) as pur_cnt ".
    	   	"from d_day_origin a1 left join ( ".
    	   	"   select today, count(useridx) as pur_cnt ".
    	   	"   from ( ".
    	   	"       select t1.useridx, min(datediff('day', createdate, writedate)) as today ".
    	   	"       from ( ".
    	   	"           select useridx, createdate ".
    	   	"           from t5_user ".
    	   	"           where '$b_startdate 00:00:00' <= createdate and createdate <= '$b_enddate 23:59:59' $platform_sql $b_user_sql $b_detail_sql $b_play_sql".
    	   	"       ) t1 join t5_product_order_all t2 on t1.useridx = t2.useridx and t2.status = 1 ".
    	   	"       where datediff('day', createdate, writedate) < $result_date ".
    	   	"       group by t1.useridx ".
    	   	"   ) total ".
    	   	"   group by today ".
    	   	") a2 on a1.num = a2.today ".
    	   	"where num < $result_date ".
    	   	"order by num asc";
	$b_pur_list = $db_redshift->gettotallist($sql);
	
	$db_redshift->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>

<script type="text/javascript">
	$(function() {
	    $("#a_startdate").datepicker({ });
	    $("#b_startdate").datepicker({ });
	    $("#a_enddate").datepicker({ });
	    $("#b_enddate").datepicker({ });
	});
	
	function change_platform(term)
	{
		var search_form = document.search_form;
		
		var term_all = document.getElementById("term_all");
		var term_fb = document.getElementById("term_fb");
		var term_ios = document.getElementById("term_ios");
		var term_and = document.getElementById("term_and");
		var term_ama = document.getElementById("term_ama");

		document.search_form.platform.value = term;
		
		if (term == "ALL")
		{
			term_all.className="btn_schedule_select";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 0)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule_select";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 1)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule_select";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 2)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule_select";
			term_ama.className="btn_schedule";
		}
		else if (term == 3)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule_select";
		}

		search_form.submit();
	}

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		if(search_form.a_enable.value == "1")
		{
			if (!IsNumber(search_form.a_std1.value) || search_form.a_std1.value == "")
			{
				alert("숫자만 입력 가능합니다.");
				search_form.a_std1.focus();
				return;
			}

			if (!IsNumber(search_form.a_std2.value) || search_form.a_std2.value == "")
			{
				alert("숫자만 입력 가능합니다.");
				search_form.a_std2.focus();
				return;
			}
		}

		if(search_form.b_enable.value == "1")
		{
			if (!IsNumber(search_form.b_std1.value) || search_form.b_std1.value == "")
			{
				alert("숫자만 입력 가능합니다.");
				search_form.b_std1.focus();
				return;
			}

			if (!IsNumber(search_form.b_std2.value) || search_form.b_std2.value == "")
			{
				alert("숫자만 입력 가능합니다.");
				search_form.b_std2.focus();
				return;
			}
		}
		
		search_form.submit();
	}

	function fn_toggle(id)
	{
		$("#"+id).toggle(1);
	}

	google.load("visualization", "1", {packages:["corechart"]});

    function drawChart() 
    {
    	var data0 = new google.visualization.DataTable();
        data0.addColumn('string', 'D+Day');
        data0.addColumn('number', 'A 그룹');
        data0.addColumn('number', 'B 그룹');
        data0.addRows([
<?
    $a_total_money = 0;
    $b_total_money = 0;

    for ($i=0; $i<$result_date; $i++)
	{
	    $a_day_money = $a_order_list[$i]["total_money"];
	    $a_total_money += $a_day_money;
	    
	    $b_day_money = $b_order_list[$i]["total_money"];
	    $b_total_money += $b_day_money;
	    
	    $a_arpu = round($a_total_money/$a_user_cnt, 2);
	    $b_arpu = round($b_total_money/$b_user_cnt, 2);
	    
	    echo("['".$i."',".number_format($a_arpu, 2).",".number_format($b_arpu, 2)."]");
		
	    if ($i != $result_date - 1)
			echo(",");
	}
?>
        ]);

        var data1 = new google.visualization.DataTable();
        data1.addColumn('string', 'PUR(%)');
        data1.addColumn('number', 'A 그룹');
        data1.addColumn('number', 'B 그룹');
        data1.addRows([
<?
    $a_total_pur = 0;
    $b_total_pur = 0;

    for ($i=0; $i<$result_date; $i++)
	{
	    $a_day_pur = $a_pur_list[$i]["pur_cnt"];
	    $a_total_pur += $a_day_pur;
	    
	    $b_day_pur = $b_pur_list[$i]["pur_cnt"];
	    $b_total_pur += $b_day_pur;
	    
	    $a_pur = round($a_total_pur/$a_user_cnt*100, 2);
	    $b_pur = round($b_total_pur/$b_user_cnt*100, 2);
	    
	    echo("['".$i."',".number_format($a_pur, 2).",".number_format($b_pur, 2)."]");
		
	    if ($i != $result_date - 1)
			echo(",");
	}
?>
        ]);
        
        var data2 = new google.visualization.DataTable();
        data2.addColumn('string', 'Retention(%)');
        data2.addColumn('number', 'A 그룹');
        data2.addColumn('number', 'B 그룹');
        data2.addRows([
<?

    for ($i=0; $i<$result_date; $i++)
	{
	    $a_retention_cnt = $a_retentnion_list[$i]["retention_cnt"];
	    $b_retention_cnt = $b_retentnion_list[$i]["retention_cnt"];

	    $a_retention = round($a_retention_cnt/$a_user_cnt*100, 2);
	    $b_retention = round($b_retention_cnt/$b_user_cnt*100, 2);
	    
	    echo("['".($i+1)."',".number_format($a_retention, 2).",".number_format($b_retention, 2)."]");
		
	    if ($i != $result_date)
			echo(",");
	}
?>
        ]);

    	var options1 = {
            width:1050,                         
            height:470,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:300}
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div0'));
        chart.draw(data0, options1);

        chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
        chart.draw(data1, options1);

        chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
        chart.draw(data2, options1);
    }
        
	google.setOnLoadCallback(drawChart);
</script>
		<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; 앱 퍼포먼스 비교(신규)</div>
	        </div>
	    	<!-- //title_warp -->
	    	
	    	<form name="search_form" id="search_form"  method="get" action="app_performance.php">
	    		<input type="hidden" name="platform" id="platform" value="<?= $platform ?>" />
	    		<div class="detail_search_wrap">
	    			<span class="search_lbl ml20">플랫폼</span>
	    			<input type="button" class="<?= ($platform == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_all" onclick="change_platform('ALL')" value="통합" />
	    			<input type="button" class="<?= ($platform == "0") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_fb" onclick="change_platform('0')" value="Facebook" />
	    			<input type="button" class="<?= ($platform == "1") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ios" onclick="change_platform('1')" value="iOS" />
	    			<input type="button" class="<?= ($platform == "2") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_and" onclick="change_platform('2')" value="Android" />
	    			<input type="button" class="<?= ($platform == "3") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ama" onclick="change_platform('3')" value="Amazon" />

					<span class="search_lbl ml20">결과 추출 기간</span>
	    			<select name="result_date" id="result_date">										
						<option value="7" <?= ($result_date=="7") ? "selected" : "" ?>>D+7</option>
						<option value="14" <?= ($result_date=="14") ? "selected" : "" ?>>D+14</option>
						<option value="21" <?= ($result_date=="21") ? "selected" : "" ?>>D+21</option>
						<option value="28" <?= ($result_date=="28") ? "selected" : "" ?>>D+28</option>
						<option value="56" <?= ($result_date=="56") ? "selected" : "" ?>>D+56</option>
						<option value="112" <?= ($result_date=="112") ? "selected" : "" ?>>D+112</option>
						<option value="224" <?= ($result_date=="224") ? "selected" : "" ?>>D+224</option>
						<option value="336" <?= ($result_date=="336") ? "selected" : "" ?>>D+336</option>
					</select>
	    			
					<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>

					<div class="clear"  style="padding-top:20px"></div>
					
					<table style="width:100%">
						<colgroup>
            				<col width="50%">
            				<col width="50%">
        				</colgroup>
        				<thead>
                            <tr>
                                <th>A 그룹 조건</th>
                                <th>B 그룹 조건</th>
    						</tr>
						</thead>
						<tbody>
							<tr>
								<td class="tdc point">
    								<span class="search_lbl ml20">&nbsp;&nbsp;&nbsp;날짜</span>
                	    			<input type="text" class="search_text" id="a_startdate" name="a_startdate" value="<?= $a_startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
                		            <input type="text" class="search_text" id="a_enddate" name="a_enddate" value="<?= $a_enddate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
    							</td>
    							<td class="tdc point">
    								<span class="search_lbl ml20">&nbsp;&nbsp;&nbsp;날짜</span>
                	    			<input type="text" class="search_text" id="b_startdate" name="b_startdate" value="<?= $b_startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
                		            <input type="text" class="search_text" id="b_enddate" name="b_enddate" value="<?= $b_enddate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
    							</td>
							</tr>
							<tr>
								<td class="tdc point">
									<span class="search_lbl ml20">&nbsp;&nbsp;&nbsp;유저 조건</span>
									<select name="a_user_type" id="a_user_type">										
                						<option value="ALL" <?= ($a_user_type=="ALL") ? "selected" : "" ?>>전체</option>
                						<option value="organic" <?= ($a_user_type=="organic") ? "selected" : "" ?>>organic</option>
                						<option value="ad_all" <?= ($a_user_type=="ad_all") ? "selected" : "" ?>>광고 유저 전체</option>
                						<option value="fbself201" <?= ($a_user_type=="fbself201") ? "selected" : "" ?>>fbself201</option>
                						<option value="fbself301" <?= ($a_user_type=="fbself301") ? "selected" : "" ?>>fbself301</option>
                						<option value="m_ddi" <?= ($a_user_type=="m_ddi") ? "selected" : "" ?>>m_ddi</option>
                						<option value="m_duc" <?= ($a_user_type=="m_duc") ? "selected" : "" ?>>m_duc</option>
                						<option value="Facebook Ads" <?= ($a_user_type=="Facebook Ads") ? "selected" : "" ?>>Facebook Ads</option>
                						<option value="amazon" <?= ($a_user_type=="amazon") ? "selected" : "" ?>>amazon</option>                       				
                					</select>
    							</td>
    							<td class="tdc point">
									<span class="search_lbl ml20">&nbsp;&nbsp;&nbsp;유저 조건</span>
									<select name="b_user_type" id="b_user_type">										
                						<option value="ALL" <?= ($b_user_type=="ALL") ? "selected" : "" ?>>전체</option>
                						<option value="organic" <?= ($b_user_type=="organic") ? "selected" : "" ?>>organic</option>
                						<option value="ad_all" <?= ($b_user_type=="ad_all") ? "selected" : "" ?>>광고 유저 전체</option>
                						<option value="fbself201" <?= ($b_user_type=="fbself201") ? "selected" : "" ?>>fbself201</option>
                						<option value="fbself301" <?= ($b_user_type=="fbself301") ? "selected" : "" ?>>fbself301</option>
                						<option value="m_ddi" <?= ($b_user_type=="m_ddi") ? "selected" : "" ?>>m_ddi</option>
                						<option value="m_duc" <?= ($b_user_type=="m_duc") ? "selected" : "" ?>>m_duc</option>
                						<option value="Facebook Ads" <?= ($b_user_type=="Facebook Ads") ? "selected" : "" ?>>Facebook Ads</option>
                						<option value="amazon" <?= ($b_user_type=="amazon") ? "selected" : "" ?>>amazon</option>
                					</select>
    							</td>
							</tr>
							<tr>
								<td class="tdc point">
									<span class="search_lbl ml20">&nbsp;&nbsp;&nbsp;플레이 조건</span>
                	    			<select name="a_play_type" id="a_play_type">										
                						<option value="ALL" <?= ($a_play_type=="ALL") ? "selected" : "" ?>>전체</option>
                						<option value="1" <?= ($a_play_type=="1") ? "selected" : "" ?>>가입 후 1 스핀이상</option>
                						<option value="50" <?= ($a_play_type=="50") ? "selected" : "" ?>>가입 후 50 스핀이상</option>
                						<option value="100" <?= ($a_play_type=="100") ? "selected" : "" ?>>가입 후 100 스핀이상</option>
                						<option value="200" <?= ($a_play_type=="200") ? "selected" : "" ?>>가입 후 200 스핀이상</option>
                						<option value="500" <?= ($a_play_type=="500") ? "selected" : "" ?>>가입 후 500 스핀이상</option>
                						<option value="1000" <?= ($a_play_type=="1000") ? "selected" : "" ?>>가입 후 1000 스핀이상</option>                    				
                					</select>
								</td>
								<td class="tdc point">
									<span class="search_lbl ml20">&nbsp;&nbsp;&nbsp;플레이 조건</span>
                	    			<select name="b_play_type" id="b_play_type">										
                						<option value="ALL" <?= ($b_play_type=="ALL") ? "selected" : "" ?>>전체</option>
                						<option value="1" <?= ($b_play_type=="1") ? "selected" : "" ?>>가입 후 1 스핀이상</option>
                						<option value="50" <?= ($b_play_type=="50") ? "selected" : "" ?>>가입 후 50 스핀이상</option>
                						<option value="100" <?= ($b_play_type=="100") ? "selected" : "" ?>>가입 후 100 스핀이상</option>
                						<option value="200" <?= ($b_play_type=="200") ? "selected" : "" ?>>가입 후 200 스핀이상</option>
                						<option value="500" <?= ($b_play_type=="500") ? "selected" : "" ?>>가입 후 500 스핀이상</option>
                						<option value="1000" <?= ($b_play_type=="1000") ? "selected" : "" ?>>가입 후 1000 스핀이상</option>                    				
                					</select>
								</td>
							</tr>
							<tr>
								<td class="tdc point">
									<span class="search_lbl ml20">세부 조건 활성화 <input type="checkbox" name="a_enable" id="a_enable" value="a" align="absmiddle" onclick="fn_toggle('a_detail')" <?= ($a_enable == "1") ? "checked" : "" ?> /></span>
									<span id="a_detail" style="<?= ($a_enable==1) ? "" : "display:none;" ?>">
										(useridx %
										<input type="text" class="search_text" id="a_std1" name="a_std1" value="<?= $a_std1 ?>" style="width:40px" />)
										<select name="a_equl" id="a_equl">										
                    						<option value="<" <?= ($a_equl=="<") ? "selected" : "" ?>><</option>
                    						<option value="<=" <?= ($a_equl=="<=") ? "selected" : "" ?>><=</option>
                    						<option value="=" <?= ($a_equl=="=") ? "selected" : "" ?>>=</option>
                    						<option value=">" <?= ($a_equl==">") ? "selected" : "" ?>>></option>
                    						<option value=">=" <?= ($a_equl==">=") ? "selected" : "" ?>>>=</option>
                    					</select>
										<input type="text" class="search_text" id="a_std2" name="a_std2" value="<?= $a_std2 ?>" style="width:40px" /> 
									</span>
								</td>
								<td class="tdc point">
									<span class="search_lbl ml20">세부 조건 활성화 <input type="checkbox" name="b_enable" id="b_enable" value="b" align="absmiddle" onclick="fn_toggle('b_detail')" <?= ($b_enable == "1") ? "checked" : "" ?> /></span>
									<span id="b_detail" style="<?= ($b_enable==1) ? "" : "display:none;" ?>">
										(useridx %
										<input type="text" class="search_text" id="b_std1" name="b_std1" value="<?= $b_std1 ?>" style="width:40px" />)
										<select name="b_equl" id="b_equl">										
                    						<option value="<" <?= ($b_equl=="<") ? "selected" : "" ?>><</option>
                    						<option value="<=" <?= ($b_equl=="<=") ? "selected" : "" ?>><=</option>
                    						<option value="=" <?= ($b_equl=="=") ? "selected" : "" ?>>=</option>
                    						<option value=">" <?= ($b_equl==">") ? "selected" : "" ?>>></option>
                    						<option value=">=" <?= ($b_equl==">=") ? "selected" : "" ?>>>=</option>
                    					</select>
										<input type="text" class="search_text" id="b_std2" name="b_std2" value="<?= $b_std2 ?>" style="width:40px" /> 
									</span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
    		</form>

    		<div class="h2_title">A 그룹 모수: <?= number_format($a_user_cnt) ?>명</div>
    		<div class="h2_title">B 그룹 모수: <?= number_format($b_user_cnt) ?>명</div>
    		
	    	<div class="h2_title">[ARPU]</div>
	    	<div id="chart_div0" style="height:480px; min-width: 500px"></div>
	    	
	    	<div class="h2_title">[PUR]</div>
	    	<div id="chart_div1" style="height:480px; min-width: 500px"></div>
	    	
	    	<div class="h2_title">[Retention]</div>
	    	<div id="chart_div2" style="height:480px; min-width: 500px"></div>
    	</div>
    	<!--  //CONTENTS WRAP -->
    	
    	<div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>