<? 
    include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
    
    check_login_layer();
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    
    $startdate = ($_GET["startdate"] == "") ? date("Y-m-d") : $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
    $platform = ($_GET["platform"] == "") ? "platform" : $_GET["platform"];
    $search_agencyidx = ($_GET["agencyidx"] == "") ? "" : $_GET["agencyidx"];
    
    $db_main2 = new CDatabase_Main2();
    
    $sql = "SELECT today, SUM(spend) AS spend
			FROM tbl_agency_spend_daily
    		WHERE today BETWEEN '$startdate' AND '$enddate'
    		AND platform = $platform AND agencyidx = $search_agencyidx 
			GROUP BY today
    		ORDER BY today ASC";
    $spend_list = $db_main2->gettotallist($sql);
    
    $sql = "SELECT agencyidx, agencyname
		    FROM tbl_agency_spend_daily
		    WHERE today BETWEEN '$startdate' AND '$enddate'
		    GROUP BY agencyidx
		    ORDER BY agencyname ASC";
    $adflag_list = $db_main2->gettotallist($sql);
    
    $db_main2->end();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>오늘 상품별 결제 내역</title>
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script>
<script type="text/javascript" src="/js/ui/ui.datepicker.js"></script>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	function search()
	{
	    var search_form = document.search_form;
	    search_form.submit();
	}

	$(function() {
        $("#startdate").datepicker({ });
    });

    $(function() {
        $("#enddate").datepicker({ });
    });

    function layer_close()
    {
        window.parent.close_layer_popup("<?= $_GET["layer_no"] ?>");
    }
</script>
</head>
<body class="layer_body"  onload="try { window_onload() } catch(e) {}" onkeyup="if (getEventKeycode(event) == 27) { layer_close(); }">
<div id="layer_wrap">   
    <div class="layer_header" >
        <div class="layer_title">마케팅 비용 상세보기</div>
        <img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="layer_close()" />
    </div>
    <div style="width:330px;height:690px;overflow-y:auto;">
         <!--  레이어 내용  -->
         
         <div class="layer_user_fix_height">
         	<form name="search_form" id="search_form" onsubmit="return false">
	         	플랫폼
			   	<select name="platform" id="platform">
			    	<option value="platform" value="platform" <?= ($platform == "platform") ? "selected" : "" ?>>전체</option>
			        <option value="1" value="1" <?= ($platform == "1") ? "selected" : "" ?>>iOS</option>
			        <option value="2" value="2" <?= ($platform == "2") ? "selected" : "" ?>>Android</option>
			        <option value="3" value="3" <?= ($platform == "3") ? "selected" : "" ?>>Amazon</option>
			    </select>
			    
			    <br/><br/>
			    
				Adflag : 
				<select name="agencyidx" id="agencyidx">
					<option value="" <?= ($search_agencyidx == "" || $search_agencyidx == "agencyidx") ? "selected" : "" ?>>전체</option>
<?
	for($i=0; $i<sizeof($adflag_list); $i++)
	{
		$agencyidx = $adflag_list[$i]["agencyidx"];
		$agencyname = $adflag_list[$i]["agencyname"];
		
		if($search_agencyidx == $agencyidx)
			$agencyname_str = $agencyname; 
?>
					<option value="<?= $agencyidx ?>" <?= ($search_agencyidx == $agencyidx) ? "selected" : "" ?>><?= $agencyname ?></option>
<?
	}
?>
				</select>
			
				<br/><br/>
			
			날짜 : <input type="input" class="search_text" id="startdate" name="startdate" style="width:70px" value="<?= $startdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
			   - <input type="input" class="search_text" id="enddate" name="enddate" style="width:70px" value="<?= $enddate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />

				<input style=""type="button" class="btn_search" value="검색" onclick="search()" />
				<br/><br/>
			   
<?
	if($platform == "platform")
		$platform_str = "전체";
	else if($platform == "1")
		$platform_str = "iOS";
	else if($platform == "2")
		$platform_str = "Android";
	else if($platform == "3")
		$platform_str = "Amazon";
?>
			    <text style="font-weight:bold;font-size:13px;">[ <?= $agencyname_str ?> - <?= $platform_str ?> ]</text>
		    </form>

		    
            <table class="tbl_list_basic1" style="margin-top:5px">
                    <colgroup>
                        <col width="">
                        <col width="">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>날짜</th>
                            <th>비용</th>
                        </tr>
                    </thead>
                    <tbody>
<?
    for($i=0; $i<sizeof($spend_list); $i++)
    {
         $today = $spend_list[$i]["today"];
         $spend = $spend_list[$i]["spend"];
         
         $total_spend += $spend; 
?>
                        <tr>
                            <td class="tdc point"><?= $today ?></td>
                            <td class="tdr"><?= ($spend == 0) ? "-" : "$ ".number_format($spend, 2) ?></td>
                        </tr>
<?
    }
?>
						<tr style="border-top:double 1px;">
                            <td class="tdc point">Total</td>
                            <td class="tdr point"><?= ($total_spend == 0) ? "-" : "$ ".number_format($total_spend, 2) ?></td>
                        </tr>
                    </tbody>
                </table> 
            </form>  
         </div>
    </div>
</div>
</body>
</html>

