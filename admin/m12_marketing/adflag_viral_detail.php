<?
    $top_menu = "marketing";
    $sub_menu = "adflag_viral_detail";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    $search_payday = $_GET["payday"];
	$isearch = $_GET["issearch"];
		
	if ($isearch == "")
	{
		$search_end_createdate  = date("Y-m-d", time());
		$search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 6);
	}
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	
	$now = time();
	
	if ($search_end_createdate != "")
	{
		$now = strtotime($search_end_createdate);
	}
	else
	{
		$search_end_createdate = date('Y-m-d');
	}
	
	if ($search_start_createdate == "")
	{
		$search_start_createdate = '2015-10-20';
	}

	$sql = "SELECT fbsource,DATE_FORMAT(createdate,'%Y-%m-%d') AS day,COUNT(*) AS totalcount, ".
			"ABS(IFNULL(SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			",0)) AS unplaycount,";
	
	
	if ($search_payday != "")
	{
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0)) AS paycount,".
				"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0))AS totalcredit ";
	}
	else
	{
		
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0)) AS paycount,".
				"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1)),0)) AS totalcredit ";
	}
	
	$total_sql = $sql." FROM tbl_user_ext WHERE createdate >='$search_start_createdate 00:00:00' AND createdate <='$search_end_createdate 23:59:59' AND adflag='' GROUP BY fbsource ORDER BY fbsource ASC";
	
	$sql .= " FROM tbl_user_ext WHERE createdate >='$search_start_createdate 00:00:00' AND createdate <='$search_end_createdate 23:59:59' AND adflag='' GROUP BY fbsource,DATE_FORMAT(createdate,'%Y-%m-%d') ORDER BY day DESC, fbsource ASC";
	$summarylist = $db_main->gettotallist($sql);

	$totalsummarylist = $db_main->gettotallist($total_sql);
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
$(function() {
    $("#start_createdate").datepicker({ });
});

$(function() {
    $("#end_createdate").datepicker({ });
});

function search_press(e)
{
    if (((e.which) ? e.which : e.keyCode) == 13)
    {
        search();
    }
}

function search()
{
    var search_form = document.search_form;

    search_form.submit();
}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">  
<form name="search_form" id="search_form"  method="get" action="adflag_viral_detail.php">
	<input type=hidden name=issearch value="1" />
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Viral 유입 현황</div>
		<div class="search_box">
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			&nbsp;&nbsp;&nbsp;&nbsp;결제액 기준 : 가입 후 <input type="text" class="search_text" id="payday" name="payday" style="width:60px" value="<?= $search_payday ?>" onkeypress="search_press(event); return checknum();" />일 이내 결제
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
        
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
        
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">                
                <col width=""> 
                <col width="">
                <col width=""> 
            </colgroup>
        	<thead>
	            <tr>
	                <th>유입일</th>
	                <th class="tdr">유입경로</th>
	                <th class="tdr">회원가입수</th>	                
	                <th class="tdr">미게임회원수</th>
	                <th class="tdr">결제회원수</th>
	                <th class="tdr">총결제 credit<br>(평균)</th>
	            </tr>
        	</thead>
        	<tbody>
<?
$temp_writedate = "";

for($i=0; $i<sizeof($summarylist); $i++)
{
	$today = $summarylist[$i]["day"];
	$fbsource = $summarylist[$i]["fbsource"];
	$totalcount = $summarylist[$i]["totalcount"];	
	$unplaycount = $summarylist[$i]["unplaycount"];
	$paycount = $summarylist[$i]["paycount"];
	$totalcredit = $summarylist[$i]["totalcredit"];
	
	$unplayratio = ($totalcount > 0) ? round($unplaycount * 10000 / $totalcount) / 100 : 0;
	$payratio = ($totalcount > 0) ? round($paycount * 10000 / $totalcount) / 100 : 0;
	$averagecredit = ($totalcount > 0) ? round($totalcredit * 1000 / $totalcount) / 1000 : 0;
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
	if($temp_writedate != $today)
	{
		$temp_writedate = $today;
		
		$sql = "SELECT COUNT(fbsource) ".
				"FROM ( ".
				"	SELECT fbsource ".
				"	FROM tbl_user_ext ".
				"WHERE createdate >= '$today 00:00:00' AND createdate <='$today 23:59:59' AND adflag='' ".
				"GROUP BY fbsource ".
				") t1";
							
		$rowcount = $db_main->getvalue($sql);
?>
					<td class="tdc point_title" rowspan="<?= $rowcount?>" valign="center"><?= $today ?></td>
<?
	}
?>
					<td class="tdr point"><?= ($fbsource == "") ? "viral" : $fbsource ?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>    
                    <td class="tdr point"><?= number_format($unplaycount) ?> (<?= $unplayratio ?> %)</td>
                    <td class="tdr point"><?= number_format($paycount) ?> (<?= $payratio ?> %)</td>
                    <td class="tdr point"><?= number_format($totalcredit) ?> (<?= $averagecredit ?>)</td>
				</tr>			
<?
}

for($i=0; $i<sizeof($totalsummarylist); $i++)
{
	$sum_fbsource = $totalsummarylist[$i]["fbsource"];
	$sum_totalcount = $totalsummarylist[$i]["totalcount"];
	$sum_unplaycount = $totalsummarylist[$i]["unplaycount"];
	$sum_paycount = $totalsummarylist[$i]["paycount"];
	$sum_totalcredit = $totalsummarylist[$i]["totalcredit"];
	
	$sum_unplayratio = ($sum_totalcount > 0) ? round($sum_unplaycount * 10000 / $sum_totalcount) / 100 : 0;
	$sum_payratio = ($sum_totalcount > 0) ? round($sum_paycount * 10000 / $sum_totalcount) / 100 : 0;
	$sum_averagecredit = ($sum_totalcount > 0) ? round($sum_totalcredit * 1000 / $sum_totalcount) / 1000 : 0;
			
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
	if($i == 0)
	{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($totalsummarylist)?>" valign="center">Total</td>
<?
	}
?>
					<td class="tdr point"><?= ($sum_fbsource == "") ? "viral" : $sum_fbsource ?></td>
                    <td class="tdr point"><?= number_format($sum_totalcount) ?></td>    
                    <td class="tdr point"><?= number_format($sum_unplaycount) ?> (<?= $sum_unplayratio ?> %)</td>
                    <td class="tdr point"><?= number_format($sum_paycount) ?> (<?= $sum_payratio ?> %)</td>
                    <td class="tdr point"><?= number_format($sum_totalcredit) ?> (<?= $sum_averagecredit ?>)</td>
				</tr>
<?
}
?>
        	</tbody>
        </table>
    </div>
</form>
</div>
<!--  //CONTENTS WRAP -->
<?
$db_main->end();
$db_main2->end();

include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>