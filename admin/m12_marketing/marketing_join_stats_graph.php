<?
	$top_menu = "marketing";
	$sub_menu = "marketing_join_stats_graph";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	
	$startdate = ($_GET["startdate"] == "") ? date("Y-m-d", strtotime("-28 day")) : $_GET["startdate"];
	$enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
	
	$term = ($_GET["term"] == "") ? "platform" : $_GET["term"];
	
	$platform = $term;
	
	$sql = "SELECT DATE_FORMAT(install_time, '%Y-%m-%d') AS today, COUNT(appsflyer_device_id) AS cnt
			FROM tbl_appsflyer_install
			WHERE install_time BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' AND platform = $term
			GROUP BY today";
	$appsflyer_install_list = $db_main2 ->gettotallist($sql);
	
	$sql = "SELECT DATE_FORMAT(install_time, '%Y-%m-%d') AS today, COUNT(appsflyer_device_id) AS cnt
			FROM tbl_appsflyer_install_organic
			WHERE install_time BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' AND platform = $term
			GROUP BY today";
	$appsflyer_organic_install_list = $db_main2 ->gettotallist($sql);
	
	if($term != "platform")
	{
		$adflag_tail = " AND platform = $term ";
	}
	else
	{
		$adflag_tail = " ";
	}
	
	$sql = "SELECT DATE_FORMAT(createdate, '%Y-%m-%d') AS today, COUNT(*) AS cnt
			FROM tbl_user_ext
			WHERE createdate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $adflag_tail
			GROUP BY today";
	$user_join_list = $db_main->gettotallist($sql);
	
	$appsflyer_install_cnt_list = array();
	$appsflyer_organic_install_cnt_list = array();
	$user_join_cnt_list = array();
	
	$list_pointer = sizeof($user_join_list);
		
	$date_pointer = $enddate;
	
	for ($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
	{
		$today = $user_join_list[$list_pointer-1]["today"];
		
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$appsflyer_install_cnt_list[$i] = $appsflyer_install_list[$list_pointer-1]["cnt"];
			$appsflyer_organic_install_cnt_list[$i] = $appsflyer_organic_install_list[$list_pointer-1]["cnt"];
			$user_join_cnt_list[$i] = $user_join_list[$list_pointer-1]["cnt"];
			
			$list_pointer--;
		}
		else 
		{
			$appsflyer_install_cnt_list[$i] = 0;
			$appsflyer_organic_install_cnt_list[$i] = 0;
			$user_join_cnt_list[$i] = 0;
		}
		
		$date_list[$i] = $date_pointer;
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<link type="text/css" href="/js/themes/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
	function tab_change(tab)
	{
		var search_form = document.search_form;
		search_form.tab.value = tab;
		search_form.submit();
	}
	
	function change_term(term)
	{
		var search_form = document.search_form;
		
		var all = document.getElementById("term_all");
		var ios = document.getElementById("term_ios");
		var android = document.getElementById("term_android");
		var amazon = document.getElementById("term_amazon");
		
		search_form.term.value = term;
		
		if (term == "term")
		{
			all.className 		= "btn_schedule_select";
			ios.className 		= "btn_schedule";
			android.className 	= "btn_schedule";
			amazon.className 	= "btn_schedule";
		}
		else if (term == "1")
		{
			all.className 		= "btn_schedule";
			ios.className 		= "btn_schedule_select";
			android.className 	= "btn_schedule";
			amazon.className 	= "btn_schedule";
		}
		else if (term == "1")
		{
			all.className 		= "btn_schedule";
			ios.className 		= "btn_schedule_select";
			android.className 	= "btn_schedule";
			amazon.className 	= "btn_schedule";
		}
		else if (term == "2")
		{
			all.className 		= "btn_schedule";
			ios.className 		= "btn_schedule";
			android.className 	= "btn_schedule_select";
			amazon.className 	= "btn_schedule";
		}
		else if (term == "3")
		{
			all.className 		= "btn_schedule";
			ios.className 		= "btn_schedule";
			android.className 	= "btn_schedule";
			amazon.className 	= "btn_schedule_select";
		}
	
		search_form.submit();
	}
	
	$(function() {
		$("#startdate").datepicker({});
		$("#enddate").datepicker({});
	});

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
		
	function search()
	{
	    var search_form = document.search_form;
	    
	    if (search_form.startdate.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    }
	    
	    if (search_form.enddate.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    }
	    
	    search_form.submit();
	}
	
	google.load("visualization", "1", {packages:["corechart"]});

	function drawChart() 
	{
		var data1 = new google.visualization.DataTable();
		data1.addColumn('string', '날짜');
		data1.addColumn('number', 'Appsflyer');
		data1.addColumn('number', 'GameDB');
		data1.addRows([
<?
		    for ($j=sizeof($date_list); $j>0; $j--)
		    {
			    $_date = $date_list[$j-1];
				$_appsflyer_cnt = $appsflyer_install_cnt_list[$j-1] + $appsflyer_organic_install_cnt_list[$j-1];;
				$_gamedb_cnt = $user_join_cnt_list[$j-1];
				
				echo("['".$_date."',".$_appsflyer_cnt.",".$_gamedb_cnt."]");
				
				if ($j > 1)
				{
					echo(",");
				}
			}
?>
		]);
		
		var data2 = new google.visualization.DataTable();
		data2.addColumn('string', '날짜');
		data2.addColumn('number', '비중(GameDB/Appsflyer)');
		data2.addRows([
<?
		    for ($j=sizeof($date_list); $j>0; $j--)
		    {
			    $_date = $date_list[$j-1];
				$_appsflyer_cnt = $appsflyer_install_cnt_list[$j-1] + $appsflyer_organic_install_cnt_list[$j-1];
				$_gamedb_cnt = $user_join_cnt_list[$j-1];
				
				$_all_cnt = $_appsflyer_cnt + $_gamedb_cnt; 
				$_join_rate = number_format($_gamedb_cnt / $_appsflyer_cnt * 100, 2);
				
				echo("['".$_date."',".$_join_rate."]");
				
				if ($j > 1)
				{
					echo(",");
				}
			}
?>
		]);
		
        var options = {
    	        title:'',                                                
    	        width:1100,                         
    	        height:200,
    	        axisTitlesPosition:'in',
    	        curveType:'none',
    	        focusTarget:'category',
    	        interpolateNulls:'true',
    	        legend:'top',
    	        fontSize : 12,
    	        chartArea:{left:100,top:40,width:1100,height:130}
    	    };

		chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
		chart.draw(data1, options);
		
		chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
		chart.draw(data2, options);
	}

	google.setOnLoadCallback(drawChart);
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; appsflyer 유입 전환 비중</div>
		
		<br/>
		
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<input type="hidden" name="term" id="term" value="<?= $term ?>" />
			<div class="search_box">
				<input type="button" class="<?= ($term == "platform") ? "btn_schedule_select" : "btn_schedule" ?>" value="통합" id="term_all" onclick="change_term('platform')" />
				<input type="button" class="<?= ($term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_term('1')" />
				<input type="button" class="<?= ($term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_term('2')"    />
				<input type="button" class="<?= ($term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_term('3')"    />
				
				<br/><br/>
				
				<div style="float: right; margin-right: 2px">
					그래프 날짜 : &nbsp;&nbsp;
					<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" />~
					<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" />
					<input type="button" class="btn_search" value="검색" onclick="search()" />
				</div>
			</div>
		</form>
	</div>
	<!-- //title_warp -->


	<div class="h2_title mt30" name="div_normal">가입자수</div>
	<div id="chart_div1" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title mt30" name="div_normal">가입 비중</div>
	<div id="chart_div2" name="div_normal" style="height:230px; min-width: 500px"></div>
</div>
<?
    $db_main->end();
	$db_main2 ->end();
	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>