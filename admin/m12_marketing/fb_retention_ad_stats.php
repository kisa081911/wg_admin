<?
	$top_menu = "marketing";
	$sub_menu = "fb_retention_campaign_web";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$start_searchdate = $_GET["start_searchdate"];
	$end_searchdate = $_GET["end_searchdate"];
	$start_orderdate = $_GET["start_orderdate"];
	$end_orderdate = $_GET["end_orderdate"];

	$ids = $_GET["ids"];
		
	$today = date("Y-m-d");
	
	if($start_searchdate == "")
		$start_searchdate = date("Y-m-d", strtotime("-15 day"));
	
	if($end_searchdate == "")
		$end_searchdate = $today;
	
	$ad_stats_table = "tbl_ad_retention_stats";
	
	$db_main2 = new CDatabase_Main2();
	
	if(($start_orderdate != "" && $end_orderdate != ""))
	{
		$add_sql = " WHERE sub2.writedate BETWEEN '$start_orderdate 00:00:00' AND '$end_orderdate 23:59:59' ";
	}
	
	$sql = "SELECT t1.ad_id, ad_name, adset_name, adset_id, campaign_name, campaign_id, spend, thumbnail_url, IFNULL(money, 0) AS money, IFNULL(buy_user_cnt, 0) AS buy_user_cnt ".
			"FROM ( ".
			"	SELECT ad_id, (SELECT ad_name FROM $ad_stats_table  WHERE ad_id = a.ad_id  ORDER BY today DESC  LIMIT 1) AS ad_name, ". 
			"		(SELECT adset_name FROM $ad_stats_table  WHERE adset_id = a.adset_id  ORDER BY today DESC  LIMIT 1) AS adset_name, adset_id, ". 
			"		(SELECT campaign_name FROM $ad_stats_table  WHERE campaign_id = a.campaign_id  ORDER BY today DESC  LIMIT 1) AS campaign_name, campaign_id, ". 
			"		SUM(spend) AS spend, STATUS, thumbnail_url, MAX(updated_time) AS updated_time ".
			"	FROM $ad_stats_table a ".
			"	WHERE today BETWEEN '$start_searchdate' AND '$end_searchdate'  AND adset_id = $ids ".  
			"	GROUP BY ad_id ".
			") t1 LEFT JOIN ( ".
			"	SELECT sub1.ad_id, IFNULL(SUM(money), 0) AS money, COUNT(DISTINCT useridx) AS buy_user_cnt ".
			"	FROM ( ".
			"		SELECT ad_id ".
			"		FROM $ad_stats_table ".
			"		WHERE today BETWEEN '$start_searchdate' AND '$end_searchdate' AND adset_id = $ids ".
			"		GROUP BY ad_id ".
			"	) sub1 JOIN tbl_user_marketing_retention_order sub2 ON sub1.ad_id = sub2.fb_rt_id ".
			$add_sql.
			"	GROUP BY ad_id ".
			") t2 ON t1.ad_id = t2.ad_id ".
			"ORDER BY updated_time DESC";
	$contents_list = $db_main2->gettotallist($sql);
	
 	$sql = "SELECT t1.ad_id, SUM(return_count) AS return_count ".
			"FROM ( ".
			"		SELECT ad_id, SUM(spend) AS spend, MAX(updated_time) AS updated_time ".
			"		FROM $ad_stats_table a ".
			"		WHERE today BETWEEN '$start_searchdate' AND '$end_searchdate' AND adset_id = $ids GROUP BY ad_id ".
			") t1 LEFT JOIN ( ".
			"	SELECT ad_id, COUNT(*) AS return_count ".
			"	FROM ( ".
			"		SELECT ad_id FROM tbl_ad_retention_stats WHERE today BETWEEN '$start_searchdate' AND '$end_searchdate' AND adset_id = $ids GROUP BY ad_id ".
			"	) sub1 JOIN tbl_user_marketing_retention sub2 ON sub1.ad_id = sub2.fb_rt_id ".
			"	GROUP BY ad_id ".
			") t2 ON t1.ad_id = t2.ad_id ".
			"GROUP BY ad_id ".
			"ORDER BY updated_time DESC";
	$return_count_list = $db_main2->gettotallist($sql);

	$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, COUNT(*) AS return_count ".
			"FROM ( ".
			"	SELECT ad_id FROM tbl_ad_retention_stats WHERE today BETWEEN '$start_searchdate' AND '$end_searchdate' AND adset_id = $ids GROUP BY ad_id ".
			") sub1 JOIN tbl_user_marketing_retention sub2 ON sub1.ad_id = sub2.fb_rt_id ".
			"GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ";
	$chart_list = $db_main2->gettotallist($sql);
	
	$sql = "SELECT today, SUM(spend) AS spend ".
			"FROM tbl_ad_retention_stats ".
			"WHERE today BETWEEN '$start_searchdate' AND '$end_searchdate' AND adset_id = $ids ".
			"GROUP BY today ";
	$spend_list = $db_main2->gettotallist($sql);
	
	$db_main2->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<link type="text/css" rel="stylesheet" href="../../css/marketing.css" />
<script type="text/javascript">
	$(function() {
		$("#start_searchedate").datepicker({});
		$("#end_searchdate").datepicker({});
		$("#start_orderdate").datepicker({});
		$("#end_orderdate").datepicker({});
	});

google.load("visualization", "1", {packages:["corechart"]});
	function drawChart()
	{
		var data1 = new google.visualization.DataTable();
	    data1.addColumn('string', '날짜');
	    data1.addColumn('number', '결과: 복귀');
	    data1.addRows([<?
		foreach ($chart_list as $chart)
		{
			$return_count = $chart['return_count'];
			$chart_date = date("m-d", strtotime($chart['today']));
			
			if($chart_date != date("m-d", strtotime($today)))
			{
				echo "['".$chart_date."',  {v:".$return_count." ,f:'".number_format($return_count)."'}],";
			}
			else
			{
				echo "['오늘',  {v:".$return_count." ,f:'".number_format($return_count)."'}]";
			}
		}
		?>]);
	    
	    var data2 = new google.visualization.DataTable();
	    data2.addColumn('string', '날짜');
	    data2.addColumn('number', '결과당 비용');
	    data2.addRows([<?
		for($i=0; $i<sizeof($chart_list); $i++)
		{
			$spend = $spend_list[$i]['spend'] / $chart_list[$i]['return_count'];
			$chart_date = date("m-d", strtotime($chart_list[$i]['today']));
			
			if($chart_date != date("m-d", strtotime($today)))
			{
				echo "['".$chart_date."',  {v:".$spend." ,f:'".number_format($spend,2)."'}],";
			}
			else
			{
				echo "['오늘',  {v:".$spend." ,f:'".number_format($spend,2)."'}]";
			}
		}
		?>]);
	
	
	    var options1 = {
	            width:1050,                       
	            height:190,
	            axisTitlesPosition:'in',
	            curveType:'none',
	            focusTarget:'category',
	            interpolateNulls:'true',
	            legend:'top',
	            fontSize : 12,
	            chartArea:{left:60,top:40,width:1040,height:100},
	        vAxis: { gridlines: { count: 3 } }
	        };
	        
	        var options2 = {
	            width:1050,                       
	            height:190,
	            axisTitlesPosition:'in',
	            curveType:'none',
	            focusTarget:'category',
	            interpolateNulls:'true',
	            legend:'top',
	            fontSize : 12,
	            chartArea:{left:60,top:40,width:1040,height:100},
	        vAxis: { gridlines: { count: 3 } }
	        };        
	    var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
	    chart.draw(data1, options1);        
	    
	    chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
	    chart.draw(data2, options2);      
	}
        
	google.setOnLoadCallback(drawChart);	

	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.start_date.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.start_date.focus();
	        return;
	    } 
	
	    search_form.submit();
	}

</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Facebook Retention 광고 현황 - Web </div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<input type="hidden" name="select_type" value="<?=$select_type?>"> 
				<input type="hidden" name="ids" value="<?=$ids?>">
				<input type="text" class="search_text" id="start_searchedate" name="start_searchdate" value="<?= $start_searchdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_searchdate" name="end_searchdate" value="<?= $end_searchdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
			
			<div class="clear"></div>
                
			<div class="search_box">
				<input type="text" class="search_text" id="start_orderdate" name="start_orderdate" value="<?= $start_orderdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_orderdate" name="end_orderdate" value="<?= $end_orderdate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
				&nbsp;&nbsp;&nbsp;&nbsp;기간내 결제
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $start_searchdate ?></span> ~ <span><?= $end_searchdate ?></span> 통계입니다
	</div>
		<div id="chart_div1" style="height:200px; min-width: 500px"></div>
		<div id="chart_div2" style="height:200px; min-width: 500px"></div>
	<br/>
	<br/>
	<div id="js_1_layer" style="width: 220px;margin-left: 392px;margin-top: 32px;position: absolute; display:none;">
		<div class="io1" style="margin-left: 183px;">&nbsp;</div>
		<div class="_53il" style="border:0;border-radius:2px;box-shadow:0 0 0 1px rgba(0, 0, 0, .1), 0 1px 10px rgba(0, 0, 0, .35);">
			<div class="_53ij">
				<div data-reactroot="">
					<div class="_2ny1">
						<div class="_2ny2">결과</div>
						<div class="_f-m _2ny3">
							<div class="_if">
								<p class="_3p8">광고로 인해 발생한 결과. 이 결과는 선택한 목표를 기준으로 합니다.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="js_2_layer" style="width: 220px;margin-left: 503px;margin-top: 32px;position: absolute; display:none;">
		<div class="io1" style="margin-left: 183px;">&nbsp;</div>
		<div class="_53il" style="border:0;border-radius:2px;box-shadow:0 0 0 1px rgba(0, 0, 0, .1), 0 1px 10px rgba(0, 0, 0, .35);">
			<div class="_53ij">
				<div data-reactroot="">
					<div class="_2ny1">
						<div class="_2ny2">도달</div>
						<div class="_f-m _2ny3">
							<div class="_if">
								<p class="_3p8">광고가 게재된 사람 수입니다.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="js_3_layer" style="width: 220px;margin-left: 616px;margin-top: 32px;position: absolute; display:none;">
		<div class="io1" style="margin-left: 183px;">&nbsp;</div>
		<div class="_53il" style="border:0;border-radius:2px;box-shadow:0 0 0 1px rgba(0, 0, 0, .1), 0 1px 10px rgba(0, 0, 0, .35);">
			<div class="_53ij">
				<div data-reactroot="">
					<div class="_2ny1">
						<div class="_2ny2">비용</div>
						<div class="_f-m _2ny3">
							<div class="_if">
								<p class="_3p8">목표와 관련되어 발생한 각 행동당 평균 지출 비용.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="js_4_layer" style="width: 220px;margin-left: 808px;margin-top: 32px;position: absolute; display:none;">
		<div class="io1" style="margin-left: 183px;">&nbsp;</div>
		<div class="_53il" style="border:0;border-radius:2px;box-shadow:0 0 0 1px rgba(0, 0, 0, .1), 0 1px 10px rgba(0, 0, 0, .35);">
			<div class="_53ij">
				<div data-reactroot="">
					<div class="_2ny1">
						<div class="_2ny2">지출금액</div>
						<div class="_f-m _2ny3">
							<div class="_if">
								<p class="_3p8">광고 관리자에서 선택한 기간 동안 지출한 총 금액입니다.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<table class="tbl_list_basic_matket1" >
		<colgroup>
			<col width="5">
			<col width="270">
			<col width="40">
			<col width="50">
            <col width="60">
            <col width="50">
            <col width="60">
            <col width="60">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="5">
		</colgroup>
        <thead>
        	<tr>
        		<th class="tdl">&nbsp;</th>
        		<th class="tdl">광고 이름</th>
            	<th class="tdr">결과 <i class="_2ny7 img sp_rHAvPENsMSL sx_ff2fa9" aria-owns="js_24" aria-haspopup="true" tabindex="0" id="js_1" onmouseover="javascript:$('#js_1_layer').css('display','block');$('#js_1').css('opacity','1');" onmouseout="javascript:$('#js_1_layer').css('display','none');$('#js_1').css('opacity','.6');"></i></th>             	
               	<th class="tdr">비용 <i class="_2ny7 img sp_rHAvPENsMSL sx_ff2fa9" aria-owns="js_24" aria-haspopup="true" tabindex="0" id="js_3" onmouseover="javascript:$('#js_3_layer').css('display','block');$('#js_3').css('opacity','1');" onmouseout="javascript:$('#js_3_layer').css('display','none');$('#js_3').css('opacity','.6');"></i></th>
               	<th class="tdr">지출금액 <i class="_2ny7 img sp_rHAvPENsMSL sx_ff2fa9" aria-owns="js_24" aria-haspopup="true" tabindex="0" id="js_5" onmouseover="javascript:$('#js_5_layer').css('display','block');$('#js_5').css('opacity','1');" onmouseout="javascript:$('#js_5_layer').css('display','none');$('#js_5').css('opacity','.6');"></i></th>               	
               	<th class="tdr">매출<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_7"></i></th>
               	<th class="tdr">결제자수<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_8"></i></th>
               	<th class="tdr">PUR<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_9"></i></th>
               	<th class="tdr">ARPU<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_10"></i></th>
               	<th class="tdr">ARPPU<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_11"></i></th>
               	<th class="tdr">ROI<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_12"></i></th>
               	<th class="tdl">&nbsp;</th>
         	</tr>
        </thead>
		<tbody>
<?
		$total_money = 0;
		$total_buy_user_cnt = 0;
		
		for ($i=0; $i<sizeof($contents_list); $i++)
		{			
			$ad_id = $contents_list[$i]['ad_id'];
			$ad_name = $contents_list[$i]['ad_name'];
			$adset_id = $contents_list[$i]['adset_id'];
			$adset_name = $contents_list[$i]['adset_name'];
			$campaign_name = $contents_list[$i]['campaign_name'];
			$campaign_id = $contents_list[$i]['campaign_id'];
			$return_count = $return_count_list[$i]['return_count'];
			$spend = $contents_list[$i]['spend'];
			$money = $contents_list[$i]['money'];
			$buy_user_cnt = $contents_list[$i]['buy_user_cnt'];
			$thumbnail_url = $contents_list[$i]['thumbnail_url'];
			
			$pur = ($return_count == 0) ? 0 : round(($buy_user_cnt / $return_count) * 100, 2);
			$arpu = ($return_count == 0) ? 0 : round($money / $return_count, 2);
			$arppu = ($buy_user_cnt == 0) ? 0 : round($money / $buy_user_cnt, 1);
			$roi = ($spend == 0) ? 0 : round($money / $spend * 100, 2);
			
			$total_money += $money;
			$total_buy_user_cnt += $buy_user_cnt;
			
			$total_return_count += $return_count;
			$total_spend +=$spend;
 ?>
	
			<tr>
				<td class="tdl">&nbsp;</td>
        		<td class="tdl point_title">
        			<img src="<?=$thumbnail_url?>" style="width: 46px;height: 46px;float: left;margin-top: 5px;">
        			<div style="margin-top: 7px;margin-left: 5px;float: left;overflow: visible;text-overflow: ellipsis;overflow: hidden;">
        				<span style="text-decoration:none; color:#519eed;"><?=$ad_name?></span>
        				<br/>
        				<span>
        					<a href="./fb_retention_campaign_stats.php?start_searchdate=<?= $start_searchdate ?>&end_searchdate=<?= $end_searchdate ?>&start_orderdate=<?= $start_orderdate ?>&end_orderdate=<?= $end_orderdate ?>" style="font-size: 12px;line-height: 16px;"><?=$campaign_name?></a> &lt; 
        					<a href="./fb_retention_adsets_stats.php?ids=<?=$campaign_id?>&select_type=<?=$select_type?>&start_searchdate=<?= $start_searchdate ?>&end_searchdate=<?= $end_searchdate ?>&start_orderdate=<?= $start_orderdate ?>&end_orderdate=<?= $end_orderdate ?>" style="font-size: 12px;line-height: 16px;"><?=$adset_name?></a>
        				</span>
        			</div>
        		</td>
            	<td class="tdr "><?=number_format($return_count)?><br/><span >복귀</span></td>
               	<td class="tdr ">$<?=($return_count == 0) ? 0 : number_format($spend/$return_count, 2)?><br/><span >복귀 당</span></td>
               	<td class="tdr ">$<?=number_format($spend,2)?></td>
               	<td class="tdr ">$<?=number_format($money,2)?></td>
               	<td class="tdr "><?=number_format($buy_user_cnt)?></td>
               	<td class="tdr "><?=number_format($pur, 2)?>%</td>
               	<td class="tdr ">$<?=number_format($arpu, 2)?></td>
               	<td class="tdr ">$<?=number_format($arppu, 1)?></td>
               	<td class="tdr "><?=number_format($roi, 2)?>%</td>
               	<td class="tdl ">&nbsp;</td>
         	</tr>
<?
 	}
 	
 	$total_pur = ($total_return_count == 0) ? 0 : round(($total_buy_user_cnt/$total_return_count)*100, 2);
	$total_arpu = ($total_return_count == 0) ? 0 : round($total_money/$total_return_count, 2);
	$total_arppu = ($total_buy_user_cnt == 0) ? 0 : round($total_money/$total_buy_user_cnt, 1);
	$total_roi = ($total_spend == 0) ? 0 : round($total_money/$total_spend*100, 2);
?>
			<tr>
         		<td class="tdl">&nbsp;</td>
        		<td class="tdl point_title">광고 <?=number_format(sizeof($contents_list))?>개 결과</td>            	
            	<td class="tdr "><?=number_format($total_return_count)?><br/><span >복귀</span></td>
               	<td class="tdr ">$<?=($total_return_count == 0) ? 0 : number_format($total_spend/$total_return_count, 2)?><br/><span >복귀 당</span></td>
               	<td class="tdr ">$<?=number_format($total_spend,2) ?><br/><span >총 지출</span></td>
               	<td class="tdr ">$<?=number_format($total_money,2)?><br/><span >총 매출</span></td>
               	<td class="tdr "><?=number_format($total_buy_user_cnt)?><br/><span >총 결제자수</span></td>
               	<td class="tdr "><?=number_format($total_pur, 2)?>%<br/><span >총 PUR</span></td>
               	<td class="tdr ">$<?=number_format($total_arpu, 2)?><br/><span >총 ARPU</span></td>
               	<td class="tdr ">$<?=number_format($total_arppu, 1)?><br/><span >총 ARPPU</span></td>  
               	<td class="tdr "><?=number_format($total_roi, 2)?>%<br/><span >총 ROI</span></td>
               	<td class="tdl">&nbsp;</td>
         	</tr>
        </tbody>
	</table>
	
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>