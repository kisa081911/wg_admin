<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");

	$dbaccesstype = $_SESSION["dbaccesstype"];
	
	$basic_date = $_GET["basic_date"];
	
	if(basic_date == "")
		$basic_date = date("Y").'-'.date("m");
	
	$basic_date = explode("-", $basic_date);
	
	$basic_year = $basic_date[0];
	$basic_month = $basic_date[1];
	
	$spend_value = explode(",", $_POST["spend_value"]);
	$platform = $_GET["platform"];
	$agency_index = $_GET["agency_index"];
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>비용입력</title>
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script>
<script type="text/javascript">
var tmp_hosturl = "<?= HOST_URL ?>";
	function change_platform(platform)
	{
		var agency_title = document.getElementById("agency_title");
		var agency_index_1 = document.getElementById("agency_index_1");
		var agency_index_2 = document.getElementById("agency_index_2");
		
		
		if(platform == 1 || platform == 2)
		{
			agency_title.style.display = "";
			agency_index_1.style.display = "";
			agency_index_2.style.display = "none";
		}
 		else if (platform == 3)
 		{
 			agency_title.style.display = "";
 			agency_index_1.style.display = "none";
 			agency_index_2.style.display = "";
 		}
		else
		{
			agency_title.style.display = "none";
			agency_index_1.style.display = "none";
			agency_index_2.style.display = "none";
		}
	}

	function get_agency_spend()
	{
		var input_form = document.input_form;
		var basic_year = input_form.basic_year.value;
		var basic_month = input_form.basic_month.value;
		var platform = input_form.platform.value;
		var agency_index = 0;
		
		if(platform == 1 || platform == 2)
			agency_index = input_form.agency_index_1.value;
 		else if(platform == 3)
 			agency_index = input_form.agency_index_2.value;

		if(basic_year == "")
		{
			alert("기준년을 입력하세요.");
			input_form.basic_year.focus();
			return;
		}
		if(basic_month == "")
		{
			alert("기준월을 입력하세요.");
			input_form.basic_month.focus();
			return;
		}

		if(platform == "")
		{
			alert("platform을 선택하세요.");
			input_form.platform.focus();
			return;
		}
		if(agency_index == "")
		{
			alert("agency를 선택하세요.");
			return;
		}

		if(!basic_month.match(/0.*/))
		{
			if(basic_month < 10)
			{
				basic_month = "0" + basic_month;
			}
		}

		var param = {};
		param.basic_date = basic_year + "-" + basic_month;
		param.platform = platform;
		param.agency_index = agency_index;

		WG_ajax_list("user/get_agency_spend", param, get_agency_spend_callback);
	}

	function get_agency_spend_callback(result, reason, totalcount, list)
	{
		if (!result)
		{
			alert("오류 발생 - " + reason);
		}
		else
		{
			var tbody = document.getElementById("agency_contents");

			while (tbody.childNodes.length > 0)
                tbody.removeChild(tbody.childNodes[0]);

           for(var i=0; i<list.length; i++)
            {
                var tr = document.createElement("tr");
                var td = document.createElement("td");
                var th = document.createElement("th");
                
                var inputtxt = document.createElement('INPUT');
                inputtxt.name = 'spend[]';
                inputtxt.id = 'spend[]';
                inputtxt.type = 'text';
                inputtxt.value = list[i].spend;
                inputtxt.size = '8';
                inputtxt.style.textAlign = 'right';
                
                var inputtxt2 = document.createElement('INPUT');
                inputtxt2.name = 'spend_origin[]';
                inputtxt2.id = 'spend_origin[]';
                inputtxt2.type = 'text';
                inputtxt2.value = list[i].spend_origin;
                inputtxt2.size = '8';
                inputtxt2.style.textAlign = 'right';
                
                var inputtxt3 = document.createElement('INPUT');
                inputtxt3.name = 'spend_minus[]';
                inputtxt3.id = 'spend_minus[]';
                inputtxt3.type = 'text';
                inputtxt3.value = list[i].spend_minus;
                inputtxt3.size = '8';
                inputtxt3.style.textAlign = 'right';

                var tmp_txt = document.createTextNode("=");
                
                var txt1 = document.createElement('text');
                txt1.type = 'text';
                txt1.value = 1;
                txt1.size = '8';
                txt1.style.textAlign = 'center';
                txt1.appendChild(tmp_txt);

                tmp_txt = document.createTextNode("-");
                
                var txt2 = document.createElement('text');
                txt2.type = 'text';
                txt2.value = '-';
                txt2.size = '8';
                txt2.style.textAlign = 'center';
                txt2.appendChild(tmp_txt);
                
                td.className = "tdc";
                td.innerText = list[i].today;
                tr.appendChild(td);

                // 최종 비용
				td = document.createElement("td");
				td.className = "tdr";
				td.appendChild(inputtxt);
				tr.appendChild(td);

				// = 
				td = document.createElement("td");
				td.className = "tdc";
				td.appendChild(txt1);
				tr.appendChild(td);

				// 비용
				td = document.createElement("td");
				td.className = "tdr";
				td.appendChild(inputtxt2);
				tr.appendChild(td);

				// -
				td = document.createElement("td");
				td.className = "tdc";
				td.appendChild(txt2);
				tr.appendChild(td);

				// 차감 비용
				td = document.createElement("td");
				td.className = "tdr";
				td.appendChild(inputtxt3);
				tr.appendChild(td);
				
                tbody.appendChild(tr);
            }

            var input_form = document.input_form;
            var platform = input_form.platform.value;
            var agency_index = 0;
			var platform_name = "";
			var agency_name = "";
	
			if(platform == 1 || platform == 2)
				agency_index = input_form.agency_index_1.value;
 			else if(platform == 3)
 				agency_index = input_form.agency_index_2.value; 
            
            if(input_form.platform.value == "1")
        		platform_name = "iOS";
        	else if(input_form.platform.value == "2")
        		platform_name = "Android";
         	else if(input_form.platform.value == "3")
         		platform_name = "Amazon";
        	
        	if(agency_index == "8")
        		agency_name = "Facebook Ads";
        	else if(agency_index == "2")
        		agency_name = "vungle_int";
        	else if(agency_index == "10")
        		agency_name = "258angel_int";
        	else if(agency_index == "11")
        		agency_name = "amazon";
        	else if(agency_index == "12")
        		agency_name = "cheetahmobile_int";
        	else if(agency_index == "13")
        		agency_name = "mobvista_int";
        	else if(agency_index == "14")
        		agency_name = "yeahmobi_int";
        	else if(agency_index == "15")
        		agency_name = "socialclicks";
        	else if(agency_index == "16")
        		agency_name = "adaction_int";
        	else if(agency_index == "17")
        		agency_name = "Twitter Ads";
        	else if(agency_index == "18")
        		agency_name = "taptica_int";
        	else if(agency_index == "19")
        		agency_name = "glispa_int";
        	else if(agency_index == "22")
        		agency_name = "googleadwords_int";
        	else if(agency_index == "23")
        		agency_name = "moloco_int";
        	else if(agency_index == "24")
        		agency_name = "heyzap_int";
        	else if(agency_index == "26")
        		agency_name = "crossinstall_int";
        	else if(agency_index == "29")
        		agency_name = "iconpeak_int";
        	else if(agency_index == "30")
        		agency_name = "dqna_int";
        	else if(agency_index == "31")
        		agency_name = "Apple Search Ads";
        	else if(agency_index == "32")
        		agency_name = "amazon";
			else if(agency_index == "36")
        		agency_name = "liftoff_int";
			else if(agency_index == "37")
        		agency_name = "m_ddi_int";
			else if(agency_index == "38")
        		agency_name = "m_duc_int";
    		
        	document.getElementById("agency_name").innerHTML = agency_name + "(" + platform_name + ")";
		}
	}

	function update_agency_spend()
	{
		var input_form = document.input_form;
		var submit_form = document.submit_form;

		var platform = input_form.platform.value;
		var agency_index = 0;

		if(platform == 1 || platform == 2)
			agency_index = input_form.agency_index_1.value;
 		else if(platform == 3)
 			agency_index = input_form.agency_index_2.value;
		
		if(submit_form.elements.length == 1)
		{
			var spend_length = 1;
		}
		else
		{
			var spend_length = submit_form.elements['spend_origin[]'].length;
		}
		
		var spend_value = new Array(spend_length);
		var spend_minus_value = new Array(spend_length);

		if(spend_length == 1)
		{
			spend_value = submit_form.elements['spend_origin[]'].value;
			spend_minus_value = submit_form.elements['spend_minus[]'].value;
		}
		else
		{
	 		for(i = 0; i < spend_length; i++)
	 		{
	 	 		if(submit_form.elements['spend[]'][i].value == "")
	 	 		{
	 	 	 		alert("비용을 입력해주세요.\n(기본값 0)");
	 	 	 		submit_form.elements['spend_origin[]'][i].focus();
	 	 	 		return;
	 	 		}
	 			spend_value[i] = submit_form.elements['spend_origin[]'][i].value;
	 			
	 	 		if(submit_form.elements['spend_minus[]'][i].value == "")
	 	 		{
	 	 	 		alert("차감금액을 입력해주세요.\n(기본값 0)");
	 	 	 		submit_form.elements['spend_minus[]'][i].focus();
	 	 	 		return;
	 	 		}
	 			spend_minus_value[i] = submit_form.elements['spend_minus[]'][i].value;
	 		}
		}
		
 		if(confirm("수정하시겠습니까?") == 0)
			return;
			
  		var param = {};
		param.basic_date = input_form.basic_year.value + "-" + input_form.basic_month.value;
		param.platform = platform;
		param.agency_index = agency_index;
		param.spend_value = spend_value;
		param.spend_minus_value=spend_minus_value;
 	    
  	  	WG_ajax_execute("user/update_agency_spend", param, update_agency_spend_callback);
	}

	function update_agency_spend_callback(result, reason)
	{
	    if (!result)
	    {
	        alert("오류 발생 - " + reason);
	    }
	    else
	    {
		    alert("수정 완료되었습니다.");
		    get_agency_spend();
	    }
	}

	function layer_close()
	{
	    window.parent.close_layer_popup("<?= $_GET["layer_no"] ?>");
	}
</script>
</head>

<body class="layer_body" style="width:750px" onload="window.focus()" onkeyup="if (getEventKeycode(event) == 27) { layer_close(); }">
	<div id="layer_wrap" style="width:750px;height:450px;">
		<br/>
		<form name="input_form" id="input_form" method="get" onsubmit="return false;">
		    platform
		    <select name="platform" id="platform" onchange="change_platform(this.value);">
		    	<option value="">선택하세요</option>
		        <option value="1">iOS</option>
		        <option value="2">Android</option>
		       	<option value="3">Amazon</option>
		    </select>
		    <br/>
		    <text name="agency_title" id="agency_title" style="display:none">agency</text>
		    <select name="agency_index_1" id="agency_index_1" style="display:none">
		    	<option value="">선택하세요</option>
		        <option value="8">Facebook Ads</option>
			<option value="2">vungle_int</option>
		        <option value="10">258angel_int</option>
		        <option value="12">cheetahmobile_int</option>
		        <option value="13">mobvista_int</option>
		        <option value="14">yeahmobi_int</option>
		        <option value="15">socialclicks</option>
		        <option value="16">adaction_int</option>
		        <option value="17">Twitter Ads</option>
		        <option value="18">taptica_int</option>
		        <option value="19">glispa_int</option>
		        <option value="22">googleadwords_int</option>
		        <option value="23">moloco_int</option>
		        <option value="24">heyzap_int</option>
		        <option value="26">crossinstall_int</option>
		        <option value="29">iconpeak_int</option>
		        <option value="30">dqna_int</option>
		        <option value="31">Apple Search Ads</option>
				<option value="36">liftoff_int</option>
				<option value="37">m_ddi_int</option>
				<option value="38">m_duc_int</option>
		    </select>
		    <select name="agency_index_2" id="agency_index_2" style="display:none">
		    	<option value="">선택하세요</option>		        
		        <option value="8">Facebook Ads</option>
		        <option value="32">amazon</option>
		    </select>
		    <br/>
		    <input type="text" class="search_text" id="basic_year" name="basic_year" value="<?= $basic_year ?>" maxlength="10" style="width:30px" />년
		    <input type="text" class="search_text" id="basic_month" name="basic_month" value="<?= $basic_month ?>" maxlength="10" style="width:30px" />월
		    <input type="button" class="btn_02" onclick="get_agency_spend()" value="조회"/>
	    </form>
	    <div class="layer_header" >
	    	<div class="layer_title" id="agency_name"></div>    
	    	<img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="layer_close()" />	
	    </div>
    	
    	<div class="layer_contents_wrap" style="width:720px;height:450px; overflow-y:auto;">
    		<form name="submit_form" id="submit_form" method="get" onsubmit="return false;">
				<table>
					<colgroup>	                
	                <col width="35">
	                <col width="95">
	                <col width="35">
	                <col width="95">
	                <col width="35">
	                <col width="95">
	            	</colgroup>
	            	
					<thead>
						<tr>
							<th>날짜</th>
							<th>최종 비용</th>
							<th></th>
							<th>비용</th>
							<th></th>
							<th>차감 비용</th>
						</tr>
					</thead>
					<tbody id="agency_contents"></tbody>
				</table>
    		</form>
    	</div>
<?
	if($dbaccesstype == "1" || $dbaccesstype == "3")
	{   
?>
		<div style="text-align:right"><input type="button" class="btn_02" onclick="update_agency_spend();" value="수정"></div>
<?
	}
?>
    </div>
</body>