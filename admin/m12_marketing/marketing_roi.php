<?
    $top_menu = "marketing";
    $sub_menu = "roi";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $startdate = $_GET["startdate"];
    $enddate = $_GET["enddate"];
    
    $search_adflag = ($_GET["search_adflag"] == "") ? 0 : $_GET["search_adflag"];
    $search_data = ($_GET["search_data"] == "") ? 0 : $_GET["search_data"];
    $search_view = ($_GET["search_view"] == "") ? 0 : $_GET["search_view"];
    
    //오늘 날짜 정보
    $today = get_past_date(date("Y-m-d"),1,"d");
    $before_day = "2016-06-28";
    $startdate = ($startdate == "") ? $before_day : $startdate;
    $enddate = ($enddate == "") ? $today : $enddate;
    
    $pagename = "marketing_roi.php";
    
    $db_other = new CDatabase_Other();
    
    $search_data_field = "";
    $result_prefix = "";
    
    if($search_data == 0)
    {
    	$search_data_field = "pay_amount";
    }
    else if($search_data == 1)
    {
    	$search_data_field = "pay_amount";
    	$result_prefix = "$";
    }
    else if($search_data == 2)
    {
    	$search_data_field = "pay_count";
    }
    else if($search_data == 3)
    {
    	$search_data_field = "payer_count";
    }
    
    $search_view_table = "";
    $max_field = "";
    $max_sql = "";
    $add_sql = "";
    $where_sql = "";
    $adflag_sql = "";
    
    if($search_adflag != "0")
    {
    	$adflag_sql = "AND adflag='$search_adflag' ";
    }
    
    if($search_view == 0)
    {
    	$search_view_table = "tbl_marketing_roi_month";
    	$search_view_goal_query = " (SELECT goal FROM tbl_marketing_roi_month_goal WHERE month = MAX(t1.month)+1) AS goal_num ";
    	$max_field = "month";
    	$max_sql = "MAX($max_field)";
    	$column_name = "M";
    	$where_sql = "WHERE DATE_FORMAT('$startdate', '%Y-%m') <= today AND today <= DATE_FORMAT('$enddate', '%Y-%m') $adflag_sql";
    }
    else if($search_view == 1)
    {
    	$search_view_table = "tbl_marketing_roi_week";
    	
    	if(date('w') == 1)
    		$search_view_goal_query = " (SELECT goal FROM tbl_marketing_roi_week_goal WHERE week = MAX(t1.week)+1) AS goal_num ";
    	else
    		$search_view_goal_query = " (SELECT goal FROM tbl_marketing_roi_week_goal WHERE week = max_std+1) AS goal_num ";
    	
    	$max_field = "week";
    	
    	if(date('w') == 1)
    		$max_sql = "MAX(week) ";
    	else 
    		$max_sql = "(WEEK(NOW()) - SUBSTRING(today, 5, 2)) ";
    	
    	$column_name = "W";
    	$add_sql = " MAX(s_date) AS s_date, MAX(e_date) AS e_date, ";
    	$where_sql = "WHERE CONCAT(DATE_FORMAT('$startdate', '%Y'), WEEKOFYEAR('$startdate')) <= today AND today <= CONCAT(DATE_FORMAT('$enddate', '%Y'), WEEKOFYEAR('$enddate')) $adflag_sql";
    }
    else if($search_view == 2)
    {
    	$search_view_table = "tbl_marketing_roi_day";
    	$search_view_goal_query = " (SELECT goal FROM tbl_marketing_roi_week_goal WHERE week = MAX(t1.week)+1) AS goal_num ";
    	$max_field = "week";
    	$max_sql = "FLOOR(DATEDIFF(NOW(), today)/7)";
    	$column_name = "W";
    	$where_sql = "WHERE '$startdate' <= today AND today <= '$enddate' $adflag_sql";
    }
    
    $sql = "SELECT today, SUM(join_count) AS join_count, SUM(nogame_count) AS nogame_count, SUM(IF(adflag='nanigans', spend*1.0425, spend)) AS spend, $add_sql".
			"	SUM(IF($max_field = 0, $search_data_field, 0)) AS m_0, ".
			"	SUM(IF($max_field = 1, $search_data_field, 0)) AS m_1, ".
			"	SUM(IF($max_field = 2, $search_data_field, 0)) AS m_2, ".
			"	SUM(IF($max_field = 3, $search_data_field, 0)) AS m_3, ".
			"	SUM(IF($max_field = 4, $search_data_field, 0)) AS m_4, ".
			"	SUM(IF($max_field = 5, $search_data_field, 0)) AS m_5, ".
			"	SUM(IF($max_field = 6, $search_data_field, 0)) AS m_6, ".
			"	SUM(IF($max_field = 7, $search_data_field, 0)) AS m_7, ".
			"	SUM(IF($max_field = 8, $search_data_field, 0)) AS m_8, ".
			"	SUM(IF($max_field = 9, $search_data_field, 0)) AS m_9, ".
			"	SUM(IF($max_field = 10, $search_data_field, 0)) AS m_10, ".
			"	SUM(IF($max_field = 11, $search_data_field, 0)) AS m_11, ".
			"	SUM(return_count) AS return_count, ".
			"	$max_sql AS max_std, ".
			"	$search_view_goal_query ".			
			"FROM $search_view_table t1 ".
			"$where_sql ".
			"GROUP BY today";

    $roi_list = $db_other->gettotallist($sql);
    
    $sql = "SELECT adflag ".
			"FROM `tbl_marketing_roi_month` ".
			"GROUP BY adflag ".
			"ORDER BY adflag ASC";
    $adflag_list = $db_other->gettotallist($sql);
    
    $db_other->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#startdate").datepicker({ });
	});
	
	$(function() {
		$("#enddate").datepicker({ });
	});
	
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.startdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    } 
	
	    if (search_form.enddate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    } 
	
	    search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Marketing ROI</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<span class="search_lbl ml20">구분&nbsp;</span>
				<select name="search_adflag" id="search_adflag">
					<option value="0" <?= ($search_adflag == "0") ? "selected" : "" ?>>전체</option>
<?
				for($i=0; $i<sizeof($adflag_list); $i++)
				{
					$adflag = $adflag_list[$i]["adflag"];
?>
					<option value="<?= $adflag ?>" <?= ("$search_adflag" == "$adflag") ? "selected" : "" ?>><?= $adflag ?></option>
<?
				}
?>
				</select>
				<span class="search_lbl ml20">Data&nbsp;&nbsp;&nbsp;</span>
				<select name="search_data" id="search_data">
					<option value="0" <?= ($search_data == "0") ? "selected" : "" ?>>ROI</option>
					<option value="1" <?= ($search_data == "1") ? "selected" : "" ?>>결제금액</option>
					<option value="2" <?= ($search_data == "2") ? "selected" : "" ?>>결제건수</option>
					<option value="3" <?= ($search_data == "3") ? "selected" : "" ?>>결제자수</option>
				</select>
				<span class="search_lbl ml20">기간&nbsp;&nbsp;&nbsp;</span>
				<select name="search_view" id="search_view">
					<option value="0" <?= ($search_view == "0") ? "selected" : "" ?>>월</option>
					<option value="1" <?= ($search_view == "1") ? "selected" : "" ?>>주</option>
					<option value="2" <?= ($search_view == "2") ? "selected" : "" ?>>일</option>
				</select>
				<span class="search_lbl ml20">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" /> ~
				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $startdate ?> ~ <?= $enddate ?></span> 통계입니다
	</div>
	
	<table class="tbl_list_basic1" style="width:1400px;">
		<colgroup>
			<col width="110">
			<col width="50">
			<col width="50">
            <col width="50">
            <col width="60">
            <col width="60">
            <col width="45">
            <col width="45">
            <col width="45">
            <col width="45">
            <col width="45">
            <col width="45">
			<col width="45">
            <col width="45">
            <col width="45">
            <col width="45">
            <col width="45">
            <col width="45">
<?
	if($search_data == 0)
	{
?>
            <col width="90">
<?
	}
?>
		</colgroup>
        <thead>
         	<th>가입월</th>
			<th>비용</th>
			<th>CPI</th>
			<th>가입자수</th>
			<th>미게임</th>
			<th>이탈자복귀</th>
			<th><?= $column_name?>+1</th>
			<th><?= $column_name?>+2</th>
			<th><?= $column_name?>+3</th>
			<th><?= $column_name?>+4</th>
			<th><?= $column_name?>+5</th>
			<th><?= $column_name?>+6</th>
			<th><?= $column_name?>+7</th>
			<th><?= $column_name?>+8</th>
			<th><?= $column_name?>+9</th>
			<th><?= $column_name?>+10</th>
			<th><?= $column_name?>+11</th>
			<th><?= $column_name?>+12</th>
<?
	if($search_data == 0)
	{
?>
			<th>목표 ROI</th>
<?
	}
?>
        </thead>
        <tbody>
<?

    if(sizeof($roi_list) == 0)
    {
?>
   			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   				<td class="tdc" colspan="<?= ($search_data == 0) ? 19 : 18?>">검색 결과가 없습니다.</td>
   			</tr>
<?
   	}
   	else
   	{
   		for($i=0; $i<sizeof($roi_list); $i++)
   		{
   			$today = $roi_list[$i]["today"];
   			$spend = $roi_list[$i]["spend"];
   			$join_count = $roi_list[$i]["join_count"];
   			$cpi = ($join_count == 0) ? 0 : round($spend/$join_count, 2);
   			$nogame_count = $roi_list[$i]["nogame_count"];
   			$nogame_rate = ($join_count == 0) ? 0 : round($nogame_count/$join_count*100, 1);
   			$return_count = $roi_list[$i]["return_count"];
   			$return_rate = ($join_count == 0) ? 0 : round($return_count/$join_count*100, 1);
   			$max_std = $roi_list[$i]["max_std"];
   			$goal_num = $roi_list[$i]["goal_num"];
   			
   			if($search_view == 1)
   			{
   				$s_date = $roi_list[$i]["s_date"];
   				$e_date = $roi_list[$i]["e_date"];
   				
   				$today = $s_date."~".$e_date;
   			}
   			
   			$m_0 = $roi_list[$i]["m_0"];
   			$m_1 = ($max_std >= 1) ? $m_0 + $roi_list[$i]["m_1"] : "-";
   			$m_2 = ($max_std >= 2) ? $m_1 + $roi_list[$i]["m_2"] : "-";
   			$m_3 = ($max_std >= 3) ? $m_2 + $roi_list[$i]["m_3"] : "-";
   			$m_4 = ($max_std >= 4) ? $m_3 + $roi_list[$i]["m_4"] : "-";
   			$m_5 = ($max_std >= 5) ? $m_4 + $roi_list[$i]["m_5"] : "-";
   			$m_6 = ($max_std >= 6) ? $m_5 + $roi_list[$i]["m_6"] : "-";
   			$m_7 = ($max_std >= 7) ? $m_6 + $roi_list[$i]["m_7"] : "-";
   			$m_8 = ($max_std >= 8) ? $m_7 + $roi_list[$i]["m_8"] : "-";
   			$m_9 = ($max_std >= 9) ? $m_8 + $roi_list[$i]["m_9"] : "-";
   			$m_10 = ($max_std >= 10) ? $m_9 + $roi_list[$i]["m_10"] : "-";
   			$m_11 = ($max_std >= 11) ? $m_10 + $roi_list[$i]["m_11"] : "-";
   			
   			$m_0_roi = (strcmp($m_0, "-") == false) ? "-" : round($m_0/$spend*100, 2);
   			$m_1_roi = (strcmp($m_1, "-") == false) ? "-" : round($m_1/$spend*100, 2);
   			$m_2_roi = (strcmp($m_2, "-") == false) ? "-" : round($m_2/$spend*100, 2);
   			$m_3_roi = (strcmp($m_3, "-") == false) ? "-" : round($m_3/$spend*100, 2);
   			$m_4_roi = (strcmp($m_4, "-") == false) ? "-" : round($m_4/$spend*100, 2);
   			$m_5_roi = (strcmp($m_5, "-") == false) ? "-" : round($m_5/$spend*100, 2);
   			$m_6_roi = (strcmp($m_6, "-") == false) ? "-" : round($m_6/$spend*100, 2);
   			$m_7_roi = (strcmp($m_7, "-") == false) ? "-" : round($m_7/$spend*100, 2);
   			$m_8_roi = (strcmp($m_8, "-") == false) ? "-" : round($m_8/$spend*100, 2);
   			$m_9_roi = (strcmp($m_9, "-") == false) ? "-" : round($m_9/$spend*100, 2);
   			$m_10_roi = (strcmp($m_10, "-") == false) ? "-" : round($m_10/$spend*100, 2);
   			$m_11_roi = (strcmp($m_11, "-") == false) ? "-" : round($m_11/$spend*100, 2);
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point"><?= $today ?></td>
				<td class="tdc">$<?= number_format($spend, 2) ?></td>
				<td class="tdc">$<?= number_format($cpi, 2) ?></td>
	        	<td class="tdc"><?= number_format($join_count) ?></td>
	            <td class="tdc"><?= number_format($nogame_count) ?> (<?= number_format($nogame_rate, 1) ?>%)</td>
	            <td class="tdc"><?= number_format($return_count) ?> (<?= number_format($return_rate, 1) ?>%)</td>

<?
	if($search_data == 0)
	{
?>
	            <td class="tdc"><?= (strcmp($m_0_roi, "-") == false) ? $m_0_roi : number_format($m_0_roi, 1)."%" ?></td>
	            <td class="tdc"><?= (strcmp($m_1_roi, "-") == false) ? $m_1_roi : number_format($m_1_roi, 1)."%" ?></td>
	            <td class="tdc"><?= (strcmp($m_2_roi, "-") == false) ? $m_2_roi : number_format($m_2_roi, 1)."%" ?></td>
	            <td class="tdc"><?= (strcmp($m_3_roi, "-") == false) ? $m_3_roi : number_format($m_3_roi, 1)."%" ?></td>
	            <td class="tdc"><?= (strcmp($m_4_roi, "-") == false) ? $m_4_roi : number_format($m_4_roi, 1)."%" ?></td>
	            <td class="tdc"><?= (strcmp($m_5_roi, "-") == false) ? $m_5_roi : number_format($m_5_roi, 1)."%" ?></td>
	            <td class="tdc"><?= (strcmp($m_6_roi, "-") == false) ? $m_6_roi : number_format($m_6_roi, 1)."%" ?></td>
	            <td class="tdc"><?= (strcmp($m_7_roi, "-") == false) ? $m_7_roi : number_format($m_7_roi, 1)."%" ?></td>
	            <td class="tdc"><?= (strcmp($m_8_roi, "-") == false) ? $m_8_roi : number_format($m_8_roi, 1)."%" ?></td>
	            <td class="tdc"><?= (strcmp($m_9_roi, "-") == false) ? $m_9_roi : number_format($m_9_roi, 1)."%" ?></td>
	            <td class="tdc"><?= (strcmp($m_10_roi, "-") == false) ? $m_10_roi : number_format($m_10_roi, 1)."%" ?></td>
	            <td class="tdc"><?= (strcmp($m_11_roi, "-") == false) ? $m_11_roi : number_format($m_11_roi, 1)."%" ?></td>
<?	            

			$total_roi = 0;
			
			if($max_std >= 11)
				$total_roi = $m_11_roi;
			else if($max_std >= 10)
				$total_roi = $m_10_roi;
			else if($max_std >= 9)
				$total_roi = $m_9_roi;
			else if($max_std >= 8)
				$total_roi = $m_8_roi;
			else if($max_std >= 7)
				$total_roi = $m_7_roi;
			else if($max_std >= 6)
				$total_roi = $m_6_roi;
			else if($max_std >= 5)
				$total_roi = $m_5_roi;
			else if($max_std >= 4)
				$total_roi = $m_4_roi;
			else if($max_std >= 3)
				$total_roi = $m_3_roi;
			else if($max_std >= 2)
				$total_roi = $m_2_roi;
			else if($max_std >= 1)
				$total_roi = $m_1_roi;
			else 
				$total_roi = $m_0_roi;
			
			$diff_roi = $total_roi - $goal_num;

			if($diff_roi == 0)
				$diff_roi_tag = "<span>-</span>";
			else if($diff_roi > 0)
				$diff_roi_tag = "<span style=\"color: red\">▲ ".number_format($diff_roi, 1)."%</span>";
			else if($diff_roi < 0)
				$diff_roi_tag = "<span style=\"color: blue\">▼ ".number_format($diff_roi, 1)."%</span>";
?>				
				<td class="tdc"><?= number_format($goal_num, 1)."%"?> (<?= $diff_roi_tag?>)</td>
<?
	}
	else 
	{
?>
				<td class="tdc"><?= ($m_0 == "-") ? $m_0 : $result_prefix.number_format($m_0) ?></td>
	            <td class="tdc"><?= ($m_1 == "-") ? $m_1 : $result_prefix.number_format($m_1) ?></td>
	            <td class="tdc"><?= ($m_2 == "-") ? $m_2 : $result_prefix.number_format($m_2) ?></td>
	            <td class="tdc"><?= ($m_3 == "-") ? $m_3 : $result_prefix.number_format($m_3) ?></td>
	            <td class="tdc"><?= ($m_4 == "-") ? $m_4 : $result_prefix.number_format($m_4) ?></td>
	            <td class="tdc"><?= ($m_5 == "-") ? $m_5 : $result_prefix.number_format($m_5) ?></td>
	            <td class="tdc"><?= ($m_6 == "-") ? $m_6 : $result_prefix.number_format($m_6) ?></td>
	            <td class="tdc"><?= ($m_7 == "-") ? $m_7 : $result_prefix.number_format($m_7) ?></td>
	            <td class="tdc"><?= ($m_8 == "-") ? $m_8 : $result_prefix.number_format($m_8) ?></td>
	            <td class="tdc"><?= ($m_9 == "-") ? $m_9 : $result_prefix.number_format($m_9) ?></td>
	            <td class="tdc"><?= ($m_10 == "-") ? $m_10 : $result_prefix.number_format($m_10) ?></td>
	            <td class="tdc"><?= ($m_11 == "-") ? $m_11 : $result_prefix.number_format($m_11) ?></td>
<?
	}
?>
	        </tr>
<?
   		}
   	}
?>
        </tbody>
	</table>
</div>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>