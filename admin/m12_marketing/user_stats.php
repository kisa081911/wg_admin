<?
	$top_menu = "marketing";
	$sub_menu = "user_stats";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
	$pagename = "user_stats.php";
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today,15,"d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;

	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT dau_rank, mau_rank FROM user_join_log WHERE today = '$today'";
	$rank_info = $db_analysis->getarray($sql);
	
	$dau_rank = $rank_info["dau_rank"];
	$mau_rank = $rank_info["mau_rank"];
	
	$tail = "WHERE 1=1  AND today >= '$startdate 00:00:00' AND today <= '$enddate 23:59:59'";
	
	$group_by = " GROUP BY LEFT(today,10) ";
	 
	$sql = "SELECT LEFT(today,10) AS today, totalcount, todayjoincount, todayactivecount, todayfacebookactivecount, todayiosactivecount, todayandroidactivecount, todayamazonactivecount,  todaypaycount, todaypackagecount, haspaycount, haspackagecount, week2count, ".
			"month1count, month3count, month6count ,month9count, daily_active_users, weekly_active_users, monthly_active_users, fan_likes, fan_talking ".
			"FROM user_join_log $tail $group_by ORDER BY today ASC";
	 
	$joinlog_list = $db_analysis->gettotallist($sql);
	
	$start_week = get_week($startdate);
	$end_week = get_week($enddate);
	$start_month = substr($startdate,0,7);
	$end_month = substr($enddate,0,7);
	$end_year = substr($enddate,0,4);
	
	//datelist,  회원수, 일일 회원가입수, 일일 Active 회원 수, 일일 결제 회원수 리스트, 일일 프리미엄 회원수 리스트, 앱 사용자 리스트, 팬 페이지 리스트
	$date_list = array();
	$total_list = array();
	$todayjoin_list = array();
	$todayactive_list = array();
	$todayfacebookactive_list = array();
	$todaymobileactive_list = array();
	$todayandroidactive_list = array();
	$todayamazonactive_list = array();
	$todaypay_list = array();
	$todaypackage_list = array();
	$haspay_list = array();
	$haspackagecount_list = array();
	$week2count_list = array();
	$month1count_list = array();
	$month3count_list = array();
	$month6count_list = array();
	$month9count_list = array();
	$daily_active_users = array();
	$weekly_active_users = array();
	$monthly_active_users = array();
	$fan_likes_list = array();
	$fan_talking_list = array();
	
	$list_pointer = sizeof($joinlog_list);
	
	$date_pointer = $enddate;
	 
	for ($i=0; $i<get_diff_date($enddate, $startdate, "d"); $i++)
	{
		if ($list_pointer > 0)
			$today = $joinlog_list[$list_pointer-1]["today"];
	
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$joinlog = $joinlog_list[$list_pointer-1];
			
			$date_list[$i] = $date_pointer;
			$total_list[$i] = $joinlog["totalcount"];
			$todayjoin_list[$i] = $joinlog["todayjoincount"];
			$todayactive_list[$i] = $joinlog["todayactivecount"];			
			$todayfacebookactive_list[$i] = $joinlog["todayfacebookactivecount"];
			$todaymobileactive_list[$i] = $joinlog["todaymobileactivecount"];
			$todayandroidactive_list[$i] = $joinlog["todayandroidactivecount"];
			$todayamazonactive_list[$i] = $joinlog["todayamazonactivecount"];
			$todaypay_list[$i] = $joinlog["todaypaycount"];		
			$todaypackage_list[$i] = $joinlog["todaypackagecount"];
			$haspay_list[$i] = $joinlog["haspaycount"];
			$haspackagecount_list[$i] = $joinlog["haspackagecount"];
			$week2_list[$i] = $joinlog["week2count"];
			$month1_list[$i] = $joinlog["month1count"];
			$month3_list[$i] = $joinlog["month3count"];
			$month6_list[$i] = $joinlog["month6count"];
			$month9_list[$i] = $joinlog["month9count"];
			$daily_active_users[$i] = $joinlog["daily_active_users"];
			$weekly_active_users[$i] = $joinlog["weekly_active_users"];
			$monthly_active_users[$i] = $joinlog["monthly_active_users"];
			$fan_likes_list[$i] = $joinlog["fan_likes"];
			$fan_talking_list[$i] = $joinlog["fan_talking"];
	                	
			$list_pointer--;
		}
		else
		{
			$date_list[$i] = $date_pointer;
			$total_list[$i] = 0;
			$todayjoin_list[$i] = 0;
			$todayactive_list[$i] = 0;	
			$todayfacebookactive_list[$i] = 0;
			$todaymobileactive_list[$i] = 0;
			$todayandroidactive_list[$i] = 0;
			$todayamazonactive_list[$i] = 0;
			$todaypay_list[$i] = 0;
			$todaypackage_list[$i] = 0;
			$haspay_list[$i] = 0;
			$haspackagecount_list[$i] = 0;
			$week2_list[$i] = 0;
			$month1_list[$i] = 0;
			$month3_list[$i] = 0;
			$month6_list[$i] = 0;
			$month9_list[$i] = 0;
			$daily_active_users[$i] = 0;
			$weekly_active_users[$i] = 0;
			$monthly_active_users[$i] = 0;
			$fan_likes_list[$i] = 0;
			$fan_talking_list[$i] = 0;
		}
	
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}

	$db_analysis->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
google.load("visualization", "1", {packages:["corechart"]});

function drawChart() 
{
	var dataTotaluser = new google.visualization.DataTable();
    
	dataTotaluser.addColumn('string', '날짜');
	dataTotaluser.addColumn('number', '누적회원수');
	dataTotaluser.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
    	$date = $date_list[$i-1];
    	$totalcount = $total_list[$i-1];

    	
        echo("['".$date."'");
        
        if ($totalcount != "")
            echo(",{v:".$totalcount.",f:'".make_price_format($totalcount)."'}]");
        else
            echo(",0]");
        
        if ($i > 1)
			echo(",");
    }
?>     
    ]);

	var dataDayuser = new google.visualization.DataTable();
    
	dataDayuser.addColumn('string', '날짜');
	dataDayuser.addColumn('number', '활동회원수');	
	dataDayuser.addColumn('number', 'Facebook활동회원수');
	dataDayuser.addColumn('number', 'IOS활동회원수');
	dataDayuser.addColumn('number', 'Android활동회원수');
	dataDayuser.addColumn('number', 'Amazon활동회원수');
	dataDayuser.addColumn('number', '재방문회원수');
	dataDayuser.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
    	$date = $date_list[$i-1];
    	$todayactivecount = $todayactive_list[$i-1];    	
    	$todayfacebookactivecount = $todayfacebookactive_list[$i-1];
    	$todaymobileactivecount = $todaymobileactive_list[$i-1];
    	$todayandroidactivecount = $todayandroidactive_list[$i-1];
    	$todayamazonactivecount = $todayamazonactive_list[$i-1];
    	$todayjoincount = $todayjoin_list[$i-1];
        
        echo("['".$date."'");
        
        if ($todayactivecount != "")
            echo(",{v:".$todayactivecount.",f:'".make_price_format($todayactivecount)."'}");
        else
            echo(",0");
        
        if ($todayfacebookactivecount != "")
        	echo(",{v:".$todayfacebookactivecount.",f:'".make_price_format($todayfacebookactivecount)."'}");
        else
        	echo(",0");
        
        if ($todaymobileactivecount != "")
        	echo(",{v:".$todaymobileactivecount.",f:'".make_price_format($todaymobileactivecount)."'}");
        else
        	echo(",0");
        
        if ($todayandroidactivecount != "")
        	echo(",{v:".$todayandroidactivecount.",f:'".make_price_format($todayandroidactivecount)."'}");
        else
        	echo(",0");
        
        if ($todayamazonactivecount != "")
        	echo(",{v:".$todayamazonactivecount.",f:'".make_price_format($todayamazonactivecount)."'}");
        else
        	echo(",0");
        	        
		$revisitcount = $todayactivecount - $todayjoincount;
		
        if ($revisitcount != "")
            echo(",{v:".$revisitcount.",f:'".make_price_format($revisitcount)."'}]");
        else
            echo(",0]");        
        
        if ($i > 1)
			echo(",");
    }
?>     
    ]);

	var dataRegActive = new google.visualization.DataTable();
    
	dataRegActive.addColumn('string', '날짜');       
	dataRegActive.addColumn('number', '가입회원수');
	dataRegActive.addColumn('number', '가입2주경과');
	dataRegActive.addColumn('number', '가입1달경과');
	dataRegActive.addColumn('number', '가입3달경과');
	dataRegActive.addColumn('number', '가입6달경과');
	dataRegActive.addColumn('number', '가입9달경과');
	dataRegActive.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
    	$date = $date_list[$i-1];    	
    	$todayjoincount = $todayjoin_list[$i-1];
        $week2count = $week2_list[$i-1];
        $month1count = $month1_list[$i-1];
        $month3count = $month3_list[$i-1];
        $month6count = $month6_list[$i-1];
        $month9count = $month9_list[$i-1];
        
        echo("['".$date."'");
                
        if ($todayjoincount != "")
            echo(",{v:".$todayjoincount.",f:'".make_price_format($todayjoincount)."'}");
        else
            echo(",0");
        
        if ($week2count != "")
            echo(",{v:".$week2count.",f:'".make_price_format($week2count)."'}");
        else
            echo(",0");
        
        if ($month1count != "")
            echo(",{v:".$month1count.",f:'".make_price_format($month1count)."'}");
        else
            echo(",0");
        
        if ($month3count != "")
            echo(",{v:".$month3count.",f:'".make_price_format($month3count)."'}");
        else
            echo(",0");
        
        if ($month6count != "")
            echo(",{v:".$month6count.",f:'".make_price_format($month6count)."'}");
        else
            echo(",0");
        
        if ($month9count != "")
            echo(",{v:".$month9count.",f:'".make_price_format($month9count)."'}]");
        else
            echo(",0]");
        
        if ($i > 1)
			echo(",");
    }
?>     
    ]);

	var dataDayPay = new google.visualization.DataTable();
    
	dataDayPay.addColumn('string', '날짜');
	dataDayPay.addColumn('number', '오늘결제회원수');
	dataDayPay.addColumn('number', '오늘패키지결제회원수');
	dataDayPay.addColumn('number', '결제경험회원수');
	dataDayPay.addColumn('number', '패키지결제경험회원수');
	dataDayPay.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
    	$date = $date_list[$i-1];    	
    	$todaypaycount = $todaypay_list[$i-1];
    	$todaypackagecount = $todaypackage_list[$i-1];
        $haspaycount = $haspay_list[$i-1];
        $haspackagecount = $haspackagecount_list[$i-1];
        
        echo("['".$date."'");
        
        if ($todaypaycount != "")
            echo(",{v:".$todaypaycount.",f:'".make_price_format($todaypaycount)."'}");
        else
            echo(",0");
        
        if ($todaypackagecount != "")
        	echo(",{v:".$todaypackagecount.",f:'".make_price_format($todaypackagecount)."'}");
        else
        	echo(",0");
        
        if ($haspaycount != "")
            echo(",{v:".$haspaycount.",f:'".make_price_format($haspaycount)."'}");
        else
            echo(",0");
        
        if ($haspackagecount != "")
        	echo(",{v:".$haspackagecount.",f:'".make_price_format($haspackagecount)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
			echo(",");
    }
?>     
    ]);

	var dataAppUser = new google.visualization.DataTable();
    
	dataAppUser.addColumn('string', '날짜');
	dataAppUser.addColumn('number', '일일 활동회원수');
	dataAppUser.addColumn('number', '주간 활동회원수');
	dataAppUser.addColumn('number', '월간 활동회원수');
	dataAppUser.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $date = $date_list[$i-1];
        $daily_users = $daily_active_users[$i-1];
        $weekly_users = $weekly_active_users[$i-1];
        $monthly_users = $monthly_active_users[$i-1];
        
        echo("['".$date."'");
        
        if ($daily_users != "")
            echo(",{v:".$daily_users.",f:'".make_price_format($daily_users)."'}");
        else
            echo(",0");
        
        if ($weekly_users != "")
            echo(",{v:".$weekly_users.",f:'".make_price_format($weekly_users)."'}");
        else
            echo(",0");
        
        if ($monthly_users != "")
            echo(",{v:".$monthly_users.",f:'".make_price_format($monthly_users)."'}]");
        else
            echo(",0]");
        
        if ($i > 1)
            echo(",");
    }
?>     
    ]);

	var dataDayUserPayRate = new google.visualization.DataTable();
    
	dataDayUserPayRate.addColumn('string', '날짜');
	dataDayUserPayRate.addColumn('number', '일일사용자 결제율(%)');
	dataDayUserPayRate.addColumn('number', '일일사용자 패키지 결제율(%)');
	dataDayUserPayRate.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $date = $date_list[$i-1];
        $todayactivecount = $todayactive_list[$i-1];
        $todaypaycount = $todaypay_list[$i-1];
        $todaypackagecount = $todaypackage_list[$i-1];
 
        echo("['".$date."'");
        
        if ($todaypaycount != "")
            echo(",{v:".get_percentage($todaypaycount, $todayactivecount).",f:'".get_percentage($todaypaycount, $todayactivecount)."'}");
        else
            echo(",0");
        
        if ($todaypackagecount != "")
        	echo(",{v:".get_percentage($todaypackagecount, $todayactivecount).",f:'".get_percentage($todaypackagecount, $todayactivecount)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
            echo(",");
    }
?>     
    ]);

	var dataDayFanPage = new google.visualization.DataTable();

	dataDayFanPage.addColumn('string', '날짜');
	dataDayFanPage.addColumn('number', 'Like 수');
	dataDayFanPage.addColumn('number', 'Talking 수');

	dataDayFanPage.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $date = $date_list[$i-1];
        $fan_likes = $fan_likes_list[$i-1];
        $fan_talking = $fan_talking_list[$i-1];
        
        echo("['".$date."'");
        
        if ($fan_likes != "")
            echo(",{v:".$fan_likes.",f:'".make_price_format($fan_likes)."'}");
        else
            echo(",0");
        
        if ($fan_talking != "")
            echo(",{v:".$fan_talking.",f:'".make_price_format($fan_talking)."'}]");
        else
            echo(",0]");
        
        if ($i > 1)
            echo(",");
    }
?>     
    ]);

	var options = {
            title:'',                                                      
            width:1050,                         
            height:200,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:80,top:40,width:1020,height:130}
    };

	var options_user = {
            title:'',                                                      
            width:1050,                         
            height:200,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            colors: ['#0000CD', '#008000', '#D2691E', '#C0C0C0', '#FFD700'],
            chartArea:{left:80,top:40,width:1020,height:130}
    };

    var chart = new google.visualization.LineChart(document.getElementById('chart_divTotalUser'));
    chart.draw(dataTotaluser, options_user);

    chart = new google.visualization.LineChart(document.getElementById('chart_divDayUser'));
    chart.draw(dataDayuser, options);

    chart = new google.visualization.LineChart(document.getElementById('chart_divRegActive'));
    chart.draw(dataRegActive, options);

    chart = new google.visualization.LineChart(document.getElementById('chart_divDayPay'));
    chart.draw(dataDayPay, options);

    chart = new google.visualization.LineChart(document.getElementById('chart_divAppUser'));
    chart.draw(dataAppUser, options);

    chart = new google.visualization.LineChart(document.getElementById('chart_divDayUserPayRate'));
    chart.draw(dataDayUserPayRate, options);

    chart = new google.visualization.LineChart(document.getElementById('chart_divFanPage'));
    chart.draw(dataDayFanPage, options);
}

google.setOnLoadCallback(drawChart);

function search_press(e)
{
    if (((e.which) ? e.which : e.keyCode) == 13)
    {
        search();
    }
}

function search()
{
    var search_form = document.search_form;

    search_form.submit();
}

$(function() {
    $("#startdate").datepicker({ });
});

$(function() {
    $("#enddate").datepicker({ });
});
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 사용자 통계</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다<span style="padding-left:530px;"></span></span>FB DAU Rank: <span style="color:#5ab2da;"><?= make_price_format($dau_rank)?></span>위 &nbsp;&nbsp;&nbsp; FB MAU Rank: <span style="color:#5ab2da;"><?= make_price_format($mau_rank)?></span>위
	</div>
	
	<div class="h2_title">[누적 회원수]</div>
	<div id="chart_divTotalUser" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title">[일일 회원수]</div>
	<div id="chart_divDayUser" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title">[가입 기간별 활동 회원수]</div>
	<div id="chart_divRegActive" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title">[일일 결제 관련 회원수]</div>
	<div id="chart_divDayPay" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title">[앱 사용자]</div>
	<div id="chart_divAppUser" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title">[일일 사용자 결제율]</div>
	<div id="chart_divDayUserPayRate" style="height:230px; min-width: 500px"></div>	    
	
	<div class="h2_title">[팬 페이지]</div>
	<div id="chart_divFanPage" style="height:230px; min-width: 500px"></div>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>