<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	check_login();
	
	$start_searchdate = $_GET["start_searchdate"];
	$end_searchdate = $_GET["end_searchdate"];
	$start_orderdate = $_GET["start_orderdate"];
	$end_orderdate = $_GET["end_orderdate"];
	$ids = $_GET["ids"];
	
	$filename = "fb_retention_campaign_".$start_searchdate."_".$end_searchdate.".xls";
		
	$db_main2 = new CDatabase_Main2();
	
	$ad_stats_table = "tbl_ad_retention_stats";
	$order_table = "tbl_user_marketing_retention_order";
	$user_marketing_table = "tbl_user_marketing_retention";
	
	$search_sql = "";
	
	if(($start_orderdate != "" && $end_orderdate != ""))
	{
		$add_sql = " WHERE sub2.writedate BETWEEN '$start_orderdate 00:00:00' AND '$end_orderdate 23:59:59' ";
	}
		
	$sql = "SELECT campaign_name, adset_name, ad_name, t1.ad_id, t1.today, spend, reach, impressions, clicks, IFNULL(money, 0) AS money, IFNULL(buy_user_cnt, 0) AS buy_user_cnt, IFNULL(buy_cnt, 0) AS buy_cnt ".
			"FROM ( ".
			"	SELECT today, ad_id, (SELECT ad_name FROM $ad_stats_table  WHERE ad_id = a.ad_id  ORDER BY today DESC  LIMIT 1) AS ad_name, ".  
			"		(SELECT adset_name FROM $ad_stats_table  WHERE adset_id = a.adset_id  ORDER BY today DESC  LIMIT 1) AS adset_name, adset_id, ".  
			"		(SELECT campaign_name FROM $ad_stats_table  WHERE campaign_id = a.campaign_id  ORDER BY today DESC  LIMIT 1) AS campaign_name, campaign_id, ".  
			"		SUM(spend) AS spend, SUM(impressions) AS impressions, MAX(ad_reach) AS reach, SUM(clicks) AS clicks, MAX(updated_time) AS updated_time ".
			"	FROM $ad_stats_table a ". 
			"	WHERE today BETWEEN '$start_searchdate' AND '$end_searchdate'  AND campaign_id = $ids ".   
			"	GROUP BY today, ad_id ". 
			") t1 LEFT JOIN ( ". 
			"	SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, sub1.ad_id, IFNULL(SUM(money), 0) AS money, COUNT(DISTINCT useridx) AS buy_user_cnt, COUNT(useridx) AS buy_cnt ".
			"	FROM ( ".
			"		SELECT ad_id ". 
			"		FROM $ad_stats_table ". 
			"		WHERE today BETWEEN '$start_searchdate' AND '$end_searchdate' AND campaign_id = $ids ". 
			"		GROUP BY ad_id ". 
			"	) sub1 JOIN $order_table sub2 ON sub1.ad_id = sub2.fb_rt_id ".
			$add_sql.
			"	GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), ad_id ". 
			") t2 ON t1.ad_id = t2.ad_id AND t1.today = t2.today ".
			"ORDER BY today ASC, adset_name ASC, ad_name ASC ";
	$contents_list = $db_main2->gettotallist($sql);
	
	if (sizeof($contents_list) == 0)
		error_go("저장할 데이터가 없습니다.", "fb_retention_adsets_stats.php?start_searchdate=$start_searchdate&end_searchdate=$end_searchdate&start_orderdate=$start_orderdate&end_orderdate=$end_orderdate&ids=$ids");
	
	$sql = "SELECT t1.today, IFNULL(return_count, 0) as return_count ".
			"FROM ( ".
			"	SELECT today, adset_name, ad_id, ad_name ".
			"	FROM $ad_stats_table a ". 
			"	WHERE today BETWEEN '$start_searchdate' AND '$end_searchdate' AND campaign_id = $ids ". 
			"	GROUP BY today, ad_id ". 
			") t1 LEFT JOIN ( ". 
			"	SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, ad_id, COUNT(*) AS return_count ". 
			"	FROM ( ". 
			"		SELECT ad_id FROM $ad_stats_table WHERE today BETWEEN '$start_searchdate' AND '$end_searchdate' AND campaign_id = $ids GROUP BY ad_id ". 
			"	) sub1 JOIN $user_marketing_table sub2 ON sub1.ad_id = sub2.fb_rt_id ". 
			"	GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'),ad_id ". 
			") t2 ON t1.ad_id = t2.ad_id AND t1.today = t2.today ".
			"ORDER BY today ASC, adset_name ASC, ad_name ASC ";
	$return_count_list = $db_main2->gettotallist($sql);
	
	$db_main2->end();
	
	
	
	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
	"<table border=1>".
		"<tr>".
			"<td style='font-weight:bold;'>Campaign</td>".
			"<td style='font-weight:bold;'>Adset</td>".
			"<td style='font-weight:bold;'>Ads</td>".
			"<td style='font-weight:bold;'>Ad ID</td>".
			"<td style='font-weight:bold;'>날짜</td>".
			"<td style='font-weight:bold;'>복귀회원</td>".
			"<td style='font-weight:bold;'>비용</td>".
			"<td style='font-weight:bold;'>CPI</td>".
			"<td style='font-weight:bold;'>Impression</td>".
			"<td style='font-weight:bold;'>Reach</td>".
			"<td style='font-weight:bold;'>Clicks</td>".
			"<td style='font-weight:bold;'>Frequency</td>".
			"<td style='font-weight:bold;'>CTR</td>".
			"<td style='font-weight:bold;'>CVR</td>".
			"<td style='font-weight:bold;'>매출</td>".
			"<td style='font-weight:bold;'>ROI</td>".
			"<td style='font-weight:bold;'>결제자</td>".
			"<td style='font-weight:bold;'>결제비율</td>".
			"<td style='font-weight:bold;'>구매횟수</td>".
			"<td style='font-weight:bold;'>평균구매횟수</td>".
			"<td style='font-weight:bold;'>결제당 평균 구매액</td>".
			"<td style='font-weight:bold;'>ARPU</td>".
			"<td style='font-weight:bold;'>ARPPU</td>".
		"</tr>";
	
		for ($i=0; $i < sizeof($contents_list); $i++)
		{
			$campaign_name = $contents_list[$i]["campaign_name"];
			$adset_name = $contents_list[$i]["adset_name"];
			$ad_name = $contents_list[$i]["ad_name"];
			$ad_id = $contents_list[$i]["ad_id"];
			$today = $contents_list[$i]["today"];
			$return_count = $return_count_list[$i]["return_count"];
			$spend = $contents_list[$i]["spend"];
			$cpi = ($return_count == 0) ? 0 : number_format($spend / $return_count, 2); 
			$impressions = $contents_list[$i]["impressions"];
			$reach = $contents_list[$i]["reach"];
			$clicks = $contents_list[$i]["clicks"];
			$frequency = ($reach == 0) ? 0 : number_format($impressions / $reach, 2);
			$ctr = ($impressions == 0) ? 0 : number_format(($clicks / $impressions) * 100, 1);
			$cvr = ($clicks == 0) ? 0 : number_format(($return_count / $clicks) * 100, 1);
			$money = number_format($contents_list[$i]["money"]);
			$roi = ($spend == 0) ? 0 : number_format($money / $spend, 1);
			$buy_user_cnt = $contents_list[$i]["buy_user_cnt"]; 									// 결제자
			$buy_rate = ($return_count == 0) ? 0 : number_format(($buy_user_cnt / $return_count) * 100, 1);	// 결제비율
			$buy_cnt = $contents_list[$i]["buy_cnt"];												// 구매횟수
			$buy_cnt_avg = ($buy_user_cnt == 0) ? 0 : number_format($buy_cnt / $buy_user_cnt, 1);	// 평균구매횟수
			$money_avg = ($buy_cnt == 0) ? 0 : number_format($money / $buy_cnt, 1);					// 평균구매액
			$arpu = ($return_count == 0) ? 0 : number_format($money / $return_count, 1);
			$arppu = ($buy_user_cnt == 0) ? 0 : number_format($money / $buy_user_cnt, 1);
						
			$excel_contents .= "<tr>".
					"<td>".$campaign_name."</td>".
					"<td>".$adset_name."</td>".
					"<td>".$ad_name."</td>".
					"<td style='mso-number-format:\@'>".$ad_id."</td>".
					"<td>".$today."</td>".
					"<td>".$return_count."</td>".
					"<td>$ ".$spend."</td>".
					"<td>$ ".$cpi."</td>".
					"<td>".$impressions."</td>".
					"<td>".$reach."</td>".
					"<td>".$clicks."</td>".
					"<td>".$frequency."</td>".
					"<td>".$ctr."%</td>".
					"<td>".$cvr."%</td>".
					"<td>$ ".$money."</td>".
					"<td>".$roi."%</td>".
					"<td>".$buy_user_cnt."</td>".
					"<td>".$buy_rate."%</td>".
					"<td>".$buy_cnt."</td>".
					"<td>".$buy_cnt_avg."</td>".
					"<td>$ ".$money_avg."</td>".
					"<td>$ ".$arpu."</td>".
					"<td>$ ".$arppu."</td>".
					"</tr>";
		}
						  
		$excel_contents .= "</table>";
  
		Header("Content-type: application/x-msdownload");
		Header("Content-type: application/x-msexcel");
		Header("Content-Disposition: attachment; filename=$filename");
  
		echo($excel_contents);
?>