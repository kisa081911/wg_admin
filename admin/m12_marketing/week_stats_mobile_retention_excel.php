<?
	include_once("../common/dbconnect/db_util_redshift.inc.php");
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	check_login();
	
	$search_startdate = $_GET["sdate"];
	$search_enddate = $_GET["edate"];
	$pay_startdate = $_GET["pay_sdate"];
	$pay_enddate = $_GET["pay_edate"];
	$search_platform = isset($_GET["search_platform"]) ? $_GET["search_platform"] : "0";
	$search_agency = $_GET["agency"];
	
	$filename = "week_stats_mobile_retention_".$search_startdate."_".$search_enddate.".xls";
		
	$search_sql = "";
	$agency_sql = "";
	
	if($search_platform == 0)
		$platform_sql = "platform > $search_platform";
	else
		$platform_sql = "platform = $search_platform";
	
	if($search_agency != "")
		$agency_sql = " AND adflag LIKE '$search_agency%' ";
	
	$db_redshift = new CDatabase_Redshift();
	
	$sql = "SELECT DISTINCT adflag AS agencyname ".
			"FROM t5_user_adflag_mobile_log ".
			"WHERE $platform_sql AND '$search_startdate 00:00:00' <= writedate AND writedate <= '$search_enddate 23:59:59' ".
			"	AND adflag LIKE 'm_retention%' ".
			"ORDER BY LOWER(adflag) ASC";
	$agency_list = $db_redshift->gettotallist($sql);

	$sql = "SELECT t1.week_count, start_date, end_date, total_cnt, new_cnt, (total_cnt - new_cnt) AS rejoin_cnt, spend, nvl(payer_cnt, 0) AS payer_cnt, nvl(pay_amount, 0) AS pay_amount, nvl(pay_cnt, 0) AS pay_cnt ".
			"FROM ( ".
  			"	SELECT date_part(w, writedate) AS week_count, date(MIN(writedate)) AS start_date, date(MAX(writedate)) AS end_date, COUNT(DISTINCT useridx) AS total_cnt, SUM(case when isnew = 1 then 1 end) AS new_cnt, SUM(spend) AS spend ".
  			"	FROM t5_user_adflag_mobile_log ".
  			"	WHERE '$search_startdate 00:00:00' <= writedate AND writedate <= '$search_enddate 23:59:59' $agency_sql ".
    		"		and $platform_sql AND adflag LIKE 'm_retention%' ".
  			"	GROUP BY date_part(w, writedate) ".
			") t1 LEFT JOIN ( ".
  			"	SELECT date_part(w, t1.writedate) AS week_count, COUNT(DISTINCT t2.useridx) AS payer_cnt, ROUND(SUM(money), 2) AS pay_amount, COUNT(orderidx) AS pay_cnt ".
  			"	FROM ( ".
      		"		SELECT useridx, platform, adflag, isnew, writedate, nvl(enddate, CURRENT_DATE) as enddate ".
      		"		FROM t5_user_adflag_mobile_log ".
       		"		WHERE '$search_startdate 00:00:00' <= writedate AND writedate <= '$search_enddate 23:59:59' $agency_sql ".
			"			AND $platform_sql AND adflag LIKE 'm_retention%' ".
  			"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx and t1.writedate <= t2.writedate and '$pay_enddate 23:59:59' > t2.writedate ".
  			"	WHERE status = 1 AND '$pay_startdate 00:00:00' <= t2.writedate AND t2.writedate <= '$pay_enddate 23:59:59' ".
  			"	GROUP BY date_part(w, t1.writedate) ".
			") t2 ON t1.week_count = t2.week_count ".
			"ORDER BY t1.week_count ASC";
	$week_list = $db_redshift->gettotallist($sql);

	$db_redshift->end();
	
	if (sizeof($week_list) == 0)
		error_go("저장할 데이터가 없습니다.", "week_stats_mobile_retention.php");
	
	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
	"<table border=1>".
		"<tr>".
			"<td style='font-weight:bold;'>구간</td>".
			"<td style='font-weight:bold;'>가입수</td>".
			"<td style='font-weight:bold;'>CPI</td>".
			"<td style='font-weight:bold;'>비용</td>".
			"<td style='font-weight:bold;'>총결제</td>".
			"<td style='font-weight:bold;'>ROI</td>".
			"<td style='font-weight:bold;'>결제자수</td>".
			"<td style='font-weight:bold;'>결제자 비율</td>".
			"<td style='font-weight:bold;'>결제건수</td>".
			"<td style='font-weight:bold;'>ARPU</td>".
			"<td style='font-weight:bold;'>ARPPU</td>".
		"</tr>";
	
		$total_install = 0;
		$total_spend = 0;
		$total_cpi = 0;
		$total_pay_amount = 0;
		$total_roi = 0;
		$total_payer_count = 0;
		$total_pay_count = 0;
		$total_payer_rate = 0;
		$total_arpu = 0;
		$total_arppu = 0;
	
		for($i=0; $i<sizeof($week_list); $i++)
		{
			$spend = 0;
			$iap_amount = 0;
			$week_count = $week_list[$i]["week_count"];
			$start_date = $week_list[$i]["start_date"];
			$end_date = $week_list[$i]["end_date"];
			$total_cnt = $week_list[$i]["total_cnt"];
			$spend = $week_list[$i]["spend"];
			$cpi = ($total_cnt == 0) ? 0 : round($spend/$total_cnt, 2);
			$pay_amount = $week_list[$i]["pay_amount"];
			$roi = ($spend == 0) ? 0 : round($pay_amount/$spend*100, 2);
			$payer_cnt = $week_list[$i]["payer_cnt"];
			$payer_rate = ($total_cnt == 0) ? 0 : round($payer_cnt/$total_cnt*100, 2);
			$pay_count = $week_list[$i]["pay_cnt"];
			$arpu = ($total_cnt == 0) ? 0 : round($pay_amount/$total_cnt, 2);
			$arppu = ($payer_cnt == 0) ? 0 : round($pay_amount/$payer_cnt, 2);
			
			$total_install += $total_cnt;
			$total_spend += $spend;
			$total_pay_amount += $pay_amount;
			$total_payer_count += $payer_cnt;
			$total_pay_count += $pay_count;
  			
  			$excel_contents .= "<tr>".
    			"<td>구간 ".$week_count." - $start_date ~ $end_date</td>".
		  		"<td>".number_format($total_cnt)."</td>".
		  		"<td>$".number_format($cpi, 2)."</td>".
		  		"<td>$".number_format($spend, 2)."</td>".
		  		"<td>$".number_format($pay_amount, 2)."</td>".
		  		"<td>".number_format($roi, 2)."%</td>".
		  		"<td>".number_format($payer_cnt)."</td>".
		  		"<td>".number_format($payer_rate, 2)."%</td>".
		  		"<td>".number_format($pay_count)."</td>".
		  		"<td>".number_format($arpu, 2)."</td>".
		  		"<td>".number_format($arppu, 2)."</td>".
	  		"</tr>";
  		}
  		
  		$total_cpi = ($total_install == 0) ? 0 : round($total_spend/$total_install, 2);
		$total_payer_rate = ($total_install == 0) ? 0 : round($total_payer_count/$total_install*100, 2);
		$total_arpu = ($total_install == 0) ? 0 : round($total_pay_amount/$total_install, 2);
		$total_arppu = ($total_payer_count == 0) ? 0 : round($total_pay_amount/$total_payer_count, 2);
		
		$excel_contents .= "<tr>".
				"<td>합계</td>".
				"<td>".number_format($total_install)."</td>".
				"<td>$".number_format($total_cpi, 2)."</td>".
				"<td>$".number_format($total_spend, 2)."</td>".
				"<td>$".number_format($total_pay_amount, 2)."</td>".
				"<td>".number_format($total_roi, 2)."%</td>".
				"<td>".number_format($total_payer_count)."</td>".
				"<td>".number_format($total_payer_rate, 2)."%</td>".
				"<td>".number_format($total_pay_count)."</td>".
				"<td>".number_format($total_arpu, 2)."</td>".
				"<td>".number_format($total_arppu, 2)."</td>".
				"</tr>";
						  
		$excel_contents .= "</table>";
  
		Header("Content-type: application/x-msdownload");
		Header("Content-type: application/x-msexcel");
		Header("Content-Disposition: attachment; filename=$filename");
  
		echo($excel_contents);
?>