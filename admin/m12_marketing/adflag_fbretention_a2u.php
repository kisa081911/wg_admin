<?
    $top_menu = "marketing";
    $sub_menu = "adflag_fbretention_a2u";
    include("../common/dbconnect/db_util_redshift.inc.php");
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    $search_payday = ($_GET["payday"]=="")? 28 : $_GET["payday"];
    $isearch = $_GET["issearch"];
    $adflag = ($_GET["search_tab"] == "") ? "0" : $_GET["search_tab"];
    
    if ($isearch == "")
    {
        $search_end_createdate  = date("Y-m-d", time());
        $search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 9);
    }
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
	
	function search()
	{
	    var search_form = document.search_form;
	
	    search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="adflag_fbretention_a2u.php">
		<input type=hidden name=issearch value="1">
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; A2U 복귀 분석</div>
                <div class="search_box">
                	<select name="search_tab" id="search_tab">
                		<option value="0" <?= ($adflag=="0") ? "selected" : "" ?>>전체</option>
                		<option value="gamenotifygroup1" <?= ($adflag=="gamenotifygroup1") ? "selected" : "" ?>>[신규]가입 후 1 주일간 매일, 20만코인 - Group 1</option>
                		<option value="gamenotifygroup2" <?= ($adflag=="gamenotifygroup2") ? "selected" : "" ?>>[비결제자]3일~7일 미만 이탈자,10만코인- Group 2</option>
                		<option value="gamenotifygroup3" <?= ($adflag=="gamenotifygroup3") ? "selected" : "" ?>>[전체]2주이상 이탈자, Coin 200만 미만- Group 3</option>
                		<option value="gamenotifygroup4" <?= ($adflag=="gamenotifygroup4") ? "selected" : "" ?>>[전체]2주이상 이탈자, Coin 200만 초과 1000만 이하 - Group 4</option>
                		<option value="gamenotifygroup5" <?= ($adflag=="gamenotifygroup5") ? "selected" : "" ?>>[전체]2주이상 이탈자, Coin 1000만 초과 1억 이하 - Group 5</option>
                		<option value="gamenotifygroup6" <?= ($adflag=="gamenotifygroup6") ? "selected" : "" ?>>[전체]2주이상 이탈자, Coin 1억 초과 10억 이하 - Group 6</option>
                		<option value="gamenotifygroup7" <?= ($adflag=="gamenotifygroup7") ? "selected" : "" ?>>[전체]2주이상 이탈자, Coin 10억 초과 - Group 7</option>
                		<option value="gamenotifygroup12" <?= ($adflag=="gamenotifygroup12") ? "selected" : "" ?>>[비결제자]1~2주미만 이탈자, 10만코인 - Group 12</option>
                		<option value="gamenotifygroup8" <?= ($adflag=="gamenotifygroup8") ? "selected" : "" ?>>[결제경험자]2~7일미만 이탈자, 피에스타 12시간 전부터 발송 - Group 8</option>
                		<option value="gamenotifygroup11" <?= ($adflag=="gamenotifygroup11") ? "selected" : "" ?>>[결제경험자]1~2주미만 이탈자- Group 11</option>
					</select>
					<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
                    <input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
                    &nbsp;&nbsp;&nbsp;&nbsp;결제액 기준 : 복귀 후 <input type="text" class="search_text" id="payday" name="payday" style="width:60px" value="<?= $search_payday ?>" onkeypress="search_press(event); return checknum();" />일 이내 결제
                     <input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
                </div>
            </div>
            <!-- //title_warp -->
            
            <div class="search_result" style="margin-top: 50px;">
                <span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
            </div>
            
            <div id="tab_content_1">
            	<table class="tbl_list_basic1">
		            <colgroup>
		                <col width="">
		                <col width="">
		                <col width="">
		                <col width="">
		                <col width="">
		                <col width="">
		                <col width="">
		                <col width=""> 
		            </colgroup>
            		<thead>
			            <tr>
			                <th rowspan="2">유입일</th>
			                <th rowspan="2"class="tdr">노티발송수</th>
			                <th rowspan="2"class="tdr">총 복귀회원수</th>
			                <th colspan="3"class="tdc">복귀일 별 회원수</th>
			                <th rowspan="2" class="tdr">평균복귀일</th>
			                <th rowspan="2"class="tdr">결제회원수</th>
			                <th rowspan="2"class="tdr">결제 금액</th>
			                <th rowspan="2"class="tdr">결제 수</th>
			                <th rowspan="2"class="tdr">평균결제 금액</br>(복귀일 비중)</th>
			            </tr>
				    <tr>
			         
			                <th class="tdc">7일 미만 </th>
			                <th class="tdc">7일~28일미만 </th>
			                <th class="tdc">28일이상 </th>
			               
			            </tr>
            		</thead>
            		<tbody>
<?
    $db_redshift = new CDatabase_Redshift();

	$adflag_tail ="and adflag LIKE 'gamenotify%'";
	
	if($adflag != "0")
	{
	    $adflag_tail ="and adflag = '$adflag'";
	}
	
    $sendcount_tail = "left join  t5_notification_group_log_new t3 on date(senddate = date(t1.writedate)";
	    
	if ($adflag != "0")
        $sendcount_tail = "left join  t5_notification_group_log_new t3 on date(senddate) = date(t1.writedate) AND  group_no =  REPLACE('$adflag', 'gamenotifygroup', '') ";
	
	$sql=" select date(t1.writedate) as today, count(t1.useridx) as usercnt, SUM(leaveuser0_6) as leaveuser0_6, SUM(leaveuser7_27) as leaveuser7_27, SUM(leaveuser28) as leaveuser28
                  , round(avg(leavedays)) as avg_leavedays, count(distinct t2.useridx) payer_cnt, SUM(case when money is not null then 1 else 0 end) as pay_cnt, round(SUM(money), 2) as total_money, round(SUM(money::float*multi_value::float), 2) as rate_money
            from (
            		  SELECT useridx, adflag, (case when leavedays < 7 then 1 else 0 end) as leaveuser0_6, (case when leavedays >= 7 and leavedays < 27 then 1 else 0 end) as leaveuser7_27
            				, (case when leavedays >= 28 then 1 else 0 end) as leaveuser28
            				, leavedays, writedate, (case when (leavedays+20) >= 540 then 1 else round((leavedays+20)::float/540::float, 2) end) as multi_value
            		  FROM t5_user_retention_log
            		  WHERE useridx > 20000 $adflag_tail and writedate >= '$search_start_createdate 00:00:00' and writedate <= '$search_end_createdate 23:59:59'
            ) t1 left join t5_product_order_all t2 on t1.useridx = t2.useridx and t2.status = 1 and t1.writedate < t2.writedate and datediff('day', t1.writedate, t2.writedate) < $search_payday
            group by date(t1.writedate)
            order by 1 desc";
	$list = $db_redshift->gettotallist($sql);
	
	for ($i=0; $i<sizeof($list); $i++)
	{
        $today = $list[$i]["today"];
        $usercnt = $list[$i]["usercnt"];
        $avg_leavedays = $list[$i]["avg_leavedays"];
        $payer_cnt = $list[$i]["payer_cnt"];
        $total_money = $list[$i]["total_money"];
        $rate_money = $list[$i]["rate_money"];
        $pay_cnt = $list[$i]["pay_cnt"];
    	
        if($adflag == "0")
            $notisendcount = $db_redshift->getvalue("SELECT COUNT(*) FROM t5_notification_group_log_new WHERE date(senddate)= '$today'");
        else if ($adflag != "0")
            $notisendcount = $db_redshift->getvalue("SELECT COUNT(*) FROM t5_notification_group_log_new WHERE group_no =  REPLACE('$adflag', 'gamenotifygroup', '')  AND  date(senddate) = '$today'");
	    
	    $leaveuser0_6 = $list[$i]["leaveuser0_6"];
        $leaveuser7_27 = $list[$i]["leaveuser7_27"];
        $leaveuser28 = $list[$i]["leaveuser28"];
?>
		<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="tdc point_title" valign="center"><?= $today ?></td>
            <td class="tdr point"><?= number_format($notisendcount) ?></td>
            <td class="tdr point"><?= number_format($usercnt) ?></td>
            <td class="tdr point"><?= number_format($leaveuser0_6) ?></td>
            <td class="tdr point"><?= number_format($leaveuser7_27) ?></td>
            <td class="tdr point"><?= number_format($leaveuser28) ?></td>
            <td class="tdr point"><?= number_format($avg_leavedays) ?></td>
            <td class="tdr point"><?= number_format($payer_cnt)?></td>
            <td class="tdr point">$<?= number_format($total_money,2) ?></td>
	    	<td class="tdr point"><?= number_format($pay_cnt) ?></td>
            <td class="tdr point">$<?= number_format($rate_money,2) ?></td>
        </tr>
<?
	}
	$db_redshift->end();
?>    
            	</tbody>
			</table>
		</div>   
	</form>
</div>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
