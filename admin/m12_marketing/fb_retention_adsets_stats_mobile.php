<?
	$top_menu = "marketing";
	$sub_menu = "fb_retention_campaign_mobile";
		
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_start_searchdate = $_GET["start_searchdate"];
	$search_end_searchdate = $_GET["end_searchdate"];
	$search_start_orderdate = $_GET["start_orderdate"];
	$search_end_orderdate = $_GET["end_orderdate"];

	$ids = $_GET["ids"];
	
	$today = date("Y-m-d");
	
	if($search_start_searchdate == "")
		$search_start_searchdate = date("Y-m-d", strtotime("-15 day"));
	
	if($search_end_searchdate == "")
		$search_end_searchdate = $today;
	
	$ad_stats_table = "tbl_ad_retention_stats_mobile";
	
	$db_main2 = new CDatabase_Main2();
	
	if(($search_start_orderdate != "" && $search_end_orderdate != ""))
	{
		$add_sql = " WHERE sub2.writedate BETWEEN '$search_start_orderdate 00:00:00' AND '$search_end_orderdate 23:59:59' ";
	}
	
	$sql = "SELECT t1.adset_id, adset_name, campaign_name, campaign_id, clicks, reach, cpc, spend, STATUS, daily_budget, IFNULL(money, 0) AS money, IFNULL(buy_user_cnt, 0) AS buy_user_cnt ".
			"FROM ( ".
			"	SELECT adset_id, (SELECT adset_name FROM $ad_stats_table  WHERE adset_id = a.adset_id  ORDER BY today DESC LIMIT 1) AS adset_name, ".			
			"		(SELECT campaign_name FROM $ad_stats_table  WHERE campaign_id = a.campaign_id  ORDER BY today DESC LIMIT 1) AS campaign_name, campaign_id, ". 
			"		(SELECT COUNT(useridx) FROM tbl_user_marketing_retention WHERE fb_rt_id = a.ad_id AND createdate BETWEEN '$search_start_searchdate' AND '$search_end_searchdate') AS join_count, ".
			"		SUM(clicks) AS clicks, MAX(adset_reach) AS reach, cpc, SUM(spend) AS spend, STATUS, MAX(daily_budget) AS daily_budget, updated_time ".  
			"	FROM $ad_stats_table a ".
			"	WHERE today BETWEEN '$search_start_searchdate' AND '$search_end_searchdate' AND campaign_id = $ids ".  
			"	GROUP BY adset_id ".  
			") t1 LEFT JOIN ( ".
			"	SELECT adset_id, IFNULL(SUM(money/10), 0) AS money, COUNT(DISTINCT useridx) AS buy_user_cnt ".
			"	FROM ( ".
			"		SELECT adset_id, ad_id ".
			"		FROM $ad_stats_table ".
			"		WHERE today BETWEEN '$search_start_searchdate' AND '$search_end_searchdate' AND campaign_id = $ids ".  
			"		GROUP BY adset_id, ad_id ".
			"	) sub1 JOIN tbl_user_marketing_retention_order sub2 ON sub1.ad_id = sub2.fb_rt_id ".
			$add_sql.
			"	GROUP BY adset_id ".
			") t2 ON t1.adset_id = t2.adset_id ".
			"ORDER BY updated_time DESC";	
	
	$contents_list = $db_main2->gettotallist($sql);
	
	$sql = "SELECT adset_id, SUM(join_count) AS sum_join_count ".
			"FROM ".
			"( ".
			"	SELECT adset_id, ad_id, (SELECT COUNT(useridx) FROM tbl_user_marketing_retention WHERE fb_rt_id = a.ad_id AND createdate BETWEEN '2016-05-26' AND '2016-06-10') AS join_count ".
			"	FROM $ad_stats_table a ".
			"	WHERE today BETWEEN '$search_start_searchdate' AND '$search_end_searchdate' AND campaign_id = $ids ".
			"	GROUP BY adset_id, ad_id ".
			") b GROUP BY adset_id ";
	$sum_join_count_list = $db_main2->gettotallist($sql);
	
	for($i = 0; $i < sizeof($sum_join_count_list); $i++)
	{
		$join_adset_id = $sum_join_count_list[$i]["adset_id"];
		$join_count = $sum_join_count_list[$i]["sum_join_count"];
								
		$sum_join_count_array[$join_adset_id] = $join_count;
	}
	
 	$sql = " SELECT sum(clicks) as clicks, today, cpc ". 
		   " FROM $ad_stats_table ".
		   " WHERE today BETWEEN '$search_start_searchdate' AND '$search_end_searchdate' ".
		   " AND campaign_id = $ids ".
		   " GROUP BY today ORDER BY today ASC ";
	$chart_list = $db_main2->gettotallist($sql);
	
	$sql = " SELECT SUM(a.campaign_reach) AS total_campaign_reach ". 
			" FROM ( ".
					" SELECT MAX(campaign_reach) as campaign_reach, today ". 
					" FROM $ad_stats_table ".
					" WHERE campaign_id = $ids ".
					" AND today BETWEEN '$search_start_searchdate' AND '$search_end_searchdate' ". 
					" GROUP BY today)a";
	$total_campaign_reach = $db_main2->getvalue($sql);
	
	$db_main2->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="/js/ui/ui.core.js"></script>
<script type="text/javascript" src="/js/ui/ui.datepicker.js"></script>
<link type="text/css" rel="stylesheet" href="../../css/marketing.css" />
<script type="text/javascript">
	$(function() {
		$("#start_searchedate").datepicker({});
		$("#end_searchdate").datepicker({});
		$("#start_orderdate").datepicker({});
		$("#end_orderdate").datepicker({});
	});

	google.load("visualization", "1", {packages:["corechart"]});
	  function drawChart() 
	    {
	        var data1 = new google.visualization.DataTable();
	        data1.addColumn('string', '날짜');
	        data1.addColumn('number', '결과: 클릭');
	        data1.addRows([<?
			foreach ($chart_list as $chart)
			{
				$clicks = $chart['clicks'];
				$chart_date = date("m-d", strtotime($chart['today']));
				
				$sum_toal_spend += $toal_spend;
				if($chart_date != date("m-d", strtotime($today)))
				{
					echo "['".$chart_date."',  {v:".$clicks." ,f:'".number_format($clicks)."'}],";
				}
				else
				{
					echo "['오늘',  {v:".$clicks." ,f:'".number_format($clicks)."'}]";
				}
			}
			?>]);
	        
	        var data2 = new google.visualization.DataTable();
	        data2.addColumn('string', '날짜');
	        data2.addColumn('number', '결과당 비용');
	        data2.addRows([<?
			foreach ($chart_list as $chart){
				$cpc = $chart['cpc'];
				$chart_date = date("m-d", strtotime($chart['today']));
				
				$sum_toal_spend += $toal_spend;
				if($chart_date != date("m-d", strtotime($today)))
				{
					echo "['".$chart_date."',  {v:".$cpc." ,f:'".number_format($cpc,2)."'}],";
				}
				else
				{
					echo "['오늘',  {v:".$cpc." ,f:'".number_format($cpc,2)."'}]";
				}
			}
			?>]);
	    

	        var options1 = {
	                width:1050,                       
	                height:190,
	                axisTitlesPosition:'in',
	                curveType:'none',
	                focusTarget:'category',
	                interpolateNulls:'true',
	                legend:'top',
	                fontSize : 12,
	                chartArea:{left:60,top:40,width:1040,height:100},
		        vAxis: { gridlines: { count: 3 } }
	            };
	            
	            var options2 = {
	                width:1050,                       
	                height:190,
	                axisTitlesPosition:'in',
	                curveType:'none',
	                focusTarget:'category',
	                interpolateNulls:'true',
	                legend:'top',
	                fontSize : 12,
	                chartArea:{left:60,top:40,width:1040,height:100},
		        vAxis: { gridlines: { count: 3 } }
	            };        
	        var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
	        chart.draw(data1, options1);        
	        
	        chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
	        chart.draw(data2, options2);      
	    }
	        
		google.setOnLoadCallback(drawChart);	


	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.start_date.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.start_date.focus();
	        return;
	    } 
	
	    search_form.submit();
	}

</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Facebook Retention 광고 세트 현황 - Mobile </div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<input type="hidden" name="ids" value="<?=$ids?>"> 
				<input type="text" class="search_text" id="start_searchedate" name="start_searchdate" value="<?= $search_start_searchdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_searchdate" name="end_searchdate" value="<?= $search_end_searchdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
			
			<div class="clear"></div>
                
			<div class="search_box">
				<input type="text" class="search_text" id="start_orderdate" name="start_orderdate" value="<?= $search_start_orderdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_orderdate" name="end_orderdate" value="<?= $search_end_orderdate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
				&nbsp;&nbsp;&nbsp;&nbsp;기간내 결제
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $search_start_searchdate ?></span> ~ <span><?= $search_end_searchdate ?></span> 통계입니다
	</div>
		<div id="chart_div1" style="height:200px; min-width: 500px"></div>
		<div id="chart_div2" style="height:200px; min-width: 500px"></div>
	<br/>
	<br/>
	<br/>
	<div id="js_1_layer" style="width: 220px;margin-left: 392px;margin-top: 32px;position: absolute; display:none;">
		<div class="io1" style="margin-left: 183px;">&nbsp;</div>
		<div class="_53il" style="border:0;border-radius:2px;box-shadow:0 0 0 1px rgba(0, 0, 0, .1), 0 1px 10px rgba(0, 0, 0, .35);">
			<div class="_53ij">
				<div data-reactroot="">
					<div class="_2ny1">
						<div class="_2ny2">결과</div>
						<div class="_f-m _2ny3">
							<div class="_if">
								<p class="_3p8">광고로 인해 발생한 결과. 이 결과는 선택한 목표를 기준으로 합니다.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="js_2_layer" style="width: 220px;margin-left: 503px;margin-top: 32px;position: absolute; display:none;">
		<div class="io1" style="margin-left: 183px;">&nbsp;</div>
		<div class="_53il" style="border:0;border-radius:2px;box-shadow:0 0 0 1px rgba(0, 0, 0, .1), 0 1px 10px rgba(0, 0, 0, .35);">
			<div class="_53ij">
				<div data-reactroot="">
					<div class="_2ny1">
						<div class="_2ny2">도달</div>
						<div class="_f-m _2ny3">
							<div class="_if">
								<p class="_3p8">광고가 게재된 사람 수입니다.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="js_3_layer" style="width: 220px;margin-left: 616px;margin-top: 32px;position: absolute; display:none;">
		<div class="io1" style="margin-left: 183px;">&nbsp;</div>
		<div class="_53il" style="border:0;border-radius:2px;box-shadow:0 0 0 1px rgba(0, 0, 0, .1), 0 1px 10px rgba(0, 0, 0, .35);">
			<div class="_53ij">
				<div data-reactroot="">
					<div class="_2ny1">
						<div class="_2ny2">비용</div>
						<div class="_f-m _2ny3">
							<div class="_if">
								<p class="_3p8">목표와 관련되어 발생한 각 행동당 평균 지출 비용.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="js_4_layer" style="width: 220px;margin-left: 808px;margin-top: 32px;position: absolute; display:none;">
		<div class="io1" style="margin-left: 183px;">&nbsp;</div>
		<div class="_53il" style="border:0;border-radius:2px;box-shadow:0 0 0 1px rgba(0, 0, 0, .1), 0 1px 10px rgba(0, 0, 0, .35);">
			<div class="_53ij">
				<div data-reactroot="">
					<div class="_2ny1">
						<div class="_2ny2">지출금액</div>
						<div class="_f-m _2ny3">
							<div class="_if">
								<p class="_3p8">광고 관리자에서 선택한 기간 동안 지출한 총 금액입니다.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<table class="tbl_list_basic_matket1" >
		<colgroup>
			<col width="5">
			<col width="180">
			<col width="50">
			<col width="50">
            <col width="60">
            <col width="60">
            <col width="60">
            <col width="60">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="5">
		</colgroup>
        <thead>
        	<tr>               	
               	<th class="tdl">&nbsp;</th>
        		<th class="tdl">광고세트 이름</th>
             	<th class="tdr">결과 <i class="_2ny7 img sp_rHAvPENsMSL sx_ff2fa9" aria-owns="js_24" aria-haspopup="true" tabindex="0" id="js_1" onmouseover="javascript:$('#js_1_layer').css('display','block');$('#js_1').css('opacity','1');" onmouseout="javascript:$('#js_1_layer').css('display','none');$('#js_1').css('opacity','.6');"></i></th>             	
               	<th class="tdr">도달수 <i class="_2ny7 img sp_rHAvPENsMSL sx_ff2fa9" aria-owns="js_24" aria-haspopup="true" tabindex="0" id="js_2" onmouseover="javascript:$('#js_2_layer').css('display','block');$('#js_2').css('opacity','1');" onmouseout="javascript:$('#js_2_layer').css('display','none');$('#js_2').css('opacity','.6');"></i></th>
               	<th class="tdr">비용 <i class="_2ny7 img sp_rHAvPENsMSL sx_ff2fa9" aria-owns="js_24" aria-haspopup="true" tabindex="0" id="js_3" onmouseover="javascript:$('#js_3_layer').css('display','block');$('#js_3').css('opacity','1');" onmouseout="javascript:$('#js_3_layer').css('display','none');$('#js_3').css('opacity','.6');"></i></th>
               	<th class="tdr">예산 <i class="_2ny7 img sp_rHAvPENsMSL sx_ff2fa9" aria-owns="js_24" aria-haspopup="true" tabindex="0" id="js_4" onmouseover="javascript:$('#js_4_layer').css('display','block');$('#js_4').css('opacity','1');" onmouseout="javascript:$('#js_4_layer').css('display','none');$('#js_4').css('opacity','.6');"></i></th>
               	<th class="tdr">지출금액 <i class="_2ny7 img sp_rHAvPENsMSL sx_ff2fa9" aria-owns="js_24" aria-haspopup="true" tabindex="0" id="js_5" onmouseover="javascript:$('#js_5_layer').css('display','block');$('#js_5').css('opacity','1');" onmouseout="javascript:$('#js_5_layer').css('display','none');$('#js_5').css('opacity','.6');"></i></th>               	
               	<th class="tdr">신규 가입<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_6"></i></th>
               	<th class="tdr">매출<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_7"></i></th>
               	<th class="tdr">결제자수<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_8"></i></th>
               	<th class="tdr">PUR<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_9"></i></th>
               	<th class="tdr">ARPU<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_10"></i></th>
               	<th class="tdr">ARPPU<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_11"></i></th>
               	<th class="tdr">ROI<i class="_2ny7" aria-owns="js_24" tabindex="0" id="js_12"></i></th>
               	<th class="tdl">&nbsp;</th>
         	</tr>
        </thead>
		<tbody>
<?
	$total_money = 0;
	$total_buy_user_cnt = 0;
	
	foreach ($contents_list as $contents)
	{
		$adset_id = $contents['adset_id'];
		$adset_name = $contents['adset_name'];
		$campaign_id = $contents['campaign_id'];
		$campaign_name = $contents['campaign_name'];
		$join_count = $sum_join_count_array[$adset_id];
		$clicks = $contents['clicks'];		
		$reach = $contents['reach'];
		$cpc = $contents['cpc'];
		$spend = $contents['spend'];
		$status = $contents['status'];
		$daily_budget = $contents['daily_budget'];
		$money = $contents['money'];
		$buy_user_cnt = $contents['buy_user_cnt'];
		
		$pur = ($buy_user_cnt == 0) ? 0 : round(($buy_user_cnt/$clicks)*100, 2);
		$arpu = ($clicks == 0) ? 0 : round($money/$clicks, 2);
		$arppu = ($money == 0) ? 0 : round($money/$buy_user_cnt, 1);
		$roi = ($spend == 0) ? 0 : round($money/$spend*100, 2);
		
		$total_money += $money;
		$total_buy_user_cnt += $buy_user_cnt;
		
		$total_clicks += $clicks;
		$total_join_count_result += $join_count ;
		$total_reach += $reach;
		$total_spend +=$spend;
?>    
			<tr>
				<td class="tdl">&nbsp;</td>
        		<td class="tdl point_title">
        			<a href="./fb_retention_ad_stats_mobile.php?ids=<?=$adset_id?>&start_searchdate=<?= $search_start_searchdate ?>&end_searchdate=<?= $search_end_searchdate ?>&start_orderdate=<?= $search_start_orderdate ?>&end_orderdate=<?= $search_end_orderdate ?>"><?=$adset_name?></a>
        			<br/>
        			<span>
        				<a href="./fb_retention_campaign_stats_mobile.php?start_searchdate=<?= $search_start_searchdate ?>&end_searchdate=<?= $search_end_searchdate ?>&start_orderdate=<?= $search_start_orderdate ?>&end_orderdate=<?= $search_end_orderdate ?>" style="font-size: 12px;line-height: 16px;"><?=$campaign_name?></a>
        			</span>
        		</td>
             	<td class="tdr "><?=number_format($clicks)?><br/><span >클릭</span></td>
               	<td class="tdr "><?=number_format($reach)?></td>
               	<td class="tdr ">$<?=number_format($cpc,2)?><br/><span >클릭 당</span></td>
               	<td class="tdr ">$<?=number_format($daily_budget)?></td>
               	<td class="tdr ">$<?=number_format($spend,2)?></td>
               	<td class="tdr "><?=number_format($join_count)?><br/><span >신규 가입</span></td>
               	<td class="tdr ">$<?=number_format($money,2)?></td>
               	<td class="tdr "><?=number_format($buy_user_cnt)?></td>
               	<td class="tdr "><?=number_format($pur, 2)?>%</td>
               	<td class="tdr ">$<?=number_format($arpu, 2)?></td>
               	<td class="tdr ">$<?=number_format($arppu, 1)?></td>
               	<td class="tdr "><?=number_format($roi, 2)?>%</td>
               	<td class="tdl ">&nbsp;</td>
         	</tr>
<?
	}
	
	$total_pur = ($total_buy_user_cnt == 0) ? 0 : round(($total_buy_user_cnt/$total_clicks)*100, 2);
	$total_arpu = ($total_clicks == 0) ? 0 : round($total_money/$total_clicks, 2);
	$total_arppu = ($total_money == 0) ? 0 : round($total_money/$total_buy_user_cnt, 1);
	$total_roi = ($total_spend == 0) ? 0 : round($total_money/$total_spend*100, 2);
?>
			<tr>
         		<td class="tdl">&nbsp;</td>
        		<td class="tdl point_title">광고세트 <?=number_format(sizeof($contents_list))?>개 결과</td>
             	<td class="tdr "><?=number_format($total_clicks)?><br/><span >클릭</span></td>
               	<td class="tdr "><?=number_format($total_campaign_reach)?><br/><span >명</span></td>
               	<td class="tdr ">$<?=($total_clicks == 0) ? 0 : number_format($total_spend/$total_clicks,2)?><br/><span >클릭 당</span></td>
               	<td class="tdr ">&nbsp;</td>
               	<td class="tdr ">$<?=number_format($toal_spend,2) ?><br/><span >총 지출</span></td>
               	<td class="tdr "><?=number_format($total_join_count_result)?><br/><span >총 신규 가입</span></td>
               	<td class="tdr ">$<?=number_format($total_money,2)?><br/><span >총 매출</span></td>
               	<td class="tdr "><?=number_format($total_buy_user_cnt)?><br/><span >총 결제자수</span></td>
               	<td class="tdr "><?=number_format($total_pur, 2)?>%<br/><span >총 PUR</span></td>
               	<td class="tdr ">$<?=number_format($total_arpu, 2)?><br/><span >총 ARPU</span></td>
               	<td class="tdr ">$<?=number_format($total_arppu, 1)?><br/><span >총 ARPPU</span></td>  
               	<td class="tdr "><?=number_format($total_roi, 2)?>%<br/><span >총 ROI</span></td>
               	<td class="tdl">&nbsp;</td>
         	</tr>
        </tbody>
	</table>
	
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>