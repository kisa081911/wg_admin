<?
    $top_menu = "marketing";
    $sub_menu = "adflag_detail";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    $search_start_orderdate = $_GET["start_orderdate"];
    $search_end_orderdate = $_GET["end_orderdate"];
    $search_payday = $_GET["payday"];
	$isearch = $_GET["issearch"];

	if($search_payday != "" && ($search_start_orderdate != "" || $search_end_orderdate != ""))
		error_back("검색값이 잘못되었습니다.");
		
	if ($isearch == "")
	{
		$search_end_createdate  = date("Y-m-d", time() - 60 * 60);
		$search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 6);
	}
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	
	$now = time();
	
	if ($search_end_createdate != "")
	{
		$now = strtotime($search_end_createdate);
	}
	else
	{
		$search_end_createdate = date('Y-m-d');
	}
	
	if ($search_start_createdate == "")
	{
		$search_start_createdate = '2015-10-20';
	}
	
	$sql =	"SELECT adflag, DATE_FORMAT(createdate,'%Y-%m-%d') AS day, COUNT(*) AS totalcount, ".
			"ABS(IFNULL(SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			"- IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)".
			",0)) AS unplaycount,";
	
	if ($search_payday != "")
	{
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0)) AS paycount,".
				"(IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) + IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0)) AS totalpaycount,".
				"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0))AS totalcredit ";
	}
	else if(($search_start_orderdate != "" && $search_end_orderdate != ""))
	{
		$search_start_orderdate_minute = $search_start_orderdate." 00:00:00";
		$search_end_orderdate_minute = $search_end_orderdate." 23:59:59";

		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute') THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute') THEN 1 ELSE 0 END),0)) AS paycount,".
				"(IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0) + IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0)) AS totalpaycount,".
				"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate BETWEEN '$search_start_orderdate_minute' AND '$search_end_orderdate_minute')),0)) AS totalcredit ";
	}
	else
	{
		$sql .= "(IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0) + IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0)) AS paycount,".
				"(IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) + IFNULL(SUM((SELECT IFNULL(COUNT(useridx),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1)),0)) AS totalpaycount,".
				"(IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) + IFNULL(SUM((SELECT IFNULL(SUM(money*10),0) FROM tbl_product_order_mobile WHERE useridx=tbl_user_ext.useridx AND status=1)),0)) AS totalcredit ";
	}
	
	$sdate = date('Y-m-d H:i:s', strtotime($search_start_createdate)); // 서버 시간
	$tail =  "createdate >='$sdate' AND createdate <='$search_end_createdate 23:59:59' GROUP BY adflag,DATE_FORMAT(createdate,'%Y-%m-%d')";
	
	$sql .= " FROM tbl_user_ext WHERE ".$tail." ORDER BY day DESC, adflag ASC";
	$summarylist = $db_main->gettotallist($sql);
	
	$sql = "SELECT adflag FROM tbl_adflag_flag WHERE category != 100";
	$marketing_list = $db_main2->gettotallist($sql);

	function get_stat($summarylist, $marketing_list, $adflag, $today, $property)
	{
		$stat = 0;
	
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if ($summarylist[$i]["day"] == $today)
			{
				$dummy = $summarylist[$i]["adflag"];
	
				if($dummy == "")
					$dummy = "viral";
				
				if($dummy == $adflag)
				{
				    $stat += $summarylist[$i][$property];
				    break;
				}
			}
		}
	
		return $stat;
	}
	
	function get_total_stat($summarylist, $marketing_list, $adflag, $property)
	{
		$stat = 0;
	
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			$dummy = $summarylist[$i]["adflag"];
			
			if($dummy == "")
				$dummy = "viral";
			
			if($dummy == $adflag)
			{
			    $stat += $summarylist[$i][$property];
			    break;
			}
		}
	
		return $stat;
	}
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});

	$(function() {
	    $("#start_orderdate").datepicker({ });
	});

	$(function() {
	    $("#end_orderdate").datepicker({ });
	});
	
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	
	    search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">  
	<form name="search_form" id="search_form"  method="get" action="adflag_detail.php">
		<input type=hidden name=issearch value="1" />
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 유입경로별 유입 현황</div>
			<div class="search_box">
				<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
				&nbsp;&nbsp;&nbsp;&nbsp;결제액 기준 : 가입 후 <input type="text" class="search_text" id="payday" name="payday" style="width:60px" value="<?= $search_payday ?>" onkeypress="search_press(event); return checknum();" />일 이내 결제
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
                
            <div class="clear"></div>
                
			<div class="search_box">
				<input type="text" class="search_text" id="start_orderdate" name="start_orderdate" value="<?= $search_start_orderdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_orderdate" name="end_orderdate" value="<?= $search_end_orderdate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
				&nbsp;&nbsp;&nbsp;&nbsp;기간내 결제
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
		</div>
		<!-- //title_warp -->
            
		<div class="search_result">
			<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
		</div>
            
		<div id="tab_content_1">
			<table class="tbl_list_basic1">
				<colgroup>
	                <col width="">
	                <col width="">
	                <col width="">	                
	                <col width="">
	                <col width=""> 
	                <col width="">
	                <col width=""> 
	                <col width=""> 
	            </colgroup>
            	<thead>
		            <tr>
		                <th>유입일</th>
		                <th class="tdr">유입경로</th>
		                <th class="tdr">회원가입수</th>
		                <th class="tdr">미게임회원수</th>
		                <th class="tdr">2주이탈복귀</th>
		                <th class="tdr">결제회원수</th>
		                <th class="tdr">총결제횟수</th>
		                <th class="tdr">총결제 credit<br>(평균)</th>
		            </tr>
            	</thead>
            	<tbody>
<?
    
	
	for($i=0; $i<sizeof($summarylist); $i++)
	{
		$today = $summarylist[$i]["day"];
		$adflag = $summarylist[$i]["adflag"];
		
		$summarylist[$i]["returncount"] = $db_main2->getvalue("SELECT IFNULL(SUM(usercount),0) FROM tbl_user_retention WHERE adflag = '$adflag' AND retentiondate='$today'");
	}
		
	$currenttoday = "";
		
	for($i=0; $i<sizeof($summarylist); $i++)
	{
		$today = $summarylist[$i]["day"];
		
		if($currenttoday != $today)
		{
			$currenttoday = $today;
			
			for($j=0; $j<sizeof($marketing_list); $j++)
			{
				$adflag_info = $marketing_list[$j]["adflag"];
				
				$totalcount = get_stat($summarylist, $marketing_list, $adflag_info, $today, "totalcount");				
				$unplaycount = get_stat($summarylist, $marketing_list, $adflag_info, $today, "unplaycount");
				$returncount = get_stat($summarylist, $marketing_list, $adflag_info, $today, "returncount");
				$paycount = get_stat($summarylist, $marketing_list, $adflag_info, $today, "paycount");
				$totalpaycount = get_stat($summarylist, $marketing_list, $adflag_info, $today, "totalpaycount");
				$totalcredit = get_stat($summarylist, $marketing_list, $adflag_info, $today, "totalcredit");

				
				$unplayratio = ($totalcount > 0) ? round($unplaycount * 10000 / $totalcount) / 100 : 0;
				$payratio = ($totalcount > 0) ? round($paycount * 10000 / $totalcount) / 100 : 0;
				$averagecredit = ($totalcount > 0) ? round($totalcredit * 1000 / $totalcount) / 1000 : 0;
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($j == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($marketing_list)?>" valign="center"><?= $today ?></td>
<?
				} 
?>
					<td class="tdr point"><?= $adflag_info ?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>                    
                    <td class="tdr point"><?= number_format($unplaycount) ?> (<?= $unplayratio ?> %)</td>
                    <td class="tdr point"><?= number_format($returncount) ?></td>
                    <td class="tdr point"><?= number_format($paycount) ?> (<?= $payratio ?> %)</td>
                    <td class="tdr point"><?= number_format($totalpaycount) ?></td>
                    <td class="tdr point"><?= number_format($totalcredit) ?> (<?= $averagecredit ?>)</td>
				</tr>
<?
			}
		}
	}
	
	if(sizeof($summarylist) > 0)
	{
		for($j=0; $j<sizeof($marketing_list); $j++)
		{
			$adflag_info = $marketing_list[$j]["adflag"];
			
			$sum_totalcount = get_total_stat($summarylist, $marketing_list, $adflag_info, "totalcount");
			$sum_unplaycount = get_total_stat($summarylist, $marketing_list, $adflag_info, "unplaycount");
			$sum_returncount = get_total_stat($summarylist, $marketing_list, $adflag_info, "returncount");
			$sum_paycount = get_total_stat($summarylist, $marketing_list, $adflag_info, "paycount");
			$sum_totalpaycount = get_total_stat($summarylist, $marketing_list, $adflag_info, "totalpaycount");
			$sum_totalcredit = get_total_stat($summarylist, $marketing_list, $adflag_info, "totalcredit");

			$sum_unplayratio = ($sum_totalcount > 0) ? round($sum_unplaycount * 10000 / $sum_totalcount) / 100 : 0;
			$sum_payratio = ($sum_totalcount > 0) ? round($sum_paycount * 10000 / $sum_totalcount) / 100 : 0;
			$sum_averagecredit = ($sum_totalcount > 0) ? round($sum_totalcredit * 1000 / $sum_totalcount) / 1000 : 0;
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($j == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($marketing_list)?>" valign="center">Total</td>
<?
				}
?>
					<td class="tdr point"><?= $adflag_info ?></td>
                    <td class="tdr point"><?= number_format($sum_totalcount) ?></td>
                    <td class="tdr point"><?= number_format($sum_unplaycount) ?> (<?= $sum_unplayratio ?> %)</td>
                    <td class="tdr point"><?= number_format($sum_returncount) ?></td>
                    <td class="tdr point"><?= number_format($sum_paycount) ?> (<?= $sum_payratio ?> %)</td>
                    <td class="tdr point"><?= number_format($sum_totalpaycount) ?></td>
                    <td class="tdr point"><?= number_format($sum_totalcredit) ?> (<?= $sum_averagecredit ?>)</td>
			</tr>
<?
		}
	}

	$db_main->end();
	$db_main2->end();
?>    
            	</tbody>
            </table>
        </div>
	</form>
</div>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>