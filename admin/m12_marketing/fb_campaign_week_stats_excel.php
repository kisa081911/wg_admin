<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	check_login();
	
	$search_startdate = $_GET["sdate"];
	$search_enddate = $_GET["edate"];
	$pay_startdate = $_GET["pay_sdate"];
	$pay_enddate = $_GET["pay_edate"];
	$search_campaign = $_GET["campaign_id"];
	$search_adset = $_GET["adset_id"];
	
	$filename = "fb_campaign_week_execel.xls";
		
	$db_main2 = new CDatabase_main2();
	
	$ad_stats_table="tbl_ad_stats";
	
	$search_sql = "";
	
	if($search_campaign != "")
		$search_sql .= " AND campaign_id = $search_campaign ";
	
	if($search_adset != "")
		$search_sql .= " AND adset_id = $search_adset ";

	$sql = "SELECT t3.week_count, start_date, end_date, app_install, spend, IFNULL(pay_user_count, 0) AS pay_user_count, IFNULL(pay_count, 0) AS pay_count, IFNULL(money, 0) AS money ".
			"FROM ( ".
			"	SELECT WEEKOFYEAR(today) AS week_count, MIN(today) AS start_date, MAX(today) AS end_date, SUM(app_install_result) AS app_install, ROUND(SUM(spend), 2) AS spend ".
			"	FROM $ad_stats_table ".
			"	WHERE today BETWEEN '$search_startdate' AND '$search_enddate' $search_sql ".
			"	GROUP BY WEEKOFYEAR(today) ".
			") t3 LEFT JOIN ( ".
			"	SELECT WEEKOFYEAR(DATE_SUB(writedate, INTERVAL dayafterinstall DAY)) AS week_count, SUM(money) AS money, COUNT(DISTINCT useridx) AS pay_user_count, COUNT(orderidx) AS pay_count ".
			"	FROM tbl_user_marketing_order ".
			"	WHERE EXISTS ( ".
			"		SELECT useridx, fb_ad_id, createdate ".
			"		FROM ( ".
			"			SELECT ad_id FROM $ad_stats_table ".
			"			WHERE today BETWEEN '$search_startdate' AND '$search_enddate' $search_sql ". 
			"		) t1 JOIN ( ".
			"			SELECT fb_ad_id, useridx, createdate FROM tbl_user_marketing ".
			"			WHERE '$search_startdate 00:00:00' <= createdate AND createdate <= '$search_enddate 23:59:59' ".
			"		) t2 ON t1.ad_id = t2.fb_ad_id ".
			"		WHERE useridx = tbl_user_marketing_order.useridx ".
			"	) AND '$pay_startdate 00:00:00' <= writedate AND writedate <= '$pay_enddate 23:59:59' ".
			"	GROUP BY WEEKOFYEAR(DATE_SUB(writedate, INTERVAL dayafterinstall DAY)) ".
			") t4 ON t3.week_count = t4.week_count";
	
	$contents_list = $db_main2->gettotallist($sql);
	
	$db_main2->end();
	
	if (sizeof($contents_list) == 0)
		error_go("저장할 데이터가 없습니다.", "fb_campaign_week_stats.php");
	
	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
	"<table border=1>".
		"<tr>".
			"<td style='font-weight:bold;'>구간</td>".
			"<td style='font-weight:bold;'>회원가입수</td>".
			"<td style='font-weight:bold;'>CPI</td>".
			"<td style='font-weight:bold;'>비용</td>".
			"<td style='font-weight:bold;'>총결제</td>".
			"<td style='font-weight:bold;'>ROI</td>".
			"<td style='font-weight:bold;'>결제자수</td>".
			"<td style='font-weight:bold;'>결제자 비율</td>".
			"<td style='font-weight:bold;'>결제 건수</td>".
			"<td style='font-weight:bold;'>평균 결제 횟수</td>".
			"<td style='font-weight:bold;'>평균 결제 금액</td>".
			"<td style='font-weight:bold;'>ARPU</td>".
			"<td style='font-weight:bold;'>ARPPU</td>".
		"</tr>";
	
		$total_install = 0;
		$total_spend = 0;
		$total_cpi = 0;
		$total_money = 0;
		$total_roi = 0;
		$total_pay_user_count = 0;
		$total_pay_count = 0;
		$total_payer_rate = 0;
		$total_avg_pay_count = 0;
		$total_avg_pay_money = 0;
		$total_arpu = 0;
		$total_arppu = 0;
	
		for ($i=0; $i < sizeof($contents_list); $i++)
		{
			$week_count = $contents_list[$i]["week_count"];
			$start_date = $contents_list[$i]["start_date"];
			$end_date = $contents_list[$i]["end_date"];
			$app_install = $contents_list[$i]["app_install"];
			$spend = $contents_list[$i]["spend"];
			$cpi = ($app_install == 0) ? 0 : round($spend/$app_install, 2);
			$money = $contents_list[$i]["money"];
			$roi = ($spend == 0) ? 0 : round($money/$spend*100, 2);
			$pay_user_count = $contents_list[$i]["pay_user_count"];
			$payer_rate = ($app_install == 0) ? 0 : round($pay_user_count/$app_install*100, 2);
			$pay_count = $contents_list[$i]["pay_count"];
			$avg_pay_count = ($pay_user_count == 0) ? 0 : round($pay_count/$pay_user_count, 2);
			$avg_pay_money = ($pay_count == 0) ? 0 : round($money/$pay_count, 2);
			$arpu = ($app_install == 0) ? 0 : round($money/$app_install, 2);
			$arppu = ($pay_user_count == 0) ? 0 : round($money/$pay_user_count, 2);
			
			$total_install += $app_install;
			$total_spend += $spend;
			$total_money += $money;
			$total_pay_user_count += $pay_user_count;
			$total_pay_count += $pay_count;
  			
  			$excel_contents .= "<tr>".
    			"<td>구간 ".$week_count." - $start_date ~ $end_date</td>".
		  		"<td>".number_format($app_install)."</td>".
		  		"<td>$".number_format($cpi, 2)."</td>".
		  		"<td>$".number_format($spend, 2)."</td>".
		  		"<td>$".number_format($money, 2)."</td>".
		  		"<td>".number_format($roi, 2)."%</td>".
		  		"<td>".number_format($pay_user_count)."</td>".
		  		"<td>".number_format($payer_rate,2)."%</td>".
		  		"<td>".number_format($pay_count)."</td>".
		  		"<td>".number_format($avg_pay_count, 2)."</td>".
		  		"<td>$".number_format($avg_pay_money, 2)."</td>".
		  		"<td>".number_format($arpu, 2)."</td>".
		  		"<td>".number_format($arppu, 2)."</td>".
	  		"</tr>";
  		}
  		
  		$total_cpi = ($total_install == 0) ? 0 : round($total_spend/$total_install, 2);
		$total_roi = ($total_spend == 0) ? 0 : round($total_money/$total_spend*100, 2);
		$total_payer_rate = ($total_install == 0) ? 0 : round($total_pay_user_count/$total_install*100, 2);
		$total_avg_pay_count = ($total_pay_user_count == 0) ? 0 : round($total_pay_count/$total_pay_user_count, 2);
		$total_avg_pay_money = ($total_pay_count == 0) ? 0 : round($total_money/$total_pay_count, 2);
		$total_arpu = ($total_install == 0) ? 0 : round($total_money/$total_install, 2);
		$total_arppu = ($total_pay_user_count == 0) ? 0 : round($total_money/$total_pay_user_count, 2);
		
		$excel_contents .= "<tr>".
				"<td>합계</td>".
				"<td>".number_format($total_install)."</td>".
				"<td>$".number_format($total_cpi, 2)."</td>".
				"<td>$".number_format($total_spend, 2)."</td>".
				"<td>$".number_format($total_money, 2)."</td>".
				"<td>".number_format($total_roi, 2)."%</td>".
				"<td>".number_format($total_pay_user_count)."</td>".
				"<td>".number_format($total_payer_rate,2)."%</td>".
				"<td>".number_format($total_pay_count)."</td>".
				"<td>".number_format($total_avg_pay_count, 2)."</td>".
				"<td>$".number_format($total_avg_pay_money, 2)."</td>".
				"<td>".number_format($total_arpu, 2)."</td>".
				"<td>".number_format($total_arppu, 2)."</td>".
				"</tr>";
						  
		$excel_contents .= "</table>";
  
		Header("Content-type: application/x-msdownload");
		Header("Content-type: application/x-msexcel");
		Header("Content-Disposition: attachment; filename=$filename");
  
		echo($excel_contents);
?>