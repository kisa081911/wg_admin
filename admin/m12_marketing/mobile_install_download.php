<?
	$top_menu = "marketing";
	$sub_menu = "mobile_install_download";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$page = ($_GET["page"] == "") ? "1" : $_GET["page"];
	
	$lastweek = time() - (7 * 24 * 60 * 60);
	
	$search_platform = $_GET["platform"];
	$search_install_sd = ($_GET["install_sd"] == "")? date('Y-m-d', $lastweek) : $_GET["install_sd"];
	$search_install_ed = ($_GET["install_ed"] == "")? date('Y-m-d') : $_GET["install_ed"];
	$search_media = ($_GET["media"] == "")? "all" : $_GET["media"];
	
	$listcount = "10";
	$pagefield = "platform=$search_platform&install_sd=$search_install_sd&install_ed=$search_install_ed&media=$search_media";
	
	$tail = " WHERE install_time BETWEEN '$search_install_sd 00:00:00' AND '$search_install_ed 23:59:59' ";
	$order_by = "ORDER BY install_time DESC, media_source ASC ";
	
	$db_main2 = new CDatabase_Main2();
	
	$sql = "SELECT media_source FROM tbl_appsflyer_install $tail GROUP BY media_source";
	$media_source_list = $db_main2->gettotallist($sql);
	
	if($search_media != "all")
	{
		if($search_media == "blank")
			$tail .= "AND media_source = '' ";
		else
			$tail .= "AND media_source = '$search_media' ";
	}
	
	if($search_platform != 0)
		$tail .= "AND platform = $search_platform ";
		
	$totalcount = $db_main2->getvalue("SELECT COUNT(*) FROM tbl_appsflyer_install $tail");
	
	$sql = "SELECT * FROM tbl_appsflyer_install $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;
	$install_raw_data_list = $db_main2->gettotallist($sql);
		
	if ($totalcount < ($page-1) * $listcount && page != 1)
		$page = floor(($totalcount + $listcount - 1) / $listcount);
		
	$db_main2->end();
?>

<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>

<script>
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}

	function search()
	{
	    var search_form = document.search_form;
	
	    search_form.submit();
	}

	$(function() {
	    $("#install_sd").datepicker({ });
	    $("#install_ed").datepicker({ });
	});
</script>

	<!-- CONTENTS WRAP -->
	<div class="contents_wrap">
		<!-- title_wrap -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 모바일 Install Raw Data 추출 <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
		
		<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="mobile_install_download.php">
			<div class="search_box">
				플랫폼
				<select name="platform" id="platform">
	            	<option value="" <?= ($search_platform == "") ? "selected" : "" ?>>전체</option>
	                <option value="1" <?= ($search_platform == "1") ? "selected" : "" ?>>iOS</option>
	                <option value="2" <?= ($search_platform == "2") ? "selected" : "" ?>>Android</option>
	                <option value="3" <?= ($search_platform == "3") ? "selected" : "" ?>>Amazon</option>
				</select>
				&nbsp;&nbsp;&nbsp;
				media source
				<select name="media" id="media">
	            	<option value="all" <?= ($search_media == "all") ? "selected" : "" ?>>전체</option>
<?
				for($i=0; $i<sizeof($media_source_list); $i++)
				{
					$media = $media_source_list[$i]["media_source"];
					
					$media = ($media == "") ? "blank" : $media; 
					$media_name = ($media == "blank") ? "" : $media; 
?>
					<option value="<?= $media?>" <?= ($search_media == $media) ? "selected" : "" ?>><?= $media_name?></option>
<?
				}
?>
				</select>
				&nbsp;&nbsp;&nbsp;
				<input type="text" class="search_text" id="install_sd" name="install_sd" style="width:65px" value="<?= $search_install_sd ?>" 
				onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
                <input type="text" class="search_text" id="install_ed" name="install_ed" style="width:65px" value="<?= $search_install_ed ?>" 
                onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
                
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</div>
		<!-- //title_wrap -->
		
		<div class="search_result">
			<span><?= $search_install_sd ?></span> ~ <span><?= $search_install_ed ?></span> 통계입니다
			<input type="button" class="btn_03" value="Excel 다운로드" 
			onclick="window.location.href='mobile_install_download_excel.php?<?= $pagefield ?>'">
		</div>	
		
		<div id="tab_content_1">
		<table class="tbl_list_basic1">
		<colgroup>
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
        </colgroup>
            
		<thead>
			<tr>
				<th>no</th>
				<th>platform</th>
				<th>install_time</th>
				<th>agency</th>
				<th>media_source</th>
				<th>country_code</th>
				<th>city</th>
				<th>device</th>
			</tr>
		</thead>
			
		<tbody>
<?
			for($i=0; $i < sizeof($install_raw_data_list); $i++)
			{
				$platform = $install_raw_data_list[$i]["platform"]; 
                $install_time = $install_raw_data_list[$i]["install_time"];  
                $agency =  $install_raw_data_list[$i]["agency"];
                $media_source =  $install_raw_data_list[$i]["media_source"];
                $country_code =  $install_raw_data_list[$i]["country_code"];
                $city = $install_raw_data_list[$i]["city"];
                $device = $install_raw_data_list[$i]["device_type"];
                
				if ($platform == "1")
					$platform = "iOS";
				else if ($platform == "2")
					$platform = "Android";
				else if ($platform == "3")
					$platform = "Amazon";
?>
			<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc"><?=  $totalcount - (($page-1) * $listcount) - $i ?></td>
				<td class="tdc"><?= $platform ?></td>
				<td class="tdc"><?= $install_time ?></td>
				<td class="tdc"><?= $agency ?></td>
				<td class="tdc"><?= $media_source ?></td>
				<td class="tdc"><?= $country_code ?></td>
				<td class="tdc"><?= $city ?></td>
				<td class="tdc"><?= $device ?></td>
			</tr>
<?
			}
?>
			</tbody>
			</table>
			</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
			<div class="button_warp tdr">
				           
			</div>
		</form>
	</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>