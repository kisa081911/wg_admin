<?
	$top_menu = "marketing";
	$sub_menu = "marketing_spend_stat";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$db_main2 = new CDatabase_Main2();
	
	$startdate = ($_GET["startdate"] == "") ? date("Y-m-d", strtotime("-28 day")) : $_GET["startdate"];
	$enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
	
	$term = ($_GET["term"] == "") ? "platform" : $_GET["term"];
	$search_agencyidx = ($_GET["agencyidx"] == "") ? "" : $_GET["agencyidx"];
	
	/* if ($term != "0" && $term != "1" && $term != "2" && $term != "3")
		error_back("잘못된 접근입니다."); */
	
	$platform = $term;
	
	// Data SQL
	/* $sql = "SELECT week, SUM(IF(type = 0, spend, 0)) AS spend,
			SUM(IF(type= 0, user_cnt, 0)) AS type_0_user_cnt,
			SUM(IF(type= 1, user_cnt, 0)) AS type_1_user_cnt,
			SUM(IF(type = 2, user_cnt, 0)) AS type_2_user_cnt,
			SUM(IF(type = 0, money, 0)) AS type_0_money,
			SUM(IF(type = 1, money, 0)) AS type_1_money,
			SUM(IF(type = 2, money, 0)) AS type_2_money
			FROM tbl_marketing_stat_daily_new
			WHERE today = '$today' AND platform = $platform $adflag_tail AND spend <> 0
			GROUP BY week
			ORDER BY week ASC";
	$marketing_week_stats = $db_other->gettotallist($sql); */
	
	$sql = "SELECT t1.agencyidx, t1.agencyname, hand_input
			FROM tbl_agency_spend_daily t1 JOIN tbl_agency_list t2 ON t1.agencyidx = t2.agencyidx 
			WHERE today BETWEEN '$startdate' AND '$enddate'
			GROUP BY t1.agencyidx
			ORDER BY t1.agencyname ASC";
	$adflag_list = $db_main2->gettotallist($sql);
	
	$parent_arr = array();
	
	$spend_index = 0;
	
	for($i=0; $i<sizeof($adflag_list); $i++)
	{
		$agencyidx = $adflag_list[$i]["agencyidx"];
		$agencyname = $adflag_list[$i]["agencyname"];
		$hand_input = $adflag_list[$i]["hand_input"];
		
		if($search_agencyidx != "" && $agencyidx != $search_agencyidx)
			continue;
		
		$sql = "SELECT today, SUM(spend) AS spend
				FROM tbl_agency_spend_daily
				WHERE today BETWEEN '$startdate' AND '$enddate'
				AND platform = $platform AND agencyidx = $agencyidx
				AND spend > 0
				GROUP BY today";
		$spend_arr = $db_main2->gettotallist($sql);
		
		if(sizeof($spend_arr) == 0)
			continue;
		
		$parent_arr[$spend_index][0] = $agencyidx;
		$parent_arr[$spend_index][1] = $agencyname;
		$parent_arr[$spend_index][2] = $spend_arr;
		$parent_arr[$spend_index++][3] = $hand_input;
	}
	
	$date_pointer = $enddate;
	
	for($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
	{
		$date_list[$i] = $date_pointer;
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	function pop_marketing_spend_history(platform, agencyidx, startdate, enddate)
	{
		 window.open('/m12_marketing/pop_marketing_spend_history.php?platform='+platform+'&agencyidx='+agencyidx+'&startdate='+startdate+'&enddate='+enddate, '', 'width=330,height=750,toolbar=no,menubar=no,scrollbars=yes,resizable=no');
	}
	
	function tab_change(tab)
	{
		var search_form = document.search_form;
		search_form.tab.value = tab;
		search_form.submit();
	}
	
	function change_term(term)
	{
		var search_form = document.search_form;
		
		var all = document.getElementById("term_all");
		var facebook = document.getElementById("term_facebook");
		var ios = document.getElementById("term_ios");
		var android = document.getElementById("term_android");
		var amazon = document.getElementById("term_amazon");
		var web = document.getElementById("term_web");
		
		search_form.term.value = term;
		
		if (term == "term")
		{
			all.className 		= "btn_schedule_select";
			facebook.className 	= "btn_schedule";
			ios.className 		= "btn_schedule";
			android.className 	= "btn_schedule";
			amazon.className 	= "btn_schedule";
			web.className 		= "btn_schedule";
		}
		else if (term == "0")
		{
			all.className 		= "btn_schedule";
			facebook.className 	= "btn_schedule_select";
			ios.className 		= "btn_schedule";
			android.className 	= "btn_schedule";
			amazon.className 	= "btn_schedule";
			web.className 		= "btn_schedule";
		}
		else if (term == "1")
		{
			all.className 		= "btn_schedule";
			facebook.className 	= "btn_schedule";
			ios.className 		= "btn_schedule_select";
			android.className 	= "btn_schedule";
			amazon.className 	= "btn_schedule";
			web.className 		= "btn_schedule";
		}
		else if (term == "1")
		{
			all.className 		= "btn_schedule";
			facebook.className 	= "btn_schedule";
			ios.className 		= "btn_schedule_select";
			android.className 	= "btn_schedule";
			amazon.className 	= "btn_schedule";
			web.className 		= "btn_schedule";
		}
		else if (term == "2")
		{
			all.className 		= "btn_schedule";
			facebook.className 	= "btn_schedule";
			ios.className 		= "btn_schedule";
			android.className 	= "btn_schedule_select";
			amazon.className 	= "btn_schedule";
			web.className 		= "btn_schedule";
		}
		else if (term == "3")
		{
			all.className 		= "btn_schedule";
			facebook.className 	= "btn_schedule";
			ios.className 		= "btn_schedule";
			android.className 	= "btn_schedule";
			amazon.className 	= "btn_schedule_select";
			web.className 		= "btn_schedule";
		}
		else if (term == "4")
		{
			all.className 		= "btn_schedule";
			facebook.className 	= "btn_schedule";
			ios.className 		= "btn_schedule";
			android.className 	= "btn_schedule";
			amazon.className 	= "btn_schedule";
			web.className 		= "btn_schedule_select";
		}
	
		search_form.submit();
	}
	
	$(function() {
		$("#startdate").datepicker({});
		$("#enddate").datepicker({});
	});

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
		
	function search()
	{
	    var search_form = document.search_form;
	    
	    if (search_form.startdate.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    }
	    
	    if (search_form.enddate.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    }
	    
	    search_form.submit();
	}

	function pop_marketing_cost_input_list()
	{
		open_layer_popup("pop_marketing_cost_input.php?basic_date=<?= substr($enddate, 0, 7) ?>&is_retention=0&is_spend_page=1", 750, 620);
	}
	
	google.load("visualization", "1", {packages:["corechart"]});

	function drawChart() 
	{
<?
		for($i=0; $i<sizeof($parent_arr); $i++)
		{
			$agencyidx = $parent_arr[$i][0];
			$agencyname = $parent_arr[$i][1];
			$spend_list = $parent_arr[$i][2];
			
			$spend_index = 0;
?>
			var data<?= $i ?> = new google.visualization.DataTable();
			data<?= $i ?>.addColumn('string', '날짜');
			data<?= $i ?>.addColumn('number', '비용');
			data<?= $i ?>.addRows([
<?
		    for ($j=sizeof($date_list); $j>0; $j--)
		    {
		    	$is_graph = 0;
		    	$_date = $date_list[$j-1];
		    	$_today = $spend_list[$spend_index]["today"];
		    	
				if($_date == $_today)
					$is_graph = 1;
				
				$_spend = ($is_graph == 1) ? $spend_list[$spend_index++]["spend"] : "0";
				
				echo("['".$_date."'");
				
				if ($_spend != "")
					echo(",{v:".$_spend.",f:'".number_format($_spend, 2)."'}]");
				else
					echo(",0]");
				
				if ($j > 1)
					echo(",");
			}
?>
			]);
<?
		}
?>
        var options = {
    	        title:'',                                                
    	        width:1100,                         
    	        height:200,
    	        axisTitlesPosition:'in',
    	        curveType:'none',
    	        focusTarget:'category',
    	        interpolateNulls:'true',
    	        legend:'top',
    	        fontSize : 12,
    	        chartArea:{left:100,top:40,width:1100,height:130}
    	    };

<?
		for($i=0; $i<sizeof($parent_arr); $i++)
		{
?>
			chart = new google.visualization.LineChart(document.getElementById('chart_div<?= $i ?>'));
			chart.draw(data<?= $i ?>, options);
<?
		}
?>
	}

	google.setOnLoadCallback(drawChart);
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 마케팅 비용 현황  <input type="button" class="btn_03" value="비용 수기 입력" onclick="pop_marketing_cost_input_list()"/></div>
		
		<br/>
		
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<input type="hidden" name="term" id="term" value="<?= $term ?>" />
			<div class="search_box">
				
				Adflag : 
				<select name="agencyidx" id="agencyidx">
					<option value="" <?= ($search_agencyidx == "" || $search_agencyidx == "agencyidx") ? "selected" : "" ?>>전체</option>
<?
	for($i=0; $i<sizeof($adflag_list); $i++)
	{
		$agencyidx = $adflag_list[$i]["agencyidx"];
		$agencyname = $adflag_list[$i]["agencyname"];
?>
					<option value="<?= $agencyidx ?>" <?= ($search_agencyidx == $agencyidx) ? "selected" : "" ?>><?= $agencyname ?></option>
<?
	}
?>
			    </select>
			    &nbsp;&nbsp;
				<input type="button" class="<?= ($term == "platform") ? "btn_schedule_select" : "btn_schedule" ?>" value="통합" id="term_all" onclick="change_term('platform')" />
				<input type="button" class="<?= ($term == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Facebook" id="term_facebook" onclick="change_term('0')" />
				<input type="button" class="<?= ($term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_term('1')" />
				<input type="button" class="<?= ($term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_term('2')"    />
				<input type="button" class="<?= ($term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_term('3')"    />
				
				<br/><br/>
				
				<div style="float: right; margin-right: 2px">
					그래프 날짜 : &nbsp;&nbsp;
					<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" />~
					<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" />
					<input type="button" class="btn_search" value="검색" onclick="search()" />
				</div>
			</div>
		</form>
	</div>
	<!-- //title_warp -->
<?
	for($i=0; $i<sizeof($parent_arr); $i++)
	{
		$agencyidx = $parent_arr[$i][0];
		$agencyname = $parent_arr[$i][1];
		$hand_input = $parent_arr[$i][3];
?>
		<div class="h2_title mt30" name="div_normal">
			[<?= $agencyname ?>] <?= ($hand_input == 1) ? "<text style='font-size:8px'>수기</text>" : "" ?><a href="#" style="font-size:11px;color:#28AEFF;font-weight:bold;cursor:ponter;" onclick="pop_marketing_spend_history('<?= $platform ?>', <?= $agencyidx ?>, '<?= $startdate ?>', '<?= $enddate ?>')"> - 상세보기 </a>
		</div>
		<div id="chart_div<?= $i ?>" name="div_normal" style="height:230px; min-width: 500px"></div>
<?
	}
?>
</div>
<?
$db_main2->end();
	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>