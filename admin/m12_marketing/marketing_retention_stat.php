<?
    $top_menu = "marketing";
    $sub_menu = "marketing_retention_stat";
    
    include_once("../common/dbconnect/db_util_redshift.inc.php");
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $startdate = ($_GET["startdate"] == "") ? date("Y-m-d", strtotime("-29 day")) : $_GET["startdate"];
    $enddate = $_GET["enddate"];
    
    $search_data = ($_GET["search_data"] == "") ? 0 : $_GET["search_data"];
    $os_type = ($_GET["os_type"] == "") ? 0 : $_GET["os_type"];
    $payday = ($_GET["payday"] == "") ? 28 : $_GET["payday"];
    
    //오늘 날짜 정보
    $today = get_past_date(date("Y-m-d"),1,"d");
    $enddate = ($enddate == "") ? $today : $enddate;
    
    $tail = "";
    
    if($search_data == "0")
    	$tail = "AND agencyname = 'Facebook Ads'";
    
    $pay_tail = " AND DATEDIFF(day, retentiondate, writedate) < $payday";
    
    if($os_type == 0)
    {
    	$adset_querystr = "SUBSTRING(adset_name, 8, 9) AS adflag";
    	$adset2_querystr = "SUBSTRING(adset_name, 8, 16) AS adflag";
    	$adflag_querystr = "(CASE WHEN adflag LIKE 'retention%' THEN 'retention' ELSE adflag END) AS adflag";
    	$adflag2_querystr = "AND adflag LIKE 'retention%'";
    	$retention_table = "t5_user_retention_log";
    	$retention_adflag_querystr = "(CASE WHEN adflag LIKE 'retention%' THEN 'retention' ELSE adflag END) AS adflag";
    	$retention_adflag_querystr2 = "AND adflag LIKE 'retention%'";
    }
    else
    {
    	$adset_querystr = "SUBSTRING(adset_name, 8, 11) AS adflag";
    	$adset2_querystr = "SUBSTRING(adset_name, 8, 18) AS adflag";
    	$adflag_querystr = "(CASE WHEN adflag LIKE 'm_retention%' THEN 'm_retention' ELSE adflag END) AS adflag";
    	$adflag2_querystr = "AND adflag LIKE 'm_retention%'";
    	$retention_table = "t5_user_retention_mobile_log";
    	$retention_adflag_querystr = "(CASE WHEN adflag LIKE 'm_retention%' THEN 'm_retention' ELSE adflag END) AS adflag";
    	$retention_adflag_querystr2 = "AND adflag LIKE 'm_retention%'";
    }
    	
    $pagename = "marketing_retention_stat.php";
    
    $db_other = new CDatabase_Other();
    $db_redshift = new CDatabase_Redshift();
    
    if($search_data == "0")
    {
    	$sql = "SELECT today, platform, adflag, spend, SUM(newuser_cnt) AS newuser_cnt, SUM(leavedays_7) AS leavedays_7, SUM(leavedays_7_28) AS leavedays_7_28, SUM(leavedays_28) AS leavedays_28
				FROM (
				    SELECT t1.today, t1.platform, t1.adflag, t1.spend, SUM(reg_cnt) AS newuser_cnt, 0 AS leavedays_7, 0 AS leavedays_7_28, 0 AS leavedays_28
				    FROM (
				    	SELECT today, platform, agencyname AS adflag, spend 
                        FROM t5_agency_spend_daily 
                        WHERE agencyidx > 1000 AND today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type AND spend > 0 $tail
				    	UNION ALL
				    	SELECT today, platform, $adset_querystr, SUM(spend) AS spend 
                        FROM t5_ad_retention_stats 
                        WHERE today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type 
				    	GROUP BY today, platform, adflag
					) t1 LEFT JOIN (
				    	SELECT platform, TRUNC(createdate) AS createdate, adflag, COUNT(*) AS reg_cnt
				    	FROM (
					      	SELECT platform, useridx, $adflag_querystr, createdate
					      	FROM t5_user
					      	WHERE useridx > 20000 AND platform = $os_type AND createdate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $adflag2_querystr
				    	) t1
				    	GROUP BY platform, TRUNC(createdate), adflag
				    ) t2 ON t1.platform = t2.platform AND t1.adflag = t2.adflag AND t1.today = t2.createdate
				    GROUP BY t1.today, t1.platform, t1.adflag, t1.spend
				    UNION ALL
				    SELECT t1.today, t1.platform, t1.adflag, t1.spend, 0 as newuser_cnt, sum(leavedays_7) AS leavedays_7, sum(leavedays_7_28) AS leavedays_7_28, sum(leavedays_28) AS leavedays_28
				    FROM (
				    	SELECT today, platform, agencyname AS adflag, spend 
                        FROM t5_agency_spend_daily 
                        WHERE agencyidx > 1000 AND today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type AND spend > 0 $tail
				    	UNION ALL
				    	SELECT today, platform, $adset_querystr, SUM(spend) AS spend 
                        FROM t5_ad_retention_stats 
                        WHERE today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type
				    	GROUP BY today, platform, adflag
				    ) t1 LEFT JOIN (
				    	SELECT platform, TRUNC(writedate) AS today, adflag, SUM(leavedays_7) AS leavedays_7, SUM(leavedays_7_28) AS leavedays_7_28, SUM(leavedays_28) AS leavedays_28
				    	FROM (
				      		SELECT platform, useridx, $retention_adflag_querystr, leavedays, is_payer, writedate,
				      			(CASE WHEN leavedays < 7 THEN 1 ELSE 0 end) AS leavedays_7,
				      			(CASE WHEN leavedays >= 7 AND leavedays < 28 THEN 1 ELSE 0 END) AS leavedays_7_28,
				      			(CASE WHEN leavedays >= 28 THEN 1 ELSE 0 END) AS leavedays_28
				      		FROM $retention_table
				      		WHERE useridx > 20000 AND platform = $os_type AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $retention_adflag_querystr2
				    	) t1
				    	GROUP BY platform, TRUNC(writedate), adflag
				    ) t2 ON t1.platform = t2.platform AND t1.adflag = t2.adflag AND t1.today = t2.today
				    GROUP BY t1.today, t1.platform, t1.adflag, t1.spend
				) t3
				GROUP BY today, platform, adflag, spend 
    			ORDER BY 1 DESC, 2 ASC, 3 ASC ";
    	$mkt_spend_list = $db_redshift->gettotallist($sql);    	
    	
    	// 비용 + 신규 매출
    	$sql = "SELECT t1.today, t1.platform, t1.adflag, t1.spend, SUM(total_money) AS total_money
		    	FROM (
		    		SELECT today, platform, agencyname AS adflag, spend FROM t5_agency_spend_daily WHERE agencyidx > 1000 AND today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type AND spend > 0 $tail
					UNION ALL
					SELECT today, platform, $adset_querystr, SUM(spend) AS spend FROM t5_ad_retention_stats WHERE today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type
					GROUP BY today, platform, adflag
		    	) t1 LEFT JOIN (
                    SELECT platform, today, adflag, COUNT(DISTINCT(CASE WHEN money > 0 THEN useridx ELSE null END)) AS payer_cnt, NVL(SUM(money), 0) AS total_money
                    FROM (
                    	SELECT platform, t1.useridx, TRUNC(createdate) AS today, adflag, money
                    	FROM (
                      	SELECT platform, useridx, $adflag_querystr, createdate
                      	FROM t5_user
                      	WHERE useridx > 20000 AND platform = $os_type AND createdate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $adflag2_querystr
                    	) t1 LEFT JOIN t5_product_order_all t2 ON t1.useridx = t2.useridx AND t2.status = 1 AND DATEDIFF(day, createdate, writedate) < $payday
                    ) total
                    GROUP BY platform, today, adflag
		    	) t2 ON t1.platform = t2.platform AND t1.adflag = t2.adflag AND t1.today = t2.today
		    	GROUP BY t1.today, t1.platform, t1.adflag, t1.spend
		    	ORDER BY 1 DESC, 2 ASC, 3 ASC";    	
    	$new_user_sales_list = $db_redshift->gettotallist($sql);
    	 
    	for($i=0; $i<sizeof($new_user_sales_list); $i++)
    	{
    		$today = $new_user_sales_list[$i]["today"];
    		$platform = $new_user_sales_list[$i]["platform"];
    		$adflag = $new_user_sales_list[$i]["adflag"];
    		$newuser_money = $new_user_sales_list[$i]["total_money"];
    	
    		for($j=0; $j<sizeof($mkt_spend_list); $j++)
    		{
    			$array_today = $mkt_spend_list[$j]["today"];
    			$array_platform = $mkt_spend_list[$j]["platform"];
    			$array_adflag = $mkt_spend_list[$j]["adflag"];
    			 
    			if($today == $array_today && $platform == $array_platform && $adflag == $array_adflag)
    			{
    				$mkt_spend_list[$j]["newuser_money"] = $newuser_money;
    	
    				break;
    			}
    		}
    	}
    	
    	// 비용 + 복귀자 매출
    	$sql = "SELECT t1.today, t1.platform, t1.adflag, t1.spend, SUM(total_money) AS total_money
		    	FROM (
		    		SELECT today, platform, agencyname AS adflag, spend FROM t5_agency_spend_daily WHERE agencyidx > 1000 AND today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type AND spend > 0 $tail
					UNION ALL
					SELECT today, platform, $adset_querystr, SUM(spend) AS spend FROM t5_ad_retention_stats WHERE today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type
					GROUP BY today, platform, adflag
		    	) t1 LEFT JOIN (
                    SELECT platform, today, adflag, NVL(SUM(total_money), 0) AS total_money
                    FROM (
                    	SELECT t1.platform, t1.useridx, TRUNC(retentiondate) AS today, adflag, (multi_value::float * money::float) AS total_money
                    	FROM (
                      	SELECT platform, useridx, $retention_adflag_querystr, 
                      	 (case when leavedays = 0 then 0.000346 when leavedays = 1 then 0.011076 when leavedays = 2 then 0.045827 when leavedays = 3 then 0.082923 when leavedays = 4 then 0.100089
                                when leavedays = 5 then 0.117452 when leavedays = 6 then 0.151834 when leavedays < 14 then 0.17545 when leavedays < 28 then 0.252959 when leavedays < 56 then 0.33947
                                when leavedays < 112 then 0.399101 when leavedays < 365 then 0.513731 else 0.596342 end) as multi_value, leavedays, is_payer, writedate as retentiondate
                      	FROM $retention_table
                      	WHERE useridx > 20000 AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' AND platform = $os_type $retention_adflag_querystr2
                    	) t1 LEFT JOIN t5_product_order_all t2 ON t1.useridx = t2.useridx AND t2.status = 1 AND retentiondate < writedate $pay_tail
                    ) total
                    GROUP BY platform, today, adflag
		    	) t2 ON t1.platform = t2.platform AND t1.adflag = t2.adflag AND t1.today = t2.today
		    	GROUP BY t1.today, t1.platform, t1.adflag, t1.spend
		    	ORDER BY 1 DESC, 2 ASC, 3 ASC ";    	
    	$retention_user_sales_list = $db_redshift->gettotallist($sql);
    	
    	for($i=0; $i<sizeof($retention_user_sales_list); $i++)
    	{
    		$today = $retention_user_sales_list[$i]["today"];
    		$platform = $retention_user_sales_list[$i]["platform"];
    		$adflag = $retention_user_sales_list[$i]["adflag"];
    		$retention_money = $retention_user_sales_list[$i]["total_money"];
    		 
    		for($j=0; $j<sizeof($mkt_spend_list); $j++)
    		{
    			$array_today = $mkt_spend_list[$j]["today"];
    			$array_platform = $mkt_spend_list[$j]["platform"];
    			$array_adflag = $mkt_spend_list[$j]["adflag"];
    	
    			if($today == $array_today && $platform == $array_platform && $adflag == $array_adflag)
    			{
    				$mkt_spend_list[$j]["retention_money"] = $retention_money;
    				 
    				break;
    			}
    		}
    	}
    }
    else
    {	
    	// 세부    	
    	$sql = "SELECT today, platform, adflag, spend, SUM(newuser_cnt) AS newuser_cnt, SUM(leavedays_7) AS leavedays_7, SUM(leavedays_7_28) AS leavedays_7_28, SUM(leavedays_28) AS leavedays_28
		    	FROM (
			    	SELECT t2.createdate AS today, t2.platform, t2.adflag, t1.spend, SUM(reg_cnt) AS newuser_cnt, 0 AS leavedays_7, 0 AS leavedays_7_28, 0 AS leavedays_28
			    	FROM (
				    	SELECT today, platform, agencyname AS adflag, spend 
                        FROM t5_agency_spend_daily 
                        WHERE agencyidx > 1000 AND today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type AND spend > 0 $tail
				    	UNION ALL
				    	SELECT today, platform, $adset2_querystr, SUM(spend) AS spend 
                        FROM t5_ad_retention_stats 
                        WHERE today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type
				    	GROUP BY today, platform, adflag
			    	) t1 RIGHT OUTER JOIN (
				    	SELECT platform, TRUNC(createdate) AS createdate, adflag, COUNT(*) AS reg_cnt
				    	FROM (
					    	SELECT platform, useridx, (CASE WHEN platform = 0 THEN adflag ELSE fbsource END) AS adflag, createdate
					    	FROM t5_user
					    	WHERE useridx > 20000 AND platform = $os_type AND createdate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $adflag2_querystr
				    	) t1
				    	GROUP BY platform, TRUNC(createdate), adflag
			    	) t2 ON t1.platform = t2.platform AND t1.adflag = t2.adflag AND t1.today = t2.createdate
			    	GROUP BY t2.createdate, t2.platform, t2.adflag, t1.spend
			    	UNION ALL
			    	SELECT t2.today, t2.platform, t2.adflag, t1.spend, 0 as newuser_cnt, sum(leavedays_7) AS leavedays_7, sum(leavedays_7_28) AS leavedays_7_28, sum(leavedays_28) AS leavedays_28
			    	FROM (
				    	SELECT today, platform, agencyname AS adflag, spend 
                        FROM t5_agency_spend_daily 
                        WHERE agencyidx > 1000 AND today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type AND spend > 0 $tail
				    	UNION ALL
				    	SELECT today, platform, $adset2_querystr, SUM(spend) AS spend 
                        FROM t5_ad_retention_stats 
                        WHERE today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type
				    	GROUP BY today, platform, adflag
			    	) t1 RIGHT OUTER JOIN (
				    	SELECT platform, TRUNC(writedate) AS today, adflag, SUM(leavedays_7) AS leavedays_7, SUM(leavedays_7_28) AS leavedays_7_28, SUM(leavedays_28) AS leavedays_28
				    	FROM (
					    	SELECT platform, useridx, adflag, leavedays, is_payer, writedate,
					    	(CASE WHEN leavedays < 7 THEN 1 ELSE 0 end) AS leavedays_7,
					    	(CASE WHEN leavedays >= 7 AND leavedays < 28 THEN 1 ELSE 0 END) AS leavedays_7_28,
					    	(CASE WHEN leavedays >= 28 THEN 1 ELSE 0 END) AS leavedays_28
					    	FROM $retention_table
					    	WHERE useridx > 20000 AND platform = $os_type AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $retention_adflag_querystr2
				    	) t1
				    	GROUP BY platform, TRUNC(writedate), adflag
			    	) t2 ON t1.platform = t2.platform AND t1.adflag = t2.adflag AND t1.today = t2.today
			    	GROUP BY t2.today, t2.platform, t2.adflag, t1.spend
		    	) t3
    			GROUP BY today, platform, adflag, spend
                HAVING spend > 0
    			ORDER BY 1 DESC, 2 ASC, 3 ASC ";    	
    	write_log($sql);
    	$mkt_spend_list = $db_redshift->gettotallist($sql);
    	
    	// 비용 + 신규 매출
    	$sql = "SELECT t2.today, t2.platform, t2.adflag, t1.spend, SUM(total_money) AS total_money
		    	FROM (
		    		SELECT today, platform, agencyname AS adflag, spend FROM t5_agency_spend_daily WHERE agencyidx > 1000 AND today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type AND spend > 0 $tail
					UNION ALL
					SELECT today, platform, $adset2_querystr, SUM(spend) AS spemnd FROM t5_ad_retention_stats WHERE today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type
					GROUP BY today, platform, adflag
		    	) t1 RIGHT OUTER JOIN (
                    SELECT platform, today, adflag, NVL(SUM(money), 0) AS total_money
                    FROM (
                    	SELECT platform, t1.useridx, TRUNC(createdate) AS today, adflag, money
                    	FROM (
                          	SELECT platform, useridx, (CASE WHEN platform = 0 THEN adflag ELSE fbsource END) AS adflag, createdate
                          	FROM t5_user
                          	WHERE useridx > 20000 AND platform = $os_type AND createdate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $adflag2_querystr
                    	) t1 LEFT JOIN t5_product_order_all t2 ON t1.useridx = t2.useridx AND t2.status = 1 AND DATEDIFF(day, createdate, writedate) < $payday
                    ) total
                    GROUP BY platform, today, adflag
		    	) t2 ON t1.platform = t2.platform AND t1.adflag = t2.adflag AND t1.today = t2.today
		    	GROUP BY t2.today, t2.platform, t2.adflag, t1.spend
		    	ORDER BY 1 DESC, 2 ASC, 3 ASC";    	
    	$new_user_sales_list = $db_redshift->gettotallist($sql);
    	 
    	for($i=0; $i<sizeof($new_user_sales_list); $i++)
    	{
    		$today = $new_user_sales_list[$i]["today"];
    		$platform = $new_user_sales_list[$i]["platform"];
    		$adflag = $new_user_sales_list[$i]["adflag"];
    		$newuser_money = $new_user_sales_list[$i]["total_money"];
    	
    		for($j=0; $j<sizeof($mkt_spend_list); $j++)
    		{
    			$array_today = $mkt_spend_list[$j]["today"];
    			$array_platform = $mkt_spend_list[$j]["platform"];
    			$array_adflag = $mkt_spend_list[$j]["adflag"];
    			 
    			if($today == $array_today && $platform == $array_platform && $adflag == $array_adflag)
    			{
    				$mkt_spend_list[$j]["newuser_money"] = $newuser_money;
    	
    				break;
    			}
    		}
    	}
    	
    	// 비용 + 복귀자 매출
    	$sql = "SELECT t2.today, t2.platform, t2.adflag, t1.spend, SUM(total_money) AS total_money
		    	FROM (
		    		SELECT today, platform, agencyname AS adflag, spend FROM t5_agency_spend_daily WHERE agencyidx > 1000 AND today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type AND spend > 0 $tail
					UNION ALL
					SELECT today, platform, $adset2_querystr, SUM(spend) AS spend FROM t5_ad_retention_stats WHERE today BETWEEN '$startdate' AND '$enddate' AND platform = $os_type
					GROUP BY today, platform, adflag
		    	) t1 RIGHT OUTER JOIN (
                    SELECT platform, today, adflag, NVL(SUM(total_money), 0) AS total_money
                    FROM (
                    	SELECT t1.platform, t1.useridx, TRUNC(retentiondate) AS today, adflag, (multi_value::float * money::float) AS total_money
                    	FROM (
                      	SELECT platform, useridx, adflag, 
                      	 (case when leavedays = 0 then 0.000346 when leavedays = 1 then 0.011076 when leavedays = 2 then 0.045827 when leavedays = 3 then 0.082923 when leavedays = 4 then 0.100089
                                when leavedays = 5 then 0.117452 when leavedays = 6 then 0.151834 when leavedays < 14 then 0.17545 when leavedays < 28 then 0.252959 when leavedays < 56 then 0.33947
                                when leavedays < 112 then 0.399101 when leavedays < 365 then 0.513731 else 0.596342 end) as multi_value, leavedays, is_payer, writedate as retentiondate
                      	FROM $retention_table
                      	WHERE useridx > 20000 AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' AND platform = $os_type $retention_adflag_querystr2
                    	) t1 LEFT JOIN t5_product_order_all t2 ON t1.useridx = t2.useridx AND t2.status = 1 AND retentiondate < writedate $pay_tail
                    ) total
                    GROUP BY platform, today, adflag
		    	) t2 ON t1.platform = t2.platform AND t1.adflag = t2.adflag AND t1.today = t2.today
		    	GROUP BY t2.today, t2.platform, t2.adflag, t1.spend
		    	ORDER BY 1 DESC, 2 ASC, 3 ASC ";    	
    	$retention_user_sales_list = $db_redshift->gettotallist($sql);
    	
    	for($i=0; $i<sizeof($retention_user_sales_list); $i++)
    	{
    		$today = $retention_user_sales_list[$i]["today"];
    		$platform = $retention_user_sales_list[$i]["platform"];
    		$adflag = $retention_user_sales_list[$i]["adflag"];
    		$retention_money = $retention_user_sales_list[$i]["total_money"];
    		 
    		for($j=0; $j<sizeof($mkt_spend_list); $j++)
    		{
    			$array_today = $mkt_spend_list[$j]["today"];
    			$array_platform = $mkt_spend_list[$j]["platform"];
    			$array_adflag = $mkt_spend_list[$j]["adflag"];
    	
    			if($today == $array_today && $platform == $array_platform && $adflag == $array_adflag)
    			{
    				$mkt_spend_list[$j]["retention_money"] = $retention_money;

    				break;
    			}
    		}
    	}
    }
    
    
    $db_redshift->end();
    $db_other->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#startdate").datepicker({ });
	});
	
	$(function() {
		$("#enddate").datepicker({ });
	});
	
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.startdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    } 
	
	    if (search_form.enddate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    } 
	
	    search_form.submit();
	}

	function change_os_type(type)
	{
		var search_form = document.search_form;

		var facebook = document.getElementById("type_facebook");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			facebook.className 	= "btn_schedule_select";
			ios.className 		= "btn_schedule";
			android.className 	= "btn_schedule";
		}
		else if (type == "1")
		{
			facebook.className 	= "btn_schedule";
			ios.className 		= "btn_schedule_select";
			android.className 	= "btn_schedule";
		}
		else if (type == "2")
		{
			facebook.className 	= "btn_schedule";
			ios.className 		= "btn_schedule";
			android.className 	= "btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Retention 통계</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />				
				
				<span class="search_lbl ml20">Adflag&nbsp;&nbsp;&nbsp;</span>
				<select name="search_data" id="search_data">
					<option value="0" <?= ($search_data == "0") ? "selected" : "" ?>>Facebook 전체</option>
					<option value="1" <?= ($search_data == "1") ? "selected" : "" ?>>세부</option>
				</select>
				
				<span class="search_lbl ml20">유입일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:75px"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" /> ~
				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:75px" maxlength="10"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
				&nbsp;&nbsp;&nbsp;&nbsp;
				복귀 후
				<input type="text" class="search_text" id="payday" name="payday" style="width:30px" value="<?= $payday ?>" onkeypress="search_press(event); return checknum();" />일 이내 결제
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	
	<div class="search_result">
		<span><?= $startdate ?> ~ <?= $enddate ?></span> 통계입니다
	</div>
	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">
		<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Facebook" id="type_facebook" onclick="change_os_type('0')"    />
		<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
		<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
	</span>
	<br/><br/>
	
		<table class="tbl_list_basic1">
		<colgroup>
			<col width="">
			<col width="">
			<col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">
			<col width="">
            <col width="">
		</colgroup>
        <thead>
        	<tr>
	         	<th rowspan=2>유입일</th>
				<th rowspan=2>adflag</th>
				<th rowspan=2>비용</th>
				<th rowspan=2>신규수</th>
				<th colspan=3>복귀자수</th>
				<th rowspan=2>신규+복귀자수</th>
				<th rowspan=2>CPI</th>
				<th rowspan=2>신규매출</th>
				<th rowspan=2>복귀자매출</th>
				<th rowspan=2>신규+복귀자매출</th>
				<th rowspan=2>ROI</th>
			</tr>
			<tr>
				<th>7일 미만</th>
				<th>7~28일미만</th>
				<th>28일이상</th>
			</tr>
        </thead>
        <tbody>
<?	
		for($i=0; $i<sizeof($mkt_spend_list); $i++)
		{
			$today = $mkt_spend_list[$i]["today"];
			$platform = $mkt_spend_list[$i]["platform"];
			$adflag = $mkt_spend_list[$i]["adflag"];
			$spend = $mkt_spend_list[$i]["spend"];
			$newuser_cnt = $mkt_spend_list[$i]["newuser_cnt"];
			$leavedays_7 = $mkt_spend_list[$i]["leavedays_7"];
			$leavedays_7_28 = $mkt_spend_list[$i]["leavedays_7_28"];
			$leavedays_28 = $mkt_spend_list[$i]["leavedays_28"];
			$newuser_money = $mkt_spend_list[$i]["newuser_money"];
			$retention_money = $mkt_spend_list[$i]["retention_money"];
			
			$total_usercnt = $newuser_cnt + $leavedays_7 + $leavedays_7_28 + $leavedays_28;
			$total_money = $newuser_money + $retention_money;
			
			$roi = ($spend == 0) ? 0 : round($total_money/$spend*100, 2);
			$cpi = ($spend == 0 || $total_usercnt == 0) ? 0 : round($spend/$total_usercnt, 2);
			
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
	            <td class="tdc"><?= $today ?></td>
	            <td class="tdc"><?= $adflag ?></td>
	            <td class="tdc">$<?=number_format($spend,2)?></td>
	            <td class="tdc"><?= number_format($newuser_cnt) ?></td>
	            <td class="tdc"><?= number_format($leavedays_7) ?></td>
	            <td class="tdc"><?= number_format($leavedays_7_28) ?></td>
	            <td class="tdc"><?= number_format($leavedays_28) ?></td>
	            <td class="tdc"><?= number_format($total_usercnt) ?></td>
	            <td class="tdc">$<?=number_format($cpi,2)?></td>
	            <td class="tdc">$<?=number_format($newuser_money,2)?></td>
	            <td class="tdc">$<?=number_format($retention_money,2)?></td>
	            <td class="tdc">$<?=number_format($total_money,2)?></td>
	            <td class="tdc"><?=number_format($roi, 2)?>%</td>
	        </tr>
<?
		}
	
    if(sizeof($mkt_spend_list) == 0)
    {
?>
   			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   				<td class="tdc" colspan="13">검색 결과가 없습니다.</td>
   			</tr>
<?
   	}
?>

        </tbody>
	</table>
</div>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>