<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	check_login_layer();
	
	$db_main2 = new CDatabase_Main2();
	
	$max_today = $db_main2->getvalue("SELECT MAX(today) FROM tbl_marketing_stat_daily");
	
	$today = ($_GET["today"] == "") ? $max_today : $_GET["today"];
	$startdate = ($_GET["startdate"] == "") ? date("Y-m-d", strtotime("-29 day")) : $_GET["startdate"];
	$enddate = ($_GET["enddate"] == "") ? $max_today : $_GET["enddate"];
	
	$term = ($_GET["term"] == "") ? "0" : $_GET["term"];
	$search_subtype = ($_GET["subtype"] == "") ? "1" : $_GET["subtype"];
	
	if ($term != "ALL" && $term != "0" && $term != "1" && $term != "2" && $term != "3")
		error_back("잘못된 접근입니다.");
	
	$platform = $term;
	
	$sql = "SELECT today, REPLACE(adflag, '_viral', '') AS adflag, TYPE, subtype, platform, SUM(spend) AS spend,
			SUM(newuser) AS newuser, SUM(newuser_payer) AS newuser_payer, SUM(newuser_money) AS newuser_money, SUM(newuser_noplay) AS newuser_noplay,
			SUM(reuser) AS reuser, AVG(reuser_leavedays) AS reuser_leavedays, SUM(reuser_payer) AS reuser_payer, AVG(reuser_payer_leavedays) AS reuser_payer_leavedays, SUM(reuser_money) AS reuser_money,
			SUM(lessthan28_user) AS lessthan28_user, AVG(lessthan28_user_leavedays) AS lessthan28_user_leavedays
			FROM tbl_marketing_stat_daily
			WHERE today = '$today' AND TYPE = 1 AND subtype = $search_subtype AND platform = $platform
			GROUP BY REPLACE(adflag, '_viral', '')
			ORDER BY spend DESC, adflag ASC";
	$newuser_week_stats = $db_main2->gettotallist($sql);
	
	$date_pointer = $enddate;
	
	for($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
	{
		$date_list[$i] = $date_pointer;
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	$db_main2->end();
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9, IE=8, IE=7" />
<meta name="viewport" content="user-scalable=yes, initial-scale=1.0, minimum-scale=0.25,maximum-scale=1.0, width=device-width" />
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
	function tab_change(tab)
	{
		var search_form = document.search_form;
		search_form.tab.value = tab;
		search_form.submit();
	}
	
	function change_term(term)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("term_web");
		var ios = document.getElementById("term_ios");
		var android = document.getElementById("term_android");
		var amazon = document.getElementById("term_amazon");
		
		search_form.term.value = term;
		
		if (term == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (term == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
	
	function change_week_term(term)
	{
		var search_form = document.search_form;
		
		var term_1 = document.getElementById("term_1");
		var term_2 = document.getElementById("term_2");
		var term_3 = document.getElementById("term_3");
		var term_4 = document.getElementById("term_4");
		
		search_form.week_term.value = term;
		
		if (term == "1")
		{
			term_1.className="btn_schedule_select";
			term_2.className="btn_schedule";
			term_3.className="btn_schedule";
			term_4.className="btn_schedule";
		}
		else if (term == "2")
		{
			term_1.className="btn_schedule";
			term_2.className="btn_schedule_select";
			term_3.className="btn_schedule";
			term_4.className="btn_schedule";
		}
		else if (term == "3")
		{
			term_1.className="btn_schedule";
			term_2.className="btn_schedule";
			term_3.className="btn_schedule_select";
			term_4.className="btn_schedule";
		}
		else if (term == "4")
		{
			term_1.className="btn_schedule";
			term_2.className="btn_schedule";
			term_3.className="btn_schedule";
			term_4.className="btn_schedule_select";
		}
	
		search_form.submit();
	}

	$(function() {
		$("#today").datepicker({});
		$("#startdate").datepicker({});
		$("#enddate").datepicker({});
		$("#appsflyer_startdate").datepicker({});
		$("#appsflyer_enddate").datepicker({});
		$("#duc_startdate").datepicker({});
		$("#duc_enddate").datepicker({});
	});

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
		
	function search()
	{
	    var search_form = document.search_form;
	    
	    if (search_form.today.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.today.focus();
	        return;
	    }

	    search_form.submit();
	}

	function layer_close()
    {
        window.parent.close_layer_popup("<?= $_GET["layer_no"] ?>");
    }
</script>

<html>
<body class="layer_body" onload="try { window_onload() } catch(e) {}">
<div id="layer_wrap">   
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
		<input type="hidden" name="term" id="term" value="<?= $term ?>" />
		<input type="hidden" name="week_term" id="week_term" value="<?= $week_term ?>" />
		<div class="search_box" style="float:left">
			날짜 : &nbsp;&nbsp;<input type="text" class="search_text" id="today" name="today" value="<?= $today ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" />
			
			&nbsp;&nbsp;
			
			<input type="button" class="<?= ($term == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="term_web" onclick="change_term('0')" />
			<input type="button" class="<?= ($term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_term('1')" />
			<input type="button" class="<?= ($term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_term('2')"    />
			<input type="button" class="<?= ($term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_term('3')"    />
			
			&nbsp;&nbsp;
			
			기간 : 
			<select name="subtype" id="subtype">
				<option value="1" <?= ($search_subtype == "1" || $search_subtype == "") ? "selected" : "" ?>>최근 1주</option>
				<option value="2" <?= ($search_subtype == "2") ? "selected" : "" ?>>최근 2주</option>
				<option value="3" <?= ($search_subtype == "3") ? "selected" : "" ?>>최근 3주</option>
				<option value="4" <?= ($search_subtype == "4") ? "selected" : "" ?>>최근 4주</option>
		    </select>
		    
			<input type="button" class="btn_search" value="검색" onclick="search()" />
		</div>
	</form>
	
	<br/><br/><br/>
	
<?
	$week = date("Y-m-d", strtotime($today."-".((7 * $search_subtype) - 1)." days"))." ~ ".$today;
?>

	<text style="font-weight:bold;font-size:13px;color:#ff7171;"><?= $week ?></text> <text style="font-size:13px;">통계 입니다.</text>
	
	<!-- Web -->
	<table class="tbl_list_basic1" style="width:1300px;">
		<colgroup>
			<col width="250">
			<col width="70">
			
			<col width="55">
			<col width="60">
			<col width="80">
			<col width="55">
			<col width="65">
			<col width="50">
			<col width="85">
			
			<col width="65">
			<col width="85">
			<col width="70">
			<col width="90">
			<col width="70">

			<col width="65">
        </colgroup>
        
        <thead>
        	<tr>
        		<th class="tdc" style="font-size: 10pt; border-right: 1px solid #dbdbdb;" rowspan="2">Adflag</th>
        		<th class="tdc" style="font-size: 10pt; border-right: 1px solid #dbdbdb;" rowspan="2">광고비용 </th>
        		<th class="tdc" style="font-size: 10pt; border-right: 1px solid #dbdbdb;" colspan="7">신규</th>
        		<th class="tdc" style="font-size: 10pt; border-right: 1px solid #dbdbdb;" colspan="5">복귀</th>
        		<th class="tdc" style="font-size: 10pt; border-right: 1px solid #dbdbdb;" rowspan="2">ROI</th>
        	</tr>
        	
        	<tr>
            	<th class="tdr" style="font-size: 10pt;">유입수</th>
            	<th class="tdr" style="font-size: 10pt;">CPI</th>
            	<th class="tdc" style="font-size: 10pt;">미게임율</th>
            	<th class="tdr" style="font-size: 10pt;">결제자수</th>
            	<th class="tdc" style="font-size: 10pt;">PUR</th>
            	<th class="tdr" style="font-size: 10pt;">ARPU</th>
             	<th class="tdr" style="font-size: 10pt; border-right: 1px solid #dbdbdb;">매출</th>
               	
               	<th class="tdr" style="font-size: 10pt;">복귀자수</th>
               	<th class="tdr" style="font-size: 10pt;">평균복귀일</th>
               	<th class="tdr" style="font-size: 10pt;">결제자수</th>
               	<th class="tdr" style="font-size: 10pt;">결제자<br/>평균 복귀일</th>
               	<th class="tdr" style="font-size: 10pt; border-right: 1px solid #dbdbdb;">매출</th>
         	</tr>
        </thead>
        
        <tbody>
<?
	for($i=0; $i<sizeof($newuser_week_stats); $i++)
	{
		$subtype = $newuser_week_stats[$i]["subtype"];
		$adflag = $newuser_week_stats[$i]["adflag"];
		$spend = $newuser_week_stats[$i]["spend"];
		
		$newuser = $newuser_week_stats[$i]["newuser"];
		$newuser_money = $newuser_week_stats[$i]["newuser_money"];
		$newuser_noplay = $newuser_week_stats[$i]["newuser_noplay"];
		$noplay_rate = ($newuser == 0) ? 0 : number_format(($newuser_noplay / $newuser) * 100, 2);
		$newuser_payer = $newuser_week_stats[$i]["newuser_payer"];
		$pur = ($newuser_payer == 0) ? 0 : number_format(($newuser_payer / $newuser) * 100, 2);
		$arpu = ($newuser_money == 0) ? 0 : number_format(($newuser_money / $newuser), 2);
		$cpi = ($newuser == 0) ? 0 : number_format($spend / $newuser, 2);
		
		$reuser = $newuser_week_stats[$i]["reuser"];
		$reuser_leavedays = $newuser_week_stats[$i]["reuser_leavedays"];
		$reuser_payer = $newuser_week_stats[$i]["reuser_payer"];
		$reuser_payer_leavedays = $newuser_week_stats[$i]["reuser_payer_leavedays"];
		$reuser_money = $newuser_week_stats[$i]["reuser_money"];
		$roi = ($spend == 0) ? 0 : number_format((($newuser_money + $reuser_money) / $spend) * 100, 2);

		$week = date("Y-m-d", strtotime($today."-".((7 * $subtype) - 1)." days"))." ~ ".$today;
?>
			<tr>
				<td class="tdc point" style="font-size: 10pt; border-right: 1px solid #dbdbdb;"><?= $adflag ?></td>
				<td class="tdc" style="font-size: 10pt; border-right: 1px solid #dbdbdb;"><?= ($spend == 0) ? "-" : "$ ".number_format($spend) ?></td>
				<td class="tdr" style="font-size: 10pt;"><?= number_format($newuser) ?></td>
				<td class="tdr" style="font-size: 10pt;"><?= ($cpi == 0) ? "-" : "$ ".$cpi ?></td>
				<td class="tdc" style="font-size: 10pt;"><?= ($noplay_rate == 0) ? "-" : $noplay_rate." %" ?></td>
				<td class="tdr" style="font-size: 10pt;"><?= ($newuser_payer == 0) ? "-" : number_format($newuser_payer) ?></td>
				<td class="tdr" style="font-size: 10pt;"><?= ($pur == 0) ? "-" : $pur." %" ?></td>
				<td class="tdr" style="font-size: 10pt;"><?= ($arpu == 0) ? "-" : $arpu ?></td>
				<td class="tdr" style="font-size: 10pt; border-right: 1px solid #dbdbdb;"><?= ($newuser_money == 0) ? "-" : "$ ".number_format($newuser_money) ?></td>
				
				<td class="tdr" style="font-size: 10pt;"><?= ($reuser == 0) ? "-" : number_format($reuser) ?></td>
				<td class="tdr" style="font-size: 10pt;"><?= ($reuser_leavedays == 0) ? "-" : number_format($reuser_leavedays)." 일" ?></td>
				<td class="tdr" style="font-size: 10pt;"><?= ($reuser_payer == 0) ? "-" : number_format($reuser_payer) ?></td>
				<td class="tdr" style="font-size: 10pt;"><?= ($reuser_payer_leavedays == 0) ? "-" : number_format($reuser_payer_leavedays)." 일" ?></td>
				<td class="tdr" style="font-size: 10pt; border-right: 1px solid #dbdbdb;"><?= ($reuser_money == 0) ? "-" : "$ ".number_format($reuser_money) ?></td>

				<td class="tdc" style="font-size: 10pt; border-right: 1px solid #dbdbdb;"><?= ($roi == 0) ? "-" : $roi." %" ?></td>
			</tr>
<?
	}
?>
        </tbody>
	</table>
	</div>
</body>
</html>