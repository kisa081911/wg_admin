<?
    $top_menu = "marketing";
    $sub_menu = "adflag_fbretention_mobile_stats";
    
    include_once("../common/dbconnect/db_util_redshift.inc.php");
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $os_term = ($_GET["term"] == "") ? "0" : $_GET["term"];
    
    if ($os_term != "0" && $os_term != "1" && $os_term != "2" && $os_term != "3")
    	error_back("잘못된 접근입니다.");
    
    if($os_term == 1)
    	$platform_sql = "AND platform = 1";
    else if($os_term == 2)
    	$platform_sql = "AND platform = 2";
    else if($os_term == 3)
    	$platform_sql = "AND platform = 3";
    	
    $search_start_returndate = $_GET["start_returndate"];
    $search_end_returndate = $_GET["end_returndate"];
    $search_start_orderdate = $_GET["start_orderdate"];
    $search_end_orderdate = $_GET["end_orderdate"];
    $search_payday = $_GET["payday"];
	$issearch = $_GET["issearch"];
	$select_time = $_GET["select_time"];
	
	$search_main_adflag = $_GET["adflag_list"];
	$search_sub_adflag_1 = $_GET["cr_list"];
	$search_sub_adflag_2 = $_GET["fb_list"];
	$search_sub_adflag_3 = $_GET["gn_list"];
	$search_sub_adflag_4 = $_GET["etc_list"];
	
	if($search_main_adflag == 1)
	{
		if($search_sub_adflag_1 == "0")
			$adflag_sql = " AND adflag LIKE 'cr_%' ";
		else
			$adflag_sql = " AND adflag = '$search_sub_adflag_1' ";
	}
	else if($search_main_adflag == 2)
	{
		if($search_sub_adflag_2 == "0")
			$adflag_sql = " AND adflag LIKE 'm_retention%' ";
		else
			$adflag_sql = " AND adflag = '$search_sub_adflag_2' ";
	}
	else if($search_main_adflag == 3)
	{
		if($search_sub_adflag_3 == "0")
			$adflag_sql = " AND adflag LIKE 'gamenoti%' ";
		else
			$adflag_sql = " AND adflag = '$search_sub_adflag_3' ";
	}
	else if($search_main_adflag == 4)
	{
		if($search_sub_adflag_4 == "0")
			$adflag_sql = " AND adflag NOT LIKE 'cr_%' AND adflag NOT LIKE 'm_retention%' AND adflag NOT LIKE 'gamenoti%' AND adflag <> '' ";
		else
			$adflag_sql = " AND adflag = '$search_sub_adflag_4' ";
	}
	else
		$adflag_sql = " AND adflag <> '' ";
	
	if($search_payday != "" && ($search_start_orderdate != "" || $search_end_orderdate != ""))
		error_back("검색값이 잘못되었습니다.");
		
	if ($issearch == "")
	{
		$search_start_returndate  = date("Y-m-d", time() - 60 * 60 * 24 * 6);
		$search_end_returndate  = date("Y-m-d", time() - 60 * 60);
		
		$search_start_orderdate  = date("Y-m-d", time() - 60 * 60 * 24 * 6);
		$search_end_orderdate  = date("Y-m-d", time() - 60 * 60);
	}
	
	if ($select_time == "")
	{
		$select_time = "marketing";
	}
	
	function get_stat($summarylist, $adflag, $today, $property)
	{
		$stat = 0;
		
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if ($summarylist[$i]["today"] == $today)
			{
				if($summarylist[$i]["adflag"] == $adflag)
					$stat += $summarylist[$i][$property];
			}
		}	
		
		return $stat;
	}
	
	function get_total_stat($summarylist, $adflag, $property)
	{
		$stat = 0;
	
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if($summarylist[$i]["adflag"] == $adflag)
				$stat += $summarylist[$i][$property];
		}
		
		return $stat;
	}
	
	$db_redshift = new CDatabase_Redshift();
	$db_main2 = new CDatabase_Main2();
	
	if($select_time == "marketing")
	{
		$sdate = date('Y-m-d H:i:s', strtotime($search_start_returndate) - 60 * 60 * 9); //(마케팅)-UTC
		$tail = " WHERE writedate BETWEEN '$sdate' AND '$search_end_returndate 14:59:59' ";
		 
	}
	else if($select_time == "server")
	{
		$sdate = date('Y-m-d H:i:s', strtotime($search_start_returndate)); // 서버 시간
		$tail = " WHERE writedate BETWEEN '$sdate' AND '$search_end_returndate 23:59:59' ";
	}
	
	$adflag_list = array("cr_", "m_retention", "gamenoti");
	
	for($i=0; $i<sizeof($adflag_list); $i++)
	{
		$sql = "SELECT DISTINCT(adflag)
				FROM t5_user_retention_mobile_log
				$tail AND adflag LIKE '$adflag_list[$i]%'
				ORDER BY adflag ASC ";
		
		if($adflag_list[$i] == "cr_")
			$cr_list = $db_redshift->gettotallist($sql);
		else if($adflag_list[$i] == "m_retention")
			$fb_list = $db_redshift->gettotallist($sql);
		else if($adflag_list[$i] == "gamenoti")
			$gn_list = $db_redshift->gettotallist($sql);
	}
	
	$sql = "SELECT DISTINCT(adflag)
			FROM t5_user_retention_mobile_log
			$tail AND adflag NOT LIKE 'cr_%' AND adflag NOT LIKE 'm_retention%' AND adflag NOT LIKE 'gamenoti%' AND adflag <> ''
			ORDER BY adflag ASC ";
	$etc_list = $db_redshift->gettotallist($sql);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_returndate").datepicker({ });
	});
	
	$(function() {
	    $("#end_returndate").datepicker({ });
	});
	
	$(function() {
	    $("#start_orderdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_orderdate").datepicker({ });
	});
	
	function adflag_change(type)
	{
		var cr_list = document.getElementById("cr_list");
		var fb_list = document.getElementById("fb_list");
		var gn_list = document.getElementById("gn_list");
		var etc_list = document.getElementById("etc_list");
	
		if(type == 1)
		{
			cr_list.style.display = "";
			fb_list.style.display = "none";
			gn_list.style.display = "none";
			etc_list.style.display = "none";
		}
		else if(type == 2)
		{
			cr_list.style.display = "none";
			fb_list.style.display = "";
			gn_list.style.display = "none";
			etc_list.style.display = "none";
		}
		else if(type == 3)
		{
			cr_list.style.display = "none";
			fb_list.style.display = "none";
			gn_list.style.display = "";
			etc_list.style.display = "none";
		}
		else if(type == 4)
		{
			cr_list.style.display = "none";
			fb_list.style.display = "none";
			gn_list.style.display = "none";
			etc_list.style.display = "";
		}
		else
		{
			cr_list.style.display = "none";
			fb_list.style.display = "none";
			gn_list.style.display = "none";
			etc_list.style.display = "none";
		}
	}

	function change_term(term)
	{
		var search_form = document.search_form;
		
		var all = document.getElementById("term_all");
		var ios = document.getElementById("term_ios");
		var android = document.getElementById("term_android");
		var amazon = document.getElementById("term_amazon");
		
		search_form.term.value = term;
		
		if (term == "0")
		{
			all.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "1")
		{
			all.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "2")
		{
			all.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (term == "3")
		{
			all.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>

        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; Retention 복귀현황 - 통합 (Mobile)</div>
                
            </div>
            <div class="search_result"><span><?= $search_start_returndate ?></span> ~ <span><?= $search_end_returndate ?></span> 통계입니다</div>
            <!-- //title_warp -->
            
            <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="adflag_fbretention_mobile_stats.php">
            <input type="hidden" name="term" id="term" value="<?= $os_term ?>" />
            <div class="detail_search_wrap" style="height:60px;">
            	<div class="floatr">
            		<input type="hidden" name="issearch" value="1">
	            	<span class="search_lbl">시간 기준 :</span> 				
					<input type="radio" value="marketing" name="select_time" id="select_time" <?= ($select_time == "" || $select_time == "marketing") ? "checked=\"true\"" : ""?>/> 마케팅 
					<input type="radio" value="server" name="select_time" id="select_time" <?= ($select_time == "server") ? "checked=\"true\"" : ""?>/> 서버
					&nbsp;&nbsp;
					<span class="search_lbl">복귀날짜 : </span>
	                <input type="text" class="search_text" id="start_returndate" name="start_returndate" value="<?= $search_start_returndate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
	                <input type="text" class="search_text" id="end_returndate" name="end_returndate" value="<?= $search_end_returndate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
	                &nbsp;&nbsp;
	                <span class="search_lbl">결제액 기준 : 복귀 후 </span> <input type="text" class="search_text" id="payday" name="payday" style="width:60px" value="<?= $search_payday ?>" onkeypress="search_press(event); return checknum();" /><span class="search_lbl">일 이내 결제</span>
	                <input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
	            </div>
	            
	            <div class="clear" style="height:9px;"></div>
	            
                <div class="floatr">
                 <span class="search_lbl">OS :</span>
                <input type="button" class="<?= ($os_term == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="전체" id="term_all" onclick="change_term('0')" />
				<input type="button" class="<?= ($os_term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_term('1')" />
				<input type="button" class="<?= ($os_term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_term('2')"    />
				<input type="button" class="<?= ($os_term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_term('3')"    />
				&nbsp;&nbsp;
                <span class="search_lbl">유입경로 :</span>
	                <select name="adflag_list" id="adflag_list" onchange="adflag_change(this.value)">
	                	<option value="0" <?= ($search_main_adflag == "0") ? "selected" : "" ?>>전체</option>
<?
	if(sizeof($cr_list) > 0)
	{
?>
                		<option value="1" <?= ($search_main_adflag == "1") ? "selected" : "" ?>>크로스 프로모션</option>
<?
	}
	if(sizeof($fb_list) > 0)
	{
?>
                		<option value="2" <?= ($search_main_adflag == "2") ? "selected" : "" ?>>페이스북 리텐션</option>
<?
	}
	if(sizeof($gn_list) > 0)
	{
?>
                		<option value="3" <?= ($search_main_adflag == "3") ? "selected" : "" ?>>ATU</option>
<?
	}
	if(sizeof($etc_list) > 0)
	{
?>
						<option value="4" <?= ($search_main_adflag == "4") ? "selected" : "" ?>>ETC</option>
<?
	}
?>
               		</select>
                
					<select name="cr_list" id="cr_list" <?= ($search_main_adflag == "1" && $search_sub_adflag_1 != "") ? "" : "style='display:none'" ?>>
						<option value="0" >전체</option>
<?
	for($i=0; $i<sizeof($cr_list); $i++)
	{
?>
						<option value="<?= $cr_list[$i]["adflag"] ?>" <?= ($search_sub_adflag_1 == $cr_list[$i]["adflag"]) ? "selected" : "" ?>><?= $cr_list[$i]["adflag"] ?></option>
<?
	}
?>
					</select>

					<select name="fb_list" id="fb_list" <?= ($search_main_adflag == "2" && $search_sub_adflag_2 != "") ? "" : "style='display:none'" ?>>
						<option value="0">전체</option>
<?
	for($i=0; $i<sizeof($fb_list); $i++)
	{
?>
						<option value="<?= $fb_list[$i]["adflag"] ?>" <?= ($search_sub_adflag_2 == $fb_list[$i]["adflag"]) ? "selected" : "" ?>><?= $fb_list[$i]["adflag"] ?></option>
<?
	}
?>
					</select>
				
					<select name="gn_list" id="gn_list" <?= ($search_main_adflag == "3" && $search_sub_adflag_3 != "") ? "" : "style='display:none'" ?>>
						<option value="0">전체</option>
<?
	for($i=0; $i<sizeof($gn_list); $i++)
	{
?>
						<option value="<?= $gn_list[$i]["adflag"] ?>" <?= ($search_sub_adflag_3 == $gn_list[$i]["adflag"]) ? "selected" : "" ?>><?= $gn_list[$i]["adflag"] ?></option>
<?
	}
?>
					</select>
				
					<select name="etc_list" id="etc_list" <?= ($search_main_adflag == "4" && $search_sub_adflag_4 != "") ? "" : "style='display:none'" ?>>
						<option value="0">전체</option>
<?
	for($i=0; $i<sizeof($etc_list); $i++)
	{
?>
						<option value="<?= $etc_list[$i]["adflag"] ?>" <?= ($search_sub_adflag_4 == $etc_list[$i]["adflag"]) ? "selected" : "" ?>><?= $etc_list[$i]["adflag"] ?></option>
<?
	}
?>
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" class="search_text" id="start_orderdate" name="start_orderdate" value="<?= $search_start_orderdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
	                <input type="text" class="search_text" id="end_orderdate" name="end_orderdate" value="<?= $search_end_orderdate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
	                <span class="search_lbl">기간내 결제</span>
				
					<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
				</div>                
            </div>
            </form>
            
            <div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
                <col width=""> 
                <col width="">
                <col width="">
            </colgroup>
            <thead>
            <tr>
                <th>유입일</th>
                <th class="tdr">유입경로</th>
                <th class="tdr">복귀자수</th>
                <th class="tdr">평균복귀일</th>
                <th class="tdr">결제회원수</th>
                <th class="tdr">총결제횟수</th>
                <th class="tdr">총결제금액<br>(평균)</th>
                <th class="tdr">비용</th>
            </tr>
            </thead>
            <tbody>
<?
    

    if($select_time == "marketing")
    {
    	$sdate = date('Y-m-d H:i:s', strtotime($search_start_returndate) - 60 * 60 * 9); //(마케팅)-UTC
    	$tail =  "WHERE writedate BETWEEN '$sdate' AND '$search_end_returndate 14:59:59' AND leavedays >= 7 AND adflag <> '' $adflag_sql $platform_sql";
    	
    }
    else if($select_time == "server")
    {
    	$sdate = date('Y-m-d H:i:s', strtotime($search_start_returndate)); // 서버 시간
    	$tail =  "WHERE writedate BETWEEN '$sdate' AND '$search_end_returndate 23:59:59' AND leavedays >= 7 AND adflag <> '' $adflag_sql $platform_sql";
    }
	
    if($search_payday != "")
    {
    	$sql = "SELECT t1.today, t1.adflag, t1.leavedays, nvl(t1.return_count,0) AS return_count, nvl(t2.buy_user_count,0) AS buy_user_count, nvl(t2.buy_count, 0) AS buy_count, nvl(t2.money, 0) AS money
    			FROM
    			(
    				SELECT date(writedate) AS today, adflag, COUNT(useridx) AS return_count, AVG(leavedays) AS leavedays
    				FROM t5_user_retention_mobile_log
	    			$tail
    				GROUP BY adflag, today ORDER BY today DESC, adflag ASC
    			) t1 LEFT JOIN (
    					SELECT date(writedate) AS today, adflag, COUNT(DISTINCT useridx) AS buy_user_count, COUNT(useridx) AS buy_count, SUM(money) AS money
    					FROM (
	    					SELECT t1.writedate, adflag, t1.useridx, (facebookcredit / 10) AS money  
							FROM (
								SELECT useridx, adflag, writedate
								FROM t5_user_retention_mobile_log
								$tail			 
							) t1 JOIN t5_product_order t2 ON t1.useridx = t2.useridx AND (t2.writedate BETWEEN t1.writedate AND DATE_ADD(t1.writedate, INTERVAL $search_payday DAY))
							UNION ALL
	    					SELECT t1.writedate, adflag, t1.useridx, money    
							FROM (
								SELECT useridx, adflag, writedate
								FROM t5_user_retention_mobile_log
								$tail			 
							) t1 JOIN t5_product_order_mobile t2 ON t1.useridx = t2.useridx AND (t2.writedate BETWEEN t1.writedate AND DATE_ADD(t1.writedate, INTERVAL $search_payday DAY))
						) t1
						GROUP BY adflag, today ORDER BY today DESC, adflag ASC
    			) t2 ON t1.today = t2.today AND t1.adflag = t2.adflag ";
    }
    else
    {
    	if(($search_start_orderdate != "" && $search_end_orderdate != ""))
    	{
    		if($select_time == "marketing")
    		{
    			$sdate = date('Y-m-d H:i:s', strtotime($search_start_orderdate) - 60 * 60 * 9); //(마케팅)-UTC
    			$order_tail =  " AND t2.writedate BETWEEN '$sdate' AND '$search_end_orderdate 14:59:59'  ";
    		}
    		else if($select_time == "server")
    		{
    			$sdate = date('Y-m-d H:i:s', strtotime($search_start_orderdate)); // 서버 시간
    			$order_tail =  " AND t2.writedate BETWEEN '$sdate' AND '$search_end_orderdate 23:59:59'  ";
    		}
    		
    	}
    	
    	$sql = "SELECT t1.today, t1.adflag, t1.leavedays, nvl(t1.return_count,0) AS return_count, nvl(t2.buy_user_count,0) AS buy_user_count, nvl(t2.buy_count, 0) AS buy_count, nvl(t2.money, 0) AS money
		    	FROM
		    	(
			    	SELECT date(writedate) AS today, adflag, COUNT(useridx) AS return_count, AVG(leavedays) AS leavedays
			    	FROM t5_user_retention_mobile_log
			    	$tail
			    	GROUP BY adflag, today ORDER BY today DESC, adflag ASC
		    	) t1 LEFT JOIN (
			    	SELECT date(writedate) AS today, adflag, COUNT(DISTINCT useridx) AS buy_user_count, COUNT(useridx) AS buy_count, SUM(money) AS money
			    	FROM (
				    	SELECT t1.writedate, adflag, t1.useridx, (facebookcredit / 10) AS money
				    	FROM (
					    	SELECT useridx, adflag, writedate
					    	FROM t5_user_retention_mobile_log
					    	$tail
				    	) t1 JOIN t5_product_order t2 ON t1.useridx = t2.useridx $order_tail
			    		UNION ALL
				    	SELECT t1.writedate, adflag, t1.useridx, money
				    	FROM (
					    	SELECT useridx, adflag, writedate
					    	FROM t5_user_retention_mobile_log
					    	$tail
				    	) t1 JOIN t5_product_order_mobile t2 ON t1.useridx = t2.useridx $order_tail
			    	) t1
			    	GROUP BY adflag, today ORDER BY today DESC, adflag ASC
		    	) t2 ON t1.today = t2.today AND t1.adflag = t2.adflag
    			ORDER BY today DESC, adflag ASC";
    }
    
	$summarylist = $db_redshift->gettotallist($sql);
		
	$total_marketing_list = $db_redshift->gettotallist("SELECT DISTINCT(adflag) FROM t5_user_retention_mobile_log $tail ORDER BY adflag ASC");
	
	$currenttoday = "";
		
	for($i=0; $i<sizeof($summarylist); $i++)
	{
		$today = $summarylist[$i]["today"];
		
		if($currenttoday != $today)
		{
			$currenttoday = $today;
			
			$sql = "SELECT DISTINCT(adflag)
					FROM t5_user_retention_mobile_log
					WHERE writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' $adflag_sql $platform_sql
					ORDER BY adflag ASC";
			$day_marketing_list = $db_redshift->gettotallist($sql);
			
			for($j=0; $j<sizeof($day_marketing_list); $j++)
			{
				$adflag = $day_marketing_list[$j]["adflag"];
				$return_count = get_stat($summarylist, $adflag, $today, "return_count");
				$leavedays = get_stat($summarylist, $adflag, $today, "leavedays");
				$buy_user_count = get_stat($summarylist, $adflag, $today, "buy_user_count");
				$buy_count = get_stat($summarylist, $adflag, $today, "buy_count");
				$money = get_stat($summarylist, $adflag, $today, "money");
				
				$money = round($money, 1);
								
				$payratio = ($return_count > 0) ? round(($buy_user_count / $return_count) * 100, 2) : 0;
				$average_money = ($return_count > 0) ? round($money / $return_count, 3) : 0;
				
				if($adflag == "remerge_int" || $adflag == "dataliftretargeting_int")
				{
					$sql = "SELECT SUM(spend) AS spend
							FROM tbl_agency_retention_spend_daily
							WHERE today = '$today' AND agencyname = '$adflag'";
					$spend = $db_main2->getvalue($sql);
				}
				else
					$spend = 0;
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($j == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($day_marketing_list)?>" valign="center"><?= $today ?></td>
<?
				}
				
				if($return_count == 0)
					continue;
?>
					<td class="tdr point"><?= $adflag ?></td>
                    <td class="tdr point"><?= number_format($return_count) ?></td>
                    <td class="tdr point"><?= number_format($leavedays) ?></td>
                    <td class="tdr point"><?= ($buy_user_count == 0) ? "-" : number_format($buy_user_count) ?> <?= ($payratio == 0) ? "" : "(".$payratio." %)" ?></td>
                    <td class="tdr point"><?= ($buy_count == 0) ? "-" : number_format($buy_count) ?></td>
                    <td class="tdr point"><?= ($money == 0) ? "-" : "$ ".number_format($money) ?> <?= ($average_money == 0) ? "" : "($ ".$average_money.")" ?></td>
                    <td class="tdr point"><?= ($spend == 0) ? "-" : "$ ".number_format($spend) ?></td>
				</tr>
<?
			}
		}
	}
	
	if(sizeof($summarylist) > 0)
	{
		for($j=0; $j<sizeof($total_marketing_list); $j++)
		{
			$adflag = $total_marketing_list[$j]["adflag"];
			
			$sum_leavedays = get_total_stat($summarylist, $adflag, "leavedays");
			$sum_return_count = get_total_stat($summarylist, $adflag, "return_count");
			$sum_buy_user_count = get_total_stat($summarylist, $adflag, "buy_user_count");
			$sum_buy_count = get_total_stat($summarylist, $adflag, "buy_count");
			$sum_money = get_total_stat($summarylist, $adflag, "money");
			
			$sum_money = round($sum_money, 1);
							
			$sum_payratio = ($sum_return_count > 0) ? round(($sum_buy_user_count / $sum_return_count) * 100, 2) : 0;
			$sum_average_money = ($sum_return_count > 0) ? round($sum_money / $sum_return_count, 3) : 0;
			
			if($adflag == "remerge_int" || $adflag == "dataliftretargeting_int")
			{
				$sql = "SELECT SUM(spend) AS spend
						FROM tbl_agency_retention_spend_daily
						WHERE today BETWEEN '$search_start_returndate' AND '$search_end_returndate' AND agencyname = '$adflag'";
				$spend = $db_main2->getvalue($sql);
			}
			else
				$spend = 0;
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($j == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($total_marketing_list)?>" valign="center">Total</td>
<?
				}
?>
					<td class="tdr point"><?= $adflag ?></td>
                    <td class="tdr point"><?= number_format($sum_return_count) ?></td>
                    <td class="tdr point"><?= number_format($sum_leavedays) ?></td>
                    <td class="tdr point"><?= ($sum_buy_user_count == 0) ? "-" : number_format($sum_buy_user_count) ?> <?= ($sum_payratio == 0) ? "" : "(".$sum_payratio." %)" ?></td>
                    <td class="tdr point"><?= ($sum_buy_count == 0) ? "-" : number_format($sum_buy_count) ?></td>
                    <td class="tdr point"><?= ($sum_money == 0) ? "-" : "$ ".number_format($sum_money) ?> <?= ($sum_average_money == 0) ? "" : "($ ".$sum_average_money.")" ?></td>
                    <td class="tdr point"><?= ($spend == 0) ? "-" : "$ ".number_format($spend) ?></td>
			</tr>
<?
		}
	}

	$db_redshift->end();
?>    
            </tbody>
            </table>
        </div>
</div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>