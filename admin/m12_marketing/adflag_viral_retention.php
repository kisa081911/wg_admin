<?
    $top_menu = "marketing";
    $sub_menu = "adflag_viral_retention";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
	$isearch = $_GET["issearch"];
		
	if ($isearch == "")
	{
		$search_end_createdate  = date("Y-m-d", time() - 60 * 60);
		$search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 6);
	}
	
	function make_percent($count, $total)
	{
		if ($total == 0)
			return 0;
		
		return round($count * 1000 / $total) / 10;	
	}
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
$(function() {
    $("#start_createdate").datepicker({ });
});

$(function() {
    $("#end_createdate").datepicker({ });
});
</script>

        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
        <form name="search_form" id="search_form"  method="get" action="adflag_viral_retention.php">
            <input type=hidden name=issearch value="1">
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; Game Viral 유지 분석</div>
                <div class="search_box">
                    <input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
                    <input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
                     <input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
                </div>
            </div>
            <!-- //title_warp -->
            
            <div class="search_result">
                <span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
            </div>
            
            <div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
                <col width="">
                <col width=""> 
                <col width=""> 
                <col width=""> 
            </colgroup>
            <thead>
            <tr>
                <th>접속일</th>
                <th class="tdr">유입경로</th>
                <th class="tdr">2주이탈복귀</th>
                <th class="tdr">2주이탈복귀<br>Engaged</th>
                <th class="tdr">1주이탈복귀</th>
                <th class="tdr">1주이탈복귀<br>Engaged</th>
                <th class="tdr">2일이탈복귀</th>
                <th class="tdr">2일이탈복귀<br>Engaged</th>
                <th class="tdr">접속</th>
                <th class="tdr">접속<br>Engaged</th>
            </tr>
            </thead>
            <tbody>
<?
    $db_main2 = new CDatabase_Main2();

    $now = time();
    
    if ($search_end_createdate != "")
    {
    	$now = strtotime($search_end_createdate);
    }
    else
    {
    	$search_end_createdate = date('Y-m-d');
    }
    
    if ($search_start_createdate == "")
    {
    	$search_start_createdate = '2015-11-01';
    }
    
	$totalcount = array();
	
    while (true)
    {
    	$today = date("Y-m-d", $now);
    	$tomorrow = date("Y-m-d", $now + 24 * 60 * 60);
    	
    	if ($tomorrow < $search_start_createdate)
    		break;

		$sql = "SELECT adflag,usercount,usercount_engage,1weekcount,1weekcount_engage,2daycount,2daycount_engage,clickcount,clickcount_engage FROM tbl_user_retention WHERE retentiondate='$today' AND adflag IN ('event','fanpage','usershare','userad','userwall', 'website', 'levelup', 'jackpot') ORDER BY adflag";
		$list = $db_main2->gettotallist($sql);
		
		if (sizeof($list) != 0)
		{
?>
                <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
                   <td class="tdc point_title" rowspan="<?= sizeof($list) ?>" valign="center"><?= $today ?></td>
<?			
    		for ($i=0; $i<sizeof($list); $i++)
    		{
    			$adflag = $list[$i]["adflag"];
    			$usercount = $list[$i]["usercount"];
    			$usercount_engage = $list[$i]["usercount_engage"];
    			$weekcount = $list[$i]["1weekcount"];
    			$weekcount_engage = $list[$i]["1weekcount_engage"];
    			$daycount = $list[$i]["2daycount"];
    			$daycount_engage = $list[$i]["2daycount_engage"];
    			$clickcount = $list[$i]["clickcount"];
    			$clickcount_engage = $list[$i]["clickcount_engage"];

				if ($totalcount[$adflag]["usercount"] == "")
					$totalcount[$adflag]["usercount"] = 0;
				
				if ($totalcount[$adflag]["usercount_engage"] == "")
					$totalcount[$adflag]["usercount_engage"] = 0;
				
				if ($totalcount[$adflag]["1weekcount"] == "")
					$totalcount[$adflag]["1weekcount"] = 0;
				
				if ($totalcount[$adflag]["1weekcount_engage"] == "")
					$totalcount[$adflag]["1weekcount_engage"] = 0;
				
				if ($totalcount[$adflag]["2daycount"] == "")
					$totalcount[$adflag]["2daycount"] = 0;
				
				if ($totalcount[$adflag]["2daycount_engage"] == "")
					$totalcount[$adflag]["2daycount_engage"] = 0;
				
				if ($totalcount[$adflag]["clickcount"] == "")
					$totalcount[$adflag]["clickcount"] = 0;
				
				if ($totalcount[$adflag]["clickcount_engage"] == "")
					$totalcount[$adflag]["clickcount_engage"] = 0;
				
				$totalcount[$adflag]["adflag"] = $adflag;
				$totalcount[$adflag]["usercount"] += $usercount;
				$totalcount[$adflag]["usercount_engage"] += $usercount_engage;
				$totalcount[$adflag]["1weekcount"] += $weekcount;
				$totalcount[$adflag]["1weekcount_engage"] += $weekcount_engage;
				$totalcount[$adflag]["2daycount"] += $daycount;
				$totalcount[$adflag]["2daycount_engage"] += $daycount_engage;
				$totalcount[$adflag]["clickcount"] += $clickcount;
				$totalcount[$adflag]["clickcount_engage"] += $clickcount_engage;				
?>                            
                    <td class="tdr point"><?= ($adflag == "") ? "viral" : str_replace("%", "", $adflag) ?></td>
                    <td class="tdr point"><?= number_format($usercount) ?></td>
                    <td class="tdr point"><?= number_format($usercount_engage) ?> (<?= make_percent($usercount_engage, $usercount) ?>%)</td>
                    <td class="tdr point"><?= number_format($weekcount) ?></td>
                    <td class="tdr point"><?= number_format($weekcount_engage) ?> (<?= make_percent($weekcount_engage, $weekcount) ?>%)</td>
                    <td class="tdr point"><?= number_format($daycount) ?></td>
                    <td class="tdr point"><?= number_format($daycount_engage) ?> (<?= make_percent($daycount_engage, $daycount) ?>%)</td>
                    <td class="tdr point"><?= number_format($clickcount) ?></td>
                    <td class="tdr point"><?= number_format($clickcount_engage) ?> (<?= make_percent($clickcount_engage, $clickcount) ?>%)</td>
                </tr>
<?
    		}
		}
    	
    	$now -= 60 * 60 * 24;
    }

	$db_main2->end();
?>    
                <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
                   <td class="tdc point_title" rowspan="<?= sizeof($totalcount) ?>" valign="center"><b>Total</b></td>
<?			
    		for ($i=0; $i<sizeof($totalcount); $i++)
    		{
    			if ($i != 0)
				{
?>
				<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">					
<?					
				}
		    			
    			$list = array_keys($totalcount);
				$adflag = $list[$i];
    			$usercount = $totalcount[$adflag]["usercount"];
    			$usercount_engage = $totalcount[$adflag]["usercount_engage"];
    			$weekcount = $totalcount[$adflag]["1weekcount"];
    			$weekcount_engage = $totalcount[$adflag]["1weekcount_engage"];
    			$daycount = $totalcount[$adflag]["2daycount"];
    			$daycount_engage = $totalcount[$adflag]["2daycount_engage"];
    			$clickcount = $totalcount[$adflag]["clickcount"];
    			$clickcount_engage = $totalcount[$adflag]["clickcount_engage"];
?>                            
                    <td class="tdr point"><?= ($adflag == "") ? "viral" : str_replace("%", "", $adflag) ?></td>
                    <td class="tdr point"><?= number_format($usercount) ?></td>
                    <td class="tdr point"><?= number_format($usercount_engage) ?> (<?= make_percent($usercount_engage, $usercount) ?>%)</td>
                    <td class="tdr point"><?= number_format($weekcount) ?></td>
                    <td class="tdr point"><?= number_format($weekcount_engage) ?> (<?= make_percent($weekcount_engage, $weekcount) ?>%)</td>
                    <td class="tdr point"><?= number_format($daycount) ?></td>
                    <td class="tdr point"><?= number_format($daycount_engage) ?> (<?= make_percent($daycount_engage, $daycount) ?>%)</td>
                    <td class="tdr point"><?= number_format($clickcount) ?></td>
                    <td class="tdr point"><?= number_format($clickcount_engage) ?> (<?= make_percent($clickcount_engage, $clickcount) ?>%)</td>
                </tr>
<?
    		}
?>                
            </tbody>
            </table>
        </div>
        
        </form>
</div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>