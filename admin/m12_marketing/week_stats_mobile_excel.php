<?
	include_once("../common/dbconnect/db_util_redshift.inc.php");
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	check_login();
	
	$search_startdate = $_GET["sdate"];
	$search_enddate = $_GET["edate"];
	$pay_startdate = $_GET["pay_sdate"];
	$pay_enddate = $_GET["pay_edate"];
	$search_platform = isset($_GET["search_platform"]) ? $_GET["search_platform"] : "0";
	$search_agency = $_GET["agency"];
	
	$filename = "week_stats_mobile_".$search_startdate."_".$search_enddate.".xls";
		
	$agency_sql = "";
	$spend_sql = "";
	
	if($search_platform == 0)
		$platform_sql = "platform > $search_platform";
	else
		$platform_sql = "platform = $search_platform";
	
	if($search_agency != "")
	{
		$spend_sql = " AND agencyname = '$search_agency' ";
		$agency_sql = " AND adflag LIKE '$search_agency%' ";
	}
	
	$db_main2 = new CDatabase_Main2();
	$db_redshift = new CDatabase_Redshift();
	
	$sql = "SELECT DISTINCT adflag AS agencyname ".
			"FROM t5_user_adflag_log ".
			"WHERE $platform_sql AND '$search_startdate 00:00:00' <= writedate AND writedate <= '$search_enddate 23:59:59' ".
  			"	AND (adflag LIKE '%_int' OR adflag IN ('Facebook Ads', 'nanigans', 'Twitter', 'amazon') ) ".
			"ORDER BY LOWER(adflag) ASC";
	$agency_list = $db_redshift->gettotallist($sql);
	
	$sql = "SELECT WEEKOFYEAR(today) AS week_count, SUM(spend) AS spend ".
			"FROM ( ".
			"	SELECT today, agencyname, ROUND(SUM(spend), 2) AS spend ".
			"	FROM tbl_agency_spend_daily ".
			"	WHERE today BETWEEN '$search_startdate' AND '$search_enddate' AND agencyidx != 8 AND $platform_sql ".
			"	GROUP BY today, agencyname ".
			"	UNION ALL ".
			"	SELECT today, (CASE WHEN campaign_name LIKE 'Nanigans%' THEN 'nanigans' ELSE 'Facebook Ads' END) AS agencyname, ROUND(SUM(spend), 2) AS spend ".
			"	FROM `tbl_ad_stats_mobile` ".
			"	WHERE today BETWEEN '$search_startdate' AND '$search_enddate' AND $platform_sql ".
			"	GROUP BY today ".
			") t1 ".
			"WHERE 1=1 $spend_sql".
			"GROUP BY WEEKOFYEAR(today)";
	$spend_list = $db_main2->gettotallist($sql);
	
	$sql = "SELECT t1.week_count, start_date, end_date, total_cnt, new_cnt, (total_cnt - new_cnt) AS rejoin_cnt, nvl(payer_cnt, 0) AS payer_cnt, nvl(pay_amount, 0) AS pay_amount, nvl(pay_cnt, 0) AS pay_cnt ".
			"FROM ( ".
  			"	SELECT date_part(w, writedate) AS week_count, date(MIN(writedate)) AS start_date, date(MAX(writedate)) AS end_date, COUNT(DISTINCT useridx) AS total_cnt, SUM(case when isnew = 1 then 1 end) AS new_cnt ".
  			"	FROM t5_user_adflag_log ".
  			"	WHERE $platform_sql AND '$search_startdate 00:00:00' <= writedate AND writedate <= '$search_enddate 23:59:59' ".
    		"		and (adflag like '%_int' OR adflag like '%_viral' OR adflag IN ('Facebook Ads', 'nanigans', 'Twitter', 'amazon')) $agency_sql ".
  			"	GROUP BY date_part(w, writedate) ".
			") t1 LEFT JOIN ( ".
  			"	SELECT date_part(w, t1.writedate) AS week_count, COUNT(DISTINCT t2.useridx) AS payer_cnt, SUM(money) AS pay_amount, COUNT(orderidx) AS pay_cnt ".
  			"	FROM ( ".
    		"		SELECT useridx, platform, adflag, isnew, writedate, nvl(enddate, CURRENT_DATE) as enddate ".
    		"		FROM t5_user_adflag_log ".
    		"		WHERE $platform_sql AND '$search_startdate 00:00:00' <= writedate AND writedate <= '$search_enddate 23:59:59' ".
        	"			and (adflag like '%_int' OR adflag like '%_viral' OR adflag IN ('Facebook Ads', 'nanigans', 'Twitter', 'amazon')) $agency_sql ".
  			"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx and t1.writedate <= t2.writedate and t1.enddate > t2.writedate and platform = os_type ".
  			"	WHERE status = 1 AND '$pay_startdate 00:00:00' <= t2.writedate AND t2.writedate <= '$pay_enddate 23:59:59' ".
  			"	GROUP BY date_part(w, t1.writedate) ".
			") t2 ON t1.week_count = t2.week_count ".
			"ORDER BY t1.week_count ASC";
	$week_list = $db_redshift->gettotallist($sql);
	
	$sql = "SELECT WEEKOFYEAR(install_time) AS week_count, SUM(event_value) AS iap_amount ".
			"FROM ( ".
			"	SELECT install_time, event_time, event_value, (CASE WHEN (media_source = 'Facebook Ads' AND agency = 'nanigans') THEN 'nanigans' WHEN (media_source = 'Facebook Ads' AND agency != 'nanigans') THEN 'Facebook Ads' ELSE media_source END) AS agencyname ".
			"	FROM `tbl_appsflyer_inappevent` ".
			"	WHERE $platform_sql AND '$search_startdate 00:00:00' <= install_time AND install_time < '$search_enddate 00:00:00' AND '$pay_startdate 00:00:00' <= event_time AND event_time <= '$pay_enddate 23:59:59' AND media_source != '' AND fb_campaign_name NOT LIKE '%reten%' ".
			") t1 ".
			"WHERE 1=1 $spend_sql ".
			"GROUP BY WEEKOFYEAR(install_time)";
	$apps_iap_list = $db_main2->gettotallist($sql);

	$db_main2->end();
	$db_redshift->end();
	
	if (sizeof($week_list) == 0)
		error_go("저장할 데이터가 없습니다.", "week_stats_mobile.php");
	
	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
	"<table border=1>".
		"<tr>".
			"<td style='font-weight:bold;'>구간</td>".
			"<td style='font-weight:bold;'>유입수</td>".
			"<td style='font-weight:bold;'>CPI</td>".
			"<td style='font-weight:bold;'>비용</td>".
			"<td style='font-weight:bold;'>총결제</td>".
			"<td style='font-weight:bold;'>ROI</td>".
			"<td style='font-weight:bold;'>총결제(A)</td>".
			"<td style='font-weight:bold;'>ROI(A)</td>".
			"<td style='font-weight:bold;'>결제자수</td>".
			"<td style='font-weight:bold;'>결제자 비율</td>".
			"<td style='font-weight:bold;'>결제건수</td>".
			"<td style='font-weight:bold;'>ARPU</td>".
			"<td style='font-weight:bold;'>ARPPU</td>".
		"</tr>";
	
		$total_install = 0;
		$total_spend = 0;
		$total_cpi = 0;
		$total_pay_amount = 0;
		$total_iap_amount = 0;
		$total_roi = 0;
		$total_payer_count = 0;
		$total_pay_count = 0;
		$total_payer_rate = 0;
		$total_arpu = 0;
		$total_arppu = 0;
	
		for($i=0; $i<sizeof($week_list); $i++)
		{
			$spend = 0;
			$iap_amount = 0;
			$week_count = $week_list[$i]["week_count"];
			$start_date = $week_list[$i]["start_date"];
			$end_date = $week_list[$i]["end_date"];
			$total_cnt = $week_list[$i]["total_cnt"];
	
			for($j=0; $j<sizeof($spend_list); $j++)
			{
				if($spend_list[$j]["week_count"] == $week_count)
				{
					$spend = $spend_list[$j]["spend"];
					break;
				}
			}
			
			for($j=0; $j<sizeof($apps_iap_list); $j++)
			{
				if($apps_iap_list[$j]["week_count"] == $week_count)
				{
					$iap_amount = $apps_iap_list[$j]["iap_amount"];
					break;
				}
			}
			
			$cpi = ($total_cnt == 0) ? 0 : round($spend/$total_cnt, 2);
			$pay_amount = $week_list[$i]["pay_amount"];
			$roi = ($spend == 0) ? 0 : round($pay_amount/$spend*100, 2);
			$apps_roi = ($spend == 0) ? 0 : round($iap_amount/$spend*100, 2);
			$payer_cnt = $week_list[$i]["payer_cnt"];
			$payer_rate = ($total_cnt == 0) ? 0 : round($payer_cnt/$total_cnt*100, 2);
			$pay_count = $week_list[$i]["pay_cnt"];
			$arpu = ($total_cnt == 0) ? 0 : round($pay_amount/$total_cnt, 2);
			$arppu = ($payer_cnt == 0) ? 0 : round($pay_amount/$payer_cnt, 2);
			
			$total_install += $total_cnt;
			$total_spend += $spend;
			$total_pay_amount += $pay_amount;
			$total_iap_amount += $iap_amount;
			$total_payer_count += $payer_cnt;
			$total_pay_count += $pay_count;
  			
  			$excel_contents .= "<tr>".
    			"<td>구간 ".$week_count." - $start_date ~ $end_date</td>".
		  		"<td>".number_format($total_cnt)."</td>".
		  		"<td>$".number_format($cpi, 2)."</td>".
		  		"<td>$".number_format($spend, 2)."</td>".
		  		"<td>$".number_format($pay_amount, 2)."</td>".
		  		"<td>".number_format($roi, 2)."%</td>".
		  		"<td>$".number_format($iap_amount, 2)."</td>".
		  		"<td>".number_format($apps_roi,2)."%</td>".
		  		"<td>".number_format($payer_cnt)."</td>".
		  		"<td>".number_format($payer_rate, 2)."%</td>".
		  		"<td>".number_format($pay_count)."</td>".
		  		"<td>".number_format($arpu, 2)."</td>".
		  		"<td>".number_format($arppu, 2)."</td>".
	  		"</tr>";
  		}
  		
  		$total_cpi = ($total_install == 0) ? 0 : round($total_spend/$total_install, 2);
		$total_roi = ($total_spend == 0) ? 0 : round($total_pay_amount/$total_spend*100, 2);
		$total_apps_roi = ($total_spend == 0) ? 0 : round($total_iap_amount/$total_spend*100, 2);
		$total_payer_rate = ($total_install == 0) ? 0 : round($total_payer_count/$total_install*100, 2);
		$total_arpu = ($total_install == 0) ? 0 : round($total_pay_amount/$total_install, 2);
		$total_arppu = ($total_payer_count == 0) ? 0 : round($total_pay_amount/$total_payer_count, 2);
		
		$excel_contents .= "<tr>".
				"<td>합계</td>".
				"<td>".number_format($total_install)."</td>".
				"<td>$".number_format($total_cpi, 2)."</td>".
				"<td>$".number_format($total_spend, 2)."</td>".
				"<td>$".number_format($total_pay_amount, 2)."</td>".
				"<td>".number_format($total_roi, 2)."%</td>".
				"<td>$".number_format($total_iap_amount, 2)."</td>".
				"<td>".number_format($total_apps_roi,2)."%</td>".
				"<td>".number_format($total_payer_count)."</td>".
				"<td>".number_format($total_payer_rate, 2)."%</td>".
				"<td>".number_format($total_pay_count)."</td>".
				"<td>".number_format($total_arpu, 2)."</td>".
				"<td>".number_format($total_arppu, 2)."</td>".
				"</tr>";
						  
		$excel_contents .= "</table>";
  
		Header("Content-type: application/x-msdownload");
		Header("Content-type: application/x-msexcel");
		Header("Content-Disposition: attachment; filename=$filename");
  
		echo($excel_contents);
?>