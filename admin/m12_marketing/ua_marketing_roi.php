<?
    $top_menu = "marketing";
    $sub_menu = "ua_marketing_roi";
    
    include_once("../common/dbconnect/db_util_redshift.inc.php");
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $pagename = "ua_marketing_roi.php";
    
    $startdate = $_GET["startdate"];
    $enddate = $_GET["enddate"];
    
    //오늘 날짜 정보
    $std_start = get_past_date(date("Y-m-d"),56,"d");
    $std_end = get_past_date(date("Y-m-d"),29,"d");
    $startdate = ($startdate == "") ? $std_start : $startdate;
    $enddate = ($enddate == "") ? $std_end : $enddate;
    
    $db_main = new CDatabase_main();
    $db_main2 = new CDatabase_main2();
    $db_redshift = new CDatabase_Redshift();
    
    $retention_0_ratio = 0.000260;
    $retention_1_ratio = 0.009950;
    $retention_2_ratio = 0.074510;
    $retention_3_ratio = 0.079086;
    $retention_4_ratio = 0.163284;
    $retention_5_ratio = 0.191318;
    $retention_6_ratio = 0.209345;
    $retention_14_ratio = 0.266420;
    $retention_28_ratio = 0.448425;
    $retention_56_ratio = 0.598538;
    $retention_112_ratio = 0.666005;
    $retention_224_ratio = 0.712735;
    $retention_300_ratio = 0.779716;
    $retention_343_ratio = 0.808075;
    $retention_else_ratio = 0.876456;
        
    // iOS 마케팅 비용
    $sql = "SELECT SUM(spend) AS spend ".
            "FROM `tbl_agency_spend_daily` ".
            "WHERE '$startdate' <= today AND today <= '$enddate' AND platform = 1";
    $ios_spend = $db_main2->getvalue($sql);
    
    // iOS 신규 매출
    $sql = "SELECT ROUND(SUM(money), 2) AS money ".
            "FROM ( ".
            "	SELECT FLOOR(TIMESTAMPDIFF(SECOND, createdate, writedate)/(24 * 60 * 60)) AS dayafterinstall, SUM(facebookcredit/10) AS money ".
            "	FROM ( ".
            "		SELECT useridx, createdate ".
            "		FROM tbl_user_ext ".
            "		WHERE platform = 1 AND '$startdate 00:00:00' <= createdate AND createdate <= '$enddate 23:59:59' AND useridx > 20000 ".
            "			AND (adflag LIKE 'Face%' OR adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Apple%') AND adflag NOT LIKE 'm_rete%' ".
            "	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
            "	WHERE STATUS = 1 AND TIMESTAMPDIFF(SECOND, createdate, writedate) < 28 * 24 * 60 * 60 ".
            "	GROUP BY dayafterinstall ".
            "	UNION ALL ".
            "	SELECT FLOOR(TIMESTAMPDIFF(SECOND, createdate, writedate)/(24 * 60 * 60)) AS dayafterinstall, SUM(money) AS money ".
            "	FROM ( ".
            "		SELECT useridx, createdate ".
            "		FROM tbl_user_ext ".
            "		WHERE platform = 1 AND '$startdate 00:00:00' <= createdate AND createdate <= '$enddate 23:59:59' AND useridx > 20000 ".
            "			AND (adflag LIKE 'Face%' OR adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Apple%') AND adflag NOT LIKE 'm_rete%' ".
            "	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
            "	WHERE STATUS = 1 AND TIMESTAMPDIFF(SECOND, createdate, writedate) < 28 * 24 * 60 * 60 ".
            "	GROUP BY dayafterinstall ".
            ") total";
    $ios_ua_sales = $db_main->getvalue($sql);
    
    // iOS 신규 광고로 인한 복귀 매출
    $sql = "SELECT SUM(money * multi_value) as total_money
            FROM (
              select t3.useridx, max(leavedays) as leavedays, SUM(money) as money,
                (case when max(leavedays) = 0 then $retention_0_ratio when max(leavedays) = 1 then $retention_1_ratio when max(leavedays) = 2 then $retention_2_ratio when max(leavedays) = 3 then $retention_3_ratio when max(leavedays) = 4 then $retention_4_ratio
                      when max(leavedays) = 5 then $retention_5_ratio when max(leavedays) = 6 then $retention_6_ratio when max(leavedays) < 14 then $retention_14_ratio when max(leavedays) < 28 then $retention_28_ratio when max(leavedays) < 56 then $retention_56_ratio
                      when max(leavedays) < 112 then $retention_112_ratio when max(leavedays) < 224 then $retention_224_ratio  when max(leavedays) < 300 then $retention_300_ratio when max(leavedays) < 343 then $retention_343_ratio else $retention_else_ratio end) as multi_value
              from (
                select t1.useridx, retentiondate, leavedays
                from (
                  select useridx, max(leavedays) as leavedays, min(createdate) as createdate, min(retentiondate) as retentiondate
                  from (
                    select useridx, leavedays, createdate, writedate as retentiondate
                    from t5_user_retention_mobile_log
                    where useridx > 20000 and platform = 1 AND (adflag LIKE 'Face%' OR adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'FB_%' OR adflag LIKE 'Apple%')
                      AND adflag NOT LIKE 'm_reten%' AND adflag NOT LIKE 'reten%'
                      AND '$startdate 00:00:00' <= writedate AND writedate <= '$enddate 23:59:59'
                      AND '$startdate 00:00:00' > createdate  
                  ) a
                  group by useridx        
                ) t1 join t5_user t2 on t1.useridx = t2.useridx
              ) t3 join t5_product_order_all t4 on t3.useridx=t4.useridx and t3.retentiondate <= t4.writedate
              where status = 1 and datediff(second, retentiondate, writedate) < 28  * 24 * 60 * 60
              group by t3.useridx
            ) total";
    $ios_retention_sales = $db_redshift->getvalue($sql);
    
    $ios_total_sales = $ios_ua_sales + $ios_retention_sales;
    $ios_roi = ROUND($ios_total_sales / $ios_spend * 100, 2);
    
    // Android 마케팅 비용
    $sql = "SELECT SUM(spend) AS spend ".
        "FROM `tbl_agency_spend_daily` ".
        "WHERE '$startdate' <= today AND today <= '$enddate' AND platform = 2";
    $and_spend = $db_main2->getvalue($sql);
    
    // Android 신규 매출
    $sql = "SELECT ROUND(SUM(money), 2) AS money ".
            "FROM ( ".
            "	SELECT FLOOR(TIMESTAMPDIFF(SECOND, createdate, writedate)/(24 * 60 * 60)) AS dayafterinstall, SUM(facebookcredit/10) AS money ".
            "	FROM ( ".
            "		SELECT useridx, createdate ".
            "		FROM tbl_user_ext ".
            "		WHERE platform = 2 AND '$startdate 00:00:00' <= createdate AND createdate <= '$enddate 23:59:59' AND useridx > 20000 ".
            "			AND (adflag LIKE 'Face%' OR adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Apple%') AND adflag NOT LIKE 'm_rete%' ".
            "	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
            "	WHERE STATUS = 1 AND TIMESTAMPDIFF(SECOND, createdate, writedate) < 28 * 24 * 60 * 60 ".
            "	GROUP BY dayafterinstall ".
            "	UNION ALL ".
            "	SELECT FLOOR(TIMESTAMPDIFF(SECOND, createdate, writedate)/(24 * 60 * 60)) AS dayafterinstall, SUM(money) AS money ".
            "	FROM ( ".
            "		SELECT useridx, createdate ".
            "		FROM tbl_user_ext ".
            "		WHERE platform = 2 AND '$startdate 00:00:00' <= createdate AND createdate <= '$enddate 23:59:59' AND useridx > 20000 ".
            "			AND (adflag LIKE 'Face%' OR adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'Apple%') AND adflag NOT LIKE 'm_rete%' ".
            "	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
            "	WHERE STATUS = 1 AND TIMESTAMPDIFF(SECOND, createdate, writedate) < 28 * 24 * 60 * 60 ".
            "	GROUP BY dayafterinstall ".
            ") total";
    $and_ua_sales = $db_main->getvalue($sql);
    
    // Android 신규 광고로 인한 복귀 매출
    $sql = "SELECT SUM(money * multi_value) as total_money
            FROM (
              select t3.useridx, max(leavedays) as leavedays, SUM(money) as money,
                (case when max(leavedays) = 0 then $retention_0_ratio when max(leavedays) = 1 then $retention_1_ratio when max(leavedays) = 2 then $retention_2_ratio when max(leavedays) = 3 then $retention_3_ratio when max(leavedays) = 4 then $retention_4_ratio
                      when max(leavedays) = 5 then $retention_5_ratio when max(leavedays) = 6 then $retention_6_ratio when max(leavedays) < 14 then $retention_14_ratio when max(leavedays) < 28 then $retention_28_ratio when max(leavedays) < 56 then $retention_56_ratio
                      when max(leavedays) < 112 then $retention_112_ratio when max(leavedays) < 224 then $retention_224_ratio  when max(leavedays) < 300 then $retention_300_ratio when max(leavedays) < 343 then $retention_343_ratio else $retention_else_ratio end) as multi_value
              from (
                select t1.useridx, retentiondate, leavedays
                from (
                  select useridx, max(leavedays) as leavedays, min(createdate) as createdate, min(retentiondate) as retentiondate
                  from (
                    select useridx, leavedays, createdate, writedate as retentiondate
                    from t5_user_retention_mobile_log
                    where useridx > 20000 and platform = 2 AND (adflag LIKE 'Face%' OR adflag LIKE '%_int' OR adflag LIKE '%_viral' OR adflag LIKE 'FB_%' OR adflag LIKE 'Apple%')
                      AND adflag NOT LIKE 'm_reten%' AND adflag NOT LIKE 'reten%'
                      AND '$startdate 00:00:00' <= writedate AND writedate <= '$enddate 23:59:59'
                      AND '$startdate 00:00:00' > createdate
                  ) a
                  group by useridx
                ) t1 join t5_user t2 on t1.useridx = t2.useridx
              ) t3 join t5_product_order_all t4 on t3.useridx=t4.useridx and t3.retentiondate <= t4.writedate
              where status = 1 and datediff(second, retentiondate, writedate) < 28  * 24 * 60 * 60
              group by t3.useridx
            ) total";
    $and_retention_sales = $db_redshift->getvalue($sql);
    
    $and_total_sales = $and_ua_sales + $and_retention_sales;
    $and_roi = ROUND($and_total_sales / $and_spend * 100, 2);
    
    // Amazon 마케팅 비용
    $sql = "SELECT SUM(spend) AS spend ".
            "FROM `tbl_agency_spend_daily` ".
            "WHERE '$startdate' <= today AND today <= '$enddate' AND platform = 3";
    $ama_spend = $db_main2->getvalue($sql);
    
    // Amazon 신규 매출
    $sql = "SELECT ROUND(SUM(money), 2) AS money ".
        "FROM ( ".
        "	SELECT FLOOR(TIMESTAMPDIFF(SECOND, createdate, writedate)/(24 * 60 * 60)) AS dayafterinstall, SUM(facebookcredit/10) AS money ".
        "	FROM ( ".
        "		SELECT useridx, createdate ".
        "		FROM tbl_user_ext ".
        "		WHERE platform = 3 AND '$startdate 00:00:00' <= createdate AND createdate <= '$enddate 23:59:59' AND useridx > 20000 ".
        "			AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') AND adflag NOT LIKE 'm_rete%' ".
        "	) t1 JOIN tbl_product_order t2 ON t1.useridx = t2.useridx ".
        "	WHERE STATUS = 1 AND TIMESTAMPDIFF(SECOND, createdate, writedate) < 28 * 24 * 60 * 60 ".
        "	GROUP BY dayafterinstall ".
        "	UNION ALL ".
        "	SELECT FLOOR(TIMESTAMPDIFF(SECOND, createdate, writedate)/(24 * 60 * 60)) AS dayafterinstall, SUM(money) AS money ".
        "	FROM ( ".
        "		SELECT useridx, createdate ".
        "		FROM tbl_user_ext ".
        "		WHERE platform = 3 AND '$startdate 00:00:00' <= createdate AND createdate <= '$enddate 23:59:59' AND useridx > 20000 ".
        "			AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%') AND adflag NOT LIKE 'm_rete%' ".
        "	) t1 JOIN tbl_product_order_mobile t2 ON t1.useridx = t2.useridx ".
        "	WHERE STATUS = 1 AND TIMESTAMPDIFF(SECOND, createdate, writedate) < 28 * 24 * 60 * 60 ".
        "	GROUP BY dayafterinstall ".
        ") total";
    $ama_ua_sales = $db_main->getvalue($sql);
    
    // Amazon 신규 광고로 인한 복귀 매출
    $sql = "SELECT SUM(money * multi_value) as total_money
            FROM (
              select t3.useridx, max(leavedays) as leavedays, SUM(money) as money,
                (case when max(leavedays) = 0 then $retention_0_ratio when max(leavedays) = 1 then $retention_1_ratio when max(leavedays) = 2 then $retention_2_ratio when max(leavedays) = 3 then $retention_3_ratio when max(leavedays) = 4 then $retention_4_ratio
                      when max(leavedays) = 5 then $retention_5_ratio when max(leavedays) = 6 then $retention_6_ratio when max(leavedays) < 14 then $retention_14_ratio when max(leavedays) < 28 then $retention_28_ratio when max(leavedays) < 56 then $retention_56_ratio
                      when max(leavedays) < 112 then $retention_112_ratio when max(leavedays) < 224 then $retention_224_ratio  when max(leavedays) < 300 then $retention_300_ratio when max(leavedays) < 343 then $retention_343_ratio else $retention_else_ratio end) as multi_value
              from (
                select t1.useridx, retentiondate, leavedays
                from (
                  select useridx, max(leavedays) as leavedays, min(createdate) as createdate, min(retentiondate) as retentiondate
                  from (
                    select useridx, leavedays, createdate, writedate as retentiondate
                    from t5_user_retention_mobile_log
                    where useridx > 20000 and platform = 3 AND (adflag LIKE 'amazon%' OR adflag LIKE 'Face%')
                      AND adflag NOT LIKE 'm_reten%' AND adflag NOT LIKE 'reten%'
                      AND '$startdate 00:00:00' <= writedate AND writedate <= '$enddate 23:59:59'
                      AND '$startdate 00:00:00' > createdate
                  ) a
                  group by useridx
                ) t1 join t5_user t2 on t1.useridx = t2.useridx
              ) t3 join t5_product_order_all t4 on t3.useridx=t4.useridx and t3.retentiondate <= t4.writedate
              where status = 1 and datediff(second, retentiondate, writedate) < 28  * 24 * 60 * 60
              group by t3.useridx
            ) total";
    $ama_retention_sales = $db_redshift->getvalue($sql);
    
    $ama_total_sales = $ama_ua_sales + $ama_retention_sales;
    $ama_roi = ROUND($ama_total_sales / $ama_spend * 100, 2);
    
    $db_main->end();
    $db_main2->end();
    $db_redshift->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#startdate").datepicker({ });
	});
	
	$(function() {
		$("#enddate").datepicker({ });
	});

	function search()
	{
	    var search_form = document.search_form;

	    search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title" style="padding-right:10px;">신규 마케팅 ROI (D+28)</div>
	</div>
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
		<div class="search_box">
			<span class="search_lbl ml20">가입일&nbsp;&nbsp;&nbsp;</span>
			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" /> ~
			<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
			<input type="button" class="btn_search" value="검색" onclick="search()" />
		</div>
	</form>
	<!-- //title_warp -->
	<br/><br/>
	
	<table class="tbl_list_basic1" style="margin-top:5px">
		<colgroup>
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">        
		</colgroup>
		<thead>
			<tr>
				<th class="tdc" rowspan="2">플랫폼</th>
				<th class="tdc" rowspan="2">비용</th>
				<th class="tdc" colspan="4">D+28</th>
			</tr>
			<tr>
				<th class="tdc">신규 매출</th>
				<th class="tdc">복귀 매출</th>
				<th class="tdc">총 매출</th>
				<th class="tdc">ROI</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="tdc point">iOS</td>
				<td class="tdc">$<?= number_format($ios_spend) ?></td>
				<td class="tdc">$<?= number_format($ios_ua_sales) ?></td>
				<td class="tdc">$<?= number_format($ios_retention_sales) ?></td>
				<td class="tdc">$<?= number_format($ios_total_sales) ?></td>
				<td class="tdc"><?= number_format($ios_roi, 2)?>%</td>
			</tr>
			<tr>
				<td class="tdc point">Android</td>
				<td class="tdc">$<?= number_format($and_spend) ?></td>
				<td class="tdc">$<?= number_format($and_ua_sales) ?></td>
				<td class="tdc">$<?= number_format($and_retention_sales) ?></td>
				<td class="tdc">$<?= number_format($and_total_sales) ?></td>
				<td class="tdc"><?= number_format($and_roi, 2)?>%</td>
			</tr>
			<tr>
				<td class="tdc point">Amazon</td>
				<td class="tdc">$<?= number_format($ama_spend) ?></td>
				<td class="tdc">$<?= number_format($ama_ua_sales) ?></td>
				<td class="tdc">$<?= number_format($ama_retention_sales) ?></td>
				<td class="tdc">$<?= number_format($ama_total_sales) ?></td>
				<td class="tdc"><?= number_format($ama_roi, 2)?>%</td>
			</tr>
		</tbody>
	</table>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>