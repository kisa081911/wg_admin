<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	ini_set("memory_limit", "-1");
	check_login();
	
	$search_platform = $_GET["platform"];
	$search_install_sd = $_GET["install_sd"];
	$search_install_ed = $_GET["install_ed"];
	$search_media = ($_GET["media"] == "")? "all" : $_GET["media"];
	
	$filename = "mobile_Install_Raw_Data_excel_".$search_install_sd."_".$search_install_ed.".xls";
	
	$tail = " WHERE install_time BETWEEN '$search_install_sd 00:00:00' AND '$search_install_ed 23:59:59' ";
	
	$order_by = "ORDER BY install_time DESC, media_source ASC ";	
	
	if($search_platform != 0)
		$tail .= "AND platform = $search_platform ";
	
	if($search_media != "all")
	{
		if($search_media == "blank")
			$tail .= "AND media_source = '' ";
		else
			$tail .= "AND media_source = '$search_media' ";
	}
	
	$db_main2 = new CDatabase_Main2();
	
	$sql = "SELECT * FROM tbl_appsflyer_install $tail $order_by";
	$iaplist = $db_main2->gettotallist($sql);
	
	$db_main2->end();
	
	if (sizeof($iaplist) == 0)
		error_go("저장할 데이터가 없습니다.", "mobile_install_download.php");
	
	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
	"<table border=1>".
		"<tr>".
			"<td style='font-weight:bold;'>logidx</td>".
			"<td style='font-weight:bold;'>platform</td>".
			"<td style='font-weight:bold;'>fb_ad_id</td>".
			"<td style='font-weight:bold;'>install_time</td>".
			"<td style='font-weight:bold;'>agency</td>".
			"<td style='font-weight:bold;'>media_source</td>".
			"<td style='font-weight:bold;'>fb_campaign_id</td>".
			"<td style='font-weight:bold;'>fb_campaign_name</td>".
			"<td style='font-weight:bold;'>fb_adset_id</td>".
			"<td style='font-weight:bold;'>fb_adset_name</td>".
			"<td style='font-weight:bold;'>fb_adgroup_name</td>".
			"<td style='font-weight:bold;'>campaign</td>".
			"<td style='font-weight:bold;'>channel</td>".
			"<td style='font-weight:bold;'>keyword</td>".
			"<td style='font-weight:bold;'>site_id</td>".
			"<td style='font-weight:bold;'>sub1</td>".
			"<td style='font-weight:bold;'>sub2</td>".
			"<td style='font-weight:bold;'>sub3</td>".
			"<td style='font-weight:bold;'>sub4</td>".
			"<td style='font-weight:bold;'>sub5</td>".
			"<td style='font-weight:bold;'>country_code</td>".
			"<td style='font-weight:bold;'>city</td>".
			"<td style='font-weight:bold;'>languege</td>".
			"<td style='font-weight:bold;'>appsflyer_device_id</td>".
			"<td style='font-weight:bold;'>adset_id</td>".
			"<td style='font-weight:bold;'>adset</td>".
			"<td style='font-weight:bold;'>ad_id</td>".
			"<td style='font-weight:bold;'>ad</td>".
			"<td style='font-weight:bold;'>adv_id</td>".
			"<td style='font-weight:bold;'>device_type</td>".
		"</tr>";
	
		for ($i=0; $i < sizeof($iaplist); $i++)
		{
			$logidx = $iaplist[$i]["logidx"];
			$platform = $iaplist[$i]["platform"];
			$fb_ad_id = $iaplist[$i]["fb_ad_id"];
  			$install_time = $iaplist[$i]["install_time"];
  			$agency =  $iaplist[$i]["agency"];
  			$media_source =  $iaplist[$i]["media_source"];
  			$fb_campaign_id =  $iaplist[$i]["fb_campaign_id"];
  			$fb_campaign_name =  $iaplist[$i]["fb_campaign_name"];
  			$fb_adset_id =  $iaplist[$i]["fb_adset_id"];
  			$fb_adset_name =  $iaplist[$i]["fb_adset_name"];
  			$fb_adgroup_name =  $iaplist[$i]["fb_adgroup_name"];
  			$campaign = $iaplist[$i]["campaign"];
  			$channel = $iaplist[$i]["channel"];
  			$keyword = $iaplist[$i]["keyword"];
  			$site_id = $iaplist[$i]["site_id"];
  			$sub1 = $iaplist[$i]["sub1"];
  			$sub2 = $iaplist[$i]["sub2"];
  			$sub3 = $iaplist[$i]["sub3"];
  			$sub4 = $iaplist[$i]["sub4"];
  			$sub5 = $iaplist[$i]["sub5"];
  			$country_code =  $iaplist[$i]["country_code"];
  			$city = $iaplist[$i]["city"];
  			$languege =  $iaplist[$i]["languege"];
  			$appsflyer_device_id =  $iaplist[$i]["appsflyer_device_id"];
  			$adset_id = $iaplist[$i]["adset_id"];
  			$adset = $iaplist[$i]["adset"];
  			$ad_id = $iaplist[$i]["ad_id"];
  			$ad = $iaplist[$i]["ad"];
  			$adv_id = $iaplist[$i]["adv_id"];
  			$device_id = $iaplist[$i]["device_id"];
  			$device_type = $iaplist[$i]["device_type"];
  		
  			if ($platform == "1")
  				$platform = "iOS";
  			else if ($platform == "2")
  				$platform = "Android";
  			else if ($platform == "3")
  				$platform = "Amazon";
  			
  			$excel_contents .= "<tr>".
    			"<td>".$logidx."</td>".
		  		"<td>".$platform."</td>".
		  		"<td>".$fb_ad_id."</td>".
		  		"<td>".$install_time."</td>".
		  		"<td>".$agency."</td>".
		  		"<td>".$media_source."</td>".
		  		"<td width='200'>".$fb_campaign_id."</td>".
		  		"<td>".$fb_campaign_name."</td>".
		  		"<td>".$fb_adset_id."</td>".
		  		"<td>".$fb_adset_name."</td>".
		  		"<td>".$fb_adgroup_name."</td>".
		  		"<td>".$campaign."</td>".
		  		"<td>".$channel."</td>".
		  		"<td>".$keyword."</td>".
		  		"<td>".$site_id."</td>".
		  		"<td>".$sub1."</td>".
		  		"<td>".$sub2."</td>".
		  		"<td>".$sub3."</td>".
		  		"<td>".$sub4."</td>".
		  		"<td>".$sub5."</td>".
		  		"<td>".$country_code."</td>".
		  		"<td>".$city."</td>".
		  		"<td>".$languege."</td>".
		  		"<td>".$appsflyer_device_id."</td>".
		  		"<td>".$adset_id."</td>".
		  		"<td>".$adset."</td>".
		  		"<td>".$ad_id."</td>".
		  		"<td>".$ad."</td>".
		  		"<td>".$adv_id."</td>".
		  		"<td>".$device_type."</td>".
	  		"</tr>";
  		}
						  
		$excel_contents .= "</table>";
  
		Header("Content-type: application/x-msdownload");
		Header("Content-type: application/x-msexcel");
		Header("Content-Disposition: attachment; filename=$filename");
  
		echo($excel_contents);
?>