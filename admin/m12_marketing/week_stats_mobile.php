<?
	$top_menu = "marketing";
	$sub_menu = "week_mobile";

	include_once("../common/dbconnect/db_util_redshift.inc.php");
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_startdate = $_GET["start_searchdate"];
	$search_enddate = $_GET["end_searchdate"];
	
	$pay_startdate = $_GET["start_paydate"];
	$pay_enddate = $_GET["end_paydate"];
	
	$search_platform = isset($_GET["search_platform"]) ? $_GET["search_platform"] : "0";
	$search_agency = $_GET["search_agency"];

	$today = date("Y-m-d", strtotime("-1 day"));
	$before_1w = date("Y-m-d", strtotime("-7 day"));
	$before_2w = date("Y-m-d", strtotime("-14 day"));
	$before_4w = date("Y-m-d", strtotime("-28 day"));
	
	if($search_startdate == "")
		$search_startdate = date("Y-m-d", strtotime("-60 day"));
	
	if($search_enddate == "")
		$search_enddate = $today;
	
	if($pay_startdate == "")
		$pay_startdate = date("Y-m-d", strtotime("-60 day"));
	
	if($pay_enddate == "")
		$pay_enddate = $today;
	
	$pagefield = "sdate=$search_startdate&edate=$search_enddate&pay_sdate=$pay_startdate&pay_edate=$pay_enddate&platform=$search_platform&agency=$search_agency";
	
	$search_sql = "";
	$agency_sql = "";
	$spend_sql = "";
	
	if($search_platform == 0)
		$platform_sql = "platform > $search_platform";
	else
		$platform_sql = "platform = $search_platform";
	
	if($search_agency != "")
	{
		$spend_sql = " AND agencyname = '$search_agency' ";
		$agency_sql = " AND adflag LIKE '$search_agency%' ";
	}
	
	$db_main2 = new CDatabase_Main2();
	$db_redshift = new CDatabase_Redshift();
	
	$sql = "SELECT DISTINCT adflag AS agencyname ".
			"FROM t5_user_adflag_mobile_log ".
			"WHERE $platform_sql AND '$search_startdate 00:00:00' <= writedate AND writedate <= '$search_enddate 23:59:59' ".
  			"	AND (adflag LIKE '%_int' OR adflag IN ('Facebook Ads', 'nanigans', 'Twitter', 'amazon', 'Apple Search Ads') ) ".
			"ORDER BY LOWER(adflag) ASC";
	$agency_list = $db_redshift->gettotallist($sql);
	
	$sql = "SELECT WEEKOFYEAR(today) AS week_count, SUM(spend) AS spend ".
			"FROM ( ".
			"	SELECT today, agencyname, ROUND(SUM(spend), 2) AS spend ".
			"	FROM tbl_agency_spend_daily ".
			"	WHERE today BETWEEN '$search_startdate' AND '$search_enddate' AND agencyidx != 8 AND $platform_sql ".
			"	GROUP BY today, agencyname ".
			"	UNION ALL ".
			"	SELECT today, (CASE WHEN campaign_name LIKE 'Nanigans%' THEN 'nanigans' ELSE 'Facebook Ads' END) AS agencyname, ROUND(SUM(spend), 2) AS spend ".
			"	FROM `tbl_ad_stats_mobile` ".
			"	WHERE today BETWEEN '$search_startdate' AND '$search_enddate' AND $platform_sql ".
			"	GROUP BY today ".
			") t1 ".
			"WHERE 1=1 $spend_sql".
			"GROUP BY WEEKOFYEAR(today)";
	$spend_list = $db_main2->gettotallist($sql);
	
	$sql = "SELECT t1.week_count, start_date, end_date, total_cnt, new_cnt, (total_cnt - new_cnt) AS rejoin_cnt, nvl(payer_cnt, 0) AS payer_cnt, nvl(pay_amount, 0) AS pay_amount, nvl(pay_cnt, 0) AS pay_cnt ".
			"FROM ( ".
  			"	SELECT date_part(w, writedate) AS week_count, date(MIN(writedate)) AS start_date, date(MAX(writedate)) AS end_date, COUNT(DISTINCT useridx) AS total_cnt, SUM(case when isnew = 1 then 1 end) AS new_cnt ".
  			"	FROM t5_user_adflag_mobile_log ".
  			"	WHERE $platform_sql AND '$search_startdate 00:00:00' <= writedate AND writedate <= '$search_enddate 23:59:59' ".
    		"		and (adflag like '%_int' OR adflag like '%_viral' OR adflag IN ('Facebook Ads', 'nanigans', 'Twitter', 'amazon', 'Apple Search Ads')) $agency_sql ".
  			"	GROUP BY date_part(w, writedate) ".
			") t1 LEFT JOIN ( ".
  			"	SELECT date_part(w, t1.writedate) AS week_count, COUNT(DISTINCT t2.useridx) AS payer_cnt, SUM(money) AS pay_amount, COUNT(orderidx) AS pay_cnt ".
  			"	FROM ( ".
    		"		SELECT useridx, platform, adflag, isnew, writedate, nvl(enddate, CURRENT_DATE) as enddate ".
    		"		FROM t5_user_adflag_mobile_log ".
    		"		WHERE $platform_sql AND '$search_startdate 00:00:00' <= writedate AND writedate <= '$search_enddate 23:59:59' ".
        	"			and (adflag like '%_int' OR adflag like '%_viral' OR adflag IN ('Facebook Ads', 'nanigans', 'Twitter', 'amazon', 'Apple Search Ads')) $agency_sql ".
  			"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx and t1.writedate <= t2.writedate and t1.enddate > t2.writedate and platform = os_type ".
  			"	WHERE status = 1 AND '$pay_startdate 00:00:00' <= t2.writedate AND t2.writedate <= '$pay_enddate 23:59:59' ".
  			"	GROUP BY date_part(w, t1.writedate) ".
			") t2 ON t1.week_count = t2.week_count ".
			"ORDER BY t1.week_count ASC";
	$week_list = $db_redshift->gettotallist($sql);
	
	$sql = "SELECT WEEKOFYEAR(install_time) AS week_count, SUM(event_value) AS iap_amount ".
			"FROM ( ".
			"	SELECT install_time, event_time, event_value, (CASE WHEN (media_source = 'Facebook Ads' AND agency = 'nanigans') THEN 'nanigans' WHEN (media_source = 'Facebook Ads' AND agency != 'nanigans') THEN 'Facebook Ads' ELSE media_source END) AS agencyname ".
			"	FROM `tbl_appsflyer_inappevent` ".
			"	WHERE $platform_sql AND '$search_startdate 00:00:00' <= install_time AND install_time < '$search_enddate 23:59:59' AND '$pay_startdate 00:00:00' <= event_time AND event_time <= '$pay_enddate 23:59:59' AND media_source != '' AND fb_campaign_name NOT LIKE '%reten%' ".
			") t1 ".
			"WHERE 1=1 $spend_sql ".
			"GROUP BY WEEKOFYEAR(install_time)";
	$apps_iap_list = $db_main2->gettotallist($sql);
	
	// 최근 1주
	$sql = "SELECT SUM(spend) AS spend ".
			"FROM ( ".
			"	SELECT today, agencyname, ROUND(SUM(spend), 2) AS spend ".
			"	FROM tbl_agency_spend_daily ".
			"	WHERE today BETWEEN '$before_1w' AND '$today' AND agencyidx != 8 AND $platform_sql ".
			"	GROUP BY today, agencyname ".
			"	UNION ALL ".
			"	SELECT today, (CASE WHEN campaign_name LIKE 'Nanigans%' THEN 'nanigans' ELSE 'Facebook Ads' END) AS agencyname, ROUND(SUM(spend), 2) AS spend ".
			"	FROM `tbl_ad_stats_mobile` ".
			"	WHERE today BETWEEN '$before_1w' AND '$today' AND $platform_sql ".
			"	GROUP BY today ".
			") t1 ".
			"WHERE 1=1 $spend_sql";
	$spend_1w = $db_main2->getvalue($sql);
	
	$sql = "SELECT start_date, end_date, total_cnt, new_cnt, (total_cnt - new_cnt) AS rejoin_cnt, nvl(payer_cnt, 0) AS payer_cnt, nvl(pay_amount, 0) AS pay_amount, nvl(pay_cnt, 0) AS pay_cnt ".
			"FROM ( ".
			"	SELECT 0 AS week_count, date(MIN(writedate)) AS start_date, date(MAX(writedate)) AS end_date, COUNT(DISTINCT useridx) AS total_cnt, SUM(case when isnew = 1 then 1 end) AS new_cnt ".
			"	FROM t5_user_adflag_mobile_log ".
			"	WHERE $platform_sql AND '$before_1w 00:00:00' <= writedate AND writedate <= '$today 23:59:59' ".
			"		and (adflag like '%_int' OR adflag like '%_viral' OR adflag IN ('Facebook Ads', 'nanigans', 'Twitter', 'amazon', 'Apple Search Ads')) $agency_sql ".
			") t1 LEFT JOIN ( ".
			"	SELECT 0 AS week_count, COUNT(DISTINCT t2.useridx) AS payer_cnt, SUM(money) AS pay_amount, COUNT(orderidx) AS pay_cnt ".
			"	FROM ( ".
			"		SELECT useridx, platform, adflag, isnew, writedate, nvl(enddate, CURRENT_DATE) as enddate ".
			"		FROM t5_user_adflag_mobile_log ".
			"		WHERE $platform_sql AND '$before_1w 00:00:00' <= writedate AND writedate <= '$today 23:59:59' ".
			"			and (adflag like '%_int' OR adflag like '%_viral' OR adflag IN ('Facebook Ads', 'nanigans', 'Twitter', 'amazon', 'Apple Search Ads')) $agency_sql ".
			"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx and t1.writedate <= t2.writedate and t1.enddate > t2.writedate and platform = os_type ".
			"	WHERE status = 1 AND '$before_1w 00:00:00' <= t2.writedate AND t2.writedate <= '$today 23:59:59' ".
			") t2 ON t1.week_count = t2.week_count";
	$week_list_1w = $db_redshift->getarray($sql);
	
	$sql = "SELECT SUM(event_value) AS iap_amount ".
			"FROM ( ".
			"	SELECT install_time, event_time, event_value, (CASE WHEN (media_source = 'Facebook Ads' AND agency = 'nanigans') THEN 'nanigans' WHEN (media_source = 'Facebook Ads' AND agency != 'nanigans') THEN 'Facebook Ads' ELSE media_source END) AS agencyname ".
			"	FROM `tbl_appsflyer_inappevent` ".
			"	WHERE $platform_sql AND '$before_1w 00:00:00' <= install_time AND install_time < '$today 23:59:59' AND '$before_1w 00:00:00' <= event_time AND event_time <= '$today 23:59:59' AND media_source != '' AND fb_campaign_name NOT LIKE '%reten%' ".
			") t1 ".
			"WHERE 1=1 $spend_sql";
	$iap_1w = $db_main2->getvalue($sql);
	
	// 최근2주
	$sql = "SELECT SUM(spend) AS spend ".
			"FROM ( ".
			"	SELECT today, agencyname, ROUND(SUM(spend), 2) AS spend ".
			"	FROM tbl_agency_spend_daily ".
			"	WHERE today BETWEEN '$before_2w' AND '$today' AND agencyidx != 8 AND $platform_sql ".
			"	GROUP BY today, agencyname ".
			"	UNION ALL ".
			"	SELECT today, (CASE WHEN campaign_name LIKE 'Nanigans%' THEN 'nanigans' ELSE 'Facebook Ads' END) AS agencyname, ROUND(SUM(spend), 2) AS spend ".
			"	FROM `tbl_ad_stats_mobile` ".
			"	WHERE today BETWEEN '$before_2w' AND '$today' AND $platform_sql ".
			"	GROUP BY today ".
			") t1 ".
			"WHERE 1=1 $spend_sql";
	$spend_2w = $db_main2->getvalue($sql);
	
	$sql = "SELECT start_date, end_date, total_cnt, new_cnt, (total_cnt - new_cnt) AS rejoin_cnt, nvl(payer_cnt, 0) AS payer_cnt, nvl(pay_amount, 0) AS pay_amount, nvl(pay_cnt, 0) AS pay_cnt ".
			"FROM ( ".
			"	SELECT 0 AS week_count, date(MIN(writedate)) AS start_date, date(MAX(writedate)) AS end_date, COUNT(DISTINCT useridx) AS total_cnt, SUM(case when isnew = 1 then 1 end) AS new_cnt ".
			"	FROM t5_user_adflag_mobile_log ".
			"	WHERE $platform_sql AND '$before_2w 00:00:00' <= writedate AND writedate <= '$today 23:59:59' ".
			"		and (adflag like '%_int' OR adflag like '%_viral' OR adflag IN ('Facebook Ads', 'nanigans', 'Twitter', 'amazon', 'Apple Search Ads')) $agency_sql ".
			") t1 LEFT JOIN ( ".
			"	SELECT 0 AS week_count, COUNT(DISTINCT t2.useridx) AS payer_cnt, SUM(money) AS pay_amount, COUNT(orderidx) AS pay_cnt ".
			"	FROM ( ".
			"		SELECT useridx, platform, adflag, isnew, writedate, nvl(enddate, CURRENT_DATE) as enddate ".
			"		FROM t5_user_adflag_mobile_log ".
			"		WHERE $platform_sql AND '$before_2w 00:00:00' <= writedate AND writedate <= '$today 23:59:59' ".
			"			and (adflag like '%_int' OR adflag like '%_viral' OR adflag IN ('Facebook Ads', 'nanigans', 'Twitter', 'amazon', 'Apple Search Ads')) $agency_sql ".
			"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx and t1.writedate <= t2.writedate and t1.enddate > t2.writedate and platform = os_type ".
			"	WHERE status = 1 AND '$before_2w 00:00:00' <= t2.writedate AND t2.writedate <= '$today 23:59:59' ".
			") t2 ON t1.week_count = t2.week_count";
	$week_list_2w = $db_redshift->getarray($sql);
	
	$sql = "SELECT SUM(event_value) AS iap_amount ".
			"FROM ( ".
			"	SELECT install_time, event_time, event_value, (CASE WHEN (media_source = 'Facebook Ads' AND agency = 'nanigans') THEN 'nanigans' WHEN (media_source = 'Facebook Ads' AND agency != 'nanigans') THEN 'Facebook Ads' ELSE media_source END) AS agencyname ".
			"	FROM `tbl_appsflyer_inappevent` ".
			"	WHERE $platform_sql AND '$before_2w 00:00:00' <= install_time AND install_time < '$today 23:59:59' AND '$before_2w 00:00:00' <= event_time AND event_time <= '$today 23:59:59' AND media_source != '' AND fb_campaign_name NOT LIKE '%reten%' ".
			") t1 ".
			"WHERE 1=1 $spend_sql";
	$iap_2w = $db_main2->getvalue($sql);
	
	// 최근 4주
	$sql = "SELECT SUM(spend) AS spend ".
			"FROM ( ".
			"	SELECT today, agencyname, ROUND(SUM(spend), 2) AS spend ".
			"	FROM tbl_agency_spend_daily ".
			"	WHERE today BETWEEN '$before_4w' AND '$today' AND agencyidx != 8 AND $platform_sql ".
			"	GROUP BY today, agencyname ".
			"	UNION ALL ".
			"	SELECT today, (CASE WHEN campaign_name LIKE 'Nanigans%' THEN 'nanigans' ELSE 'Facebook Ads' END) AS agencyname, ROUND(SUM(spend), 2) AS spend ".
			"	FROM `tbl_ad_stats_mobile` ".
			"	WHERE today BETWEEN '$before_4w' AND '$today' AND $platform_sql ".
			"	GROUP BY today ".
			") t1 ".
			"WHERE 1=1 $spend_sql";
	$spend_4w = $db_main2->getvalue($sql);
	
	$sql = "SELECT start_date, end_date, total_cnt, new_cnt, (total_cnt - new_cnt) AS rejoin_cnt, nvl(payer_cnt, 0) AS payer_cnt, nvl(pay_amount, 0) AS pay_amount, nvl(pay_cnt, 0) AS pay_cnt ".
			"FROM ( ".
			"	SELECT 0 AS week_count, date(MIN(writedate)) AS start_date, date(MAX(writedate)) AS end_date, COUNT(DISTINCT useridx) AS total_cnt, SUM(case when isnew = 1 then 1 end) AS new_cnt ".
			"	FROM t5_user_adflag_mobile_log ".
			"	WHERE $platform_sql AND '$before_4w 00:00:00' <= writedate AND writedate <= '$today 23:59:59' ".
			"		and (adflag like '%_int' OR adflag like '%_viral' OR adflag IN ('Facebook Ads', 'nanigans', 'Twitter', 'amazon', 'Apple Search Ads')) $agency_sql ".
			") t1 LEFT JOIN ( ".
			"	SELECT 0 AS week_count, COUNT(DISTINCT t2.useridx) AS payer_cnt, SUM(money) AS pay_amount, COUNT(orderidx) AS pay_cnt ".
			"	FROM ( ".
			"		SELECT useridx, platform, adflag, isnew, writedate, nvl(enddate, CURRENT_DATE) as enddate ".
			"		FROM t5_user_adflag_mobile_log ".
			"		WHERE $platform_sql AND '$before_4w 00:00:00' <= writedate AND writedate <= '$today 23:59:59' ".
			"			and (adflag like '%_int' OR adflag like '%_viral' OR adflag IN ('Facebook Ads', 'nanigans', 'Twitter', 'amazon', 'Apple Search Ads')) $agency_sql ".
			"	) t1 join t5_product_order_mobile t2 on t1.useridx = t2.useridx and t1.writedate <= t2.writedate and t1.enddate > t2.writedate and platform = os_type ".
			"	WHERE status = 1 AND '$before_4w 00:00:00' <= t2.writedate AND t2.writedate <= '$today 23:59:59' ".
			") t2 ON t1.week_count = t2.week_count";
	$week_list_4w = $db_redshift->getarray($sql);
	
	$sql = "SELECT SUM(event_value) AS iap_amount ".
			"FROM ( ".
			"	SELECT install_time, event_time, event_value, (CASE WHEN (media_source = 'Facebook Ads' AND agency = 'nanigans') THEN 'nanigans' WHEN (media_source = 'Facebook Ads' AND agency != 'nanigans') THEN 'Facebook Ads' ELSE media_source END) AS agencyname ".
			"	FROM `tbl_appsflyer_inappevent` ".
			"	WHERE $platform_sql AND '$before_4w 00:00:00' <= install_time AND install_time < '$today 23:59:59' AND '$before_4w 00:00:00' <= event_time AND event_time <= '$today 23:59:59' AND media_source != '' AND fb_campaign_name NOT LIKE '%reten%' ".
			") t1 ".
			"WHERE 1=1 $spend_sql";
	$iap_4w = $db_main2->getvalue($sql);

	$db_main2->end();
	$db_redshift->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">	
	$(function() {
		$("#start_searchdate").datepicker({});
		$("#end_searchdate").datepicker({});
		$("#start_paydate").datepicker({});
		$("#end_paydate").datepicker({});
	});
		
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.start_searchdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.start_searchdate.focus();
	        return;
	    } 

	    if (search_form.end_searchdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.end_searchdate.focus();
	        return;
	    } 
	
	    search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 광고 누적 퍼포먼스 - Mobile</div>
	</div>
	<!-- //title_warp -->
	
	<form name="search_form" id="search_form"  method="get">
		<div class="detail_search_wrap">
			<span class="search_lbl" style="padding-left:270px;">플랫폼</span>
			<span class="search_lbl ml10">
				<select id="search_platform" name="search_platform">
					<option value="0" <?= ($search_platform==0) ? "selected" : "" ?>>전체</option>
					<option value="1" <?= ($search_platform==1) ? "selected" : "" ?>>iOS</option>
					<option value="2" <?= ($search_platform==2) ? "selected" : "" ?>>Android</option>
					<option value="3" <?= ($search_platform==3) ? "selected" : "" ?>>Amazon</option>
				</select>
			</span>
			<span class="search_lbl" style="padding-left: 177px;">Agency</span>
			<span class="search_lbl ml10">
				<select id="search_agency" name="search_agency">
					<option value="">전체</option>
<?
				for($i=0; $i<sizeof($agency_list); $i++)
				{
					$agency_name = $agency_list[$i]["agencyname"];
?>
					<option value="<?= $agency_name?>" <?= ($search_agency==$agency_name) ? "selected" : "" ?>><?= $agency_name?></option>
<?
				}
?>
				</select>
			</span>

			<div class="clear" style="height:5px;"></div>
			
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
			
			<span class="search_lbl" style="padding-left:270px;">기준일</span>
			<span class="search_lbl ml10">
				<input type="text" class="search_text" id="start_searchdate" name="start_searchdate" value="<?= $search_startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_searchdate" name="end_searchdate" value="<?= $search_enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			</span>
			
			<span class="search_lbl" style="padding-left: 57px;">결제일</span>
			<span class="search_lbl ml10">
				<input type="text" class="search_text" id="start_paydate" name="start_paydate" value="<?= $pay_startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_paydate" name="end_paydate" value="<?= $pay_enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			</span>
		</div>
	</form>

	<div class="search_result">
		<span><?= $search_startdate ?></span> ~ <span><?= $search_enddate ?></span> 통계입니다
		<input type="button" class="btn_03" value="Excel 다운로드" onclick="window.location.href='week_stats_mobile_excel.php?<?= $pagefield ?>'">
	</div>
	
	<table class="tbl_list_basic1" style="width:1200px;">
		<colgroup>
			<col width="130">
			<col width="50">
			<col width="40">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="40">
            <col width="45">
            <col width="50">
            <col width="40">
            <col width="40">
            <col width="40">
		</colgroup>
        <thead>
        	<tr>
        		<th class="tdc">구간</th>
            	<th class="tdc">유입수</th>
             	<th class="tdc">CPI</th>
               	<th class="tdc">비용</th>
               	<th class="tdc">총결제</th>
               	<th class="tdc">ROI</th>
               	<th class="tdc">총결제(A)</th>
               	<th class="tdc">ROI(A)</th>
               	<th class="tdc">결제자수 </th>
               	<th class="tdc">결제자 비율</th>
               	<th class="tdc">결제건수</th>
               	<th class="tdc">ARPU</th>
               	<th class="tdc">ARPPU</th>
         	</tr>
        </thead>
		<tbody>
<?
	$total_install = 0;
	$total_spend = 0;
	$total_cpi = 0;
	$total_pay_amount = 0;
	$total_iap_amount = 0;
	$total_roi = 0;
	$total_payer_count = 0;
	$total_pay_count = 0;
	$total_payer_rate = 0;
	$total_arpu = 0;
	$total_arppu = 0;

	for($i=0; $i<sizeof($week_list); $i++)
	{
		$spend = 0;
		$iap_amount = 0;
		$week_count = $week_list[$i]["week_count"];
		$start_date = $week_list[$i]["start_date"];
		$end_date = $week_list[$i]["end_date"];
		$total_cnt = $week_list[$i]["total_cnt"];

		for($j=0; $j<sizeof($spend_list); $j++)
		{
			if($spend_list[$j]["week_count"] == $week_count)
			{
				$spend = $spend_list[$j]["spend"];
				break;
			}
		}
		
		for($j=0; $j<sizeof($apps_iap_list); $j++)
		{
			if($apps_iap_list[$j]["week_count"] == $week_count)
			{
				$iap_amount = $apps_iap_list[$j]["iap_amount"];
				break;
			}
		}
		
		$cpi = ($total_cnt == 0) ? 0 : round($spend/$total_cnt, 2);
		$pay_amount = $week_list[$i]["pay_amount"];
		$roi = ($spend == 0) ? 0 : round($pay_amount/$spend*100, 2);
		$apps_roi = ($spend == 0) ? 0 : round($iap_amount/$spend*100, 2);
		$payer_cnt = $week_list[$i]["payer_cnt"];
		$payer_rate = ($total_cnt == 0) ? 0 : round($payer_cnt/$total_cnt*100, 2);
		$pay_count = $week_list[$i]["pay_cnt"];
		$arpu = ($total_cnt == 0) ? 0 : round($pay_amount/$total_cnt, 2);
		$arppu = ($payer_cnt == 0) ? 0 : round($pay_amount/$payer_cnt, 2);
		
		$total_install += $total_cnt;
		$total_spend += $spend;
		$total_pay_amount += $pay_amount;
		$total_iap_amount += $iap_amount;
		$total_payer_count += $payer_cnt;
		$total_pay_count += $pay_count;
?>    
			<tr>
        		<td class="tdc point">구간 <?= $week_count ?> - <?= $start_date?> ~ <?= $end_date?></td>
            	<td class="tdc"><?= number_format($total_cnt) ?></td>
             	<td class="tdc">$<?=number_format($cpi, 2)?></td>
               	<td class="tdc">$<?=number_format($spend, 2)?></td>
               	<td class="tdc">$<?=number_format($pay_amount, 2)?></td>
               	<td class="tdc"><?=number_format($roi, 2)?>%</td>
               	<td class="tdc">$<?=number_format($iap_amount, 2)?></td>
               	<td class="tdc"><?=number_format($apps_roi, 2)?>%</td>
               	<td class="tdc"><?=number_format($payer_cnt)?></td>
               	<td class="tdc"><?=number_format($payer_rate,2)?>%</td>
               	<td class="tdc"><?=number_format($pay_count)?></td>
               	<td class="tdc"><?=number_format($arpu, 2)?></td>
               	<td class="tdc"><?=number_format($arppu, 2)?></td>
         	</tr>
<?
	}
	
	$total_cpi = ($total_install == 0) ? 0 : round($total_spend/$total_install, 2);
	$total_roi = ($total_spend == 0) ? 0 : round($total_pay_amount/$total_spend*100, 2);
	$total_apps_roi = ($total_spend == 0) ? 0 : round($total_iap_amount/$total_spend*100, 2);
	$total_payer_rate = ($total_install == 0) ? 0 : round($total_payer_count/$total_install*100, 2);
	$total_arpu = ($total_install == 0) ? 0 : round($total_pay_amount/$total_install, 2);
	$total_arppu = ($total_payer_count == 0) ? 0 : round($total_pay_amount/$total_payer_count, 2);
?>
			<tr>
        		<td class="tdc point_title">합계</td>
            	<td class="tdc point"><?= number_format($total_install) ?></td>
             	<td class="tdc point">$<?= number_format($total_cpi, 2) ?></td>
               	<td class="tdc point">$<?= number_format($total_spend, 2) ?></td>
               	<td class="tdc point">$<?= number_format($total_pay_amount, 2) ?></td>
               	<td class="tdc point"><?= number_format($total_roi, 2) ?>%</td>
               	<td class="tdc point">$<?= number_format($total_iap_amount, 2) ?></td>
               	<td class="tdc point"><?= number_format($total_apps_roi, 2) ?>%</td>
               	<td class="tdc point"><?= number_format($total_payer_count) ?></td>
               	<td class="tdc point"><?= number_format($total_payer_rate, 2) ?>%</td>
               	<td class="tdc point"><?=number_format($total_pay_count)?></td>
               	<td class="tdc point"><?=number_format($total_arpu, 2)?></td>
               	<td class="tdc point"><?=number_format($total_arppu, 2)?></td>
         	</tr>
        </tbody>
	</table>
	
	<div class="clear"></div>
	<br/><br/>

	<table class="tbl_list_basic1" style="width:1200px;">
		<colgroup>
			<col width="130">
			<col width="50">
			<col width="40">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="40">
            <col width="45">
            <col width="50">
            <col width="40">
            <col width="40">
            <col width="40">
		</colgroup>
        <thead>
        	<tr>
        		<th class="tdc">구간</th>
            	<th class="tdc">유입수</th>
             	<th class="tdc">CPI</th>
               	<th class="tdc">비용</th>
               	<th class="tdc">총결제</th>
               	<th class="tdc">ROI</th>
               	<th class="tdc">총결제(A)</th>
               	<th class="tdc">ROI(A)</th>
               	<th class="tdc">결제자수 </th>
               	<th class="tdc">결제자 비율</th>
               	<th class="tdc">결제건수</th>
               	<th class="tdc">ARPU</th>
               	<th class="tdc">ARPPU</th>
         	</tr>
        </thead>
		<tbody>
			<tr>
<?
	$total_cnt_1w = $week_list_1w["total_cnt"];
	$cpi_1w = ($total_cnt_1w == 0) ? 0 : round($spend_1w/$total_cnt_1w, 2);
	$pay_amount_1w = $week_list_1w["pay_amount"];
	$roi_1w = ($spend_1w == 0) ? 0 : round($pay_amount_1w/$spend_1w*100, 2);
	$apps_roi_1w = ($spend_1w == 0) ? 0 : round($iap_1w/$spend_1w*100, 2);
	$payer_cnt_1w = $week_list_1w["payer_cnt"];
	$payer_rate_1w = ($total_cnt_1w == 0) ? 0 : round($payer_cnt_1w/$total_cnt_1w*100, 2);
	$pay_count_1w = $week_list_1w["pay_cnt"];
	$arpu_1w = ($total_cnt_1w == 0) ? 0 : round($pay_amount_1w/$total_cnt_1w, 2);
	$arppu_1w = ($payer_cnt_1w == 0) ? 0 : round($pay_amount_1w/$payer_cnt_1w, 2);
?>
        		<td class="tdc point">최근 1주(<?= $before_1w?> ~ <?= $today?>)</td>
            	<td class="tdc"><?= number_format($total_cnt_1w) ?></td>
             	<td class="tdc">$<?=number_format($cpi_1w, 2)?></td>
               	<td class="tdc">$<?=number_format($spend_1w, 2)?></td>
               	<td class="tdc">$<?=number_format($pay_amount_1w, 2)?></td>
               	<td class="tdc"><?=number_format($roi_1w, 2)?>%</td>
               	<td class="tdc">$<?=number_format($iap_1w, 2)?></td>
               	<td class="tdc"><?=number_format($apps_roi_1w, 2)?>%</td>
               	<td class="tdc"><?=number_format($payer_cnt_1w)?></td>
               	<td class="tdc"><?=number_format($payer_rate_1w, 2)?>%</td>
               	<td class="tdc"><?=number_format($pay_count_1w)?></td>
               	<td class="tdc"><?=number_format($arpu_1w, 2)?></td>
               	<td class="tdc"><?=number_format($arppu_1w, 2)?></td>
         	</tr>
         	<tr>
<?
	$total_cnt_2w = $week_list_2w["total_cnt"];
	$cpi_2w = ($total_cnt_2w == 0) ? 0 : round($spend_2w/$total_cnt_2w, 2);
	$pay_amount_2w = $week_list_2w["pay_amount"];
	$roi_2w = ($spend_2w == 0) ? 0 : round($pay_amount_2w/$spend_2w*100, 2);
	$apps_roi_2w = ($spend_2w == 0) ? 0 : round($iap_2w/$spend_2w*100, 2);
	$payer_cnt_2w = $week_list_2w["payer_cnt"];
	$payer_rate_2w = ($total_cnt_2w == 0) ? 0 : round($payer_cnt_2w/$total_cnt_2w*100, 2);
	$pay_count_2w = $week_list_2w["pay_cnt"];
	$arpu_2w = ($total_cnt_2w == 0) ? 0 : round($pay_amount_2w/$total_cnt_2w, 2);
	$arppu_2w = ($payer_cnt_2w == 0) ? 0 : round($pay_amount_2w/$payer_cnt_2w, 2);
?>
        		<td class="tdc point">최근 2주(<?= $before_2w?> ~ <?= $today?>)</td>
            	<td class="tdc"><?= number_format($total_cnt_2w) ?></td>
             	<td class="tdc">$<?=number_format($cpi_2w, 2)?></td>
               	<td class="tdc">$<?=number_format($spend_2w, 2)?></td>
               	<td class="tdc">$<?=number_format($pay_amount_2w, 2)?></td>
               	<td class="tdc"><?=number_format($roi_2w, 2)?>%</td>
               	<td class="tdc">$<?=number_format($iap_2w, 2)?></td>
               	<td class="tdc"><?=number_format($apps_roi_2w, 2)?>%</td>
               	<td class="tdc"><?=number_format($payer_cnt_2w)?></td>
               	<td class="tdc"><?=number_format($payer_rate_2w, 2)?>%</td>
               	<td class="tdc"><?=number_format($pay_count_2w)?></td>
               	<td class="tdc"><?=number_format($arpu_2w, 2)?></td>
               	<td class="tdc"><?=number_format($arppu_2w, 2)?></td>
         	</tr>
         	<tr>
<?
	$total_cnt_4w = $week_list_4w["total_cnt"];
	$cpi_4w = ($total_cnt_4w == 0) ? 0 : round($spend_4w/$total_cnt_4w, 2);
	$pay_amount_4w = $week_list_4w["pay_amount"];
	$roi_4w = ($spend_4w == 0) ? 0 : round($pay_amount_4w/$spend_4w*100, 2);
	$apps_roi_4w = ($spend_4w == 0) ? 0 : round($iap_4w/$spend_4w*100, 2);
	$payer_cnt_4w = $week_list_4w["payer_cnt"];
	$payer_rate_4w = ($total_cnt_4w == 0) ? 0 : round($payer_cnt_4w/$total_cnt_4w*100, 2);
	$pay_count_4w = $week_list_4w["pay_cnt"];
	$arpu_4w = ($total_cnt_4w == 0) ? 0 : round($pay_amount_4w/$total_cnt_4w, 2);
	$arppu_4w = ($payer_cnt_4w == 0) ? 0 : round($pay_amount_4w/$payer_cnt_4w, 2);
?>
        		<td class="tdc point">최근 4주(<?= $before_4w?> ~ <?= $today?>)</td>
            	<td class="tdc"><?= number_format($total_cnt_4w) ?></td>
             	<td class="tdc">$<?=number_format($cpi_4w, 2)?></td>
               	<td class="tdc">$<?=number_format($spend_4w, 2)?></td>
               	<td class="tdc">$<?=number_format($pay_amount_4w, 2)?></td>
               	<td class="tdc"><?=number_format($roi_4w, 2)?>%</td>
               	<td class="tdc">$<?=number_format($iap_4w, 2)?></td>
               	<td class="tdc"><?=number_format($apps_roi_4w, 2)?>%</td>
               	<td class="tdc"><?=number_format($payer_cnt_4w)?></td>
               	<td class="tdc"><?=number_format($payer_rate_4w, 2)?>%</td>
               	<td class="tdc"><?=number_format($pay_count_4w)?></td>
               	<td class="tdc"><?=number_format($arpu_4w, 2)?></td>
               	<td class="tdc"><?=number_format($arppu_4w, 2)?></td>
         	</tr>
        </tbody>
	</table>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>