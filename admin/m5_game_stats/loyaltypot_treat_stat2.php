<?
	$top_menu = "game_stats";
	$sub_menu = "loyaltypot_treat_stat";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_type = ($_GET["search_type"] == "") ? 0 : $_GET["search_type"];
	
	if($_GET["start_date"] == "")
		$search_sdate = date("Y-m-d",strtotime("-13 days"));
	else
		$search_sdate = $_GET["start_date"];
	
	if($_GET["end_date"] == "")
		$search_edate = date("Y-m-d");
	else
		$search_edate = $_GET["end_date"];
	
	$start_date = $search_date;
	
	$db_analysis = new CDatabase_Analysis();
	
	if($search_type == 0) //total
	{	
		$search_type_str = "Total";
		$sql = "  SELECT today, SUM(total_user) AS total_user, SUM(more_loyal_treat) AS more_loyal,  SUM(more_treat) AS more_treat".
				" 		, SUM(more_treat_total_loyal) AS more_treat_total_loyal , SUM(more_treat_total_treat) AS more_treat_total_treat".
				" 		, SUM(more_loyal_total_loyal) AS more_loyal_total_loyal , SUM(more_loyal_total_treat) AS more_loyal_total_treat".
				" FROM `tbl_weight_loyaltypot_treat` ".
				" WHERE '$search_sdate' <= today AND today <= '$search_edate' ".
				" GROUP BY today";
		$data_list=$db_analysis->gettotallist($sql);
	}
	else if($search_type == 2) //결제자
	{	
		$search_type_str = "결제자";
		$sql = "  SELECT today, SUM(total_user) AS total_user, SUM(more_loyal_treat) AS more_loyal,  SUM(more_treat) AS more_treat". 		
				" 		, SUM(more_treat_total_loyal) AS more_treat_total_loyal , SUM(more_treat_total_treat) AS more_treat_total_treat".
				" 		, SUM(more_loyal_total_loyal) AS more_loyal_total_loyal , SUM(more_loyal_total_treat) AS more_loyal_total_treat".
				" FROM `tbl_weight_loyaltypot_treat` ".
				" WHERE '$search_sdate' <= today AND today <= '$search_edate' ".
				" AND typeid = 2".
				" GROUP BY today";
		$data_list=$db_analysis->gettotallist($sql);
	}
	else if($search_type == 1)//비결제자
	{	
		$search_type_str = "비 결제자";
		$sql = "  SELECT today, SUM(total_user) AS total_user, SUM(more_loyal_treat) AS more_loyal,  SUM(more_treat) AS more_treat". 		
				" 		, SUM(more_treat_total_loyal) AS more_treat_total_loyal , SUM(more_treat_total_treat) AS more_treat_total_treat".
				" 		, SUM(more_loyal_total_loyal) AS more_loyal_total_loyal , SUM(more_loyal_total_treat) AS more_loyal_total_treat".
				" FROM `tbl_weight_loyaltypot_treat` ".
				" WHERE '$search_sdate' <= today AND today <= '$search_edate' ".
				" AND typeid = 1".
				" GROUP BY today";
		$data_list=$db_analysis->gettotallist($sql);
	}
	$db_analysis->end();	
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	$(function() {
		$("#start_date").datepicker({ });
		$("#end_date").datepicker({ });
	});

	google.load("visualization", "1", {packages:["corechart"]});
	
	function drawChart() 
	{

		var data1 = new google.visualization.DataTable();
	    
		data1.addColumn('string', '날짜');
		data1.addColumn('number', '로열티가 더 많은 유저(%)');	
		data1.addColumn('number', '트리트가 더 많은 유저(%)');
		data1.addRows([
<?
	    for ($i=0; $i<sizeof($data_list); $i++)
	    {
	    	$totaltreat = $data_list[$i]["more_loyal"]+$data_list[$i]["more_treat"];
	    	$more_loyal_rate = round($data_list[$i]["more_loyal"]/$totaltreat*100, 1);
	    	$more_treat_rate = round($data_list[$i]["more_treat"]/$totaltreat*100, 1);
	    	
	    	echo("['".$data_list[$i]["today"]."'");
	    	echo(",{v:".$more_loyal_rate.",f:'".$more_loyal_rate."'}");
	    	echo(",{v:".$more_treat_rate.",f:'".$more_treat_rate."'}]");
	    	
	    	if($i+1<sizeof($data_list))
	    		echo(",");
		}
?>
		]);
		
		var data2 = new google.visualization.DataTable();
	    
		data2.addColumn('string', '날짜');
		data2.addColumn('number', '로열티 보유 금액');	
		data2.addColumn('number', '트리트 보유 금액');
		data2.addRows([
<?
	    for ($j=0; $j<sizeof($data_list); $j++)
	    {
	    	echo("['".$data_list[$j]["today"]."'");
	    	echo(",{v:".$data_list[$j]["more_treat_total_loyal"].",f:'".make_price_format($data_list[$j]["more_treat_total_loyal"])."'}");
	    	echo(",{v:".$data_list[$j]["more_treat_total_treat"].",f:'".make_price_format($data_list[$j]["more_treat_total_treat"])."'}]");
	    	
	    	if($j+1<sizeof($data_list))
	    		echo(",");

		}
?>
		]);
		
		var data3 = new google.visualization.DataTable();
	    
		data3.addColumn('string', '날짜');
		data3.addColumn('number', '로열티 보유 금액');	
		data3.addColumn('number', '트리트 보유 금액');
		data3.addRows([
<?
	    for ($j=0; $j<sizeof($data_list); $j++)
	    {
	    	echo("['".$data_list[$j]["today"]."'");
	    	echo(",{v:".$data_list[$j]["more_loyal_total_loyal"].",f:'".make_price_format($data_list[$j]["more_loyal_total_loyal"])."'}");
	    	echo(",{v:".$data_list[$j]["more_loyal_total_treat"].",f:'".make_price_format($data_list[$j]["more_loyal_total_treat"])."'}]");
	    	
	    	if($j+1<sizeof($data_list))
	    		echo(",");

		}
?>
		]);

 		var options = {
 	            title:'',                                                      
 	            width:1050,                         
 	            height:200,
 	            axisTitlesPosition:'in',
 	            curveType:'none',
 	            focusTarget:'category',
 	            interpolateNulls:'true',
 	            legend:'top',
 	            fontSize : 12,
 	            chartArea:{left:80,top:40,width:1020,height:130}
 	    };

 		var chart = new google.visualization.LineChart(document.getElementById('chart_data1'));
		chart.draw(data1, options);
		 
		chart = new google.visualization.LineChart(document.getElementById('chart_data2'));
		chart.draw(data2, options);
		
		chart = new google.visualization.LineChart(document.getElementById('chart_data3'));
		chart.draw(data3, options);
		                                           		
	}

	google.setOnLoadCallback(drawChart);

	function search()
	{
		var search_form = document.search_form;
	    
		if (search_form.start_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.start_date.focus();
	    	return;
		} 
	
		if (search_form.end_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.end_date.focus();
	    	return;
		} 
	
		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="loyaltypot_treat_stat.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Treat loyalty 밸런스 통계 </div>
			<div class="search_box">
			타입
			<select name="search_type" id="search_type">
				<option value = "0" <?= ($search_type == 0) ? "selected" : "" ?>>Total</option>
				<option value = "2" <?= ($search_type == 2) ? "selected" : "" ?>>결제자</option>
				<option value = "1" <?= ($search_type == 1) ? "selected" : "" ?>>비결제자</option>
			</select>	
			<span class="search_lbl">기준일&nbsp;&nbsp;&nbsp;</span>
			<input type="text" class="search_text" id="start_date" name="start_date" style="width:65px" readonly="readonly" value="<?= $search_sdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/> ~
			<input type="text" class="search_text" id="end_date" name="end_date" style="width:65px" readonly="readonly" value="<?= $search_edate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/>
			<input type="button" class="btn_search" value="검색" onclick="search()" />
		</div>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $search_sdate ?></span> ~ <span><?= $search_edate ?></span> 통계입니다
	</div>
     	<div class="h2_title">[ <?= $search_type_str ?> Treat loyalt 비율]</div>
	 	<div id="chart_data1" style="height:330px; min-width: 500px"></div>
	 		       
     	<div class="h2_title">[ <?= $search_type_str ?> Treat loyalt 보유금액  - 트리트가 더많은 유저]</div>
	 	<div id="chart_data2" style="height:230px; min-width: 500px"></div>
	 		       
     	<div class="h2_title">[ <?= $search_type_str ?> Treat loyalt 보유금액 - 로열티가 더많은 유저]</div>
	 	<div id="chart_data3" style="height:230px; min-width: 500px"></div>	       
    </form>	
</div>
<!--  //CONTENTS WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
