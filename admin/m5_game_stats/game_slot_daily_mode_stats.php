<?
    $top_menu = "game_stats";
    $sub_menu = "game_slot_daily_mode_stats";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $os_term = ($_GET["os_term"] == "") ? "0" : $_GET["os_term"];
    $tab = ($_GET["tab"] == "") ? "0" :$_GET["tab"];
    $viewmode = ($_GET["viewmode"] == "") ? "0" :$_GET["viewmode"];
    $startdate = ($_GET["startdate"] == "") ? date("Y-m-d", mktime(0,0,0,date("m"),date("d")-15,date("Y"))) : $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
    $pagename = "game_slot_daily_mode_stats.php";
    
    $mode_list = array(
    		array("mode_name"=>"신규가입자 1일차(11)",			"mode_num"=>11,	"mode_index"=>0, "mode_tip"=>"신규가입"),
    		array("mode_name"=>"재방문자(30일) 1일차(17)",		"mode_num"=>17,	"mode_index"=>1, "mode_tip"=>"재방문자"),
    		array("mode_name"=>"플래티넘 1일차(18)",			"mode_num"=>18,	"mode_index"=>2, "mode_tip"=>"플래티넘"),
    		array("mode_name"=>"첫결제자(12)",				"mode_num"=>12,	"mode_index"=>3, "mode_tip"=>"첫결제"),
    		array("mode_name"=>"$5이상 재결제자(1차)(19)",		"mode_num"=>19,	"mode_index"=>4, "mode_tip"=>"$5 재결제자(1차)"),
    		array("mode_name"=>"$5이상 재결제자(2차)(20)",	 	"mode_num"=>20,	"mode_index"=>5, "mode_tip"=>"$5 재결제자(2차)"),
    		array("mode_name"=>"웨일급 이탈감지자 부양(21)",	 	"mode_num"=>21,	"mode_index"=>6, "mode_tip"=>"웨일급이탈"),
    		array("mode_name"=>"최저 승률 기준 부양(22)", 		"mode_num"=>22,	"mode_index"=>7, "mode_tip"=>"최저승률부양"),
    		array("mode_name"=>"럭키 오퍼 (1차)(23)", 		"mode_num"=>23,	"mode_index"=>8, "mode_tip"=>"럭키오퍼 (1차)"),
    		array("mode_name"=>"럭키 오퍼 (2차)(24)", 		"mode_num"=>24,	"mode_index"=>9, "mode_tip"=>"럭키오퍼 (2차)"),
    		array("mode_name"=>"베팅금액별 트래킹 (비결제자)(25)", 		"mode_num"=>25,	"mode_index"=>10, "mode_tip"=>"베팅금액별 (비결제자)"),
    		array("mode_name"=>"베팅금액별 트래킹 (결제자)(28)", 		"mode_num"=>28,	"mode_index"=>11, "mode_tip"=>"베팅금액별 (결제자)")
    		
    );
    
    if ($tab == "")
        error_back("잘못된 접근입니다.");       

    $db_main2 = new CDatabase_Main2();    
    
    $tail = " WHERE 1=1 ";
    
    if($os_term == "0")
    {
    	$table = "tbl_game_cash_stats_daily2";
    	$log_table = "user_online_game_log";
    	$type = 0;
    	$os_txt = "Web";
    }
    else
    {
    	if($os_term == "1")
    	{
    		$tail .= " AND ios = 1 ";
    		$table = "tbl_game_cash_stats_mobile_daily2";
    		$type = 1;
    		$category = "category = 1 AND ";
    		$os_txt = "iOS";
    	}
    	else if($os_term == "2")
    	{
    		$tail .= " AND android = 1 ";
    		$table = "tbl_game_cash_stats_android_daily2";
    		$type = 2;
    		$category = "category = 2 AND ";
    		$os_txt = "Android";
    	}
    	else if($os_term == "3")
    	{
    		$tail .= " AND amazon = 1 ";
    		$table = "tbl_game_cash_stats_amazon_daily2";
    		$type = 3;
    		$category = "category = 3 AND ";
    		$os_txt = "Amazon";
    	}
    	
    	$log_table = "user_online_game_mobile_log";
    }
    
	$sql = "SELECT slottype,slotname FROM tbl_slot_list $tail ";
	$slotlist = $db_main2->gettotallist($sql);
	
	$slotstartlist = array(array("slottype" => "0", "slotname" => "ALL"));
	$slotlist = array_merge($slotstartlist, $slotlist);
	
	if($tab == 0)
		$slottype = "slottype";
	else
		$slottype = $tab;
	
	if($viewmode == "0")
	{
		$sql = "SELECT MODE, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout),0) AS moneyout, LEFT(writedate, 10) AS writedate
				FROM $table
				WHERE slottype=$slottype AND MODE NOT IN (4, 9, 26, 29) AND writedate BETWEEN '$startdate' AND '$enddate' 
				GROUP BY writedate 
				ORDER BY writedate DESC";
		$all_rate_list = $db_main2->gettotallist($sql);
		
		$sql = "SELECT MODE, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout),0) AS moneyout, LEFT(writedate, 10) AS writedate
				FROM $table
				WHERE slottype=$slottype AND MODE IN (0,31) AND slottype=$slottype AND writedate BETWEEN '$startdate' AND '$enddate' 
				GROUP BY writedate 
				ORDER BY writedate DESC";
		$nomal_rate_list = $db_main2->gettotallist($sql);
	}
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    
    function tab_change(tab)
    {
        var search_form = document.search_form;
        search_form.tab.value = tab;
        search_form.submit();
    }
    
    $(function() {
        $("#startdate").datepicker({ });
    });

    $(function() {
        $("#enddate").datepicker({ });
    });

    function change_os_term(term)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("term_web");
		var ios = document.getElementById("term_ios");
		var android = document.getElementById("term_android");
		var amazon = document.getElementById("term_amazon");
		
		document.search_form.os_term.value = term;

		if (term == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (term == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}

		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>" style="width:1300px">
            	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
					<input type="button" class="<?= ($os_term == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="term_web" onclick="change_os_term('0')"    />
					<!-- <input type="button" class="<?= ($os_term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_os_term('1')" />
					<input type="button" class="<?= ($os_term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_os_term('2')"    />
					<input type="button" class="<?= ($os_term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_os_term('3')"    /> -->
				</span>
				
                <!-- title_warp -->
                <div class="title_wrap">
                    <div class="title"><?= $top_menu_txt ?> &gt; MODE별 승률 비중(<?= $os_txt ?>)</div>
                    <div class="search_box">
                    	<input type="hidden" name="os_term" id="os_term" value="<?= $os_term ?>" />
                        <input type="hidden" name="tab" id="tab" value="<?= $tab ?>" />
                        <input type="input" class="search_text" id="startdate" name="startdate" style="width:65px" value="<?= $startdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
                        <input type="input" class="search_text" id="enddate" name="enddate" style="width:65px" value="<?= $enddate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
                        <input type="button" class="btn_search" value="검색" onclick="search()" />
                    </div>
                </div>
                <!-- //title_warp -->
                
                <ul class="tab">
<?
	for ($i=0; $i<sizeof($slotlist); $i++)
	{
		$slottype1 = $slotlist[$i]["slottype"];
		$slotname1 = str_replace(" Slot", "", $slotlist[$i]["slotname"]);
?>	
                <li id="tab_<?= $slottype1 ?>" class="<?= ($tab == $slottype1) ? "select" : "" ?>" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('<?= $slottype1 ?>')"><?= $slotname1 ?></li>
<?
	}
?>                
                </ul>
                <table class="tbl_list_basic1">
                <colgroup>
                    <col width="80">
                    <col width="80">
                    <col width="80">
<? 
	for ($i=0; $i<sizeof($mode_list); $i++)
	{
?>
					<col width="105">
<?
	}
?>                    
                </colgroup>
                <thead>
                    <tr>
                        <th style="border-top:none;">날짜</th>
                        <th style="border-top:none;">전체승률</th>
                        <th style="border-top:none;">일반승률</th>
<?
	for($i=0; $i<sizeof($mode_list); $i++)
	{
?>
						<th style="border-top:none;"><?= $mode_list[$i]["mode_tip"] ?><br/>(<?= $mode_list[$i]["mode_num"] ?>)</th>
<?
	}
?>
                    </tr>
                </thead>
                <tbody>
<?
    $datetmp = "";
    
	// xx_per_moneyin	: 전체 머니인 - 모드 머니인
	// xx_per_moneyout	: 전체 머니아웃 - 모드 머니아웃
	// xx_per_rate 		: 전체 승률 - 모드 승률
    // xx_contrinution	: 기여도
    
    $total_all_rate_moneyin = "";
    $total_all_rate_moneyout = "";
    
    $total_nomal_rate_moneyin = "";
    $total_nomal_rate_moneyout = "";
    
    $total_mode_per_moneyin = "";
    $total_mode_per_moneyout = "";
    
    $mode_contribution = array_fill(0, sizeof($mode_list), null);
    
    for ($i=0; $i<sizeof($all_rate_list); $i++)
    {
    	$mode_rate_moneyin = "";
    	$mode_rate_moneyout = "";
    	$mode_rate = "";
    	
        $writedate = $all_rate_list[$i]["writedate"];
        
        // 전체 승률
        $all_rate_moneyin = $all_rate_list[$i]["moneyin"];
        $all_rate_moneyout = $all_rate_list[$i]["moneyout"];
        
        $all_rate = round(($all_rate_moneyout / $all_rate_moneyin) * 10000) / 100;
        
        $total_all_rate_moneyin += $all_rate_moneyin;
        $total_all_rate_moneyout += $all_rate_moneyout;
        
        // 일반 승률
        $nomal_rate_moneyin = $nomal_rate_list[$i]["moneyin"];
        $nomal_rate_moneyout = $nomal_rate_list[$i]["moneyout"];
        
        $nomal_rate = round(($nomal_rate_moneyout / $nomal_rate_moneyin) * 10000) / 100;
        
        $total_nomal_rate_moneyin += $nomal_rate_moneyin;
        $total_nomal_rate_moneyout += $nomal_rate_moneyout;
        
        $sql = "SELECT MODE, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout),0) AS moneyout, LEFT(writedate, 10) AS writedate
	     	 	FROM $table
	     	 	WHERE slottype=$slottype AND MODE NOT IN (4, 9, 26, 29) AND writedate BETWEEN '$writedate 00:00:00' AND '$writedate 23:59:59'
	     	 	GROUP BY writedate, MODE
	     	 	ORDER BY writedate DESC, MODE ASC";
        $mode_rate_list = $db_main2->gettotallist($sql);
        
        // 각 모드 기여도
        for($j=0; $j<sizeof($mode_rate_list); $j++)
        {
        	$mode_num = $mode_rate_list[$j]["MODE"];
        	
        	for($k=0; $k<sizeof($mode_list); $k++)
        	{
	        	if($mode_num == $mode_list[$k]["mode_num"])
	        	{
 	        		$mode_per_moneyin = $all_rate_moneyin -  $mode_rate_list[$j]["moneyin"];
 	        		$mode_per_moneyout = $all_rate_moneyout -  $mode_rate_list[$j]["moneyout"];
 	        		
 	        		$mode_per_rate = round(($mode_per_moneyout / $mode_per_moneyin) * 10000) / 100;
 	        		
 	        		$mode_contribution[$mode_list[$k]["mode_index"]] = $all_rate - $mode_per_rate;
 	        		
 	        		$total_mode_per_moneyin[$mode_list[$k]["mode_index"]] += $mode_per_moneyin;
 	        		$total_mode_per_moneyout[$mode_list[$k]["mode_index"]] += $mode_per_moneyout;
 	        		
	        		break;
	        	}
        	}
        }
            
        if($datetmp != $writedate && datetmp != "" && $viewmode != "0" && $i != "0")
        {
?>
        			<tr onmouseover="className='tr_over'" onmouseout="className=''" style="border-top:1px double;">
<?
        }
		else
		{
?>
					<tr onmouseover="className='tr_over'" onmouseout="className=''" >
<? 
		}
		
		$datetmp = $writedate;
		
		
?>
						<td class="tdc point"><?= $writedate ?></td>
                        <td class="tdc point"><?= $all_rate."%" ?></td>
                        <td class="tdc point"><?= $nomal_rate."%" ?></td>
<?
		for($j=0; $j<sizeof($mode_contribution); $j++)
		{
			$contrinution = number_format($mode_contribution[$j], 2);
?>
						<td class="tdc point"><?= $contrinution."%" ?></td>
<?
		}
?>
                    </tr>
<?
    }
    
    $total_all_rate = ($total_all_rate_moneyin == 0) ? 0 : round(($total_all_rate_moneyout / $total_all_rate_moneyin) * 10000) / 100;
    $total_nomal_rate = ($total_nomal_rate_moneyin == 0) ? 0 : round(($total_nomal_rate_moneyout / $total_nomal_rate_moneyin) * 10000) / 100;
    
    // 일반 모드 기여도
    $total_nomal_per_moneyin = $total_all_rate_moneyin - $total_nomal_rate_moneyin;
    $total_nomal_per_moneyout = $total_all_rate_moneyout - $total_nomal_rate_moneyout;
    
    $total_nomal_per_rate = ($total_nomal_per_moneyin == 0) ? 0 : round(($total_nomal_per_moneyout / $total_nomal_per_moneyin) * 10000) / 100;
    
    $total_nomal_contribution = $total_all_rate - $total_nomal_per_rate;
    	
?>
					<tr style="border-top:1px double;">
						<td class="tdc point_title">Total</td>
						<td class="tdc point"><?= $total_all_rate ?>%</td>
						<td class="tdc point"><?= $total_nomal_rate ?>%</td>
<?
	if($mode == 0)
	{
?>
						<td class="tdc point"><?= number_format($total_nomal_contribution, 2) ?>%</td>
<?
	}
		
	for($i=1; $i<sizeof($mode_contribution); $i++)
	{
		$total_mode_per_rate = ($total_mode_per_moneyin[$i] == 0) ? 0 : round(($total_mode_per_moneyout[$i] / $total_mode_per_moneyin[$i]) * 10000) / 100;
 	        		
		$total_mode_contribution = ($total_mode_per_rate == 0) ? 0 : $total_all_rate - $total_mode_per_rate;
?>
						<td class="tdc point"><?= number_format($total_mode_contribution, 2) ?>%</td>
<?
	}
?>
                    </tbody>
                </table>
            </form>
        </div>
        <!--  //CONTENTS WRAP -->
<?
    $db_main2->end();    
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>