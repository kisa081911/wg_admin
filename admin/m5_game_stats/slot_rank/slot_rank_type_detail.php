<?
	$top_menu = "game_stats";
	
	$type = $_GET["type"];
	
	$sub_menu = "slot_rank_type".$type;
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$slottype = $_GET["slottype"];
	$term = $_GET["term"];
	
	$search_start_date = $_GET["start_date"];
	$search_end_date = $_GET["end_date"];
	
	if ($type == "" ||$slottype == "" || $term == "")
		error_back("잘못된 접근입니다.");
	
	$db_main2 = new CDatabase_Main2();
   	$db_other = new CDatabase_Other();

	if($search_start_date == "")
	{
		$search_start_date = get_past_date(date("Y-m-d"),60,"d");
	}
	
	if($search_end_date == "")
	{
		$sql = "SELECT today FROM `tbl_slot_rank_daily` WHERE type = $type ORDER BY today DESC LIMIT 1";
		$search_end_date = $db_other->getvalue($sql);
	}
	
	$pagename = "slot_rank_type_detail.php";
	
	$sql = "SELECT slotname FROM tbl_slot_list WHERE slottype=$slottype";
    $slotname = $db_main2->getvalue($sql);   
	
    $sql = "SELECT today, money, share_rate, rank FROM tbl_slot_rank_daily  ".
    		"WHERE '$search_start_date' <= today AND today <= '$search_end_date' AND type = $type AND platform = $term AND slottype = $slottype";
    $rank_list = $db_other->gettotallist($sql);
?>
<!-- CONTENTS WRAP -->
<link type="text/css" href="../../js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="../../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../../js/common_util.js"></script>
<script type="text/javascript" src="../../js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_date").datepicker({ });
	    $("#end_date").datepicker({ });
	});

	function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }

	google.charts.load('current', {packages: ['corechart', 'line', 'line', 'line']});
	google.charts.setOnLoadCallback(drawChartRank);
	google.charts.setOnLoadCallback(drawChartMoney);
	google.charts.setOnLoadCallback(drawChartShareRate);

	function drawChartRank()
	{
		var data_rank = google.visualization.arrayToDataTable([
			['', 'Rank'],
  <?
		for($i=0; $i<sizeof($rank_list); $i++)
		{
			$date = $rank_list[$i]["today"];
			$rank = $rank_list[$i]["rank"];
                                      			
			echo "['".$date."', $rank]";
                                      			
			if ($i < sizeof($rank_list))
				echo(",");
		}
?>
		]);
		
		var options_rank = {
			vAxis: {direction: -1, format: 'decimal'},
			minValue: 0,
			ticks: [0, 1, 5, 10, 15, 20],
			height: 400
		};

		var chart_rank = new google.visualization.LineChart(document.getElementById('chart_div_rank'));
		chart_rank.draw(data_rank, options_rank);
	}

    function drawChartMoney() 
    {
		var data_money = google.visualization.arrayToDataTable([
			['', '매출'],
<?
		for($i=0; $i<sizeof($rank_list); $i++)
		{
			$date = $rank_list[$i]["today"];
			$money = $rank_list[$i]["money"];
			
			echo "['".$date."', $money]";
			
			if ($i < sizeof($rank_list))
				echo(",");
		}
?>
		]);

		var options_money = {
          bars: 'vertical',
          vAxis: {format: 'decimal'},
          height: 400
        };

        var chart_money = new google.visualization.LineChart(document.getElementById('chart_div_money'));

        chart_money.draw(data_money, options_money);
	}

    function drawChartShareRate() 
    {
		var data_share_rate = google.visualization.arrayToDataTable([
			['', '점유율'],
<?
		for($i=0; $i<sizeof($rank_list); $i++)
		{
			$date = $rank_list[$i]["today"];
			$share_rate = $rank_list[$i]["share_rate"];
			
			echo "['".$date."', ".$share_rate."]";
			
			if ($i < sizeof($rank_list))
				echo(",");
		}
?>
		]);

		var options_share_rate = {
          bars: 'vertical',
          vAxis: {format: 'decimal'},
          height: 400
        };

        var chart_share_rate = new google.visualization.LineChart(document.getElementById('chart_div_share_rate'));

        chart_share_rate.draw(data_share_rate, options_share_rate);
	}
</script>
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; <?= $slotname ?> 상세 정보</div>			
			<div class="search_box">
				<input type="text" class="search_text" id="start_date" name="start_date" value="<?= $search_start_date ?>" maxlength="10" style="width:65px" readonly="readonly" onkeypress="search_press(event)" />
				~
				<input type="text" class="search_text" id="end_date" name="end_date" value="<?= $search_end_date ?>" maxlength="10" style="width:65px" readonly="readonly" onkeypress="search_press(event)" />
				&nbsp;&nbsp;&nbsp;
				<input type="hidden" id="slottype" name="slottype" value="<?= $slottype?>"/>
				<input type="hidden" id="term" name="term" value="<?= $term?>"/>
				<input type="hidden" id="type" name="type" value="<?= $type?>"/>
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</div>
		<!-- //title_warp -->	
		<div style="font:bold 14px dotum;">1. 슬롯 순위 추이</div>
		<div id="chart_div_rank" style="width: 1100px; height: 400px"></div>
		<br/><br/>
		<div style="font:bold 14px dotum;">2. 일별 매출 추이</div>
		<div id="chart_div_money" style="width: 1100px; height: 400px"></div>
		<br/><br/>
		<div style="font:bold 14px dotum;">3. 일별 점유율 추이</div>
		<div id="chart_div_share_rate" style="width: 1100px; height: 400px"></div>
	</form>
</div>
<!--  //CONTENTS WRAP -->

<?	
	$db_main2->end();	
	$db_other->end();
	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>