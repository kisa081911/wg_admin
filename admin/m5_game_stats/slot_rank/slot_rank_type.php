<?
    $top_menu = "game_stats";
    
    $type = ($_GET["type"] == "") ? "0" : $_GET["type"];
    
    $sub_menu = "slot_rank_type".$type;
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
    $db_main2 = new CDatabase_Main2();
   	$db_other = new CDatabase_Other();
   	
   	$term = ($_GET["term"] == "") ? "0" : $_GET["term"];
   	$search_date = $_GET["start_date"];
   	
   	if ($term != "0" && $term != "1" && $term != "2" && $term != "3" && $term != "4")
   		error_back("잘못된 접근입니다.");

   	if($search_date == "")
   	{
   		$sql = "SELECT today FROM tbl_slot_rank_daily WHERE type = $type ORDER BY today DESC LIMIT 1";
   		$search_date = $db_other->getvalue($sql);
   	}
   	
   	$pagename = "slot_rank_type.php?type=$type";
    
   	$sql = "SELECT slottype, slotname FROM tbl_slot_list";
   	$slottype_list = $db_main2->gettotallist($sql);
   	
   	$sql = "SELECT slottype, user_cnt, money, ROUND(money/user_cnt, 2) AS arppu, share_rate, dayafterinstall, pre_rank, rank ".
			"FROM `tbl_slot_rank_daily` ".
			"WHERE today = '$search_date' AND type = $type AND platform=$term ".
			"ORDER BY money DESC";
    $rank_list = $db_other->gettotallist($sql);
    
    $sql = "SELECT SUM(money) ".
			"FROM `tbl_slot_rank_daily` ".
			"WHERE today = '$search_date' AND type = $type AND platform=$term  ";
    $total_money = $db_other->getvalue($sql);
    
    $title = "";
    $desc = "";
    
    if($type == 0)
    {
    	$title = "최근 1주 결제자 기준";
    	$desc = "*최근 1주일 결제자 결제액을 최근 2주 슬롯별 머니인 기준 배분";
    }
    else if($type == 1)
    {
    	$title = "최근 4주 가입자 기준";
    	$desc = "*최근 4주이내 가입자의 결제액을 최근 4주 슬롯별 머니인 기준 배분";
    }
    else if($type == 2)
    {
    	$title = "최근 4주 복귀자 기준";
    	$desc = "*최근 4주이내 복귀자(D28이상)의 결제액을 최근 4주 슬롯별 머니인 기준 배분";
    }
    else if($type == 3)
    {
    	$title = "최근 1주 결제복귀자 기준";
    	$desc = "*최근 1주 이내 결제복귀자(D28이상)의 결제액은 결제복귀시점 1주 슬롯별 머니인 기준 배분";
    }
?>
<!-- CONTENTS WRAP -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="../../js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="../../js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="../../js/common_util.js"></script>
<script type="text/javascript" src="../../js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_date").datepicker({ });
	});

	function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }

	function fn_slot_detail(type, term, slottype)
    {
    	window.location.href='slot_rank_type_detail.php?type=' + type + '&term=' + term + '&slottype=' + slottype;
    }

    function tab_change(tab)
	{
		var search_form = document.search_form;
		search_form.tab.value = tab;
		search_form.submit();
	}

	function change_term(term)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("term_web");
		var ios = document.getElementById("term_ios");
		var android = document.getElementById("term_android");
		var amazon = document.getElementById("term_amazon");
		var all = document.getElementById("term_all");
		
		document.search_form.term.value = term;

		if (term == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			all.className="btn_schedule";
		}
		else if (term == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			all.className="btn_schedule";
		}
		else if (term == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
			all.className="btn_schedule";
		}
		else if (term == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
			all.className="btn_schedule";
		}
		else if (term == "4")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			all.className="btn_schedule_select";
		}

		search_form.submit();
	}
</script>
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">	
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; <?= $title?></div>
			<input type="hidden" name="type" id="type" value="<?= $type ?>" />		
			<input type="hidden" name="term" id="term" value="<?= $term ?>" />
			<div class="search_box">
				<input type="text" class="search_text" id="start_date" name="start_date" value="<?= $search_date ?>" maxlength="10" style="width:65px" readonly="readonly" onkeypress="search_press(event)" />
				&nbsp;&nbsp;&nbsp;
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</div>
		<!-- //title_warp -->	
				
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $desc?><br/><br/>
			<input type="button" class="<?= ($term == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="term_web" onclick="change_term('0')"    />
			<input type="button" class="<?= ($term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_term('1')" />
			<input type="button" class="<?= ($term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_term('2')"    />
			<input type="button" class="<?= ($term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_term('3')"    />
			<input type="button" class="<?= ($term == "4") ? "btn_schedule_select" : "btn_schedule" ?>" value="통합" id="term_all" onclick="change_term('4')"    />
		</span>
		
		<table class="tbl_list_basic1" style="margin-top:5px">
			<colgroup>
				<col width="">   
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">순위</th>
					<th class="tdc">슬롯</th>
					<th class="tdc">오픈일</th>
					<th class="tdc">결제자수</th>
					<th class="tdc">매출액</th>
					<th class="tdc">ARPPU</th>
					<th class="tdc">평균가입경과일</th>
					<th class="tdc">점유율</th>
					<th class="tdc">순위 변동</th>
					<th class="tdc"></th>
				</tr>
			</thead>
			<tbody>
<?
	if(sizeof($rank_list) == 0)
	{
?>
   			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   				<td class="tdc" colspan="8">검색 결과가 없습니다.</td>
   			</tr>
<?
   	}
   	else
   	{
   		for($i=0; $i<sizeof($rank_list); $i++)
   		{
			$slottype = $rank_list[$i]["slottype"];			
			
			for($j=0; $j<sizeof($slottype_list); $j++)
			{
				if($slottype_list[$j]["slottype"] == $slottype)
				{
					$slotname = $slottype_list[$j]["slotname"];
					break;
				}
				else
				{
					$slotname = "Unkown";
				}
			}
			
			$user_cnt = $rank_list[$i]["user_cnt"];
			$money = $rank_list[$i]["money"];
			$arppu = $rank_list[$i]["arppu"];
			$share_rate = $rank_list[$i]["share_rate"];
			$dayafterinstall = $rank_list[$i]["dayafterinstall"];
			$rank = $rank_list[$i]["rank"];
			$pre_rank = $rank_list[$i]["pre_rank"];
			
			if($term !=0 && $term != 4)
				$open_date = $db_main2->getvalue("SELECT open_date_mobile FROM tbl_slot_list WHERE slottype = $slottype");   
			else
				$open_date = $db_main2->getvalue("SELECT open_date FROM tbl_slot_list WHERE slottype = $slottype");   
			
   			$change_rank = ($pre_rank-$rank == 0) ? "-" : $pre_rank-$rank;
   			
   			if($pre_rank == 0)
   				$change_rank = "<img src=\"/images/icon/new.gif\" />";
   			
   			if($change_rank > 0)
   				$change_rank = "<span style=\"color:red;\">▲</span> $change_rank";
   			else if($change_rank < 0)
   				$change_rank = "<span style=\"color:blue;\">▼</span> ".($change_rank * -1);

   			$rank_img = "";
   			
   			if($rank < 6)
   				$rank_img = "<img height=\"15\" style=\"vertical-align: middle;\" src=\"/images/icon/rank_$rank.gif\" />";
?>
			<tr>
				<td class="tdc point_title"><?= $rank ?></td>
				<td class="tdc point"><?= $rank_img ?> <?= $slotname ?></td>
				<td class="tdc "><?= $open_date ?></td>
				<td class="tdc"><?= number_format($user_cnt) ?> 명</td>				
				<td class="tdc">$ <?= number_format($money) ?></td>
				<td class="tdc">$ <?= number_format($arppu, 2) ?></td>
				<td class="tdc"><?= number_format($dayafterinstall) ?> 일</td>
				<td class="tdc"><?= number_format($share_rate, 2) ?>%</td>
				<td class="tdc"><?= $change_rank ?></td>
				<td class="tdc"><input class="btn_03" style="cursor: pointer;" onclick="fn_slot_detail(<?= $type ?>,<?= $term ?>,<?= $slottype ?>)" type="button" value="상세"></td>
			</tr>
<?
	    }
	}
?>
			</tbody>
		</table>
	</form>
</div>
<!--  //CONTENTS WRAP -->

<?	
	$db_main2->end();
	$db_other->end();
	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>