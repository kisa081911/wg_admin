<?
	$top_menu = "game_stats";
	$sub_menu = "game_24hour_slot_rate";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$os_type = ($_GET["os_type"] == "") ? "0" :$_GET["os_type"];
	$betlevel = ($_GET["betlevel"] == "") ? "betlevel" :$_GET["betlevel"];
	$search_time = ($_GET["search_time"] == "") ? 24 : $_GET["search_time"];
	
	$db_main2 = new CDatabase_Main2();
	
	$today = date("Y-m-d");
	$tail = " WHERE 1=1 ";
	
	if($betlevel == "betlevel")
	{
		$betlevel_str = " 1 = 1 ";
	}
	else 
	{
		$betlevel_str = " t1.betlevel = $betlevel ";
	}
	
	if($os_type == "0")
	{
		$table = "tbl_game_cash_stats_daily";
		$os_txt = "Web";
	}
	else if($os_type == "1")
	{
		$tail .= " AND ios = 1 ";
		$table = "tbl_game_cash_stats_ios_daily";
		$os_txt = "IOS";
	}
	else if($os_type == "2")
	{
		$tail .= " AND android = 1 ";
		$table = "tbl_game_cash_stats_android_daily";
		$os_txt = "Android";
	}
	else if($os_type == "3")
	{
		$tail .= " AND amazon = 1 ";
		$table = "tbl_game_cash_stats_amazon_daily";
		$os_txt = "Amazon";
	}
	
	$sql = "SELECT slottype, SUM(moneyin) AS sum_moneyin, SUM(moneyout) AS sum_moneyout, ".
			"ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unit_rate, ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2) AS unit_rate_new, ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS rate_money ".
			"FROM ". 	
			"(	".
			"	SELECT t1.slottype, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, ".
			"	ROUND(SUM(moneyout)/SUM(moneyin)*100, 2), ".  
			"	ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout,SUM(unit_moneyin)AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout ".
			"	FROM `$table` t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel ". 
			"	WHERE mode IN (0) AND $betlevel_str  AND DATE_SUB(NOW(), INTERVAL $search_time HOUR) <= writedate AND writedate <= NOW() ".
			"	GROUP BY slottype, t1.betlevel ".
			") t3 GROUP BY slottype ORDER BY sum_moneyin DESC;";
	$slot_rate_info = $db_main2->gettotallist($sql);
	
	$sql = "SELECT * FROM `tbl_slot_list`";
	$slottype_list = $db_main2->gettotallist($sql);
	
	$sql = "SELECT bettype, betname FROM tbl_slot_betlevel";
	$betlist = $db_main2->gettotallist($sql);
	
	$db_main2->end();
?>
<link type="text/css" href="/js/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="/js/ui/ui.core.js"></script>
<script type="text/javascript" src="/js/ui/ui.datepicker.js"></script>  
<script type="text/javascript">
	function search()
	{
		var search_form = document.search_form;	    
		search_form.submit();
	}

	function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">     
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
			<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
			<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
			<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
			<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
		</span> 
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 최근 가중 승률 통계(<?= $os_txt ?>)</div>
		<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />   
		
			<div class="search_box">
				시간 설정 &nbsp;: &nbsp;
			    <select name="search_time" id="search_time" onchange="search()">
			    	<option value="24" <?= ($search_time=="24") ? "selected" : "" ?>>24시간</option>
			    	<option value="48" <?= ($search_time=="48") ? "selected" : "" ?>>48시간</option>
			    	<option value="72" <?= ($search_time=="72") ? "selected" : "" ?>>72시간</option>
			    	<option value="96" <?= ($search_time=="96") ? "selected" : "" ?>>96시간</option>
			    </select>&nbsp;&nbsp;
				베팅레벨 &nbsp; : &nbsp;
				<select name="betlevel" id="betlevel">
					<option value="" <?= ($bettype=="betlevel") ? "selected" : "" ?>>베팅레벨 - 전체</option>
<?
	for ($i=0; $i<sizeof($betlist); $i++)
	{
		$bettype = $betlist[$i]["bettype"];
		$betname = $betlist[$i]["betname"];
?>	
					<option value="<?= $bettype ?>" <?= ($bettype == $betlevel) ? "selected=\"true\"" : "" ?>><?= $betname ?></option>
<?						
	}
?>
															
				</select>
				&nbsp;&nbsp;&nbsp;
				<input type="button" class="btn_search" value="검색" onclick="search()" />	
			</div>
		</form>
	</div>
	
	* 최근 24시간 승률입니다.
	
	<!-- //title_warp -->
	
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
		</colgroup>
		<thead>
			<th>Slot</th>
			<th>MoneyIn</th>
			<th>MoneyOut</th>
			<th>단위승률</th>
			<th>일반승률</th>
		</thead>
		<tbody>			
<?
		for ($i=0; $i<sizeof($slot_rate_info); $i++)
		{
			$slottype = $slot_rate_info[$i]["slottype"];
			$sum_moneyin = $slot_rate_info[$i]["sum_moneyin"];
			$sum_moneyout = $slot_rate_info[$i]["sum_moneyout"];
			$unit_playcount = ($slot_rate_info[$i]["unit_rate"]=="")?0:$slot_rate_info[$i]["unit_rate"];
			$rate_money = $slot_rate_info[$i]["rate_money"];
			$unit_rate_new = $slot_rate_info[$i]["unit_rate_new"];
			
			for($j=0; $j<sizeof($slottype_list); $j++)
			{
				if($slottype_list[$j]["slottype"] == $slottype)
				{
					$slot_name = $slottype_list[$j]["slotname"];
					break;
				}
				else
				{
					$slot_name = "Unkown";
				}
			}
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point"><?= $slot_name ?></td>
				<td class="tdc"><?= number_format($sum_moneyin) ?></td>
				<td class="tdc"><?= number_format($sum_moneyout) ?></td>
				<td class="tdc"><?= number_format((($today >'2017-11-20' && $os_type == 0) ||($today >'2017-11-22' && $os_type != 0))?$unit_rate_new :$unit_playcount, 2) ?>%</td>
				<td class="tdc"><?= number_format($rate_money, 2) ?>%</td>
			</tr>
				
<?
		}
?>
			
		</tbody>
	</table>
  </div>

<!--  //CONTENTS WRAP -->        
	<div class="clear"></div>

<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
		