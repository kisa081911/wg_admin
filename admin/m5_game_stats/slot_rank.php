<?
    $top_menu = "game_stats";
    $sub_menu = "slot_rank";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $db_main2 = new CDatabase_Main2();
   	$db_other = new CDatabase_Other();
   	
   	$term = ($_GET["term"] == "") ? "0" : $_GET["term"];
   	$search_start_date = $_GET["start_date"];
   	
   	if ($term != "0" && $term != "1" && $term != "2" && $term != "3")
   		error_back("잘못된 접근입니다.");

   	if($search_start_date == "")
   	{
   		$sql = "SELECT e_date FROM `tbl_slot_stat_week` ORDER BY today DESC LIMIT 1";
   		$search_start_date = $db_other->getvalue($sql);
   	}
   	
   	$pagename = "slot_rank.php";
    
    $sql = "SELECT slottype, slotname, open_date FROM tbl_slot_list";
    $slottype_list = $db_main2->gettotallist($sql);    
    
    $sql = "SELECT (IF(WEEKOFYEAR('$search_start_date') > 52 && MONTH('$search_start_date') = 1, YEAR(DATE_SUB('$search_start_date', INTERVAL 1 YEAR)), YEAR('$search_start_date'))*100 + WEEKOFYEAR('$search_start_date')) AS today";
    $search_today = $db_other->getvalue($sql);
    
    $sql = "SELECT t1.today, s_date, e_date, slottype, t1.money_in, t1.money_out, t1.jackpot, t1.treatamount, t1.playcount, t1.jackpotcount, play_rate, pay_rate, pre_rank, rank ".
			"FROM tbl_slot_rank_week AS t1 JOIN `tbl_slot_stat_week` AS t2 ON t1.today = t2.today  AND t1.os_type = t2.os_type ".
			"WHERE t1.today = '$search_today' AND t1.os_type = $term  ".
			"ORDER BY rank ASC";
    
    $rank_list = $db_other->gettotallist($sql);
    
    if(sizeof($rank_list) > 0)
        $title = "기간: ".$rank_list[0]["s_date"]." ~ ".$rank_list[0]["e_date"];
    else
    	$title = "";
?>
<!-- CONTENTS WRAP -->
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_date").datepicker({ });
	});

	function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }

    function fn_slot_detail(term, slottype)
    {
    	window.location.href='slot_rank_detail.php?term=' + term + '&slottype=' + slottype;
    }

    function tab_change(tab)
	{
		var search_form = document.search_form;
		search_form.tab.value = tab;
		search_form.submit();
	}

	function change_term(term)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("term_web");
		var ios = document.getElementById("term_ios");
		var android = document.getElementById("term_android");
		var amazon = document.getElementById("term_amazon");
		
		document.search_form.term.value = term;

		if (term == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (term == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}

		search_form.submit();
	}
</script>
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">	
	        
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 주간 슬롯 순위</div>			
			<input type="hidden" name="term" id="term" value="<?= $term ?>" />	
			<div class="search_box">
				<input type="text" class="search_text" id="start_date" name="start_date" value="<?= $search_start_date ?>" maxlength="10" style="width:65px" readonly="readonly" onkeypress="search_press(event)" />
				&nbsp;&nbsp;&nbsp;
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</div>
		<!-- //title_warp -->	
				
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
			<input type="button" class="<?= ($term == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="term_web" onclick="change_term('0')"    />
			<input type="button" class="<?= ($term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_term('1')" />
			<input type="button" class="<?= ($term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_term('2')"    />
			<input type="button" class="<?= ($term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_term('3')"    />
		</span>
		
		<table class="tbl_list_basic1" style="margin-top:5px">
			<colgroup>
				<col width="">   
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
<?
			if($term == 0)
			{
?>
				<col width="">
<?
			}
?>
				<col width="">  
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">순위</th>
					<th class="tdc">슬롯</th>
					<th class="tdc">money in</th>
					<th class="tdc">money out</th>
					<th class="tdc">jackpot</th>
					<th class="tdc">승률</th>
					<th class="tdc">플레이카운트</th>
					<th class="tdc">플레이 기여도</th>
					<th class="tdc">결제 기여도</th>
					<th class="tdc">포인트</th>
					<th class="tdc">순위 변동</th>
<?
			if($term == 0)
			{
?>
					<th class="tdc">오픈일</th>
<?
			}
?>
					<th class="tdc"></th>
				</tr>
			</thead>
			<tbody>
<?
	if(sizeof($rank_list) == 0)
	{
?>
   			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   				<td class="tdc" colspan="11">검색 결과가 없습니다.</td>
   			</tr>
<?
   	}
   	else
   	{
   		for($i=0; $i<sizeof($rank_list); $i++)
   		{
   			$today = $rank_list[$i]["today"];
			$rank = $rank_list[$i]["rank"];
			$slottype = $rank_list[$i]["slottype"];
   			$slotname = ($slottype_list[$slottype-1]["slotname"] == "") ? "Unknown" : $slottype_list[$slottype-1]["slotname"];
   			$money_in = round($rank_list[$i]["money_in"]/100000000);
   			$money_out = round($rank_list[$i]["money_out"]/100000000);
   			$jackpot = round($rank_list[$i]["jackpot"]/100000000);
   			$jackpot_count = $rank_list[$i]["jackpotcount"];
   			$win_rate = ($money_in == 0) ? 0 : round($money_out/$money_in*100, 2);
   			$play_count = $rank_list[$i]["playcount"];
   			$play_rate = $rank_list[$i]["play_rate"];
   			$pay_rate = $rank_list[$i]["pay_rate"];
   			$open_date = $slottype_list[$slottype-1]["open_date"];
   			
   			if($today < 201613)
   				$point = round($money_in*$play_rate/1000);
   			else
   				$point = round($money_in*$play_rate*$pay_rate/100);
   				
   			$pre_rank = $rank_list[$i]["pre_rank"];
   			$change_rank = ($pre_rank-$rank == 0) ? "-" : $pre_rank-$rank;
   			
   			if($pre_rank == 0)
   				$change_rank = "<img src=\"/images/icon/new.gif\" />";
   			
   			if($change_rank > 0)
   				$change_rank = "<span style=\"color:red;\">▲</span> $change_rank";
   			else if($change_rank < 0)
   				$change_rank = "<span style=\"color:blue;\">▼</span> ".($change_rank * -1);

   			$rank_img = "";
   			
   			if($rank < 6)
   				$rank_img = "<img height=\"15\" style=\"vertical-align: middle;\" src=\"/images/icon/rank_$rank.gif\" />";
?>
			<tr>
				<td class="tdc point_title"><?= $rank ?></td>
				<td class="tdc point"><?= $rank_img ?> <?= $slotname ?></td>
				<td class="tdc"><?= number_format($money_in) ?> 억</td>				
				<td class="tdc"><?= number_format($money_out) ?> 억</td>
				<td class="tdc"><?= number_format($jackpot) ?> 억 (<?= number_format($jackpot_count) ?>회)</td>
				<td class="tdc"><?= number_format($win_rate, 2) ?>%</td>
				<td class="tdc"><?= number_format($play_count) ?></td>
				<td class="tdc"><?= number_format($play_rate, 2) ?>%</td>
				<td class="tdc"><?= number_format($pay_rate, 2) ?>%</td>
				<td class="tdc"><?= number_format($point) ?></td>
				<td class="tdc"><?= $change_rank ?></td>
<?
			if($term == 0)
			{
?>
				<td class="tdc"><?= $open_date ?></td>
<?
			}
?>
				<td class="tdc"><input class="btn_03" style="cursor: pointer;" onclick="fn_slot_detail(<?= $term ?>,<?= $slottype ?>)" type="button" value="상세"></td>
			</tr>
<?
	    }
	}
?>
			</tbody>
		</table>
	</form>
</div>
<!--  //CONTENTS WRAP -->

<?	
	$db_main2->end();	
	$db_other->end();
	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
