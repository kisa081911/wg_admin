<?
	$top_menu = "game_stats";
	$sub_menu = "game_slot_summarystate_daily";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$os_type = ($_GET["os_type"] == "") ? "0" :$_GET["os_type"];
	$viewmode = ($_GET["viewmode"] == "") ? "100" :$_GET["viewmode"];
	
	if($_GET["start_date"] == "")
		$search_sdate = date("Y-m-d",strtotime("-4 days"));
	else
		$search_sdate = $_GET["start_date"];
	
	if($_GET["end_date"] == "")
		$search_edate = date("Y-m-d");
	else
		$search_edate = $_GET["end_date"];
	
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();

	$tail = " WHERE 1=1 ";
	
	if($os_type == "0")
	{
		$table = "tbl_game_cash_stats_daily2";
		$os_txt = "Web";
	}
	else if($os_type == "1")
	{
		$tail .= " AND ios = 1 ";
		$table = "tbl_game_cash_stats_ios_daily2";
		$os_txt = "IOS";
	}
	else if($os_type == "2")
	{
		$tail .= " AND android = 1 ";
		$table = "tbl_game_cash_stats_android_daily2";
		$os_txt = "Android";
	}
	else if($os_type == "3")
	{
		$tail .= " AND amazon = 1 ";
		$table = "tbl_game_cash_stats_amazon_daily2";
		$os_txt = "Amazon";
	}
	
	if($viewmode == 100)
	{
	
		$sql = "SELECT * FROM `tbl_slot_summarystat_daily` WHERE os_type = $os_type AND '$search_sdate' <= today AND today <= '$search_edate' AND type = 100 ORDER BY today DESC, type, subtype";	
		$daylist = $db_analysis->gettotallist($sql);
	}
	else if($viewmode == 200)
	{
		// unit 승률
		$unit_rate_sql = "SELECT writedate, ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2) AS unit_rate_new, slottype, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unit_rate ".
						"FROM	".
						"	(	".
						"		SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout,SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout	".
						"		FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
						"		WHERE writedate BETWEEN '$search_sdate 00:00:00' AND '$search_edate 23:59:59' AND mode IN (0,31) ".
						"		GROUP BY writedate, slottype, betlevel	".
						") t3 GROUP BY writedate, slottype ORDER BY writedate DESC, slottype ASC;";
		
		$unit_rate_list = $db_main2->gettotallist($unit_rate_sql);

		$unit_rate_array = array();

		for($e = 0; $e < sizeof($unit_rate_list); $e++)
		{
			$today = $unit_rate_list[$e]["writedate"];
			$slottype = $unit_rate_list[$e]["slottype"];
			$unit_rate = $unit_rate_list[$e]["unit_rate"];

			$unit_rate_new = ($unit_rate_list[$e]["unit_rate_new"]=="")? 0 : $unit_rate_list[$e]["unit_rate_new"];
			
			if(($today >'2017-11-20' && $os_type == 0) ||($today >'2017-11-22' && $os_type != 0))
				$unit_rate_array[$today][$slottype] = $unit_rate_new;
			else 
				$unit_rate_array[$today][$slottype] = $unit_rate;
		}
		
		$sql = "SELECT writedate FROM `$table` WHERE writedate BETWEEN '$search_sdate' AND '$search_edate' GROUP BY writedate ORDER BY writedate DESC";
		$play_rate_count = $db_main2->gettotallist($sql);
	}	
	
	$sql = "SELECT * FROM `tbl_slot_list` ORDER BY slottype";
	$slotlist = $db_main2->gettotallist($sql);
	
	$slotcnt = sizeof($slotlist);
	
	$db_main2->end();	
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#start_date").datepicker({ });
		$("#end_date").datepicker({ });
	});

	function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
	
		search_form.submit();
	}

	function search()
	{
		var search_form = document.search_form;
	    
		if (search_form.start_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.start_date.focus();
	    	return;
		} 
	
		if (search_form.end_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.end_date.focus();
	    	return;
		} 
	
		search_form.submit();
	}
</script>


<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">    
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
			<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
			<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
			<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
			<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
		</span> 
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 일별 요약통계 (<?=$os_txt?>/슬롯) </div>
			<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />   
		
			<div class="search_box">
				<input type="radio" value="100" name="viewmode" id="viewmode_0" <?= ($viewmode == "100") ? "checked=\"true\"" : ""?>/> 일반                
                <input type="radio" value="200" name="viewmode" id="viewmode_200" <?= ($viewmode == "200") ? "checked=\"true\"" : ""?>/> 단위승률     
                <br>           
				<span class="search_lbl">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="start_date" name="start_date" style="width:65px" readonly="readonly" value="<?= $search_sdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/> ~
				<input type="text" class="search_text" id="end_date" name="end_date" style="width:65px" readonly="readonly" value="<?= $search_edate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/>
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->	
	<div class="search_result">
		<span><?= $search_sdate ?></span> ~ <span><?= $search_edate ?></span> 현황입니다
	</div>
<?
	if($viewmode == 1 || $viewmode == 100)
	{
?>
	
		<table class="tbl_list_basic1" style="width:1080px">
			<colgroup>
				<col width="70">
<?
			if($viewmode != "100")
			{
?>
				<col width="100">
<?
			}
		
			for($i=0; $i<$slotcnt; $i++)
			{
?>
				<col width="70">
<?
			}
?>            
			</colgroup>
        	<thead>
        		<tr>
        			<th>일자</th>
<?
				if($viewmode == "1")
				{
?>
        			<th>MODE</th>
<?
				}
				else if($viewmode == "2")
				{
?>
					<th>베팅레벨</th>
<?
				}
				for($i=0; $i<$slotcnt; $i++)
				{
?>
					<th><?= str_replace(" Slot", "", $slotlist["$i"]["slotname"])?></th>
<?
				}
?>   
	         	</tr>
	        </thead>
	        <tbody>
<?
	    for($i=0; $i<sizeof($daylist); $i++)
	    {
	    	$today = $daylist[$i]["today"];
	    	$type = $daylist[$i]["type"];
	    	$subtype = $daylist[$i]["subtype"];
	    	
	    	$modename = "";
	    	
	    	if($type == "1")
	    	{
	    		if($subtype == "0")
	    			$modename = "일반모드";
	    		if($subtype == "3")
	    			$modename = "트리트 티켓";
	    		else if($subtype == "4")
	    			$modename = "튜토리얼";
	    		else if($subtype == "5")
	    			$modename = "개인 승률부양";
	    		else if($subtype == "6")
	    			$modename = "승률부양(신규, 300만 미만)";
	    		else if($subtype == "7")
	    			$modename = "승률부양(30일이탈, 300미만, 30달러 미만)";
	    		else if($subtype == "8")
	    			$modename = "승률부양(30일이탈, 30달러이상)";
	    	}     	
	    	else if($type == "100")
	    		$modename = "전체";
	    	
	
	    	if($i == 0 || $today != $daylist[$i-1]["today"])
	    	{
	    		$sql = "SELECT count(*) FROM `tbl_slot_summarystat_daily` WHERE os_type = $os_type AND type = 100 AND today BETWEEN '$today' AND '$today'";
	    		$datecount = $db_analysis->getvalue($sql);
	    	}
    	
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($viewmode == "100")
				{
?>
				<td class="tdc point"><?= $today?></td>
<?
				}			
				else if($i == 0 || $today != $daylist[$i-1]["today"])
				{			
?>
					<td class="tdc point" rowspan="<?=$datecount?>"><?= $today?></td>

<?
				}
			
				if($viewmode != "100")
				{
?>
				<td class="tdc point"><?= $modename?></td>
<?	
				}
			
		    	for($j=4; $j<$slotcnt + 4; $j++)
		    	{
		    		$winrate = $daylist[$i][$j];
		    		$fontcolor = "#888";
		    		$fontweight = "normal";
		    		
		    		if($type == 100) // 전체
		    		{
		    			$fontweight = "bold";
		    			
		    			if($winrate > 99)
		    				$fontcolor = "#fb7878";
		    			else if(0 < $winrate && $winrate < 95)
		    				$fontcolor = "#5ab2da";
		    		}
		    		else if($type == 1 && ($subtype == 0 || $subtype == 1)) // 일반모드
		    		{
		    			if($winrate > 99)
		    				$fontcolor = "#fb7878";
		    			else if(0 < $winrate && $winrate < 95)
		    				$fontcolor = "#5ab2da";
		    		}
		    		else if($type == 1 && $subtype == 4) // 승률부양
		    		{
		    			if($winrate > 150)
		    				$fontcolor = "#fb7878";
		    			else if(0 < $winrate && $winrate < 100)
		    				$fontcolor = "#5ab2da";
		    		}
		    		else if($type == 1 && $subtype == 5) //개인 승률부양
		    		{
		    			if($winrate > 150)
		    				$fontcolor = "#fb7878";
		    			else if(0 < $winrate && $winrate < 100)
		    				$fontcolor = "#5ab2da";
		    		}    		
?>
	            	<td class="tdc" style="color: <?= $fontcolor?>;font-weight:<?= $fontweight?>;"><?= ($daylist[$i][$j] == "0.00") ? "-" : $daylist[$i][$j]."%" ?></td>
<?
	    		}
?>
        	</tr>
<?
    	}
    
    	if(sizeof($daylist) == 0)
    	{
?>
   			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   				<td class="tdc" colspan="<?= $slotcnt + 1?>">검색 결과가 없습니다.</td>
   			</tr>
<?
   		}
?>

        	</tbody>
		</table>
<?
	}
	else if($viewmode == 200)
	{
?>
		<table class="tbl_list_basic1" style="width:1080px">
			<colgroup>
				<col width="70">
<?
			for($i=0; $i<$slotcnt; $i++)
			{
?>
				<col width="70">
<?
			}
?>            
			</colgroup>
        	<thead>
        		<tr>
        			<th>일자</th>
<?
   				for($i=0; $i<$slotcnt; $i++)
				{
?>
					<th><?= str_replace(" Slot", "", $slotlist["$i"]["slotname"])?></th>
<?
				}
?>   
	         	</tr>
	        </thead>
	        <tbody>	        	
<?
	    for($i=0; $i<sizeof($play_rate_count); $i++)
	    {
	    	$today = $play_rate_count[$i]["writedate"];	    	
   	
	    	$cnt = 0;
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point"><?= $today?></td>
<?
			for($j=0; $j<$slotcnt; $j++)
			{
				$slottype = $slotlist[$j]["slottype"];
				$rate_playcount = $unit_rate_array[$today][$slottype];
				
				$fontcolor = "#888";
				$fontweight = "bold";
				
				$cnt ++;
					 
				if($rate_playcount > 99)
					$fontcolor = "#fb7878";
				else if(0 < $rate_playcount && $rate_playcount < 95)
					$fontcolor = "#5ab2da";			
?>
				<td class="tdc" style="color: <?= $fontcolor?>;font-weight:<?= $fontweight?>;"><?= ($rate_playcount == "0.00") ? "-" : $rate_playcount."%" ?></td>
<?
	    	}
?>
			</tr>
<?
		}
	}
?>
        	</tbody>
		</table>
	

</div>	
<div class="clear"></div>

<?
	$db_analysis->end();

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>