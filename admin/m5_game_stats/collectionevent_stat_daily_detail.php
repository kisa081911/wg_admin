<?
    $top_menu = "game_stats";
    $sub_menu = "collectionevent_stat_daily_detail";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $today = date("Y-m-d");
    
    $os_type = ($_GET["os_type"] == "") ? "4" :$_GET["os_type"];
    $search_event_idx = ($_GET["event_idx"] == "") ? "ALL" : $_GET["event_idx"];
    $search_startdate = $_GET["startdate"];
    $search_enddate = $_GET["enddate"];

    
    if($search_startdate == "")
        $search_startdate = date("Y-m-d", strtotime("-3 day"));
        
    if($search_enddate == "")
        $search_enddate = $today;
        
	$tail ="1=1";

    if($search_event_idx != "ALL")
        $tail = "eventidx = $search_event_idx";
    
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
    
    $event_idx_arr = $db_main2->gettotallist("SELECT event_idx FROM tbl_collection_event_setting ORDER BY event_idx ASC");
    
    $sql = " SELECT today, SUM(theme1_count) as theme1_count, SUM(theme2_count) as theme2_count
					, SUM(theme3_count) as theme3_count, SUM(themeall_count) as themeall_count, SUM(star_token1_count) as star_token1_count
					, SUM(star_token2_count) as star_token2_count, SUM(star_token3_count) as star_token3_count, SUM(reward_coin) as reward_coin
             FROM tbl_collection_stat_daily 
             WHERE $tail";
	$collection_data = $db_analysis->gettotallist($sql);
	
	$sql = " SELECT today, SUM(step1) as step1, SUM(step2) as step2
					, SUM(step3) as step3, SUM(step4) as step4, SUM(step5) as step5
					, SUM(step1_max_amount) as step1_max_amount, SUM(step2_max_amount) as step2_max_amount, SUM(step3_max_amount) as step3_max_amount
					, SUM(step4_max_amount) as step4_max_amount, SUM(step1_avg_amount) as step1_avg_amount, SUM(step2_avg_amount) as step2_avg_amount
					, SUM(step3_avg_amount) as step3_avg_amount, SUM(step4_avg_amount) as step4_avg_amount
					, SUM(theme1_max_amount) as theme1_max_amount, SUM(theme2_max_amount) as theme2_max_amount, SUM(theme3_max_amount) as theme3_max_amount
					, SUM(themeall_max_amount) as themeall_max_amount, SUM(theme1_avg_amount) as theme1_avg_amount, SUM(theme2_avg_amount) as theme2_avg_amount
					, SUM(theme3_avg_amount) as theme3_avg_amount, SUM(themeall_avg_amount) as themeall_avg_amount
					, SUM(reward_coin) as reward_coin
             FROM tbl_collection_stat_daily 
             WHERE  $tail
			 AND today BETWEEN '$search_startdate' AND '$search_enddate'
			 group by today
			 ORDER BY today DESC";
			 write_log($sql);
    $step_data = $db_analysis->gettotallist($sql);
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#startdate").datepicker({ });
	});
	
	$(function() {
		$("#enddate").datepicker({ });
	});
	
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.startdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    } 
	
	    if (search_form.enddate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    } 
	
	    search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap" style="height:76px;">
		<div class="title"><?= $top_menu_txt ?> &gt; <?=$platform_name?> Collection Event 통계</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
			<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />
				
				<span class="search_lbl ml20">이벤트 회차&nbsp;&nbsp;&nbsp;</span>
				<select name="event_idx" id="event_idx">										
				<option value="ALL" <?= ($event_idx=="ALL") ? "selected" : "" ?>>전체</option>
<?
    for ($i=0; $i<sizeof($event_idx_arr); $i++)
	{
	    $event_idx = $event_idx_arr[$i]["event_idx"];
?>	
                    
					<option value="<?= $event_idx ?>" <?= ($event_idx == $search_event_idx) ? "selected=\"true\"" : "" ?>><?= $event_idx ?></option>
<?						
	}
?>				</select>&nbsp;&nbsp;&nbsp;
				<span class="search_lbl ml20">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $search_startdate ?>" maxlength="10" style="width:65px"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" /> ~
				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $search_enddate ?>" style="width:65px" maxlength="10"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->

			<div class="search_result">
        		<span><?= $search_startdate ?> ~ <?= $search_enddate ?></span> 통계입니다
			</div>
			<div class="h2_title">[Collection 통계 (누적)]</div>
    		<table class="tbl_list_basic1">
    		<colgroup>
				<col width="120">
				<col width="120">
				<col width="120">
				<col width="120">
				<col width="120">
				<col width="120">
				<col width="120">
				<col width="120">
    		</colgroup>
            <thead>
                <tr>
        			<th>테마 1 </br>완성 유저 수</th>
					<th>테마 2 </br>완성 유저 수</th>
					<th>테마 3 </br>완성 유저 수</th>
					<th>모든 테마 </br>완성 유저 수</th>
        			<th>1성 아이템 </br>획득 수량</th>
					<th>2성 아이템 </br>획득 수량</th>
					<th>3성 아이템 </br>획득 수량</th>
        			
        		</tr>
            </thead>
            <tbody>
	
<?
    
    for($i=0; $i<sizeof($collection_data); $i++)
    {
        $today = $collection_data[$i]["today"];
		$theme1_count = $collection_data[$i]["theme1_count"];
		$theme2_count = $collection_data[$i]["theme2_count"];
		$theme3_count = $collection_data[$i]["theme3_count"];
		$themeall_count = $collection_data[$i]["themeall_count"];

		$star_token1_count = $collection_data[$i]["star_token1_count"];
		$star_token2_count = $collection_data[$i]["star_token2_count"];
		$star_token3_count = $collection_data[$i]["star_token3_count"];

		$total_token_count = $star_token1_count+$star_token2_count+$star_token3_count;
		$star_token1_rate = round($star_token1_count/$total_token_count*100,2);
		$star_token2_rate = round($star_token2_count/$total_token_count*100,2);
		$star_token3_rate = round($star_token3_count/$total_token_count*100,2);

		
        
        ?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="tdc"><?= number_format($theme1_count) ?></td>
			<td class="tdc"><?= number_format($theme2_count) ?></td>
			<td class="tdc"><?= number_format($theme3_count) ?></td>
			<td class="tdc"><?= number_format($themeall_count) ?></td>
			<td class="tdc"><?= number_format($star_token1_count)."(".$star_token1_rate."%)" ?></td>
			<td class="tdc"><?= number_format($star_token2_count)."(".$star_token2_rate."%)" ?></td>
			<td class="tdc"><?= number_format($star_token3_count)."(".$star_token3_rate."%)" ?></td>
       </tr>
<?
	}
	if(sizeof($collection_data) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="7">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>

            </tbody>
		</table>
		</br></br></br>
		<div class="h2_title">[미션 통계 (일자별)]</div>
		<table class="tbl_list_basic1">
    		<colgroup>
    			<col width="">
    			<col width="">
                <col width="">
				<col width="">
				<col width="">
				<col width="">
    		</colgroup>
            <thead>
                <tr>
                 	<th>날짜</th>
					<th>완료한 스탭 </br>단계 1</th>
					<th>완료한 스탭 </br>단계 2</th>
					<th>완료한 스탭 </br>단계 3</th>
					<th>완료한 스탭 </br>단계 4</th>
					<th>완료한 스탭 </br>단계 5</th>
        		</tr>
            </thead>
            <tbody>
	
<?
    
    for($i=0; $i<sizeof($step_data); $i++)
    {
        $today = $step_data[$i]["today"];
		$step1 = $step_data[$i]["step1"];
		$step2 = $step_data[$i]["step2"];
		$step3 = $step_data[$i]["step3"];
		$step4 = $step_data[$i]["step4"];
		$step5 = $step_data[$i]["step5"];
        
        ?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">

			<td class="tdc point_title" valign="center"><?= $today ?></td>
            <td class="tdc"><?= number_format($step1) ?></td>
			<td class="tdc"><?= number_format($step2) ?></td>
			<td class="tdc"><?= number_format($step3) ?></td>
			<td class="tdc"><?= number_format($step4) ?></td>
			<td class="tdc"><?= number_format($step5) ?></td>
       </tr>
<?
	}
	if(sizeof($collection_data) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="7">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>

            </tbody>
		</table>
		</br></br></br>
		<div class="h2_title">[리워드 통계 (일자별)] -  리워드 평균(1인최대)</div>
		<table class="tbl_list_basic1">
    		<colgroup>
    			<col width="">
    			<col width="">
                <col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
    		</colgroup>
            <thead>
                <tr>
                 	<th>날짜</th>
					<th>테마 1 완성</th>
					<th>테마 2 완성</th>
					<th>테마 3 완성</th>
					<th>최종 테마 완성</th>
					<th>스탭 1 완성</th>
					<th>스탭 2 완성</th>
					<th>스탭 3 완성</th>
					<th>스탭 4 완성</th>
					<th>스타토큰 휠 리워드 </br> 평균 획득 코인</th>
        		</tr>
            </thead>
            <tbody>
	
<?
    
    for($i=0; $i<sizeof($step_data); $i++)
    {
        $today = $step_data[$i]["today"];
		$theme1_max_amount = $step_data[$i]["theme1_max_amount"];
		$theme1_avg_amount = $step_data[$i]["theme1_avg_amount"];
		$theme2_max_amount = $step_data[$i]["theme2_max_amount"];
		$theme2_avg_amount = $step_data[$i]["theme2_avg_amount"];
		$theme3_max_amount = $step_data[$i]["theme3_max_amount"];
		$theme3_avg_amount = $step_data[$i]["theme3_avg_amount"];
		$themeall_max_amount = $step_data[$i]["themeall_max_amount"];
		$themeall_avg_amount = $step_data[$i]["themeall_avg_amount"];

		$step1_max_amount = $step_data[$i]["step1_max_amount"];
		$step1_avg_amount = $step_data[$i]["step1_avg_amount"];
		$step2_max_amount = $step_data[$i]["step2_max_amount"];
		$step2_avg_amount = $step_data[$i]["step2_avg_amount"];
		$step3_max_amount = $step_data[$i]["step3_max_amount"];
		$step3_avg_amount = $step_data[$i]["step3_avg_amount"];
		$step4_max_amount = $step_data[$i]["step4_max_amount"];
		$step4_avg_amount = $step_data[$i]["step4_avg_amount"];
		$reward_coin = $step_data[$i]["reward_coin"];
        
        ?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">

			<td class="tdc point_title" valign="center"><?= $today ?></td>
			<td class="tdc"><?= number_format($theme1_avg_amount)."(". number_format($theme1_max_amount).")" ?></td>
			<td class="tdc"><?= number_format($theme2_avg_amount)."(". number_format($theme2_max_amount).")" ?></td>
			<td class="tdc"><?= number_format($theme3_avg_amount)."(". number_format($theme3_max_amount).")" ?></td>
			<td class="tdc"><?= number_format($themeall_avg_amount)."(". number_format($themeall_max_amount).")" ?></td>
			<td class="tdc"><?= number_format($step1_avg_amount)."(". number_format($step1_max_amount).")" ?></td>
			<td class="tdc"><?= number_format($step2_avg_amount)."(". number_format($step2_max_amount).")" ?></td>
			<td class="tdc"><?= number_format($step3_avg_amount)."(". number_format($step3_max_amount).")" ?></td>
			<td class="tdc"><?= number_format($step4_avg_amount)."(". number_format($step4_max_amount).")" ?></td>
			<td class="tdc"><?= number_format($reward_coin) ?></td>
       </tr>
<?
	}
	if(sizeof($collection_data) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="7">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>

            </tbody>
    	</table>
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main2->end();
	$db_analysis->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>