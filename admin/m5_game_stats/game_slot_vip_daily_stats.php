<?
	$top_menu = "game_stats";
	$sub_menu = "game_slot_vip_daily_stats";
	 
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$viplevel = ($_GET["viplevel"] == "") ? "0" :$_GET["viplevel"];
	$search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    
    if($search_start_createdate == "")
    	$search_start_createdate = date("Y-m-d",strtotime("-4 days"));
    
    if($search_end_createdate == "")
    	$search_end_createdate = $today;
    
    $viplevel_sql = "";
    
    if($viplevel == 1)
    	$viplevel_sql = " AND vip_level = 0 ";
    else if($viplevel == 2)
    	$viplevel_sql = " AND vip_level = 1 ";
    else if($viplevel == 3)
    	$viplevel_sql = " AND vip_level = 2 ";
    else if($viplevel == 4)
    	$viplevel_sql = " AND vip_level = 3 ";
    else if($viplevel == 5)
    	$viplevel_sql = " AND vip_level = 4 ";
    else if($viplevel == 6)
    	$viplevel_sql = " AND vip_level = 5 ";
    else if($viplevel == 7)
    	$viplevel_sql = " AND vip_level = 6 ";
    else if($viplevel == 8)
    	$viplevel_sql = " AND vip_level = 7 ";
    else if($viplevel == 9)
    	$viplevel_sql = " AND vip_level = 8 ";
    else if($viplevel == 10)
    	$viplevel_sql = " AND vip_level = 9 ";
    else if($viplevel == 11)
    	$viplevel_sql = " AND vip_level = 10 ";
    
    $db_other = new CDatabase_Other();
    
    $sql = "SELECT today, ".
			"vip_level AS vip_level,	".
			"COUNT(useridx) AS usercnt, FLOOR(AVG(currentcoin)) AS currentcoin,	".
			"SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS winrate,	".
			"SUM(h_moneyin) AS h_moneyin, SUM(h_moneyout) AS h_moneyout, ROUND(SUM(h_moneyout)/SUM(h_moneyin)*100, 2) AS h_winrate,	".
			"SUM(moneyin) + SUM(h_moneyin) AS total_moneyin,	SUM(moneyout) + SUM(h_moneyout) AS total_moneyout, ". 
			"ROUND((SUM(moneyout) + SUM(h_moneyout))/(SUM(moneyin) + SUM(h_moneyin))*100, 2) AS total_winrate,	".
			"SUM(playtime) + SUM(h_playtime) AS playtime, SUM(playcount) + SUM(h_playcount) AS playcount,	".
			"SUM(redeem) AS redeem, SUM(purchasecount) AS purchasecount, SUM(purchaseamount) AS purchaseamount, SUM(purchasecoin) AS purchasecoin,	".
			"SUM(famelevel) AS famelevel, SUM(ty_point) AS ty_point, SUM(freecoin) AS freecoin, SUM(jackpot) AS jackpot	".
			"FROM `tbl_user_playstat_daily`	".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' $viplevel_sql".
			"GROUP BY today, vip_level	".
			"ORDER BY today DESC, vip_level ASC";
    $viplevel_data = $db_other->gettotallist($sql);
    
    $sql = "SELECT ".
    		"vip_level AS vip_level,	".
			"COUNT(useridx) AS usercnt, FLOOR(AVG(currentcoin)) AS currentcoin,	".
			"SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout, ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS winrate,	".
			"SUM(h_moneyin) AS h_moneyin, SUM(h_moneyout) AS h_moneyout, ROUND(SUM(h_moneyout)/SUM(h_moneyin)*100, 2) AS h_winrate,	".
			"SUM(moneyin) + SUM(h_moneyin) AS total_moneyin,	SUM(moneyout) + SUM(h_moneyout) AS total_moneyout, ". 
			"ROUND((SUM(moneyout) + SUM(h_moneyout))/(SUM(moneyin) + SUM(h_moneyin))*100, 2) AS total_winrate,	".
			"SUM(playtime) + SUM(h_playtime) AS playtime, SUM(playcount) + SUM(h_playcount) AS playcount,	".
			"SUM(redeem) AS redeem, SUM(purchasecount) AS purchasecount, SUM(purchaseamount) AS purchaseamount, SUM(purchasecoin) AS purchasecoin,	".
			"SUM(famelevel) AS famelevel, SUM(ty_point) AS ty_point, SUM(freecoin) AS freecoin, SUM(jackpot) AS jackpot	".
    		"FROM `tbl_user_playstat_daily`	".
    		"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' $viplevel_sql".
    		"GROUP BY vip_level	".
    		"ORDER BY vip_level ASC";
    $total_viplevel_data = $db_other->gettotallist($sql);
    
    $sql = "SELECT ".
			"today, vip_level, SUM(treatamount) AS treatamount, SUM(h_treatamount) AS h_treatamount, SUM(treatamount)+SUM(h_treatamount) AS total_treatamount ".
			"FROM `tbl_slot_playstat_daily`	".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' $viplevel_sql ".
			"GROUP BY today, vip_level ".
			"ORDER BY today DESC, vip_level ASC";    
    $viplevel_treat_data = $db_other->gettotallist($sql);
    
    $viplevel_normal_treat_array = array();
    $viplevel_highroller_treat_array = array();
    $viplevel_total_treat_array = array();
    
    $total_vip_level_normal_treat = array();
    $total_vip_level_highroller_treat = array();
    $total_vip_level_total_treat = array();
    
    for($i=0; $i<sizeof($viplevel_treat_data); $i++)
    {
    	$today = $viplevel_treat_data[$i]["today"];
    	$vip_level = $viplevel_treat_data[$i]["vip_level"];
    	$treatamount = $viplevel_treat_data[$i]["treatamount"];
    	$h_treatamount = $viplevel_treat_data[$i]["h_treatamount"];
    	$total_treatamount = $viplevel_treat_data[$i]["total_treatamount"];
    	
    	$viplevel_normal_treat_array[$today][$vip_level] = $treatamount;
    	$total_vip_level_normal_treat[$vip_level] += $treatamount;
    	$viplevel_highroller_treat_array[$today][$vip_level] = $h_treatamount;
    	$total_vip_level_highroller_treat[$vip_level] += $h_treatamount;
    	$viplevel_total_treat_array[$today][$vip_level] = $total_treatamount;
    	$total_vip_level_total_treat[$vip_level] += $total_treatamount;
    }
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="game_slot_vip_daily_stats.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 게임 Vip 통계</div>
		<div class="search_box">
			Viplevel&nbsp;:&nbsp; 
			<select name="viplevel" id="viplevel">
					<option value="" <?= ($viplevel=="0") ? "selected" : "" ?>>전체</option>					
					<option value="1" <?= ($viplevel=="1") ? "selected" : "" ?>>0</option>
					<option value="2" <?= ($viplevel=="2") ? "selected" : "" ?>>1</option>                       
					<option value="3" <?= ($viplevel=="3") ? "selected" : "" ?>>2</option>
					<option value="4" <?= ($viplevel=="4") ? "selected" : "" ?>>3</option>
					<option value="5" <?= ($viplevel=="5") ? "selected" : "" ?>>4</option>
					<option value="6" <?= ($viplevel=="6") ? "selected" : "" ?>>5</option>
					<option value="7" <?= ($viplevel=="7") ? "selected" : "" ?>>6</option>
					<option value="8" <?= ($viplevel=="8") ? "selected" : "" ?>>7</option>
					<option value="9" <?= ($viplevel=="9") ? "selected" : "" ?>>8</option>
					<option value="10" <?= ($viplevel=="10") ? "selected" : "" ?>>9</option>
					<option value="11" <?= ($viplevel=="11") ? "selected" : "" ?>>10</option>
			</select>&nbsp;&nbsp;&nbsp;
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다.(참고 : moneyout은 트리트 티켓이 포함된 값)
	</div>
	
	<div id="tab_content_1">
            <table class="tbl_list_basic1" style="width:1520px">
            <colgroup>
                <col width="100">
                <col width="50">
                <col width="50">
                <col width="100">
               <!-- <col width="100">
                <col width="100">
                <col width="100">
                <col width="70">
                <col width="70">
                <col width="100">
                <col width="100">
                <col width="100">
                <col width="70">
                <col width="70"> -->
                <col width="100">
                <col width="100">
                <col width="100">
                <col width="100">
                <col width="50">
                <col width="70">
                <col width="80">                
                <col width="70">
                <col width="50">
                <col width="50">
                <col width="50">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">                                 
            </colgroup>
            <thead>
            <tr>
                <th rowspan="2">날짜</th>
                <th class="tdc" rowspan="2">Viplevel</th>
                <th class="tdc" rowspan="2">유저수</th>
                <th class="tdc" rowspan="2">평균보유코인</th>                                
                <!--<th class="tdc" colspan="5">레귤러</th>
                <th class="tdc" colspan="5">하이롤러</th> -->
                <th class="tdc" colspan="7">전체</th>                
                <th class="tdc" rowspan="2">평균<br>playtime</th>
                <th class="tdc" rowspan="2">평균<br>playcount</th>
                <th class="tdc" rowspan="2">결제<br>횟수</th>
                <th class="tdc" rowspan="2">결제<br>금액</th>
                <th class="tdc" rowspan="2">평균<br>결제금액</th>
                <th class="tdc" rowspan="2">결제<br>코인</th>
                <th class="tdc" rowspan="2">평균<br>FameLevel</th>
                <th class="tdc" rowspan="2">jackpot</th>                
            </tr>
            <tr>
            	<!--<th class="tdc">moneyin(A)</th>
                <th class="tdc">moneyout(B)</th>                
                <th class="tdc">트리트티켓(C)</th>
                <th class="tdc">(B/A)</th>
                <th class="tdc">(B-C)/A</th>
                <th class="tdc">moneyin(D)</th>
                <th class="tdc">moneyout(E)</th>
                <th class="tdc">트리트티켓(F)</th>
                <th class="tdc">(E/D)</th>                
                <th class="tdc">(E-F)/D</th> -->
                <th class="tdc">moneyin(A)</th>
                <th class="tdc">moneyout(B)</th>
                <th class="tdc">트리트티켓(C)</th>
                <th class="tdc">Redeem(D)</th>
                <th class="tdc">(B/A)</th>                
                <th class="tdc">(B-C)/A</th>
                <th class="tdc">(B-C+D)/A</th>
            </tr>
            </thead>
            <tbody>
<?
			for($i=0; $i<sizeof($viplevel_data); $i++)
			{
				$today = $viplevel_data[$i]["today"];
				$vip_level = $viplevel_data[$i]["vip_level"];
				$usercnt = $viplevel_data[$i]["usercnt"];
				$currentcoin = $viplevel_data[$i]["currentcoin"];
				
				$moneyin = $viplevel_data[$i]["moneyin"];
				$treatamount = $viplevel_normal_treat_array[$today][$vip_level];
				$moneyout = $viplevel_data[$i]["moneyout"];
				$winrate = ($moneyin == 0 ? 0:round(($moneyout) / $moneyin * 10000) / 100);
				
				$h_moneyin = $viplevel_data[$i]["h_moneyin"];				
				$h_treatamount = $viplevel_highroller_treat_array[$today][$vip_level];
				$h_moneyout = $viplevel_data[$i]["h_moneyout"];
				$h_winrate = ($h_moneyin == 0 ? 0:round(($h_moneyout) / $h_moneyin * 10000) / 100);				
				
				$total_moneyin = $viplevel_data[$i]["total_moneyin"];				
				$total_treatamount = $viplevel_total_treat_array[$today][$vip_level];
				$total_moneyout = $viplevel_data[$i]["total_moneyout"];
				$total_winrate = ($total_treatamount == 0 ? 0:round(($total_moneyout) / $total_moneyin * 10000) / 100);
				$redeem = $viplevel_data[$i]["redeem"];				
				
				$playtime = $viplevel_data[$i]["playtime"]/$usercnt;
				$playcount = $viplevel_data[$i]["playcount"]/$usercnt;
				$purchasecount = $viplevel_data[$i]["purchasecount"];
				$purchaseamount = $viplevel_data[$i]["purchaseamount"]/10;
				$avg_purchaseamount = ($viplevel_data[$i]["purchaseamount"]/10)/$usercnt;
				$purchasecoin = $viplevel_data[$i]["purchasecoin"];
				$famelevel = $viplevel_data[$i]["famelevel"]/$usercnt;				
				$jackpot = $viplevel_data[$i]["jackpot"];
				
				
				$normal_sub_treatamount_rate = ($moneyin == 0 ? 0:round(($moneyout - $treatamount) / $moneyin * 10000) / 100);
				$highroller_sub_treatamount_rate = ($h_moneyin == 0 ? 0:round(($h_moneyout - $h_treatamount) / $h_moneyin * 10000) / 100);
				$total_sub_treatamount_rate = ($total_moneyin == 0 ? 0:round(($total_moneyout - $total_treatamount) / $total_moneyin * 10000) / 100);
				$total_sub_treatamount_add_redeem_rate = ($total_moneyin == 0 ? 0:round(($total_moneyout - $total_treatamount+$redeem) / $total_moneyin * 10000) / 100);
				
				$std_viplevel_str = "";
				
				if($vip_level == "0")
					$std_viplevel_str = "0";
				else if($vip_level == "1")
					$std_viplevel_str = "1";
				else if($vip_level == "2")
					$std_viplevel_str = "2";
				else if($vip_level == "3")
					$std_viplevel_str = "3";
				else if($vip_level == "4")
					$std_viplevel_str = "4";
				else if($vip_level == "5")
					$std_viplevel_str = "5";
				else if($vip_level == "6")
					$std_viplevel_str = "6";
				else if($vip_level == "7")
					$std_viplevel_str = "7";
				else if($vip_level == "8")
					$std_viplevel_str = "8";
				else if($vip_level == "9")
					$std_viplevel_str = "9";
				else if($vip_level == "10")
					$std_viplevel_str = "10";

?>
				<tr>
<?
				if($i == 0 || $today != $viplevel_data[$i-1]["today"])
				{
					if($std_viplevel_str != "")
					{
						$sql = "SELECT today, ".
								"vip_level AS vip_level ".								
								"FROM `tbl_user_playstat_daily` ".
								"WHERE '$today' <= today AND today <= '$today' $viplevel_sql".
								"GROUP BY vip_level ".
								"ORDER BY vip_level ASC;";
						$viplevel_rowdata = $db_other->gettotallist($sql);
?>				
					<td class="tdc point_title" rowspan="<?=sizeof($viplevel_rowdata)?>"><?= $today ?></td>
					
<?
					}
					else
					{
						
?>
						<td class="tdc point_title" rowspan="1"><?= $today ?></td>
<?					
					}
				}
?>	
	
					<td class="tdc"><?= $std_viplevel_str ?></td>
					<td class="tdc"><?= number_format($usercnt) ?></td>
					<td class="tdc"><?= number_format($currentcoin) ?></td>
					<!--<td class="tdc"><?= number_format($moneyin) ?></td>
					<td class="tdc"><?= number_format($moneyout) ?></td>
					<td class="tdc"><?= number_format($treatamount) ?></td>
					<td class="tdc"><?= $winrate ?>%</td>					
					<td class="tdc"><?= $normal_sub_treatamount_rate ?>%</td>
					<td class="tdc"><?= number_format($h_moneyin) ?></td>
					<td class="tdc"><?= number_format($h_moneyout) ?></td>
					<td class="tdc"><?= number_format($h_treatamount) ?></td>
					<td class="tdc"><?= $h_winrate ?>%</td>					
					<td class="tdc"><?= $highroller_sub_treatamount_rate ?>%</td>-->
					<td class="tdc"><?= number_format($total_moneyin) ?></td>
					<td class="tdc"><?= number_format($total_moneyout) ?></td>
					<td class="tdc"><?= number_format($total_treatamount) ?></td>
					<td class="tdc"><?= number_format($redeem) ?></td>
					<td class="tdc"><?= $total_winrate ?>%</td>					
					<td class="tdc"><?= $total_sub_treatamount_rate ?>%</td>
					<td class="tdc"><?= $total_sub_treatamount_add_redeem_rate ?>%</td>
					<td class="tdc"><?= number_format($playtime) ?></td>
					<td class="tdc"><?= number_format($playcount) ?></td>
					<td class="tdc"><?= number_format($purchasecount) ?></td>
					<td class="tdc">$<?= number_format($purchaseamount) ?></td>
					<td class="tdc">$<?= number_format($avg_purchaseamount,2) ?></td>					
					<td class="tdc"><?= number_format($purchasecoin) ?></td>
					<td class="tdc"><?= number_format($famelevel) ?></td>
					<td class="tdc"><?= number_format($jackpot) ?></td>					
				</tr>
<?
			}

			for($i=0; $i<sizeof($total_viplevel_data); $i++)
			{
				$vip_level = $total_viplevel_data[$i]["vip_level"];
				$usercnt = $total_viplevel_data[$i]["usercnt"];
				$currentcoin = $total_viplevel_data[$i]["currentcoin"];
				
				$moneyin = $total_viplevel_data[$i]["moneyin"];
				$moneyout = $total_viplevel_data[$i]["moneyout"] + $total_vip_level_normal_treat[$vip_level];
				$winrate = ($moneyin == 0 ? 0:round(($moneyout) / $moneyin * 10000) / 100);				
				
				$h_moneyin = $total_viplevel_data[$i]["h_moneyin"];
				$h_moneyout = $total_viplevel_data[$i]["h_moneyout"] + $total_vip_level_highroller_treat[$vip_level];
				$h_winrate = ($h_moneyin == 0 ? 0:round(($h_moneyout) / $h_moneyin * 10000) / 100);				
				
				$total_moneyin = $total_viplevel_data[$i]["total_moneyin"];
				$total_moneyout = $total_viplevel_data[$i]["total_moneyout"] + $total_vip_level_total_treat[$vip_level];
				$total_winrate = ($total_moneyin == 0 ? 0:round(($total_moneyout) / $total_moneyin * 10000) / 100);
				$redeem = $total_viplevel_data[$i]["redeem"];				
				
				$playtime = $total_viplevel_data[$i]["playtime"]/$usercnt;
				$playcount = $total_viplevel_data[$i]["playcount"]/$usercnt;
				$purchasecount = $total_viplevel_data[$i]["purchasecount"];
				$purchaseamount = $total_viplevel_data[$i]["purchaseamount"]/10;
				$avg_purchaseamount = ($total_viplevel_data[$i]["purchaseamount"]/10)/$usercnt;
				$purchasecoin = $total_viplevel_data[$i]["purchasecoin"];
				$famelevel = $total_viplevel_data[$i]["famelevel"]/$usercnt;				
				$jackpot = $total_viplevel_data[$i]["jackpot"];
				
				$normal_sub_treatamount_rate = ($moneyin == 0 ? 0:round(($moneyout - $total_vip_level_normal_treat[$vip_level]) / $moneyin * 10000) / 100);
				$highroller_sub_treatamount_rate = ($h_moneyin == 0 ? 0:round(($h_moneyout - $total_vip_level_highroller_treat[$vip_level]) / $h_moneyin * 10000) / 100);
				$total_sub_treatamount_rate = ($total_moneyin == 0 ? 0:round(($total_moneyout - $total_vip_level_total_treat[$vip_level]) / $total_moneyin * 10000) / 100);
				$total_sub_treatamount_add_redeem_rate = ($total_moneyin == 0 ? 0:round(($total_moneyout - $total_vip_level_total_treat[$vip_level]+$redeem) / $total_moneyin * 10000) / 100);
				
				$std_viplevel_str = "";
				
				if($vip_level == "0")
					$std_viplevel_str = "0";
				else if($vip_level == "1")
					$std_viplevel_str = "1";
				else if($vip_level == "2")
					$std_viplevel_str = "2";
				else if($vip_level == "3")
					$std_viplevel_str = "3";
				else if($vip_level == "4")
					$std_viplevel_str = "4";
				else if($vip_level == "5")
					$std_viplevel_str = "5";
				else if($vip_level == "6")
					$std_viplevel_str = "6";
				else if($vip_level == "7")
					$std_viplevel_str = "7";
				else if($vip_level == "8")
					$std_viplevel_str = "8";
				else if($vip_level == "9")
					$std_viplevel_str = "9";
				else if($vip_level == "10")
					$std_viplevel_str = "10";
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($i == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($total_viplevel_data)?>" valign="center">Total</td>
<?
				}
?>
					<td class="tdc"><?= $std_viplevel_str ?></td>
					<td class="tdc"><?= number_format($usercnt) ?></td>
					<td class="tdc"><?= number_format($currentcoin) ?></td>
					<!--<td class="tdc"><?= number_format($moneyin) ?></td>
					<td class="tdc"><?= number_format($moneyout) ?></td>
					<td class="tdc"><?= number_format($total_vip_level_normal_treat[$vip_level]) ?></td>
					<td class="tdc"><?= $winrate ?>%</td>					
					<td class="tdc"><?= $normal_sub_treatamount_rate ?>%</td>
					<td class="tdc"><?= number_format($h_moneyin) ?></td>
					<td class="tdc"><?= number_format($h_moneyout) ?></td>
					<td class="tdc"><?= number_format($total_vip_level_highroller_treat[$vip_level]) ?></td>
					<td class="tdc"><?= $h_winrate ?>%</td>					
					<td class="tdc"><?= $highroller_sub_treatamount_rate ?>%</td>-->
					<td class="tdc"><?= number_format($total_moneyin) ?></td>
					<td class="tdc"><?= number_format($total_moneyout) ?></td>
					<td class="tdc"><?= number_format($total_vip_level_total_treat[$vip_level]) ?></td>
					<td class="tdc"><?= number_format($redeem) ?></td>
					<td class="tdc"><?= $total_winrate ?>%</td>					
					<td class="tdc"><?= $total_sub_treatamount_rate ?>%</td>
					<td class="tdc"><?= $total_sub_treatamount_add_redeem_rate ?>%</td>					
					<td class="tdc"><?= number_format($playtime) ?></td>
					<td class="tdc"><?= number_format($playcount) ?></td>
					<td class="tdc"><?= number_format($purchasecount) ?></td>
					<td class="tdc">$<?= number_format($purchaseamount) ?></td>
					<td class="tdc">$<?= number_format($avg_purchaseamount,2) ?></td>
					<td class="tdc"><?= number_format($purchasecoin) ?></td>
					<td class="tdc"><?= number_format($famelevel) ?></td>
					<td class="tdc"><?= number_format($jackpot) ?></td>	
				</tr>

<?
			}
?>
			</tbody>
            </table>
     	</div>        
     </form>	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_other->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
