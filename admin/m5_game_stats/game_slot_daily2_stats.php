<?
    $top_menu = "game_stats";
    $sub_menu = "game_slot_daily2_stats";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");    

    $os_type = ($_GET["os_type"] == "") ? "0" :$_GET["os_type"];
    $startdate = ($_GET["startdate"] == "") ? date("Y-m-d", mktime(0,0,0,date("m"),date("d")-15,date("Y"))) : $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
    $pagename = "game_slot_daily2_stats.php";
        
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    $db_analysis = new CDatabase_Analysis();  
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}
	
	$tail = " WHERE 1=1 ";
	
	if($os_type == "0")
	{
		$table = "tbl_game_cash_stats_daily2";
		$online_log_table = "user_online_game_log WHERE 1=1 AND ";
		$online_log_daily_table = "user_online_game_log_daily WHERE 1=1 AND ";
		$os_txt = "Web";
	}
	else if($os_type == "1")
	{
		$tail .= " AND ios = 1 ";
		$table = "tbl_game_cash_stats_ios_daily2";
		$online_log_table = "user_online_game_mobile_log WHERE os_type = 1 AND ";
		$online_log_daily_table = "user_online_game_mobile_log_daily WHERE os_type = 1 AND ";
		$os_txt = "IOS";
	}
	else if($os_type == "2")
	{
		$tail .= " AND android = 1 ";
		$table = "tbl_game_cash_stats_android_daily2";
		$online_log_table = "user_online_game_mobile_log WHERE os_type = 2 AND ";
		$online_log_daily_table = "user_online_game_mobile_log_daily WHERE os_type = 2 AND ";
		$os_txt = "Android";
	}
	else if($os_type == "3")
	{
		$tail .= " AND amazon = 1 ";
		$table = "tbl_game_cash_stats_amazon_daily2";
		$online_log_table = "user_online_game_mobile_log WHERE os_type = 3 AND ";
		$online_log_daily_table = "user_online_game_mobile_log_daily WHERE os_type = 3 AND ";
		$os_txt = "Amazon";
	}

	$sql = "SELECT slottype, betlevel, MODE, IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout),0) AS moneyout, LEFT(writedate, 10) AS writedate ".
			"FROM $table ".
			"WHERE MODE IN (0,31) AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY writedate ORDER BY writedate DESC";			
	$action_list = $db_main2->gettotallist($sql);
	
	// unit 승률
	$unit_rate_sql = "SELECT writedate, ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2) AS unit_rate_new, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unit_rate ".
					"FROM	".
					"	(	".
					"		SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout	,SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout ".
					"		FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	". 
					"		WHERE 0 <= t1.betlevel AND t1.betlevel <= 115 AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' AND MODE IN (0,31) ".
					"		GROUP BY slottype, betlevel, writedate	".
					") t3 GROUP BY writedate;";
	
	$unit_rate_list = $db_main2->gettotallist($unit_rate_sql);
		
	$unit_rate_array = array();
	
	for($e = 0; $e < sizeof($unit_rate_list); $e++)
	{
		$today = $unit_rate_list[$e]["writedate"];
		$unit_rate = $unit_rate_list[$e]["unit_rate"];
		$unit_rate_new = ($unit_rate_list[$e]["unit_rate_new"]=="")? 0 : $unit_rate_list[$e]["unit_rate_new"];
		
		if(($today >'2017-11-20' && $os_type == 0) ||($today >'2017-11-22' && $os_type != 0))
			$unit_rate_array[$today] = $unit_rate_new;
		else 
			$unit_rate_array[$today] = $unit_rate;
	}
	
	if($enddate > '2017-11-20' && $os_type == 0)
	{
		$total_unit_rate_sql = "SELECT SUM(tt_moneyout) AS tt_moneyout, SUM(tt_moneyin) AS tt_moneyin ".
				"FROM	".
				"(	".
				"	SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout	".
				"	FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
				"	WHERE 0 <= t1.betlevel AND t1.betlevel <= 115 AND writedate BETWEEN '$startdate 00:00:00' AND '2017-11-20 23:59:59' AND MODE IN (0,31) ".
				"	GROUP BY slottype, betlevel	".
				") t3";
		$total_unit_rate_arr = $db_main2->getarray($total_unit_rate_sql);
		$total_unit_rate_moneyin =$total_unit_rate_arr["tt_moneyin"];
		$total_unit_rate_moneyout =$total_unit_rate_arr["tt_moneyout"];
		
		$tail_startdate ="2017-11-20";
		if($startdate > "2017-11-20" )
		{
			$tail_startdate = $startdate;
		}
		
		
		$total_unit_rate_new_sql=" SELECT SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout ".
				" FROM ( ".
				"	  SELECT t1.slottype, betlevel, SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout ".
				"      FROM   $table t1   ".
				"	   WHERE 0 <= t1.betlevel AND t1.betlevel <= 115 AND writedate BETWEEN '$tail_startdate 00:00:00' AND '$enddate 23:59:59' AND MODE IN (0,31) ".
				" AND MODE NOT IN ( 4, 9, 26, 29 ) GROUP  BY slottype, betlevel ) t3 ";
		$total_unit_rate_new_arr = $db_main2->getarray($total_unit_rate_new_sql);
		$total_unit_rate_moneyin += ($total_unit_rate_new_arr["unit_moneyin"]=="")?0:$total_unit_rate_new_arr["unit_moneyin"];
		$total_unit_rate_moneyout += ($total_unit_rate_new_arr["unit_moneyout"]=="")?0:$total_unit_rate_new_arr["unit_moneyout"];
		
		$total_unit_rate_value = round($total_unit_rate_moneyout/$total_unit_rate_moneyin*100,2);
	}
	else if ($enddate > '2017-11-22' && $os_type != 0)
	{
		$total_unit_rate_sql = "SELECT SUM(tt_moneyout) AS tt_moneyout, SUM(tt_moneyin) AS tt_moneyin ".
				"FROM	".
				"(	".
				"	SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout	".
				"	FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
				"	WHERE 0 <= t1.betlevel AND t1.betlevel <= 115 AND writedate BETWEEN '$startdate 00:00:00' AND '2017-11-22 23:59:59' AND MODE IN (0,31) ".
				"	GROUP BY slottype, betlevel	".
				") t3";
		$total_unit_rate_arr = $db_main2->getarray($total_unit_rate_sql);
		$total_unit_rate_moneyin =$total_unit_rate_arr["tt_moneyin"];
		$total_unit_rate_moneyout =$total_unit_rate_arr["tt_moneyout"];
		
		$tail_startdate ="2017-11-22";
		if($startdate > "2017-11-22" )
		{
			$tail_startdate = $startdate;
		}
		
		
		$total_unit_rate_new_sql=" SELECT SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout ".
				" FROM ( ".
				"	  SELECT t1.slottype, betlevel, SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout ".
				"      FROM   $table t1   ".
				"	   WHERE 0 <= t1.betlevel AND t1.betlevel <= 115 AND writedate BETWEEN '$tail_startdate 00:00:00' AND '$enddate 23:59:59' AND MODE IN (0,31) ".
				" AND MODE NOT IN ( 4, 9, 26, 29 ) GROUP  BY slottype, betlevel ) t3 ";
		$total_unit_rate_new_arr = $db_main2->getarray($total_unit_rate_new_sql);
		$total_unit_rate_moneyin += ($total_unit_rate_new_arr["unit_moneyin"]=="")?0:$total_unit_rate_new_arr["unit_moneyin"];
		$total_unit_rate_moneyout += ($total_unit_rate_new_arr["unit_moneyout"]=="")?0:$total_unit_rate_new_arr["unit_moneyout"];
		
		$total_unit_rate_value = round($total_unit_rate_moneyout/$total_unit_rate_moneyin*100,2);
	}
	else
	{
	
	$total_unit_rate_sql = "SELECT ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unit_rate ".
			"FROM	".
			"(	".
			"	SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout	".
			"	FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
			"	WHERE 0 <= t1.betlevel AND t1.betlevel <= 115 AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' AND MODE IN (0,31) ".
			"	GROUP BY slottype, betlevel	".
			") t3";
		$total_unit_rate_value = $db_main2->getvalue($total_unit_rate_sql);
	}
				
			
	$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
				"FROM $table ".
				"WHERE MODE IN (0,31) AND writedate BETWEEN '[WRITEDATE] 00:00:00' AND '[WRITEDATE] 23:59:59'";
	
	$royalty_sql = "SELECT SUM(freeamount) FROM tbl_user_freecoin_daily WHERE category = $os_type AND type IN (5, 17) AND today='[WRITEDATE]'";

	$jackpot_sql = "SELECT IFNULL(SUM(amount), 0) FROM tbl_jackpot_stat_daily WHERE today='[WRITEDATE]' AND devicetype=$os_type ";
	
	$usercount_sql = "SELECT  IFNULL(SUM(totalcount), 0) FROM $online_log_table writedate BETWEEN '[WRITEDATE] 00:00:00' AND '[WRITEDATE] 23:59:59'";
	$usercount_daily_sql = "SELECT IFNULL(SUM(totalcount), 0) FROM $online_log_daily_table today='[WRITEDATE]'";		
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    
    function check_sleeptime()
    {
        //setTimeout("window.location.reload(false)",60000);
    }

    function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
    
    function tab_change(tab)
    {
        var search_form = document.search_form;
        search_form.tab.value = tab;
        search_form.submit();
    }
    
    $(function() {
        $("#startdate").datepicker({ });
    });

    $(function() {
        $("#enddate").datepicker({ });
    });
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
			<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
			<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
			<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
			<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
		</span>
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 게임 활동 추이(<?= $os_txt ?>)- 일별 슬률 통계 (일반 모드)</div>
			<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />   
			<div class="search_box">
				<input type="input" class="search_text" id="startdate" name="startdate" style="width:65px" value="<?= $startdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
				<input type="input" class="search_text" id="enddate" name="enddate" style="width:65px" value="<?= $enddate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
	</div>
		
		<!-- //title_warp -->
		<table class="tbl_list_basic1" style="width:1300px">
			<colgroup>
				<col width="80">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">						
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="70">
				<col width="70">                    
            </colgroup>
               <thead>
                    <tr>
                        <th style="border-top:none;">날짜</th>
						<th style="text-align:right;border-top:none;">money_in(A)</th>
                        <th style="text-align:right;border-top:none;">money_out(B)</th>
                        <th style="text-align:right;border-top:none;">jackpot(C)</th>
                        <th style="text-align:right;border-top:none;">게임 이익</th>
                        <th style="text-align:right;border-top:none;">트리트 금액(D)</th>
                        <th style="text-align:right;border-top:none;">로얄티 금액(E)</th>
                        <th style="border-top:none;">단위 승률</th>
                        <th style="border-top:none;">승률(B/A)</th>
                        <th style="border-top:none;">승률<br>((B+C)/A)</th>                        
                        <th style="border-top:none;">승률<br>((B-D)/A)</th>
                        <th style="border-top:none;">승률<br>((B-D+E)/A)</th>
                        <th style="border-top:none;">게임횟수</th>
                        <th style="border-top:none;">접속자수</th>

                    </tr>
                </thead>
                <tbody>
<?
    $list = $action_list;
    $datetmp = "";
    
    $total_moneyin = 0;
    $total_moneyout = 0;
    $total_jackpot = 0;
    $total_game_profit = 0;
    $total_playcount = 0;
    $total_usercount = 0;
    $total_treat = 0;
    $total_royalty = 0;
    $total_rate1 = 0;
    $total_rate2 = 0;
    $total_rate3 = 0;
    $total_rate4 = 0;
    
    for ($j=0; $j<sizeof($list); $j++)
    {
        $writedate = $list[$j]["writedate"];
        $slottype_data = $list[$j]["slottype"];
        $moneyin = $list[$j]["moneyin"];
        $moneyout = $list[$j]["moneyout"];
        $playcount = $list[$j]["playcount"];       
        
        if(date("Y-m-d", strtotime("-1 days")) < $writedate)
        {
        	$usercount = $db_analysis->getvalue(str_replace("[WRITEDATE]", $writedate, $usercount_sql));
        }
        else
        {
        	$usercount = $db_analysis->getvalue(str_replace("[WRITEDATE]", $writedate, $usercount_daily_sql));
        }
        
        $jackpot = $db_analysis->getvalue(str_replace("[WRITEDATE]", $writedate, $jackpot_sql));
        $treat_data = $db_main2->getvalue(str_replace("[WRITEDATE]", $writedate, $treat_sql));
        $royalty_data = $db_analysis->getvalue(str_replace("[WRITEDATE]", $writedate, $royalty_sql));
        
        $rate1 = round(($moneyout) / $moneyin * 10000) / 100;
        $rate2 = round(($moneyout+$jackpot) / $moneyin * 10000) / 100;
        $rate3 = round(($moneyout - $treat_data) / $moneyin * 10000) / 100;
        $rate4 = round(($moneyout - $treat_data+$royalty_data) / $moneyin * 10000) / 100;
        
        $total_moneyin += $moneyin;
        $total_moneyout += $moneyout;
        $total_jackpot += $jackpot;
        $total_playcount += $playcount;
        $total_usercount += $usercount;
        $total_treat += $treat_data;
        $total_royalty += $royalty_data;

        $total_rate1 = round(($total_moneyout) / $total_moneyin * 10000) / 100;
        $total_rate2 = round(($total_moneyout+$total_playcount) / $total_moneyin * 10000) / 100;
        $total_rate3 = round(($total_moneyout-$total_treat) / $total_moneyin * 10000) / 100;
        $total_rate4 = round(($total_moneyout-$total_treat+$total_royalty) / $total_moneyin * 10000) / 100;

        if ($rate2 <= "80" || $rate2 >= "120")
            $style = "style='color:#fb7878;font-weight:bold;'";
        else
            $style = "";        
        
        if($datetmp != $writedate && datetmp != "" && $j != "0")
        {
?>
        	<tr onmouseover="className='tr_over'" onmouseout="className=''" style="border-top:1px double;">
<?
        }
		else 
		{
?>
			<tr onmouseover="className='tr_over'" onmouseout="className=''" >           
<? 
		}
		
		$datetmp = $writedate;

		$game_profit = $moneyin - ($moneyout + $jackpot);
		$total_game_profit += $game_profit;
?>
				<td class="tdc point" <?= $style ?>><?= $writedate ?></td>
				<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($moneyin) ?></td>
				<td class="tdr point" <?= $style ?>><?= number_format($moneyout) ?></td>
				<td class="tdr point" <?= $style ?>><?= number_format($jackpot) ?></td> 
				<td class="tdr point" <?= $style ?>><?= number_format($game_profit) ?></td>
				<td class="tdr point" <?= $style ?>><?= number_format($treat_data) ?></td>
				<td class="tdr point" <?= $style ?>><?= number_format($royalty_data) ?></td>
				<td class="tdc point" <?= $style ?>><?= $unit_rate_array[$writedate]."%" ?></td>
				<td class="tdc point" <?= $style ?>><?= $rate1."%" ?></td>
				<td class="tdc point" <?= $style ?>><?= $rate2."%" ?></td>
				<td class="tdc point" <?= $style ?>><?= $rate3."%" ?></td>
				<td class="tdc point" <?= $style ?>><?= $rate4."%" ?></td>
				<td class="tdc" <?= $style ?>><?= number_format($playcount) ?></td>
				<td class="tdc" <?= $style ?>><?= number_format($usercount) ?></td>
			</tr>
<?
    }
?>
			<tr style="border-top:1px double;">
				<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
				<td class="tdr point"><?= number_format($total_moneyin) ?></td>
				<td class="tdr point"><?= number_format($total_moneyout) ?></td>
				<td class="tdr point"><?= number_format($total_jackpot) ?></td>
				<td class="tdr point"><?= number_format($total_game_profit) ?></td>
				<td class="tdr point"><?= number_format($total_treat) ?></td>
				<td class="tdr point"><?= number_format($total_royalty) ?></td>
				<td class="tdc point" <?= $style ?>><?= $total_unit_rate_value."%" ?></td>
				<td class="tdc point" <?= $style ?>><?= $total_rate1."%" ?></td>
				<td class="tdc point" <?= $style ?>><?= $total_rate2."%" ?></td>
				<td class="tdc point" <?= $style ?>><?= $total_rate3."%" ?></td>
				<td class="tdc point" <?= $style ?>><?= $total_rate4."%" ?></td>
				<td class="tdc" <?= $style ?>><?= number_format($total_playcount) ?></td>
				<td class="tdc" <?= $style ?>><?= number_format($total_usercount) ?></td>
			</tr>		
		</tbody>
	</table>
	</form>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    $db_main->end();
    $db_main2->end();
    $db_analysis->end();   
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
		