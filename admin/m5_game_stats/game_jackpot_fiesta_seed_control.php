<?
	$top_menu = "game_stats";
	$sub_menu = "game_jackpot_fiesta_seed_control";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

	$slottype = ($_GET["slottype"] == "") ? "all" : $_GET["slottype"];	
	
	$db_main2 = new CDatabase_Main2();
	
	if($slottype == "all")
		$sql = "SELECT slottype, betlevel, fiesta_jackpot_ratio FROM tbl_slot_jackpot_ratio";
	else
		$sql = "SELECT slottype, betlevel, fiesta_jackpot_ratio FROM tbl_slot_jackpot_ratio WHERE slottype = $slottype";
	
	$jackpot_fiesta_ratio = $db_main2->gettotallist($sql);
	
	$sql = "SELECT slottype,slotname FROM tbl_slot_list";
	$slottype_list = $db_main2->gettotallist($sql);
		
	$db_main2->end();	
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	function change_ratio(ratio)
	{
		var ratio = ratio;
	
		var before_ratio = document.getElementById("update_ratio").value;
		var update_ratio = Number(before_ratio) + Number(ratio);
	
		update_ratio = update_ratio.toFixed(1);
		
		document.getElementById("update_ratio").value = update_ratio;
	}

	 function update_change_ratio()
    {
        var input_form = document.input_form;

        if (input_form.slot.value == "")
        {			         
            alert("슬롯을 선택해주세요.");
            input_form.slot.focus();
            return;
        }
        
        if (input_form.update_ratio.value == "0.0")
        {
            alert("조정 승률을 입력해주세요.");
            input_form.update_ratio.focus();
            return;
        }

        var param = {};
		param.slot = input_form.slot.value;
		param.update_ratio = input_form.update_ratio.value;

        WG_ajax_execute("game/change_ratio", param, update_change_ratio_callback, false);      
    }
    
    function update_change_ratio_callback(result, reason)
    {
        var input_form = document.input_form; 
        
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            alert("승률 변경 완료했습니다.");
            select_slot(input_form.slot.value); 
        }
    }
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->	
	<div class="title_wrap">
		<div class="title">잭팟 피에스타 시드</div>
		<div class="search_box">
			<form name="search_form" id="search_form"  method="get" action="game_jackpot_fiesta_seed_control.php">	
					<font style="font:bold 16px 'Malgun Gothic'; color:#5ab2da;">Slot : </font>&nbsp;&nbsp;
					<select name="slottype" id="slottype">				
						<option value="all"  <?= ($slottype=="all") ? "selected" : "" ?>>ALL</option>
		<?
			for ($i=0; $i<sizeof($slottype_list); $i++)
			{
				$slottype = $slottype_list[$i]["slottype"];
				$slotname = $slottype_list[$i]["slotname"];
		?>	
						<option value="<?= $slottype ?>"  <?= ($slottype=="$slot_type") ? "selected" : "" ?>><?= $slotname ?></option>
		<?						
			}
		?>					
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</form>
			<form name="input_form" id="input_form" onsubmit="return false">
				<input type="text" class="view_tbl_text" style="width:50px" name="update_ratio" id="update_ratio" value="0.0" readonly="readonly" />&nbsp;&nbsp;&nbsp;&nbsp;
	    		<input type="button" class="btn_02" value="+" onclick="change_ratio(1)" />&nbsp;<input type="button" class="btn_02" value="-" onclick="change_ratio(-1)" />
	    		<input type="button" class="btn_02" value="변경" onclick="update_change_ratio()" />
    		</form>	
		</div>
	</div>	

	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
                <col width="">
                <col width="">
                <col width="">               
                
            </colgroup>
            <thead>
	            <tr>
	            	<th class="tdc">Slot</th>
	                <th class="tdc">Betlevel</th>
	                <th class="tdc">Fiest Ration</th>	                	                	                
	            </tr>
            </thead>
            <tbody>
<?
        for($i = 0; $i < sizeof($jackpot_fiesta_ratio); $i++)
	    {
	        $slottype = $jackpot_fiesta_ratio[$i]["slottype"];
	        $betelvel = $jackpot_fiesta_ratio[$i]["betlevel"];
	        $fiesta_jackpot_ratio = $jackpot_fiesta_ratio[$i]["fiesta_jackpot_ratio"];
?>
				<tr>
<?
			if($tmp_slottype != $slottype || $i == 0)
			{
?>
        		<td class="tdc point" rowspan="16"><?= $slottype ?></td>
<?
			}
?>
       
					<td class="tdc"><?= $betelvel ?></td>
					<td class="tdc"><?= $fiesta_jackpot_ratio ?></td>
				</tr>
<?
			$tmp_slottype = $slottype;
    }
?>
			</tbody>
		</table>
	</div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>