<?
	$top_menu = "game_stats";
	$sub_menu = "daily_highroller_viplevel_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$viplevel = ($_GET["viplevel"] == "") ? "0" :$_GET["viplevel"];
	$search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    
    if($search_start_createdate == "")
    	$search_start_createdate = "2016-03-10";
    
    if($search_end_createdate == "")
    	$search_end_createdate = $today;
    
    $viplevel_sql = "";
    
    if($viplevel == 1)
    	$viplevel_sql = " AND vip_level = 0 ";
    else if($viplevel == 2)
    	$viplevel_sql = " AND vip_level = 1 ";
    else if($viplevel == 3)
    	$viplevel_sql = " AND vip_level = 2 ";
    else if($viplevel == 4)
    	$viplevel_sql = " AND vip_level = 3 ";
    else if($viplevel == 5)
    	$viplevel_sql = " AND vip_level = 4 ";
    else if($viplevel == 6)
    	$viplevel_sql = " AND vip_level = 5 ";
    else if($viplevel == 7)
    	$viplevel_sql = " AND vip_level = 6 ";
    else if($viplevel == 8)
    	$viplevel_sql = " AND vip_level = 7 ";
    else if($viplevel == 9)
    	$viplevel_sql = " AND vip_level = 8 ";
    else if($viplevel == 10)
    	$viplevel_sql = " AND vip_level = 9 ";
    else if($viplevel == 11)
    	$viplevel_sql = " AND vip_level = 10 ";
	
	$db_other = new CDatabase_Other();
	
	$sql = "SELECT today, ".
			"vip_level AS vip_level, ".
			"COUNT(useridx) AS usercnt, FLOOR(AVG(currentcoin)) AS currentcoin, ".
			"SUM(h_moneyin) AS h_moneyin, SUM(h_moneyout) AS h_moneyout, ROUND(SUM(h_moneyout)/SUM(h_moneyin)*100, 2) AS h_winrate, ".
			"SUM(h_playtime) AS h_playtime, SUM(h_playcount) AS h_playcount, SUM(redeem) AS redeem, ".
			"SUM(purchasecount) AS purchasecount, SUM(purchaseamount) AS purchaseamount, SUM(purchasecoin) AS purchasecoin, ".
			"SUM(famelevel) AS famelevel, SUM(ty_point) AS ty_point, SUM(freecoin) AS freecoin, SUM(jackpot) AS jackpot ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' AND h_moneyin >= 10000000 $viplevel_sql".
			"GROUP BY today, vip_level ".
			"ORDER BY today DESC, vip_level ASC";
	$highroller_viplevel_data = $db_other->gettotallist($sql);	
	
	$sql = "SELECT today, ".
			"vip_level AS vip_level, ".
			"COUNT(useridx) AS usercnt ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' $viplevel_sql".
			"GROUP BY today, vip_level ".
			"ORDER BY today DESC, vip_level ASC";
	$viplevel_data = $db_other->gettotallist($sql);
	
	$viplevel_data_array = array();
	
	for($j = 0; $j < sizeof($viplevel_data); $j++)
	{
		$viplevel_data_today = $viplevel_data[$j]["today"];
		$viplevel_data_vip_level = $viplevel_data[$j]["vip_level"];
		$viplevel_data_usercnt = $viplevel_data[$j]["usercnt"];
		
		$viplevel_data_array[$viplevel_data_today][$viplevel_data_vip_level] = $viplevel_data_usercnt;
	}
		
	$sql = "SELECT ".
			"vip_level AS vip_level, ".
			"COUNT(useridx) AS usercnt, FLOOR(AVG(currentcoin)) AS currentcoin, ".
			"SUM(h_moneyin) AS h_moneyin, SUM(h_moneyout) AS h_moneyout, ROUND(SUM(h_moneyout)/SUM(h_moneyin)*100, 2) AS h_winrate, ".
			"SUM(h_playtime) AS h_playtime, SUM(h_playcount) AS h_playcount, SUM(redeem) AS redeem, ".
			"SUM(purchasecount) AS purchasecount, SUM(purchaseamount) AS purchaseamount, SUM(purchasecoin) AS purchasecoin, ".
			"SUM(famelevel) AS famelevel, SUM(ty_point) AS ty_point, SUM(freecoin) AS freecoin, SUM(jackpot) AS jackpot ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' AND h_moneyin >= 10000000 $viplevel_sql".
			"GROUP BY vip_level ".
			"ORDER BY vip_level ASC";
	$total_highroller_viplevel_data = $db_other->gettotallist($sql);	
		
	$sql = "SELECT today, ".
			"vip_level AS vip_level, ".
			"COUNT(useridx) AS usercnt ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' $viplevel_sql".
			"GROUP BY vip_level ".
			"ORDER BY vip_level ASC";
	$total_viplevel_data = $db_other->gettotallist($sql);
	
	$total_viplevel_data_array = array();
	
	for($j = 0; $j < sizeof($total_viplevel_data); $j++)
	{
		$viplevel_data_vip_level = $total_viplevel_data[$j]["vip_level"];
		$viplevel_data_usercnt = $total_viplevel_data[$j]["usercnt"];
	
		$total_viplevel_data_array[$viplevel_data_vip_level] = $viplevel_data_usercnt;
	}
	
	$sql = "SELECT t1.today AS today, vip_level, SUM(moneyin) AS b_moneyin, SUM(moneyout) AS b_moneyout ".
			"FROM ".
			"(	".
			"	SELECT  today, useridx, vip_level, h_moneyin ".
			"	FROM tbl_user_playstat_daily ".
			"	WHERE '$search_start_createdate' <= today AND today < '$search_end_createdate' AND h_moneyin >= 10000000  $viplevel_sql ".
			") t1 ".
			"JOIN	".
			"(	".
			"	SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, useridx, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout	".
			"	FROM `tbl_user_gamelog`	". 
			"	WHERE MODE IN (6,7,8,9,11,12,13,14,15,16,17,18) AND betlevel >= 10 AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d'), useridx	".
			") t2 ON t1.today = t2.today AND t1.useridx = t2.useridx GROUP BY t1.today, vip_level ORDER BY today DESC";
	$boost_viplevel_data = $db_other->gettotallist($sql);
	
	$boost_viplevel_moneyin_data_array = array();
	$boost_viplevel_moneyout_data_array = array();
	
	$total_boost_viplevel_moneyin = 0;
	$total_boost_viplevel_moneyout = 0;
	
	for($j = 0; $j < sizeof($boost_viplevel_data); $j++)
	{
		$boost_data_today = $boost_viplevel_data[$j]["today"];
		$boost_data_vip_level = $boost_viplevel_data[$j]["vip_level"];
		$boost_data_b_moneyin = $boost_viplevel_data[$j]["b_moneyin"];
		$boost_data_b_moneyout = $boost_viplevel_data[$j]["b_moneyout"];
		
		$boost_viplevel_moneyin_data_array[$boost_data_today][$boost_data_vip_level] = $boost_data_b_moneyin;
		$total_boost_viplevel_moneyin += $boost_data_b_moneyin;		
		$boost_viplevel_moneyout_data_array[$boost_data_today][$boost_data_vip_level] = $boost_data_b_moneyout;
		$total_boost_viplevel_moneyout += $boost_data_b_moneyout;
	}
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="daily_highroller_viplevel_stats.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 하이롤러 VipLevel 통계</div>
		<div class="search_box">
			Viplevel&nbsp;:&nbsp; 
			<select name="viplevel" id="viplevel">
					<option value="" <?= ($viplevel=="0") ? "selected" : "" ?>>전체</option>					
					<option value="1" <?= ($viplevel=="1") ? "selected" : "" ?>>0</option>
					<option value="2" <?= ($viplevel=="2") ? "selected" : "" ?>>1</option>                       
					<option value="3" <?= ($viplevel=="3") ? "selected" : "" ?>>2</option>
					<option value="4" <?= ($viplevel=="4") ? "selected" : "" ?>>3</option>
					<option value="5" <?= ($viplevel=="5") ? "selected" : "" ?>>4</option>
					<option value="6" <?= ($viplevel=="6") ? "selected" : "" ?>>5</option>
					<option value="7" <?= ($viplevel=="7") ? "selected" : "" ?>>6</option>
					<option value="8" <?= ($viplevel=="8") ? "selected" : "" ?>>7</option>
					<option value="9" <?= ($viplevel=="9") ? "selected" : "" ?>>8</option>
					<option value="10" <?= ($viplevel=="10") ? "selected" : "" ?>>9</option>
					<option value="11" <?= ($viplevel=="11") ? "selected" : "" ?>>10</option>
			</select>&nbsp;&nbsp;&nbsp;
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	
	<div id="tab_content_1">
            <table class="tbl_list_basic1" style="width:1400px">
            <colgroup>
                <col width="70">
                <col width="90">
                <col width="50">
                <col width="100">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="90">
                <col width="70">
                <col width="70">
                <col width="70"> 
                <col width="100">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">                 
            </colgroup>
            <thead>
            <tr>
                <th>날짜</th>
                <th class="tdc">Viplevel</th>
                <th class="tdc">유저수</th>
                <th class="tdc">평균보유코인</th>                
                <th class="tdc">moneyin</th>
                <th class="tdc">moneyout</th>
                <th class="tdc">승률</th>
                <th class="tdc">승률<br>(boost 제외)</th>
                <th class="tdc">Redeem</th>
                <th class="tdc">평균 playtime</th>
                <th class="tdc">평균 playcount</th>
                <th class="tdc">결제횟수</th>
                <th class="tdc">결제금액</th>
                <th class="tdc">결제코인</th>
                <th class="tdc">평균 FameLevel</th>
                <th class="tdc">jackpot</th>
                <th class="tdc">전체유저<br>대비</th>
            </tr>
            </thead>
            <tbody>
<?
			for($i=0; $i<sizeof($highroller_viplevel_data); $i++)
			{
				$today = $highroller_viplevel_data[$i]["today"];
				$vip_level = $highroller_viplevel_data[$i]["vip_level"];
				$usercnt = $highroller_viplevel_data[$i]["usercnt"];
				$currentcoin = $highroller_viplevel_data[$i]["currentcoin"];
				$h_moneyin = $highroller_viplevel_data[$i]["h_moneyin"];
				$h_moneyout = $highroller_viplevel_data[$i]["h_moneyout"];
				$h_winrate = $highroller_viplevel_data[$i]["h_winrate"];
				$redeem = $highroller_viplevel_data[$i]["redeem"];
				$h_playtime = $highroller_viplevel_data[$i]["h_playtime"]/$usercnt;
				$h_playcount = $highroller_viplevel_data[$i]["h_playcount"]/$usercnt;
				$purchasecount = $highroller_viplevel_data[$i]["purchasecount"];
				$purchaseamount = $highroller_viplevel_data[$i]["purchaseamount"]/10;
				$purchasecoin = $highroller_viplevel_data[$i]["purchasecoin"];
				$famelevel = $highroller_viplevel_data[$i]["famelevel"]/$usercnt;				
				$jackpot = $highroller_viplevel_data[$i]["jackpot"];
				$viplevel_usercnt = $viplevel_data_array[$today][$vip_level];				
				
				$usercnt_rate = ($usercnt/$viplevel_usercnt)*100;	
				
				$boost_moneyin = ($boost_viplevel_moneyin_data_array[$today][$vip_level] == ""?0:$boost_viplevel_moneyin_data_array[$today][$vip_level]);
				$boost_moneyout = ($boost_viplevel_moneyout_data_array[$today][$vip_level] == ""?0:$boost_viplevel_moneyout_data_array[$today][$vip_level]);
				
				$h_sub_boost_moneyin = $h_moneyin - $boost_moneyin;
				$h_sub_boost_moneyout = $h_moneyout - $boost_moneyout;
				
				$h_sub_boost_rate = ($h_sub_boost_moneyin == 0 ? 0:round(($h_sub_boost_moneyout) / $h_sub_boost_moneyin * 10000) / 100);				

				$std_viplevel_str = "";
				
				if($vip_level == "0")
					$std_viplevel_str = "0";
				else if($vip_level == "1")
					$std_viplevel_str = "1";
				else if($vip_level == "2")
					$std_viplevel_str = "2";
				else if($vip_level == "3")
					$std_viplevel_str = "3";
				else if($vip_level == "4")
					$std_viplevel_str = "4";
				else if($vip_level == "5")
					$std_viplevel_str = "5";
				else if($vip_level == "6")
					$std_viplevel_str = "6";
				else if($vip_level == "7")
					$std_viplevel_str = "7";
				else if($vip_level == "8")
					$std_viplevel_str = "8";
				else if($vip_level == "9")
					$std_viplevel_str = "9";
				else if($vip_level == "10")
					$std_viplevel_str = "10";

?>
				<tr>
<?
				if($i == 0 || $today != $highroller_viplevel_data[$i-1]["today"])
				{
					if($std_viplevel_str != "")
					{
						$sql = "SELECT today, ".
								"vip_level AS vip_level ".								
								"FROM `tbl_user_playstat_daily` ".
								"WHERE '$today' <= today AND today <= '$today' AND h_moneyin >= 10000000 $viplevel_sql".
								"GROUP BY vip_level ".
								"ORDER BY vip_level ASC;";
						$highroller_viplevel_rowdata = $db_other->gettotallist($sql);
?>				
					<td class="tdc point_title" rowspan="<?=sizeof($highroller_viplevel_rowdata)?>"><?= $today ?></td>
					
<?
					}
					else
					{
						
?>
						<td class="tdc point_title" rowspan="1"><?= $today ?></td>
<?					
					}
				}
?>	
	
					<td class="tdc"><?= $std_viplevel_str ?></td>
					<td class="tdc"><?= number_format($usercnt) ?></td>
					<td class="tdc"><?= number_format($currentcoin) ?></td>
					<td class="tdc"><?= number_format($h_moneyin) ?></td>
					<td class="tdc"><?= number_format($h_moneyout) ?></td>
					<td class="tdc"><?= $h_winrate ?>%</td>
					<td class="tdc"><?= $h_sub_boost_rate ?>%</td>
					<td class="tdc"><?= number_format($redeem) ?></td>					
					<td class="tdc"><?= number_format($h_playtime) ?></td>
					<td class="tdc"><?= number_format($h_playcount) ?></td>
					<td class="tdc"><?= number_format($purchasecount) ?></td>
					<td class="tdc">$<?= number_format($purchaseamount) ?></td>
					<td class="tdc"><?= number_format($purchasecoin) ?></td>
					<td class="tdc"><?= number_format($famelevel) ?></td>
					<td class="tdc"><?= number_format($jackpot) ?></td>
					<td class="tdc"><?= number_format($usercnt_rate,2) ?>%</td>
				</tr>
<?
			}
			
			for($i=0; $i<sizeof($total_highroller_viplevel_data); $i++)
			{
				$today = $total_highroller_viplevel_data[$i]["today"];
				$vip_level = $total_highroller_viplevel_data[$i]["vip_level"];
				$usercnt = $total_highroller_viplevel_data[$i]["usercnt"];
				$currentcoin = $total_highroller_viplevel_data[$i]["currentcoin"];
				$h_moneyin = $total_highroller_viplevel_data[$i]["h_moneyin"];
				$h_moneyout = $total_highroller_viplevel_data[$i]["h_moneyout"];
				$h_winrate = $total_highroller_viplevel_data[$i]["h_winrate"];
				$redeem = $total_highroller_viplevel_data[$i]["redeem"];
				$h_playtime = $total_highroller_viplevel_data[$i]["h_playtime"]/$usercnt;
				$h_playcount = $total_highroller_viplevel_data[$i]["h_playcount"]/$usercnt;
				$purchasecount = $total_highroller_viplevel_data[$i]["purchasecount"];
				$purchaseamount = $total_highroller_viplevel_data[$i]["purchaseamount"]/10;
				$purchasecoin = $total_highroller_viplevel_data[$i]["purchasecoin"];
				$famelevel = $total_highroller_viplevel_data[$i]["famelevel"]/$usercnt;				
				$jackpot = $total_highroller_viplevel_data[$i]["jackpot"];
				$viplevel_usercnt = $total_viplevel_data_array[$vip_level];
				
				$usercnt_rate = ($usercnt/$viplevel_usercnt)*100;
				
				$h_sub_boost_moneyin = $h_moneyin - $total_boost_viplevel_moneyin;
				$h_sub_boost_moneyout = $h_moneyout - $total_boost_viplevel_moneyout;
				
				$h_sub_boost_rate = ($h_sub_boost_moneyin == 0 ? 0:round(($h_sub_boost_moneyout) / $h_sub_boost_moneyin * 10000) / 100);
				
				$std_viplevel_str = "";
				
				if($vip_level == "0")
					$std_viplevel_str = "0";
				else if($vip_level == "1")
					$std_viplevel_str = "1";
				else if($vip_level == "2")
					$std_viplevel_str = "2";
				else if($vip_level == "3")
					$std_viplevel_str = "3";
				else if($vip_level == "4")
					$std_viplevel_str = "4";
				else if($vip_level == "5")
					$std_viplevel_str = "5";
				else if($vip_level == "6")
					$std_viplevel_str = "6";
				else if($vip_level == "7")
					$std_viplevel_str = "7";
				else if($vip_level == "8")
					$std_viplevel_str = "8";
				else if($vip_level == "9")
					$std_viplevel_str = "9";
				else if($vip_level == "10")
					$std_viplevel_str = "10";
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($i == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($total_highroller_viplevel_data)?>" valign="center">Total</td>
<?
				}
?>
					<td class="tdc"><?= $std_viplevel_str ?></td>
					<td class="tdc"><?= number_format($usercnt) ?></td>
					<td class="tdc"><?= number_format($currentcoin) ?></td>
					<td class="tdc"><?= number_format($h_moneyin) ?></td>
					<td class="tdc"><?= number_format($h_moneyout) ?></td>
					<td class="tdc"><?= $h_winrate ?>%</td>
					<td class="tdc"><?= $h_sub_boost_rate ?>%</td>
					<td class="tdc"><?= number_format($redeem) ?></td>
					<td class="tdc"><?= number_format($h_playtime) ?></td>
					<td class="tdc"><?= number_format($h_playcount) ?></td>
					<td class="tdc"><?= number_format($purchasecount) ?></td>
					<td class="tdc">$<?= number_format($purchaseamount) ?></td>
					<td class="tdc"><?= number_format($purchasecoin) ?></td>
					<td class="tdc"><?= number_format($famelevel) ?></td>
					<td class="tdc"><?= number_format($jackpot) ?></td>
					<td class="tdc"><?= number_format($usercnt_rate,2) ?>%</td>
				</tr>

<?
			}
?>
			</tbody>
            </table>
     	</div>        
     </form>	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_other->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>