<?
	$top_menu = "game_stats";
	$sub_menu = "guest_connect_facebook_stat";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
	
	$os_term = ($_GET["os_term"] == "") ? "ALL" : $_GET["os_term"];
	
	check_xss($startdate);
	check_xss($enddate);
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today,15,"d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db_otherdb = new CDatabase_Other();
	
	if($os_term != "ALL")
		$sql = "SELECT * FROM tbl_facebook_connect_stat_daily WHERE today BETWEEN '$startdate' AND '$enddate' AND platform = $os_term ORDER BY today DESC";
	else
		$sql = "SELECT today, SUM(usercount) AS usercount, SUM(elapsed_days) AS elapsed_days FROM tbl_facebook_connect_stat_daily WHERE today BETWEEN '$startdate' AND '$enddate' $tail  GROUP BY today  ORDER BY today DESC";
	
	$connect_stat_list = $db_otherdb->gettotallist($sql);
	
	$db_otherdb->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

	function tab_change(tab)
	{
		var search_form = document.search_form;
		search_form.tab.value = tab;
		search_form.submit();
	}

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}

	function change_os_term(term)
	{
		var search_form = document.search_form;
		
		var all = document.getElementById("term_all");
		var ios = document.getElementById("term_ios");
		var android = document.getElementById("term_android");
		var amazon = document.getElementById("term_amazon");
		
		document.search_form.os_term.value = term;

		if (term == "ALL")
		{
			all.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "1")
		{
			all.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "2")
		{
			all.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (term == "3")
		{
			all.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}

		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">

	<!-- title_warp -->
	<div class="title_wrap">
		<form name="search_form" id="search_form"  method="get" action="guest_connect_facebook_stat.php">		
			<div class="title"><?= $top_menu_txt ?> &gt; Guest Facebook 연동 통계</div>
			<div class="search_box"> 
				<input type="button" class="<?= ($os_term == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" value="ALL" id="term_all" onclick="change_os_term('ALL')"    />
				<input type="button" class="<?= ($os_term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_os_term('1')" />
				<input type="button" class="<?= ($os_term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_os_term('2')"    />
				<input type="button" class="<?= ($os_term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_os_term('3')"    />
				&nbsp;&nbsp;
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
	            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
	            <input type=hidden name="os_term" value="<?= $os_term ?>">
	             <input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
    </div>
	<!-- //title_warp -->
	
    <div class="search_result">
    	<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다
    </div>

	<div id="contents_wrap">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">사용자 수</th>
					<th class="tdc">평균 경과일</th>
				</tr>
			</thead>
			<tbody>
<?
			$total_usercount = 0;
			$total_elapsed_days = 0;
			
			for($i=0; $i<sizeof($connect_stat_list); $i++)
			{
				$today = $connect_stat_list[$i]["today"];
				$usercount = $connect_stat_list[$i]["usercount"];
				$elapsed_days = $connect_stat_list[$i]["elapsed_days"];
				$avg_elapsed_days = round($elapsed_days/$usercount);
				
				$total_usercount += $usercount;
				$total_elapsed_days += $elapsed_days;
				
?>
				<tr>
					<td class="tdc point_title"><?= $today ?></td>
					<td class="tdc"><?= number_format($usercount) ?></td>
					<td class="tdc"><?= number_format($avg_elapsed_days, 1) ?></td>
				</tr>
<?
			}
?>
				<tr>
					<td class="tdc point_title">Total</td>
					<td class="tdc point"><?= number_format($total_usercount) ?></td>
					<td class="tdc point"><?= number_format(round($total_elapsed_days/$total_usercount), 1) ?></td>
					<td class="tdc point"></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
    	<!--  //CONTENTS WRAP -->    	
	<div class="clear"></div>

<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>