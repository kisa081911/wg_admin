<?
    $top_menu = "game_stats";
    $sub_menu = "game_jackpot_detail_save_daily";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $pagename = "game_jackpot_detail_save_daily.php";
    
    $startdate = $_GET["startdate"];
    $enddate = $_GET["enddate"];
       
    
    if($startdate == "")
    	$startdate = date("Y-m-d", strtotime("-3 day"));
    
    if($enddate == "")
    	$enddate = date("Y-m-d", strtotime("-1 day"));
    
    $db_analysis = new CDatabase_Analysis();
    
	$sql = "SELECT today, SUM(normal_amount) AS normal_amount, SUM(highroller_amount) AS highroller_amount ".
			"FROM	".
			"(	".
			"	SELECT today, ROUND(SUM(amount)/100000000, 0) AS normal_amount, 0 AS highroller_amount FROM tbl_jackpot_save_stat_daily WHERE bettype = 0 AND today BETWEEN '$startdate' AND '$enddate' GROUP BY today ".
			"	UNION ALL	".
			"	SELECT today, 0 AS normal_amount, ROUND(SUM(amount)/100000000, 0) AS highroller_amount FROM tbl_jackpot_save_stat_daily WHERE bettype = 1 AND today BETWEEN '$startdate' AND '$enddate' GROUP BY today ".
			") t1 GROUP BY today";
	$jackpot_data = $db_analysis->gettotallist($sql);
	
	$sql = "SELECT today, SUM(normal_amount) AS normal_amount, SUM(highroller_amount) AS highroller_amount ".
			"FROM	".
			"(	".
			"	SELECT today, ROUND(SUM(amount)/100000000, 0) AS normal_amount, 0 AS highroller_amount FROM tbl_jackpot_save_stat_daily WHERE bettype = 0 AND today BETWEEN '$startdate' AND '$enddate' GROUP BY today ".
			"	UNION ALL	".
			"	SELECT today, 0 AS normal_amount, ROUND(SUM(amount)/100000000, 0) AS highroller_amount FROM tbl_jackpot_save_stat_daily WHERE bettype = 1 AND today BETWEEN '$startdate' AND '$enddate' GROUP BY today ".
			") t1 GROUP BY today order by today DESC";
	$jackpot_data2 = $db_analysis->gettotallist($sql);
	
	$db_analysis->end();
	
	$date_list = array();
	$normal_amount_list = array();
	$highroller_amount_list = array();

	$list_pointer = sizeof($jackpot_data);
	$date_pointer = $enddate;
	
	for($i=0; $i<sizeof($jackpot_data); $i++)
	{
		if ($list_pointer > 0)
			$today = $jackpot_data[$list_pointer-1]["today"];
		
		if(get_diff_date($date_pointer, $today, "d") == 0)
		{
			$jackpot = $jackpot_data[$list_pointer-1];
			
			$normal_amount_list[$i] = $jackpot["normal_amount"];
			$highroller_amount_list[$i] = $jackpot["highroller_amount"];
			
			$list_pointer--;
		}
		else
		{
			$normal_amount_list[$i] = 0;
			$highroller_amount_list[$i] = 0;
		}
		
		$date_list[$i] = $date_pointer;
		
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}    
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
google.load("visualization", "1", {packages:["corechart"]});

function drawChart() 
{
    var data1 = new google.visualization.DataTable();
    
    data1.addColumn('string', '날짜');
    data1.addColumn('number', '일반 금액');
    data1.addColumn('number', '하이롤러 금액');
    data1.addRows([
<?
	for ($i=sizeof($date_list); $i>0; $i--)
	{
		$date = $date_list[$i-1];
		$normal_amount = $normal_amount_list[$i-1];
		$highroller_amount = $highroller_amount_list[$i-1];
		
				
		echo("['".$date."'");		
        
        if ($normal_amount != "")
            echo(",{v:".$normal_amount.",f:'".make_price_format($normal_amount)."'}");
        else
            echo(",0");
        
        if ($highroller_amount != "")
        	echo(",{v:".$highroller_amount.",f:'".make_price_format($highroller_amount)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
			echo(",");
	}
?>     
    ]);
        
    var options = {
            title:'',            
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize:12,
            chartArea:{left:80,top:25,width:1000,height:100}
	};

    var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
    chart.draw(data1, options);
}

    google.setOnLoadCallback(drawChart);
    
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }

    $(function() {
        $("#startdate").datepicker({ });
    });

    $(function() {
        $("#enddate").datepicker({ });
    });
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
                <!-- title_warp -->
                <div class="title_wrap">
                    <div class="title"><?= $top_menu_txt ?> &gt; 누적 잭팟금액 통계</div>
                    <div class="search_box">                    	
                   		<input type="input" class="search_text" id="startdate" name="startdate" style="width:75px" value="<?= $startdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                        
                        <input type="input" class="search_text" id="enddate" name="enddate" style="width:75px" value="<?= $enddate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
                        <input type="button" class="btn_search" value="검색" onclick="search()" />
                    </div>
                </div>
                <!-- //title_warp -->
                <div class="search_result">
					<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다
				</div>
  	
                <div style="font-weight:bold;paddig-top:10px">[누적 잭팟 금액(억)]</div>
                <div id="chart_div" style="height:200px; min-width:500px;"></div>
                <br/><br/>
                <div id="tab_content_1">
					<table class="tbl_list_basic1">
						<colgroup>
	                		<col width="">
	                		<col width="">
	                		<col width="">	                
            			</colgroup>
            		<thead>
	            		<tr>
	            			<th class="tdc">Today</th>
	                		<th class="tdc">일반 누적 잭팟 금액</th>
	                		<th class="tdc">하이롤러 누적 잭팟 금액</th>	                	                	                	                
	            		</tr>
            		</thead>
            		<tbody>
<?			
			for($i=0; $i<sizeof($jackpot_data2); $i++)
			{
				$today =  $jackpot_data2[$i]["today"];
				$normal_amount =  $jackpot_data2[$i]["normal_amount"];
				$highroller_amount =  $jackpot_data2[$i]["highroller_amount"];
?>
				<tr>					
                   	<td class="tdc point"><?= $today ?></td>
                   	<td class="tdc point"><?= number_format($normal_amount) ?></td>
                   	<td class="tdc point"><?= number_format($highroller_amount) ?></td>
				</tr>
<?							
				
			}
?>
					</tbody>
				</table>
            </form>
            
        </div>
        <!--  //CONTENTS WRAP -->        
        <div class="clear"></div>

<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
