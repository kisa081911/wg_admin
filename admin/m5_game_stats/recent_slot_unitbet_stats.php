<?
	$top_menu = "game_stats";
	$sub_menu = "recent_slot_unitbet_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$platform = ($_GET["platform"] == "") ? 0 : $_GET["platform"];
	$search_time = ($_GET["search_time"] == "") ? 168 : $_GET["search_time"];
	$min_winrate = ($_GET["min_winrate"] == "") ? "96" : $_GET["min_winrate"];
	$max_winrate = ($_GET["max_winrate"] == "") ? "98" : $_GET["max_winrate"];
	
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	
	$table_name = "tbl_game_cash_stats_daily";
	
	if($platform == 1)
		$table_name = "tbl_game_cash_stats_ios_daily";
	else if($platform == 2)
		$table_name = "tbl_game_cash_stats_android_daily";
	else if($platform == 3)
		$table_name = "tbl_game_cash_stats_amazon_daily";
	
	$sql = " SELECT slottype, ROUND(SUM(sum_moneyout)/SUM(sum_moneyin) * 100, 2) AS win_rate, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unit_rate, playcount ".
			" FROM (	".		
			"	SELECT t1.slottype, SUM(moneyin) AS sum_moneyin, SUM(moneyout) AS sum_moneyout, SUM(unit_moneyin) AS tt_moneyin, SUM(unit_moneyout) AS tt_moneyout, SUM(playcount) AS playcount	". 	
			"	FROM `$table_name` t1 ". 	
			"	WHERE mode IN (0) AND DATE_SUB(NOW(), INTERVAL $search_time HOUR) <= writedate AND writedate <= NOW()	". 	
			"	GROUP BY slottype".
			" ) t3 GROUP BY slottype	". 
			" HAVING unit_rate < $min_winrate OR unit_rate > $max_winrate";
	$slot_unit_rate_info = $db_main2->gettotallist($sql);
	
	$sql = "SELECT * FROM tbl_slot_list";
	$slottype_list = $db_main2->gettotallist($sql);
	
	
?>
<link type="text/css" href="/js/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="/js/ui/ui.core.js"></script>
<script type="text/javascript" src="/js/ui/ui.datepicker.js"></script> 
<script type="text/javascript">
	function change_platform(platform)
	{
		var search_form = document.search_form;
		
		var all = document.getElementById("platform_web");
		var ios = document.getElementById("platform_ios");
		var and = document.getElementById("platform_and");
		var ama = document.getElementById("platform_ama");
		
		document.search_form.platform.value = platform;

		if (platform == 0)
		{
			all.className="btn_schedule_select";
			ios.className="btn_schedule";
			and.className="btn_schedule";
			ama.className="btn_schedule";
		}
		else if (platform == 1)
		{
			all.className="btn_schedule";
			ios.className="btn_schedule_select";
			and.className="btn_schedule";
			ama.className="btn_schedule";			
		}
		else if (platform == 2)
		{
			all.className="btn_schedule";
			ios.className="btn_schedule";
			and.className="btn_schedule_select";
			ama.className="btn_schedule";	
		}
		else if (platform == 3)
		{
			all.className="btn_schedule";
			ios.className="btn_schedule";
			and.className="btn_schedule";
			ama.className="btn_schedule";	
		}

		search_form.submit();
	}
	
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">

	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="recent_slot_unitbet_stats.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 최근 단위 승률 통계</div>
		<div class="search_box">
			<span class="search_lbl">최소 승률</span>&nbsp;&nbsp;
			<input type="text" class="search_text" id="min_winrate" name="min_winrate" style="width:30px" value="<?= $min_winrate ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
			
			<span class="search_lbl">최대 승률</span>&nbsp;&nbsp;
			<input type="text" class="search_text" id="max_winrate" name="max_winrate" style="width:30px" value="<?= $max_winrate ?>" />&nbsp;&nbsp;
			
			<input type="button" class="btn_search" onclick="search()" value="검색" />&nbsp;&nbsp;
			
			시간 설정 : 
		    <select name="search_time" id="search_time" onchange="search()">
		    	<option value="24" <?= ($search_time=="24") ? "selected" : "" ?>>24시간</option>
		    	<option value="48" <?= ($search_time=="48") ? "selected" : "" ?>>48시간</option>
		    	<option value="72" <?= ($search_time=="72") ? "selected" : "" ?>>72시간</option>
		    	<option value="96" <?= ($search_time=="96") ? "selected" : "" ?>>96시간</option>
		    	<option value="168" <?= ($search_time=="168") ? "selected" : "" ?>>1주일</option>
		    	<option value="672" <?= ($search_time=="672") ? "selected" : "" ?>>4주일</option>
		    	<option value="2190" <?= ($search_time=="2190") ? "selected" : "" ?>>3개월</option>
		    </select>&nbsp;&nbsp;&nbsp;&nbsp;
		    
	    
			<input type="hidden" name="platform" id="platform" value="<?= $platform ?>" />
			<input type="button" class="<?= ($platform == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="platform_web" onclick="change_platform(0)" />
			<input type="button" class="<?= ($platform == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS"  id="platform_ios"  onclick="change_platform(1)" />
			<input type="button" class="<?= ($platform == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="platform_and" onclick="change_platform(2)"/>
			<input type="button" class="<?= ($platform == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="platform_ama" onclick="change_platform(3)"/>				
		</div>
    </div>
    
    <span style="color:red; font-size:11px;">* 최근  <?= $search_time ?>시간 단위 승률입니다.</span>
    
	<!-- //title_warp -->	    	

	<div id="tab_content_1">
		<table class="tbl_list_basic1">
		<colgroup>
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="200px">
			<col width="200px">
		</colgroup>
		<thead>
			<th>Slot</th>
			<th>일반승률</th>
			<th>단위승률</th>
			<th>플레이 카운트</th>
			<th>승률 조정 현황(ratio)</th>
			<th>승률 조정 현황(ratio3)</th>
		</thead>
		<tbody>			
<?
		for ($i=0; $i<sizeof($slot_unit_rate_info); $i++)
		{
			$slottype = $slot_unit_rate_info[$i]["slottype"];
			$win_rate = $slot_unit_rate_info[$i]["win_rate"];
			$playcount = $slot_unit_rate_info[$i]["playcount"];
			$unit_rate = $slot_unit_rate_info[$i]["unit_rate"];
			
			for($j=0; $j<sizeof($slottype_list); $j++)
			{
				if($slottype_list[$j]["slottype"] == $slottype)
				{
					$slot_name = $slottype_list[$j]["slotname"];
					break;
				}
				else
				{
					$slot_name = "Unkown";
				}
			}
			
			if($platform == 0)
				$category = "web";
			else if($platform == 1)
				$category = "ios";
			else if($platform == 2)
				$category = "android";
			else if($platform == 3)
				$category = "amazon";
			
			$sql = "SELECT writedate, (current_ratio - before_ratio) AS change_ratio FROM `tbl_slotratio_change_history` WHERE category = '$category' AND slottype = $slottype ORDER BY writedate DESC LIMIT 3";
			$change_history_list = $db_analysis->gettotallist($sql);
			
			$sql = "SELECT writedate, (current_ratio - before_ratio) AS change_ratio FROM `tbl_slotratio3_change_history` WHERE category = '$category' AND slottype = $slottype ORDER BY writedate DESC LIMIT 3";
			$change_history3_list = $db_analysis->gettotallist($sql);
			
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point"><?= $slot_name ?></td>
				<td class="tdc"><?= number_format($win_rate, 2) ?>%</td>
				<td class="tdc"><?= number_format($unit_rate, 2) ?>%</td>
				<td class="tdc"><?= $playcount ?></td>
				<td class="tdc">
<?
			if($change_history_list != "")
			{
				for($k=0; $k<sizeof($change_history_list); $k++)
				{
					$writedate = $change_history_list[$k]["writedate"];
					$change_ratio = $change_history_list[$k]["change_ratio"];
					
					if($change_ratio > 0)
						echo $writedate."&nbsp;&nbsp;&nbsp;<span style='color:red'>"."▲ ".$change_ratio."</span><br/>";
					else
						echo $writedate."&nbsp;&nbsp;&nbsp;<span style='color:blue'>"."▼".$change_ratio."</span><br/>";
				}
			}
?>
				</td>
				<td class="tdc">
<?
			if($change_history3_list != "")
			{
				for($k=0; $k<sizeof($change_history3_list); $k++)
				{
					$writedate = $change_history3_list[$k]["writedate"];
					$change_ratio = $change_history3_list[$k]["change_ratio"];
					
					if($change_ratio > 0)
						echo $writedate."&nbsp;&nbsp;&nbsp;<span style='color:red'>"."▲ ".$change_ratio."</span><br/>";
					else
						echo $writedate."&nbsp;&nbsp;&nbsp;<span style='color:blue'>"."▼".$change_ratio."</span><br/>";
				}
			}
?>
				</td>
			</tr>
				
<?
		}
?>
			
		</tbody>
	</table>
	</div>

	</form>	
</div>
    	<!--  //CONTENTS WRAP -->
    	
	<div class="clear"></div>

<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
	
	$db_main2->end();
	$db_analysis->end();
?>