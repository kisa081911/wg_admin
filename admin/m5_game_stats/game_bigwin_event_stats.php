<?
    $top_menu = "game_stats";
    $sub_menu = "game_bigwin_event_stats";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    
    $useridx = $_GET["useridx"];    
    $start_date = $_GET["startdate"];
    $end_date = $_GET["enddate"];
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    
     $sql = "SELECT MAX(idx) ".
			"FROM	".
			"(	".
			"	SELECT idx, startdate, enddate FROM `tbl_bigwin_event`	".
			"	UNION ALL	".
			"	SELECT idx, startdate, enddate FROM `tbl_bigwin_event_v2`	".
			") t1";
    $max_eventidx = $db_main2->getvalue($sql);
    
    $eventidx = ($_GET["eventidx"] == "") ? $max_eventidx : $_GET["eventidx"];    
    
    $pagename = "game_bigwin_event_stats.php";
    $listcount = 10;
    $pagefield = "useridx=$useridx&eventidx=$eventidx";    
    
    $tail = " WHERE eventidx=$eventidx ";
    $order_by = " ORDER BY writedate DESC ";
    
    if ($useridx != "")
    	$tail .= " AND useridx=$useridx";
    
    //if ($search_start_date != "")
    //	$tail .= " AND writedate >= '$search_start_date 00:00:00'";
    
    //if ($search_end_date != "")
    //	$tail .= " AND writedate <= '$search_end_date 23:59:59'";
    
    $sql = "SELECT idx, startdate, enddate ".
			"FROM	".
			"(	".
			"	SELECT idx, startdate, enddate FROM `tbl_bigwin_event`	".
			"	UNION ALL	".
			"	SELECT idx, startdate, enddate FROM `tbl_bigwin_event_v2`	".
			") t1 ORDER BY idx ASC";
    $bigwin_event_list = $db_main2->gettotallist($sql);
    
   	$totalcount = $db_main2->getvalue("SELECT COUNT(*) FROM tbl_bigwin_event_win_user $tail");
    
   	if($eventidx < 12)
   	{
    	$sql = "SELECT eventidx, writedate, useridx, slot_num, ROUND(IF(amount >= 20000000000, amount-10000000000, amount/2), 0) AS win_amount, ".
				"ROUND(IF(amount >= 20000000000, 10000000000, amount/2), 0) AS bonus_amount, multiple, mode	".
				"FROM tbl_bigwin_event_win_user $tail $order_by  LIMIT ".(($page-1) * $listcount).", ".$listcount;
    	$bigwin_event_user_list = $db_main2->gettotallist($sql);
   	}
   	else 
   	{
   		$sql = "SELECT eventidx, writedate, useridx, slot_num, ROUND(amount/2) AS win_amount, ".
   				"ROUND(amount/2) AS bonus_amount, multiple, mode	".
   				"FROM tbl_bigwin_event_win_user $tail $order_by  LIMIT ".(($page-1) * $listcount).", ".$listcount;
   		$bigwin_event_user_list = $db_main2->gettotallist($sql);
   	}
    
    if($eventidx < 12)
    {
    	$sql = "SELECT amount_2 FROM `tbl_bigwin_event` WHERE idx = $eventidx";
   		$bigwin_remain_amount = $db_main2->getvalue($sql);
    }
    
    //Slot 정보
    $sql = "SELECT slottype, slotname FROM tbl_slot_list";
    $slottype_list = $db_main2->gettotallist($sql);
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
    	$page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">	
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();	
	}
    
    function check_sleeptime()
    {
        setTimeout("window.location.reload(false)",60000);
    }
</script>
		<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; BigWin Event User (<?= make_price_format($totalcount) ?>건)&nbsp;&nbsp;&nbsp;<?if($eventidx < 12){?>(Bigwin Event 남은 금액 : <?=number_format($bigwin_remain_amount)?>)<?}?></div>
	        </div>
	    	<!-- //title_warp -->
	    	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?=$pagename?>">
	    	<div class="detail_search_wrap">
	    		<span class="search_lbl">useridx&nbsp;&nbsp;&nbsp;</span>
	    		<input type="text" class="search_text" value="<?= $useridx ?>" id="useridx" name="useridx"  onkeypress="search_press(event)" style="width:130px;" />                 
	            <span class="search_lbl ml20">Event 종류</span>
                <select name="eventidx" id="eventidx">
<?
					for($i = 0; $i < sizeof($bigwin_event_list); $i++)
					{
						$bingwin_eventidx = $bigwin_event_list[$i]["idx"];
						$event_startdate = $bigwin_event_list[$i]["startdate"];
						$event_enddate = $bigwin_event_list[$i]["enddate"];
					
?>

                    <option value="<?=$bingwin_eventidx?>" <?= ($eventidx == "$bingwin_eventidx") ? "selected" : "" ?>><?=$bingwin_eventidx?>회차(<?=$event_startdate?>~<?=$event_enddate?>)</option>                    
<?
					}
?>
                </select>		
	            <div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
	    	</div>
	    	
	    	</form>
	    	<table class="tbl_list_basic1">
		    	<colgroup>
					<col width="">
					<col width="">
					<col width="">
					<col width="">
					<col width="">
					<col width="">
					<col width="">
					<col width="">					
				</colgroup>
				<thead>
					<tr>
		    			<th>Eventidx</th>
		    			<th>username</th>
		    			<th>SLOT</th>
		    			<th>윈금액</th>
		                <th>보상금액</th>
		    			<th>윈배수</th>
		    			<th>승률부양 대상자</th>
		    			<th>일시</th>		    			
		    		</tr>
				</thead>
				<tbody>
<?
				for($i=0; $i<sizeof($bigwin_event_user_list); $i++)
				{
					$eventidx = $bigwin_event_user_list[$i]["eventidx"];
					$useridx = $bigwin_event_user_list[$i]["useridx"];
					$slottype = $bigwin_event_user_list[$i]["slot_num"];
					$writedate = $bigwin_event_user_list[$i]["writedate"];
					$win_amount = $bigwin_event_user_list[$i]["win_amount"];
					$bonus_amount = $bigwin_event_user_list[$i]["bonus_amount"];
					$multiple = $bigwin_event_user_list[$i]["multiple"];
					$mode = $bigwin_event_user_list[$i]["mode"];
					
					$sql = "SELECT nickname, userid FROM tbl_user WHERE useridx = $useridx";
					$userinfo = $db_main->getarray($sql);
					
					$username = $userinfo["nickname"];
					$userid = $userinfo["userid"];
					
					$photourl = get_fb_pictureURL($userid,$client_accesstoken);
					
					if($mode > 0)
						$check_boost = "Y";
    				else					
    					$check_boost = "N";
    				
    				for($j=0; $j<sizeof($slottype_list); $j++)
    				{
	    				if($slottype_list[$j]["slottype"] == $slottype)
	    				{
	    					$slot_name = $slottype_list[$j]["slotname"];
	    					break;
	    				}
						else
	    				{
	    					$slot_name = "Unkown";
	    				}
    				}
?>
					<tr  class="" onmouseover="" onmouseout="" onclick="">	
						<td class="tdc"><?= $eventidx ?></td>
						<td class="point_title"><div style="float:left; cursor: pointer;" onclick="view_user_dtl(<?= $useridx ?>,'')"><img src="<?= $photourl?>"/><?=$username?></div></td>
						<td class="tdc point"><?= $slot_name ?></td>
						<td class="tdc point"><?= number_format($win_amount) ?></td>
						<td class="tdc point"><?= number_format($bonus_amount)  ?></td>
						<td class="tdr point"><?= number_format($multiple) ?></td>
						<td class="tdc point"><?= $check_boost ?></td>
						<td class="tdc point"><?= $writedate ?></td>
					</tr>
<?
				}
?>
				</tbody>
        	</table>
 <? 
	include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
	</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main->end();
	$db_main2->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>

				