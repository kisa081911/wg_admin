<?
	$top_menu = "game_stats";
	$sub_menu = "daily_treat_ticket_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	
	if($search_start_createdate == "")
		$search_start_createdate = date("Y-m-d", strtotime("-3 day"));
	
	if($search_end_createdate == "")
		$search_end_createdate = $today;
	
	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT today, treat_type, treat_count, (SELECT SUM(treat_count) FROM tbl_treat_ticket_stat WHERE today = t1.today) AS total_click ".
			"FROM tbl_treat_ticket_platinum_stat t1 WHERE today BETWEEN '$search_start_createdate' AND '$search_end_createdate' ORDER BY today DESC";
	$treat_ticket_click_data = $db_analysis->gettotallist($sql);
	
	$sql = "SELECT treat_type, SUM(treat_count) AS total_treat_type_count ".
			"FROM tbl_treat_ticket_platinum_stat t1 WHERE today BETWEEN '$search_start_createdate' AND '$search_end_createdate' GROUP BY treat_type ORDER BY treat_type DESC";
	$total_treat_ticket_click_data = $db_analysis->gettotallist($sql);	
	
	$sql = "SELECT today, daily_platinum_loyalty_amount, daily_platinum_treat_amount, loyalty_keep_amount, treat_keep_amount, exchange_treat_count, exchange_treat_amount ".
			"FROM tbl_treat_ticket_use_stat WHERE today BETWEEN '$search_start_createdate' AND '$search_end_createdate' ORDER BY today DESC";
	$treat_ticket_amount_data = $db_analysis->gettotallist($sql);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="daily_treat_ticket_stats.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Treat Ticket Highroller 통계</div>
		<div class="search_box">
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">1. Treat Ticket Click 통계 </span>
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">
				<col width="">							
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">Tick Type</th>
					<th class="tdc">Click 수</th>
					<th class="tdc">Click 비율</th>					
				</tr>
			</thead>
			<tbody>
<?
			$total_treat_ticket_count = 0;
			$day_treat_count = 0;
			$day_total_treat_count = 0;
			
			for($i=0; $i<sizeof($treat_ticket_click_data); $i++)
			{
				$today = $treat_ticket_click_data[$i]["today"];
				$treat_type = $treat_ticket_click_data[$i]["treat_type"];
				$treat_count = $treat_ticket_click_data[$i]["treat_count"];
				$total_click = $treat_ticket_click_data[$i]["total_click"];
			
				if($treat_type == 0)
					$treat_name = "None";
				else if($treat_type == 1)
					$treat_name = "Ticket 1";
				else if($treat_type == 2)
					$treat_name = "Ticket 2";
				else if($treat_type == 3)
					$treat_name = "Ticket 3";
				
				$total_treat_ticket_count += $treat_count;
				$day_treat_count += $treat_count;
			
				if($i == 0 || $today != $treat_ticket_click_data[$i-1]["today"])
				{
					$sql = "SELECT COUNT(*) FROM tbl_treat_ticket_stat WHERE today BETWEEN '$today' AND '$today'";
					$datecount = $db_analysis->getvalue($sql);
				}
?>
				<tr>
<?
				if($i == 0 || $today != $treat_ticket_click_data[$i-1]["today"])
				{
					$day_total_treat_count += $total_click;
?>
					<td class="tdc point_title" rowspan="<?= $datecount + 1?>"><?= $today ?></td>					
<?
				} 
?>					
					<td class="tdc"><?= $treat_name ?></td>
					<td class="tdc"><?= number_format($treat_count) ?></td>
					<td class="tdc"><?= ($total_click == 0) ? "0" : round($treat_count/$total_click * 100, 2) ?>%</td>					
				</tr>
						
<?
				if(($i != 0) && ($today != $treat_ticket_click_data[$i+1]["today"]))
				{
?>
					<tr>
						<td class="tdc point_title">Total</td>
						<td class="tdc point"><?= number_format($day_treat_count) ?></td>						
						<td class="tdc"><?= ($day_total_treat_count == 0) ? "0" : round($day_treat_count / $day_total_treat_count * 100, 2) ?>%</td>						
					</tr>					
<?
					$day_treat_count = 0;
					$day_total_treat_count = 0;
			
				}
			}
			
			for($i=0; $i<sizeof($total_treat_ticket_click_data); $i++)
			{
				$total_treat_type = $total_treat_ticket_click_data[$i]["treat_type"];
				$total_treat_type_count = $total_treat_ticket_click_data[$i]["total_treat_type_count"];	

				if($total_treat_type == 0)
					$treat_name = "None";
				else if($total_treat_type == 1)
					$treat_name = "Ticket 1";
				else if($total_treat_type == 2)
					$treat_name = "Ticket 2";
				else if($total_treat_type == 3)
					$treat_name = "Ticket 3";
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($i == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($total_treat_ticket_click_data)?>" valign="center">Total</td>
<?
				}
?>
					<td class="tdc"><?= $treat_name ?></td>
					<td class="tdc"><?= number_format($total_treat_type_count) ?></td>
					<td class="tdc"><?= ($total_treat_ticket_count == 0) ? "0" : round($total_treat_type_count/$total_treat_ticket_count * 100, 2) ?>%</td>
				</tr>
<?
			}
?>
			</tbody>
		</table>
		
        <br><br>
        
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">2. Treat Ticket 금액 통계 </span>
		<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">							
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>					
					<th class="tdc">로열티 누적 금액</th>
					<th class="tdc">트리트 누적 금액</th>
					<th class="tdc">누적 비율</th>	
					<th class="tdc">데일리 로열티 금액</th>
					<th class="tdc">데일리 트리트 금액</th>					
					<th class="tdc">데일리 환전 금액</th>
					<th class="tdc">데일리 환전 인원수</th>
					<th class="tdc">데일리 비율</th>
				</tr>
			</thead>
			<tbody>
<?
			for($i=0; $i<sizeof($treat_ticket_amount_data); $i++)
			{
				$today = $treat_ticket_amount_data[$i]["today"];
				$daily_loyalty_amount = $treat_ticket_amount_data[$i]["daily_platinum_loyalty_amount"];
				$daily_treat_amount = $treat_ticket_amount_data[$i]["daily_platinum_treat_amount"];
				$loyalty_keep_amount = $treat_ticket_amount_data[$i]["loyalty_keep_amount"];
				$treat_keep_amount = $treat_ticket_amount_data[$i]["treat_keep_amount"];
				$exchange_treat_count = $treat_ticket_amount_data[$i]["exchange_treat_count"];
				$exchange_treat_amount = $treat_ticket_amount_data[$i]["exchange_treat_amount"];
				
				$daily_loyalty_amount = $daily_loyalty_amount*0.01;				
				
				if($i == 0 || $today != $treat_ticket_amount_data[$i-1]["today"])
				{
					$sql = "SELECT COUNT(*) FROM tbl_treat_ticket_use_stat WHERE today BETWEEN '$today' AND '$today'";
					$datecount = $db_analysis->getvalue($sql);
				}				
?>
				<tr>
<?
				if($i == 0 || $today != $treat_ticket_amount_data[$i-1]["today"])
				{
?>
					<td class="tdc point_title" rowspan="<?= $datecount?>"><?= $today ?></td>
<?
				} 
?>					
					<td class="tdc"><?= number_format($loyalty_keep_amount) ?></td>
					<td class="tdc"><?= number_format($treat_keep_amount) ?></td>
					<td class="tdc"><?= ($treat_keep_amount == 0) ? "0" : round($loyalty_keep_amount/$treat_keep_amount * 100, 2) ?>%</td>		
					<td class="tdc"><?= number_format($daily_loyalty_amount) ?></td>					
					<td class="tdc"><?= number_format($daily_treat_amount) ?></td>					
					<td class="tdc"><?= number_format($exchange_treat_amount) ?></td>
					<td class="tdc"><?= number_format($exchange_treat_count) ?></td>
					<td class="tdc"><?= ($daily_treat_amount == 0) ? "0" : round($daily_loyalty_amount/$daily_treat_amount * 100, 2) ?>%</td>									
				</tr>

<?
			}
?>
			</tbody>
		</table>
     </div>        
     </form>	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_analysis->end();
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>