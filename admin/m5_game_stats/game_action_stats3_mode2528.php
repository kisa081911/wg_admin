<?
    $top_menu = "game_stats";
    $sub_menu = "game_action_stats3_mode2528";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $os_term = ($_GET["os_term"] == "") ? "0" : $_GET["os_term"];
    
    $pagename = "game_action_stats3_mode2528.php";
    
    $min_winrate = ($_GET["min_winrate"] == "") ? 94 : $_GET["min_winrate"];
    $max_winrate = ($_GET["max_winrate"] == "") ? 96 : $_GET["max_winrate"];
    $mode = ($_GET["mode"] == "") ? "2528" :$_GET["mode"];
    $search_playcount = ($_GET["search_playcount"] == "") ? "100000" :$_GET["search_playcount"];

    $startdate = ($_GET["startdate"] == "") ? date("Y-m-d", time() - (60 * 60 * 24 * 30)) : $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
    
    if($mode == "2528")
    	$modestr = "MODE IN (25, 28)";
    else
    	$modestr = "MODE=$mode";
    
    if($os_term == "0")
    	$table = "tbl_game_cash_stats_daily2";
    else if($os_term == "1")
    	$table = "tbl_game_cash_stats_ios_daily2";
    else if($os_term == "2")
    	$table = "tbl_game_cash_stats_android_daily2";
    else if($os_term == "3")
    	$table = "tbl_game_cash_stats_amazon_daily2";
    
    $db_main2 = new CDatabase_Main2();
    
    $sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = a.slottype) AS slotname, 
				slottype, SUM(moneyin) AS money_in, SUM(moneyout) AS money_out, SUM(moneyout)/SUM(moneyin)*100 winrate, 
				SUM(unit_moneyout)/SUM(unit_moneyin)*100 AS unit_winrate, SUM(playcount) AS playcount
			FROM $table a
			WHERE  $modestr AND writedate BETWEEN '$startdate' AND '$enddate'
			GROUP BY slottype HAVING (unit_winrate < $min_winrate OR unit_winrate > $max_winrate) AND playcount > $search_playcount
			ORDER BY writedate DESC ";
    
    $winrate_data = $db_main2->gettotallist($sql);
    
?>
<link type="text/css" href="/js/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }

    $(function() {
        $("#startdate").datepicker({ });
    });

    $(function() {
        $("#enddate").datepicker({ });
    });

    function change_os_term(term)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("term_web");
		var ios = document.getElementById("term_ios");
		var android = document.getElementById("term_android");
		var amazon = document.getElementById("term_amazon");
		
		document.search_form.os_term.value = term;

		if (term == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (term == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}

		search_form.submit();
	}
    
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
            	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
					<input type="button" class="<?= ($os_term == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="term_web" onclick="change_os_term('0')"    />
					<input type="button" class="<?= ($os_term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_os_term('1')" />
					<input type="button" class="<?= ($os_term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_os_term('2')"    />
					<input type="button" class="<?= ($os_term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_os_term('3')"    />
				</span>
				
                <!-- title_warp -->
                <div class="title_wrap">
<?
	if($os_term == 0)
		$os_txt = "Web";
	else if($os_term == 1)
		$os_txt = "iOS";
	else if($os_term == 2)
		$os_txt = "Android";
	else if($os_term == 3)
		$os_txt = "Amazon";
?>
                    <div class="title"><?= $top_menu_txt ?> &gt; 베팅금액별 트래킹(<?= $os_txt ?>) - Mode 25, 28 통계</div>
                    <div class="search_box">
                    	<div class="floatr">
	                    	단위 승률 :
	                        <input type="input" class="search_text" id="min_winrate" name="min_winrate" style="width:30px" value="<?= $min_winrate?>" onkeypress="search_press(event)" />&nbsp;%
	                        &nbsp;미만
	                        
	                        <div class="clear" style="height:9px;"></div>
	                        
	                       	 단위 승률 :                     
	                        <input type="input" class="search_text" id="max_winrate" name="max_winrate" style="width:30px" value="<?= $max_winrate?>" onkeypress="search_press(event)" />&nbsp;%
	                        &nbsp;초과
	                        
	                        <div class="clear" style="height:9px;"></div>
	                        
	                        Play Count :                     
	                        <input type="input" class="search_text" id="search_playcount" name="search_playcount" style="width:70px" value="<?= $search_playcount?>" onkeypress="search_press(event)" />
	                        &nbsp;이상
						</div>
                        
                        <div class="clear" style="height:9px;"></div>
	            		<input type="hidden" name="os_term" id="os_term" value="<?= $os_term ?>" /> 
	            		<select name="mode" id="mode">
                    		<option value="2528" <?= ($mode=="2528") ? "selected" : "" ?>>승률 부양 25,28(베팅금액별 트래킹, 전체)</option>
                    		<option value="25" <?= ($mode=="25") ? "selected" : "" ?>>승률 부양 25(베팅금액별 트래킹, 비결제)</option>
                    		<option value="28" <?= ($mode=="28") ? "selected" : "" ?>>승률 부양 28(베팅금액별 트래킹, 결제)</option>
                    	</select>
	            		&nbsp;&nbsp;&nbsp;&nbsp;
	            		<div class="floatr">
                    		<input type="input" class="search_text" id="startdate" name="startdate" style="width:70px" value="<?= $startdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />~                    
                        	<input type="input" class="search_text" id="enddate" name="enddate" style="width:70px" value="<?= $enddate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
                        	&nbsp;&nbsp;&nbsp;&nbsp;
                        	<input type="button" class="btn_search" value="검색" onclick="search()" />
                        </div>
                		
                		<div class="clear" style="height:9px;"></div>
                    </div>
                </div>
                <!-- //title_warp -->
                
                
                <table class="tbl_list_basic1">

                <colgroup>
                    <col width="">
                    <col width="">
                    <col width="">
                    <col width="">
                    <col width="">
                    <col width="">
                    <col width="">
                </colgroup>

                <thead>
                    <tr>
                        <th>Slot Number</th>
                        <th>Slot Name</th>
                        <th>Money In</th>
                        <th>Money Out</th>         
                        <th>일반 승률</th>
                        <th>단위 승률</th>
                        <th>playcount</th>
                    </tr>
                </thead>
                <tbody>
<?
    
    for ($i=0; $i<sizeof($winrate_data); $i++)
    {
        $slotname = $winrate_data[$i]["slotname"];
        $slottype = $winrate_data[$i]["slottype"];
        $money_in = $winrate_data[$i]["money_in"];
        $money_out = $winrate_data[$i]["money_out"];
        $winrate = $winrate_data[$i]["winrate"];
        $unit_winrate = $winrate_data[$i]["unit_winrate"];
        $playcount = $winrate_data[$i]["playcount"];
        
?>
                  <tr onmouseover="className='tr_over'" onmouseout="className=''" >
                        <td class="tdc point"><?= $slottype ?></td>
                        <td class="tdc point"><?= $slotname ?></td>
                        <td class="tdc point"><?= number_format($money_in) ?></td>
                        <td class="tdc point"><?= number_format($money_out) ?></td>
                        <td class="tdc point"><?= round($winrate, 2) ?>%</td>
                        <td class="tdc point"><?= round($unit_winrate, 2) ?>%</td>
                        <td class="tdc point"><?= number_format($playcount) ?></td>
                    </tr>

<?
    }
?>
                    </tbody>
                </table>
            </form>
            
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    $db_main2->end();
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>