<?
	$top_menu = "game_stats";
	$sub_menu = "daily_free_stat";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$os_type = ($_GET["os_type"] == "") ? "4" :$_GET["os_type"];
	$search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    
    if($search_start_createdate == "")
    	$search_start_createdate = date("Y-m-d", strtotime("-3 day"));
    
    if($search_end_createdate == "")
    	$search_end_createdate = $today;
        
        $tail = "";
        
        if($os_type == "0")
        {
            $os_txt = "Web";
            $os_type_sql = "category = 0 ";
            
        }
        else if($os_type == "1")
        {
            $os_txt = "IOS";
            $os_type_sql = "category = 1 ";
        }
        else if($os_type == "2")
        {
            $os_txt = "Android";
            $os_type_sql = "category = 2 ";
        }
        else if($os_type == "3")
        {
            $os_txt = "Amazon";
            $os_type_sql = "category = 3 ";
        }
        else if($os_type == "4")
        {
            $os_txt = "Total";
            $os_type_sql = "1=1 ";
            $tail = "group by today, t1.type ";
        }
        
        $db_main2 = new CDatabase_Main2();
        $db_analysis = new CDatabase_Analysis();
        
        if($os_type == 4)
        {
            $sql = "SELECT today, category, t1.type AS type, name, SUM(freecount) AS freecount, SUM(freeamount) AS freeamount, (SELECT SUM(freeamount) FROM tbl_user_freecoin_daily WHERE today = t1.today) AS total_amount ".
                "FROM tbl_user_freecoin_daily t1 LEFT JOIN tbl_freecoin_type t2 ".
                "ON t1.type = t2.type WHERE $os_type_sql AND today BETWEEN '$search_start_createdate' AND '$search_end_createdate' $tail ORDER BY today DESC, t1.type ASC;";
        }
        else
        {
            $sql = "SELECT today, category, t1.type AS type, name, SUM(freecount) AS freecount, SUM(freeamount) AS freeamount, (SELECT SUM(freeamount) FROM tbl_user_freecoin_daily WHERE today = t1.today AND category = $os_type) AS total_amount ".
                "FROM tbl_user_freecoin_daily t1 LEFT JOIN tbl_freecoin_type t2 ".
                "ON t1.type = t2.type WHERE $os_type_sql AND today BETWEEN '$search_start_createdate' AND '$search_end_createdate' GROUP BY today ,t1.type ORDER BY today DESC, t1.type ASC;";
        }
        $freecoin_data = $db_analysis->gettotallist($sql);
        
        ?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});

	function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		var total = document.getElementById("type_total");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
			total.className="btn_schedule";
		}
		else if (type == "4")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="daily_free_stat.php">
	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
		<input type="button" class="<?= ($os_type == "4") ? "btn_schedule_select" : "btn_schedule" ?>" value="Total" id="type_total" onclick="change_os_type('4')"    />
		<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
		<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
		<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
		<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
	</span>
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 무료 코인 통계(<?= $os_txt ?>)</div>
		<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />
		<div class="search_box">
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	
	<div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">                
            </colgroup>
            <thead>
            <tr>
                <th>날짜</th>
                <th class="tdc">보너스타입</th>
                <th class="tdc">횟수</th>                
                <th class="tdc">금액</th>
                <th class="tdc">비중</th>
            </tr>
            </thead>
            <tbody>
<?
			$day_amount_cnt = 0;
			$day_amount_coin = 0;
			$day_amount_total_coin = 0;

			$sum_amount_cnt = 0;			
			$sum_amount_coin = 0;
			$sum_amount_total_coin = 0;			

			for($i=0; $i<sizeof($freecoin_data); $i++)
			{
				$today = $freecoin_data[$i]["today"];
				$type = $freecoin_data[$i]["type"];
				$name = $freecoin_data[$i]["name"];
				$free_count = $freecoin_data[$i]["freecount"];
				$free_amount = $freecoin_data[$i]["freeamount"];
				$free_total_amount = $freecoin_data[$i]["total_amount"];
				
				if($i == 0 || $today != $freecoin_data[$i-1]["today"])
				{
					if($os_type == 4)
					{
						$sql = "SELECT COUNT(*) ".
							   "FROM ".
							   "( ".
							   "  SELECT COUNT(*) FROM tbl_user_freecoin_daily WHERE today BETWEEN '$today' AND '$today' GROUP BY type ".
							   ") t1";
						
						$platinum_wheel_sql = "SELECT DATE_FORMAT(writedate,'%Y-%m-%d'), more_bonus, COUNT(DISTINCT useridx) AS cnt, SUM(award_amount)  AS amount
                                               FROM (
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_0 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_1 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_2 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_3 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_4 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_5 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_6 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL 
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_7 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_8 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_9 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	) t1
                                               GROUP BY more_bonus, DATE_FORMAT(writedate,'%Y-%m-%d') ORDER BY DATE_FORMAT(writedate,'%Y-%m-%d')";
			                       $platinum_wheel_arr = $db_main2->gettotallist($platinum_wheel_sql);
					}
					else
						$sql = "SELECT COUNT(*) ".
								"FROM ".
								"( ".
								"  SELECT COUNT(*) FROM tbl_user_freecoin_daily WHERE $os_type_sql AND today BETWEEN '$today' AND '$today' GROUP BY type ".
								") t1";
					
					$datecount = $db_analysis->getvalue($sql);
					
					$datecount = $datecount + 1 + sizeof($platinum_wheel_arr);
				}
				
				$day_amount_cnt += $free_count;
				$day_amount_coin += $free_amount;
				$day_amount_total_coin += $free_amount;
				
				$sum_amount_cnt += $free_count;				
				$sum_amount_coin += $free_amount;
				$sum_amount_total_coin += $free_amount;				
?>
				<tr>
<?
				if($i == 0 || $today != $freecoin_data[$i-1]["today"])
				{
?>				
					<td class="tdc point_title" rowspan="<?= $datecount?>"><?= $today ?></td>
<?
				} 
?>					
					<td class="tdc" alt=<?=$type?>><?= $name ?></td>
					<td class="tdc"><?= number_format($free_count) ?></td>
					<td class="tdc"><?= number_format($free_amount) ?></td>
					<td class="tdc"><?= ($free_total_amount == 0) ? "0" : round($free_amount / $free_total_amount * 100, 2) ?>%</td>
				</tr>
						
<?
				if(($i != 0) && ($today != $freecoin_data[$i+1]["today"]))
				{
				    if(sizeof($platinum_wheel_arr) > 0)
				    {   
				        for($u=0; $u<sizeof($platinum_wheel_arr); $u++)
				        {
				            $more_bonus = $platinum_wheel_arr[$u]["more_bonus"];
				            $cnt = $platinum_wheel_arr[$u]["cnt"];
				            $amount = $platinum_wheel_arr[$u]["amount"];
?>
						<td class="tdc"><?= "Platinum Wheel More ".$more_bonus."%" ?></td>
        					<td class="tdc"><?= number_format($cnt) ?></td>
        					<td class="tdc"><?= number_format($amount) ?></td>
        					<td class="tdc"><?= ($free_total_amount == 0) ? "0" : round($amount / $free_total_amount * 100, 2) ?>%</td>
        				</tr>
<?
				        }
				    }
?>
					<tr>
						<td class="tdc point">Total</td>
						<td class="tdc point"><?= number_format($day_amount_cnt) ?></td>
						<td class="tdc point"><?= number_format($day_amount_coin) ?></td>
						<td class="tdc"><?= ($day_amount_total_coin == 0) ? "0" : round($day_amount_coin / $day_amount_total_coin * 100, 2) ?>%</td>						
					</tr>					
<?
					$day_amount_cnt = 0;					
					$day_amount_coin = 0;
					$day_amount_total_coin = 0;					
				}
			}
?>
			<tr>
				<td class="tdc point" colspan=2>Total</td>
				<td class="tdc point"><?= number_format($sum_amount_cnt) ?></td>				
				<td class="tdc point"><?= number_format($sum_amount_coin) ?></td>
				<td class="tdc point"><?= ($sum_amount_total_coin == 0) ? "0" : round($sum_amount_coin / $sum_amount_total_coin * 100, 2) ?>%</td>
			</tr>
	
			</tbody>
            </table>
     </div>
        
     </form>
	
</div>
<!--  //CONTENTS WRAP -->
<?
    $db_main2->end();
	$db_analysis->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>