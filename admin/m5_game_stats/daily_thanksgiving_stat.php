<?
	$top_menu = "game_stats";
	$sub_menu = "daily_thanksgiving_stat2";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$os_type = ($_GET["os_type"] == "") ? "4" :$_GET["os_type"];
	$search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    
    if($search_start_createdate == "")
    	$search_start_createdate = date("Y-m-d", strtotime("-7 day"));
    
    if($search_end_createdate == "")
    	$search_end_createdate = $today;
        
    $tail = "";
    
    if($os_type == "0")
    {
    	$os_txt = "Web";
    	$os_type_sql = "os_type = 0 ";
    	
    }
    else if($os_type == "1")
    {
    	$os_txt = "IOS";
    	$os_type_sql = "os_type = 1 ";
    }
    else if($os_type == "2")
    {
    	$os_txt = "Android";
    	$os_type_sql = "os_type = 2 ";
    }
    else if($os_type == "3")
    {
    	$os_txt = "Amazon";
    	$os_type_sql = "os_type = 3 ";
    }
    else if($os_type == "4")
    {
    	$os_txt = "Total";
    	$os_type_sql = "1=1 ";
    	$tail = "group by today, t1.type ";
    }
	
	$db_main2 = new CDatabase_Main2();
	
	if($os_type == 4)
	{
		//tbl_user_event_quest_collect_log
		$sql = "SELECT DATE_FORMAT(t1.write_date,'%Y-%m-%d') AS date_day, reward_group, COUNT(t1.logidx) AS cnt_reward,  SUM(t2.amount) AS sum_reward
				FROM 
				(
					SELECT * FROM tbl_user_event_quest_collect_log WHERE event_idx = 3 AND reward_group != -1 AND useridx > 20000
				)t1
				LEFT JOIN 
				(
						SELECT * FROM `tbl_user_freecoin_log_0` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_1` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_2` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_3` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_4` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_5` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_6` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_7` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_8` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_9` WHERE TYPE = 137 AND  useridx > 20000
				) t2
				ON t1.useridx = t2.useridx AND t1.os_type = t2.category AND DATE_FORMAT(t1.write_date,'%Y-%m-%d') = DATE_FORMAT(t2.writedate,'%Y-%m-%d')
				WHERE '$search_start_createdate 00:00:00' <= t1.write_date AND t1.write_date <= '$search_end_createdate 23:59:59'
				GROUP BY DATE_FORMAT(t1.write_date,'%Y-%m-%d'), reward_group order by DATE_FORMAT(t1.write_date,'%Y-%m-%d') desc";
	}
	else 
	{
		$sql = "SELECT DATE_FORMAT(t1.write_date,'%Y-%m-%d') AS date_day, reward_group, COUNT(t1.logidx) AS cnt_reward, SUM(t2.amount) AS sum_reward
				FROM 
				(
					SELECT * FROM tbl_user_event_quest_collect_log WHERE event_idx = 3  AND reward_group != -1  AND useridx > 20000
				)t1
				LEFT JOIN 
				(
						SELECT * FROM `tbl_user_freecoin_log_0` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_1` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_2` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_3` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_4` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_5` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_6` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_7` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_8` WHERE TYPE = 137 AND  useridx > 20000
						UNION ALL
						SELECT * FROM `tbl_user_freecoin_log_9` WHERE TYPE = 137 AND  useridx > 20000
				) t2
				ON t1.useridx = t2.useridx AND t1.os_type = t2.category AND DATE_FORMAT(t1.write_date,'%Y-%m-%d') = DATE_FORMAT(t2.writedate,'%Y-%m-%d')
				WHERE $os_type_sql AND '$search_start_createdate 00:00:00' <= t1.write_date AND t1.write_date <= '$search_end_createdate 23:59:59'
				GROUP BY DATE_FORMAT(t1.write_date,'%Y-%m-%d'), reward_group order by DATE_FORMAT(t1.write_date,'%Y-%m-%d') desc";
	}
	
	function get_today_row_array($summarylist, $today)
	{
	    $row_array = array();
	    
	    for ($i=0; $i<sizeof($summarylist); $i++)
	    {
	        if ($summarylist[$i]["date_day"] == $today)
	        {
	            array_push($row_array, $summarylist[$i]["reward_group"]);
	        }
	    }
	    
	    return $row_array;
	}
	
	$quest_data = $db_main2->gettotallist($sql);
	
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});

	function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		var total = document.getElementById("type_total");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
			total.className="btn_schedule";
		}
		else if (type == "4")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="daily_thanksgiving_stat.php">
	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
		<input type="button" class="<?= ($os_type == "4") ? "btn_schedule_select" : "btn_schedule" ?>" value="Total" id="type_total" onclick="change_os_type('4')"    />
		<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
		<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
		<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
		<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
	</span>
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Thanksgiving event 통계(<?= $os_txt ?>)</div>
		<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />
		<div class="search_box">
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	
	<div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">                
            </colgroup>
            <thead>
            <tr>
                <th>날짜</th>
                <th class="tdc">머니인 등급</th>
                <th class="tdc">리워드 받은 유저 수</th>                
                <th class="tdc">리워드 금액</th>
                <th class="tdc">1인당 평균 리워드 금액</th>
            </tr>
            </thead>
            <tbody>
<?
			$day_amount_cnt = 0;
			$day_amount_coin = 0;
			$day_amount_total_coin = 0;

			$sum_amount_cnt = 0;			
			$sum_amount_coin = 0;
			$sum_amount_total_coin = 0;

			for($i=0; $i<sizeof($quest_data); $i++)
			{
				$today = $quest_data[$i]["date_day"];
				$reward_group = $quest_data[$i]["reward_group"];
				$cnt_reward = $quest_data[$i]["cnt_reward"];
				$sum_reward = $quest_data[$i]["sum_reward"];
				
				$day_cnt_reward += $cnt_reward;
				$day_sum_reward += $sum_reward;								
?>
				<tr>
<?
                if($currenttoday != $today)
                {
                    $currenttoday = $today;
                    $viral_list = get_today_row_array($quest_data, $today);
?>
					<td class="tdc point_title" rowspan="<?= sizeof($viral_list)+1?>" valign="center"><?= $today ?></td>
<?
                }
?>	
					<td class="tdc"><?= $reward_group?></td>
					<td class="tdc"><?= number_format($cnt_reward) ?></td>
					<td class="tdc"><?= number_format($sum_reward) ?></td>
					<td class="tdc"><?= ($cnt_reward == 0) ? "0" : number_format($sum_reward/$cnt_reward) ?></td>
				</tr>
<?
				if(($today != $quest_data[$i+1]["date_day"]))
				{
?>
					<tr>
						<td class="tdc point_title">Total</td>
						<td class="tdc point"><?= number_format($day_cnt_reward) ?></td>
						<td class="tdc point"><?= number_format($day_sum_reward) ?></td>						
						<td class="tdc"><?= ($day_cnt_reward == 0) ? "0" : number_format($day_sum_reward/$day_cnt_reward) ?></td>						
					</tr>					
<?
					$day_cnt_reward = 0;
					$day_sum_reward = 0;			
				}
			}
?>
			</tbody>
            </table>
     </div>
        
     </form>
	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main2->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>