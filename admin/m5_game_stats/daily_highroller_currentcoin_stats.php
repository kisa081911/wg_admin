<?
	$top_menu = "game_stats";
	$sub_menu = "daily_highroller_currentcoin_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$user_current = ($_GET["user_current"] == "") ? "0" :$_GET["user_current"];
	$search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    
    if($search_start_createdate == "")
    	$search_start_createdate = "2016-03-10";
    
    if($search_end_createdate == "")
    	$search_end_createdate = $today;
    
    $currentcoin_sql = "";
    
    if($user_current == 1)
    	$currentcoin_sql = " AND FLOOR(currentcoin/1000000) <= 1 ";
    else if($user_current == 2)
    	$currentcoin_sql = " AND FLOOR(currentcoin/1000000) > 1 AND FLOOR(currentcoin/1000000) <= 5 ";
    else if($user_current == 3)
    	$currentcoin_sql = " AND FLOOR(currentcoin/1000000) > 5 AND FLOOR(currentcoin/1000000) <= 10 ";
    else if($user_current == 4)
    	$currentcoin_sql = " AND FLOOR(currentcoin/1000000) > 10 AND FLOOR(currentcoin/1000000) <= 20 ";
    else if($user_current == 5)
    	$currentcoin_sql = " AND FLOOR(currentcoin/1000000) > 20 AND FLOOR(currentcoin/1000000) <= 50 ";
    else if($user_current == 6)
    	$currentcoin_sql = " AND FLOOR(currentcoin/1000000) > 50 AND FLOOR(currentcoin/1000000) <= 100 ";
    else if($user_current == 7)
    	$currentcoin_sql = " AND FLOOR(currentcoin/1000000) > 100 AND FLOOR(currentcoin/1000000) <= 500 ";
    else if($user_current == 8)
    	$currentcoin_sql = " AND FLOOR(currentcoin/1000000) > 500 AND FLOOR(currentcoin/1000000) <= 1000 ";
    else if($user_current == 9)
    	$currentcoin_sql = " AND FLOOR(currentcoin/1000000) > 1000 ";
	
	$db_other = new CDatabase_Other();
		
	$sql = "SELECT today, ".
			"(CASE WHEN FLOOR(currentcoin/1000000) > 1000 THEN '1000_1000+' ".
			"WHEN FLOOR(currentcoin/1000000) > 500 THEN '0500_1000' ".
			"WHEN FLOOR(currentcoin/1000000) > 100 THEN '0100_0500' ".
			"WHEN FLOOR(currentcoin/1000000) > 50 THEN '0050_0100' ".
			"WHEN FLOOR(currentcoin/1000000) > 20 THEN '0020_0050' ".
			"WHEN FLOOR(currentcoin/1000000) > 10 THEN '0010_0020' ".
			"WHEN FLOOR(currentcoin/1000000) > 5 THEN '0005_0010' ".
			"WHEN FLOOR(currentcoin/1000000) > 1 THEN '0001_0005' ".
			"ELSE '0000_0001' END) AS std_coin, ".
			"COUNT(useridx) AS usercnt, FLOOR(AVG(currentcoin)) AS currentcoin,	".
			"SUM(h_moneyin) AS h_moneyin, SUM(h_moneyout) AS h_moneyout, ROUND(SUM(h_moneyout)/SUM(h_moneyin)*100, 2) AS h_winrate, ".
			"SUM(h_playtime) AS h_playtime, SUM(h_playcount) AS h_playcount, ".
			"SUM(purchasecount) AS purchasecount, SUM(purchaseamount) AS purchaseamount, SUM(purchasecoin) AS purchasecoin, ".
			"SUM(famelevel) AS famelevel, SUM(ty_point) AS ty_point, SUM(freecoin) AS freecoin, SUM(jackpot) AS jackpot ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' AND h_moneyin >= 10000000 $currentcoin_sql".
			"GROUP BY today, std_coin ".
			"ORDER BY today DESC, std_coin ASC;";	
	$highroller_currentcoin_data = $db_other->gettotallist($sql);	
	
	$sql = "SELECT today, ".
			"(CASE WHEN FLOOR(currentcoin/1000000) > 1000 THEN '1000_1000+' ".
			"WHEN FLOOR(currentcoin/1000000) > 500 THEN '0500_1000' ".
			"WHEN FLOOR(currentcoin/1000000) > 100 THEN '0100_0500' ".
			"WHEN FLOOR(currentcoin/1000000) > 50 THEN '0050_0100' ".
			"WHEN FLOOR(currentcoin/1000000) > 20 THEN '0020_0050' ".
			"WHEN FLOOR(currentcoin/1000000) > 10 THEN '0010_0020' ".
			"WHEN FLOOR(currentcoin/1000000) > 5 THEN '0005_0010' ".
			"WHEN FLOOR(currentcoin/1000000) > 1 THEN '0001_0005' ".
			"ELSE '0000_0001' END) AS std_coin, ".
			"COUNT(useridx) AS usercnt ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' $currentcoin_sql".
			"GROUP BY today, std_coin ".
			"ORDER BY today DESC, std_coin ASC;";
	$currentcoin_data = $db_other->gettotallist($sql);
	
	$currentcoin_data_array = array();
	
	for($j = 0; $j < sizeof($currentcoin_data); $j++)
	{
		$currentcoin_data_today = $currentcoin_data[$j]["today"];
		$currentcoin_data_std_coin = $currentcoin_data[$j]["std_coin"];
		$currentcoin_data_usercnt = $currentcoin_data[$j]["usercnt"];
		
		$currentcoin_data_array[$currentcoin_data_today][$currentcoin_data_std_coin] = $currentcoin_data_usercnt;
	}
	
	$sql = "SELECT ".
			"(CASE WHEN FLOOR(currentcoin/1000000) > 1000 THEN '1000_1000+' ".
			"WHEN FLOOR(currentcoin/1000000) > 500 THEN '0500_1000' ".
			"WHEN FLOOR(currentcoin/1000000) > 100 THEN '0100_0500' ".
			"WHEN FLOOR(currentcoin/1000000) > 50 THEN '0050_0100' ".
			"WHEN FLOOR(currentcoin/1000000) > 20 THEN '0020_0050' ".
			"WHEN FLOOR(currentcoin/1000000) > 10 THEN '0010_0020' ".
			"WHEN FLOOR(currentcoin/1000000) > 5 THEN '0005_0010' ".
			"WHEN FLOOR(currentcoin/1000000) > 1 THEN '0001_0005' ".
			"ELSE '0000_0001' END) AS std_coin, ".
			"COUNT(useridx) AS usercnt, FLOOR(AVG(currentcoin)) AS currentcoin,	".
			"SUM(h_moneyin) AS h_moneyin, SUM(h_moneyout) AS h_moneyout, ROUND(SUM(h_moneyout)/SUM(h_moneyin)*100, 2) AS h_winrate, ".
			"SUM(h_playtime) AS h_playtime, SUM(h_playcount) AS h_playcount, ".
			"SUM(purchasecount) AS purchasecount, SUM(purchaseamount) AS purchaseamount, SUM(purchasecoin) AS purchasecoin, ".
			"SUM(famelevel) AS famelevel, SUM(ty_point) AS ty_point, SUM(freecoin) AS freecoin, SUM(jackpot) AS jackpot ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' AND h_moneyin >= 10000000 $currentcoin_sql".
			"GROUP BY std_coin ".
			"ORDER BY std_coin ASC;";
	$total_highroller_currentcoin_data = $db_other->gettotallist($sql);
	
	$sql = "SELECT ".
			"(CASE WHEN FLOOR(currentcoin/1000000) > 1000 THEN '1000_1000+' ".
			"WHEN FLOOR(currentcoin/1000000) > 500 THEN '0500_1000' ".
			"WHEN FLOOR(currentcoin/1000000) > 100 THEN '0100_0500' ".
			"WHEN FLOOR(currentcoin/1000000) > 50 THEN '0050_0100' ".
			"WHEN FLOOR(currentcoin/1000000) > 20 THEN '0020_0050' ".
			"WHEN FLOOR(currentcoin/1000000) > 10 THEN '0010_0020' ".
			"WHEN FLOOR(currentcoin/1000000) > 5 THEN '0005_0010' ".
			"WHEN FLOOR(currentcoin/1000000) > 1 THEN '0001_0005' ".
			"ELSE '0000_0001' END) AS std_coin, ".
			"COUNT(useridx) AS usercnt ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' $currentcoin_sql".
			"GROUP BY std_coin ".
			"ORDER BY std_coin ASC;";
	$total_currentcoin_data = $db_other->gettotallist($sql);
	
	$total_currentcoin_data_array = array();
	
	for($j = 0; $j < sizeof($total_currentcoin_data); $j++)
	{
		$currentcoin_data_std_coin = $total_currentcoin_data[$j]["std_coin"];
		$currentcoin_data_usercnt = $total_currentcoin_data[$j]["usercnt"];
	
		$total_currentcoin_data_array[$currentcoin_data_std_coin] = $currentcoin_data_usercnt;
	}

?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="daily_highroller_currentcoin_stats.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 하일롤러 사용자 현재 코인 통계</div>
		<div class="search_box">
			사용자 Coin&nbsp;:&nbsp; 
			<select name="user_current" id="user_current">
					<option value="" <?= ($user_current=="0") ? "selected" : "" ?>>전체</option>					
					<option value="1" <?= ($user_current=="1") ? "selected" : "" ?>>0 ~ 백만</option>
					<option value="2" <?= ($user_current=="2") ? "selected" : "" ?>>백만 ~ 오백만</option>                       
					<option value="3" <?= ($user_current=="3") ? "selected" : "" ?>>오백만 ~ 천만</option>
					<option value="4" <?= ($user_current=="4") ? "selected" : "" ?>>천만 ~ 이천만</option>
					<option value="5" <?= ($user_current=="5") ? "selected" : "" ?>>이천만 ~ 오천만</option>
					<option value="6" <?= ($user_current=="6") ? "selected" : "" ?>>오천만 ~ 일억</option>
					<option value="7" <?= ($user_current=="7") ? "selected" : "" ?>>일억 ~ 오억</option>
					<option value="8" <?= ($user_current=="8") ? "selected" : "" ?>>오억 ~ 십억</option>
					<option value="9" <?= ($user_current=="9") ? "selected" : "" ?>>십억 이상</option>					
			</select>&nbsp;&nbsp;&nbsp;
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	
	<div id="tab_content_1">
            <table class="tbl_list_basic1" style="width:1300px">
            <colgroup>
                <col width="70">
                <col width="90">
                <col width="90">
                <col width="100">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70"> 
                <col width="100">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">                 
            </colgroup>
            <thead>
            <tr>
                <th>날짜</th>
                <th class="tdc">보유코인 범위</th>
                <th class="tdc">유저수<br>(전일 유지율)</th>
                <th class="tdc">평균보유코인</th>                
                <th class="tdc">moneyin</th>
                <th class="tdc">moneyout</th>
                <th class="tdc">승률</th>
                <th class="tdc">평균 playtime</th>
                <th class="tdc">평균 playcount</th>
                <th class="tdc">결제횟수</th>
                <th class="tdc">결제금액</th>
                <th class="tdc">결제코인</th>
                <th class="tdc">평균 FameLevel</th>
                <th class="tdc">jackpot</th>
                <th class="tdc">전체유저<br>대비</th>
            </tr>
            </thead>
            <tbody>
<?
			for($i=0; $i<sizeof($highroller_currentcoin_data); $i++)
			{
				$today = $highroller_currentcoin_data[$i]["today"];
				$yester_day =  date("Y-m-d", strtotime("-1 day", strtotime($today)));				
				$std_coin = $highroller_currentcoin_data[$i]["std_coin"];
				$usercnt = $highroller_currentcoin_data[$i]["usercnt"];
				$currentcoin = $highroller_currentcoin_data[$i]["currentcoin"];
				$h_moneyin = $highroller_currentcoin_data[$i]["h_moneyin"];
				$h_moneyout = $highroller_currentcoin_data[$i]["h_moneyout"];
				$h_winrate = $highroller_currentcoin_data[$i]["h_winrate"];
				$h_playtime = $highroller_currentcoin_data[$i]["h_playtime"]/$usercnt;
				$h_playcount = $highroller_currentcoin_data[$i]["h_playcount"]/$usercnt;
				$purchasecount = $highroller_currentcoin_data[$i]["purchasecount"];
				$purchaseamount = $highroller_currentcoin_data[$i]["purchaseamount"]/10;
				$purchasecoin = $highroller_currentcoin_data[$i]["purchasecoin"];
				$famelevel = $highroller_currentcoin_data[$i]["famelevel"]/$usercnt;				
				$jackpot = $highroller_currentcoin_data[$i]["jackpot"];
				$currentcoin_usercnt = $currentcoin_data_array[$today][$std_coin];
				
				$usercnt_rate = ($usercnt/$currentcoin_usercnt)*100;
				
				$std_currentcoin_str = "";
				
				if($std_coin == "0000_0001")
					$std_currentcoin_str = "0 ~ 백만";
				else if($std_coin == "0001_0005")
					$std_currentcoin_str = "백만 ~ 오백만";
				else if($std_coin == "0005_0010")
					$std_currentcoin_str = "오백만 ~ 천만";
				else if($std_coin == "0010_0020")
					$std_currentcoin_str = "천만 ~ 이천만";
				else if($std_coin == "0020_0050")
					$std_currentcoin_str = "이천만 ~ 오천만";
				else if($std_coin == "0050_0100")
					$std_currentcoin_str = "오천만 ~ 일억";
				else if($std_coin == "0100_0500")
					$std_currentcoin_str = "일억 ~ 오억";
				else if($std_coin == "0500_1000")
					$std_currentcoin_str = "오억 ~ 십억";
				else if($std_coin == "1000_1000+")
					$std_currentcoin_str = "십억 이상";
?>
				<tr>
<?
				if($i == 0 || $today != $highroller_currentcoin_data[$i-1]["today"])
				{
					if($std_currentcoin_str != "")
					{
						$sql = "SELECT today, ".
								"(CASE WHEN FLOOR(currentcoin/1000000) > 1000 THEN '1000_1000+' ".
								"WHEN FLOOR(currentcoin/1000000) > 500 THEN '0500_1000' ".
								"WHEN FLOOR(currentcoin/1000000) > 100 THEN '0100_0500' ".
								"WHEN FLOOR(currentcoin/1000000) > 50 THEN '0050_0100' ".
								"WHEN FLOOR(currentcoin/1000000) > 20 THEN '0020_0050' ".
								"WHEN FLOOR(currentcoin/1000000) > 10 THEN '0010_0020' ".
								"WHEN FLOOR(currentcoin/1000000) > 5 THEN '0005_0010' ".
								"WHEN FLOOR(currentcoin/1000000) > 1 THEN '0001_0005' ".
								"ELSE '0000_0001' END) AS std_coin ".								
								"FROM `tbl_user_playstat_daily` ".
								"WHERE '$today' <= today AND today <= '$today' AND h_moneyin >= 10000000 $currentcoin_sql".
								"GROUP BY std_coin ".
								"ORDER BY std_coin ASC;";
						$highroller_currentcoin_rowdata = $db_other->gettotallist($sql);
						
						$sql = "SELECT t1.std_coin AS std_coin, ROUND(COUNT(t2.std_coin)/COUNT(t1.std_coin)*100, 2) AS std_yesterday_rate ".
								"FROM ".
								"( ".
								"	SELECT today, useridx, ".
								"	(CASE WHEN FLOOR(currentcoin/1000000) > 1000 THEN '1000_1000+' ".
								"	WHEN FLOOR(currentcoin/1000000) > 500 THEN '0500_1000' ".
								"	WHEN FLOOR(currentcoin/1000000) > 100 THEN '0100_0500' ".
								"	WHEN FLOOR(currentcoin/1000000) > 50 THEN '0050_0100' ".
								"	WHEN FLOOR(currentcoin/1000000) > 20 THEN '0020_0050' ".
								"	WHEN FLOOR(currentcoin/1000000) > 10 THEN '0010_0020' ".
								"	WHEN FLOOR(currentcoin/1000000) > 5 THEN '0005_0010' ".
								"	WHEN FLOOR(currentcoin/1000000) > 1 THEN '0001_0005' ".
								"	ELSE '0000_0001' END) AS std_coin ".	
								"	FROM `tbl_user_playstat_daily` ".
								"	WHERE '$yester_day' <= today AND today <= '$yester_day' AND h_moneyin >= 10000000 ".
								"	ORDER BY std_coin ".
								") t1 ".
								"LEFT JOIN	".
								"( ".
								"	SELECT today, useridx, ".
								"	(CASE WHEN FLOOR(currentcoin/1000000) > 1000 THEN '1000_1000+' ".
								"	WHEN FLOOR(currentcoin/1000000) > 500 THEN '0500_1000' ".
								"	WHEN FLOOR(currentcoin/1000000) > 100 THEN '0100_0500' ".
								"	WHEN FLOOR(currentcoin/1000000) > 50 THEN '0050_0100' ".
								"	WHEN FLOOR(currentcoin/1000000) > 20 THEN '0020_0050' ".
								"	WHEN FLOOR(currentcoin/1000000) > 10 THEN '0010_0020' ".
								"	WHEN FLOOR(currentcoin/1000000) > 5 THEN '0005_0010' ".
								"	WHEN FLOOR(currentcoin/1000000) > 1 THEN '0001_0005' ".
								"	ELSE '0000_0001' END) AS std_coin ".	
								"	FROM `tbl_user_playstat_daily` ".
								"	WHERE '$today' <= today AND today <= '$today' AND h_moneyin >= 10000000 ".
								"	ORDER BY std_coin ".
								") t2 ON t1.useridx = t2.useridx AND t1.std_coin = t2.std_coin GROUP BY t1.std_coin";
						$highroller_currentcoin_yesterday_rate = $db_other->gettotallist($sql);
						
						for($k = 0; $k < sizeof($highroller_currentcoin_yesterday_rate); $k++)
						{
							$std_coin_yesterday_std_coin = $highroller_currentcoin_yesterday_rate[$k]["std_coin"];
							$std_coin_yesterday_std_yesterday_rate = $highroller_currentcoin_yesterday_rate[$k]["std_yesterday_rate"];
							
							$std_coin_yesterday_rate_array[$std_coin_yesterday_std_coin] = $std_coin_yesterday_std_yesterday_rate;
						}						
						
?>				
						<td class="tdc point_title" rowspan="<?=sizeof($highroller_currentcoin_rowdata)?>"><?= $today ?></td>
					
<?
					}
					else
					{
						
?>
						<td class="tdc point_title" rowspan="1"><?= $today ?></td>
<?					
					} 
				}
?>	
	
					<td class="tdc"><?= $std_currentcoin_str ?></td>
					<td class="tdc"><?= number_format($usercnt)?>(<?=$std_coin_yesterday_rate_array[$std_coin] ?> %)</td>
					<td class="tdc"><?= number_format($currentcoin) ?></td>
					<td class="tdc"><?= number_format($h_moneyin) ?></td>
					<td class="tdc"><?= number_format($h_moneyout) ?></td>
					<td class="tdc"><?= $h_winrate ?>%</td>
					<td class="tdc"><?= number_format($h_playtime) ?></td>
					<td class="tdc"><?= number_format($h_playcount) ?></td>
					<td class="tdc"><?= number_format($purchasecount) ?></td>
					<td class="tdc">$<?= number_format($purchaseamount) ?></td>
					<td class="tdc"><?= number_format($purchasecoin) ?></td>
					<td class="tdc"><?= number_format($famelevel) ?></td>
					<td class="tdc"><?= number_format($jackpot) ?></td>
					<td class="tdc"><?= number_format($usercnt_rate,2) ?>%</td>
				</tr>
<?
			}
			
			for($i=0; $i<sizeof($total_highroller_currentcoin_data); $i++)
			{
				$std_coin = $total_highroller_currentcoin_data[$i]["std_coin"];
				$usercnt = $total_highroller_currentcoin_data[$i]["usercnt"];
				$currentcoin = $total_highroller_currentcoin_data[$i]["currentcoin"];
				$h_moneyin = $total_highroller_currentcoin_data[$i]["h_moneyin"];
				$h_moneyout = $total_highroller_currentcoin_data[$i]["h_moneyout"];
				$h_winrate = $total_highroller_currentcoin_data[$i]["h_winrate"];
				$h_playtime = $total_highroller_currentcoin_data[$i]["h_playtime"]/$usercnt;
				$h_playcount = $total_highroller_currentcoin_data[$i]["h_playcount"]/$usercnt;
				$purchasecount = $total_highroller_currentcoin_data[$i]["purchasecount"];
				$purchaseamount = $total_highroller_currentcoin_data[$i]["purchaseamount"]/10;
				$purchasecoin = $total_highroller_currentcoin_data[$i]["purchasecoin"];
				$famelevel = $total_highroller_currentcoin_data[$i]["famelevel"]/$usercnt;
				$jackpot = $total_highroller_currentcoin_data[$i]["jackpot"];
				$currentcoin_usercnt = $total_currentcoin_data_array[$std_coin];
				
				$usercnt_rate = ($usercnt/$currentcoin_usercnt)*100;

				$std_currentcoin_str = "";
				
				if($std_coin == "0000_0001")
					$std_currentcoin_str = "0 ~ 백만";
				else if($std_coin == "0001_0005")
					$std_currentcoin_str = "백만 ~ 오백만";
				else if($std_coin == "0005_0010")
					$std_currentcoin_str = "오백만 ~ 천만";
				else if($std_coin == "0010_0020")
					$std_currentcoin_str = "천만 ~ 이천만";
				else if($std_coin == "0020_0050")
					$std_currentcoin_str = "이천만 ~ 오천만";
				else if($std_coin == "0050_0100")
					$std_currentcoin_str = "오천만 ~ 일억";
				else if($std_coin == "0100_0500")
					$std_currentcoin_str = "일억 ~ 오억";
				else if($std_coin == "0500_1000")
					$std_currentcoin_str = "오억 ~ 십억";
				else if($std_coin == "1000_1000+")
					$std_currentcoin_str = "십억 이상";
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($i == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($total_highroller_currentcoin_data)?>" valign="center">Total</td>
<?
				}
?>
					<td class="tdc"><?= $std_currentcoin_str ?></td>
					<td class="tdc"><?= number_format($usercnt) ?></td>
					<td class="tdc"><?= number_format($currentcoin) ?></td>
					<td class="tdc"><?= number_format($h_moneyin) ?></td>
					<td class="tdc"><?= number_format($h_moneyout) ?></td>
					<td class="tdc"><?= $h_winrate ?>%</td>
					<td class="tdc"><?= number_format($h_playtime) ?></td>
					<td class="tdc"><?= number_format($h_playcount) ?></td>
					<td class="tdc"><?= number_format($purchasecount) ?></td>
					<td class="tdc">$<?= number_format($purchaseamount) ?></td>
					<td class="tdc"><?= number_format($purchasecoin) ?></td>
					<td class="tdc"><?= number_format($famelevel) ?></td>
					<td class="tdc"><?= number_format($jackpot) ?></td>
					<td class="tdc"><?= number_format($usercnt_rate,2) ?>%</td>
				</tr>

<?
			}
?>
			</tbody>
            </table>
     	</div>        
     </form>	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_other->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>