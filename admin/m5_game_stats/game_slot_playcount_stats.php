<?
    $top_menu = "game_stats";
    $sub_menu = "game_slot_playcount_stats";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $db_main2 = new CDatabase_Main2();
    
    $os_type = ($_GET["os_type"] == "") ? "0" :$_GET["os_type"];
    $term = ($_GET["term"] == "") ? "10" : $_GET["term"];    
    $alllist = $_GET["alllist"];
    $totallist = $_GET["totallist"];
    $lastdate = $_GET["lastdate"];
    $issearch = $_GET["issearch"];
    $total_mode = ($_GET["total_mode"] == "") ? "0" :$_GET["total_mode"];
    $search_slot_chk = ($_GET["search_slot_chk"] == "")?"1" : $_GET["search_slot_chk"];
    $exist_slot_value = 1;    
    
    $tail = " WHERE 1=1 ";
    
    if($os_type == "0")
    {
    	$table = "tbl_game_cash_stats_daily";
    	$os_txt = "Web";
    }    
	else if($os_type == "1")
	{
		$tail .= " AND ios = 1 ";
		$table = "tbl_game_cash_stats_ios_daily";
		$os_txt = "IOS";
	}
	else if($os_type == "2")
	{	
		$tail .= " AND android = 1 ";
		$table = "tbl_game_cash_stats_android_daily";
		$os_txt = "Android";
	}
	else if($os_type == "3")
	{
		$tail .= " AND amazon = 1 ";
		$table = "tbl_game_cash_stats_amazon_daily";
		$os_txt = "Amazon";
	}    
    
    $sql = "SELECT slottype FROM tbl_slot_list $tail ORDER BY slottype DESC LIMIT 1";
    $recently_slotlist = $db_main2->getarray($sql);
    
    $slottype_value = ($_GET["slotlist"] == "") ? $recently_slotlist : $_GET["slotlist"];
    
    if($issearch == 1)
    	$search_slot_chk = 0;
    
    if($total_mode == 0)
    	$mode_name = "전체";
    else if($total_mode == 1)
    	$mode_name = "일반";
    else if($total_mode == 2)
    	$mode_name = "하이롤러(싱글X)";
    else if($total_mode == 3)
    	$mode_name = "하이롤러(싱글 포함)";
    else if($total_mode == 4)
    	$mode_name = "싱글모드";
    
    $pagename = "game_slot_playcount_stats.php";
    
    check_number($term);
    
    if(is_array($slottype_value))
    {
    	$sel_slotlist = implode(",", $slottype_value);
    }    
    
    if ($term != "10" && $term != "30" && $term != "60" && $term != "360" && $term != "720" && $term != "1440" && $term != "10080" && $term != "43200")
        error_back("잘못된 접근입니다.");
    
    if ($lastdate == "")
    {
        $end_date = date("Y-m-d", time());
        $end_time = date("H:i:s", time());    
    }
    else 
    {
        $end_date = $lastdate;
        $end_time = "23:59:59";    
    }
   
    if ($term == "10") // 최근 12시간
    {
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time))-12,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time))-12,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));      
    }
    else if ($term == "30") // 최근 1일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-1),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-1),date("Y",strtotime($end_date))));
    }
    else if ($term == "60") // 최근 2일 
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-2),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-2),date("Y",strtotime($end_date))));
    }
    else if ($term == "360") // 최근 7일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-7),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-7),date("Y",strtotime($end_date))));
    }
    else if ($term == "720") // 최근 10일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-10),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-10),date("Y",strtotime($end_date))));
    }
    else if ($term == "1440") // 최근 30일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-30),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-30),date("Y",strtotime($end_date))));
    }
    else if ($term == "10080") // 최근 60일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-60),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-60),date("Y",strtotime($end_date))));
    }
    else if ($term == "43200") // 최근 12개월
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),(date("m", strtotime($end_date))-12),(date("d", strtotime($end_date))),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),(date("m", strtotime($end_date))-12),(date("d", strtotime($end_date))),date("Y",strtotime($end_date))));
    }   
          	
	$sql = "SELECT slottype,slotname FROM tbl_slot_list $tail ";
	$total_slotlist = $db_main2->gettotallist($sql);
        
    if($alllist == "all")
    {
    	$sql = "SELECT slottype,slotname FROM tbl_slot_list $tail ";
    	$slotlist = $db_main2->gettotallist($sql);
    }
    else if($alllist == ""  && $totallist == "total")
    {
    	$sql = "SELECT 999999 AS slottype, 'total' AS slotname FROM tbl_slot_list  WHERE 1=1 LIMIT 1";
    	$slotlist = $db_main2->gettotallist($sql);
    }
    else if($alllist == ""  && $sel_slotlist != "")
    {    	
		$sql = "SELECT slottype,slotname FROM tbl_slot_list $tail AND slottype IN ($sel_slotlist)";
		$slotlist = $db_main2->gettotallist($sql);
    }
    else
    {
    	$slotlist = array();
    	$exist_slot_value = 0;
    	$search_slot_chk = 1;
    }

    $list = array();
	
    
    if($totallist == "total")
    {
    	for ($i=0; $i<sizeof($slotlist); $i++)
    	{
	    	$slottype = $slotlist[$i]["slottype"];
	    	$slotname = $slotlist[$i]["slotname"];
	    	
	    	if($total_mode == 0)
	    	{
	    		$sql = "SELECT IFNULL(SUM(playcount),0) AS playcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
	    				"FROM $table ".
	    				"WHERE mode IN (0,31) AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
	    				"GROUP BY minute ";		
	    		
	    	}
	    	else if($total_mode == 1)
	    	{
	    		$sql = "SELECT IFNULL(SUM(playcount),0) AS playcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
	    				"FROM $table ".
	    				"WHERE mode IN (0,31) AND betlevel BETWEEN 0 AND 9 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
	    				"GROUP BY minute ";
	    	}
	    	else if($total_mode == 2)
	    	{
	    		$sql = "SELECT IFNULL(SUM(playcount),0) AS playcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
	    				"FROM $table ".
	    				"WHERE mode IN (0,31) AND betlevel BETWEEN 10 AND 19 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
	    				"GROUP BY minute ";
	    	}
	    	else if($total_mode == 3)
	    	{
	    		$sql = "SELECT IFNULL(SUM(playcount),0) AS playcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
	    				"FROM $table ".
	    				"WHERE mode IN (0,31) AND betlevel BETWEEN 10 AND 119 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
	    				"GROUP BY minute ";
	    	}
	    	else if($total_mode == 4)
	    	{
	    		$sql = "SELECT IFNULL(SUM(playcount),0) AS playcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
	    				"FROM $table ".
	    				"WHERE mode IN (0,31) AND betlevel BETWEEN 110 AND 119 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
	    				"GROUP BY minute ";
	    	}
    	
    		$list[$slottype] = $db_main2->gettotallist($sql);
    	}
    }
    else 
	{
		for ($i=0; $i<sizeof($slotlist); $i++)
		{
			$slottype = $slotlist[$i]["slottype"];
			$slotname = $slotlist[$i]["slotname"];
	
			if($total_mode == 0)
			{
		    	$sql = "SELECT IFNULL(SUM(playcount),0) AS playcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
		        	    "FROM $table ".
		            	"WHERE mode IN (0,31) AND slottype=$slottype AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
		            	"GROUP BY minute ";
			}
			else if($total_mode == 1)
			{
				$sql = "SELECT IFNULL(SUM(playcount),0) AS playcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
						"FROM $table ".
						"WHERE mode IN (0,31) AND betlevel BETWEEN 0 AND 9 AND slottype=$slottype AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
						"GROUP BY minute ";
			}
			else if($total_mode == 2)
			{
				$sql = "SELECT IFNULL(SUM(playcount),0) AS playcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
						"FROM $table ".
						"WHERE mode IN (0,31) AND betlevel BETWEEN 10 AND 19 AND slottype=$slottype AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
						"GROUP BY minute ";
			}
			else if($total_mode == 3)
			{
				$sql = "SELECT IFNULL(SUM(playcount),0) AS playcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
						"FROM $table ".
						"WHERE mode IN (0,31) AND betlevel BETWEEN 10 AND 119 AND slottype=$slottype AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
						"GROUP BY minute ";
			}
			else if($total_mode == 4)
			{
				$sql = "SELECT IFNULL(SUM(playcount),0) AS playcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
						"FROM $table ".
						"WHERE mode IN (0,31) AND betlevel BETWEEN 110 AND 119 AND slottype=$slottype AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
						"GROUP BY minute ";
			}
		    
		    $list[$slottype] = $db_main2->gettotallist($sql);
		}
	}
            	
    $db_main2->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

	$(document).ready(function()
	{
		if(<?=$search_slot_chk?> == 0 && <?=$exist_slot_value?> == 1)
			document.getElementById("tbl_slotlist").style.display = "none";	
		else
			document.getElementById("tbl_slotlist").style.display = "";	
	});

    google.load("visualization", "1", {packages:["corechart"]});
      
    function drawChart() 
    {
        var options = {
            title:'',            
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize:12,
            chartArea:{left:80,top:25,width:1000,height:100}
    };
        
<?    	
	for ($k=0; $k<sizeof($slotlist); $k++)
	{
		$slottype = $slotlist[$k]["slottype"];
		$slotname = $slotlist[$k]["slotname"];
?>    	
        var datatable<?= $slottype ?> = new google.visualization.DataTable();
        
        datatable<?= $slottype ?>.addColumn('string', '시간');
        datatable<?= $slottype ?>.addColumn('number', 'playcount');
        datatable<?= $slottype ?>.addRows([
<?
    $start = floor(mktime(date("H",strtotime($start_time)), date("i",strtotime($start_time)), date("s", strtotime($start_time)), date("m",strtotime($start_date)), date("d", strtotime($start_date)), date("Y", strtotime($start_date)))/($term*60));
    $end = floor(mktime(date("H",strtotime($end_time)), date("i",strtotime($end_time)), date("s", strtotime($end_time)), date("m",strtotime($end_date)), date("d", strtotime($end_date)), date("Y", strtotime($end_date)))/($term*60)) + 1;
    
    for ($i=$start; $i<$end; $i=$i+1)
    {  
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
         
        if ($term > 10080)
            echo("['".substr($list_date, 0, 7)."'");  
        else if ($term > 720)
            echo("['$list_date'");        
        else
            echo("['".make_time_format($temp_minute)."'");
        
        $print = false;        
        
        for ($j=0; $j<sizeof($list[$slottype]); $j++)
        {
            if ($list[$slottype][$j]["minute"] == $i)
            { 
                echo(",{v:".$list[$slottype][$j]["playcount"].",f:'".make_price_format($list[$slottype][$j]["playcount"])."'}");
                $print = true;      
                break;
            }           
        }
        
        if (!$print)
           echo(",0");
        
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");
    }   
?>          
        ]);

        var chart = new google.visualization.LineChart(document.getElementById('chart_div<?= $slottype ?>'));
        chart.draw(datatable<?= $slottype ?>, options);
<?
	}
?>    
    }
  
    google.setOnLoadCallback(drawChart);

    function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
    
    function change_term(term)
    {
        var search_form = document.search_form;        
        var minute10 = document.getElementById("term_10minute");
        var minute30 = document.getElementById("term_30minute");
        var minute60 = document.getElementById("term_60minute");
        var minute360 = document.getElementById("term_360minute");
        var minute720 = document.getElementById("term_720minute");
        var minute1440 = document.getElementById("term_1440minute");
        var minute10080 = document.getElementById("term_10080minute");
        var minute43200 = document.getElementById("term_43200minute");
        
        search_form.term.value = term;
        
        if (term == "10")
        {
            minute10.className="btn_schedule_select";
            minute30.className="btn_schedule";
            minute60.className="btn_schedule";
            minute360.className="btn_schedule";
            minute720.className="btn_schedule";
            minute1440.className="btn_schedule";
            minute10080.className="btn_schedule"; 
            minute43200.className="btn_schedule";    
        }
        else if (term == "30")
        {
            minute10.className="btn_schedule";
            minute30.className="btn_schedule_select";
            minute60.className="btn_schedule";
            minute360.className="btn_schedule";
            minute720.className="btn_schedule";
            minute1440.className="btn_schedule";
            minute10080.className="btn_schedule"; 
            minute43200.className="btn_schedule";  
        }
        else if (term == "60")
        {
            minute10.className="btn_schedule";
            minute30.className="btn_schedule";
            minute60.className="btn_schedule_select";
            minute360.className="btn_schedule";
            minute720.className="btn_schedule";
            minute1440.className="btn_schedule";
            minute10080.className="btn_schedule"; 
            minute43200.className="btn_schedule";   
        }
        else if (term == "360")
        {
            minute10.className="btn_schedule";
            minute30.className="btn_schedule";
            minute60.className="btn_schedule";
            minute360.className="btn_schedule_select";
            minute720.className="btn_schedule";
            minute1440.className="btn_schedule";
            minute10080.className="btn_schedule"; 
            minute43200.className="btn_schedule";       
        }
        else if (term == "720")
        {
            minute10.className="btn_schedule";
            minute30.className="btn_schedule";
            minute60.className="btn_schedule";
            minute360.className="btn_schedule";
            minute720.className="btn_schedule_select";
            minute1440.className="btn_schedule";
            minute10080.className="btn_schedule"; 
            minute43200.className="btn_schedule";    
        }
        else if (term == "1440")
        {
            minute10.className="btn_schedule";
            minute30.className="btn_schedule";
            minute60.className="btn_schedule";
            minute360.className="btn_schedule";
            minute720.className="btn_schedule";
            minute1440.className="btn_schedule_select";
            minute10080.className="btn_schedule"; 
            minute43200.className="btn_schedule";    
        }
        else if (term == "10080")
        {
            minute10.className="btn_schedule";
            minute30.className="btn_schedule";
            minute60.className="btn_schedule";
            minute360.className="btn_schedule";
            minute720.className="btn_schedule";
            minute1440.className="btn_schedule";
            minute10080.className="btn_schedule_select"; 
            minute43200.className="btn_schedule"; 
        }
        else if (term == "43200")
        {
            minute10.className="btn_schedule";
            minute30.className="btn_schedule";
            minute60.className="btn_schedule";
            minute360.className="btn_schedule";
            minute720.className="btn_schedule";
            minute1440.className="btn_schedule";
            minute10080.className="btn_schedule"; 
            minute43200.className="btn_schedule_select";     
        }
        
        search_form.submit();
    }  
    
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    
    function check_sleeptime()
    {
        setTimeout("window.location.reload(false)",60000);
    }

    function change_search_slotlist(ischecked)
    {
        if(ischecked)
        {
        	document.getElementById("search_slot_chk").value = "1";
        	document.getElementById("tbl_slotlist").style.display = "";
        }
        else
        {
        	document.getElementById("search_slot_chk").value = "0";
        	document.getElementById("tbl_slotlist").style.display = "none";        	
        }        
    }	

    $(function() {
        $("#lastdate").datepicker({ });
    });        
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
            	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
					<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
					<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
					<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
					<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
				</span>
                <!-- title_warp -->
                <div class="title_wrap">
                    <div class="title"><?= $top_menu_txt ?> &gt; 게임 활동 추이(<?=$os_txt?>) - 게임횟수 추이 (슬롯)(<?=$mode_name?>)</div>
                    <input type="hidden" name="issearch" id="issearch" value="1"/>
                    <input type="hidden" name="term" id="term" value="<?= $term ?>" />
                    <input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />        		
	        		
                    </br></br>
                    <div class="search_box">
                    	Mode&nbsp; :&nbsp; 
                    	<select name="total_mode" id="total_mode">										
							<option value="0" <?= ($total_mode=="0") ? "selected" : "" ?>>전체</option>
							<option value="1" <?= ($total_mode=="1") ? "selected" : "" ?>>일반</option>                       
							<option value="2" <?= ($total_mode=="2") ? "selected" : "" ?>>하이롤러(싱글X)</option>
							<option value="3" <?= ($total_mode=="3") ? "selected" : "" ?>>하이롤러(싱글포함)</option>
							<option value="4" <?= ($total_mode=="4") ? "selected" : "" ?>>싱글모드</option>
						</select>&nbsp;&nbsp;&nbsp;
                    	<input type="checkbox"  name="search_slot_chk"  id="search_slot_chk" onchange="change_search_slotlist(this.checked);" <?= ($search_slot_chk == "1") ? "checked" : "" ?>/><font style="font:bold 16px 'Malgun Gothic'; color:#5ab2da;">Slot List 보기</font>&nbsp;&nbsp;
                        <input type="button" class="<?= ($term == "10") ? "btn_schedule_select" : "btn_schedule" ?>" value="10분" id="term_10minute" onclick="change_term(10)" />
                        <input type="button" class="<?= ($term == "30") ? "btn_schedule_select" : "btn_schedule" ?>" value="30분" id="term_30minute" onclick="change_term(30)" />
                        <input type="button" class="<?= ($term == "60") ? "btn_schedule_select" : "btn_schedule" ?>" value="1시간" id="term_60minute" onclick="change_term(60)" />
                        <input type="button" class="<?= ($term == "360") ? "btn_schedule_select" : "btn_schedule" ?>" value="6시간" id="term_360minute" onclick="change_term(360)" />
                        <input type="button" class="<?= ($term == "720") ? "btn_schedule_select" : "btn_schedule" ?>" value="12시간" id="term_720minute" onclick="change_term(720)" />
                        <input type="button" class="<?= ($term == "1440") ? "btn_schedule_select" : "btn_schedule" ?>" value="1일" id="term_1440minute" onclick="change_term(1440)" />
                        <input type="button" class="<?= ($term == "10080") ? "btn_schedule_select" : "btn_schedule" ?>" value="7일" id="term_10080minute" onclick="change_term(10080)" />
                        <input type="button" class="<?= ($term == "43200") ? "btn_schedule_select" : "btn_schedule" ?>" value="30일" id="term_43200minute" onclick="change_term(43200)" />                    
                        <input type="input" class="search_text" id="lastdate" name="lastdate" style="width:65px" value="<?= $lastdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
                        <input type="button" class="btn_search" value="검색" onclick="search()" />
                    </div>  
				</div> 
				<div style="padding-top:10px; margin-bottom:10px">
					<table id="tbl_slotlist">
						<input type="checkbox"  name="alllist"  value="all"  <?= ($alllist == "all") ? "checked" : "" ?>/>ALL</br>
						<input type="checkbox"  name="totallist"  value="total"  <?= ($totallist == "total") ? "checked" : "" ?>/>Total</br>
						<tr>				
				
<?	
	$count = 1;
	for ($k=0; $k<sizeof($total_slotlist); $k++)
	{			
		$slotname = $total_slotlist[$k]["slotname"];
		$slottype = $total_slotlist[$k]["slottype"];
?>
						
							<td><input type="checkbox" name="slotlist[]"  value="<?=$slottype?>"/><?= $slotname ?></td>
<?
		if($count%5 == 0)
		{
?>
				
						</tr>
<?		
		}
		$count++;
	}
?>	
					</table>
<script>
						var sel_slotlist_array = new Array();						
						var sel_slotlist = "<?=$sel_slotlist?>";
						
						sel_slotlist_array = sel_slotlist.split(',');
														
						$("input:checkbox[name='slotlist[]']").each(function(index){
						    if(sel_slotlist_array.indexOf($(this).val()) > -1){
						        $(this).attr("checked", true);						        
						    }
						});

						$("input:checkbox[name='alllist']").click(function(){
							if($("input:checkbox[name='alllist']").is(":checked"))
							{
								$("input:checkbox[name='slotlist[]']").attr("checked", "checked");
								$("input:checkbox[name='totallist']").attr("checked", false);
							}
							else
							{	
								$("input:checkbox[name='slotlist[]']").attr("checked", "");
							}								
						});

						$("input:checkbox[name='totallist']").click(function(){
							if($("input:checkbox[name='totallist']").is(":checked"))
							{
								$("input:checkbox[name='slotlist[]']").attr("checked", "checked");
								$("input:checkbox[name='alllist']").attr("checked", false);
							}
							else
							{	
								$("input:checkbox[name='slotlist[]']").attr("checked", "");
							}								
						});

						$("input:checkbox[name='slotlist[]']").click(function(){
							if($("input:checkbox[name='slotlist[]']").is(":unchecked"))
							{
								$("input:checkbox[name='alllist']").attr("checked", false);
								$("input:checkbox[name='totallist']").attr("checked", false);
							}							
						});
</script>
				</div>

<?    	
	for ($k=0; $k<sizeof($slotlist); $k++)
	{
		$slottype = $slotlist[$k]["slottype"];
		$slotname = $slotlist[$k]["slotname"];
?>    	
                <div style="font-weight:bold;padding-top:10px">[<?= $slotname ?> 활동 추이]</div>
                <div id="chart_div<?= $slottype ?>" style="height:200px; min-width:500px;"></div>
<?
	}
?>
            </form>
            
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>