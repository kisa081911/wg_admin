<?
    $top_menu = "game_stats";
    $sub_menu = "game_coupon_stat";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $pagename = "game_coupon_stat.php";
    
    $platform = $_GET["platform"];
    $isrecentbuy = $_GET["isrecentbuy"];
    $search_member_level = $_GET["search_member_level"];
    
    $start_date = $_GET["startdate"];
    $end_date = $_GET["enddate"];
    
    $today = date("Y-m-d");
    $before_day = get_past_date($today,15,"d");
    $start_date = ($start_date == "") ? $before_day : $start_date;
    $end_date = ($end_date == "") ? $today : $end_date;
    
    $db_other = new CDatabase_Other();

    $tail = "WHERE today BETWEEN '$start_date' AND '$end_date' AND member_level NOT IN (0,1,2,3) ";
    $group_by = "GROUP BY today";
    $order_by = "ORDER BY t1.today DESC";
    
    if($platform != "")
    	$tail .= "AND platform = $platform ";
    
    if($isrecentbuy != "")
    	$tail .= "AND isrecentbuy = $isrecentbuy ";
    
    if($search_member_level != "")
    {
    	$group_by .= ", member_level";
    	$order_by .= ", member_level ASC";
    }
    
	$sql = "SELECT * ".
			"FROM (".
			"	SELECT today, platform, isrecentbuy, member_level, ".
			"		(SUM(total_coupon) + SUM(use_coupon)) AS all_coupon, (SUM(total_usercount) + SUM(use_usercount)) AS all_usercount, (SUM(total_more) + SUM(use_more)) AS all_more, ".
			"		SUM(total_coupon) AS total_coupon, SUM(total_usercount) AS total_usercount, SUM(total_more) AS total_more, SUM(total_expireday) AS total_expireday, ".
			"		SUM(use_coupon) AS use_coupon, SUM(use_usercount) AS use_usercount, SUM(use_more) AS use_more, ".
			"		SUM(issue_coupon) AS issue_coupon, SUM(issue_usercount) AS issue_usercount, SUM(issue_more) AS issue_more ".
			"	FROM tbl_coupon_stat_daily ".
			"	$tail $group_by ".
			") t1 JOIN (".
			"		SELECT today, COUNT(DISTINCT member_level) AS rowcount".
			"		FROM tbl_coupon_stat_daily".
			"		GROUP BY today".
			") t2 ON t1.today = t2.today ".
			"$order_by";
	
	$coupon_stat_list = $db_other->gettotallist($sql);
	
 	$sql = "SELECT COUNT(DISTINCT member_level) AS rowcount FROM tbl_coupon_stat_daily $tail GROUP BY today ORDER BY today DESC";
 	$rowspan_list = $db_other->gettotallist($sql);
	
	$db_other->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    
    $(function() {
        $("#startdate").datepicker({ });
    });

    $(function() {
        $("#enddate").datepicker({ });
    });
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
                <!-- title_warp -->
                <div class="title_wrap">
                    <div class="title"><?= $top_menu_txt ?> &gt; 쿠폰 현황</div>
                </div>
                <!-- //title_warp -->
                
                <div class="detail_search_wrap">
					<!-- <span class="search_lbl">플랫폼</span>
					<select name="platform" id="platform">
						<option value="" <?= ($platform == "") ? "selected" : "" ?>>전체</option>
						<option value="0" <?= ($platform == "0") ? "selected" : "" ?>>Web</option>
						<option value="1" <?= ($platform == "1") ? "selected" : "" ?>>iOS</option>
						<option value="2" <?= ($platform == "2") ? "selected" : "" ?>>Android</option>
						<option value="3" <?= ($platform == "3") ? "selected" : "" ?>>Amazon</option>
					</select>  -->
			
					&nbsp;
					&nbsp;
					
					<span class="search_lbl">최근 결제 여부</span>
					<select name="isrecentbuy" id="isrecentbuy">
						<option value="" <?= ($isrecentbuy == "") ? "selected" : "" ?>>전체</option>
						<option value="1" <?= ($isrecentbuy == "1") ? "selected" : "" ?>>28일이내 결제</option>
					</select>
					
					&nbsp;
					&nbsp;
					
					<span class="search_lbl">등급별</span>
					<select name="search_member_level" id="search_member_level">
						<option value="" <?= ($search_member_level == "") ? "selected" : "" ?>>전체</option>
						<option value="1" <?= ($search_member_level == "1") ? "selected" : "" ?>>등급별</option>
					</select>
					
					&nbsp;
					&nbsp;
					
					<span class="search_lbl">기간</span>
					<input type="text" class="search_text" id="startdate" name="startdate" style="width:65px" value="<?= $start_date ?>" 
						onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
		            <input type="text" class="search_text" id="enddate" name="enddate" style="width:65px" value="<?= $end_date ?>" 
		            	onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
					
					<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
				</div>
                
                <div class="h2_title pt20"></div>
                <table class="tbl_list_basic1">
                <colgroup>
					<col width="70">
					
					<col width="">
					<col width="">
					<col width="70">
					<col width="80">
					
		            <col width="">
		            <col width="">
		            <col width="70">
		            <col width="75">
		            
		            <col width="">
		            <col width="">
		            <col width="70">
		            
		            <col width="">
		            <col width="">
		            <col width="70">
				</colgroup>
		        <thead>
		        	<tr>
		        		<th rowspan="2">일자</th>
<?
	if($search_member_level != "")
	{
?>
		        		<th rowspan="2">등급</th>
<?
	}
?>
						<th colspan="4">전체 쿠폰</th>
		            	<th colspan="4">잔여 쿠폰</th>
		               	<th colspan="3">사용한 쿠폰</th>
		             	<th colspan="3">발행한 쿠폰</th>
		         	</tr>
		        	<tr>
		        		<th>쿠폰수</th>
		               	<th>유저수</th>
		             	<th>평균 more</th>
		             	<th>쿠폰 소진율</th>
		            	<th>쿠폰수</th>
		               	<th>유저수</th>
		             	<th>평균 more</th>
		               	<th>평균만료일</th>
		               	<th>쿠폰수</th>
		               	<th>유저수</th>
		             	<th>평균 more</th>
		             	<th>쿠폰수</th>
		               	<th>유저수</th>
		             	<th>평균 more</th>
		         	</tr>
		        </thead>
                <tbody>
<?
	$sum_all_coupon = 0;
	$sum_all_usercount = 0;
 	$sum_all_more = 0;

	$sum_total_coupon = 0;
	$sum_total_usercount = 0;
 	$sum_total_more = 0;
 	$sum_total_expireday = 0;
 	
 	$sum_use_coupon = 0;
 	$sum_use_usercount = 0;
 	$sum_use_more = 0;
 	
 	$sum_issue_coupon = 0;
 	$sum_issue_usercount = 0;
 	$sum_issue_more = 0;
 	
 	$row_index = 0;
	$rowspan_index = 0;
	
    for ($i=0; $i<sizeof($coupon_stat_list); $i++)
    {
    	$today = $coupon_stat_list[$i]["today"];
    	$member_level = $coupon_stat_list[$i]["member_level"];
    	
    	$all_coupon = $coupon_stat_list[$i]["all_coupon"];
    	$all_usercount = $coupon_stat_list[$i]["all_usercount"];
    	$all_more = $coupon_stat_list[$i]["all_more"];
    	$all_avg_more = ($all_coupon == 0) ? 0 : round($all_more / $all_coupon, 1);
    	
    	$total_coupon = $coupon_stat_list[$i]["total_coupon"];
    	$total_usercount = $coupon_stat_list[$i]["total_usercount"];
    	$total_more = $coupon_stat_list[$i]["total_more"];
    	$total_avg_more = ($total_coupon == 0) ? 0 : round($total_more / $total_coupon, 1);
    	$total_expireday = $coupon_stat_list[$i]["total_expireday"];
    	$total_avg_expireday = ($total_coupon == 0) ? 0 : round($total_expireday / $total_coupon, 1);
    	
    	$use_coupon = $coupon_stat_list[$i]["use_coupon"];
    	$use_usercount = $coupon_stat_list[$i]["use_usercount"];
    	$use_more = $coupon_stat_list[$i]["use_more"];
    	$use_avg_more = ($use_more == 0) ? 0 : round($use_more / $use_coupon, 1);
    	
    	$issue_coupon = $coupon_stat_list[$i]["issue_coupon"];
    	$issue_usercount = $coupon_stat_list[$i]["issue_usercount"];
    	$issue_more = $coupon_stat_list[$i]["issue_more"];
    	$issue_avg_more = ($issue_more == 0) ? 0 : round($issue_more / $issue_coupon, 1);
    	
    	$all_coupon_runout_rate = ($use_coupon / $total_coupon) * 100;
  
    	$sum_all_coupon += $all_coupon;
    	$sum_all_usercount += $all_usercount;
    	$sum_all_more += $all_more;
    	
    	$sum_total_coupon += $total_coupon;
		$sum_total_usercount += $total_usercount;
	 	$sum_total_more += $total_more;
	 	$sum_total_expireday += $total_expireday;
	 	
	 	$sum_use_coupon += $use_coupon;
	 	$sum_use_usercount += $use_usercount;
	 	$sum_use_more += $use_more;
	 	
	 	$sum_issue_coupon += $issue_coupon;
	 	$sum_issue_usercount += $issue_usercount;
	 	$sum_issue_more += $issue_more;
?>
					<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
		if($search_member_level != "")
		{
			if($row_index == 0)
			{
	    		$rowspan = $rowspan_list[$rowspan_index]["rowcount"];
?>
    					<td rowspan="<?= $rowspan ?>" class="tdc point_title"><?= $today ?></td>
    			
<?
				$row_index++;
			}
	    	else if($row_index == ($rowspan - 1))
	    	{
	    		$row_index = 0;
	    		$rowspan_index++;
	    		$rowspan = $rowspan_list[$rowspan_index]["rowcount"];
    		}
    		else
    			$row_index++;
		}
		else 
		{
?>
			<td class="tdc point_title"><?= $today ?></td>
<?
		}
		
		if($search_member_level != "")
		{
?>
                        <td class="tdc">VIP Level <?= $member_level ?></td>
<?
		}
?>
						<td class="tdc"><?= number_format($all_coupon) ?></td>
						<td class="tdc"><?= number_format($all_usercount) ?></td>
						<td class="tdc"><?= number_format($all_avg_more, 1) ?>%</td>
						<td class="tdc" style="border-right: 1px dotted #dbdbdb;"><?= number_format($all_coupon_runout_rate, 2) ?>%</td>
                        <td class="tdc"><?= number_format($total_coupon) ?></td>
                        <td class="tdc"><?= number_format($total_usercount) ?></td>
                        <td class="tdc"><?= number_format($total_avg_more, 1) ?>%</td>
                        <td class="tdc" style="border-right: 1px dotted #dbdbdb;"><?= number_format($total_avg_expireday, 1) ?>일</td>
                        <td class="tdc"><?= number_format($use_coupon) ?></td>
                        <td class="tdc"><?= number_format($use_usercount) ?></td>
                        <td class="tdc" style="border-right: 1px dotted #dbdbdb;"><?= number_format($use_avg_more, 1) ?>%</td>
                        <td class="tdc"><?= number_format($issue_coupon) ?></td>
                        <td class="tdc"><?= number_format($issue_usercount) ?></td>
                        <td class="tdc"><?= number_format($issue_avg_more, 1) ?>%</td>
                    </tr>
<?
    }
    
    $sum_all_coupon_runout_rate = ($sum_use_coupon / $sum_total_coupon) * 100;
?>
					<tr>
<?
	if($search_member_level != "")
	{
?>
						<td colspan="2" class="tdc point_title">Total</td>
<?
	}
	else 
	{
?>
						<td class="tdc point_title">Total</td>
<?
	}
?>
<?
	if($search_member_level != "")
	{
?>
						<td class="tdc point""><?= number_format($sum_all_coupon) ?></td>
<?						
	}
	else 
	{
?>
						<td class="tdc point"><?= number_format($sum_all_coupon) ?></td>
<?
	}
?>

						<td class="tdc point"><?= number_format($sum_all_usercount) ?></td>
						<td class="tdc point"><?= ($sum_all_coupon == 0) ? 0 : round($sum_all_more / $sum_all_coupon, 1) ?>%</td>
						<td class="tdc point" style="border-right: 1px dotted #dbdbdb;"><?= number_format($sum_all_coupon_runout_rate,2) ?>%</td>

						<td class="tdc point"><?= number_format($sum_total_coupon) ?></td>
						<td class="tdc point"><?= number_format($sum_total_usercount) ?></td>
						<td class="tdc point"><?= ($sum_total_coupon == 0) ? 0 : round($sum_total_more / $sum_total_coupon, 1) ?>%</td>
						<td class="tdc point" style="border-right: 1px dotted #dbdbdb;"><?= ($sum_total_coupon == 0) ? 0 : round($sum_total_expireday / $sum_total_coupon, 1) ?>일</td>
						
						<td class="tdc point"><?= number_format($sum_use_coupon) ?></td>
						<td class="tdc point"><?= number_format($sum_use_usercount) ?></td>
						<td class="tdc point" style="border-right: 1px dotted #dbdbdb;"><?= ($sum_use_more == 0) ? 0 : round($sum_use_more / $sum_use_coupon, 1) ?>%</td>
								
						<td class="tdc point"><?= number_format($sum_issue_coupon) ?></td>
						<td class="tdc point"><?= number_format($sum_issue_usercount) ?></td>
						<td class="tdc point"><?= ($sum_issue_more == 0) ? 0 : round($sum_issue_more / $sum_issue_coupon, 1) ?>%</td>
					</tr>
				</tbody>
                </table>
            </form>
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>