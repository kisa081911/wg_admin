<?
	$top_menu = "game_stats";
    $sub_menu = "game_mobile_push_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];	
	$pagename = "game_mobile_push_stats.php";
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today,7,"d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db_mobile = new CDatabase_Mobile();
	
	$tail = "WHERE 1=1  AND writedate >= '$startdate 00:00:00' AND writedate <= '$enddate 23:59:59'";
	$group_by = " GROUP BY LEFT(writedate,10) ";
 
	$sql = "SELECT LEFT(writedate,10) AS today, SUM(success) AS total_success, SUM(fail) AS total_fail, SUM(invalid) AS total_invalid ".
		   "FROM `tbl_push_result` $tail $group_by ORDER BY today ASC";
	
	$ios_push_list = $db_mobile->gettotallist($sql);

	$sql = "SELECT LEFT(writedate,10) AS today, SUM(success) AS total_success, SUM(fail) AS total_fail, SUM(invalid) AS total_invalid ".
			"FROM `tbl_push_result_android` $tail $group_by ORDER BY today ASC";
	
	$android_push_list = $db_mobile->gettotallist($sql);
	
	$sql = "SELECT LEFT(writedate,10) AS today, SUM(success) AS total_success, SUM(fail) AS total_fail, SUM(invalid) AS total_invalid ".
			"FROM `tbl_push_result_amazon` $tail $group_by ORDER BY today ASC";
	
	$amazon_push_list = $db_mobile->gettotallist($sql);
	
	$date_ios_list = array();
	$date_android_list = array();
	$date_amazon_list = array();
	
	$ios_total_success = array();
	$ios_total_fail = array();
	$ios_total_invalid = array();
	
	$android_total_success = array();
	$android_total_fail = array();
	$android_total_invalid = array();
	
	$amazon_total_success = array();
	$amazon_total_fail = array();
	$amazon_total_invalid = array();
	
	$ios_push_list_pointer = sizeof($ios_push_list);
	$android_push_list_pointer = sizeof($android_push_list);
	$amazon_push_list_pointer = sizeof($amazon_push_list);
	
	$date_pointer = $enddate;
	
	$loop_count = get_diff_date($enddate, $startdate, "d") + 1;	

	//ios
	for ($i=0; $i < $loop_count; $i++)
	{
		if ($ios_push_list_pointer > 0)
			$today = $ios_push_list[$ios_push_list_pointer-1]["today"];
	
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$ios_list = $ios_push_list[$ios_push_list_pointer-1];
	
			$date_ios_list[$i] = $date_pointer;
			$ios_total_success[$i] = $ios_list["total_success"];
			$ios_total_fail[$i] = $ios_list["total_fail"];
			$ios_total_invalid[$i] = $ios_list["total_invalid"];
	
			$ios_push_list_pointer--;
		}
		else
		{
			$date_ios_list[$i] = $date_pointer;
			$ios_total_success[$i] = 0;
			$ios_total_fail[$i] = 0;
			$ios_total_invalid[$i] = 0;	
		}
	
			$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	$date_pointer = $enddate;
	
	//android
	for ($i=0; $i < $loop_count; $i++)
	{
		if ($android_push_list_pointer > 0)
			$today = $android_push_list[$android_push_list_pointer-1]["today"];
	
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$android_list = $android_push_list[$android_push_list_pointer-1];
	
			$date_android_list[$i] = $date_pointer;
			$android_total_success[$i] = $android_list["total_success"];
			$android_total_fail[$i] = $android_list["total_fail"];
			$android_total_invalid[$i] = $android_list["total_invalid"];
	
			$android_push_list_pointer--;
		}
		else
		{
			$date_android_list[$i] = $date_pointer;
			$android_total_success[$i] = 0;
			$android_total_fail[$i] = 0;
			$android_total_invalid[$i] = 0;	
		}
	
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	$date_pointer = $enddate;
	
	//amazon
	for ($i=0; $i < $loop_count; $i++)
	{
		if ($amazon_push_list_pointer > 0)
			$today = $amazon_push_list[$amazon_push_list_pointer-1]["today"];
	
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$amazon_list = $amazon_push_list[$amazon_push_list_pointer-1];
	
			$date_amazon_list[$i] = $date_pointer;
			$amazon_total_success[$i] = $amazon_list["total_success"];
			$amazon_total_fail[$i] = $amazon_list["total_fail"];
			$amazon_total_invalid[$i] = $amazon_list["total_invalid"];
	
			$amazon_push_list_pointer--;
		}
		else
		{
			$date_amazon_list[$i] = $date_pointer;
			$amazon_total_success[$i] = 0;
			$amazon_total_fail[$i] = 0;
			$amazon_total_invalid[$i] = 0;	
		}
	
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}

	$db_mobile->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});

	function drawChart() 
	{
		//1. ios
		var data1 = new google.visualization.DataTable();
		
        data1.addColumn('string', '날짜');
        data1.addColumn('number', 'Success');
        data1.addColumn('number', 'Fail');
        data1.addColumn('number', 'Invalid');
        data1.addRows([
<?
    		for ($i=sizeof($date_ios_list); $i>0; $i--)
    		{
        		$_success = $ios_total_success[$i-1];
        		$_fail = $ios_total_fail[$i-1];
        		$_invalid = $ios_total_invalid[$i-1];
        		$_date = $date_ios_list[$i-1];
        
        		echo("['".$_date."'");
		
				if ($_success != "")
					echo(",{v:".$_success.",f:'".make_price_format($_success)."'}");
				else
					echo(",0");
				
				if ($_fail != "")
					echo(",{v:".$_fail.",f:'".make_price_format($_fail)."'}");
				else
					echo(",0");
				
				if ($_invalid != "")
					echo(",{v:".$_invalid.",f:'".make_price_format($_invalid)."'}]");
				else
					echo(",0]");
        
        		if ($i > 1)
            		echo(",");
    		}
?>
        ]);

		//2. android
		var data2 = new google.visualization.DataTable();
		
        data2.addColumn('string', '날짜');
        data2.addColumn('number', 'Success');
        data2.addColumn('number', 'Fail');
        data2.addColumn('number', 'Invalid');
        data2.addRows([
<?
    		for ($i=sizeof($date_android_list); $i>0; $i--)
    		{
        		$_success = $android_total_success[$i-1];
        		$_fail = $android_total_fail[$i-1];
        		$_invalid = $android_total_invalid[$i-1];
        		$_date = $date_android_list[$i-1];
        
        		echo("['".$_date."'");
		
				if ($_success != "")
					echo(",{v:".$_success.",f:'".make_price_format($_success)."'}");
				else
					echo(",0");
				
				if ($_fail != "")
					echo(",{v:".$_fail.",f:'".make_price_format($_fail)."'}");
				else
					echo(",0");
				
				if ($_invalid != "")
					echo(",{v:".$_invalid.",f:'".make_price_format($_invalid)."'}]");
				else
					echo(",0]");
        
        		if ($i > 1)
            		echo(",");
    		}
?>
        ]);

		//3. amazon
		var data3 = new google.visualization.DataTable();
		
        data3.addColumn('string', '날짜');
        data3.addColumn('number', 'Success');
        data3.addColumn('number', 'Fail');
        data3.addColumn('number', 'Invalid');
        data3.addRows([
<?
    		for ($i=sizeof($date_amazon_list); $i>0; $i--)
    		{
        		$_success = $amazon_total_success[$i-1];
        		$_fail = $amazon_total_fail[$i-1];
        		$_invalid = $amazon_total_invalid[$i-1];
        		$_date = $date_amazon_list[$i-1];
        
        		echo("['".$_date."'");
		
				if ($_success != "")
					echo(",{v:".$_success.",f:'".make_price_format($_success)."'}");
				else
					echo(",0");
				
				if ($_fail != "")
					echo(",{v:".$_fail.",f:'".make_price_format($_fail)."'}");
				else
					echo(",0");
				
				if ($_invalid != "")
					echo(",{v:".$_invalid.",f:'".make_price_format($_invalid)."'}]");
				else
					echo(",0]");
        
        		if ($i > 1)
            		echo(",");
    		}
?>
        ]);

		var options = {
	        title:'',                                                      
	        width:1050,                         
	        height:200,
	        axisTitlesPosition:'in',
	        curveType:'none',
	    	focusTarget:'category',
	        interpolateNulls:'true',
	        legend:'top',
	        fontSize : 12,
	        chartArea:{left:80,top:40,width:1020,height:130}
		};
	
		var chart = new google.visualization.LineChart(document.getElementById('chart_data1'));
	    chart.draw(data1, options);
	
	    var chart = new google.visualization.LineChart(document.getElementById('chart_data2'));
	    chart.draw(data2, options);
	
	    var chart = new google.visualization.LineChart(document.getElementById('chart_data3'));
	    chart.draw(data3, options);
	}

    google.setOnLoadCallback(drawChart);		

    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    	
    function search()
    {
        var search_form = document.search_form;
    	
        search_form.submit();
    }
    
    $(function() {
        $("#startdate").datepicker({ });
    });
    	
    $(function() {
        $("#enddate").datepicker({ });
    });
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
   	<!-- title_warp -->
   	<div class="title_wrap">
   		<div class="title"><?= $top_menu_txt ?> &gt; Mobile Push 통계</div>
   		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
   			<div class="search_box">
   				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
   				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
   				<input type="button" class="btn_search" value="검색" onclick="search()" />
   			</div>
   		</form>
   	</div>
   	<!-- //title_warp -->
    	
   	<div class="search_result">
   		<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다
   	</div>
    	
   	<div class="h2_title">[IOS Push Result]</div>
   	<div id="chart_data1" style="height:230px; min-width: 500px"></div>
    	
   	<div class="h2_title">[Android Push Result]</div>
   	<div id="chart_data2" style="height:230px; min-width: 500px"></div>
    	
   	<div class="h2_title">[Amazon Push Result]</div>
   	<div id="chart_data3" style="height:230px; min-width: 500px"></div>
    	
 
</div>
<!--  //CONTENTS WRAP -->
            
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");    
?>
            