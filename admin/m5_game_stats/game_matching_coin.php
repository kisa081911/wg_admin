<?
    $top_menu = "game_stats";
    $sub_menu = "game_matching_coin";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $db_analysis = new CDatabase_Analysis();
    $db_main = new CDatabase_Main();
    
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script>
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 코인 정합성 목록</div>
	</div>
	<!-- //title_warp -->
            
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="80">
			<col width="110">
			<col width="110">
			<col width="120">
			<col width="120">
			<col width="120">
			<col width="120">
			<col width="130">
			<col width="120">
			<col width="120">
			<col width="120">
		</colgroup>
		<thead>
			<tr>
				<th>날짜</th>
                <th class="tdc">무료코인</th>
                <th class="tdc">구매코인</th>
                <th class="tdc">게임수익</th>
                <th class="tdc">가입코인</th>
                <th class="tdc">미확인코인</th>
                <th class="tdc">미확인코인(로그)</th>
                <th class="tdc">집계된 코인변화(A)</th>
                <th class="tdc">사용자 코인변화(B)</th>
                <th class="tdc">만료된 Treat 금액(C)</th>

                <th class="tdc">오차(A-(B+C))</th>
            </tr>
		</thead>
		<tbody>
<?
    $sql = "SELECT writedate,free_coin,order_coin,game_profit,join_coin,unknown_coin,unknown_coin_log,sum_coin,change_coin FROM user_unknown_coin ORDER BY writedate DESC";
    $coin_list = $db_analysis->gettotallist($sql);
    
    for ($i=0; $i<sizeof($coin_list); $i++)
    {
        $writedate = $coin_list[$i]["writedate"];
        $free_coin = $coin_list[$i]["free_coin"];
        $order_coin = $coin_list[$i]["order_coin"];
        $game_profit = $coin_list[$i]["game_profit"];
        $join_coin = $coin_list[$i]["join_coin"];
        $unknown_coin = $coin_list[$i]["unknown_coin"];
        $unknown_coin_log = $coin_list[$i]["unknown_coin_log"];
        $sum_coin = $coin_list[$i]["sum_coin"];
        $change_coin = $coin_list[$i]["change_coin"];
        
        $treatamount = $db_main->getvalue("SELECT treatamount FROM `tbl_delete_treatamount_daily` WHERE today ='$writedate'");
        $treatamount = ($treatamount=="")? 0:$treatamount;
        
?>

                <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
                    <td class="tdc point"><?= $writedate ?></td>
                    <td class="tdr point"><?= number_format($free_coin) ?></td>
                    <td class="tdr point"><?= number_format($order_coin) ?></td>
                    <td class="tdr point"><?= number_format($game_profit) ?></td>
                    <td class="tdr point"><?= number_format($join_coin) ?></td>
                    <td class="tdr point"><?= number_format($unknown_coin) ?></td>
                    <td class="tdr point"><?= number_format($unknown_coin_log) ?></td>
                    <td class="tdr point"><?= number_format($sum_coin) ?></td>
                    <td class="tdr point"><?= number_format($change_coin) ?></td>
                    <td class="tdr point"><?= number_format($treatamount) ?></td>
		    <td class="tdr point"><?= number_format($sum_coin - ($change_coin+$treatamount)) ?></td>
		    
                </tr>
<?
    }
?>
		</tbody>
	</table>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?

    $db_analysis->end();
    $db_main->end();
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>