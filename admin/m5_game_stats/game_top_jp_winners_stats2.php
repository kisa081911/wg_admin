<?
	$top_menu = "game_stats";
	$sub_menu = "game_top_jp_winners_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$os_type = ($_GET["os_type"] == "") ? "4" :$_GET["os_type"];
	$total_mode = ($_GET["total_mode"] == "") ? "0" :$_GET["total_mode"];
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	
	if($search_start_createdate == "")
		$search_start_createdate = date("Y-m-d", strtotime("-3 day"));
	
	if($search_end_createdate == "")
		$search_end_createdate = $today;
	
	$onwer_sql = "t2.owner = 1 AND ";
	 
	if($os_type == "4")
	{
		$os_txt = "All";
		$os_type = "devicetype";
		$onwer_sql = "";
	}
	else if($os_type == "0")
	{
		$os_txt = "Web";
	}
	else if($os_type == "1")
	{
		$os_txt = "IOS";
	}
	else if($os_type == "2")
	{
		$os_txt = "Android";
	}
	else if($os_type == "3")
	{
		$os_txt = "Amazon";
	}
	
	if($total_mode == 0)
		$mode_name = "전체";
	else if($total_mode == 1)
		$mode_name = "레귤러";
	else if($total_mode == 2)
		$mode_name = "하이롤러";
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	
	if($total_mode == 0)
	{
		$sql = "SELECT slottype, t1.jackpothallidx AS jackpothallidx, remain_jackpot, (SELECT high_roller FROM tbl_jackpot_hall WHERE jackpothallidx = t1.jackpothallidx) AS high_roller, writedate, fiestaidx ".
				"FROM ( ".
				"	SELECT useridx, slottype, jackpothallidx, remain_jackpot, writedate, fiestaidx ".
				"	FROM tbl_jackpot_log ".
				"	WHERE jackpothallidx != 0  AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
				"	AND fiestaidx > 0  ORDER BY jackpotidx DESC ".
				") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx WHERE $onwer_sql t2.useridx > 10000 ".
				"ORDER BY jackpothallidx DESC, owner DESC";
		
		$ultral_free_sql = "SELECT slottype, t1.jackpothallidx AS jackpothallidx, remain_jackpot, (SELECT high_roller FROM tbl_jackpot_hall WHERE jackpothallidx = t1.jackpothallidx) AS high_roller, writedate, fiestaidx ".
					"FROM ( ".
					"	SELECT useridx, slottype, jackpothallidx, remain_jackpot, writedate, fiestaidx ".
					"	FROM tbl_jackpot_log ".
					"	WHERE jackpothallidx = 0  AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
					"	AND fiestaidx > 0  ORDER BY jackpotidx DESC ".
					") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx WHERE $onwer_sql  t2.useridx > 10000 ".
					"ORDER BY writedate DESC, owner DESC";
	}
	else if($total_mode == 1)
	{
		$sql = "SELECT slottype, t1.jackpothallidx AS jackpothallidx, remain_jackpot, 0 AS high_roller, writedate, fiestaidx ".
				"FROM ( ".
				"	SELECT useridx, slottype, jackpothallidx, remain_jackpot, writedate, fiestaidx ".
				"	FROM tbl_jackpot_log ".
				"	WHERE objectidx NOT IN (0) AND objectidx < 1000000 AND devicetype = $os_type AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
				"	AND fiestaidx > 0 ORDER BY jackpotidx DESC ".
				") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx WHERE $onwer_sql t2.useridx > 10000 ".
				"ORDER BY jackpothallidx DESC, owner DESC";
		
		$ultral_free_sql = "SELECT slottype, t1.jackpothallidx AS jackpothallidx, remain_jackpot, 0 AS high_roller, writedate, fiestaidx ".
							"FROM ( ".
							"	SELECT useridx, slottype, jackpothallidx, remain_jackpot, writedate, fiestaidx ".
							"	FROM tbl_jackpot_log ".
							"	WHERE objectidx IN (0) AND objectidx < 1000000 AND devicetype = $os_type AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
							"	AND fiestaidx > 0  ORDER BY jackpotidx DESC ".
							") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx WHERE  t2.owner = 1 AND t2.useridx > 10000 ".
							"ORDER BY writedate DESC, owner DESC";
	}
	else if($total_mode == 2)
	{
		$sql = "SELECT slottype, t1.jackpothallidx AS jackpothallidx, remain_jackpot, 1 AS high_roller, writedate, fiestaidx ".
							"FROM ( ".
							"	SELECT useridx, slottype, jackpothallidx, remain_jackpot, writedate, fiestaidx ".
							"	FROM tbl_jackpot_log ".
							"	WHERE objectidx >= 1000000 AND devicetype = $os_type AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
							"	AND fiestaidx > 0  ORDER BY jackpotidx DESC ".
							") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx WHERE $onwer_sql t2.useridx > 10000 ".
							"ORDER BY jackpothallidx DESC, owner DESC";
		
		$ultral_free_sql = "SELECT slottype, t1.jackpothallidx AS jackpothallidx, remain_jackpot, 0 AS high_roller, writedate, fiestaidx ".
							"FROM ( ".
							"	SELECT useridx, slottype, jackpothallidx, remain_jackpot, writedate, fiestaidx ".
							"	FROM tbl_jackpot_log ".
							"	WHERE objectidx IN (0) AND devicetype = $os_type AND objectidx >= 1000000 AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
							"	AND fiestaidx > 0  ORDER BY jackpotidx DESC ".
							") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx WHERE $onwer_sql t2.useridx > 10000 ".
							"ORDER BY writedate DESC, owner DESC";
	}
	
	$jackpot_data = $db_main->gettotallist($sql);
	
	$ultral_free_jackpot_data = "";
	
	if($ultral_free_sql != "")
		$ultral_free_jackpot_data = $db_main->gettotallist($ultral_free_sql);
	
	//Slot 정보
	$sql = "SELECT slottype, slotname FROM tbl_slot_list";
	$slottype_list = $db_main2->gettotallist($sql);
	
	if($os_type == "devicetype")
	{
		$os_type = "4";		
	}
	
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});

	function change_os_type(type)
	{
		var search_form = document.search_form;

		var all = document.getElementById("type_all");
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		
		document.search_form.os_type.value = type;

		if (type == "4")
		{
			all.className="btn_schedule_select";
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "0")
		{
			all.className="btn_schedule";
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "1")
		{
			all.className="btn_schedule";
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "2")
		{
			all.className="btn_schedule";
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (type == "3")
		{
			all.className="btn_schedule";
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="game_top_jp_winners_stats.php">
	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
		<input type="button" class="<?= ($os_type == "4") ? "btn_schedule_select" : "btn_schedule" ?>" value="all" id="type_all" onclick="change_os_type('4')"    />
		<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
		<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
		<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
		<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
	</span>
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Fiesta JackPot Winners 통계(<?= $os_txt ?>/<?=$mode_name?>)</div>
		<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />  
		<div class="search_box">
			mode&nbsp;:&nbsp; 
			<select name="total_mode" id="total_mode">										
					<option value="0" <?= ($total_mode=="0") ? "selected" : "" ?>>전체</option>
					<option value="1" <?= ($total_mode=="1") ? "selected" : "" ?>>레귤러</option>                       
					<option value="2" <?= ($total_mode=="2") ? "selected" : "" ?>>하이롤러</option>
			</select>&nbsp;&nbsp;&nbsp;
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	<div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">                
                <col width="">
                <col width="">
                <col width="">                
<?
			if($total_mode == 2)
			{
?>
				<col width="">
<?
 			}
?>
				<col width="">
                <col width="">
            </colgroup>
            <thead>
	            <tr>
	                <th>Jackpothall</th>
	                <th class="tdc">Slot</th>
	                <th class="tdc">종류</th>
	                <th class="tdc">총 금액</th>
	                <th class="tdc">Owner 금액</th>	                
	                <th class="tdc">유저</th>
<?
			if($total_mode == 2)
			{
?>
					<th class="tdc">남은 잭팟금액</th>
<?
			}
?>
					<th class="tdc">bet</th>
	                <th class="tdc">Jackpot 획득일</th>	                
	            </tr>
            </thead>
            <tbody>
<?			
			for($i=0; $i<sizeof($jackpot_data); $i++)
			{
				$slottype = $jackpot_data[$i]["slottype"];
				$jackpothallidx = $jackpot_data[$i]["jackpothallidx"];
				$writedate = $jackpot_data[$i]["writedate"];
				$remain_jackpot = $jackpot_data[$i]["remain_jackpot"];
				$high_roller = $jackpot_data[$i]["high_roller"];
				$fiestaidx = $jackpot_data[$i]["fiestaidx"];
				
				if($high_roller == 2)
					$high_roller_str = "레귤러";
				else
					$high_roller_str = "하이롤러";

             	if($i == 0 || $jackpothallidx != $jackpot_data[$i-1]["jackpothallidx"])
				{
					$sql = "SELECT COUNT(*) AS cnt_jackpot_user, SUM(amount) AS jackpot_amount, IF(owner=1, amount, 0) AS owner_amount, fiestaidx ". 
							"FROM tbl_jackpot_log t1 ".
							"LEFT JOIN tbl_jackpot_hall_member t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx  ".
							"WHERE t1.jackpothallidx = $jackpothallidx AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' AND fiestaidx > 0";
					$jackpot_info = $db_main->getarray($sql);
					
					$cnt_jackpot_user = $jackpot_info["cnt_jackpot_user"];
					$jackpot_amount = $jackpot_info["jackpot_amount"];
					$owner_amount = $jackpot_info["owner_amount"];
					$fiestaidx = $jackpot_info["fiestaidx"];
					
					for($j=0; $j<sizeof($slottype_list); $j++)
					{
						if($slottype_list[$j]["slottype"] == $slottype)
						{
							$slot_name = $slottype_list[$j]["slotname"];
							break;
						}
						else
						{
							$slot_name = "Unkown";
						}
					}
					
					
					if($fiestaidx > 0)
						$jackpot_name = "Fiesta JACKPOT";
					else if($cnt_jackpot_user == 1)
						$jackpot_name = "DECENT JACKPOT";
					else if($cnt_jackpot_user == 2)
						$jackpot_name = "FABULOUS JACKPOT";
					else if($cnt_jackpot_user == 3)
						$jackpot_name = "AWESOME JACKPOT";
					else if($cnt_jackpot_user == 4)
						$jackpot_name = "AMAZING JACKPOT";
					else if($cnt_jackpot_user == 5)
						$jackpot_name = "TAKE5 JACKPOT";

					
					$sql = "SELECT (SELECT userid FROM tbl_user WHERE useridx = t1.useridx) AS userid, t1.useridx, owner, devicetype AS platform, fiestaidx ". 
							"FROM tbl_jackpot_log t1 ".
							"LEFT JOIN tbl_jackpot_hall_member t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx ".
							"WHERE t1.jackpothallidx = $jackpothallidx AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' AND fiestaidx > 0 ORDER BY owner DESC";
					
					$jackpot_user_info = $db_main->gettotallist($sql);
?>
					<tr  class="" onmouseover="" onmouseout="" onclick="">					
                    	<td class="tdc point"><?= ($jackpothallidx == 0)?'Ultra':$jackpothallidx?></td>
                    	<td class="tdc point"><?= $slot_name ?></td>
						<td class="tdc point"><?= $jackpot_name ?></td>
						<td class="tdc point"><?= number_format($jackpot_amount) ?></td>
						<td class="tdc point"><?= number_format($owner_amount) ?></td>											
						<td class="tdc point">
<?
						for($j=0; $j<sizeof($jackpot_user_info); $j++)
						{
							$userid = $jackpot_user_info[$j]["userid"];
							$platform = $jackpot_user_info[$j]["platform"];
							
							if($userid < 10)    
						    	$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
						    else 
						        $photourl = get_fb_pictureURL($userid,$client_accesstoken);
							
							$useridx = $jackpot_user_info[$j]["useridx"];
							$owner = $jackpot_user_info[$j]["owner"];
							
							$owner_user = "";
							
							if($platform == 0)
								$owner_user = "(web)";
							else if($platform == 1)
								$owner_user = "(ios)";
							else if($platform == 2)
								$owner_user = "(and)";
							else if($platform == 3)
								$owner_user = "(ama)";							
							
							if($owner == 1)
							{
								$owner_user = "(owner)".$owner_user;
							}
							
							
?>
							<div style="float:left; cursor: pointer;" onclick="view_user_dtl(<?= $useridx ?>,'')"><img src="<?= $photourl?>"  height="40" width="40" class="summary_user_image"/><?=$owner_user?></div>
<?
						}
?>
						</td>
<?
					if($total_mode == 2)
					{
?>
							<td class="tdc point"><?= number_format($remain_jackpot) ?></td>
<?
					}
?>
						<td class="tdc point"><?= $high_roller_str ?></td>
						<td class="tdc point"><?= $writedate ?></td>
					</tr>
<?
				}
			}
			
			for($i=0; $i<sizeof($ultral_free_jackpot_data); $i++)
			{
				$slottype = $ultral_free_jackpot_data[$i]["slottype"];
				$jackpothallidx = $ultral_free_jackpot_data[$i]["jackpothallidx"];
				$writedate = $ultral_free_jackpot_data[$i]["writedate"];
				
				$high_roller_str = "레귤러";

             	if($i == 0 || $writedate != $ultral_free_jackpot_data[$i-1]["writedate"])
				{
					$sql = "SELECT COUNT(*) AS cnt_jackpot_user, SUM(amount) AS jackpot_amount, IF(owner=1, amount, 0) AS owner_amount, fiestaidx ". 
							"FROM tbl_jackpot_log t1 ".
							"LEFT JOIN tbl_jackpot_hall_member t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx ".
							"WHERE t1.jackpothallidx = 0 AND fiestaidx > 0 AND t1.slottype = $slottype AND writedate = '$writedate'";
					$jackpot_info = $db_main->getarray($sql);
					
					$cnt_jackpot_user = $jackpot_info["cnt_jackpot_user"];
					$jackpot_amount = $jackpot_info["jackpot_amount"];
					$owner_amount = $jackpot_info["owner_amount"];
					$fiestaidx = $jackpot_info["fiestaidx"];
					
					for($j=0; $j<sizeof($slottype_list); $j++)
					{
						if($slottype_list[$j]["slottype"] == $slottype)
						{
							$slot_name = $slottype_list[$j]["slotname"];
							break;
						}
						else
						{
							$slot_name = "Unkown";
						}
					}
					
					if($fiestaidx > 0)
						$jackpot_name = "Fiesta JACKPOT";
					else if($cnt_jackpot_user == 1)
						$jackpot_name = "DECENT JACKPOT";
					else if($cnt_jackpot_user == 2)
						$jackpot_name = "FABULOUS JACKPOT";
					else if($cnt_jackpot_user == 3)
						$jackpot_name = "AWESOME JACKPOT";
					else if($cnt_jackpot_user == 4)
						$jackpot_name = "AMAZING JACKPOT";
					else if($cnt_jackpot_user == 5)
						$jackpot_name = "TAKE5 JACKPOT";
					
					$sql = "SELECT (SELECT userid FROM tbl_user WHERE useridx = t1.useridx) AS userid, t1.useridx, owner, fiestaidx ". 
							"FROM tbl_jackpot_log t1 ".
							"LEFT JOIN tbl_jackpot_hall_member t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx ".
							"WHERE t1.jackpothallidx = 0 AND  t1.slottype = $slottype AND writedate = '$writedate' AND fiestaidx > 0 ORDER BY owner DESC";
					
					$jackpot_user_info = $db_main->gettotallist($sql);
?>
					<tr  class="" onmouseover="" onmouseout="" onclick="">					
                    	<td class="tdc point"><?= ($jackpothallidx == 0)?'Ultra':$jackpothallidx?></td>
                    	<td class="tdc point"><?= $slot_name ?></td>
						<td class="tdc point"><?= $jackpot_name ?></td>
						<td class="tdc point"><?= number_format($jackpot_amount) ?></td>
						<td class="tdc point"><?= number_format($owner_amount) ?></td>											
						<td class="tdc point">
<?
						for($j=0; $j<sizeof($jackpot_user_info); $j++)
						{
							$userid = $jackpot_user_info[$j]["userid"];
							$photourl = get_fb_pictureURL($userid,$client_accesstoken);
							$useridx = $jackpot_user_info[$j]["useridx"];
							$owner = $jackpot_user_info[$j]["owner"];
							
							$owner_user = "";
							
							if($owner == 1)
								$owner_user = "(owner)";
?>
							<div style="float:left; cursor: pointer;" onclick="view_user_dtl(<?= $useridx ?>,'')"><img src="<?= $photourl?>"/><?=$owner_user?></div>
<?
						}
?>
						</td>
						<td class="tdc point"><?= $high_roller_str ?></td>
						<td class="tdc point"><?= $writedate ?></td>
					</tr>
<?
				}
			}
			
?>
			</tbody>
            </table>
     </div>
        
     </form>
	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main->end();
	$db_main2->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
