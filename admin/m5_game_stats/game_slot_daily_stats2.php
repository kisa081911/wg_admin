
<?
    $top_menu = "game_stats";
    $sub_menu = "game_slot_daily_stats";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $os_type = ($_GET["os_type"] == "") ? "0" :$_GET["os_type"];
    $tab = ($_GET["tab"] == "") ? "0" :$_GET["tab"];
    $total_mode = ($_GET["total_mode"] == "") ? "0" :$_GET["total_mode"];
    $viewmode = ($_GET["viewmode"] == "") ? "0" :$_GET["viewmode"];
    $mode = ($_GET["mode"] == "") ? "mode" :$_GET["mode"];
    $betlevel = ($_GET["betlevel"] == "") ? "betlevel" :$_GET["betlevel"];
    $startdate = ($_GET["startdate"] == "") ? date("Y-m-d", mktime(0,0,0,date("m"),date("d")-15,date("Y"))) : $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
    $pagename = "game_slot_daily_stats.php";
    
    if ($tab == "")
        error_back("잘못된 접근입니다.");
        
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    $db_other = new CDatabase_Other();
    $db_analysis = new CDatabase_Analysis();    
    
    $tail = " WHERE 1=1 ";
    
    if($os_type == "0")
    {
    	$table = "tbl_game_cash_stats_daily2";
    	$online_log_table = "user_online_game_log WHERE 1=1 AND ";
    	$online_log_daily_table = "user_online_game_log_daily WHERE 1=1 AND ";
    	$os_txt = "Web";
    }
    else if($os_type == "1")
    {
    	$tail .= " AND ios = 1 ";
    	$table = "tbl_game_cash_stats_ios_daily2";
    	$online_log_table = "user_online_game_mobile_log WHERE os_type = 1 AND ";
    	$online_log_daily_table = "user_online_game_mobile_log_daily WHERE os_type = 1 AND ";
    	$os_txt = "IOS";
    }
    else if($os_type == "2")
    {
    	$tail .= " AND android = 1 ";
    	$table = "tbl_game_cash_stats_android_daily2";
    	$online_log_table = "user_online_game_mobile_log WHERE os_type = 2 AND ";
    	$online_log_daily_table = "user_online_game_mobile_log_daily WHERE os_type = 2 AND ";
    	$os_txt = "Android";
    }
    else if($os_type == "3")
    {
    	$tail .= " AND amazon = 1 ";
    	$table = "tbl_game_cash_stats_amazon_daily2";
    	$online_log_table = "user_online_game_mobile_log WHERE os_type = 3 AND ";
    	$online_log_daily_table = "user_online_game_mobile_log_daily WHERE os_type = 3 AND ";
    	$os_txt = "Amazon";
    }
    
	$sql = "SELECT slottype, slotname FROM tbl_slot_list $tail";
	$slotlist = $db_main2->gettotallist($sql);
	
	$slostartlist = array(array("slottype" => "0", "slotname" => "ALL"));
	$slotlist = array_merge($slostartlist, $slotlist);
	
	$sql = "SELECT bettype, betname FROM tbl_slot_betlevel ORDER BY bettype ASC";
	$betlist = $db_main2->gettotallist($sql);
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}	

	$total_betlevel = "1=1";
	$jackpot_betlevel = "1=1";
	$total_unit_betlevel = "1=1";
		
	if($total_mode == "1")//전체(0), 일반(1), 플래티엄 (싱글X)(2), 싱글모드(3), 플래티엄 (싱글포함)(4)
	{
		$total_betlevel = "betlevel between 0 AND 9";
		$total_unit_betlevel = "t1.betlevel between 0 AND 9";
		$jackpot_betlevel = "bettype=0";
	}
	else if($total_mode == "2")
	{
		$total_betlevel = "betlevel between 10 AND 19";
		$total_unit_betlevel = "t1.betlevel between 10 AND 19";
		$jackpot_betlevel = "bettype=1";
	}
	else if($total_mode == "3")
	{
		$total_betlevel = "betlevel between 110 AND 119";
		$total_unit_betlevel = "t1.betlevel between 110 AND 119";
		$jackpot_betlevel = "bettype=1";
	}
	else if($total_mode == "4")
	{
		$total_betlevel = "betlevel between 10 AND 119";
		$total_unit_betlevel = "t1.betlevel between 10 AND 119";
		$jackpot_betlevel = "bettype=1";
	}
	
	if($mode == "0")//일반(0), 튜토리얼(4), 승률부양(6,7,8), 울트라프리스핀(9), 승률부양(11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 29, 30, 30+27)
		$modestr = "mode IN (0,31)";
	else if($mode == "3027")//일반(0), 튜토리얼(4), 승률부양(6,7,8), 울트라프리스핀(9), 승률부양(11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 29, 30, 30+27)
		$modestr = "mode IN (27,31)";
	else
		$modestr = "mode=$mode";

	if($tab == "0")//all, slot
	{
		if($viewmode == "0") //전체(0), mode(1), betlevel(2)
		{
			$moneyout_sql = " IFNULL(SUM(IF(MODE!=26, moneyout, 0)), 0) AS moneyout";
			
			if($mode == 26)
				$moneyout_sql = " IFNULL(SUM(IF(MODE=26, moneyout, 0)), 0) AS moneyout";
				
			$sql = "SELECT slottype, betlevel, MODE, IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, $moneyout_sql, LEFT(writedate, 10) AS writedate, IFNULL(SUM(usercount),0) AS usercount ".
					"FROM $table ".
					"WHERE $modestr AND $total_betlevel AND betlevel=$betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY writedate ORDER BY writedate DESC";
			
			if($mode == "mode" && $total_mode == 0)
			{	
				$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
							"FROM $table ".
							"WHERE MODE NOT IN (9, 26, 29, 30) AND writedate BETWEEN '[WRITEDATE] 00:00:00' AND '[WRITEDATE] 23:59:59'";
				
				if($betlevel == "betlevel")
				{
					$exchange_treat_sql = "SELECT today, freeamount FROM `tbl_user_freecoin_daily` WHERE category = $os_type AND type = 5 AND today BETWEEN '$startdate' AND '$enddate'";
					$exchange_treat_list = $db_analysis->gettotallist($exchange_treat_sql);
					
					$exchange_treat_array = array();
					
					for($e = 0; $e < sizeof($exchange_treat_list); $e++)
					{
						$today = $exchange_treat_list[$e]["today"];
						$freeamount = $exchange_treat_list[$e]["freeamount"];
						
						$exchange_treat_array[$today] = $freeamount;
					}
				}
			}
			else if($mode == "mode" && $total_mode == 1)
			{
				$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
						"FROM $table ".
						"WHERE MODE NOT IN (9, 26, 29, 30) AND $total_betlevel AND writedate BETWEEN '[WRITEDATE] 00:00:00' AND '[WRITEDATE] 23:59:59'";
			}
			else if($mode == "mode" && $total_mode == 2)
			{
				$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
						"FROM $table ".
						"WHERE MODE NOT IN (9, 26, 29, 30) AND $total_betlevel AND writedate BETWEEN '[WRITEDATE] 00:00:00' AND '[WRITEDATE] 23:59:59'";
			}
			else if($mode == "mode" && $total_mode == 3)
			{
				$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
						"FROM $table ".
						"WHERE MODE NOT IN (9, 26, 29, 30) AND $total_betlevel AND writedate BETWEEN '[WRITEDATE] 00:00:00' AND '[WRITEDATE] 23:59:59'";
			}
			else if($mode == "mode" && $total_mode == 4)
			{
				$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
						"FROM $table ".
						"WHERE MODE NOT IN (9, 26, 29, 30) AND $total_betlevel AND writedate BETWEEN '[WRITEDATE] 00:00:00' AND '[WRITEDATE] 23:59:59'";
			}
			
			if($mode == 0 || $mode == 25 || $mode == 28 ||  $mode == "mode")
			{
				//가중 승률
				if($mode == "mode")
					$palycount_mode = " AND MODE NOT IN (4, 9, 26, 29) ";
				else if($mode == 0)
					$palycount_mode = " AND MODE IN (0,31) ";
				else if($mode == 25)
					$palycount_mode = " AND MODE = 25 ";
				else if($mode == 28)
					$palycount_mode = " AND MODE = 28 ";
					
// 				$rate_playcount_sql = "SELECT writedate, ROUND(SUM(rate_playcount)/ SUM(sum_playcount), 2) AS rate_playcount ".
// 						"FROM	".
// 						"(	".
// 						"	SELECT writedate, ".
// 						"	ROUND(SUM(moneyout)/SUM(moneyin)*100, 2), ".
// 						"	ROUND((ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) * SUM(playcount)),0) AS rate_playcount, ".
// 						"	SUM(playcount) AS sum_playcount ".
// 						"	FROM `$table` ".
// 						"	WHERE $total_betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $palycount_mode ".
// 						"	GROUP BY writedate, betlevel ".
// 						") t1 GROUP BY writedate";

// 				$rate_playcount_list = $db_main2->gettotallist($rate_playcount_sql);
			
// 				$rate_playcount_array = array();
								
// 				for($e = 0; $e < sizeof($rate_playcount_list); $e++)
// 				{
// 					$today = $rate_playcount_list[$e]["writedate"];
// 					$rate_playcount = $rate_playcount_list[$e]["rate_playcount"];
			
// 					$rate_playcount_array[$today] = $rate_playcount;
// 				}
				
// 				$total_rate_playcount = "SELECT ROUND(SUM(rate_playcount)/ SUM(sum_playcout), 2) ".
// 										"FROM	".
// 										"(	".
// 										"	SELECT ".  	 
// 										"	ROUND((ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) * SUM(playcount)),0) AS rate_playcount, ". 
// 										"	SUM(playcount) AS sum_playcout ". 
// 										"	FROM `$table` ". 
// 										"	WHERE $total_betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $palycount_mode ".
// 										"	GROUP BY betlevel ".
// 										") t1;";
// 				$total_rate_playcount_value = $db_main2->getvalue($total_rate_playcount);
				
				// unit 승률
				$unit_rate_sql = "SELECT writedate, ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2) AS unit_rate_new, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unit_rate ".
								"FROM	".
								"	(	".
								"		SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout,SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout	".
								"		FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	". 
								"		WHERE $total_unit_betlevel AND writedate BETWEEN '$startdate' AND '$enddate' $palycount_mode ".
								"		GROUP BY t1.slottype, betlevel, writedate	".
								") t3 GROUP BY writedate;";

				$unit_rate_list = $db_main2->gettotallist($unit_rate_sql);
					
				$unit_rate_array = array();
				
				for($e = 0; $e < sizeof($unit_rate_list); $e++)
				{
					$today = $unit_rate_list[$e]["writedate"];
					$unit_rate = $unit_rate_list[$e]["unit_rate"];
					$unit_rate_new = ($unit_rate_list[$e]["unit_rate_new"]=="")? 0 : $unit_rate_list[$e]["unit_rate_new"];
					
					if($today > '2017-11-20' && $os_type == 0)
						$unit_rate_array[$today] = $unit_rate_new;
					else 
						$unit_rate_array[$today] = $unit_rate;
				}
					
				if($enddate > '2017-11-20' && $os_type == 0)
				{
					$total_unit_rate_sql = "SELECT SUM(tt_moneyout) AS tt_moneyout, SUM(tt_moneyin) AS tt_moneyin ".
											"FROM	".
											"(	".
											"	SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout	".
											"	FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	". 
											"	WHERE $total_unit_betlevel AND writedate BETWEEN '$startdate' AND '2017-11-20' $palycount_mode".
											"	GROUP BY t1.slottype, betlevel	".
											") t3";
					$total_unit_rate_arr = $db_main2->getarray($total_unit_rate_sql);
					$total_unit_rate_moneyin =$total_unit_rate_arr["tt_moneyin"];
					$total_unit_rate_moneyout =$total_unit_rate_arr["tt_moneyout"];
					
					$total_unit_rate_new_sql=" SELECT SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout ".
						  					 " FROM ( ".
											 "	  SELECT t1.slottype, betlevel, SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout ".
										 	 "      FROM   $table t1   ".
										 	 "      WHERE  $total_unit_betlevel AND writedate BETWEEN '2017-11-20' AND '$enddate' ". 
											 " AND MODE NOT IN ( 4, 9, 26, 29 ) GROUP  BY t1.slottype, betlevel ) t3 ";
					$total_unit_rate_new_arr = $db_main2->getarray($total_unit_rate_sql);
					$total_unit_rate_moneyin += ($total_unit_rate_new_arr["unit_moneyin"]=="")?0:$total_unit_rate_new_arr["unit_moneyin"];
					$total_unit_rate_moneyout += ($total_unit_rate_new_arr["unit_moneyout"]=="")?0:$total_unit_rate_new_arr["unit_moneyout"];
						
				}
				else 
				{
					$total_unit_rate_sql = "SELECT SUM(tt_moneyout) AS tt_moneyout, SUM(tt_moneyin) AS tt_moneyin ".
							"FROM	".
							"(	".
							"	SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout	".
							"	FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
							"	WHERE $total_unit_betlevel  AND writedate BETWEEN '$startdate' AND '$enddate' $palycount_mode".
							"	GROUP BY t1.slottype, betlevel	".
							") t3";
					$total_unit_rate_arr = $db_main2->getarray($total_unit_rate_sql);
					$total_unit_rate_moneyin =$total_unit_rate_arr["tt_moneyin"];
					$total_unit_rate_moneyout =$total_unit_rate_arr["tt_moneyout"];
				}
				
				$total_unit_rate_value = round($total_unit_rate_moneyout/$total_unit_rate_moneyin*100,2);
			}
			
			if($mode == 26)
			{
				$mode_26_startdate =  $db_main2->getvalue("SELECT MIN(writedate) FROM $table WHERE mode=26 AND $total_betlevel AND $startdate <= writedate");
				
				$mode_total_sql = "SELECT IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout),0) AS moneyout, LEFT(writedate, 10) AS writedate ".
								"FROM $table WHERE mode IN (26) AND $total_betlevel AND writedate BETWEEN '$mode_26_startdate' AND '$enddate' GROUP BY writedate ORDER BY writedate DESC";
				
				$mode_total_list = $db_main2->gettotallist($mode_total_sql);
					
				$mode_total_array = array();
				
				$mode_total_sum_moneyin = 0;
				$mode_total_sum_moneyout = 0;
				
				for($e = 0; $e < sizeof($mode_total_list); $e++)
				{
					$mode_total_today = $mode_total_list[$e]["writedate"];
					$mode_total_moneyin = $mode_total_list[$e]["moneyin"];
					$mode_total_moneyout = $mode_total_list[$e]["moneyout"];
		
					$mode_total_array[$mode_total_today]["moneyin"] = $mode_total_moneyin;
					$mode_total_array[$mode_total_today]["moneyout"] = $mode_total_moneyout;
					
					$mode_total_sum_moneyin += $mode_total_moneyin;
					$mode_total_sum_moneyout += $mode_total_moneyout;
				}
			}
		}
		else if($viewmode == "1")
		{
			if($mode == "0" || $mode == "mode" )
			{
				$sql = "SELECT slottype, betlevel, REPLACE(MODE,30,0) AS MODE, IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout), 0) AS moneyout, LEFT(writedate, 10) AS writedate, IFNULL(SUM(usercount),0) AS usercount  ".
						" FROM $table ".
						" WHERE $modestr AND $total_betlevel AND betlevel=$betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY writedate, REPLACE(MODE,31,0)".
						" UNION ALL".
						" SELECT slottype, betlevel, MODE, IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout), 0) AS moneyout, LEFT(writedate, 10) AS writedate, IFNULL(SUM(usercount),0) AS usercount  ".
						" FROM $table ".
						" WHERE MODE = 31 AND $total_betlevel AND betlevel=$betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY writedate, MODE ORDER BY writedate DESC, mode ASC ";
				
			}
			else if($mode == "3027" )
			{
				$sql = "SELECT slottype, betlevel, IF(MODE IN(31,27),3027,0) AS MODE, IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout), 0) AS moneyout, LEFT(writedate, 10) AS writedate, IFNULL(SUM(usercount),0) AS usercount  ".
						"FROM $table ".
						"WHERE $modestr AND $total_betlevel AND betlevel=$betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY writedate, IF(MODE IN(31,27),3027,0) ORDER BY writedate DESC, mode ASC ";
			}
			else if($mode == "30")
			{
				$sql = " SELECT slottype, betlevel, MODE, IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout), 0) AS moneyout, LEFT(writedate, 10) AS writedate, IFNULL(SUM(usercount),0) AS usercount  ".
						" FROM $table ".
						" WHERE MODE = 31 AND $total_betlevel AND betlevel=$betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY writedate, MODE ORDER BY writedate DESC, mode ASC ";
			}
			else
			{
				$sql = "SELECT slottype, betlevel, REPLACE(MODE,31,0) AS MODE, IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout), 0) AS moneyout, LEFT(writedate, 10) AS writedate, IFNULL(SUM(usercount),0) AS usercount  ".
						"FROM $table ".
						"WHERE $modestr AND $total_betlevel AND betlevel=$betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY writedate, REPLACE(MODE,31,0) ORDER BY writedate DESC, mode ASC ";
			}
			
			if($mode == 0 && $mode != "mode")
			{
// 				//가중 승률				
// 				$palycount_mode = " AND MODE = 0 ";
				
// 				$rate_playcount_sql = "SELECT writedate, ROUND(SUM(rate_playcount)/ SUM(sum_playcount), 2) AS rate_playcount ".
// 										"FROM	".
// 										"(	".
// 										"	SELECT writedate, ". 
// 										"	ROUND(SUM(moneyout)/SUM(moneyin)*100, 2), ". 
// 										"	ROUND((ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) * SUM(playcount)),0) AS rate_playcount, ". 
// 										"	SUM(playcount) AS sum_playcount ". 
// 										"	FROM `$table` ". 
// 										"	WHERE $total_betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $palycount_mode ".
// 										"	GROUP BY writedate, betlevel ".
// 										") t1 GROUP BY writedate";
// 				$rate_playcount_list = $db_main2->gettotallist($rate_playcount_sql);
				
// 				$rate_playcount_array = array();
					
// 				for($e = 0; $e < sizeof($rate_playcount_list); $e++)
// 				{
// 					$today = $rate_playcount_list[$e]["writedate"];
// 					$rate_playcount = $rate_playcount_list[$e]["rate_playcount"];
				
// 					$rate_playcount_array[$today] = $rate_playcount;
// 				}
				
				// unit 승률
				$unit_rate_sql = "SELECT writedate, ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2) AS unit_rate_new, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unit_rate ".
								"FROM	".
								"	(	".
								"		SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout,SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout	".
								"		FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	". 
								"		WHERE $total_unit_betlevel  AND writedate BETWEEN '$startdate' AND '$enddate' $palycount_mode ".
								"		GROUP BY slottype, betlevel, writedate	".
								") t3 GROUP BY writedate;";

				$unit_rate_list = $db_main2->gettotallist($unit_rate_sql);
					
				$unit_rate_array = array();
				
				for($e = 0; $e < sizeof($unit_rate_list); $e++)
				{
					$today = $unit_rate_list[$e]["writedate"];
					$unit_rate = $unit_rate_list[$e]["unit_rate"];
					$unit_rate_new = ($unit_rate_list[$e]["unit_rate_new"]=="")? 0 : $unit_rate_list[$e]["unit_rate_new"];
					
					if($today > '2017-11-20' && $os_type == 0)
						$unit_rate_array[$today] = $unit_rate_new;
					else 
						$unit_rate_array[$today] = $unit_rate;
				}				
			}
		}
		else if($viewmode == "2")
		{
			$moneyout_sql = " IFNULL(SUM(IF(MODE!=26, moneyout, 0)), 0) AS moneyout";
				
			if($mode == 26)
				$moneyout_sql = " IFNULL(SUM(IF(MODE=26, moneyout, 0)), 0) AS moneyout";
			
			
			$sql = "SELECT slottype, betlevel, IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, $moneyout_sql, LEFT(writedate, 10) AS writedate, IFNULL(SUM(usercount),0) AS usercount  ".
					"FROM $table ".
					"WHERE $modestr AND $total_betlevel AND betlevel=$betlevel AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate, betlevel ORDER BY writedate DESC, betlevel ASC";
		}
		
		$action_list = $db_main2->gettotallist($sql);
		
		if($viewmode != "0" || $betlevel != "betlevel" && $mode != "mode")
			$jackpot_sql = "SELECT 0";
		else 
			$jackpot_sql = "SELECT IFNULL(SUM(amount), 0) FROM tbl_jackpot_stat_daily WHERE $jackpot_betlevel AND today='[WRITEDATE]' AND devicetype=$os_type ";
		
		$usercount_sql = "SELECT  IFNULL(SUM(totalcount), 0) FROM user_online_game_log WHERE writedate BETWEEN '[WRITEDATE] 00:00:00' AND '[WRITEDATE] 23:59:59'";		
		$usercount_daily_sql = "SELECT IFNULL(SUM(totalcount), 0) FROM user_online_game_log_daily WHERE today='[WRITEDATE]'";	
	}	
	else 
	{		
		if($viewmode == "0")
		{
			$moneyout_sql = " IFNULL(SUM(IF(MODE!=26, moneyout, 0)), 0) AS moneyout";
			
			if($mode == 26)
				$moneyout_sql = " IFNULL(SUM(IF(MODE=26, moneyout, 0)), 0) AS moneyout";
			
			$sql = "SELECT slottype, betlevel, MODE, IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, $moneyout_sql, LEFT(writedate, 10) AS writedate, IFNULL(SUM(usercount),0) AS usercount  ".
					"FROM $table ".
					"WHERE slottype=$tab AND $total_betlevel AND betlevel=$betlevel AND $modestr AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate ORDER BY writedate DESC";
			
			if($mode == "mode" && $total_mode == 0)
			{
				$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
							"FROM $table ".
							"WHERE MODE NOT IN (9, 26, 29, 30) AND slottype=$tab AND writedate BETWEEN '[WRITEDATE] 00:00:00' AND '[WRITEDATE] 23:59:59'";
			}			
			else if($mode == "mode" && $total_mode == 1)
			{
				$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
							"FROM $table ".
							"WHERE MODE NOT IN (9, 26, 29, 30) AND slottype=$tab AND $total_betlevel AND writedate BETWEEN '[WRITEDATE] 00:00:00' AND '[WRITEDATE] 23:59:59'";
			}
			else if($mode == "mode" && $total_mode == 2)
			{
				$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
							"FROM $table ".
							"WHERE MODE NOT IN (9, 26, 29, 30) AND slottype=$tab AND $total_betlevel AND writedate BETWEEN '[WRITEDATE] 00:00:00' AND '[WRITEDATE] 23:59:59'";
			}
			else if($mode == "mode" && $total_mode == 3)
			{
				$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
							"FROM $table ".
							"WHERE MODE NOT IN (9, 26, 29, 30) AND slottype=$tab AND $total_betlevel AND writedate BETWEEN '[WRITEDATE] 00:00:00' AND '[WRITEDATE] 23:59:59'";
			}
			else if($mode == "mode" && $total_mode == 4)
			{
				$treat_sql = "SELECT IFNULL(SUM(treatamount),0) AS treatamount ".
							"FROM $table ".
							"WHERE MODE NOT IN (9, 26, 29, 30) AND slottype=$tab AND $total_betlevel AND writedate BETWEEN '[WRITEDATE] 00:00:00' AND '[WRITEDATE] 23:59:59'";
			}
			
			if($mode == 0  || $mode == 25 || $mode == 28 || $mode == "mode")
			{
				//가중 승률
				if($mode == "mode")
					$palycount_mode = " AND MODE NOT IN (4, 9, 26, 29) ";
				else if($mode == 0)
					$palycount_mode = " AND MODE IN (0,31) ";
				else if($mode == 25)
					$palycount_mode = " AND MODE = 25 ";
				else if($mode == 28)
					$palycount_mode = " AND MODE = 28 ";
				
// 				$rate_playcount_sql = "SELECT writedate, ROUND(SUM(rate_playcount)/ SUM(sum_playcount), 2) AS rate_playcount ".
// 						"FROM	".
// 						"(	".
// 						"	SELECT writedate, ".
// 						"	ROUND(SUM(moneyout)/SUM(moneyin)*100, 2), ".
// 						"	ROUND((ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) * SUM(playcount)),0) AS rate_playcount, ".
// 						"	SUM(playcount) AS sum_playcount ".
// 						"	FROM `$table` ".
// 						"	WHERE slottype=$tab AND $total_betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $palycount_mode ".
// 						"	GROUP BY writedate, betlevel ".
// 						") t1 GROUP BY writedate";
			
// 				$rate_playcount_list = $db_main2->gettotallist($rate_playcount_sql);
					
// 				$rate_playcount_array = array();
			
// 				for($e = 0; $e < sizeof($rate_playcount_list); $e++)
// 				{
// 					$today = $rate_playcount_list[$e]["writedate"];
// 					$rate_playcount = $rate_playcount_list[$e]["rate_playcount"];
						
// 					$rate_playcount_array[$today] = $rate_playcount;
// 				}
			
// 				$total_rate_playcount = "SELECT ROUND(SUM(rate_playcount)/ SUM(sum_playcout), 2) ".
// 						"FROM	".
// 						"(	".
// 						"	SELECT ".
// 						"	ROUND((ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) * SUM(playcount)),0) AS rate_playcount, ".
// 						"	SUM(playcount) AS sum_playcout ".
// 						"	FROM `$table` ".
// 						"	WHERE slottype=$tab AND $total_betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $palycount_mode ".
// 						"	GROUP BY betlevel ".
// 						") t1;";
// 				$total_rate_playcount_value = $db_main2->getvalue($total_rate_playcount);
				
				// unit 승률
				$unit_rate_sql = "SELECT writedate, ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2) AS unit_rate_new, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unit_rate ".
								"FROM	".
								"	(	".
								"		SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout,SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout	".
								"		FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	". 
								"		WHERE t1.slottype=$tab AND $total_unit_betlevel AND writedate BETWEEN '$startdate' AND '$enddate' $palycount_mode ".
								"		GROUP BY slottype, betlevel, writedate	".
								") t3 GROUP BY writedate;";
				$unit_rate_list = $db_main2->gettotallist($unit_rate_sql);
					
				$unit_rate_array = array();
				
				for($e = 0; $e < sizeof($unit_rate_list); $e++)
				{
					$today = $unit_rate_list[$e]["writedate"];
					$unit_rate = $unit_rate_list[$e]["unit_rate"];
					$unit_rate_new = ($unit_rate_list[$e]["unit_rate_new"]=="")? 0 : $unit_rate_list[$e]["unit_rate_new"];
					
					if($today > '2017-11-20' && $os_type == 0)
						$unit_rate_array[$today] = $unit_rate_new;
					else 
						$unit_rate_array[$today] = $unit_rate;
				}
					
				if($enddate > '2017-11-20' && $os_type == 0)
				{
					$total_unit_rate_sql = "SELECT SUM(tt_moneyout) AS tt_moneyout, SUM(tt_moneyin) AS tt_moneyin ".
											"FROM	".
											"(	".
											"	SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout	".
											"	FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	". 
											"	WHERE t1.slottype=$tab AND $total_unit_betlevel  AND writedate BETWEEN '$startdate' AND '2017-11-20' $palycount_mode".
											"	GROUP BY t1.slottype, betlevel	".
											") t3";
					$total_unit_rate_arr = $db_main2->getarray($total_unit_rate_sql);
					$total_unit_rate_moneyin =$total_unit_rate_arr["tt_moneyin"];
					$total_unit_rate_moneyout =$total_unit_rate_arr["tt_moneyout"];
					
					$total_unit_rate_new_sql=" SELECT SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout ".
						  					 " FROM ( ".
											 "	  SELECT t1.slottype, betlevel, SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout ".
										 	 "      FROM   $table t1   ".
										 	 "      WHERE  t1.slottype=$tab AND $total_unit_betlevel  AND writedate BETWEEN '2017-11-20' AND '$enddate' ". 
											 " AND MODE NOT IN ( 4, 9, 26, 29 ) GROUP  BY t1.slottype, betlevel ) t3 ";
					$total_unit_rate_new_arr = $db_main2->getarray($total_unit_rate_sql);
					$total_unit_rate_moneyin += ($total_unit_rate_new_arr["unit_moneyin"]=="")?0:$total_unit_rate_new_arr["unit_moneyin"];
					$total_unit_rate_moneyout += ($total_unit_rate_new_arr["unit_moneyout"]=="")?0:$total_unit_rate_new_arr["unit_moneyout"];
						
				}
				else 
				{
					$total_unit_rate_sql = "SELECT SUM(tt_moneyout) AS tt_moneyout, SUM(tt_moneyin) AS tt_moneyin ".
							"FROM	".
							"(	".
							"	SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout	".
							"	FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
							"	WHERE t1.slottype=$tab AND $total_unit_betlevel  AND writedate BETWEEN '$startdate' AND '$enddate' $palycount_mode".
							"	GROUP BY t1.slottype, betlevel	".
							") t3";
					$total_unit_rate_arr = $db_main2->getarray($total_unit_rate_sql);
					$total_unit_rate_moneyin =$total_unit_rate_arr["tt_moneyin"];
					$total_unit_rate_moneyout =$total_unit_rate_arr["tt_moneyout"];
				}
				
				$total_unit_rate_value = round($total_unit_rate_moneyout/$total_unit_rate_moneyin*100,2);
			}
			
			if($mode == 26)
			{
				$mode_26_slot_check = $db_main2->getvalue("SELECT COUNT(*) FROM $table WHERE mode=26 AND slottype = $tab");
				
				if($mode_26_slot_check > 0)
				{
					$mode_26_startdate =  $db_main2->getvalue("SELECT MIN(writedate) FROM $table WHERE mode=26 AND $total_betlevel AND $startdate <= writedate");
					
					$mode_total_sql = "SELECT IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout),0) AS moneyout, LEFT(writedate, 10) AS writedate ".
							"FROM $table WHERE mode IN (26) AND slottype=$tab AND $total_betlevel AND writedate BETWEEN '$mode_26_startdate' AND '$enddate' GROUP BY writedate ORDER BY writedate DESC";
				
					$mode_total_list = $db_main2->gettotallist($mode_total_sql);
						
					$mode_total_array = array();
				
					$mode_total_sum_moneyin = 0;
					$mode_total_sum_moneyout = 0;
				
					for($e = 0; $e < sizeof($mode_total_list); $e++)
					{
						$mode_total_today = $mode_total_list[$e]["writedate"];
						$mode_total_moneyin = $mode_total_list[$e]["moneyin"];
						$mode_total_moneyout = $mode_total_list[$e]["moneyout"];
				
						$mode_total_array[$mode_total_today]["moneyin"] = $mode_total_moneyin;
						$mode_total_array[$mode_total_today]["moneyout"] = $mode_total_moneyout;
																
						$mode_total_sum_moneyin += $mode_total_moneyin;
						$mode_total_sum_moneyout += $mode_total_moneyout;
					}
				}
			}
		}
		else if($viewmode == "1")
		{
			if($mode == "0" || $mode == "mode")
			{
				$sql = "SELECT slottype, betlevel, REPLACE(MODE,31,0) AS MODE, IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout), 0) AS moneyout, LEFT(writedate, 10) AS writedate ".
						" FROM $table ".
						" WHERE $modestr AND $total_betlevel AND slottype = $tab AND betlevel=$betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY writedate, REPLACE(MODE,31,0) ".
						" UNION ALL".
						" SELECT slottype, betlevel, MODE, IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout), 0) AS moneyout, LEFT(writedate, 10) AS writedate ".
						" FROM $table ".
						" WHERE MODE = 31 AND $total_betlevel  AND slottype=$tab AND betlevel=$betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY writedate, MODE ORDER BY writedate DESC, mode ASC ";
				
			}
			else if($mode == "3027")
			{
				$sql = "SELECT slottype, betlevel, IF(MODE IN(31,27),3027,0) AS MODE, IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout), 0) AS moneyout, LEFT(writedate, 10) AS writedate ".
						"FROM $table ".
						"WHERE $modestr AND $total_betlevel AND slottype=$tab AND betlevel=$betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY writedate, IF(MODE IN(31,27),3027,0) ORDER BY writedate DESC, mode ASC ";
			}
			else if($mode == "31")
			{
				$sql = " SELECT slottype, betlevel, MODE, IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout), 0) AS moneyout, LEFT(writedate, 10) AS writedate ".
						" FROM $table ".
						" WHERE MODE = 31 AND $total_betlevel AND slottype=$tab AND betlevel=$betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY writedate, MODE ORDER BY writedate DESC, mode ASC ";
			}
			else 
			{
				$sql = "SELECT slottype, betlevel, REPLACE(MODE,31,0) AS MODE, IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout), 0) AS moneyout, LEFT(writedate, 10) AS writedate ".
						"FROM $table ".
						"WHERE $modestr AND $total_betlevel AND slottype=$tab AND betlevel=$betlevel AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY writedate, REPLACE(MODE,31,0) ORDER BY writedate DESC, mode ASC ";
			}
			
			if($mode == 0 && $mode != "mode")
			{
				//가중 승률
// 				$palycount_mode = " AND MODE = 0 ";
				
// 				$rate_playcount_sql = "SELECT writedate, ROUND(SUM(rate_playcount)/ SUM(sum_playcount), 2) AS rate_playcount ".
// 						"FROM	".
// 						"(	".
// 						"	SELECT writedate, ".
// 						"	ROUND(SUM(moneyout)/SUM(moneyin)*100, 2), ".
// 						"	ROUND((ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) * SUM(playcount)),0) AS rate_playcount, ".
// 						"	SUM(playcount) AS sum_playcount ".
// 						"	FROM `$table` ".
// 						"	WHERE slottype=$tab AND $total_betlevel AND $modestr AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $palycount_mode ".
// 						"	GROUP BY writedate, betlevel ".
// 						") t1 GROUP BY writedate";
// 				$rate_playcount_list = $db_main2->gettotallist($rate_playcount_sql);
			
// 				$rate_playcount_array = array();
					
// 				for($e = 0; $e < sizeof($rate_playcount_list); $e++)
// 				{
// 					$today = $rate_playcount_list[$e]["writedate"];
// 					$rate_playcount = $rate_playcount_list[$e]["rate_playcount"];
			
// 					$rate_playcount_array[$today] = $rate_playcount;
// 				}
				
// 				$total_rate_playcount = "SELECT ROUND(SUM(rate_playcount)/ SUM(sum_playcout), 2) ".
// 						"FROM	".
// 						"(	".
// 						"	SELECT ".
// 						"	ROUND((ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) * SUM(playcount)),0) AS rate_playcount, ".
// 						"	SUM(playcount) AS sum_playcout ".
// 						"	FROM `$table` ".
// 						"	WHERE slottype=$tab AND $total_betlevel AND $modestr AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $palycount_mode ".
// 						"	GROUP BY betlevel ".
// 						") t1;";
// 				$total_rate_playcount_value = $db_main2->getvalue($total_rate_playcount);
				
				// unit 승률
				$unit_rate_sql = "SELECT writedate, ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2) AS unit_rate_new, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unit_rate ".
								"FROM	".
								"	(	".
								"		SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout,SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout	".
								"		FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	". 
								"		WHERE  t1.slottype=$tab AND $total_unit_betlevel  AND writedate BETWEEN '$startdate' AND '$enddate' $palycount_mode ".
								"		GROUP BY t1.slottype, betlevel, writedate	".
								") t3 GROUP BY writedate;";
				$unit_rate_list = $db_main2->gettotallist($unit_rate_sql);
					
				$unit_rate_array = array();
				
				for($e = 0; $e < sizeof($unit_rate_list); $e++)
				{
					$today = $unit_rate_list[$e]["writedate"];
					$unit_rate = $unit_rate_list[$e]["unit_rate"];
					$unit_rate_new = ($unit_rate_list[$e]["unit_rate_new"]=="")? 0 : $unit_rate_list[$e]["unit_rate_new"];
					
					if($today > '2017-11-20' && $os_type == 0)
						$unit_rate_array[$today] = $unit_rate_new;
					else 
						$unit_rate_array[$today] = $unit_rate;
				}
					
				if($enddate > '2017-11-20' && $os_type == 0)
				{
					$total_unit_rate_sql = "SELECT SUM(tt_moneyout) AS tt_moneyout, SUM(tt_moneyin) AS tt_moneyin ".
											"FROM	".
											"(	".
											"	SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout	".
											"	FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	". 
											"	WHERE  t1.slottype=$tab AND $total_unit_betlevel AND writedate BETWEEN '$startdate' AND '2017-11-20' $palycount_mode".
											"	GROUP BY t1.slottype, betlevel	".
											") t3";
					$total_unit_rate_arr = $db_main2->getarray($total_unit_rate_sql);
					$total_unit_rate_moneyin =$total_unit_rate_arr["tt_moneyin"];
					$total_unit_rate_moneyout =$total_unit_rate_arr["tt_moneyout"];
					
					$total_unit_rate_new_sql=" SELECT SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout ".
						  					 " FROM ( ".
											 "	  SELECT t1.slottype, betlevel, SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout ".
										 	 "      FROM   $table t1   ".
										 	 "      WHERE  t1.slottype=$tab AND $total_unit_betlevel  AND writedate BETWEEN '2017-11-20' AND '$enddate' ". 
											 " AND MODE NOT IN ( 4, 9, 26, 29 ) GROUP  BY t1.slottype, betlevel ) t3 ";
					$total_unit_rate_new_arr = $db_main2->getarray($total_unit_rate_sql);
					$total_unit_rate_moneyin += ($total_unit_rate_new_arr["unit_moneyin"]=="")?0:$total_unit_rate_new_arr["unit_moneyin"];
					$total_unit_rate_moneyout += ($total_unit_rate_new_arr["unit_moneyout"]=="")?0:$total_unit_rate_new_arr["unit_moneyout"];
						
				}
				else 
				{
					$total_unit_rate_sql = "SELECT SUM(tt_moneyout) AS tt_moneyout, SUM(tt_moneyin) AS tt_moneyin ".
							"FROM	".
							"(	".
							"	SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout	".
							"	FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
							"	WHERE  t1.slottype=$tab AND $total_unit_betlevel AND writedate BETWEEN '$startdate' AND '$enddate' $palycount_mode".
							"	GROUP BY t1.slottype, betlevel	".
							") t3";
					$total_unit_rate_arr = $db_main2->getarray($total_unit_rate_sql);
					$total_unit_rate_moneyin =$total_unit_rate_arr["tt_moneyin"];
					$total_unit_rate_moneyout =$total_unit_rate_arr["tt_moneyout"];
				}
				
				$total_unit_rate_value = round($total_unit_rate_moneyout/$total_unit_rate_moneyin*100,2);
			}
		}
		else if($viewmode == "2")
		{
			$moneyout_sql = " IFNULL(SUM(IF(MODE!=26, moneyout, 0)), 0) AS moneyout";
				
			if($mode == 26)
				$moneyout_sql = " IFNULL(SUM(IF(MODE=26, moneyout, 0)), 0) AS moneyout";
			
			$sql = "SELECT slottype, betlevel, IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, $moneyout_sql, LEFT(writedate, 10) AS writedate, IFNULL(SUM(usercount),0) AS usercount  ".
					"FROM $table ".
					"WHERE slottype=$tab AND $total_betlevel AND $modestr AND betlevel=$betlevel AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate, betlevel ORDER BY writedate DESC, betlevel ASC";
		}	
		write_log($sql);
		$action_list = $db_main2->gettotallist($sql);
	
		if($viewmode != "0" || $betlevel != "betlevel"  && $mode != "mode")
			$jackpot_sql = "SELECT 0";
		else
			$jackpot_sql = "SELECT IFNULL(SUM(amount), 0) FROM tbl_jackpot_stat_daily WHERE $jackpot_betlevel AND devicetype = $os_type AND today='[WRITEDATE]' AND devicetype=$os_type  AND slottype=$tab ";
	
		$usercount_sql = "SELECT  IFNULL(SUM(totalcount), 0) FROM $online_log_table gameslottype=$tab AND writedate BETWEEN '[WRITEDATE] 00:00:00' AND '[WRITEDATE] 23:59:59'";		
		$usercount_daily_sql = "SELECT IFNULL(SUM(totalcount), 0) FROM $online_log_daily_table gameslottype=$tab AND today='[WRITEDATE]'";
	}
	
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    
    function check_sleeptime()
    {
        //setTimeout("window.location.reload(false)",60000);
    }

    function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
    
    function tab_change(tab)
    {
        var search_form = document.search_form;
        search_form.tab.value = tab;
        search_form.submit();
    }
    
    $(function() {
        $("#startdate").datepicker({ });
    });

    $(function() {
        $("#enddate").datepicker({ });
    });
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
			<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
			<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
			<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
			<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
		</span>
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 게임 활동 추이(<?= $os_txt ?>) - 일별 통계</div>
			<input type="hidden" name="term" id="term" value="<?= $term ?>" />
			<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />   
			<div class="search_box">
				<select name="total_mode" id="total_mode">										
					<option value="0" <?= ($total_mode=="0") ? "selected" : "" ?>>전체</option>
					<option value="1" <?= ($total_mode=="1") ? "selected" : "" ?>>일반</option>                       
					<option value="2" <?= ($total_mode=="2") ? "selected" : "" ?>>하이롤러(싱글X)</option>					
					<option value="3" <?= ($total_mode=="3") ? "selected" : "" ?>>싱글모드</option>					
					<option value="4" <?= ($total_mode=="4") ? "selected" : "" ?>>하이롤러(싱글포함)</option>					
				</select>&nbsp;
				<input type="radio" value="0" name="viewmode" id="viewmode_0" <?= ($viewmode == "0") ? "checked=\"true\"" : ""?> /> 전체 
				<input type="radio" value="1" name="viewmode" id="viewmode_1" <?= ($viewmode == "1") ? "checked=\"true\"" : ""?> /> MODE
				<input type="radio" value="2" name="viewmode" id="viewmode_2" <?= ($viewmode == "2") ? "checked=\"true\"" : ""?> /> 베팅레벨&nbsp;
				<select name="mode" id="mode">
					<option value="mode" <?= ($mode=="mode") ? "selected" : "" ?>>Mode - 전체</option>					
					<option value="0" <?= ($mode=="0") ? "selected" : "" ?>>일반</option>
					<!-- <option value="3" <?= ($mode=="3") ? "selected" : "" ?>>트리트금액</option>  -->                       
					<option value="4" <?= ($mode=="4") ? "selected" : "" ?>>튜토리얼</option>
					<!--<option value="6" <?= ($mode=="6") ? "selected" : "" ?>>개인승률부양6(데일리 로그인 카운트 7이하,코인 300백만 미만)</option>
					<option value="7" <?= ($mode=="7") ? "selected" : "" ?>>개인승률부양7(30일 이탈자,결제 30달러 미만,코인 300백만 미만)</option>
					<option value="8" <?= ($mode=="8") ? "selected" : "" ?>>개인승률부양8(30일 이탈자,결제 30달러 이상)</option>  -->
					<option value="9" <?= ($mode=="9") ? "selected" : "" ?>>울트라프리스핀</option>
					<option value="11" <?= ($mode=="11") ? "selected" : "" ?>>승률 부양11(신규사용자)</option>
					<option value="12" <?= ($mode=="12") ? "selected" : "" ?>>승률 부양12($18 이상 첫결제)</option>
					<!-- <option value="13" <?= ($mode=="13") ? "selected" : "" ?>>승률 부양13($5 이상 결제시, $1이상 결제 횟수 2회 이상 5회 미만 시)</option>
					<option value="14" <?= ($mode=="14") ? "selected" : "" ?>>승률 부양14($9 이상 결제시, $1이상 결제 횟수 5회 이상 10회 미만 시)</option>
					<option value="15" <?= ($mode=="15") ? "selected" : "" ?>>승률 부양15($19 이상 결제시, $1이상 결제 횟수 10회 이상 시 )</option>
					<option value="16" <?= ($mode=="16") ? "selected" : "" ?>>승률 부양16(신규사용자(dailycount 4~6))</option> -->
					<option value="17" <?= ($mode=="17") ? "selected" : "" ?>>승률 부양17(재방문자)</option>
					<option value="18" <?= ($mode=="18") ? "selected" : "" ?>>승률 부양18(Platinum)</option>
					<option value="19" <?= ($mode=="19") ? "selected" : "" ?>>승률 부양19($5 이상 재결제, 1차)</option>
					<option value="20" <?= ($mode=="20") ? "selected" : "" ?>>승률 부양20($5 이상 재결제, 2차)</option>
					<!-- <option value="21" <?= ($mode=="21") ? "selected" : "" ?>>승률 부양21(웨일급 일탈감지자)</option>
					<option value="22" <?= ($mode=="22") ? "selected" : "" ?>>승률 부양22(최저 승률 기준(65%))</option> -->
					<option value="23" <?= ($mode=="23") ? "selected" : "" ?>>승률 부양23(Lucky Offer, 1차)</option>
					<option value="24" <?= ($mode=="24") ? "selected" : "" ?>>승률 부양24(Lucky Offer, 2차)</option>
					<option value="25" <?= ($mode=="25") ? "selected" : "" ?>>승률 부양25(베팅금액별 트래킹(비결제자))</option>
					<option value="28" <?= ($mode=="28") ? "selected" : "" ?>>승률 부양28(베팅금액별 트래킹(결제자))</option>
					<option value="26" <?= ($mode=="26") ? "selected" : "" ?>>승률 부양26(Multi Jackpot)</option>
					<option value="27" <?= ($mode=="27") ? "selected" : "" ?>>승률 부양27(홀수 승률 부양)</option>
					<option value="29" <?= ($mode=="29") ? "selected" : "" ?>>승률 부양29(DailyFreeSpin)</option>
					<option value="30" <?= ($mode=="30") ? "selected" : "" ?>>승률 부양30(DailyFreeSpin Bonus)</option>
					<option value="31" <?= ($mode=="31") ? "selected" : "" ?>>승률 부양31(홀수 승률 부양 중 미적용)</option>
					<option value="3027" <?= ($mode=="3027") ? "selected" : "" ?>>승률 부양27+31(홀수 승률 부양 합계)</option>
				</select>&nbsp;
				<select name="betlevel" id="betlevel">
					<option value="betlevel" <?= ($bettype=="betlevel") ? "selected" : "" ?>>베팅레벨 - 전체</option>
<?
	for ($i=0; $i<sizeof($betlist); $i++)
	{
		$bettype = $betlist[$i]["bettype"];
		$betname = $betlist[$i]["betname"];
?>	
					<option value="<?= $bettype ?>" <?= ($bettype == $betlevel) ? "selected=\"true\"" : "" ?>><?= $betname ?></option>
<?						
	}
?>						
				</select>
				<input type="hidden" name="term" id="term" value="<?= $term ?>" />
				<input type="hidden" name="tab" id="tab" value="<?= $tab ?>" />
				<input type="input" class="search_text" id="startdate" name="startdate" style="width:65px" value="<?= $startdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
				<input type="input" class="search_text" id="enddate" name="enddate" style="width:65px" value="<?= $enddate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
	</div>
	<br><br>
		
		<!-- //title_warp -->
               
		<ul class="tab">
<?
	for ($i=0; $i<sizeof($slotlist); $i++)
	{
		$slottype1 = $slotlist[$i]["slottype"];
		$slotname1 = str_replace(" Slot", "", $slotlist[$i]["slotname"]);
?>	
			<li id="tab_<?= $slottype1 ?>" class="<?= ($tab == $slottype1) ? "select" : "" ?>" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('<?= $slottype1 ?>')"><?= $slotname1 ?></li>
<?
	}
?>                
		</ul>
		<table class="tbl_list_basic1"  style="width:1300px">
<?
	if ($tab == "0")
	{
		if($mode != 26)
		{
?>
			<colgroup>
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="110">
<?					
			if($viewmode == "0")
			{
?>							
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
<?
				if($mode == "mode")
				{
?>
					<col width="100">
					<col width="100">
<?
				}
			}
?>
				<col width="100">
				<col width="100">
				<col width="70">
				<col width="70">
			
<?
		}
		else if($mode == 26)
		{			
?>
			<colgroup>
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="70">
				<col width="100">

<?
		}
	}
	else 
	{
		if($mode == 26 && $mode_26_slot_check > 0)
		{
?>
			<colgroup>
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="70">
				<col width="100">
<?
		}
		else 
		{
?>
			<colgroup>
				<col width="100">
				<col width="120">
				<col width="120">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
<? 
			if($viewmode == "0")
			{
?>
				<col width="100">				
<?
			}		
		}
	}
?>
                    
                </colgroup>
                <thead>
                    <tr>
                        <th style="border-top:none;">날짜</th>
<?	
	if ($tab == "0")
	{
		if($viewmode == "0")
		{
			if($mode != 26)
			{	
?>
						<th style="text-align:right;border-top:none;">money_in(A)</th>
                        <th style="text-align:right;border-top:none;">money_out(B)</th>
<?
				if($mode == 0 || $mode == "mode")
				{			
?>
                        <th style="text-align:right;border-top:none;">jackpot(C)</th>
<?
				}
?>
                        <th style="text-align:right;border-top:none;">게임 이익</th>
<?
				if($mode == "mode" && $total_mode != 3)
				{
?>
                        <th style="text-align:right;border-top:none;">트리트 금액(D)</th>
<?
					if($betlevel == "betlevel" && $total_mode == 0)
					{
?>
						<th style="text-align:right;border-top:none;">트리트 환전(E)</th>
<?
					}
				}
			
				if($mode == 0 || $mode == 25 || $mode == 28  || $mode == "mode")
				{				
?>
						<!-- <th style="border-top:none;">가중 승률</th> -->
						<th style="border-top:none;">단위 승률</th>
<? 
				} 
?>
						<th style="border-top:none;">승률<br>(B/A)</th>  
<?
				if($mode == 0 || $mode == "mode")
				{			
?>                                          
                        <th style="border-top:none;">승률<br>((B+C)/A)</th>
<?
				}
			
				if($mode == "mode")
				{
?>                        
                        <th style="border-top:none;">승률<br>(B-D)/A)</th>
<?
					if($betlevel == "betlevel" && $total_mode == 0)
					{
?>
						<th style="border-top:none;">승률<br>(B-D+E)/A)</th>
<?
					}
				}
?>
                        <th style="border-top:none;">게임횟수</th>
                        <th style="border-top:none;">유저수</th>
			
<?
			}
			else if($mode == 26)
			{
?>
				<th style="text-align:right;border-top:none;">money_in(A)</th>
                <th style="text-align:right;border-top:none;">money_out(B)</th>
                <th style="text-align:right;border-top:none;">money_multi_out(C)</th>
               	<th style="border-top:none;">승률<br>(B/A)</th>
               	<th style="border-top:none;">승률<br>(B-C/A)</th>
               	<th style="border-top:none;">승률 차이</th>
               	<th style="border-top:none;">유저수</th>

<?
			}
		}
		else if($viewmode == "1")
		{		
?>
						<th style="text-align:right;border-top:none;">MODE</th>
						<th style="text-align:right;border-top:none;">money_in(A)</th>
                        <th style="text-align:right;border-top:none;">money_out(B)</th>
<?
			if($mode == 0  && $mode != "mode")
			{
?>
                        <th style="border-top:none;">가중 승률</th>
                        <th style="border-top:none;">단위 승률</th>
<?
			}
?>
                        <th style="border-top:none;">승률<br/>(B/A)</th>
                        <th style="border-top:none;">게임횟수</th>
                        <th style="border-top:none;">유저수</th>
	

<?
		}
		else if($viewmode == "2")
		{
?>
						<th style="text-align:right;border-top:none;">베팅레벨</th>
						<th style="text-align:right;border-top:none;">money_in(A)</th>
                        <th style="text-align:right;border-top:none;">money_out(B)</th>		                        
                        <th style="border-top:none;">승률<br/>(B/A)</th>
                        <th style="border-top:none;">게임횟수</th>
                        <th style="border-top:none;">유저수</th>
<?
		}
	}
	else
	{
		if($viewmode == "0")
		{
			if($mode == 26 && $mode_26_slot_check > 0)
			{
?>
				<th style="text-align:right;border-top:none;">money_in(A)</th>
                <th style="text-align:right;border-top:none;">money_out(B)</th>
                <th style="text-align:right;border-top:none;">money_multi_out(C)</th>
               	<th style="border-top:none;">승률<br>(B/A)</th>
               	<th style="border-top:none;">승률<br>(B-C/A)</th>
               	<th style="border-top:none;">승률 차이</th>
               	<th style="border-top:none;">유저수</th>
						
<?
			}
			else
			{
?>
						<th style="text-align:right;border-top:none;">money_in(A)</th>
                        <th style="text-align:right;border-top:none;">money_out(B)</th>
<?
				if($mode == 0 || $mode == "mode")
				{			
?>
                        <th style="text-align:right;border-top:none;">jackpot(C)</th>
<?
				}

				if($mode == "mode" && $total_mode != 3)
				{
?>
                        <th style="text-align:right;border-top:none;">트리트 금액(D)</th>
<?
				}
			
				if($mode == 0   || $mode == 25 || $mode == 28 || $mode == "mode")
				{				
?>
						<!-- <th style="border-top:none;">가중 승률</th>-->
						<th style="border-top:none;">단위 승률</th>
<? 
				} 
?>
                        <th style="border-top:none;">승률(B/A)</th> 
<?
				if($mode == 0 || $mode == "mode")
				{			
?>                                           
                        <th style="border-top:none;">승률((B+C)/A)</th>
<?
				}
			
				if($mode == "mode")
				{
?>                        
                        <th style="border-top:none;">승률(B-D)/A)</th>
<?
				}
?>                         
                        <th style="border-top:none;">게임횟수</th>
                        <th style="border-top:none;">유저수</th>
<?
			}

		}
		else if($viewmode == "1")
		{
?>
						<th style="text-align:right;border-top:none;">MODE</th>
						<th style="text-align:right;border-top:none;">money_in(A)</th>
                        <th style="text-align:right;border-top:none;">money_out(B)</th>
<?                        
			if($mode == 0  && $mode != "mode")
			{				
?>
						<th style="border-top:none;">가중 승률</th>
						<th style="border-top:none;">단위 승률</th>
<? 
			} 
?>
                        <th style="border-top:none;">승률<br/>(B/A)</th>                        
                        <th style="border-top:none;">게임횟수</th>
                        <th style="border-top:none;">유저수</th>
<?
		}
		else if($viewmode == "2")
		{
?>
						<th style="text-align:right;border-top:none;">베팅레벨</th>
						<th style="text-align:right;border-top:none;">money_in(A)</th>
                        <th style="text-align:right;border-top:none;">money_out(B)</th>		                        
                        <th style="border-top:none;">승률<br/>(B/A)</th>
                        <th style="border-top:none;">게임횟수</th>
                        <th style="border-top:none;">유저수</th>
<?
		}
								
	}
?>
                    </tr>
                </thead>
                <tbody>
<?
    $list = $action_list;
    $datetmp = "";
    
    $total_moneyin = 0;
    $total_moneyout = 0;
    $total_jackpot = 0;
    $total_game_profit = 0;
    $total_playcount = 0;
    $total_usercount = 0;
    $total_treat = 0;
    $total_exchange_treat = 0;
    $total_rate1 = 0;
    $total_rate2 = 0;
    $total_rate3 = 0;
    $total_rate4 = 0;
    
    $total_normal_moneyin= 0;
    $total_normal_moneyout = 0;
    $total_normal_rate1 = 0;
    $total_normal_playcount = 0;
    $total_normal_usercount = 0;
    
    $total_treat_moneyin= 0;
    $total_treat_moneyout = 0;
    $total_treat_rate1 = 0;
    $total_treat_playcount = 0;
    $total_treat_usercount = 0;
    
    $total_tutorial_moneyin= 0;
    $total_tutorial_moneyout = 0;
    $total_tutorial_rate1 = 0;
    $total_tutorial_playcount = 0;
    $total_tutorial_usercount = 0;   

    $total_ultra_moneyin = 0;
    $total_ultra_moneyout = 0;
    $total_ultra_rate1 = 0;
    $total_ultra_playcount = 0;
    $total_ultra_usercount = 0;
    
    $total_user_winrate11_moneyin= 0;
    $total_user_winrate11_moneyout = 0;
    $total_user_winrate11_rate1 = 0;
    $total_user_winrate11_playcount = 0;
    $total_user_winrate11_usercount = 0;
    
    $total_user_winrate12_moneyin= 0;
    $total_user_winrate12_moneyout = 0;
    $total_user_winrate12_rate1 = 0;
    $total_user_winrate12_playcount = 0;
    $total_user_winrate12_usercount = 0;
    
    $total_user_winrate13_moneyin= 0;
    $total_user_winrate13_moneyout = 0;
    $total_user_winrate13_rate1 = 0;
    $total_user_winrate13_playcount = 0;
    $total_user_winrate13_usercount = 0;
    
    $total_user_winrate14_moneyin= 0;
    $total_user_winrate14_moneyout = 0;
    $total_user_winrate14_rate1 = 0;
    $total_user_winrate14_playcount = 0;
    $total_user_winrate14_usercount = 0;
    
    $total_user_winrate15_moneyin= 0;
    $total_user_winrate15_moneyout = 0;
    $total_user_winrate15_rate1 = 0;
    $total_user_winrate15_playcount = 0;
    $total_user_winrate15_usercount = 0;
    
    $total_user_winrate16_moneyin= 0;
    $total_user_winrate16_moneyout = 0;
    $total_user_winrate16_rate1 = 0;
    $total_user_winrate16_playcount = 0;
    $total_user_winrate16_usercount = 0;
    
    $total_user_winrate17_moneyin= 0;
    $total_user_winrate17_moneyout = 0;
    $total_user_winrate17_rate1 = 0;
    $total_user_winrate17_playcount = 0;
    $total_user_winrate17_usercount = 0;
    
    $total_user_winrate18_moneyin= 0;
    $total_user_winrate18_moneyout = 0;
    $total_user_winrate18_rate1 = 0;
    $total_user_winrate18_playcount = 0;
    $total_user_winrate18_usercount = 0;
    
    $total_user_winrate19_moneyin= 0;
    $total_user_winrate19_moneyout = 0;
    $total_user_winrate19_rate1 = 0;
    $total_user_winrate19_playcount = 0;
    $total_user_winrate19_usercount = 0;
    
    $total_user_winrate20_moneyin= 0;
    $total_user_winrate20_moneyout = 0;
    $total_user_winrate20_rate1 = 0;
    $total_user_winrate20_playcount = 0;
    $total_user_winrate20_usercount = 0;
    
    $total_user_winrate21_moneyin= 0;
    $total_user_winrate21_moneyout = 0;
    $total_user_winrate21_rate1 = 0;
    $total_user_winrate21_playcount = 0;
    $total_user_winrate21_usercount = 0;
    
    $total_user_winrate22_moneyin= 0;
    $total_user_winrate22_moneyout = 0;
    $total_user_winrate22_rate1 = 0;
    $total_user_winrate22_playcount = 0;
    $total_user_winrate22_usercount = 0;
    
    $total_user_winrate23_moneyin= 0;
    $total_user_winrate23_moneyout = 0;
    $total_user_winrate23_rate1 = 0;
    $total_user_winrate23_playcount = 0;
    $total_user_winrate23_usercount = 0;
    
    $total_user_winrate24_moneyin= 0;
    $total_user_winrate24_moneyout = 0;
    $total_user_winrate24_rate1 = 0;
    $total_user_winrate24_playcount = 0;
    $total_user_winrate24_usercount = 0;
    
    $total_user_winrate25_moneyin= 0;
    $total_user_winrate25_moneyout = 0;
    $total_user_winrate25_rate1 = 0;
    $total_user_winrate25_playcount = 0;
    $total_user_winrate25_usercount = 0;
    
    $total_user_winrate26_moneyin= 0;
    $total_user_winrate26_moneyout = 0;
    $total_user_winrate26_rate1 = 0;
    $total_user_winrate26_playcount = 0;
    $total_user_winrate26_usercount = 0;
    
    $total_user_winrate27_moneyin= 0;
    $total_user_winrate27_moneyout = 0;
    $total_user_winrate27_rate1 = 0;
    $total_user_winrate27_playcount = 0;
    $total_user_winrate27_usercount = 0;
    
    $total_user_winrate28_moneyin= 0;
    $total_user_winrate28_moneyout = 0;
    $total_user_winrate28_rate1 = 0;
    $total_user_winrate28_playcount = 0;
    $total_user_winrate28_usercount = 0;
    
    $total_user_winrate29_moneyin= 0;
    $total_user_winrate29_moneyout = 0;
    $total_user_winrate29_rate1 = 0;
    $total_user_winrate29_playcount = 0;
    $total_user_winrate29_usercount = 0;
    
    $total_user_winrate30_moneyin= 0;
    $total_user_winrate30_moneyout = 0;
    $total_user_winrate30_rate1 = 0;
    $total_user_winrate30_playcount = 0;
    $total_user_winrate30_usercount = 0;
    
    $total_user_winrate31_moneyin= 0;
    $total_user_winrate31_moneyout = 0;
    $total_user_winrate31_rate1 = 0;
    $total_user_winrate31_playcount = 0;
    $total_user_winrate31_usercount = 0;
    
    $total_user_winrate3027_moneyin= 0;
    $total_user_winrate3027_moneyout = 0;
    $total_user_winrate3027_rate1 = 0;
    $total_user_winrate3027_playcount = 0;
    $total_user_winrate3027_usercount = 0;
    
    for($k=0; $k<sizeof($betlist); $k++)
    {
    	
    	if($betlist[$k]["bettype"] > 100)
    	{
    		$total_betlist_name[$betlist[$k]["bettype"]] = $betlist[$k]["betname"];
    	}
    	else 
    	{
	    	$total_betlist_name[$k] = $betlist[$k]["betname"];
    	}
    	
    	$total_betlist_moneyin[$k] = 0;
    	$total_betlist_moneyout[$k] = 0;
    	$total_betlist_rate1[$k] = 0;
    	$total_betlist_playcount[$k] = 0;
    	$total_betlist_usercount[$k] = 0;
    }
    
    $bet_cnt = 0;
    
    $total_betlist_num = array();
    
    for ($j=0; $j<sizeof($list); $j++)
    {
        $writedate = $list[$j]["writedate"];
        $mode_data = $list[$j]["MODE"];
        $betlevel_data = $list[$j]["betlevel"];
        $slottype_data = $list[$j]["slottype"];
        $moneyin = $list[$j]["moneyin"];
        $moneyout = $list[$j]["moneyout"];
        $playcount = $list[$j]["playcount"];
        $usercount = $list[$j]["usercount"];
        
        if($tab == 0 && $viewmode == 0 && $mode == "mode" && $betlevel == "betlevel" && $total_mode == 0)
        {
        	//전체
        	$usercnt_sql = "SELECT usercount FROM tbl_game_distinct_usercnt WHERE os_type = $os_type AND cnt_type = 1 AND writedate = '$writedate'";
        	$usercount = $db_other->getvalue($usercnt_sql);
        }
        else if($tab == 0 && $viewmode == 0 && $mode == "mode" && $betlevel == "betlevel" && $total_mode > 0)
        {
        	//전체
        	$usercnt_sql = "SELECT usercount FROM tbl_game_betmode_distinct_usercnt WHERE os_type = $os_type AND betmode = $total_mode AND cnt_type = 1 AND writedate = '$writedate'";
        	$usercount = $db_other->getvalue($usercnt_sql);
        }
        else if($tab > 0 && $viewmode == 0 && $mode == "mode" && $betlevel == "betlevel" && $total_mode == 0)
       	{
       		// slottype 
        	$usercnt_sql = "SELECT usercount FROM tbl_game_distinct_usercnt WHERE os_type = $os_type AND cnt_type = 2 AND cnt_type_value = $slottype_data AND writedate = '$writedate'";
        	$usercount = $db_other->getvalue($usercnt_sql);
        }
        else if($tab > 0 && $viewmode == 0 && $mode == "mode" && $betlevel == "betlevel" && $total_mode > 0)
        {
        	// slottype
        	$usercnt_sql = "SELECT usercount FROM tbl_game_betmode_distinct_usercnt WHERE os_type = $os_type AND betmode = $total_mode AND cnt_type = 2 AND slottype = $slottype_data AND writedate = '$writedate'";
        	$usercount = $db_other->getvalue($usercnt_sql);
        }
        if($tab == 0 && $viewmode == 1 && $mode == "mode" && $betlevel == "betlevel")
        {
        	// mode
        	$usercnt_sql = "SELECT usercount FROM tbl_game_distinct_usercnt WHERE os_type = $os_type AND cnt_type = 3 AND cnt_type_value = $mode_data AND writedate = '$writedate'";
        	$usercount = $db_other->getvalue($usercnt_sql);
        }
        if($tab == 0 && $viewmode == 1 && $mode != "mode" && $betlevel == "betlevel")
        {
        	$usercnt_sql = "SELECT usercount FROM tbl_game_distinct_usercnt WHERE os_type = $os_type AND cnt_type = 3 AND cnt_type_value = $mode_data AND writedate = '$writedate'";
        	$usercount = $db_other->getvalue($usercnt_sql);
        }
        else if($tab == 0 && $viewmode == 2 && $mode == "mode" && $betlevel == "betlevel")
        {
        	//betlevel
        	$usercnt_sql = "SELECT usercount FROM tbl_game_distinct_usercnt WHERE os_type = $os_type AND cnt_type = 4 AND cnt_type_value = $betlevel_data AND writedate = '$writedate'";
        	$usercount = $db_other->getvalue($usercnt_sql);
        }
        else if($tab == 0 && $viewmode == 2 && $mode == "mode" && $betlevel != "betlevel")
        {
        	$usercnt_sql = "SELECT usercount FROM tbl_game_distinct_usercnt WHERE os_type = $os_type AND cnt_type = 4 AND cnt_type_value = $betlevel_data AND writedate = '$writedate'";
        	$usercount = $db_other->getvalue($usercnt_sql);
        }

        if($mode == 0 || $mode == "mode")
        	$jackpot = $db_analysis->getvalue(str_replace("[WRITEDATE]", $writedate, $jackpot_sql));

        /*
        if(date("Y-m-d", strtotime("-1 days")) < $writedate)
        {
        	$usercount = $db_analysis->getvalue(str_replace("[WRITEDATE]", $writedate, $usercount_sql));
        }
        else 
        {
        	$usercount = $db_analysis->getvalue(str_replace("[WRITEDATE]", $writedate, $usercount_daily_sql));
        } 
        */       
                        	
        $rate1 = ($moneyin == 0 ? 0:round(($moneyout) / $moneyin * 10000) / 100);
        $rate2 = ($moneyin == 0 ? 0:round(($moneyout+$jackpot) / $moneyin * 10000) / 100);
        
        $total_moneyin += $moneyin;
        $total_moneyout += $moneyout;
        $total_jackpot += $jackpot;
        $total_playcount += $playcount;
        $total_usercount += $usercount;    

        $total_rate1 = ($total_moneyin == 0 ? 0:round(($total_moneyout) / $total_moneyin * 10000) / 100);
        $total_rate2 = ($total_moneyin == 0 ? 0:round(($total_moneyout+$total_playcount) / $total_moneyin * 10000) / 100);
        

        if($viewmode == "0" && $mode == "mode")
        {
        	$treat_data = $db_main2->getvalue(str_replace("[WRITEDATE]", $writedate, $treat_sql));
        	 
        	$rate3 = ($moneyin == 0 ? 0:round(($moneyout - $treat_data) / $moneyin * 10000) / 100);
        	 
        	$total_treat += $treat_data;
        	 
        	$total_rate3 = ($total_moneyin == 0 ? 0:round(($total_moneyout-$total_treat) / $total_moneyin * 10000) / 100);
			
        	if($betlevel == "betlevel"  && $total_mode == 0)
        	{
        		$exchange_treat_data = $exchange_treat_array[$writedate];
        		
        		$rate4 =  ($moneyin == 0 ? 0:round(($moneyout - $treat_data+$exchange_treat_data) / $moneyin * 10000) / 100);
        		
        		$total_exchange_treat += $exchange_treat_data;
        		
        		$total_rate4 = ($total_moneyin == 0 ? 0:round(($total_moneyout-$total_treat+$total_exchange_treat) / $total_moneyin * 10000) / 100);
        	}
        }

        if ($rate2 <= "80" || $rate2 >= "120")
            $style = "style='color:#fb7878;font-weight:bold;'";
        else
            $style = "";
        
        if($mode_data == "0")
        {
        	$mode_str = "일반모드";
        	
        	$total_normal_moneyin += $moneyin;        	
        	$total_normal_moneyout += $moneyout;        	
        	$total_normal_playcount += $playcount;
        	$total_normal_usercount += $usercount;
        	
        	$total_normal_rate1 = ($total_normal_moneyin == 0 ? 0:round(($total_normal_moneyout) / $total_normal_moneyin * 10000) / 100);
        }
        else if($mode_data == "3")
        {
        	$mode_str = "트리트금액";
        	 
        	$total_treat_moneyin += $moneyin;
        	$total_treat_moneyout += $moneyout;
        	$total_treat_playcount += $playcount;
        	$total_treat_usercount += $usercount;
        	 
        	$total_treat_rate1 = ($total_treat_moneyin == 0 ? 0:round(($total_treat_moneyout) / $total_treat_moneyin * 10000) / 100);
        }
        else if($mode_data == "4")
        {
        	$mode_str = "튜토리얼";
        	
        	if($slottype_data == "5" || $slottype_data == "9")
        		$mode_str = "승률부양";
        	
        	$total_tutorial_moneyin += $moneyin;
        	$total_tutorial_moneyout += $moneyout;
        	$total_tutorial_playcount += $playcount;
        	$total_tutorial_usercount += $usercount;
        	
        	$total_tutorial_rate1 = ($total_tutorial_moneyin == 0 ? 0:round(($total_tutorial_moneyout) / $total_tutorial_moneyin * 10000) / 100);
        }		
        else if($mode_data == "9")
        {
        	$mode_str = "울트라프리스핀";
        
        	$total_ultra_moneyin += $moneyin;
        	$total_ultra_moneyout += $moneyout;
        	$total_ultra_playcount += $playcount;
        	$total_ultra_usercount += $usercount;
        
        	$total_ultra_rate1 = ($total_ultra_moneyin == 0 ? 0:round(($total_ultra_moneyout) / $total_ultra_moneyin * 10000) / 100);
        }
        else if($mode_data == "11")
        {
        	$mode_str = "신규사용자";
        
        	$total_user_winrate11_moneyin += $moneyin;
        	$total_user_winrate11_moneyout += $moneyout;
        	$total_user_winrate11_playcount += $playcount;
        	$total_user_winrate11_usercount += $usercount;
        
        	$total_user_winrate11_rate1 = ($total_user_winrate11_moneyin == 0 ? 0:round(($total_user_winrate11_moneyout) / $total_user_winrate11_moneyin * 10000) / 100);
        }
        else if($mode_data == "12")
        {
           	$mode_str = "$18이상 첫결제";
        
           	$total_user_winrate12_moneyin += $moneyin;
           	$total_user_winrate12_moneyout += $moneyout;
           	$total_user_winrate12_playcount += $playcount;
           	$total_user_winrate12_usercount += $usercount;
        
           	$total_user_winrate12_rate1 = ($total_user_winrate12_moneyin == 0 ? 0:round(($total_user_winrate12_moneyout) / $total_user_winrate12_moneyin * 10000) / 100);
        }
        else if($mode_data == "13")
        {
        	$mode_str = "승률부양13";
        
        	$total_user_winrate13_moneyin += $moneyin;
           	$total_user_winrate13_moneyout += $moneyout;
           	$total_user_winrate13_playcount += $playcount;
           	$total_user_winrate13_usercount += $usercount;
        
           	$total_user_winrate13_rate1 = ($total_user_winrate13_moneyin == 0 ? 0:round(($total_user_winrate13_moneyout) / $total_user_winrate13_moneyin * 10000) / 100);
        }
        else if($mode_data == "14")
        {
        	$mode_str = "승률부양14";
        
        	$total_user_winrate14_moneyin += $moneyin;
           	$total_user_winrate14_moneyout += $moneyout;
           	$total_user_winrate14_playcount += $playcount;
           	$total_user_winrate14_usercount += $usercount;
        
           	$total_user_winrate14_rate1 = ($total_user_winrate14_moneyin == 0 ? 0:round(($total_user_winrate14_moneyout) / $total_user_winrate14_moneyin * 10000) / 100);
        }
        else if($mode_data == "15")
        {
        	$mode_str = "승률부양15";
        
        	$total_user_winrate15_moneyin += $moneyin;
        	$total_user_winrate15_moneyout += $moneyout;
        	$total_user_winrate15_playcount += $playcount;
        	$total_user_winrate15_usercount += $usercount;
        
        	$total_user_winrate15_rate1 = ($total_user_winrate15_moneyin == 0 ? 0:round(($total_user_winrate15_moneyout) / $total_user_winrate15_moneyin * 10000) / 100);
        }
        else if($mode_data == "16")
        {
        	$mode_str = "승률부양16";
        
        	$total_user_winrate16_moneyin += $moneyin;
        	$total_user_winrate16_moneyout += $moneyout;
        	$total_user_winrate16_playcount += $playcount;
        	$total_user_winrate16_usercount += $usercount;
        
        	$total_user_winrate16_rate1 = ($total_user_winrate16_moneyin == 0 ? 0:round(($total_user_winrate16_moneyout) / $total_user_winrate16_moneyin * 10000) / 100);
        }
        else if($mode_data == "17")
        {
        	$mode_str = "재방문자";
        
        	$total_user_winrate17_moneyin += $moneyin;
        	$total_user_winrate17_moneyout += $moneyout;
        	$total_user_winrate17_playcount += $playcount;
        	$total_user_winrate17_usercount += $usercount;
        
        	$total_user_winrate17_rate1 = ($total_user_winrate17_moneyin == 0 ? 0:round(($total_user_winrate17_moneyout) / $total_user_winrate17_moneyin * 10000) / 100);
        }
        else if($mode_data == "18")
        {
        	$mode_str = "Platinum";
        
        	$total_user_winrate18_moneyin += $moneyin;
        	$total_user_winrate18_moneyout += $moneyout;
        	$total_user_winrate18_playcount += $playcount;
        	$total_user_winrate18_usercount += $usercount;
        
        	$total_user_winrate18_rate1 = ($total_user_winrate18_moneyin == 0 ? 0:round(($total_user_winrate18_moneyout) / $total_user_winrate18_moneyin * 10000) / 100);
        }
        else if($mode_data == "19")
        {
        	$mode_str = "승률부양19";
        
        	$total_user_winrate19_moneyin += $moneyin;
        	$total_user_winrate19_moneyout += $moneyout;
        	$total_user_winrate19_playcount += $playcount;
        	$total_user_winrate19_usercount += $usercount;
        
        	$total_user_winrate19_rate1 = ($total_user_winrate19_moneyin == 0 ? 0:round(($total_user_winrate19_moneyout) / $total_user_winrate19_moneyin * 10000) / 100);
        }
        else if($mode_data == "20")
        {
        	$mode_str = "승률부양20";
        
        	$total_user_winrate20_moneyin += $moneyin;
        	$total_user_winrate20_moneyout += $moneyout;
        	$total_user_winrate20_playcount += $playcount;
        	$total_user_winrate20_usercount += $usercount;
        
        	$total_user_winrate20_rate1 = ($total_user_winrate20_moneyin == 0 ? 0:round(($total_user_winrate20_moneyout) / $total_user_winrate20_moneyin * 10000) / 100);
        }
        else if($mode_data == "21")
        {
        	$mode_str = "승률부양21";
        
        	$total_user_winrate21_moneyin += $moneyin;
        	$total_user_winrate21_moneyout += $moneyout;
        	$total_user_winrate21_playcount += $playcount;
        	$total_user_winrate21_usercount += $usercount;
        
        	$total_user_winrate21_rate1 = ($total_user_winrate21_moneyin == 0 ? 0:round(($total_user_winrate21_moneyout) / $total_user_winrate21_moneyin * 10000) / 100);
        }
        else if($mode_data == "22")
        {
        	$mode_str = "승률부양22";
        
        	$total_user_winrate22_moneyin += $moneyin;
        	$total_user_winrate22_moneyout += $moneyout;
        	$total_user_winrate22_playcount += $playcount;
        	$total_user_winrate22_usercount += $usercount;
        
        	$total_user_winrate22_rate1 = ($total_user_winrate22_moneyin == 0 ? 0:round(($total_user_winrate22_moneyout) / $total_user_winrate22_moneyin * 10000) / 100);
        }
        else if($mode_data == "23")
        {
        	$mode_str = "승률부양23";
        
        	$total_user_winrate23_moneyin += $moneyin;
        	$total_user_winrate23_moneyout += $moneyout;
        	$total_user_winrate23_playcount += $playcount;
        	$total_user_winrate23_usercount += $usercount;
        
        	$total_user_winrate23_rate1 = ($total_user_winrate23_moneyin == 0 ? 0:round(($total_user_winrate23_moneyout) / $total_user_winrate23_moneyin * 10000) / 100);
        }
        else if($mode_data == "24")
        {
        	$mode_str = "승률부양24";
        
        	$total_user_winrate24_moneyin += $moneyin;
        	$total_user_winrate24_moneyout += $moneyout;
        	$total_user_winrate24_playcount += $playcount;
        	$total_user_winrate24_usercount += $usercount;
        
        	$total_user_winrate24_rate1 = ($total_user_winrate24_moneyin == 0 ? 0:round(($total_user_winrate24_moneyout) / $total_user_winrate24_moneyin * 10000) / 100);
        }
        else if($mode_data == "25")
        {
        	$mode_str = "승률부양25";
        
        	$total_user_winrate25_moneyin += $moneyin;
        	$total_user_winrate25_moneyout += $moneyout;
        	$total_user_winrate25_playcount += $playcount;
        	$total_user_winrate25_usercount += $usercount;
        
        	$total_user_winrate25_rate1 = ($total_user_winrate25_moneyin == 0 ? 0:round(($total_user_winrate25_moneyout) / $total_user_winrate25_moneyin * 10000) / 100);
        }
        else if($mode_data == "26")
        {
        	$mode_str = "Multi Jackpot";
        
        	$total_user_winrate26_moneyin += $moneyin;
        	$total_user_winrate26_moneyout += $moneyout;
        	$total_user_winrate26_playcount += $playcount;
        	$total_user_winrate26_usercount += $usercount;
        
        	$total_user_winrate26_rate1 = ($total_user_winrate26_moneyin == 0 ? 0:round(($total_user_winrate26_moneyout) / $total_user_winrate26_moneyin * 10000) / 100);
        }
        else if($mode_data == "27")
        {
        	$mode_str = "홀수 결제 승률 부양";
        
        	$total_user_winrate27_moneyin += $moneyin;
        	$total_user_winrate27_moneyout += $moneyout;
        	$total_user_winrate27_playcount += $playcount;
        	$total_user_winrate27_usercount += $usercount;
        
        	$total_user_winrate27_rate1 = ($total_user_winrate27_moneyin == 0 ? 0:round(($total_user_winrate27_moneyout) / $total_user_winrate27_moneyin * 10000) / 100);
        }
        else if($mode_data == "28")
        {
        	$mode_str = "승률부양28";
        
        	$total_user_winrate28_moneyin += $moneyin;
        	$total_user_winrate28_moneyout += $moneyout;
        	$total_user_winrate28_playcount += $playcount;
        	$total_user_winrate28_usercount += $usercount;
        
        	$total_user_winrate28_rate1 = ($total_user_winrate28_moneyin == 0 ? 0:round(($total_user_winrate28_moneyout) / $total_user_winrate28_moneyin * 10000) / 100);
        }
        else if($mode_data == "29")
        {
        	$mode_str = "DailyFreeSpin";
        
        	$total_user_winrate29_moneyin += $moneyin;
        	$total_user_winrate29_moneyout += $moneyout;
        	$total_user_winrate29_playcount += $playcount;
        	$total_user_winrate29_usercount += $usercount;
        
        	$total_user_winrate29_rate1 = ($total_user_winrate29_moneyin == 0 ? 0:round(($total_user_winrate29_moneyout) / $total_user_winrate29_moneyin * 10000) / 100);
        }
    	else if($mode_data == "30")
        {
        	$mode_str = "DailyFreeSpin Bonus";
        
        	$total_user_winrate30_moneyin += $moneyin;
        	$total_user_winrate30_moneyout += $moneyout;
        	$total_user_winrate30_playcount += $playcount;
        	$total_user_winrate30_usercount += $usercount;
        
        	$total_user_winrate30_rate1 = ($total_user_winrate30_moneyin == 0 ? 0:round(($total_user_winrate30_moneyout) / $total_user_winrate30_moneyin * 10000) / 100);
        }
        else if($mode_data == "31")
        {
        	$mode_str = "홀수 승률 부양 중 미적용";
        
        	$total_user_winrate30_moneyin += $moneyin;
        	$total_user_winrate30_moneyout += $moneyout;
        	$total_user_winrate30_playcount += $playcount;
        	$total_user_winrate30_usercount += $usercount;
        
        	$total_user_winrate30_rate1 = ($total_user_winrate30_moneyin == 0 ? 0:round(($total_user_winrate30_moneyout) / $total_user_winrate30_moneyin * 10000) / 100);
        }
        else if($mode_data == "3027")
        {
        	$mode_str = "홀수 승률 부양 합계";
        
        	$total_user_winrate3027_moneyin += $moneyin;
        	$total_user_winrate3027_moneyout += $moneyout;
        	$total_user_winrate3027_playcount += $playcount;
        	$total_user_winrate3027_usercount += $usercount;
        
        	$total_user_winrate3027_rate1 = ($total_user_winrate3027_moneyin == 0 ? 0:round(($total_user_winrate3027_moneyout) / $total_user_winrate3027_moneyin * 10000) / 100);
        }
        
        
        if(!in_array($betlevel_data, $total_betlist_num))
        {
        	$total_betlist_num[$bet_cnt] = $betlevel_data;
        	$bet_cnt++;
        }
        		
        $betlevel_str = $total_betlist_name[$betlevel_data];
        $total_betlist_moneyin[$betlevel_data] += $moneyin;
        $total_betlist_moneyout[$betlevel_data] += $moneyout;
        $total_betlist_playcount[$betlevel_data] += $playcount;
        $total_betlist_usercount[$betlevel_data] += $usercount;
        
        $total_betlist_rate1[$betlevel_data] = ($total_betlist_moneyin[$betlevel_data] == 0 ? 0:round(($total_betlist_moneyout[$betlevel_data]) / $total_betlist_moneyin[$betlevel_data] * 10000) / 100);
        
    
        
        if($datetmp != $writedate && datetmp != "" && $viewmode != "0" && $j != "0")
        {
?>
        	<tr onmouseover="className='tr_over'" onmouseout="className=''" style="border-top:1px double;">
<?
        }
		else 
		{
?>
			<tr onmouseover="className='tr_over'" onmouseout="className=''" >           
<? 
		}
		
		$datetmp = $writedate;
		
	if($tab == "0")
	{
		if($viewmode == "0")
		{
			if($mode != 26)
			{
				$game_profit = $moneyin - ($moneyout + $jackpot);
				$total_game_profit += $game_profit;
?>
					<td class="tdc point" <?= $style ?>><?= $writedate ?></td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($moneyout) ?></td>
<?
				if($mode == 0 || $mode == "mode")
				{			
?>
                	<td class="tdr point" <?= $style ?>><?= number_format($jackpot) ?></td>
<?
				}
?>
				 
					<td class="tdr point" <?= $style ?>><?= number_format($game_profit) ?></td>
<?
				if($mode == "mode" && $total_mode != 3)
				{
?>
					<td class="tdr point" <?= $style ?>><?= number_format($treat_data) ?></td>
<?
					if($betlevel == "betlevel" && $total_mode == 0)
					{
?>
						<td class="tdr point" <?= $style ?>><?= number_format($exchange_treat_data) ?></td>
<?
					}
				}
			
				if($mode == 0  || $mode == 25 || $mode == 28 || $mode == "mode")
				{
			
?>
					<!-- <td class="tdc point" <?= $style ?>><?= $rate_playcount_array[$writedate]."%" ?></td>  -->
					<td class="tdc point" <?= $style ?>><?= $unit_rate_array[$writedate]."%" ?></td>
<?
				}
?>
					<td class="tdc point" <?= $style ?>><?= $rate1."%" ?></td>
<?
				if($mode == 0 || $mode == "mode")
				{			
?>
					<td class="tdc point" <?= $style ?>><?= $rate2."%" ?></td>
<?
				}
			
				if($mode == "mode" )
				{
?>
					<td class="tdc point" <?= $style ?>><?= $rate3."%" ?></td>
<?
					if($betlevel == "betlevel" && $total_mode == 0)
					{
?>
						<td class="tdc point" <?= $style ?>><?= $rate4."%" ?></td>
<?
					}
				}
?>
					<td class="tdc" <?= $style ?>><?= number_format($playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($usercount) ?></td>
<?
			}
			else if($mode == 26)
			{								
				$mode_26_rate_1 = round($mode_total_array[$writedate]["moneyout"]/$mode_total_array[$writedate]["moneyin"]*100,2);
				$mode_26_rate_2 = round(($mode_total_array[$writedate]["moneyout"]-$moneyout)/$mode_total_array[$writedate]["moneyin"]*100,2);
?>
				<td class="tdc point"><?= $writedate ?></td>
				<td class="tdr point"><?= number_format($mode_total_array[$writedate]["moneyin"]) ?></td>
				<td class="tdr point"><?= number_format($mode_total_array[$writedate]["moneyout"]) ?></td>
				<td class="tdr point"><?= number_format($moneyout) ?></td>
				<td class="tdc point"><?= $mode_26_rate_1 ?>%</td>
				<td class="tdc point"><?= $mode_26_rate_2 ?>%</td>
				<td class="tdc point"><?= round($mode_26_rate_1 - $mode_26_rate_2, 2)?></td>
				<td class="tdc"><?= number_format($usercount) ?></td>
				
<?
			}
		}
		else if($viewmode == "1")
		{
?>
				<td class="tdc point" <?= $style ?>><?= $writedate ?></td>
				<td class="tdr point" <?= $style ?> <?= $style ?>><?= $mode_str ?></td>
				<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($moneyin) ?></td>
				<td class="tdr point" <?= $style ?>><?= number_format($moneyout) ?></td>
<?
			if($mode == 0  && $mode != "mode")
			{
?>
				<td class="tdc point" <?= $style ?>><?= $rate_playcount_array[$writedate]."%" ?></td>
				<td class="tdc point" <?= $style ?>><?= $unit_rate_array[$writedate]."%" ?></td>
<?
			}
?>
				<td class="tdc point" <?= $style ?>><?= $rate1."%" ?></td>                        
				<td class="tdc" <?= $style ?>><?= number_format($playcount) ?></td>
				<td class="tdc" <?= $style ?>><?= number_format($usercount) ?></td>		
<?
		}
		else if($viewmode == "2")
		{
?>
						<td class="tdc point" <?= $style ?>><?= $writedate ?></td>
						<td class="tdr point" <?= $style ?> <?= $style ?>><?= $betlevel_str ?></td>
						<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($moneyin) ?></td>
						<td class="tdr point" <?= $style ?>><?= number_format($moneyout) ?></td>                        
						<td class="tdc point" <?= $style ?>><?= $rate1."%" ?></td>                        
						<td class="tdc" <?= $style ?>><?= number_format($playcount) ?></td>
						<td class="tdc" <?= $style ?>><?= number_format($usercount) ?></td>		
<?
		}
	}
	else 
	{	
		
		if($viewmode == "0")
		{
			if($mode == 26 && $mode_26_slot_check > 0)
			{
				$mode_26_rate_1 = round($mode_total_array[$writedate]["moneyout"]/$mode_total_array[$writedate]["moneyin"]*100,2);
				$mode_26_rate_2 = round(($mode_total_array[$writedate]["moneyout"]-$moneyout)/$mode_total_array[$writedate]["moneyin"]*100,2);
?>
				<td class="tdc point"><?= $writedate ?></td>
				<td class="tdr point"><?= number_format($mode_total_array[$writedate]["moneyin"]) ?></td>
				<td class="tdr point"><?= number_format($mode_total_array[$writedate]["moneyout"]) ?></td>
				<td class="tdr point"><?= number_format($moneyout) ?></td>
				<td class="tdc point"><?= $mode_26_rate_1 ?>%</td>
				<td class="tdc point"><?= $mode_26_rate_2 ?>%</td>
				<td class="tdc point"><?= round($mode_26_rate_1 - $mode_26_rate_2, 2)?></td>
				<td class="tdc"><?= number_format($usercount) ?></td>
			
<?
			}
			else
			{
?>
					<td class="tdc point" <?= $style ?>><?= $writedate ?></td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($moneyout) ?></td>
<?
				if($mode == 0 || $mode == "mode")
				{			
?>
                	<td class="tdr point" <?= $style ?>><?= number_format($jackpot) ?></td>
<?
				}

				if($mode == "mode" && $total_mode != 3)
				{
?>
					<td class="tdr point" <?= $style ?>><?= number_format($treat_data) ?></td>
<?
				}	
			
				if($mode == 0  || $mode == 25 || $mode == 28 || $mode == "mode")
				{
					
?>
					<!-- <td class="tdc point" <?= $style ?>><?= $rate_playcount_array[$writedate]."%" ?></td>  -->
					<td class="tdc point" <?= $style ?>><?= $unit_rate_array[$writedate]."%" ?></td>
<?
				}
?>
					<td class="tdc point" <?= $style ?>><?= $rate1."%" ?></td>
<?
				if($mode == 0 || $mode == "mode")
				{			
?>
					<td class="tdc point" <?= $style ?>><?= $rate2."%" ?></td>
<?
				}
			
				if($mode == "mode")
				{
?>				
					<td class="tdc point" <?= $style ?>><?= $rate3."%" ?></td>
<?
				}
?>				
					<td class="tdc" <?= $style ?>><?= number_format($playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($usercount) ?></td>
<?
			}
		}
		else if($viewmode == "1")
		{
?>
				<td class="tdc point" <?= $style ?>><?= $writedate ?></td>
				<td class="tdr point" <?= $style ?> <?= $style ?>><?= $mode_str ?></td>
				<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($moneyin) ?></td>
				<td class="tdr point" <?= $style ?>><?= number_format($moneyout) ?></td>
<?
			if($mode == 0  && $mode != "mode")
			{
?>
				<!-- <td class="tdc point" <?= $style ?>><?= $rate_playcount_array[$writedate]."%" ?></td>  -->
				<td class="tdc point" <?= $style ?>><?= $unit_rate_array[$writedate]."%" ?></td>
<?
			}
?>                 
				<td class="tdc point" <?= $style ?>><?= $rate1."%" ?></td>                        
				<td class="tdc" <?= $style ?>><?= number_format($playcount) ?></td>
				<td class="tdc" <?= $style ?>><?= number_format($usercount) ?></td>
<?
		}
		else if($viewmode == "2")
		{
?>
						<td class="tdc point" <?= $style ?>><?= $writedate ?></td>
						<td class="tdr point" <?= $style ?> <?= $style ?>><?= $betlevel_str ?></td>
						<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($moneyin) ?></td>
						<td class="tdr point" <?= $style ?>><?= number_format($moneyout) ?></td>                        
						<td class="tdc point" <?= $style ?>><?= $rate1."%" ?></td>                        
						<td class="tdc" <?= $style ?>><?= number_format($playcount) ?></td>
						<td class="tdc" <?= $style ?>><?= number_format($usercount) ?></td>
<?
		}
		
	}
?>
			</tr>
<?
    }
?>   


<?
	if($tab == "0")
	{
		if($viewmode == "0")
		{
			if($mode != 26)
			{
?>
				<tr style="border-top:1px double;">
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point"><?= number_format($total_moneyin) ?></td>
					<td class="tdr point"><?= number_format($total_moneyout) ?></td>
<?
				if($mode == 0 || $mode == "mode")
				{			
?>
    				<td class="tdr point"><?= number_format($total_jackpot) ?></td>
<?
				}
?>
	
					<td class="tdr point"><?= number_format($total_game_profit) ?></td>
<?
				if($mode == "mode" && $total_mode != 3)
				{
?>
					<td class="tdr point"><?= number_format($total_treat) ?></td>
<?
					if($betlevel == "betlevel" && $total_mode == 0)
					{
?>	
						<td class="tdr point"><?= number_format($total_exchange_treat) ?></td>
<?	
					}
				}
			
				if($mode == 0  || $mode == 25 || $mode == 28 || $mode == "mode")
				{			
?>

					<!-- <td class="tdc point" <?= $style ?>><?= $total_rate_playcount_value."%" ?></td>  -->
					<td class="tdc point" <?= $style ?>><?= $total_unit_rate_value."%" ?></td>							
<?
				}
?>
					<td class="tdc point" <?= $style ?>><?= $total_rate1."%" ?></td>
<?
				if($mode == 0 || $mode == "mode")
				{			
?>
					<td class="tdc point" <?= $style ?>><?= $total_rate2."%" ?></td>
<?
				}
			
				if($mode == "mode")
				{
?>				
					<td class="tdc point" <?= $style ?>><?= $total_rate3."%" ?></td>
<?
					if($betlevel == "betlevel" && $total_mode == 0)
					{
?>
						<td class="tdc point" <?= $style ?>><?= $total_rate4."%" ?></td>
<?
					}
				}
?>
					<td class="tdc" <?= $style ?>><?= number_format($total_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_usercount) ?></td>
				</tr>
<?
			}
			else if($mode == 26)
			{
				$mode_total_rate_1 = round($mode_total_sum_moneyout/$mode_total_sum_moneyin*100,2);
				$mode_total_rate_2 = round(($mode_total_sum_moneyout-$total_moneyout)/$mode_total_sum_moneyin*100,2);
?>
				<tr style="border-top:1px double;">
					<td class="tdc point">Total</td>
					<td class="tdr point"><?= number_format($mode_total_sum_moneyin) ?></td>
					<td class="tdr point"><?= number_format($mode_total_sum_moneyout) ?></td>
					<td class="tdr point"><?= number_format($total_moneyout) ?></td>
					<td class="tdc point"><?= $mode_total_rate_1 ?>%</td>
					<td class="tdc point"><?= $mode_total_rate_2 ?>%</td>
					<td class="tdc point"><?= round($mode_total_rate_1-$mode_total_rate_2, 2)?></td>
					<td class="tdc"><?= number_format($total_usercount) ?></td>
				</tr>
<?
			}
		}
		else if($viewmode == "1")
		{
			if($mode=="mode")
			{
?>
				<tr style="border-top:1px double;">
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>일반모드</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_normal_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_normal_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_normal_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_normal_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_normal_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>튜토리얼</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_tutorial_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_tutorial_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_tutorial_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_tutorial_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_tutorial_usercount) ?></td>
				</tr>				
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>울트라프리스핀</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_ultra_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_ultra_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_ultra_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_ultra_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_ultra_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양11</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate11_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate11_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate11_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate11_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate11_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양12</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate12_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate12_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate12_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate12_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate12_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양13</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate13_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate13_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate13_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate13_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate13_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양14</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate14_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate14_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate14_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate14_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate14_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양15</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate15_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate15_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate15_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate15_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate15_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양16</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate16_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate16_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate16_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate16_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate16_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양17</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate17_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate17_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate17_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate17_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate17_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양18</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate18_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate18_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate18_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate18_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate18_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양19</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate19_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate19_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate19_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate19_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate19_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양20</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate20_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate20_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate20_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate20_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate20_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양21</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate21_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate21_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate21_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate21_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate21_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양22</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate22_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate22_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate22_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate22_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate22_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양23</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate23_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate23_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate23_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate23_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate23_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양24</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate24_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate24_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate24_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate24_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate24_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양25</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate25_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate25_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate25_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate25_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate25_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양28</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate28_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate28_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate28_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate28_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate28_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>Multi Jacpot</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate26_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate26_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate26_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate26_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate26_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>홀수 결제 승률부양</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate27_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate27_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate27_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate27_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate27_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>DailyFreeSpin</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate29_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate29_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate29_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate29_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate29_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>DailyFreeSpin Bonus</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate30_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate30_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate30_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate30_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate30_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>홀수 승률 부양 중 미적용</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate31_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate31_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate31_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate31_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate31_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>홀수 승률 부양 합계</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate3027_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate3027_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate3027_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate3027_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate3027_usercount) ?></td>
				</tr>
<?
			}
		}	
		else if($viewmode == "2")
		{
			if($betlevel == "betlevel")
			{
				sort($total_betlist_num);
				
				for($n = 0; $n < sizeof($total_betlist_num); $n++)
				{
						$bet_num = $total_betlist_num[$n];
						
						write_log($bet_num);
						
						$bet_str = $total_betlist_name[$bet_num];
						$total_betlevel_moneyin = $total_betlist_moneyin[$bet_num];
						$total_betlevel_moneyout = $total_betlist_moneyout[$bet_num];
						$total_betlevel_rate1 = $total_betlist_rate1[$bet_num];
						$total_betlevel_playcount = $total_betlist_playcount[$bet_num];
						$total_betlevel_usercount = $total_betlist_usercount[$bet_num];
						
?>						
							<tr style="border-top:1px double;">
<?
								if($n == 0)
								{
?>
									<td class="tdc point" <?= $style ?> rowspan="<?=sizeof($total_betlist_num)?>">Total</td>
<?
								}
?>
								<td class="tdr point" <?= $style ?> <?= $style ?>><?=$bet_str?></td>
								<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_betlevel_moneyin) ?></td>
								<td class="tdr point" <?= $style ?>><?= number_format($total_betlevel_moneyout) ?></td>                        
								<td class="tdc point" <?= $style ?>><?= $total_betlevel_rate1."%" ?></td>                        
								<td class="tdc" <?= $style ?>><?= number_format($total_betlevel_playcount) ?></td>
								<td class="tdc" <?= $style ?>><?= number_format($total_betlevel_usercount) ?></td>
							</tr>
<?
				}
			}
			else 
			{
?>		
				<tr style="border-top:1px double;">
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= $bet_str ?></td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_usercount) ?></td>
				</tr>
<?
			}
		}
	}
	else
	{
		if($viewmode == "0")
		{
			if($mode == 26 && $mode_26_slot_check > 0)
			{
				$mode_total_rate_1 = round($mode_total_sum_moneyout/$mode_total_sum_moneyin*100,2);
				$mode_total_rate_2 = round(($mode_total_sum_moneyout-$total_moneyout)/$mode_total_sum_moneyin*100,2);
?>
				<tr style="border-top:1px double;">
					<td class="tdc point">Total</td>
					<td class="tdr point"><?= number_format($mode_total_sum_moneyin) ?></td>
					<td class="tdr point"><?= number_format($mode_total_sum_moneyout) ?></td>
					<td class="tdr point"><?= number_format($total_moneyout) ?></td>
					<td class="tdc point"><?= $mode_total_rate_1 ?>%</td>
					<td class="tdc point"><?= $mode_total_rate_2 ?>%</td>
					<td class="tdc point"><?= round($mode_total_rate_1-$mode_total_rate_2, 2)?></td>
					<td class="tdc"><?= number_format($total_usercount) ?></td>
				</tr>
<?				
			}
			else 
			{
?>
				<tr style="border-top:1px double;">
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point"><?= number_format($total_moneyin) ?></td>
					<td class="tdr point"><?= number_format($total_moneyout) ?></td>
<?
				if($mode == 0 || $mode == "mode")
				{			
?>
    				<td class="tdr point"><?= number_format($total_jackpot) ?></td>
<?
				}

				if($mode == "mode" && $total_mode != 3)
				{
?>
					<td class="tdr point"><?= number_format($total_treat) ?></td>
<?
				}
			
				if($mode == 0  || $mode == 25 || $mode == 28 || $mode == "mode")
				{
			
?>
					<!-- <td class="tdc point" <?= $style ?>><?= $total_rate_playcount_value."%" ?></td>  -->
					<td class="tdc point" <?= $style ?>><?= $total_unit_rate_value."%" ?></td>		
<?
				}
?>
					<td class="tdc point" <?= $style ?>><?= $total_rate1."%" ?></td>

<?
				if($mode == 0 || $mode == "mode")
				{			
?>
					<td class="tdc point" <?= $style ?>><?= $total_rate2."%" ?></td>			
<?
				}
			
				if($mode == "mode")
				{
?>				 
					<td class="tdc point" <?= $style ?>><?= $total_rate3."%" ?></td>
<?
				}
?>
					<td class="tdc" <?= $style ?>><?= number_format($total_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_usercount) ?></td>
				</tr>

<?
			}
		}
		else if($viewmode == "1")
		{
			if($mode=="mode")
			{
?>	
				<tr style="border-top:1px double;">
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>일반모드</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_normal_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_normal_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_normal_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_normal_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_normal_usercount) ?></td>
				</tr>
				 <tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>튜토리얼</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_tutorial_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_tutorial_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_tutorial_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_tutorial_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_tutorial_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>울트라프리스핀</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_ultra_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_ultra_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_ultra_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_ultra_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_ultra_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양11</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate11_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate11_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate11_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate11_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate11_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양12</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate12_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate12_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate12_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate12_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate12_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양13</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate13_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate13_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate13_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate13_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate13_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양14</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate14_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate14_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate14_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate14_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate14_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양15</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate15_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate15_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate15_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate15_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate15_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양16</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate16_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate16_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate16_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate16_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate16_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양17</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate17_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate17_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate17_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate17_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate17_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양18</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate18_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate18_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate18_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate18_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate18_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양19</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate19_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate19_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate19_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate19_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate19_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양20</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate20_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate20_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate20_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate20_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate20_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양21</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate21_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate21_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate21_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate21_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate21_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양22</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate22_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate22_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate22_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate22_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate22_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양23</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate23_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate23_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate23_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate23_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate23_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양24</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate24_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate24_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate24_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate24_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate24_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양25</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate25_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate25_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate25_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate25_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate25_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>개임승률부양28</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate28_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate28_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate28_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate28_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate28_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>Multi Jackpot</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate26_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate26_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate26_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate26_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate26_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>홀수 결제 승률 부양</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate27_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate27_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate27_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate27_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate27_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>DailyFreeSpin</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate29_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate29_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate29_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate29_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate29_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>DailyFreeSpin Bonus</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate30_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate30_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate30_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate30_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate30_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>홀수 승률 부양 중 미적용</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate31_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate31_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate31_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate31_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate31_usercount) ?></td>
				</tr>
				<tr>
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>>홀수 승률 부양 합계</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_user_winrate3027_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_user_winrate3027_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_user_winrate3027_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate3027_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_user_winrate3027_usercount) ?></td>
				</tr>
<?
			}			
			else
			{
?>
				<tr style="border-top:1px double;">
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= $mode_str ?></td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_moneyout) ?></td>
<?					
				if($mode == 0 && $mode != "mode")
				{
?>
					<!-- <td class="tdc point" <?= $style ?>><?= $total_rate_playcount_value."%" ?></td>  -->
					<td class="tdc point" <?= $style ?>><?= $total_unit_rate_value."%" ?></td>		
<?
				}
?>                        
					<td class="tdc point" <?= $style ?>><?= $total_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_usercount) ?></td>
				</tr>
<?
			}
		}
		else if($viewmode == "2")
		{			
			if($betlevel == "betlevel")
			{
				sort($total_betlist_num);
				
				for($n = 0; $n < sizeof($total_betlist_num); $n++)
				{
						$bet_num = $total_betlist_num[$n];
						
						$bet_str = $total_betlist_name[$bet_num];
						$total_betlevel_moneyin = $total_betlist_moneyin[$bet_num];
						$total_betlevel_moneyout = $total_betlist_moneyout[$bet_num];
						$total_betlevel_rate1 = $total_betlist_rate1[$bet_num];
						$total_betlevel_playcount = $total_betlist_playcount[$bet_num];
						$total_betlevel_usercount = $total_betlist_usercount[$bet_num];
?>						
							<tr style="border-top:1px double;">
<?
								if($n == 0)
								{
?>
									<td class="tdc point" <?= $style ?> rowspan="<?=sizeof($total_betlist_num)?>">Total</td>
<?
								}
?>
								<td class="tdr point" <?= $style ?> <?= $style ?>><?=$bet_str?></td>
								<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_betlevel_moneyin) ?></td>
								<td class="tdr point" <?= $style ?>><?= number_format($total_betlevel_moneyout) ?></td>                        
								<td class="tdc point" <?= $style ?>><?= $total_betlevel_rate1."%" ?></td>                        
								<td class="tdc" <?= $style ?>><?= number_format($total_betlevel_playcount) ?></td>
								<td class="tdc" <?= $style ?>><?= number_format($total_betlevel_usercount) ?></td>
							</tr>
<?
				}			
			}
			else 
			{
?>
				<tr style="border-top:1px double;">
					<td class="tdc point" <?= $style ?> <?= $style ?>>Total</td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= $betlevel_str ?></td>
					<td class="tdr point" <?= $style ?> <?= $style ?>><?= number_format($total_moneyin) ?></td>
					<td class="tdr point" <?= $style ?>><?= number_format($total_moneyout) ?></td>                        
					<td class="tdc point" <?= $style ?>><?= $total_rate1."%" ?></td>                        
					<td class="tdc" <?= $style ?>><?= number_format($total_playcount) ?></td>
					<td class="tdc" <?= $style ?>><?= number_format($total_usercount) ?></td>
				</tr>
<?
			}			
		}	
	}
?>

		
		</tbody>
	</table>
	</form>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    $db_main->end();
    $db_main2->end();
    $db_other->end();
    $db_analysis->end();   
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>		