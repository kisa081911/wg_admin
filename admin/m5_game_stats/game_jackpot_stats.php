<?
    $top_menu = "game_stats";
    $sub_menu = "game_jackpot_stats";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $db_main = new CDatabase_Main();    
    
    $today = date("Y-m-d");
?>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 잭팟 현황</div>
	</div>
	<!-- //title_warp -->
	                
	<table class="tbl_list_basic1" style="width:1080px">
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">일별 잭팟 기록</span>
		<table class="tbl_list_basic1" style="margin-top:5px">
			<colgroup>
				<col width="">   
				<col width="100">
				<col width="150">
				<col width="150">            
			</colgroup>
			<thead>
				<tr>
					<th class="tdl">일자</th>
					<th class="tdr">잭팟수</th>
					<th class="tdr">총금액</th>
					<th class="tdr">최대금액</th>
				</tr>
			</thead>
			<tbody>
	<?
	    $sql = "SELECT DATE_FORMAT(writedate,'%Y-%m-%d') AS date, COUNT(*) AS totalcount, SUM(amount) AS totalamount, max(amount) AS maxamount ".
			" FROM tbl_jackpot_log ".
			" WHERE devicetype = 0 ".
			" GROUP BY DATE_FORMAT(writedate,'%Y-%m-%d') ". 
			" ORDER BY DATE_FORMAT(writedate,'%Y-%m-%d') DESC LIMIT 10";
	           
	    $game_list = $db_main->gettotallist($sql);
	    
	    for($i = 0; $i < sizeof($game_list); $i++)
	    {
	        $date = $game_list[$i]["date"];
	        $totalcount = $game_list[$i]["totalcount"];
	        $totalamount = $game_list[$i]["totalamount"];
	        $maxamount = $game_list[$i]["maxamount"];
			
			$style = "";
	?>
				<tr>
					<td class="tdl" <?= $style ?>><?= $date ?></td>
					<td class="tdr" <?= $style ?>><?= make_price_format($totalcount) ?></td>
					<td class="tdr" <?= $style ?>><?= make_price_format($totalamount) ?></td>
					<td class="tdr" <?= $style ?>><?= make_price_format($maxamount) ?></td>
				</tr>
	<?
	    }
	?>
			</tbody>
		</table>
	</div>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
    
<?
	$db_main->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>