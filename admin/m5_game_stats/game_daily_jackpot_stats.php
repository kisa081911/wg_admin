<?
	$top_menu = "game_stats";
	$sub_menu = "game_daily_jackpot_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$os_type = ($_GET["os_type"] == "") ? "4" :$_GET["os_type"];
	$total_mode = ($_GET["total_mode"] == "") ? "0" :$_GET["total_mode"];
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	$slot_type = ($_GET["slottype"] == "") ? "all" : $_GET["slottype"];
	
	if($search_start_createdate == "")
		$search_start_createdate = date("Y-m-d", strtotime("-7 day"));
	
	if($search_end_createdate == "")
		$search_end_createdate = $today;
	
	$tail = " WHERE 1=1 ";
	
	$onwer_sql = "t2.owner = 1 AND ";
	
	if($os_type == "4")
	{
		$os_txt = "All";
		$os_device_type = "devicetype";
		$onwer_sql = "";
	}	
	else if($os_type == "0")
	{
		$table = "tbl_game_cash_stats_daily";
		$os_txt = "Web";
		$os_device_type = $os_type;
	}
	else if($os_type == "1")
	{
		$tail .= " AND ios = 1 ";
		$table = "tbl_game_cash_stats_ios_daily";
		$os_txt = "IOS";
		$os_device_type = $os_type;
	}
	else if($os_type == "2")
	{
		$tail .= " AND android = 1 ";
		$table = "tbl_game_cash_stats_android_daily";
		$os_txt = "Android";
		$os_device_type = $os_type;
	}
	else if($os_type == "3")
	{
		$tail .= " AND amazon = 1 ";
		$table = "tbl_game_cash_stats_amazon_daily";
		$os_txt = "Amazon";
		$os_device_type = $os_type;
	}
	
	if($total_mode == 0)
		$mode_name = "전체";
	else if($total_mode == 1)
		$mode_name = "레귤러";
	else if($total_mode == 2)
		$mode_name = "하이롤러(싱글X)";
	else if($total_mode == 3)
		$mode_name = "울트라프리스핀";
	else if($total_mode == 4)
		$mode_name = "싱글모드";
	else if($total_mode == 5)
		$mode_name = "하이롤러(싱글포함)";
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();

	if($total_mode == 0)
	{
		if($slot_type == "all")
		{
			$sql = "SELECT today, COUNT(t1.jackpothallidx) AS cnt_jackpot, SUM(cnt_jp_user) AS sum_jp_user, SUM(sum_amount) AS sum_jp_amount, SUM(fiesta_cnt) AS sum_fiesta_cnt, sum(fiesta_sum_amount) as  fiesta_jp_amount ".
					"FROM ( ".
					"	SELECT DATE_FORMAT(writedate,'%Y-%m-%d') AS today, slottype, jackpothallidx, COUNT(useridx) AS cnt_jp_user, SUM(IF(fiestaidx > 0, 1, 0)) AS fiesta_cnt, SUM(amount) AS sum_amount, SUM(IF(fiestaidx > 0, amount, 0)) AS fiesta_sum_amount, useridx ".
					"	FROM tbl_jackpot_log ".
					"	WHERE devicetype = $os_device_type AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
					"	GROUP BY today, jackpothallidx ORDER BY jackpotidx DESC ". 	
					") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx WHERE $onwer_sql t2.useridx > 10000 GROUP BY today ORDER BY today DESC";
		}
		else
		{
			$sql = "SELECT today, COUNT(t1.jackpothallidx) AS cnt_jackpot, SUM(cnt_jp_user) AS sum_jp_user, SUM(sum_amount) AS sum_jp_amount, SUM(fiesta_cnt) AS sum_fiesta_cnt, sum(fiesta_sum_amount) as  fiesta_jp_amount ".
					"FROM ( ".
					"	SELECT DATE_FORMAT(writedate,'%Y-%m-%d') AS today, slottype, jackpothallidx, COUNT(useridx) AS cnt_jp_user, SUM(IF(fiestaidx > 0, 1, 0)) AS fiesta_cnt,  SUM(amount) AS sum_amount, SUM(IF(fiestaidx > 0, amount, 0)) AS fiesta_sum_amount, useridx ".
					"	FROM tbl_jackpot_log ".
					"	WHERE devicetype = $os_device_type AND slottype = $slot_type AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
					"	GROUP BY today, jackpothallidx ORDER BY jackpotidx DESC ".
					") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx WHERE $onwer_sql t2.useridx > 10000 GROUP BY today ORDER BY today DESC";
		}
	}
	else if($total_mode == 1)
	{
		if($slot_type == "all")
		{
			$sql = "SELECT today, COUNT(t1.jackpothallidx) AS cnt_jackpot, SUM(cnt_jp_user) AS sum_jp_user, SUM(sum_amount) AS sum_jp_amount, SUM(fiesta_cnt) AS sum_fiesta_cnt, sum(fiesta_sum_amount) as  fiesta_jp_amount ".
					"FROM ( ".
					"	SELECT DATE_FORMAT(writedate,'%Y-%m-%d') AS today, slottype, jackpothallidx, COUNT(useridx) AS cnt_jp_user, SUM(IF(fiestaidx > 0, 1, 0)) AS fiesta_cnt, SUM(amount) AS sum_amount, SUM(IF(fiestaidx > 0, amount, 0)) AS fiesta_sum_amount, useridx ".
					"	FROM tbl_jackpot_log ".
					"	WHERE devicetype = $os_device_type AND objectidx NOT IN (0) AND objectidx < 1000000 AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
					"	GROUP BY today, jackpothallidx ORDER BY jackpotidx DESC ". 	
					") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx WHERE $onwer_sql t2.useridx > 10000 GROUP BY today ORDER BY today DESC";
		}
		else
		{
			$sql = "SELECT today, COUNT(t1.jackpothallidx) AS cnt_jackpot, SUM(cnt_jp_user) AS sum_jp_user, SUM(sum_amount) AS sum_jp_amount, SUM(fiesta_cnt) AS sum_fiesta_cnt, sum(fiesta_sum_amount) as  fiesta_jp_amount ".
					"FROM ( ".
					"	SELECT DATE_FORMAT(writedate,'%Y-%m-%d') AS today, slottype, jackpothallidx, COUNT(useridx) AS cnt_jp_user, SUM(IF(fiestaidx > 0, 1, 0)) AS fiesta_cnt, SUM(amount) AS sum_amount, SUM(IF(fiestaidx > 0, amount, 0)) AS fiesta_sum_amount, useridx ".
					"	FROM tbl_jackpot_log ".
					"	WHERE  devicetype = $os_device_type AND objectidx NOT IN (0) AND objectidx < 1000000 AND slottype = $slot_type AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
					"	GROUP BY today, jackpothallidx ORDER BY jackpotidx DESC ".
					") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx WHERE $onwer_sql t2.useridx > 10000 GROUP BY today ORDER BY today DESC";
		}
	}
	else if($total_mode == 2)
	{
		if($slot_type == "all")
		{
			$sql = "SELECT today, COUNT(t1.jackpothallidx) AS cnt_jackpot, SUM(cnt_jp_user) AS sum_jp_user, SUM(sum_amount) AS sum_jp_amount, SUM(fiesta_cnt) AS sum_fiesta_cnt, sum(fiesta_sum_amount) as  fiesta_jp_amount ".
					"FROM ( ".
					"	SELECT DATE_FORMAT(writedate,'%Y-%m-%d') AS today, slottype, jackpothallidx, COUNT(useridx) AS cnt_jp_user, SUM(IF(fiestaidx > 0, 1, 0)) AS fiesta_cnt, SUM(amount) AS sum_amount, SUM(IF(fiestaidx > 0, amount, 0)) AS fiesta_sum_amount, useridx ".
					"	FROM tbl_jackpot_log ".
					"	WHERE devicetype = $os_device_type AND objectidx >= 1000000 AND objectidx < 2000000 AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
					"	GROUP BY today, jackpothallidx ORDER BY jackpotidx DESC ".
					") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx  WHERE $onwer_sql t2.useridx > 10000 GROUP BY today ORDER BY today DESC";
		}
		else
		{
			$sql = "SELECT today, COUNT(t1.jackpothallidx) AS cnt_jackpot, SUM(cnt_jp_user) AS sum_jp_user, SUM(sum_amount) AS sum_jp_amount, SUM(fiesta_cnt) AS sum_fiesta_cnt, sum(fiesta_sum_amount) as  fiesta_jp_amount ".
					"FROM ( ".
					"	SELECT DATE_FORMAT(writedate,'%Y-%m-%d') AS today, slottype, jackpothallidx, COUNT(useridx) AS cnt_jp_user, SUM(IF(fiestaidx > 0, 1, 0)) AS fiesta_cnt, SUM(amount) AS sum_amount, SUM(IF(fiestaidx > 0, amount, 0)) AS fiesta_sum_amount, useridx ".
					"	FROM tbl_jackpot_log ".
					"	WHERE devicetype = $os_device_type AND objectidx >= 1000000 AND objectidx < 2000000 AND slottype = $slot_type AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
					"	GROUP BY today, jackpothallidx ORDER BY jackpotidx DESC ".
					") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx  WHERE $onwer_sql t2.useridx > 10000 GROUP BY today ORDER BY today DESC";
		}
	}
	else if($total_mode == 3)
	{
		if($slot_type == "all")
		{
			$sql = "SELECT today, COUNT(t1.jackpothallidx) AS cnt_jackpot, SUM(cnt_jp_user) AS sum_jp_user, SUM(sum_amount) AS sum_jp_amount, SUM(fiesta_cnt) AS sum_fiesta_cnt, sum(fiesta_sum_amount) as  fiesta_jp_amount ".
					"FROM ( ".
					"	SELECT DATE_FORMAT(writedate,'%Y-%m-%d') AS today, slottype, jackpothallidx, COUNT(useridx) AS cnt_jp_user, SUM(IF(fiestaidx > 0, 1, 0)) AS fiesta_cnt, SUM(amount) AS sum_amount, SUM(IF(fiestaidx > 0, amount, 0)) AS fiesta_sum_amount, useridx ".
					"	FROM tbl_jackpot_log ".
					"	WHERE devicetype = $os_device_type AND objectidx = 0 AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
					"	GROUP BY today, jackpothallidx ORDER BY jackpotidx DESC ".
					") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx  WHERE $onwer_sql t2.useridx > 10000 GROUP BY today ORDER BY today DESC";
		}
		else
		{
			$sql = "SELECT today, COUNT(t1.jackpothallidx) AS cnt_jackpot, SUM(cnt_jp_user) AS sum_jp_user, SUM(sum_amount) AS sum_jp_amount, SUM(fiesta_cnt) AS sum_fiesta_cnt, sum(fiesta_sum_amount) as  fiesta_jp_amount ".
					"FROM ( ".
					"	SELECT DATE_FORMAT(writedate,'%Y-%m-%d') AS today, slottype, jackpothallidx, COUNT(useridx) AS cnt_jp_user, SUM(IF(fiestaidx > 0, 1, 0)) AS fiesta_cnt,  SUM(amount) AS sum_amount, SUM(IF(fiestaidx > 0, amount, 0)) AS fiesta_sum_amount, useridx ".
					"	FROM tbl_jackpot_log ".
					"	WHERE devicetype = $os_device_type AND objectidx = 0 AND slottype = $slot_type AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
					"	GROUP BY today, jackpothallidx ORDER BY jackpotidx DESC ".
					") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx  WHERE $onwer_sql t2.useridx > 10000 GROUP BY today ORDER BY today DESC";
		}
	}	
	else if($total_mode == 4)
	{
		if($slot_type == "all")
		{
			$sql = "SELECT today, COUNT(t1.jackpothallidx) AS cnt_jackpot, SUM(cnt_jp_user) AS sum_jp_user, SUM(sum_amount) AS sum_jp_amount, SUM(fiesta_cnt) AS sum_fiesta_cnt, sum(fiesta_sum_amount) as  fiesta_jp_amount ".
					"FROM ( ".
					"	SELECT DATE_FORMAT(writedate,'%Y-%m-%d') AS today, slottype, jackpothallidx, COUNT(useridx) AS cnt_jp_user, SUM(IF(fiestaidx > 0, 1, 0)) AS fiesta_cnt, SUM(amount) AS sum_amount, SUM(IF(fiestaidx > 0, amount, 0)) AS fiesta_sum_amount, useridx ".
					"	FROM tbl_jackpot_log ".
					"	WHERE devicetype = $os_device_type AND objectidx >= 2000000 AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
					"	GROUP BY today, jackpothallidx ORDER BY jackpotidx DESC ".
					") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx  WHERE $onwer_sql t2.useridx > 10000 GROUP BY today ORDER BY today DESC";
		}
		else
		{
			$sql = "SELECT today, COUNT(t1.jackpothallidx) AS cnt_jackpot, SUM(cnt_jp_user) AS sum_jp_user, SUM(sum_amount) AS sum_jp_amount, SUM(fiesta_cnt) AS sum_fiesta_cnt, sum(fiesta_sum_amount) as  fiesta_jp_amount ".
					"FROM ( ".
					"	SELECT DATE_FORMAT(writedate,'%Y-%m-%d') AS today, slottype, jackpothallidx, COUNT(useridx) AS cnt_jp_user, SUM(IF(fiestaidx > 0, 1, 0)) AS fiesta_cnt, SUM(amount) AS sum_amount, SUM(IF(fiestaidx > 0, amount, 0)) AS fiesta_sum_amount, useridx ".
					"	FROM tbl_jackpot_log ".
					"	WHERE devicetype = $os_device_type AND objectidx >= 2000000 AND slottype = $slot_type AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
					"	GROUP BY today, jackpothallidx ORDER BY jackpotidx DESC ".
					") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx  WHERE $onwer_sql t2.useridx > 10000 GROUP BY today ORDER BY today DESC";
		}
	}
	else if($total_mode == 5)
	{
		if($slot_type == "all")
		{
			$sql = "SELECT today, COUNT(t1.jackpothallidx) AS cnt_jackpot, SUM(cnt_jp_user) AS sum_jp_user, SUM(sum_amount) AS sum_jp_amount, SUM(fiesta_cnt) AS sum_fiesta_cnt, sum(fiesta_sum_amount) as  fiesta_jp_amount ".
					"FROM ( ".
					"	SELECT DATE_FORMAT(writedate,'%Y-%m-%d') AS today, slottype, jackpothallidx, COUNT(useridx) AS cnt_jp_user, SUM(IF(fiestaidx > 0, 1, 0)) AS fiesta_cnt, SUM(amount) AS sum_amount, SUM(IF(fiestaidx > 0, amount, 0)) AS fiesta_sum_amount, useridx ".
					"	FROM tbl_jackpot_log ".
					"	WHERE devicetype = $os_device_type AND objectidx >= 1000000 AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
					"	GROUP BY today, jackpothallidx ORDER BY jackpotidx DESC ".
					") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx  WHERE $onwer_sql t2.useridx > 10000 GROUP BY today ORDER BY today DESC";
		}
		else
		{
			$sql = "SELECT today, COUNT(t1.jackpothallidx) AS cnt_jackpot, SUM(cnt_jp_user) AS sum_jp_user, SUM(sum_amount) AS sum_jp_amount, SUM(fiesta_cnt) AS sum_fiesta_cnt, sum(fiesta_sum_amount) as  fiesta_jp_amount ".
					"FROM ( ".
					"	SELECT DATE_FORMAT(writedate,'%Y-%m-%d') AS today, slottype, jackpothallidx, COUNT(useridx) AS cnt_jp_user, SUM(IF(fiestaidx > 0, 1, 0)) AS fiesta_cnt, SUM(amount) AS sum_amount, SUM(IF(fiestaidx > 0, amount, 0)) AS fiesta_sum_amount, useridx ".
					"	FROM tbl_jackpot_log ".
					"	WHERE devicetype = $os_device_type AND objectidx >= 1000000 AND slottype = $slot_type AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ".
					"	GROUP BY today, jackpothallidx ORDER BY jackpotidx DESC ".
					") t1 LEFT JOIN `tbl_jackpot_hall_member` t2 ON t1.jackpothallidx = t2.jackpothallidx AND t1.useridx = t2.useridx  WHERE $onwer_sql t2.useridx > 10000 GROUP BY today ORDER BY today DESC";
		}
	}
	$jackpot_data = $db_main->gettotallist($sql);
	
	//Slot 정보
	$sql = "SELECT slottype, slotname FROM tbl_slot_list $tail";
	$slottype_list = $db_main2->gettotallist($sql);	
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});

	function change_os_type(type)
	{
		var search_form = document.search_form;

		var all = document.getElementById("type_all");
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		
		document.search_form.os_type.value = type;

		if (type == "4")
		{
			all.className="btn_schedule_select";
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "0")
		{
			all.className="btn_schedule";
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "1")
		{
			all.className="btn_schedule";
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "2")
		{
			all.className="btn_schedule";
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (type == "3")
		{
			all.className="btn_schedule";
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="game_daily_jackpot_stats.php">
	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
		<input type="button" class="<?= ($os_type == "4") ? "btn_schedule_select" : "btn_schedule" ?>" value="All" id="type_all" onclick="change_os_type('4')"    />
		<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
		<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
		<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
		<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
	</span>
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 잭팟 일별 현황 통계(<?= $os_txt ?>/<?=$mode_name?>)</div>
		<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" /> 
		<div class="search_box">
			mode&nbsp;:&nbsp; 
			<select name="total_mode" id="total_mode">										
					<option value="0" <?= ($total_mode=="0") ? "selected" : "" ?>>전체</option>
					<option value="1" <?= ($total_mode=="1") ? "selected" : "" ?>>레귤러</option>                       
					<option value="2" <?= ($total_mode=="2") ? "selected" : "" ?>>하이롤러(싱글X)</option>
					<option value="3" <?= ($total_mode=="3") ? "selected" : "" ?>>울트라프리스핀</option>
					<option value="4" <?= ($total_mode=="4") ? "selected" : "" ?>>싱글모드</option>
					<option value="5" <?= ($total_mode=="5") ? "selected" : "" ?>>하이롤러(싱글포함)</option>
			</select>&nbsp;&nbsp;&nbsp;
			<font style="font:bold 16px 'Malgun Gothic'; color:#5ab2da;">Slot : </font>&nbsp;&nbsp;
			<select name="slottype" id="slottype">				
				<option value="all"  <?= ($slottype=="all") ? "selected" : "" ?>>ALL</option>
<?
	for ($i=0; $i<sizeof($slottype_list); $i++)
	{
		$slottype = $slottype_list[$i]["slottype"];
		$slotname = $slottype_list[$i]["slotname"];
?>	
				<option value="<?= $slottype ?>"  <?= ($slottype=="$slot_type") ? "selected" : "" ?>><?= $slotname ?></option>
<?						
	}
?>					
			</select>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
                <col width="">
                <col width="">
                <col width="">                
                <col width="">
                <col width="">
                <col width="">
                <col width="120">
                <col width="150">                
            </colgroup>
            <thead>
	            <tr>
	            	<th class="tdc">일자</th>
	                <th class="tdc">잭팟횟수</th>
	                <th class="tdc">잭팟유저수</th>
	                <th class="tdc">총스핀/총잭팟유저수</th>
	                <th class="tdc">총금액</th>	                
	                <th class="tdc">평균금액</th>
	                <th class="tdc">잭팟 피에스타 횟수</th>
	                <th class="tdc">잭팟 피에스타 총 금액</th>	                	                
	            </tr>
            </thead>
            <tbody>
<?			
			for($i=0; $i<sizeof($jackpot_data); $i++)
			{
				$today =  $jackpot_data[$i]["today"];
				$cnt_jackpot =  $jackpot_data[$i]["cnt_jackpot"];
				$sum_jp_user=  $jackpot_data[$i]["sum_jp_user"];
				$sum_jp_amount =  $jackpot_data[$i]["sum_jp_amount"];
				$sum_fiesta_cnt =  $jackpot_data[$i]["sum_fiesta_cnt"];
				$fiesta_jp_amount =  $jackpot_data[$i]["fiesta_jp_amount"];				
				
				$end_search_day = date("Y-m-d", strtotime($jackpot_data[$i+1]["today"]." +1 days"));				

				if($i == (sizeof($jackpot_data)-1))
					$end_search_day = $today;				
				
				if($total_mode == 0)
				{
					if($os_type == "4")
					{
						if($slot_type == "all")
						{
							$web_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_daily WHERE writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$web_playcnt = $db_main2->getvalue($web_sql);
							
							$ios_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_ios_daily WHERE writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$ios_playcnt = $db_main2->getvalue($ios_sql);
							
							$android_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_android_daily WHERE writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$android_playcnt = $db_main2->getvalue($android_sql);
							
							$amazon_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_amazon_daily WHERE writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$amazon_playcnt = $db_main2->getvalue($amazon_sql);
							
							$total_cnt_spin = $web_playcnt + $ios_playcnt + $android_playcnt + $amazon_playcnt;
						}
						else
						{
							$web_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_daily WHERE slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$web_playcnt = $db_main2->getvalue($web_sql);
								
							$ios_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_ios_daily WHERE slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$ios_playcnt = $db_main2->getvalue($ios_sql);
								
							$android_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_android_daily WHERE slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$android_playcnt = $db_main2->getvalue($android_sql);
								
							$amazon_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_amazon_daily WHERE slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$amazon_playcnt = $db_main2->getvalue($amazon_sql);
								
							$total_cnt_spin = $web_playcnt + $ios_playcnt + $android_playcnt + $amazon_playcnt;
						}
					}
					else
					{
						if($slot_type == "all")
						{
							$sql = "SELECT SUM(playcount) FROM $table WHERE writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
						}
						else
						{
							$sql = "SELECT SUM(playcount) FROM $table WHERE slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
						}
					}
				}
				else if($total_mode == 1)
				{
					if($os_type == "4")
					{
						if($slot_type == "all")
						{
							$web_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_daily WHERE betlevel BETWEEN 0 AND 9 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$web_playcnt = $db_main2->getvalue($web_sql);
								
							$ios_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_ios_daily WHERE betlevel BETWEEN 0 AND 9 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$ios_playcnt = $db_main2->getvalue($ios_sql);
								
							$android_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_android_daily WHERE betlevel BETWEEN 0 AND 9 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$android_playcnt = $db_main2->getvalue($android_sql);
								
							$amazon_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_amazon_daily WHERE betlevel BETWEEN 0 AND 9 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$amazon_playcnt = $db_main2->getvalue($amazon_sql);
								
							$total_cnt_spin = $web_playcnt + $ios_playcnt + $android_playcnt + $amazon_playcnt;
						}
						else
						{
							$web_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_daily WHERE betlevel BETWEEN 0 AND 9 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$web_playcnt = $db_main2->getvalue($web_sql);
						
							$ios_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_ios_daily WHERE betlevel BETWEEN 0 AND 9 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$ios_playcnt = $db_main2->getvalue($ios_sql);
						
							$android_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_android_daily WHERE betlevel BETWEEN 0 AND 9 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$android_playcnt = $db_main2->getvalue($android_sql);
						
							$amazon_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_amazon_daily WHERE betlevel BETWEEN 0 AND 9 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$amazon_playcnt = $db_main2->getvalue($amazon_sql);
						
							$total_cnt_spin = $web_playcnt + $ios_playcnt + $android_playcnt + $amazon_playcnt;
						}
					}
					else 
					{
						if($slot_type == "all")
						{
							$sql = "SELECT SUM(playcount) FROM $table WHERE betlevel BETWEEN 0 AND 9 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
						}
						else
						{
							$sql = "SELECT SUM(playcount) FROM $table WHERE  betlevel BETWEEN 0 AND 9 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
						}
					}
				}
				else if($total_mode == 2)
				{
					if($os_type == "4")
					{
						if($slot_type == "all")
						{
							$web_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_daily WHERE betlevel BETWEEN 10 AND 19 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$web_playcnt = $db_main2->getvalue($web_sql);
								
							$ios_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_ios_daily WHERE betlevel BETWEEN 10 AND 19 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$ios_playcnt = $db_main2->getvalue($ios_sql);
								
							$android_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_android_daily WHERE betlevel BETWEEN 10 AND 19 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$android_playcnt = $db_main2->getvalue($android_sql);
								
							$amazon_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_amazon_daily WHERE betlevel BETWEEN 10 AND 19 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$amazon_playcnt = $db_main2->getvalue($amazon_sql);
								
							$total_cnt_spin = $web_playcnt + $ios_playcnt + $android_playcnt + $amazon_playcnt;
						}
						else
						{
							$web_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_daily WHERE betlevel BETWEEN 10 AND 19 AND  slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$web_playcnt = $db_main2->getvalue($web_sql);
						
							$ios_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_ios_daily WHERE betlevel BETWEEN 10 AND 19 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$ios_playcnt = $db_main2->getvalue($ios_sql);
						
							$android_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_android_daily WHERE betlevel BETWEEN 10 AND 19 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$android_playcnt = $db_main2->getvalue($android_sql);
						
							$amazon_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_amazon_daily WHERE betlevel BETWEEN 10 AND 19 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$amazon_playcnt = $db_main2->getvalue($amazon_sql);
						
							$total_cnt_spin = $web_playcnt + $ios_playcnt + $android_playcnt + $amazon_playcnt;
						}
					}
					else
					{
						if($slot_type == "all")
						{
							$sql = "SELECT SUM(playcount) FROM $table WHERE betlevel BETWEEN 10 AND 19 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
						}
						else
						{
							$sql = "SELECT SUM(playcount) FROM $table WHERE  betlevel BETWEEN 10 AND 19 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
						}
					}
				}
				else if($total_mode == 3)
				{
					if($os_type == "4")
					{
						if($slot_type == "all")
						{
							$web_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_daily WHERE mode = 9 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$web_playcnt = $db_main2->getvalue($web_sql);
								
							$ios_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_ios_daily WHERE mode = 9 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$ios_playcnt = $db_main2->getvalue($ios_sql);
								
							$android_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_android_daily WHERE mode = 9 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$android_playcnt = $db_main2->getvalue($android_sql);
								
							$amazon_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_amazon_daily WHERE mode = 9 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$amazon_playcnt = $db_main2->getvalue($amazon_sql);
								
							$total_cnt_spin = $web_playcnt + $ios_playcnt + $android_playcnt + $amazon_playcnt;
						}
						else
						{
							$web_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_daily WHERE mode = 9 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$web_playcnt = $db_main2->getvalue($web_sql);
						
							$ios_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_ios_daily WHERE mode = 9 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$ios_playcnt = $db_main2->getvalue($ios_sql);
						
							$android_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_android_daily WHERE mode = 9 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$android_playcnt = $db_main2->getvalue($android_sql);
						
							$amazon_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_amazon_daily WHERE mode = 9 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$amazon_playcnt = $db_main2->getvalue($amazon_sql);
						
							$total_cnt_spin = $web_playcnt + $ios_playcnt + $android_playcnt + $amazon_playcnt;
						}
					}
					else
					{
						if($slot_type == "all")
						{
							$sql = "SELECT SUM(playcount) FROM $table WHERE mode = 9 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
						}
						else
						{
							$sql = "SELECT SUM(playcount) FROM $table WHERE mode = 9 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
						}
					}
				}
				else if($total_mode == 4)
				{
					if($os_type == "4")
					{
						if($slot_type == "all")
						{
							$web_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_daily WHERE betlevel BETWEEN 110 AND 119 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$web_playcnt = $db_main2->getvalue($web_sql);
				
							$ios_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_ios_daily WHERE betlevel BETWEEN 110 AND 119 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$ios_playcnt = $db_main2->getvalue($ios_sql);
				
							$android_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_android_daily WHERE betlevel BETWEEN 110 AND 119 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$android_playcnt = $db_main2->getvalue($android_sql);
				
							$amazon_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_amazon_daily WHERE betlevel BETWEEN 110 AND 119 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$amazon_playcnt = $db_main2->getvalue($amazon_sql);
				
							$total_cnt_spin = $web_playcnt + $ios_playcnt + $android_playcnt + $amazon_playcnt;
						}
						else
						{
							$web_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_daily WHERE betlevel BETWEEN 110 AND 119 AND  slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$web_playcnt = $db_main2->getvalue($web_sql);
				
							$ios_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_ios_daily WHERE betlevel BETWEEN 110 AND 119 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$ios_playcnt = $db_main2->getvalue($ios_sql);
				
							$android_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_android_daily WHERE betlevel BETWEEN 110 AND 119 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$android_playcnt = $db_main2->getvalue($android_sql);
				
							$amazon_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_amazon_daily WHERE betlevel BETWEEN 110 AND 119 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$amazon_playcnt = $db_main2->getvalue($amazon_sql);
				
							$total_cnt_spin = $web_playcnt + $ios_playcnt + $android_playcnt + $amazon_playcnt;
						}
					}
					else
					{
						if($slot_type == "all")
						{
							$sql = "SELECT SUM(playcount) FROM $table WHERE betlevel BETWEEN 110 AND 119 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
						}
						else
						{
							$sql = "SELECT SUM(playcount) FROM $table WHERE  betlevel BETWEEN 110 AND 119 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
						}
					}
				}
				else if($total_mode == 5)
				{
					if($os_type == "4")
					{
						if($slot_type == "all")
						{
							$web_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_daily WHERE betlevel BETWEEN 10 AND 119 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$web_playcnt = $db_main2->getvalue($web_sql);
				
							$ios_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_ios_daily WHERE betlevel BETWEEN 10 AND 119 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$ios_playcnt = $db_main2->getvalue($ios_sql);
				
							$android_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_android_daily WHERE betlevel BETWEEN 10 AND 119 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$android_playcnt = $db_main2->getvalue($android_sql);
				
							$amazon_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_amazon_daily WHERE betlevel BETWEEN 10 AND 119 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$amazon_playcnt = $db_main2->getvalue($amazon_sql);
				
							$total_cnt_spin = $web_playcnt + $ios_playcnt + $android_playcnt + $amazon_playcnt;
						}
						else
						{
							$web_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_daily WHERE betlevel BETWEEN 10 AND 119 AND  slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$web_playcnt = $db_main2->getvalue($web_sql);
				
							$ios_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_ios_daily WHERE betlevel BETWEEN 10 AND 119 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$ios_playcnt = $db_main2->getvalue($ios_sql);
				
							$android_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_android_daily WHERE betlevel BETWEEN 10 AND 119 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$android_playcnt = $db_main2->getvalue($android_sql);
				
							$amazon_sql = "SELECT SUM(playcount) FROM tbl_game_cash_stats_amazon_daily WHERE betlevel BETWEEN 10 AND 119 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
							$amazon_playcnt = $db_main2->getvalue($amazon_sql);
				
							$total_cnt_spin = $web_playcnt + $ios_playcnt + $android_playcnt + $amazon_playcnt;
						}
					}
					else
					{
						if($slot_type == "all")
						{
							$sql = "SELECT SUM(playcount) FROM $table WHERE betlevel BETWEEN 10 AND 119 AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
						}
						else
						{
							$sql = "SELECT SUM(playcount) FROM $table WHERE  betlevel BETWEEN 10 AND 119 AND slottype = $slot_type AND writedate BETWEEN '$end_search_day 00:00:00' AND '$today 23:59:59'";
						}
					}
				}
								
				if($os_type != "4")
				{
					$total_cnt_spin = $db_main2->getvalue($sql);
				}
				
				$jackpot_rate = $total_cnt_spin/$sum_jp_user;
				
				$avg_amount = $sum_jp_amount/$sum_jp_user;
				
?>
				<tr>					
                   	<td class="tdc point"><?= $today ?></td>
                   	<td class="tdc point"><?= number_format($cnt_jackpot) ?></td>
					<td class="tdc point"><?= number_format($sum_jp_user) ?></td>
					<td class="tdc point"><?= number_format($jackpot_rate) ?></td>
					<td class="tdc point"><?= number_format($sum_jp_amount) ?></td>											
					<td class="tdc point"><?= number_format($avg_amount) ?></td>
					<td class="tdc point"><?= number_format($sum_fiesta_cnt) ?></td>
					<td class="tdc point"><?= number_format($fiesta_jp_amount) ?></td>				
				</tr>
<?
			}
?>
			</tbody>
		</table>
	</div>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
    
<?
	$db_main->end();
	$db_main2->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
