<?
	$top_menu = "game_stats";
	$sub_menu = "game_jackpot_deduction_info";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");	
	
	$today = date("Y-m-d");
	
	$total_mode = ($_GET["total_mode"] == "") ? "0" :$_GET["total_mode"];
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	$slot_type = ($_GET["slottype"] == "") ? "all" : $_GET["slottype"];
	
	if($search_start_createdate == "")
		$search_start_createdate = date("Y-m-d", strtotime("-7 day"));
	
	if($search_end_createdate == "")
		$search_end_createdate = $today;
	
	$tail = " WHERE 1=1 ";
	
	if($slot_type != "all")
		$tail .= " AND slottype = $slot_type";  
	
	if($total_mode == 1)
		$tail .= " AND ishigh = 0";
	else if($total_mode == 2)
		$tail .= " AND ishigh = 1";	
	
	if($total_mode == 0)
		$mode_name = "전체";
	else if($total_mode == 1)
		$mode_name = "레귤러";
	else if($total_mode == 2)
		$mode_name = "하이롤러";
	
	$db_game = new CDatabase_Game();
	$db_main2 = new CDatabase_Main2();
	
	$sql = "SELECT objectidx, amount, slottype, ishigh, writedate FROM tbl_jackpot_deduction_force ".
			"$tail AND '$search_start_createdate 00:00:00' <= writedate AND writedate <= '$search_end_createdate 23:59:59' ORDER BY writedate DESC";

	$jackpot_deduction_data = $db_game->gettotallist($sql);
	
	//Slot 정보
	$sql = "SELECT slottype, slotname FROM tbl_slot_list";
	$slottype_list = $db_main2->gettotallist($sql);
	
	$db_game->end();
	$db_main2->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="game_jackpot_deduction_info.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Jackpot Deduction 현황(<?=$mode_name?>)</div>		 
		<div class="search_box">
			mode&nbsp;:&nbsp; 
			<select name="total_mode" id="total_mode">										
					<option value="0" <?= ($total_mode=="0") ? "selected" : "" ?>>전체</option>
					<option value="1" <?= ($total_mode=="1") ? "selected" : "" ?>>레귤러</option>                       
					<option value="2" <?= ($total_mode=="2") ? "selected" : "" ?>>하이롤러</option>					
			</select>&nbsp;&nbsp;&nbsp;
			<font style="font:bold 16px 'Malgun Gothic'; color:#5ab2da;">Slot : </font>&nbsp;&nbsp;
			<select name="slottype" id="slottype">				
				<option value="all"  <?= ($slottype=="all") ? "selected" : "" ?>>ALL</option>
<?
	for ($i=0; $i<sizeof($slottype_list); $i++)
	{
		$slottype = $slottype_list[$i]["slottype"];
		$slotname = $slottype_list[$i]["slotname"];
?>	
				<option value="<?= $slottype ?>"  <?= ($slottype=="$slot_type") ? "selected" : "" ?>><?= $slotname ?></option>
<?						
	}
?>					
			</select>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
                <col width="">
                <col width="">
                <col width="">                
                <col width="">
                <col width="">
            </colgroup>
            <thead>
	            <tr>
	            	<th class="tdc">Objectidx</th>
	                <th class="tdc">금액</th>
	                <th class="tdc">Slot</th>
	                <th class="tdc">Mode</th>
	                <th class="tdc">일자</th>	                	                	                
	            </tr>
            </thead>
            <tbody>
<?			
			for($i=0; $i<sizeof($jackpot_deduction_data); $i++)
			{
				$objectidx =  $jackpot_deduction_data[$i]["objectidx"];
				$amount =  $jackpot_deduction_data[$i]["amount"];
				$slottype =  $jackpot_deduction_data[$i]["slottype"];
				$ishigh =  $jackpot_deduction_data[$i]["ishigh"];
				$writedate =  $jackpot_deduction_data[$i]["writedate"];
				
				if($ishigh == 0)
					$mode = "레귤러";
				else if($ishigh == 1)
					$mode = "하이롤러";
				
				for($j=0; $j<sizeof($slottype_list); $j++)
				{
					if($slottype_list[$j]["slottype"] == $slottype)
					{
						$slot_name = $slottype_list[$j]["slotname"];
						break;
					}
					else
					{
						$slot_name = "Unkown";
					}
				}
?>
				<tr>					
                   	<td class="tdc point"><?= $objectidx ?></td>
                   	<td class="tdc point"><?= number_format($amount) ?></td>
					<td class="tdc point"><?= $slot_name ?></td>
					<td class="tdc point"><?= $mode ?></td>														
					<td class="tdc point"><?= $writedate ?></td>
				</tr>
<?							
				
			}
?>
			</tbody>
		</table>
	</div>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
    
<?	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>