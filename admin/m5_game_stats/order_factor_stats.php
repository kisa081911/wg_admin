<?
    $top_menu = "game_stats";
    $sub_menu = "order_factor_stats";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $startdate = $_GET["startdate"];
    $enddate = $_GET["enddate"];
    $tab = ($_GET["tab"] == "") ? "1":$_GET["tab"];
    
    $pagename = "order_factor_stats.php";
    $search_field = "&startdate=$startdate&enddate=$enddate";
    
    if ($tab != "1" && $tab != "2" && $tab != "3" && $tab != "4")
    	error_back("잘못된 접근입니다.");
    
    //오늘 날짜 정보
    $today = date("Y-m-d");
    $before_day = get_past_date($today,7,"d");
    $startdate = ($startdate == "") ? $before_day : $startdate;
    $enddate = ($enddate == "") ? $today : $enddate;
    
    $db_main2 = new CDatabase_Main2();
    $db_analysis = new CDatabase_Analysis();
    
    $sql = "SELECT slottype, slotname FROM tbl_slot_list";
    $slot_list = $db_main2->gettotallist($sql);
    
    $tail = "WHERE 1=1 AND category NOT IN ('Bigwin', 'SlotPanel', 'AUTO POPUP', 'MainLobby', 'BonusPanel', 'TreatTicket', 'Take5_IN', 'HallOfFamePanel', 'LoungePanel', 'GrandLobby', 'TutorialInGame', 'Tutorial', 'BigwinPopup', 'BigwinPopup_OKBtn', 'BigwinPopup(TIMEOUT)', 'INGAME', 'CONNECT LOST', ".
    		"'ABUSE DETECTED','Trialpay Earn','CONNECT ERROR','Take5') AND category NOT LIKE 'INGAME%' AND category NOT LIKE 'BigwinPopup%' ";
    
    if ($startdate != "")
    	$tail .= "AND writedate >= '$startdate' ";
    
    if ($enddate != "")
    	$tail .=" AND writedate <= '$enddate' ";
    
    if ($tab == "1")
    {
    	$sql = "SELECT category,detail_category,SUM(frequency) AS frequency,SUM(ordercount_point) AS ordercount_point,SUM(orderprice_point) AS orderprice_point,SUM(amount_count) AS amount_count,SUM(amount_price) AS amount_price ".
    			"FROM user_order_factor_log $tail GROUP BY category ORDER BY orderprice_point DESC";
    
    }
    else if ($tab == "2")
    {
    	$sql = "SELECT category,detail_category,SUM(frequency) AS frequency,SUM(ordercount_point) AS ordercount_point,SUM(orderprice_point) AS orderprice_point,SUM(amount_count) AS amount_count,SUM(amount_price) AS amount_price ".
    			"FROM user_order_factor_log $tail GROUP BY category,detail_category ORDER BY orderprice_point DESC";
    
    }
    else if ($tab == "3")
    {
    	$sql = "SELECT category,detail_category,SUM(frequency) AS frequency,SUM(ordercount_point) AS ordercount_point,SUM(orderprice_point) AS orderprice_point,SUM(amount_count) AS amount_count,SUM(amount_price) AS amount_price ".
    			"FROM user_order_factor_log $tail GROUP BY category ORDER BY ordercount_point DESC";
    
    }
    else if ($tab == "4")
    {
    	$sql = "SELECT category,detail_category,SUM(frequency) AS frequency,SUM(ordercount_point) AS ordercount_point,SUM(orderprice_point) AS orderprice_point,SUM(amount_count) AS amount_count,SUM(amount_price) AS amount_price ".
    			"FROM user_order_factor_log $tail GROUP BY category,detail_category ORDER BY ordercount_point DESC";
    
    }
    
    $action_list = $db_analysis->gettotallist($sql);
    
    
    $total_frequency = $db_analysis->getvalue("SELECT SUM(frequency) FROM user_order_factor_log $tail");
    $total_ordercount_point = $db_analysis->getvalue("SELECT SUM(ordercount_point) FROM user_order_factor_log $tail");
    $total_orderprice_point = $db_analysis->getvalue("SELECT SUM(orderprice_point) FROM user_order_factor_log $tail");
    $total_amount_count = $db_analysis->getvalue("SELECT SUM(amount_count) FROM user_order_factor_log $tail");
    $total_amount_price = $db_analysis->getvalue("SELECT SUM(amount_price) FROM user_order_factor_log $tail");
    
    $db_main2->end();
    $db_analysis->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

	function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }

    function search()
    {
        var search_form = document.search_form;

        search_form.submit();
    }
    
    function tab_change(tab)
    {
        window.location.href = "order_factor_stats.php?tab="+tab+"<?= $search_field ?>";
    }
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 결제 요인 분석</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
            <div class="search_box">
                <input type="hidden" id="tab" name="tab" value="<?= $tab ?>" />
                <input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> ~
                <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
                <input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 총 <?= make_price_format(round($total_amount_count)) ?> 건($<?= number_format((round($total_amount_price)*0.1), 1) ?>) 통계입니다
	</div>
	
	<ul class="tab">
		<li id="tab_1" class="<?= ($tab == "1") ? "select" : "" ?>" onclick="tab_change('1')">구매액별</li>
		<li id="tab_2" class="<?= ($tab == "2") ? "select" : "" ?>" onclick="tab_change('2')">구매액별(상세)</li>
		<li id="tab_3" class="<?= ($tab == "3") ? "select" : "" ?>" onclick="tab_change('3')">구매건수별</li>
		<li id="tab_4" class="<?= ($tab == "4") ? "select" : "" ?>" onclick="tab_change('4')">구매건수별(상세)</li>
	</ul>
	<div class="vbar_unit" style="height:30px">
		<div class="vbar_lbl_title" style="width:230px"> 카테고리 </div>
		<div class="vbar_1_title" style="width:190px">빈도</div>
		<div class="vbar_1_title" style="width:210px">가중점수</div>
		<div class="vbar_2_title" style="width:210px">기여도</div>
		<div class="vbar_2_title" style="width:190px">기여<?= ($tab == "1" || $tab == "2") ? "금액" : "횟수" ?></div>
	</div>
<?
    foreach ($action_list as $action)
    {   
    	if($tab == "2" || $tab == "4")
    	{ 	
	    	$explode_detail_category = explode(" ", $action["detail_category"]);    	
	    	
	    	preg_match_all("/[^()]+/", $explode_detail_category[2], $slotidx);
	    	
	    	for($j=0; $j<sizeof($slot_list); $j++)
	    	{
	    		if($slot_list[$j]["slottype"] == $slotidx[0][0])
	    		{
	    			$slotname = $slot_list[$j]["slotname"];
	    			break;
	    		}
	    		
	    		$slotname = $slotidx[0][0];
	    	}
	
	    	$detail_category = $explode_detail_category[0]." ".$explode_detail_category[1]."(".$slotname.")";
    	}
    	
    	$category = ($tab == "1" || $tab == "3")? $action["category"] : $detail_category;        
        $frequency = $action["frequency"];
        $ordercount_point = $action["ordercount_point"];
        $orderprice_point = $action["orderprice_point"];
        $amount_price = $action["amount_price"];
        $amount_count = $action["amount_count"];
?>
            <div class="vbar_unit" onmouseover="className='vbar_unit vbar_over'" onmouseout="className='vbar_unit'">
                <div class="vbar_lbl" style="width:230px"><?= $category ?></div>
                <div class="vbar_1" style="width:190px">
                    <div class="vbar_1_bar" style="width:<?= get_bar_chart_value(100, $total_frequency, $frequency)?>px"></div>
                    <div class="vbar_number"><?= make_price_format($frequency) ?></div>
                </div>
<?  
        if ($tab == "1" || $tab == "2")
        {
?>
                <div class="vbar_1" style="width:230px">
                    <div class="vbar_1_bar" style="width:<?= get_bar_chart_value(120, $total_orderprice_point, $orderprice_point)?>px"></div>
                    <div class="vbar_number"><?= make_price_format($orderprice_point) ?></div>
                </div>
                <div class="vbar_3" style="width:190px">
                    <div class="vbar_3_bar" style="width:<?= get_bar_chart_value(100, $total_orderprice_point, $orderprice_point)?>px"></div>
                    <div class="vbar_number"><?= (round($orderprice_point / $total_orderprice_point * 10000) / 100)."%" ?></div>
                </div>
                <div class="vbar_1" style="width:230px">
                    <div class="vbar_1_bar" style="width:<?= get_bar_chart_value(120, $total_orderprice_point, $orderprice_point)?>px"></div>
                    <div class="vbar_number">$<?= round($amount_price * 10) / 100 ?></div>
                </div>
            </div>
<?
        }
        else if ($tab == "3" || $tab == "4")
        {
?>
    
                <div class="vbar_1" style="width:230px">
                    <div class="vbar_1_bar" style="width:<?= get_bar_chart_value(120, $total_ordercount_point, $ordercount_point)?>px"></div>
                    <div class="vbar_number"><?= make_price_format($ordercount_point) ?></div>
                </div>
                <div class="vbar_3" style="width:190px">
                    <div class="vbar_3_bar" style="width:<?= get_bar_chart_value(100, $total_ordercount_point, $ordercount_point)?>px"></div>
                    <div class="vbar_number"><?= (round($ordercount_point / $total_ordercount_point * 10000) / 100)."%" ?></div>
                </div>
                <div class="vbar_1" style="width:230px">
                    <div class="vbar_1_bar" style="width:<?= get_bar_chart_value(120, $total_ordercount_point, $ordercount_point)?>px"></div>
                    <div class="vbar_number"><?= round($amount_count * 100) / 100 ?>건</div>
                </div>
            </div>
<?
        }
    }
?> 
</div>
<!--  //CONTENTS WRAP -->
        
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>