<?
    $top_menu = "game_stats";
    $sub_menu = "christmas_stat_daily";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $today = date("Y-m-d");
    
    $os_type = ($_GET["os_type"] == "") ? "4" :$_GET["os_type"];
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];

    $event_idx = 6;
    
    if($search_start_createdate == "")
        $search_start_createdate = date("Y-m-d", strtotime("-3 day"));
        
    if($search_end_createdate == "")
        $search_end_createdate = $today;
        
    $tail = "";
    
    if($os_type == "0")
    {
        $os_txt = "Web";
        $os_type_sql = "AND os_type = 0 ";
        
    }
    else if($os_type == "1")
    {
        $os_txt = "IOS";
        $os_type_sql = "AND os_type = 1 ";
    }
    else if($os_type == "2")
    {
        $os_txt = "Android";
        $os_type_sql = "AND os_type = 2 ";
    }
    else if($os_type == "3")
    {
        $os_txt = "Amazon";
        $os_type_sql = "AND os_type = 3 ";
    }
    else if($os_type == "4")
    {
        $os_txt = "Total";
    }
        
    $db_main2 = new CDatabase_Main2();
    
        $sql = " SELECT today, collection_cnt, SUM(usercount) as usercnt, SUM(reward) as reward FROM tbl_user_collection_stat_daily
                    WHERE today BETWEEN '$search_start_createdate' AND '$search_end_createdate' $os_type_sql
                    GROUP BY today,collection_cnt
                    ORDER BY today desc, collection_cnt asc ";
        $christmas_data = $db_main2->gettotallist($sql);
    
    function get_today_row_array($list, $today)
    {
        $row_array = array();
        
        for ($i=0; $i<sizeof($list); $i++)
        {
            if ($list[$i]["today"] == $today)
            {
                array_push($row_array, $list[$i]["collection_cnt"]);
            }
        }
        
        return $row_array;
    }
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#start_createdate").datepicker({ });
	});
	
	$(function() {
		$("#end_createdate").datepicker({ });
	});
	
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.startdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    } 
	
	    if (search_form.enddate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    } 
	
	    search_form.submit();
	}
	function change_os_type(type)
	{
		var search_form = document.search_form;

		var total = document.getElementById("type_total");
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
		else if (type == "4")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap" style="height:76px;">
		<div class="title"><?= $top_menu_txt ?> &gt; <?=$platform_name?> christmas 통계</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
			<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />

        		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
					<input type="button" class="<?= ($os_type == "4") ? "btn_schedule_select" : "btn_schedule" ?>" value="ALL" id="type_total" onclick="change_os_type('4')"    />
					<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
					<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
					<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
					<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
				</span>
				<span class="search_lbl ml20">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" /> ~
				<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	<div class="search_result"><?= $os_txt?></div>
			<div class="search_result">
        		<span><?= $search_start_createdate ?> ~ <?= $search_end_createdate ?></span> 통계입니다
        	</div>
    		<table class="tbl_list_basic1">
    		<colgroup>
    			<col width="70">
    			<col width="120">
    			<col width="120">
                <col width="120">
                <col width="120">
                <col width="90">
                <col width="90">
                <col width="90">
                <col width="90">
    		</colgroup>
            <thead>
                <tr>
                 	<th>날짜</th>
        			<th>완성 리워드<br/>유저수</th>
        			<th>완성 리워드<br/>유저금액</th>
        			<th>획득 횟수</th>
        			<th>획득 유저 수</th>
        			<th>걸렉션 리워드 금액<br/>(전체 달성 금액 포함)</th>
        			<th>1인당 평균 <br/> 리워드 금액</th>
        		</tr>
            </thead>
            <tbody>
	
<?
    $christmas_data_stat = array();
    
    for($i=0; $i<sizeof($christmas_data); $i++)
    {
        $today = $christmas_data[$i]["today"];
        $collection_cnt = $christmas_data[$i]["collection_cnt"];
        $usercnt = $christmas_data[$i]["usercnt"];
        $reward = $christmas_data[$i]["reward"];
        
        $day_sum_usercnt += $christmas_data[$i]["usercnt"];
        $day_sum_reward += $christmas_data[$i]["reward"];
        
        ?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
                if($currenttoday != $today)
                {
                    $currenttoday = $today;
                    $viral_list = get_today_row_array($christmas_data, $today);
                    $complete_arr = $db_main2->getarray("SELECT COUNT(DISTINCT useridx) as usercount, SUM(collection_coin) as collection_coin FROM `tbl_user_collection_log` WHERE event_idx = $event_idx AND collect_type = 1 AND DATE_FORMAT(write_date,'%Y-%m-%d') = '$today'");
                    $complete_coin =  $complete_arr["collection_coin"];
                    $complete_usercount =  $complete_arr["usercount"];
                    
                    $day_sum_complete_coin += $complete_coin;
                    $day_sum_complete_usercount += $complete_usercount;
?>
					<td class="tdc point_title" rowspan="<?= sizeof($viral_list)?>" valign="center"><?= $today ?></td>
					<td class="tdc point_title" rowspan="<?= sizeof($viral_list)?>" valign="center"><?= number_format($complete_usercount) ?></td>
					<td class="tdc point_title" rowspan="<?= sizeof($viral_list)?>" valign="center"><?= number_format($complete_coin)?></td>
<?
                }
?>
            <td class="tdc"><?= $collection_cnt?></td>
            <td class="tdc"><?= number_format($usercnt) ?></td>
            <td class="tdc"><?= number_format($reward) ?></td>
            <td class="tdc"><?= number_format(ROUND($reward/$usercnt)) ?></td>
        </tr>
<?
	}
	if(sizeof($christmas_data) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="7">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
            </tbody>
    	</table>
</div>
<!--  //CONTENTS WRAP -->
<?
    $db_main2->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>