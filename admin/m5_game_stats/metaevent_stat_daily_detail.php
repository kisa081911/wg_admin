<?
    $top_menu = "game_stats";
    $sub_menu = "metaevent_stat_daily_detail";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $today = date("Y-m-d");
    
    $os_type = ($_GET["os_type"] == "") ? "4" :$_GET["os_type"];
    $search_event_idx = ($_GET["event_idx"] == "") ? "ALL" : $_GET["event_idx"];
    $search_startdate = $_GET["startdate"];
    $search_enddate = $_GET["enddate"];

    
    if($search_startdate == "")
        $search_startdate = date("Y-m-d", strtotime("-3 day"));
        
    if($search_enddate == "")
        $search_enddate = $today;
        
    $tail = "";
    
    if($os_type == "0") 
    {
        $os_txt = "Web";
        $os_type_sql = "AND os_type = 0 ";
        
    }
    else if($os_type == "1")
    {
        $os_txt = "IOS";
        $os_type_sql = "AND os_type = 1 ";
    }
    else if($os_type == "2")
    {
        $os_txt = "Android";
        $os_type_sql = "AND os_type = 2 ";
    }
    else if($os_type == "3")
    {
        $os_txt = "Amazon";
        $os_type_sql = "AND os_type = 3 ";
    }
    else if($os_type == "4")
    {
        $os_txt = "Total";
    }
    
    if($search_event_idx != "ALL")
        $tail = "AND event_idx = $search_event_idx";
    
    $db_main2 = new CDatabase_Main2();
    
    $event_idx_arr = $db_main2->gettotallist("SELECT event_idx FROM tbl_meta_event_setting ORDER BY event_idx ASC");
    
    $sql = " SELECT DATE_FORMAT(write_date,'%Y-%m-%d') AS today ,stage, COUNT(useridx) AS cnt, SUM(amount) AS total_amount ,write_date
             FROM tbl_user_meta_event_collect_log 
             WHERE collect_type = 1 $tail $os_type_sql AND useridx > 20000 AND  DATE_FORMAT(write_date,'%Y-%m-%d') BETWEEN '$search_startdate' AND '$search_enddate'
             GROUP BY DATE_FORMAT(write_date,'%Y-%m-%d') ,stage  ORDER BY DATE_FORMAT(write_date,'%Y-%m-%d')  DESC, stage ASC ";
    $metaevent_data = $db_main2->gettotallist($sql);
    
    function get_today_row_array($list, $today)
    {
        $row_array = array();
        
        for ($i=0; $i<sizeof($list); $i++)
        {
            if ($list[$i]["today"] == $today)
            {
                array_push($row_array, $list[$i]["stage"]);
            }
        }
        
        return $row_array;
    }
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#startdate").datepicker({ });
	});
	
	$(function() {
		$("#enddate").datepicker({ });
	});
	
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.startdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    } 
	
	    if (search_form.enddate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    } 
	
	    search_form.submit();
	}
	function change_os_type(type)
	{
		var search_form = document.search_form;

		var total = document.getElementById("type_total");
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
		else if (type == "4")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap" style="height:76px;">
		<div class="title"><?= $top_menu_txt ?> &gt; <?=$platform_name?> Meta Event 통계</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
			<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />

        		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
					<input type="button" class="<?= ($os_type == "4") ? "btn_schedule_select" : "btn_schedule" ?>" value="ALL" id="type_total" onclick="change_os_type('4')"    />
					<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
					<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
					<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
					<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
				</span>
				
				<span class="search_lbl ml20">이벤트 회차&nbsp;&nbsp;&nbsp;</span>
				<select name="event_idx" id="event_idx">										
					<option value="ALL" <?= ($event_idx=="ALL") ? "selected" : "" ?>>전체</option>
<?
    for ($i=0; $i<sizeof($event_idx_arr); $i++)
	{
	    $event_idx = $event_idx_arr[$i]["event_idx"];
?>	
                    
					<option value="<?= $event_idx ?>" <?= ($event_idx == $search_event_idx) ? "selected=\"true\"" : "" ?>><?= $event_idx ?></option>
<?						
	}
?>				</select>&nbsp;&nbsp;&nbsp;
				<span class="search_lbl ml20">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $search_startdate ?>" maxlength="10" style="width:65px"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" /> ~
				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $search_enddate ?>" style="width:65px" maxlength="10"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	<div class="search_result"><?= $os_txt?></div>
			<div class="search_result">
        		<span><?= $search_startdate ?> ~ <?= $search_enddate ?></span> 통계입니다
        	</div>
    		<table class="tbl_list_basic1">
    		<colgroup>
    			<col width="70">
    			<col width="120">
                <col width="120">
                <col width="120">
    		</colgroup>
            <thead>
                <tr>
                 	<th>날짜</th>
        			<th>Treasure 단계</th>
        			<th>획득 유저 수</th>
        			<th>각 단계 완료 시<br/>평균 획득 금액</th>
        		</tr>
            </thead>
            <tbody>
	
<?
    $metaevent_data_stat = array();
    
    for($i=0; $i<sizeof($metaevent_data); $i++)
    {
        $today = $metaevent_data[$i]["today"];
        $stage = $metaevent_data[$i]["stage"];
        $usercnt = $metaevent_data[$i]["cnt"];
        $reward = $metaevent_data[$i]["total_amount"];
        ?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
                if($currenttoday != $today)
                {
                    $currenttoday = $today;
                    $viral_list = get_today_row_array($metaevent_data, $today);
?>
					<td class="tdc point_title" rowspan="<?= sizeof($viral_list)+1?>" valign="center"><?= $today ?></td>
<?
                }
?>

            <td class="tdc"><?= $stage?></td>
            <td class="tdc"><?= number_format($usercnt) ?></td>
            <td class="tdc"><?= number_format(ROUND($reward/$usercnt)) ?></td>
       </tr>
  
<?
            if($today!=$metaevent_data[$i+1]["today"])
            {
                $complete_arr = $db_main2->getarray("SELECT COUNT(DISTINCT useridx) as usercount, SUM(amount) as amount FROM `tbl_user_meta_event_collect_log` WHERE collect_type = 2 $tail $os_type_sql AND useridx > 20000  AND DATE_FORMAT(write_date,'%Y-%m-%d') = '$today'");
                $complete_coin =  $complete_arr["amount"];
                $complete_usercount =  $complete_arr["usercount"];
?>				
     <tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
			<td class="tdc">완료 유저</td>
            <td class="tdc"><?= number_format($complete_usercount) ?></td>
            <td class="tdc"><?= number_format(ROUND($complete_coin/$complete_usercount)) ?></td>
        </tr>
<?
            }
        
?>            
            
<?
	}
	if(sizeof($metaevent_data) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="7">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
            </tbody>
    	</table>
</div>
<!--  //CONTENTS WRAP -->
<?
    $db_main2->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>