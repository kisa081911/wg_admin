<?
	$top_menu = "game_stats";
	$sub_menu = "daily_highroller_famelevel_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$famelevel = ($_GET["famelevel"] == "") ? "0" :$_GET["famelevel"];
	$search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    
    if($search_start_createdate == "")
    	$search_start_createdate = "2016-03-10";
    
    if($search_end_createdate == "")
    	$search_end_createdate = $today;
    
    $famelevel_sql = "";
    
    if($famelevel == 1)
    	$famelevel_sql = " AND famelevel <= 10 ";
    else if($famelevel == 2)
    	$famelevel_sql = " AND famelevel > 10 AND famelevel <= 20 ";
    else if($famelevel == 3)
    	$famelevel_sql = " AND famelevel > 20 AND famelevel <= 30 ";
    else if($famelevel == 4)
    	$famelevel_sql = " AND famelevel > 30 AND famelevel <= 20 ";
    else if($famelevel == 5)
    	$famelevel_sql = " AND famelevel > 40 AND famelevel <= 50 ";
    else if($famelevel == 6)
    	$famelevel_sql = " AND famelevel > 50 AND famelevel <= 60 ";
    else if($famelevel == 7)
    	$famelevel_sql = " AND famelevel > 60 AND famelevel <= 70 ";
    else if($famelevel == 8)
    	$famelevel_sql = " AND famelevel > 70 AND famelevel <= 80 ";
    else if($famelevel == 9)
    	$famelevel_sql = " AND famelevel > 80 AND famelevel <= 90 ";
    else if($famelevel == 10)
    	$famelevel_sql = " AND famelevel > 90 ";
	
	$db_other = new CDatabase_Other();
	
	$sql = "SELECT today, ".
			"(CASE WHEN famelevel > 90 THEN '090_100' ".
			"WHEN famelevel > 80 THEN '080_090' ".
			"WHEN famelevel > 70 THEN '070_080' ".
			"WHEN famelevel > 60 THEN '060_070' ".
			"WHEN famelevel > 50 THEN '050_060' ".
			"WHEN famelevel > 40 THEN '040_050' ".
			"WHEN famelevel > 30 THEN '030_040' ".
			"WHEN famelevel > 20 THEN '020_030' ".
			"WHEN famelevel > 10 THEN '010_020' ".
			"ELSE '000_010' END) AS std_famelevel, ".
			"COUNT(useridx) AS usercnt, FLOOR(AVG(currentcoin)) AS currentcoin, ".
			"SUM(h_moneyin) AS h_moneyin, SUM(h_moneyout) AS h_moneyout, ROUND(SUM(h_moneyout)/SUM(h_moneyin)*100, 2) AS h_winrate,	".
			"SUM(h_playtime) AS h_playtime, SUM(h_playcount) AS h_playcount, ".
			"SUM(purchasecount) AS purchasecount, SUM(purchaseamount) AS purchaseamount, SUM(purchasecoin) AS purchasecoin, ".
			"SUM(famelevel) AS famelevel, SUM(ty_point) AS ty_point, SUM(freecoin) AS freecoin, SUM(jackpot) AS jackpot ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' AND h_moneyin >= 10000000 $famelevel_sql".
			"GROUP BY today, std_famelevel ".
			"ORDER BY today DESC, std_famelevel ASC;";
	$highroller_famelevel_data = $db_other->gettotallist($sql);	
	
	$sql = "SELECT today, ".
			"(CASE WHEN famelevel > 90 THEN '090_100' ".
			"WHEN famelevel > 80 THEN '080_090' ".
			"WHEN famelevel > 70 THEN '070_080' ".
			"WHEN famelevel > 60 THEN '060_070' ".
			"WHEN famelevel > 50 THEN '050_060' ".
			"WHEN famelevel > 40 THEN '040_050' ".
			"WHEN famelevel > 30 THEN '030_040' ".
			"WHEN famelevel > 20 THEN '020_030' ".
			"WHEN famelevel > 10 THEN '010_020' ".
			"ELSE '000_010' END) AS std_famelevel, ".
			"COUNT(useridx) AS usercnt ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' $famelevel_sql".
			"GROUP BY today, std_famelevel ".
			"ORDER BY today DESC, std_famelevel ASC;";
	$famelevel_data = $db_other->gettotallist($sql);
	
	$famelevel_data_array = array();
	
	for($j = 0; $j < sizeof($famelevel_data); $j++)
	{
		$famelevel_data_today = $famelevel_data[$j]["today"];
		$famelevel_data_std_famelevel = $famelevel_data[$j]["std_famelevel"];
		$famelevel_data_usercnt = $famelevel_data[$j]["usercnt"];
		
		$famelevel_data_array[$famelevel_data_today][$famelevel_data_std_famelevel] = $famelevel_data_usercnt;
	}
	
	$sql = "SELECT ".
			"(CASE WHEN famelevel > 90 THEN '090_100' ".
			"WHEN famelevel > 80 THEN '080_090' ".
			"WHEN famelevel > 70 THEN '070_080' ".
			"WHEN famelevel > 60 THEN '060_070' ".
			"WHEN famelevel > 50 THEN '050_060' ".
			"WHEN famelevel > 40 THEN '040_050' ".
			"WHEN famelevel > 30 THEN '030_040' ".
			"WHEN famelevel > 20 THEN '020_030' ".
			"WHEN famelevel > 10 THEN '010_020' ".
			"ELSE '000_010' END) AS std_famelevel, ".
			"COUNT(useridx) AS usercnt, FLOOR(AVG(currentcoin)) AS currentcoin, ".
			"SUM(h_moneyin) AS h_moneyin, SUM(h_moneyout) AS h_moneyout, ROUND(SUM(h_moneyout)/SUM(h_moneyin)*100, 2) AS h_winrate,	".
			"SUM(h_playtime) AS h_playtime, SUM(h_playcount) AS h_playcount, ".
			"SUM(purchasecount) AS purchasecount, SUM(purchaseamount) AS purchaseamount, SUM(purchasecoin) AS purchasecoin, ".
			"SUM(famelevel) AS famelevel, SUM(ty_point) AS ty_point, SUM(freecoin) AS freecoin, SUM(jackpot) AS jackpot ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' AND h_moneyin >= 10000000 $famelevel_sql".
			"GROUP BY std_famelevel ".
			"ORDER BY std_famelevel ASC;";
	$total_highroller_famelevel_data = $db_other->gettotallist($sql);
	
	$sql = "SELECT ".
			"(CASE WHEN famelevel > 90 THEN '090_100' ".
			"WHEN famelevel > 80 THEN '080_090' ".
			"WHEN famelevel > 70 THEN '070_080' ".
			"WHEN famelevel > 60 THEN '060_070' ".
			"WHEN famelevel > 50 THEN '050_060' ".
			"WHEN famelevel > 40 THEN '040_050' ".
			"WHEN famelevel > 30 THEN '030_040' ".
			"WHEN famelevel > 20 THEN '020_030' ".
			"WHEN famelevel > 10 THEN '010_020' ".
			"ELSE '000_010' END) AS std_famelevel, ".
			"COUNT(useridx) AS usercnt ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' $famelevel_sql".
			"GROUP BY std_famelevel ".
			"ORDER BY std_famelevel ASC;";
	$total_famelevel_data = $db_other->gettotallist($sql);
	
	$total_famelevel_data_array = array();
	
	for($j = 0; $j < sizeof($total_famelevel_data); $j++)
	{
		$famelevel_data_std_famelevel = $total_famelevel_data[$j]["std_famelevel"];
		$famelevel_data_usercnt = $total_famelevel_data[$j]["usercnt"];
	
		$total_famelevel_data_array[$famelevel_data_std_famelevel] = $famelevel_data_usercnt;
	}

?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="daily_highroller_famelevel_stats.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 하일롤러 FameLevel 통계</div>
		<div class="search_box">
			Famelevel&nbsp;:&nbsp; 
			<select name="famelevel" id="famelevel">
					<option value="" <?= ($famelevel=="0") ? "selected" : "" ?>>전체</option>					
					<option value="1" <?= ($famelevel=="1") ? "selected" : "" ?>>0 ~ 10</option>
					<option value="2" <?= ($famelevel=="2") ? "selected" : "" ?>>11 ~ 20</option>                       
					<option value="3" <?= ($famelevel=="3") ? "selected" : "" ?>>21 ~ 30</option>
					<option value="4" <?= ($famelevel=="4") ? "selected" : "" ?>>31 ~ 40</option>
					<option value="5" <?= ($famelevel=="5") ? "selected" : "" ?>>41 ~ 50</option>
					<option value="6" <?= ($famelevel=="6") ? "selected" : "" ?>>51 ~ 60</option>
					<option value="7" <?= ($famelevel=="7") ? "selected" : "" ?>>61 ~ 70</option>
					<option value="8" <?= ($famelevel=="8") ? "selected" : "" ?>>71 ~ 80</option>
					<option value="9" <?= ($famelevel=="9") ? "selected" : "" ?>>81 ~ 90</option>
					<option value="10" <?= ($famelevel=="10") ? "selected" : "" ?>>91 ~ 100</option>
			</select>&nbsp;&nbsp;&nbsp;
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	
	<div id="tab_content_1">
            <table class="tbl_list_basic1" style="width:1300px">
            <colgroup>
                <col width="70">
                <col width="90">
                <col width="50">
                <col width="100">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70"> 
                <col width="100">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">                 
            </colgroup>
            <thead>
            <tr>
                <th>날짜</th>
                <th class="tdc">Famelevel</th>
                <th class="tdc">유저수</th>
                <th class="tdc">평균보유코인</th>                
                <th class="tdc">moneyin</th>
                <th class="tdc">moneyout</th>
                <th class="tdc">승률</th>
                <th class="tdc">평균 playtime</th>
                <th class="tdc">평균 playcount</th>
                <th class="tdc">결제횟수</th>
                <th class="tdc">결제금액</th>
                <th class="tdc">결제코인</th>
                <th class="tdc">평균 FameLevel</th>
                <th class="tdc">jackpot</th>
                <th class="tdc">전체유저<br>대비</th>
            </tr>
            </thead>
            <tbody>
<?
			for($i=0; $i<sizeof($highroller_famelevel_data); $i++)
			{
				$today = $highroller_famelevel_data[$i]["today"];
				$std_famelevel = $highroller_famelevel_data[$i]["std_famelevel"];
				$usercnt = $highroller_famelevel_data[$i]["usercnt"];
				$currentcoin = $highroller_famelevel_data[$i]["currentcoin"];
				$h_moneyin = $highroller_famelevel_data[$i]["h_moneyin"];
				$h_moneyout = $highroller_famelevel_data[$i]["h_moneyout"];
				$h_winrate = $highroller_famelevel_data[$i]["h_winrate"];
				$h_playtime = $highroller_famelevel_data[$i]["h_playtime"]/$usercnt;
				$h_playcount = $highroller_famelevel_data[$i]["h_playcount"]/$usercnt;
				$purchasecount = $highroller_famelevel_data[$i]["purchasecount"];
				$purchaseamount = $highroller_famelevel_data[$i]["purchaseamount"]/10;
				$purchasecoin = $highroller_famelevel_data[$i]["purchasecoin"];
				$famelevel = $highroller_famelevel_data[$i]["famelevel"]/$usercnt;				
				$jackpot = $highroller_famelevel_data[$i]["jackpot"];
				$famelevel_usercnt = $famelevel_data_array[$today][$std_famelevel];
				
				$usercnt_rate = ($usercnt/$famelevel_usercnt)*100;
				
				$std_famelevel_str = "";
				
				if($std_famelevel == "000_010")
					$std_famelevel_str = "0 ~ 10";
				else if($std_famelevel == "010_020")
					$std_famelevel_str = "11 ~ 20";
				else if($std_famelevel == "020_030")
					$std_famelevel_str = "21 ~ 30";
				else if($std_famelevel == "030_040")
					$std_famelevel_str = "31 ~ 40";
				else if($std_famelevel == "040_050")
					$std_famelevel_str = "41 ~ 50";
				else if($std_famelevel == "050_060")
					$std_famelevel_str = "51 ~ 60";
				else if($std_famelevel == "060_070")
					$std_famelevel_str = "61 ~ 70";
				else if($std_famelevel == "070_080")
					$std_famelevel_str = "71 ~ 80";
				else if($std_famelevel == "080_090")
					$std_famelevel_str = "81 ~ 90";
				else if($std_famelevel == "090_100")
					$std_famelevel_str = "91 ~ 100";				

?>
				<tr>
<?
				if($i == 0 || $today != $highroller_famelevel_data[$i-1]["today"])
				{
					if($std_famelevel_str != "")
					{
						$sql = "SELECT today, ".
								"(CASE WHEN famelevel > 90 THEN '090_100' ".
								"WHEN famelevel > 80 THEN '080_090' ".
								"WHEN famelevel > 70 THEN '070_080' ".
								"WHEN famelevel > 60 THEN '060_070' ".
								"WHEN famelevel > 50 THEN '050_060' ".
								"WHEN famelevel > 40 THEN '040_050' ".
								"WHEN famelevel > 30 THEN '030_040' ".
								"WHEN famelevel > 20 THEN '020_030' ".
								"WHEN famelevel > 10 THEN '010_020' ".
								"ELSE '000_010' END) AS std_famelevel ".								
								"FROM `tbl_user_playstat_daily` ".
								"WHERE '$today' <= today AND today <= '$today' AND h_moneyin >= 10000000 $famelevel_sql".
								"GROUP BY std_famelevel ".
								"ORDER BY std_famelevel ASC;";
						$highroller_famelevel_rowdata = $db_other->gettotallist($sql);
?>				
					<td class="tdc point_title" rowspan="<?=sizeof($highroller_famelevel_rowdata)?>"><?= $today ?></td>
					
<?
					}
					else
					{
						
?>
						<td class="tdc point_title" rowspan="1"><?= $today ?></td>
<?					
					} 
				}
?>	
	
					<td class="tdc"><?= $std_famelevel_str ?></td>
					<td class="tdc"><?= number_format($usercnt) ?></td>
					<td class="tdc"><?= number_format($currentcoin) ?></td>
					<td class="tdc"><?= number_format($h_moneyin) ?></td>
					<td class="tdc"><?= number_format($h_moneyout) ?></td>
					<td class="tdc"><?= $h_winrate ?>%</td>
					<td class="tdc"><?= number_format($h_playtime) ?></td>
					<td class="tdc"><?= number_format($h_playcount) ?></td>
					<td class="tdc"><?= number_format($purchasecount) ?></td>
					<td class="tdc">$<?= number_format($purchaseamount) ?></td>
					<td class="tdc"><?= number_format($purchasecoin) ?></td>
					<td class="tdc"><?= number_format($famelevel) ?></td>
					<td class="tdc"><?= number_format($jackpot) ?></td>
					<td class="tdc"><?= number_format($usercnt_rate,2) ?>%</td>
				</tr>
<?
			}
			
			for($i=0; $i<sizeof($total_highroller_famelevel_data); $i++)
			{
				$today = $total_highroller_famelevel_data[$i]["today"];
				$std_famelevel = $total_highroller_famelevel_data[$i]["std_famelevel"];
				$usercnt = $total_highroller_famelevel_data[$i]["usercnt"];
				$currentcoin = $total_highroller_famelevel_data[$i]["currentcoin"];
				$h_moneyin = $total_highroller_famelevel_data[$i]["h_moneyin"];
				$h_moneyout = $total_highroller_famelevel_data[$i]["h_moneyout"];
				$h_winrate = $total_highroller_famelevel_data[$i]["h_winrate"];
				$h_playtime = $total_highroller_famelevel_data[$i]["h_playtime"]/$usercnt;
				$h_playcount = $total_highroller_famelevel_data[$i]["h_playcount"]/$usercnt;
				$purchasecount = $total_highroller_famelevel_data[$i]["purchasecount"];
				$purchaseamount = $total_highroller_famelevel_data[$i]["purchaseamount"]/10;
				$purchasecoin = $total_highroller_famelevel_data[$i]["purchasecoin"];
				$famelevel = $total_highroller_famelevel_data[$i]["famelevel"]/$usercnt;				
				$jackpot = $total_highroller_famelevel_data[$i]["jackpot"];
				$famelevel_usercnt = $total_famelevel_data_array[$std_famelevel];
				
				$usercnt_rate = ($usercnt/$famelevel_usercnt)*100;

				$std_famelevel_str = "";
				
				if($std_famelevel == "000_010")
					$std_famelevel_str = "0 ~ 10";
				else if($std_famelevel == "010_020")
					$std_famelevel_str = "11 ~ 20";
				else if($std_famelevel == "020_030")
					$std_famelevel_str = "21 ~ 30";
				else if($std_famelevel == "030_040")
					$std_famelevel_str = "31 ~ 40";
				else if($std_famelevel == "040_050")
					$std_famelevel_str = "41 ~ 50";
				else if($std_famelevel == "050_060")
					$std_famelevel_str = "51 ~ 60";
				else if($std_famelevel == "060_070")
					$std_famelevel_str = "61 ~ 70";
				else if($std_famelevel == "070_080")
					$std_famelevel_str = "71 ~ 80";
				else if($std_famelevel == "080_090")
					$std_famelevel_str = "81 ~ 90";
				else if($std_famelevel == "090_100")
					$std_famelevel_str = "91 ~ 100";
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($i == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($total_highroller_famelevel_data)?>" valign="center">Total</td>
<?
				}
?>
					<td class="tdc"><?= $std_famelevel_str ?></td>
					<td class="tdc"><?= number_format($usercnt) ?></td>
					<td class="tdc"><?= number_format($currentcoin) ?></td>
					<td class="tdc"><?= number_format($h_moneyin) ?></td>
					<td class="tdc"><?= number_format($h_moneyout) ?></td>
					<td class="tdc"><?= $h_winrate ?>%</td>
					<td class="tdc"><?= number_format($h_playtime) ?></td>
					<td class="tdc"><?= number_format($h_playcount) ?></td>
					<td class="tdc"><?= number_format($purchasecount) ?></td>
					<td class="tdc">$<?= number_format($purchaseamount) ?></td>
					<td class="tdc"><?= number_format($purchasecoin) ?></td>
					<td class="tdc"><?= number_format($famelevel) ?></td>
					<td class="tdc"><?= number_format($jackpot) ?></td>
					<td class="tdc"><?= number_format($usercnt_rate,2) ?>%</td>
				</tr>

<?
			}
?>
			</tbody>
            </table>
     	</div>        
     </form>	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_other->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>