<?
    $top_menu = "game_stats";
    $sub_menu = "ab_test";
    
    include_once("../../common/dbconnect/db_util_redshift.inc.php");
    include_once($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $db_redshift = new CDatabase_Redshift();
    
    $sql = "select a1.markingidx, title, start_date, status, is_test, user_cnt, payer_cnt, total_money, gain_money, web_total_money, web_payer_cnt, web_gain_money
            from (
                    select re.markingidx, title, start_date, status, is_test, user_cnt, payer_cnt, total_money, web_total_money, web_payer_cnt
                    from (
                            select markingidx, is_test, count(useridx) as user_cnt, SUM(total_money) as total_money, SUM(case when total_money > 0 then 1 else 0 end) as payer_cnt
                            , SUM(web_total_money) as web_total_money, SUM(case when web_total_money > 0 then 1 else 0 end) as web_payer_cnt
                            from (
                                    select markingidx, t1.useridx, is_test, nvl(SUM(money), 0) as total_money , nvl(SUM(case when os_type = 0 then money end), 0) as web_total_money
                                     from (
                                             select markingidx, useridx, is_test, min(logindate) as min_logindate
                                             from
                                            (
                                              	select markingidx, useridx, is_test, logindate
                                              	from t5_user_marking_log
                                              	where markingidx > 3 and markingidx NOT IN (11, 28) and useridx > 20000
                                              	union all
                                              	select markingidx, useridx, is_test, logindate
                                              	from t5_user_marking_log
                                              	where markingidx = 11 and d28_play_days >= 23 and useridx > 20000
    										  	union all
											  	select markingidx, useridx, is_test, actiondate
												from t5_user_marking_log
												where markingidx = 28 and logindate >= '2019-11-20 00:00:00' and useridx > 20000
                                             ) aa 
                                             where useridx not in (select useridx from t5_ab_test_except_man) group by markingidx, useridx, is_test
                                     ) t1 left join t5_product_order_all t2 on t1.useridx = t2.useridx and t1.min_logindate <= writedate and t2.status = 1
                                     group by markingidx, t1.useridx, is_test
                                      union all
                                      select markingidx, t3.useridx, is_test, nvl(SUM(revenue), 0) as total_money , nvl(SUM(case when platform = 0 then revenue end), 0) as web_total_money
                                        from (
                                                select markingidx, useridx, is_test, min(logindate) as min_logindate
                                                from
                                                    (
                                                        select markingidx, useridx, is_test, logindate
                                                        from t5_user_marking_log
                                                        where markingidx > 3 and markingidx NOT IN (11, 28) and useridx > 20000
                                                        union all
                                                        select markingidx, useridx, is_test, logindate
                                                        from t5_user_marking_log
                                                        where markingidx = 11 and d28_play_days >= 23 and useridx > 20000
                                                        union all
                                                        select markingidx, useridx, is_test, actiondate
                                                        from t5_user_marking_log
                                                        where markingidx = 28 and logindate >= '2019-11-20 00:00:00' and useridx > 20000
                                                    ) aa 
                                                    where useridx not in (select useridx from t5_ab_test_except_man) group by markingidx, useridx, is_test
                                      ) t3 left join t5_iron_ad_revenue_measurement_impression_level t4 on  t3.useridx = t4.user_id and t3.min_logindate <= event_timestamp  
                                      group by markingidx, t3.useridx, is_test
                           ) total
                            group by markingidx, is_test
                          ) re join t5_marking_info info on re.markingidx = info.markingidx
                          where status = 1
                  ) a1 join (
                              select markingidx, SUM(user_cnt) as total_user, ((SUM(case when is_test = 1 then arpu end) - SUM(case when is_test = 0 then arpu end)) *  SUM(user_cnt)) as gain_money
                              , ((SUM(case when is_test = 1 then web_arpu end) - SUM(case when is_test = 0 then web_arpu end)) *  SUM(user_cnt)) as web_gain_money
                               from (
                                       select markingidx, is_test, count(useridx) as user_cnt, (SUM(total_money)::float/count(useridx)::float) as arpu, (SUM(web_total_money)::float/count(useridx)::float) as web_arpu
                                        from (
                                                  select markingidx, t1.useridx, is_test, nvl(SUM(money), 0) as total_money, nvl(SUM(case when os_type = 0 then money end), 0) as web_total_money
                                                  from (
                                                          select markingidx, useridx, is_test, min(logindate) as min_logindate
                                                          from
                                                          (
                                                             	select markingidx, useridx, is_test, logindate
				                                              	from t5_user_marking_log
				                                              	where markingidx > 3 and markingidx NOT IN (11, 28) and useridx > 20000
				                                              	union all
				                                              	select markingidx, useridx, is_test, logindate
				                                              	from t5_user_marking_log
				                                              	where markingidx = 11 and d28_play_days >= 23 and useridx > 20000
				    										  	union all
															  	select markingidx, useridx, is_test, actiondate
																from t5_user_marking_log
																where markingidx = 28 and logindate >= '2019-11-20 00:00:00' and useridx > 20000
                                                          ) aa 
                                                          WHERE useridx not in (select useridx from t5_ab_test_except_man) group by markingidx, useridx, is_test
                                                  ) t1 left join t5_product_order_all t2 on t1.useridx = t2.useridx and t1.min_logindate <= writedate and t2.status = 1
                                                  group by markingidx, t1.useridx, is_test
                                                  union all
                                                  select markingidx, t3.useridx, is_test, nvl(SUM(revenue), 0) as total_money, nvl(SUM(case when platform = 0 then revenue end), 0) as web_total_money
                                                  from (
                                                          select markingidx, useridx, is_test, min(logindate) as min_logindate
                                                          from
                                                          (
                                                              select markingidx, useridx, is_test, logindate
                                                              from t5_user_marking_log
                                                              where markingidx > 3 and markingidx NOT IN (11, 28) and useridx > 20000
                                                              union all
                                                              select markingidx, useridx, is_test, logindate
                                                              from t5_user_marking_log
                                                              where markingidx = 11 and d28_play_days >= 23 and useridx > 20000
                                                              union all
                                                              select markingidx, useridx, is_test, actiondate
                                                              from t5_user_marking_log
                                                              where markingidx = 28 and logindate >= '2019-11-20 00:00:00' and useridx > 20000
                                                          ) aa 
                                                          WHERE useridx not in (select useridx from t5_ab_test_except_man) group by markingidx, useridx, is_test
                                                    ) t3 left join t5_iron_ad_revenue_measurement_impression_level t4 on  t3.useridx = t4.user_id and t3.min_logindate <= event_timestamp  
                                                    group by markingidx, t3.useridx, is_test
                                        ) total
                                        group by markingidx, is_test
                                ) re
                                group by markingidx
                          ) a2 on a1.markingidx = a2.markingidx
                          order by 1 asc, is_test asc";
    $test_list = $db_redshift->gettotallist($sql);
    
    $db_redshift->end();
?>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; AB Test 현황</div>
	</div>
	<span>* 매일 오전 11시에 업데이트</span>
			
	<table class="tbl_list_basic1" style="margin-top:5px">
		<colgroup>
			<col width="">   
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
		</colgroup>
		<thead>
			<tr>
				<th class="tdc">idx</th>
				<th class="tdc">테스트명</th>
				<th class="tdc">시작시간</th>
				<th class="tdc">상태</th>
				<th class="tdc">구분</th>
				<th class="tdc">대상자수</th>
				<th class="tdc">결제자수</th>
				<th class="tdc">결제금액</th>
				<th class="tdc">PUR</th>
				<th class="tdc">ARPU</th>
				<th class="tdc">ARPPU</th>
				<th class="tdc">이득금액</th>
				<th class="tdc"></th>
			</tr>
		</thead>
		<tbody>
<?
        $row_check = -1;
        $style_str = "";

        for($i=0; $i<sizeof($test_list); $i++)
        {
            $markingidx = $test_list[$i]["markingidx"];
            
            $title = $test_list[$i]["title"];
            $start_date = $test_list[$i]["start_date"];
            $status = $test_list[$i]["status"];
            $status_str = ($status == 0) ? "종료" : "진행 중";
            $is_test = $test_list[$i]["is_test"];
            $is_test_str = ($is_test == 1) ? "실험군" : "대조군";
            
            $user_cnt = $test_list[$i]["user_cnt"];
            
            if($markingidx == 28)
            	$start_date = "2019-11-20 00:00:00";
            
            if($markingidx == 16)
            {
                $payer_cnt = $test_list[$i]["web_payer_cnt"];
                $total_money = $test_list[$i]["web_total_money"];
                $pur = round($payer_cnt / $user_cnt * 100, 2);
                $arpu = round($total_money / $user_cnt, 2);
                $arppu = round($total_money / $payer_cnt, 2);
                
                $gain_money = $test_list[$i]["web_gain_money"];
            }
            else
            {
                $payer_cnt = $test_list[$i]["payer_cnt"];
                $total_money = $test_list[$i]["total_money"];
                $pur = round($payer_cnt / $user_cnt * 100, 2);
                $arpu = round($total_money / $user_cnt, 2);
                $arppu = round($total_money / $payer_cnt, 2);
                
                $gain_money = $test_list[$i]["gain_money"];
            }
            
            if($markingidx == 11)
            	$title = "$1구매 럭키휠(최근 28일 중 23일 이상 플레이 사용자)_ver1";
            else if($markingidx == 12)
            	$title = "$1구매 럭키휠(최근 28일 중 23일 이상 플레이 사용자)_ver2";
            
            if($row_check == $markingidx)
            {
                $style_str = "";
            }
            else
            {
                $style_str = "rowspan=\"2\"";
            }
?>
			<tr>
<?
            if($row_check != $markingidx)
            {
?>
				<td class="tdc" <?= $style_str?>><?= $markingidx ?></td>
				<td class="tdc point" <?= $style_str?>><?= $title ?></td>
				<td class="tdc" <?= $style_str?>><?= $start_date ?></td>
				<td class="tdc" <?= $style_str?>><?= $status_str ?></td>
<?
            }
?>
				<td class="tdc"><?= $is_test_str?></td>
				<td class="tdc"><?= number_format($user_cnt)?></td>
				<td class="tdc"><?= number_format($payer_cnt)?></td>
				<td class="tdc">$ <?= number_format($total_money)?></td>
				<td class="tdc"><?= number_format($pur, 2)?>%</td>
				<td class="tdc">$ <?= number_format($arpu, 2)?></td>
				<td class="tdc">$ <?= number_format($arppu, 1)?></td>
<?
            if($row_check != $markingidx)
            {
?>
				<td class="tdc" <?= $style_str?>>$ <?= number_format($gain_money)?></td>
				<td class="tdc" <?= $style_str?>><input type="button" class="btn_03" value="세부지표" style="cursor:pointer" onclick="event.cancelBubble=true;view_ab_test_detail(<?= $markingidx ?>)" /></td>
<?
            }
?>
			</tr>
<?
            $row_check = $markingidx;
        }
?>
		</tbody>
	</table>
</div>
<!--  //CONTENTS WRAP -->
<?	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
