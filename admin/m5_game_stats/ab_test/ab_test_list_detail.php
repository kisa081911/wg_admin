<?
    $top_menu = "game_stats";
    $sub_menu = "ab_test";
    
    include_once("../../common/dbconnect/db_util_redshift.inc.php");
    include_once($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $markingidx = $_GET["markingidx"];
    
    if ($markingidx == "")
    {
        just_go("../../m5_game_stats/ab_test/ab_test_list.php");
    }
    
    $db_main2 = new CDatabase_Main2();
    $db_redshift = new CDatabase_Redshift();
    
    $sql = "SELECT title, start_date FROM tbl_marking_info WHERE markingidx = $markingidx ";
    $marking_info = $db_main2->getarray($sql);
    
    $title = $marking_info["title"];
    $start_date = $marking_info["start_date"];
    
    if($markingidx == 28)
    	$start_date = "2019-11-20 00:00:00";
    
    if($markingidx == 11)
    	$title = "$1구매 럭키휠(최근 28일 중 23일 이상 플레이 사용자)_ver1";
    else if($markingidx == 12)
    	$title = "$1구매 럭키휠(최근 28일 중 23일 이상 플레이 사용자)_ver2";
    
    
    $search_start_date = ($_GET["start_date"] == "") ? $start_date : $_GET["start_date"];
    $search_end_date = ($_GET["end_date"] == "") ? date("Y-m-d") : $_GET["end_date"];
    $search_after_date = ($_GET["after_date"] == "") ? 28 : $_GET["after_date"];
    
    $search_isexclude = ($_GET["isexclude"] == "") ? 0 : $_GET["isexclude"];
    $search_exclude_rate = ($_GET["exclude_rate"] == "") ? 0.1 : $_GET["exclude_rate"];
    
    $exclude_sql = "";
    $user_condition = " 1=1  and useridx not in (select useridx from t5_ab_test_except_man) ";
    
    $add_sql = "";
    $add_sql2 = "";
    $add_sql3 = "";
    
    if($markingidx == 11)
    	$add_sql = " and d28_play_days >= 23 ";
    
    if($markingidx == 16)
    	$add_sql2 = " and t2.os_type = 0 ";
    
    if($markingidx == 16)
    	$add_sql3 = " and t2.platform = 0 ";
    
    if($search_isexclude == 1 && $search_exclude_rate > 0)
    {
        $sql = "select round(count(distinct t1.useridx)::float*$search_exclude_rate/100) as payer_cnt
                from (
                   select useridx, is_test, min(logindate) as min_logindate
                   from t5_user_marking_log
                   where useridx > 20000 and $user_condition and markingidx = $markingidx $add_sql and '$search_start_date' <= logindate AND logindate <= '$search_end_date 23:59:59'
                   group by useridx, is_test
                ) t1 join t5_product_order_all t2 on t1.useridx = t2.useridx $add_sql2  and t1.min_logindate <= writedate $add_sql2 and t2.status = 1 and trunc(datediff(hour, min_logindate, writedate)/24, 0) <= $search_after_date";
        $exclude_count = $db_redshift->getvalue($sql);
    
        $exclude_str = "";
        
        if($exclude_count > 0)
        {
            $sql = "select t1.useridx
                    from (
                       select useridx, is_test, min(logindate) as min_logindate
                       from t5_user_marking_log
                       where useridx > 20000 and $user_condition and markingidx = $markingidx $add_sql and '$search_start_date' <= logindate AND logindate <= '$search_end_date 23:59:59'
                       group by useridx, is_test
                    ) t1 join t5_product_order_all t2 on t1.useridx = t2.useridx $add_sql2 and t1.min_logindate <= writedate and t2.status = 1 and trunc(datediff(hour, min_logindate, writedate)/24, 0) <= $search_after_date
                    group by t1.useridx, is_test
                    order by nvl(SUM(money), 0) desc
                    limit $exclude_count";
            $exclude_list = $db_redshift->gettotallist($sql);
            
            for($i=0; $i<sizeof($exclude_list); $i++)
            {
                if($exclude_str == "")
                {
                    $exclude_str = $exclude_list[$i]["useridx"];
                }
                else
                {
                    $exclude_str .= ",".$exclude_list[$i]["useridx"];
                }
            }
        }
        
        if($exclude_str != "")
        {
            $exclude_sql = " AND useridx NOT IN ($exclude_str) ";
        }
    }
       
    $sql = "select re3.is_test, user_cnt, total_money, payer_cnt, play_user_cnt, money_in, money_out, playcount, d48_play_user, freecoin_user_cnt, total_freecoin
			from
			(
				select re1.is_test, user_cnt, total_money, payer_cnt, play_user_cnt, money_in, money_out, playcount, d48_play_user
	            from (
	              select is_test, count(useridx) as user_cnt, SUM(total_money) as total_money, SUM(case when total_money > 0 then 1 else 0 end) as payer_cnt
	              from (
	                 select t1.useridx, is_test, nvl(SUM(money), 0) as total_money
	                 from (
	                     select useridx, is_test, min(logindate) as min_logindate
	                     from t5_user_marking_log
	                     where useridx > 20000 and $user_condition and markingidx = $markingidx $add_sql and '$search_start_date' <= logindate AND logindate <= '$search_end_date 23:59:59' $exclude_sql
	                     group by useridx, is_test
	                 ) t1 left join t5_product_order_all t2 on t1.useridx = t2.useridx $add_sql2  and t1.min_logindate <= writedate and t2.status = 1 and trunc(datediff(hour, min_logindate, writedate)/24, 0) <= $search_after_date
	                 group by t1.useridx, is_test
	              ) total
	              group by is_test
	            ) re1 left join (
	              select is_test, count(distinct useridx) as play_user_cnt, SUM(money_in) as money_in, SUM(money_out) as money_out, SUM(playcount) as playcount, count(distinct(case when dayafterlogin > 1 then useridx end)) as d48_play_user
	              from (
	                 select t1.useridx, is_test, SUM(money_in) as money_in, SUM(money_out) as money_out, SUM(playcount) as playcount, max(trunc(datediff(hour, min_logindate, writedate)/24, 0)) as dayafterlogin
	                 from (
	                     select useridx, is_test, min(logindate) as min_logindate
	                     from t5_user_marking_log
	                     where useridx > 20000 and $user_condition and markingidx = $markingidx $add_sql and '$search_start_date' <= logindate AND logindate <= '$search_end_date 23:59:59' $exclude_sql
	                     group by useridx, is_test
	                 ) t1 join t5_user_gamelog t2 on t1.useridx = t2.useridx and t1.min_logindate <= writedate and trunc(datediff(hour, min_logindate, writedate)/24, 0) <= $search_after_date
	                 group by t1.useridx, is_test
	                 union all
	                 select t1.useridx, is_test, SUM(money_in) as money_in, SUM(money_out) as money_out, SUM(playcount) as playcount, max(trunc(datediff(hour, min_logindate, writedate)/24, 0)) as dayafterlogin
	                 from (
	                     select useridx, is_test, min(logindate) as min_logindate
	                     from t5_user_marking_log
	                     where useridx > 20000 and $user_condition and markingidx = $markingidx $add_sql and '$search_start_date' <= logindate AND logindate <= '$search_end_date 23:59:59' $exclude_sql
	                     group by useridx, is_test
	                 ) t1 join t5_user_gamelog_ios t2 on t1.useridx = t2.useridx and t1.min_logindate <= writedate and trunc(datediff(hour, min_logindate, writedate)/24, 0) <= $search_after_date
	                 group by t1.useridx, is_test
	                 union all
	                 select t1.useridx, is_test, SUM(money_in) as money_in, SUM(money_out) as money_out, SUM(playcount) as playcount, max(trunc(datediff(hour, min_logindate, writedate)/24, 0)) as dayafterlogin
	                 from (
	                     select useridx, is_test, min(logindate) as min_logindate
	                     from t5_user_marking_log
	                     where useridx > 20000 and $user_condition and markingidx = $markingidx $add_sql and '$search_start_date' <= logindate AND logindate <= '$search_end_date 23:59:59' $exclude_sql
	                     group by useridx, is_test
	                 ) t1 join t5_user_gamelog_android t2 on t1.useridx = t2.useridx and t1.min_logindate <= writedate and trunc(datediff(hour, min_logindate, writedate)/24, 0) <= $search_after_date
	                 group by t1.useridx, is_test
	                 union all
	                 select t1.useridx, is_test, SUM(money_in) as money_in, SUM(money_out) as money_out, SUM(playcount) as playcount, max(trunc(datediff(hour, min_logindate, writedate)/24, 0)) as dayafterlogin
	                 from (
	                     select useridx, is_test, min(logindate) as min_logindate
	                     from t5_user_marking_log
	                     where useridx > 20000 and $user_condition and markingidx = $markingidx $add_sql and '$search_start_date' <= logindate AND logindate <= '$search_end_date 23:59:59' $exclude_sql
	                     group by useridx, is_test
	                 ) t1 join t5_user_gamelog_amazon t2 on t1.useridx = t2.useridx and t1.min_logindate <= writedate and trunc(datediff(hour, min_logindate, writedate)/24, 0) <= $search_after_date
	                 group by t1.useridx, is_test
	              ) total
	              group by is_test
	            ) re2 on re1.is_test = re2.is_test
	        ) re3
            left join
            ( 
              select is_test, count(useridx) as freecoin_user_cnt, SUM(total_freecoin) as total_freecoin
              from (
                 select t1.useridx, is_test, nvl(SUM(amount), 0) as total_freecoin
                 from (
                     select useridx, is_test, min(logindate) as min_logindate                     
                     from t5_user_marking_log
                     where useridx > 20000 and $user_condition and markingidx = $markingidx $add_sql and '$search_start_date' <= logindate AND logindate <= '$search_end_date 23:59:59' $exclude_sql 
                     group by useridx, is_test
                 ) t1 left join duc_user_freecoin_log t2 on t1.useridx = t2.useridx and t1.min_logindate <= writedate and trunc(datediff(hour, min_logindate, writedate)/24, 0) <= $search_after_date
                 group by t1.useridx, is_test
              ) total
              group by is_test
             ) re4 on re3.is_test = re4.is_test
            order by 1 asc";
    $test_list = $db_redshift->gettotallist($sql);
    
    $sql = "select ((SUM(case when is_test = 1 then arpu end) - SUM(case when is_test = 0 then arpu end)) *  SUM(user_cnt)) as gain_money
          from (
            select markingidx, is_test, count(useridx) as user_cnt, (SUM(total_money)::float/count(useridx)::float) as arpu
            from (
               select markingidx, t1.useridx, is_test, nvl(SUM(money), 0) as total_money
               from (
                   select markingidx, useridx, is_test, min(logindate) as min_logindate
                   from t5_user_marking_log
                   where useridx > 20000 and $user_condition and markingidx = $markingidx $add_sql and '$search_start_date' <= logindate AND logindate <= '$search_end_date 23:59:59' $exclude_sql
                   group by markingidx, useridx, is_test
               ) t1 left join t5_product_order_all t2 on t1.useridx = t2.useridx $add_sql2 and t1.min_logindate <= writedate and t2.status = 1 and trunc(datediff(hour, min_logindate, writedate)/24, 0) <= $search_after_date
               group by markingidx, t1.useridx, is_test
            ) total
            group by markingidx, is_test
          ) re";
    $gain_money = $db_redshift->getvalue($sql);
    
    $sql = "select dayafterlogin, SUM(case when is_test = 1 then total_money end) as test_money, SUM(case when is_test = 0 then total_money end) as condition_money
            from (
               select is_test, trunc(datediff(hour, min_logindate, writedate)/24, 0) as dayafterlogin, SUM(money) as total_money
               from (
                   select useridx, is_test, min(logindate) as min_logindate
                   from t5_user_marking_log
                   where useridx > 20000 and $user_condition and markingidx = $markingidx $add_sql and '$search_start_date' <= logindate AND logindate <= '$search_end_date 23:59:59' $exclude_sql
                   group by markingidx, useridx, is_test
               ) t1 join t5_product_order_all t2 on t1.useridx = t2.useridx $add_sql2  and t1.min_logindate <= writedate and t2.status = 1 and trunc(datediff(hour, min_logindate, writedate)/24, 0) <= $search_after_date
               group by is_test, trunc(datediff(hour, min_logindate, writedate)/24, 0)
            ) total
            group by dayafterlogin
            order by 1 asc";
    $arpu_list = $db_redshift->gettotallist($sql);
    
    $sql = "select dayafterlogin, SUM(case when is_test = 1 then payer_cnt end) as test_payer_cnt, SUM(case when is_test = 0 then payer_cnt end) as condition_payer_cnt
            from (
              select is_test, dayafterlogin, count(distinct useridx) as payer_cnt
              from (
                select t1.useridx, is_test, min(trunc(datediff(hour, min_logindate, writedate)/24, 0)) as dayafterlogin
                   from (
                       select useridx, is_test, min(logindate) as min_logindate
                       from t5_user_marking_log
                       where useridx > 20000 and $user_condition and markingidx = $markingidx $add_sql and '$search_start_date' <= logindate AND logindate <= '$search_end_date 23:59:59' $exclude_sql
                       group by markingidx, useridx, is_test
                   ) t1 join t5_product_order_all t2 on t1.useridx = t2.useridx $add_sql2  and t1.min_logindate <= writedate and t2.status = 1 and trunc(datediff(hour, min_logindate, writedate)/24, 0) <= $search_after_date
                group by t1.useridx, is_test
              ) total
              group by is_test, dayafterlogin
            ) re
            group by dayafterlogin
            order by 1 asc";
    $pur_list = $db_redshift->gettotallist($sql);
    
    $sql = "select dayafterlogin, SUM(case when is_test = 1 then retention_cnt end) as test_retention_cnt, SUM(case when is_test = 0 then retention_cnt end) as condition_retention_cnt
            from (
              select is_test, dayafterlogin, count(distinct useridx) as retention_cnt
              from (
                select t1.useridx, is_test, trunc(datediff(hour, min_logindate, writedate)/24, 0) as dayafterlogin
                from (
                  select useridx, is_test, min(logindate) as min_logindate
                  from t5_user_marking_log
                  where useridx > 20000 and $user_condition and markingidx = $markingidx $add_sql and '$search_start_date' <= logindate AND logindate <= '$search_end_date 23:59:59' $exclude_sql
                  group by markingidx, useridx, is_test
                ) t1 join t5_user_gamelog t2 on t1.useridx = t2.useridx and t1.min_logindate <= writedate and trunc(datediff(hour, min_logindate, writedate)/24, 0) <= $search_after_date
                union all
                select t1.useridx, is_test, trunc(datediff(hour, min_logindate, writedate)/24, 0) as dayafterlogin
                from (
                  select useridx, is_test, min(logindate) as min_logindate
                  from t5_user_marking_log
                  where useridx > 20000 and $user_condition and markingidx = $markingidx $add_sql and '$search_start_date' <= logindate AND logindate <= '$search_end_date 23:59:59' $exclude_sql
                  group by markingidx, useridx, is_test
                ) t1 join t5_user_gamelog_ios t2 on t1.useridx = t2.useridx and t1.min_logindate <= writedate and trunc(datediff(hour, min_logindate, writedate)/24, 0) <= $search_after_date
                union all
                select t1.useridx, is_test, trunc(datediff(hour, min_logindate, writedate)/24, 0) as dayafterlogin
                from (
                  select useridx, is_test, min(logindate) as min_logindate
                  from t5_user_marking_log
                  where useridx > 20000 and $user_condition and markingidx = $markingidx $add_sql and '$search_start_date' <= logindate AND logindate <= '$search_end_date 23:59:59' $exclude_sql
                  group by markingidx, useridx, is_test
                ) t1 join t5_user_gamelog_android t2 on t1.useridx = t2.useridx and t1.min_logindate <= writedate and trunc(datediff(hour, min_logindate, writedate)/24, 0) <= $search_after_date
                union all
                select t1.useridx, is_test, trunc(datediff(hour, min_logindate, writedate)/24, 0) as dayafterlogin
                from (
                  select useridx, is_test, min(logindate) as min_logindate
                  from t5_user_marking_log
                  where useridx > 20000 and $user_condition and markingidx = $markingidx $add_sql and '$search_start_date' <= logindate AND logindate <= '$search_end_date 23:59:59' $exclude_sql
                  group by markingidx, useridx, is_test
                ) t1 join t5_user_gamelog_amazon t2 on t1.useridx = t2.useridx and t1.min_logindate <= writedate and trunc(datediff(hour, min_logindate, writedate)/24, 0) <= $search_after_date
              ) total
              group by is_test, dayafterlogin
            ) re
            group by dayafterlogin
            order by 1 asc";
    $retention_list = $db_redshift->gettotallist($sql);
    
    $db_main2->end();
    $db_redshift->end();
?>
<!-- CONTENTS WRAP -->
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
$(function() {
    $("#start_date").datepicker({ });
    $("#end_date").datepicker({ });
});

function search_press(e)
{
    if (((e.which) ? e.which : e.keyCode) == 13)
    {
	    search();
    }
}

function search()
{
	var search_form = document.search_form;

    var category = search_form.isexclude.value;
	
	if (!IsNumber(search_form.after_date.value) || search_form.after_date.value == "")
	{
		alert("숫자만 입력 가능합니다.");
		search_form.a_std1.focus();
		return;
	}
	
	search_form.submit();
}

google.load("visualization", "1", {packages:["corechart"]});

function drawChart() 
{
	var data0 = new google.visualization.DataTable();
    data0.addColumn('string', 'D+Day');
    data0.addColumn('number', '대조군');
    data0.addColumn('number', '실험군');
    data0.addRows([
<?
    $a_total_money = 0;
    $b_total_money = 0;

    for ($i=0; $i<sizeof($arpu_list); $i++)
    {
        $a_day_money = $arpu_list[$i]["condition_money"];
        $a_total_money += $a_day_money;
        
        $b_day_money = $arpu_list[$i]["test_money"];
        $b_total_money += $b_day_money;
        
        $a_arpu = round($a_total_money/$test_list[0]["user_cnt"], 2);
        $b_arpu = round($b_total_money/$test_list[1]["user_cnt"], 2);
        
        echo("['D+".$i."',".number_format($a_arpu, 2).",".number_format($b_arpu, 2)."]");
    	
        if ($i != sizeof($arpu_list) - 1)
    		echo(",");
    }
?>
    ]);

    var data1 = new google.visualization.DataTable();
    data1.addColumn('string', 'D+Day');
    data1.addColumn('number', '대조군');
    data1.addColumn('number', '실험군');
    data1.addRows([
<?
    $a_total_payer = 0;
    $b_total_payer = 0;

    for ($i=0; $i<sizeof($pur_list); $i++)
    {
        $a_day_payer = $pur_list[$i]["condition_payer_cnt"];
        $a_total_payer += $a_day_payer;
        
        $b_day_payer = $pur_list[$i]["test_payer_cnt"];
        $b_total_payer += $b_day_payer;
        
        $a_pur = round($a_total_payer/$test_list[0]["user_cnt"]*100, 2);
        $b_pur = round($b_total_payer/$test_list[1]["user_cnt"]*100, 2);
        
        echo("['D+".$i."',".number_format($a_pur, 2).",".number_format($b_pur, 2)."]");
    	
        if ($i != sizeof($pur_list) - 1)
    		echo(",");
    }
?>
    ]);

    var data2 = new google.visualization.DataTable();
    data2.addColumn('string', 'D+Day');
    data2.addColumn('number', '대조군');
    data2.addColumn('number', '실험군');
    data2.addRows([
<?
    $a_total_retention = 0;
    $b_total_retention = 0;

    for ($i=1; $i<sizeof($retention_list); $i++)
    {
        $a_day_retention = $retention_list[$i]["condition_retention_cnt"];
        $a_total_retention = $a_day_retention;
        
        $b_day_retention = $retention_list[$i]["test_retention_cnt"];
        $b_total_retention = $b_day_retention;
        
        $a_retention = round($a_total_retention/$test_list[0]["user_cnt"]*100, 2);
        $b_retention = round($b_total_retention/$test_list[1]["user_cnt"]*100, 2);
        
        echo("['D+".$i."',".number_format($a_retention, 2).",".number_format($b_retention, 2)."]");
    	
        if ($i != sizeof($retention_list) - 1)
    		echo(",");
    }
?>
    ]);

    var data3 = new google.visualization.DataTable();
    data3.addColumn('string', 'D+Day');
    data3.addColumn('number', '대조군');
    data3.addColumn('number', '실험군');
    data3.addRows([
<?
    $a_total_money = 0;
    $b_total_money = 0;
    
    $a_total_payer = 0;
    $b_total_payer = 0;

    for ($i=0; $i<sizeof($arpu_list); $i++)
    {
        $a_day_money = $arpu_list[$i]["condition_money"];
        $a_total_money += $a_day_money;
        
        $a_day_payer = $pur_list[$i]["condition_payer_cnt"];
        $a_total_payer += $a_day_payer;
        
        $b_day_money = $arpu_list[$i]["test_money"];
        $b_total_money += $b_day_money;
        
        $b_day_payer = $pur_list[$i]["test_payer_cnt"];
        $b_total_payer += $b_day_payer;
        
        $a_arppu = round($a_total_money/$a_total_payer, 2);
        $b_arppu = round($b_total_money/$b_total_payer, 2);
        
        echo("['D+".$i."',".number_format($a_arppu, 2).",".number_format($b_arppu, 2)."]");
    	
        if ($i != sizeof($arpu_list) - 1)
    		echo(",");
    }
?>
    ]);

    var options1 = {
	    width:530,                         
	    height:380,
	    axisTitlesPosition:'in',
	    curveType:'none',
	    focusTarget:'category',
	    interpolateNulls:'true',
	    legend:'top',
	    fontSize : 12,
	    chartArea:{left:60,top:40,width:500,height:300}
	};

	var chart = new google.visualization.LineChart(document.getElementById('chart_div0'));
	chart.draw(data0, options1);

	chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
    chart.draw(data1, options1);
    
    chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
    chart.draw(data2, options1);

    chart = new google.visualization.LineChart(document.getElementById('chart_div3'));
    chart.draw(data3, options1);
}

google.setOnLoadCallback(drawChart);
</script>
<div class="contents_wrap">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; AB Test 세부현황 - <?= $title?></div>
	</div>
	
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="ab_test_list_detail.php">
		<div class="detail_search_wrap">
			<span class="search_lbl ml20">기간</span>
			<input type="text" class="search_text" id="start_date" name="start_date" style="width:65px" value="<?= $search_start_date ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_date" name="end_date" style="width:65px" value="<?= $search_end_date ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />

			<span class="ml20">D+</span>
			<input type="text" class="search_text" id="after_date" name="after_date" style="width:20px" value="<?= encode_html_attribute($search_after_date) ?>" onkeypress="search_press(event); return checkonlynum();" />
			<span class="search_lbl">까지</span>
			
			<span class="ml20"><input type="checkbox" value="1" id="isexclude" name="isexclude" <?= ($search_isexclude == "1") ? "checked" : "" ?> align="absmiddle" />결제 상위</span>
			<input type="text" class="search_text" id="exclude_rate" name="exclude_rate" style="width:25px" value="<?= encode_html_attribute($search_exclude_rate) ?>" onkeypress="search_press(event); return checkonlynum();" />
			<span class="search_lbl">% 제외</span>

			<input type="hidden" name="markingidx" id="markingidx" value="<?= $markingidx ?>" />

			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>                
	</form>
			
	<table class="tbl_list_basic1" style="margin-top:5px">
		<colgroup> 
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
			<col width="">
		</colgroup>
		<thead>
			<tr>
				<th class="tdc">구분</th>
				<th class="tdc">대상자수</th>
				<th class="tdc">결제자수</th>
				<th class="tdc">결제금액</th>
				<th class="tdc">PUR</th>
				<th class="tdc">ARPU</th>
				<th class="tdc">ARPPU</th>
				<th class="tdc">플레이유저수</th>
				<th class="tdc">평균머니인</th>
				<th class="tdc">평균플레이수</th>
				<th class="tdc">승률</th>
				<th class="tdc">평균프리코인</th>
				<th class="tdc">재방문율(D+0,1 제외)</th>
				<th class="tdc">이득금액</th>
			</tr>
		</thead>
		<tbody>
<?
        $row_check = -1;
        $style_str = "";

        for($i=0; $i<sizeof($test_list); $i++)
        {
            $is_test = $test_list[$i]["is_test"];
            $is_test_str = ($is_test == 1) ? "실험군" : "대조군";
            
            $user_cnt = $test_list[$i]["user_cnt"];
            $payer_cnt = $test_list[$i]["payer_cnt"];
            $total_money = $test_list[$i]["total_money"];
            $pur = round($payer_cnt / $user_cnt * 100, 2);
            $arpu = round($total_money / $user_cnt, 2);
            $arppu = round($total_money / $payer_cnt, 2);
            
            $play_user_cnt = $test_list[$i]["play_user_cnt"];
            $money_in = $test_list[$i]["money_in"];
            $money_out = $test_list[$i]["money_out"];
            $playcount = $test_list[$i]["playcount"];
            $d48_play_user = $test_list[$i]["d48_play_user"];
            $freecoin_user_cnt = $test_list[$i]["freecoin_user_cnt"];
            $total_freecoin = $test_list[$i]["total_freecoin"];
                        
            $avg_moneyin = round($money_in/$play_user_cnt);
            $avg_playcount = round($playcount/$play_user_cnt);
            $avg_freecoin = round($total_freecoin/$freecoin_user_cnt);
            $winrate = round($money_out / $money_in * 100, 2);
            
            $d48_retention = round($d48_play_user / $user_cnt * 100, 2);
            
            if($row_check == $markingidx)
            {
                $style_str = "";
            }
            else
            {
                $style_str = "rowspan=\"2\"";
            }
?>
			<tr>
				<td class="tdc"><?= $is_test_str?></td>
				<td class="tdc"><?= number_format($user_cnt)?></td>
				<td class="tdc"><?= number_format($payer_cnt)?></td>
				<td class="tdc">$ <?= number_format($total_money)?></td>
				<td class="tdc"><?= number_format($pur, 2)?>%</td>
				<td class="tdc">$ <?= number_format($arpu, 2)?></td>
				<td class="tdc">$ <?= number_format($arppu, 1)?></td>
				<td class="tdc"><?= number_format($play_user_cnt)?></td>
				<td class="tdc"><?= number_format($avg_moneyin)?></td>
				<td class="tdc"><?= number_format($avg_playcount)?></td>
				<td class="tdc"><?= number_format($winrate, 2)?>%</td>
				<td class="tdc"><?= number_format($avg_freecoin)?></td>
				<td class="tdc"><?= number_format($d48_retention, 2)?>%</td>
<?
            if($row_check != $markingidx)
            {
?>
				<td class="tdc" <?= $style_str?>>$ <?= number_format($gain_money)?></td>
<?
            }
?>
			</tr>
<?
            $row_check = $markingidx;
        }
?>
		</tbody>
	</table>
	
	<br/>
	<div style="width:530px;float:left;margin:20px 0 0 0;">
        <div class="h2_title">[ARPU]</div>
        <div id="chart_div0" style="height:300px;width:530px;"></div>
    </div>
    <div style="width:530px;float:left;margin:20px 0 0 20px;">
        <div class="h2_title">[PUR]</div>
        <div id="chart_div1" style="height:300px;width:530px;"></div>
    </div>
    <div class="clear" style="height:30px;"> </div>
    <div style="width:530px;float:left;margin:50px 0 0 0;">
        <div class="h2_title">[Retention]</div>
        <div id="chart_div2" style="height:300px;width:530px;"></div>
    </div>
    <div style="width:530px;float:left;margin:50px 0 0 20px;">
        <div class="h2_title">[ARPPU]</div>
        <div id="chart_div3" style="height:300px;width:530px;"></div>
</div>
<!--  //CONTENTS WRAP -->
<?	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
