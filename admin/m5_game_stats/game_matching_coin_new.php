<?
    $top_menu = "game_stats";
    $sub_menu = "game_matching_coin";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $search_startdate = $_GET["search_startdate"];
    $search_enddate = $_GET["search_enddate"];
    
    
    if($search_startdate == "")
    	$search_startdate = date("Y-m-d", strtotime("-30 day"));
    
    if($search_enddate == "")
    	$search_enddate = $today = date("Y-m-d");
    
    $db_analysis = new CDatabase_Analysis();
    $db_main = new CDatabase_Main();
    
    $sql = "SELECT writedate, free_coin, order_coin, game_profit, join_coin,
    				ROUND(unknown_coin/1000000) AS unknown_coin, ROUND(ios_unknown_coin/1000000) AS ios_unknown_coin, ROUND(and_unknown_coin/1000000) AS and_unknown_coin, ROUND(ama_unknown_coin/1000000) AS ama_unknown_coin,
    				ROUND(unknown_coin_log/1000000) AS unknown_coin_log, ROUND(ios_unknown_coin_log/1000000) AS ios_unknown_coin_log, ROUND(and_unknown_coin_log/1000000) AS and_unknown_coin_log, ROUND(ama_unknown_coin_log/1000000) AS ama_unknown_coin_log,
    				sum_coin, change_coin
    		FROM user_unknown_coin_new
    		WHERE writedate BETWEEN '$search_startdate' AND '$search_enddate'
    		ORDER BY writedate DESC";
    $coin_list = $db_analysis->gettotallist($sql);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script>
	$(function() {
		$("#search_startdate").datepicker({});
		$("#search_enddate").datepicker({});
	});

	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.search_startdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.search_startdate.focus();
	        return;
	    } 

	    if (search_form.search_enddate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.search_enddate.focus();
	        return;
	    } 
	
	    search_form.submit();
	}
</script>
    <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 코인 정합성 목록</div>
                
                <form style="float: right" name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
                	날짜  :
					<input type="text" class="search_text" id="search_startdate" name="search_startdate" value="<?= $search_startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
					<input type="text" class="search_text" id="search_enddate" name="search_enddate" value="<?= $search_enddate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
					<input type="button" class="btn_search" value="검색" onclick="search()" />
                </form>
            </div>
            <!-- //title_warp -->
            
            <div class="search_result">
				<span><?= $search_startdate ?></span> ~ <span><?= $search_enddate ?></span> 통계입니다
			</div>
            
            <table class="tbl_list_basic1" style="width:1400px">
            <colgroup>
                <col width="110">
                
                <col width="110">
                <col width="110">
                <col width="120">
                <col width="110">
                
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                
                <col width="120">
                <col width="120">
                <col width="120">          
            </colgroup>
            <thead>
            <tr>
                <th rowspan="2">날짜</th>
                <th class="tdc" rowspan="2">무료코인</th>
                <th class="tdc" rowspan="2">구매코인</th>
                <th class="tdc" rowspan="2">게임수익</th>
                <th class="tdc" rowspan="2">가입코인</th>
                <th class="tdc" colspan="4" style="border-left: 1px solid;">Unknown Coin(백만)</th>
                <th class="tdc" colspan="4">Unknown Coin Log(백만)</th>
                <th class="tdc" rowspan="2" style="border-left: 1px solid;">집계된 코인변화(A)</th>
                <th class="tdc" rowspan="2">사용자 코인변화(B)</th>
                <!-- th class="tdc" rowspan="2">만료된 Treat 금액(C)</th-->
                <th class="tdc" rowspan="2">오차(A+C)-B)</th>
            </tr>
            <tr>
                <th class="tdr">Web</th>
                <th class="tdr">iOS</th>
                <th class="tdr">And</th>
                <th class="tdr">Ama</th>
                <th class="tdr">Web</th>
                <th class="tdr">iOS</th>
                <th class="tdr">And</th>
                <th class="tdr">Ama</th>
            </tr>
		</thead>
		<tbody>
<?
    
    for ($i=0; $i<sizeof($coin_list); $i++)
    {
        $writedate = $coin_list[$i]["writedate"];
        $free_coin = $coin_list[$i]["free_coin"];
        $order_coin = $coin_list[$i]["order_coin"];
        $game_profit = $coin_list[$i]["game_profit"];
        $join_coin = $coin_list[$i]["join_coin"];
        $unknown_coin = $coin_list[$i]["unknown_coin"];
        $ios_unknown_coin = $coin_list[$i]["ios_unknown_coin"];
        $and_unknown_coin = $coin_list[$i]["and_unknown_coin"];
        $ama_unknown_coin = $coin_list[$i]["ama_unknown_coin"];
        
        $unknown_coin_log = $coin_list[$i]["unknown_coin_log"];
        $ios_unknown_coin_log = $coin_list[$i]["ios_unknown_coin_log"];
        $and_unknown_coin_log = $coin_list[$i]["and_unknown_coin_log"];
        $ama_unknown_coin_log = $coin_list[$i]["ama_unknown_coin_log"];
        
        $sum_coin = $coin_list[$i]["sum_coin"];
        $change_coin = $coin_list[$i]["change_coin"];
        
        $treatamount = $db_main->getvalue("SELECT treatamount FROM `tbl_delete_treatamount_daily` WHERE today ='$writedate'");
        $treatamount = ($treatamount=="")? 0:$treatamount;
        //$sum_coin = $sum_coin+$treatamount;
        
?>

                <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
                    <td class="tdc point"><?= $writedate ?></td>
                    <td class="tdr point"><?= number_format($free_coin) ?></td>
                    <td class="tdr point"><?= number_format($order_coin) ?></td>
                    <td class="tdr point"><?= number_format($game_profit) ?></td>
                    <td class="tdr point"><?= number_format($join_coin) ?></td>
                    <td class="tdr point" style="border-left: 1px solid;"><?= number_format($unknown_coin) ?></td>
                    <td class="tdr point"><?= number_format($ios_unknown_coin) ?></td>
                    <td class="tdr point"><?= number_format($and_unknown_coin) ?></td>
                    <td class="tdr point"><?= number_format($ama_unknown_coin) ?></td>
                    <td class="tdr point"><?= number_format($unknown_coin_log) ?></td>
                    <td class="tdr point"><?= number_format($ios_unknown_coin_log) ?></td>
                    <td class="tdr point"><?= number_format($and_unknown_coin_log) ?></td>
                    <td class="tdr point"><?= number_format($ama_unknown_coin_log) ?></td>
                    <td class="tdr point" style="border-left: 1px solid;"><?= number_format($sum_coin) ?></td>
                    <td class="tdr point"><?= number_format($change_coin) ?></td>
                    <!-- td class="tdr point"><?= number_format($treatamount) ?></td-->
                    <td class="tdr point"><?= number_format($sum_coin - $change_coin) ?></td>
                </tr>
<?
    }
?>
		</tbody>
	</table>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?

    $db_analysis->end();
    $db_main->end();
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>