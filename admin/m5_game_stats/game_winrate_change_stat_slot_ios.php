<?
    $top_menu = "game_stats";
    $sub_menu = "game_winrate_change_stat_slot_ios";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $pagename = "game_winrate_change_stat_slot_ios.php";
    
    $dbaccesstype = $_SESSION["dbaccesstype"];
    
    $min_winrate = ($_GET["min_winrate"] == "") ? 0.96 : $_GET["min_winrate"];
    $max_winrate = ($_GET["max_winrate"] == "") ? 0.975 : $_GET["max_winrate"];

    $db_main2 = new CDatabase_Main2();
    
//     $sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t1.slottype) slotname, slottype, ROUND(SUM(rate_playcount)/ SUM(playcount)/100, 4) AS winrate, ".
// 			"SUM(money_out)-SUM(money_in) AS win_money, SUM(money_in) AS money_in, SUM(money_out) AS money_out, SUM(playcount) AS playcount, MIN(writedate) AS writedate ".
// 			"FROM ".
// 			"( ".
// 			"	SELECT slottype, ROUND((ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) * SUM(playcount)),0) AS rate_playcount, ".
// 			"	SUM(moneyin) AS money_in, SUM(moneyout) AS money_out, SUM(playcount) AS playcount, MIN(writedate) AS writedate ".
// 			"	FROM tbl_game_cash_stats_ios_daily2 a ". 
// 			"	WHERE MODE = 0 AND writedate >= (SELECT writedate FROM tbl_slot_profit_setting_ios WHERE slottype = a.slottype) ". 
// 			"	GROUP BY slottype, betlevel ".
// 			") t1 GROUP BY slottype ".
// 			"HAVING winrate < $min_winrate OR winrate > $max_winrate";				
//     $winrate_data = $db_main2->gettotallist($sql);
    
    $sql = "SELECT (SELECT slotname FROM tbl_slot_list WHERE slottype = t3.slottype) slotname, slottype, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin), 4) AS winrate, ROUND(SUM(money_out)/SUM(money_in), 4) AS org_winrate, ".
    		"SUM(money_out)-SUM(money_in) AS win_money, SUM(money_in) AS money_in, SUM(money_out) AS money_out, SUM(playcount) AS playcount, MIN(writedate) AS writedate	".
    		"FROM	".
    		"(	".
    		"	SELECT MIN(writedate) AS writedate, t1.slottype, t2.betlevel, SUM(moneyin) AS money_in, SUM(moneyout) AS money_out, SUM(playcount) AS playcount, t2.bet_amount,	".
    		"	ROUND(SUM(moneyin/t2.bet_amount)) AS tt_moneyin, ROUND(SUM(moneyout/t2.bet_amount)) AS tt_moneyout	".
    		"	FROM tbl_game_cash_stats_ios_daily2 t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
    		"	WHERE mode IN (0,31)  AND 0 <= t1.betlevel AND t1.betlevel <= 15 AND writedate  >= (SELECT writedate FROM tbl_slot_profit_setting_ios WHERE slottype = t1.slottype)	".
    		"	GROUP BY slottype, betlevel	".
    		") t3  GROUP BY slottype	".
    		"HAVING winrate < $min_winrate OR winrate > $max_winrate ".
			"ORDER BY writedate ASC";
   	$winrate_data = $db_main2->gettotallist($sql);
    
?>
<script type="text/javascript">
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }

    function pop_change_ratio()
	{		
		try
		{
			var request = $.ajax({
	    		url: "pop_change_ratio.php",
				type: "POST",
				data: {category: "ios"},
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}

    function pop_change_ratio3()
	{		
		try
		{
			var request = $.ajax({
	    		url: "pop_change_ratio3.php",
				type: "POST",
				data: {category: "ios"},
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}

    function pop_change_ratio_all()
	{		
		try
		{
			var request = $.ajax({
	    		url: "pop_change_ratio.php",
				type: "POST",
				data: {category: "all"},
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}
    
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
                <!-- title_warp -->
                <div class="title_wrap">
                    <div class="title"><?= $top_menu_txt ?> &gt; 슬롯 승률 조정 후 승률 추이
<?
	if($dbaccesstype == "1" || $dbaccesstype == "5")
	{
?>
                    	<input type="button" class="btn_03" style="margin-bottom:5px;" value="승률 조정하기" onclick="pop_change_ratio()">
                    	<input type="button" class="btn_03" style="margin-bottom:5px;" value="승률 조정하기(ratio3)" onclick="pop_change_ratio3()">
                    	<input type="button" class="btn_03" style="margin-bottom:5px;" value="승률 조정하기(통합)" onclick="pop_change_ratio_all()">
<?
	}
?>
                    </div>
                    <div class="search_box">
                    	Min_WinRate :
                        <input type="input" class="search_text" id="min_winrate" name="min_winrate" style="width:40px" value="<?= $min_winrate?>" onkeypress="search_press(event)" />
                        &nbsp;&nbsp;~&nbsp;&nbsp;
                        Max_WinRate :                     
                        <input type="input" class="search_text" id="max_winrate" name="max_winrate" style="width:40px" value="<?= $max_winrate?>" onkeypress="search_press(event)" />
                        &nbsp;&nbsp;&nbsp;&nbsp;                    
                        <input type="button" class="btn_search" value="검색" onclick="search()" />
                    </div>
                </div>
                <!-- //title_warp -->
                
                
                <table class="tbl_list_basic1">

                <colgroup>
                    <col width="">
                    <col width="">
                    <col width="">
                    <col width="">
                    <col width="">
                    <col width="">
                    <col width="">
                    <col width="">
                </colgroup>

                <thead>
                    <tr>
                        <th>slot</th>
                        <th>일반 승률</th>
                        <th>단위 승률</th>
                        <th>win money</th>         
                        <th>money in</th>
                        <th>money out</th>
                        <th>playcount</th>
                        <th>writedate</th>
                    </tr>
                </thead>
                <tbody>
<?
    
    for ($i=0; $i<sizeof($winrate_data); $i++)
    {
        $slotname = $winrate_data[$i]["slotname"];
        $org_winrate = $winrate_data[$i]["org_winrate"];
        $winrate = $winrate_data[$i]["winrate"];
        $win_money = $winrate_data[$i]["win_money"];
        $money_in = $winrate_data[$i]["money_in"];
        $money_out = $winrate_data[$i]["money_out"];
        $playcount = $winrate_data[$i]["playcount"];
        $writedate = $winrate_data[$i]["writedate"];

        $org_winrate = $org_winrate * 100;
        $winrate = $winrate * 100;        
?>
                  <tr onmouseover="className='tr_over'" onmouseout="className=''" >
                        <td class="tdc point"><?= $slotname ?></td>
                        <td class="tdc point"><?= round($org_winrate, 2) ?>%</td>
                        <td class="tdc point"><?= round($winrate, 2) ?>%</td>
                        <td class="tdc point"><?= number_format($win_money) ?></td>
                        <td class="tdc point"><?= number_format($money_in) ?></td>
                        <td class="tdc point"><?= number_format($money_out) ?></td>
                        <td class="tdc point"><?= number_format($playcount) ?></td>
                        <td class="tdc point"><?= $writedate ?></td>    
                    </tr>

<?
    }
?>
                    </tbody>
                </table>
            </form>
            
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
<?
    $db_main2->end();
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>