<?
    $top_menu = "game_stats";
    $sub_menu = "thanksgiving_stat_daily";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $today = date("Y-m-d");
    
    $os_type = ($_GET["os_type"] == "") ? "4" :$_GET["os_type"];
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    
    if($search_start_createdate == "")
        $search_start_createdate = date("Y-m-d", strtotime("-3 day"));
        
    if($search_end_createdate == "")
        $search_end_createdate = $today;
        
    $tail = "";
    
    if($os_type == "0")
    {
        $os_txt = "Web";
        $os_type_sql = "AND os_type = 0 ";
        
    }
    else if($os_type == "1")
    {
        $os_txt = "IOS";
        $os_type_sql = "AND os_type = 1 ";
    }
    else if($os_type == "2")
    {
        $os_txt = "Android";
        $os_type_sql = "AND os_type = 2 ";
    }
    else if($os_type == "3")
    {
        $os_txt = "Amazon";
        $os_type_sql = "AND os_type = 3 ";
    }
    else if($os_type == "4")
    {
        $os_txt = "Total";
    }
    
        
    $db_main2 = new CDatabase_Main2();
    
    $sql = " SELECT today
                    , reward_group
                    , os_type
                    , SUM(quest1_count) AS quest1_count
                    , SUM(quest2_count) AS quest2_count
                    , SUM(questall_count) AS questall_count
                    , SUM(money_in) AS money_in
                    , SUM(reward_amount) AS reward_amount
                    ,SUM(reward_count) AS reward_count 
                    FROM `tbl_user_event_quest_stat` 
                    WHERE 1=1 $os_type_sql  AND event_idx = 3
                   AND today BETWEEN '$search_start_createdate' AND '$search_end_createdate' $os_type_sql 
             GROUP BY today, reward_group ORDER BY today desc, reward_group asc ";
    $thanksgiving_data = $db_main2->gettotallist($sql);
    
    function get_today_row_array($list, $today)
    {
        $row_array = array();
        
        for ($i=0; $i<sizeof($list); $i++)
        {
            if ($list[$i]["today"] == $today)
            {
                array_push($row_array, $list[$i]["reward_group"]);
            }
        }
        
        return $row_array;
    }
    
    
    $db_main2->end();
    
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#start_createdate").datepicker({ });
	});
	
	$(function() {
		$("#end_createdate").datepicker({ });
	});
	
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.startdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    } 
	
	    if (search_form.enddate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    } 
	
	    search_form.submit();
	}
	function change_os_type(type)
	{
		var search_form = document.search_form;

		var total = document.getElementById("type_total");
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
		else if (type == "4")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap" style="height:76px;">
		<div class="title"><?= $top_menu_txt ?> &gt; <?=$platform_name?> Thanksgiving 통계</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
			<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />

        		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
					<input type="button" class="<?= ($os_type == "4") ? "btn_schedule_select" : "btn_schedule" ?>" value="ALL" id="type_total" onclick="change_os_type('4')"    />
					<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
					<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
					<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
					<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
				</span>
				<span class="search_lbl ml20">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" /> ~
				<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	<div class="search_result"><?= $os_txt?></div>
			<div class="search_result">
        		<span><?= $search_start_createdate ?> ~ <?= $search_end_createdate ?></span> 통계입니다
        	</div>
    		<table class="tbl_list_basic1">
    		<colgroup>
    			<col width="70">
    			<col width="120">
    			<col width="120">
                <col width="120">
                <col width="120">
                <col width="90">
                <col width="90">
                <col width="90">
                <col width="90">
    		</colgroup>
            <thead>
                <tr>
                 	<th>날짜</th>
        			<th>칠면조 단계</th>
        			<th>미션 1달성</th>
        			<th>미션 2달성</th>
        			<th>전체 달성 유저 수 </th>
        			<th>money_in</th>
        			<th>리워드 받은 <br/> 유저 수</th>
        			<th>리워드 금액</th>
        			<th>1인당 평균 <br/> 리워드 금액</th>
        		</tr>
            </thead>
            <tbody>
	
<?
$thanksgiving_data_stat = array();
    
    for($i=0; $i<sizeof($thanksgiving_data); $i++)
    {
        $today = $thanksgiving_data[$i]["today"];
        $reward_group = $thanksgiving_data[$i]["reward_group"];
        $quest1_count = $thanksgiving_data[$i]["quest1_count"];
        $quest2_count = $thanksgiving_data[$i]["quest2_count"];
        $questall_count = $thanksgiving_data[$i]["questall_count"];
        $money_in = $thanksgiving_data[$i]["money_in"];
        $reward_amount = $thanksgiving_data[$i]["reward_amount"];
        $reward_count = $thanksgiving_data[$i]["reward_count"];
        
        $day_sum_quest1_count += $thanksgiving_data[$i]["quest1_count"];
        $day_sum_quest2_count += $thanksgiving_data[$i]["quest2_count"];
        $day_sum_questall_count += $thanksgiving_data[$i]["questall_count"];
        $day_sum_money_in += $thanksgiving_data[$i]["money_in"];
        $day_sum_reward_amount += $thanksgiving_data[$i]["reward_amount"];
        $day_sum_reward_count += $thanksgiving_data[$i]["reward_count"];
        
        ?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
                if($currenttoday != $today)
                {
                    $currenttoday = $today;
                    $viral_list = get_today_row_array($thanksgiving_data, $today);
?>
					<td class="tdc point_title" rowspan="<?= sizeof($viral_list)+1?>" valign="center"><?= $today ?></td>
<?
                }
?>
            <td class="tdc"><?= $reward_group?></td>
            <td class="tdc"><?= number_format($quest1_count) ?></td>
            <td class="tdc"><?= number_format($quest2_count) ?></td>
            <td class="tdc"><?= number_format($questall_count) ?></td>
            <td class="tdc"><?= number_format($money_in) ?></td>
            <td class="tdc"><?= number_format($reward_count) ?></td>
            <td class="tdc"><?= number_format($reward_amount) ?></td>
            <td class="tdc"><?= number_format(ROUND($reward_amount/$reward_count)) ?></td>
        </tr>
<?
                if((($today != $thanksgiving_data[$i+1]["today"])) || $i+1 == sizeof($thanksgiving_data))
				{
?>
					<tr>
						<td class="tdc point_title">Total</td>
						 <td class="tdc"><?= number_format($day_sum_quest1_count) ?></td>
                         <td class="tdc"><?= number_format($day_sum_quest2_count) ?></td>
                         <td class="tdc"><?= number_format($day_sum_questall_count) ?></td>
                         <td class="tdc"><?= number_format($day_sum_money_in) ?></td>
                         <td class="tdc"><?= number_format($day_sum_reward_count) ?></td>
                         <td class="tdc"><?= number_format($day_sum_reward_amount) ?></td>
                         <td class="tdc"><?= number_format(ROUND($day_sum_reward_amount/$day_sum_reward_count)) ?></td>						
					</tr>					
<?
                    $day_sum_quest1_count =0;
                    $day_sum_quest2_count =0;
                    $day_sum_questall_count =0;
                    $day_sum_money_in =0;
                    $day_sum_reward_amount =0;
                    $day_sum_reward_count =0;
				}
			}
            if(sizeof($thanksgiving_data) == 0)
            {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="5">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
            </tbody>
    	</table>
</div>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>