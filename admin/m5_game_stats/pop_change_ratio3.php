<? 
    include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
    
    check_login_layer();
    
    $category = $_POST["category"];
    
    if ($category == "")
        error_close_layer("잘못된 접근입니다.");
    
    $db_main2 = new CDatabase_Main2();
    
    if($category == "web")
    {
    	$sql = "SELECT t1.slottype, t2.slotname FROM tbl_slot_profit_setting t1 LEFT JOIN tbl_slot_list t2 ON t1.slottype = t2.slottype WHERE t1.special_value > 0;";
    	$slotlist = $db_main2->gettotallist($sql);
    }
    else if($category == "ios")
    {
    	$sql = "SELECT t1.slottype, t2.slotname FROM tbl_slot_profit_setting_ios t1 LEFT JOIN tbl_slot_list t2 ON t1.slottype = t2.slottype WHERE ios = 1 AND special_value > 0";
    	$slotlist = $db_main2->gettotallist($sql);
    }
    else if($category == "android")
    {
    	$sql = "SELECT t1.slottype, t2.slotname FROM tbl_slot_profit_setting_android t1 LEFT JOIN tbl_slot_list t2 ON t1.slottype = t2.slottype WHERE ios = 1 AND special_value > 0";
    	$slotlist = $db_main2->gettotallist($sql);
    }
    else if($category == "amazon")
    {
    	$sql = "SELECT t1.slottype, t2.slotname FROM tbl_slot_profit_setting_amazon t1 LEFT JOIN tbl_slot_list t2 ON t1.slottype = t2.slottype WHERE ios = 1 AND special_value > 0";
    	$slotlist = $db_main2->gettotallist($sql);
    }

    $db_main2->end();	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $category ?> Ratio3 승률 조정</title>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script>
<script type="text/javascript">
    function layer_close()
    {
        window.parent.close_layer_popup("<?= $_GET["layer_no"] ?>");
    }

    function change_ratio(ratio)
    {
		var ratio = ratio;

		var before_ratio = document.getElementById("update_ratio").value;
		var update_ratio = Number(before_ratio) + Number(ratio);
		
		document.getElementById("update_ratio").value = update_ratio;
    }
    
    function update_change_ratio()
    {
        var input_form = document.input_form;

        if (input_form.slot.value == "")
        {			         
            alert("슬롯을 선택해주세요.");
            input_form.slot.focus();
            return;
        }
        
        if (input_form.update_ratio.value == "0")
        {
            alert("조정 승률을 입력해주세요.");
            input_form.update_ratio.focus();
            return;
        }

        var param = {};
		param.slot = input_form.slot.value;
		param.current_ratio = input_form.current_ratio.value;
		param.update_ratio = input_form.update_ratio.value;
		param.category = '<?= $category ?>';
		param.ratio_type = 3;

        WG_ajax_execute("game/change_ratio", param, update_change_ratio_callback, false);      
    }
    
    function update_change_ratio_callback(result, reason)
    {
        var input_form = document.input_form; 
        
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            alert("승률 변경 완료했습니다.");
            select_slot(input_form.slot.value); 
        }
    }

    function select_slot(slottype)
    {
		if(slottype == "")
			return;

        var param = {};
		param.slottype = slottype;
		param.category = '<?= $category ?>';
		param.ratio_type = 3;
		
        WG_ajax_query("game/get_select_slot", param, get_select_slot_callback, false);
    }

    function get_select_slot_callback(result, reason, map)
    {
    	if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            var ratio = map.ratio_org;
            var variation = map.variation;
            var variation_dev = map.variation_dev;
            var writedate = map.writedate;
            var slottype = map.slottype;
            var current_ratio = "";

            current_ratio = ratio;                
            
            document.getElementById("current_ratio").value = current_ratio;
            document.getElementById("update_ratio").value = "0";
            document.getElementById("writedate").value = writedate;            

            var param = {};
    		param.slottype = slottype;
    		param.category = '<?= $category ?>'; 
    		param.ratio_type = 3;

            WG_ajax_list("game/get_ratio_history", param, get_ratio_history_callback, true);
        }
    }

    function get_ratio_history_callback(result, reason, totalcount, list)
    {
    	if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
			var tbody = document.getElementById("history_contents");
            
            while (tbody.childNodes.length > 0)
                tbody.removeChild(tbody.childNodes[0]);
                
            for (var i=0; i<list.length; i++)
            {
                var tr = document.createElement("tr");
                var td = document.createElement("td");
                var th = document.createElement("th");
                
                tr.appendChild(td);
                td.className = "tdl point";
                td.innerText = list[i].category;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdr point";
                td.innerText = list[i].before_ratio;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].current_ratio;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].adminid;
                    
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].writedate;
                
                tbody.appendChild(tr);
            }
        }
    }
</script>
</head>
<body class="layer_body" onload="window.focus()" onkeyup="if (getEventKeycode(event) == 27) { layer_close(); }">
<div id="layer_wrap" style="overflow-y:hidden">   
    <div class="layer_header" >
        <div class="layer_title"><?= $category ?> 승률 조정</div>
        <img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>        
    <div class="layer_contents_wrap" style="width:520px; overflow-y:hidden;">
         <!--  레이어 내용  -->
         
         <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
            <table class="tbl_view_basic" style="width:500px">
                <colgroup>
                    <col width="">
                    <col width="">
                </colgroup>
                    <tbody>
                         <tr>
                             <th>슬롯</th>
                             <td>
            					<select name="slot" id="slot" onchange="select_slot(this.value);">
									<option value="">선택하세요</option>
<?
	for ($i=0; $i<sizeof($slotlist); $i++)
	{
		$slottype = $slotlist[$i]["slottype"];
		$slotname = $slotlist[$i]["slotname"];
?>	
									<option value="<?= $slottype ?>"><?= $slotname ?></option>
<?						
	}
?>						
								</select>
            				</td>
                         </tr>
                         
                         <tr>
                             <th>현재 승률</th>
                             <td><input type="text" class="view_tbl_text" style="width:50px" name="current_ratio" id="current_ratio" value="" readonly="readonly" /></td>
                         </tr>
                         
                         <tr>
                             <th>수정일자</th>
                             <td><input type="text" class="view_tbl_text" style="width:190px" name="writedate" id="writedate" value="" readonly="readonly" /></td>
                         </tr>
                         
                         <tr>
                             <th>승률 조정</th>
                             <td>
                             	<input type="text" class="view_tbl_text" style="width:50px" name="update_ratio" id="update_ratio" value="0" readonly="readonly" />&nbsp;&nbsp;&nbsp;&nbsp;
                             	<input type="button" class="btn_02" value="+" onclick="change_ratio(1)" />&nbsp;<input type="button" class="btn_02" value="-" onclick="change_ratio(-1)" />
                             </td>
                         </tr>
                     </tbody>
             </table>  
             </form>  
         </div>
         
         <!-- 확인 버튼 -->
         <div class="layer_button_wrap" style="width:420px;text-align:right;">
            <input type="button" class="btn_02" value="변경" onclick="update_change_ratio()" />
            <input type="button" class="btn_02" value="닫기" onclick="fnPopupClose()" />
         </div>
         <!-- 확인 버튼 -->
         
         <span>승률 변경 History</span>
        <div style="width:500px; height:250px; overflow-y:auto;">
         <table class="tbl_view_basic">
         	<thead>
         		<tr>
         			<th>OS</th>
         			<th>이전 승률</th>
         			<th>이후 승률</th>
         			<th>관리자</th>
         			<th>수정일</th>
         		</tr>
         	</thead>         	 
         	<tbody id="history_contents"></tbody>         	
         </table>
         </div>         
         
         
         <!--  //레이어 내용  -->
    </div>
</div>
</body>
</html>

