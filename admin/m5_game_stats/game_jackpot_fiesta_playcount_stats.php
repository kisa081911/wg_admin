<?
    $top_menu = "game_stats";
    $sub_menu = "game_jackpot_fiesta_playcount_stats";

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $tab = ($_GET["tab"] == "") ? "slottype" :$_GET["tab"];    
    $viewmode = ($_GET["viewmode"] == "") ? "0" :$_GET["viewmode"];
    $betlevel = ($_GET["betlevel"] == "") ? "betlevel" :$_GET["betlevel"];
    $search_fiestaidx = ($_GET["fiestaidx"] == "") ? "fiestaidx" :$_GET["fiestaidx"];
    $pagename = "game_jackpot_fiesta_playcount_stats.php";
    
    if ($tab == "")
    	error_back("잘못된 접근입니다.");
    
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    
   	$betlevel_tail = "AND betlevel=$betlevel";
    
    $sql = "SELECT slottype,slotname FROM tbl_slot_list";
    $slotlist = $db_main2->gettotallist($sql);
    
    $sql = "SELECT bettype, betname FROM tbl_slot_betlevel ORDER BY bettype ASC";
    $betlist = $db_main2->gettotallist($sql);
    
    for($k=0; $k<sizeof($betlist); $k++)
    {     
	    $total_betlist_name[$betlist[$k]["bettype"]] = $betlist[$k]["betname"];
    }
    
    if($search_fiestaidx == "fiestaidx")
    {
    	$sql = "SELECT DISTINCT fiestaidx FROM tbl_jackpot_fiesta_playcount ORDER BY fiestaidx DESC LIMIT 1";
    	$top_fiestaidx = $db_main2->getvalue($sql);
    	
    	$fiestaidx_sql = "AND fiestaidx BETWEEN $search_fiestaidx - 10 AND $search_fiestaidx";
    }
    
    if($viewmode == "0")
    {
    	$sql = "SELECT fiestaidx, COUNT(DISTINCT slottype) as rowcount
    			FROM `tbl_jackpot_fiesta_playcount`
    			WHERE slottype=$tab $betlevel_tail AND fiestaidx=$search_fiestaidx $fiestaidx_sql
    			GROUP BY fiestaidx
    			ORDER BY fiestaidx DESC";
    }
    else
    {
    	$sql = "SELECT COUNT(slottype) AS rowcount
    			FROM (
    				SELECT *
    				FROM tbl_jackpot_fiesta_playcount
    				GROUP BY fiestaidx, slottype, betlevel
    				ORDER BY fiestaidx DESC
    			)`tbl_jackpot_fiesta_playcount`
    			WHERE slottype=$tab $betlevel_tail AND fiestaidx=$search_fiestaidx $fiestaidx_sql
    			GROUP BY fiestaidx
    			ORDER BY fiestaidx DESC";
    }
    $fiestaidx_rowspan_list = $db_main2->gettotallist($sql);
    
    $sql = "SELECT fiestaidx, slottype, COUNT(DISTINCT betlevel) as rowcount
    		FROM tbl_jackpot_fiesta_playcount
    		WHERE slottype=$tab $betlevel_tail AND fiestaidx=$search_fiestaidx $fiestaidx_sql
    		GROUP BY fiestaidx, slottype
    		ORDER BY fiestaidx DESC";
    $slot_rowspan_list = $db_main2->gettotallist($sql);
    
    if($viewmode == "0")
    {
    	$sql = "SELECT fiestaidx, slottype,
      			SUM(IF(devicetype=0,playcount,0)) AS web_fiesta_playcount,
				SUM(IF(devicetype=1,playcount,0)) AS ios_fiesta_playcount,
				SUM(IF(devicetype=2,playcount,0)) AS and_fiesta_playcount,
				SUM(IF(devicetype=3,playcount,0)) AS ama_fiesta_playcount,
				SUM(IF(devicetype=0,jackpot_count,0)) AS web_jackpot_count,
				SUM(IF(devicetype=1,jackpot_count,0)) AS ios_jackpot_count,
				SUM(IF(devicetype=2,jackpot_count,0)) AS and_jackpot_count,
				SUM(IF(devicetype=3,jackpot_count,0)) AS ama_jackpot_count
    			FROM tbl_jackpot_fiesta_playcount
    			WHERE slottype=$tab $betlevel_tail AND fiestaidx=$search_fiestaidx $fiestaidx_sql
    			GROUP BY fiestaidx, slottype
    			ORDER BY fiestaidx DESC";
    }
    else if($viewmode == "1")
    {
    	$sql = "SELECT fiestaidx, slottype, betlevel,
      			SUM(IF(devicetype=0,playcount,0)) AS web_fiesta_playcount,
				SUM(IF(devicetype=1,playcount,0)) AS ios_fiesta_playcount,
				SUM(IF(devicetype=2,playcount,0)) AS and_fiesta_playcount,
				SUM(IF(devicetype=3,playcount,0)) AS ama_fiesta_playcount,
      			SUM(IF(devicetype=0,jackpot_count,0)) AS web_jackpot_count,
				SUM(IF(devicetype=1,jackpot_count,0)) AS ios_jackpot_count,
				SUM(IF(devicetype=2,jackpot_count,0)) AS and_jackpot_count,
				SUM(IF(devicetype=3,jackpot_count,0)) AS ama_jackpot_count
    			FROM tbl_jackpot_fiesta_playcount
    			WHERE slottype=$tab $betlevel_tail AND fiestaidx=$search_fiestaidx $fiestaidx_sql
    			GROUP BY fiestaidx, slottype, betlevel
    			ORDER BY fiestaidx DESC"; 
    }
    $playcount_list = $db_main2->gettotallist($sql);   

    if($playcount_list != null)
    {
    	$fiestaidx = $playcount_list[0]["fiestaidx"];
    
    	$fiesta_status = $db_main2->getvalue("SELECT web_status FROM tbl_jackpot_fiesta_info WHERE fiestaidx = $fiestaidx");
    	 
    	if($fiesta_status)
    		$style = "style='width:1400px'";
    }

    $db_main2->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    
    function tab_change(tab)
    {
        var search_form = document.search_form;
        search_form.tab.value = tab;
        search_form.submit();
    }

    function pop_change_jackpot_seed()
	{
		open_layer_popup("pop_change_jackpot_seed.php?", 500, 550);
	}

    function change_version_term(term)
	{
		var search_form = document.search_form;
		
		var old_version = document.getElementById("term_old");
		var new_version = document.getElementById("term_new");
		
		search_form.version_term.value = term;

		if (term == "0")
		{
			old_version.className="btn_schedule_select";
			new_version.className="btn_schedule";
		}
		else if (term == "1")
		{
			old_version.className="btn_schedule";
			new_version.className="btn_schedule_select";
		}

		search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
	<div class="contents_wrap">
        <form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>" <?= $style ?> >
        	<!-- title_warp -->
            <div class="title_wrap">
            	<div class="title"><?= $top_menu_txt ?> &gt; 일별 잭팟 피에스타</div>
                <div class="search_box">
                    <input type="hidden" name="tab" id="tab" value="<?= $tab ?>" />
                    fiestaidx&nbsp;<input type="input" class="search_text" id="fiestaidx" name="fiestaidx" style="width:65px" value="<?= ($search_fiestaidx == "fiestaidx") ? "" : $search_fiestaidx ?>" onkeypress="search_press(event)" />&nbsp;
                    <input type="button" class="btn_search" value="검색" onclick="search()" />
                    </div>
            </div>
            <!-- //title_warp -->
            
            <ul class="tab">
            	<li id="tab_0" class="<?= ($tab == "slottype") ? "select" : "" ?>" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('slottype')">ALL</li>
<?
	for ($i=0; $i<sizeof($slotlist); $i++)
	{
		$tab_slottype = $slotlist[$i]["slottype"];
		$tab_slotname = str_replace(" Slot", "", $slotlist[$i]["slotname"]);
?>	
                <li id="tab_<?= $tab_slottype ?>" class="<?= ($tab == $tab_slottype) ? "select" : "" ?>" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('<?= $tab_slottype ?>')"><?= $tab_slotname ?></li>
<?
	}
?>                
            </ul>
                
            <table class="tbl_list_basic1" style='width:1400px'>
                <colgroup>
                	<col width="80">
<?
	if($viewmode == "1")
	{
?>
					<col width="">
<?
	}
?>
                	<col width="">
                	<col width="">
                	<col width="">
                	<col width="">
                	<col width="">
                	<col width="">
                	<col width="">
                	<col width="">
                	<col width="">
                	<col width="">
                	<col width="">
                	<col width="">
                	<col width="">
<?
	if($fiesta_status == "1")
	{
?>
					<col width="">
                	<col width="">
                	<col width="">
                	<col width="">
<?
	}
?>
                </colgroup>
                
                <thead>
                	<tr>
                		<th style="border-top:none;" rowspan="2">fiestaidx</th>
                		<th style="text-align:center;border-top:none;" rowspan="2">슬롯</th>
<?
	if($viewmode == "1")
	{
?>
						<th style="text-align:center;border-top:none;" rowspan="2">베팅레벨</th>
<?
	}
?>
						<th style="text-align:center;border-top:none;" colspan="4">잭팟파티 플레이 카운트</th>
                        <th style="text-align:center;border-top:none;" colspan="4">잭팟 건당 스핀수</th>
                        <th style="text-align:center;border-top:none;" colspan="4">잭팟 금액</th>
                        <th style="text-align:center;border-top:none;" colspan="4">잭팟 수</th>
<?
	if($fiesta_status == 1)
	{
?>
                        <th style="text-align:center;border-top:none;" colspan="4">최근 5분 잭팟수</th>
<?
	}
?>
					</tr>
					
					<tr>
						<th style="text-align:center;border-top:none;">Web</th>
                		<th style="text-align:center;border-top:none;">iOS</th>
                		<th style="text-align:center;border-top:none;">Android</th>
                		<th style="text-align:center;border-top:none;border-right: 1px dotted #dbdbdb;">Amazon</th>
                		
                		<th style="text-align:center;border-top:none;">Web</th>
                		<th style="text-align:center;border-top:none;">iOS</th>
                		<th style="text-align:center;border-top:none;">Android</th>
                		<th style="text-align:center;border-top:none;">Amazon</th>
                		
                		<th style="text-align:center;border-top:none;">Web</th>
                		<th style="text-align:center;border-top:none;">iOS</th>
                		<th style="text-align:center;border-top:none;">Android</th>
                		<th style="text-align:center;border-top:none;">Amazon</th>
                		
                		<th style="text-align:center;border-top:none;">Web</th>
                		<th style="text-align:center;border-top:none;">iOS</th>
                		<th style="text-align:center;border-top:none;">Android</th>
                		<th style="text-align:center;border-top:none;">Amazon</th>
                		
<?
	if($fiesta_status == 1)
	{
?>
						<th style="text-align:center;border-top:none;">Web</th>
						<th style="text-align:center;border-top:none;">iOS</th>
                		<th style="text-align:center;border-top:none;">Android</th>
                		<th style="text-align:center;border-top:none;">Amazon</th>
<?
						
	}
?>
                	</tr>
                </thead>
                
                <tbody>
<?
	$playcount_list = $playcount_list;
	$fiestaidx_tmp = "";
	$slottmp = "";
	
	$fiestaidx_rowspan_index = 0;
	$slot_rowspan_index = 0;
	
	$jackpot_log_web_sum = array();
	$jackpot_log_ios_sum = array();
	$jackpot_log_and_sum = array();
	$jackpot_log_ama_sum = array();
	
	$total_web_jackpot_amount_sum = array();
	$total_ios_jackpot_amount_sum = array();
	$total_and_jackpot_amount_sum = array();
	$total_ama_jackpot_amount_sum = array();
	
	$sum_web_fiesta_playcount_list = array();
	$sum_ios_fiesta_playcount_list = array();
	$sum_and_fiesta_playcount_list = array();
	$sum_ama_fiesta_playcount_list = array();
	
	$sum_web_jackpot_count_list = array();
	$sum_ios_jackpot_count_list = array();
	$sum_and_jackpot_count_list = array();
	$sum_ama_jackpot_count_list = array();
	
	$total_web_jackpot_amount_sum = array();
	$total_ios_jackpot_amount_sum = array();
	$total_and_jackpot_amount_sum = array();
	$total_ama_jackpot_amount_sum = array();
	
	$total_web_jackpot_cnt_sum = array();
	$total_ios_jackpot_cnt_sum = array();
	$total_and_jackpot_cnt_sum = array();
	$total_ama_jackpot_cnt_sum = array();
	
	$betlevel_count_list = array();
	$slotname_list = array();
	$slotname_list_index = 0;
	
	$total_row_count = 0;
	$recent_5minute_flag = 1;
	
	
	for($i=0; $i<sizeof($playcount_list); $i++)
	{
		$fiestaidx = $playcount_list[$i]["fiestaidx"];	
		$betlevel_data = $playcount_list[$i]["betlevel"];
		$slottype = $playcount_list[$i]["slottype"];
		
		$web_fiesta_playcount = $playcount_list[$i]["web_fiesta_playcount"];
		$ios_fiesta_playcount = $playcount_list[$i]["ios_fiesta_playcount"];
		$and_fiesta_playcount = $playcount_list[$i]["and_fiesta_playcount"];
		$ama_fiesta_playcount = $playcount_list[$i]["ama_fiesta_playcount"];	
	
		$sum_web_fiesta_playcount += $playcount_list[$i]["web_fiesta_playcount"];
		$sum_ios_fiesta_playcount += $playcount_list[$i]["ios_fiesta_playcount"];
		$sum_and_fiesta_playcount += $playcount_list[$i]["and_fiesta_playcount"];
		$sum_ama_fiesta_playcount += $playcount_list[$i]["ama_fiesta_playcount"];
		
		$web_jackpot_count = $playcount_list[$i]["web_jackpot_count"];
		$ios_jackpot_count = $playcount_list[$i]["ios_jackpot_count"];
		$and_jackpot_count = $playcount_list[$i]["and_jackpot_count"];
		$ama_jackpot_count = $playcount_list[$i]["ama_jackpot_count"];
		
		if($i == 0)
		{
			if($fiesta_status == 1)
			{
				if($viewmode == "0")
				{
					$select = "slottype";
					$groupby = " GROUP BY slottype ";
					$orderby = " ORDER BY slottype ASC ";
				}
				else
				{
					$select = "slottype, betlevel";
					$groupby = " GROUP BY slottype, betlevel ";
					$orderby = " ORDER BY slottype ASC, betlevel ASC ";
				}
				
				$sql = "SELECT $select,
						SUM(IF(devicetype=0,1,0)) AS web_jackpot_cnt,
						SUM(IF(devicetype=1,1,0)) AS ios_jackpot_cnt,
						SUM(IF(devicetype=2,1,0)) AS and_jackpot_cnt,
						SUM(IF(devicetype=3,1,0)) AS ama_jackpot_cnt
						FROM tbl_jackpot_log
						WHERE fiestaidx = $fiestaidx 
						$groupby
						$orderby";
				$recent_5minute_jackpot_stat = $db_main->gettotallist($sql);
			}
		}

		if($viewmode == "0")
		{
			$jackpot_sql = "SELECT fiestaidx, slottype, ".
					"IFNULL(SUM(IF(devicetype=0, amount, 0)), 0) AS web_jackpot_sum, ".
					"IFNULL(SUM(IF(devicetype=1, amount, 0)), 0) AS ios_jackpot_sum, ".
					"IFNULL(SUM(IF(devicetype=2, amount, 0)), 0) AS and_jackpot_sum, ".
					"IFNULL(SUM(IF(devicetype=3, amount, 0)), 0) AS ama_jackpot_sum, ".
					"IFNULL(SUM(IF(devicetype=0, 1, 0)), 0) AS web_jackpot_cnt, ".
					"IFNULL(SUM(IF(devicetype=1, 1, 0)), 0) AS ios_jackpot_cnt, ".
					"IFNULL(SUM(IF(devicetype=2, 1, 0)), 0) AS and_jackpot_cnt, ".
					"IFNULL(SUM(IF(devicetype=3, 1, 0)), 0) AS ama_jackpot_cnt ".
					"FROM tbl_jackpot_log ".
					"WHERE fiestaidx = $fiestaidx ".
					"GROUP BY slottype ".
					"ORDER BY slottype ASC";
			$jackpot_sum_amount = $db_main->gettotallist($jackpot_sql);	

			for($j=0; $j<sizeof($jackpot_sum_amount); $j++)
			{
				$jackpot_log_fiestaidx = $jackpot_sum_amount[$j]["fiestaidx"];
				$jackpot_log_slottype = $jackpot_sum_amount[$j]["slottype"];
				$jackpot_log_web_sum[$jackpot_log_fiestaidx][$jackpot_log_slottype] = $jackpot_sum_amount[$j]["web_jackpot_sum"];
				$jackpot_log_ios_sum[$jackpot_log_fiestaidx][$jackpot_log_slottype] = $jackpot_sum_amount[$j]["ios_jackpot_sum"];
				$jackpot_log_and_sum[$jackpot_log_fiestaidx][$jackpot_log_slottype] = $jackpot_sum_amount[$j]["and_jackpot_sum"];
				$jackpot_log_ama_sum[$jackpot_log_fiestaidx][$jackpot_log_slottype] = $jackpot_sum_amount[$j]["ama_jackpot_sum"];
				
				$jackpot_log_web_cnt[$jackpot_log_fiestaidx][$jackpot_log_slottype] = $jackpot_sum_amount[$j]["web_jackpot_cnt"];
				$jackpot_log_ios_cnt[$jackpot_log_fiestaidx][$jackpot_log_slottype] = $jackpot_sum_amount[$j]["ios_jackpot_cnt"];
				$jackpot_log_and_cnt[$jackpot_log_fiestaidx][$jackpot_log_slottype] = $jackpot_sum_amount[$j]["and_jackpot_cnt"];
				$jackpot_log_ama_cnt[$jackpot_log_fiestaidx][$jackpot_log_slottype] = $jackpot_sum_amount[$j]["ama_jackpot_cnt"];
			}
		}
		else
		{
			$jackpot_sql = "SELECT fiestaidx, slottype, betlevel, ".
					"IFNULL(SUM(IF(devicetype=0, amount, 0)), 0) AS web_jackpot_sum, ".
					"IFNULL(SUM(IF(devicetype=1, amount, 0)), 0) AS ios_jackpot_sum, ".
					"IFNULL(SUM(IF(devicetype=2, amount, 0)), 0) AS and_jackpot_sum, ".
					"IFNULL(SUM(IF(devicetype=3, amount, 0)), 0) AS ama_jackpot_sum ".
					"FROM tbl_jackpot_log ".
					"WHERE fiestaidx = $fiestaidx ".					
					"GROUP BY slottype, betlevel ".
					"ORDER BY slottype ASC, betlevel ASC";
			$jackpot_sum_amount = $db_main->gettotallist($jackpot_sql);
			
			for($j=0; $j<sizeof($jackpot_sum_amount); $j++)
			{
				$jackpot_log_fiestaidx = $jackpot_sum_amount[$j]["fiestaidx"];
				$jackpot_log_slottype = $jackpot_sum_amount[$j]["slottype"];
				$jackpot_log_betlevel = $jackpot_sum_amount[$j]["betlevel"];
				$jackpot_log_web_sum[$jackpot_log_fiestaidx][$jackpot_log_slottype][$jackpot_log_betlevel] = $jackpot_sum_amount[$j]["web_jackpot_sum"];
				$jackpot_log_ios_sum[$jackpot_log_fiestaidx][$jackpot_log_slottype][$jackpot_log_betlevel] = $jackpot_sum_amount[$j]["ios_jackpot_sum"];
				$jackpot_log_and_sum[$jackpot_log_fiestaidx][$jackpot_log_slottype][$jackpot_log_betlevel] = $jackpot_sum_amount[$j]["and_jackpot_sum"];
				$jackpot_log_ama_sum[$jackpot_log_fiestaidx][$jackpot_log_slottype][$jackpot_log_betlevel] = $jackpot_sum_amount[$j]["ama_jackpot_sum"];
			}
		}
		
		for ($j=0; $j<sizeof($slotlist); $j++)
		{
			if($slottype == $slotlist[$j]["slottype"])
				$slotname = str_replace(" Slot", "", $slotlist[$j]["slotname"]);
		}

		// 슬롯명 추출
		if(strpos($slotname_list[$slotname_list_index], $slotname) === false)
			$slotname_list[$slotname_list_index++] = $slotname;
		
		if($fiestaidx_tmp != $fiestaidx)
		{
?>
				<tr onmouseover="className='tr_over'" onmouseout="className=''" style="border-top:1px double;">
					<td class="tdc point" rowspan="<?= $fiestaidx_rowspan_list[$fiestaidx_rowspan_index]["rowcount"] ?>"><?= $fiestaidx?></td>
<?
			$fiestaidx_rowspan_index++;
		}
		else 
		{
?>
				<tr onmouseover="className='tr_over'" onmouseout="className=''" >
<?
		}
?>
					
<?
		if($viewmode == "0")
		{
?>
					<td class="tdc point"><?= $slotname ?></td>
<?
		}
		else if($viewmode == "1" && ($slottmp != $slottype))				
		{
?>					
					<td class="tdc point" rowspan="<?= $slot_rowspan_list[$slot_rowspan_index]["rowcount"] ?>"><?= $slotname ?></td>
<?
			$slot_rowspan_index++;
		}
		
		if($viewmode == "1")
		{
			$betlevel_str = $total_betlist_name[$betlevel_data];
			
			// 슬롯별 베팅레벨 추출
			if(strpos($betlevel_count_list[$slotname]["betlevel"], $betlevel_data) === false)
				$betlevel_count_list[$slotname]["betlevel"] .= "/".$betlevel_data;
?>
					<td class="tdc point"><?= $betlevel_str ?></td>
<?
		}
		
		if($viewmode == "0")
		{
			$sum_web_fiesta_playcount_list[$slotname] += $web_fiesta_playcount;
			$sum_ios_fiesta_playcount_list[$slotname] += $ios_fiesta_playcount;
			$sum_and_fiesta_playcount_list[$slotname] += $and_fiesta_playcount;
			$sum_ama_fiesta_playcount_list[$slotname] += $ama_fiesta_playcount;
			
			$sum_web_jackpot_count_list[$slotname] += $web_jackpot_count;
			$sum_ios_jackpot_count_list[$slotname] += $ios_jackpot_count;
			$sum_and_jackpot_count_list[$slotname] += $and_jackpot_count;
			$sum_ama_jackpot_count_list[$slotname] += $ama_jackpot_count;
			
			
			$total_web_jackpot_amount_sum[$slottype] += $jackpot_log_web_sum[$fiestaidx][$slottype];
			$total_ios_jackpot_amount_sum[$slottype] += $jackpot_log_ios_sum[$fiestaidx][$slottype];
			$total_and_jackpot_amount_sum[$slottype] += $jackpot_log_and_sum[$fiestaidx][$slottype];
			$total_ama_jackpot_amount_sum[$slottype] += $jackpot_log_ama_sum[$fiestaidx][$slottype];
			
			$total_web_jackpot_cnt_sum[$slottype] += $jackpot_log_web_cnt[$fiestaidx][$slottype];
			$total_ios_jackpot_cnt_sum[$slottype] += $jackpot_log_ios_cnt[$fiestaidx][$slottype];
			$total_and_jackpot_cnt_sum[$slottype] += $jackpot_log_and_cnt[$fiestaidx][$slottype];
			$total_ama_jackpot_cnt_sum[$slottype] += $jackpot_log_ama_cnt[$fiestaidx][$slottype];
		}
		else if($viewmode == "1")
		{
			$sum_web_fiesta_playcount_list[$slotname][$betlevel_str] += $web_fiesta_playcount;
			$sum_ios_fiesta_playcount_list[$slotname][$betlevel_str] += $ios_fiesta_playcount;
			$sum_and_fiesta_playcount_list[$slotname][$betlevel_str] += $and_fiesta_playcount;
			$sum_ama_fiesta_playcount_list[$slotname][$betlevel_str] += $ama_fiesta_playcount;
			
			$sum_web_jackpot_count_list[$slotname][$betlevel_str] += $web_jackpot_count;
			$sum_ios_jackpot_count_list[$slotname][$betlevel_str] += $ios_jackpot_count;
			$sum_and_jackpot_count_list[$slotname][$betlevel_str] += $and_jackpot_count;
			$sum_ama_jackpot_count_list[$slotname][$betlevel_str] += $ama_jackpot_count;
		}
		
		if($i == sizeof($playcount_list) - 1)
		{
			$style = "style='border-right: 1px dotted #dbdbdb;'";
		}
?>
					<td class="tdc point"><?= number_format($web_fiesta_playcount) ?></td>
					<td class="tdc point"><?= number_format($ios_fiesta_playcount) ?></td>
					<td class="tdc point"><?= number_format($and_fiesta_playcount) ?></td>
					<td class="tdc point" style="border-right: 1px dotted #dbdbdb;"><?= number_format($ama_fiesta_playcount) ?></td>
					
					<td class="tdc point"><?= number_format($web_jackpot_count) ?> (<?= ($web_jackpot_count == 0) ? "0" : (($category == 0) ?  number_format($web_fiesta_playcount / $web_jackpot_count) : number_format($web_fiesta_playcount / $web_jackpot_count)) ?>)</td>
					<td class="tdc point"><?= number_format($ios_jackpot_count) ?> (<?= ($ios_jackpot_count == 0) ? "0" : (($category == 0) ?  number_format($ios_fiesta_playcount / $ios_jackpot_count) : number_format($ios_fiesta_playcount / $ios_jackpot_count)) ?>)</td>
					<td class="tdc point"><?= number_format($and_jackpot_count) ?> (<?= ($and_jackpot_count == 0) ? "0" : (($category == 0) ?  number_format($and_fiesta_playcount / $and_jackpot_count) : number_format($and_fiesta_playcount / $and_jackpot_count)) ?>)</td>
					<td class="tdc point"><?= number_format($ama_jackpot_count) ?> (<?= ($ama_jackpot_count == 0) ? "0" : (($category == 0) ?  number_format($ama_fiesta_playcount / $ama_jackpot_count) : number_format($ama_fiesta_playcount / $ama_jackpot_count)) ?>)</td>
<?
		if($viewmode == "0")
		{
?>
					<td class="tdc point"><?= number_format($jackpot_log_web_sum[$fiestaidx][$slottype]) ?></td>
					<td class="tdc point"><?= number_format($jackpot_log_ios_sum[$fiestaidx][$slottype]) ?></td>
					<td class="tdc point"><?= number_format($jackpot_log_and_sum[$fiestaidx][$slottype]) ?></td>
					<td class="tdc point"><?= number_format($jackpot_log_ama_sum[$fiestaidx][$slottype]) ?></td>
					
					<td class="tdc point"><?= number_format($jackpot_log_web_cnt[$fiestaidx][$slottype]) ?></td>
					<td class="tdc point"><?= number_format($jackpot_log_ios_cnt[$fiestaidx][$slottype]) ?></td>
					<td class="tdc point"><?= number_format($jackpot_log_and_cnt[$fiestaidx][$slottype]) ?></td>
					<td class="tdc point"><?= number_format($jackpot_log_ama_cnt[$fiestaidx][$slottype]) ?></td>
					
<?
		}
				
		// fiesta
		if($i != 0 && $fiestaidx_tmp != $fiestaidx && $fiesta_status == 1)
			$recent_5minute_flag = 0;
		
		if($recent_5minute_flag == 1 && $fiesta_status == 1)
		{
			for($j=0; $j<sizeof($recent_5minute_jackpot_stat); $j++)
			{
				if($viewmode == "0")
				{
					if($slottype == $recent_5minute_jackpot_stat[$j]["slottype"])
					{
							$web_5minute_jackpot_count = $recent_5minute_jackpot_stat[$j]["web_jackpot_cnt"]; 
							$ios_5minute_jackpot_count = $recent_5minute_jackpot_stat[$j]["ios_jackpot_cnt"]; 
							$and_5minute_jackpot_count = $recent_5minute_jackpot_stat[$j]["and_jackpot_cnt"]; 
							$ama_5minute_jackpot_count = $recent_5minute_jackpot_stat[$j]["ama_jackpot_cnt"];
							
							break;
					}
				}
				else
				{
					if($slottype == $recent_5minute_jackpot_stat[$j]["slottype"] && $betlevel_data == $recent_5minute_jackpot_stat[$j]["betlevel"])
					{
						$web_5minute_jackpot_count = $recent_5minute_jackpot_stat[$j]["web_jackpot_cnt"];
						$ios_5minute_jackpot_count = $recent_5minute_jackpot_stat[$j]["ios_jackpot_cnt"];
						$and_5minute_jackpot_count = $recent_5minute_jackpot_stat[$j]["and_jackpot_cnt"];
						$ama_5minute_jackpot_count = $recent_5minute_jackpot_stat[$j]["ama_jackpot_cnt"];
							
						break;
					}
				}
				
			}
?>
					<td class="tdc point"><?= $web_5minute_jackpot_count ?></td>
					<td class="tdc point"><?= $ios_5minute_jackpot_count ?></td>
					<td class="tdc point"><?= $and_5minute_jackpot_count ?></td>
					<td class="tdc point"><?= $ama_5minute_jackpot_count ?></td>
<?
		}
?>
				</tr>
<?
		$fiestaidx_tmp = $fiestaidx;
		$slottmp = $slottype;
		
		$total_row_count++;
	}
	
	if($playcount_list != null)
	{
		for ($i=0; $i<sizeof($slotlist); $i++)
		{
			$slotname = str_replace(" Slot", "", $slotlist[$i]["slotname"]);
			$slottype = $slotlist[$i]["slottype"];
?>
				
<?
			if($viewmode == "0" && $i == 0)
			{
?>
				<tr style="border-top:1px double;">
					<td class="tdc point_title" rowspan="<?= sizeof($slotlist) ?>">Total</td>
<?
			}
			else if($viewmode == "1"  && $i == 0)
			{
?>
				<tr style="border-top:1px double;">
					<td class="tdc point_title" rowspan="<?= $total_row_count ?>">Total</td>
<?
			}
		
			if($viewmode == "0" && in_array($slotname, $slotname_list))
			{
?>
					<td class="tdc point"><?= $slotname ?></td>
					<td class="tdc point"><?= number_format($sum_web_fiesta_playcount_list[$slotname]) ?></td>
					<td class="tdc point"><?= number_format($sum_ios_fiesta_playcount_list[$slotname]) ?></td>
					<td class="tdc point"><?= number_format($sum_and_fiesta_playcount_list[$slotname]) ?></td>
					<td class="tdc point" style="border-right: 1px dotted #dbdbdb;"><?= number_format($sum_ama_fiesta_playcount_list[$slotname]) ?></td>
					
					<td class="tdc point"><?= number_format($sum_web_jackpot_count_list[$slotname]) ?> (<?= ($sum_web_jackpot_count_list[$slotname] == 0) ? "0" : number_format($sum_web_fiesta_playcount_list[$slotname] / $sum_web_jackpot_count_list[$slotname]) ?>)</td>
					<td class="tdc point"><?= number_format($sum_ios_jackpot_count_list[$slotname]) ?> (<?= ($sum_ios_jackpot_count_list[$slotname] == 0) ? "0" : number_format($sum_ios_fiesta_playcount_list[$slotname] / $sum_ios_jackpot_count_list[$slotname]) ?>)</td>
					<td class="tdc point"><?= number_format($sum_and_jackpot_count_list[$slotname]) ?> (<?= ($sum_and_jackpot_count_list[$slotname] == 0) ? "0" : number_format($sum_and_fiesta_playcount_list[$slotname] / $sum_and_jackpot_count_list[$slotname]) ?>)</td>
					<td class="tdc point"><?= number_format($sum_ama_jackpot_count_list[$slotname]) ?> (<?= ($sum_ama_jackpot_count_list[$slotname] == 0) ? "0" : number_format($sum_ama_fiesta_playcount_list[$slotname] / $sum_ama_jackpot_count_list[$slotname]) ?>)</td>
					
					<td class="tdc point"><?= number_format($total_web_jackpot_amount_sum[$slottype]) ?></td>
					<td class="tdc point"><?= number_format($total_ios_jackpot_amount_sum[$slottype]) ?></td>
					<td class="tdc point"><?= number_format($total_and_jackpot_amount_sum[$slottype]) ?></td>
					<td class="tdc point"><?= number_format($total_ama_jackpot_amount_sum[$slottype]) ?></td>
					
					<td class="tdc point"><?= number_format($total_web_jackpot_cnt_sum[$slottype]) ?></td>
					<td class="tdc point"><?= number_format($total_ios_jackpot_cnt_sum[$slottype]) ?></td>
					<td class="tdc point"><?= number_format($total_and_jackpot_cnt_sum[$slottype]) ?></td>
					<td class="tdc point"><?= number_format($total_ama_jackpot_cnt_sum[$slottype]) ?></td>
				</tr>	
<?
			}
			else if($viewmode == "1")
			{
				$betlevel = explode("/" , $betlevel_count_list[$slotname]["betlevel"]);
				sort($betlevel);
			
				for($j=0; $j<sizeof($betlevel)-1; $j++)
				{
					if($j == 0)
					{
?>
					<td class="tdc point" rowspan="<?= sizeof($betlevel) - 1 ?>"><?= $slotname ?></td>
<?
					}

					$betlevel_str = $total_betlist_name[$j+1];
?>
					<td class="tdc point"><?= $betlevel_str ?></td>
					
					<td class="tdc point"><?= number_format($sum_web_fiesta_playcount_list[$slotname][$betlevel_str]) ?></td>
					<td class="tdc point"><?= number_format($sum_ios_fiesta_playcount_list[$slotname][$betlevel_str]) ?></td>
					<td class="tdc point"><?= number_format($sum_and_fiesta_playcount_list[$slotname][$betlevel_str]) ?></td>
					<td class="tdc point" style="border-right: 1px dotted #dbdbdb;"><?= number_format($sum_ama_fiesta_playcount_list[$slotname][$betlevel_str]) ?></td>
					
					<td class="tdc point"><?= number_format($sum_web_jackpot_count_list[$slotname][$betlevel_str]) ?> (<?= ($sum_web_jackpot_count_list[$slotname][$betlevel_str] == 0) ? "0" : number_format($sum_web_fiesta_playcount_list[$slotname][$betlevel_str] / $sum_web_jackpot_count_list[$slotname][$betlevel_str]) ?>)</td>
					<td class="tdc point"><?= number_format($sum_ios_jackpot_count_list[$slotname][$betlevel_str]) ?> (<?= ($sum_ios_jackpot_count_list[$slotname][$betlevel_str] == 0) ? "0" : number_format($sum_ios_fiesta_playcount_list[$slotname][$betlevel_str] / $sum_ios_jackpot_count_list[$slotname][$betlevel_str]) ?>)</td>
					<td class="tdc point"><?= number_format($sum_and_jackpot_count_list[$slotname][$betlevel_str]) ?> (<?= ($sum_and_jackpot_count_list[$slotname][$betlevel_str] == 0) ? "0" : number_format($sum_and_fiesta_playcount_list[$slotname][$betlevel_str] / $sum_and_jackpot_count_list[$slotname][$betlevel_str]) ?>)</td>
					<td class="tdc point"><?= number_format($sum_ama_jackpot_count_list[$slotname][$betlevel_str]) ?> (<?= ($sum_ama_jackpot_count_list[$slotname][$betlevel_str] == 0) ? "0" : number_format($sum_ama_fiesta_playcount_list[$slotname][$betlevel_str] / $sum_ama_jackpot_count_list[$slotname][$betlevel_str]) ?>)</td>
				</tr>
<?
				}
			}
		}
	}
?>
                </tbody>
			</table>
		</form>
	</div>
<!-- CONTENTS WRAP -->     

<?
	$db_main->end();

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>