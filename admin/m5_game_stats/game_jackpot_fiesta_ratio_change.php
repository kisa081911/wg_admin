<?
	$top_menu = "game_stats";
	$sub_menu = "game_jackpot_fiesta_ratio_change";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$db_main2 = new CDatabase_Main2();
	
	$sql = "SELECT t1.slottype, t2.slotname, betlevel, fiesta_jackpot_ratio FROM tbl_slot_jackpot_ratio t1
			JOIN tbl_slot_list t2
			ON t1.slottype = t2.slottype
			ORDER BY t1.slottype ASC , betlevel ASC";	
	$jackpot_ratio_list = $db_main2->gettotallist($sql);
	
	$db_main2->end();
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $category ?> 승률 조정</title>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script>
<script type="text/javascript">

function update_change_ratio()
    {
        var input_form = document.input_form;
        var param = {};
        var total_mode_str = "";
        var change_mode_str = "";
		var ratio_val_str = "";
        
		param.total_mode = input_form.total_mode.value;
		param.change_mode = input_form.change_mode.value;
		param.ratio_val = input_form.ratio_val.value;
		param.adminid = "<?= $login_adminid?>";

		
		if(input_form.total_mode.value==0)
			total_mode_str = "전체";
		else if(input_form.total_mode.value==1)
			total_mode_str = "일반(0~9)";
		else if(input_form.total_mode.value==2)
			total_mode_str = "하이롤러(10~15)";
		
		if(input_form.change_mode.value==0)
			change_mode_str = "증가";
		else if(input_form.change_mode.value==1)
			change_mode_str = "감소";

		if(input_form.ratio_val.value == "05")
			ratio_val_str = "5";
		else
			ratio_val_str = input_form.ratio_val.value;
			
		var confirm_str="'"+total_mode_str+" "+ ratio_val_str+"% "+change_mode_str+"' 적용 하시겠습니까?";
		
		var cfm=confirm(confirm_str);
		
		if(cfm)
		{
	        WG_ajax_execute("game/fiesta_change_ratio", param, update_change_ratio_callback, false);      
		}

    }
    
    function update_change_ratio_callback(result, reason)
    {
        var input_form = document.input_form; 
        
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            alert("승률 변경 완료했습니다.");
            window.location.reload();
        }
    }
    </script>
    </head>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 잭팟 피에스타 확률 조정</div>
	</div>
	
	<div class="search_box">
	<form name="input_form" id="input_form" onsubmit="return false">
		<select name="total_mode" id="total_mode" style="margin-top: 5px;">										
			<option value="0" selected>전체</option>
			<option value="1" >일반(0~9)</option>                       
			<option value="2" >하이롤러(10~15)</option>					
		</select>&nbsp;
		
		<select name="change_mode" id="change_mode" style="margin-top: 5px;">										
			<option value="0" >Up</option>
			<option value="1" >Down</option>                       
		</select>&nbsp;
		
		<select name="ratio_val" id="ratio_val" style="margin-top: 5px;">										
			<option value="05" >5%</option>
			<option value="10" >10%</option>                       
			<option value="15" >15%</option>                       
			<option value="20" >20%</option>                       
			<option value="30" >30%</option>                       
		</select>&nbsp;
		                    
		<input type="button" class="btn_search" value="적용" onclick="update_change_ratio()" >
	</form>
	</div>
	<br><br>
	<br>
	
	<table class="tbl_list_basic1" style="margin-top:5px">
		<colgroup>
			<col width="">   
			<col width="">			
		</colgroup>
		<thead>
			<tr>
				<th class="tdc">슬롯종류(slottype)</th>
				<th class="tdc">BetLevel</th>				
				<th class="tdc">Fiesta Ratio</th>				
			</tr>
		</thead>
		<tbody>
<?
	for($i=0; $i<sizeof($jackpot_ratio_list); $i++)
    {
    	$slottype = $jackpot_ratio_list[$i]["slottype"];
    	$slotname = $jackpot_ratio_list[$i]["slotname"];
    	$betlevel = $jackpot_ratio_list[$i]["betlevel"];
    	$fiesta_jackpot_ratio = $jackpot_ratio_list[$i]["fiesta_jackpot_ratio"];
    	
?>
			<tr>
				<td class="tdc" <?= $style ?>><?= $slotname."(".$slottype.")" ?></td>
				<td class="tdc" <?= $style ?>><?= $betlevel ?></td>
				<td class="tdc" <?= $style ?>><?= number_format($fiesta_jackpot_ratio) ?></td>
			</tr>
<?
    }
?>
		</tbody>
	</table>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
