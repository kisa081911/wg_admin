<?
	$top_menu = "game_stats";
	$sub_menu = "game_slot_empty_seat";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$db_main2 = new CDatabase_Main2();
	$db_game = new CDatabase_Game();
	
	$sql = "SELECT slottype, slotname FROM tbl_slot_list";
	$slottype_list = $db_main2->gettotallist($sql);	
	
	$sql = "SELECT slottype, SUM(5-currentuser) AS seat_cnt ".
			"FROM tbl_slot_object ".
			"WHERE enable = 1 ".
			"GROUP BY slottype ".
			"ORDER BY seat_cnt ASC ";	
	$empty_seat_list = $db_game->gettotallist($sql);
	
	$db_main2->end();
	$db_game->end();
?>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 슬롯 빈자리 현황</div>
	</div>
	<!-- //title_warp -->	
	<table class="tbl_list_basic1" style="margin-top:5px">
		<colgroup>
			<col width="">   
			<col width="">			
		</colgroup>
		<thead>
			<tr>
				<th class="tdc">슬롯종류</th>
				<th class="tdc">빈자리 현황</th>				
			</tr>
		</thead>
		<tbody>
<?
	for($i=0; $i<sizeof($empty_seat_list); $i++)
    {
    	$slottype = $empty_seat_list[$i]["slottype"];
    	$seat_cnt = $empty_seat_list[$i]["seat_cnt"];
    	
    	if ($seat_cnt <= "50")
    		$style = "style='color:red;'";
    	else
    		$style = "";
    	
    	for($j=0; $j<sizeof($slottype_list); $j++)
    	{
    		if($slottype_list[$j]["slottype"] == $slottype)
    		{
    			$slot_name = $slottype_list[$j]["slotname"];
					break;
    		}
    		else
    		{
    			$slot_name = "Unkown";
    		}
    	}
?>
			<tr>
				<td class="tdc" <?= $style ?>><?= $slot_name ?></td>
				<td class="tdc" <?= $style ?>><?= number_format($seat_cnt) ?></td>
			</tr>
<?
    }
?>
		</tbody>
	</table>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
