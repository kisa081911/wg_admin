<?
    $top_menu = "game_stats";
    $sub_menu = "game_coupon_stat_graph";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $pagename = "game_coupon_stat_graph.php";
    $platform = $_GET["platform"];
    $isrecentbuy = $_GET["isrecentbuy"];
    $search_member_level = $_GET["search_member_level"];
    
    $start_date = $_GET["startdate"];
    $end_date = $_GET["enddate"];
    
    $today = date("Y-m-d");
    $before_day = get_past_date($today,15,"d");
    $start_date = ($start_date == "") ? $before_day : $start_date;
    $end_date = ($end_date == "") ? $today : $end_date;
    
    $db_other = new CDatabase_Other();
    
    $tail = "WHERE today BETWEEN '$start_date' AND '$end_date' AND member_level NOT IN (0, 1, 2, 3) ";
    $group_by = "GROUP BY today";
    
    if($platform != "")
    	$tail .= "AND platform = $platform ";
    
	if($isrecentbuy != "")
		$tail .= "AND isrecentbuy = $isrecentbuy ";
    
	if($search_member_level != "")
	{
		$group_by .= ", member_level";
		$order_by .= "ORDER BY member_level ASC, today ASC";
	}
    
    $sql = "SELECT today, member_level, (SUM(total_coupon) + SUM(use_coupon)) AS all_coupon, ".
    		"SUM(total_coupon) AS total_coupon, SUM(use_coupon) AS use_coupon, SUM(issue_coupon) AS issue_coupon, SUM(expire_coupon) AS expire_coupon ".
			"FROM tbl_coupon_stat_daily $tail $group_by $order_by";
    
    $couponcount_list = $db_other->gettotallist($sql);
    
    $db_other->end();
    
    $today_list = array();
    $all_coupon_runout_rate = array();
    $total_coupon_list = array();
    $use_coupon_list = array();
    $issue_coupon_list = array();
    $expire_coupon_list = array();
    
    $couponcount_list_pointer = 0;
    
    if($search_member_level == "")
    {
	    $date_pointer = $start_date;
	    
	    $loop_count = get_diff_date($end_date, $start_date, "d") + 1;
	    
	    for ($i=0; $i < $loop_count; $i++)
	    {
	    	if ($couponcount_list_pointer < sizeof($couponcount_list))
	    		$today = $couponcount_list[$couponcount_list_pointer]["today"];
	    		
	    	if(get_diff_date($date_pointer, $today, "d") == 0)
	    	{
		    	$today_list[$i] = $couponcount_list[$couponcount_list_pointer]["today"];
		    	$total_coupon_list[$i] = $couponcount_list[$couponcount_list_pointer]["total_coupon"];
		    	$use_coupon_list[$i] = $couponcount_list[$couponcount_list_pointer]["use_coupon"];
		    	$issue_coupon_list[$i] = $couponcount_list[$couponcount_list_pointer]["issue_coupon"];
		    	$expire_coupon_list[$i] = $couponcount_list[$couponcount_list_pointer]["expire_coupon"];
		    	$all_coupon_runout_rate[$i] = ($use_coupon_list[$i] / $total_coupon_list[$i]) * 100;
		    	
		    	$couponcount_list_pointer++;
	    	}
	    	else 
	    	{
	    		$today_list[$i] = $date_pointer;
	    		$all_coupon_runout_rate[$i] = 0;
	    		$total_coupon_list[$i] = 0;
	    		$use_coupon_list[$i] = 0;
	    		$issue_coupon_list[$i] = 0;
	    		$expire_coupon_list[$i] = 0;
	    	}
	    	
	    	$date_pointer = date("Y-m-d", strtotime($date_pointer."+1 days"));
	    }
    }
    else if($search_member_level != "")
    {    	    	
    	for ($i=0; $i < 9; $i++)
    	{
    		$date_pointer = $start_date;
    		 
    		$loop_count = get_diff_date($end_date, $start_date, "d") + 1;
    		
    		for ($j=0; $j < $loop_count; $j++)
    		{
    			if ($couponcount_list_pointer < sizeof($couponcount_list))
    			{
    				$today = $couponcount_list[$couponcount_list_pointer]["today"];
    			}
    				 
    			if((get_diff_date($date_pointer, $today, "d") == 0) && ($couponcount_list[$couponcount_list_pointer]["member_level"] == ($i + 2)))
    			{
    				$today_list[$i][$j] = $couponcount_list[$couponcount_list_pointer]["today"];
    				$total_coupon_list[$i][$j] = $couponcount_list[$couponcount_list_pointer]["total_coupon"];
    				$use_coupon_list[$i][$j] = $couponcount_list[$couponcount_list_pointer]["use_coupon"];
    				$issue_coupon_list[$i][$j] = $couponcount_list[$couponcount_list_pointer]["issue_coupon"];
    				$expire_coupon_list[$i][$j] = $couponcount_list[$couponcount_list_pointer]["expire_coupon"];
    				$all_coupon_runout_rate[$i][$j] = ($use_coupon_list[$i][$j] / $total_coupon_list[$i][$j]) * 100;
    				
    				$couponcount_list_pointer++;
    			}
    			else
    			{
    				$today_list[$i][$j] = $date_pointer;
    				$all_coupon_runout_rate[$i][$j] = 0;
    				$total_coupon_list[$i][$j] = 0;
    				$use_coupon_list[$i][$j] = 0;
    				$issue_coupon_list[$i][$j] = 0;
    				$expire_coupon_list[$i][$j] = 0;
    			}
    		
    			$date_pointer = date("Y-m-d", strtotime($date_pointer."+1 days"));
    		}
    	}
    }
?>

<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});

	function drawChart()
	{
		 var options = {
	    	        title:'',                                                      
	    	        width:1050,                         
	    	        height:200,
	    	        axisTitlesPosition:'in',
	    	        curveType:'none',
	    	    	focusTarget:'category',
	    	        interpolateNulls:'true',
	    	        legend:'top',
	    	        fontSize : 12,
	    	        chartArea:{left:80,top:40,width:1020,height:130}
	    	};
<?
	    if($search_member_level == "")
		{
?>
	    	var data1 = new google.visualization.DataTable();

			data1.addColumn('string', '날짜');
	        data1.addColumn('number', '잔여 쿠폰');
		    data1.addColumn('number', '사용한 쿠폰');      
		    data1.addColumn('number', '발행한 쿠폰');      
		    data1.addColumn('number', '만료된 쿠폰');
		    
	    	data1.addRows([
<?
				for($i=0; $i < sizeof($today_list); $i++)
				{
					$_today = $today_list[$i];
					$_total_coupon = $total_coupon_list[$i];
					$_use_coupon = $use_coupon_list[$i];
					$_issue_coupon = $issue_coupon_list[$i];
					$_expire_coupon = $expire_coupon_list[$i];
					
					if($i == sizeof($today_list) - 1)
						echo("['".$_today."', {v:".$_total_coupon.",f:'".make_price_format($_total_coupon)."'}, {v:".$_use_coupon.",f:'".make_price_format($_use_coupon)."'}, {v:".$_issue_coupon.",f:'".make_price_format($_issue_coupon)."'}, {v:".$_expire_coupon.",f:'".make_price_format($_expire_coupon)."'}]");
					else
						echo("['".$_today."', {v:".$_total_coupon.",f:'".make_price_format($_total_coupon)."'}, {v:".$_use_coupon.",f:'".make_price_format($_use_coupon)."'}, {v:".$_issue_coupon.",f:'".make_price_format($_issue_coupon)."'}, {v:".$_expire_coupon.",f:'".make_price_format($_expire_coupon)."'}], ");
				}
?>
			]);

	    	var data2 = new google.visualization.DataTable();

	    	data2.addColumn('string', '날짜');
	        data2.addColumn('number', '쿠폰 소진율(%)');
	    	
	    	data2.addRows([
<?
				for($i=0; $i < sizeof($today_list); $i++)
				{
					$_today = $today_list[$i];
					$_all_coupon = $all_coupon_runout_rate[$i];
					
					if($i == sizeof($today_list) - 1)
						echo("['".$_today."', ".round($_all_coupon, 2)."]");
					else
						echo("['".$_today."', ".round($_all_coupon, 2)."],");
				}
?>
			]);
			
	        var chart1 = new google.visualization.LineChart(document.getElementById('chart_data1'));
	        var chart2 = new google.visualization.LineChart(document.getElementById('chart_data2'));
	        
	        chart1.draw(data1, options);
	        chart2.draw(data2, options);
<?
		}
	    else if($search_member_level != "")
	    {
	    	for($i=2; $i<9; $i++)
	    	{
	    		$data_index = $i + 1;
?>
		    	var data<?= $data_index ?>_1 = new google.visualization.DataTable();
		    	var data<?= $data_index ?>_2 = new google.visualization.DataTable();
		    	
				data<?= $data_index ?>_1.addColumn('string', '날짜');
				data<?= $data_index ?>_1.addColumn('number', '잔여 쿠폰');
				data<?= $data_index ?>_1.addColumn('number', '사용한 쿠폰');      
				data<?= $data_index ?>_1.addColumn('number', '발행한 쿠폰');      
				data<?= $data_index ?>_1.addColumn('number', '만료된 쿠폰');

				data<?= $data_index ?>_2.addColumn('string', '날짜');
		        data<?= $data_index ?>_2.addColumn('number', '쿠폰 소진율(%)');
				
				data<?= $data_index ?>_1.addRows([
<?
				for($j=0; $j < sizeof($today_list[$i]); $j++)
				{
					$_today = $today_list[$i][$j];
					$_total_coupon = $total_coupon_list[$i][$j];
					$_use_coupon = $use_coupon_list[$i][$j];
					$_issue_coupon = $issue_coupon_list[$i][$j];
					$_expire_coupon = $expire_coupon_list[$i][$j];
						
					if($j == sizeof($today_list[$i]) - 1)
						echo("['".$_today."', {v:".$_total_coupon.",f:'".make_price_format($_total_coupon)."'}, {v:".$_use_coupon.",f:'".make_price_format($_use_coupon)."'}, {v:".$_issue_coupon.",f:'".make_price_format($_issue_coupon)."'}, {v:".$_expire_coupon.",f:'".make_price_format($_expire_coupon)."'}]");
					else
						echo("['".$_today."', {v:".$_total_coupon.",f:'".make_price_format($_total_coupon)."'}, {v:".$_use_coupon.",f:'".make_price_format($_use_coupon)."'}, {v:".$_issue_coupon.",f:'".make_price_format($_issue_coupon)."'}, {v:".$_expire_coupon.",f:'".make_price_format($_expire_coupon)."'}], ");
				}
?>
				]);

		        data<?= $data_index ?>_2.addRows([
<?
					for($j=0; $j < sizeof($today_list[$i]); $j++)
					{
						$_today = $today_list[$i][$j];
						$_all_coupon = $all_coupon_runout_rate[$i][$j];
						
						if($j == sizeof($today_list[$i]) - 1)
							echo("['".$_today."', ".round($_all_coupon, 2)."]");
						else
							echo("['".$_today."', ".round($_all_coupon, 2)."],");
							
					}
?>
				]);
		    	
		    	var chart<?= $data_index ?>_1 = new google.visualization.LineChart(document.getElementById('chart_data<?= $data_index ?>_1'));
		        var chart<?= $data_index ?>_2 = new google.visualization.LineChart(document.getElementById('chart_data<?= $data_index ?>_2'));
		        
		        chart<?= $data_index ?>_1.draw(data<?= $data_index ?>_1, options);
		    	chart<?= $data_index ?>_2.draw(data<?= $data_index ?>_2, options);
<?
	    	}
		}
?>
	}
	
	google.setOnLoadCallback(drawChart);
	
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    function search_press(e)
	  {
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	  }
    
    $(function() {
        $("#startdate").datepicker({ });
    });

    $(function() {
        $("#enddate").datepicker({ });
    });
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
                <!-- title_warp -->
                <div class="title_wrap">
                    <div class="title"><?= $top_menu_txt ?> &gt; 쿠폰 현황</div>
                </div>
                <!-- //title_warp -->
                
                <div class="detail_search_wrap">
					<!-- <span class="search_lbl">플랫폼</span>
					<select name="platform" id="platform">
						<option value="" <?= ($platform == "") ? "selected" : "" ?>>전체</option>
						<option value="0" <?= ($platform == "0") ? "selected" : "" ?>>Web</option>
						<option value="1" <?= ($platform == "1") ? "selected" : "" ?>>iOS</option>
						<option value="2" <?= ($platform == "2") ? "selected" : "" ?>>Android</option>
						<option value="3" <?= ($platform == "3") ? "selected" : "" ?>>Amazon</option>
					</select>  -->
			
					&nbsp;
					&nbsp;
					
					<span class="search_lbl">최근 결제 여부</span>
					<select name="isrecentbuy" id="isrecentbuy">
						<option value="" <?= ($isrecentbuy == "") ? "selected" : "" ?>>전체</option>
						<option value="1" <?= ($isrecentbuy == "1") ? "selected" : "" ?>>28일이내 결제</option>
					</select>
					
					&nbsp;
					&nbsp;
					
					<span class="search_lbl">등급별</span>
					<select name="search_member_level" id="search_member_level">
						<option value="" <?= ($search_member_level == "") ? "selected" : "" ?>>전체</option>
						<option value="1" <?= ($search_member_level == "1") ? "selected" : "" ?>>등급별</option>
					</select>
					
					&nbsp;
					&nbsp;
					
					<span class="search_lbl">기간</span>
					<input type="text" class="search_text" id="startdate" name="startdate" style="width:65px" value="<?= $start_date ?>" 
						onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
		            <input type="text" class="search_text" id="enddate" name="enddate" style="width:65px" value="<?= $end_date ?>" 
		            	onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
					
					<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
				</div>
				
<?
	if($search_member_level == "")
	{
?>	
				<div class="h2_title">[쿠폰수]</div>
				<div id="chart_data1" style="height:230px; min-width: 500px"></div>
				<div class="h2_title">[쿠폰 소진율]</div>
				<div id="chart_data2" style="height:230px; min-width: 500px"></div>
<?
	}
	else if($search_member_level != "")
	{
		for($i=3; $i<10; $i++)
		{
?>			
				<div class="h2_title">[VIP Level <?= $i + 1 ?>]</div>
				<div id="chart_data<?= $i ?>_1" style="height:230px; min-width: 500px"></div>
				<div id="chart_data<?= $i ?>_2" style="height:230px; min-width: 500px"></div>
<?
		}
	}
?>
        	</form>
        </div>
<!--  //CONTENTS WRAP -->

<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");    
?>
 