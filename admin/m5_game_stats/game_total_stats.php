<?
    $top_menu = "game_stats";
    $sub_menu = "game_total_stats";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
   	$db_analysis = new CDatabase_Analysis();
   	
   	$os_type = ($_GET["os_type"] == "") ? "ALL" :$_GET["os_type"];
   	$total_mode = ($_GET["total_mode"] == "") ? "0" :$_GET["total_mode"];   	
   	$pagename = "game_total_stats.php";
    
    $sql = "SELECT slottype, slotname FROM tbl_slot_list";
    $slottype_list = $db_main2->gettotallist($sql);    
    
    $today = date("Y-m-d");
    $yesterday = date("Y-m-d",strtotime("-1 days"));
    $tomorrow = date("Y-m-d",strtotime("+1 days"));
    
    $std_useridx = 20000;
    
    if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
    {
    	$port = ":8081";
    	$std_useridx = 10000;
    }
    
    if($os_type == "0")
    {
    	$platform_name = "WEB";
    	$platform_table_today = "tbl_game_cash_stats_daily2 t1 WHERE mode IN (0,3) AND writedate = '$today' ";
    	$platform_table_yesterday = "tbl_game_cash_stats_daily2 t1 WHERE mode IN (0,3) AND writedate = '$yesterday' ";
    	$online_game_log_daily_today = "user_online_game_log_daily WHERE 1=1 AND mode IN (0,3) AND today = '$today'";
    	$online_game_log_daily_yesterday = "user_online_game_log_daily WHERE 1=1 AND mode IN (0,3) AND today = '$yesterday'";
    }
    else if($os_type == "1")
    {
    	$platform_name = "IOS";
    	$platform_table_today = "tbl_game_cash_stats_ios_daily2 t1 WHERE mode IN (0,3) AND writedate = '$today'";
    	$platform_table_yesterday = "tbl_game_cash_stats_ios_daily2 t1 WHERE mode IN (0,3) AND writedate = '$yesterday'";
    	$online_game_log_daily_today = "user_online_game_mobile_log_daily WHERE os_type=1  AND mode IN (0,3) AND today = '$today'";
    	$online_game_log_daily_yesterday = "user_online_game_mobile_log_daily WHERE os_type=1  AND mode IN (0,3) AND today = '$yesterday'";
    }
    else if($os_type == "2")
	{		
    	$platform_name = "Android";
    	$platform_table_today = "tbl_game_cash_stats_android_daily2 t1 WHERE mode IN (0,3) AND writedate = '$today'";
    	$platform_table_yesterday = "tbl_game_cash_stats_android_daily2 t1 WHERE mode IN (0,3) AND writedate = '$yesterday'";
    	$online_game_log_daily_today = "user_online_game_mobile_log_daily WHERE os_type=2   AND mode IN (0,3) AND today = '$today'";
    	$online_game_log_daily_yesterday = "user_online_game_mobile_log_daily WHERE os_type=2   AND mode IN (0,3) AND today = '$yesterday'";
	}
    else if($os_type == "3")
    {
    	$platform_name = "Amazon";
    	$platform_table_today = "tbl_game_cash_stats_amazon_daily2 t1 WHERE mode IN (0,3) AND writedate = '$today'";
    	$platform_table_yesterday = "tbl_game_cash_stats_amazon_daily2 t1 WHERE mode IN (0,3) AND writedate = '$yesterday'";
    	$online_game_log_daily_today = "user_online_game_mobile_log_daily WHERE os_type=3   AND mode IN (0,3) AND today = '$today'";
    	$online_game_log_daily_yesterday = "user_online_game_mobile_log_daily WHERE os_type=3   AND mode IN (0,3) AND today = '$yesterday'";
    }
    else if($os_type == "ALL")
    {
    	$platform_name = "Total";
    	$platform_table_today = "( ".
        	"	SELECT * FROM tbl_game_cash_stats_daily2 WHERE mode IN (0,3) AND writedate = '$today'".
        	"	UNION ALL	".
        	"	SELECT * FROM tbl_game_cash_stats_ios_daily2 WHERE mode IN (0,3) AND writedate = '$today'".
        	"	UNION ALL	".
        	"	SELECT * FROM tbl_game_cash_stats_android_daily2 WHERE mode IN (0,3) AND writedate = '$today'".
        	"	UNION ALL	".
        	"	SELECT * FROM tbl_game_cash_stats_amazon_daily2 WHERE mode IN (0,3) AND writedate = '$today'".
        	") t1 WHERE 1=1";
    	
    	$platform_table_yesterday = "( ".
        	"	SELECT * FROM tbl_game_cash_stats_daily2 WHERE mode IN (0,3) AND writedate = '$yesterday' ".
        	"	UNION ALL	".
        	"	SELECT * FROM tbl_game_cash_stats_ios_daily2 WHERE mode IN (0,3) AND writedate = '$yesterday'".
        	"	UNION ALL	".
        	"	SELECT * FROM tbl_game_cash_stats_android_daily2 WHERE mode IN (0,3) AND writedate = '$yesterday'".
        	"	UNION ALL	".
        	"	SELECT * FROM tbl_game_cash_stats_amazon_daily2 WHERE mode IN (0,3) AND writedate = '$yesterday'".
        	") t1 WHERE 1=1";
    	
    	$online_game_log_daily_today = "( ".
        	" SELECT today,totalcount,gameslottype FROM user_online_game_log_daily WHERE  mode IN (0,3) AND today = '$today'".
        	" UNION ALL ".
        	" SELECT today,totalcount,gameslottype FROM user_online_game_mobile_log_daily WHERE mode IN (0,3) AND today = '$today'".
        	") t1 WHERE 1=1 ";
    	$online_game_log_daily_yesterday = "( ".
        	" SELECT today,totalcount,gameslottype FROM user_online_game_log_daily WHERE mode IN (0,3) AND today = '$yesterday'".
        	" UNION ALL ".
        	" SELECT today,totalcount,gameslottype FROM user_online_game_mobile_log_daily WHERE mode IN (0,3) AND today = '$yesterday'".
        	") t1 WHERE 1=1";
    }
    
    $bet_name = "";
    $bet_tail = "";
    if($total_mode == 0)
    	$mode_name = "전체";
    else if($total_mode == 1)
    {
    	$mode_name = "레귤러";
        $bet_tail = "AND t1.betlevel BETWEEN 0 AND 9 ";        
    }
    else if($total_mode == 2)
    {
    	$mode_name = "하이롤러(싱글 X)";
    	$bet_tail = "AND t1.betlevel BETWEEN 10 AND 9 ";
    }
    else if($total_mode == 3)
    {
    	$mode_name = "하이롤러(싱글포함)";
    	$bet_tail = "AND t1.betlevel BETWEEN 10 AND 119 ";
    }
    else if($total_mode == 4)
    {
    	$mode_name = "싱글모드";
    	$bet_tail = "AND t1.betlevel BETWEEN 110 AND 119 ";
        
    }
?>
<!-- CONTENTS WRAP -->
<script type="text/javascript">
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    
	function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		var all = document.getElementById("type_ALL");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			all.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			all.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
			all.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
			all.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			all.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
	
		search_form.submit();
	}
</script>

<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">	
	        
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; <?=$platform_name?> 게임 접속/활동(<?=$mode_name?>)</div>		
		<!-- //title_warp -->		
			<div class="search_box">
				<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />
        		
        		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
					<input type="button" class="<?= ($os_type == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" value=ALL id="type_ALL" onclick="change_os_type('ALL')"    />
					<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
					<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
					<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
					<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
				</span>
				Mode :&nbsp;
				<select name="total_mode" id="total_mode">										
					<option value="0" <?= ($total_mode=="0") ? "selected" : "" ?>>전체</option>
					<option value="1" <?= ($total_mode=="1") ? "selected" : "" ?>>레귤러</option>                       
					<option value="2" <?= ($total_mode=="2") ? "selected" : "" ?>>하이롤러(싱글X)</option>
					<option value="3" <?= ($total_mode=="3") ? "selected" : "" ?>>하이롤러(싱글포함)</option>
					<option value="4" <?= ($total_mode=="4") ? "selected" : "" ?>>싱글모드</option>
				</select>&nbsp;&nbsp;&nbsp;				
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</div>
				
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">슬롯 활동 (<?= $yesterday ?>)</span>
		<table class="tbl_list_basic1" style="margin-top:5px">
			<colgroup>
				<col width="">   
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">            
			</colgroup>
			<thead>
				<tr>
					<th class="tdl">슬롯종류</th>
					<th class="tdr">접속자수</th>
					<th class="tdr">플레이수</th>
					<th class="tdr">money in</th>
					<th class="tdr">money out</th>
					<th class="tdr">jackpot</th>
					<th class="tdr">Fameball</th>
					<th class="tdr">평균 Fameball</th>
					<th class="tdc">승률</th>
					<th class="tdc">단위승률</th>
				</tr>
			</thead>
			<tbody>
<?
    	$sql = "SELECT slottype, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout),0) AS moneyout, IFNULL(SUM(playcount), 0) AS playcount ".
            	"FROM $platform_table_yesterday ".
            	"		$bet_tail ".
				"GROUP BY slottype ORDER BY moneyin DESC LIMIT 20;";
    	
    	
    	// unit 승률
    	$yesterday_unit_rate_sql = "SELECT slottype, ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2) AS unit_rate ".
    			"FROM	".
    			"	(	".
    			"		SELECT writedate, t1.slottype, t1.betlevel, SUM(moneyin), SUM(moneyout), SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout	".
    			"		FROM $platform_table_yesterday  ".
    			"		$bet_tail".
    			"		GROUP BY slottype, betlevel".
    			") t3 GROUP BY slottype";
    		
    	$yesterday_unit_rate_list = $db_main2->gettotallist($yesterday_unit_rate_sql);
    	
    	$yesterday_unit_rate_array = array();
    		
    	for($e = 0; $e < sizeof($yesterday_unit_rate_list); $e++)
    	{
    		$slottype = $yesterday_unit_rate_list[$e]["slottype"];
    		$unit_rate = $yesterday_unit_rate_list[$e]["unit_rate"];
			$yesterday_unit_rate_array[$slottype] = $unit_rate;
    		
    	}
	    
	    $slot_list = $db_main2->gettotallist($sql);
	
	    for($i=0; $i<sizeof($slot_list); $i++)
	    {
	        $slottype = $slot_list[$i]["slottype"];
	        $moneyin = $slot_list[$i]["moneyin"];
	        $moneyout = $slot_list[$i]["moneyout"];
	        $playcount = $slot_list[$i]["playcount"];
	        
	        $devicetype_tail="";
	        $platform_str="";
	        $os_type_str="";
	        
	        if($os_type != "ALL")
	        {
	            $devicetype_tail="AND devicetype=$os_type";
	            $os_type_str = "AND os_type = $os_type";
	            $platform_str = "AND platform = $os_type ";
	        }
	        
	        if($total_mode=="0")
	        {
	        	$sql = "SELECT IFNULL(SUM(amount),0) FROM tbl_jackpot_stat_daily WHERE today='$yesterday' $devicetype_tail AND slottype=$slottype";
	        	$jackpot = $db_analysis->getvalue($sql);
	        	
	        	$sql = "SELECT IFNULL(SUM(totalcount),0) FROM $online_game_log_daily_yesterday AND gameslottype=$slottype;";
	        	$online_count = $db_analysis->getvalue($sql);	        	
	        	
	        	$sql = "SELECT fameball FROM tbl_fameball_daily WHERE slottype=$slottype AND today = '$yesterday' $os_type_str";
	        	$fameball_count = $db_analysis->getvalue($sql);
	        }
	        else if($total_mode=="1")
	        {
	        	$sql = "SELECT IFNULL(SUM(amount),0) FROM tbl_jackpot_stat_daily WHERE bettype = 0 AND today='$yesterday' $devicetype_tail AND slottype=$slottype";
	        	$jackpot = $db_analysis->getvalue($sql);	        	
	        	
	        	$sql = "SELECT IFNULL(SUM(totalcount),0) FROM $online_game_log_daily_yesterday AND gameslottype=$slottype;";
	        	$online_count = $db_analysis->getvalue($sql);
	        	
	        	$sql = "SELECT fameball FROM tbl_fameball_daily WHERE bettype = 0 $os_type_str  AND slottype=$slottype AND today = '$yesterday'";
	        	$fameball_count = $db_analysis->getvalue($sql);
	        }
	        else if($total_mode=="2" || $total_mode=="3" || $total_mode=="4")
	        {
	        	$sql = "SELECT IFNULL(SUM(amount),0) FROM tbl_jackpot_stat_daily WHERE bettype = 1 AND today='$yesterday' $devicetype_tail AND slottype=$slottype";
	        	$jackpot = $db_analysis->getvalue($sql);
	        	
	        	$sql = "SELECT IFNULL(SUM(totalcount),0) FROM $online_game_log_daily_yesterday AND gameslottype=$slottype;";
	        	$online_count = $db_analysis->getvalue($sql);	        	
	        	
	        	$sql = "SELECT fameball FROM tbl_fameball_daily WHERE bettype = 1 $os_type_str  AND slottype=$slottype AND today = '$yesterday'";
	        	$fameball_count = $db_analysis->getvalue($sql);
	        }	        
	        
	    	if($slottype == 1)
	        {
	        	$sql = "SELECT COUNT(*)*5 AS tutorial_fameball ".
	        			"FROM tbl_user_ext t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx ".
	        			"WHERE t1.useridx > $std_useridx AND tutorial >= 4 $platform_str ".
	        			"AND createdate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59'";
	        	$tutorial_fameball_count = $db_main->getvalue($sql);

	        	if($total_mode=="2")
	        		$tutorial_fameball_count = 0;
	        
	        	$fameball_count = $fameball_count - $tutorial_fameball_count;
	        }
	        
	        $fameball_per_playcount = round($fameball_count / $playcount, 2);
	        
	        $rate = round(($moneyout) / $moneyin * 10000) / 100;
	        
	        if ($rate <= "90" || $rate >= "100" || $jackpot*100/$moneyin > 2)
	            $style = "style='color:red;'";
	        else
	            $style = "";
			
	    	for($j=0; $j<sizeof($slottype_list); $j++)
			{
				if($slottype_list[$j]["slottype"] == $slottype)
				{
					$slot_name = $slottype_list[$j]["slotname"];
					break;
				}
				else
				{
					$slot_name = "Unkown";
				}
			}
?>
				<tr>
					<td class="tdl" <?= $style ?>><?= $slot_name ?></td>
					<td class="tdr" <?= $style ?>><?= number_format($online_count) ?></td>
					<td class="tdr" <?= $style ?>><?= number_format($playcount) ?></td>				
					<td class="tdr" <?= $style ?>><?= number_format($moneyin) ?></td>
					<td class="tdr" <?= $style ?>><?= number_format($moneyout) ?></td>
					<td class="tdr" <?= $style ?>><?= number_format($jackpot) ?></td>
					<td class="tdr" <?= $style ?>><?= number_format($fameball_count) ?></td>
					<td class="tdr" <?= $style ?>><?= $fameball_per_playcount ?></td>
					<td class="tdc" <?= $style ?>><?= $rate."%" ?></td>
					<td class="tdc" <?= $style ?>><?= $yesterday_unit_rate_array[$slottype]."%" ?></td>
				</tr>
<?
	    }
?>
			</tbody>
		</table>
	            
		<div class="clear"></div>
		<br/>
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">슬롯 활동 (<?= $today ?>)</span>
		<table class="tbl_list_basic1" style="margin-top:5px">
			<colgroup>
				<col width="">   
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">            
			</colgroup>
			<thead>
				<tr>
					<th class="tdl">슬롯종류</th>
					<th class="tdr">접속자수</th>
					<th class="tdr">플레이수</th>
					<th class="tdr">money in</th>
					<th class="tdr">money out</th>
					<th class="tdr">jackpot</th>
					<th class="tdr">Fameball</th>
					<th class="tdr">평균 Fameball</th>
					<th class="tdc">승률</th>
					<th class="tdc">단위승률</th>
				</tr>
			</thead>
<?  
    	$sql = "SELECT slottype, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout),0) AS moneyout, IFNULL(SUM(playcount), 0) AS playcount ".
    			"FROM $platform_table_today ".
    			"	  $bet_tail ".
    			"GROUP BY slottype ORDER BY moneyin DESC LIMIT 20;";
		 
		 // unit 승률
		 $today_unit_rate_sql = "SELECT slottype, ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2) AS unit_rate ".
				    		 		"FROM	".
				    		 		"	(	".
				    		 		"		SELECT writedate, t1.slottype, t1.betlevel, SUM(unit_moneyin) AS unit_moneyin, SUM(unit_moneyout) AS unit_moneyout	".
				    		 		"		FROM $platform_table_today  ".
				    		 		"		$bet_tail".
				    		 		"		GROUP BY slottype, betlevel".
				    		 		") t3 GROUP BY slottype";
				    		 
		 $today_unit_rate_list = $db_main2->gettotallist($today_unit_rate_sql);
		 	
		 $today_unit_rate_array = array();
		 
		 for($e = 0; $e < sizeof($today_unit_rate_list); $e++)
		 {
		 	$slottype = $today_unit_rate_list[$e]["slottype"];
		 	$unit_rate = $today_unit_rate_list[$e]["unit_rate"];
 			$today_unit_rate_array[$slottype] = $unit_rate;
		 }
	    
	    $slot_list = $db_main2->gettotallist($sql);
	
	    for($i=0; $i<sizeof($slot_list); $i++)
	    {
	        $slottype = $slot_list[$i]["slottype"];
	        $moneyin = $slot_list[$i]["moneyin"];
	        $moneyout = $slot_list[$i]["moneyout"];
	        $playcount = $slot_list[$i]["playcount"];
	        
	        $devicetype_tail="";
	        $platform_str="";
	        $os_type_str="";
	        
	        if($os_type != "ALL")
	        {
	            $devicetype_tail="AND devicetype=$os_type";
	            $os_type_str = "AND os_type = $os_type";
	            $platform_str = "AND platform = $os_type ";
	        }
			
	    	if($total_mode=="0")
	        {
	        	$sql = "SELECT IFNULL(SUM(amount),0) FROM tbl_jackpot_stat_daily WHERE today='$today' $devicetype_tail AND slottype=$slottype";
	        	$jackpot = $db_analysis->getvalue($sql);
	        	
	        	$sql = "SELECT IFNULL(SUM(totalcount),0) FROM $online_game_log_daily_today AND gameslottype=$slottype;";
	        	$online_count = $db_analysis->getvalue($sql);	        	
	        	
	        	$sql = "SELECT fameball FROM tbl_fameball_daily WHERE  slottype=$slottype AND today = '$today' $os_type_str ";
	        	$fameball_count = $db_analysis->getvalue($sql);
	        }
	        else if($total_mode=="1")
	        {
	        	$sql = "SELECT IFNULL(SUM(amount),0) FROM tbl_jackpot_stat_daily WHERE bettype = 0 AND today='$today' $devicetype_tail AND slottype=$slottype";
	        	$jackpot = $db_analysis->getvalue($sql);	        	
	        	
	        	$sql = "SELECT IFNULL(SUM(totalcount),0) FROM $online_game_log_daily_today AND gameslottype=$slottype;";
	        	$online_count = $db_analysis->getvalue($sql);
	        	
	        	$sql = "SELECT fameball FROM tbl_fameball_daily WHERE bettype = 0 $os_type_str  AND slottype=$slottype AND today = '$today'";
	        	$fameball_count = $db_analysis->getvalue($sql);
	        }
	        else if($total_mode=="2" || $total_mode=="3" || $total_mode=="4")
	        {
	        	$sql = "SELECT IFNULL(SUM(amount),0) FROM tbl_jackpot_stat_daily WHERE bettype = 1 AND today='$today' $devicetype_tail AND slottype=$slottype";
	        	$jackpot = $db_analysis->getvalue($sql);
	        	
	        	$sql = "SELECT IFNULL(SUM(totalcount),0) FROM $online_game_log_daily_today AND gameslottype=$slottype;";
	        	$online_count = $db_analysis->getvalue($sql);	        	
	        	
	        	$sql = "SELECT fameball FROM tbl_fameball_daily WHERE bettype = 1 $os_type_str  AND slottype=$slottype AND today = '$today'";
	        	$fameball_count = $db_analysis->getvalue($sql);
	        }       
	        
	        if($slottype == 1)
	        {
	        	$sql = "SELECT COUNT(*)*5 AS tutorial_fameball ".
	        			"FROM tbl_user_ext t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx ".
	        			"WHERE t1.useridx > $std_useridx AND tutorial >= 4 $platform_str ".
	        			"AND createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
	        	$tutorial_fameball_count = $db_main->getvalue($sql);
	        	
	        	if($total_mode=="2")
	        		$tutorial_fameball_count = 0;
	        
	        	$fameball_count = $fameball_count - $tutorial_fameball_count;
	        }
	        
	        $fameball_per_playcount = ($playcount == 0) ? 0 : round($fameball_count / $playcount, 2);
	        
	        $rate = ($moneyin == 0) ? 0 :round(($moneyout) / $moneyin * 10000) / 100;
	        
	        if ($rate <= "90" || $rate >= "100" || $jackpot*100/$moneyin > 2)
	            $style = "style='color:red;'";
	        else
	            $style = "";
			
	    	for($j=0; $j<sizeof($slottype_list); $j++)
			{
				if($slottype_list[$j]["slottype"] == $slottype)
				{
					$slot_name = $slottype_list[$j]["slotname"];
					break;
				}
				else
				{
					$slot_name = "Unkown";
				}
			}
?>
				<tr>
					<td class="tdl" <?= $style ?>><?= $slot_name ?></td>
					<td class="tdr" <?= $style ?>><?= number_format($online_count) ?></td>
					<td class="tdr" <?= $style ?>><?= number_format($playcount) ?></td>				
					<td class="tdr" <?= $style ?>><?= number_format($moneyin) ?></td>
					<td class="tdr" <?= $style ?>><?= number_format($moneyout) ?></td>
					<td class="tdr" <?= $style ?>><?= number_format($jackpot) ?></td>
					<td class="tdr" <?= $style ?>><?= number_format($fameball_count) ?></td>
					<td class="tdr" <?= $style ?>><?= $fameball_per_playcount ?></td>
					<td class="tdc" <?= $style ?>><?= $rate."%" ?></td>
					<td class="tdc" <?= $style ?>><?= $today_unit_rate_array[$slottype]."%" ?></td>
				</tr>
<?
	    }
?>
			</tbody>
		</table>
	</form>
</div>
<!--  //CONTENTS WRAP -->

<?	
	$db_main->end();
	$db_main2->end();	
	$db_analysis->end();
	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
