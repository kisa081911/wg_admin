<?
    $top_menu = "game_stats";
    $sub_menu = "game_slot_daily_statsV2";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    //platform
    $os_type = ($_GET["os_type"] == "") ? "all" :$_GET["os_type"];    
    //all, kind of slot
    $tab = ($_GET["tab"] == "") ? "0" :$_GET["tab"];    
    //kind of room : 전체, 일반, 하이롤러(싱글X), 싱글모드, 하이롤러(싱글포함)
    $room_mode = ($_GET["room_mode"] == "") ? "room_mode" :$_GET["room_mode"];    
    //MODE, 베팅레벨
    $viewmode = ($_GET["viewmode"] == "") ? "viewmode" :$_GET["viewmode"];    
    //slot mode
    $mode = ($_GET["mode"] == "") ? "mode" :$_GET["mode"];    
    //slot betlevel
    $betlevel = ($_GET["betlevel"] == "") ? "betlevel" :$_GET["betlevel"];
    
    $startdate = ($_GET["startdate"] == "") ? date("Y-m-d", mktime(0,0,0,date("m"),date("d")-15,date("Y"))) : $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
    
    $pagename = "game_slot_daily_statsV2.php";
    
    if ($tab == "")
    	error_back("잘못된 접근입니다.");
    
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    $db_other = new CDatabase_Other();
    $db_analysis = new CDatabase_Analysis();
    
    $std_useridx = 20000;
    
    if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
    {
    	$port = ":8081";
    	$std_useridx = 10000;
    }
    
    $tail = "WHERE slottype in (1,2,34,69,86,88,89,99,153,142,24,96,117,143,144)";
    //slot 정보 가져오기
    $sql = "SELECT slottype, slotname FROM tbl_slot_list $tail";
    $slotlist = $db_main2->gettotallist($sql);
    
    $slot_default = array(array("slottype" => "0", "slotname" => "ALL"));
    $slotlist = array_merge($slot_default, $slotlist);
    
    //bet 정보 가져오기
    $sql = "SELECT bettype, betname FROM tbl_slot_betlevel ORDER BY bettype ASC";
    $betlist = $db_main2->gettotallist($sql);
    
    for($k=0; $k<sizeof($betlist); $k++)
    {
    	 
    	if($betlist[$k]["bettype"] > 100)
    	{
    		$total_betlevel_name[$betlist[$k]["bettype"]] = $betlist[$k]["betname"];
    	}
    	else
    	{
    		$total_betlevel_name[$k] = $betlist[$k]["betname"];
    	}
    	 
    	$total_betlevel_moneyin[$k] = 0;
    	$total_betlevel_moneyout[$k] = 0;
    	$total_betlevel_slot_rate[$k] = 0;
    	$total_betlevel_playcount[$k] = 0;
    	$total_betlevel_usercount[$k] = 0;
    }
    
    $bet_cnt = 0;    
    $total_betlevel_num = array();
    
    $tail = " AND is_v2 = 1 ";
    
    //4:tutorial, 5 : 스핀당 Gage 누적금액, 9:Ultra Freespin, 26:multi jackpot, 29:Daily Freespin, 31:부양중스킵(Push Freespin)
    $exclude_mode = "AND mode NOT IN (4, 5, 9, 26, 29, 31)";
    
    if($mode != "mode")
    	$exclude_mode = "AND 1=1";
    
    if($mode >= 100)
    {
    	if($mode == 100)
    		$in_mode = "(0, 3, 5)";
    	else if($mode == 101)
    		$in_mode = "(0, 3)";
    }
    
    if($os_type == "0")
    {
    	$table = "tbl_game_cash_stats_daily2 t1";
    	$os_txt = "Web";
    }
    else if($os_type == "1")
    {
    	$table = "tbl_game_cash_stats_ios_daily2 t1";
    	$os_txt = "IOS";
    }
    else if($os_type == "2")
    {
    	$table = "tbl_game_cash_stats_android_daily2 t1";
    	$os_txt = "Android";
    }
    else if($os_type == "3")
    {
    	$table = "tbl_game_cash_stats_amazon_daily2 t1";
    	$os_txt = "Amazon";
    }
    else
    {
    	$table = "( ".
		    	"	SELECT * FROM tbl_game_cash_stats_daily2 WHERE writedate BETWEEN '$startdate' AND '$enddate' AND is_v2 = 1	".
		    	"	UNION ALL	".
		    	"	SELECT * FROM tbl_game_cash_stats_ios_daily2 WHERE writedate BETWEEN '$startdate' AND '$enddate' AND is_v2 = 1	".
		    	"	UNION ALL	".
		    	"	SELECT * FROM tbl_game_cash_stats_android_daily2 WHERE writedate BETWEEN '$startdate' AND '$enddate' AND is_v2 = 1	".
		    	"	UNION ALL	".
		    	"	SELECT * FROM tbl_game_cash_stats_amazon_daily2 WHERE  writedate BETWEEN '$startdate' AND '$enddate' AND is_v2 = 1	".
    			") t1";
    	$os_txt = "ALL";
    }
    
    // os_type 구분
    $type = ($os_type == "all") ? "type" : $os_type;    
    $category = ($os_type == "all") ? "category" : $os_type;
    $devicetype = ($os_type == "all") ? "devicetype" : $os_type;
    $os_type_value = ($os_type == "all") ? "os_type" : $os_type;
    
    // default
    $total_betlevel = "1=1";
    $jackpot_betlevel = "1=1 and slottype in (1,2,34,69,86,88,89,99,153,142,24,96,117,143,144)";
    $total_unit_betlevel = "1=1";
    
    //전체(0), 일반(1), 플래티엄 (싱글X)(2), 싱글모드(3), 플래티엄 (싱글포함)(4)
    if($room_mode == "1") //일반
    {
    	$total_betlevel = "betlevel between 0 AND 9";
    	$total_unit_betlevel = "t1.betlevel between 0 AND 9";
    	$jackpot_betlevel = "bettype=0 and slottype in (1,2,34,69,86,88,89,99,153,142,24,96,117,143,144)";
    }
    else if($room_mode == "2") //플래티엄(싱글룸불포함)
    {
    	$total_betlevel = "betlevel between 10 AND 19";
    	$total_unit_betlevel = "t1.betlevel between 10 AND 19";
    	$jackpot_betlevel = "bettype=1 and slottype in (1,2,34,69,86,88,89,99,153,142,24,96,117,143,144)";
    }
    else if($room_mode == "3") //싱글모드
    {
    	$total_betlevel = "betlevel between 110 AND 119";
    	$total_unit_betlevel = "t1.betlevel between 110 AND 119";
    	$jackpot_betlevel = "bettype=1 and slottype in (1,2,34,69,86,88,89,99,153,142,24,96,117,143,144)";
    }
    else if($room_mode == "4") //플래티엄(싱글룸포함)
    {
    	$total_betlevel = "betlevel between 10 AND 119";
    	$total_unit_betlevel = "t1.betlevel between 10 AND 119";
    	$jackpot_betlevel = "bettype=1 and slottype in (1,2,34,69,86,88,89,99,153,142,24,96,117,143,144)";
    }
    
    if($tab == "0")//slottype = all
    {
    	//전체(0), mode(1), betlevel(2)
    	if($viewmode == "viewmode")  //all
    	{
    		$data_sql ="SELECT LEFT(writedate, 10) AS writedate, slottype, betlevel, mode, IFNULL(SUM(playcount),0) AS playcount, ".
	    				"	IFNULL(SUM(moneyin),0) AS moneyin,  IFNULL(SUM(moneyout), 0) AS moneyout,	".
	    				"	IFNULL(SUM(unit_moneyin),0) AS unit_moneyin,  IFNULL(SUM(unit_moneyout), 0) AS unit_moneyout,	".
	    				"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
						"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount, IFNULL(SUM(usercount),0) AS usercount	".
						"   , is_v2 ".
						"	FROM $table	". 
						"	WHERE mode=$mode AND $total_betlevel AND betlevel=$betlevel $tail ".
						"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $exclude_mode ".
						"		GROUP BY writedate ".
						"		ORDER BY writedate DESC;";
    		
    		if($mode >= 100)
    		{
    			$data_sql ="SELECT LEFT(writedate, 10) AS writedate, slottype, betlevel, mode, IFNULL(SUM(playcount),0) AS playcount, ".
    					"	IFNULL(SUM(moneyin),0) AS moneyin,  IFNULL(SUM(moneyout), 0) AS moneyout,	".
    					"	IFNULL(SUM(unit_moneyin),0) AS unit_moneyin,  IFNULL(SUM(unit_moneyout), 0) AS unit_moneyout,	".
    					"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
    					"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount, IFNULL(SUM(usercount),0) AS usercount	".
    					"	FROM $table	".
    					"	WHERE mode IN $in_mode AND $total_betlevel AND betlevel=$betlevel ".
    					"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $exclude_mode ".
    					"		GROUP BY writedate ".
    					"		ORDER BY writedate DESC;";
    			
    			if($mode == 100)
    			{
    				$data_sql ="SELECT LEFT(writedate, 10) AS writedate, slottype, betlevel, mode, IFNULL(SUM(playcount),0) AS playcount, ".
    						"	IFNULL(SUM(IF(MODE != 5, moneyin, 0)),0) AS moneyin,   IFNULL(SUM(IF(MODE != 5, moneyout, 0)), 0) + IFNULL(SUM(IF(MODE = 5, moneyout - moneyin, 0)),0) AS moneyout,	".
    						"	IFNULL(SUM(IF(MODE != 5, unit_moneyin, 0)),0) AS unit_moneyin,  IFNULL(SUM(IF(MODE != 5, unit_moneyout, 0)), 0) + IFNULL(SUM(IF(MODE = 5, unit_moneyout - unit_moneyin, 0)),0) AS unit_moneyout,	".
    						"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
    						"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount, IFNULL(SUM(usercount),0) AS usercount	".
    						"	FROM $table	".
    						"	WHERE mode IN $in_mode AND $total_betlevel AND betlevel=$betlevel ".
    						"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $exclude_mode ".
    						"		GROUP BY writedate ".
    						"		ORDER BY writedate DESC;";
    			}
    		}
    		
    		if($mode == "mode" && $room_mode == "room_mode" && $betlevel == "betlevel")
			{
				//트리트 환전 금액 계산
			    if($category != 'category')
				   $exchange_treat_sql = "SELECT today, freeamount FROM `tbl_user_freecoin_daily` WHERE category = '$category' AND type = 5 AND today BETWEEN '$startdate' AND '$enddate'";
			    else
				   $exchange_treat_sql = "SELECT today, SUM(freeamount) as freeamount FROM `tbl_user_freecoin_daily` WHERE TYPE = 5 AND today BETWEEN '$startdate' AND '$enddate' GROUP BY today";
				
				$exchange_treat_list = $db_analysis->gettotallist($exchange_treat_sql);
				
				$exchange_treat_array = array();
				
				for($e = 0; $e < sizeof($exchange_treat_list); $e++)
				{
					$today = $exchange_treat_list[$e]["today"];
					$freeamount = $exchange_treat_list[$e]["freeamount"];
					
					$exchange_treat_array[$today] = $freeamount;
				}
				
				//all 유저수
				$usercnt_sql = "SELECT writedate, SUM(usercount) AS usercount FROM tbl_game_distinct_usercntV2 WHERE os_type = $os_type_value AND cnt_type = 1 AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate";
				$usercount_list = $db_other->gettotallist($usercnt_sql);
					
				$usercount_array = array();
					
				for($u = 0; $u < sizeof($usercount_list); $u++)
				{
					$writedate = $usercount_list[$u]["writedate"];
					$usercount = $usercount_list[$u]["usercount"];
						
					$usercount_array[$writedate] = $usercount;
				}
			}
			
			//jackpot
			$jackpot_sql = "SELECT IFNULL(SUM(amount), 0) FROM tbl_jackpot_stat_daily WHERE $jackpot_betlevel AND today='[WRITEDATE]' AND devicetype='$devicetype' ";						
    	}
    	else if($viewmode == "1") //mode
    	{
    		$data_sql ="SELECT LEFT(writedate, 10) AS writedate, mode, IFNULL(SUM(playcount),0) AS playcount, ".
	    				"	IFNULL(SUM(moneyin),0) AS moneyin,  IFNULL(SUM(moneyout), 0) AS moneyout,	".
	    				"	IFNULL(SUM(unit_moneyin),0) AS unit_moneyin,  IFNULL(SUM(unit_moneyout), 0) AS unit_moneyout,	".
	    				"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
	    				"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount,	IFNULL(SUM(usercount),0) AS usercount	".
	    				"   , is_v2 ".
	    				"	FROM $table	".
	    				"	WHERE mode=$mode AND $total_betlevel AND betlevel=$betlevel $tail ".
	    				"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' ".
	    				"		GROUP BY writedate, mode ".
	    				"		ORDER BY writedate DESC, mode ASC ;";
    		
    		if($mode >= 100)
    		{
    			$data_sql ="SELECT LEFT(writedate, 10) AS writedate, mode, IFNULL(SUM(playcount),0) AS playcount, ".
    					"	IFNULL(SUM(moneyin),0) AS moneyin,  IFNULL(SUM(moneyout), 0) AS moneyout,	".
    					"	IFNULL(SUM(unit_moneyin),0) AS unit_moneyin,  IFNULL(SUM(unit_moneyout), 0) AS unit_moneyout,	".
    					"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
    					"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount,	IFNULL(SUM(usercount),0) AS usercount	".
    					"	FROM $table	".
    					"	WHERE mode IN $in_mode AND $total_betlevel AND betlevel=$betlevel ".
    					"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' ".
    					"		GROUP BY writedate, mode ".
    					"		ORDER BY writedate DESC, mode ASC ;";
    			
    			if($mode == 100)
    			{
    				$data_sql ="SELECT LEFT(writedate, 10) AS writedate, mode, IFNULL(SUM(playcount),0) AS playcount, ".
    						"	IFNULL(SUM(IF(MODE != 5, moneyin, 0)),0) AS moneyin,  IFNULL(SUM(IF(MODE != 5, moneyout, 0)), 0) + IFNULL(SUM(IF(MODE = 5, moneyout - moneyin, 0)),0) AS moneyout, ".
    						"	IFNULL(SUM(IF(MODE != 5, unit_moneyin, 0)),0) AS unit_moneyin,  IFNULL(SUM(IF(MODE != 5, unit_moneyout, 0)), 0) + IFNULL(SUM(IF(MODE = 5, unit_moneyout - unit_moneyin, 0)),0) AS unit_moneyout,	".
    						"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
    						"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount,	IFNULL(SUM(usercount),0) AS usercount	".
    						"	FROM $table	".
    						"	WHERE mode IN $in_mode AND $total_betlevel AND betlevel=$betlevel ".
    						"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' ".
    						"		GROUP BY writedate, mode ".
    						"		ORDER BY writedate DESC, mode ASC ;";
    			}
    		}
    	}
    	else //betlevel
    	{
    		$data_sql ="SELECT LEFT(writedate, 10) AS writedate, betlevel, IFNULL(SUM(playcount),0) AS playcount, ".
	    				"	IFNULL(SUM(moneyin),0) AS moneyin,  IFNULL(SUM(moneyout), 0) AS moneyout,	".
	    				"	IFNULL(SUM(unit_moneyin),0) AS unit_moneyin,  IFNULL(SUM(unit_moneyout), 0) AS unit_moneyout,	".
	    				"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
	    				"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount,	IFNULL(SUM(usercount),0) AS usercount	".
	    				"   , is_v2 ".
	    				"	FROM $table	".
	    				"	WHERE mode=$mode AND $total_betlevel AND betlevel=$betlevel $tail ".
	    				"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $exclude_mode ".
	    				"		GROUP BY writedate, betlevel ".
	    				"		ORDER BY writedate DESC, betlevel ASC ;";
    		
    		if($mode >= 100)
    		{
    			$data_sql ="SELECT LEFT(writedate, 10) AS writedate, betlevel, IFNULL(SUM(playcount),0) AS playcount, ".
    					"	IFNULL(SUM(moneyin),0) AS moneyin,  IFNULL(SUM(moneyout), 0) AS moneyout,	".
    					"	IFNULL(SUM(unit_moneyin),0) AS unit_moneyin,  IFNULL(SUM(unit_moneyout), 0) AS unit_moneyout,	".
    					"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
    					"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount,	IFNULL(SUM(usercount),0) AS usercount	".
    					"	FROM $table	".
    					"	WHERE mode IN  $in_mode AND $total_betlevel AND betlevel=$betlevel ".
    					"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $exclude_mode ".
    					"		GROUP BY writedate, betlevel ".
    					"		ORDER BY writedate DESC, betlevel ASC ;";
    			
    			if($mode == 100)
    			{
    				$data_sql ="SELECT LEFT(writedate, 10) AS writedate, betlevel, IFNULL(SUM(playcount),0) AS playcount, ".
    						"	IFNULL(SUM(IF(MODE != 5, moneyin, 0)),0) AS moneyin,  IFNULL(SUM(IF(MODE != 5, moneyout, 0)), 0) + IFNULL(SUM(IF(MODE = 5, moneyout - moneyin, 0)),0) AS moneyout,	".
    						"	IFNULL(SUM(IF(MODE != 5, unit_moneyin, 0)),0) AS unit_moneyin,  IFNULL(SUM(IF(MODE != 5, unit_moneyout, 0)), 0) + IFNULL(SUM(IF(MODE = 5, unit_moneyout - unit_moneyin, 0)),0) AS unit_moneyout,	".
    						"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
    						"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount,	IFNULL(SUM(usercount),0) AS usercount	".
    						"	FROM $table	".
    						"	WHERE mode IN  $in_mode AND $total_betlevel AND betlevel=$betlevel ".
    						"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $exclude_mode ".
    						"		GROUP BY writedate, betlevel ".
    						"		ORDER BY writedate DESC, betlevel ASC ;";
    			}
    		}
    	}
    	
    	//all mode 유저수
    	if(($viewmode == "1" || $mode != "mode") && $room_mode == "room_mode" && $betlevel == "betlevel")
    	{
	    	if($mode == "mode")
	    		$cnt_type_value = "cnt_type_value";
	    	else
	    		$cnt_type_value = $mode;
	    	
	    	$usercnt_sql = "SELECT writedate, cnt_type_value, SUM(usercount) AS usercount ".
	    					"FROM tbl_game_distinct_usercntV2 ".
	    					"WHERE os_type = $os_type_value AND cnt_type = 3 AND cnt_type_value=$cnt_type_value AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate, cnt_type_value";
	    	
	    	if($mode >= 100)
	    	{
	    		$usercnt_sql = "SELECT writedate, 100 AS cnt_type_value, SUM(usercount) AS usercount ".
	    				"FROM tbl_game_distinct_usercnt ".
	    				"WHERE os_type = $os_type_value AND cnt_type = 3 AND cnt_type_value IN $in_mode AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate";
	    	}
	    	
	    	
	    	$usercount_list = $db_other->gettotallist($usercnt_sql);
	    	
	    	$usercount_array = array();
	    	 
	    	for($u = 0; $u < sizeof($usercount_list); $u++)
	    	{
	    		$writedate = $usercount_list[$u]["writedate"];
	    		$cnt_type_value = $usercount_list[$u]["cnt_type_value"];
	    		$usercount = $usercount_list[$u]["usercount"];
	    	
	    		$usercount_array[$writedate][$cnt_type_value] = $usercount;
	    	}
    	}
    	
    	//all betlevel 유저수
    	if(($viewmode == "2" || $betlevel != "betlevel") &&  $room_mode == "room_mode" && $mode == "mode")
    	{
    		if($betlevel == "betlevel")
    			$cnt_type_value = "cnt_type_value";
    		else
    			$cnt_type_value = $betlevel;
    	
    		$usercnt_sql = "SELECT writedate, cnt_type_value, SUM(usercount) AS usercount ".
    				"FROM tbl_game_distinct_usercntV2 ".
    				"WHERE os_type = $os_type_value AND cnt_type = 4 AND cnt_type_value=$cnt_type_value AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate, cnt_type_value";
    		$usercount_list = $db_other->gettotallist($usercnt_sql);
    	
    		$usercount_array = array();
    		 
    		for($u = 0; $u < sizeof($usercount_list); $u++)
    		{
    			$writedate = $usercount_list[$u]["writedate"];
    			$cnt_type_value = $usercount_list[$u]["cnt_type_value"];
    			$usercount = $usercount_list[$u]["usercount"];
    	
    			$usercount_array[$writedate][$cnt_type_value] = $usercount;
    		}
    	}
    	
    	
    	if($room_mode > 0 && $viewmode=="viewmode" && $mode == "mode" && betlevel == "betlevel")
    	{
    		// 일반, 하이롤러(싱글 x), 싱글모드, 하이롤러(싱글 0) group by writedate
    		$usercnt_sql = "SELECT writedate, betmode, SUM(usercount) AS usercount ".
    						"FROM tbl_game_betmode_distinct_usercntV2 ".
    						"WHERE os_type = $os_type_value AND cnt_type = 1 AND betmode = $room_mode  AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate";
    		$usercount_list = $db_other->gettotallist($usercnt_sql);
    		
    		$statnd_room_usercount_array = array();
    		 
    		for($u = 0; $u < sizeof($usercount_list); $u++)
    		{
    			$writedate = $usercount_list[$u]["writedate"];
    			$usercount = $usercount_list[$u]["usercount"];
    		
    			$statnd_room_array[$writedate] = $usercount;
    		}
    	}
    	else if($room_mode > 0 && $viewmode=="1" && $mode != "mode" && betlevel == "betlevel")
		{
			// 일반, 하이롤러(싱글 x), 싱글모드, 하이롤러(싱글 0) group by writedate, mode
			$usercnt_sql = "SELECT writedate, betmode, SUM(usercount) AS usercount ".
							"FROM tbl_game_betmode_distinct_usercntV2 ".
							"WHERE os_type = $os_type_value AND cnt_type = 3 AND betmode = $room_mode  AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate";
			$usercount_list = $db_other->gettotallist($usercnt_sql);
    		
			$statnd_room_array = array();
    			 
			for($u = 0; $u < sizeof($usercount_list); $u++)
			{
				$writedate = $usercount_list[$u]["writedate"];
				$usercount = $usercount_list[$u]["usercount"];
    		
				$statnd_room_array[$writedate] = $usercount;
			}
		}
    	$slot_data_list = $db_main2->gettotallist($data_sql);
    }
    else //slottype = num
    {
    	//전체(0), mode(1), betlevel(2)
    	if($viewmode == "viewmode")  //all
    	{
    		$data_sql ="SELECT slottype, betlevel, mode, IFNULL(SUM(playcount),0) AS playcount, ".
	    				"	IFNULL(SUM(moneyin),0) AS moneyin,  IFNULL(SUM(moneyout), 0) AS moneyout,	".
	    				"	IFNULL(SUM(unit_moneyin),0) AS unit_moneyin,  IFNULL(SUM(unit_moneyout), 0) AS unit_moneyout,	".
	    				"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
	    				"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount,	".
	    				"	LEFT(writedate, 10) AS writedate, IFNULL(SUM(usercount),0) AS usercount	".
	    				"   , is_v2 ".
	    				"	FROM $table	".
	    				"	WHERE slottype=$tab AND mode=$mode AND $total_betlevel AND betlevel=$betlevel $tail ".
	    				"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $exclude_mode ".
	    				"		GROUP BY writedate ORDER BY writedate DESC;";
    		
    		if($mode >= 100)
    		{
    			$data_sql ="SELECT slottype, betlevel, mode, IFNULL(SUM(playcount),0) AS playcount, ".
    					"	IFNULL(SUM(moneyin),0) AS moneyin,  IFNULL(SUM(moneyout), 0) AS moneyout,	".
    					"	IFNULL(SUM(unit_moneyin),0) AS unit_moneyin,  IFNULL(SUM(unit_moneyout), 0) AS unit_moneyout,	".
    					"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
    					"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount,	".
    					"	LEFT(writedate, 10) AS writedate, IFNULL(SUM(usercount),0) AS usercount	".
    					"	FROM $table	".
    					"	WHERE slottype=$tab AND mode IN $in_mode AND $total_betlevel AND betlevel=$betlevel ".
    					"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $exclude_mode ".
    					"		GROUP BY writedate ORDER BY writedate DESC;";
    			
    			if($mode == 100)
    			{
    				$data_sql ="SELECT slottype, betlevel, mode, IFNULL(SUM(playcount),0) AS playcount, ".
    						"	IFNULL(SUM(IF(MODE != 5, moneyin, 0)),0) AS moneyin,  IFNULL(SUM(IF(MODE != 5, moneyout, 0)), 0) + IFNULL(SUM(IF(MODE = 5, moneyout - moneyin, 0)),0) AS moneyout, ".
    						"	IFNULL(SUM(IF(MODE != 5, unit_moneyin, 0)),0) AS unit_moneyin,  IFNULL(SUM(IF(MODE != 5, unit_moneyout, 0)), 0) + IFNULL(SUM(IF(MODE = 5, unit_moneyout - unit_moneyin, 0)),0) AS unit_moneyout,	".
    						"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
    						"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount,	".
    						"	LEFT(writedate, 10) AS writedate, IFNULL(SUM(usercount),0) AS usercount	".
    						"	FROM $table	".
    						"	WHERE slottype=$tab AND mode IN $in_mode AND $total_betlevel AND betlevel=$betlevel ".
    						"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $exclude_mode ".
    						"		GROUP BY writedate ORDER BY writedate DESC;";
    			}
    				 
    		}
    		
    		//jackpot
   			$jackpot_sql = "SELECT IFNULL(SUM(amount), 0) FROM tbl_jackpot_stat_daily WHERE $jackpot_betlevel AND devicetype = $devicetype AND today='[WRITEDATE]' AND slottype=$tab ";
   			
   			if($mode == "mode" && $room_mode == "room_mode" && $betlevel == "betlevel")
   			{
	   			//유저수
	   			$usercnt_sql = "SELECT writedate, SUM(usercount) AS usercount FROM tbl_game_distinct_usercntV2 WHERE os_type = $os_type_value AND cnt_type = 2 AND cnt_type_value = $tab AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate";
	   			$usercount_list = $db_other->gettotallist($usercnt_sql);
	   				
	   			$usercount_array = array();
	   				
	   			for($u = 0; $u < sizeof($usercount_list); $u++)
	   			{
	   				$writedate = $usercount_list[$u]["writedate"];
	   				$usercount = $usercount_list[$u]["usercount"];
	   					
	   				$usercount_array[$writedate] = $usercount;
	   			}
   			}
    	}
    	else if($viewmode == "1") //mode
    	{
    		$data_sql ="SELECT LEFT(writedate, 10) AS writedate, mode, IFNULL(SUM(playcount),0) AS playcount, ".
	    				"	IFNULL(SUM(moneyin),0) AS moneyin,  IFNULL(SUM(moneyout), 0) AS moneyout,	".
	    				"	IFNULL(SUM(unit_moneyin),0) AS unit_moneyin,  IFNULL(SUM(unit_moneyout), 0) AS unit_moneyout,	".
	    				"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
	    				"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount,	IFNULL(SUM(usercount),0) AS usercount	".
	    				"   , is_v2 ".
	    				"	FROM $table	".
	    				"	WHERE slottype=$tab AND mode=$mode AND $total_betlevel AND betlevel=$betlevel $tail ".
	    				"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' ".
	    				"		GROUP BY writedate, mode ".
	    				"		ORDER BY writedate DESC, mode ASC ;";
    		
    		if($mode >= 100)
    		{
    			$data_sql ="SELECT LEFT(writedate, 10) AS writedate, mode, IFNULL(SUM(playcount),0) AS playcount, ".
    					"	IFNULL(SUM(moneyin),0) AS moneyin,  IFNULL(SUM(moneyout), 0) AS moneyout,	".
    					"	IFNULL(SUM(unit_moneyin),0) AS unit_moneyin,  IFNULL(SUM(unit_moneyout), 0) AS unit_moneyout,	".
    					"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
    					"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount,	IFNULL(SUM(usercount),0) AS usercount	".
    					"	FROM $table	".
    					"	WHERE slottype=$tab AND mode IN $in_mode AND $total_betlevel AND betlevel=$betlevel ".
    					"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' ".
    					"		GROUP BY writedate, mode ".
    					"		ORDER BY writedate DESC, mode ASC ;";
    			
    			if($mode == 100)
    			{
    				$data_sql ="SELECT LEFT(writedate, 10) AS writedate, mode, IFNULL(SUM(playcount),0) AS playcount, ".
    						"	IFNULL(SUM(IF(MODE != 5, moneyin, 0)),0) AS moneyin,  IFNULL(SUM(IF(MODE != 5, moneyout, 0)), 0) + IFNULL(SUM(IF(MODE = 5, moneyout - moneyin, 0)),0) AS moneyout,		".
    						"	IFNULL(SUM(IF(MODE != 5, unit_moneyin, 0)),0) AS unit_moneyin,  IFNULL(SUM(IF(MODE != 5, unit_moneyout, 0)), 0) + IFNULL(SUM(IF(MODE = 5, unit_moneyout - unit_moneyin, 0)),0) AS unit_moneyout,	".
    						"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
    						"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount,	IFNULL(SUM(usercount),0) AS usercount	".
    						"	FROM $table	".
    						"	WHERE slottype=$tab AND mode IN $in_mode AND $total_betlevel AND betlevel=$betlevel ".
    						"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' ".
    						"		GROUP BY writedate, mode ".
    						"		ORDER BY writedate DESC, mode ASC ;";
    			}
    		}
    	}
    	else //betlevel
    	{
    		$data_sql ="SELECT LEFT(writedate, 10) AS writedate, betlevel, IFNULL(SUM(playcount),0) AS playcount, ".
	    				"	IFNULL(SUM(moneyin),0) AS moneyin,  IFNULL(SUM(moneyout), 0) AS moneyout,	".
	    				"	IFNULL(SUM(unit_moneyin),0) AS unit_moneyin,  IFNULL(SUM(unit_moneyout), 0) AS unit_moneyout,	".
	    				"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
	    				"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount,	IFNULL(SUM(usercount),0) AS usercount	".
	    				"   , is_v2 ".
	    				"	FROM $table	".
	    				"	WHERE slottype=$tab AND mode=$mode AND $total_betlevel AND betlevel=$betlevel $tail ".
	    				"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $exclude_mode ".
	    				"		GROUP BY writedate, betlevel ".
	    				"		ORDER BY writedate DESC, betlevel ASC ;";
    		
    		if($mode >= 100)
    		{
    			$data_sql ="SELECT LEFT(writedate, 10) AS writedate, betlevel, IFNULL(SUM(playcount),0) AS playcount, ".
    					"	IFNULL(SUM(moneyin),0) AS moneyin,  IFNULL(SUM(moneyout), 0) AS moneyout,	".
    					"	IFNULL(SUM(unit_moneyin),0) AS unit_moneyin,  IFNULL(SUM(unit_moneyout), 0) AS unit_moneyout,	".
    					"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
    					"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount,	IFNULL(SUM(usercount),0) AS usercount	".
    					"	FROM $table	".
    					"	WHERE slottype=$tab AND mode in $in_mode AND $total_betlevel AND betlevel=$betlevel ".
    					"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $exclude_mode ".
    					"		GROUP BY writedate, betlevel ".
    					"		ORDER BY writedate DESC, betlevel ASC ;";
    			
    			if($mode == 100)
    			{
    				$data_sql ="SELECT LEFT(writedate, 10) AS writedate, betlevel, IFNULL(SUM(playcount),0) AS playcount, ".
    						"	IFNULL(SUM(IF(MODE != 5, moneyin, 0)),0) AS moneyin,  IFNULL(SUM(IF(MODE != 5, moneyout, 0)), 0) + IFNULL(SUM(IF(MODE = 5, moneyout - moneyin, 0)),0) AS moneyout,	".
    						"	IFNULL(SUM(IF(MODE != 5, unit_moneyin, 0)),0) AS unit_moneyin,  IFNULL(SUM(IF(MODE != 5, unit_moneyout, 0)), 0) + IFNULL(SUM(IF(MODE = 5, unit_moneyout - unit_moneyin, 0)),0) AS unit_moneyout,	".
    						"	IFNULL(ROUND(SUM(moneyout) / SUM(moneyin) * 100, 2), 0) AS slot_rate,	".
    						"	IFNULL(ROUND(SUM(unit_moneyout) / SUM(unit_moneyin) * 100, 2), 0) AS unit_rate, IFNULL(SUM(treatamount),0) AS treatamount,	IFNULL(SUM(usercount),0) AS usercount	".
    						"	FROM $table	".
    						"	WHERE slottype=$tab AND mode in $in_mode AND $total_betlevel AND betlevel=$betlevel ".
    						"		AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' $exclude_mode ".
    						"		GROUP BY writedate, betlevel ".
    						"		ORDER BY writedate DESC, betlevel ASC ;";
    			}
    		}
    	}    	
    	
    	if($room_mode > 0 && $viewmode=="viewmode" && $mode == "mode" && betlevel == "betlevel")
    	{
    		$usercnt_sql = "SELECT writedate, betmode, SUM(usercount) AS usercount ".
    						"FROM tbl_game_betmode_distinct_usercntV2 ".
    						"WHERE os_type = $os_type_value AND betmode = $room_mode AND cnt_type = 2 AND slottype = $tab AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate";
    		$usercount_list = $db_other->gettotallist($usercnt_sql);
    		
    		$statnd_room_array = array();
    		 
    		for($u = 0; $u < sizeof($usercount_list); $u++)
    		{
    			$writedate = $usercount_list[$u]["writedate"];
    			$usercount = $usercount_list[$u]["usercount"];
    	
    			$statnd_room_array[$writedate] = $usercount;
    		}
    	}
    	
    	
    	$slot_data_list = $db_main2->gettotallist($data_sql);    	
    }    
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }

    function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		var all = document.getElementById("type_all");
		
		document.search_form.os_type.value = type;
		
		if (type == "all")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			all.className="btn_schedule_select";
		}
		else if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			all.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			all.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
			all.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
			all.className="btn_schedule";
		}
	
		search_form.submit();
	}
    
    function tab_change(tab)
    {
        var search_form = document.search_form;
        search_form.tab.value = tab;
        search_form.submit();
    }
    
    $(function() {
        $("#startdate").datepicker({ });
    });

    $(function() {
        $("#enddate").datepicker({ });
    });
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">        
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
			<input type="button" class="<?= ($os_type == "all") ? "btn_schedule_select" : "btn_schedule" ?>" value="ALL" id="type_all" onclick="change_os_type('all')"    >
			<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
			<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="IOS" id="type_ios" onclick="change_os_type('1')" />
			<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
			<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />			
		</span>
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 게임 활동 추이(<?= $os_txt ?>) - 일별 통계</div>
			<input type="hidden" name="term" id="term" value="<?= $term ?>" />
			<input type="hidden" name="os_type" id="os_type" value="<?= ($os_type == "all") ? "all" : $os_type  ?>" />   
			<div class="search_box">
			
				<span class="search_lb2 ml20" style="font:12px;color:#000;font-weight:bold;">View Mode</span>
				<select name="viewmode" id="viewmode">										
					<option value="viewmode" <?= ($viewmode=="viewmode") ? "selected" : "" ?>>view - 전체</option>
					<option value="1" <?= ($viewmode=="1") ? "selected" : "" ?>>MODE</option>                       
					<option value="2" <?= ($viewmode=="2") ? "selected" : "" ?>>베팅레벨</option>				
				</select>&nbsp;
				
				<span class="search_lb2 ml20" style="font:12px;color:#000;font-weight:bold;">Room</span>
				<select name="room_mode" id="room_mode">										
					<option value="room_mode" <?= ($room_mode=="room_mode") ? "selected" : "" ?>>Room - 전체</option>
					<option value="1" <?= ($room_mode=="1") ? "selected" : "" ?>>일반</option>                       
					<option value="2" <?= ($room_mode=="2") ? "selected" : "" ?>>하이롤러(싱글X)</option>					
					<option value="3" <?= ($room_mode=="3") ? "selected" : "" ?>>싱글모드</option>					
					<option value="4" <?= ($room_mode=="4") ? "selected" : "" ?>>하이롤러(싱글포함)</option>					
				</select>&nbsp;			
				
				<span class="search_lb2 ml20" style="font:12px;color:#000;font-weight:bold;">Slot Mode</span>
				<select name="mode" id="mode">
					<option value="mode" <?= ($mode=="mode") ? "selected" : "" ?>>Mode - 전체</option>
					<option value="0" <?= ($mode=="0") ? "selected" : "" ?>>일반(0)</option>
					<option value="3" <?= ($mode=="3") ? "selected" : "" ?>>Gage 획득(3)</option>
					<option value="101" <?= ($mode=="101") ? "selected" : "" ?>>Gage 획득 승률(0,3)</option>					
					<option value="4" <?= ($mode=="4") ? "selected" : "" ?>>튜토리얼(4)</option>
					<option value="5" <?= ($mode=="5") ? "selected" : "" ?>>Gage 잔여 (5)</option>
					<option value="100" <?= ($mode=="100") ? "selected" : "" ?>>Gage 획득/잔여 승률 (0,3,5)</option>
					<option value="9" <?= ($mode=="9") ? "selected" : "" ?>>프리스핀 - Ultra (9)</option>
					<option value="11" <?= ($mode=="11") ? "selected" : "" ?>>신규 가입자 승률 부양(11)</option>
					<option value="17" <?= ($mode=="17") ? "selected" : "" ?>>재방문자 승률 부양(17)</option>
					<option value="18" <?= ($mode=="18") ? "selected" : "" ?>>플래티넘 승률 부양(18)</option>
					<option value="25" <?= ($mode=="25") ? "selected" : "" ?>>비결제자(25)</option>
					<option value="26" <?= ($mode=="26") ? "selected" : "" ?>>Multi Jackpot(26)</option>
					<option value="27" <?= ($mode=="27") ? "selected" : "" ?>>결제자 이탈 부양(27)</option>
					<option value="28" <?= ($mode=="28") ? "selected" : "" ?>>결제자(28)</option>					
					<option value="29" <?= ($mode=="29") ? "selected" : "" ?>>프리스핀 - Daily(29)</option>
					<option value="31" <?= ($mode=="31") ? "selected" : "" ?>>프리스핀 - Push (31)</option>					
					<option value="33" <?= ($mode=="33") ? "selected" : "" ?>>첫결제자 승률 부양(33)</option>					
				</select>
				
				<span class="search_lb2 ml20" style="font:12px;color:#000;font-weight:bold;">BetLevel</span>
				<select name="betlevel" id="betlevel">
					<option value="betlevel" <?= ($bettype=="betlevel") ? "selected" : "" ?>>베팅레벨 - 전체</option>
<?
	for ($i=0; $i<sizeof($betlist); $i++)
	{
		$bettype = $betlist[$i]["bettype"];
		$betname = $betlist[$i]["betname"];
?>	
					<option value="<?= $bettype ?>" <?= ($bettype == $betlevel) ? "selected=\"true\"" : "" ?>><?= $betname ?></option>
<?						
	}
?>						
				</select>
				<input type="hidden" name="term" id="term" value="<?= $term ?>" />
				<input type="hidden" name="tab" id="tab" value="<?= $tab ?>" />
				&nbsp;&nbsp;
				<input type="input" class="search_text" id="startdate" name="startdate" style="width:65px" value="<?= $startdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
				<input type="input" class="search_text" id="enddate" name="enddate" style="width:65px" value="<?= $enddate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
	</div>
	<br><br>		
	<!-- //title_warp -->
	<ul class="tab">
<?
	for ($i=0; $i<sizeof($slotlist); $i++)
	{
		$slottype1 = $slotlist[$i]["slottype"];
		$slotname1 = str_replace(" Slot", "", $slotlist[$i]["slotname"]);
?>	
			<li id="tab_<?= $slottype1 ?>" class="<?= ($tab == $slottype1) ? "select" : "" ?>" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('<?= $slottype1 ?>')"><?= $slotname1 ?></li>
<?
	}
?>                
	</ul>
	<!-- view table -->
	<table class="tbl_list_basic1"  style="width:1300px">
		<colgroup>
			<col width="110">
			<col width="110">
			<col width="110">
			<col width="110">
			<col width="110">
			<col width="110">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
		</colgroup>
		<thead>
			<tr>
				<th style="border-top:none;">날짜</th>
<?
			if($viewmode == "1")
			{
?>
				<th style="border-top:none;">Slot Mode</th>
<?
			}
			else if($viewmode == "2")
			{
?>
				<th style="border-top:none;">베팅레벨</th>
<?
			}
?>
				<th style="text-align:right;border-top:none;">money_in(A)</th>
				<th style="text-align:right;border-top:none;">money_out(B)</th>
<?
			if($viewmode == "viewmode" && $mode == "mode")
			{
?>
				<th style="border-top:none;">Jackpot(C)</th>
				<th style="border-top:none;">게임 이익</th>
				<th style="border-top:none;">트리트 금액(D)</th>
<?
				if($tab == "0")
				{
?>
					<!-- th style="border-top:none;">트리트 환전(E)</th-->
<?
				}
			}

			if($viewmode != "2")
			{
				if($mode == 5)
				{
?>
					<th style="border-top:none;">B-A</th>				
<?
				}
?>
					<th style="border-top:none;">단위 승률</th>
<?
			}
?>
				<th style="border-top:none;">승률<br>(B/A)</th>
<?
			if($viewmode == "viewmode" && $mode == "mode")
			{
?>
				<th style="border-top:none;">승률<br>((B+C)/A)</th>
				<th style="border-top:none;">승률<br>(B-D)/A)</th>
<?
				if($tab == "0")
				{
?>
					<!-- th style="border-top:none;">승률<br>(B-D+E)/A)</th-->
<?
				}
			}
?>
				<th style="border-top:none;">게임횟수</th>
				<th style="border-top:none;">유저수</th>
			</tr>
		</thead>
    	<tbody>
<?
   	 for ($j=0; $j<sizeof($slot_data_list); $j++)
     {
    	$writedate = $slot_data_list[$j]["writedate"];
    	$mode_data = $slot_data_list[$j]["mode"];
    	$betlevel_data = $slot_data_list[$j]["betlevel"];
    	$slottype_data = $slot_data_list[$j]["slottype"];
    	$moneyin = $slot_data_list[$j]["moneyin"];
    	$moneyout = $slot_data_list[$j]["moneyout"];
    	$slot_rate = $slot_data_list[$j]["slot_rate"];
    	$unit_moneyin= $slot_data_list[$j]["unit_moneyin"];
    	$unit_moneyout = $slot_data_list[$j]["unit_moneyout"];
    	$unit_rate = $slot_data_list[$j]["unit_rate"];
    	$treatamount = $slot_data_list[$j]["treatamount"];
    	$playcount = $slot_data_list[$j]["playcount"];
    	$usercount = $slot_data_list[$j]["usercount"];
    	
    	//tbl_game_distinct_usercntV2(cnt_type : 1(all), 2(slottype))
    	if($viewmode == "viewmode" && $mode == "mode" && $room_mode == "room_mode" && $betlevel == "betlevel")   		
    		$usercount = $usercount_array[$writedate];    	
    	
    		//tbl_game_distinct_usercntV2(cnt_type : 3(mode))
    	if(($viewmode == "1" || $mode != "mode") && $room_mode == "room_mode" && $betlevel == "betlevel" && $tab == "0")
    		$usercount = $usercount_array[$writedate][$mode_data];
    	
    		//tbl_game_distinct_usercntV2(cnt_type : 4(betlevel))    	
    	if(($viewmode == "2" || $betlevel != "betlevel") &&  $room_mode == "room_mode" && $mode == "mode" && $tab == "0")
    		$usercount = $usercount_array[$writedate][$betlevel_data];
    	
    		//tbl_game_betmode_distinct_usercntV2(cnt_type : 1(all), 2(slottype), 3(mode))
    	if(sizeof($statnd_room_array) > 0)
    		$usercount = $statnd_room_array[$writedate];    		

    	$jackpot = 0;
    	
    	if($viewmode == "viewmode")
    		$jackpot = $db_analysis->getvalue(str_replace("[WRITEDATE]", $writedate, $jackpot_sql));    	
    	
    	$exchange_treat_data = $exchange_treat_array[$writedate];    	
    	
    	//게임 이익
    	$game_profit = $moneyin - ($moneyout + $jackpot);
    	
    	//승률((B+C)/A)
    	$include_jackpot_rate = ($moneyin == 0 ? 0:round(($moneyout + $jackpot) / $moneyin * 10000) / 100);    	
    	//승률(B-D)/A)
    	$exclude_treat_rate = ($moneyin == 0 ? 0:round(($moneyout - $treatamount) / $moneyin * 10000) / 100);
    	//승률(B-D+E)/A)   	
    	$exclude_treat_include_exchange_treat_rate =  ($moneyin == 0 ? 0:round(($moneyout - $treatamount + $exchange_treat_data) / $moneyin * 10000) / 100);
    	
    	if ($include_jackpot_rate <= "80" || $include_jackpot_rate >= "120")
    		$style = "style='color:#fb7878;font-weight:bold;'";
    	else
    		$style = ""; 
    	
    	
    	//total value
    	$total_moneyin += $moneyin;
    	$total_moneyout += $moneyout;
    	$total_game_profit += $game_profit;
    	$total_unit_moneyin += $unit_moneyin;
    	$total_unit_moneyout += $unit_moneyout;
    	$total_jackpot += $jackpot;
    	$total_treatamount += $treatamount;
    	$total_exchange_treat_data += $exchange_treat_data;
    	$total_playcount += $playcount;
    	$total_usercount += $usercount;
    	
    	//승률
    	$total_slot_rate = ($total_moneyin == 0 ? 0:round($total_moneyout / $total_moneyin * 10000) / 100);
    	//단위 승률
    	$total_unit_rate = ($total_unit_moneyin == 0 ? 0:round($total_unit_moneyout / $total_unit_moneyin * 10000) / 100);
    	//승률((B+C)/A)
    	$total_include_jackpot_rate = ($total_moneyin == 0 ? 0:round(($total_moneyout + $total_jackpot) / $total_moneyin * 10000) / 100);
    	//승률(B-D)/A)
    	$total_exclude_treat_rate = ($total_moneyin == 0 ? 0:round(($total_moneyout - $total_treatamount) / $total_moneyin * 10000) / 100);
    	//승률(B-D+E)/A)
    	$total_exclude_treat_include_exchange_treat_rate =  ($total_moneyin == 0 ? 0:round(($total_moneyout - $total_treatamount + $total_exchange_treat_data) / $total_moneyin * 10000) / 100);    	
    	
    	
    	//mode & mode total value
    	if($mode_data == "0")
        	$mode_str = "일반모드(0)";
        else if($mode_data == "3")
        	$mode_str = "특수모드 (3)";
        else if($mode_data == "4")
        	$mode_str = "튜토리얼(4)";
        else if($mode_data == "5")
        	$mode_str = "스핀당 Gage누적금액(5)";
        else if($mode_data == "9")
        	$mode_str = "프리스핀 -Ultra(9)";
        else if($mode_data == "11")
        	$mode_str = "신규 가입자 부양(11)";
        else if($mode_data == "17")
        	$mode_str = "재방문자 부양(17)";
        else if($mode_data == "18")
        	$mode_str = "플래티넘 부양(18)";
        else if($mode_data == "25")
        	$mode_str = "비결제자(25)";
        else if($mode_data == "26")
        	$mode_str = "Multi Jackpot(26)";
        else if($mode_data == "27")
        	$mode_str = "결제자 이탈 부양(27)";
        else if($mode_data == "28")
        	$mode_str = "결제자(28)";
        else if($mode_data == "29")
        	$mode_str = "프리스핀-Daily(29)";
        else if($mode_data == "31")
        	$mode_str = "프리스핀 -Push(31)";
        else if($mode_data == "33")
        	$mode_str = "첫결제자 부양(33)";
        else if($mode_data == "100")
        	$mode_str = "Gage누적금액포함승률";
        else if($mode_data == "101")
        	$mode_str = "Gage슬롯 전체승률";

        $total_mode_moneyin[$mode_data] += $moneyin;
        $total_mode_moneyout[$mode_data] += $moneyout;
        $total_mode_unit_moneyin[$mode_data] += $unit_moneyin;
        $total_mode_unit_moneyout[$mode_data] += $unit_moneyout;
        $total_mode_playcount[$mode_data] += $playcount;
        $total_mode_usercount[$mode_data] += $usercount;
        
        $total_mode_slot_rate[$mode_data] = ($total_mode_moneyin[$mode_data] == 0) ? 0 : round(($total_mode_moneyout[$mode_data]) / $total_mode_moneyin[$mode_data] * 10000) / 100;
        $total_mode_unit_rate[$mode_data] = ($total_mode_unit_moneyin[$mode_data] == 0) ? 0 : round(($total_mode_unit_moneyout[$mode_data]) / $total_mode_unit_moneyin[$mode_data] * 10000) / 100;
        
        //betlevel
        if(!in_array($betlevel_data, $total_betlevel_num))
        {
        	$total_betlevel_num[$bet_cnt] = $betlevel_data;
        	$bet_cnt++;
        }
        
        $betlevel_str = $total_betlevel_name[$betlevel_data];
        $total_betlevel_moneyin[$betlevel_data] += $moneyin;
        $total_betlevel_moneyout[$betlevel_data] += $moneyout;
        $total_betlevel_playcount[$betlevel_data] += $playcount;
        $total_betlevel_usercount[$betlevel_data] += $usercount;
        
        $total_betlevel_slot_rate[$betlevel_data] = ($total_betlevel_moneyin[$betlevel_data] == 0 ? 0:round(($total_betlevel_moneyout[$betlevel_data]) / $total_betlevel_moneyin[$betlevel_data] * 10000) / 100);
        
        if($datetmp != $writedate)
		{
?>
			<tr onmouseover="className='tr_over'" onmouseout="className=''" style="border-top:1px double;">    	

<?
		}
		else
		{
?>
			<tr onmouseover="className='tr_over'" onmouseout="className=''" >
<?
		}	
			
		$datetmp = $writedate;       
?>
				<td class="tdc point" <?= $style ?>><?= $writedate?></td>
<?
			if($viewmode == "1")
			{
?>
				<td class="tdc point" <?= $style ?>><?= $mode_str ?></td>
<?
			}
			else if($viewmode == "2")
			{
?>
				<td class="tdc point" <?= $style ?>><?= $betlevel_str ?></td>
<?
			}
?>
				<td class="tdr point" <?= $style ?>><?= number_format($moneyin)?></td>
				<td class="tdr point" <?= $style ?>><?= number_format($moneyout)?></td>
<?
			if($viewmode == "viewmode" && $mode == "mode")
			{
?>
				<td class="tdr point" <?= $style ?>><?= number_format($jackpot)?></td>
				<td class="tdr point" <?= $style ?>><?= number_format($game_profit)?></td>
				<td class="tdr point" <?= $style ?>><?= number_format($treatamount)?></td>
<?
				if($tab == "0")
				{
?>
					<!-- td class="tdc point" <?= $style ?>><?= number_format($exchange_treat_data)?></td-->
<?
				}
			}
			
			if($viewmode != "2")
			{
				
				if($mode == 5)
				{
?>
					<td class="tdc point" <?= $style ?>><?= number_format($moneyout-$moneyin)?></td>
<?
				}				
?>
					<td class="tdc point" <?= $style ?>><?= number_format($unit_rate, 2)?>%</td>			
<?
			}
?>
				<td class="tdc point" <?= $style ?>><?= number_format($slot_rate, 2)?>%</td>
<?
			if($viewmode == "viewmode" && $mode == "mode")
			{
?>
				<td class="tdc point" <?= $style ?>><?= number_format($include_jackpot_rate, 2)?>%</td>
				<td class="tdc point" <?= $style ?>><?= number_format($exclude_treat_rate, 2)?>%</td>
<?
				if($tab == "0")
				{
?>
					<!-- td class="tdc point" <?= $style ?>><?= number_format($exclude_treat_include_exchange_treat_rate, 2)?>%</td-->
<?
				}
			}
?>
				<td class="tdc" <?= $style ?>><?= number_format($playcount)?></td>
				<td class="tdc" <?= $style ?>><?= number_format($usercount)?></td>    					
    					   				
    		</tr>
<?
    }
    
    if($viewmode == "viewmode") //all
    {
    	if ($total_include_jackpot_rate <= "80" || $total_include_jackpot_rate >= "120")
    		$style = "style='color:#fb7878;font-weight:bold;'";
    	else
    		$style = "";    	
?>
			<tr onmouseover="className='tr_over'" onmouseout="className=''" style="border-top:2px double;">
				<td class="tdc point" <?= $style ?>>Total</td>
				<td class="tdr point" <?= $style ?>><?= number_format($total_moneyin)?></td>
				<td class="tdr point" <?= $style ?>><?= number_format($total_moneyout)?></td>
<?
			if($viewmode == "viewmode" && $mode == "mode")
			{
?>
				<td class="tdr point" <?= $style ?>><?= number_format($total_jackpot)?></td>
				<td class="tdr point" <?= $style ?>><?= number_format($total_game_profit)?></td>
				<td class="tdr point" <?= $style ?>><?= number_format($total_treatamount)?></td>
<?
				if($tab == "0")
				{
?>
				<!-- td class="tdc point" <?= $style ?>><?= number_format($total_exchange_treat_data)?></td-->
<?
				}
			}
			
			if($viewmode != "2")
			{
				if($mode == 5)
				{
?>
					<td class="tdc point" <?= $style ?>><?= number_format($total_moneyout - $total_moneyin)?></td>				
<?
				}
?>
					<td class="tdc point" <?= $style ?>><?= number_format($total_unit_rate, 2)?>%</td>
<?
			}
?>
				<td class="tdc point" <?= $style ?>><?= number_format($total_slot_rate, 2)?>%</td>
<?
			if($viewmode == "viewmode" && $mode == "mode")
			{
?>
				<td class="tdc point" <?= $style ?>><?= number_format($total_include_jackpot_rate, 2)?>%</td>
				<td class="tdc point" <?= $style ?>><?= number_format($total_exclude_treat_rate, 2)?>%</td>
<?
				if($tab == "0")
				{
?>
					<!-- td class="tdc point" <?= $style ?>><?= number_format($total_exclude_treat_include_exchange_treat_rate, 2)?>%</td-->
<?
				}
			}
?>
				<td class="tdc" <?= $style ?>><?= number_format($total_playcount)?></td>
				<td class="tdc" <?= $style ?>><?= number_format($total_usercount)?></td>    	
			</tr>
<?
    }
    else if($viewmode == "1") //slot mode
    {
    	$mode_data_list = [0, 3, 4, 9, 11, 17, 18, 25, 26, 27, 28, 29, 31, 33];
    		
    	$mode_str_list = ['일반모드(0)', '특수모드(3)', '튜토리얼(4)', '프리스핀-Ultra(9)', '신규 가입자 부양(11)',
    						'재방문자 부양(17)', '플래티넘 부양(18)', '비결제자(25)', 'Multi Jackpot(26)', '결제자 이탈 부양(27)',
    						'결제자(28)', '프리스핀-Daily(29)', '프리스핀 -Push(31)', '첫결제자 부양(33)'];
    	
		for($k = 0; $k < sizeof($mode_data_list); $k++)
		{
			
			$mode_num = $mode_data_list[$k];
			
			if ($total_mode_slot_rate[$mode_num] <= "80" || $total_mode_slot_rate[$mode_num] >= "120")
				$style = "style='color:#fb7878;font-weight:bold;'";
			else
				$style = "";
			
			if($total_mode_moneyin[$mode_num] == "" && $total_mode_moneyout[$mode_num] == "")
				continue;
			
			if($k == 0)
			{
?>
				<tr onmouseover="className='tr_over'" onmouseout="className=''" style="border-top:2px double;">
<?
			}
			else 
			{
?>
				<tr onmouseover="className='tr_over'" onmouseout="className=''">
<?
			}
?>
					<td class="tdc point" <?= $style ?>>Total</td>
					<td class="tdc point" <?= $style ?>><?=$mode_str_list[$k]?></td>
					<td class="tdr point" <?= $style ?>><?=number_format($total_mode_moneyin[$mode_num])?></td>
					<td class="tdr point" <?= $style ?>><?=number_format($total_mode_moneyout[$mode_num])?></td>
					<td class="tdc point" <?= $style ?>><?=number_format($total_mode_unit_rate[$mode_num], 2)?>%</td>
					<td class="tdc point" <?= $style ?>><?=number_format($total_mode_slot_rate[$mode_num], 2)?>%</td>
					<td class="tdc point" <?= $style ?>><?=number_format($total_mode_playcount[$mode_num])?></td>
					<td class="tdc point" <?= $style ?>><?=number_format($total_mode_usercount[$mode_num])?></td>					
				</tr>
<?
		}
    }
    else if($viewmode == "2") //betlevel
    {
    	sort($total_betlevel_num);

		for($k = 0; $k < sizeof($total_betlevel_num); $k++)
    	{
    		$betlevel_num = $total_betlevel_num[$k];
    		
    		if ($total_betlevel_slot_rate[$betlevel_num] <= "80" || $total_betlevel_slot_rate[$betlevel_num] >= "120")
    			$style = "style='color:#fb7878;font-weight:bold;'";
    		else
    			$style = "";
    		
			if($k == 0)
			{
?>
				<tr onmouseover="className='tr_over'" onmouseout="className=''" style="border-top:2px double;">
<?
			}
			else 
			{
?>
				<tr onmouseover="className='tr_over'" onmouseout="className=''">
<?
			}
?>
					<td class="tdc point" <?= $style ?>>Total</td>
					<td class="tdc point" <?= $style ?>><?=$total_betlevel_name[$betlevel_num]?></td>
					<td class="tdr point" <?= $style ?>><?=number_format($total_betlevel_moneyin[$betlevel_num])?></td>
					<td class="tdr point" <?= $style ?>><?=number_format($total_betlevel_moneyout[$betlevel_num])?></td>
					<td class="tdc point" <?= $style ?>><?=number_format($total_betlevel_slot_rate[$betlevel_num], 2)?>%</td>
					<td class="tdc point" <?= $style ?>><?=number_format($total_betlevel_playcount[$betlevel_num])?></td>
					<td class="tdc point" <?= $style ?>><?=number_format($total_betlevel_usercount[$betlevel_num])?></td>					
				</tr>
<?
    	}
    }
?>
			
    	</tbody>
	</table>
	</form>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    $db_main->end();
    $db_main2->end();
    $db_other->end();
    $db_analysis->end();   
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>		
	