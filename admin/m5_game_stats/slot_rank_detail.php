<?
    $top_menu = "game_stats";
    $sub_menu = "slot_rank";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    include("../common/dbconnect/db_util_redshift.inc.php");
    
   	$slottype = $_GET["slottype"];
	$term = $_GET["term"];
   	$search_start_date = $_GET["start_date"];
   	$search_end_date = $_GET["end_date"];
   	
	if ($slottype == "" || $term == "")
   		error_back("잘못된 접근입니다.");
   	
   	$db_main2 = new CDatabase_Main2();
   	$db_other = new CDatabase_Other();
   	$db_redshift = new CDatabase_Redshift();
   	
   	if($search_start_date == "")
   	{
   		$search_start_date = get_past_date(date("Y-m-d"),180,"d");
   	}
   	
   	if($search_end_date == "")
   	{
   		$sql = "SELECT e_date FROM `tbl_slot_stat_week` ORDER BY today DESC LIMIT 1";
   		$search_end_date = $db_other->getvalue($sql);
   	}
   	
	if($term == 0)
	{
		$sql_redshift_table = "t5_user_gamelog";
		$sql_game_cash_table = "tbl_game_cash_stats_daily2";
	}
	else if($term == 1)
	{
		$sql_redshift_table = "t5_user_gamelog_ios";
		$sql_game_cash_table = "tbl_game_cash_stats_ios_daily2";
	}
	else if($term == 2)
	{
		$sql_redshift_table = "t5_user_gamelog_android";
		$sql_game_cash_table = "tbl_game_cash_stats_android_daily2";
	}
	else if($term == 3)
	{
		$sql_redshift_table = "t5_user_gamelog_amazon";
		$sql_game_cash_table = "tbl_game_cash_stats_amazon_daily2";
	}
   	$pagename = "slot_rank_detail.php";
    
    $sql = "SELECT slotname FROM tbl_slot_list WHERE slottype=$slottype";
    $slotname = $db_main2->getvalue($sql);    
    
    $sql = "SELECT s_date, e_date, rank FROM tbl_slot_rank_week t1 JOIN tbl_slot_stat_week t2 ON t1.today = t2.today WHERE t1.os_type = $term AND slottype = $slottype AND '$search_start_date' <= s_date AND e_date <= '$search_end_date'";
    $rank_list = $db_other->gettotallist($sql);
    
    $sql = "SELECT today, MIN(s_date) AS s_date, SUM(CASE WHEN sex = 1 THEN 1 ELSE 0 END) AS male, SUM(CASE WHEN sex = 2 THEN 1 ELSE 0 END) AS female ".
			"FROM ( ".
  			"	SELECT ((CASE WHEN date_part(w, writedate) > 52 AND date_part(month, writedate) = 1 THEN date_part(year, dateadd(year, -1, writedate)) ".
  			"		WHEN date_part(month, writedate) = 12 AND date_part(w, writedate) = 1 THEN date_part(year, dateadd(year, 1, writedate)) ".
  			"		ELSE date_part(year, writedate) END) * 100 + date_part(w, writedate)) AS today, date(MIN(writedate)) AS s_date, useridx ".
  			"	from $sql_redshift_table ".
  			"	where useridx > 20000 AND slottype = $slottype AND '$search_start_date 00:00:00' <= writedate and writedate <= '$search_end_date 00:00:00' ".
  			"	group by ((CASE WHEN date_part(w, writedate) > 52 AND date_part(month, writedate) = 1 THEN date_part(year, dateadd(year, -1, writedate)) ".
  			"		WHEN date_part(month, writedate) = 12 AND date_part(w, writedate) = 1 THEN date_part(year, dateadd(year, 1, writedate)) ".
  			"		ELSE date_part(year, writedate) END) * 100 + date_part(w, writedate)), useridx ".
			") t1 JOIN t5_user t2 ON t1.useridx = t2.useridx ".
			"GROUP BY today ".
			"ORDER BY today ASC";
    $week_sex_list = $db_redshift->gettotallist($sql);
    
    $sql = "SELECT country, COUNT(t1.useridx) AS user_count ".
			"FROM ( ".
  			"	SELECT distinct useridx ".
  			"	FROM $sql_redshift_table ".
  			"	WHERE useridx > 20000 AND slottype = $slottype AND '$search_start_date 00:00:00' <= writedate AND writedate <= '$search_end_date 00:00:00' ".
			") t1 JOIN t5_user t2 ON t1.useridx = t2.useridx ".
			"WHERE country != '' ".
			"GROUP BY country ".
			"ORDER BY COUNT(t1.useridx) desc";
    $week_geo_list = $db_redshift->gettotallist($sql);
    
    $sql = "SELECT writedate AS today, ".
    		"	ROUND(SUM(moneyout)/SUM(moneyin)*100, 2) AS winrate_all, ".	
			"	ROUND(SUM(IF(mode IN (0,31), moneyout, 0))/SUM(IF(MODE = 0, moneyin, 0))*100, 2) AS winrate_normal ".
			"FROM $sql_game_cash_table ".
			"WHERE slottype = $slottype AND '$search_start_date' <= writedate AND writedate <= '$search_end_date' AND MODE != 9 ".
			"GROUP BY writedate";
    
    $week_winrate_list = $db_main2->gettotallist($sql);
    
    if ($slotname == "")
    	error_back("잘못된 접근입니다.");
?>
<!-- CONTENTS WRAP -->
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_date").datepicker({ });
	    $("#end_date").datepicker({ });
	});

	function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }

	google.charts.load('current', {packages: ['corechart', 'line', 'bar', 'geochart']});
	google.charts.setOnLoadCallback(drawChartRank);
	google.charts.setOnLoadCallback(drawChartSex);
	google.charts.setOnLoadCallback(drawRegionsMap);
	google.charts.setOnLoadCallback(drawChartWinRate);

	function drawChartRank()
	{
		var data_rank = google.visualization.arrayToDataTable([
			['', 'Rank'],
  <?
		for($i=0; $i<sizeof($rank_list); $i++)
		{
			$date = $rank_list[$i]["s_date"];
			$rank = $rank_list[$i]["rank"];
                                      			
			echo "['".$date."', $rank]";
                                      			
			if ($i < sizeof($rank_list))
				echo(",");
		}
?>
		]);
		
		var options_rank = {
			vAxis: {direction: -1, format: 'decimal'},
			minValue: 0,
			ticks: [0, 1, 5, 10, 15, 20],
			height: 400
		};

		var chart_rank = new google.visualization.LineChart(document.getElementById('chart_div_rank'));
		chart_rank.draw(data_rank, options_rank);
	}

    function drawChartSex() 
    {
		var data = google.visualization.arrayToDataTable([
			['', 'Male', 'Female'],
<?
		for($i=0; $i<sizeof($week_sex_list); $i++)
		{
			$date = $week_sex_list[$i]["s_date"];
			$male_count = $week_sex_list[$i]["male"];
			$female_count = $week_sex_list[$i]["female"];
			
			echo "['".$date."', $male_count, $female_count]";
			
			if ($i < sizeof($week_sex_list))
				echo(",");
		}
?>
		]);

		var options = {
          bars: 'vertical',
          vAxis: {format: 'decimal'},
          height: 400,
          colors: ['#1b9e77', '#d95f02']
        };

        var chart_sex = new google.charts.Bar(document.getElementById('chart_div_sex'));

        chart_sex.draw(data, google.charts.Bar.convertOptions(options));
	}

	function drawRegionsMap() 
	{
		var data = google.visualization.arrayToDataTable([
			['Country', 'UserCount'],
<?
		for($i=0; $i<sizeof($week_geo_list); $i++)
		{
			$country = $week_geo_list[$i]["country"];
			$user_count = $week_geo_list[$i]["user_count"];
			
			echo "['".$country."', $user_count]";
				
			if ($i < sizeof($week_geo_list))
				echo(",");
		}
?>
		]);

		var options = {};

		var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

		chart.draw(data, options);
	}

	function drawChartWinRate()
	{
		var data = google.visualization.arrayToDataTable([
			['', '기준 승률', '전체 승률', '일반 승률'],
  <?
		for($i=0; $i<sizeof($week_winrate_list); $i++)
		{
			$date = $week_winrate_list[$i]["today"];
			$winrate_all = $week_winrate_list[$i]["winrate_all"];
			$winrate_normal = $week_winrate_list[$i]["winrate_normal"];
                                      			
			echo "['".$date."', 97.5, $winrate_all, $winrate_normal]";
                                      			
			if ($i < sizeof($week_winrate_list))
				echo(",");
		}
?>
		]);
		
		var options = {
			vAxis: {format: 'decimal'},
			height: 400
		};

		var chart_winrate = new google.visualization.LineChart(document.getElementById('chart_div_winrate'));
		chart_winrate.draw(data, options);
	}
</script>
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; <?= $slotname ?> 상세 정보</div>			
			<div class="search_box">
				<input type="text" class="search_text" id="start_date" name="start_date" value="<?= $search_start_date ?>" maxlength="10" style="width:65px" readonly="readonly" onkeypress="search_press(event)" />
				~
				<input type="text" class="search_text" id="end_date" name="end_date" value="<?= $search_end_date ?>" maxlength="10" style="width:65px" readonly="readonly" onkeypress="search_press(event)" />
				&nbsp;&nbsp;&nbsp;
				<input type="hidden" id="slottype" name="slottype" value="<?= $slottype?>"/>
				<input type="hidden" id="term" name="term" value="<?= $term?>"/>
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</div>
		<!-- //title_warp -->	
		<div style="font:bold 14px dotum;">1. 슬롯 순위 추이</div>
		<div id="chart_div_rank" style="width: 1100px; height: 400px"></div>
		<br/><br/>
		<div style="font:bold 14px dotum;">2. 플레이 유저 성별 추이</div>
		<div id="chart_div_sex" style="width: 1100px; height: 400px"></div>
		<br/><br/>
		<div style="font:bold 14px dotum;">3. 플레이 유저 국가별 분포도</div>
		<div id="regions_div" style="width: 1100px; height: 400px"></div>
		<br/><br/>
		<div style="font:bold 14px dotum;">4. 승률 추이</div>
		<div id="chart_div_winrate" style="width: 1100px; height: 400px"></div>
	</form>
</div>
<!--  //CONTENTS WRAP -->

<?	
	$db_main2->end();	
	$db_other->end();
	$db_redshift->end();
	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
