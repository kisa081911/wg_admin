<?
    $top_menu = "game_stats";
    $sub_menu = "game_swp_stat";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $pagename = "game_super_wheel_party_stat.php";
    
    $search_startdate = $_GET["startdate"];
    $search_enddate = $_GET["enddate"];
    
    if($search_startdate == "" || $search_enddate == "")
    {
    	$search_startdate = date("Y-m-d",strtotime("-14 day"));
    	$search_enddate = date("Y-m-d");
    }    

    $str_useridx = 20000;
    
    if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
    {
    	$port = ":8081";
    	$str_useridx = 10000;
    }
    
    $db_main2 = new CDatabase_Main2();
    
    $sql = "SELECT t1.event_idx, MIN(writedate) AS startdate, MAX(writedate) AS finish_date, enddate, SUM(collect_amount) AS total_pot, ".
			"COUNT(IF(collect_type = 1, 1, NULL)) AS major_jackpot_cnt, SUM(IF(collect_type = 1, collect_amount, NULL)) AS major_pot,	".
			"COUNT(IF(collect_type = 2, 1, NULL)) AS minor_jackpot_cnt, SUM(IF(collect_type = 2, collect_amount, NULL)) AS minor_pot,	".
			"COUNT(IF(collect_type = 3, 1, NULL)) AS mini_jackpot_cnt, SUM(IF(collect_type = 3, collect_amount, NULL)) AS mini_pot,	".
			"SUM(current_spin) AS spin_cnt, COUNT(*) AS jackpot_cnt	".
			"FROM `tbl_user_superwheelparty_collect_log` t1 JOIN `tbl_superwheelparty_event` t2 ON t1.event_idx = t2.idx	".
			"WHERE useridx > $str_useridx AND collect_type <> 0	".
			"AND writedate BETWEEN '$search_startdate 00:00:00' AND '$search_enddate 23:59:59'	".
			"GROUP BY t1.event_idx	".
			"ORDER BY t1.event_idx DESC";
    $swp_stats = $db_main2->gettotallist($sql);
    
    $sql = "SELECT t1.event_idx as event_idx, COUNT(*) as total ".
    		"FROM `tbl_user_superwheelparty_collect_log` t1 JOIN `tbl_superwheelparty_event` t2 ON t1.event_idx = t2.idx	".
    		"WHERE useridx > $str_useridx	".
    		"AND writedate BETWEEN '$search_startdate 00:00:00' AND '$search_enddate 23:59:59'	".
    		"GROUP BY t1.event_idx	".
    		"ORDER BY t1.event_idx DESC	";
    $total_swp_stats = $db_main2->gettotallist($sql);
    
    $total_swp_user = array();
    
    for($t = 0; $t < sizeof($total_swp_stats); $t++)
    {
    	$event_idx = $total_swp_stats[$t]["event_idx"];
    	$total_user = $total_swp_stats[$t]["total"];
    	
    	$total_swp_user[$event_idx] = $total_user;
    }
    
    $sql = "SELECT event_idx, COUNT(*) AS rowcount	".
			"FROM	".
			"(	".
			"	SELECT event_idx	".
			"	FROM tbl_user_superwheelparty_collect_log	".
			"	WHERE useridx > $str_useridx AND collect_type <> 0	".
			"	AND writedate BETWEEN '$search_startdate 00:00:00' AND '$search_enddate 23:59:59'	".
			"	GROUP BY event_idx, DATE_FORMAT(writedate, '%Y-%m-%d')	".
			") t1	".
			"GROUP BY event_idx	".
			"ORDER BY event_idx DESC	".
    $rowcount_list = $db_main2->gettotallist($sql);
    
	$db_main2->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    
    $(function() {
        $("#startdate").datepicker({ });
    });

    $(function() {
        $("#enddate").datepicker({ });
    });
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
                <!-- title_warp -->
                <div class="title_wrap">
                    <div class="title"><?= $top_menu_txt ?> &gt; Super Wheel Party 통계</div>
                    <div class="search_box">
                   		<input type="input" class="search_text" id="startdate" name="startdate" style="width:75px" value="<?= $search_startdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
                   		~                        
                        <input type="input" class="search_text" id="enddate" name="enddate" style="width:75px" value="<?= $search_enddate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
                        <input type="button" class="btn_search" value="검색" onclick="search()" />
                    </div>
                </div>
                <!-- //title_warp -->
				<table class="tbl_list_basic1" style="width:1300px">
	                <colgroup>
	                	<col width="70">
						<col width="150">
						<col width="">
						<col width="">
						<col width="">
						<col width="">
						<col width="">
						<col width="">
						<col width="">
						<col width="">
	                </colgroup>
	                
	                <thead>
	                	<tr>
	                		<th class="tdc" rowspan="2">회차</th>
	                		<th class="tdc" rowspan="2">획득 시작 일자</th>
	                		<th class="tdc" rowspan="2">획득 마지막 일자</th>
	                		<th class="tdc" rowspan="2">종료 일자</th>
	                		<th class="tdc" rowspan="2">총 시도 수</th>
	                		<th class="tdr" rowspan="2">총 지급액</th>
	                		<th class="tdc" colspan="2">Major</th>
	                		<th class="tdc" colspan="2">Minor</th>
	                		<th class="tdc" colspan="2">Mini</th>
	                		<th class="tdr" rowspan="2">평균 스핀수</th>
	                	</tr>
		                <tr>
		                	<th class="tdr">개수</th>
		                	<th class="tdr">지급액</th>
		                	<th class="tdr">개수</th>
		                	<th class="tdr">지급액</th>
		                	<th class="tdr">개수</th>
		                	<th class="tdc">지급액</th>
		                </tr>
	                </thead>
	                
	                <tbody>
<?
	$sum_total_swp = 0;
	$sum_total_pot = 0;
	
	$sum_major_jackpot_cnt = 0;
	$sum_major_pot = 0;
	
	$sum_minor_jackpot_cnt = 0;
	$sum_minor_pot = 0;
	
	$sum_mini_jackpot_cnt = 0;
	$sum_mini_pot = 0;
	
	$sum_total_jackpot_cnt = 0;
	$sum_spin_cnt = 0;
	
	for($i=0; $i<sizeof($swp_stats); $i++)
	{
		$partyidx = $swp_stats[$i]["event_idx"];
		$startdate = $swp_stats[$i]["startdate"];
		$finish_date = $swp_stats[$i]["finish_date"];
		$enddate = $swp_stats[$i]["enddate"];
		$total_swp = $total_swp_user[$partyidx];
		
		$total_pot = $swp_stats[$i]["total_pot"];
		
		$major_jackpot_cnt = $swp_stats[$i]["major_jackpot_cnt"];
		$major_pot = $swp_stats[$i]["major_pot"];
		
		$minor_jackpot_cnt = $swp_stats[$i]["minor_jackpot_cnt"];
		$minor_pot = $swp_stats[$i]["minor_pot"];
		
		$mini_jackpot_cnt = $swp_stats[$i]["mini_jackpot_cnt"];
		$mini_pot = $swp_stats[$i]["mini_pot"];
		
		$spin_cnt = $swp_stats[$i]["spin_cnt"];
		$jackpot_cnt = $swp_stats[$i]["jackpot_cnt"];
		
		$avg_spin_cnt = number_format($spin_cnt / $jackpot_cnt, 2);  
		
		$sum_total_swp += $total_swp;
		$sum_total_pot += $total_pot;
		
		$sum_major_jackpot_cnt += $major_jackpot_cnt;
		$sum_major_pot += $major_pot;
		
		$sum_minor_jackpot_cnt += $minor_jackpot_cnt;
		$sum_minor_pot += $minor_pot;
		
		$sum_mini_jackpot_cnt += $mini_jackpot_cnt;
		$sum_mini_pot += $mini_pot;
		
		$sum_total_jackpot_cnt += $jackpot_cnt;
		$sum_spin_cnt += $spin_cnt;
?>
						<tr onmouseover="className='tr_over'" onmouseout="className=''">
							<td class="tdc point"><?= $partyidx ?></td>
							<td class="tdc point"><?= $startdate ?></td>
							<td class="tdc point"><?= $finish_date ?></td>
							<td class="tdc point"><?= $enddate ?></td>
							<td class="tdr point"><?= $total_swp ?></td>
							<td class="tdr point"><?= number_format($total_pot) ?></td>
							
							<td class="tdr"><?= number_format($major_jackpot_cnt)?>(<?=($total_swp == 0) ? 0 : (round($major_jackpot_cnt / $total_swp * 100,2)) ?>%)</td>
							<td class="tdr"><?= number_format($major_pot) ?></td>
							
							<td class="tdr"><?= number_format($minor_jackpot_cnt)?>(<?=($total_swp == 0) ? 0 : (round($minor_jackpot_cnt / $total_swp * 100,2)) ?>%)</td>
							<td class="tdr"><?= number_format($minor_pot) ?></td>
							
							<td class="tdr"><?= number_format($mini_jackpot_cnt)?>(<?=($total_swp == 0) ? 0 : (round($mini_jackpot_cnt / $total_swp * 100,2)) ?>%)</td>
							<td class="tdr"><?= number_format($mini_pot) ?></td>
							
							<td class="tdr"><?= $avg_spin_cnt ?></td>
						</tr>

						
<?
	}
	
	$sum_avg_spin_cnt = number_format($sum_spin_cnt / $sum_total_jackpot_cnt, 2);
?>
						<tr onmouseover="className='tr_over'" onmouseout="className=''" style="border-top:1px double;">
							<td class="tdc point_title" colspan="4">Total</td>							
							<td class="tdr point"><?= number_format($sum_total_swp) ?></td>
							<td class="tdr point"><?= number_format($sum_total_pot) ?></td>
							
							<td class="tdr point"><?= number_format($sum_major_jackpot_cnt) ?>(<?=($sum_total_swp == 0) ? 0 : (round($sum_major_jackpot_cnt / $sum_total_swp * 100,2)) ?>%)</td>
							<td class="tdr point"><?= number_format($sum_major_pot) ?></td>
							
							<td class="tdr point"><?= number_format($sum_minor_jackpot_cnt) ?>(<?=($sum_total_swp == 0) ? 0 : (round($sum_minor_jackpot_cnt / $sum_total_swp * 100,2)) ?>%)</td>
							<td class="tdr point"><?= number_format($sum_minor_pot) ?></td>
							
							<td class="tdr point"><?= number_format($sum_mini_jackpot_cnt) ?>(<?=($sum_total_swp == 0) ? 0 : (round($sum_mini_jackpot_cnt / $sum_total_swp * 100,2)) ?>%)</td>
							<td class="tdr point"><?= number_format($sum_mini_pot) ?></td>
							
							<td class="tdr point"><?= $sum_avg_spin_cnt ?></td>
						</tr>
	                </tbody>
                </table>
            </form>
            
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>    
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>