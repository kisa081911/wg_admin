<?
    $top_menu = "game_stats";
    $sub_menu = "game_jackpot_save_amount_highroller_detail";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $tab = ($_GET["tab"] == "") ? "0" :$_GET["tab"];
    $pagename = "game_jackpot_save_amount_highroller_detail.php";
    
    if ($tab == "")
        error_back("잘못된 접근입니다.");        
    
    $db_main2 = new CDatabase_Main2();
    $db_game = new CDatabase_Game();
    
    $sql = "SELECT t1.slottype, slotname FROM tbl_jackpot_high t1 JOIN tbl_slot_list t2 ON t1.slottype = t2.slottype;";
    $slotlist = $db_game->gettotallist($sql);    ;
	
	$slostartlist = array(array("slottype" => "0", "slotname" => "ALL"));
	$slotlist = array_merge($slostartlist, $slotlist);

	if($tab == "0")
	{
		
		$sql = "SELECT slottype, jackpot ".
				"FROM tbl_jackpot_high WHERE slottype NOT IN (SELECT slottype FROM tbl_jackpot_high WHERE multi_jackpot = 1) ".
				"UNION ALL ".
				"SELECT slottype, jackpot1 AS jackpot ".
				"FROM tbl_jackpot_high WHERE slottype IN (SELECT slottype FROM tbl_jackpot_high WHERE multi_jackpot = 1) ".					
				"ORDER BY slottype ASC";		
		$jackpot_money_list = $db_game->gettotallist($sql);
	}
	else
	{
		$sql = "SELECT multi_jackpot FROM tbl_jackpot_high WHERE slottype = $tab";
		$multi_jackpot_data = $db_game->getvalue($sql);
		
		if($multi_jackpot_data == "1")
		{
			$sql = "SELECT slottype, jackpot1 AS jackpot ".
					"FROM tbl_jackpot_high WHERE slottype = $tab ".					
					"ORDER BY slottype ASC";
			
			$jackpot_money_list = $db_game->gettotallist($sql);
		}
		else
		{
			$sql = "SELECT slottype, jackpot ".
					"FROM tbl_jackpot_high WHERE slottype = $tab ".
					"ORDER BY slottype ASC";
			
			$jackpot_money_list = $db_game->gettotallist($sql);
		}
	}

?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    
    function tab_change(tab)
    {
        var search_form = document.search_form;
        search_form.tab.value = tab;
        search_form.submit();
    }
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
                <!-- title_warp -->
                <div class="title_wrap">
                    <div class="title"><?= $top_menu_txt ?> &gt; 잭팟상세현황 - 적립급 현황(하이롤러)</div>
                    <div class="search_box">
                    	<input type="hidden" name="tab" id="tab" value="<?= $tab ?>" />
                    </div>
                </div>
                <!-- //title_warp -->            
                
                <ul class="tab">
<?
	for ($i=0; $i<sizeof($slotlist); $i++)
	{
		$slottype1 = $slotlist[$i]["slottype"];
		$slotname1 = str_replace(" Slot", "", $slotlist[$i]["slotname"]);
?>	
                <li id="tab_<?= $slottype1 ?>" class="<?= ($tab == $slottype1) ? "select" : "" ?>" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('<?= $slottype1 ?>')"><?= $slotname1 ?></li>
<?
	}
?>                
                </ul>
                <div class="h2_title pt20"></div>
                <table class="tbl_list_basic1">
                <colgroup>
                    <col width="">                    
                    <col width="">                    
                </colgroup>
                <thead>
                    <tr>
						<th class="tdc">Slot Name</th>						
                        <th class="tdc">Remain Jackpot</th>                        
                    </tr>
                </thead>
                <tbody>
<?
    $slottype = "";
    
    for ($i=0; $i<sizeof($jackpot_money_list); $i++)
    {
    	$slottype = $jackpot_money_list[$i]["slottype"];        
        $jackpot = $jackpot_money_list[$i]["jackpot"];
        
        
        for($j=0; $j<sizeof($slotlist); $j++)
        {
        	if($slotlist[$j]["slottype"] == $slottype)
       	 	{
        		$slotname = $slotlist[$j]["slotname"];
        		break;
        	}
			else
        	{
        		$slotname = "Unkown";
        	}
        }		
?>
					<tr>
                        <td class="tdc point"><?= $slotname ?></td>
                        <td class="tdc point"><?= make_price_format($jackpot) ?></td>                        
                    </tr>
<?
    }
?>
				</tbody>
                </table>
            </form>
        </div>
        <!--  //CONTENTS WRAP -->
<?
    $db_main2->end();
    $db_game->end();
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>