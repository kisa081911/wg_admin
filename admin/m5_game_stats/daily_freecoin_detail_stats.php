<?
$top_menu = "game_stats";
$sub_menu = "daily_freecoin_detail_stats";

include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

$today = date("Y-m-d");

$os_type = ($_GET["os_type"] == "") ? "4" :$_GET["os_type"];
$search_start_createdate = $_GET["start_createdate"];
$search_end_createdate = $_GET["end_createdate"];

if($search_start_createdate == "")
    $search_start_createdate = date("Y-m-d", strtotime("-3 day"));
    
    if($search_end_createdate == "")
        $search_end_createdate = $today;
        
        if($os_type == "0")
        {
            $os_txt = "Web";
            $os_type_sql = "category = 0 ";
            
        }
        else if($os_type == "1")
        {
            $os_txt = "IOS";
            $os_type_sql = "category = 1 ";
        }
        else if($os_type == "2")
        {
            $os_txt = "Android";
            $os_type_sql = "category = 2 ";
        }
        else if($os_type == "3")
        {
            $os_txt = "Amazon";
            $os_type_sql = "category = 3 ";
        }
        else if($os_type == "4")
        {
            $os_txt = "Total";
            $os_type_sql = "1=1 ";
        }
        $db_main2 = new CDatabase_Main2();
        $db_analysis = new CDatabase_Analysis();
        
        $sql = "SELECT today, t2.name, t1.type, SUM(usercount) AS usercount, SUM(freeamount) AS freeamount, MAX(max_amount) AS max_amount ".
            "FROM `tbl_inbox_freecoin_daily` t1 JOIN `tbl_total_freecoin_type` t2 ON t1.type=t2.type AND t1.inbox_type=t2.inbox_type	".
            "WHERE $os_type_sql AND today BETWEEN '$search_start_createdate' AND '$search_end_createdate' AND t1.type != 22 group by today, t1.type, t2.inbox_type ORDER BY today DESC, t1.type ASC;";
        $freecoin_data = $db_analysis->gettotallist($sql);
        ?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});

	function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		var total = document.getElementById("type_total");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
			total.className="btn_schedule";
		}
		else if (type == "4")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="daily_freecoin_detail_stats.php">
	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
		<input type="button" class="<?= ($os_type == "4") ? "btn_schedule_select" : "btn_schedule" ?>" value="Total" id="type_total" onclick="change_os_type('4')"    />
		<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
		<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
		<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
		<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
	</span>
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 무료 코인 상세 통계(<?= $os_txt ?>)</div>
		<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />
		<div class="search_box">
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	
	<div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">                
                <col width="">
                <col width="">
            </colgroup>
            <thead>
            <tr>
                <th>날짜</th>
                <th class="tdc">종류</th>
                <th class="tdc">전체금액</th>                
                <th class="tdc">유저수</th>
                <th class="tdc">평균</th>
                <th class="tdc">$환산</th>
                <th class="tdc">1인최고금액</th>
            </tr>
            </thead>
            <tbody>
<?
			$day_total_amount = 0;
			$day_usercount = 0;
			$day_max_amount = 0;

			$total_platinum_wheel_amount = 0;			
			$sum_total_amount = 0;			
			$sum_usercount = 0;
			$sum_max_amount = 0;			

			for($i=0; $i<sizeof($freecoin_data); $i++)
			{
				$today = $freecoin_data[$i]["today"];
				$type = $freecoin_data[$i]["type"];
				$name = $freecoin_data[$i]["name"];
				$usercount = $freecoin_data[$i]["usercount"];
				$free_amount = $freecoin_data[$i]["freeamount"];
				$max_amount = $freecoin_data[$i]["max_amount"];

				$avg_amount = round($free_amount/$usercount);
				
				$coinsper = round($avg_amount/600000, 2);
				
				if($i == 0 || $today != $freecoin_data[$i-1]["today"])
				{
					if($os_type == 4)
					{
					    if($type == 22)
					        $total_platinum_wheel_amount = $free_amount;
						$sql = "SELECT COUNT(*) ".
							   "FROM ".
							   "( ".
							   "  SELECT COUNT(*) FROM tbl_inbox_freecoin_daily WHERE today BETWEEN '$today' AND '$today' AND type != 22 GROUP BY type, inbox_type ".
							   ") t1";
						
						$platinum_wheel_sql = "SELECT DATE_FORMAT(writedate,'%Y-%m-%d'), more_bonus, COUNT(DISTINCT useridx) AS cnt, SUM(award_amount)  AS amount ,MAX(award_amount) AS max_amount
                                               FROM (
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_0 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_1 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_2 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_3 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_4 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_5 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_6 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_7 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_8 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	UNION ALL
                                                	SELECT * FROM tbl_platinum_wheel_reward_log_9 WHERE DATE_FORMAT(writedate,'%Y-%m-%d') = '$today' AND useridx > 20000
                                                	) t1
                                               GROUP BY more_bonus, DATE_FORMAT(writedate,'%Y-%m-%d') ORDER BY DATE_FORMAT(writedate,'%Y-%m-%d')";
						$platinum_wheel_arr = $db_main2->gettotallist($platinum_wheel_sql);
					}
					else
						$sql = "SELECT COUNT(*) FROM tbl_inbox_freecoin_daily WHERE $os_type_sql  AND today BETWEEN '$today' AND '$today'";
					
					$time_bonus_sql = "SELECT DATE_FORMAT(collect_date,'%Y-%m-%d'),  COUNT(DISTINCT t1.useridx) AS cnt, SUM(amount)  AS amount ,MAX(amount) AS max_amount FROM tbl_user_timebonus_info t1
                                            JOIN (
                                            	SELECT * FROM `tbl_user_freecoin_log_0` WHERE TYPE = 3 AND writedate >= '2019-03-21 00:00:00' AND $os_type_sql AND useridx > 20000
                                            	UNION ALL
                                            	SELECT * FROM `tbl_user_freecoin_log_1` WHERE TYPE = 3 AND writedate >= '2019-03-21 00:00:00' AND $os_type_sql AND useridx > 20000
                                            	UNION ALL
                                            	SELECT * FROM `tbl_user_freecoin_log_2` WHERE TYPE = 3 AND writedate >= '2019-03-21 00:00:00' AND $os_type_sql AND useridx > 20000
                                            	UNION ALL
                                            	SELECT * FROM `tbl_user_freecoin_log_3` WHERE TYPE = 3 AND writedate >= '2019-03-21 00:00:00' AND $os_type_sql AND useridx > 20000
                                            	UNION ALL
                                            	SELECT * FROM `tbl_user_freecoin_log_4` WHERE TYPE = 3 AND writedate >= '2019-03-21 00:00:00' AND $os_type_sql AND useridx > 20000
                                            	UNION ALL
                                            	SELECT * FROM `tbl_user_freecoin_log_5` WHERE TYPE = 3 AND writedate >= '2019-03-21 00:00:00' AND $os_type_sql AND useridx > 20000
                                            	UNION ALL
                                            	SELECT * FROM `tbl_user_freecoin_log_6` WHERE TYPE = 3 AND writedate >= '2019-03-21 00:00:00' AND $os_type_sql AND useridx > 20000
                                            	UNION ALL
                                            	SELECT * FROM `tbl_user_freecoin_log_7` WHERE TYPE = 3 AND writedate >= '2019-03-21 00:00:00' AND $os_type_sql AND useridx > 20000
                                            	UNION ALL
                                            	SELECT * FROM `tbl_user_freecoin_log_8` WHERE TYPE = 3 AND writedate >= '2019-03-21 00:00:00' AND $os_type_sql AND useridx > 20000
                                            	UNION ALL
                                            	SELECT * FROM `tbl_user_freecoin_log_9` WHERE TYPE = 3 AND writedate >= '2019-03-21 00:00:00' AND $os_type_sql AND useridx > 20000
                                            ) t2 
                                            ON t1.useridx = t2.useridx
                                            WHERE  collect_date != '0000-00-00 00:00:00'
                                            and DATE_FORMAT(writedate,'%Y-%m-%d') = '$today'
                                            GROUP BY DATE_FORMAT(writedate,'%Y-%m-%d')";
					$time_bonus_arr = $db_main2->gettotallist($time_bonus_sql);
					
					$datecount = $db_analysis->getvalue($sql);
					if(sizeof($time_bonus_arr) > 0 )
					{
						$datecount = $datecount+1;
					}
					$datecount = $datecount;
					
					if($os_type == 4)
					{
					    $datecount = $datecount+1+ sizeof($platinum_wheel_arr);
					}
				}
				
				$sum_total_amount += $free_amount;
				$sum_usercount +=$usercount;
				$sum_max_amount +=$max_amount;

				$day_total_amount += $free_amount;
				$day_usercount +=$usercount;
				$day_max_amount +=$max_amount;			
?>
				<tr>
<?
				if($i == 0 || $today != $freecoin_data[$i-1]["today"])
				{
?>				
					<td class="tdc point_title" rowspan="<?= $datecount?>"><?= $today ?></td>
<?
				} 
?>					
					<td class="tdc"><?= $name ?></td>
					<td class="tdc"><?= number_format($free_amount) ?></td>
					<td class="tdc"><?= number_format($usercount) ?></td>
					<td class="tdc"><?= number_format($avg_amount) ?></td>
					<td class="tdc">$ <?= $coinsper ?></td>
					<td class="tdc"><?= number_format($max_amount) ?></td>					
				</tr>
						
<?
				if(($i != 0) && ($today != $freecoin_data[$i+1]["today"]))
				{
				    $sql="SELECT * FROM tbl_total_user_freecoin_daily WHERE category = $os_type AND today ='$today';";
				    $total_user_freecoin_daily = $db_analysis->getarray($sql);
				    
				    $day_usercount  = ($total_user_freecoin_daily["usercount"] == "")? $day_usercount : $total_user_freecoin_daily["usercount"];
				    $day_max_amount = ($total_user_freecoin_daily["maxfreeamount"] == "")? $day_max_amount : $total_user_freecoin_daily["maxfreeamount"];
				    
					$day_avg_amount = round($day_total_amount/$day_usercount);
					$day_coinsper = round($day_avg_amount/600000, 2);
					
					if(sizeof($time_bonus_arr) > 0)
					{
					    for($u=0; $u<sizeof($time_bonus_arr); $u++)
					    {
					        $cnt = $time_bonus_arr[$u]["cnt"];
					        $amount = $time_bonus_arr[$u]["amount"];
					        $max_amount = $time_bonus_arr[$u]["max_amount"];
					        
					        $avg_amount = round($amount/$cnt);
					        $coinsper = round($avg_amount/600000, 2);
					        
					        ?>
						<td class="tdc"><?= "Time Bonus(Test)" ?></td>
        					<td class="tdc"><?= number_format($amount) ?></td>
        					<td class="tdc"><?= number_format($cnt) ?></td>
        					<td class="tdc"><?= number_format($avg_amount) ?></td>
        					<td class="tdc">$ <?= $coinsper ?></td>
        					<td class="tdc"><?= number_format($max_amount) ?></td>	
        				</tr>
<?
				        }
				    }
					if(sizeof($platinum_wheel_arr) > 0)
					{
					    for($u=0; $u<sizeof($platinum_wheel_arr); $u++)
					    {
					        $more_bonus = $platinum_wheel_arr[$u]["more_bonus"];
					        $cnt = $platinum_wheel_arr[$u]["cnt"];
					        $amount = $platinum_wheel_arr[$u]["amount"];
					        $max_amount = $platinum_wheel_arr[$u]["max_amount"];
					        
					        $avg_amount = round($amount/$cnt);
					        $coinsper = round($avg_amount/600000, 2);
					        
					        ?>
						<td class="tdc"><?= "Platinum Wheel More ".$more_bonus."%" ?></td>
        					<td class="tdc"><?= number_format($amount) ?></td>
        					<td class="tdc"><?= number_format($cnt) ?></td>
        					<td class="tdc"><?= number_format($avg_amount) ?></td>
        					<td class="tdc">$ <?= $coinsper ?></td>
        					<td class="tdc"><?= number_format($max_amount) ?></td>	
        				</tr>
<?
				        }
				    }
?>
					<tr>
						<td class="tdc point">Total</td>
						<td class="tdc"><?= number_format($day_total_amount) ?></td>
						<td class="tdc"><?= number_format($day_usercount) ?></td>
						<td class="tdc"><?= number_format($day_avg_amount) ?></td>
						<td class="tdc">$ <?= $day_coinsper ?></td>
						<td class="tdc"><?= number_format($day_max_amount) ?></td>						
					</tr>					
<?
					$day_total_amount = 0;					
					$day_usercount = 0;
					$day_max_amount = 0;					
				}
			}
			
			$sum_avg_amount = round($sum_total_amount/$sum_usercount);
			$sum_coinsper = round($sum_avg_amount/600000, 2);			
?>
			<tr>
				<td class="tdc point" colspan=2>Total</td>
				<td class="tdc"><?= number_format($sum_total_amount) ?></td>
				<td class="tdc"><?= number_format($sum_usercount) ?></td>
				<td class="tdc"><?= number_format($sum_avg_amount) ?></td>
				<td class="tdc">$ <?= $sum_coinsper ?></td>
				<td class="tdc"><?= number_format($sum_max_amount) ?></td>	
			</tr>
	
			</tbody>
            </table>
     </div>
        
     </form>
	
</div>
<!--  //CONTENTS WRAP -->
<?
    $db_main2->end();
	$db_analysis->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
