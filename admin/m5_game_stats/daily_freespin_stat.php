<?
	$top_menu = "game_stats";
	$sub_menu = "daily_freespin_stat";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$os_type = ($_GET["os_type"] == "") ? "4" :$_GET["os_type"];
	$search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    
    if($search_start_createdate == "")
    	$search_start_createdate = date("Y-m-d", strtotime("-3 day"));
    
    if($search_end_createdate == "")
    	$search_end_createdate = $today;
        
    $tail = "";
    
    if($os_type == "0")
    {
    	$os_txt = "Web";
    	$os_type_sql = "AND os_type = 0 ";
    	
    }
    else if($os_type == "1")
    {
    	$os_txt = "MOBILE";
    	$os_type_sql = "AND os_type = 1 ";
    }
    else if($os_type == "2")
    {
    	$os_txt = "Android";
    	$os_type_sql = "AND os_type = 2 ";
    }
    else if($os_type == "3")
    {
    	$os_txt = "Amazon";
    	$os_type_sql = "AND os_type = 3 ";
    }
    else if($os_type == "4")
    {
    	$os_txt = "Total";
    }
	
	$db_main2 = new CDatabase_Main2();
	
	$sql = " SELECT *,(SELECT COUNT(*) FROM tbl_daily_freespin_stat WHERE today = t1.today AND days = t1.days  $os_type_sql) AS daysrowcount,(SELECT COUNT(*) FROM tbl_daily_freespin_stat WHERE today = t1.today $os_type_sql) AS todaysrowcount FROM tbl_daily_freespin_stat t1 ".
           " WHERE today BETWEEN '$search_start_createdate' AND '$search_end_createdate' $os_type_sql ;";
	$freespin_data = $db_main2->gettotallist($sql);
	
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});

	function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");

		var total = document.getElementById("type_total");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";

			total.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			
			total.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			
			total.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			
			total.className="btn_schedule";
		}
		else if (type == "4")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
		
			total.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="daily_freespin_stat.php">
	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
		<input type="button" class="<?= ($os_type == "4") ? "btn_schedule_select" : "btn_schedule" ?>" value="Total" id="type_total" onclick="change_os_type('4')"    />
		<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
		<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="MOBILE" id="type_ios" onclick="change_os_type('1')" />
	
	</span>
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; DailyFreespin 통계(<?= $os_txt ?>)</div>
		<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />
		<div class="search_box">
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	
	<div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">                
            </colgroup>
            <thead>
            <tr>
                <th>날짜</th>
                <th class="tdc">일자</th>
                <th class="tdc">슬롯 번호</th>                
                <th class="tdc">참여자 수</th>
                <th class="tdc">완료</th>
                <th class="tdc">도중 포기</th>
                <th class="tdc">지급 금액</th>
                <th class="tdc">1인당 지급 금액</th>
                <th class="tdc">완료자 금액</th>
                <th class="tdc">완료자 1인당 금액</th>
            </tr>
            </thead>
            <tbody>
<?
            $sum_total_count = 0;
            $sum_finish_user_count = 0;
            $sum_notfinish_user_count = 0;
            $sum_total_winamount = 0;
            $sum_finish_user_winamount = 0;
            $sum_notfinish_user_winamount = 0;

			for($i=0; $i<sizeof($freespin_data); $i++)
			{
			    $today = $freespin_data[$i]["today"];
			    $days = $freespin_data[$i]["days"];
			    $slottype = $freespin_data[$i]["slottype"];
			    $total_count = $freespin_data[$i]["total_count"];
			    $finish_user_count = $freespin_data[$i]["finish_user_count"];
			    $notfinish_user_count = $freespin_data[$i]["notfinish_user_count"];
			    $total_winamount = $freespin_data[$i]["total_winamount"];
			    $avg_winamount = $freespin_data[$i]["avg_winamount"];
			    $finish_user_winamount = $freespin_data[$i]["finish_user_winamount"];
			    $notfinish_user_winamount = $freespin_data[$i]["notfinish_user_winamount"];
			    $daysrowcount = $freespin_data[$i]["daysrowcount"];
			    $todaysrowcount = $freespin_data[$i]["todaysrowcount"];
			    
			    $sum_total_count += $total_count;
			    $sum_finish_user_count += $finish_user_count;
			    $sum_notfinish_user_count += $notfinish_user_count;
			    $sum_total_winamount += $total_winamount;
			    $sum_finish_user_winamount += $finish_user_winamount;
			    $sum_notfinish_user_winamount += $notfinish_user_winamount;
			    
?>
				<tr>
<?
if($i == 0 || $today != $freespin_data[$i-1]["today"])
				{
?>				
					<td class="tdc point_title" rowspan="<?= $todaysrowcount+1?>"><?= $today ?></td>
<?
				} 
?>					
<?
if($i == 0 || $days != $freespin_data[$i-1]["days"])
				{
?>				
					<td class="tdc point_title" rowspan="<?= $daysrowcount?>"><?= $days ?></td>
<?
				} 
?>					
                    <td class="tdc"><?= $slottype?></td>
                    <td class="tdc"><?= number_format($total_count)?></td>
                    <td class="tdc"><?= number_format($finish_user_count)?></td>
                    <td class="tdc"><?= number_format($notfinish_user_count)?></td>
                    <td class="tdc"><?= number_format($total_winamount)?></td>
                    <td class="tdc"><?= number_format($avg_winamount)?></td>
                    <td class="tdc"><?= number_format($finish_user_winamount)?></td>
                    <td class="tdc"><?= number_format($notfinish_user_winamount)?></td>
				</tr>
						
<?
                if(($i != 0) && ($today != $freespin_data[$i+1]["today"]))
				{
?>
					<tr>
						<td class="tdc point" colspan="2">Total</td>
						<td class="tdc point"><?= number_format($sum_total_count) ?></td>
						<td class="tdc point"><?= number_format($sum_finish_user_count) ?></td>
						<td class="tdc point"><?= number_format($sum_notfinish_user_count) ?></td>
						<td class="tdc point"><?= number_format($sum_total_winamount) ?></td>
						<td class="tdc point"><?= ($sum_total_count == 0) ? "0" : round($sum_total_winamount/$sum_total_count , 2) ?></td>
						<td class="tdc point"><?= number_format($sum_finish_user_winamount) ?></td>
						<td class="tdc"><?= ($sum_finish_user_count == 0) ? "0" : round($sum_finish_user_winamount/$sum_finish_user_count , 2) ?></td>						
					</tr>					
<?
                    $sum_total_count = 0;
                    $sum_finish_user_count = 0;
                    $sum_notfinish_user_count = 0;
                    $sum_total_winamount = 0;
                    $sum_finish_user_winamount = 0;
                    $sum_notfinish_user_winamount = 0;

				}
			}
?>
			
	
			</tbody>
            </table>
     </div>
        
     </form>
	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main2->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>