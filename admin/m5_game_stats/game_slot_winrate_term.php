<?
    $top_menu = "game_stats";
    $sub_menu = "game_slot_winrate_term";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $os_type = ($_GET["os_type"] == "") ? "0" :$_GET["os_type"];
    
    $db_main2 = new CDatabase_Main2();
    
    $tail = " WHERE 1=1 ";
    
    if($os_type == "0")
    {
    	$table = "tbl_game_cash_stats_daily2";
    	$os_txt = "Web";
    }
    else if($os_type == "1")
    {
    	$tail .= " AND ios = 1 ";
    	$table = "tbl_game_cash_stats_ios_daily2";
    	$os_txt = "IOS";
    }
    else if($os_type == "2")
    {
    	$tail .= " AND android = 1 ";
    	$table = "tbl_game_cash_stats_android_daily2";
    	$os_txt = "Android";
    }
    else if($os_type == "3")
    {
    	$tail .= " AND amazon = 1 ";
    	$table = "tbl_game_cash_stats_amazon_daily2";
    	$os_txt = "Amazon";
    }

   	$pagename = "game_slot_winrate_term.php";
    
    $sql = "SELECT slottype, slotname FROM tbl_slot_list";
    $slottype_list = $db_main2->gettotallist($sql);
    
    $yesterday = date("Y-m-d",strtotime("-1 days"));
    $yesterday_sub_7day = date("Y-m-d",strtotime("-7 days"));
    
    //일반 - 최근 1주일
    $normal_recently_7day = "SELECT slottype, FLOOR(SUM(moneyin)/1000000) AS moneyin, FLOOR(SUM(moneyout)/1000000) AS moneyout, SUM(playcount) AS playcount, ROUND(SUM(moneyout)/SUM(moneyin)*100,2) AS winrate ".
							"FROM `$table`	". 
							"WHERE mode IN (0,31) AND betlevel < 10 AND writedate BETWEEN '$yesterday_sub_7day' AND '$yesterday' GROUP BY slottype ORDER BY moneyin DESC;";
    
    $normal_recently_7day_data = $db_main2->gettotallist($normal_recently_7day);
    
    // unit 승률
    $normal_7day_unit_rate_sql = "SELECT slottype, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unit_rate ".
    		"FROM	".
    		"	(	".
    		"		SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout	".
    		"		FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
    		"		WHERE  mode IN (0,31) AND t1.betlevel < 10 AND writedate BETWEEN '$yesterday_sub_7day' AND '$yesterday' ".
    		"		GROUP BY slottype, betlevel".
    		") t3 GROUP BY slottype";
    
	$normal_7day_unit_rate_list = $db_main2->gettotallist($normal_7day_unit_rate_sql);
    	
    $normal_7day_unit_rate_array = array();
    
    for($e = 0; $e < sizeof($normal_7day_unit_rate_list); $e++)
    {
	    $slottype = $normal_7day_unit_rate_list[$e]["slottype"];
	    $unit_rate = $normal_7day_unit_rate_list[$e]["unit_rate"];
	    
	    $normal_7day_unit_rate_array[$slottype] = $unit_rate;
    }
    
    //일반 - 어제
    $normal_yesterday = "SELECT slottype, FLOOR(SUM(moneyin)/1000000) AS moneyin, FLOOR(SUM(moneyout)/1000000) AS moneyout, SUM(playcount) AS playcount, ROUND(SUM(moneyout)/SUM(moneyin)*100,2) AS winrate ".
    					"FROM `$table`	".
    					"WHERE mode IN (0,31) AND betlevel < 10 AND writedate = '$yesterday' GROUP BY slottype ORDER BY moneyin DESC;";
    
    $normal_yesterday_data = $db_main2->gettotallist($normal_yesterday);
    
    // unit 승률
    $normal_yesterday_unit_rate_sql = "SELECT slottype, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unit_rate ".
		    		"FROM	".
		    		"	(	".
		    		"		SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout	".
		    		"		FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
		    		"		WHERE  mode IN (0,31) AND t1.betlevel < 10 AND writedate = '$yesterday' ".
		    		"		GROUP BY slottype, betlevel".
		    		") t3 GROUP BY slottype";
    
    $normal_yesterday_unit_rate_list = $db_main2->gettotallist($normal_yesterday_unit_rate_sql);
     
    $normal_yesterday_unit_rate_array = array();
    
    for($e = 0; $e < sizeof($normal_yesterday_unit_rate_list); $e++)
    {
    	$slottype = $normal_yesterday_unit_rate_list[$e]["slottype"];
    	$unit_rate = $normal_yesterday_unit_rate_list[$e]["unit_rate"];
	  
    	$normal_yesterday_unit_rate_array[$slottype] = $unit_rate;
    }
        
    //하이롤러 - 최근 1주일
    $highroller_recently_7day = "SELECT slottype, FLOOR(SUM(moneyin)/1000000) AS moneyin, FLOOR(SUM(moneyout)/1000000) AS moneyout, SUM(playcount) AS playcount, ROUND(SUM(moneyout)/SUM(moneyin)*100,2) AS winrate ".
    						"FROM `$table`	".
    						"WHERE mode IN (0,31) AND betlevel >= 10 AND writedate BETWEEN '$yesterday_sub_7day' AND '$yesterday' GROUP BY slottype ORDER BY moneyin DESC;";
    
    $highroller_recently_7day_data = $db_main2->gettotallist($highroller_recently_7day);
    
    // unit 승률
    $highroller_7day_unit_rate_sql = "SELECT slottype, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unit_rate ".
						    		"FROM	".
						    		"	(	".
						    		"		SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout	".
						    		"		FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
						    		"		WHERE  mode IN (0,31) AND t1.betlevel >= 10 AND writedate BETWEEN '$yesterday_sub_7day' AND '$yesterday' ".
						    		"		GROUP BY slottype, betlevel".
						    		") t3 GROUP BY slottype";
    
    $highroller_7day_unit_rate_list = $db_main2->gettotallist($highroller_7day_unit_rate_sql);
     
    $highroller_7day_unit_rate_array = array();
    
    for($e = 0; $e < sizeof($highroller_7day_unit_rate_list); $e++)
    {
    	$slottype = $highroller_7day_unit_rate_list[$e]["slottype"];
    	$unit_rate = $highroller_7day_unit_rate_list[$e]["unit_rate"];
	 
    	$highroller_7day_unit_rate_array[$slottype] = $unit_rate;
    }    
    
    //하이롤러 - 어제
    $highroller_yesterday = "SELECT slottype, FLOOR(SUM(moneyin)/1000000) AS moneyin, FLOOR(SUM(moneyout)/1000000) AS moneyout, SUM(playcount) AS playcount, ROUND(SUM(moneyout)/SUM(moneyin)*100,2) AS winrate ".
    					"FROM `$table`	".
    					"WHERE mode IN (0,31) AND betlevel >= 10 AND writedate = '$yesterday' GROUP BY slottype ORDER BY moneyin DESC;";
    
    $highroller_yesterday_data = $db_main2->gettotallist($highroller_yesterday);
   
    $highroller_yesterday_unit_rate_sql = "SELECT slottype, ROUND(SUM(tt_moneyout)/SUM(tt_moneyin) * 100, 2) AS unit_rate ".
						    		"FROM	".
						    		"	(	".
						    		"		SELECT writedate, t1.slottype, t2.betlevel, SUM(moneyin), SUM(moneyout), t2.bet_amount,  ROUND(SUM(moneyin)/t2.bet_amount) AS tt_moneyin, ROUND(SUM(moneyout)/t2.bet_amount) AS tt_moneyout	".
						    		"		FROM $table t1 JOIN `tbl_slot_bet_info` t2 ON t1.slottype = t2.slottype AND t1.betlevel = t2.betlevel	".
						    		"		WHERE  mode IN (0,31) AND t1.betlevel >= 10 AND writedate = '$yesterday' ".
						    		"		GROUP BY slottype, betlevel".
						    		") t3 GROUP BY slottype";
    
    $highroller_yesterday_unit_rate_list = $db_main2->gettotallist($highroller_yesterday_unit_rate_sql);
     
    $highroller_yesterday_unit_rate_array = array();
    
    for($e = 0; $e < sizeof($highroller_yesterday_unit_rate_list); $e++)
    {
    	$slottype = $highroller_yesterday_unit_rate_list[$e]["slottype"];
    	$unit_rate = $highroller_yesterday_unit_rate_list[$e]["unit_rate"];
    
    	$highroller_yesterday_unit_rate_array[$slottype] = $unit_rate;
    }
?>
<script type="text/javascript">
    function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
			<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
			<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
			<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
			<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
		</span> 
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 게임 어제/최근 1주일 통계(<?= $os_txt ?>)</div>
			<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />   	
		</div>	
		<!-- //title_warp -->		
		
		<div class="search_result">
			* moneyin, moneyout 백만단위 입니다.
		</div>
	</form>
	
	<div style="width:1400px;">
		<div style="width:530px;float:left;margin-left:10px;">		
			<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">일반 - 최근 1주일(<?=$yesterday_sub_7day?> ~ <?=$yesterday?>)</span>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="">
					<col width="">
					<col width=""> 
					<col width=""> 
					<col width=""> 
					<col width="">                
				</colgroup>
				<thead>
					<tr>
						<th class="tdl">슬롯종류</th>
						<th class="tdc">승률</th>
						<th class="tdc">단위승률</th>
						<th class="tdr">money in</th>
						<th class="tdr">money out</th>
						<th class="tdr">playcount</th>					
					</tr>
				</thead>
				<tbody>
<?
		for($i=0; $i<sizeof($normal_recently_7day_data); $i++)
	    {
	    	$slottype = $normal_recently_7day_data[$i]["slottype"];
	    	$moneyin = $normal_recently_7day_data[$i]["moneyin"];
	    	$moneyout = $normal_recently_7day_data[$i]["moneyout"];
	    	$playcount = $normal_recently_7day_data[$i]["playcount"];
	    	$winrate = $normal_recently_7day_data[$i]["winrate"];
	    	
	    	for($j=0; $j<sizeof($slottype_list); $j++)
	    	{
		    	if($slottype_list[$j]["slottype"] == $slottype)
		    	{
	    			$slot_name = $slottype_list[$j]["slotname"];
	    			break;
		    	}
		    	else
		    	{
		    		$slot_name = "Unkown";
		    	}
	    	}
	    	
	    	if ($winrate <= "90" || $winrate >= "100")
	    		$style = "style='color:red;'";
	    	else
	    		$style = "";
?>
					<tr>
						<td class="tdl" <?= $style ?>><?= $slot_name ?></td>
						<td class="tdc" <?= $style ?>><?= $winrate."%" ?></td>
						<td class="tdc" <?= $style ?>><?= $normal_7day_unit_rate_array[$slottype]."%" ?></td>				
						<td class="tdr" <?= $style ?>><?= number_format($moneyin) ?></td>
						<td class="tdr" <?= $style ?>><?= number_format($moneyout) ?></td>
						<td class="tdr" <?= $style ?>><?= number_format($playcount) ?></td>				
					</tr>
<?
	    }
?>			
				</tbody>
			</table>
		</div>
	
		<div style="width:530px;float:left;margin-left:20px;margin-right:10px;">
			<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">하이롤러 - 최근 1주일(<?=$yesterday_sub_7day?> ~ <?=$yesterday?>)</span>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="">
					<col width="">
					<col width=""> 
					<col width=""> 
					<col width=""> 
					<col width="">                
				</colgroup>
				<thead>
					<tr>
						<th class="tdl">슬롯종류</th>
						<th class="tdc">승률</th>
						<th class="tdc">단위승률</th>
						<th class="tdr">money in</th>
						<th class="tdr">money out</th>
						<th class="tdr">playcount</th>					
					</tr>
				</thead>
				<tbody>
<?
		for($i=0; $i<sizeof($highroller_recently_7day_data); $i++)
	    {
	    	$slottype = $highroller_recently_7day_data[$i]["slottype"];
	    	$moneyin = $highroller_recently_7day_data[$i]["moneyin"];
	    	$moneyout = $highroller_recently_7day_data[$i]["moneyout"];
	    	$playcount = $highroller_recently_7day_data[$i]["playcount"];
	    	$winrate = $highroller_recently_7day_data[$i]["winrate"];
	    	
	    	for($j=0; $j<sizeof($slottype_list); $j++)
	    	{
		    	if($slottype_list[$j]["slottype"] == $slottype)
		    	{
	    			$slot_name = $slottype_list[$j]["slotname"];
	    			break;
		    	}
		    	else
		    	{
		    		$slot_name = "Unkown";
		    	}
	    	}
	    	
	    	if ($winrate <= "90" || $winrate >= "100")
	    		$style = "style='color:red;'";
	    	else
	    		$style = "";
?>
					<tr>
						<td class="tdl" <?= $style ?>><?= $slot_name ?></td>
						<td class="tdc" <?= $style ?>><?= $winrate."%" ?></td>
						<td class="tdc" <?= $style ?>><?= $highroller_7day_unit_rate_array[$slottype]."%" ?></td>				
						<td class="tdr" <?= $style ?>><?= number_format($moneyin) ?></td>
						<td class="tdr" <?= $style ?>><?= number_format($moneyout) ?></td>
						<td class="tdr" <?= $style ?>><?= number_format($playcount) ?></td>				
					</tr>
<?
	    }
?>			
				</tbody>
			</table>
		</div>
		
		<div class="clear"></div>
		
		<div style="width:530px;float:left;margin-left:10px;margin-top: 20px; ">		
			<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">일반 - 어제(<?=$yesterday?>)</span>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="">
					<col width="">
					<col width=""> 
					<col width=""> 
					<col width=""> 
					<col width="">                
				</colgroup>
				<thead>
					<tr>
						<th class="tdl">슬롯종류</th>
						<th class="tdc">승률</th>
						<th class="tdc">단위승률</th>
						<th class="tdr">money in</th>
						<th class="tdr">money out</th>
						<th class="tdr">playcount</th>					
					</tr>
				</thead>
				<tbody>
<?
		for($i=0; $i<sizeof($normal_yesterday_data); $i++)
	    {
	    	$slottype = $normal_yesterday_data[$i]["slottype"];
	    	$moneyin = $normal_yesterday_data[$i]["moneyin"];
	    	$moneyout = $normal_yesterday_data[$i]["moneyout"];
	    	$playcount = $normal_yesterday_data[$i]["playcount"];
	    	$winrate = $normal_yesterday_data[$i]["winrate"];
	    	
	    	for($j=0; $j<sizeof($slottype_list); $j++)
	    	{
		    	if($slottype_list[$j]["slottype"] == $slottype)
		    	{
	    			$slot_name = $slottype_list[$j]["slotname"];
	    			break;
		    	}
		    	else
		    	{
		    		$slot_name = "Unkown";
		    	}
	    	}
	    	
	    	if ($winrate <= "90" || $winrate >= "100")
	    		$style = "style='color:red;'";
	    	else
	    		$style = "";
?>
					<tr>
						<td class="tdl" <?= $style ?>><?= $slot_name ?></td>
						<td class="tdc" <?= $style ?>><?= $winrate."%" ?></td>
						<td class="tdc" <?= $style ?>><?= $normal_yesterday_unit_rate_array[$slottype]."%" ?></td>				
						<td class="tdr" <?= $style ?>><?= number_format($moneyin) ?></td>
						<td class="tdr" <?= $style ?>><?= number_format($moneyout) ?></td>
						<td class="tdr" <?= $style ?>><?= number_format($playcount) ?></td>				
					</tr>
<?
	    }
?>			
				</tbody>
			</table>
		</div>
	
		<div style="width:530px;float:left;margin-left:20px;margin-right:10px;margin-top: 20px; ">
			<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">하이롤러 - 어제(<?=$yesterday?>)</span>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="">
					<col width="">
					<col width=""> 
					<col width=""> 
					<col width=""> 
					<col width="">                
				</colgroup>
				<thead>
					<tr>
						<th class="tdl">슬롯종류</th>
						<th class="tdc">승률</th>
						<th class="tdc">단위승률</th>
						<th class="tdr">money in</th>
						<th class="tdr">money out</th>
						<th class="tdr">playcount</th>					
					</tr>
				</thead>
				<tbody>
<?
		for($i=0; $i<sizeof($highroller_yesterday_data); $i++)
	    {
	    	$slottype = $highroller_yesterday_data[$i]["slottype"];
	    	$moneyin = $highroller_yesterday_data[$i]["moneyin"];
	    	$moneyout = $highroller_yesterday_data[$i]["moneyout"];
	    	$playcount = $highroller_yesterday_data[$i]["playcount"];
	    	$winrate = $highroller_yesterday_data[$i]["winrate"];
	    	
	    	for($j=0; $j<sizeof($slottype_list); $j++)
	    	{
		    	if($slottype_list[$j]["slottype"] == $slottype)
		    	{
	    			$slot_name = $slottype_list[$j]["slotname"];
	    			break;
		    	}
		    	else
		    	{
		    		$slot_name = "Unkown";
		    	}
	    	}
	    	
	    	if ($winrate <= "90" || $winrate >= "100")
	    		$style = "style='color:red;'";
	    	else
	    		$style = "";
?>
					<tr>
						<td class="tdl" <?= $style ?>><?= $slot_name ?></td>
						<td class="tdc" <?= $style ?>><?= $winrate."%" ?></td>
						<td class="tdc" <?= $style ?>><?= $highroller_yesterday_unit_rate_array[$slottype]."%" ?></td>				
						<td class="tdr" <?= $style ?>><?= number_format($moneyin) ?></td>
						<td class="tdr" <?= $style ?>><?= number_format($moneyout) ?></td>
						<td class="tdr" <?= $style ?>><?= number_format($playcount) ?></td>				
					</tr>
<?
	    }
?>			
				</tbody>
			</table>
		</div>	
	</div>
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main2->end();
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>