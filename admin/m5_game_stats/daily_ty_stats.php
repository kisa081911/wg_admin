<?
	$top_menu = "game_stats";
	$sub_menu = "daily_ty_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    
    if($search_start_createdate == "")
    	$search_start_createdate = date("Y-m-d", strtotime("-3 day"));
    
    if($search_end_createdate == "")
    	$search_end_createdate = $today;
	
	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT today, send_cnt, user_cnt, reward_cnt, reward_user_cnt, reward_amount ".
		 	"FROM tbl_ty_stat WHERE today BETWEEN '$search_start_createdate' AND '$search_end_createdate' ORDER BY today DESC;";
	$ty_data = $db_analysis->gettotallist($sql);	
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="daily_ty_stats.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; TY 통계</div>
		<div class="search_box">
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	
	<div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">                
            </colgroup>
            <thead>
	            <tr>
	                <th>날짜</th>
	                <th class="tdc">데일리 발송 수</th>
	                <th class="tdc">데일리 발송 인원</th>                
	                <th class="tdc">1인 평균 발송 수 </th>
	                <th class="tdc">보너스 금액</th>
	                <th class="tdc">보너스 획득 인원</th>
	                <th class="tdc">보너스 평균</th>
	            </tr>
            </thead>
            <tbody>
<?
			$sum_send_cnt = 0;
			$sum_user_cnt = 0;
			$sum_reward_cnt = 0;			
			$sum_reward_user_cnt = 0;
			$sum_reward_amount = 0;
			
			for($i=0; $i<sizeof($ty_data); $i++)
			{
				$today = $ty_data[$i]["today"];
				$send_cnt = $ty_data[$i]["send_cnt"];
				$user_cnt = $ty_data[$i]["user_cnt"];
				$reward_cnt = $ty_data[$i]["reward_cnt"];
				$reward_user_cnt = $ty_data[$i]["reward_user_cnt"];
				$reward_amount = $ty_data[$i]["reward_amount"];
				
				if($i == 0 || $today != $ty_data[$i-1]["today"])
				{
					$sql = "SELECT COUNT(*) FROM tbl_ty_stat WHERE category = 0 AND today BETWEEN '$today' AND '$today'";
					$datecount = $db_analysis->getvalue($sql);
				}
				
				$sum_send_cnt += $send_cnt;
				$sum_user_cnt += $user_cnt;
				$sum_reward_cnt += $reward_cnt;			
				$sum_reward_user_cnt += $reward_user_cnt;
				$sum_reward_amount += $reward_amount;			
?>
				<tr>
<?
				if($i == 0 || $today != $ty_data[$i-1]["today"])
				{
?>				
					<td class="tdc point_title" rowspan="<?= $datecount?>"><?= $today ?></td>
<?
				} 
?>					
					<td class="tdc"><?= number_format($send_cnt) ?></td>
					<td class="tdc"><?= number_format($user_cnt) ?></td>
					<td class="tdc"><?= ($user_cnt == 0) ? "0" : round($send_cnt / $user_cnt, 0) ?></td>
					<td class="tdc"><?= number_format($reward_amount) ?></td>
					<td class="tdc"><?= number_format($reward_user_cnt) ?></td>					
					<td class="tdc"><?= ($reward_user_cnt == 0) ? "0" : round($reward_amount / $reward_user_cnt, 0) ?></td>
				</tr>
<?
			}
?>
			<tr>
				<td class="tdc point">Total</td>
				<td class="tdc"><?= number_format($sum_send_cnt) ?></td>
				<td class="tdc"><?= number_format($sum_user_cnt) ?></td>
				<td class="tdc"><?= ($sum_user_cnt == 0) ? "0" : round($sum_send_cnt / $sum_user_cnt, 0) ?></td>				
				<td class="tdc"><?= number_format($sum_reward_amount) ?></td>
				<td class="tdc"><?= number_format($sum_reward_user_cnt) ?></td>
				<td class="tdc"><?= ($sum_reward_user_cnt == 0) ? "0" : round($sum_reward_amount / $sum_reward_user_cnt, 0) ?></td>
			</tr>
	
			</tbody>
            </table>
     </div>
        
     </form>
	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_analysis->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>