<?
	$top_menu = "game_stats";
	$sub_menu = "daily_highroller_join_week_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$join_week = ($_GET["join_week"] == "") ? "0" :$_GET["join_week"];
	$search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    
    if($search_start_createdate == "")
    	$search_start_createdate = "2016-03-10";
    
    if($search_end_createdate == "")
    	$search_end_createdate = $today;
    
    $joine_week_sql = "";
    
    if($join_week == 1)
    	$joine_week_sql = " AND (days_after_install/7) <= 1 ";
    else if($join_week == 2)
    	$joine_week_sql = " AND (days_after_install/7) > 1 AND (days_after_install/7) <= 2";
    else if($join_week == 3)
    	$joine_week_sql = " AND (days_after_install/7) > 2 AND (days_after_install/7) <= 3 ";
    else if($join_week == 4)
    	$joine_week_sql = " AND (days_after_install/7) > 3 AND (days_after_install/7) <= 4 ";
    else if($join_week == 5)
    	$joine_week_sql = " AND (days_after_install/7) > 4 AND (days_after_install/7) <= 6 ";
    else if($join_week == 6)
    	$joine_week_sql = " AND (days_after_install/7) > 6 AND (days_after_install/7) <= 8 ";
    else if($join_week == 7)
    	$joine_week_sql = " AND (days_after_install/7) > 8 AND (days_after_install/7) <= 12 ";
    else if($join_week == 8)
    	$joine_week_sql = " AND (days_after_install/7) > 12 AND (days_after_install/7) <= 16 ";
    else if($join_week == 9)
    	$joine_week_sql = " AND (days_after_install/7) > 16 AND (days_after_install/7) <= 20 ";
    else if($join_week == 10)
    	$joine_week_sql = " AND (days_after_install/7) > 20 ";
	
	$db_other = new CDatabase_Other();
	
	$sql = "SELECT today, ".
			"(CASE WHEN (days_after_install/7) > 20 THEN '20_20+' ".
			"WHEN (days_after_install/7) > 16 THEN '16_20' ".
			"WHEN (days_after_install/7) > 12 THEN '12_16' ".
			"WHEN (days_after_install/7) > 8 THEN '08_12' ".
			"WHEN (days_after_install/7) > 6 THEN '06_08' ".
			"WHEN (days_after_install/7) > 4 THEN '04_06' ".
			"WHEN (days_after_install/7) > 3 THEN '03_04' ".
			"WHEN (days_after_install/7) > 2 THEN '02_03' ".
			"WHEN (days_after_install/7) > 1 THEN '01_02' ".
			"ELSE '00_01' END) AS std_week, ".
			"COUNT(useridx) AS usercnt, FLOOR(AVG(currentcoin)) AS currentcoin, ".
			"SUM(h_moneyin) AS h_moneyin, SUM(h_moneyout) AS h_moneyout, ROUND(SUM(h_moneyout)/SUM(h_moneyin)*100, 2) AS h_winrate, ".
			"SUM(h_playtime) AS h_playtime, SUM(h_playcount) AS h_playcount, ".
			"SUM(purchasecount) AS purchasecount, SUM(purchaseamount) AS purchaseamount, SUM(purchasecoin) AS purchasecoin, ".
			"SUM(famelevel) AS famelevel, SUM(ty_point) AS ty_point, SUM(freecoin) AS freecoin, SUM(jackpot) AS jackpot ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' AND h_moneyin >= 10000000 $joine_week_sql".
			"GROUP BY today, std_week ".
			"ORDER BY today DESC, std_week ASC;";
	$highroller_join_week_data = $db_other->gettotallist($sql);
	
	$sql = "SELECT today, ".
			"(CASE WHEN (days_after_install/7) > 20 THEN '20_20+' ".
			"WHEN (days_after_install/7) > 16 THEN '16_20' ".
			"WHEN (days_after_install/7) > 12 THEN '12_16' ".
			"WHEN (days_after_install/7) > 8 THEN '08_12' ".
			"WHEN (days_after_install/7) > 6 THEN '06_08' ".
			"WHEN (days_after_install/7) > 4 THEN '04_06' ".
			"WHEN (days_after_install/7) > 3 THEN '03_04' ".
			"WHEN (days_after_install/7) > 2 THEN '02_03' ".
			"WHEN (days_after_install/7) > 1 THEN '01_02' ".
			"ELSE '00_01' END) AS std_week, ".
			"COUNT(useridx) AS usercnt ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '2016-03-10' <= today AND today <= '$search_end_createdate' $joine_week_sql".
			"GROUP BY today, std_week ".
			"ORDER BY today DESC, std_week ASC;";
	$join_week_data = $db_other->gettotallist($sql);
	
	$join_week_data_array = array();
	
	for($j = 0; $j < sizeof($join_week_data); $j++)
	{
		$join_week_data_today = $join_week_data[$j]["today"];
		$join_week_data_std_week = $join_week_data[$j]["std_week"];
		$join_week_data_usercnt = $join_week_data[$j]["usercnt"];
		
		$join_week_data_array[$join_week_data_today][$join_week_data_std_week] = $join_week_data_usercnt;
	}
	
	$sql = "SELECT ".
			"(CASE WHEN (days_after_install/7) > 20 THEN '20_20+' ".
			"WHEN (days_after_install/7) > 16 THEN '16_20' ".
			"WHEN (days_after_install/7) > 12 THEN '12_16' ".
			"WHEN (days_after_install/7) > 8 THEN '08_12' ".
			"WHEN (days_after_install/7) > 6 THEN '06_08' ".
			"WHEN (days_after_install/7) > 4 THEN '04_06' ".
			"WHEN (days_after_install/7) > 3 THEN '03_04' ".
			"WHEN (days_after_install/7) > 2 THEN '02_03' ".
			"WHEN (days_after_install/7) > 1 THEN '01_02' ".
			"ELSE '00_01' END) AS std_week, ".
			"COUNT(useridx) AS usercnt, FLOOR(AVG(currentcoin)) AS currentcoin, ".
			"SUM(h_moneyin) AS h_moneyin, SUM(h_moneyout) AS h_moneyout, ROUND(SUM(h_moneyout)/SUM(h_moneyin)*100, 2) AS h_winrate, ".
			"SUM(h_playtime) AS h_playtime, SUM(h_playcount) AS h_playcount, ".
			"SUM(purchasecount) AS purchasecount, SUM(purchaseamount) AS purchaseamount, SUM(purchasecoin) AS purchasecoin, ".
			"SUM(famelevel) AS famelevel, SUM(ty_point) AS ty_point, SUM(freecoin) AS freecoin, SUM(jackpot) AS jackpot ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '$search_start_createdate' <= today AND today <= '$search_end_createdate' AND h_moneyin >= 10000000 $joine_week_sql".
			"GROUP BY std_week ".
			"ORDER BY std_week ASC;";	
	$total_highroller_join_week_data = $db_other->gettotallist($sql);
	
	$sql = "SELECT ".
			"(CASE WHEN (days_after_install/7) > 20 THEN '20_20+' ".
			"WHEN (days_after_install/7) > 16 THEN '16_20' ".
			"WHEN (days_after_install/7) > 12 THEN '12_16' ".
			"WHEN (days_after_install/7) > 8 THEN '08_12' ".
			"WHEN (days_after_install/7) > 6 THEN '06_08' ".
			"WHEN (days_after_install/7) > 4 THEN '04_06' ".
			"WHEN (days_after_install/7) > 3 THEN '03_04' ".
			"WHEN (days_after_install/7) > 2 THEN '02_03' ".
			"WHEN (days_after_install/7) > 1 THEN '01_02' ".
			"ELSE '00_01' END) AS std_week, ".
			"COUNT(useridx) AS usercnt ".
			"FROM `tbl_user_playstat_daily` ".
			"WHERE '2016-03-10' <= today AND today <= '$search_end_createdate' $joine_week_sql".
			"GROUP BY std_week ".
			"ORDER BY std_week ASC;";
	$total_join_week_data = $db_other->gettotallist($sql);
	
	$total_join_week_data_array = array();
	
	for($j = 0; $j < sizeof($total_join_week_data); $j++)
	{
		$join_week_data_std_week = $total_join_week_data[$j]["std_week"];
		$join_week_data_usercnt = $total_join_week_data[$j]["usercnt"];
	
		$total_join_week_data_array[$join_week_data_std_week] = $join_week_data_usercnt;
	}

?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="daily_highroller_join_week_stats.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 하일롤러 가입기간(주단위) 통계</div>
		<div class="search_box">
			가입주간&nbsp;:&nbsp; 
			<select name="join_week" id="join_week">
					<option value="" <?= ($join_week=="0") ? "selected" : "" ?>>전체</option>					
					<option value="1" <?= ($join_week=="1") ? "selected" : "" ?>>00주 ~ 01주</option>
					<option value="2" <?= ($join_week=="2") ? "selected" : "" ?>>01주 ~ 02주</option>                       
					<option value="3" <?= ($join_week=="3") ? "selected" : "" ?>>02주 ~ 03주</option>
					<option value="4" <?= ($join_week=="4") ? "selected" : "" ?>>03주 ~ 04주</option>
					<option value="5" <?= ($join_week=="5") ? "selected" : "" ?>>04주 ~ 06주</option>
					<option value="6" <?= ($join_week=="6") ? "selected" : "" ?>>06주 ~ 08주</option>
					<option value="7" <?= ($join_week=="7") ? "selected" : "" ?>>08주 ~ 12주</option>
					<option value="8" <?= ($join_week=="8") ? "selected" : "" ?>>12주 ~ 16주</option>
					<option value="9" <?= ($join_week=="9") ? "selected" : "" ?>>16주 ~ 20주</option>
					<option value="10" <?= ($join_week=="10") ? "selected" : "" ?>>20주 이상</option>
			</select>&nbsp;&nbsp;&nbsp;
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	
	<div id="tab_content_1">
            <table class="tbl_list_basic1" style="width:1300px">
            <colgroup>
                <col width="70">
                <col width="90">
                <col width="50">
                <col width="100">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70"> 
                <col width="100">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">
                <col width="70">                 
            </colgroup>
            <thead>
            <tr>
                <th>날짜</th>
                <th class="tdc">가입주간</th>
                <th class="tdc">유저수</th>
                <th class="tdc">평균보유코인</th>                
                <th class="tdc">moneyin</th>
                <th class="tdc">moneyout</th>
                <th class="tdc">승률</th>
                <th class="tdc">평균 playtime</th>
                <th class="tdc">평균 playcount</th>
                <th class="tdc">결제횟수</th>
                <th class="tdc">결제금액</th>
                <th class="tdc">결제코인</th>
                <th class="tdc">평균 FameLevel</th>
                <th class="tdc">jackpot</th>
                <th class="tdc">전체유저<br>대비</th>
            </tr>
            </thead>
            <tbody>
<?
			for($i=0; $i<sizeof($highroller_join_week_data); $i++)
			{
				$today = $highroller_join_week_data[$i]["today"];
				$std_week = $highroller_join_week_data[$i]["std_week"];
				$usercnt = $highroller_join_week_data[$i]["usercnt"];
				$currentcoin = $highroller_join_week_data[$i]["currentcoin"];
				$h_moneyin = $highroller_join_week_data[$i]["h_moneyin"];
				$h_moneyout = $highroller_join_week_data[$i]["h_moneyout"];
				$h_winrate = $highroller_join_week_data[$i]["h_winrate"];
				$h_playtime = $highroller_join_week_data[$i]["h_playtime"]/$usercnt;
				$h_playcount = $highroller_join_week_data[$i]["h_playcount"]/$usercnt;
				$purchasecount = $highroller_join_week_data[$i]["purchasecount"];
				$purchaseamount = $highroller_join_week_data[$i]["purchaseamount"]/10;
				$purchasecoin = $highroller_join_week_data[$i]["purchasecoin"];
				$famelevel = $highroller_join_week_data[$i]["famelevel"]/$usercnt;				
				$jackpot = $highroller_join_week_data[$i]["jackpot"];
				$join_week_usercnt = $join_week_data_array[$today][$std_week];
				
				$usercnt_rate = ($usercnt/$join_week_usercnt)*100;
				
				$std_week_str = "";
				
				if($std_week == "00_01")
					$std_week_str = "00주 ~ 01주";
				else if($std_week == "01_02")
					$std_week_str = "01주 ~ 02주";
				else if($std_week == "02_03")
					$std_week_str = "02주 ~ 03주";
				else if($std_week == "03_04")
					$std_week_str = "03주 ~ 04주";
				else if($std_week == "04_06")
					$std_week_str = "04주 ~ 06주";
				else if($std_week == "06_08")
					$std_week_str = "06주 ~ 08주";
				else if($std_week == "08_12")
					$std_week_str = "08주 ~ 12주";
				else if($std_week == "12_16")
					$std_week_str = "12주 ~ 16주";
				else if($std_week == "16_20")
					$std_week_str = "16주 ~ 20주";
				else if($std_week == "20_20+")
					$std_week_str = "20주 이상";
?>
				<tr>
<?
				if($i == 0 || $today != $highroller_join_week_data[$i-1]["today"])
				{
					if($std_week_str != "")
					{
						$sql = "SELECT today, ".
								"(CASE WHEN (days_after_install/7) > 20 THEN '20_20+' ".
								"WHEN (days_after_install/7) > 16 THEN '16_20' ".
								"WHEN (days_after_install/7) > 12 THEN '12_16' ".
								"WHEN (days_after_install/7) > 8 THEN '08_12' ".
								"WHEN (days_after_install/7) > 6 THEN '06_08' ".
								"WHEN (days_after_install/7) > 4 THEN '04_06' ".
								"WHEN (days_after_install/7) > 3 THEN '03_04' ".
								"WHEN (days_after_install/7) > 2 THEN '02_03' ".
								"WHEN (days_after_install/7) > 1 THEN '01_02' ".
								"ELSE '00_01' END) AS std_week ".								
								"FROM `tbl_user_playstat_daily` ".
								"WHERE '$today' <= today AND today <= '$today' AND h_moneyin >= 10000000 $joine_week_sql".
								"GROUP BY std_week ".
								"ORDER BY std_week ASC;";
						$highroller_join_week_rowdata = $db_other->gettotallist($sql);
?>				
					<td class="tdc point_title" rowspan="<?=sizeof($highroller_join_week_rowdata)?>"><?= $today ?></td>
					
<?
					}
					else
					{
						
?>
						<td class="tdc point_title" rowspan="1"><?= $today ?></td>
<?					
					} 
				}
?>	
	
					<td class="tdc"><?= $std_week_str ?></td>
					<td class="tdc"><?= number_format($usercnt) ?></td>
					<td class="tdc"><?= number_format($currentcoin) ?></td>
					<td class="tdc"><?= number_format($h_moneyin) ?></td>
					<td class="tdc"><?= number_format($h_moneyout) ?></td>
					<td class="tdc"><?= $h_winrate ?>%</td>
					<td class="tdc"><?= number_format($h_playtime) ?></td>
					<td class="tdc"><?= number_format($h_playcount) ?></td>
					<td class="tdc"><?= number_format($purchasecount) ?></td>
					<td class="tdc">$<?= number_format($purchaseamount) ?></td>
					<td class="tdc"><?= number_format($purchasecoin) ?></td>
					<td class="tdc"><?= number_format($famelevel) ?></td>
					<td class="tdc"><?= number_format($jackpot) ?></td>
					<td class="tdc"><?= number_format($usercnt_rate,2) ?>%</td>
				</tr>
<?
			}
			
			for($i=0; $i<sizeof($total_highroller_join_week_data); $i++)
			{
				$std_week = $total_highroller_join_week_data[$i]["std_week"];
				$usercnt = $total_highroller_join_week_data[$i]["usercnt"];
				$currentcoin = $total_highroller_join_week_data[$i]["currentcoin"];
				$h_moneyin = $total_highroller_join_week_data[$i]["h_moneyin"];
				$h_moneyout = $total_highroller_join_week_data[$i]["h_moneyout"];
				$h_winrate = $total_highroller_join_week_data[$i]["h_winrate"];
				$h_playtime = $total_highroller_join_week_data[$i]["h_playtime"]/$usercnt;
				$h_playcount = $total_highroller_join_week_data[$i]["h_playcount"]/$usercnt;
				$purchasecount = $total_highroller_join_week_data[$i]["purchasecount"];
				$purchaseamount = $total_highroller_join_week_data[$i]["purchaseamount"]/10;
				$purchasecoin = $total_highroller_join_week_data[$i]["purchasecoin"];
				$famelevel = $total_highroller_join_week_data[$i]["famelevel"]/$usercnt;
				$jackpot = $total_highroller_join_week_data[$i]["jackpot"];
				$join_week_usercnt = $total_join_week_data_array[$std_week];
				
				$usercnt_rate = ($usercnt/$join_week_usercnt)*100;
				
				$std_week_str = "";
				
				if($std_week == "00_01")
					$std_week_str = "00주 ~ 01주";
				else if($std_week == "01_02")
					$std_week_str = "01주 ~ 02주";
				else if($std_week == "02_03")
					$std_week_str = "02주 ~ 03주";
				else if($std_week == "03_04")
					$std_week_str = "03주 ~ 04주";
				else if($std_week == "04_06")
					$std_week_str = "04주 ~ 06주";
				else if($std_week == "06_08")
					$std_week_str = "06주 ~ 08주";
				else if($std_week == "08_12")
					$std_week_str = "08주 ~ 12주";
				else if($std_week == "12_16")
					$std_week_str = "12주 ~ 16주";
				else if($std_week == "16_20")
					$std_week_str = "16주 ~ 20주";
				else if($std_week == "20_20+")
					$std_week_str = "20주 이상";
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($i == 0)
				{
?>
					<td class="tdc point_title" rowspan="<?= sizeof($total_highroller_join_week_data)?>" valign="center">Total</td>
<?
				}
?>
					<td class="tdc"><?= $std_week_str ?></td>
					<td class="tdc"><?= number_format($usercnt) ?></td>
					<td class="tdc"><?= number_format($currentcoin) ?></td>
					<td class="tdc"><?= number_format($h_moneyin) ?></td>
					<td class="tdc"><?= number_format($h_moneyout) ?></td>
					<td class="tdc"><?= $h_winrate ?>%</td>
					<td class="tdc"><?= number_format($h_playtime) ?></td>
					<td class="tdc"><?= number_format($h_playcount) ?></td>
					<td class="tdc"><?= number_format($purchasecount) ?></td>
					<td class="tdc">$<?= number_format($purchaseamount) ?></td>
					<td class="tdc"><?= number_format($purchasecoin) ?></td>
					<td class="tdc"><?= number_format($famelevel) ?></td>
					<td class="tdc"><?= number_format($jackpot) ?></td>
					<td class="tdc"><?= number_format($usercnt_rate,2) ?>%</td>
				</tr>

<?
			}
?>
			</tbody>
            </table>
     	</div>        
     </form>	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_other->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>