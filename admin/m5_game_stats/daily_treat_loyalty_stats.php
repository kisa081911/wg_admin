<?
	$top_menu = "game_stats";
	$sub_menu = "daily_treat_loyalty_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	
	if($search_start_createdate == "")
		$search_start_createdate = date("Y-m-d", strtotime("-3 day"));
	
	if($search_end_createdate == "")
		$search_end_createdate = date("Y-m-d", strtotime("-1 day"));
	
	$db_other = new CDatabase_Other();
	
	$sql = "SELECT * FROM tbl_level_treat_loyaltypot_stats_daily WHERE today BETWEEN '$search_start_createdate' AND '$search_end_createdate' ORDER BY today DESC, level ASC";
	$treat_loyaltypot_stats_data = $db_other->gettotallist($sql);
	
	$sql = "SELECT level, ROUND(AVG(avg_have_treatamount),2) AS total_avg_have_treatamount,  ".
			"ROUND(AVG(avg_have_loyaltyamount),2) AS total_avg_have_loyaltyamount, ". 
			"ROUND(AVG(treat_loyalty_rate),2) AS total_treat_loyalty_rate, ". 
			"ROUND(AVG(avg_treatamount),2) AS total_avg_treatamount ".
			"FROM tbl_level_treat_loyaltypot_stats_daily WHERE today BETWEEN '$search_start_createdate' AND '$search_end_createdate' GROUP BY LEVEL ORDER BY today DESC, LEVEL ASC";
	$total_treat_loyaltypot_stats_data = $db_other->gettotallist($sql);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="daily_treat_loyalty_stats.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Treat Ticket & LoyatyPot 통계</div>
		<div class="search_box">
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>	
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">	
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">Level</th>
					<th class="tdc">평균 트리트 티켓 보유금액(A)</th>
					<th class="tdc">평균 로열티팟 보유금액(B)</th>
					<th class="tdc">A/B(%)</th>
					<th class="tdc">평균 트리트 티켓 금액</th>					
				</tr>
			</thead>
			<tbody>
<?		
			for($i=0; $i<sizeof($treat_loyaltypot_stats_data); $i++)
			{
				$today = $treat_loyaltypot_stats_data[$i]["today"];
				$level = $treat_loyaltypot_stats_data[$i]["level"];
				$avg_have_treatamount = $treat_loyaltypot_stats_data[$i]["avg_have_treatamount"];
				$avg_have_loyaltyamount = $treat_loyaltypot_stats_data[$i]["avg_have_loyaltyamount"];
				$treat_loyalty_rate = $treat_loyaltypot_stats_data[$i]["treat_loyalty_rate"];
				$avg_treatamount = $treat_loyaltypot_stats_data[$i]["avg_treatamount"];
			
				if($level == 0)
					$level_name = "전체";
				else if($level == 1)
					$level_name = "VIP전체";
				else if($level == 2)
					$level_name = "결제자";
				else if($level == 3)
					$level_name = "당일 결제 VIP";
				else if($level == 4)
					$level_name = "비결제";
				else if($level == 5)
					$level_name = "당일 비결제 VIP";				
?>
				<tr>
<?
				if($i == 0 || $today != $treat_loyaltypot_stats_data[$i-1]["today"])
				{
					$sql = "SELECT COUNT(*) FROM tbl_level_treat_loyaltypot_stats_daily WHERE today = '$today'";
					$datecount = $db_other->getvalue($sql);
?>
					<td class="tdc point_title" rowspan="<?= $datecount?>"><?= $today ?></td>					
<?
				} 
?>					
					<td class="tdc"><?= $level_name ?></td>
					<td class="tdc"><?= number_format($avg_have_treatamount) ?></td>					
					<td class="tdc"><?= number_format($avg_have_loyaltyamount) ?></td>
					<td class="tdc"><?= $treat_loyalty_rate ?> %</td>
					<td class="tdc"><?= number_format($avg_treatamount) ?></td>					
				</tr>
<?
			}

			for($i=0; $i<sizeof($total_treat_loyaltypot_stats_data); $i++)
			{
				$level = $total_treat_loyaltypot_stats_data[$i]["level"];
				$total_avg_have_treatamount = $total_treat_loyaltypot_stats_data[$i]["total_avg_have_treatamount"];
				$total_avg_have_loyaltyamount = $total_treat_loyaltypot_stats_data[$i]["total_avg_have_loyaltyamount"];
				$total_treat_loyalty_rate = $total_treat_loyaltypot_stats_data[$i]["total_treat_loyalty_rate"];
				$total_avg_treatamount = $total_treat_loyaltypot_stats_data[$i]["total_avg_treatamount"];

				if($level == 0)
					$level_name = "전체";
				else if($level == 1)
					$level_name = "VIP전체";
				else if($level == 2)
					$level_name = "결제자";
				else if($level == 3)
					$level_name = "당일 결제 VIP";
				else if($level == 4)
					$level_name = "비결제";
				else if($level == 5)
					$level_name = "당일 비결제 VIP";	
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
				if($i == 0)
				{
?>
					<td class="tdc point_title" rowspan="6" valign="center">Total</td>
<?
				}
?>
					<td class="tdc"><?= $level_name ?></td>
					<td class="tdc"><?= number_format($total_avg_have_treatamount) ?></td>					
					<td class="tdc"><?= number_format($total_avg_have_loyaltyamount) ?></td>
					<td class="tdc"><?= $total_treat_loyalty_rate ?> %</td>
					<td class="tdc"><?= number_format($total_avg_treatamount) ?></td>	
				</tr>
<?
			}
?>
			</tbody>
		</table>
	</div>        
  	</form>	
</div>
<!--  //CONTENTS WRAP -->
<?		
	$db_other->end();
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>