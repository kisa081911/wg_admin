<?
	$top_menu = "game_stats";
	$sub_menu = "daily_free_stat_graph";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$os_type = ($_GET["os_type"] == "") ? "4" :$_GET["os_type"];
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
	$pagename = "daily_free_stat_graph.php";
	
	$today = date("Y-m-d");
	$before_day = get_past_date($today,7,"d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	
	if($os_type == "0")
	{
		$os_txt = "Web";
		$os_type_sql = "category = 0 AND ";
		$active_type = "todayfacebookactivecount";
	}
	else if($os_type == "1")
	{
		$os_txt = "IOS";
		$os_type_sql = "category = 1 AND ";
		$active_type = "todayiosactivecount";
	}
	else if($os_type == "2")
	{
		$os_txt = "Android";
		$os_type_sql = "category = 2 AND ";
		$active_type = "todayandroidactivecount";
	}
	else if($os_type == "3")
	{
		$os_txt = "Amazon";
		$os_type_sql = "category = 3 AND ";
		$active_type = "todayamazonactivecount";
	}
	else if($os_type == "4")
	{
		$os_txt = "Total";
		$os_type_sql = "1=1 AND ";
		$active_type = "todayactivecount";
	}
	else if($os_type == "5")
	{
		$os_txt = "Web_2.0";
		$os_type_sql = "1=1 AND is_v2 = 1 AND";
		$active_type = "todayactivecount";
	}

	$sql = "SELECT ".
			"today,	".
			"SUM(freeamount) AS total_freeamount,	".
			"SUM(IF(TYPE = 0, freeamount, 0)) AS wheel_freeamount,	".
			"SUM(IF(TYPE = 2, freeamount, 0)) AS tybonus_freeamount,	".
			"SUM(IF(TYPE = 3, freeamount, 0)) AS time_freeamount,	".
			"SUM(IF(TYPE = 4, freeamount, 0)) AS vipup_freeamount,	".
			"SUM(IF(TYPE = 5, freeamount, 0)) AS treat_freeamount,	".
			"SUM(IF(TYPE = 6, freeamount, 0)) AS celebup_freeamount,	".
			"SUM(IF(TYPE = 8, freeamount, 0)) AS share_freeamount,	".
			"SUM(IF(TYPE = 9, freeamount, 0)) AS bankrupt_freeamount,	".
			"SUM(IF(TYPE = 11, freeamount, 0)) AS fanpage_freeamount,	".
			"SUM(IF(TYPE = 12, freeamount, 0)) AS fanpage_ty_freeamount,	".
			"SUM(IF(TYPE = 13, freeamount, 0)) AS special_bankrupt_freeamount,	".
			"SUM(IF(TYPE = 14, freeamount, 0)) AS welcomeback_freeamount,	".
			"SUM(IF(TYPE = 15, freeamount, 0)) AS giveaway_freeamount,	".
			"SUM(IF(TYPE = 16, freeamount, 0)) AS fame_tourney_freeamount,	".
			"SUM(IF(TYPE = 17, freeamount, 0)) AS tutorial_freeamount,	".
			"SUM(IF(TYPE = 18, freeamount, 0)) AS fame_weekly_freeamount,	".
			"SUM(IF(TYPE = 19, freeamount, 0)) AS fame_monthly_freeamount,	".
			"SUM(IF(TYPE = 21, freeamount, 0)) AS invite_challenge_freeamount,	".
			"SUM(IF(TYPE = 22, freeamount, 0)) AS platinum_wheel_freeamount,	".
			"SUM(IF(TYPE = 23, freeamount, 0)) AS piggypot_freeamount,	".
			"SUM(IF(TYPE = 24, freeamount, 0)) AS freeSpin_freeamount,	".
			"SUM(IF(TYPE = 49, freeamount, 0)) AS wheel_4by_freeamount,	".
			"SUM(IF(TYPE = 50, freeamount, 0)) AS wheel_2by_freeamount,	".
			"SUM(IF(TYPE = 51, freeamount, 0)) AS dailystam_1_2by_freeamount,	".
			"SUM(IF(TYPE = 52, freeamount, 0)) AS dailystam_2_2by_freeamount,	".
			"SUM(IF(TYPE = 53, freeamount, 0)) AS dailystam_3_2by_freeamount,	".
			"SUM(IF(TYPE = 54, freeamount, 0)) AS dailystam_4_2by_freeamount,	".
			"SUM(IF(TYPE = 55, freeamount, 0)) AS dailystam_5_2by_freeamount,	".
			"SUM(IF(TYPE = 56, freeamount, 0)) AS dailystam_6_2by_freeamount,	".
			"SUM(IF(TYPE = 57, freeamount, 0)) AS dailystam_7_2by_freeamount,	".
			"SUM(IF(TYPE = 61, freeamount, 0)) AS dailystam_1_4by_freeamount,	".
			"SUM(IF(TYPE = 62, freeamount, 0)) AS dailystam_2_4by_freeamount,	".
			"SUM(IF(TYPE = 63, freeamount, 0)) AS dailystam_3_4by_freeamount,	".
			"SUM(IF(TYPE = 64, freeamount, 0)) AS dailystam_4_4by_freeamount,	".
			"SUM(IF(TYPE = 65, freeamount, 0)) AS dailystam_5_4by_freeamount,	".
			"SUM(IF(TYPE = 66, freeamount, 0)) AS dailystam_6_4by_freeamount,	".
			"SUM(IF(TYPE = 68, freeamount, 0)) AS dailyfreespin_1_freeamount,	".
			"SUM(IF(TYPE = 69, freeamount, 0)) AS dailyfreespin_2_freeamount,	".
			"SUM(IF(TYPE = 70, freeamount, 0)) AS dailyfreespin_3_freeamount,	".
			"SUM(IF(TYPE = 71, freeamount, 0)) AS dailyfreespin_4_freeamount,	".
			"SUM(IF(TYPE = 72, freeamount, 0)) AS dailyfreespin_5_freeamount,	".
			"SUM(IF(TYPE = 73, freeamount, 0)) AS dailyfreespin_6_freeamount,	".
			"SUM(IF(TYPE = 74, freeamount, 0)) AS dailyfreespin_7_freeamount,	".
			"SUM(IF(TYPE = 75, freeamount, 0)) AS dailyfreespin_1_2by_freeamount,	".
			"SUM(IF(TYPE = 76, freeamount, 0)) AS dailyfreespin_2_2by_freeamount,	".
			"SUM(IF(TYPE = 77, freeamount, 0)) AS dailyfreespin_3_2by_freeamount,	".
			"SUM(IF(TYPE = 78, freeamount, 0)) AS dailyfreespin_4_2by_freeamount,	".
			"SUM(IF(TYPE = 79, freeamount, 0)) AS dailyfreespin_5_2by_freeamount,	".
			"SUM(IF(TYPE = 80, freeamount, 0)) AS dailyfreespin_6_2by_freeamount,	".
			"SUM(IF(TYPE = 81, freeamount, 0)) AS dailyfreespin_7_2by_freeamount,	".
			"SUM(IF(TYPE = 82, freeamount, 0)) AS dailyfreespin_1_2by_freeamount,	".
			"SUM(IF(TYPE = 83, freeamount, 0)) AS dailyfreespin_2_4by_freeamount,	".
			"SUM(IF(TYPE = 84, freeamount, 0)) AS dailyfreespin_3_4by_freeamount,	".
			"SUM(IF(TYPE = 85, freeamount, 0)) AS dailyfreespin_4_4by_freeamount,	".
			"SUM(IF(TYPE = 86, freeamount, 0)) AS dailyfreespin_5_4by_freeamount,	".
			"SUM(IF(TYPE = 87, freeamount, 0)) AS dailyfreespin_6_4by_freeamount,	".
			"SUM(IF(TYPE = 88, freeamount, 0)) AS dailyfreespin_7_4by_freeamount,	".
			"SUM(IF(TYPE = 96, freeamount, 0)) AS fanpage_scratch_freeamount,	".
			"SUM(IF(TYPE = 97, freeamount, 0)) AS fanpage_fortune_freeamount,	".
			"SUM(IF(TYPE = 98, freeamount, 0)) AS fanpage_goldegg_freeamount,	".
			"SUM(IF(TYPE = 99, freeamount, 0)) AS youtube_retention_freeamount,	".
			"SUM(IF(TYPE = 100, freeamount, 0)) AS inbox_freeamount,	".
			"SUM(IF(TYPE = 101, freeamount, 0)) AS dailystam_1_freeamount,	".
			"SUM(IF(TYPE = 102, freeamount, 0)) AS dailystam_2_freeamount,	".
			"SUM(IF(TYPE = 103, freeamount, 0)) AS dailystam_3_freeamount,	".
			"SUM(IF(TYPE = 104, freeamount, 0)) AS dailystam_4_freeamount,	".
			"SUM(IF(TYPE = 105, freeamount, 0)) AS dailystam_5_freeamount,	".
			"SUM(IF(TYPE = 106, freeamount, 0)) AS dailystam_6_freeamount,	".
			"SUM(IF(TYPE = 107, freeamount, 0)) AS dailystam_7_freeamount,	".
			"SUM(IF(TYPE = 108, freeamount, 0)) AS collection_event_middle_stage,	".
			"SUM(IF(TYPE = 109, freeamount, 0)) AS collection_wheel,	".
			"SUM(IF(TYPE = 111, freeamount, 0)) AS collection_theme_reward,	".
			"SUM(IF(TYPE = 112, freeamount, 0)) AS collection_final_theme_reward,	".
			"SUM(IF(TYPE = 127, freeamount, 0)) AS pushon_freeamount,	".
			"SUM(IF(TYPE = 142, freeamount, 0)) AS piggy_bank_bonus_freeamount,	".
			"SUM(IF(TYPE = 143, freeamount, 0)) AS daily_multiplier_bonus_freeamount,	".
			"SUM(IF(TYPE = 144, freeamount, 0)) AS weekly_card_bonus_freeamount,	".
			"SUM(IF(TYPE = 145, freeamount, 0)) AS special_daily_wheel_freeamount,	".
			"SUM(IF(TYPE = 146, freeamount, 0)) AS singleplay_levelup_freeamount,	".
			"SUM(IF(TYPE = 113, freeamount, 0)) AS subscribe_wheel_freeamount,	".
			"SUM(IF(TYPE = 200, freeamount, 0)) AS metaevent_stage1_freeamount,	".
			"SUM(IF(TYPE = 201, freeamount, 0)) AS metaevent_stage2_freeamount,	".
			"SUM(IF(TYPE = 202, freeamount, 0)) AS metaevent_stage3_freeamount,	".
			"SUM(IF(TYPE = 203, freeamount, 0)) AS metaevent_stage4_freeamount,	".
			"SUM(IF(TYPE = 204, freeamount, 0)) AS metaevent_stage5_freeamount,	".
			"SUM(IF(TYPE = 205, freeamount, 0)) AS metaevent_stagefinal_freeamount,	".
			"SUM(IF(TYPE = 148, freeamount, 0)) AS superfreespin_1_freeamount,	".
			"SUM(IF(TYPE = 149, freeamount, 0)) AS superfreespin_2_freeamount,	".
			"SUM(IF(TYPE = 150, freeamount, 0)) AS superfreespin_3_freeamount,	".
			"SUM(IF(TYPE = 151, freeamount, 0)) AS superfreespin_4_freeamount,	".
			"SUM(IF(TYPE = 152, freeamount, 0)) AS superfreespin_5_freeamount,	".
			"SUM(IF(TYPE = 153, freeamount, 0)) AS superfreespin_6_freeamount,	".
			"SUM(IF(TYPE = 154, freeamount, 0)) AS superfreespin_7_freeamount,	".
			"SUM(IF(TYPE = 155, freeamount, 0)) AS advertisement_freeamount,	".
			"SUM(IF(INBOX_TYPE = 500, freeamount, 0)) AS wakeup_freeamount	".
			"FROM tbl_user_freecoin_daily	".
			"WHERE $os_type_sql today BETWEEN '$startdate' AND '$enddate' ".
			"GROUP BY today";
	$freecoin_list = $db_analysis->gettotallist($sql);
	
	$sql = "select ".
			"today,	".
			"SUM(freeamount) AS total_freeamount,	".
			"SUM(IF(inbox_type = 701, freeamount, 0)) AS vip_daily_bonus_freeamount,".
			"SUM(IF(inbox_type = 801, freeamount, 0)) AS mobile_popup_reward_bonus_freeamount,	".
			"SUM(IF(inbox_type = 901, freeamount, 0)) AS surprise_bonus_freeamount	".
			"from `tbl_user_freecoin_daily` ".
			"WHERE $os_type_sql today BETWEEN '$startdate' AND '$enddate' ".
			"GROUP BY today";
	$inbox_list = $db_analysis->gettotallist($sql);
		
	$sql = "SELECT $active_type FROM user_join_log ".			
			"WHERE today BETWEEN '$startdate' AND '$enddate' ORDER BY today DESC";
	$dau_data = $db_analysis->gettotallist($sql);	
	
	$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, SUM(freecoin)  AS freeamount ".
			"FROM tbl_freecoin_admin_log ".
			"WHERE useridx > 20000 AND admin_id != 'admin' AND writedate BETWEEN '$startdate' AND '$enddate' AND freecoin > 0 ".
			"GROUP BY today ORDER BY today ASC";
	$admin_freecoin_data = $db_main->gettotallist($sql);
	
	$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, IFNULL(SUM(collect_amount),0) AS freeamount ".
			"FROM tbl_bankrupt_vip_collect_log WHERE  writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' GROUP BY today ORDER BY today ASC";
	$vip_bankrupt_data = $db_main2->gettotallist($sql);
	
	$total_amount_list = array();	
	$wheel_amount_list = array();
	$tybonus_amount_list = array();
	$time_amount_list = array();
	$vipup_amount_list = array();
	$treat_amount_list = array();
	$celebup_amount_list = array();
	$share_amount_list = array();
	$inbox_amount_list = array();
	$admin_freecoin_amount_list = array();
	
	$bankrupt_amount_list = array();
	$fanpage_freecoin_amount_list = array();
	$fanpage_ty_amount_list = array();
	$special_bankrupt_amount_list = array();
	$welcomeback_amount_list = array();
	$giveaway_amount_list = array();
	$fame_tourney_amount_list = array();
	
	$tutorial_treat_ticket_amount_list = array();
	$fame_weekly_amount_list = array();
	$fame_monthly_amount_list = array();
	$invite_challenge_amount_list = array();
	$platinum_wheel_amount_list = array();
	$piggypot_amount_list = array();
	$freespin_amount_list = array();
	
	$wheel_4by_amount_list = array();
	$wheel_2by_amount_list = array();	
	
	$dailystamp1_4by_amount_list = array();
	$dailystamp2_4by_amount_list = array();
	$dailystamp3_4by_amount_list = array();
	$dailystamp4_4by_amount_list = array();
	$dailystamp5_4by_amount_list = array();
	$dailystamp6_4by_amount_list = array();
	$dailystamp7_4by_amount_list = array();
	
	$dailystamp1_2by_amount_list = array();
	$dailystamp2_2by_amount_list = array();
	$dailystamp3_2by_amount_list = array();
	$dailystamp4_2by_amount_list = array();
	$dailystamp5_2by_amount_list = array();
	$dailystamp6_2by_amount_list = array();
	$dailystamp7_2by_amount_list = array();
	
	$dailystamp1_amount_list = array();
	$dailystamp2_amount_list = array();
	$dailystamp3_amount_list = array();
	$dailystamp4_amount_list = array();
	$dailystamp5_amount_list = array();
	$dailystamp6_amount_list = array();
	$dailystamp7_amount_list = array();
	
	$dailyfreespin1_amount_list = array();
	$dailyfreespin2_amount_list = array();
	$dailyfreespin3_amount_list = array();
	$dailyfreespin4_amount_list = array();
	$dailyfreespin5_amount_list = array();
	$dailyfreespin6_amount_list = array();
	$dailyfreespin7_amount_list = array();
	
	$dailyfreespin1_2by_amount_list = array();
	$dailyfreespin2_2by_amount_list = array();
	$dailyfreespin3_2by_amount_list = array();
	$dailyfreespin4_2by_amount_list = array();
	$dailyfreespin5_2by_amount_list = array();
	$dailyfreespin6_2by_amount_list = array();
	$dailyfreespin7_2by_amount_list = array();
	
	
	$dailyfreespin1_4by_amount_list = array();
	$dailyfreespin2_4by_amount_list = array();
	$dailyfreespin3_4by_amount_list = array();
	$dailyfreespin4_4by_amount_list = array();
	$dailyfreespin5_4by_amount_list = array();
	$dailyfreespin6_4by_amount_list = array();
	$dailyfreespin7_4by_amount_list = array();
	$scratch_amount_list = array();
	$fortune_amount_list = array();
	$goldegg_amount_list = array();
	$youtube_retention_amount_list = array();
	$subscribe_wheel_freeamount_list = array();
	
	$pushon_amount_list = array();
	
	$wakeup_amount_list = array();
	
	$date_list = array();
	$admin_freecoin_list = array();

	$vip_bankrupt_list = array();
	$vip_bankrupt_amount_list = array();
	
	$vip_daily_bonus_list = array();
	$vip_daily_bonus_amount_list = array();
	
	$mobile_popup_reward_list = array();
	$mobile_popup_reward_amount_list = array();
	
	$surprise_bonus_list = array();
	$surprise_bonus_freeamount_list = array();

	$metaevent_stage1_freeamount_list = array();
	$metaevent_stage2_freeamount_list = array();
	$metaevent_stage3_freeamount_list = array();
	$metaevent_stage4_freeamount_list = array();
	$metaevent_stage5_freeamount_list = array();
	$metaevent_stagefinal_freeamount_list = array();
	
	$piggy_bank_bonus_freeamount_list = array();
	$daily_multiplier_bonus_freeamount_list = array();
	$weekly_card_bonus_freeamount_list = array();
	$special_daily_wheel_freeamount_list = array();
	$singleplay_levelup_freeamount_list = array();
	
	$collection_event_middle_stage_list = array();
	$collection_wheel_list = array();
	$collection_theme_reward_list = array();
	$collection_final_theme_reward_list = array();
	
	$advertisement_list = array();
	
	
	$freecoin_list_pointer = sizeof($freecoin_list);
	$admin_freecoin_list_pointer = sizeof($admin_freecoin_data);	
	$vip_bankrupt_amount_pointer = sizeof($vip_bankrupt_data);
	
	$inbox_list_pointer = sizeof($inbox_list);
	
	$date_pointer = $enddate;
	
	$loop_count = get_diff_date($enddate, $startdate, "d") + 1;

	for($i=0; $i<$loop_count; $i++)
	{
		if ($freecoin_list_pointer > 0)
			$today = $freecoin_list[$freecoin_list_pointer-1]["today"];
		
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$freecoin_info = $freecoin_list[$freecoin_list_pointer-1];
		
			$total_amount_list[$i] = $freecoin_info["total_freeamount"];
			$wheel_amount_list[$i] = $freecoin_info["wheel_freeamount"];
			$tybonus_amount_list[$i] = $freecoin_info["tybonus_freeamount"];
			$time_amount_list[$i] = $freecoin_info["time_freeamount"];
			$vipup_amount_list[$i] = $freecoin_info["vipup_freeamount"];
			$treat_amount_list[$i] = $freecoin_info["treat_freeamount"];
			$celebup_amount_list[$i] = $freecoin_info["celebup_freeamount"];
			$share_amount_list[$i] = $freecoin_info["share_freeamount"];
			$inbox_amount_list[$i] = $freecoin_info["inbox_freeamount"];			
			$bankrupt_amount_list[$i] = $freecoin_info["bankrupt_freeamount"];
			$fanpage_freecoin_amount_list[$i] = $freecoin_info["fanpage_freeamount"];
			$fanpage_ty_amount_list[$i] = $freecoin_info["fanpage_ty_freeamount"];
			$special_bankrupt_amount_list[$i] = $freecoin_info["special_bankrupt_freeamount"];
			$welcomeback_amount_list[$i] = $freecoin_info["welcomeback_freeamount"];
			$giveaway_amount_list[$i] = $freecoin_info["giveaway_freeamount"];
			$fame_tourney_amount_list[$i] = $freecoin_info["fame_tourney_freeamount"];
			$tutorial_treat_ticket_amount_list[$i] = $freecoin_info["tutorial_freeamount"];
			$fame_weekly_amount_list[$i] = $freecoin_info["fame_weekly_freeamount"];
			$fame_monthly_amount_list[$i] = $freecoin_info["fame_monthly_freeamount"];
			$invite_challenge_amount_list[$i] = $freecoin_info["invite_challenge_freeamount"];
			$platinum_wheel_amount_list[$i] = $freecoin_info["platinum_wheel_freeamount"];
			$piggypot_amount_list[$i] = $freecoin_info["piggypot_freeamount"];
			$freespin_amount_list[$i] = $freecoin_info["freeSpin_freeamount"];
			$wheel_4by_amount_list[$i] = $freecoin_info["wheel_4by_freeamount"];
			$wheel_2by_amount_list[$i] = $freecoin_info["wheel_2by_freeamount"];
			$dailystamp1_4by_amount_list[$i] = $freecoin_info["dailystam_1_4by_freeamount"];
			$dailystamp2_4by_amount_list[$i] = $freecoin_info["dailystam_2_4by_freeamount"];
			$dailystamp3_4by_amount_list[$i] = $freecoin_info["dailystam_3_4by_freeamount"];
			$dailystamp4_4by_amount_list[$i] = $freecoin_info["dailystam_4_4by_freeamount"];
			$dailystamp5_4by_amount_list[$i] = $freecoin_info["dailystam_5_4by_freeamount"];
			$dailystamp6_4by_amount_list[$i] = $freecoin_info["dailystam_6_4by_freeamount"];
			$dailystamp7_4by_amount_list[$i] = $freecoin_info["dailystam_7_4by_freeamount"];
			$dailystamp1_2by_amount_list[$i] = $freecoin_info["dailystam_1_2by_freeamount"];
			$dailystamp2_2by_amount_list[$i] = $freecoin_info["dailystam_2_2by_freeamount"];
			$dailystamp3_2by_amount_list[$i] = $freecoin_info["dailystam_3_2by_freeamount"];
			$dailystamp4_2by_amount_list[$i] = $freecoin_info["dailystam_4_2by_freeamount"];
			$dailystamp5_2by_amount_list[$i] = $freecoin_info["dailystam_5_2by_freeamount"];
			$dailystamp6_2by_amount_list[$i] = $freecoin_info["dailystam_6_2by_freeamount"];
			$dailystamp7_2by_amount_list[$i] = $freecoin_info["dailystam_7_2by_freeamount"];
			$dailystamp1_amount_list[$i] = $freecoin_info["dailystam_1_freeamount"];
			$dailystamp2_amount_list[$i] = $freecoin_info["dailystam_2_freeamount"];
			$dailystamp3_amount_list[$i] = $freecoin_info["dailystam_3_freeamount"];
			$dailystamp4_amount_list[$i] = $freecoin_info["dailystam_4_freeamount"];
			$dailystamp5_amount_list[$i] = $freecoin_info["dailystam_5_freeamount"];
			$dailystamp6_amount_list[$i] = $freecoin_info["dailystam_6_freeamount"];
			$dailystamp7_amount_list[$i] = $freecoin_info["dailystam_7_freeamount"];
			$dailyfreespin1_amount_list[$i] = $freecoin_info["dailyfreespin_1_freeamount"];
			$dailyfreespin2_amount_list[$i] = $freecoin_info["dailyfreespin_2_freeamount"];
			$dailyfreespin3_amount_list[$i] = $freecoin_info["dailyfreespin_3_freeamount"];
			$dailyfreespin4_amount_list[$i] = $freecoin_info["dailyfreespin_4_freeamount"];
			$dailyfreespin5_amount_list[$i] = $freecoin_info["dailyfreespin_5_freeamount"];
			$dailyfreespin6_amount_list[$i] = $freecoin_info["dailyfreespin_6_freeamount"];
			$dailyfreespin7_amount_list[$i] = $freecoin_info["dailyfreespin_7_freeamount"];
			
			$dailyfreespin1_2by_amount_list[$i] = $freecoin_info["dailyfreespin_1_2by_freeamount"];
			$dailyfreespin2_2by_amount_list[$i] = $freecoin_info["dailyfreespin_2_2by_freeamount"];
			$dailyfreespin3_2by_amount_list[$i] = $freecoin_info["dailyfreespin_3_2by_freeamount"];
			$dailyfreespin4_2by_amount_list[$i] = $freecoin_info["dailyfreespin_4_2by_freeamount"];
			$dailyfreespin5_2by_amount_list[$i] = $freecoin_info["dailyfreespin_5_2by_freeamount"];
			$dailyfreespin6_2by_amount_list[$i] = $freecoin_info["dailyfreespin_6_2by_freeamount"];
			$dailyfreespin7_2by_amount_list[$i] = $freecoin_info["dailyfreespin_7_2by_freeamount"];
			
			$dailyfreespin1_4by_amount_list[$i] = $freecoin_info["dailyfreespin_1_4by_freeamount"];
			$dailyfreespin2_4by_amount_list[$i] = $freecoin_info["dailyfreespin_2_4by_freeamount"];
			$dailyfreespin3_4by_amount_list[$i] = $freecoin_info["dailyfreespin_3_4by_freeamount"];
			$dailyfreespin4_4by_amount_list[$i] = $freecoin_info["dailyfreespin_4_4by_freeamount"];
			$dailyfreespin5_4by_amount_list[$i] = $freecoin_info["dailyfreespin_5_4by_freeamount"];
			$dailyfreespin6_4by_amount_list[$i] = $freecoin_info["dailyfreespin_6_4by_freeamount"];
			$dailyfreespin7_4by_amount_list[$i] = $freecoin_info["dailyfreespin_7_4by_freeamount"];
			$pushon_amount_list[$i] = $freecoin_info["pushon_freeamount"];
			$wakeup_amount_list[$i] = $freecoin_info["wakeup_freeamount"];
			$scratch_amount_list[$i] = $freecoin_info["fanpage_scratch_freeamount"];
			$fortune_amount_list[$i] = $freecoin_info["fanpage_fortune_freeamount"];
			$goldegg_amount_list[$i] = $freecoin_info["fanpage_goldegg_freeamount"];
			$youtube_retention_amount_list[$i] = $freecoin_info["youtube_retention_freeamount"];
			$subscribe_wheel_freeamount_list[$i] = $freecoin_info["subscribe_wheel_freeamount"];
			
			$metaevent_stage1_freeamount_list[$i] = $freecoin_info["metaevent_stage1_freeamount"];
			$metaevent_stage2_freeamount_list[$i] = $freecoin_info["metaevent_stage2_freeamount"];
			$metaevent_stage3_freeamount_list[$i] = $freecoin_info["metaevent_stage3_freeamount"];
			$metaevent_stage4_freeamount_list[$i] = $freecoin_info["metaevent_stage4_freeamount"];
			$metaevent_stage5_freeamount_list[$i] = $freecoin_info["metaevent_stage5_freeamount"];
			$metaevent_stagefinal_freeamount_list[$i] = $freecoin_info["metaevent_stagefinal_freeamount"];
			
			$piggy_bank_bonus_freeamount_list[$i] = $freecoin_info["piggy_bank_bonus_freeamount"];
			$daily_multiplier_bonus_freeamount_list[$i] = $freecoin_info["daily_multiplier_bonus_freeamount"];
			$weekly_card_bonus_freeamount_list[$i] = $freecoin_info["weekly_card_bonus_freeamount"];
			$special_daily_wheel_freeamount_list[$i] = $freecoin_info["special_daily_wheel_freeamount"];
			$singleplay_levelup_freeamount_list[$i] = $freecoin_info["singleplay_levelup_freeamount"];
			
			$superfreespin_1_amount_list[$i] = $freecoin_info["superfreespin_1_freeamount"];
			$superfreespin_2_amount_list[$i] = $freecoin_info["superfreespin_2_freeamount"];
			$superfreespin_3_amount_list[$i] = $freecoin_info["superfreespin_3_freeamount"];
			$superfreespin_4_amount_list[$i] = $freecoin_info["superfreespin_4_freeamount"];
			$superfreespin_5_amount_list[$i] = $freecoin_info["superfreespin_5_freeamount"];
			$superfreespin_6_amount_list[$i] = $freecoin_info["superfreespin_6_freeamount"];
			$superfreespin_7_amount_list[$i] = $freecoin_info["superfreespin_7_freeamount"];
			
			$collection_event_middle_stage_list[$i] = $freecoin_info["collection_event_middle_stage"];
			$collection_wheel_list[$i] = $freecoin_info["collection_wheel"];
			$collection_theme_reward_list[$i] = $freecoin_info["collection_theme_reward"];
			$collection_final_theme_reward_list[$i] = $freecoin_info["collection_final_theme_reward"];
			$advertisement_list[$i] = $freecoin_info["advertisement_freeamount"];
			
			$active_list[$i] = $dau_data[$i]["todayactivecount"];
		
			$freecoin_list_pointer--;
		}
		else
		{
			$total_amount_list[$i] = 0;
			$wheel_amount_list[$i] = 0;
			$tybonus_amount_list[$i] = 0;
			$time_amount_list[$i] = 0;
			$vipup_amount_list[$i] = 0;
			$treat_amount_list[$i] = 0;
			$celebup_amount_list[$i] = 0;
			$share_amount_list[$i] = 0;
			$inbox_amount_list[$i] = 0;			
			$bankrupt_amount_list[$i] = 0;
			$fanpage_freecoin_amount_list[$i] = 0;
			$fanpage_ty_amount_list[$i] = 0;
			$special_bankrupt_amount_list[$i] = 0;
			$welcomeback_amount_list[$i] = 0;
			$giveaway_amount_list[$i] = 0;
			$fame_tourney_amount_list[$i] = 0;
			$tutorial_treat_ticket_amount_list[$i] = 0;
			$fame_weekly_amount_list[$i] = 0;
			$fame_monthly_amount_list[$i] = 0;
			$invite_challenge_amount_list[$i] = 0;
			$platinum_wheel_amount_list[$i] = 0;
			$piggypot_amount_list[$i] = 0;
			$freespin_amount_list[$i] = 0;
			$wheel_4by_amount_list[$i] = 0;
			$wheel_2by_amount_list[$i] = 0;
			$dailystamp1_4by_amount_list[$i] = 0;
			$dailystamp2_4by_amount_list[$i] = 0;
			$dailystamp3_4by_amount_list[$i] = 0;
			$dailystamp4_4by_amount_list[$i] = 0;
			$dailystamp5_4by_amount_list[$i] = 0;
			$dailystamp6_4by_amount_list[$i] = 0;
			$dailystamp7_4by_amount_list[$i] = 0;
			$dailystamp1_2by_amount_list[$i] = 0;
			$dailystamp2_2by_amount_list[$i] = 0;
			$dailystamp3_2by_amount_list[$i] = 0;
			$dailystamp4_2by_amount_list[$i] = 0;
			$dailystamp5_2by_amount_list[$i] = 0;
			$dailystamp6_2by_amount_list[$i] = 0;
			$dailystamp7_2by_amount_list[$i] = 0;
			$dailystamp1_amount_list[$i] = 0;
			$dailystamp2_amount_list[$i] = 0;
			$dailystamp3_amount_list[$i] = 0;
			$dailystamp4_amount_list[$i] = 0;
			$dailystamp5_amount_list[$i] = 0;
			$dailystamp6_amount_list[$i] = 0;
			$dailystamp7_amount_list[$i] = 0;
			$dailyfreespin1_amount_list[$i] = 0;
			$dailyfreespin2_amount_list[$i] = 0;
			$dailyfreespin3_amount_list[$i] = 0;
			$dailyfreespin4_amount_list[$i] = 0;
			$dailyfreespin5_amount_list[$i] = 0;
			$dailyfreespin6_amount_list[$i] = 0;
			$dailyfreespin7_amount_list[$i] = 0;
			
			$dailyfreespin1_2by_amount_list[$i] = 0;
			$dailyfreespin2_2by_amount_list[$i] = 0;
			$dailyfreespin3_2by_amount_list[$i] = 0;
			$dailyfreespin4_2by_amount_list[$i] = 0;
			$dailyfreespin5_2by_amount_list[$i] = 0;
			$dailyfreespin6_2by_amount_list[$i] = 0;
			$dailyfreespin7_2by_amount_list[$i] = 0;
			
			$dailyfreespin1_4by_amount_list[$i] = 0;
			$dailyfreespin2_4by_amount_list[$i] = 0;
			$dailyfreespin3_4by_amount_list[$i] = 0;
			$dailyfreespin4_4by_amount_list[$i] = 0;
			$dailyfreespin5_4by_amount_list[$i] = 0;
			$dailyfreespin6_4by_amount_list[$i] = 0;
			$dailyfreespin7_4by_amount_list[$i] = 0;
			$pushon_amount_list[$i] = 0;
			$wakeup_amount_list[$i] = 0;
			$scratch_amount_list[$i] = 0;
			$fortune_amount_list[$i] = 0;
			$goldegg_amount_list[$i] = 0;
			$youtube_retention_amount_list[$i] = 0;
			$subscribe_wheel_freeamount_list[$i] = 0;
			
			$metaevent_stage1_freeamount_list[$i] = 0;
			$metaevent_stage2_freeamount_list[$i] = 0;
			$metaevent_stage3_freeamount_list[$i] = 0;
			$metaevent_stage4_freeamount_list[$i] = 0;
			$metaevent_stage5_freeamount_list[$i] = 0;
			$metaevent_stagefinal_freeamount_list[$i] = 0;
			
			$piggy_bank_bonus_freeamount_list[$i] = 0;
			$daily_multiplier_bonus_freeamount_list[$i] = 0;
			$weekly_card_bonus_freeamount_list[$i] = 0;
			$special_daily_wheel_freeamount_list[$i] = 0;
			$singleplay_levelup_freeamount_list[$i] = 0;
			
			$superfreespin_1_amount_list[$i] = 0;
			$superfreespin_2_amount_list[$i] = 0;
			$superfreespin_3_amount_list[$i] = 0;
			$superfreespin_4_amount_list[$i] = 0;
			$superfreespin_5_amount_list[$i] = 0;
			$superfreespin_6_amount_list[$i] = 0;
			$superfreespin_7_amount_list[$i] = 0;
			
			$collection_event_middle_stage_list[$i] = 0;
			$collection_wheel_list[$i] = 0;
			$collection_theme_reward_list[$i] = 0;
			$collection_final_theme_reward_list[$i] = 0;
			$advertisement_list[$i] = 0;
			
			$active_list[$i] = 0;
		}
		
		$date_list[$i] = $date_pointer;		 
		
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}

	
	$date_pointer = $enddate;
	
	for ($i=0; $i<$loop_count; $i++)
	{
		if ($admin_freecoin_list_pointer > 0)
			$today = $admin_freecoin_data[$admin_freecoin_list_pointer-1]["today"];

		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$total_info = $admin_freecoin_data[$admin_freecoin_list_pointer-1];
	
			$admin_freecoin_list[$i] = $date_pointer;
			$admin_freecoin_amount_list[$i] = $total_info["freeamount"];
	
			$admin_freecoin_list_pointer--;
		}
		else
		{
			$admin_freecoin_list[$i] = $date_pointer;
			$admin_freecoin_amount_list[$i] = 0;
		}
	
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	$date_pointer = $enddate;
	
	for ($i=0; $i<$loop_count; $i++)
	{
		write_log($vip_bankrupt_amount_pointer);
		if ($vip_bankrupt_amount_pointer > 0)
			$today = $vip_bankrupt_data[$vip_bankrupt_amount_pointer-1]["today"];
	
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$total_info = $vip_bankrupt_data[$vip_bankrupt_amount_pointer-1];
	
			$vip_bankrupt_list[$i] = $date_pointer;
			$vip_bankrupt_amount_list[$i] = $total_info["freeamount"];
	
			$vip_bankrupt_amount_pointer--;
		}
		else
		{
			$vip_bankrupt_list[$i] = $date_pointer;
			$vip_bankrupt_amount_list[$i] = 0;
		}
	
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	$date_pointer = $enddate;
	
	for ($i=0; $i<$loop_count; $i++)
	{
		if ($inbox_list_pointer > 0)
			$today = $inbox_list[$inbox_list_pointer-1]["today"];
	
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$total_info = $inbox_list[$inbox_list_pointer-1];
	
			$vip_daily_bonus_list[$i] = $date_pointer;
			$vip_daily_bonus_amount_list[$i] = $total_info["vip_daily_bonus_freeamount"];
			
			$mobile_popup_reward_list[$i] = $date_pointer;
			$mobile_popup_reward_amount_list[$i] = $total_info["mobile_popup_reward_bonus_freeamount"];
			
			$surprise_bonus_list[$i] = $date_pointer;
			$surprise_bonus_freeamount_list[$i] = $total_info["surprise_bonus_freeamount"];
			

			write_log($date_pointer."|".$total_info["vip_daily_bonus_freeamount"]);
	
			$inbox_list_pointer--;
		}
		else
		{
			$vip_daily_bonus_list[$i] = $date_pointer;
			$vip_daily_bonus_amount_list[$i] = 0;
			
			$mobile_popup_reward_list[$i] = $date_pointer;
			$mobile_popup_reward_amount_list[$i] = 0;
			
			$surprise_bonus_list[$i] = $date_pointer;
			$surprise_bonus_freeamount_list[$i] = 0;
		}
	
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	$db_main->end();
	$db_main2->end();
	$db_analysis->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});

	function drawChart() 
	{
		var data0 = new google.visualization.DataTable();
		
        data0.addColumn('string', '날짜');
        data0.addColumn('number', '지급금액');
        data0.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $total_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data0_avg = new google.visualization.DataTable();
		
		data0_avg.addColumn('string', '날짜');
		data0_avg.addColumn('number', '지급금액/활동유저수');
		data0_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1] == 0)?0:round($total_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data1 = new google.visualization.DataTable();
		
        data1.addColumn('string', '날짜');
        data1.addColumn('number', '지급금액');
        data1.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $wheel_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data1_avg = new google.visualization.DataTable();
		
		data1_avg.addColumn('string', '날짜');
		data1_avg.addColumn('number', '지급금액/활동유저수');
		data1_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1] == 0)?0:round($wheel_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data2 = new google.visualization.DataTable();
		
        data2.addColumn('string', '날짜');
        data2.addColumn('number', '지급금액');
        data2.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $tybonus_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data2_avg = new google.visualization.DataTable();
		
		data2_avg.addColumn('string', '날짜');
		data2_avg.addColumn('number', '지급금액/활동유저수');
		data2_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1] == 0)?0:round($tybonus_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data3 = new google.visualization.DataTable();
		
        data3.addColumn('string', '날짜');
        data3.addColumn('number', '지급금액');
        data3.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $time_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data3_avg = new google.visualization.DataTable();
		
		data3_avg.addColumn('string', '날짜');
		data3_avg.addColumn('number', '지급금액/활동유저수');
		data3_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($time_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data4 = new google.visualization.DataTable();
		
        data4.addColumn('string', '날짜');
        data4.addColumn('number', '지급금액');
        data4.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $vipup_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data4_avg = new google.visualization.DataTable();
		
		data4_avg.addColumn('string', '날짜');
		data4_avg.addColumn('number', '지급금액/활동유저수');
		data4_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1] == 0)?0:round($vipup_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data5 = new google.visualization.DataTable();
		
        data5.addColumn('string', '날짜');
        data5.addColumn('number', '지급금액');
        data5.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $treat_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data5_avg = new google.visualization.DataTable();
		
		data5_avg.addColumn('string', '날짜');
		data5_avg.addColumn('number', '지급금액/활동유저수');
		data5_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($treat_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data6 = new google.visualization.DataTable();
		
        data6.addColumn('string', '날짜');
        data6.addColumn('number', '지급금액');
        data6.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $celebup_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data6_avg = new google.visualization.DataTable();
		
		data6_avg.addColumn('string', '날짜');
		data6_avg.addColumn('number', '지급금액/활동유저수');
		data6_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($celebup_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data8 = new google.visualization.DataTable();
		
        data8.addColumn('string', '날짜');
        data8.addColumn('number', '지급금액');
        data8.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $share_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data8_avg = new google.visualization.DataTable();
		
		data8_avg.addColumn('string', '날짜');
		data8_avg.addColumn('number', '지급금액/활동유저수');
		data8_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($share_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data9 = new google.visualization.DataTable();
		
		data9.addColumn('string', '날짜');
		data9.addColumn('number', '지급금액');
		data9.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $bankrupt_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data9_avg = new google.visualization.DataTable();
		
		data9_avg.addColumn('string', '날짜');
		data9_avg.addColumn('number', '지급금액/활동유저수');
		data9_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($bankrupt_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data10 = new google.visualization.DataTable();
		
		data10.addColumn('string', '날짜');
		data10.addColumn('number', '지급금액');
		data10.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $vip_bankrupt_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data10_avg = new google.visualization.DataTable();
		
		data10_avg.addColumn('string', '날짜');
		data10_avg.addColumn('number', '지급금액/활동유저수');
		data10_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($vip_bankrupt_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data11 = new google.visualization.DataTable();
		
		data11.addColumn('string', '날짜');
		data11.addColumn('number', '지급금액');
		data11.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $fanpage_freecoin_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data11_avg = new google.visualization.DataTable();
		
		data11_avg.addColumn('string', '날짜');
		data11_avg.addColumn('number', '지급금액/활동유저수');
		data11_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($fanpage_freecoin_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data12 = new google.visualization.DataTable();
		
		data12.addColumn('string', '날짜');
		data12.addColumn('number', '지급금액');
		data12.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $fanpage_ty_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data12_avg = new google.visualization.DataTable();
		
		data12_avg.addColumn('string', '날짜');
		data12_avg.addColumn('number', '지급금액/활동유저수');
		data12_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($fanpage_ty_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data13 = new google.visualization.DataTable();
		
		data13.addColumn('string', '날짜');
		data13.addColumn('number', '지급금액');
		data13.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $special_bankrupt_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data13_avg = new google.visualization.DataTable();
		
		data13_avg.addColumn('string', '날짜');
		data13_avg.addColumn('number', '지급금액/활동유저수');
		data13_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($special_bankrupt_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data14 = new google.visualization.DataTable();
		
		data14.addColumn('string', '날짜');
		data14.addColumn('number', '지급금액');
		data14.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $welcomeback_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data14_avg = new google.visualization.DataTable();
		
		data14_avg.addColumn('string', '날짜');
		data14_avg.addColumn('number', '지급금액/활동유저수');
		data14_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($welcomeback_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data15 = new google.visualization.DataTable();
		
		data15.addColumn('string', '날짜');
		data15.addColumn('number', '지급금액');
		data15.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $giveaway_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data15_avg = new google.visualization.DataTable();
		
		data15_avg.addColumn('string', '날짜');
		data15_avg.addColumn('number', '지급금액/활동유저수');
		data15_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($giveaway_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data16 = new google.visualization.DataTable();
		
		data16.addColumn('string', '날짜');
		data16.addColumn('number', '지급금액');
		data16.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $fame_tourney_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data16_avg = new google.visualization.DataTable();
		
		data16_avg.addColumn('string', '날짜');
		data16_avg.addColumn('number', '지급금액/활동유저수');
		data16_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($fame_tourney_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data17 = new google.visualization.DataTable();
		
		data17.addColumn('string', '날짜');
		data17.addColumn('number', '지급금액');
		data17.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $tutorial_treat_ticket_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data17_avg = new google.visualization.DataTable();
		
		data17_avg.addColumn('string', '날짜');
		data17_avg.addColumn('number', '지급금액/활동유저수');
		data17_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($tutorial_treat_ticket_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data18 = new google.visualization.DataTable();
		
		data18.addColumn('string', '날짜');
		data18.addColumn('number', '지급금액');
		data18.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $fame_weekly_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data18_avg = new google.visualization.DataTable();
		
		data18_avg.addColumn('string', '날짜');
		data18_avg.addColumn('number', '지급금액/활동유저수');
		data18_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($fame_weekly_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data19 = new google.visualization.DataTable();
		
		data19.addColumn('string', '날짜');
		data19.addColumn('number', '지급금액');
		data19.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $fame_monthly_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data19_avg = new google.visualization.DataTable();
		
		data19_avg.addColumn('string', '날짜');
		data19_avg.addColumn('number', '지급금액/활동유저수');
		data19_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($fame_monthly_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data21 = new google.visualization.DataTable();
		
		data21.addColumn('string', '날짜');
		data21.addColumn('number', '지급금액');
		data21.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $invite_challenge_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data21_avg = new google.visualization.DataTable();
		
		data21_avg.addColumn('string', '날짜');
		data21_avg.addColumn('number', '지급금액/활동유저수');
		data21_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($invite_challenge_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data22 = new google.visualization.DataTable();
		
		data22.addColumn('string', '날짜');
		data22.addColumn('number', '지급금액');
		data22.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $platinum_wheel_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data22_avg = new google.visualization.DataTable();
		
		data22_avg.addColumn('string', '날짜');
		data22_avg.addColumn('number', '지급금액/활동유저수');
		data22_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($platinum_wheel_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data23 = new google.visualization.DataTable();
		
		data23.addColumn('string', '날짜');
		data23.addColumn('number', '지급금액');
		data23.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $piggypot_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data23_avg = new google.visualization.DataTable();
		
		data23_avg.addColumn('string', '날짜');
		data23_avg.addColumn('number', '지급금액/활동유저수');
		data23_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($piggypot_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data24 = new google.visualization.DataTable();
		
		data24.addColumn('string', '날짜');
		data24.addColumn('number', '지급금액');
		data24.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $freespin_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data24_avg = new google.visualization.DataTable();
		
		data24_avg.addColumn('string', '날짜');
		data24_avg.addColumn('number', '지급금액/활동유저수');
		data24_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($freespin_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data49 = new google.visualization.DataTable();
		
		data49.addColumn('string', '날짜');
		data49.addColumn('number', '지급금액');
		data49.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $wheel_4by_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data49_avg = new google.visualization.DataTable();
		
		data49_avg.addColumn('string', '날짜');
		data49_avg.addColumn('number', '지급금액/활동유저수');
		data49_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($wheel_4by_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);        

		var data50 = new google.visualization.DataTable();
		
		data50.addColumn('string', '날짜');
		data50.addColumn('number', '지급금액');
		data50.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $wheel_2by_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data50_avg = new google.visualization.DataTable();
		
		data50_avg.addColumn('string', '날짜');
		data50_avg.addColumn('number', '지급금액/활동유저수');
		data50_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($wheel_2by_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data51 = new google.visualization.DataTable();
		
		data51.addColumn('string', '날짜');
		data51.addColumn('number', 'Day 1');
		data51.addColumn('number', 'Day 2');
		data51.addColumn('number', 'Day 3');
		data51.addColumn('number', 'Day 4');
		data51.addColumn('number', 'Day 5');
		data51.addColumn('number', 'Day 6');
		data51.addColumn('number', 'Day 7');
		data51.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount_1 = $dailystamp1_2by_amount_list[$i-1];
        $_amount_2 = $dailystamp2_2by_amount_list[$i-1];
        $_amount_3 = $dailystamp3_2by_amount_list[$i-1];
        $_amount_4 = $dailystamp4_2by_amount_list[$i-1];
        $_amount_5 = $dailystamp5_2by_amount_list[$i-1];
        $_amount_6 = $dailystamp6_2by_amount_list[$i-1];
        $_amount_7 = $dailystamp7_2by_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_1 != "")
        	echo(",{v:".$_amount_1.",f:'".number_format($_amount_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_2 != "")
        	echo(",{v:".$_amount_2.",f:'".number_format($_amount_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_3 != "")
        	echo(",{v:".$_amount_3.",f:'".number_format($_amount_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_4 != "")
        	echo(",{v:".$_amount_4.",f:'".number_format($_amount_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_5 != "")
        	echo(",{v:".$_amount_5.",f:'".number_format($_amount_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_6 != "")
        	echo(",{v:".$_amount_6.",f:'".number_format($_amount_6)."'}");
        else
        	echo(",0");
        
        if ($_amount_7 != "")
        	echo(",{v:".$_amount_7.",f:'".number_format($_amount_7)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

        

		var data51_avg = new google.visualization.DataTable();
		
		data51_avg.addColumn('string', '날짜');
		data51_avg.addColumn('number', 'Day 1');
		data51_avg.addColumn('number', 'Day 2');
		data51_avg.addColumn('number', 'Day 3');
		data51_avg.addColumn('number', 'Day 4');
		data51_avg.addColumn('number', 'Day 5');
		data51_avg.addColumn('number', 'Day 6');
		data51_avg.addColumn('number', 'Day 7');
		data51_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {

        $_amount_avg_1 = ($active_list[$i-1]==0)?0:round($dailystamp1_2by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_2 = ($active_list[$i-1]==0)?0:round($dailystamp2_2by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_3 = ($active_list[$i-1]==0)?0:round($dailystamp3_2by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_4 = ($active_list[$i-1]==0)?0:round($dailystamp4_2by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_5 = ($active_list[$i-1]==0)?0:round($dailystamp5_2by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_6 = ($active_list[$i-1]==0)?0:round($dailystamp6_2by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_7 = ($active_list[$i-1]==0)?0:round($dailystamp7_2by_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_avg_1 != "")
        	echo(",{v:".$_amount_avg_1.",f:'".number_format($_amount_avg_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_2 != "")
        	echo(",{v:".$_amount_avg_2.",f:'".number_format($_amount_avg_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_3 != "")
        	echo(",{v:".$_amount_avg_3.",f:'".number_format($_amount_avg_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_4 != "")
        	echo(",{v:".$_amount_avg_4.",f:'".number_format($_amount_avg_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_5 != "")
        	echo(",{v:".$_amount_avg_5.",f:'".number_format($_amount_avg_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_6 != "")
        	echo(",{v:".$_amount_avg_6.",f:'".number_format($_amount_avg_6)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_7 != "")
        	echo(",{v:".$_amount_avg_7.",f:'".number_format($_amount_avg_7)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);


		var data61 = new google.visualization.DataTable();
		
		data61.addColumn('string', '날짜');
		data61.addColumn('number', 'Day 1');
		data61.addColumn('number', 'Day 2');
		data61.addColumn('number', 'Day 3');
		data61.addColumn('number', 'Day 4');
		data61.addColumn('number', 'Day 5');
		data61.addColumn('number', 'Day 6');
		data61.addColumn('number', 'Day 7');
		data61.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount_1 = $dailystamp1_4by_amount_list[$i-1];
        $_amount_2 = $dailystamp2_4by_amount_list[$i-1];
        $_amount_3 = $dailystamp3_4by_amount_list[$i-1];
        $_amount_4 = $dailystamp4_4by_amount_list[$i-1];
        $_amount_5 = $dailystamp5_4by_amount_list[$i-1];
        $_amount_6 = $dailystamp6_4by_amount_list[$i-1];
        $_amount_7 = $dailystamp7_4by_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_1 != "")
        	echo(",{v:".$_amount_1.",f:'".number_format($_amount_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_2 != "")
        	echo(",{v:".$_amount_2.",f:'".number_format($_amount_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_3 != "")
        	echo(",{v:".$_amount_3.",f:'".number_format($_amount_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_4 != "")
        	echo(",{v:".$_amount_4.",f:'".number_format($_amount_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_5 != "")
        	echo(",{v:".$_amount_5.",f:'".number_format($_amount_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_6 != "")
        	echo(",{v:".$_amount_6.",f:'".number_format($_amount_6)."'}");
        else
        	echo(",0");
        
        if ($_amount_7 != "")
        	echo(",{v:".$_amount_7.",f:'".number_format($_amount_7)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

        

		var data61_avg = new google.visualization.DataTable();
		
		data61_avg.addColumn('string', '날짜');
		data61_avg.addColumn('number', 'Day 1');
		data61_avg.addColumn('number', 'Day 2');
		data61_avg.addColumn('number', 'Day 3');
		data61_avg.addColumn('number', 'Day 4');
		data61_avg.addColumn('number', 'Day 5');
		data61_avg.addColumn('number', 'Day 6');
		data61_avg.addColumn('number', 'Day 7');
		data61_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {

        $_amount_avg_1 = ($active_list[$i-1]==0)?0:round($dailystamp1_4by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_2 = ($active_list[$i-1]==0)?0:round($dailystamp2_4by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_3 = ($active_list[$i-1]==0)?0:round($dailystamp3_4by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_4 = ($active_list[$i-1]==0)?0:round($dailystamp4_4by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_5 = ($active_list[$i-1]==0)?0:round($dailystamp5_4by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_6 = ($active_list[$i-1]==0)?0:round($dailystamp6_4by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_7 = ($active_list[$i-1]==0)?0:round($dailystamp7_4by_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_avg_1 != "")
        	echo(",{v:".$_amount_avg_1.",f:'".number_format($_amount_avg_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_2 != "")
        	echo(",{v:".$_amount_avg_2.",f:'".number_format($_amount_avg_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_3 != "")
        	echo(",{v:".$_amount_avg_3.",f:'".number_format($_amount_avg_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_4 != "")
        	echo(",{v:".$_amount_avg_4.",f:'".number_format($_amount_avg_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_5 != "")
        	echo(",{v:".$_amount_avg_5.",f:'".number_format($_amount_avg_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_6 != "")
        	echo(",{v:".$_amount_avg_6.",f:'".number_format($_amount_avg_6)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_7 != "")
        	echo(",{v:".$_amount_avg_7.",f:'".number_format($_amount_avg_7)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

		var data100 = new google.visualization.DataTable();
		
		data100.addColumn('string', '날짜');
		data100.addColumn('number', '지급금액');
		data100.addColumn('number', '관리자 지급금액');
		data100.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $inbox_amount_list[$i-1];
        $_admin_amount = $admin_freecoin_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}");
		else
			echo(",0");
		
		if ($_admin_amount != "")
			echo(",{v:".$_admin_amount.",f:'".number_format($_admin_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data100_avg = new google.visualization.DataTable();
		
		data100_avg.addColumn('string', '날짜');
		data100_avg.addColumn('number', '지급금액/활동유저수');
		data100_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($inbox_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data101 = new google.visualization.DataTable();
		
		data101.addColumn('string', '날짜');
		data101.addColumn('number', 'Day 1');
		data101.addColumn('number', 'Day 2');
		data101.addColumn('number', 'Day 3');
		data101.addColumn('number', 'Day 4');
		data101.addColumn('number', 'Day 5');
		data101.addColumn('number', 'Day 6');
		data101.addColumn('number', 'Day 7');
		data101.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount_1 = $dailystamp1_amount_list[$i-1];
        $_amount_2 = $dailystamp2_amount_list[$i-1];
        $_amount_3 = $dailystamp3_amount_list[$i-1];
        $_amount_4 = $dailystamp4_amount_list[$i-1];
        $_amount_5 = $dailystamp5_amount_list[$i-1];
        $_amount_6 = $dailystamp6_amount_list[$i-1];
        $_amount_7 = $dailystamp7_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_1 != "")
        	echo(",{v:".$_amount_1.",f:'".number_format($_amount_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_2 != "")
        	echo(",{v:".$_amount_2.",f:'".number_format($_amount_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_3 != "")
        	echo(",{v:".$_amount_3.",f:'".number_format($_amount_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_4 != "")
        	echo(",{v:".$_amount_4.",f:'".number_format($_amount_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_5 != "")
        	echo(",{v:".$_amount_5.",f:'".number_format($_amount_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_6 != "")
        	echo(",{v:".$_amount_6.",f:'".number_format($_amount_6)."'}");
        else
        	echo(",0");
        
        if ($_amount_7 != "")
        	echo(",{v:".$_amount_7.",f:'".number_format($_amount_7)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

        

		var data101_avg = new google.visualization.DataTable();
		
		data101_avg.addColumn('string', '날짜');
		data101_avg.addColumn('number', 'Day 1');
		data101_avg.addColumn('number', 'Day 2');
		data101_avg.addColumn('number', 'Day 3');
		data101_avg.addColumn('number', 'Day 4');
		data101_avg.addColumn('number', 'Day 5');
		data101_avg.addColumn('number', 'Day 6');
		data101_avg.addColumn('number', 'Day 7');
		data101_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {

        $_amount_avg_1 = ($active_list[$i-1]==0)?0:round($dailystamp1_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_2 = ($active_list[$i-1]==0)?0:round($dailystamp2_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_3 = ($active_list[$i-1]==0)?0:round($dailystamp3_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_4 = ($active_list[$i-1]==0)?0:round($dailystamp4_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_5 = ($active_list[$i-1]==0)?0:round($dailystamp5_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_6 = ($active_list[$i-1]==0)?0:round($dailystamp6_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_7 = ($active_list[$i-1]==0)?0: round($dailystamp7_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_avg_1 != "")
        	echo(",{v:".$_amount_avg_1.",f:'".number_format($_amount_avg_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_2 != "")
        	echo(",{v:".$_amount_avg_2.",f:'".number_format($_amount_avg_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_3 != "")
        	echo(",{v:".$_amount_avg_3.",f:'".number_format($_amount_avg_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_4 != "")
        	echo(",{v:".$_amount_avg_4.",f:'".number_format($_amount_avg_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_5 != "")
        	echo(",{v:".$_amount_avg_5.",f:'".number_format($_amount_avg_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_6 != "")
        	echo(",{v:".$_amount_avg_6.",f:'".number_format($_amount_avg_6)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_7 != "")
        	echo(",{v:".$_amount_avg_7.",f:'".number_format($_amount_avg_7)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

		var data127 = new google.visualization.DataTable();
		
		data127.addColumn('string', '날짜');
		data127.addColumn('number', '지급금액');
		data127.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $pushon_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data127_avg = new google.visualization.DataTable();
		
		data127_avg.addColumn('string', '날짜');
		data127_avg.addColumn('number', '지급금액/활동유저수');
		data127_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($pushon_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data500 = new google.visualization.DataTable();
		
		data500.addColumn('string', '날짜');
		data500.addColumn('number', '지급금액');
		data500.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $wakeup_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data500_avg = new google.visualization.DataTable();
		
		data500_avg.addColumn('string', '날짜');
		data500_avg.addColumn('number', '지급금액/활동유저수');
		data500_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($wakeup_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data100_701 = new google.visualization.DataTable();
		
		data100_701.addColumn('string', '날짜');
		data100_701.addColumn('number', '지급금액');
		data100_701.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $vip_daily_bonus_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data100_701_avg = new google.visualization.DataTable();
		
		data100_701_avg.addColumn('string', '날짜');
		data100_701_avg.addColumn('number', '지급금액/활동유저수');
		data100_701_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($vip_daily_bonus_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);
        
		var data100_801 = new google.visualization.DataTable();
		
		data100_801.addColumn('string', '날짜');
		data100_801.addColumn('number', '지급금액');
		data100_801.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $mobile_popup_reward_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data100_801_avg = new google.visualization.DataTable();
		
		data100_801_avg.addColumn('string', '날짜');
		data100_801_avg.addColumn('number', '지급금액/활동유저수');
		data100_801_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($mobile_popup_reward_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

	var data96 = new google.visualization.DataTable();
		
		data96.addColumn('string', '날짜');
		data96.addColumn('number', '지급금액');
		data96.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $scratch_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);
        
	var data96_avg = new google.visualization.DataTable();
		
		data96_avg.addColumn('string', '날짜');
		data96_avg.addColumn('number', '지급금액/활동유저수');
		data96_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($scratch_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);
        
	var data97 = new google.visualization.DataTable();
		
		data97.addColumn('string', '날짜');
		data97.addColumn('number', '지급금액');
		data97.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $fortune_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);
        
	var data97_avg = new google.visualization.DataTable();
		
		data97_avg.addColumn('string', '날짜');
		data97_avg.addColumn('number', '지급금액/활동유저수');
		data97_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($fortune_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);
        
	var data98 = new google.visualization.DataTable();
		
		data98.addColumn('string', '날짜');
		data98.addColumn('number', '지급금액');
		data98.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $goldegg_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);
        
	var data98_avg = new google.visualization.DataTable();
		
		data98_avg.addColumn('string', '날짜');
		data98_avg.addColumn('number', '지급금액/활동유저수');
		data98_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($goldegg_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);
        
            	
		var options = {
                title:'',                                                      
                width:1050,                         
                height:200,
                axisTitlesPosition:'in',
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize : 12,
                chartArea:{left:80,top:40,width:1020,height:130}
        };
	var data99 = new google.visualization.DataTable();
		
		data99.addColumn('string', '날짜');
		data99.addColumn('number', '지급금액');
		data99.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $youtube_retention_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);
        
	var data99_avg = new google.visualization.DataTable();
		
		data99_avg.addColumn('string', '날짜');
		data99_avg.addColumn('number', '지급금액/활동유저수');
		data99_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($youtube_retention_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

        
	var data100_901 = new google.visualization.DataTable();
		
		data100_901.addColumn('string', '날짜');
		data100_901.addColumn('number', '지급금액');
		data100_901.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $surprise_bonus_freeamount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);

		var data100_901_avg = new google.visualization.DataTable();
		
		data100_901_avg.addColumn('string', '날짜');
		data100_901_avg.addColumn('number', '지급금액/활동유저수');
		data100_901_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($surprise_bonus_freeamount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);
var data68 = new google.visualization.DataTable();
		
		data68.addColumn('string', '날짜');
		data68.addColumn('number', 'Day 1');
		data68.addColumn('number', 'Day 2');
		data68.addColumn('number', 'Day 3');
		data68.addColumn('number', 'Day 4');
		data68.addColumn('number', 'Day 5');
		data68.addColumn('number', 'Day 6');
		data68.addColumn('number', 'Day 7');
		data68.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount_1 = $dailyfreespin1_amount_list[$i-1];
        $_amount_2 = $dailyfreespin2_amount_list[$i-1];
        $_amount_3 = $dailyfreespin3_amount_list[$i-1];
        $_amount_4 = $dailyfreespin4_amount_list[$i-1];
        $_amount_5 = $dailyfreespin5_amount_list[$i-1];
        $_amount_6 = $dailyfreespin6_amount_list[$i-1];
        $_amount_7 = $dailyfreespin7_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_1 != "")
        	echo(",{v:".$_amount_1.",f:'".number_format($_amount_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_2 != "")
        	echo(",{v:".$_amount_2.",f:'".number_format($_amount_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_3 != "")
        	echo(",{v:".$_amount_3.",f:'".number_format($_amount_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_4 != "")
        	echo(",{v:".$_amount_4.",f:'".number_format($_amount_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_5 != "")
        	echo(",{v:".$_amount_5.",f:'".number_format($_amount_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_6 != "")
        	echo(",{v:".$_amount_6.",f:'".number_format($_amount_6)."'}");
        else
        	echo(",0");
        
        if ($_amount_7 != "")
        	echo(",{v:".$_amount_7.",f:'".number_format($_amount_7)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

        

		var data68_avg = new google.visualization.DataTable();
		
		data68_avg.addColumn('string', '날짜');
		data68_avg.addColumn('number', 'Day 1');
		data68_avg.addColumn('number', 'Day 2');
		data68_avg.addColumn('number', 'Day 3');
		data68_avg.addColumn('number', 'Day 4');
		data68_avg.addColumn('number', 'Day 5');
		data68_avg.addColumn('number', 'Day 6');
		data68_avg.addColumn('number', 'Day 7');
		data68_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {

        $_amount_avg_1 = ($active_list[$i-1]==0)?0:round($dailyfreespin1_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_2 = ($active_list[$i-1]==0)?0:round($dailyfreespin2_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_3 = ($active_list[$i-1]==0)?0:round($dailyfreespin3_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_4 = ($active_list[$i-1]==0)?0:round($dailyfreespin4_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_5 = ($active_list[$i-1]==0)?0:round($dailyfreespin5_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_6 = ($active_list[$i-1]==0)?0:round($dailyfreespin6_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_7 = ($active_list[$i-1]==0)?0: round($dailyfreespin7_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_avg_1 != "")
        	echo(",{v:".$_amount_avg_1.",f:'".number_format($_amount_avg_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_2 != "")
        	echo(",{v:".$_amount_avg_2.",f:'".number_format($_amount_avg_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_3 != "")
        	echo(",{v:".$_amount_avg_3.",f:'".number_format($_amount_avg_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_4 != "")
        	echo(",{v:".$_amount_avg_4.",f:'".number_format($_amount_avg_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_5 != "")
        	echo(",{v:".$_amount_avg_5.",f:'".number_format($_amount_avg_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_6 != "")
        	echo(",{v:".$_amount_avg_6.",f:'".number_format($_amount_avg_6)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_7 != "")
        	echo(",{v:".$_amount_avg_7.",f:'".number_format($_amount_avg_7)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

		var data75 = new google.visualization.DataTable();
		
		data75.addColumn('string', '날짜');
		data75.addColumn('number', 'Day 1');
		data75.addColumn('number', 'Day 2');
		data75.addColumn('number', 'Day 3');
		data75.addColumn('number', 'Day 4');
		data75.addColumn('number', 'Day 5');
		data75.addColumn('number', 'Day 6');
		data75.addColumn('number', 'Day 7');
		data75.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount_1 = $dailyfreespin1_2by_amount_list[$i-1];
        $_amount_2 = $dailyfreespin2_2by_amount_list[$i-1];
        $_amount_3 = $dailyfreespin3_2by_amount_list[$i-1];
        $_amount_4 = $dailyfreespin4_2by_amount_list[$i-1];
        $_amount_5 = $dailyfreespin5_2by_amount_list[$i-1];
        $_amount_6 = $dailyfreespin6_2by_amount_list[$i-1];
        $_amount_7 = $dailyfreespin7_2by_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_1 != "")
        	echo(",{v:".$_amount_1.",f:'".number_format($_amount_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_2 != "")
        	echo(",{v:".$_amount_2.",f:'".number_format($_amount_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_3 != "")
        	echo(",{v:".$_amount_3.",f:'".number_format($_amount_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_4 != "")
        	echo(",{v:".$_amount_4.",f:'".number_format($_amount_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_5 != "")
        	echo(",{v:".$_amount_5.",f:'".number_format($_amount_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_6 != "")
        	echo(",{v:".$_amount_6.",f:'".number_format($_amount_6)."'}");
        else
        	echo(",0");
        
        if ($_amount_7 != "")
        	echo(",{v:".$_amount_7.",f:'".number_format($_amount_7)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

        

		var data75_avg = new google.visualization.DataTable();
		
		data75_avg.addColumn('string', '날짜');
		data75_avg.addColumn('number', 'Day 1');
		data75_avg.addColumn('number', 'Day 2');
		data75_avg.addColumn('number', 'Day 3');
		data75_avg.addColumn('number', 'Day 4');
		data75_avg.addColumn('number', 'Day 5');
		data75_avg.addColumn('number', 'Day 6');
		data75_avg.addColumn('number', 'Day 7');
		data75_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {

        $_amount_avg_1 = ($active_list[$i-1]==0)?0:round($dailyfreespin1_2by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_2 = ($active_list[$i-1]==0)?0:round($dailyfreespin2_2by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_3 = ($active_list[$i-1]==0)?0:round($dailyfreespin3_2by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_4 = ($active_list[$i-1]==0)?0:round($dailyfreespin4_2by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_5 = ($active_list[$i-1]==0)?0:round($dailyfreespin5_2by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_6 = ($active_list[$i-1]==0)?0:round($dailyfreespin6_2by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_7 = ($active_list[$i-1]==0)?0: round($dailyfreespin7_2by_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_avg_1 != "")
        	echo(",{v:".$_amount_avg_1.",f:'".number_format($_amount_avg_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_2 != "")
        	echo(",{v:".$_amount_avg_2.",f:'".number_format($_amount_avg_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_3 != "")
        	echo(",{v:".$_amount_avg_3.",f:'".number_format($_amount_avg_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_4 != "")
        	echo(",{v:".$_amount_avg_4.",f:'".number_format($_amount_avg_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_5 != "")
        	echo(",{v:".$_amount_avg_5.",f:'".number_format($_amount_avg_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_6 != "")
        	echo(",{v:".$_amount_avg_6.",f:'".number_format($_amount_avg_6)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_7 != "")
        	echo(",{v:".$_amount_avg_7.",f:'".number_format($_amount_avg_7)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);
        
	var data82 = new google.visualization.DataTable();
		
		data82.addColumn('string', '날짜');
		data82.addColumn('number', 'Day 1');
		data82.addColumn('number', 'Day 2');
		data82.addColumn('number', 'Day 3');
		data82.addColumn('number', 'Day 4');
		data82.addColumn('number', 'Day 5');
		data82.addColumn('number', 'Day 6');
		data82.addColumn('number', 'Day 7');
		data82.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount_1 = $dailyfreespin1_4by_amount_list[$i-1];
        $_amount_2 = $dailyfreespin2_4by_amount_list[$i-1];
        $_amount_3 = $dailyfreespin3_4by_amount_list[$i-1];
        $_amount_4 = $dailyfreespin4_4by_amount_list[$i-1];
        $_amount_5 = $dailyfreespin5_4by_amount_list[$i-1];
        $_amount_6 = $dailyfreespin6_4by_amount_list[$i-1];
        $_amount_7 = $dailyfreespin7_4by_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_1 != "")
        	echo(",{v:".$_amount_1.",f:'".number_format($_amount_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_2 != "")
        	echo(",{v:".$_amount_2.",f:'".number_format($_amount_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_3 != "")
        	echo(",{v:".$_amount_3.",f:'".number_format($_amount_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_4 != "")
        	echo(",{v:".$_amount_4.",f:'".number_format($_amount_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_5 != "")
        	echo(",{v:".$_amount_5.",f:'".number_format($_amount_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_6 != "")
        	echo(",{v:".$_amount_6.",f:'".number_format($_amount_6)."'}");
        else
        	echo(",0");
        
        if ($_amount_7 != "")
        	echo(",{v:".$_amount_7.",f:'".number_format($_amount_7)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

        

		var data82_avg = new google.visualization.DataTable();
		
		data82_avg.addColumn('string', '날짜');
		data82_avg.addColumn('number', 'Day 1');
		data82_avg.addColumn('number', 'Day 2');
		data82_avg.addColumn('number', 'Day 3');
		data82_avg.addColumn('number', 'Day 4');
		data82_avg.addColumn('number', 'Day 5');
		data82_avg.addColumn('number', 'Day 6');
		data82_avg.addColumn('number', 'Day 7');
		data82_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {

        $_amount_avg_1 = ($active_list[$i-1]==0)?0:round($dailyfreespin1_4by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_2 = ($active_list[$i-1]==0)?0:round($dailyfreespin2_4by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_3 = ($active_list[$i-1]==0)?0:round($dailyfreespin3_4by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_4 = ($active_list[$i-1]==0)?0:round($dailyfreespin4_4by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_5 = ($active_list[$i-1]==0)?0:round($dailyfreespin5_4by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_6 = ($active_list[$i-1]==0)?0:round($dailyfreespin6_4by_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_7 = ($active_list[$i-1]==0)?0: round($dailyfreespin7_4by_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_avg_1 != "")
        	echo(",{v:".$_amount_avg_1.",f:'".number_format($_amount_avg_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_2 != "")
        	echo(",{v:".$_amount_avg_2.",f:'".number_format($_amount_avg_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_3 != "")
        	echo(",{v:".$_amount_avg_3.",f:'".number_format($_amount_avg_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_4 != "")
        	echo(",{v:".$_amount_avg_4.",f:'".number_format($_amount_avg_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_5 != "")
        	echo(",{v:".$_amount_avg_5.",f:'".number_format($_amount_avg_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_6 != "")
        	echo(",{v:".$_amount_avg_6.",f:'".number_format($_amount_avg_6)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_7 != "")
        	echo(",{v:".$_amount_avg_7.",f:'".number_format($_amount_avg_7)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);
	var data113 = new google.visualization.DataTable();
		
		data113.addColumn('string', '날짜');
		data113.addColumn('number', '지급금액');
		data113.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = $subscribe_wheel_freeamount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);
        
	var data113_avg = new google.visualization.DataTable();
		
		data113_avg.addColumn('string', '날짜');
		data113_avg.addColumn('number', '지급금액/활동유저수');
		data113_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount = ($active_list[$i-1]==0)?0:round($subscribe_wheel_freeamount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");

		if ($_amount != "")
			echo(",{v:".$_amount.",f:'".number_format($_amount)."'}]");
		else
			echo(",0]");
		
        if ($i > 1)
            echo(",");
    }
?>
        ]);
        
	var data200 = new google.visualization.DataTable();
		
		data200.addColumn('string', '날짜');
		data200.addColumn('number', 'stage 1');
		data200.addColumn('number', 'stage 2');
		data200.addColumn('number', 'stage 3');
		data200.addColumn('number', 'stage 4');
		data200.addColumn('number', 'stage 5');
		data200.addColumn('number', 'stage final');
		data200.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        
        $_amount_1 = $metaevent_stage1_freeamount_list[$i-1];
        $_amount_2 = $metaevent_stage2_freeamount_list[$i-1];
        $_amount_3 = $metaevent_stage3_freeamount_list[$i-1];
        $_amount_4 = $metaevent_stage4_freeamount_list[$i-1];
        $_amount_5 = $metaevent_stage5_freeamount_list[$i-1];
        $_amount_6 = $metaevent_stagefinal_freeamount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_1 != "")
        	echo(",{v:".$_amount_1.",f:'".number_format($_amount_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_2 != "")
        	echo(",{v:".$_amount_2.",f:'".number_format($_amount_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_3 != "")
        	echo(",{v:".$_amount_3.",f:'".number_format($_amount_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_4 != "")
        	echo(",{v:".$_amount_4.",f:'".number_format($_amount_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_5 != "")
        	echo(",{v:".$_amount_5.",f:'".number_format($_amount_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_6 != "")
        	echo(",{v:".$_amount_6.",f:'".number_format($_amount_6)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

        

		var data200_avg = new google.visualization.DataTable();
		
		data200_avg.addColumn('string', '날짜');
		data200_avg.addColumn('number', 'stage1');
		data200_avg.addColumn('number', 'stage2');
		data200_avg.addColumn('number', 'stage3');
		data200_avg.addColumn('number', 'stage4');
		data200_avg.addColumn('number', 'stage5');
		data200_avg.addColumn('number', 'stagefinal');
		data200_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {

        $_amount_avg_1 = ($active_list[$i-1]==0)?0:round($metaevent_stage1_freeamount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_2 = ($active_list[$i-1]==0)?0:round($metaevent_stage2_freeamount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_3 = ($active_list[$i-1]==0)?0:round($metaevent_stage3_freeamount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_4 = ($active_list[$i-1]==0)?0:round($metaevent_stage4_freeamount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_5 = ($active_list[$i-1]==0)?0:round($metaevent_stage5_freeamount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_6 = ($active_list[$i-1]==0)?0:round($metaevent_stage6_freeamount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_avg_1 != "")
        	echo(",{v:".$_amount_avg_1.",f:'".number_format($_amount_avg_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_2 != "")
        	echo(",{v:".$_amount_avg_2.",f:'".number_format($_amount_avg_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_3 != "")
        	echo(",{v:".$_amount_avg_3.",f:'".number_format($_amount_avg_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_4 != "")
        	echo(",{v:".$_amount_avg_4.",f:'".number_format($_amount_avg_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_5 != "")
        	echo(",{v:".$_amount_avg_5.",f:'".number_format($_amount_avg_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_6 != "")
        	echo(",{v:".$_amount_avg_6.",f:'".number_format($_amount_avg_6)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);
        
        
	var data142 = new google.visualization.DataTable();
		
		data142.addColumn('string', '날짜');
		data142.addColumn('number', 'Piggy Bank');
		data142.addColumn('number', 'Daily Multiplier');
		data142.addColumn('number', 'Weekly Card');
		data142.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        
        $piggy_bank_bonus_freeamount = $piggy_bank_bonus_freeamount_list[$i-1];
        $daily_multiplier_bonus_freeamount = $daily_multiplier_bonus_freeamount_list[$i-1];
        $weekly_card_bonus_freeamount = $weekly_card_bonus_freeamount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($piggy_bank_bonus_freeamount != "")
            echo(",{v:".$piggy_bank_bonus_freeamount.",f:'".number_format($piggy_bank_bonus_freeamount)."'}");
        else
        	echo(",0");
        
        if ($daily_multiplier_bonus_freeamount != "")
            echo(",{v:".$daily_multiplier_bonus_freeamount.",f:'".number_format($daily_multiplier_bonus_freeamount)."'}");
        else
        	echo(",0");
        
        if ($weekly_card_bonus_freeamount != "")
            echo(",{v:".$weekly_card_bonus_freeamount.",f:'".number_format($weekly_card_bonus_freeamount)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

        

		var data142_avg = new google.visualization.DataTable();
		
		data142_avg.addColumn('string', '날짜');
		data142_avg.addColumn('number', 'Piggy Bank');
		data142_avg.addColumn('number', 'Daily Multiplier');
		data142_avg.addColumn('number', 'Weekly Card');
		data142_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        
        $piggy_bank_bonus_freeamount_avg = ($active_list[$i-1]==0)?0:round($piggy_bank_bonus_freeamount_list[$i-1]/ $active_list[$i-1]);
        $daily_multiplier_bonus_freeamount_avg = ($active_list[$i-1]==0)?0:round($daily_multiplier_bonus_freeamount_list[$i-1]/ $active_list[$i-1]);
        $weekly_card_bonus_freeamount_avg =($active_list[$i-1]==0)?0:round($weekly_card_bonus_freeamount_list[$i-1]/ $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($piggy_bank_bonus_freeamount_avg != "")
            echo(",{v:".$piggy_bank_bonus_freeamount_avg.",f:'".number_format($piggy_bank_bonus_freeamount_avg)."'}");
        else
        	echo(",0");
        
        if ($daily_multiplier_bonus_freeamount_avg != "")
            echo(",{v:".$daily_multiplier_bonus_freeamount_avg.",f:'".number_format($daily_multiplier_bonus_freeamount_avg)."'}");
        else
        	echo(",0");
        
        if ($weekly_card_bonus_freeamount_avg != "")
            echo(",{v:".$weekly_card_bonus_freeamount_avg.",f:'".number_format($weekly_card_bonus_freeamount_avg)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);
        
        
	var data145 = new google.visualization.DataTable();
		
		data145.addColumn('string', '날짜');
		data145.addColumn('number', '지금금액');
		data145.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        
        $special_daily_wheel_freeamount = $special_daily_wheel_freeamount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($piggy_bank_bonus_freeamount != "")
            echo(",{v:".$special_daily_wheel_freeamount.",f:'".number_format($special_daily_wheel_freeamount)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

        

		var data145_avg = new google.visualization.DataTable();
		
		data145_avg.addColumn('string', '날짜');
		data145_avg.addColumn('number', '지금금액');
		data145_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $special_daily_wheel_freeamount_avg = ($active_list[$i-1]==0)?0:round($special_daily_wheel_freeamount_list[$i-1]/ $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($special_daily_wheel_freeamount_avg != "")
            echo(",{v:".$special_daily_wheel_freeamount_avg.",f:'".number_format($special_daily_wheel_freeamount_avg)."'}]");
        else
        	echo(",0]");
     
        if ($i > 1)
        	echo(",");
    }
?>
        ]);
	var data146 = new google.visualization.DataTable();
		
		data146.addColumn('string', '날짜');
		data146.addColumn('number', '지금금액');
		data146.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        
        $singleplay_levelup_freeamount = $singleplay_levelup_freeamount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($piggy_bank_bonus_freeamount != "")
            echo(",{v:".$singleplay_levelup_freeamount.",f:'".number_format($singleplay_levelup_freeamount)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

        

		var data146_avg = new google.visualization.DataTable();
		
		data146_avg.addColumn('string', '날짜');
		data146_avg.addColumn('number', '지금금액');
		data146_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $singleplay_levelup_freeamount_avg = ($active_list[$i-1]==0)?0:round($singleplay_levelup_freeamount_list[$i-1]/ $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($singleplay_levelup_freeamount_avg != "")
            echo(",{v:".$singleplay_levelup_freeamount_avg.",f:'".number_format($singleplay_levelup_freeamount_avg)."'}]");
        else
        	echo(",0]");
     
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

		var data148 = new google.visualization.DataTable();
		
		data148.addColumn('string', '날짜');
		data148.addColumn('number', 'Day 1');
		data148.addColumn('number', 'Day 2');
		data148.addColumn('number', 'Day 3');
		data148.addColumn('number', 'Day 4');
		data148.addColumn('number', 'Day 5');
		data148.addColumn('number', 'Day 6');
		data148.addColumn('number', 'Day 7');
		data148.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_amount_1 = $superfreespin_1_amount_list[$i-1];
        $_amount_2 = $superfreespin_2_amount_list[$i-1];
        $_amount_3 = $superfreespin_3_amount_list[$i-1];
        $_amount_4 = $superfreespin_4_amount_list[$i-1];
        $_amount_5 = $superfreespin_5_amount_list[$i-1];
        $_amount_6 = $superfreespin_6_amount_list[$i-1];
        $_amount_7 = $superfreespin_7_amount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_1 != "")
        	echo(",{v:".$_amount_1.",f:'".number_format($_amount_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_2 != "")
        	echo(",{v:".$_amount_2.",f:'".number_format($_amount_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_3 != "")
        	echo(",{v:".$_amount_3.",f:'".number_format($_amount_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_4 != "")
        	echo(",{v:".$_amount_4.",f:'".number_format($_amount_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_5 != "")
        	echo(",{v:".$_amount_5.",f:'".number_format($_amount_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_6 != "")
        	echo(",{v:".$_amount_6.",f:'".number_format($_amount_6)."'}");
        else
        	echo(",0");
        
        if ($_amount_7 != "")
        	echo(",{v:".$_amount_7.",f:'".number_format($_amount_7)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

        

		var data148_avg = new google.visualization.DataTable();
		
		data148_avg.addColumn('string', '날짜');
		data148_avg.addColumn('number', 'Day 1');
		data148_avg.addColumn('number', 'Day 2');
		data148_avg.addColumn('number', 'Day 3');
		data148_avg.addColumn('number', 'Day 4');
		data148_avg.addColumn('number', 'Day 5');
		data148_avg.addColumn('number', 'Day 6');
		data148_avg.addColumn('number', 'Day 7');
		data148_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {

        $_amount_avg_1 = ($active_list[$i-1]==0)?0:round($superfreespin_1_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_2 = ($active_list[$i-1]==0)?0:round($superfreespin_2_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_3 = ($active_list[$i-1]==0)?0:round($superfreespin_3_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_4 = ($active_list[$i-1]==0)?0:round($superfreespin_4_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_5 = ($active_list[$i-1]==0)?0:round($superfreespin_5_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_6 = ($active_list[$i-1]==0)?0:round($superfreespin_6_amount_list[$i-1] / $active_list[$i-1]);
        $_amount_avg_7 = ($active_list[$i-1]==0)?0: round($superfreespin_7_amount_list[$i-1] / $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($_amount_avg_1 != "")
        	echo(",{v:".$_amount_avg_1.",f:'".number_format($_amount_avg_1)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_2 != "")
        	echo(",{v:".$_amount_avg_2.",f:'".number_format($_amount_avg_2)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_3 != "")
        	echo(",{v:".$_amount_avg_3.",f:'".number_format($_amount_avg_3)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_4 != "")
        	echo(",{v:".$_amount_avg_4.",f:'".number_format($_amount_avg_4)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_5 != "")
        	echo(",{v:".$_amount_avg_5.",f:'".number_format($_amount_avg_5)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_6 != "")
        	echo(",{v:".$_amount_avg_6.",f:'".number_format($_amount_avg_6)."'}");
        else
        	echo(",0");
        
        if ($_amount_avg_7 != "")
        	echo(",{v:".$_amount_avg_7.",f:'".number_format($_amount_avg_7)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

	var data108 = new google.visualization.DataTable();
		
	data108.addColumn('string', '날짜');
	data108.addColumn('number', '지금금액');
	data108.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        
        $collection_event_middle_stage = $collection_event_middle_stage_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($collection_event_middle_stage != "")
            echo(",{v:".$collection_event_middle_stage.",f:'".number_format($collection_event_middle_stage)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

        

		var data108_avg = new google.visualization.DataTable();
		
		data108_avg.addColumn('string', '날짜');
		data108_avg.addColumn('number', '지금금액');
		data108_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $collection_event_middle_stage_avg = ($active_list[$i-1]==0)?0:round($collection_event_middle_stage_list[$i-1]/ $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($collection_event_middle_stage_avg != "")
            echo(",{v:".$collection_event_middle_stage_avg.",f:'".number_format($collection_event_middle_stage_avg)."'}]");
        else
        	echo(",0]");
     
        if ($i > 1)
        	echo(",");
    }
?>
        ]);
        
	var data109 = new google.visualization.DataTable();
		
	data109.addColumn('string', '날짜');
	data109.addColumn('number', '지금금액');
	data109.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        
        $collection_wheel = $collection_wheel_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($piggy_bank_bonus_freeamount != "")
            echo(",{v:".$collection_wheel.",f:'".number_format($collection_wheel)."'}]");
        else
        	echo(",0]");
        
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

        

		var data109_avg = new google.visualization.DataTable();
		
		data109_avg.addColumn('string', '날짜');
		data109_avg.addColumn('number', '지금금액');
		data109_avg.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $collection_wheel_avg = ($active_list[$i-1]==0)?0:round($collection_wheel_list[$i-1]/ $active_list[$i-1]);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."'");        
        
        if ($collection_wheel_avg != "")
            echo(",{v:".$collection_wheel_avg.",f:'".number_format($collection_wheel_avg)."'}]");
        else
        	echo(",0]");
     
        if ($i > 1)
        	echo(",");
    }
?>
        ]);

		var data111 = new google.visualization.DataTable();
		
		data111.addColumn('string', '날짜');
		data111.addColumn('number', '지금금액');
		data111.addRows([
	<?
	    for ($i=sizeof($date_list); $i>0; $i--)
	    {
	        
	        $collection_theme_reward = $collection_theme_reward_list[$i-1];
	        $_date = $date_list[$i-1];
	        
	        echo("['".$_date."'");        
	        
	        if ($collection_theme_reward != "")
	            echo(",{v:".$collection_theme_reward.",f:'".number_format($collection_theme_reward)."'}]");
	        else
	        	echo(",0]");
	        
	        if ($i > 1)
	        	echo(",");
	    }
	?>
	        ]);

			var data111_avg = new google.visualization.DataTable();
			
			data111_avg.addColumn('string', '날짜');
			data111_avg.addColumn('number', '지금금액');
			data111_avg.addRows([
	<?
	    for ($i=sizeof($date_list); $i>0; $i--)
	    {
	        $collection_theme_reward_avg = ($active_list[$i-1]==0)?0:round($collection_theme_reward_list[$i-1]/ $active_list[$i-1]);
	        $_date = $date_list[$i-1];
	        
	        echo("['".$_date."'");        
	        
	        if ($collection_theme_reward_avg != "")
	            echo(",{v:".$collection_theme_reward_avg.",f:'".number_format($collection_theme_reward_avg)."'}]");
	        else
	        	echo(",0]");
	     
	        if ($i > 1)
	        	echo(",");
	    }
	?>
	        ]);
	        
		var data112 = new google.visualization.DataTable();
		
		data112.addColumn('string', '날짜');
		data112.addColumn('number', '지금금액');
		data112.addRows([
	<?
	    for ($i=sizeof($date_list); $i>0; $i--)
	    {
	        
	        $collection_final_theme_reward = $collection_final_theme_reward_list[$i-1];
	        $_date = $date_list[$i-1];
	        
	        echo("['".$_date."'");        
	        
	        if ($collection_final_theme_reward != "")
	            echo(",{v:".$collection_final_theme_reward.",f:'".number_format($collection_final_theme_reward)."'}]");
	        else
	        	echo(",0]");
	        
	        if ($i > 1)
	        	echo(",");
	    }
	?>
	        ]);

			var data112_avg = new google.visualization.DataTable();
			
			data112_avg.addColumn('string', '날짜');
			data112_avg.addColumn('number', '지금금액');
			data112_avg.addRows([
	<?
	    for ($i=sizeof($date_list); $i>0; $i--)
	    {
	        $collection_final_theme_reward_avg = ($active_list[$i-1]==0)?0:round($collection_final_theme_reward_list[$i-1]/ $active_list[$i-1]);
	        $_date = $date_list[$i-1];
	        
	        echo("['".$_date."'");        
	        
	        if ($collection_final_theme_reward_avg != "")
	            echo(",{v:".$collection_final_theme_reward_avg.",f:'".number_format($collection_final_theme_reward_avg)."'}]");
	        else
	        	echo(",0]");
	     
	        if ($i > 1)
	        	echo(",");
	    }
	?>
	        ]);
            	
		var data155 = new google.visualization.DataTable();
		
		data155.addColumn('string', '날짜');
		data155.addColumn('number', '지금금액');
		data155.addRows([
	<?
	    for ($i=sizeof($date_list); $i>0; $i--)
	    {
	        
	        $$advertisement_reward = $advertisement_list[$i-1];
	        $_date = $date_list[$i-1];
	        
	        echo("['".$_date."'");        
	        
	        if ($$advertisement_reward != "")
	            echo(",{v:".$$advertisement_reward.",f:'".number_format($$advertisement_reward)."'}]");
	        else
	        	echo(",0]");
	        
	        if ($i > 1)
	        	echo(",");
	    }
	?>
	        ]);

			var data155_avg = new google.visualization.DataTable();
			
			data155_avg.addColumn('string', '날짜');
			data155_avg.addColumn('number', '지금금액');
			data155_avg.addRows([
	<?
	    for ($i=sizeof($date_list); $i>0; $i--)
	    {
	        $advertisement_reward_avg = ($active_list[$i-1]==0)?0:round($advertisement_list[$i-1]/ $active_list[$i-1]);
	        $_date = $date_list[$i-1];
	        
	        echo("['".$_date."'");        
	        
	        if ($advertisement_reward_avg != "")
	            echo(",{v:".$advertisement_reward_avg.",f:'".number_format($advertisement_reward_avg)."'}]");
	        else
	        	echo(",0]");
	     
	        if ($i > 1)
	        	echo(",");
	    }
	?>
	        ]);
            	
		var options = {
                title:'',                                                      
                width:1050,                         
                height:200,
                axisTitlesPosition:'in',
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize : 12,
                chartArea:{left:80,top:40,width:1020,height:130}
        };

		var chart = new google.visualization.LineChart(document.getElementById('chart_data0'));
        chart.draw(data0, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data0_avg'));
        chart.draw(data0_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data1'));
        chart.draw(data1, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data1_avg'));
        chart.draw(data1_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data2'));
        chart.draw(data2, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data2_avg'));
        chart.draw(data2_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data3'));
        chart.draw(data3, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data3_avg'));
        chart.draw(data3_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data4'));
        chart.draw(data4, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data4_avg'));
        chart.draw(data4_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data5'));
        chart.draw(data5, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data5_avg'));
        chart.draw(data5_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data6'));
        chart.draw(data6, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data6_avg'));
        chart.draw(data6_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data8'));
        chart.draw(data8, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data8_avg'));
        chart.draw(data8_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data9'));
        chart.draw(data9, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data9_avg'));
        chart.draw(data9_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data10'));
        chart.draw(data10, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data10_avg'));
        chart.draw(data10_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data11'));
        chart.draw(data11, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data11_avg'));
        chart.draw(data11_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data12'));
        chart.draw(data12, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data12_avg'));
        chart.draw(data12_avg, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data13'));
        chart.draw(data13, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data13_avg'));
        chart.draw(data13_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data14'));
        chart.draw(data14, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data14_avg'));
        chart.draw(data14_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data15'));
        chart.draw(data15, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data15_avg'));
        chart.draw(data15_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data16'));
        chart.draw(data16, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data16_avg'));
        chart.draw(data16_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data17'));
        chart.draw(data17, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data17_avg'));
        chart.draw(data17_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data18'));
        chart.draw(data18, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data18_avg'));
        chart.draw(data18_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data19'));
        chart.draw(data19, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data19_avg'));
        chart.draw(data19_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data21'));
        chart.draw(data21, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data21_avg'));
        chart.draw(data21_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data22'));
        chart.draw(data22, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data22_avg'));
        chart.draw(data22_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data23'));
        chart.draw(data23, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data23_avg'));
        chart.draw(data23_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data24'));
        chart.draw(data24, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data24_avg'));
        chart.draw(data24_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data49'));
        chart.draw(data49, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data49_avg'));
        chart.draw(data49_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data50'));
        chart.draw(data50, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data50_avg'));
        chart.draw(data50_avg, options);

//         chart = new google.visualization.LineChart(document.getElementById('chart_data51'));
//         chart.draw(data51, options);

//         chart = new google.visualization.LineChart(document.getElementById('chart_data51_avg'));
//         chart.draw(data51_avg, options);

//         chart = new google.visualization.LineChart(document.getElementById('chart_data61'));
//         chart.draw(data61, options);

//         chart = new google.visualization.LineChart(document.getElementById('chart_data61_avg'));
//         chart.draw(data61_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data100'));
        chart.draw(data100, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data100_avg'));
        chart.draw(data100_avg, options);       

//         chart = new google.visualization.LineChart(document.getElementById('chart_data101'));
//         chart.draw(data101, options);

//         chart = new google.visualization.LineChart(document.getElementById('chart_data101_avg'));
//         chart.draw(data101_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data127'));
        chart.draw(data127, options); 

        chart = new google.visualization.LineChart(document.getElementById('chart_data127_avg'));
        chart.draw(data127_avg, options);  

        chart = new google.visualization.LineChart(document.getElementById('chart_data500'));
        chart.draw(data500, options); 

        chart = new google.visualization.LineChart(document.getElementById('chart_data500_avg'));
        chart.draw(data500_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data100_701'));
        chart.draw(data100_701, options); 

        chart = new google.visualization.LineChart(document.getElementById('chart_data100_701_avg'));
        chart.draw(data100_701_avg, options);
              
        chart = new google.visualization.LineChart(document.getElementById('chart_data100_801'));
        chart.draw(data100_801, options); 

        chart = new google.visualization.LineChart(document.getElementById('chart_data100_801_avg'));
        chart.draw(data100_801_avg, options);
              
        chart = new google.visualization.LineChart(document.getElementById('chart_data96'));
        chart.draw(data96, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data96_avg'));
        chart.draw(data96_avg, options);
              
        chart = new google.visualization.LineChart(document.getElementById('chart_data97'));
        chart.draw(data97, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data97_avg'));
        chart.draw(data97_avg, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data98'));
        chart.draw(data98, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data98_avg'));
        chart.draw(data98_avg, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data99'));
        chart.draw(data99, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data99_avg'));
        chart.draw(data99_avg, options);
	
		chart = new google.visualization.LineChart(document.getElementById('chart_data100_901'));
        chart.draw(data100_901, options); 

        chart = new google.visualization.LineChart(document.getElementById('chart_data100_901_avg'));
        chart.draw(data100_901_avg, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data68'));
        chart.draw(data68, options); 

        chart = new google.visualization.LineChart(document.getElementById('chart_data68_avg'));
        chart.draw(data68_avg, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data75'));
        chart.draw(data75, options); 

        chart = new google.visualization.LineChart(document.getElementById('chart_data75_avg'));
        chart.draw(data75_avg, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data82'));
        chart.draw(data82, options); 

        chart = new google.visualization.LineChart(document.getElementById('chart_data82_avg'));
        chart.draw(data82_avg, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data113'));
        chart.draw(data113, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data113_avg'));
        chart.draw(data113_avg, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data200'));
        chart.draw(data200, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data200_avg'));
        chart.draw(data200_avg, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data142'));
        chart.draw(data142, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data142_avg'));
        chart.draw(data142_avg, options);
              
        chart = new google.visualization.LineChart(document.getElementById('chart_data145'));
        chart.draw(data145, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data145_avg'));
        chart.draw(data145_avg, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data146'));
        chart.draw(data146, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data146_avg'));
        chart.draw(data146_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data148'));
        chart.draw(data148, options); 

        chart = new google.visualization.LineChart(document.getElementById('chart_data148_avg'));
        chart.draw(data148_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data108'));
        chart.draw(data108, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data108_avg'));
        chart.draw(data108_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data109'));
        chart.draw(data109, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data109_avg'));
        chart.draw(data109_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data111'));
        chart.draw(data111, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data111_avg'));
        chart.draw(data111_avg, options);

        chart = new google.visualization.LineChart(document.getElementById('chart_data112'));
        chart.draw(data112, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data112_avg'));
        chart.draw(data112_avg, options);      
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data155'));
        chart.draw(data155, options);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_data155_avg'));
        chart.draw(data155_avg, options);      
	}

	google.setOnLoadCallback(drawChart);

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	
	    search_form.submit();
	}
	
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

	function fnFreeCoinView(type)
	{
	    if(type == 0)
	    {
	    	$("div[name='div_normal']").show();
	    	$("div[name='div_avg']").show();
	    }
	    else if(type == 1)
	    {
	    	$("div[name='div_normal']").show();
	    	$("div[name='div_avg']").hide();
	    } 
	    else if(type == 2)
	    {
	    	$("div[name='div_normal']").hide();
	    	$("div[name='div_avg']").show();
	    }    
	}

	function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		var total = document.getElementById("type_total");
		var type_web_v2 = document.getElementById("type_web_v2");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
			type_web_v2.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
			type_web_v2.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
			type_web_v2.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
			total.className="btn_schedule";
			type_web_v2.className="btn_schedule";
		}
		else if (type == "4")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule_select";
			type_web_v2.className="btn_schedule";
		}
		else if (type == "5")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
			type_web_v2.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
		<input type="button" class="<?= ($os_type == "4") ? "btn_schedule_select" : "btn_schedule" ?>" value="Total" id="type_total" onclick="change_os_type('4')"    />
		<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
		<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
		<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
		<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
		<input type="button" class="<?= ($os_type == "5") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web_2.0" id="type_web_v2" onclick="change_os_type('5')"    />
	</span>
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 무료 자원 (<?=$os_txt?>/Graph)</div>
		<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />
			<div class="search_box">
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다.
		<div style="float:right;">그래프 Display
			<input type="button" class="btn_search" value="전체" onclick="fnFreeCoinView(0);" />
            <input type="button" class="btn_search" value="일반" onclick="fnFreeCoinView(1);" />
            <input type="button" class="btn_search" value="평균" onclick="fnFreeCoinView(2);" />
		</div>
	</div>
	
	<div class="h2_title" name="div_normal">[Total]</div>
	<div id="chart_data0" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Total/DAU]</div>
	<div id="chart_data0_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>	
	
	<div class="h2_title" name="div_normal">[Advertisement]</div>
	<div id="chart_data155" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Advertisement/DAU]</div>
	<div id="chart_data155_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[SuperFreeSpin]</div>
	<div id="chart_data148" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[SuperFreeSpin/DAU]</div>
	<div id="chart_data148_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Collection - 중간 스테이지 완료 보상]</div>
	<div id="chart_data108" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Collection - 중간 스테이지 완료 보상/DAU]</div>
	<div id="chart_data108_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Collection - 이벤트 휠 금액]</div>
	<div id="chart_data109" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Collection - 이벤트 휠 금액/DAU]</div>
	<div id="chart_data109_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Collection - 테마 완료 보상]</div>
	<div id="chart_data111" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Collection - 테마 완료 보상/DAU]</div>
	<div id="chart_data111_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Collection - 테마 최종 완료 보상]</div>
	<div id="chart_data112" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Collection - 테마 최종 완료 보상/DAU]</div>
	<div id="chart_data112_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Single Play LevelUP]</div>
	<div id="chart_data146" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Single Play LevelUP/DAU]</div>
	<div id="chart_data146_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[DailyFreeSpin]</div>
	<div id="chart_data68" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[DailyFreeSpin/DAU]</div>
	<div id="chart_data68_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[DailyFreeSpin 2X]</div>
	<div id="chart_data75" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[DailyFreeSpin 2X/DAU]</div>
	<div id="chart_data75_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[DailyFreeSpin 4X]</div>
	<div id="chart_data82" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[DailyFreeSpin 4X/DAU]</div>
	<div id="chart_data82_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Lucky Wheel]</div>
	<div id="chart_data1" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Lucky Wheel/DAU]</div>
	<div id="chart_data1_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Lucky Wheel X 2]</div>
	<div id="chart_data50" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Lucky Wheel X 2/DAU]</div>
	<div id="chart_data50_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Lucky Wheel X 4]</div>
	<div id="chart_data49" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Lucky Wheel X 4/DAU]</div>
	<div id="chart_data49_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[TY Bonus]</div>
	<div id="chart_data2" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[TY Bonus/DAU]</div>
	<div id="chart_data2_avg"name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Time Bonus]</div>
	<div id="chart_data3" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Time Bonus/DAU]</div>
	<div id="chart_data3_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[VIP Lv UP]</div>
	<div id="chart_data4" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[VIP Lv UP/DAU]</div>
	<div id="chart_data4_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Treat Ticket]</div>
	<div id="chart_data5" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Treat Ticket/DAU]</div>
	<div id="chart_data5_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Celeb Lv UP]</div>
	<div id="chart_data6" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Celeb Lv UP/avg]</div>
	<div id="chart_data6_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Share Coin]</div>
	<div id="chart_data8" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Share Coin/DAU]</div>
	<div id="chart_data8_avg"name="div_avg" style="display:none;height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Bankrupt]</div>
	<div id="chart_data9" name="div_normal" style="height:230px; min-width: 500px"></div>	
	
	<div class="h2_title"name="div_avg" style="display:none;">[Bankrupt/DAU]</div>
	<div id="chart_data9_avg"name="div_avg" style="display:none;height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[VIP Bankrupt]</div>
	<div id="chart_data10" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title"name="div_avg" style="display:none;">[VIP Bankrupt/DAU]</div>
	<div id="chart_data10_avg"name="div_avg" style="display:none;height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[FanPage FreeCoin]</div>
	<div id="chart_data11" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[FanPage FreeCoin/DAU]</div>
	<div id="chart_data11_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[FanPage TY]</div>
	<div id="chart_data12" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[FanPage TY]/DAU</div>
	<div id="chart_data12_avg" name="div_avg" style="display:none;height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Special Bankrupt]</div>
	<div id="chart_data13" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Special Bankrupt/DAU]</div>
	<div id="chart_data13_avg" name="div_avg" style="display:none;height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Welcomeback Bonus]</div>
	<div id="chart_data14" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Welcomeback Bonus/DAU]</div>
	<div id="chart_data14_avg" name="div_avg" style="display:none;height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Giveaway]</div>
	<div id="chart_data15" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Giveaway/DAU]</div>
	<div id="chart_data15_avg" name="div_avg" style="display:none;height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Fame Tourney]</div>
	<div id="chart_data16" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Fame Tourney/DAU]</div>
	<div id="chart_data16_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Tutorial Treat Ticket]</div>
	<div id="chart_data17" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Tutorial Treat Ticket/DAU]</div>
	<div id="chart_data17_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Fame Weekly Reward]</div>
	<div id="chart_data18" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Fame Weekly Reward/DAU]</div>
	<div id="chart_data18_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Fame Monthly Reward]</div>
	<div id="chart_data19" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Fame Monthly Reward/DAU]</div>
	<div id="chart_data19_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Invite Challenge Reward]</div>
	<div id="chart_data21" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Invite Challenge Reward/DAU]</div>
	<div id="chart_data21_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Platinum Wheel Reward]</div>
	<div id="chart_data22" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Platinum Wheel Reward/DAU]</div>
	<div id="chart_data22_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Piggy Pot Bonus]</div>
	<div id="chart_data23" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Piggy Pot Bonus/DAU]</div>
	<div id="chart_data23_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[FanPage FreeSpin Bonus]</div>
	<div id="chart_data24" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[FanPage FreeSpin Bonus/DAU]</div>
	<div id="chart_data24_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>

	<div class="h2_title" name="div_normal">[Inbox]</div>
	<div id="chart_data100" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Inbox/DAU]</div>
	<div id="chart_data100_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<!-- div class="h2_title" name="div_normal">[DailyStamp X 2]</div>
	<div id="chart_data51" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[DailyStamp X 2/DAU]</div>
	<div id="chart_data51_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[DailyStamp X 4]</div>
	<div id="chart_data61" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[DailyStamp X 4/DAU]</div>
	<div id="chart_data61_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[DailyStamp]</div>
	<div id="chart_data101" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[DailyStamp/DAU]</div>
	<div id="chart_data101_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div-->
	
	<div class="h2_title" name="div_normal">[Push on]</div>
	<div id="chart_data127" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Push on/DAU]</div>
	<div id="chart_data127_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Wake Up]</div>
	<div id="chart_data500" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Wake Up/DAU]</div>
	<div id="chart_data500_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[VIP Daily Bonus]</div>
	<div id="chart_data100_701" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[VIP Daily Bonus]</div>
	<div id="chart_data100_701_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Mobile Popup Reward]</div>
	<div id="chart_data100_801" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Mobile Popup Reward]</div>
	<div id="chart_data100_801_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[펜페이지 스크래치]</div>
	<div id="chart_data96" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[펜페이지 스크래치/DAU]</div>
	<div id="chart_data96_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[펜페이지 슬롯 오브 포춘]</div>
	<div id="chart_data97" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[펜페이지 슬롯 오브 포춘/DAU]</div>
	<div id="chart_data97_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[펜페이지 황금알]</div>
	<div id="chart_data98" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[펜페이지 황금알/DAU]</div>
	<div id="chart_data98_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[YouTube 리텐션]</div>
	<div id="chart_data99" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[YouTube 리텐션/DAU]</div>
	<div id="chart_data99_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
		<div class="h2_title" name="div_normal">[Surprise Bonus]</div>
	<div id="chart_data100_901" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Surprise Bonus/DAU]</div>
	<div id="chart_data100_901_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[기간제 오퍼 Wheel]</div>
	<div id="chart_data113" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[기간제 오퍼 Wheel/DAU]</div>
	<div id="chart_data113_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Meta Event]</div>
	<div id="chart_data200" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Meta Event/DAU]</div>
	<div id="chart_data200_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Premium Booster]</div>
	<div id="chart_data142" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Premium Booster/DAU]</div>
	<div id="chart_data142_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_normal">[Special Daily Wheel]</div>
	<div id="chart_data145" name="div_normal" style="height:230px; min-width: 500px"></div>
	
	<div class="h2_title" name="div_avg" style="display:none;">[Special Daily Wheel/DAU]</div>
	<div id="chart_data145_avg" name="div_avg" style="display:none; height:230px; min-width: 500px"></div>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>