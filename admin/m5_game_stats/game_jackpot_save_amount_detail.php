<?
    $top_menu = "game_stats";
    $sub_menu = "game_jackpot_save_amount_detail";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $tab = ($_GET["tab"] == "") ? "0" :$_GET["tab"];
    $pagename = "game_jackpot_save_amount_detail.php";
    
    if ($tab == "")
        error_back("잘못된 접근입니다.");        
    
    $db_main2 = new CDatabase_Main2();
    $db_game = new CDatabase_Game();
    
	$sql = "SELECT slottype, slotname FROM tbl_slot_list";
	$slotlist = $db_main2->gettotallist($sql);
	
	$slostartlist = array(array("slottype" => "0", "slotname" => "ALL"));
	$slotlist = array_merge($slostartlist, $slotlist);

	if($tab == "0")
	{
		
		$sql = "SELECT slottype, objectname, MAX(jackpot) AS max_jackpot, ROUND(SUM(jackpot)/COUNT(*), 0) AS avg_jackpot ".
				"FROM tbl_slot_object WHERE slottype NOT IN (2,13,16,18,20,22,28,31,34) GROUP BY slottype ".
				"UNION ALL ".
				"SELECT slottype, objectname, MAX(jackpot) AS max_jackpot, ROUND(SUM(jackpot)/COUNT(*), 0) AS avg_jackpot ".
				"FROM ( ".
				"	SELECT t1.slottype, t1.objectname, t2.jackpot1 AS jackpot ".
				"	FROM ( ".
				"		SELECT objectidx, slottype, objectname, minbet FROM tbl_slot_object WHERE slottype IN (2,13,16,18,20,22,28,31,34) ".
				"	) t1 LEFT JOIN tbl_multi_jackpot t2 ON t1.objectidx = t2.objectidx ".
				") t3 ".
				"GROUP BY slottype ".
				"ORDER BY slottype ASC";
		
		$jackpot_money_list = $db_game->gettotallist($sql);
	}
	else
	{
		if($tab == "2" || $tab == "13" || $tab == "16" || $tab == "18" || $tab == "20" || $tab == "22" || $tab == "28" || $tab == "31" || $tab == "34")
		{
			$sql = "SELECT slottype, objectname, MAX(jackpot) AS max_jackpot, ROUND(SUM(jackpot)/COUNT(*), 0) AS avg_jackpot ".
					"FROM ( ".
					"	SELECT t1.slottype, t1.objectname, t2.jackpot1 AS jackpot ".
					"	FROM ( ".
					"		SELECT objectidx, slottype, objectname, minbet FROM tbl_slot_object WHERE slottype = $tab ".
					"	) t1 LEFT JOIN tbl_multi_jackpot t2 ON t1.objectidx = t2.objectidx ".
					") t3 ".
					"GROUP BY slottype";
			
			$jackpot_money_list = $db_game->gettotallist($sql);
		}
		else
		{
			$sql = "SELECT slottype, objectname, MAX(jackpot) AS max_jackpot, ROUND(SUM(jackpot)/COUNT(*), 0) AS avg_jackpot ".
					"FROM tbl_slot_object WHERE slottype = $tab GROUP BY slottype";
			
			$jackpot_money_list = $db_game->gettotallist($sql);
		}
	}
	
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    
    function tab_change(tab)
    {
        var search_form = document.search_form;
        search_form.tab.value = tab;
        search_form.submit();
    }
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
                <!-- title_warp -->
                <div class="title_wrap">
                    <div class="title"><?= $top_menu_txt ?> &gt; 잭팟상세현황 - 적립급 현황(일반)</div>
                    <div class="search_box">
                    	<input type="hidden" name="tab" id="tab" value="<?= $tab ?>" />
                    </div>
                </div>
                <!-- //title_warp -->            
                
                <ul class="tab">
<?
	for ($i=0; $i<sizeof($slotlist); $i++)
	{
		$slottype1 = $slotlist[$i]["slottype"];
		$slotname1 = str_replace(" Slot", "", $slotlist[$i]["slotname"]);
?>	
                <li id="tab_<?= $slottype1 ?>" class="<?= ($tab == $slottype1) ? "select" : "" ?>" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('<?= $slottype1 ?>')"><?= $slotname1 ?></li>
<?
	}
?>                
                </ul>
                <div class="h2_title pt20"></div>
                <table class="tbl_list_basic1">
                <colgroup>
                    <col width="">                    
                    <col width="">
                    <col width="">                    
                </colgroup>
                <thead>
                    <tr>
						<th class="tdc">Slot Name</th>						
                        <th class="tdc">Max Jackpot</th>
                        <th class="tdc">AVG Jackpot</th>
                    </tr>
                </thead>
                <tbody>
<?
    $slottype = "";
    
    for ($i=0; $i<sizeof($jackpot_money_list); $i++)
    {
    	$slottype = $jackpot_money_list[$i]["slottype"];        
        $max_jackpot = $jackpot_money_list[$i]["max_jackpot"];
        $avg_jackpot = $jackpot_money_list[$i]["avg_jackpot"];
        
        for($j=0; $j<sizeof($slotlist); $j++)
        {
        	if($slotlist[$j]["slottype"] == $slottype)
       	 	{
        		$slotname = $slotlist[$j]["slotname"];
        		break;
        	}
			else
        	{
        		$slotname = "Unkown";
        	}
        }		
?>
					<tr>
                        <td class="tdc point"><?= $slotname ?></td>
                        <td class="tdc point"><?= make_price_format($max_jackpot) ?></td>
                        <td class="tdc point"><?= make_price_format($avg_jackpot) ?></td>
                    </tr>
<?
    }
?>
				</tbody>
                </table>
            </form>
        </div>
        <!--  //CONTENTS WRAP -->
<?
    $db_main2->end();
    $db_game->end();
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>