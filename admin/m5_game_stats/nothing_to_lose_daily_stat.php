<?
    $top_menu = "game_stats";
    $sub_menu = "nothing_to_lose_daily_stat";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $today = date("Y-m-d");
    
    $search_event_idx = ($_GET["event_idx"] == "") ? "ALL" : $_GET["event_idx"];
    $search_startdate = $_GET["startdate"];
    $search_enddate = $_GET["enddate"];

    
    if($search_startdate == "")
        $search_startdate = date("Y-m-d", strtotime("-3 day"));
        
    if($search_enddate == "")
        $search_enddate = $today;
        
    $tail = "";
    if($search_event_idx != "ALL")
        $tail = "AND event_idx = $search_event_idx";
    
    $db_main2 = new CDatabase_Main2();
    
    $event_idx_arr = $db_main2->gettotallist("SELECT event_idx FROM tbl_meta_event_setting ORDER BY event_idx ASC");
    
    $sql = " SELECT * FROM tbl_nothingtolose_stat
             WHERE  today BETWEEN '$search_startdate' AND '$search_enddate' ORDER BY today  DESC";
    $data_list = $db_main2->gettotallist($sql);
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#startdate").datepicker({ });
	});
	
	$(function() {
		$("#enddate").datepicker({ });
	});
	
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.startdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    } 
	
	    if (search_form.enddate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    } 
	
	    search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap" style="height:76px;">
		<div class="title"><?= $top_menu_txt ?> &gt; <?=$platform_name?> Nothing to Lose 통계</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
						</select>&nbsp;&nbsp;&nbsp;
				<span class="search_lbl ml20">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $search_startdate ?>" maxlength="10" style="width:65px"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" /> ~
				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $search_enddate ?>" style="width:65px" maxlength="10"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	<div class="search_result"><?= $os_txt?></div>
			<div class="search_result">
        		<span><?= $search_startdate ?> ~ <?= $search_enddate ?></span> 통계입니다
        	</div>
    		<table class="tbl_list_basic1">
    		<colgroup>
    			<col width="70">
    			<col width="120">
                <col width="120">
                <col width="120">
    			<col width="70">
    			<col width="120">
                <col width="120">
                <col width="120">
    		</colgroup>
            <thead>
                <tr>
                 	<th>날짜</th>
        			<th>대상자 수</th>
        			<th>구매 유저 수</th>
        			<th>참여 유저 수</th>
        			<th>캐쉬백 금액</th>
        			<th>획득 유저 수</th>
        			<th>지급 금액</th>
        			<th>획득 률</th>
        		</tr>
            </thead>
            <tbody>
	
<?
    
    for($i=0; $i<sizeof($data_list); $i++)
    {
        $today = $data_list[$i]["today"];
        $target_user = $data_list[$i]["target_user"];
        $payuser_count = $data_list[$i]["payuser_count"];
        $end3hour_user = $data_list[$i]["end3hour_user"];
        $saveamount = $data_list[$i]["saveamount"];
        $take_user_count = $data_list[$i]["take_user_count"];
        $give_saveamount = $data_list[$i]["give_saveamount"];
        $give_rate = ROUND($take_user_count/$end3hour_user*100,2)
?>
			<td class="tdc point_title" valign="center"><?= $today ?></td>
            <td class="tdc"><?= number_format($target_user) ?></td>
            <td class="tdc"><?= number_format($payuser_count) ?></td>
            <td class="tdc"><?= number_format($end3hour_user) ?></td>
            <td class="tdc"><?= number_format($saveamount) ?></td>
            <td class="tdc"><?= number_format($take_user_count) ?></td>
            <td class="tdc"><?= number_format($give_saveamount) ?></td>
            <td class="tdc"><?= number_format($give_rate)."%" ?></td>
       </tr>
            
<?
	}
	if(sizeof($data_list) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="7">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
            </tbody>
    	</table>
</div>
<!--  //CONTENTS WRAP -->
<?
    $db_main2->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>