<?
    $top_menu = "customer";
    $sub_menu = "mobile_ios_category";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $categoryidx = $_GET["categoryidx"];
    
    check_number($categoryidx);
    
    $db_analysis = new CDatabase_Analysis();
    
    if ($categoryidx != "")
    {       
        $sql = "SELECT categoryidx,category FROM support_mobile_category WHERE categoryidx=$categoryidx";
        $data = $db_analysis->getarray($sql);
        
        $categoryidx = $data["categoryidx"];
        $category = $data["category"];
        
        if ($categoryidx == "")
            error_back("존재하지 않는 QA 분류입니다.");
		
		$sql = "SELECT categoryidx,seqno,category FROM support_mobile_category ORDER BY seqno";
	    $categorylist = $db_analysis->gettotallist($sql);
    }
    
    $db_analysis->end();
?>
<script>
    function save_mobile_ios_category()
    {
        var input_form = document.input_form;
        
        if (input_form.category.value == "")
        {
            alert("분류명을 입력하세요.");
            input_form.category.focus();
            return;
        }
        
        var param = {};
        param.categoryidx = "<?= $categoryidx ?>";
        param.category = input_form.category.value;

        if ("<?= $categoryidx ?>" == "")
        	WG_ajax_execute("customer/save_mobile_ios_category", param, save_mobile_ios_category_callback);
        else
        	WG_ajax_execute("customer/update_mobile_ios_category", param, save_mobile_ios_category_callback);
    }

    function save_mobile_ios_category_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            window.location.href = "mobile_ios_category.php";
        }
    }

    function delete_mobile_ios_category()
    {
        if (!confirm("QA 분류를 삭제 하시겠습니까?"))
            return;

        var param = {};
        param.categoryidx = "<?= $categoryidx ?>";

        WG_ajax_execute("customer/delete_mobile_ios_category", param, delete_mobile_ios_category_callback);
    }

    function delete_mobile_ios_category_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.href = "mobile_ios_category.php";
        }
    }
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; IOS QA 분류 상세</div>
<?
    if ($categoryidx != "")
    { 
?>
            <div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_mobile_ios_category()"></div>
<?
    } 
?>
            </div>
            <!-- //title_warp -->

            <form name="input_form" id="input_form" onsubmit="return false">
            <table class="tbl_view_basic">
            <colgroup>
                <col width="150">
                <col width="">
            </colgroup>
            <tbody>
                <tr>
                    <th><span>*</span> 분류명</th>
                    <td><input type="text" class="view_tbl_text" name="category" id="category" value="<?= encode_html_attribute($category) ?>" maxlength="1024"></td>
                </tr>
            </tbody>
            </table>    
            </form> 
            
            <div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="저장" onclick="save_mobile_ios_category()">
                <input type="button" class="btn_setting_02" value="취소" onclick="go_page('mobile_ios_category.php')">           
            </div>
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>