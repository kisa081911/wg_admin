<?
    $top_menu = "customer";
    $sub_menu = "qa_amazon_special";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_name = trim($_GET["name"]);
    $search_useridx = trim($_GET["useridx"]);
    $search_facebookid = trim($_GET["facebookid"]);
    $search_status = $_GET["status"];
    $search_start_writedate = $_GET["start_writedate"];
    $search_end_writedate = $_GET["end_writedate"];
    $search_start_responsedate = $_GET["start_responsedate"];
    $search_end_responsedate = $_GET["end_responsedate"];
    $search_start_credit = trim($_GET["start_credit"]);
    $search_end_credit = trim($_GET["end_credit"]);
	$search_check = $_GET["searchcheck"];
    $search_contents = trim($_GET["contents"]);
    $search_ispublic = $_GET["ispublic"];
    $search_ispending = $_GET["ispending"];
    $search_usereporting = $_GET["usereporting"];
    $search_categoryidx = $_GET["categoryidx"];
    $search_noreplycount = $_GET["noreplycount"];
    $search_adminid = $_GET["adminid"];
    $search_answer = $_GET["answer"];
    $search_level = $_GET["level"];
    		
	if ($search_check != "1")
		$search_status = "0";
	
    $listcount = 10;
    $pagename = "qa_android_special.php";
    $pagefield = "answer=".urlencode($search_answer)."&level=$search_level&adminid=$search_adminid&noreplycount=$search_noreplycount&searchcheck=$search_check&categoryidx=$search_categoryidx&name=$search_name&useridx=$search_useridx&facebookid=$search_facebookid&status=$search_status&start_writedate=$search_start_writedate&end_writedate=$search_end_writedate&start_responsedate=$search_start_responsedate&end_responsedate=$search_end_responsedate&start_credit=$search_start_credit&end_credit=$search_end_credit&ispublic=$search_ispublic&ispending=$search_ispending&usereporting=$search_usereporting&contents=".urlencode($search_contents);
    
    $db_analysis = new CDatabase_Analysis();
    
    $tail = "WHERE 1=1 AND isspecial = 1 ";
    
    if ($search_name != "")
        $tail .= " AND name LIKE '%$search_name%'";
    
    if ($search_contents != "")
        $tail .= " AND contents LIKE '%$search_contents%'";
    
    if ($search_useridx != "")
        $tail .= " AND useridx='$search_useridx'";
    
    if ($search_facebookid != "")
        $tail .= " AND facebookid LIKE '%$search_facebookid%'";
    
    if ($search_start_writedate != "")
        $tail .= " AND writedate >= '$search_start_writedate 00:00:00'";
    
    if ($search_end_writedate != "")
        $tail .= " AND writedate <= '$search_end_writedate 23:59:59'";
    
    if ($search_start_responsedate != "")
        $tail .= " AND responsedate >= '$search_start_responsedate 00:00:00'";
    
    if ($search_end_responsedate != "")
        $tail .= " AND responsedate <= '$search_end_responsedate 23:59:59'";
    
    if ($search_status != "")
        $tail .= " AND status=$search_status ";
    
    if ($search_ispublic != "")
        $tail .= " AND ispublic=$search_ispublic ";
    
    if ($search_ispending != "")
        $tail .= " AND ispending=$search_ispending ";
    
    if ($search_usereporting != "")
        $tail .= " AND usereporting=$search_usereporting ";
            
    if ($search_start_credit != "")
        $tail .= " AND IFNULL((SELECT summary_credit FROM user_order WHERE useridx=support_amazon_qa.useridx),0)>=$search_start_credit ";
    
    if ($search_end_credit != "")
        $tail .= " AND IFNULL((SELECT summary_credit FROM user_order WHERE useridx=support_amazon_qa.useridx),0)<=$search_end_credit ";
    
    if ($search_categoryidx != "")
        $tail .= " AND categoryidx=$search_categoryidx ";
    
    if ($search_noreplycount != "")
        $tail .= " AND (SELECT COUNT(*) FROM support_amazon_qa A WHERE status=0 AND useridx=support_amazon_qa.useridx)>=$search_noreplycount ";
    
    if ($search_adminid != "")
        $tail .= " AND EXISTS (SELECT * FROM support_amazon_qa_answer WHERE qaidx=support_amazon_qa.qaidx AND adminid LIKE '%$search_adminid%') ";
    
    if ($search_level != "")
        $tail .= " AND EXISTS (SELECT * FROM support_amazon_qa_answer WHERE qaidx=support_amazon_qa.qaidx AND level=$search_level) ";
    
    if ($search_answer != "")
        $tail .= " AND EXISTS (SELECT * FROM support_amazon_qa_answer WHERE qaidx=support_amazon_qa.qaidx AND answer LIKE '%$search_answer%') ";
		
    $totalcount = $db_analysis->getvalue("SELECT COUNT(*) FROM support_amazon_qa $tail");
    
    $sql = "SELECT qaidx,contents,status,writedate,name,facebookid,useridx,ispublic,responsedate,".
    	"(SELECT IFNULL(MAX(level),0) FROM support_amazon_qa_answer WHERE qaidx=support_amazon_qa.qaidx) AS level,".
    	"(SELECT category FROM support_amazon_category WHERE categoryidx=support_amazon_qa.categoryidx) AS category ".
    	" FROM support_amazon_qa $tail ORDER BY qaidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    $qalist = $db_analysis->gettotallist($sql);
    
	$sql = "SELECT categoryidx,category FROM support_amazon_category ORDER BY categoryidx ASC";
		
	$categorylist = $db_analysis->gettotallist($sql);
	
	$db_analysis->end();   
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
        
    function get_level($level)
    {
    	if ($level == '1')
			return "중급";
		else if ($level == '2')
		 	return "고급";
		else
			return "초급";
    }
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>  
<style>
	div td {word-break:break-all}
</style>
<script>
    function qa_amazon_write(qaidx)
    {
        window.location.href = "qa_amazon_special_write.php?pagefield=<?= urlencode($pagefield."&page=".$page) ?>&qaidx="+qaidx;
    }
    
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
        
    $(function() {
        $("#start_writedate").datepicker({ });
    });
    
    $(function() {
        $("#end_writedate").datepicker({ });
    });

    $(function() {
        $("#start_responsedate").datepicker({ });
    });
    
    $(function() {
        $("#end_responsedate").datepicker({ });
    });    
</script>       
        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; Amazon QA 관리  - 특별 대응<span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
            </div>
            <!-- //title_warp -->
            
            <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="qa_amazon_special.php">
                <div class="detail_search_wrap">
                	<input type=hidden name="searchcheck" value="1">
                    <span class="search_lbl ml20">이름</span>
                    <input type="text" class="search_text" id="name" name="name" style="width:100px" value="<?= encode_html_attribute($search_name) ?>" onkeypress="search_press(event)" />
                    
                    <span class="search_lbl ml20">Facebook ID</span>
                    <input type="text" class="search_text" id="facebookid" name="facebookid" style="width:100px" value="<?= encode_html_attribute($search_facebookid) ?>" onkeypress="search_press(event); return checkonlynum();" />
                    
                    <span class="search_lbl ml20">UserIdx&nbsp;</span>
                    <input type="text" class="search_text" id="useridx" name="useridx" style="width:100px" value="<?= encode_html_attribute($search_useridx) ?>" onkeypress="search_press(event)" />
                    
                    <span class="search_lbl ml20">답변</span>
                    <input type="radio" name="status" value="" <?= ($search_status == "") ? "checked" : "" ?>>전체 <input type="radio" name="status" value="1" <?= ($search_status == "1") ? "checked" : "" ?>>답변 <input type="radio" name="status" value="0" <?= ($search_status == "0") ? "checked" : "" ?>>미답변 
                    
                    <span class="search_lbl ml20">공개</span>
                    <input type="radio" name="ispublic" value="" <?= ($search_ispublic == "") ? "checked" : "" ?>>전체 <input type="radio" name="ispublic" value="1" <?= ($search_ispublic == "1") ? "checked" : "" ?>>공개 <input type="radio" name="ispublic" value="0" <?= ($search_ispublic == "0") ? "checked" : "" ?>>미공개
                    <br/><br/>  
                    
                    <span class="search_lbl">작성일&nbsp;&nbsp;</span>
                    <input type="text" class="search_text" id="start_writedate" name="start_writedate" style="width:65px" value="<?= $search_start_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
                    <input type="text" class="search_text" id="end_writedate" name="end_writedate" style="width:65px" value="<?= $search_end_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
                    <span class="search_lbl ml20">답변일</span>
                    <input type="text" class="search_text" id="start_responsedate" name="start_responsedate" style="width:65px" value="<?= $search_start_responsedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
                    <input type="text" class="search_text" id="end_responsedate" name="end_responsedate" style="width:65px" value="<?= $search_end_responsedate?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />

                    <span class="search_lbl ml20">결제액(credit)</span>
                    <input type="text" class="search_text" id="start_credit" name="start_credit" style="width:60px" value="<?= $search_start_credit ?>" onkeypress="search_press(event); return checknum();" /> ~
                    <input type="text" class="search_text" id="end_credit" name="end_credit" style="width:60px" value="<?= $search_end_credit ?>" onkeypress="search_press(event); return checknum();" />

                    <span class="search_lbl ml20">응답자ID</span>
                    <input type="text" class="search_text" id="adminid" name="adminid" style="width:100px" value="<?= encode_html_attribute($search_adminid) ?>" onkeypress="search_press(event);" />

                    <div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div><br/><br/>

                    <span class="search_lbl">Pending</span>
                    <input type="checkbox" name="ispending" value="1" <?= ($search_ispending == "1") ? "checked" : "" ?>>Pending 여부
                    
                    <span class="search_lbl ml20">리포팅</span>
                    <input type="checkbox" name="usereporting" value="1" <?= ($search_usereporting == "1") ? "checked" : "" ?>>리포팅 여부 

                    <span class="search_lbl ml20">분류</span>
                    <select name="categoryidx">
                    	<option value="">분류선택</option>
<?
	for ($i=0; $i<sizeof($categorylist); $i++)
	{
		$select_categoryidx = $categorylist[$i]["categoryidx"];
		$select_category = $categorylist[$i]["category"];
		
		if ($select_categoryidx == $search_categoryidx)
			$selected = "selected";
		else
			$selected = "";
?>
						<option value="<?= $select_categoryidx ?>" <?= $selected ?>><?= $select_category ?></option>
<?
	}
?>                    	
					</select>                    
	                     
                    <span class="search_lbl ml20">미답변횟수</span>
                    <input type="text" class="search_text" id="noreplycount" name="noreplycount" style="width:50px" value="<?= encode_html_attribute($search_noreplycount) ?>" onkeypress="search_press(event); return checknum();" /> 회 이상
					<br/><br/>
                    
                    <span class="search_lbl">문의내용</span>
                    <input type="text" class="search_text" id="contents" name="contents" style="width:170px" value="<?= encode_html_attribute($search_contents) ?>" onkeypress="search_press(event)" />
                    
                    <span class="search_lbl ml20">답변내용</span>
                    <input type="text" class="search_text" id="answer" name="answer" style="width:170px" value="<?= encode_html_attribute($search_answer) ?>" onkeypress="search_press(event)" />
                    
                    <span class="search_lbl ml20">답변난이도</span>
                    <select name="level">
                    	<option value="">난이도선택</option>
						<option value="0" <?= ($search_level == '0') ? "selected" : "" ?>>초급</option>
						<option value="1" <?= ($search_level == '1') ? "selected" : "" ?>>중급</option>
						<option value="2" <?= ($search_level == '2') ? "selected" : "" ?>>고급</option>
					</select>    
					
					<br/><br/>

                </div>
            </form>
                        
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="5">
                <col width="">
                <col width="230">
                <col width="80">
                <col width="120">
                <col width="120">
            </colgroup>
            <thead>
            <tr>
                <th></th>
                <th class="tdl">문의내용</th>
                <th>이름</th>
                <th>답변/공개</th>
                <th>답변일</th>
                <th>작성일</th>
            </tr>
            </thead>
            <tbody>
<?
	$db_main = new CDatabase_Main();

    for ($i=0; $i<sizeof($qalist); $i++)
    {
        $qaidx = $qalist[$i]["qaidx"];
        $contents = $qalist[$i]["contents"];        
        $category = $qalist[$i]["category"];
        $name = $qalist[$i]["name"];
        $useridx = $qalist[$i]["useridx"];
        $facebookid = $qalist[$i]["facebookid"];
        $status = $qalist[$i]["status"];
        $ispublic = $qalist[$i]["ispublic"];
        $responsedate = $qalist[$i]["responsedate"];
        $writedate = $qalist[$i]["writedate"];
        $level = $qalist[$i]["level"];
        
        if($facebookid < 10)
        	$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$facebookid.".png";
        else
            $photourl = get_fb_pictureURL($facebookid,$client_accesstoken);
		
		$sql = "SELECT COUNT(*) FROM tbl_user_online_sync2 WHERE useridx=$useridx";
		$online_status = $db_main->getvalue($sql);
      
        $sql = "SELECT vip_level FROM tbl_user_detail WHERE useridx=$useridx";
        $viplevel = $db_main->getvalue($sql);
        
       // if ($viplevel > 0 &&  $viplevel <= 12)
       // 	$viplevel_image = "&nbsp;<img src=\"/images/membership/vp_level_".$viplevel.".png\" height=\"30\" width=\"30\" align=\"absmiddle\" />";
        //else
        	$viplevel_image = "";
?>
            <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="qa_amazon_write(<?= $qaidx ?>)">
                <td class="tdc"></td>
                <td class="point_title"><?= encode_xml_field($contents) ?></td>
				<td class="point_title"><span style="float:left;padding-top:8px;padding-right:2px;"><img src="/images/icon/<?= ($online_status == "1") ? "status_1.png" : "status_3.png" ?>" align="absmiddle" /></span>  <span style="float:left;"><img src="<?= $photourl ?>" height="25" width="25" align="absmiddle" hspace="3"></span> <?= $name ?> <?= $viplevel_image ?></td>
                <td class="tdc"><?= ($status == "1") ? "답변(".get_level($level).")" : "<b style='color:red'>미답변</b>" ?><br><?= ($ispublic == "1") ? "<b style='color:blue'>공개</b>" : "미공개" ?></td>
                <td class="tdc"><?= ($status == "1") ? $responsedate : "" ?></td>
                <td class="tdc"><?= $writedate ?></td>
            </tr>
<?
    } 

	$db_main->end();
	
?>
            </tbody>
    </table>
            
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
        </div>
        <!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>