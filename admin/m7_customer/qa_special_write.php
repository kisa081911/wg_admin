<?
    $top_menu = "customer";
    $sub_menu = "qa_special";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    // ckeditor 경로 설정
    include_once($_SERVER["DOCUMENT_ROOT"]."/ckeditor/ckeditor.php");
    
    $qaidx = $_GET["qaidx"];
    $useridx = $_GET["useridx"];
    $pagefield = $_GET["pagefield"];
	
    check_number($qaidx);
    check_number($useridx);
    
	if ($qaidx == "" && $useridx == "")
		error_back("잘못된 접근입니다.");
	
    $db_analysis = new CDatabase_Analysis();
    $db_main = new CDatabase_Main();
	
	if ($qaidx != "")
	{
	    $sql = "SELECT qaidx,contents,useridx,name,facebookid,status,responsedate,writedate,top_categoryidx,categoryidx,ispublic,ispending,isspecial,isdelete,usereporting,reportingcategory,reviewidx,review_status,occurdate,writedate FROM support_qa WHERE qaidx=$qaidx";
	    $faqs = $db_analysis->getarray($sql);
	    
	    $qaidx = $faqs["qaidx"];
	    $contents = $faqs["contents"];
	    $status = $faqs["status"];
	    $useridx = $faqs["useridx"];
	    $name = $faqs["name"];
	    $facebookid = $faqs["facebookid"];
	    $responsedate = $faqs["responsedate"];
	    $writedate = $faqs["writedate"];
	    $top_categoryidx = $faqs["top_categoryidx"];
	    $categoryidx = $faqs["categoryidx"];
	    $ispublic = $faqs["ispublic"];
	    $ispending = $faqs["ispending"];
	    $isspecial = $faqs["isspecial"];
	    $isdelete = $faqs["isdelete"];
	    $usereporting = $faqs["usereporting"];
	    $reportingcategory = $faqs["reportingcategory"];
	    $reviewidx = $faqs["reviewidx"];
	    $review_status = $faqs["review_status"];
	    $occurdate = $faqs["occurdate"];
	    $writedate = $faqs["writedate"];

	    if ($qaidx == "")
    	    error_back("존재하지 않는 QA입니다.");
	    
	    $sql = "SELECT reviewidx FROM claim_review WHERE qaidx = $qaidx";
	    $old_reviewidx = $db_analysis->getvalue($sql);
	    
	}
	else
	{
	    $qaidx = "0";
	    $contents = "";
	    $status = "0";
	    $ispublic = "0";
		$ispending = "0";
		$isspecial = "0";
		$isdelete = "0";
		$usereporting = "0";
		
		$sql = "SELECT nickname,userid FROM tbl_user WHERE useridx=$useridx";
		$data = $db_main->getarray($sql);

	    $name = $data["nickname"];
	    $facebookid = $data["userid"];
	}
	
	if($facebookid < 10)
		$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$facebookid.".png";
	else
	    $photourl = get_fb_pictureURL($facebookid,$client_accesstoken);
    
	$sql = "SELECT categoryidx,category FROM support_category WHERE top_categoryidx=0 ORDER BY seqno";
	
	$top_categorylist = $db_analysis->gettotallist($sql);
    
	$sql = "SELECT categoryidx,top_categoryidx,category,".
		"(SELECT category FROM support_category WHERE categoryidx=A.top_categoryidx) AS top_category ".
		" FROM support_category A WHERE top_categoryidx<>0 ORDER BY (SELECT seqno FROM support_category WHERE categoryidx=A.top_categoryidx),seqno";
	
	$categorylist = $db_analysis->gettotallist($sql);
	
	$sql = "SELECT coin,sex FROM tbl_user WHERE useridx=$useridx";
	$data = $db_main->getarray($sql);
					
	$coin = $data["coin"];	
	$sex = $data["sex"];

	$sql = "SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE status=1 AND useridx=$useridx";
	$totalcredit = $db_main->getvalue($sql);
		
	$sql = "SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE status=2 AND useridx=$useridx";
	$totalcancel = $db_main->getvalue($sql);		
	
	$sql = "SELECT SUM(coin) ".
			"FROM ( ".
     		"	SELECT IFNULL(SUM(coin),0) AS coin FROM tbl_product_order WHERE status=1 AND useridx=$useridx ".     		
			") t1";
	
	$totalchip = $db_main->getvalue($sql);
	
	$sql = "SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE status=1 AND useridx=$useridx AND writedate>DATE_ADD(NOW(),INTERVAL -10 DAY)";
	$recentcredit = $db_main->getvalue($sql);	
	
	$sql = "SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE status=2 AND useridx=$useridx AND writedate>DATE_ADD(NOW(),INTERVAL -10 DAY)";
	$recentcancel = $db_main->getvalue($sql);	
	
	$sql = "SELECT IFNULL(SUM(freecoin),0) FROM tbl_freecoin_admin_log WHERE useridx=$useridx";
	$totalsent = $db_main->getvalue($sql);

	$sql = "SELECT IFNULL(SUM(freecoin),0) FROM tbl_freecoin_admin_log WHERE useridx=$useridx AND writedate>DATE_ADD(NOW(),INTERVAL -10 DAY)";
	$recentsent = $db_main->getvalue($sql);
    
	$sql = "SELECT qaidx,contents,writedate,status,isspecial,reviewidx,review_status FROM support_qa WHERE useridx=$useridx ORDER BY qaidx DESC";
	$qalist1 = $db_analysis->gettotallist($sql);
	
	$qalist = "0";
	
	for ($i=0; $i<sizeof($qalist1); $i++)
		$qalist .= ",".$qalist1[$i]["qaidx"];
	
	$sql = "SELECT qaidx,adminid,answer,writedate,coin,coin_memo,coin_message FROM support_qa_answer WHERE qaidx IN ($qalist) ORDER BY answeridx";
	$qalist2 = $db_analysis->gettotallist($sql);

	$sql = "SELECT status,facebookcredit,writedate FROM tbl_product_order WHERE useridx=$useridx AND status IN (1,2) ORDER BY orderidx DESC LIMIT 7";
	$orderlist = $db_main->gettotallist($sql);

	$sql = "SELECT freecoin,reason,admin_id,writedate FROM tbl_freecoin_admin_log WHERE useridx=$useridx ORDER BY logidx DESC LIMIT 100";
	$freecoinlist = $db_main->gettotallist($sql);
	
	$sql = "SELECT vip_level FROM tbl_user_detail WHERE useridx=$useridx";
	$vip_level = $db_main->getvalue($sql);
	
	//if ($vip_level > 0)
	//	$vip_level_image = "&nbsp;<img src=\"/images/membership/vp_level_".$vip_level.".png\" height=\"30\" width=\"30\" align=\"absmiddle\" />";
	//else
		$vip_level_image = "";
?>
<style>
	div td {word-break:break-all}
</style>
<script>
	var g_savetype = "";
	
     function setCookie(cName, cValue, cDay)
     {
          var expire = new Date();
          expire.setDate(expire.getDate() + cDay);
          cookies = cName + '=' + escape(cValue) + '; path=/ ';
          if(typeof cDay != 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
          document.cookie = cookies;
     }

     function getCookie(cName) 
     {
          cName = cName + '=';
          var cookieData = document.cookie;
          var start = cookieData.indexOf(cName);
          var cValue = '';
          if(start != -1){
               start += cName.length;
               var end = cookieData.indexOf(';', start);
               if(end == -1)end = cookieData.length;
               cValue = cookieData.substring(start, end);
          }
          return unescape(cValue);
     }	
     
	function window_onload()
	{
		return;
		
		var contents = getCookie("answer_contents");
		
		if (contents != "" && contents != null)	
			CKEDITOR.instances.contents.setData(contents);
			
		setInterval("save_contents()", 3000);
	}
	
	function save_contents()
	{
		var contents = CKEDITOR.instances.contents.getData();
		
		if (contents != "")
		{
			setCookie("answer_contents", contents, 1);
		}
	}

	function save_cr()
	{
		var input_form = document.input_form;
		
		var param = {};
		
        param.qaidx = "<?= $qaidx ?>";
        param.top_categoryidx = input_form.category.options[input_form.category.selectedIndex].getAttribute("top_categoryidx");
        param.categoryidx = input_form.category.options[input_form.category.selectedIndex].value;
        param.useridx = "<?= $useridx ?>";
        param.facebookid = "<?= $facebookid?>";
        param.name = "<?= $name?>";
        param.question = "<?= encode_script_value($contents)?>";
        param.writedate = "<?= $writedate?>";
        param.occurdate = "<?= $occurdate?>";
	        
		WG_ajax_execute("customer/save_cr", param, save_cr_callback);
	}

	function save_cr_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            alert("해당 문의가 클레임 리뷰로 등록 되었습니다.");
        }
    }
	
    function save_qa(savetype)
    {
    	g_savetype = savetype;
    	
        var input_form = document.input_form;
        
<?
	if ($qaidx == "0")
	{
?>
        if (input_form.question.value == "")
        {
            alert("문의 내용을 입력해주세요.");
            input_form.question.focus();
            return;
        }
<?
	}
?>
        
        if (savetype != "1")
        {
	        if (CKEDITOR.instances.contents.getData() == "")
	        {
	            alert("답변내용을 입력하세요.");
	            CKEDITOR.instances.contents.focus();
	            return;
	        }

	        var free_type = $("#free_type").val();
	        var free_amount = $("#free_amount").val();
			var free_reason = $("#reason").val();
			var free_message = $("#message").val();
			var free_adminid = $("#admin_id").val();

	        free_amount = strip_price_format(free_amount);
	        free_amount = Number(free_amount);

			if(free_amount != "")
			{
				if (free_type == "")
		        {			         
		            alert("보상 자원을 선택해주세요.");
		            $("#free_type").focus();
		            return;
		        }

				if (free_reason == "")
		        {			         
		            alert("보상 사유를 입력해주세요.");
		            $("#reason").focus();
		            return;
		        }

				if (free_message == "")
		        {			         
		            alert("보내는 메세지를 입력해주세요.");
		            $("#message").focus();
		            return;
		        }
			}
		}
		        
		if (input_form.category.value == "")
		{
			alert("분류를 선택해주세요.");
			input_form.category.focus();
			return;
		}
		
        var param = {};
        param.qaidx = "<?= $qaidx ?>";
        param.top_categoryidx = input_form.category.options[input_form.category.selectedIndex].getAttribute("top_categoryidx");
        param.categoryidx = input_form.category.options[input_form.category.selectedIndex].value;
        param.answer = CKEDITOR.instances.contents.getData();
        param.memo = input_form.memo.value;
        param.ispublic = get_radio("ispublic");
        param.ispending = get_radio("ispending");
		param.isspecial = get_radio("isspecial");
		param.isdelete = get_radio("isdelete");
        param.usereporting = get_radio("usereporting");
        param.reportingcategory = get_radio("reportingcategory");
        param.savetype = savetype;
        param.status = get_radio("status");
        param.free_type = free_type;
        param.free_amount = free_amount;
        param.free_reason = free_reason;
        param.free_message = free_message;
        param.free_adminid = free_adminid;
        param.level = input_form.level.value;
        param.finalansweridx = finalansweridx;
        
<?
	if ($qaidx == "0")
	{
?>
        param.question = input_form.question.value;
        param.useridx = "<?= $useridx ?>";
<?
	}
?>

        WG_ajax_execute("customer/save_qa", param, save_qa_callback);
    }

    function save_qa_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
        	if (g_savetype == "1")
        	{
        		alert("QA의 정보를 변경했습니다.");
        		window.location.href = "qa_special.php?<?= $pagefield ?>";
        	}
        	else
	            window.location.href = "qa_special.php?<?= $pagefield ?>";
        }
    }

    function delete_qa()
    {
        if (!confirm("질문과 답변을 모두 삭제 하시겠습니까?"))
            return;

        var param = {};
        param.qaidx = "<?= $qaidx ?>";

        WG_ajax_execute("customer/delete_qa", param, delete_qa_callback);
    }

    function delete_qa_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.href = "qa_special.php?<?= $pagefield ?>";
        }
    }

    function answer_qa(qaidx)
    {
        if (!confirm("해당 QA를 답변 완료 처리 하시겠습니까?"))
            return;

        var param = {};
        param.qaidx = qaidx;

        WG_ajax_execute("customer/answer_qa", param, answer_qa_callback);
    }

    function answer_qa_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.reload(false);
        }
    }
    
    function delete_answer(answeridx)
    {
        if (!confirm("해당 답변을 삭제 하시겠습니까?"))
            return;

        var param = {};
        param.qaidx = "<?= $qaidx ?>";
        param.answeridx = answeridx;

        WG_ajax_execute("customer/delete_answer", param, delete_answer_callback);
    }

    function delete_answer_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
        //	setCookie("answer_contents", "", -1);
            window.location.reload(false);
        }
    }

    function update_answer(answeridx)
    {
        if (!confirm("해당 답변 등급/피드백을 저장 하시겠습니까?"))
            return;

        var param = {};
        param.qaidx = "<?= $qaidx ?>";
        param.answeridx = answeridx;
        param.level = document.getElementById('level_' + answeridx).value;
        param.feedback = document.getElementById('feedback_' + answeridx).value;

        WG_ajax_execute("customer/update_answer", param, update_answer_callback);
    }

    function update_answer_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.reload(false);
        }
    }
    
    function template_changed()
    {
    	var contents = input_form.template.options[input_form.template.selectedIndex].value;
    	
    	if (contents == "")
    		return; 
    	
    	contents = ReplaceText(contents, "\\[NAME\\]", "<?= $name ?>");
    	
    	CKEDITOR.instances.contents.setData(contents);
    }
    
    var g_template_list = new Array();
    
    function category_changed(change_flag)
    {
        var change_flag = change_flag;
        if(change_flag != 1){
        	document.input_form.template_search.value = "";
        }
        
        var search = document.input_form.template_search.value;
        if(search == "" && change_flag != ""){
			
        	var option = document.input_form.category.options[document.input_form.category.selectedIndex];
        	var categoryidx;
        	
        	if (option.value == "")
        		categoryidx = "";
        	else if (option.value == "0")
        		categoryidx = option.getAttribute("top_categoryidx");
        	else
        		categoryidx = option.value;
        	
        	var template = document.input_form.template;
        	
        	for (var i=0; i<g_template_list.length; i++)
        	{
        		template.options[template.options.length] = g_template_list[i];
        	}
        	
        	g_template_list = new Array();
        	
        	for (var i=template.options.length-1; i>=1; i--)
        	{
        		if (categoryidx != "" && template.options[i].getAttribute("top_categoryidx") != categoryidx && template.options[i].getAttribute("categoryidx") != categoryidx && template.options[i].getAttribute("top_categoryidx") != "0" && template.options[i].getAttribute("categoryidx") != "0")
        		{
        			g_template_list.push(template.options[i]);
        			template.options[i] = null;
        		}
        	}

        	
        	
        } else {
        	        	
        	var option = document.input_form.category.options[document.input_form.category.selectedIndex];
        	var categoryidx;
        	
        	if (option.value == "")
        		categoryidx = "";
        	else if (option.value == "0")
        		categoryidx = option.getAttribute("top_categoryidx");
        	else
        		categoryidx = option.value;
        	
        	var template = document.input_form.template;
        	
        	for (var i=0; i<g_template_list.length; i++)
        	{	
        			template.options[template.options.length] = g_template_list[i];
        	}
        	
        	g_template_list = new Array();
        	
        	for (var i=template.options.length-1; i>=1; i--)
        	{
        		if (categoryidx != "" && template.options[i].getAttribute("top_categoryidx") != categoryidx && template.options[i].getAttribute("categoryidx") != categoryidx && template.options[i].getAttribute("top_categoryidx") != "0" && template.options[i].getAttribute("categoryidx") != "0")
        		{
        			g_template_list.push(template.options[i]);
        			template.options[i] = null;
        		} else {
            		template.options[i].text = template.options[i].text.toLowerCase();
            		search = search.toLowerCase();
        			if(template.options[i].text.indexOf(search, 0) == -1){        				
        				g_template_list.push(template.options[i]);
                		template.options[i] = null;
            		} 
        		}
        	}

        	if(template.options.length < 2){
				alert("검색 결과 없음");
        	} 
        	
        }

    }

    function change_freetype(type)
	{
		if(type == 1)
		{
			$("#free_type_name").html("Coin");
			$("#message").val("Take5 Help Center`s issued Coins!");
		}
		else
		{
			//$("#item_type").hide();
			$("#free_type_name").html("");
			$("#message").val("");
		}
	}

    </script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>

<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; QA 상세   - 특별 대응 문의</div>                              
            	<div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_qa()"></div>
            </div>
            <!-- //title_warp -->

            <form name="input_form" id="input_form" onsubmit="return false">

			<div style="float:left;width:600px">
				
	    	<div class="h2_title">사용자정보</div>
	    	<div class="qa_user_info_summary" onmouseover="className='qa_user_info_summary_over'" onmouseout="className='qa_user_info_summary'" onclick="window.open('/m2_user/user_view.php?useridx=<?= $useridx ?>')">
    			<img src="<?= $photourl ?>" height="50" width="50" class="summary_user_image">
    			<div class="sumary_username_wrap">
	    			<div class="summary_username">[<?= $useridx ?>] <?= $name ?> <?= ($sex == "1") ? "(M)" : "(F)" ?>&nbsp;<?= $vip_level_image?></div>
    			</div>
    			<div class="clear"></div><br/>
    			<div class="summary_user_coin"><?= number_format($coin) ?></div>
	    	</div>

			<div>
			<div style="float:left;width:295px">
	    	<div class="h2_title">결제/보상 정보</div>
	    	<div class="product_info_summary">
	    		<div class="summary_item" style="padding-top:3px">
	    			<div class="summary_item_lbl">총결제금액</div>
	    			<div class="summary_item_value"><?= make_price_format($totalcredit) ?> credit</div>
	    		</div>	    		
	    			    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">총취소금액</div>
	    			<div class="summary_item_value"><?= make_price_format($totalcancel) ?> credit</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">10일내 결제</div>
	    			<div class="summary_item_value"><?= make_price_format($recentcredit) ?> credit</div>
	    		</div>	    		

	    		<div class="summary_item">
	    			<div class="summary_item_lbl">총구매코인</div>
	    			<div class="summary_item_value"><?= make_price_format($totalchip) ?> coin</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">총보상금액</div>
	    			<div class="summary_item_value"><?= make_price_format($totalsent) ?> coin <?= ($totalchip == 0) ? "" : "(".(round($totalsent * 1000 / $totalchip)/10)."%)" ?></div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">10일내 보상금액</div>
	    			<div class="summary_item_value"><?= make_price_format($recentsent) ?> coin</div>
	    		</div>
	    	</div>
			</div>
			
			<div style="float:right;width:295px">
	    	<div class="h2_title">최근 결제 내역</div>
	    	<div class="product_info_summary">
<?
	for ($i=0; $i<sizeof($orderlist); $i++)
	{
		$order_status = $orderlist[$i]["status"];
		$order_facebookcredit = $orderlist[$i]["facebookcredit"];
		$order_writedate = $orderlist[$i]["writedate"];
?>	    		
	    		<div class="summary_item" style="<?= ($i == 0) ? "padding-top:3px" : "" ?>">
	    			<div class="summary_item_lbl" <?= ($order_status == "2") ? "style='color:red'" : "" ?>><?= $order_writedate ?></div>
	    			<div class="summary_item_value" <?= ($order_status == "2") ? "style='color:red'" : "" ?>><?= make_price_format($order_facebookcredit) ?> credit</div>
	    		</div>
<?
	}
?>	    		
	    	</div>
			</div>
						
			</div>
			
	    	<div class="h2_title">문의 정보</div>
	    	<div class="product_info_summary">
<?
	if ($qaidx != "0")
	{
?>		
	    		<div class="summary_item" style="padding-top:3px">
	    			<div class="summary_item_lbl">문의 내용</div>
	    			<div class="summary_item_value" id="qa_question" name="qa_question" style="word-break:break-all"><?= str_replace("\n", "<br>", encode_xml_field($contents)) ?></div>
	    		</div>
<?
	}
	else
	{    		
?>
	    		<div class="summary_item" style="padding-top:3px">
	    			<div class="summary_item_lbl">문의 내용</div>
	    			<div class="summary_item_value" style="word-break:break-all"><textarea id="question" name="question" style="height:150px"></textarea></div>
	    		</div>
<?
	}	    		
?>
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">답변여부</div>
	    			<div class="summary_item_value">
                        <input type="radio" class="radio" name="status" value="1" <?= ($status == "1") ? "checked" : "" ?>> 답변
                        <input type="radio" class="radio ml20" name="status" value="0" <?= ($status == "0") ? "checked" : "" ?> > 미답변
					</div>
	    		</div>

	    		<div class="summary_item">
	    			<div class="summary_item_lbl">공개여부</div>
	    			<div class="summary_item_value">
                        <input type="radio" class="radio" name="ispublic" value="1" <?= ($ispublic == "1") ? "checked" : "" ?>> 공개
                        <input type="radio" class="radio ml20" name="ispublic" value="0" <?= ($ispublic == "0") ? "checked" : "" ?> > 미공개
					</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">특별 대응 문의 여부</div>
	    			<div class="summary_item_value">
                        <input type="checkbox" class="radio" name="isspecial" value="1" <?= ($isspecial == "1") ? "checked" : "" ?>>                        
					</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">문의/답변 숨김 여부</div>
	    			<div class="summary_item_value">
                        <input type="checkbox" class="radio" name="isdelete" value="1" <?= ($isdelete == "1") ? "checked" : "" ?>>                        
					</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">Pending 여부</div>
	    			<div class="summary_item_value">
                        <input type="checkbox" class="radio" name="ispending" value="1" <?= ($ispending == "1") ? "checked" : "" ?>>
					</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">Reporting 여부</div>
	    			<div class="summary_item_value">
                        <input type="checkbox" class="radio" name="usereporting" value="1" <?= ($usereporing == "1") ? "checked" : "" ?>>
                        
					<select name="reportingcategory" id="reportingcategory">
						<option value="">Reporting 분류 선택</option>
						<option value="General" <?= ("General" == $reportingcategory) ? "selected=\"true\"" : "" ?> >General</option>
					</select>
                        
					</div>
					
	    		</div>

	    		<div class="summary_item">
	    			<div class="summary_item_lbl">발생일시</div>
	    			<div class="summary_item_value" id="qa_occurdate" name="qa_occurdate"><?= $occurdate ?></div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">작성일시</div>
	    			<div class="summary_item_value" id="qa_writedate" name="qa_writedate"><?= $writedate ?></div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">분류</div>
	    			<div class="summary_item_value">
					<select name="category" onchange="category_changed()">
                    	<option value="">분류선택</option>
<?
	for ($i=0; $i<sizeof($top_categorylist); $i++)
	{
		$select_top_categoryidx = $top_categorylist[$i]["categoryidx"];
		$select_top_category = $top_categorylist[$i]["category"];
		
		if ($select_top_categoryidx == $top_categoryidx && $categoryidx == "0")
			$selected = "selected";
		else
			$selected = "";
?>
						<option value="0" top_categoryidx="<?= $select_top_categoryidx ?>" <?= $selected ?>><?= $select_top_category ?></option>
<?
	}

	for ($i=0; $i<sizeof($categorylist); $i++)
	{
		$select_top_categoryidx = $categorylist[$i]["top_categoryidx"];
		$select_categoryidx = $categorylist[$i]["categoryidx"];
		$select_top_category = $categorylist[$i]["top_category"];
		$select_category = $categorylist[$i]["category"];
		
		if ($select_categoryidx == $categoryidx)
			$selected = "selected";
		else
			$selected = "";
?>
						<option value="<?= $select_categoryidx ?>" top_categoryidx="<?= $select_top_categoryidx ?>" <?= $selected ?>><?= $select_top_category ?>/<?= $select_category ?></option>
<?
	}
?>                    	
                    </select>
					</div>
	    		</div>
	    		<br/>
	    		
<?
	if ($qaidx != "0")
	{
?>			    		
                <input type="button" class="btn_setting_01" value="정보변경" onclick="save_qa(1)">
                
<?
		if($old_reviewidx == "")
		{
?>
                <input type="button" class="btn_setting_01" value="클레임 리뷰 등록" onclick="save_cr()">
<?
		}
	}
?>
	    	</div>
	    	
	    	</div>
	    	
	    	<div style="float:right;vertical-align:top;width:470px">
	    		<div class="h2_title">기존 문의 내역</div>
	
		    	<div class="product_info_summary" style="min-height:400px;height:400px;overflow-y:scroll;cursor:default">
<?
	for ($i=0; $i<sizeof($qalist1); $i++)
	{
		$prev_qaidx = $qalist1[$i]["qaidx"];
		$contents = $qalist1[$i]["contents"];
		$writedate = $qalist1[$i]["writedate"];
		$status = $qalist1[$i]["status"];
		$isspecial = $qalist1[$i]["isspecial"];		
		$reviewidx = $qalist1[$i]["reviewidx"];
		$review_status = $qalist1[$i]["review_status"];
		
		if ($status == "0" && $reviewidx != "0" && $review_status == "0")
			$status = "<b style='color:#FF3344;'>[미답변]/[리뷰중]</b> ";
		else if ($status == "1" && $reviewidx != "0" && $review_status == "0")
			$status = "<b style='color:#FF3344;'>[리뷰중]</b> ";
		else if ($status =="0" && $reviewidx == "0")
			$status = "<b style='color:#FF3344;'>[미답변]</b> ";
		else if ($status == "0" && $reviewidx != "0" && $review_status != "0")
			$status = "<b style='color:#FF3344;'>[미답변]</b> ";
		else 
			$status = "";
		
		if($isspecial == 0)
			$page_url = "qa_write.php";
		else
			$page_url = "qa_special_write.php";
			
?>	
		    		<div class="summary_item" <?= ($i == 0) ? "style='border-bottom:1px dashed #cdcdcd;padding-top:3px;padding-bottom:7px;min-height:70px'" : "style='border-bottom:1px dashed #cdcdcd;padding-bottom:7px;min-height:70px'" ?>>
		    			<div class="summary_item_lbl"><?= $writedate ?><br>
		    				<input type="button" class="btn_01" value="해당 문의 이동" onclick="window.location.href='<?= $page_url ?>?qaidx=<?= $prev_qaidx ?>&<?= $pagefield ?>'" title="해당 문의로 이동">
<?
		if ($status != "")
		{
?>			
		    				<input type="button" class="btn_05" style="margin-top:2px" value="답변 완료 처리" onclick="answer_qa(<?= $prev_qaidx ?>)" title="답변 완료 처리">
<?
		}
?>		    				
		    			</div>
		    			<div class="summary_item_value" <?= ($prev_qaidx == $qaidx) ? "style='color:#CC4455;word-break:break-all;font-weight:bold'" : "style='word-break:break-all'" ?>><?= $status ?><?= str_replace("\n", "<br>", encode_xml_field($contents)) ?></div>
		    		</div>
<?
		for ($j=0; $j<sizeof($qalist2); $j++)
		{
			$answer_qaidx = $qalist2[$j]["qaidx"];
			$adminid = $qalist2[$j]["adminid"];
			$answer = $qalist2[$j]["answer"];
			$writedate = $qalist2[$j]["writedate"];
			$coin = $qalist2[$j]["coin"];
			$coin_memo = $qalist2[$j]["coin_memo"];
			$coin_message = $qalist2[$j]["coin_message"];
			
			if ($prev_qaidx == $answer_qaidx)
			{
				if ($coin == 0)
					$style = "border-bottom:1px dashed #cdcdcd;padding-bottom:7px";
				else
					$style = "";
?>
		    		<div class="summary_item" style="<?= $style ?>">
		    			<div class="summary_item_lbl" style="color:#5566AA"> => <?= $adminid ?><br><?= $writedate ?></div>
		    			<div class="summary_item_value" style="word-break:break-all;min-height:30px;color:#5566AA"><?= str_replace("<body", "<b", str_replace("</body", "</b", $answer)) ?></div>
		    		</div>
<?			
				if ($coin != "0")
				{
?>
		    		<div class="summary_item" style="border-bottom:1px dashed #cdcdcd;padding-bottom:7px">
		    			<div class="summary_item_lbl" style="color:#55AA66"></div>
		    			<div class="summary_item_value" style="word-break:break-all;color:#55AA66"><b><?= make_price_format($coin) ?></b> - <?= $coin_memo ?></div>
		    		</div>
<?			
				}
			}
		}
	}
?>		    		
		    	</div>
		    	
	    		<div class="h2_title">기존 보상 내역</div>
	
		    	<div class="product_info_summary" style="min-height:150px;height:150px;overflow-y:scroll;cursor:default">
<?
	for ($i=0; $i<sizeof($freecoinlist); $i++)
	{		$free_coin = $freecoinlist[$i]["coin"];

		$reason = $freecoinlist[$i]["reason"];
		$admin_id = $freecoinlist[$i]["admin_id"];
		$writedate = $freecoinlist[$i]["writedate"];
		
		$free_msg = "";
		
		if($free_coin > 0)
			$free_msg = number_format($free_coin)." Coin";

?>	
		    		<div class="summary_item" <?= ($i == 0) ? "style='border-bottom:1px dashed #cdcdcd;padding-top:3px;padding-bottom:7px;cursor:pointer'" : "style='border-bottom:1px dashed #cdcdcd;padding-bottom:7px;cursor:pointer'" ?>>
		    			<div class="summary_item_lbl"><?= $writedate ?></div>
		    			<div class="summary_item_value" style='word-break:break-all'><b><?= $free_msg ?></b> - <?= $reason ?> (발급자: <?= $admin_id?>)</div>
		    		</div>
<?
	}
?>		    		
		    	</div>		    	
	    	</div>
	    	<br/><br/>
	    	
            <table class="tbl_view_basic">
            <colgroup>
                <col width="150">
                <col width="">
            </colgroup>
            <tbody>
            	<tr>
            		<th><span></span> 답변템플릿 검색</th>
            		<td>
            			<input type="text" class="view_tbl_text" name="template_search" id="template_search" value="" onkeydown="javascript:if(event.keyCode == 13){ category_changed(1) }" style="width:150px" />
            			<input type="button" class="btn_setting_01" name="" id="" value="검색" style="height:28px;" onclick="category_changed(1)"/>
            		</td>
            	</tr>
                <tr>
                    <th><span></span> 답변템플릿</th>
                    <td>
                        <select name="template" id="template" onchange="template_changed()">
                        	<option value="">답변 템플릿 선택</option>
<?
	$sql = "SELECT top_categoryidx,categoryidx,title,contents FROM support_qa_template ORDER BY title ASC";
	$list = $db_analysis->gettotallist($sql);
	
	for ($i=0; $i<sizeof($list); $i++)
	{
		$template_top_categoryidx = $list[$i]["top_categoryidx"];
		$template_categoryidx = $list[$i]["categoryidx"];
		$template_title = $list[$i]["title"];
		$template_contents = $list[$i]["contents"];
?>		
							<option value="<?= encode_html_attribute($template_contents) ?>" top_categoryidx="<?= $template_top_categoryidx ?>" categoryidx="<?= $template_categoryidx ?>"><?= $template_title ?></option>
<?	
	}
?>                        	
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><span>*</span> 답변내용</th>
                    <td>
                        <textarea id="contents" name="contents"></textarea>
                        <script type="text/javascript">CKEDITOR.replace("contents",{width: '100%',height: '250px'});</script>
                    </td>
                </tr>
                <tr>
                    <th><span></span> 내부메모</th>
                    <td>
                        <textarea id="memo" name="memo" style="height:50px"></textarea>
                    </td>
                </tr>
                <tr>
                    <th><span>*</span> 답변자</th>
                    <td>
                        Jessie Moore / 난이도 설정
                        <select name="level">
                        	<option value="0">초급</option>
                        	<option value="1">중급</option>
                        	<option value="2">고급</option>
                        </select>
                    </td>
                </tr>
				<tr>
					<th>보상 자원</th>
					<td>
						<select id="free_type" name="free_type" onchange="change_freetype(this.value);">
							<option value="">선택</option>
								<option value="1" <?= ($reward_type=="1") ? "selected" : "" ?>>Coin</option>
						</select>
					</td>
					<tr>
						<th>보상 금액</th>
						<td>
							<input type="text" class="view_tbl_text" style="width:200px" name="free_amount" id="free_amount" value="" onkeydown="return keep_price_format(this)" onkeyup="return keep_price_format(this)" onblur="return keep_price_format(this)" onkeypress="return checknum()"/>&nbsp;<span id="free_type_name" style="font-weight: bold;"></span>
						</td>
					</tr>
					<tr>
						<th>보상 사유</th>
						<td><input type="text" class="view_tbl_text" style="width:420px" name="reason" id="reason" maxlength="200" value="" /></td>
					</tr>
					<tr>
						<th>보내는 메세지</th>
						<td><input type="text" class="view_tbl_text" style="width:420px" name="message" id="message" maxlength="200" value="" /></td>
					</tr>
					<tr style="display:none;">
						<th>발급자</th>
						<td><input type="text" class="view_tbl_text" style="width:200px;" readonly="true" name="admin_id" id="admin_id" maxlength="50" value="<?= $_SESSION["adminid"]?>" /></td>
					</tr>
            </tbody>
            </table>
            </form> 
            
            <div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="정보변경/답변등록" onclick="save_qa(2)">
                <input type="button" class="btn_setting_02" value="취소" onclick="go_page('qa_special.php?<?= $pagefield ?>')">           
            </div>
            
<?
	$finalansweridx = "0";
	
	$sql = "SELECT answeridx,adminid,answer,memo,writedate,coin,coin_memo,coin_message,level,feedback FROM support_qa_answer WHERE qaidx=$qaidx ORDER BY answeridx";
	$list = $db_analysis->gettotallist($sql);
	
	for ($i=0; $i<sizeof($list); $i++)
	{
		$answeridx = $list[$i]["answeridx"];
		$adminid = $list[$i]["adminid"];
		$answer = $list[$i]["answer"];
		$memo = $list[$i]["memo"];
		$writedate = $list[$i]["writedate"];
		$coin = $list[$i]["coin"];
		$coin_memo = $list[$i]["coin_memo"];
		$coin_message = $list[$i]["coin_message"];
		$level = $list[$i]["level"];
		$feedback = $list[$i]["feedback"];
		
		$finalansweridx = $answeridx;
?>
			<table class="tbl_view_basic" style="border-top:1px dashed #cdcdcd;border-bottom:1px dashed #cdcdcd;margin-top:10px">
            <colgroup>
                <col width="150">
                <col width="">
            </colgroup>
            <tbody>
                <tr>
                    <th>답변자</th>
                    <td>Jessie Moore / <?= $adminid ?> <input type="button" class="btn_05" value="삭제" onclick="delete_answer('<?= $answeridx ?>')"></td>
                </tr>
                <tr>
                    <th>난이도/피드백</th>
                    <td>
						난이도 조정
                        <select name="level_<?= $answeridx ?>" id="level_<?= $answeridx ?>">
                        	<option value="0" <?= ($level == '0') ? "selected" : "" ?>>초급</option>
                        	<option value="1" <?= ($level == '1') ? "selected" : "" ?>>중급</option>
                        	<option value="2" <?= ($level == '2') ? "selected" : "" ?>>고급</option>
                        </select>
                        / 피드백
                        <input type="text" class="view_tbl_text" style="width:400px" name="feedback_<?= $answeridx ?>" id="feedback_<?= $answeridx ?>" maxlength="400" value="<?= encode_html_attribute($feedback) ?>" />
<?
	if ($login_adminid == "admin")
	{
?>		
                        <input type="button" class="btn_05" value="저장" onclick="update_answer('<?= $answeridx ?>')">
<?
	}
?>                        
					</td>
                </tr>
                <tr>
                    <th>답변내용</th>
                    <td><?= $answer ?></td>
                </tr>
                <tr>
                    <th>내부메모</th>
                    <td><?= str_replace("\n", "<br>", $memo) ?></td>
                </tr>
                <tr>
                    <th>답변일시</th>
                    <td><?= $writedate ?></td>
                </tr>
<?
		if ($coin != "0")
		{
?>
                <tr>
                    <th>보상 금액</th>
                    <td><?= make_price_format($coin) ?></td>
                </tr>
                <tr>
                    <th>보상 사유</th>
                    <td><?= $coin_memo ?></td>
                </tr>
                <tr>
                    <th>보내는 메시지</th>
                    <td><?= $coin_message ?></td>
                </tr>
<?			
		}
?>		                
            </tbody>
            </table>
<?
	}
	
	$db_main->end();
	$db_analysis->end();
?>
        </div>
        <!--  //CONTENTS WRAP -->
<script>
	category_changed();
	
	var finalansweridx = "<?= $finalansweridx ?>";
</script>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>