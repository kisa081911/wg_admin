<?
    $top_menu = "customer";
    $sub_menu = "qa_template";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    // ckeditor 경로 설정
    include_once($_SERVER["DOCUMENT_ROOT"]."/ckeditor/ckeditor.php");
    
    $templateidx = $_GET["templateidx"];
    
    check_number($templateidx);
    
    $db_analysis = new CDatabase_Analysis();
    
    if ($templateidx != "")
    {       
        $sql = "SELECT templateidx,title,contents,writedate,top_categoryidx,categoryidx FROM support_qa_template WHERE templateidx=$templateidx";
        $templates = $db_analysis->getarray($sql);
        
        $templateidx = $templates["templateidx"];
        $title = $templates["title"];
        $contents = $templates["contents"];
        $writedate = $templates["writedate"];
        $top_categoryidx = $templates["top_categoryidx"];
        $categoryidx = $templates["categoryidx"];
    
	    if ($templateidx == "")
            error_back("존재하지 않는 QA 답변 템플릿입니다.");
	}
    
	$sql = "SELECT categoryidx,top_categoryidx,category,".
		"(SELECT category FROM support_category WHERE categoryidx=A.top_categoryidx) AS top_category ".
		" FROM support_category A WHERE top_categoryidx<>0 ORDER BY (SELECT seqno FROM support_category WHERE categoryidx=A.top_categoryidx),seqno";
	
	$categorylist = $db_analysis->gettotallist($sql);
	
    $db_analysis->end();
?>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
	function save_template()
    {
        var input_form = document.input_form;
        
        if (input_form.title.value == "")
        {
            alert("제목을 입력하세요.");
            input_form.title.focus();
            return;
        }
        
        if (input_form.category.value == "")
        {
            alert("분류를 선택하세요.");
            input_form.category.focus();
            return;
        }

        if (CKEDITOR.instances.contents.getData() == "")
        {
            alert("내용을 입력하세요.");
            CKEDITOR.instances.contents.focus();
            return;
        }

		 var param = {};
		 param.templateidx = "<?= $templateidx?>";
		 param.title = input_form.title.value;
		 param.top_categoryidx = input_form.category.options[input_form.category.selectedIndex].getAttribute("top_categoryidx");
		 param.categoryidx = input_form.category.options[input_form.category.selectedIndex].value;
		 param.contents = CKEDITOR.instances.contents.getData();

		 if ("<?= $templateidx ?>" == "")
			WG_ajax_execute("customer/save_template", param, save_template_callback);
		else
			WG_ajax_execute("customer/update_template", param, save_template_callback);
    }

    function save_template_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            window.location.href = "qa_template.php";
        }
    }

    function delete_template()
    {
        if (!confirm("template를 삭제 하시겠습니까?"))
            return;

        var param = {};
        param.templateidx = "<?= $templateidx?>";

        WG_ajax_execute("customer/delete_template", param, delete_template_callback);
    }

    function delete_template_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.href = "qa_template.php";
        }
    }
</script>
	<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; QA 답변 템플릿 상세</div>
<?
    if ($templateidx != "")
    { 
?>
		<div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_template()"></div>
<?
    } 
?>
	</div>
	<!-- //title_warp -->
	
	<form name="input_form" id="input_form" onsubmit="return false">
		<table class="tbl_view_basic">
            <colgroup>
                <col width="150">
                <col width="">
            </colgroup>
            <tbody>
                <tr>
                    <th><span>*</span> 제목</th>
                    <td><input type="text" class="view_tbl_text" name="title" id="title" value="<?= encode_html_attribute($title) ?>" maxlength="1024"></td>
                </tr>
                <tr>
                    <th><span>*</span> 분류</th>
                    <td>
                    	<select name="category">
                    		<option value="">분류선택</option>
                    		<option value="0" top_categoryidx="0" <?= (0 == $categoryidx) ? "selected" : "" ?>>General</option>
<?
	for ($i=0; $i<sizeof($categorylist); $i++)
	{
		$select_top_categoryidx = $categorylist[$i]["top_categoryidx"];
		$select_categoryidx = $categorylist[$i]["categoryidx"];
		$select_top_category = $categorylist[$i]["top_category"];
		$select_category = $categorylist[$i]["category"];
		
		if ($select_categoryidx == $categoryidx)
			$selected = "selected";
		else
			$selected = "";
?>
							<option value="<?= $select_categoryidx ?>" top_categoryidx="<?= $select_top_categoryidx ?>" <?= $selected ?>><?= $select_top_category ?>/<?= $select_category ?></option>
<?
	}
?>                    	
                    	</select>
                    </td>
                </tr>
                <tr>
                    <th><span>*</span> 내용</th>
                    <td>
                        <textarea id="contents" name="contents"><?= $contents ?></textarea>
                        <script type="text/javascript">CKEDITOR.replace("contents",{width: '100%',height: '350px'});</script>
                    </td>
                </tr>
            </tbody>
		</table>    
	</form> 
            
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="저장" onclick="save_template()">
		<input type="button" class="btn_setting_02" value="취소" onclick="go_page('qa_template.php')">           
	</div>
</div>
<!--  //CONTENTS WRAP -->

<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>