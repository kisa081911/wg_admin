<?
    $top_menu = "customer";
    $sub_menu = "claim_review";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_name = trim($_GET["name"]);
    $search_facebookid = trim($_GET["facebookid"]);
    $search_useridx = trim($_GET["useridx"]);
    $search_status = $_GET["status"];//답변 여부(처리, 미처리)
    $search_response_status = $_GET["response_status"];
    $search_freecoin_status = $_GET["freecoin_status"];   //처리 여부(보상, 개평, 기타)
    $search_start_writedate = $_GET["start_writedate"];
    $search_end_writedate = $_GET["end_writedate"];
    $search_start_responsedate = $_GET["start_responsedate"];
    $search_end_responsedate = $_GET["end_responsedate"];
    $search_start_credit = trim($_GET["start_credit"]);
    $search_end_credit = trim($_GET["end_credit"]);
    $search_adminid = $_GET["adminid"];
    $search_usereporting = $_GET["usereporting"];
    $search_ispending = $_GET["ispending"];
    $search_categoryidx = $_GET["categoryidx"];
  	$search_contents = trim($_GET["contents"]); //  문의내용
  	$search_answer = $_GET["answer"]; // 답변내용
  	$search_check = $_GET["searchcheck"];
	
	if ($search_check != "1")
		$search_status = "0";
	
    $listcount = 10;
    $pagename = "claim_review.php";
    $pagefield = "answer=".urlencode($search_answer)."&searchcheck=$search_check&adminid=$search_adminid&status=$search_status&response_status=$search_response_status&freecoin_status=$search_freecoin_status&categoryidx=$search_categoryidx&name=$search_name&useridx=$search_useridx&facebookid=$search_facebookid&start_writedate=$search_start_writedate&end_writedate=$search_end_writedate&start_responsedate=$search_start_responsedate&end_responsedate=$search_end_responsedate&start_credit=$search_start_credit&end_credit=$search_end_credit&ispending=$search_ispending&usereporting=$search_usereporting&contents=".urlencode($search_contents);
    
    $db_analysis = new CDatabase_Analysis();
    
    $tail = "WHERE 1=1 ";
    
    if ($search_name != "")
        $tail .= " AND name LIKE '%$search_name%'";
    
    if ($search_contents != "")
        $tail .= " AND contents LIKE '%$search_contents%'";
    
    if ($search_useridx != "")
        $tail .= " AND useridx='$search_useridx'";
    
    if ($search_facebookid != "")
        $tail .= " AND facebookid LIKE '%$search_facebookid%'";	
    
    if ($search_start_writedate != "")
        $tail .= " AND writedate >= '$search_start_writedate 00:00:00'";
    
    if ($search_end_writedate != "")
        $tail .= " AND writedate <= '$search_end_writedate 23:59:59'";
    
    if ($search_start_responsedate != "")
        $tail .= " AND responsedate >= '$search_start_responsedate 00:00:00'";
    
    if ($search_end_responsedate != "")
        $tail .= " AND responsedate <= '$search_end_responsedate 23:59:59'";  
    
    if ($search_ispending != "")
        $tail .= " AND (SELECT ispending FROM support_qa WHERE qaidx = claim_review.qaidx) = $search_ispending";
    
    if ($search_usereporting != "")
        $tail .= " AND (SELECT usereporting FROM support_qa WHERE qaidx = claim_review.qaidx) = $search_usereporting ";
            
    if ($search_start_credit != "")
        $tail .= " AND IFNULL((SELECT summary_credit FROM user_order WHERE useridx=claim_review.useridx),0)>=$search_start_credit ";
    
    if ($search_end_credit != "")
        $tail .= " AND IFNULL((SELECT summary_credit FROM user_order WHERE useridx=claim_review.useridx),0)<=$search_end_credit ";
    
    if ($search_categoryidx != "")
        $tail .= " AND (top_categoryidx=$search_categoryidx OR categoryidx=$search_categoryidx) ";
    
    if ($search_adminid != "")
        $tail .= " AND EXISTS (SELECT * FROM claim_review_history WHERE qaidx = claim_review.qaidx AND adminid LIKE '%$search_adminid%') ";
        
    if ($search_answer != "")
        $tail .= " AND EXISTS (SELECT * FROM support_qa_answer WHERE qaidx = claim_review.qaidx AND answer LIKE '%$search_answer%') ";
    
    if ($search_status != "")
    	$tail .= " AND status = $search_status";
    
    if ($search_response_status != "")    	
    		$tail .= " AND response_status = $search_response_status";    	
    
    if ($search_freecoin_status != "")
    	$tail .= " AND freecoin_status = $search_freecoin_status";    
		
    $totalcount = $db_analysis->getvalue("SELECT COUNT(*) FROM claim_review $tail");    
    
    $sql ="SELECT reviewidx, qaidx, top_categoryidx, categoryidx, useridx, facebookid, name, contents, status, responsedate, occurdate, writedate, response_status, freecoin_status ".      		 
    		"FROM claim_review $tail ORDER BY reviewidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    $crlist = $db_analysis->gettotallist($sql);
    
	$sql = "SELECT categoryidx,category FROM support_category WHERE top_categoryidx=0 ORDER BY seqno";
	
	$top_categorylist = $db_analysis->gettotallist($sql);
    
	$sql = "SELECT categoryidx,top_categoryidx,category,".
		"(SELECT category FROM support_category WHERE categoryidx=A.top_categoryidx) AS top_category ".
		" FROM support_category A WHERE top_categoryidx<>0 ORDER BY (SELECT seqno FROM support_category WHERE categoryidx=A.top_categoryidx),seqno";
	
	$categorylist = $db_analysis->gettotallist($sql);
	
    $db_analysis->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
   
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>  
<style>
	div td {word-break:break-all}
</style>
<script>
    function claim_review_detail(reviewidx)
    {
        window.location.href = "claim_review_detail.php?pagefield=<?= urlencode($pagefield."&page=".$page) ?>&reviewidx="+reviewidx;
    }
    
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
        
    $(function() {
        $("#start_writedate").datepicker({ });
    });
    
    $(function() {
        $("#end_writedate").datepicker({ });
    });

    $(function() {
        $("#start_responsedate").datepicker({ });
    });
    
    $(function() {
        $("#end_responsedate").datepicker({ });
    });    
</script>       
        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 클레임 리뷰 <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
            </div>
            <!-- //title_warp -->
            
            <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="claim_review.php">
                <div class="detail_search_wrap">
                	<input type=hidden name="searchcheck" value="1">
                    <span class="search_lbl ml20">이름</span>
                    <input type="text" class="search_text" id="name" name="name" style="width:100px" value="<?= encode_html_attribute($search_name) ?>" onkeypress="search_press(event)" />
                    
                    <span class="search_lbl ml20">Facebook ID</span>
                    <input type="text" class="search_text" id="facebookid" name="facebookid" style="width:100px" value="<?= encode_html_attribute($search_facebookid) ?>" onkeypress="search_press(event); return checkonlynum();" />
                    
                    <span class="search_lbl ml20">UserIdx&nbsp;</span>
                    <input type="text" class="search_text" id="useridx" name="useridx" style="width:100px" value="<?= encode_html_attribute($search_useridx) ?>" onkeypress="search_press(event)" />
                    
                    <span class="search_lbl ml20">상태</span>
                    <input type="radio" name="status" value="" <?= ($search_status == "") ? "checked" : "" ?>>전체 <input type="radio" name="status" value="1" <?= ($search_status == "1") ? "checked" : "" ?>>처리 <input type="radio" name="status" value="0" <?= ($search_status == "0") ? "checked" : "" ?>>미처리 
                    
                    <span class="search_lbl ml20">유형</span>
                    <input type="radio" name="freecoin_status" value="" <?= ($search_freecoin_status == "") ? "checked" : "" ?>>전체 <input type="radio" name="freecoin_status" value="1" <?= ($search_freecoin_status == "1") ? "checked" : "" ?>>보상 <input type="radio" name="freecoin_status" value="2" <?= ($search_freecoin_status == "2") ? "checked" : "" ?>>개평 <input type="radio" name="freecoin_status" value="3" <?= ($search_freecoin_status == "3") ? "checked" : "" ?>>기타 
                                
                    <div class="clear" style="padding-top:10px"></div>
                    <span class="search_lbl">등록일&nbsp;&nbsp;</span>
                    <input type="text" class="search_text" id="start_writedate" name="start_writedate" style="width:65px" value="<?= $search_start_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
                    <input type="text" class="search_text" id="end_writedate" name="end_writedate" style="width:65px" value="<?= $search_end_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
                    <span class="search_lbl ml20">처리일</span>
                    <input type="text" class="search_text" id="start_responsedate" name="start_responsedate" style="width:65px" value="<?= $search_start_responsedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
                    <input type="text" class="search_text" id="end_responsedate" name="end_responsedate" style="width:65px" value="<?= $search_end_responsedate?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />

                    <span class="search_lbl ml20">결제액(credit)</span>
                    <input type="text" class="search_text" id="start_credit" name="start_credit" style="width:60px" value="<?= $search_start_credit ?>" onkeypress="search_press(event); return checknum();" /> ~
                    <input type="text" class="search_text" id="end_credit" name="end_credit" style="width:60px" value="<?= $search_end_credit ?>" onkeypress="search_press(event); return checknum();" />

                    <span class="search_lbl ml20">리뷰자ID</span>
                    <input type="text" class="search_text" id="adminid" name="adminid" style="width:100px" value="<?= encode_html_attribute($search_adminid) ?>" onkeypress="search_press(event);" />

                    <div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
                    
                    <div class="clear" style="padding-top:10px"></div>

                    <span class="search_lbl">Pending</span>
                    <input type="checkbox" name="ispending" value="1" <?= ($search_ispending == "1") ? "checked" : "" ?>>Pending 여부
                    
                    <span class="search_lbl ml20">리포팅</span>
                    <input type="checkbox" name="usereporting" value="1" <?= ($search_usereporting == "1") ? "checked" : "" ?>>리포팅 여부 

                    <span class="search_lbl ml20">분류</span>
                    <select name="categoryidx">
                    	<option value="">분류선택</option>
<?
	for ($i=0; $i<sizeof($top_categorylist); $i++)
	{
		$select_top_categoryidx = $top_categorylist[$i]["categoryidx"];
		$select_top_category = $top_categorylist[$i]["category"];
		
		if ($select_top_categoryidx == $search_categoryidx)
			$selected = "selected";
		else
			$selected = "";
?>
						<option value="<?= $select_top_categoryidx ?>" <?= $selected ?>><?= $select_top_category ?></option>
<?
	}

	for ($i=0; $i<sizeof($categorylist); $i++)
	{
		$select_top_categoryidx = $categorylist[$i]["top_categoryidx"];
		$select_categoryidx = $categorylist[$i]["categoryidx"];
		$select_top_category = $categorylist[$i]["top_category"];
		$select_category = $categorylist[$i]["category"];
		
		if ($select_categoryidx == $search_categoryidx)
			$selected = "selected";
		else
			$selected = "";
?>
						<option value="<?= $select_categoryidx ?>" <?= $selected ?>><?= $select_top_category ?>/<?= $select_category ?></option>
<?
	}
?>                    	
					</select>                    
	                     
                    <div class="clear" style="padding-top:10px"></div>
                    
                    <span class="search_lbl">문의내용</span>
                    <input type="text" class="search_text" id="contents" name="contents" style="width:170px" value="<?= encode_html_attribute($search_contents) ?>" onkeypress="search_press(event)" />
                    
                    <span class="search_lbl ml20">답변내용</span>
                    <input type="text" class="search_text" id="answer" name="answer" style="width:170px" value="<?= encode_html_attribute($search_answer) ?>" onkeypress="search_press(event)" />
                    
                    <span class="search_lbl ml20">처리결과</span>
                    <input type="radio" name="response_status" value="" <?= ($search_response_status == "") ? "checked" : "" ?>>전체<input type="radio" name="response_status" value="1" <?= ($search_response_status == "1") ? "checked" : "" ?>>승인 <input type="radio" name="response_status" value="2" <?= ($search_response_status == "2") ? "checked" : "" ?>>거부 <input type="radio" name="response_status" value="3" <?= ($search_response_status == "3") ? "checked" : "" ?>>유예
					
		            <div class="clear" style="padding-top:10px"></div>

                </div>
            </form>
                        
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="5">
                <col width="">
                <col width="230">
                <col width="80">
                <col width="120">
                <col width="120">
            </colgroup>
            <thead>
            <tr>
                <th></th>
                <th class="tdl">클레임 내용</th>
                <th>이름</th>
                <th>처리 여부</th>
                <th>처리일</th>
                <th>작성일</th>
            </tr>
            </thead>
            <tbody>
<?
	$db_main = new CDatabase_Main();
	$db_friend = new CDatabase_Friend();

    for ($i=0; $i<sizeof($crlist); $i++)
    {
    	$reviewidx = $crlist[$i]["reviewidx"];
        $qaidx = $crlist[$i]["qaidx"];
        $contents = $crlist[$i]["contents"];
        $top_category = $crlist[$i]["top_category"];
        $category = $crlist[$i]["category"];
        $name = $crlist[$i]["name"];
        $useridx = $crlist[$i]["useridx"];
        $facebookid = $crlist[$i]["facebookid"];
        $status = $crlist[$i]["status"];        
        $responsedate = $crlist[$i]["responsedate"];
        $writedate = $crlist[$i]["writedate"];
        $response_status = $crlist[$i]["response_status"];        
		
        if($facebookid < 10)
        	$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$facebookid.".png";
        else
            $photourl = get_fb_pictureURL($facebookid,$client_accesstoken);
        
		if ($category != "")
			$top_category = $top_category." / ".$category;
		
		$sql = "SELECT COUNT(*) FROM tbl_user_online_sync2 WHERE useridx=$useridx";
		$online_status = $db_friend->getvalue($sql);
		
?>
            <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="claim_review_detail(<?= $reviewidx ?>)">
                <td class="tdc"></td>
                <td class="point_title"><?= encode_xml_field($contents) ?></td>
                <td class="point_title"><span style="float:left;padding-top:8px;padding-right:2px;"><img src="/images/icon/<?= ($online_status == "1") ? "status_1.png" : "status_3.png" ?>" align="absmiddle" /></span>  <span style="float:left;"><img src="<?= $photourl ?>" height="25" width="25" align="absmiddle" hspace="3"></span> <?= $name ?>&nbsp;</td>
                <td class="tdc"><?= ($response_status == "0") ? "<b style='color:red'>미처리</b>" : "<b style='color:gray'>처리 완료</b>" ?></td>
                <td class="tdc"><?= ($status == "1") ? $responsedate : "" ?></td>
                <td class="tdc"><?= $writedate ?></td>
            </tr>
<?
    } 

	$db_main->end();
	$db_friend->end();
?>
            </tbody>
            </table>
            
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>