<?
    $top_menu = "customer";
    $sub_menu = "faq";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_title = $_GET["title"];
    $search_category = $_GET["category"];
    
    $listcount = 10;
    $pagename = "faq.php";
    $pagefield = "title=$search_title&category=$search_category";
    
    $db_analysis = new CDatabase_Analysis();
    
    $tail = "WHERE 1=1";
    
    if($search_title != "")
    	$tail .= " AND title LIKE '%$search_title%'";
    
    if($search_category != "")
    	$tail .= " AND categoryidx='$search_category'";
        
    $totalcount = $db_analysis->getvalue("SELECT COUNT(*) FROM support_faq $tail");
    
    $sql = "SELECT faqidx,top_categoryidx, categoryidx,title,status,faq_sort,writedate,".
    	"(SELECT category FROM support_category WHERE categoryidx=support_faq.top_categoryidx) AS top_category,".
    	"(SELECT category FROM support_category WHERE categoryidx=support_faq.categoryidx) AS category ".
    	" FROM support_faq $tail ORDER BY faq_sort LIMIT ".(($page-1) * $listcount).", ".$listcount;    
    
    $faqlist = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT categoryidx,top_categoryidx,category,".
    		"(SELECT category FROM support_category WHERE categoryidx=A.top_categoryidx) AS top_category ".
    		" FROM support_category A WHERE top_categoryidx<>0 ORDER BY (SELECT seqno FROM support_category WHERE categoryidx=A.top_categoryidx),seqno";
    
    $categorylist = $db_analysis->gettotallist($sql);
    
    $db_analysis->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script type="text/javascript">
    function faq_write(faqidx)
    {
        window.location.href = "faq_write.php?faqidx="+faqidx;
    }

    function move_faq(faqidx, movetype)
    {
        var param = {};
        param.faqidx = faqidx;
        param.movetype = movetype;
            
        WG_ajax_execute("customer/move_faq", param, move_faq_callback);	        
    }
    
    function move_faq_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            window.location.reload(false);
        }
    }

    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    } 
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; FAQ 관리 <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
	</div>
	<!-- //title_warp -->
		
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
		<div class="detail_search_wrap">
			<span class="search_lbl ml20">제목</span>
			<input type="text" class="search_text" id="title" name="title" style="width:200px" value="<?= encode_html_attribute($search_title) ?>" onkeypress="search_press(event)" />
            	
			<span class="search_lbl ml20">분류</span>
				<select name="category" id="category">
					<option value="">분류 선택</option>
<?
	for ($i=0; $i<sizeof($categorylist); $i++)
	{
		$select_top_categoryidx = $categorylist[$i]["top_categoryidx"];
		$select_categoryidx = $categorylist[$i]["categoryidx"];
		$select_top_category = $categorylist[$i]["top_category"];
		$select_category = $categorylist[$i]["category"];
		
		if ($select_categoryidx == $search_category)
			$selected = "selected";
		else
			$selected = "";
?>
					<option value="<?= $select_categoryidx ?>" top_categoryidx="<?= $select_top_categoryidx ?>" <?= $selected ?>><?= $select_top_category ?>/<?= $select_category ?></option>
<?
	}
?>
				</select>   
            	
				<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
			</div>
	</form>
	
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="5">
			<col width="">
			<col width="300">
			<col width="80">
			<col width="80">
			<col width="120">
		</colgroup>
		<thead>
			<tr>
				<th></th>
				<th class="tdl">제목</th>
                <th class="tdl">분류</th>
                <th>사용여부</th>
                <th>우선순위</th>
                <th>작성일</th>
            </tr>
		</thead>
		<tbody>
<?
    for ($i=0; $i<sizeof($faqlist); $i++)
    {
        $faqidx = $faqlist[$i]["faqidx"];
        $title = $faqlist[$i]["title"];
        $top_category = $faqlist[$i]["top_category"];
        $category = $faqlist[$i]["category"];
        $status = $faqlist[$i]["status"];
        $faq_sort = $faqlist[$i]["faq_sort"];
        $writedate = $faqlist[$i]["writedate"];
?>
            <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="faq_write(<?= $faqidx ?>)">
                <td class="tdc"></td>
                <td class="point_title"><?= $title ?></td>
                <td class="point_title"><?= $top_category."/".$category ?></td>
                <td class="tdc point"><?= ($status == "1") ? "사용" : "미사용" ?></td>
                <td class="tdc point">
                    <img src="/images/icon/up.png" align="absmiddle" onclick="event.cancelBubble=true;move_faq('<?= $faqidx ?>','up')" />
                    <img src="/images/icon/down.png" align="absmiddle" onclick="event.cancelBubble=true;move_faq('<?= $faqidx ?>','down')" />
                </td>
                <td class="tdc"><?= $writedate ?></td>
            </tr>
<?
    } 
?>
		</tbody>
	</table>
            
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="FAQ 추가" onclick="window.location.href='faq_write.php'">          
	</div>
</div>
<!--  //CONTENTS WRAP -->    
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>