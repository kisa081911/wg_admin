<?
    $top_menu = "customer";
    $sub_menu = "claim_review";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    // ckeditor 경로 설정
    include_once($_SERVER["DOCUMENT_ROOT"]."/ckeditor/ckeditor.php");
    
    $reviewidx = $_GET["reviewidx"];
    $useridx = $_GET["useridx"];
    $pagefield = $_GET["pagefield"];
	
    check_number($reviewidx);
    check_number($useridx);
    
	if ($reviewidx == "" && $useridx == "")
		error_back("잘못된 접근입니다.");
	
    $db_analysis = new CDatabase_Analysis();
    $db_main = new CDatabase_Main();
	
	if ($reviewidx != "")
	{	 
	    $sql = "SELECT reviewidx, qaidx, top_categoryidx, categoryidx, useridx, facebookid, name, contents, status, responsedate, occurdate, writedate, response_status, freecoin_status FROM claim_review WHERE reviewidx = $reviewidx";
	    $review = $db_analysis->getarray($sql);
	    
	    $reviewidx = $review["reviewidx"];
	    $qaidx = $review["qaidx"];
	    $top_categoryidx = $review["top_categoryidx"];
	    $categoryidx = $review["categoryidx"];
	    $useridx = $review["useridx"];
	    $facebookid = $review["facebookid"];
	    $name = $review["name"];
	    $contents = $review["contents"];
	    $status = $review["status"];	    
	    $responsedate = $review["responsedate"];
	    $occurdate = $review["occurdate"];
	    $writedate = $review["writedate"];	    
	    $response_status = $review["response_status"];
	    $freecoin_status = $review["freecoin_status"];

	    if($facebookid < 10)
	    	$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$facebookid.".png";
	    else
	        $photourl = get_fb_pictureURL($facebookid,$client_accesstoken);

	    if ($reviewidx == "")
    	    error_back("존재하지 않는 클레임 리뷰입니다.");
	}	
    
	$sql = "SELECT categoryidx,category FROM support_category WHERE top_categoryidx=0 ORDER BY seqno";
	
	$top_categorylist = $db_analysis->gettotallist($sql);
    
	$sql = "SELECT categoryidx,top_categoryidx,category,".
		"(SELECT category FROM support_category WHERE categoryidx=A.top_categoryidx) AS top_category ".
		" FROM support_category A WHERE top_categoryidx<>0 ORDER BY (SELECT seqno FROM support_category WHERE categoryidx=A.top_categoryidx),seqno";
	
	$categorylist = $db_analysis->gettotallist($sql);
	
	$sql = "SELECT coin, sex FROM tbl_user WHERE useridx=$useridx";
	$data = $db_main->getarray($sql);
					
	$coin = $data["coin"];
	$sex = $data["sex"];

	$sql = "SELECT IFNULL(SUM(facebookcredit),0) AS totalcredit, IFNULL(SUM(coin),0) AS totalcoin FROM tbl_product_order WHERE status=1 AND useridx=$useridx";
	$coin_info = $db_main->getarray($sql);
	
	$totalcredit = $coin_info["totalcredit"];
	$totalcoin = $coin_info["totalcoin"];
		
	$sql = "SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE status=1 AND useridx=$useridx AND writedate>DATE_ADD(NOW(),INTERVAL -30 DAY)";
	$recentcredit_month = $db_main->getvalue($sql);
	
	$sql = "SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE status=1 AND useridx=$useridx AND writedate>DATE_ADD(NOW(),INTERVAL -7 DAY)";
	$recentcredit_week = $db_main->getvalue($sql);
	
	$sql = "SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE status=2 AND useridx=$useridx AND writedate>DATE_ADD(NOW(),INTERVAL -90 DAY)";
	$recentcancel_3month = $db_main->getvalue($sql);
	
	$sql = "SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE status=2 AND useridx=$useridx AND writedate>DATE_ADD(NOW(),INTERVAL -30 DAY)";
	$recentcancel_month = $db_main->getvalue($sql);
	
	$sql = "SELECT COUNT(*) FROM tbl_product_order WHERE status=2 AND useridx=$useridx";
	$totalcancel_number = $db_main->getvalue($sql);	
	
	$sql = "SELECT IFNULL(SUM(freecoin),0) FROM tbl_freecoin_admin_log WHERE useridx=$useridx";
	$totalsentcoin = $db_main->getvalue($sql);

	$sql = "SELECT IFNULL(SUM(freecoin),0) FROM tbl_freecoin_admin_log WHERE useridx=$useridx AND writedate>DATE_ADD(NOW(),INTERVAL -30 DAY)";
	$sentcoin_month = $db_main->getvalue($sql);
	
	$sql = "SELECT COUNT(*) FROM claim_review WHERE useridx = $useridx";
	$totalclaim = $db_analysis->getvalue($sql);
	
	$sql = "SELECT COUNT(*) FROM claim_review WHERE useridx = $useridx AND writedate>DATE_ADD(NOW(),INTERVAL -30 DAY)";
	$claim_month = $db_analysis->getvalue($sql);
		
	$sql = "SELECT reviewidx, qaidx, contents, writedate, status, occurdate FROM claim_review WHERE useridx=$useridx ORDER BY reviewidx DESC";
	$crlist1 = $db_analysis->gettotallist($sql);

	$sql = "SELECT status,facebookcredit,writedate FROM tbl_product_order WHERE useridx=$useridx AND status IN (1,2) ORDER BY orderidx DESC LIMIT 7";
	$orderlist = $db_main->gettotallist($sql);
	
?>
<style>
	div td {word-break:break-all}
</style>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<script>
	var g_savetype = "";
	
     function setCookie(cName, cValue, cDay)
     {
          var expire = new Date();
          expire.setDate(expire.getDate() + cDay);
          cookies = cName + '=' + escape(cValue) + '; path=/ ';
          if(typeof cDay != 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
          document.cookie = cookies;
     }

     function getCookie(cName) 
     {
          cName = cName + '=';
          var cookieData = document.cookie;
          var start = cookieData.indexOf(cName);
          var cValue = '';
          if(start != -1){
               start += cName.length;
               var end = cookieData.indexOf(';', start);
               if(end == -1)end = cookieData.length;
               cValue = cookieData.substring(start, end);
          }
          return unescape(cValue);
     }	
     
	function window_onload()
	{
		return;
		
		var contents = getCookie("answer_contents");
		
		if (contents != "" && contents != null)	
			CKEDITOR.instances.contents.setData(contents);
			
		setInterval("save_contents()", 3000);
	}
	
	function save_contents()
	{
		var contents = CKEDITOR.instances.contents.getData();
		
		if (contents != "")
		{
			setCookie("answer_contents", contents, 1);
		}
	}

	function save_cr_history()
	{
		var input_form = document.input_form;

		var memo = document.getElementById("memo").value;
		var response_status = get_radio("response_status");
		var freecoin = document.input_form.freecoin.value;
		var freecoin_status = document.input_form.freecoin_status.value;
		var freecoin_reason = document.input_form.reason.value;
		var freecoin_message = document.input_form.message.value;

		if( memo == "" )
		{
			alert("리뷰자 메모를 입력하세요.");
			document.getElementById("memo").focus();
			return;
		}
		
		 var param = new HashMap();
		 	param.put("reviewidx", "<?= $reviewidx?>");
		 	param.put("useridx", "<?= $useridx ?>");   
	        param.put("response_status", response_status);
	        param.put("contents", "<?= encode_script_value($contents)?>");
	        param.put("freecoin", strip_price_format(freecoin));
	        param.put("freecoin_status", freecoin_status);
	        param.put("freecoin_reason", freecoin_reason);
	        param.put("freecoin_message", freecoin_message);
	        param.put("review_message", memo);	        

		SendExecuteRequest("customer/save_cr_history", param, save_cr_history_callback);
	}

	function save_cr_history_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {      
            alert("해당 클레임 리뷰가 처리 되었습니다.");
            window.location.href = "claim_review_detail.php?pagefield=<?= $pagefield?>&reviewidx=<?= $reviewidx?>";
        }
    }

	function search_gamelog()
	{
		var input_form = document.input_form;

		var search_gamelog_status = get_radio("search_gamelog_status");
		var start_gamelog_date = document.input_form.start_gamelog_date.value;
		var start_gamelog_hour = document.input_form.start_gamelog_hour.value;
		var start_gamelog_min = document.input_form.start_gamelog_min.value;
		var end_gamelog_date = document.input_form.end_gamelog_date.value;
		var end_gamelog_hour = document.input_form.end_gamelog_hour.value;
		var end_gamelog_min = document.input_form.end_gamelog_min.value;

		var start_gamelog_time = start_gamelog_date + " " + start_gamelog_hour + ":" + start_gamelog_min + ":00";
		var end_gamelog_time = end_gamelog_date + " " + end_gamelog_hour + ":" + end_gamelog_min + ":00";
		
		var gamelog = document.getElementById("gamelog");		
		var pagefield = "claim_review_gamelog.php?useridx=<?= $useridx?>&gamelog_status=" + search_gamelog_status + "&start_writedate=" + start_gamelog_time + "&end_writedate=" + end_gamelog_time;
		
		gamelog.innerHTML = "<iframe src='" + pagefield + "' style='width:900px; height:300px;'></iframe>";
	}

	function save_qa_answer()
	{
		var input_form = document.input_form		
		
		var param = new HashMap();
		param.put("useridx", "<?= $useridx?>");
		param.put("qaidx", "<?= $qaidx ?>");
		param.put("top_categoryidx", input_form.category.options[input_form.category.selectedIndex].getAttribute("top_categoryidx"));
    param.put("categoryidx", input_form.category.options[input_form.category.selectedIndex].value);
    param.put("answer", CKEDITOR.instances.contents.getData());
		param.put("qa_memo", input_form.qa_memo.value);
		param.put("qa_admincategory", get_radio("qa_admincategory"));
		param.put("qa_level", input_form.qa_level.value);
		param.put("qa_freecoin", input_form.qa_freecoin.value);
		param.put("qa_reason", input_form.qa_reason.value);
		param.put("qa_message", input_form.qa_message.value);
		param.put("free_type",input_form.free_type.value);
		
		SendExecuteRequest("customer/save_qa_answer", param, save_qa_answer_callback);
				
	}

	function save_qa_answer_callback(result, reason)
	{
		if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
        //	setCookie("answer_contents", "", -1);
        	alert("QA 답변 등록했습니다.");
        	
	        window.location.href = "claim_review.php?<?= $pagefield ?>";
	        
        }
	}   
    
    function delete_answer(answeridx)
    {
        if (!confirm("해당 답변을 삭제 하시겠습니까?"))
            return;

        var param = new HashMap();
        param.put("qaidx", "<?= $qaidx ?>");
        param.put("answeridx", answeridx);

        SendExecuteRequest("customer/delete_answer", param, delete_answer_callback);
    }

    function delete_answer_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
        //	setCookie("answer_contents", "", -1);
            window.location.reload(false);
        }
    }

    function update_answer(answeridx)
    {
        if (!confirm("해당 답변 등급/피드백을 저장 하시겠습니까?"))
            return;

        var param = new HashMap();
        param.put("qaidx", "<?= $qaidx ?>");
        param.put("answeridx", answeridx);
        param.put("level", document.getElementById('level_' + answeridx).value);
        param.put("feedback", document.getElementById('feedback_' + answeridx).value);

        SendExecuteRequest("customer/update_answer", param, update_answer_callback);
    }

    function update_answer_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
        //	setCookie("answer_contents", "", -1);
            window.location.reload(false);
        }
    }
    
    function template_changed()
    {
    	var contents = input_form.template.options[input_form.template.selectedIndex].value;
    	
    	if (contents == "")
    		return; 
    	
    	contents = ReplaceText(contents, "\\[NAME\\]", "<?= $name ?>");
    	
    	CKEDITOR.instances.contents.setData(contents);
    }
    
    var g_template_list = new Array();
    
    function category_changed(change_flag)
    {
        var change_flag = change_flag;
        if(change_flag != 1){
        	document.input_form.template_search.value = "";
        }
        
        var search = document.input_form.template_search.value;
        if(search == "" && change_flag != ""){
			
        	var option = document.input_form.category.options[document.input_form.category.selectedIndex];
        	var categoryidx;
        	
        	if (option.value == "")
        		categoryidx = "";
        	else if (option.value == "0")
        		categoryidx = option.getAttribute("top_categoryidx");
        	else
        		categoryidx = option.value;
        	
        	var template = document.input_form.template;
        	
        	for (var i=0; i<g_template_list.length; i++)
        	{
        		template.options[template.options.length] = g_template_list[i];
        	}
        	
        	g_template_list = new Array();
        	
        	for (var i=template.options.length-1; i>=1; i--)
        	{
        		if (categoryidx != "" && template.options[i].getAttribute("top_categoryidx") != categoryidx && template.options[i].getAttribute("categoryidx") != categoryidx && template.options[i].getAttribute("top_categoryidx") != "0" && template.options[i].getAttribute("categoryidx") != "0")
        		{
        			g_template_list.push(template.options[i]);
        			template.options[i] = null;
        		}
        	}

        	
        	
        } else {
        	        	
        	var option = document.input_form.category.options[document.input_form.category.selectedIndex];
        	var categoryidx;
        	
        	if (option.value == "")
        		categoryidx = "";
        	else if (option.value == "0")
        		categoryidx = option.getAttribute("top_categoryidx");
        	else
        		categoryidx = option.value;
        	
        	var template = document.input_form.template;
        	
        	for (var i=0; i<g_template_list.length; i++)
        	{	
        			template.options[template.options.length] = g_template_list[i];
        	}
        	
        	g_template_list = new Array();
        	
        	for (var i=template.options.length-1; i>=1; i--)
        	{
        		if (categoryidx != "" && template.options[i].getAttribute("top_categoryidx") != categoryidx && template.options[i].getAttribute("categoryidx") != categoryidx && template.options[i].getAttribute("top_categoryidx") != "0" && template.options[i].getAttribute("categoryidx") != "0")
        		{
        			g_template_list.push(template.options[i]);
        			template.options[i] = null;
        		} else {
            		template.options[i].text = template.options[i].text.toLowerCase();
            		search = search.toLowerCase();
        			if(template.options[i].text.indexOf(search, 0) == -1){        				
        				g_template_list.push(template.options[i]);
                		template.options[i] = null;
            		} 
        		}
        	}

        	if(template.options.length < 2){
				alert("검색 결과 없음");
        	} 
        	
        }

    }

    $(function() {
        $("#start_gamelog_date").datepicker({ });
    });

    $(function() {
        $("#end_gamelog_date").datepicker({ });
    });

    </script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>

<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 클레임 상세</div>
            	<!-- <div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_qa()"></div>  -->
            </div>
            <!-- //title_warp -->

            <form name="input_form" id="input_form" onsubmit="return false">

			<div style="float:left;width:600px">
				
	    	<div class="h2_title">사용자정보</div>
	    	<div class="user_info_summary" onmouseover="className='user_info_summary_over'" onmouseout="className='user_info_summary'" onclick="window.open('/m2_user/user_view.php?useridx=<?= $useridx ?>')">
    			<img src="<?= $photourl ?>" height="50" width="50" class="summary_user_image">
    			<div class="sumary_username_wrap">
	    			<div class="summary_username">[<?= $useridx ?>] <?= $name ?> <?= ($sex == "1") ? "(M)" : "(F)" ?></div>
    			</div>
    			<div class="summary_user_coin"><?= make_price_format($coin) ?></div>
	    	</div>

			<div>
			<div style="float:left;width:295px">
	    	<div class="h2_title">결제/취소/지급/클레임 정보</div>
	    	<div class="product_info_summary">
	    		<div class="summary_item" style="padding-top:3px">
	    			<div class="summary_item_lbl">총결제금액</div>
	    			<div class="summary_item_value"><?= make_price_format($totalcredit) ?> credit</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">총구매코인</div>
	    			<div class="summary_item_value"><?= make_price_format($totalcoin) ?> coins</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">30일 내 결제금액</div>
	    			<div class="summary_item_value"><?= make_price_format($recentcredit_month) ?> credit</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">7일 내 결제금액</div>
	    			<div class="summary_item_value"><?= make_price_format($recentcredit_week) ?> credit</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">90일 내 취소금액</div>
	    			<div class="summary_item_value"><?= make_price_format($recentcancel_3month) ?> credit</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">30일 내 취소금액</div>
	    			<div class="summary_item_value"><?= make_price_format($recentcancel_month) ?> credit</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">총 결제 취소횟수</div>
	    			<div class="summary_item_value"><?= $totalcancel_number?>회</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">총 지급금액</div>
	    			<div class="summary_item_value"><?= make_price_format($totalsentcoin) ?> coins</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">30일 내 지급금액</div>
	    			<div class="summary_item_value"><?= make_price_format($sentcoin_month) ?> coins</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">총 클레임 횟수</div>
	    			<div class="summary_item_value"><?= $totalclaim?>회</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">30일 내 클레임 횟수</div>
	    			<div class="summary_item_value"><?= $claim_month?>회</div>
	    		</div>
	    	</div>
			</div>
			
			<div style="float:right;width:295px">
	    	<div class="h2_title">최근 결제 내역</div>
		    	<div class="product_info_summary">
	<?
		for ($i=0; $i<sizeof($orderlist); $i++)
		{
			$order_status = $orderlist[$i]["status"];
			$order_facebookcredit = $orderlist[$i]["facebookcredit"];
			$order_writedate = $orderlist[$i]["writedate"];
	?>	    		
		    		<div class="summary_item" style="<?= ($i == 0) ? "padding-top:3px" : "" ?>">
		    			<div class="summary_item_lbl" <?= ($order_status == "2") ? "style='color:red'" : "" ?>><?= $order_writedate ?></div>
		    			<div class="summary_item_value" <?= ($order_status == "2") ? "style='color:red'" : "" ?>><?= make_price_format($order_facebookcredit) ?> credit</div>
		    		</div>
	<?
		}
	?>	    		
		    	</div>
			</div>
						
			</div>

	    	</div>
	    	
	    	<div style="float:right;vertical-align:top;width:470px">
	    		<div class="h2_title">기존 클레임 내역</div>
	
		    	<div class="product_info_summary" style="min-height:400px;height:400px;overflow-y:scroll;cursor:default">
<?
	for ($i=0; $i<sizeof($crlist1); $i++)
	{
		$prev_reviewidx = $crlist1[$i]["reviewidx"];
		$prev_qaidx = $crlist1[$i]["qaidx"];
		$contents = $crlist1[$i]["contents"];
		$writedate = $crlist1[$i]["writedate"];
		$status = $crlist1[$i]["status"];
		//$occurdate = $crlist1[$i]["occurdate"];
				
?>	
		    		<div class="summary_item" <?= ($i == 0) ? "style='border-bottom:1px dashed #cdcdcd;padding-top:3px;padding-bottom:7px;min-height:70px'" : "style='border-bottom:1px dashed #cdcdcd;padding-bottom:7px;min-height:70px'" ?>>
		    			<div class="summary_item_lbl"><?= $writedate ?><br>
		    				<input type="button" class="btn_01" value="해당 클레임 이동" onclick="window.location.href='claim_review_detail.php?reviewidx=<?= $prev_reviewidx ?>&<?= $pagefield ?>'" title="해당 문의로 이동">	    				
		    			</div>
		    			<div class="summary_item_value" <?= ($prev_reviewidx == $reviewidx) ? "style='color:#CC4455;word-break:break-all;font-weight:bold'" : "style='word-break:break-all'" ?>><?= ($status == "0") ? "<b style='color:#FF3344;'>[미처리]</b>" : "" ?><?= str_replace("\n", "<br>", encode_xml_field($contents)) ?></div>
		    		</div>
<?			
	}
?>		    		
		    	</div>
		    </div>
	    	
	    	<div class="clear"></div>
	    	
            <table class="tbl_view_basic">
            <colgroup>
                <col width="170">
                <col width="">
            </colgroup>
            <tbody>
            	<tr>
            		<th>Web / Mobile</th>
            		<td>
            			<input type="radio" name="search_gamelog_status" value="1" <?= ($search_gamelog_status == "1" || $search_gamelog_status == "") ? "checked" : ""?>>Web
            		</td>
            	</tr>
            	<tr>
            		<th>조회 기간 설정</th>
            		<td>
                            <input type="input" class="search_text" id="start_gamelog_date" name="start_gamelog_date" style="width:65px" value="<?= $reservation_date ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
                            
                            <select id="start_gamelog_hour" name="start_gamelog_hour">
                                <option value="">선택</option>
<?
    for ($i=0;$i<24;$i++)
    {
?>
                                <option value="<?= $i ?>" <?= ($i == $reservation_hour) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
                            </select>시&nbsp;
                            <select id="start_gamelog_min" name="start_gamelog_min">
                                <option value="">선택</option>
<?
    for ($i=0;$i<60;$i++)
    {
?>
                                <option value="<?= $i ?>" <?= ($i == $reservation_minute) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
                            </select>분&nbsp;&nbsp;-&nbsp;&nbsp;
                            
                            <input type="input" class="search_text" id="end_gamelog_date" name="end_gamelog_date" style="width:65px" value="<?= $reservation_date ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
                            <select id="end_gamelog_hour" name="end_gamelog_hour">
                                <option value="">선택</option>
<?
    for ($i=0;$i<24;$i++)
    {
?>
                                <option value="<?= $i ?>" <?= ($i == $reservation_hour) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
                            </select>시&nbsp;
                            <select id="end_gamelog_min" name="end_gamelog_min">
                                <option value="">선택</option>
<?
    for ($i=0;$i<60;$i++)
    {
?>
                                <option value="<?= $i ?>" <?= ($i == $reservation_minute) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
                            </select>분
                            <input type="button" class="btn_setting_01" value="검색" style="height: 28px; float: right;" onclick="search_gamelog()">
                        </td>
            	</tr>
            	
            	<tr>
            		<th>발생 시각</th>
            		<td><?= $occurdate?></td>
            	</tr>
            	
            	<tr>
            		<th>플레이 내역</th>
            		<td>
            			<div id="gamelog" name="gamelog" style="height:300px">
            				<textarea style="height:300px"></textarea>
            			</div>
            		</td>
            	</tr>
            	
            	<tr>
            		<th>승인 여부</th>
            		<td>
            			<div>
	            			<input type="radio" name="response_status" value="1" <?= ($response_status == "1") ? "checked" : ""?>>승인 <input type="radio" name="response_status" value="2" <?= ($response_status == "2") ? "checked" : ""?>>거부 <input type="radio" name="response_status" value="3" <?= ($response_status == "3") ? "checked" : ""?>>유예
	            			<input type="button" class="btn_setting_02" name="" id="" value="취소" style="height: 28px; float:right;" onclick="go_page('claim_review.php?<?= $pagefield ?>')"/><input type="button" class="btn_setting_01" name="" id="" value="리뷰 등록/발급" style="height:28px; float:right;" onclick="save_cr_history()"/>
            			</div>            			
            		</td>
            	</tr>
            	
            	<tr>
                    <th>리뷰자 메모</th>
                    <td>
                        <textarea id="memo" name="memo" style="height:50px"></textarea>
                    </td>
                </tr>
                
                 <tr>
                     <th>발급 금액</th>
                     <td><input type="text" class="view_tbl_text" style="width:250px" name="freecoin" id="freecoin" value="" onkeydown="return keep_price_format(this)" onkeyup="return keep_price_format(this)" onblur="return keep_price_format(this)" onkeypress="return checknum()"/> 
                     <select name="freecoin_status">
                     	<option value="1">보상</option>
                     	<option value="3">기타</option>
                     </select><br>
                     </td>
                 </tr>
                 <tr>
                     <th>발급  사유</th>
                     <td><input type="text" class="view_tbl_text" style="width:420px" name="reason" id="reason" maxlength="200" value="" /></td>
                 </tr>
                 <tr>
                    <th>발급 메시지</th>
                    <td><input type="text" class="view_tbl_text" style="width:420px" name="message" id="message" maxlength="200" value="" /></td>
                 </tr>
                
                <tr>
                	<th>분류</th>
                	<td>
                		<select name="category" onchange="category_changed()">
                    	<option value="">분류선택</option>
<?
	for ($i=0; $i<sizeof($top_categorylist); $i++)
	{
		$select_top_categoryidx = $top_categorylist[$i]["categoryidx"];
		$select_top_category = $top_categorylist[$i]["category"];
		
		if ($select_top_categoryidx == $top_categoryidx && $categoryidx == "0")
			$selected = "selected";
		else
			$selected = "";
?>
						<option value="0" top_categoryidx="<?= $select_top_categoryidx ?>" <?= $selected ?>><?= $select_top_category ?></option>
<?
	}

	for ($i=0; $i<sizeof($categorylist); $i++)
	{
		$select_top_categoryidx = $categorylist[$i]["top_categoryidx"];
		$select_categoryidx = $categorylist[$i]["categoryidx"];
		$select_top_category = $categorylist[$i]["top_category"];
		$select_category = $categorylist[$i]["category"];
		
		if ($select_categoryidx == $categoryidx)
			$selected = "selected";
		else
			$selected = "";
?>
						<option value="<?= $select_categoryidx ?>" top_categoryidx="<?= $select_top_categoryidx ?>" <?= $selected ?>><?= $select_top_category ?>/<?= $select_category ?></option>
<?
	}
?>                    	
                    </select>
                	</td>
                </tr>
                 
            	<tr>
            		<th>답변템플릿 검색</th>
            		<td>
            			<input type="text" class="view_tbl_text" name="template_search" id="template_search" value="" onkeydown="javascript:if(event.keyCode == 13){ category_changed(1) }" style="width:150px" />
            			<input type="button" class="btn_setting_01" name="" id="" value="검색" style="height:28px;" onclick="category_changed(1)"/>
            		</td>
            	</tr>
                <tr>
                    <th>답변템플릿</th>
                    <td>
                        <select name="template" id="template" onchange="template_changed()">
                        	<option value="">답변 템플릿 선택</option>
<?
	$sql = "SELECT top_categoryidx,categoryidx,title,contents FROM support_qa_template ORDER BY title ASC";
	$list = $db_analysis->gettotallist($sql);
	
	for ($i=0; $i<sizeof($list); $i++)
	{
		$template_top_categoryidx = $list[$i]["top_categoryidx"];
		$template_categoryidx = $list[$i]["categoryidx"];
		$template_title = $list[$i]["title"];
		$template_contents = $list[$i]["contents"];
?>		
							<option value="<?= encode_html_attribute($template_contents) ?>" top_categoryidx="<?= $template_top_categoryidx ?>" categoryidx="<?= $template_categoryidx ?>"><?= $template_title ?></option>
<?	
	}
?>                        	
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><span>*</span> 답변내용</th>
                    <td>
                        <textarea id="contents" name="contents"></textarea>
                        <script type="text/javascript">CKEDITOR.replace("contents",{width: '100%',height: '250px'});</script>
                    </td>
                </tr>
                <tr>
                    <th><span></span> 내부메모</th>
                    <td>
                        <textarea id="qa_memo" name="qa_memo" style="height:50px"></textarea>
                    </td>
                </tr>
                <tr>
                    <th><span>*</span> 답변자</th>
                    <td>
                        <input type="radio" class="radio" name="qa_admincategory" value="1"> Jessie Moore
                        
                        / 난이도 설정
                        <select name="qa_level">
                        	<option value="0">초급</option>
                        	<option value="1">중급</option>
                        	<option value="2">고급</option>
                        </select>
                    </td>
                </tr>
                <tr>
      					<th>보상 자원</th>
					<td>
						<select id="free_type" name="free_type" onchange="change_freetype(this.value);">
							<option value="">선택</option>
								<option value="1" <?= ($reward_type=="1") ? "selected" : "" ?>>Coin</option>							
						</select>
					</td>
				</tr>
                 <tr>
                     <th>보상 금액</th>
                     <td><input type="text" class="view_tbl_text" style="width:250px" name="qa_freecoin" id="qa_freecoin" value="" onkeydown="return keep_price_format(this)" onkeyup="return keep_price_format(this)" onblur="return keep_price_format(this)" onkeypress="return checknum()"/><br>
                     </td>
                 </tr>
                 <tr>
                     <th>보상  사유</th>
                     <td><input type="text" class="view_tbl_text" style="width:420px" name="qa_reason" id="qa_reason" maxlength="200" value="" /></td>
                 </tr>
                 <tr>
                    <th>보내는 메시지</th>
                    <td><input type="text" class="view_tbl_text" style="width:420px" name="qa_message" id="qa_message" maxlength="200" value="" /></td>
                 </tr>
                
            </tbody>
            </table>
            </form> 
            
            <div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="저장" onclick="save_qa_answer()">
                <input type="button" class="btn_setting_02" value="취소" onclick="go_page('claim_review.php?<?= $pagefield ?>')">           
            </div>
            
<?
	$finalansweridx = "0";
	
	$sql = "SELECT answeridx,adminid,answer,memo,writedate,coin,coin_memo,coin_message,level,feedback FROM support_qa_answer WHERE qaidx=$qaidx ORDER BY answeridx";
	$list = $db_analysis->gettotallist($sql);
	
	for ($i=0; $i<sizeof($list); $i++)
	{
		$answeridx = $list[$i]["answeridx"];
		$adminid = $list[$i]["adminid"];
		$answer = $list[$i]["answer"];
		$memo = $list[$i]["memo"];
		$writedate = $list[$i]["writedate"];
		$coin = $list[$i]["coin"];
		$coin_memo = $list[$i]["coin_memo"];
		$coin_message = $list[$i]["coin_message"];
		$level = $list[$i]["level"];
		$feedback = $list[$i]["feedback"];
		
		$finalansweridx = $answeridx;
?>
			<table class="tbl_view_basic" style="border-top:1px dashed #cdcdcd;border-bottom:1px dashed #cdcdcd;margin-top:10px">
            <colgroup>
                <col width="150">
                <col width="">
            </colgroup>
            <tbody>
                <tr>
                    <th>답변자</th>
                    <td>Tina Scott / <?= $adminid ?> <input type="button" class="btn_05" value="삭제" onclick="delete_answer('<?= $answeridx ?>')"></td>
                </tr>
                <tr>
                    <th>난이도/피드백</th>
                    <td>
						난이도 조정
                        <select name="level_<?= $answeridx ?>" id="level_<?= $answeridx ?>">
                        	<option value="0" <?= ($level == '0') ? "selected" : "" ?>>초급</option>
                        	<option value="1" <?= ($level == '1') ? "selected" : "" ?>>중급</option>
                        	<option value="2" <?= ($level == '2') ? "selected" : "" ?>>고급</option>
                        </select>
                        / 피드백
                        <input type="text" class="view_tbl_text" style="width:400px" name="feedback_<?= $answeridx ?>" id="feedback_<?= $answeridx ?>" maxlength="400" value="<?= encode_html_attribute($feedback) ?>" />
<?
	if ($login_adminid == "admin")
	{
?>		
                        <input type="button" class="btn_05" value="저장" onclick="update_answer('<?= $answeridx ?>')">
<?
	}
?>                        
					</td>
                </tr>
                <tr>
                    <th>답변내용</th>
                    <td><?= $answer ?></td>
                </tr>
                <tr>
                    <th>내부메모</th>
                    <td><?= str_replace("\n", "<br>", $memo) ?></td>
                </tr>
                <tr>
                    <th>답변일시</th>
                    <td><?= $writedate ?></td>
                </tr>
<?
		if ($coin != "0")
		{
?>
                <tr>
                    <th>보상 금액</th>
                    <td><?= make_price_format($coin) ?></td>
                </tr>
                <tr>
                    <th>보상 사유</th>
                    <td><?= $coin_memo ?></td>
                </tr>
                <tr>
                    <th>보내는 메시지</th>
                    <td><?= $coin_message ?></td>
                </tr> 
<?			
		}
?>		                
            </tbody>
            </table>
<?
	}
	
	$db_main->end();
	$db_analysis->end();
?>
        </div>
        <!--  //CONTENTS WRAP -->
<script>
	category_changed();
	
	var finalansweridx = "<?= $finalansweridx ?>";
</script>        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>