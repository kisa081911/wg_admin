<?
    $top_menu = "customer";
    $sub_menu = "qa_android_dashboard";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $db_analysis = new CDatabase_Analysis();
    
	function make_rate($count, $totalcount)
	{
		if ($totalcount == 0)
			return 0;
		
		return round($count * 1000 / $totalcount)/10;
	}
	
	function make_style($count, $totalcount)
	{
		if (make_rate($count, $totalcount) >= 20)
			return "style='color:#fb7878;font-weight:bold;'";
		else
			return "";
	}
?>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Android QA 현황</div>
	</div>
	<!-- //title_warp -->
            
	<div style="width:505px;float:left;margin:20px 0 0 0px;">
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">전체 QA 처리 현황</span>
		<table class="tbl_list_basic1" style="margin-top:5px">
			<colgroup>
				<col width="">   
				<col width="100">
				<col width="100">
				<col width="100">
			</colgroup>
			<thead>
				<tr>
					<th class="tdl">기간</th>
					<th class="tdr">문의건수</th>
					<th class="tdr">미응답 건수</th>
					<th class="tdc">미응답률</th>
				</tr>
			</thead>
			<tbody>
<?
    $today = date("Y-m-d");
    $yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));
    $day7 = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-6,date("Y", time())));
    $day14 = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-13,date("Y", time())));
    
    $sql = "SELECT (SELECT count(*) FROM support_android_qa WHERE writedate >= DATE_SUB(NOW(), INTERVAL 14 DAY)) AS count_14day, ".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate >= DATE_SUB(NOW(), INTERVAL 14 DAY)) AS noreply_14day, ".
            "(SELECT count(*) FROM support_android_qa WHERE writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY)) AS count_7day,".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY)) AS noreply_7day, ".
            "(SELECT count(*) FROM support_android_qa WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59') AS count_yesterday, ".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59') AS noreply_yesterday, ".
            "(SELECT count(*) FROM support_android_qa WHERE writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59') AS count_today, ".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59') AS noreply_today";
            
    $info = $db_analysis->getarray($sql);
?>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $day14 ?>&end_writedate=<?= $today ?>&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>>최근 14일</td>
		            <td class="tdr" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>><?= make_price_format($info["count_14day"]) ?></td>
		            <td class="tdr" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>><?= make_price_format($info["noreply_14day"]) ?></td>
		            <td class="tdc" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>><?= make_rate($info["noreply_14day"], $info["count_14day"]) ?>%</td>
		        </tr>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $day7 ?>&end_writedate=<?= $today ?>&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>>최근 7일</td>
		            <td class="tdr" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>><?= make_price_format($info["count_7day"]) ?></td>
		            <td class="tdr" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>><?= make_price_format($info["noreply_7day"]) ?></td>
		            <td class="tdc" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>><?= make_rate($info["noreply_7day"], $info["count_7day"]) ?>%</td>
		        </tr>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $yesterday ?>&end_writedate=<?= $yesterday ?>&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>>어제</td>
		            <td class="tdr" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>><?= make_price_format($info["count_yesterday"]) ?></td>
		            <td class="tdr" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>><?= make_price_format($info["noreply_yesterday"]) ?></td>
		            <td class="tdc" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>><?= make_rate($info["noreply_yesterday"], $info["count_yesterday"]) ?>%</td>
		        </tr>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $today ?>&end_writedate=<?= $today ?>&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>>오늘</td>
		            <td class="tdr point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>><?= make_price_format($info["count_today"]) ?></td>
		            <td class="tdr point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>><?= make_price_format($info["noreply_today"]) ?></td>
		            <td class="tdc point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>><?= make_rate($info["noreply_today"], $info["count_today"]) ?>%</td>
		        </tr>
			</tbody>
		</table>
	</div>
	
	<div style="width:505px;float:left;margin:20px 0 0 40px;">
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">VVIP QA 처리 현황 ($9,999 이상 결제)</span>
		<table class="tbl_list_basic1" style="margin-top:5px">
			<colgroup>
				<col width="">   
				<col width="100">
				<col width="100">
				<col width="100">
			</colgroup>
			<thead>
				<tr>
					<th class="tdl">기간</th>
					<th class="tdr">문의건수</th>
					<th class="tdr">미응답 건수</th>
					<th class="tdc">미응답률</th>
				</tr>
			</thead>
			<tbody>
<?
    $today = date("Y-m-d");
    $yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));
    
    $sql = "SELECT (SELECT count(*) FROM support_android_qa WHERE writedate >= DATE_SUB(NOW(), INTERVAL 14 DAY) AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)>=99990) AS count_14day, ".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate >= DATE_SUB(NOW(), INTERVAL 14 DAY) AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)>=99990) AS noreply_14day, ".
            "(SELECT count(*) FROM support_android_qa WHERE writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)>=99990) AS count_7day,".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)>=99990) AS noreply_7day, ".
            "(SELECT count(*) FROM support_android_qa WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)>=99990) AS count_yesterday, ".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)>=99990) AS noreply_yesterday, ".
            "(SELECT count(*) FROM support_android_qa WHERE writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)>=99990) AS count_today, ".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)>=99990) AS noreply_today";
            
    $info = $db_analysis->getarray($sql);
?>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $day14 ?>&end_writedate=<?= $today ?>&start_credit=99990&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>>최근 14일</td>
		            <td class="tdr" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>><?= make_price_format($info["count_14day"]) ?></td>
		            <td class="tdr" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>><?= make_price_format($info["noreply_14day"]) ?></td>
		            <td class="tdc" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>><?= make_rate($info["noreply_14day"], $info["count_14day"]) ?>%</td>
		        </tr>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $day7 ?>&end_writedate=<?= $today ?>&start_credit=99990&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>>최근 7일</td>
		            <td class="tdr" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>><?= make_price_format($info["count_7day"]) ?></td>
		            <td class="tdr" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>><?= make_price_format($info["noreply_7day"]) ?></td>
		            <td class="tdc" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>><?= make_rate($info["noreply_7day"], $info["count_7day"]) ?>%</td>
		        </tr>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $yesterday ?>&end_writedate=<?= $yesterday ?>&start_credit=99990&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>>어제</td>
		            <td class="tdr" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>><?= make_price_format($info["count_yesterday"]) ?></td>
		            <td class="tdr" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>><?= make_price_format($info["noreply_yesterday"]) ?></td>
		            <td class="tdc" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>><?= make_rate($info["noreply_yesterday"], $info["count_yesterday"]) ?>%</td>
		        </tr>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $today ?>&end_writedate=<?= $today ?>&start_credit=99990&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>>오늘</td>
		            <td class="tdr point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>><?= make_price_format($info["count_today"]) ?></td>
		            <td class="tdr point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>><?= make_price_format($info["noreply_today"]) ?></td>
		            <td class="tdc point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>><?= make_rate($info["noreply_today"], $info["count_today"]) ?>%</td>
		        </tr>
			</tbody>
		</table>
	</div>
            
	<div class="clear"></div>
  	
  	
  	<div style="width:505px;float:left;margin:20px 0 0 0px;">
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">VIP QA 처리 현황 ($499 ~ $9,998 결제)</span>
		<table class="tbl_list_basic1" style="margin-top:5px">
			<colgroup>
				<col width="">   
				<col width="100">
				<col width="100">
				<col width="100">
			</colgroup>
			<thead>
				<tr>
					<th class="tdl">기간</th>
					<th class="tdr">문의건수</th>
					<th class="tdr">미응답 건수</th>
					<th class="tdc">미응답률</th>
				</tr>
			</thead>
			<tbody>
<?
    $today = date("Y-m-d");
    $yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));
    
    $sql = "SELECT (SELECT count(*) FROM support_android_qa WHERE writedate >= DATE_SUB(NOW(), INTERVAL 14 DAY) AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)BETWEEN 4990 AND 99980) AS count_14day, ".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate >= DATE_SUB(NOW(), INTERVAL 14 DAY) AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)BETWEEN 4990 AND 99980) AS noreply_14day, ".
            "(SELECT count(*) FROM support_android_qa WHERE writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)BETWEEN 4990 AND 99980) AS count_7day,".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)BETWEEN 4990 AND 99980) AS noreply_7day, ".
            "(SELECT count(*) FROM support_android_qa WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)BETWEEN 4990 AND 99980) AS count_yesterday, ".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)BETWEEN 4990 AND 99980) AS noreply_yesterday, ".
            "(SELECT count(*) FROM support_android_qa WHERE writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)BETWEEN 4990 AND 99980) AS count_today, ".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx)BETWEEN 4990 AND 99980) AS noreply_today";
            
    $info = $db_analysis->getarray($sql);
?>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $day14 ?>&end_writedate=<?= $today ?>&start_credit=4990&end_credit=99980&&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>>최근 14일</td>
		            <td class="tdr" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>><?= make_price_format($info["count_14day"]) ?></td>
		            <td class="tdr" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>><?= make_price_format($info["noreply_14day"]) ?></td>
		            <td class="tdc" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>><?= make_rate($info["noreply_14day"], $info["count_14day"]) ?>%</td>
		        </tr>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $day7 ?>&end_writedate=<?= $today ?>&start_credit=4990&end_credit=99980&&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>>최근 7일</td>
		            <td class="tdr" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>><?= make_price_format($info["count_7day"]) ?></td>
		            <td class="tdr" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>><?= make_price_format($info["noreply_7day"]) ?></td>
		            <td class="tdc" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>><?= make_rate($info["noreply_7day"], $info["count_7day"]) ?>%</td>
		        </tr>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $yesterday ?>&end_writedate=<?= $yesterday ?>&start_credit=4990&end_credit=99980&&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>>어제</td>
		            <td class="tdr" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>><?= make_price_format($info["count_yesterday"]) ?></td>
		            <td class="tdr" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>><?= make_price_format($info["noreply_yesterday"]) ?></td>
		            <td class="tdc" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>><?= make_rate($info["noreply_yesterday"], $info["count_yesterday"]) ?>%</td>
		        </tr>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $today ?>&end_writedate=<?= $today ?>&start_credit=4990&end_credit=99980&&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>>오늘</td>
		            <td class="tdr point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>><?= make_price_format($info["count_today"]) ?></td>
		            <td class="tdr point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>><?= make_price_format($info["noreply_today"]) ?></td>
		            <td class="tdc point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>><?= make_rate($info["noreply_today"], $info["count_today"]) ?>%</td>
		        </tr>
			</tbody>
		</table>
	</div>
            
	<div style="width:505px;float:left;margin:20px 0 0 40px">
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">Premium QA 처리 현황 ($39~$499 결제)</span>
		<table class="tbl_list_basic1" style="margin-top:5px">
			<colgroup>
				<col width="">   
				<col width="100">
				<col width="100">
				<col width="100">
			</colgroup>
			<thead>
				<tr>
					<th class="tdl">기간</th>
					<th class="tdr">문의건수</th>
					<th class="tdr">미응답 건수</th>
					<th class="tdc">미응답률</th>
				</tr>
			</thead>
			<tbody>
<?
    $today = date("Y-m-d");
    $yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));
    
    $sql = "SELECT (SELECT count(*) FROM support_android_qa WHERE writedate >= DATE_SUB(NOW(), INTERVAL 14 DAY) AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx) BETWEEN 390 AND 4989) AS count_14day, ".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate >= DATE_SUB(NOW(), INTERVAL 14 DAY) AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx) BETWEEN 390 AND 4989) AS noreply_14day, ".
            "(SELECT count(*) FROM support_android_qa WHERE writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx) BETWEEN 390 AND 4989) AS count_7day,".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx) BETWEEN 390 AND 4989) AS noreply_7day, ".
            "(SELECT count(*) FROM support_android_qa WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx) BETWEEN 390 AND 4989) AS count_yesterday, ".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx) BETWEEN 390 AND 4989) AS noreply_yesterday, ".
            "(SELECT count(*) FROM support_android_qa WHERE writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx) BETWEEN 390 AND 4989) AS count_today, ".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND (SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx) BETWEEN 390 AND 4989) AS noreply_today";
            
    $info = $db_analysis->getarray($sql);
?>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $day14 ?>&end_writedate=<?= $today ?>&start_credit=390&end_credit=4989&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>>최근 14일</td>
		            <td class="tdr" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>><?= make_price_format($info["count_14day"]) ?></td>
		            <td class="tdr" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>><?= make_price_format($info["noreply_14day"]) ?></td>
		            <td class="tdc" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>><?= make_rate($info["noreply_14day"], $info["count_14day"]) ?>%</td>
		        </tr>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $day7 ?>&end_writedate=<?= $today ?>&start_credit=390&end_credit=4989&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>>최근 7일</td>
		            <td class="tdr" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>><?= make_price_format($info["count_7day"]) ?></td>
		            <td class="tdr" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>><?= make_price_format($info["noreply_7day"]) ?></td>
		            <td class="tdc" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>><?= make_rate($info["noreply_7day"], $info["count_7day"]) ?>%</td>
		        </tr>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $yesterday ?>&end_writedate=<?= $yesterday ?>&start_credit=390&end_credit=4989&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>>어제</td>
		            <td class="tdr" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>><?= make_price_format($info["count_yesterday"]) ?></td>
		            <td class="tdr" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>><?= make_price_format($info["noreply_yesterday"]) ?></td>
		            <td class="tdc" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>><?= make_rate($info["noreply_yesterday"], $info["count_yesterday"]) ?>%</td>
		        </tr>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $today ?>&end_writedate=<?= $today ?>&start_credit=390&end_credit=4989&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>>오늘</td>
		            <td class="tdr point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>><?= make_price_format($info["count_today"]) ?></td>
		            <td class="tdr point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>><?= make_price_format($info["noreply_today"]) ?></td>
		            <td class="tdc point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>><?= make_rate($info["noreply_today"], $info["count_today"]) ?>%</td>
		        </tr>
			</tbody>
		</table>
	</div>
	
	<div style="width:505px;float:left;margin:20px 0 0 0px ">
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">기타 QA 처리 현황 ($39 이하 결제)</span>
		<table class="tbl_list_basic1" style="margin-top:5px">
			<colgroup>
				<col width="">   
				<col width="100">
				<col width="100">
				<col width="100">
			</colgroup>
			<thead>
				<tr>
					<th class="tdl">기간</th>
					<th class="tdr">문의건수</th>
					<th class="tdr">미응답 건수</th>
					<th class="tdc">미응답률</th>
				</tr>
			</thead>
			<tbody>
<?
    $today = date("Y-m-d");
    $yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));
    
    $sql = "SELECT (SELECT count(*) FROM support_android_qa WHERE writedate >= DATE_SUB(NOW(), INTERVAL 14 DAY) AND IFNULL((SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx),0)<389) AS count_14day, ".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate >= DATE_SUB(NOW(), INTERVAL 14 DAY) AND IFNULL((SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx),0)<389) AS noreply_14day, ".
            "(SELECT count(*) FROM support_android_qa WHERE writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND IFNULL((SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx),0)<389) AS count_7day,".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND IFNULL((SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx),0)<389) AS noreply_7day, ".
            "(SELECT count(*) FROM support_android_qa WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND IFNULL((SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx),0)<389) AS count_yesterday, ".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59' AND IFNULL((SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx),0)<389) AS noreply_yesterday, ".
            "(SELECT count(*) FROM support_android_qa WHERE writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND IFNULL((SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx),0)<389) AS count_today, ".
            "(SELECT count(*) FROM support_android_qa WHERE status=0 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND IFNULL((SELECT summary_credit FROM user_order WHERE useridx=support_android_qa.useridx),0)<389) AS noreply_today";
            
    $info = $db_analysis->getarray($sql);
?>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $day14 ?>&end_writedate=<?= $today ?>&start_credit=0&end_credit=389&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>>최근 14일</td>
		            <td class="tdr" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>><?= make_price_format($info["count_14day"]) ?></td>
		            <td class="tdr" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>><?= make_price_format($info["noreply_14day"]) ?></td>
		            <td class="tdc" <?= make_style($info["noreply_14day"], $info["count_14day"]) ?>><?= make_rate($info["noreply_14day"], $info["count_14day"]) ?>%</td>
		        </tr>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $day7 ?>&end_writedate=<?= $today ?>&start_credit=0&end_credit=389&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>>최근 7일</td>
		            <td class="tdr" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>><?= make_price_format($info["count_7day"]) ?></td>
		            <td class="tdr" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>><?= make_price_format($info["noreply_7day"]) ?></td>
		            <td class="tdc" <?= make_style($info["noreply_7day"], $info["count_7day"]) ?>><?= make_rate($info["noreply_7day"], $info["count_7day"]) ?>%</td>
		        </tr>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $yesterday ?>&end_writedate=<?= $yesterday ?>&start_credit=0&end_credit=389&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>>어제</td>
		            <td class="tdr" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>><?= make_price_format($info["count_yesterday"]) ?></td>
		            <td class="tdr" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>><?= make_price_format($info["noreply_yesterday"]) ?></td>
		            <td class="tdc" <?= make_style($info["noreply_yesterday"], $info["count_yesterday"]) ?>><?= make_rate($info["noreply_yesterday"], $info["count_yesterday"]) ?>%</td>
		        </tr>
		        <tr style="cursor:pointer" onclick="window.location.href='qa_android.php?searchcheck=1&start_writedate=<?= $today ?>&end_writedate=<?= $today ?>&start_credit=0&end_credit=389&status=0'" title="미응답 QA 바로가기">
		            <td class="tdl point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>>오늘</td>
		            <td class="tdr point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>><?= make_price_format($info["count_today"]) ?></td>
		            <td class="tdr point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>><?= make_price_format($info["noreply_today"]) ?></td>
		            <td class="tdc point_title" <?= make_style($info["noreply_today"], $info["count_today"]) ?>><?= make_rate($info["noreply_today"], $info["count_today"]) ?>%</td>
		        </tr>
			</tbody>
		</table>
	</div>
            
	<div class="clear"></div>
	
	<div style="width:505px;float:left;margin:20px 0 0 0px;">
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;">QA 답변 작성 현황</span>
		<table class="tbl_list_basic1" style="margin-top:5px">
			<colgroup>
				<col width="">   
				<col width="100">
			</colgroup>
			<thead>
				<tr>
					<th class="tdl">기간</th>
					<th class="tdr">답변 작성 건수</th>
				</tr>
			</thead>
			<tbody>
<?
    $today = date("Y-m-d");
    $yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));
    
    $sql = "SELECT (SELECT count(*) FROM support_android_qa_answer WHERE writedate >= DATE_SUB(NOW(), INTERVAL 14 DAY)) AS count_14day, ".
            "(SELECT count(*) FROM support_android_qa_answer WHERE writedate >= DATE_SUB(NOW(), INTERVAL 7 DAY)) AS count_7day,".
            "(SELECT count(*) FROM support_android_qa_answer WHERE writedate BETWEEN '$yesterday 00:00:00' AND '$yesterday 23:59:59') AS count_yesterday, ".
            "(SELECT count(*) FROM support_android_qa_answer WHERE writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59') AS count_today";
            
    $info = $db_analysis->getarray($sql);
?>
		        <tr>
		            <td class="tdl">최근 14일</td>
		            <td class="tdr"><?= make_price_format($info["count_14day"]) ?></td>
		        </tr>
		        <tr>
		            <td class="tdl">최근 7일</td>
		            <td class="tdr"><?= make_price_format($info["count_7day"]) ?></td>
		        </tr>
		        <tr>
		            <td class="tdl">어제</td>
		            <td class="tdr"><?= make_price_format($info["count_yesterday"]) ?></td>
		        </tr>
		        <tr>
		            <td class="tdl point_title">오늘</td>
		            <td class="tdr point_title"><?= make_price_format($info["count_today"]) ?></td>
		        </tr>
			</tbody>
		</table>
	</div>

	 <div style="width:505px;float:left;margin:20px 0 0 40px;">
		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"> QA 미응답 문의 건수</span>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="">   
					<col width="100">
				</colgroup>
				<thead>
					<tr>
						<th class="tdl">미응답 기간</th>
						<th class="tdr">미응답 건수</th>
					</tr>
				</thead>
				<tbody>
<?    
		$date_today = date('w', strtotime($today));
		$now_time =  date("Y-m-d H:i:s");
		
		// 쉬는날 날짜설정
		$holiday_sdate = "2015-09-25 00:00:00";
		$holiday_edate = "2015-09-30 01:00:00";
		
		if($holiday_sdate <= date("Y-m-d H:i:s") && date("Y-m-d H:i:s") <= $holiday_edate)
		{
			$sql = "SELECT qaidx, writedate ".
					"FROM `support_android_qa` ".
					"WHERE STATUS = 0 ".
					"ORDER BY qaidx DESC";
		}
		else
		{
	
		    $sql = "SELECT qaidx, writedate, ". 
					"(CASE DAYOFWEEK(writedate) ".					
				    	"WHEN 7 THEN (FLOOR(UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(writedate))/(60*60*24) - 2) ". 
						"WHEN 1 THEN (FLOOR(UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(writedate))/(60*60*24) - 1) ". 
						"ELSE FLOOR(UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(writedate))/(60*60*24) END ) AS pastdate ".
					"FROM `support_android_qa` ".
					"WHERE STATUS = 0 ".
					"ORDER BY qaidx DESC";    
		}    
	            
	    $info = $db_analysis->gettotallist($sql);
	    
	    $one_day_date = 0;
	    $two_day_date = 0;
	    $three_day_date = 0;
	    
	    for ($i = 0; $i < sizeof($info); $i++)
	    {
	    	if($holiday_sdate <= date("Y-m-d H:i:s") && date("Y-m-d H:i:s") <= $holiday_edate)
	    	{
	    		$pastdate = $info[$i]["writedate"];
	    	
	    		if($today == "2015-09-28")
	    		{
	    	
	    		}
	    		else if($today == "2015-09-29")
	    		{
	    			$date1=date_create("$pastdate");
	    			$date2=date_create("$now_time");
	    			 
	    			$diff_day = date_diff($date1,$date2)->d;
	    	
	    			if($diff_day = 6)
	    				$two_day_date += 1;
	    			else if($diff_day <= 5)
	    				$one_day_date += 1;
	    		}
	    		else if($today == "2015-09-30")
	    		{
	    			$date1=date_create("$pastdate");
	    			$date2=date_create("$now_time");
	    			 
	    			$diff_day = date_diff($date1,$date2)->d;
	    			 
	    			if($diff_day >= 7)
	    				$three_day_date += 1;
	    			else if($diff_day >= 6)
	    				$two_day_date += 1;
	    			else if($diff_day <= 5)
	    				$one_day_date += 1;
	    		}
	    	}
	    	else
	    	{
		    	$pastdate = floor($info[$i]["pastdate"]);
		    	
		    	if($date_today == 1 && $pastdate == 3)
		    		$pastdate -= 2;
		    	else if($date_today == 1 && $pastdate == 2)
		    		$pastdate -= 1;
		    	 
		    	if($date_today == 2 && $pastdate == 4)
		    		$pastdate -= 2;
		    	else if($date_today == 2 && $pastdate == 3)
		    		$pastdate -= 1;
		    	
		    	if($pastdate >= 1)
		    		$one_day_date += 1;
		    	
		    	if($pastdate >= 2)
		    		$two_day_date += 1;
		    	
		    	if($pastdate >= 3)
		    		$three_day_date += 1;
	    	}
	    }
?>

				<tr style="cursor:pointer">
		            <td class="tdl">3일 이상</td>
		             <td class="tdr" <?= ($three_day_date > 0 ? "style='color:#fb7878;font-weight:bold;'" : "" )?>><?= number_format($three_day_date) ?></td>           
		        </tr>
		        <tr style="cursor:pointer">
		            <td class="tdl">2일 이상</td>
		            <td class="tdr"><?= number_format($two_day_date) ?></td>            
		        </tr>
		        <tr style="cursor:pointer">
		            <td class="tdl">1일 이상</td>
		            <td class="tdr"><?= number_format($one_day_date) ?></td>
		        </tr>
			</tbody>
		</table>
	</div>
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_analysis->end();
	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>