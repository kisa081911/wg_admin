<?
    $top_menu = "customer";
    $sub_menu = "mobile_amazon_template";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    // ckeditor 경로 설정
    include_once($_SERVER["DOCUMENT_ROOT"]."/ckeditor/ckeditor.php");
    
    $templateidx = $_GET["templateidx"];
    
    check_number($templateidx);
    
    $db_analysis = new CDatabase_Analysis();
    
    if ($templateidx != "")
    {       
        $sql = "SELECT templateidx,title,contents,writedate,categoryidx FROM support_amazon_qa_template WHERE templateidx=$templateidx";
        $templates = $db_analysis->getarray($sql);
        
        $templateidx = $templates["templateidx"];
        $title = $templates["title"];
        $contents = $templates["contents"];
        $writedate = $templates["writedate"];
        $categoryidx = $templates["categoryidx"];
    
	    if ($templateidx == "")
            error_back("존재하지 않는 QA 답변 템플릿입니다.");
	}
    
	$sql = "SELECT categoryidx,category FROM support_amazon_category";
	
	$categorylist = $db_analysis->gettotallist($sql);
	
    $db_analysis->end();
?>
<script>
    function save_mobile_amazon_template()
    {
        var input_form = document.input_form;
        
        if (input_form.title.value == "")
        {
            alert("제목을 입력하세요.");
            input_form.title.focus();
            return;
        }
        
        if (input_form.category.value == "")
        {
            alert("분류를 선택하세요.");
            input_form.category.focus();
            return;
        }

        if (CKEDITOR.instances.contents.getData() == "")
        {
            alert("내용을 입력하세요.");
            CKEDITOR.instances.contents.focus();
            return;
        }
        
        var param = {};
        param.templateidx = "<?= $templateidx ?>";
        param.title = input_form.title.value;
        param.categoryidx = input_form.category.options[input_form.category.selectedIndex].value;
        param.contents = CKEDITOR.instances.contents.getData();

        if ("<?= $templateidx ?>" == "")
        	WG_ajax_execute("customer/save_mobile_amazon_template", param, save_mobile_amazon_template_callback);
        else
        	WG_ajax_execute("customer/update_mobile_amazon_template", param, save_mobile_amazon_template_callback);
    }

    function save_mobile_amazon_template_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            window.location.href = "mobile_amazon_template.php";
        }
    }

    function delete_mobile_amazon_template()
    {
        if (!confirm("template를 삭제 하시겠습니까?"))
            return;

        var param = {};
        param.templateidx = "<?= $templateidx?>";

        WG_ajax_execute("customer/delete_mobile_amazon_template", param, delete_mobile_amazon_template_callback);
    }

    function delete_mobile_amazon_template_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.href = "mobile_amazon_template.php";
        }
    }
</script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>

<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; Amazon QA 답변 템플릿 상세</div>
<?
    if ($templateidx != "")
    { 
?>
            <div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_mobile_amazon_template()"></div>
<?
    } 
?>
            </div>
            <!-- //title_warp -->

            <form name="input_form" id="input_form" onsubmit="return false">
            <table class="tbl_view_basic">
            <colgroup>
                <col width="150">
                <col width="">
            </colgroup>
            <tbody>
                <tr>
                    <th><span>*</span> 제목</th>
                    <td><input type="text" class="view_tbl_text" name="title" id="title" value="<?= encode_html_attribute($title) ?>" maxlength="1024"></td>
                </tr>
                <tr>
                    <th><span>*</span> 분류</th>
                    <td><select name="category">
                    	<option value="">분류선택</option>
                    	<option value="0" <?= ($categoryidx == "0") ? "selected" : ""?>>General</option>
<?
	for ($i=0; $i<sizeof($categorylist); $i++)
	{
		$select_categoryidx = $categorylist[$i]["categoryidx"];
		$select_category = $categorylist[$i]["category"];
		
		if ($select_categoryidx == $categoryidx)
			$selected = "selected";
		else
			$selected = "";
?>
						<option value="<?= $select_categoryidx ?>" <?= $selected ?>><?= $select_category ?></option>
<?
	}
?>                    	
                    </select></td>
                </tr>
                <tr>
                    <th><span>*</span> 내용</th>
                    <td>
                        <textarea id="contents" name="contents"><?= $contents ?></textarea>
                        <script type="text/javascript">CKEDITOR.replace("contents",{width: '100%',height: '350px'});</script>
                    </td>
                </tr>
            </tbody>
            </table>    
            </form> 
            
            <div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="저장" onclick="save_mobile_amazon_template()">
                <input type="button" class="btn_setting_02" value="취소" onclick="go_page('mobile_amazon_template.php')">           
            </div>
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>