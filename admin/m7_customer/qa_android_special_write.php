<?
    $top_menu = "customer";
    $sub_menu = "qa_android_special";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    // ckeditor 경로 설정
    include_once($_SERVER["DOCUMENT_ROOT"]."/ckeditor/ckeditor.php");
    
    $qaidx = $_GET["qaidx"];
    $useridx = $_GET["useridx"];
    $pagefield = $_GET["pagefield"];
	
    check_number($qaidx);
    check_number($useridx);
    
	if ($qaidx == "" && $useridx == "")
		error_back("잘못된 접근입니다.");
	
    $db_analysis = new CDatabase_Analysis();
    $db_main = new CDatabase_Main();
    
    function GetMobileName($type)
    {
    	if ($type == "iPhone1,1")	return "iPhone 2G";
    	if ($type == "iPhone1,2")  return "iPhone 3G";
    	if ($type == "iPhone2,1")  return "iPhone 3GS";
    	if ($type == "iPhone3,1")  return "iPhone 4 (GSM)";
    	if ($type == "iPhone3,2")  return "iPhone 4 (GSM Rev. A)";
    	if ($type == "iPhone3,3")  return "iPhone 4 (CDMA)";
    	if ($type == "iPhone4,1")  return "iPhone 4S";
    	if ($type == "iPhone5,1")  return "iPhone 5 (GSM)";
    	if ($type == "iPhone5,2")  return "iPhone 5 (Global)";
    	if ($type == "iPhone5,3")  return "iPhone 5c (GSM)";
    	if ($type == "iPhone5,4")  return "iPhone 5c (Global)";
    	if ($type == "iPhone6,1")  return "iPhone 5s (GSM)";
    	if ($type == "iPhone6,2")  return "iPhone 5s (Global)";
    	if ($type == "iPhone7,1")  return "iPhone 6 Plus";
    	if ($type == "iPhone7,2")  return "iPhone 6";
    
    	if ($type == "iPod1,1")   	return "iPod Touch (1 Gen)";
    	if ($type == "iPod2,1")    return "iPod Touch (2 Gen)";
    	if ($type == "iPod3,1")    return "iPod Touch (3 Gen)";
    	if ($type == "iPod4,1")    return "iPod Touch (4 Gen)";
    	if ($type == "iPod5,1")    return "iPod Touch (5 Gen)";
    
    	if ($type == "iPad1,1")    return "iPad (WiFi)";
    	if ($type == "iPad1,2")    return "iPad 3G";
    	if ($type == "iPad2,1")    return "iPad 2 (WiFi)";
    	if ($type == "iPad2,2")    return "iPad 2 (GSM)";
    	if ($type == "iPad2,3")    return "iPad 2 (CDMA)";
    	if ($type == "iPad2,4")    return "iPad 2 (WiFi Rev. A)";
    	if ($type == "iPad2,5")    return "iPad Mini (WiFi)";
    	if ($type == "iPad2,6")    return "iPad Mini (GSM)";
    	if ($type == "iPad2,7")    return "iPad Mini (CDMA)";
    	if ($type == "iPad3,1")    return "iPad 3 (WiFi)";
    	if ($type == "iPad3,2")    return "iPad 3 (CDMA)";
    	if ($type == "iPad3,3")    return "iPad 3 (Global)";
    	if ($type == "iPad3,4")    return "iPad 4 (WiFi)";
    	if ($type == "iPad3,5")    return "iPad 4 (CDMA)";
    	if ($type == "iPad3,6")    return "iPad 4 (Global)";
    	 
    	if ($type == "iPad4,1")    return "iPad Air (WiFi)";
    	if ($type == "iPad4,2")    return "iPad Air (WiFi+GSM)";
    	if ($type == "iPad4,3")    return "iPad Air (WiFi+CDMA)";
    	if ($type == "iPad4,4")    return "iPad Mini Retina (WiFi)";
    	if ($type == "iPad4,5")    return "iPad Mini Retina (WiFi+CDMA)";
    
    	if ($type == "i386")       return "Simulator";
    	if ($type == "x86_64")     return "Simulator";
    	 
    	return "Unknown";
    }
	
	if ($qaidx != "")
	{
	    $sql = "SELECT qaidx,contents,useridx,name,facebookid,status,responsedate,writedate,categoryidx,ispublic,ispending,isspecial,usereporting,reportingcategory,mtype,mver,appver FROM support_android_qa WHERE qaidx=$qaidx";
	    $faqs = $db_analysis->getarray($sql);
	    
	    $qaidx = $faqs["qaidx"];
	    $contents = $faqs["contents"];
	    $status = $faqs["status"];
	    $useridx = $faqs["useridx"];
	    $name = $faqs["name"];
	    $facebookid = $faqs["facebookid"];
	    $responsedate = $faqs["responsedate"];
	    $writedate = $faqs["writedate"];
	    $categoryidx = $faqs["categoryidx"];
	    $ispublic = $faqs["ispublic"];	    
	    $ispending = $faqs["ispending"];
	    $isspecial = $fags["isspecial"];
	    $usereporting = $faqs["usereporting"];
	    $reportingcategory = $faqs["reportingcategory"];
	    $mtype = $faqs["mtype"];
	    $mver = $faqs["mver"];
	    $mtype = GetMobileName($faqs["mtype"]);
	    $appver = $faqs["appver"];

	    if ($qaidx == "")
    	    error_back("존재하지 않는 QA입니다.");
	}
	else
	{
	    $qaidx = "0";
	    $contents = "";
	    $status = "0";
	    $ispublic = "0";
	    $isdelete = "0";
		$ispending = "0";
		$isspecial = "0";
		$usereporting = "0";
		
		$sql = "SELECT nickname,userid FROM tbl_user WHERE useridx=$useridx";
		$data = $db_main->getarray($sql);

	    $name = $data["nickname"];
	    $facebookid = $data["userid"];
	}
	
	if($facebookid < 10)
		$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$facebookid.".png";
	else
	    $photourl = get_fb_pictureURL($facebookid,$client_accesstoken);
    
	$sql = "SELECT categoryidx,category FROM support_android_category ORDER BY categoryidx ASC";

	$categorylist = $db_analysis->gettotallist($sql);
	
	$sql = "SELECT coin,sex FROM tbl_user WHERE useridx=$useridx";
	$data = $db_main->getarray($sql);
					
	$coin = $data["coin"];	
	$sex = $data["sex"];

	$sql = "SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE status=1 AND useridx=$useridx";
	$totalcredit = $db_main->getvalue($sql);
	
	$sql = "SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE os_type=1 AND status=1 AND useridx=$useridx";
	$totalcredit_ios = $db_main->getvalue($sql);
	
	$sql = "SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE os_type=2 AND status=1 AND useridx=$useridx";
	$totalcredit_android = $db_main->getvalue($sql);
		
	$sql = "SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE status=2 AND useridx=$useridx";
	$totalcancel = $db_main->getvalue($sql);

	$sql = "SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE os_type=1 AND status=2 AND useridx=$useridx";
	$totalcancel_ios = $db_main->getvalue($sql);
	
	$sql = "SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE os_type=2 AND status=2 AND useridx=$useridx";
	$totalcancel_android = $db_main->getvalue($sql);
	
	$sql = "SELECT SUM(coin) AS total_coin ".
			"FROM ( ".
     		"	SELECT IFNULL(SUM(coin),0) AS coin FROM tbl_product_order WHERE status=1 AND useridx=$useridx ".
     		"	UNION ALL ".
     		"	SELECT IFNULL(SUM(coin),0) AS coin FROM tbl_product_order WHERE status=1 AND useridx=$useridx ".
			") t1";
	
	$totalchip = $db_main->getarray($sql);	
	$totalchip_num = $totalchip["total_coin"] + $totalchip["total_credit"];

	$sql = "SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE status=1 AND useridx=$useridx AND writedate>DATE_ADD(NOW(),INTERVAL -10 DAY)";
	$recentcredit = $db_main->getvalue($sql);

	$sql = "SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE os_type=1 AND status=1 AND useridx=$useridx AND writedate>DATE_ADD(NOW(),INTERVAL -10 DAY)";
	$recentcredit_ios = $db_main->getvalue($sql);
	
	$sql = "SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE os_type=2 AND status=1 AND useridx=$useridx AND writedate>DATE_ADD(NOW(),INTERVAL -10 DAY)";
	$recentcredit_android = $db_main->getvalue($sql);
	
	$sql = "SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE status=2 AND useridx=$useridx AND writedate>DATE_ADD(NOW(),INTERVAL -10 DAY)";
	$recentcancel = $db_main->getvalue($sql);

	$sql = "SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE os_type=1 AND status=2 AND useridx=$useridx AND writedate>DATE_ADD(NOW(),INTERVAL -10 DAY)";
	$recentcancel_ios = $db_main->getvalue($sql);
	
	$sql = "SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE os_type=2 AND status=2 AND useridx=$useridx AND writedate>DATE_ADD(NOW(),INTERVAL -10 DAY)";
	$recentcancel_android = $db_main->getvalue($sql);
	
	$sql = "SELECT IFNULL(SUM(freecoin),0) FROM tbl_freecoin_admin_log WHERE useridx=$useridx";
	$totalsent = $db_main->getvalue($sql);

	$sql = "SELECT IFNULL(SUM(freecoin),0) FROM tbl_freecoin_admin_log WHERE useridx=$useridx AND writedate>DATE_ADD(NOW(),INTERVAL -10 DAY)";
	$recentsent = $db_main->getvalue($sql);
    
	$sql = "SELECT qaidx,contents,writedate,status,isspecial FROM support_android_qa WHERE useridx=$useridx ORDER BY qaidx DESC";
	$qalist1 = $db_analysis->gettotallist($sql);
	
	$qalist = "0";
	
	for ($i=0; $i<sizeof($qalist1); $i++)
		$qalist .= ",".$qalist1[$i]["qaidx"];
	
	$sql = "SELECT qaidx,adminid,answer,writedate,coin,coin_memo,coin_message FROM support_android_qa_answer WHERE qaidx IN ($qalist) ORDER BY answeridx";
	$qalist2 = $db_analysis->gettotallist($sql);

	$sql = "SELECT * ".
		   "FROM ( ".
		   "	SELECT * FROM (SELECT  '1' AS type, status, facebookcredit, writedate FROM tbl_product_order WHERE useridx=$useridx AND status IN (1,2) ORDER BY orderidx DESC LIMIT 7) t1 ".
		   "	UNION ALL ".
		   "	SELECT * FROM (SELECT '2' AS type, status, money AS facebookcredit, writedate FROM tbl_product_order_mobile WHERE useridx=$useridx AND status IN (1,2) ORDER BY writedate DESC LIMIT 11) t1 ".
		   ") t1 ".
		   "ORDER BY writedate DESC ".
		   "LIMIT 7";			

	$orderlist = $db_main->gettotallist($sql);

	$sql = "SELECT freecoin, reason, admin_id, writedate FROM tbl_freecoin_admin_log WHERE useridx=$useridx ORDER BY logidx DESC LIMIT 100";
	$freecoinlist = $db_main->gettotallist($sql);
	
	$sql = "SELECT vip_level FROM tbl_user_detail WHERE useridx=$useridx";
	$vip_level = $db_main->getvalue($sql);
	
	//if ($vip_level > 0)
	//	$vip_level_image = "&nbsp;<img src=\"/images/membership/vp_level_".$vip_level.".png\" height=\"30\" width=\"30\" align=\"absmiddle\" />";
	//else
		$vip_level_image = "";
?>
<style>
	div td {word-break:break-all}
</style>
<script>
	var g_savetype = "";
	
     function setCookie(cName, cValue, cDay)
     {
          var expire = new Date();
          expire.setDate(expire.getDate() + cDay);
          cookies = cName + '=' + escape(cValue) + '; path=/ ';
          if(typeof cDay != 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
          document.cookie = cookies;
     }

     function getCookie(cName) 
     {
          cName = cName + '=';
          var cookieData = document.cookie;
          var start = cookieData.indexOf(cName);
          var cValue = '';
          if(start != -1){
               start += cName.length;
               var end = cookieData.indexOf(';', start);
               if(end == -1)end = cookieData.length;
               cValue = cookieData.substring(start, end);
          }
          return unescape(cValue);
     }	
     
	function window_onload()
	{
		return;
		
		var contents = getCookie("answer_contents");
		
		if (contents != "" && contents != null)	
			CKEDITOR.instances.contents.setData(contents);
			
		setInterval("save_contents()", 3000);
	}
	
	function save_contents()
	{
		var contents = CKEDITOR.instances.contents.getData();
		
		if (contents != "")
		{
			setCookie("answer_contents", contents, 1);
		}
	}
	
    function save_qa_android(savetype)
    {
    	g_savetype = savetype;
    	
        var input_form = document.input_form;
        
<?
	if ($qaidx == "0")
	{
?>
        if (input_form.question.value == "")
        {
            alert("문의 내용을 입력해주세요.");
            input_form.question.focus();
            return;
        }
<?
	}
?>
        
        if (savetype != "1")
        {
	        if (CKEDITOR.instances.contents.getData() == "")
	        {
	            alert("답변내용을 입력하세요.");
	            CKEDITOR.instances.contents.focus();
	            return;
	        }

	        var free_type = $("#free_type").val();
	        var free_amount = $("#free_amount").val();
			var free_reason = $("#reason").val();
			var free_message = $("#message").val();
			var free_adminid = $("#admin_id").val();

	        free_amount = strip_price_format(free_amount);
	        free_amount = Number(free_amount);

			if(free_amount != "")
			{
				if (free_type == "")
		        {			         
		            alert("보상 자원을 선택해주세요.");
		            $("#free_type").focus();
		            return;
		        }

				if (free_reason == "")
		        {			         
		            alert("보상 사유를 입력해주세요.");
		            $("#reason").focus();
		            return;
		        }

				if (free_message == "")
		        {			         
		            alert("보내는 메세지를 입력해주세요.");
		            $("#message").focus();
		            return;
		        }
			}
		}
		        
		if (input_form.category.value == "")
		{
			alert("분류를 선택해주세요.");
			input_form.category.focus();
			return;
		}
		
        var param = {};
        param.qaidx = "<?= $qaidx ?>";
        param.answer = CKEDITOR.instances.contents.getData();
        param.memo = input_form.memo.value;        
        param.categoryidx = input_form.category.options[input_form.category.selectedIndex].value;        
        param.ispublic = get_radio("ispublic");
        param.ispending = get_radio("ispending");
		param.isspecial = get_radio("isspecial");		
        param.usereporting = get_radio("usereporting");
        param.reportingcategory = get_radio("reportingcategory");
        param.savetype = savetype;
        param.status = get_radio("status");
        param.free_type = free_type;
        param.free_amount = free_amount;
        param.free_reason = free_reason;
        param.free_message = free_message;
        param.free_adminid = free_adminid;
        param.level = input_form.level.value;
        param.finalansweridx = finalansweridx;
        
<?
	if ($qaidx == "0")
	{
?>
        param.question = input_form.question.value;
        param.useridx = "<?= $useridx ?>";
<?
	}
?>

        WG_ajax_execute("customer/save_qa_android", param, save_qa_android_callback);
    }

    function save_qa_android_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
        	if (g_savetype == "1")
			{
        		alert("QA의 정보를 변경했습니다.");       	        		
				window.location.href = "qa_android_special.php?<?= $pagefield ?>";
			}
        	else
			{				
	            window.location.href = "qa_android_special.php?<?= $pagefield ?>";
			}
        }
    }

    function delete_qa_android()
    {
        if (!confirm("질문과 답변을 모두 삭제 하시겠습니까?"))
            return;

        var param = {};
        param.qaidx = "<?= $qaidx ?>";

        WG_ajax_execute("customer/delete_qa_android", param, delete_qa_android_callback);
    }

    function delete_qa_android_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.href = "qa_android_special.php?<?= $pagefield ?>";
        }
    }

    function answer_qa_android(qaidx)
    {
        if (!confirm("해당 QA를 답변 완료 처리 하시겠습니까?"))
            return;

        var param = {};
        param.qaidx = qaidx;

        WG_ajax_execute("customer/answer_qa_android", param, answer_qa_android_callback);
    }

    function answer_qa_android_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.reload(false);
        }
    }
    
    function delete_answer_android(answeridx)
    {
        if (!confirm("해당 답변을 삭제 하시겠습니까?"))
            return;

        var param = {};
        param.qaidx = "<?= $qaidx ?>";
        param.answeridx = answeridx;

        WG_ajax_execute("customer/delete_answer_android", param, delete_answer_android_callback);
    }

    function delete_answer_android_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
        //	setCookie("answer_contents", "", -1);
            window.location.reload(false);
        }
    }

    function update_answer_android(answeridx)
    {
        if (!confirm("해당 답변 등급/피드백을 저장 하시겠습니까?"))
            return;

        var param = {};
        param.qaidx = "<?= $qaidx ?>";
        param.answeridx = answeridx;
        param.level = document.getElementById('level_' + answeridx).value;
        param.feedback = document.getElementById('feedback_' + answeridx).value;

        WG_ajax_execute("customer/update_answer_android", param, update_answer_android_callback);
    }

    function update_answer_android_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.reload(false);
        }
    }
    
    function template_changed()
    {
    	var contents = input_form.template.options[input_form.template.selectedIndex].value;
    	
    	if (contents == "")
    		return; 
    	
    	contents = ReplaceText(contents, "\\[NAME\\]", "<?= $name ?>");
    	
    	CKEDITOR.instances.contents.setData(contents);
    }
    
    var g_template_list = new Array();
    
    function category_changed()
    {
       	var option = document.input_form.category.options[document.input_form.category.selectedIndex];
      	var categoryidx;
        	
       	if (option.value == "")
       		categoryidx = "";
       	else
       		categoryidx = option.value;
        	
       	var template = document.input_form.template;
        	
       	for (var i=0; i<g_template_list.length; i++)
       	{
       		template.options[template.options.length] = g_template_list[i];
       	}
        	
       	g_template_list = new Array();
        	
       	for (var i=template.options.length-1; i>=1; i--)
       	{
       		if (categoryidx != "" && template.options[i].getAttribute("categoryidx") != categoryidx)
       		{
       			g_template_list.push(template.options[i]);
       			template.options[i] = null;
       		}
       	}
    }

    function change_freetype(type)
	{
		if(type == 1)
		{
			$("#free_type_name").html("Coin");
			$("#message").val("Take5 Help Center`s issued Coins!");
		}
		else
		{
			//DoubleU Help Center's sent you Power-Ups!
			$("#free_type_name").html("");
			$("#message").val("");
		}
	}

    </script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>

<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; Android QA 상세 - 특별 대응</div>
            	<div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_qa_android()"></div>
            </div>
            <!-- //title_warp -->

            <form name="input_form" id="input_form" onsubmit="return false">

			<div style="float:left;width:600px">
				
	    	<div class="h2_title">사용자정보</div>
	    	<div class="qa_user_info_summary" onmouseover="className='qa_user_info_summary_over'" onmouseout="className='qa_user_info_summary'" onclick="window.open('/m2_user/user_view.php?useridx=<?= $useridx ?>')">
    			<img src="<?= $photourl ?>" height="50" width="50" class="summary_user_image">
    			<div class="sumary_username_wrap">
	    			<div class="summary_username">[<?= $useridx ?>] <?= $name ?> <?= ($sex == "1") ? "(M)" : "(F)" ?>&nbsp;<?= $vip_level_image?></div>
    			</div>
    			<div class="clear"></div><br/>
    			<div class="summary_user_coin"><?= number_format($coin) ?></div>
	    	</div>

			<div>
			<div style="float:left;width:295px">
	    	<div class="h2_title">결제/보상 정보</div>
	    	<div class="product_info_summary">
	    		<div class="summary_item" style="padding-top:3px">
	    			<div class="summary_item_lbl">총결제금액(WEB)</div>
	    			<div class="summary_item_value"><?= make_price_format($totalcredit) ?> credit</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">총결제금액(IOS)</div>
	    			<div class="summary_item_value">$<?= number_format($totalcredit_ios/100, 2) ?></div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">총결제금액(Android)</div>
	    			<div class="summary_item_value">$<?= number_format($totalcredit_android/100, 2) ?></div>
	    		</div>	    		
	    			    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">총취소금액(WEB)</div>
	    			<div class="summary_item_value"><?= make_price_format($totalcancel) ?> credit</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">총취소금액(IOS)</div>
	    			<div class="summary_item_value">$<?= number_format($totalcancel_ios/100, 2) ?></div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">총취소금액(Android)</div>
	    			<div class="summary_item_value">$<?= number_format($totalcancel_android/100, 2) ?></div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">10일내 결제(WEB)</div>
	    			<div class="summary_item_value"><?= make_price_format($recentcredit) ?> credit</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">10일내 결제(IOS)</div>
	    			<div class="summary_item_value">$<?= number_format($recentcredit_ios/100, 2) ?></div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">10일내 결제(Android)</div>
	    			<div class="summary_item_value">$<?= number_format($recentcredit_android/100, 2) ?></div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">10일내 취소(WEB)</div>
	    			<div class="summary_item_value"><?= make_price_format($recentcancel) ?> credit</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">10일내 취소(IOS)</div>
	    			<div class="summary_item_value">$<?= number_format($recentcancel_ios/100, 2) ?></div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">10일내 취소(Android)</div>
	    			<div class="summary_item_value">$<?= number_format($recentcancel_android/100, 2) ?></div>
	    		</div>

	    		<div class="summary_item">
	    			<div class="summary_item_lbl">총구매코인</div>
	    			<div class="summary_item_value"><?= make_price_format($totalchip["total_coin"]) ?> coin</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">총구매크레딧</div>
	    			<div class="summary_item_value"><?= make_price_format($totalchip["total_credit"]) ?> credit</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">총보상금액</div>
	    			<div class="summary_item_value"><?= make_price_format($totalsent) ?> coin <?= ($totalchip_num == 0) ? "" : "(".(round($totalsent * 1000 / $totalchip_num)/10)."%)" ?></div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">10일내 보상금액</div>
	    			<div class="summary_item_value"><?= make_price_format($recentsent) ?> coin</div>
	    		</div>
	    	</div>
		</div>
			
			<div style="float:right;width:295px">
	    	<div class="h2_title">최근 결제 내역</div>
	    	<div class="product_info_summary">
<?
	for ($i=0; $i<sizeof($orderlist); $i++)
	{
		$order_type = $orderlist[$i]["type"];
		$order_status = $orderlist[$i]["status"];
		$order_facebookcredit = $orderlist[$i]["facebookcredit"];
		$order_writedate = $orderlist[$i]["writedate"];
		
		if($order_type == "2")
			$order_facebookcredit = $order_facebookcredit/100;
?>	    		
	    		<div class="summary_item" style="<?= ($i == 0) ? "padding-top:3px" : "" ?>">
	    			<div class="summary_item_lbl" <?= ($order_status == "2") ? "style='color:red'" : "" ?>><?= $order_writedate ?></div>
	    			<div class="summary_item_value" <?= ($order_status == "2") ? "style='color:red'" : "" ?>>
<?
					if($order_type == 1)
					{
						echo make_price_format($order_facebookcredit)." credit";
					}
					else
					{
						echo "$".number_format($order_facebookcredit,2);
					}
?>
	    			</div>
	    		</div>
<?
	}
?>	    		
			</div>
</div>
						
			</div>
			
	    	<div class="h2_title">문의 정보</div>
	    	<div class="product_info_summary">
<?
	if ($qaidx != "0")
	{
?>		
    		<div class="summary_item" style="padding-top:3px;padding-bottom:5px;">
    			<div class="summary_item_lbl">문의 내용</div>
    			<div class="summary_item_value" style="word-break:break-all"><?= str_replace("\n", "<br>", encode_xml_field($contents)) ?></div>
    		</div>
    		
    		<div class="summary_item" style="padding-top:10px;padding-bottom:5px;">
    			<div class="summary_item_lbl">모바일 기기</div>
    			<div class="summary_item_value" style="word-break:break-all"><?= $mtype ?></div>
    		</div>
    		
    		<div class="summary_item" style="padding-top:10px;padding-bottom:5px;">
    			<div class="summary_item_lbl">App Version</div>
    			<div class="summary_item_value" style="word-break:break-all"><?= $appver ?></div>
    		</div>
<?
	}
	else
	{    		
?>
	    		<div class="summary_item" style="padding-top:3px">
	    			<div class="summary_item_lbl">문의 내용</div>
	    			<div class="summary_item_value" style="word-break:break-all"><textarea id="question" name="question" style="height:150px"></textarea></div>
	    		</div>
<?
	}	    		
?>
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">답변여부</div>
	    			<div class="summary_item_value">
                        <input type="radio" class="radio" name="status" value="1" <?= ($status == "1") ? "checked" : "" ?>> 답변
                        <input type="radio" class="radio ml20" name="status" value="0" <?= ($status == "0") ? "checked" : "" ?> > 미답변
					</div>
	    		</div>

	    		<div class="summary_item">
	    			<div class="summary_item_lbl">공개여부</div>
	    			<div class="summary_item_value">
                        <input type="radio" class="radio" name="ispublic" value="1" <?= ($ispublic == "1") ? "checked" : "" ?>> 공개
                        <input type="radio" class="radio ml20" name="ispublic" value="0" <?= ($ispublic == "0") ? "checked" : "" ?> > 미공개
					</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">특별 대응 문의 여부</div>
	    			<div class="summary_item_value">
                        <input type="checkbox" class="radio" name="isspecial" value="1" <?= ($isspecial == "1") ? "checked" : "" ?>>                        
					</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">Pending 여부</div>
	    			<div class="summary_item_value">
                        <input type="checkbox" class="radio" name="ispending" value="1" <?= ($ispending == "1") ? "checked" : "" ?>>
					</div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">Reporting 여부</div>
	    			<div class="summary_item_value">
                        <input type="checkbox" class="radio" name="usereporting" value="1" <?= ($usereporing == "1") ? "checked" : "" ?>>
                        
					<select name="reportingcategory" id="reportingcategory">
						<option value="">Reporting 분류 선택</option>
						<option value="General" <?= ("General" == $reportingcategory) ? "selected=\"true\"" : "" ?> >General</option>
					</select>
                        
					</div>
					
	    		</div>

	    		<div class="summary_item">
	    			<div class="summary_item_lbl">발생일시</div>
	    			<div class="summary_item_value" id="qa_occurdate" name="qa_occurdate"><?= $occurdate ?></div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">작성일시</div>
	    			<div class="summary_item_value" id="qa_writedate" name="qa_writedate"><?= $writedate ?></div>
	    		</div>
	    		
	    		<div class="summary_item">
	    			<div class="summary_item_lbl">분류</div>
	    			<div class="summary_item_value">
					<select name="category" onchange="category_changed()">
                    	<option value="">분류선택</option>
<?
	for ($i=0; $i<sizeof($categorylist); $i++)
	{
		$select_categoryidx = $categorylist[$i]["categoryidx"];
		$select_category = $categorylist[$i]["category"];
		
		if ($select_categoryidx == $categoryidx)
			$selected = "selected";
		else
			$selected = "";
?>
						<option value="<?= $select_categoryidx ?>" <?= $selected ?>><?= $select_category ?></option>
<?
	}
?>                    	
                    </select>
					</div>
	    		</div>
	    		<br/>
	    		
<?
	if ($qaidx != "0")
	{
?>			    		
                <input type="button" class="btn_setting_01" value="정보변경" onclick="save_qa_android(1)">
                
<?
	}
?>
	    		</div>	    	

			</div>
	    		    	
	    	<div style="float:right;vertical-align:top;width:470px">
	    		<div class="h2_title">기존 문의 내역</div>
	
		    	<div class="product_info_summary" style="min-height:400px;height:400px;overflow-y:scroll;cursor:default">
<?
	for ($i=0; $i<sizeof($qalist1); $i++)
	{
		$prev_qaidx = $qalist1[$i]["qaidx"];
		$contents = $qalist1[$i]["contents"];
		$writedate = $qalist1[$i]["writedate"];
		$status = $qalist1[$i]["status"];
		$isspecial = $qalist1[$i]["isspecial"];
		
		if ($status == "0")
			$status = "<b style='color:#FF3344;'>[미답변]</b> ";
		else
			$status = "";
		
		if($isspecial == 0)
			$page_url = "qa_android_write.php";
		else
			$page_url = "qa_android_special_write.php";
			
?>	
		    		<div class="summary_item" <?= ($i == 0) ? "style='border-bottom:1px dashed #cdcdcd;padding-top:3px;padding-bottom:7px;min-height:70px'" : "style='border-bottom:1px dashed #cdcdcd;padding-bottom:7px;min-height:70px'" ?>>
		    			<div class="summary_item_lbl"><?= $writedate ?><br>
		    				<input type="button" class="btn_01" value="해당 문의 이동" onclick="window.location.href='<?= $page_url ?>?qaidx=<?= $prev_qaidx ?>&<?= $pagefield ?>'" title="해당 문의로 이동">
<?
		if ($status != "")
		{
?>			
		    				<input type="button" class="btn_05" style="margin-top:2px" value="답변 완료 처리" onclick="answer_qa_android(<?= $prev_qaidx ?>)" title="답변 완료 처리">
<?
		}
?>		    				
		    			</div>
		    			<div class="summary_item_value" <?= ($prev_qaidx == $qaidx) ? "style='color:#CC4455;word-break:break-all;font-weight:bold'" : "style='word-break:break-all'" ?>><?= $status ?><?= str_replace("\n", "<br>", encode_xml_field($contents)) ?></div>
		    		</div>
<?
		for ($j=0; $j<sizeof($qalist2); $j++)
		{
			$answer_qaidx = $qalist2[$j]["qaidx"];
			$adminid = $qalist2[$j]["adminid"];
			$answer = $qalist2[$j]["answer"];
			$writedate = $qalist2[$j]["writedate"];
			$coin = $qalist2[$j]["coin"];
			$coin_memo = $qalist2[$j]["coin_memo"];
			$coin_message = $qalist2[$j]["coin_message"];
			
			if ($prev_qaidx == $answer_qaidx)
			{
				if ($coin == 0)
					$style = "border-bottom:1px dashed #cdcdcd;padding-bottom:7px";
				else
					$style = "";
?>
		    		<div class="summary_item" style="<?= $style ?>">
		    			<div class="summary_item_lbl" style="color:#5566AA"> => <?= $adminid ?><br><?= $writedate ?></div>
		    			<div class="summary_item_value" style="word-break:break-all;min-height:30px;color:#5566AA"><?= str_replace("<body", "<b", str_replace("</body", "</b", $answer)) ?></div>
		    		</div>
<?			
				if ($coin != "0")
				{
?>
		    		<div class="summary_item" style="border-bottom:1px dashed #cdcdcd;padding-bottom:7px">
		    			<div class="summary_item_lbl" style="color:#55AA66"></div>
		    			<div class="summary_item_value" style="word-break:break-all;color:#55AA66"><b><?= make_price_format($coin) ?></b> - <?= $coin_memo ?></div>
		    		</div>
<?			
				}
			}
		}
	}
?>		    		
		    	</div>
		    	
	    		<div class="h2_title">기존 보상 내역</div>
	
		    	<div class="product_info_summary" style="min-height:150px;height:150px;overflow-y:scroll;cursor:default">
<?
	for ($i=0; $i<sizeof($freecoinlist); $i++)
	{
		$free_coin = $freecoinlist[$i]["freecoin"];
		$reason = $freecoinlist[$i]["reason"];
		$admin_id = $freecoinlist[$i]["admin_id"];
		$writedate = $freecoinlist[$i]["writedate"];
		
		$free_msg = "";
		
		$free_msg = number_format($free_coin)." Coin";	
?>	
		    		<div class="summary_item" <?= ($i == 0) ? "style='border-bottom:1px dashed #cdcdcd;padding-top:3px;padding-bottom:7px;cursor:pointer'" : "style='border-bottom:1px dashed #cdcdcd;padding-bottom:7px;cursor:pointer'" ?>>
		    			<div class="summary_item_lbl"><?= $writedate ?></div>
		    			<div class="summary_item_value" style='word-break:break-all'><b><?= $free_msg ?></b> - <?= $reason ?> (발급자: <?= $admin_id?>)</div>
		    		</div>
<?
	}
?>		    		
		    	</div>		    	
	    	</div>
	    	<br/><br/>
	    	
            <table class="tbl_view_basic">
            <colgroup>
                <col width="150">
                <col width="">
            </colgroup>
            <tbody>
            	<tr>
                    <th><span></span> 답변템플릿</th>
                    <td>
                        <select name="template" id="template" onchange="template_changed()">
                        	<option value="">답변 템플릿 선택</option>
<?
	$sql = "SELECT categoryidx,title,contents FROM support_android_qa_template ORDER BY title ASC";
	$list = $db_analysis->gettotallist($sql);
	
	for ($i=0; $i<sizeof($list); $i++)
	{
		$template_categoryidx = $list[$i]["categoryidx"];
		$template_title = $list[$i]["title"];
		$template_contents = $list[$i]["contents"];
?>		
								<option value="<?= encode_html_attribute($template_contents) ?>" categoryidx="<?= $template_categoryidx ?>"><?= $template_title ?></option>
<?	
	}
?>                        	
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><span>*</span> 답변내용</th>
                    <td>
                        <textarea id="contents" name="contents"></textarea>
                        <script type="text/javascript">CKEDITOR.replace("contents",{width: '100%',height: '250px'});</script>
                    </td>
                </tr>
                <tr>
                    <th><span></span> 내부메모</th>
                    <td>
                        <textarea id="memo" name="memo" style="height:50px"></textarea>
                    </td>
                </tr>
                <tr>
                    <th><span>*</span> 답변자</th>
                    <td>
                        Jessie Moore / 난이도 설정
                        <select name="level">
                        	<option value="0">초급</option>
                        	<option value="1">중급</option>
                        	<option value="2">고급</option>
                        </select>
                    </td>
                </tr>
				<tr>
					<th>보상 자원</th>
					<td>
						<select id="free_type" name="free_type" onchange="change_freetype(this.value);">
							<option value="">선택</option>
								<option value="1" <?= ($reward_type=="1") ? "selected" : "" ?>>Coin</option>							
						</select>
					</td>
					<tr>
						<th>보상 금액</th>
						<td>
							<input type="text" class="view_tbl_text" style="width:200px" name="free_amount" id="free_amount" value="" onkeydown="return keep_price_format(this)" onkeyup="return keep_price_format(this)" onblur="return keep_price_format(this)" onkeypress="return checknum()"/>&nbsp;<span id="free_type_name" style="font-weight: bold;"></span>
						</td>
					</tr>
					<tr>
						<th>보상 사유</th>
						<td><input type="text" class="view_tbl_text" style="width:420px" name="reason" id="reason" maxlength="200" value="" /></td>
					</tr>
					<tr>
						<th>보내는 메세지</th>
						<td><input type="text" class="view_tbl_text" style="width:420px" name="message" id="message" maxlength="200" value="" /></td>
					</tr>
					<tr style="display:none;">
						<th>발급자</th>
						<td><input type="text" class="view_tbl_text" style="width:200px;" readonly="true" name="admin_id" id="admin_id" maxlength="50" value="<?= $_SESSION["adminid"]?>" /></td>
					</tr>
            </tbody>
            </table>
            </form> 
            
            <div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="정보변경/답변등록" onclick="save_qa_android(2)">
                <input type="button" class="btn_setting_02" value="취소" onclick="go_page('qa_android_special.php?<?= $pagefield ?>')">           
            </div>
            
<?
	$finalansweridx = "0";
	
	$sql = "SELECT answeridx,adminid,answer,memo,writedate,coin,coin_memo,coin_message,level,feedback FROM support_android_qa_answer WHERE qaidx=$qaidx ORDER BY answeridx";
	$list = $db_analysis->gettotallist($sql);
	
	for ($i=0; $i<sizeof($list); $i++)
	{
		$answeridx = $list[$i]["answeridx"];
		$adminid = $list[$i]["adminid"];
		$answer = $list[$i]["answer"];
		$memo = $list[$i]["memo"];
		$writedate = $list[$i]["writedate"];
		$coin = $list[$i]["coin"];
		$coin_memo = $list[$i]["coin_memo"];
		$coin_message = $list[$i]["coin_message"];
		$level = $list[$i]["level"];
		$feedback = $list[$i]["feedback"];
		
		$finalansweridx = $answeridx;
?>
			<table class="tbl_view_basic" style="border-top:1px dashed #cdcdcd;border-bottom:1px dashed #cdcdcd;margin-top:10px">
            <colgroup>
                <col width="150">
                <col width="">
            </colgroup>
            <tbody>
                <tr>
                    <th>답변자</th>
                    <td>Jessie Moore / <?= $adminid ?> <input type="button" class="btn_05" value="삭제" onclick="delete_answer_android('<?= $answeridx ?>')"></td>
                </tr>
                <tr>
                    <th>난이도/피드백</th>
                    <td>
						난이도 조정
                        <select name="level_<?= $answeridx ?>" id="level_<?= $answeridx ?>">
                        	<option value="0" <?= ($level == '0') ? "selected" : "" ?>>초급</option>
                        	<option value="1" <?= ($level == '1') ? "selected" : "" ?>>중급</option>
                        	<option value="2" <?= ($level == '2') ? "selected" : "" ?>>고급</option>
                        </select>
                        / 피드백
                        <input type="text" class="view_tbl_text" style="width:400px" name="feedback_<?= $answeridx ?>" id="feedback_<?= $answeridx ?>" maxlength="400" value="<?= encode_html_attribute($feedback) ?>" />
<?
	if ($login_adminid == "admin")
	{
?>		
                        <input type="button" class="btn_05" value="저장" onclick="update_answer_android('<?= $answeridx ?>')">
<?
	}
?>                        
					</td>
                </tr>
                <tr>
                    <th>답변내용</th>
                    <td><?= $answer ?></td>
                </tr>
                <tr>
                    <th>내부메모</th>
                    <td><?= str_replace("\n", "<br>", $memo) ?></td>
                </tr>
                <tr>
                    <th>답변일시</th>
                    <td><?= $writedate ?></td>
                </tr>
<?
		if ($coin != "0")
		{
?>
                <tr>
                    <th>보상 금액</th>
                    <td><?= make_price_format($coin) ?></td>
                </tr>
                <tr>
                    <th>보상 사유</th>
                    <td><?= $coin_memo ?></td>
                </tr>
                <tr>
                    <th>보내는 메시지</th>
                    <td><?= $coin_message ?></td>
                </tr>
<?			
		}
?>		                
            </tbody>
            </table>
<?
	}
	
	$db_main->end();
	$db_analysis->end();
?>
        </div>
        <!--  //CONTENTS WRAP -->
<script>
	category_changed();
	
	var finalansweridx = "<?= $finalansweridx ?>";
</script>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>