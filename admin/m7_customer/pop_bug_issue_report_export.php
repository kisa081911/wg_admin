<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	$qaidx = $_GET["qaidx"];
	$useridx = $_GET["useridx"];
	$platform = $_GET["platform"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="../js/common_util.js"></script>
<script type="text/javascript" src="../js/ajax_helper.js"></script>
<script type="text/javascript" src="../js/common_biz.js"></script>
<script type="text/javascript">
	var tmp_hosturl = "<?= HOST_URL ?>";
	
	function get_bug_issue_title(type)
	{
		var param = {};
		param.type = type;
		
		WG_ajax_list("customer/get_bug_issue_title", param, get_bug_issue_title_callback);
	}

	function get_bug_issue_title_callback(result, reason, totalcount, list)
	{
		if (!result)
		{
			alert("오류 발생 - " + reason);
		}
		else
		{
			var objSelect = document.getElementById("title");
			
			for(i=objSelect.length; i>=1; i--)
			{
				objSelect.options[i] = null;
			}
			
			if(list.length > 0)
			{
				for(var i=0; i<list.length; i++)
	            {
					var objOption = document.createElement("option");
	                
	                objOption.text = list[i].title;
	                objOption.value = list[i].title;
	                
	                objSelect.options.add(objOption);
	            }
			}
		}
	}
	
	function insert_bug_issue_title()
	{
		var input_form = document.input_form;

		var type = input_form.type.value;
		var title = input_form.new_title.value;
		
		if(type == "")
		{
			alert("분류를 선택하세요.");
			return;
		}

		if(title == "")
		{
			alert("새 제목을 입력하세요.");
			return;
		}

		var msg = "'" + title + "'로 생성하시겠습니까?";
		
		if(confirm(msg) == 0)
			return;
		
 	    var param = {};
		param.type = type;
	    param.title = title;
 	    
	    WG_ajax_execute("customer/insert_bug_issue_title", param, insert_bug_issue_title_callback);
	}

	function insert_bug_issue_title_callback(result, reason)
	{
	    if (!result)
	    {
	        alert("오류 발생 - " + reason);
	    }
	    else
	    {
		    alert("새 제목을 생성 하였습니다.");

		    var input_form = document.input_form;
		    var type = input_form.type.value;
		    get_bug_issue_title(type);
	    }
	}

	function insert_bug_issue_report()
	{
		var type = input_form.type.value;
		var title = input_form.title.value;
		var qaidx = input_form.qaidx.value;
		var useridx = input_form.useridx.value;
		var platform = input_form.platform.value;
		
		if(type == "")
		{
			alert("분류를 선택하세요.");
			return;
		}

		if(title == "")
		{
			alert("이슈 제목을 선택하세요.");
			return;
		}

		var param = {};
	    param.type = type;
	    param.title = title;
	    param.qaidx = qaidx;
	    param.useridx = useridx;
	    param.platform = platform;

	    WG_ajax_execute("customer/insert_bug_issue_report", param, insert_bug_issue_report_callback);
	}

	function insert_bug_issue_report_callback(result, reason)
	{
		if (!result)
	    {
	        alert("오류 발생 - " + reason);
	    }
	    else
	    {
		    alert("버그/이슈 리포트 관리 페이지에 등록하였습니다.")
		    layer_close();
	    }
	}
	
	function layer_close()
	{
	    window.parent.close_layer_popup("<?= $_GET["layer_no"] ?>");
	}
</script>
</head>

<body class="layer_body" style="width:640px" onload="window.focus()" onkeyup="if (getEventKeycode(event) == 27) { layer_close(); }">
	<div id="layer_wrap" style="width:640px;height:200px;">
	    <div class="layer_header" >  
	    	<div class="layer_title">버그/이슈 리포트 문의 등록</div>    
	    	<img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="layer_close()" />	
	    </div>
    	
    	<br/>
		<form name="input_form" id="input_form" method="get" onsubmit="return false;" style="margin-left:30px">
			<input type="hidden" id="qaidx" name="qaidx" value="<?= $qaidx ?>">
			<input type="hidden" id="useridx" name="useridx" value="<?= $useridx ?>">
			<input type="hidden" id="platform" name="platform" value="<?= $platform ?>">
		   	분류
		   	<select name="type" id="type" onchange="get_bug_issue_title(this.value);">
		    	<option value="">선택하세요</option>
		        <option value="1">User Feedback</option>
		        <option value="2">Help Center 문의</option>
		    </select>
		    
		    <br/><br/>
		    
			이슈 제목 선택
		    <select name="title" id="title">
		    	<option value="">선택하세요</option>
		    </select>
		    
		    <br/><br/>
		    
			새 제목 생성
			<input type="text" name="new_title" id="new_title" size="60" />
			<input type="button" class="btn_02" onclick="insert_bug_issue_title();" value="생성" />
			
			<div style="text-align:right;margin-top:20px;margin-right:35px">
				<input type="button" class="btn_02" onclick="insert_bug_issue_report();" value="등록" />
				<input type="button" class="btn_02" onclick="layer_close();" value="닫기" />
			</div>
	    </form>
    </div>
</body>