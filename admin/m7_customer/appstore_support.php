<?
    $top_menu = "customer";
    $sub_menu = "appstore_support";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_status = $_GET["status"];
    $search_title = $_GET["title"];
    $search_fbname = $_GET["fbname"];
    $search_email = $_GET["email"];
    $search_contents = trim($_GET["contents"]);
    
    $listcount = 10;
    $pagename = "appstore_support.php";
    $pagefield = "status=$search_status&contents=".urlencode($search_contents)."&title=".urlencode($search_title)."&email=".urlencode($search_email)."&fbname=".urlencode($search_fbname);
    
    $db_analysis = new CDatabase_Analysis();
    
    $tail = "WHERE 1=1";
    
    if ($search_status != "")
    	$tail .= " AND status=$search_status ";
    
    if ($search_title != "")
    	$tail .= " AND title LIKE '%$search_title%'";
    
    if ($search_fbname != "")
    	$tail .= " AND fbname LIKE '%$search_fbname%'";
    
    if ($search_email != "")
    	$tail .= " AND email LIKE '%$search_email%'";
    
    if ($search_contents != "")
    	$tail .= " AND contents LIKE '%$search_contents%'";
        
    $totalcount = $db_analysis->getvalue("SELECT COUNT(*) FROM support_mobile_qa2 $tail");
    
    $sql = "SELECT * FROM support_mobile_qa2 $tail ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;

    $qalist = $db_analysis->gettotallist($sql);
    
    $db_analysis->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
?>      
	<script type="text/javascript">
		function fnSetQAStatus(qaidx)
		{
			var param = {};
	        param.qaidx = qaidx;

	        WG_ajax_execute("customer/update_mobileqa", param, fnSetQAStatus_callback);
		}

		function fnSetQAStatus_callback(result, reason)
		{
			document.getElementById("status_" + reason).innerHTML = "답변";
		}

		function search_press(e)
	    {
	        if (((e.which) ? e.which : e.keyCode) == 13)
	        {
	            search();
	        }
	    }
	    
	    function search()
	    {
	        var search_form = document.search_form;
	        search_form.submit();
	    }
	</script>
        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; Appstore 서포트 <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
            </div>
            <!-- //title_warp -->
            
            <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="appstore_support.php">
                <div class="detail_search_wrap">
                	<span class="search_lbl ml20">제목</span>
                    <input type="text" class="search_text" id="title" name="title" style="width:170px" value="<?= encode_html_attribute($search_title) ?>" onkeypress="search_press(event)" />
                
                	<span class="search_lbl ml20">문의내용</span>
                    <input type="text" class="search_text" id="contents" name="contents" style="width:170px" value="<?= encode_html_attribute($search_contents) ?>" onkeypress="search_press(event)" />

                    <span class="search_lbl ml20">이메일</span>
                    <input type="text" class="search_text" id="email" name="email" style="width:170px" value="<?= encode_html_attribute($search_email) ?>" onkeypress="search_press(event)" />
                    
                    <span class="search_lbl ml20">FB Name</span>
                    <input type="text" class="search_text" id="fbname" name="fbname" style="width:170px" value="<?= encode_html_attribute($search_fbname) ?>" onkeypress="search_press(event)" />

                    <div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
                    
                	<div class="clear" style="padding-top:10px"></div>
                	
                	<span class="search_lbl ml20">답변</span>
                    <input type="radio" name="status" value="" <?= ($search_status == "") ? "checked" : "" ?>>전체 <input type="radio" name="status" value="1" <?= ($search_status == "1") ? "checked" : "" ?>>답변 <input type="radio" name="status" value="0" <?= ($search_status == "0") ? "checked" : "" ?>>미답변
                	
                </div>
            </form>
            
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="5">
                <col width="">
                <col width="250">
                <col width="120">
                <col width="120">
            </colgroup>
            <thead>
            <tr>
                <th></th>
                <th class="tdc">제목, 문의내용</th>
                <th class="tdc">이메일/FB Name</th>
                <th class="tdc">상태</th>
                <th class="tdc">작성일</th>
            </tr>
            </thead>
            <tbody>
<?
    for ($i=0; $i<sizeof($qalist); $i++)
    {
        $qaidx = $qalist[$i]["qaidx"];
        $title = $qalist[$i]["title"];
        $fbname = $qalist[$i]["fbname"];
        $contents = $qalist[$i]["contents"];
        $email = $qalist[$i]["email"];
        $status = $qalist[$i]["status"];
        $writedate = $qalist[$i]["writedate"];
?>
            <tr>
                <td class="tdc"></td>
                <td class="point_title" style="word-break:break-all;">제목: [<?= $title ?>]<br/><br/>내용: <?= $contents ?></td>
                <td class="point_title"><?= $email ?> / <?= $fbname?></td>
                <td id="status_<?= $qaidx?>" class="tdc"><?= ($status == 0) ? "<b style='color:red;'>미답변</b><br/><input class=\"btn_search\" onclick=\"fnSetQAStatus('".$qaidx."')\" type=\"button\" value=\"답변완료\"/>" : "답변" ?></td>
                <td class="tdc"><?= $writedate ?></td>
            </tr>
<?
    } 
?>
            </tbody>
            </table>
            
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
        </div>
        <!--  //CONTENTS WRAP -->        

<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>