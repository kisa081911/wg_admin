<?
    $top_menu = "customer";
    $sub_menu = "mobile_amazon_category";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    
    $listcount = 10;
    $pagename = "mobile_amazon_category.php";
    $pagefield = "";
    
    $db_analysis = new CDatabase_Analysis();
        
    $totalcount = $db_analysis->getvalue("SELECT COUNT(*) FROM support_amazon_category");
    
    $sql = "SELECT categoryidx,seqno,category FROM support_amazon_category ORDER BY seqno LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    $categorylist = $db_analysis->gettotallist($sql);
    
    $db_analysis->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script>
    function mobile_amazon_category_write(categoryidx)
    {
        window.location.href = "mobile_amazon_category_write.php?categoryidx="+categoryidx;
    }

    function move_mobile_amazon_category(categoryidx, movetype)
    {
        var param = {};
        param.categoryidx = categoryidx;
        param.movetype = movetype;
            
        WG_ajax_execute("customer/move_mobile_amazon_category", param, move_mobile_amazon_category_callback);
    }
    
    function move_mobile_amazon_category_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            window.location.reload(false);
        }
    }
</script>       
        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; Amazon QA 분류 관리 <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
            </div>
            <!-- //title_warp -->
            
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="5">
                <col width="100">
                <col width="">
                <col width="120">
            </colgroup>
            <thead>
            <tr>
                <th></th>
                <th class="tdc">번호</th>
                <th class="tdc">분류</th>
                <th>우선순위</th>
            </tr>
            </thead>
            <tbody>
<?
    for ($i=0; $i<sizeof($categorylist); $i++)
    {
        $categoryidx = $categorylist[$i]["categoryidx"];
        $category = $categorylist[$i]["category"];
        $seqno = $categorylist[$i]["seqno"];
?>
            <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="mobile_amazon_category_write(<?= $categoryidx ?>)">
                <td class="tdc"></td>
                <td class="tdc point"><?= $seqno ?></td>
                <td class="point_title"><?= $category ?></td>
                <td class="tdc point">
                    <img src="/images/icon/up.png" align="absmiddle" onclick="event.cancelBubble=true;move_mobile_amazon_category('<?= $categoryidx ?>', 'up')" />
                    <img src="/images/icon/down.png" align="absmiddle" onclick="event.cancelBubble=true;move_mobile_amazon_category('<?= $categoryidx ?>', 'down')" />
                </td>
            </tr>
<?
    } 
?>
            </tbody>
            </table>
            
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
            <div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="QA 분류 추가" onclick="window.location.href='mobile_amazon_category_write.php'">          
            </div>
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>