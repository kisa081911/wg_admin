<?
    $top_menu = "customer";
    $sub_menu = "faq";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    // ckeditor 경로 설정
    include_once($_SERVER["DOCUMENT_ROOT"]."/ckeditor/ckeditor.php");
    
    $faqidx = $_GET["faqidx"];
    
    check_number($faqidx);
    
    $db_analysis = new CDatabase_Analysis();
    
    if ($faqidx != "")
    {       
        $sql = "SELECT faqidx,title,contents,status,faq_sort,writedate,top_categoryidx,categoryidx FROM support_faq WHERE faqidx=$faqidx";
        $faqs = $db_analysis->getarray($sql);
        
        $faqidx = $faqs["faqidx"];
        $title = $faqs["title"];
        $contents = $faqs["contents"];
        $status = $faqs["status"];
        $faq_sort = $faqs["faq_sort"];
        $writedate = $faqs["writedate"];
        $top_categoryidx = $faqs["top_categoryidx"];
        $categoryidx = $faqs["categoryidx"];

        if ($faqidx == "")
            error_back("존재하지 않는 FAQ입니다.");
    }
    else
    {
        $status = "1";
    }
    
	$sql = "SELECT categoryidx,top_categoryidx,category,".
		"(SELECT category FROM support_category WHERE categoryidx=A.top_categoryidx) AS top_category ".
		" FROM support_category A WHERE top_categoryidx<>0 ORDER BY (SELECT seqno FROM support_category WHERE categoryidx=A.top_categoryidx),seqno";
	
	$categorylist = $db_analysis->gettotallist($sql);
	
    $db_analysis->end();
?>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script>
    function save_faq()
    {
        var input_form = document.input_form;
        
        if (input_form.title.value == "")
        {
            alert("제목을 입력하세요.");
            input_form.title.focus();
            return;
        }
        
        if (input_form.category.value == "")
        {
            alert("분류를 선택하세요.");
            input_form.category.focus();
            return;
        }

        if (CKEDITOR.instances.contents.getData() == "")
        {
            alert("내용을 입력하세요.");
            CKEDITOR.instances.contents.focus();
            return;
        }
        
        var param = {};
        param.faqidx = "<?= $faqidx ?>";
        param.title = input_form.title.value;
        param.top_categoryidx = input_form.category.options[input_form.category.selectedIndex].getAttribute("top_categoryidx");
        param.categoryidx = input_form.category.options[input_form.category.selectedIndex].value;
        param.contents = CKEDITOR.instances.contents.getData();
        param.status = get_radio("status");

        if ("<?= $faqidx ?>" == "")
        	WG_ajax_execute("customer/save_faq", param, save_faq_callback);
        else
        	WG_ajax_execute("customer/update_faq", param, save_faq_callback);
    }

    function save_faq_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            window.location.href = "faq.php";
        }
    }

    function delete_faq()
    {
        if (!confirm("FAQ를 삭제 하시겠습니까?"))
            return;

        var param = {};
        param.faqidx = "<?= $faqidx ?>";

        WG_ajax_execute("customer/delete_faq", param, delete_faq_callback);
    }

    function delete_faq_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.href = "faq.php";
        }
    }
</script>
        
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; FAQ 상세</div>
<?
    if ($faqidx != "")
    { 
?>
		<div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_faq()"></div>
<?
    } 
?>
	</div>
	<!-- //title_warp -->

	<form name="input_form" id="input_form" onsubmit="return false">
		<table class="tbl_view_basic">
			<colgroup>
				<col width="150">
				<col width="">
            </colgroup>
            <tbody>
                <tr>
                    <th><span>*</span> 제목</th>
                    <td><input type="text" class="view_tbl_text" name="title" id="title" value="<?= encode_html_attribute($title) ?>" maxlength="1024"></td>
                </tr>
                <tr>
                    <th><span>*</span> 분류</th>
                    <td><select name="category">
                    	<option value="">분류선택</option>
<?
	for ($i=0; $i<sizeof($categorylist); $i++)
	{
		$select_top_categoryidx = $categorylist[$i]["top_categoryidx"];
		$select_categoryidx = $categorylist[$i]["categoryidx"];
		$select_top_category = $categorylist[$i]["top_category"];
		$select_category = $categorylist[$i]["category"];
		
		if ($select_categoryidx == $categoryidx)
			$selected = "selected";
		else
			$selected = "";
?>
						<option value="<?= $select_categoryidx ?>" top_categoryidx="<?= $select_top_categoryidx ?>" <?= $selected ?>><?= $select_top_category ?>/<?= $select_category ?></option>
<?
	}
?>                    	
                    </select></td>
                </tr>
                <tr>
                    <th><span>*</span> 내용</th>
                    <td>
                        <textarea id="contents" name="contents"><?= $contents ?></textarea>
                        <script type="text/javascript">CKEDITOR.replace("contents",{width: '100%',height: '350px'});</script>
                    </td>
                </tr>
                <tr>
                    <th><span></span> 사용여부</th>
                    <td>
                        <input type="radio" class="radio" name="status" value="1" <?= ($status == "1") ? "checked" : "" ?>> 사용
                        <input type="radio" class="radio ml20" name="status" value="0" <?= ($status == "0") ? "checked" : "" ?> > 미사용
                    </td>
                </tr>
            </tbody>
		</table>    
	</form> 
            
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="저장" onclick="save_faq()">
		<input type="button" class="btn_setting_02" value="취소" onclick="go_page('faq.php')">           
	</div>
</div>
<!--  //CONTENTS WRAP -->    
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
