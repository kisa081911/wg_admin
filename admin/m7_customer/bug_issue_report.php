<?
    $top_menu = "customer";
    $platform = ($_GET["platform"] == "") ? "0" : $_GET["platform"];
    
    if($platform == 0)
    {
	    $sub_menu = "bug_issue_report_web";
	    $sub_title = "Web";
    }
    else if($platform == 1)
    {
	    $sub_menu = "bug_issue_report_ios";
	    $sub_title = "iOS";
    }
    else if($platform == 2)
    {
	    $sub_menu = "bug_issue_report_android";
	    $sub_title = "Android";
    }
    else if($platform == 3)
    {
	    $sub_menu = "bug_issue_report_amazon";
	    $sub_title = "Amazon";
    }
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    
    $search_type = $_GET["type"];
    $search_title = $_GET["title"];
    $search_useridx = trim($_GET["search_useridx"]);
    $search_start_writedate = ($_GET["search_start_writedate"] == "") ? date("Y-m-d", strtotime("-7 days")) : $_GET["search_start_writedate"];
    $search_end_writedate = ($_GET["search_end_writedate"] == "") ? date("Y-m-d") : $_GET["search_end_writedate"];
    $search_viewcnt = ($_GET["search_viewcnt"] == "") ? 30 : $_GET["search_viewcnt"];
	
    $listcount = $search_viewcnt;
    
    if($search_type != "")
    	$tail .= " AND type = $search_type ";
    
    if($search_title != "")
    	$tail .= " AND title = '$search_title' ";
    
    if($search_useridx != "")
    	$tail .= " AND useridx = $search_useridx ";
    
    $db_analysis = new CDatabase_Analysis();
    
    $sql = "SELECT * 
    		FROM support_bug_issue_report 
    		WHERE platform = $platform AND writedate BETWEEN '$search_start_writedate 00:00:00' AND '$search_end_writedate 23:59:59' $tail 
    		ORDER BY title ASC, writedate DESC 
    		LIMIT ".(($page-1) * $listcount).", ".$listcount;
    $bug_issue_list = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT COUNT(*)
    		FROM support_bug_issue_report
    		WHERE platform = $platform AND writedate BETWEEN '$search_start_writedate 00:00:00' AND '$search_end_writedate 23:59:59' $tail";
    $totalcount = $db_analysis->getvalue($sql);
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script>
	window.onload = function()
	{
		for(i=0; i<document.getElementById("type").length; i++)
		{
			if(document.getElementById("type").options[i].selected)
			{
				get_bug_issue_title(document.getElementById("type").options[i].value)
			}
		}
	}
	
	function select_bug_issue_export(platform) 
	{ 
		var holder = document.getElementById( "table_form" );
		var checkboxes = holder.getElementsByClassName( "chkbox" );
		var issueidx_list = "";
		
		for(var i=0; i<checkboxes.length; i++)
		{
			if(checkboxes[i].checked == true)
			{
				if(issueidx_list == "")
					issueidx_list = checkboxes[i].value;
				else
					issueidx_list += "," + checkboxes[i].value;
			}
		}
	
		if(issueidx_list == "")
		{
			alert("선택한 이슈가 없습니다.");
			return;
		}

		window.location.href = 'bug_issue_report_download_excel.php?issueidx_list=' + issueidx_list + "&platform=" + platform;   
	}

	function delete_bug_issue_report() 
	{ 
		var holder = document.getElementById( "table_form" );
		var checkboxes = holder.getElementsByClassName( "chkbox" );
		var issueidx_list = "";
		
		for(var i=0; i<checkboxes.length; i++)
		{
			if(checkboxes[i].checked == true)
			{
				if(issueidx_list == "")
					issueidx_list = checkboxes[i].value;
				else
					issueidx_list += "," + checkboxes[i].value;
			}
		}
	
		if(issueidx_list == "")
		{
			alert("선택한 이슈가 없습니다.");
			return;
		}

		var msg = "선택한 이슈를 삭제하시겠습니까?";
		
		if(confirm(msg) == 0)
			return;

		var param = {};
		param.issueidx_list = issueidx_list;
 	    
		WG_ajax_execute("customer/delete_bug_issue_report", param, delete_bug_issue_report_callback);   
	}

	function delete_bug_issue_report_callback(result, reason)
	{
		if (!result)
	    {
	        alert("오류 발생 - " + reason);
	    }
	    else
	    {
	    	window.location.reload(false);
	    }
	}

	function get_bug_issue_title(type)
	{
		var param = {};
		param.type = type;
		
		WG_ajax_list("customer/get_bug_issue_title", param, get_bug_issue_title_callback);
	}

	function get_bug_issue_title_callback(result, reason, totalcount, list)
	{
		if (!result)
		{
			alert("오류 발생 - " + reason);
		}
		else
		{
			var objSelect = document.getElementById("title");
			
			for(i=objSelect.length; i>=1; i--)
			{
				objSelect.options[i] = null;
			}
			
			if(list.length > 0)
			{
				for(var i=0; i<list.length; i++)
	            {
					var objOption = document.createElement("option");
	                
	                objOption.text = list[i].title;
	                objOption.value = list[i].title;
	                
	                objSelect.options.add(objOption);
	            }
			}

			var select_title = document.getElementById("select_title").value;

			if(select_title != "")
			{
				for(j=0; j<document.getElementById("title").length; j++)
				{
					if(document.getElementById("title").options[j].value == select_title)
					{
						
						document.getElementById("title").options[j].seleted = true;
						$("#title").val(select_title);
						break;
					}
				}
			}
		}
	}
	
	var bindCheckboxes = ( 
			function() {
				var _last_selected = null;
				
				return function() {
					var holder = document.getElementById( "table_form" );
	
					if ( holder === null ) 
						return;
					
					var checkboxes = holder.getElementsByClassName( "chkbox" );
					var index = 0;
					
					for ( var i = 0; i < checkboxes.length; ++i ) 
					{
						if ( "checkbox" !== checkboxes[ i ].getAttribute( "type" ) ) 
							continue;
						
						( function( ix ) {
							checkboxes[ ix ].addEventListener( "click", 
									function( event ) {
										var checked = this.checked;
										
										if ( event.shiftKey && ix != _last_selected ) 
										{
											var start = Math.min( ix, _last_selected ), stop = Math.max( ix, _last_selected );
											
											for ( var j = start; j <= stop; ++j ) 
											{
												checkboxes[ j ].checked = checked;
											}
	
											_last_selected = null;
										} 
										else 
										{
											_last_selected = ix;
										}
									}, false );
		      			})( index );
		      			
		      			index++;
		    		}
					
		  		}
			}
	)();
	document.addEventListener( "DOMContentLoaded", function() { bindCheckboxes( "table_form" );}, false );
    
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }

    $(function() {
        $("#start_writedate").datepicker({ });
    });
    
    $(function() {
        $("#end_writedate").datepicker({ });
    });
</script>
	
    <!-- CONTENTS WRAP -->
    <div class="contents_wrap">
    
        <!-- title_warp -->
        <div class="title_wrap">
            <div class="title"><?= $top_menu_txt ?> &gt; 버그/이슈 리포트 문의 관리 - <?= $sub_title ?> <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
        </div>
        <!-- //title_warp -->
        
        <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="bug_issue_report.php" style="width:1300px">
            <div class="detail_search_wrap">
            	<input type="hidden" id="select_title" name="select_title" value="<?= $search_title ?>">
            	<span class="search_lbl ml20">분류</span>
			   	<select name="type" id="type" onchange="get_bug_issue_title(this.value);">
			    	<option value="">선택하세요</option>
			        <option value="1" <?= ($search_type == 1) ? "selected" : "" ?>>User Feedback</option>
			        <option value="2" <?= ($search_type == 2) ? "selected" : "" ?>>Help Center 문의</option>
			    </select>
			    
			    <span class="search_lbl ml20">등록일</span>
                <input type="text" class="search_text" id="start_writedate" name="start_writedate" style="width:65px" value="<?= $search_start_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
                <input type="text" class="search_text" id="end_writedate" name="end_writedate" style="width:65px" value="<?= $search_end_writedate?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
			    
			    <div class="floatr">
			    	<select name="search_viewcnt" id="search_viewcnt">
			    		<option value="20" <?= ($search_viewcnt == 20) ? "selected" : "" ?>>20개씩 보기</option>
			    		<option value="25" <?= ($search_viewcnt == 25) ? "selected" : "" ?>>25개씩 보기</option>
			    		<option value="30" <?= ($search_viewcnt == 30) ? "selected" : "" ?>>30개씩 보기</option>
			    		<option value="50" <?= ($search_viewcnt == 50) ? "selected" : "" ?>>50개씩  보기</option>
	    			</select>
	    		</div>
			    
			    <br/><br/>
				
				<span class="search_lbl ml20">제목</span>
			    <select name="title" id="title">
			    	<option value="">선택하세요</option>
	    		</select>
				
				 
                <span class="search_lbl ml20">useridx</span>
                <input type="text" class="search_text" id="search_useridx" name="search_useridx" style="width:100px" value="<?= encode_html_attribute($search_useridx) ?>" onkeypress="search_press(event)" />

				<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>

	            <div class="clear" style="padding-top:10px"></div>
            </div>
        </form>

		<form id="table_form" style="width:1300px">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="5">
                <col width="120">
                <col width="">
                <col width="230">
                <col width="230">
                <col width="120">
                <col width="50">
                
            </colgroup>
            <thead>
            <tr>
                <th></th>
                <th>분류</th>
                <th class="tdl">문의내용</th>
                <th>이름</th>
                <th>이슈 제목</th>
                <th>등록일</th>
                <th>선택</th>
            </tr>
            </thead>
            <tbody>
<?
	$db_main = new CDatabase_Main();

    for ($i=0; $i<sizeof($bug_issue_list); $i++)
    {
        
        $issueidx = $bug_issue_list[$i]["issueidx"];
        $type = $bug_issue_list[$i]["type"];
        $title = $bug_issue_list[$i]["title"];
        $useridx = $bug_issue_list[$i]["useridx"];
        $facebookid = $bug_issue_list[$i]["facebookid"];
        $username = $bug_issue_list[$i]["name"];
        $contents = $bug_issue_list[$i]["contents"];
        $writedate = $bug_issue_list[$i]["writedate"];
		
        if($type == 1)
			$type = "User Feedback";
		else if($type == 2)
			$type = "Help Center 문의";		

		$sql = "SELECT COUNT(*) FROM tbl_user_online_sync2 WHERE useridx=$useridx";
		$online_status = $db_main->getvalue($sql);
      
?>
	            <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="">
		            <td class="tdc"></td>
		            <td class="tdc"><?= $type ?></td>		            
		            <td class="point_title"><?= encode_xml_field($contents) ?></td>
		            <td class="point_title"><span style="float:left;padding-top:8px;padding-right:2px;"><img src="/images/icon/<?= ($online_status == "1") ? "status_1.png" : "status_3.png" ?>" align="absmiddle" /></span>  <span style="float:left;"><img src="https://graph.facebook.com/<?= $facebookid ?>/picture?type=square&access_token=<?=$client_accesstoken?>" height="25" width="25" align="absmiddle" hspace="3"></span> <?= $username ?></td>
		            <td class="tdc"><?= $title ?></td>
		            <td class="tdc"><?= $writedate ?></td>
		            <td class="tdc"><div><input type="checkbox" value="<?= $issueidx ?>" class="chkbox"></div></td>
		        </tr>
<?
    } 

	$db_main->end();
	$db_analysis->end();
?>
            </tbody>
            </table>
<?
	if($totalcount > 0)
	{
?>	            
            <div class="button_warp tdr">
				<input type="button" class="btn_setting_01" value="선택한 이슈 추출" onclick="select_bug_issue_export(<?= $platform ?>)"><br/><br/>
				<input type="button" class="btn_setting_01" value="선택한 이슈 삭제" onclick="delete_bug_issue_report(<?= $platform ?>)">
			</div>
<?
	}
?> 
		</form>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
	</div>
	<!--  //CONTENTS WRAP -->
        
	<div class="clear"></div>

<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>