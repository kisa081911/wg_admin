<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	check_login();
	
	$issueidx_list = $_GET["issueidx_list"];
	$platform = $_GET["platform"];
	
	if($platform == 0)
		$filename = "[Take5-WEB]bug_issue_report_".date("Y-m-d").".xls";
	else if($platform == 1)
		$filename = "[Take5-iOS]bug_issue_report_".date("Y-m-d").".xls";
	else if($platform == 2)
		$filename = "[Take5-Android]bug_issue_report_".date("Y-m-d").".xls";
	else if($platform == 3)
		$filename = "[Take5-Amazon]bug_issue_report_".date("Y-m-d").".xls";
	
	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT * FROM support_bug_issue_report WHERE issueidx IN ($issueidx_list) ORDER BY type ASC";
	$bug_issue_list = $db_analysis->gettotallist($sql);
	
	$sql = "SELECT COUNT(type) AS rowspan FROM support_bug_issue_report WHERE issueidx IN ($issueidx_list) GROUP BY type ORDER BY type ASC";
	$rowspan_list = $db_analysis->gettotallist($sql);
	
	if (sizeof($bug_issue_list) == 0)
		error_go("저장할 데이터가 없습니다.", "bug_issue_report.php?platform=".$platform);
	
	$db_analysis->end();
	
	$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
	"<table border=1>".
		"<tr>".
			"<td style='font-weight:bold;text-align:center;font-size:15;'>분류</td>".
			"<td style='font-weight:bold;text-align:center;font-size:15;'>이슈 제목</td>".
			"<td style='font-weight:bold;text-align:center;font-size:15;'>useridx / 이름</td>".
			"<td style='font-weight:bold;text-align:center;font-size:15;'>보유칩</td>".
			"<td style='font-weight:bold;text-align:center;font-size:15;'>총 누적 결제액 / VIP 레벨</td>".
			"<td style='font-weight:bold;text-align:center;font-size:15;'>모바일 정보</td>".
			"<td style='font-weight:bold;text-align:center;font-size:15;'>문의 내용</td>".
		"</tr>";
	
		$tmp_type = 0;
		$rowspan_index = 0;
		
		for ($i=0; $i<sizeof($bug_issue_list); $i++)
		{
			$type = $bug_issue_list[$i]["type"];;
			$title = $bug_issue_list[$i]["title"];
			$useridx = $bug_issue_list[$i]["useridx"];
			$username = $bug_issue_list[$i]["username"];
			$coin = $bug_issue_list[$i]["coin"];
			$money = $bug_issue_list[$i]["money"];
			$member_level = $bug_issue_list[$i]["member_level"];
			$mtype = $bug_issue_list[$i]["mtype"];
			$contents = $bug_issue_list[$i]["contents"];
  		
			if($primeumdate != "0000-00-00 00:00:00")
				$primeumdate = "프리미엄 회원 <br style='mso-data-placement:same-cell;'> $primeumdate 까지";
			
  			if ($member_level == "0")
  				$member_level = "Bronze";
  			if ($member_level == "1")
  				$member_level = "Silver";
  			if ($member_level == "2")
  				$member_level = "Gold";
  			if ($member_level == "3")
  				$member_level = "Platinum";
  			if ($member_level == "4")
  				$member_level = "Sapphire";
  			if ($member_level == "5")
  				$member_level = "Emerald";
  			if ($member_level == "6")
  				$member_level = "Diamond";
  			if ($member_level == "7")
  				$member_level = "Ruby";
  			if ($member_level == "8")
  				$member_level = "Black";
  			if ($member_level == "9")
  				$member_level = "Premier";
  			if ($member_level == "10")
  				$member_level = "Chairman";
  			
  			$excel_contents .= "<tr>";
  			
  			if($tmp_type != $type)
  			{
  				if($type == 1)
  					$type_str = "User Feedback";
  				if($type == 2)
  					$type_str = "Help Center 문의";
  				
  				$excel_contents .= "<td style='width:300px;text-align:center;' rowspan='".$rowspan_list[$rowspan_index++]["rowspan"]."'>".$type_str."</td>";
  				
  			}
  			$excel_contents .= "<td style='width:300px;text-align:center;'>".$title."</td>".
		  						"<td style='width:200px;text-align:center;'>[".$useridx."] ".$username."</td>".
		  						"<td style='width:150px;text-align:center;'>".number_format($coin)."</td>".
		  						"<td style='width:200px;text-align:center;'>$".$money." / ".$member_level."</td>".
		  						"<td style='width:250px;text-align:center;'>".$mtype."</td>".
		  						"<td style='width:400px'>".$contents."</td>".
	  							"</tr>";
  			$tmp_type = $type;
  		}
						  
		$excel_contents .= "</table>";
  
		Header("Content-type: application/x-msdownload");
		Header("Content-type: application/x-msexcel");
		Header("Content-Disposition: attachment; filename=$filename");
  
		echo($excel_contents);
?>