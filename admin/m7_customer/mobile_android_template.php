<?
    $top_menu = "customer";
    $sub_menu = "mobile_android_template";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_title = $_GET["title"];
    $search_category = $_GET["category"];
    
    $listcount = 10;
    $pagename = "mobile_android_template.php";
    $pagefield = "title=$search_title&category=$search_category";
    
    $db_analysis = new CDatabase_Analysis();
    
    $tail = "WHERE 1=1";
    
    if($search_title != "")
    	$tail .= " AND title LIKE '%$search_title%'";
    
    if($search_category != "")
    	$tail .= " AND categoryidx='$search_category'";
    
    $totalcount = $db_analysis->getvalue("SELECT COUNT(*) FROM support_android_qa_template $tail");
    
    $sql = "SELECT templateidx,title,writedate,categoryidx,".
    	"(SELECT category FROM support_android_category WHERE categoryidx=support_android_qa_template.categoryidx) AS category ".
    	" FROM support_android_qa_template $tail ORDER BY title ASC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    $templatelist = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT categoryidx,category FROM support_android_category";
    
    $categorylist = $db_analysis->gettotallist($sql);
    
    $db_analysis->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script type="text/javascript">
    function mobile_android_template_write(templateidx)
    {
        window.location.href = "mobile_android_template_write.php?templateidx="+templateidx;
    }

    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    } 
</script>       
        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; Android QA 답변 템플릿 관리 <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
            </div>
            <!-- //title_warp -->
            
            <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
            	<div class="detail_search_wrap">
            		<span class="search_lbl ml20">제목</span>
                    <input type="text" class="search_text" id="title" name="title" style="width:200px" value="<?= encode_html_attribute($search_title) ?>" onkeypress="search_press(event)" />
                    
                    <span class="search_lbl ml20">분류</span>
                    <select name="category" id="category">
                    	<option value="">분류 선택</option>
                    	<option value="0" <?= ($search_category == "0" ? "selected" : "")?>>General</option>
<?
	for ($i=0; $i<sizeof($categorylist); $i++)
	{
		$select_categoryidx = $categorylist[$i]["categoryidx"];
		$select_category = $categorylist[$i]["category"];
		
		if ($select_categoryidx == $search_category)
			$selected = "selected";
		else
			$selected = "";
?>
						<option value="<?= $select_categoryidx ?>" <?= $selected ?>><?= $select_category ?></option>
<?
	}
?>
                    </select>
                    
                    <div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
            	</div>
            </form>
            
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="5">
                <col width="">
                <col width="300">
                <col width="120">
            </colgroup>
            <thead>
            <tr>
                <th></th>
                <th class="tdc">제목</th>
                <th class="tdc">분류</th>
                <th>작성일</th>
            </tr>
            </thead>
            <tbody>
<?
    for ($i=0; $i<sizeof($templatelist); $i++)
    {
        $templateidx = $templatelist[$i]["templateidx"];
        $title = $templatelist[$i]["title"];
        $category = $templatelist[$i]["category"];
        $categoryidx = $templatelist[$i]["categoryidx"];
        $writedate = $templatelist[$i]["writedate"];
?>
            <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="mobile_android_template_write(<?= $templateidx ?>)">
                <td class="tdc"></td>
                <td class="point_title"><?= $title ?></td>
                <td class="tdc point_title"><?= ($categoryidx == "0") ? "General" : $category ?></td>
                <td class="tdc"><?= $writedate ?></td>
            </tr>
<?
    } 
?>
            </tbody>
            </table>
            
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
            <div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="QA 답변 템플릿 추가" onclick="window.location.href='mobile_android_template_write.php'">          
            </div>
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>