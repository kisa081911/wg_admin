<?
    $top_menu = "user";
    $sub_menu = "redmine_action";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_number = ($_GET["search_number"] == "") ? "" : $_GET["search_number"];
    $search_useridx = ($_GET["search_useridx"] == "") ? "" : $_GET["search_useridx"];
    $search_os = ($_GET["search_os"] == "") ? "" : $_GET["search_os"];

    $listcount = 10;
    $pagename = "redmine_action.php";
    
    $tail = "WHERE 1=1";
    
    if($search_number != "")
    	$tail .= " AND redmine_number = $search_number ";
    
    if($search_useridx != "")
    	$tail .= " AND useridx = '$search_useridx' ";
    
    if($search_os != "")
    	$tail .= " AND os = '$search_os' ";
    
    $pagefield = "search_number=$search_number&search_useridx=$search_useridx&search_os=$search_os";
    
    $db_main = new CDatabase_Main();
    $db_analysis = new CDatabase_Analysis();
    
    $sql = "SELECT redmine_number, useridx, os FROM `user_action_log_blackbox` ".
      		"$tail ".
      		"GROUP BY redmine_number, useridx, os ".
      		"ORDER BY redmine_number DESC ".
    		"LIMIT ".(($page-1) * $listcount).", ".$listcount;

    $actionlist = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT redmine_number, useridx, os FROM `user_action_log_blackbox` ".
    		"$tail ".
    		"GROUP BY redmine_number, useridx, os ";
    
    $totallist = $db_analysis->gettotallist($sql);
    		
    $totalcount = sizeof($totallist);
    
    $db_main->end();
    $db_analysis->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script type="text/javascript">  
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }

    function pop_user_actionlist(redmine, useridx, os)
    {
        open_layer_popup("pop_user_redmine_actionlist.php?redmine="+redmine+"&useridx="+useridx+"&os="+os, 950, 670);
    }

    function pop_user_gamelist(redmine, useridx, os)
    {
        open_layer_popup("pop_user_redmine_gamelist.php?redmine="+redmine+"&useridx="+useridx+"&os="+os, 950, 670);
    }

    function pop_user_clientlist(redmine, useridx, os)
    {
        open_layer_popup("pop_user_redmine_clientlist.php?redmine="+redmine+"&useridx="+useridx+"&os="+os, 950, 670);
    }

    function delete_actionlog_blackbox()
    {
    	open_layer_popup("pop_blackbox_delete.php", 550, 150);
    }
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 레드마인 이슈 행동로그 관리 (<?= make_price_format($totalcount) ?>)</div>
                <div class="title_button"><input type="button" class="btn_01" value="완료된 이슈 삭제" onclick="delete_actionlog_blackbox()"></div>
            </div>
            <!-- //title_warp -->
            
            <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
                <div class="detail_search_wrap">
                    <span class="search_lbl ml20">레드마인 이슈 번호</span>
                    <input type="text" class="search_text" id="search_number" name="search_number" style="width:50px" value="<?= $search_number ?>" onkeypress="search_press(event)" />
                    
                    <span class="search_lbl ml20">Useridx</span>
                    <input type="text" class="search_text" id="search_useridx" name="search_useridx" style="width:100px" value="<?= $search_useridx ?>" onkeypress="search_press(event)" />
                    
                    <span class="search_lbl ml20">OS</span>
                    <select name="search_os" id="search_os">
                    	<option value="">선택하세요</option>
						<option value="0" <?= ($search_os=="0") ? "selected" : "" ?>>Web</option>
						<option value="1" <?= ($search_os=="1") ? "selected" : "" ?>>iOS</option>
						<option value="2" <?= ($search_os=="2") ? "selected" : "" ?>>Android</option>
						<option value="3" <?= ($search_os=="3") ? "selected" : "" ?>>Amazon</option>
					</select>
					
					<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
                </div>
            </form>
                        
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
            </colgroup>
            <thead>
                <tr>
                    <th>레드마인 이슈 번호</th>
                    <th>Useridx</th>
                    <th>OS</th>
                    <th>로그 목록</th>
                </tr>
            </thead>
            <tbody>
<?
    for ($i=0; $i<sizeof($actionlist); $i++)
    {
        $redmine_number = $actionlist[$i]["redmine_number"];
        $useridx = $actionlist[$i]["useridx"];
        $os = $actionlist[$i]["os"];
        $total = $actionlist[$i]["total"];
        
        if($os == "0")
        	$char_os = "Web";
        else if($os == "1")
        	$char_os = "IOS";
        else if($os == "2")
        	$char_os = "And";
        else if($os == "3")
        	$char_os = "Amazon";
?>
            <tr>
                <td class="tdc"><?= $redmine_number ?></td>
                <td class="tdc"><?= $useridx ?></td>
                <td class="tdc"><?= $char_os ?></td>
                <td class="tdc">
                	<input type="button" class="btn_03" value="액션로그 보기" onclick="pop_user_actionlist(<?= $redmine_number ?>, <?= $useridx?>, <?= $os?>)">
                	<input type="button" class="btn_03" value="게임로그 보기" onclick="pop_user_gamelist(<?= $redmine_number ?>, <?= $useridx?>, <?= $os?>)">
                	<input type="button" class="btn_03" value="통신로그 보기" onclick="pop_user_clientlist(<?= $redmine_number ?>, <?= $useridx?>, <?= $os?>)">
                </td>
            </tr>
<?
    }
?>
            </tbody>
            </table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>

            <div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="블랙박스 추가" onclick="window.location.href='redmine_action_write.php'">
            </div>            
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>