<?
	$top_menu = "user";
	$sub_menu = "user_mng";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$page = ($_GET["page"] == "") ? "1" : $_GET["page"];
	$search_order_by = ($_GET["orderby"] == "") ? "createdate desc" : $_GET["orderby"];
	
	$search_field = $_GET["search_field"];
	$search_start_value = $_GET["start_value"];
	$search_end_value = $_GET["end_value"];
	$search_nickname = trim($_GET["nickname"]);
	$search_useridx = trim($_GET["useridx"]);
	$search_userid = trim($_GET["userid"]);
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	$search_start_logindate = $_GET["start_logindate"];
	$search_end_logindate = $_GET["end_logindate"];
	$search_isblock = $_GET["isblock"];
	
	check_xss($search_nickname);
	
	$listcount = "10";
	$pagename = "user_mng.php";
	$pagefield = "orderby=$search_order_by&search_field=$search_field&start_value=$search_start_value&end_value=$search_end_value&nickname=$search_nickname&useridx=$search_useridx&userid=$search_userid&start_createdate=$search_start_createdate&end_createdate=$search_end_createdate&start_logindate=$search_start_logindate&end_logindate=$search_end_logindate&isblock=$search_isblock";
	
	$tail = " WHERE A.useridx > 10000 ";
	$order_by = "ORDER BY ".str_replace(":", " ", $search_order_by);
	
	if ($search_field == "coin" || $search_field == "logincount" || $search_field == "level" || $search_field == "experience")
	{
		if ($search_start_value != "")
			$tail .= " AND $search_field >= $search_start_value";
		 
		if ($search_end_value != "")
			$tail .= " AND $search_field <= $search_end_value";
	}
	
	if ($search_nickname != "")
		$tail .= " AND A.nickname LIKE '%$search_nickname%'";
	
	if ($search_useridx != "")
		$tail .= " AND A.useridx='$search_useridx'";
	
	if ($search_userid != "")
		$tail .= " AND userid='$search_userid'";
	
	if ($search_isblock != "")
		$tail .= " AND B.isblock=$search_isblock";
	
	if ($search_start_createdate != "")
		$tail .= " AND A.createdate >= '$search_start_createdate 00:00:00'";
	
	if ($search_end_createdate != "")
		$tail .= " AND A.createdate <= '$search_end_createdate 23:59:59'";
	
	if ($search_start_logindate != "")
		$tail .= " AND B.logindate >= '$search_start_logindate 00:00:00'";
	
	if ($search_end_logindate != "")
		$tail .= " AND B.logindate <= '$search_end_logindate 23:59:59'";
	
	$db_main = new CDatabase_Main();
	
	if($tail != " WHERE A.useridx > 10000 ")
		$totalcount = $db_main->getvalue("SELECT COUNT(*) FROM tbl_user AS A JOIN tbl_user_ext AS B ON A.useridx=B.useridx $tail");
	
	$join = " JOIN tbl_user_ext AS B ON A.useridx=B.useridx";
	
	$sql = "SELECT A.useridx, userid, A.nickname, coin, level, experience, A.createdate, B.logindate, ".
      		"(SELECT COUNT(*) FROM tbl_user_online_sync2 WHERE useridx=A.useridx) AS online_status ".
        	"FROM tbl_user AS A $join $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;
	
	if($tail != " WHERE A.useridx > 10000 ")
		$userlist = $db_main->gettotallist($sql);
	
    if ($totalcount < ($page-1) * $listcount && page != 1)
	    $page = floor(($totalcount + $listcount - 1) / $listcount);
	
	$db_main->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<link type="text/css" href="/js/themes/base/jquery.ui.datepicker.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery-ui.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>  
<script type="text/javascript">
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;
            
        if (search_form.search_field.value == "" && (search_form.start_value.value != "" || search_form.end_value.value != ""))
        {
            alert("검색필드를 선택하세요.");
            search_form.search_field.focus();
            return;
        } 

        search_form.submit();
    }
    
    function check_sleeptime()
    {
        setTimeout("window.location.reload(false)",60000);
    }

    function go_view(useridx)
    {
        window.open("user_view.php?useridx="+useridx);
    } 

    $(function() {
        $("#start_createdate").datepicker({ });
    });
    
    $(function() {
        $("#end_createdate").datepicker({ });
    });

    $(function() {
        $("#start_logindate").datepicker({ });
    });
    
    $(function() {
        $("#end_logindate").datepicker({ });
    });
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 사용자목록 <span class="totalcount"><?= ($totalcount == "") ? "" : "(".make_price_format($totalcount).")" ?></span></div>
	</div>
	<!-- //title_warp -->
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="user_mng.php">
		<div class="detail_search_wrap">
			<span class="search_lbl">검색필드</span>
			<select name="search_field" id="search_field">
				<option value="" <?= ($search_field == "") ? "selected" : "" ?>>검색 필드</option>
				<option value="coin" <?= ($search_field == "coin") ? "selected" : "" ?>>Coin</option>				
				<option value="level" <?= ($search_field == "level") ? "selected" : "" ?>>레벨</option>
				<option value="experience" <?= ($search_field == "experience") ? "selected" : "" ?>>경험치</option>
				<option value="logincount" <?= ($search_field == "logincount") ? "selected" : "" ?>>로그인횟수</option>
			</select>
			<input type="text" class="search_text" id="start_value" name="start_value" style="width:60px" value="<?= $search_start_value ?>" onkeypress="search_press(event); return checknum();" /> ~
			<input type="text" class="search_text" id="end_value" name="end_value" style="width:60px" value="<?= $search_end_value ?>" onkeypress="search_press(event); return checknum();" />
                    
			<span class="search_lbl ml20">이름</span>
			<input type="text" class="search_text" id="nickname" name="nickname" style="width:120px" value="<?= encode_html_attribute($search_nickname) ?>" onkeypress="search_press(event)" />
                    
			<span class="search_lbl ml20">Facebook ID</span>
			<input type="text" class="search_text" id="userid" name="userid" style="width:120px" value="<?= encode_html_attribute($search_userid) ?>" onkeypress="search_press(event); return checkonlynum();" />
                    
			<span class="search_lbl ml20">UserIdx&nbsp;</span>
			<input type="text" class="search_text" id="useridx" name="useridx" style="width:150px" value="<?= encode_html_attribute($search_useridx) ?>" onkeypress="search_press(event)" />
                    
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
			<div class="clear"  style="padding-top:10px"></div>
			
			<span class="search_lbl">가입일&nbsp;&nbsp;&nbsp;</span>
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" style="width:65px" value="<?= $search_start_createdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" style="width:65px" value="<?= $search_end_createdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
			
			<span class="search_lbl ml20">최근로그인</span>
			<input type="text" class="search_text" id="start_logindate" name="start_logindate" style="width:65px" value="<?= $search_start_logindate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
			<input type="text" class="search_text" id="end_logindate" name="end_logindate" style="width:65px" value="<?= $search_end_logindate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
			
			<span class="search_lbl ml20">
            	<input type="checkbox" value="1" name="isblock" <?= ($search_isblock == "1") ? "checked" : "" ?> align="absmiddle" />차단 여부                
            </span>                    
		</div>                
	</form>
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="50">
			<col width="210">
			<col width="100">
			<col width="50">
			<col width="30">
			<col width="70">
			<col width="100">
			<col width="50">               
		</colgroup>
		<thead>
            <tr>
                <th>번호</th>
                <th>이름</th>
                <th class="tdr">Coin</th>                
                <th>레벨</th>
                <th>경험치</th>
                <th>가입일</th>
                <th>최근로그인</th>
                <th></th>
            </tr>
		</thead>
		<tbody>
<?
    for($i=0; $i<sizeof($userlist); $i++)
    {
        $useridx = $userlist[$i]["useridx"];
        $userid = $userlist[$i]["userid"];
        $nickname = $userlist[$i]["nickname"];
        
        if($userid < 10)
        	$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
        else
        	$photourl = get_fb_pictureURL($userid,$client_accesstoken);
        
        $coin = $userlist[$i]["coin"];
        $level = $userlist[$i]["level"];
        $experience = $userlist[$i]["experience"];
        $online_status = $userlist[$i]["online_status"];
        $createdate = $userlist[$i]["createdate"];
        $logindate = $userlist[$i]["logindate"];
?>
            <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''" onclick="view_user_dtl(<?= $useridx ?>,'')">
                <td class="tdc"><?=  $totalcount - (($page-1) * $listcount) - $i ?></td>
                <td class="point_title"><span style="float:left;padding-top:8px;padding-right:2px;"><img src="/images/icon/<?= ($online_status == "1") ? "status_1.png" : "status_3.png" ?>" align="absmiddle" /></span>  <span style="float:left;"><img src="<?= $photourl ?>" height="25" width="25" align="absmiddle" hspace="3"></span> <?= $nickname ?>&nbsp;&nbsp;<img src="/images/icon/facebook.png" height="20" width="20" align="absmiddle" style="cursor:pointer" onclick="event.cancelBubble=true;window.open('http://www.facebook.com/<?= $userid ?>')" /></td>
                <td class="tdr point"><?= make_price_format($coin) ?></td>
                <td class="tdc point"><?= $level ?></td>
                <td class="tdc point"><?= $experience ?></td>
                <td class="tdc"><?= substr($createdate, 0, 10) ?></td>
                <td class="tdc"><?= (substr($logindate, 0, 10) != "0000-00-00") ? $logindate : "" ?></td>
                <td class="tdc"><input type="button" class="btn_03" value="새창보기" style="cursor:pointer" onclick="event.cancelBubble=true;go_view(<?= $useridx ?>)" /></td>
            </tr>
<?
    } 
?>
		</tbody>
	</table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>