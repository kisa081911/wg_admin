<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	$useridx = $_POST["useridx"];
	
	$db_main2 = new CDatabase_Main2();
	
	$sql = "SELECT COUNT(*) FROM tbl_event_result WHERE useridx=$useridx";
	$list_totalcnt = $db_main2->getvalue($sql);
	
	$db_main2->end();
?>
<script type="text/javascript">
	var g_page = 1;

	$(document).ready(function()
	{
		refresh();
	});

	function refresh()
	{
		var param = {};
		param.page = g_page;
		param.useridx = "<?= $useridx ?>";

		WG_ajax_list("user/get_user_event_list", param, fnContentLoad_callback, true);
	}

	function fnContentLoad_callback(result, reason, totalcount, list)
	{
		if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            update_pagenation(g_page, <?= $list_totalcnt?>, 10, 10);
            
            var tbody = document.getElementById("list_contents");
            
            while (tbody.childNodes.length > 0)
                tbody.removeChild(tbody.childNodes[0]);

            for (var i=0; i<list.length; i++)
            {
                var tr = document.createElement("tr");
                var td = document.createElement("td");

                tr.appendChild(td);
                td.className = "tdl point";
                td.innerText = list[i].title;

				var reward_name = "";
                
				if(list[i].reward_type == "1")
					reward_name = "Coin";
				else if(list[i].reward_type == "2")
					reward_name = "TY Point";
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdr";
                td.innerText = make_price_format(list[i].reward_amount) + " " + reward_name;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].writedate;
                
                tbody.appendChild(tr);
            }
        }
	}
</script>
<div id="layer_wrap">
	<div class="layer_header" >
        <div class="layer_title">이벤트 참여 목록</div>
    	<img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>
    
    <div class="layer_contents_wrap" style="width:800px;height:500px;">
    	 <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
	            <table class="tbl_list_basic1" width="780px;">
	                <colgroup>
	                    <col width="*">
						<col width="150">
						<col width="150">
	                </colgroup>
	                <thead>
	                    <tr>
	                        <th>이벤트</th>
							<th>지급량</th>
							<th>참여일</th>
	                    </tr>
	                </thead>
	                <tbody id="list_contents">
	                </tbody>
	            </table>  
            	<div class="pagenation mt20" id=pagenation></div>
            </form>  
         </div>
    </div>
</div>