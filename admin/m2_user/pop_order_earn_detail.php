<? 
    include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
    
    $useridx = $_POST["useridx"];
    
    if ($useridx == "")
        error_close_layer("잘못된 접근입니다.");    
    
    $db_main = new CDatabase_Main();
    
    $sql = "SELECT orderidx,usercoin,orderno,coin,status,writedate ".      		
			"FROM tbl_product_order_earn WHERE useridx=$useridx AND status IN (1,2) ORDER BY writedate DESC";
    	
	$orderlist = $db_main->gettotallist($sql);
    	
	$ordertotalcount = sizeof($orderlist);    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>슬롯별 상세보기</title>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script>
</head>
<body class="layer_body" onload="window.focus()" onkeyup="if (getEventKeycode(event) == 27) { fnPopupClose(); }">
<div id="layer_wrap" style="height:700px;">   
    <div class="layer_header" >
        <div class="layer_title">주문정보 상세보기</div>
        <img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>        
    <div class="layer_contents_wrap" style="width: 1170px;height:650px;padding-bottom:0px;">
         <!--  레이어 내용  -->
<?
	if ($ordertotalcount != "0")
	{
	
		for($i=0; $i<sizeof($orderlist); $i++)
		{
			$orderidx = $orderlist[$i]["orderidx"];
			$usercoin = $orderlist[$i]["usercoin"];			
			$orderno = $orderlist[$i]["orderno"];
			$coin = $orderlist[$i]["coin"];			
			$status = $orderlist[$i]["status"];
			$writedate = $orderlist[$i]["writedate"];		
		
		$money_str = "Earn Credit";		
?>
          <div class="user_order_unit" onmouseover="className='user_order_unit_over'" onmouseout="className='user_order_unit'" onclick="" <?= ($status == "2") ? "style='border:1px solid red'" : "" ?>>
              <dl class="user_order">
                 <dt <?= ($status == "2") ? "style='color:red'" : "" ?>>Trialpay Earn</dt>
                 <dd><span>사용자 보유 Coin</span> $<?= make_price_format($usercoin) ?></dd>                 
                 <dd><span>결제 금액</span> $<?=  number_format($coin,2) ?></dd>                                             
                 <dd><span>Earn 주문번호</span> <?= $orderno ?></dd>                            
                 <dd><span>주문상태</span>
<?
   					 if ($status == 0)
        				echo("주문진행중");
    				else if ($status == 1)
        				echo("주문완료");
    				else if ($status == 2)
        				echo("<span style='color:red'>취소 (".substr($canceldate, 0, 10).")</span>");
?>
                 </dd>
                <dd><span>주문일시</span> <?= $writedate ?></dd>
                </dl>
            </div>
<?
			}
        }
?>
    </div>
  </div>
<?    
    $db_main->end();
?>
</div>
</body>
</html>