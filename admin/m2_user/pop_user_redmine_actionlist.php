<? 
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");

    check_login_layer();
    
    $redmine = $_GET["redmine"];
    $useridx = $_GET["useridx"];
    $os = $_GET["os"];
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    
    if ($redmine == "" || $useridx == "" || $os == "")
        error_close_layer("잘못된 접근입니다.");
   $db_analysis = new CDatabase_Analysis();
   
   $tail = "WHERE redmine_number=$redmine AND useridx = $useridx AND os = $os";
   $totalcount = $db_analysis->getvalue("SELECT COUNT(*) FROM user_action_log_blackbox $tail");
   
   $db_analysis->end();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>블랙박스 액션로그</title>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script>
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
	var tmp_hosturl = "<?= HOST_URL ?>";
    function layer_close()
    {
        window.parent.close_layer_popup("<?= $_GET["layer_no"] ?>");
    }
    
    function window_onload()
    {
        refresh();
    }
    
    var g_page=1;
    function refresh()
    { 
        var input_form = document.input_form;
        
        var param = {};
        
        param.page = g_page;
        param.redmine= "<?= $redmine ?>";
        param.useridx= "<?= $useridx ?>";
        param.os= "<?= $os ?>";
        
        WG_ajax_list("user/get_blackbox_action_list", param, get_blackbox_action_list_callback, true);
    }

    function get_blackbox_action_list_callback(result, reason, totalcount, list)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            update_pagenation(g_page, "<?=$totalcount?>", 12, 10);
            
            var tbody = document.getElementById("list_contents");
            
            while (tbody.childNodes.length > 0)
                tbody.removeChild(tbody.childNodes[0]);
                
            for (var i=0; i<list.length; i++)
            {
                var tr = document.createElement("tr");
                var td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].ipaddress;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].category;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].category_info;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].action;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].action_info;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].staytime;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].isplaynow;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                
                if (list[i].coin == "-1")
                    td.innerText = "-";
                else
                    td.innerText = make_price_format(list[i].coin);
                    
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].writedate;
                
                tbody.appendChild(tr);
            }
        }
    }
</script>
</head>
<body class="layer_body"  onload="try { window_onload() } catch(e) {}" onkeyup="if (getEventKeycode(event) == 27) { layer_close(); }">
<div id="layer_wrap">   
    <div class="layer_header" >
        <div class="layer_title">사용자 행동 로그목록</div>
        <img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="layer_close()" />
    </div>        
    <div class="layer_contents_wrap" style="width:920px;height:600px">
         <!--  레이어 내용  -->
         
         <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
            <table class="tbl_list_basic1">
                <colgroup>
                    <col width="100">
                    <col width="100">
                    <col width="100">
                    <col width="100">
                    <col width="100">
                    <col width="100">
                    <col width="100">
                    <col width="100">
                    <col width="120"> 
                </colgroup>
                <thead>
                    <tr>
                        <th>IP 주소</th>
                        <th>category</th>
                        <th>category_info</th>
                        <th>action</th>
                        <th>action_info</th>
                        <th>staytime</th>
                        <th>isplaynow</th>
                        <th>coin</th>
                        <th>접속일시</th>
                    </tr>
                </thead>
                <tbody id="list_contents">
                </tbody>
            </table>  
            <div class="pagenation mt20" id=pagenation></div>
            </form>  
         </div>
    </div>
</div>
</body>
</html>

