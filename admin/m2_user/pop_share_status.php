<? 
    include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
    
    check_login_layer();
    
    $useridx = $_REQUEST["useridx"];
    
    $pagename = "pop_share_status.php";
    $pagefield = "useridx=$useridx&layer_no=".$_GET["layer_no"];
    
    if ($useridx == "")
        error_close_layer("잘못된 접근입니다.");
    
    $db_main2 = new CDatabase_Main2();
    
    $totalcount = $db_main2->getvalue("SELECT COUNT(*) FROM tbl_share_chips WHERE useridx=$useridx");
    
    $db_main2->end();
?>
<script type="text/javascript">
	var g_page = 1;

	$(document).ready(function()
	{
		refresh();
	});
			
	function refresh()
	{
		var param = {};
		param.page = g_page;
		param.useridx = "<?= $useridx ?>";

		WG_ajax_list("user/get_share_list", param, fnContentLoad_callback, true);
	}

	function fnContentLoad_callback(result, reason, totalcount, list)
	{
		if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            update_pagenation(g_page, <?= $totalcount?>, 10, 10);
            
            var tbody = document.getElementById("list_contents");
            
            while (tbody.childNodes.length > 0)
                tbody.removeChild(tbody.childNodes[0]);

            for (var i=0; i<list.length; i++)
            {
                var tr = document.createElement("tr");
                var td = document.createElement("td");
				var sharetype = list[i].sharetype;
				var sharetype_str;	
				
                if(sharetype == "1")
    				sharetype_str = "Bigwin";
    			else if(sharetype == "2")
    				sharetype_str = "Jackpot";
    			else if(sharetype == "3")
    				sharetype_str = "X2 Chance";
    			else if(sharetype == "4")
    				sharetype_str = "Celeb Lv UP";
    			else if(sharetype == "5")
    				sharetype_str = "Slot Promotion";
    			else if(sharetype == "8")
    				sharetype_str = "Season Event";
				

                tr.appendChild(td);
                td.className = "tdc point";
                td.innerText = list[i].sharecode;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = sharetype_str;
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = make_price_format(list[i].totalamount);
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = make_price_format(list[i].shareamount);
                    
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].status;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].collect_cnt;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].writedate;
                
                tbody.appendChild(tr);
            }
        }
	}
    
</script>
</head>

<div id="layer_wrap">   
    <div class="layer_header" >
        <div class="layer_title">Share 현황 상세보기</div>
        <img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>        
    
    <div class="layer_contents_wrap" style="width:800px;height:510px">
         <!--  레이어 내용  -->
         
         <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
            <table class="tbl_list_basic1" style="width:100%;">
                <colgroup>
            		<col width="">
            		<col width="">
            		<col width="">
            		<col width="">
            		<col width="">
            		<col width="">
            		<col width="">
            		<col width="">
            	</colgroup>
                <thead>
                    <tr>
                        <th class="tdc">ShareCode</th>
                        <th class="tdc">ShareType</th>
                        <th class="tdr">Total Price</th>
                        <th class="tdr">Share Price</th>
                        <th class="tdc">완료 여부</th>
                        <th class="tdr">획득 유저 수</th>
                        <th class="tdc">일시</th>
                    </tr>
                </thead>
                <tbody id="list_contents">
				</tbody>
				
            </table>  
			<div class="pagenation mt20" id=pagenation></div>
            </form>  
         </div>
    </div>
</div>

