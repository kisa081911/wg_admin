<? 
	$adminid = $_POST["adminid"];
   	$useridx = $_POST["useridx"];
    
    if ($useridx == "")
        error_close_layer("잘못된 접근입니다.");
?>
<script type="text/javascript">
    function update_block_user()
    {
        var input_form = document.input_form;

        if (input_form.reason.value == "")
        {
            alert("차단 사유를 입력해주세요.");
            input_form.reason.focus();
            return;
        }

        var param = {};
        param.useridx = '<?= $useridx ?>';
        param.admin_id = '<?= $adminid?>';
        param.isblock = '1';
        param.reason = input_form.reason.value;        
        
        WG_ajax_execute("user/update_user_block", param, update_user_block_callback, false);
    }
    
    function update_user_block_callback(result, reason)
    {
        var input_form = document.input_form; 
        
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
        	alert("해당 사용자 Block 처리 했습니다.");
            window.location.reload(false);
        }
    } 
</script>
<div id="layer_wrap">   
    <div class="layer_header" >
        <div class="layer_title">사용자 차단</div>
        <img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>        
    <div class="layer_contents_wrap" style="width:640px">
         <!--  레이어 내용  -->         
         <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
            <table class="tbl_view_basic" style="width:630px">
                <colgroup>
                    <col width="120">
                    <col width="">
                </colgroup>
                    <tbody>
                         <tr>
                             <th>차단 사유</th>
                             <td><input type="text" class="view_tbl_text" style="width:420px" name="reason" id="reason" maxlength="200" value="" /></td>
                         </tr>
                     </tbody>
             </table>  
             </form>  
         </div>
         
         <!-- 확인 버튼 -->
         <div class="layer_button_wrap" style="width:620px;text-align:right;">
            <input type="button" class="btn_02" value="차단" onclick="update_block_user()" />
            <input type="button" class="btn_02" value="닫기" onclick="fnPopupClose()" />
         </div>
         <!-- 확인 버튼 -->
         <!--  //레이어 내용  -->
    </div>
</div>