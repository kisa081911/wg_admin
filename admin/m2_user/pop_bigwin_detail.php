<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	$useridx = $_POST["useridx"];
	
	$db_main2 = new CDatabase_Main2();
	
	$sql = "SELECT COUNT(*) FROM tbl_bigwin_log WHERE useridx = $useridx";
	$list_totalcnt = $db_main2->getvalue($sql);
	
	$sql = "SELECT slottype, slotname FROM tbl_slot_list";
    $slottype_list = $db_main2->gettotallist($sql);
	
	$db_main2->end();
?>
<script type="text/javascript">
	var g_page = 1;
	var slotlist = [];
	<?
		for($i=0; $i<sizeof($slottype_list); $i++)
		{
			$slottype = $slottype_list[$i]["slottype"];
			$slotname = $slottype_list[$i]["slotname"];
	?>
			slotlist[<?= $slottype?>] = "<?= $slotname?>";
	<?
		}
	?>
		

	$(document).ready(function()
	{
		refresh();
	});

	function refresh()
	{
		var param = {};
		param.page = g_page;
		param.useridx = "<?= $useridx ?>";

		WG_ajax_list("user/get_bigwin_list", param, fnContentLoad_callback, true);
	}

	function fnContentLoad_callback(result, reason, totalcount, list)
	{
		if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            update_pagenation(g_page, <?= $list_totalcnt?>, 10, 10);
            
            var tbody = document.getElementById("list_contents");
            
            while (tbody.childNodes.length > 0)
                tbody.removeChild(tbody.childNodes[0]);

            for (var i=0; i<list.length; i++)
            {
                var tr = document.createElement("tr");
                var td = document.createElement("td");

                var slotname = "";
				var gradename = "";
				var amount = 0;
				
		        if(list[i].grade == 1)
		        	gradename = "BIG WIN";
		        else if(list[i].grade == 2)
		        	gradename = "SUPER WIN";
		        else if(list[i].grade == 3)
		        	gradename = "MEGA WIN";
		        else if(list[i].grade == 4)
		        	gradename = "ULTRA WIN";
		        else if(list[i].grade == 5)
		        	gradename = "GRAND WIN";
			else if(list[i].grade == 6)
		        	gradename = "GRAND WIN(6)";
		        else if(list[i].grade == 7)
		        	gradename = "GRAND WIN(7)";
		        else if(list[i].grade == 8)
		        	gradename = "GRAND WIN(8)";
		        else if(list[i].grade == 9)
		        	gradename = "GRAND WIN(9)";
		        else if(list[i].grade == 10)
		        	gradename = "GRAND WIN(10)";

	        	if(list[i].objectidx == 0)
	        		bigwin_location = "Ultra";
	        	else if(list[i].objectidx < 1000000)
	        		bigwin_location = "레귤러";
	        	else if(list[i].objectidx >= 1000000)
	        		bigwin_location = "하이롤러";	        
		        
		        amount = list[i].amount;	        	
                
                tr.appendChild(td);
                td.className = "tdc point";
                td.innerText = gradename;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = slotlist[list[i].slottype];
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = make_price_format(amount);

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = bigwin_location;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].writedate;
                
                tbody.appendChild(tr);
            }
        }
	}
</script>
<div id="layer_wrap">
	<div class="layer_header" >
        <div class="layer_title">BigWin 로그</div>
    	<img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>
    
    <div class="layer_contents_wrap" style="width:700px;height:435px;">
    	 <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
	            <table class="tbl_list_basic1" width="1200px;">
	                <colgroup>
	                    <col width="">
	                    <col width="">
	                    <col width="">	                    
	                    <col width="">
	                    <col width="">	                    
	                </colgroup>
	                <thead>
	                    <tr>
							<th>BigWin Name</th>
							<th>Slot</th>
							<th>BigWin Amount</th> 
							<th>BigWin Location</th>                
							<th>획득일</th>
	                    </tr>
	                </thead>
	                <tbody id="list_contents">
	                </tbody>
	            </table>  
            	<div class="pagenation mt20" id=pagenation></div>
            </form>  
         </div>
    </div>
</div>