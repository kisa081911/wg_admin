<?
	$adminid = $_POST["adminid"];
	$useridx = $_POST["useridx"];
	$category = $_POST["category"];
	$facebookid = $_POST["facebookid"];
	$nickname = $_POST["nickname"];
	$honorlevel = $_POST["honorlevel"];
?>

<script type="text/javascript">
	function save_free_resource()
	{
		var free_amount = strip_price_format($("#free_amount").val());
		var free_reason = $("#reason").val();
		var free_message = $("#message").val();
		var free_adminid = $("#admin_id").val();		

		if (free_amount == "")
        {			         
            alert("발급량을 입력해주세요.");
            $("#free_amount").focus();
            return;
        }

		if (free_reason == "")
        {			         
            alert("발급사유를 입력해주세요.");
            $("#reason").focus();
            return;
        }

		if (free_message == "")
        {			         
            alert("보내는 메세지를 입력해주세요.");
            $("#message").focus();
            return;
        }

		var param = {};
		param.useridx = '<?= $useridx ?>';		
		param.free_amount = free_amount;
		param.reason = free_reason;
		param.message = free_message;
		param.admin_id = free_adminid;
		param.category = '<?= $category ?>';
		param.facebookid = '<?= $facebookid ?>';
		param.nickname = '<?= $nickname ?>';
		param.honorlevel = '<?= $honorlevel ?>';

		WG_ajax_execute("user/save_free_resource", param, save_free_resource_callback, false);
	}

	function save_free_resource_callback(result, reason)
	{
		var input_form = document.input_form; 
        
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            alert("해당 사용자에게 무료자원을 발급했습니다.");
            window.location.reload(false);
        }
	}
</script>
<div id="layer_wrap">
	<div class="layer_header" >
        <div class="layer_title"><?= ($category == "coin") ? "코인" : "TY"  ?> 수동 발급</div>
    	<img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>
    
    <div class="layer_contents_wrap" style="width:640px">
    	<div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
            <table class="tbl_view_basic" style="width:630px">
                <colgroup>
                    <col width="120">
                    <col width="">
                </colgroup>
                    <tbody>
                         <tr>
                         	<th>발급량</th>
                         	<td>
                         		<input type="text" class="view_tbl_text" style="width:200px" name="free_amount" id="free_amount" value="" onkeydown="return keep_price_format(this)" onkeyup="return keep_price_format(this)" onblur="return keep_price_format(this)" onkeypress="return checknum()"/>&nbsp;<span id="free_type_name" style="font-weight: bold;"></span>
                         	</td>
                         </tr>
                         <tr>
                             <th>발급 사유</th>
                             <td><input type="text" class="view_tbl_text" style="width:420px" name="reason" id="reason" maxlength="200" value="" /></td>
                         </tr>
                         <tr>
                             <th>보내는 메세지</th>
                             <td><input type="text" class="view_tbl_text" style="width:420px" name="message" id="message" maxlength="200" value="" /></td>
                         </tr>
                         <tr>
                            <th>발급자</th>
                            <td><input type="text" class="view_tbl_text" style="width:200px;" readonly="true" name="admin_id" id="admin_id" maxlength="50" value="<?= $adminid?>" /></td>
                         </tr>
                     </tbody>
             </table>  
             </form>  
         </div>
         
    	<!-- 확인 버튼 -->
		<div class="layer_button_wrap" style="width:620px;text-align:right;">
			<input type="button" class="btn_02" value="발급" onclick="save_free_resource()" />
			<input type="button" class="btn_02" value="닫기" onclick="fnPopupClose()" />
		</div>
		<!-- 확인 버튼 -->
    </div>
</div>