<?	
	include($_SERVER["DOCUMENT_ROOT"]."/common/common_include.inc.php");
	
	$useridx = $_GET["useridx"];
	
	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT logidx,useridx,ipaddress,useragent,adflag,coin,writedate FROM user_login_log WHERE useridx=$useridx ORDER BY logidx DESC";
	$login_log_list = $db_analysis->gettotallist($sql);
	
	$db_analysis->end();
	
   	$EXCEL_STR = "
		<table border='1'>
			<tr>
				<td>IP 주소</td>
				<td>브라우저</td>
				<td>Adflag</td>   				
				<td>보유코인</td>
    			<td>접속일시</td>    			
			</tr>";
    
        
    for($i=0; $i<sizeof($login_log_list); $i++)
    {
    	$ipaddress = $login_log_list[$i]["ipaddress"];
        $useragent = $login_log_list[$i]["useragent"];
        $adflag = $login_log_list[$i]["adflag"];
        $coin = $login_log_list[$i]["coin"];
        $writedate = $login_log_list[$i]["writedate"];
			
		$EXCEL_STR .= "
		<tr>
			<td>".$ipaddress."</td>
			<td>".$useragent."</td>
			<td>".$adflag."</td>
			<td>".$coin."</td>			
			<td>".$writedate."</td>
		</tr> ";
				
    }
    
    $EXCEL_STR .= "</table>";
    
    header("Pragma: public");
    header("Expires: 0");
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=login_log_$useridx.xls");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Content-Description: PHP5 Generated Data");

	echo $EXCEL_STR;    
?>