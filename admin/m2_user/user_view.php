<?
    $top_menu = "user";
    $sub_menu = "user_mng";
    $category = $_GET["category"];
    $list_page = "user_mng.php";
   
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $useridx = $_GET["useridx"];
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    
    if ($useridx == "")
       error_back("잘못된 접근입니다.");
    
    $pagename = "user_view.php";
    $pagefield = "useridx=$useridx";
    
    $dbaccesstype = $_SESSION["dbaccesstype"];
    
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    $db_analysis = new CDatabase_Analysis();
    $db_friend = new CDatabase_Friend();
    $db_other = new CDatabase_Other(); 
    $db_mobile = new CDatabase_Mobile();  
    $db_livestats = new CDatabase_Livestats();
    
    $client_accesstoken= $db_main2->getvalue("SELECT accesstoken FROM `tbl_client_accesstoken`");
        
    $sql = "SELECT A.useridx, A.userid, A.userkey, A.nickname, A.sex, A.coin, A.level, A.honor_level, A.honor_point, A.experience, A.createdate, B.logindate, ".
    		"B.email ,B.country, B.locale, B.adflag, B.logincount, B.isblock, ".
    		"(SELECT vip_level FROM tbl_user_detail WHERE useridx=A.useridx) AS vip_level, ".
    		"(SELECT vip_point FROM tbl_user_detail WHERE useridx=A.useridx) AS vip_point, ".
    		"(SELECT ty_point FROM tbl_user_detail WHERE useridx=A.useridx) AS ty_point, ".
    		"(SELECT COUNT(*) FROM tbl_user_online_sync2 WHERE useridx=A.useridx) AS online_status ".
    		" FROM tbl_user AS A JOIN tbl_user_ext AS B ON A.useridx=B.useridx WHERE A.useridx=$useridx";
    
    $user = $db_main->getarray($sql);
    
    $useridx = $user["useridx"];
    $userid = $user["userid"];
    $userkey= $user["userkey"];
    $nickname = $user["nickname"];
    
    if($userid < 10)    
    	$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
    else 
        $photourl = get_fb_pictureURL($userid,$client_accesstoken);
    
    $sex = $user["sex"];
    $coin = $user["coin"];    
    $level = $user["level"];
    $honor_level = $user["honor_level"];
    $honor_point = $user["honor_point"];
    $experience = $user["experience"];
    $createdate = $user["createdate"];
    $online_status = $user["online_status"];
    $logindate = $user["logindate"];      
    $email = $user["email"];
    $country = $user["country"];
    $locale = $user["locale"];
    $memo = $user["memo"];
    $adflag = $user["adflag"];
    $logincount = $user["logincount"];
    $vippoint = $user["vip_point"];
    $viplevel = $user["vip_level"];
    $typoint = $user["ty_point"];
    $isblock = $user["isblock"];    
    
    //Mobile User Mapping(UUID)
    $sql = "SELECT uuid FROM `tbl_mobile` t1 JOIN `tbl_user_mobile_connection_log` t2 ON t1.device_id = t2.device_id WHERE useridx = $useridx AND UUID !='' " ;
    $uuid = $db_mobile->getvalue($sql);
    
    if($uuid != "")
    {
    	$sql = "SELECT device_id FROM tbl_mobile WHERE uuid = '$uuid'";
    	$mobile_mapping = $db_mobile->gettotallist($sql);
    }
    
    //deviceid
    $sql = "SELECT t1.os_type, device_name, app_version, os_version, api_version, t1.device_id ".
    		" FROM `tbl_mobile` t1 JOIN `tbl_user_mobile_connection_log` t2 ON t1.device_id = t2.device_id WHERE useridx = $useridx ";
    $device_info = $db_mobile->gettotallist($sql);

    // Web 주문 정보 조회
    $sql = "SELECT COUNT(*) AS totalcount, SUM(IF(status=1,facebookcredit,0)) AS facebookcredit,COUNT(IF(status=1,1,NULL)) AS status1,".
    		"SUM(IF(STATUS=1,coin,0)) AS sum_coin ".
    		"FROM tbl_product_order WHERE useridx=$useridx";
    
    $orderinfo = $db_main->getarray($sql);
    
    $order_totalcount = $orderinfo["totalcount"];
    $order_facebookcredit = (string)($orderinfo["facebookcredit"]*0.1);
    $order_status1 = $orderinfo["status1"];
    $order_sumcoin = $orderinfo["sum_coin"];
    
    // web earn 주문 정보 조회
    $sql = "SELECT COUNT(*) AS totalcount, SUM(money) AS sum_money, SUM(coin) AS sum_coin ".
    		"FROM tbl_product_order_earn ".
    		"WHERE useridx=$useridx";
    $earn_order_info = $db_main->getarray($sql);
    
    $earn_order_totalcount = $earn_order_info["totalcount"];
    $earn_order_money = $earn_order_info["sum_money"];
    $earn_order_sumcoin = $earn_order_info["sum_coin"];
    
    // IOS 주문정보 조회
    $sql = "SELECT COUNT(*) AS totalcount, SUM(IF(STATUS=1,money,0)) AS money, COUNT(IF(STATUS=1,1,NULL)) AS status1, ".
    		"SUM(IF(STATUS=1,coin,0)) AS sum_coin ".
    		"FROM tbl_product_order_mobile WHERE os_type=1 AND useridx=$useridx AND STATUS NOT IN (10,11)";    
    $orderinfo_ios = $db_main->getarray($sql);
    
    $order_ios_totalcount = $orderinfo_ios["totalcount"];
    $order_ios_money = $orderinfo_ios["money"];
    $order_ios_status1 = $orderinfo_ios["status1"];
    $order_ios_sumcoin = $orderinfo_ios["sum_coin"];
    $order_ios_sumcredit = $orderinfo_ios["sum_credit"];
    
    // Android 주문정보 조회
    $sql = "SELECT COUNT(*) AS totalcount, SUM(IF(STATUS=1,money,0)) AS money, COUNT(IF(STATUS=1,1,NULL)) AS status1, ".
    		"SUM(IF(STATUS=1,coin,0)) AS sum_coin ".
    		"FROM tbl_product_order_mobile WHERE os_type=2 AND useridx=$useridx";    
    $orderinfo_android = $db_main->getarray($sql);
    
    $order_android_totalcount = $orderinfo_android["totalcount"];
    $order_android_money = $orderinfo_android["money"];
    $order_android_status1 = $orderinfo_android["status1"];
    $order_android_sumcoin = $orderinfo_android["sum_coin"];
    $order_android_sumcredit = $orderinfo_android["sum_credit"];
    
    // Amazon 주문정보 조회
    $sql = "SELECT COUNT(*) AS totalcount, SUM(IF(STATUS=1,money,0)) AS money, COUNT(IF(STATUS=1,1,NULL)) AS status1, ".
    		"SUM(IF(STATUS=1,coin,0)) AS sum_coin ".
    		"FROM tbl_product_order_mobile WHERE os_type=3 AND useridx=$useridx";    
    $orderinfo_amazon = $db_main->getarray($sql);
    
    $order_amazon_totalcount = $orderinfo_amazon["totalcount"];
    $order_amazon_money = $orderinfo_amazon["money"];
    $order_amazon_status1 = $orderinfo_amazon["status1"];
    $order_amazon_sumcoin = $orderinfo_amazon["sum_coin"];
    $order_amazon_sumcredit = $orderinfo_amazon["sum_credit"];
    
    $order_sum_totalcount = $earn_order_totalcount + $order_totalcount + $order_ios_totalcount + $order_android_totalcount + $order_amazon_totalcount;
    $order_sum_money =  $earn_order_money + $order_facebookcredit + $order_ios_money + $order_android_money + $order_amazon_money;
    $order_sum_status1 = $earn_order_status1 + $order_status1 + $order_ios_status1 + $order_android_status1 + $order_amazon_status1;
    $order_sum_coin = $order_sumcoin + $order_ios_sumcoin + $order_android_sumcoin + $order_amazon_sumcoin;
    $order_sum_credit = $earn_order_sumcredit + $order_sumcredit + $order_ios_sumcredit + $order_android_sumcredit + $order_amazon_sumcredit;
    
    // 무료자원 로그
    $sql = "SELECT category, type,inbox_type, amount, writedate FROM tbl_user_freecoin_log_".($useridx % 10)." WHERE useridx=$useridx ORDER BY logidx DESC LIMIT 5";
    $freecoin_user_list = $db_main2->gettotallist($sql);
    
    //프리미엄 부스터 종료 날짜
    $sql = "SELECT IFNULL(end_date,'0000-00-00 00:00:00') AS end_date FROM tbl_user_premium_booster WHERE useridx = $useridx";
    $premium_booster_enddate = $db_main2->getvalue($sql);
    
    $sql = "SELECT type, name, inbox_type FROM tbl_total_freecoin_type";
    $freecoinlist = $db_analysis->gettotallist($sql);
    
    // inbox    
    $sql = "SELECT category AS inbox_type, type AS os_type, coin, writedate FROM tbl_user_inbox_collect_".($useridx % 20)." WHERE useridx=$useridx AND category != 301 ORDER BY logidx DESC LIMIT 5";
    $inbox_user_list = $db_livestats->gettotallist($sql);    
    
    $sql = "SELECT inbox_type, name FROM tbl_total_freecoin_type WHERE type = 100";
    $inbox_list = $db_analysis->gettotallist($sql);
    
    // 이벤트 리스트
    $sql = "SELECT eventidx, (SELECT category FROM tbl_event WHERE eventidx=tbl_event_result.eventidx) AS category, (SELECT title FROM tbl_event WHERE eventidx=tbl_event_result.eventidx) AS title, reward_type, reward_amount, ".
    		" writedate FROM tbl_event_result WHERE useridx=$useridx ORDER BY writedate DESC LIMIT 5";
    $event_resultlist = $db_main2->gettotallist($sql);
    
    //친구정보
    $sql = "SELECT  B.facebookid, B.useridx, B.nickname, CONCAT('http://graph.facebook.com/', B.facebookid, '/picture?type=square&access_token=$client_accesstoken') AS photourl FROM tbl_user_friend_".($useridx % 20)." A JOIN tbl_user_gamedata B ON A.friendidx=B.useridx WHERE A.useridx=$useridx";
    $friendlist = $db_friend->gettotallist($sql);
    
    // 최근 로그인 정보
    $sql = "SELECT logidx,useridx,ipaddress,useragent,adflag,coin,writedate FROM user_login_log WHERE useridx=$useridx ORDER BY logidx DESC LIMIT 5";
    $login_list = $db_analysis->gettotallist($sql);
    
    // 최근 클라이언트 통신로그 - WEB
    $sql = "SELECT logidx,ipaddress,facebookid,cmd,errcode,latency,writedate FROM client_communication_log WHERE facebookid='$userid' ORDER BY logidx desc LIMIT 5";
    $client_log_list = $db_analysis->gettotallist($sql);
    
    // 최근 클라이언트 통신로그 - IOS
    $sql = "SELECT logidx,ipaddress,facebookid,cmd,errcode,latency,writedate FROM client_communication_log_ios WHERE useridx='$useridx' ORDER BY logidx desc LIMIT 5";
    $client_log_ios_list = $db_analysis->gettotallist($sql);
    
    // 최근 클라이언트 통신로그 - Android
    $sql = "SELECT logidx,ipaddress,facebookid,cmd,errcode,latency,writedate FROM client_communication_log_android WHERE useridx='$useridx' ORDER BY logidx desc LIMIT 5";
    $client_log_android_list = $db_analysis->gettotallist($sql);
    
    // 최근 클라이언트 통신로그 - Amazon
    $sql = "SELECT logidx,ipaddress,facebookid,cmd,errcode,latency,writedate FROM client_communication_log_amazon WHERE useridx='$useridx' ORDER BY logidx desc LIMIT 5";
    $client_log_amazon_list = $db_analysis->gettotallist($sql);
    
    // 최근 user action 로그 - WEB
    $sql = "SELECT actionidx, ipaddress, category, category_info, action, action_info, ".
    		"staytime, isplaynow, coin, is_v2, writedate FROM user_action_log_".($userid%10)." WHERE facebookid=$userid ORDER BY actionidx DESC LIMIT 5";
    $action_log_list = $db_analysis->gettotallist($sql);
    
    // 최근 user action 로그 - IOS
    $sql = "SELECT actionidx, ipaddress, category, category_info, action, action_info, ".
    		"staytime, isplaynow, coin, writedate FROM user_action_log_ios_".($useridx%10)." WHERE useridx='$useridx' ORDER BY actionidx DESC LIMIT 5";
    $action_log_ios_list = $db_analysis->gettotallist($sql);
    
    // 최근 user action 로그 - Android
    $sql = "SELECT actionidx, ipaddress, category, category_info, action, action_info, ".
    		"staytime, isplaynow, coin, writedate FROM user_action_log_android_".($useridx%10)." WHERE useridx='$useridx' ORDER BY actionidx DESC LIMIT 5";
    $action_log_android_list = $db_analysis->gettotallist($sql);
    
    // 최근 user action 로그 - Amazon
    $sql = "SELECT actionidx, ipaddress, category, category_info, action, action_info,".
    		"staytime, isplaynow, coin, writedate FROM user_action_log_amazon WHERE useridx='$useridx' ORDER BY actionidx DESC LIMIT 5";
    $action_log_amazon_list = $db_analysis->gettotallist($sql);    
    
    //Slot 정보
    $sql = "SELECT slottype, slotname FROM tbl_slot_list";
    $slottype_list = $db_main2->gettotallist($sql);
    
    //게임정보 - Web
    $sql = "SELECT slottype, 0 AS mode, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout FROM tbl_user_stat WHERE useridx=$useridx AND slottype NOT IN (0) AND mode NOT IN (4, 5, 9, 26, 29) GROUP BY slottype";
    $slotinfolist = $db_main->gettotallist($sql);
    
    //게임정보 - Ios
    $sql = "SELECT slottype, 0 AS mode, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout FROM tbl_user_stat_ios WHERE useridx=$useridx AND slottype NOT IN (0) AND mode NOT IN (4, 5, 9, 26, 29) GROUP BY slottype";
    $slotinfolist_ios = $db_main->gettotallist($sql);
    
    //게임정보 - Android
    $sql = "SELECT slottype, 0 AS mode, SUM(moneyin) AS moneyin, SUM(moneyout) AS moneyout FROM tbl_user_stat_android WHERE useridx=$useridx AND slottype NOT IN (0) AND mode NOT IN (4, 5, 9, 26, 29) GROUP BY slottype";
    $slotinfolist_android = $db_main->gettotallist($sql);
    
    //게임정보 - Amazon
    $sql = "SELECT slottype,0 AS mode,SUM(moneyin) AS moneyin,SUM(moneyout) AS moneyout FROM tbl_user_stat_amazon WHERE useridx=$useridx AND slottype NOT IN (0) AND mode NOT IN (4, 5, 9, 26, 29) GROUP BY slottype";
    $slotinfolist_amazon = $db_main->gettotallist($sql);
    
    // Unknown 코인 정보
    $sql = "SELECT coin_change,category,writedate FROM tbl_unknown_coin WHERE useridx=$useridx ORDER BY writedate DESC LIMIT 5";
    $unknownlist = $db_main->gettotallist($sql);   
    
    //Jackpot 정보 - web
    $sql = "SELECT slottype, (amount) AS amount, devicetype, fiestaidx, (SELECT owner FROM `tbl_jackpot_hall_member` WHERE jackpothallidx = t1.jackpothallidx AND useridx = t1.useridx) AS jackpot_owner, ".
			"(SELECT COUNT(*) FROM tbl_jackpot_hall_member WHERE jackpothallidx = t1.jackpothallidx) AS jackpot_member_count, ".
			"(SELECT jackpotamount FROM tbl_jackpot_hall WHERE jackpothallidx = t1.jackpothallidx) AS total_jackpot_amount,".
			"writedate ".
			"FROM tbl_jackpot_log t1 ".
			"WHERE useridx=$useridx ORDER BY writedate DESC LIMIT 5";
    $jackpot_list = $db_main->gettotallist($sql);
        
    // BigWin 정보
    $sql = "SELECT slottype,objectidx,amount,grade,writedate FROM tbl_bigwin_log WHERE useridx=$useridx ORDER BY writedate DESC LIMIT 5";
    $bigwin_list = $db_main2->gettotallist($sql);
    
    // 1주일 슬롯 통계 - Web
    $sql = "SELECT  ".
			"DATE_FORMAT(writedate, '%Y-%m-%d') AS today, SUM(moneyin) AS money_in, SUM(moneyout) AS money_out, (SUM(moneyout)/SUM(moneyin)*100) AS slot_winrate, (SUM(moneyout) - SUM(moneyin)) AS winmoney ".
			"FROM `tbl_user_gamelog` ".
			"WHERE useridx = $useridx AND MODE NOT IN (4, 5, 9, 26, 29) ".
			" AND DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 7 DAY), '%Y-%m-%d') <= writedate AND writedate <= NOW() ". 
			"GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')";     
    $dailystatlist = $db_other->gettotallist($sql);
    
    // 1주일 슬롯 통계 - Ios
    $sql = "SELECT  ".
    		"DATE_FORMAT(writedate, '%Y-%m-%d') AS today, SUM(moneyin) AS money_in, SUM(moneyout) AS money_out, (SUM(moneyout)/SUM(moneyin)*100) AS slot_winrate, (SUM(moneyout) - SUM(moneyin)) AS winmoney ".
    		"FROM `tbl_user_gamelog_ios` ".
    		"WHERE useridx = $useridx AND MODE NOT IN (4, 5, 9, 26, 29) ".
    		" AND DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 7 DAY), '%Y-%m-%d') <= writedate AND writedate <= NOW() ".
    		"GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')";
    $dailystatlist_ios = $db_other->gettotallist($sql);
    
    // 1주일 슬롯 통계 - Android
    $sql = "SELECT  ".
    		"DATE_FORMAT(writedate, '%Y-%m-%d') AS today, SUM(moneyin) AS money_in, SUM(moneyout) AS money_out, (SUM(moneyout)/SUM(moneyin)*100) AS slot_winrate, (SUM(moneyout) - SUM(moneyin)) AS winmoney ".
    		"FROM `tbl_user_gamelog_android` ".
    		"WHERE useridx = $useridx AND MODE NOT IN (4, 5, 9, 26, 29) ".
    		" AND DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 7 DAY), '%Y-%m-%d') <= writedate AND writedate <= NOW() ".
    		"GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')";
    $dailystatlist_android = $db_other->gettotallist($sql);
    
    // 1주일 슬롯 통계 - Amazon
    $sql = "SELECT  ".
    		"DATE_FORMAT(writedate, '%Y-%m-%d') AS today, SUM(moneyin) AS money_in, SUM(moneyout) AS money_out, (SUM(moneyout)/SUM(moneyin)*100) AS slot_winrate, (SUM(moneyout) - SUM(moneyin)) AS winmoney ".
    		"FROM `tbl_user_gamelog_amazon` ".
    		"WHERE useridx = $useridx AND MODE NOT IN (4, 5, 9, 26, 29) ".
    		" AND DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 7 DAY), '%Y-%m-%d') <= writedate AND writedate <= NOW() ".
    		"GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')";
    $dailystatlist_amazon = $db_other->gettotallist($sql);
    
    // 관리자 자원 발급 정보
    $sql = "SELECT freecoin, reason, message, writedate FROM tbl_freecoin_admin_log WHERE useridx=$useridx ORDER BY writedate DESC LIMIT 5";
    $freecoin_list = $db_main->gettotallist($sql);
    
    function GetMobileName($type)
    {
    	$empty = strpos($type, " ");
    
    	if($empty !== false)
    	{
    		$type = substr($type, 0, $empty);
    	}
    
    	if ($type == "iPhone1,1")  return "iPhone 2G";
    	if ($type == "iPhone1,2")  return "iPhone 3G";
    	if ($type == "iPhone2,1")  return "iPhone 3GS";
    	if ($type == "iPhone3,1")  return "iPhone 4 (GSM)";
    	if ($type == "iPhone3,2")  return "iPhone 4 (GSM Rev. A)";
    	if ($type == "iPhone3,3")  return "iPhone 4 (CDMA)";
    	if ($type == "iPhone4,1")  return "iPhone 4S";
    	if ($type == "iPhone5,1")  return "iPhone 5 (GSM)";
    	if ($type == "iPhone5,2")  return "iPhone 5 (Global)";
    	if ($type == "iPhone5,3")  return "iPhone 5c (GSM)";
    	if ($type == "iPhone5,4")  return "iPhone 5c (Global)";
    	if ($type == "iPhone6,1")  return "iPhone 5s (GSM)";
    	if ($type == "iPhone6,2")  return "iPhone 5s (Global)";
    	if ($type == "iPhone7,1")  return "iPhone 6 Plus";
    	if ($type == "iPhone7,2")  return "iPhone 6";
    
    	if ($type == "iPod1,1")    return "iPod Touch (1 Gen)";
    	if ($type == "iPod2,1")    return "iPod Touch (2 Gen)";
    	if ($type == "iPod3,1")    return "iPod Touch (3 Gen)";
    	if ($type == "iPod4,1")    return "iPod Touch (4 Gen)";
    	if ($type == "iPod5,1")    return "iPod Touch (5 Gen)";
    
    	if ($type == "iPad1,1")    return "iPad (WiFi)";
    	if ($type == "iPad1,2")    return "iPad 3G";
    	if ($type == "iPad2,1")    return "iPad 2 (WiFi)";
    	if ($type == "iPad2,2")    return "iPad 2 (GSM)";
    	if ($type == "iPad2,3")    return "iPad 2 (CDMA)";
    	if ($type == "iPad2,4")    return "iPad 2 (WiFi Rev. A)";
    	if ($type == "iPad2,5")    return "iPad Mini (WiFi)";
    	if ($type == "iPad2,6")    return "iPad Mini (GSM)";
    	if ($type == "iPad2,7")    return "iPad Mini (CDMA)";
    	if ($type == "iPad3,1")    return "iPad 3 (WiFi)";
    	if ($type == "iPad3,2")    return "iPad 3 (CDMA)";
    	if ($type == "iPad3,3")    return "iPad 3 (Global)";
    	if ($type == "iPad3,4")    return "iPad 4 (WiFi)";
    	if ($type == "iPad3,5")    return "iPad 4 (CDMA)";
    	if ($type == "iPad3,6")    return "iPad 4 (Global)";
    
    	if ($type == "iPad4,1")    return "iPad Air (WiFi)";
    	if ($type == "iPad4,2")    return "iPad Air (WiFi+GSM)";
    	if ($type == "iPad4,3")    return "iPad Air (WiFi+CDMA)";
    	if ($type == "iPad4,4")    return "iPad Mini Retina (WiFi)";
    	if ($type == "iPad4,5")    return "iPad Mini Retina (WiFi+CDMA)";
    
    	return $type;
    }
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	function open_user_app()
	{
	    if (!confirm("현재 사용자가 로그인 중일 경우 사용자가 로그아웃 됩니다.\n로그인 하시겠습니까?"))
	        return;
	        
	    window.open("<?= FACEBOOK_CANVAS_URL ?>?login_userid=<?= $userid ?>&login_userkey=<?= $userkey ?>","_blank");
	}

	function pop_share_info()
	{
		try
		{
			var request = $.ajax({
	    		url: "pop_share_status.php",
				type: "POST",
				data: {useridx: "<?= $useridx?>"},
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}
	function pop_uuid_info()
	{
		try
		{
			var request = $.ajax({
	    		url: "pop_uuid_list.php",
				type: "POST",
				data: {uuid: "<?= $uuid?>"},
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}

	function pop_order_detail_view(type, useridx)
	{
		try
		{
			var request = $.ajax({
	    		url: "pop_order_detail.php",
				type: "POST",
				data: {type: type, useridx: "<?= $useridx?>"},
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}

	function pop_order_earn_detail_view(useridx)
	{		
		try
		{
			var request = $.ajax({
	    		url: "pop_order_earn_detail.php",
				type: "POST",
				data: {useridx: "<?= $useridx?>"},
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}

	function pop_free_resource(category)
	{
		if(category == "coin_minus")
		{
			if(!confirm("해당 사용자를 코인을 몰수 하시겠습니까?"))
				return;
			
			var param = {};
			param.useridx = "<?= $useridx?>";		
			param.admin_id = "<?= $login_adminid?>";
			
			WG_ajax_execute("user/save_block_resource", param, save_block_resource_callback, false);
		}
		else
		{
			try
			{
				var request = $.ajax({
		    		url: "pop_free_resource.php",
					type: "POST",
					data: {adminid: "<?= $login_adminid?>", useridx: "<?= $useridx?>", category: category, facebookid: "<?= $userid ?>", honorlevel: "<?= $honor_level ?>"},
					cache: false,
					dataType: "html"
				});
		    	request.done(function( data ) {
		    		data = data.replace("&lt;", "<").replace("&gt;", ">");
	
		    		$('#to_pop_up').html(data);
		    		$('#to_pop_up').bPopup({
						easing: 'easeOutBack',
						speed: 850,
						opacity: 0.3,
						transition: 'slideDown',
						modalClose: false
					});
				});
		    	request.fail(function( jqXHR, textStatus ) {
				});
			}
			catch (e)
		    {
		    }
		}
	}

	function save_block_resource_callback(result, reason)
	{
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            alert("해당 사용자코인 몰수 하였습니다.");
        }
	}
	
	function update_user_block(block)
	{
		if(block == "1")
		{
			try
			{
				var request = $.ajax({
					url: "pop_user_block.php",
					type: "POST",
					data: {adminid: "<?= $login_adminid?>", useridx: "<?= $useridx?>"},
					cache: false,
					dataType: "html"
				});
				
				request.done(function( data ) {
					data = data.replace("&lt;", "<").replace("&gt;", ">");

					$('#to_pop_up').html(data);
					$('#to_pop_up').bPopup({
						easing: 'easeOutBack',
						speed: 850,
						opacity: 0.3,
						transition: 'slideDown',
						modalClose: false
					});
				});
				request.fail(function( jqXHR, textStatus ) {
				});
			}
	    	catch (e)
			{
			}        	
		}
		else
		{
			if(!confirm("해당 사용자를 차단 해제 하시겠습니까?"))
				return;
	                
			var param = {};
			param.useridx = '<?= $useridx ?>';
			param.admin_id = '<?= $login_adminid ?>';
			param.isblock = block;
	            
			WG_ajax_execute("user/update_user_block", param, update_user_block_callback, true);
		}
	}

	function update_user_block_callback(result, reason)
	{
		if (!result)
		{
			alert("오류 발생 - " + reason);
		}
		else
		{
			alert("해당 사용자 차단 해제 하였습니다.");
			window.location.reload(false);
		}
	}

	function pop_freecoin_detail()
	{
		try
		{
			var request = $.ajax({
	    		url: "pop_freecoin_detail.php",
				type: "POST",
				data: {useridx: "<?= $useridx?>"},
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }		
	}

	function pop_inbox_detail()
	{
		try
		{
			var request = $.ajax({
	    		url: "pop_inbox_detail.php",
				type: "POST",
				data: {useridx: "<?= $useridx?>"},
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}

	function pop_jackpot_detail()
	{
		try
		{
			var request = $.ajax({
	    		url: "pop_jackpot_detail.php",
				type: "POST",
				data: {useridx: "<?= $useridx?>"},
				cache: false,
				dataType: "html"
			});
			request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}

	function pop_bigwin_detail()
	{
		try
		{
			var request = $.ajax({
	    		url: "pop_bigwin_detail.php",
				type: "POST",
				data: {useridx: "<?= $useridx?>"},
				cache: false,
				dataType: "html"
			});
			request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}

	function pop_slotdetail_view(os_type)
	{
		try
		{
			var request = $.ajax({
	    		url: "pop_slotdetail_view.php",
				type: "POST",
				data: {os_type: os_type, useridx: "<?= $useridx?>"},
				cache: false,
				dataType: "html"
			});
			request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}	

	function pop_user_actionlist(os_type)
	{
		try
		{
			var request = $.ajax({
	    		url: "pop_action_detail.php",
				type: "POST",
				data: {os_type: os_type, userid: "<?= $userid?>", useridx: "<?= $useridx?>"},				
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}

	function pop_user_event_detail()
	{
		try
		{
			var request = $.ajax({
	    		url: "pop_user_event_detail.php",
				type: "POST",
				data: {useridx: "<?= $useridx?>"},
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}

	function pop_unknown_coin_detail()
	{
		try
		{
			var request = $.ajax({
	    		url: "pop_unknown_coin_detail.php",
				type: "POST",
				data: {useridx: "<?= $useridx?>"},
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}

	function pop_free_detail()
	{
		try
		{
			var request = $.ajax({
	    		url: "pop_free_detail.php",
				type: "POST",
				data: {useridx: "<?= $useridx?>"},
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">");

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}

	function get_friend_list()
	{
		var param = {};
		param.useridx = "<?= $useridx?>";		
		param.client_accesstoken = "<?= $client_accesstoken?>";		

		WG_ajax_list("user/get_friend_list", param, get_friend_list_callback);
	}

	function get_friend_list_callback(result, reason, dummy, list)
	{
		if(!result)
		{
			alert("오류 발생 - " + reason);			
		}
		else
		{
			var listObj = "";
			
			for (var i=0; i<list.length; i++)
            {
				var userid = list[i]["facebookid"];
	            var idx = list[i]["useridx"];
	            var nickname = list[i]["nickname"];
	            	 
	            if(userid < 10)           
	            	var photourl = "https://<?=WEB_HOST_NAME?>/mobile/images/avatar_profile/guest_profile_" + userid + ".png";
	            else
	            	var photourl = "http://graph.facebook.com/" + userid + "/picture?type=square&access_token=<?=$client_accesstoken?>";

				var Obj = "<dl class='friendlist_unit' onmouseover='className=\"friendlist_unit_over\"'  onmouseout='className=\"friendlist_unit\"' onclick='view_user_dtl(" + idx + ")'><dt><img src='" + photourl + "' width='50' height='50'></dt><dd>" + nickname +"</dd></dl>";				
				listObj += Obj; 
            }

            var friendwrap = document.getElementById("friendlist_wrap");
            friendwrap.innerHTML = listObj;
		}
	}

	function update_friend()
    {
    	var param = {};
        param.useridx = '<?= $useridx ?>';
        
        WG_ajax_execute("user/update_friend", param, update_friend_callback, true);
    }

    function update_friend_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            window.location.reload(false);
        }
    }

    function update_user_uuid_block(block)
	{
		if(block == "1")
		{
			try
			{
				var request = $.ajax({
					url: "pop_user_uuid_block.php",
					type: "POST",
					data: {adminid: "<?= $login_adminid?>", useridx: "<?= $useridx?>"},
					cache: false,
					dataType: "html"
				});
				
				request.done(function( data ) {
					data = data.replace("&lt;", "<").replace("&gt;", ">");

					$('#to_pop_up').html(data);
					$('#to_pop_up').bPopup({
						easing: 'easeOutBack',
						speed: 850,
						opacity: 0.3,
						transition: 'slideDown',
						modalClose: false
					});
				});
				request.fail(function( jqXHR, textStatus ) {
				});
			}
	    	catch (e)
			{
			}        	
		}
		else
		{
			if(!confirm("해당 사용자를 차단 해제 하시겠습니까?"))
				return;
	                
			var param = {};
			param.useridx = '<?= $useridx ?>';
			param.admin_id = '<?= $login_adminid ?>';
			param.isblock = block;
	            
			WG_ajax_execute("user/update_user_uuid_block", param, update_user_uuid_block_callback, true);
		}
	}
	
    function update_user_uuid_block_callback(result, reason)
	{
		if (!result)
		{
			alert("오류 발생 - " + reason);
		}
		else
		{
			alert("해당 사용자 차단 해제 하였습니다.");
			window.location.reload(false);
		}
	}
    
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">

	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 사용자상세</div>
	</div>
	<!-- //title_warp -->
	
	<div class="h2_title">
		사용자정보
		<input type="button" class="btn_03" value="이 사용자로 앱로그인" onclick="open_user_app()">
<?
	if(count($mobile_mapping) > 0)
	{ 
?> 
		<input type="button" class="btn_03" value="동일 UUID" onclick="pop_uuid_info()">
<?
	}
	
	if($isblock == "1")
	{
		$sql = "SELECT * FROM tbl_user_block WHERE useridx='$useridx'";
		$block_info = $db_main->getarray($sql);
		
		$block_cnt = $block_info["blockcount"];
		$block_reason = $block_info["reason"];
?>
		 <span style='color:red'> *<?= ($block_cnt > 9) ? "영구" : ""?>차단 사용자 [차단 횟수: <?= ($block_cnt == "999") ? "관리자차단" : $block_cnt?>, 사유: <?= $block_reason?>]</span>
<?
	}
?>
	</div>
	<div class="h2_cont">
		<img src="<?= $photourl ?>" height="50" width="50" class="summary_user_image"/>
		<div class="sumary_username_wrap">
			<div class="summary_username" style="width:250px;"><img src="/images/icon/<?= ($online_status == "1") ? "status_1.png" : "status_3.png" ?>" align="absmiddle" /> [<?= $useridx ?>] <?= $nickname ?></div>                 
		</div>
		<div class="summary_user_coin"><?= make_price_format($coin) ?></div>
<?
	if($dbaccesstype == 1 || $dbaccesstype == 2 || $dbaccesstype == 4)
	{
		if($dbaccesstype == 1 || $dbaccesstype == 2)
		{
?>
			<div class="summary_user_free" onclick="pop_free_resource('coin');" style="cursor:pointer;">코인 발급</div>	
			<div class="summary_user_free" onclick="pop_free_resource('ty');" style="cursor:pointer;">TY 발급</div>
			<div class="summary_user_block" onclick="update_user_block(<?= ($isblock == "1") ? "0" : "1" ?>)" style="cursor:pointer;"><?= ($isblock == "1") ? "해제" : "차단" ?></div>
<?
			if($login_adminid == 'kisa0819' || $login_adminid == 'yhjung')
			{
?>
				<div class="summary_user_free" onclick="pop_free_resource('coin_minus');" style="cursor:pointer;">코인 몰수</div>
<?
			}
		}
		else if($login_adminid == 'hyotng' || $login_adminid == 'kisa0819' || $login_adminid == 'yhjung')
		{
?>
			<div class="summary_user_free" onclick="pop_free_resource('coin_minus');" style="cursor:pointer;">코인 몰수</div>
<?
		}
?>		
		<!-- div class="summary_user_block" onclick="update_user_uuid_block(<?= ($isblock == "2") ? "0" : "1" ?>)" style="cursor:pointer;"><?= ($isblock == "2") ? "UUID 해제" : "UUID 차단" ?></div-->
<?
	}
?>
		<div class="div_line"></div>
                
		<div class="button_wrap" style="float:left;padding-top:5px;">
<?
	if($userid > 10)
	{ 
?>
			<input type="button" class="btn_03" style="margin-bottom:5px;" value="facebook 가기" onclick="go_facebook('<?= $userid ?>')">
<?
	}
?> 
			<input type="button" class="btn_03" style="margin-bottom:5px;" value="사용자행동분석" onclick="alert('준비중')"><br/>                    
		</div>
		
		<div class="user_item">
			<div class="user_item_lbl">Facebook ID</div>
			<div class="user_item_value"><?= $userid ?></div>
		</div>
                
		<div class="user_item">
			<div class="user_item_lbl">국가</div>
			<div class="user_item_value"><?= $country ?></div>
		</div>
                
		<div class="user_item">
			<div class="user_item_lbl">성별</div>
			<div class="user_item_value"><?= ($sex == "1") ? "남자" : "여자" ?></div>
		</div>
                
		<div class="user_item">
			<div class="user_item_lbl">언어</div>
			<div class="user_item_value"><?= $locale ?></div>
		</div>
		
		<div class="user_item">
			<div class="user_item_lbl">레벨</div>
			<div class="user_item_value"><?= $level ?></div>
		</div>
                
		<div class="user_item">
			<div class="user_item_lbl">경험치</div>
			<div class="user_item_value"><?= $experience ?></div>
		</div>
                
		<div class="user_item">
			<div class="user_item_lbl">로그인 횟수</div>
			<div class="user_item_value"><?= $logincount ?></div>
		</div>
                
		<div class="user_item">
			<div class="user_item_lbl">생성일</div>
			<div class="user_item_value"><?= $createdate ?></div>
		</div>
                
		<div class="user_item">
			<div class="user_item_lbl">최근 로그인</div>
			<div class="user_item_value"><?= $logindate ?></div>
		</div>
                
		<div class="user_item">
			<div class="user_item_lbl">유입경로</div>
			<div class="user_item_value">
<?
    if ($adflag == "")
        echo("바이럴");
    else 
        echo($adflag); 
?>
			</div>
		</div>
		
		<div class="user_item">
			<div class="user_item_lbl">VIP 포인트</div>
			<div class="user_item_value"><?=number_format($vippoint) ?></div>
		</div>
		
		<div class="user_item">
			<div class="user_item_lbl">VIP Level</div>
			<div class="user_item_value"><?=$viplevel." Level"?></div>
		</div>
		
		<div class="user_item">
			<div class="user_item_lbl">Honor Level</div>
			<div class="user_item_value"><?=$honor_level." Level"?></div>
		</div>
		
		<div class="user_item">
			<div class="user_item_lbl">Honor Point</div>
			<div class="user_item_value"><?=number_format($honor_point)?></div>
		</div>
		
		<div class="user_item">
			<div class="user_item_lbl">TY 포인트</div>
			<div class="user_item_value"><?=number_format($typoint)?></div>
		</div>
		<div class="user_item">
			<div class="user_item_lbl">Premium </br>종료 일</div>
			<div class="user_item_value"><?=$premium_booster_enddate?></div>
		</div>
		
		<?
  if( $device_info != ""){
?>
    <div class="user_item">
      <div class="user_item_lbl">디바이스</div>
        <div class="user_item_value">
          <table style="width:650px;">
			<colgroup>
              <col width="50">        
              <col width="">
	      <col width="">
              <col width="">        
              <col width="">
              <col width="">
			</colgroup>
			<thead>
				<tr style="border-bottom: 1px solid #dbdbdb;">
					<th>OS</th>
          			<th>Device ID</th>
          			<th>Device Name</th>
          			<th>App Version</th>
          			<th>Os Version</th>
          			<th>API Version</th>
				</tr>
			</thead>
				<tbody>
<?
	for($i=0; $i<sizeof($device_info); $i++)
	{
		$os_type = $device_info[$i]["os_type"];
		$device_name = $device_info[$i]["device_name"];
		$app_version = $device_info[$i]["app_version"];
		$os_version = $device_info[$i]["os_version"];
		$api_version = $device_info[$i]["api_version"];
		$device_id = $device_info[$i]["device_id"];
		
		if($os_type == 1)
		{
		  $os_type = "IOS";
		}
		else if($os_type == 2)
		{
		  $os_type = "And";
		}
		else if($os_type == 3)
		{
		  $os_type = "Ama";
		}
?>
        <tr style="border-bottom: 1px solid #dbdbdb;">
          <td><?= $os_type?></td>
          <td><?= $device_id?></td>
          <td><?= $device_name?></td>
          <td><?= $app_version?></td>
          <td><?= $os_version?></td>
          <td><?= $api_version?></td>
        </tr>
<?
	}
?>
					
				</tbody>
		</table>
	</div>
<?
	}
?>
	</div>
		
	<div class="clear"></div>
	
	<div class="h2_title pt20">
		주문정보
	</div>
            
	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="15%">
			<col width="15%">
			<col width="*">
			<col width="20%">
			<col width="10%">
			<col width="15%">
		</colgroup>
		<thead>
			<tr>
				<th>구매위치</th>
				<th>구매건수</th>
				<th>구매코인</th>
				<th>결제금액</th>
				<th>결제율</th>
				<th></th>
				</tr>
		</thead>
		<tbody>
			<tr>
				<td class="tdc point">Earn</td>
				<td class="tdc point_title"><?= number_format($earn_order_totalcount) ?>건</td>
				<td class="tdc point"><?= number_format($earn_order_sumcoin) ?> Coins</td>						
				<td class="tdc">$<?= number_format($earn_order_money,2) ?></td>
				<td class="tdc"><?= ($earn_order_totalcount == 0 )? "-" : "100%"?></td>
				<td class="tdc"><input type="button" class="btn_03" value="상세보기" onclick="pop_order_earn_detail_view('<?= $useridx ?>');" /></td>
			</tr>
			<tr>
				<td class="tdc point">Web</td>
				<td class="tdc point_title"><?= number_format($order_status1) ?>건</td>
				<td class="tdc point">$<?= number_format($order_sumcoin) ?> Coins</td>
				<td class="tdc">$<?= number_format($order_facebookcredit,1) ?></td>
				<td class="tdc">
<?
    if ($order_totalcount == 0)
        $order_pay_rate = "0%";
    else 
        $order_pay_rate = round(($order_status1/$order_totalcount)*100)."% (".$order_status1."/".$order_totalcount.")";

    echo($order_pay_rate);
?>
				</td>
				<td class="tdc"><input type="button" class="btn_03" value="상세보기" onclick="pop_order_detail_view('web', '<?= $useridx ?>');" /></td>
			</tr>
				<tr>
				<td class="tdc point">IOS</td>
				<td class="tdc point_title"><?= number_format($order_ios_status1) ?>건</td>
				<td class="tdc point"><?= number_format($order_ios_sumcoin) ?> Coins</td>				
                <td class="tdc">$<?= number_format($order_ios_money,1) ?></td>
                <td class="tdc">
<?
    if ($order_ios_totalcount == 0)
        $order_ios_pay_rate = "0%";
    else 
        $order_ios_pay_rate = round(($order_ios_status1/$order_ios_totalcount)*100)."% (".$order_ios_status1."/".$order_ios_totalcount.")";

    echo($order_ios_pay_rate);
?>
				</td>
				<td class="tdc"><input type="button" class="btn_03" value="상세보기" onclick="pop_order_detail_view('ios', '<?= $useridx ?>');" /></td>
			</tr>
			<tr>
				<td class="tdc point">Android</td>
				<td class="tdc point_title"><?= number_format($order_android_status1) ?>건</td>
				<td class="tdc point"><?= number_format($order_android_sumcoin) ?>  Coins</td>
                <td class="tdc">$<?= number_format($order_android_money,1) ?></td>
                <td class="tdc">
<?
    if ($order_android_totalcount == 0)
        $order_android_pay_rate = "0%";
    else 
        $order_android_pay_rate = round(($order_android_status1/$order_android_totalcount)*100)."% (".$order_android_status1."/".$order_android_totalcount.")";

    echo($order_android_pay_rate);
?>
				</td>
				<td class="tdc"><input type="button" class="btn_03" value="상세보기" onclick="pop_order_detail_view('android', '<?= $useridx ?>');" /></td>
			</tr>
			<tr>
				<td class="tdc point">Amazon</td>
				<td class="tdc point_title"><?= number_format($order_amazon_status1) ?>건</td>
				<td class="tdc point"><?= number_format($order_amazon_sumcoin) ?>  Coins</td>
                <td class="tdc">$<?= number_format($order_amazon_money,1) ?></td>
                <td class="tdc">
<?
    if ($order_amazon_totalcount == 0)
        $order_amazon_pay_rate = "0%";
    else 
        $order_amazon_pay_rate = round(($order_amazon_status1/$order_amazon_totalcount)*100)."% (".$order_amazon_status1."/".$order_amazon_totalcount.")";

    echo($order_amazon_pay_rate);
?>
				</td>
				<td class="tdc"><input type="button" class="btn_03" value="상세보기" onclick="pop_order_detail_view('amazon', '<?= $useridx ?>');" /></td>
			</tr>
			<tr>
				<td class="tdc point">합계</td>
				<td class="tdc point_title"><?= number_format($order_sum_status1) ?>건</td>
				<td class="tdc point"><?= number_format($order_sum_coin) ?> Coins</td>
                <td class="tdc">$<?= number_format($order_sum_money,1) ?></td>
                <td class="tdc">
<?
    if ($order_sum_totalcount == 0)
        $order_sum_pay_rate = "0%";
    else 
        $order_sum_pay_rate = round(($order_sum_status1/$order_sum_totalcount)*100)."% (".$order_sum_status1."/".$order_sum_totalcount.")";

    echo($order_sum_pay_rate);
?>
				</td>
				<td class="tdc"></td>
			</tr>
		</tbody>
	</table>
	<div class="clear"></div>
	
	<div class="h2_title pt20">
              최근 무료 자원 획득 정보<span style="padding-left:10px;font:11px dotum;"><a href="javascript:pop_freecoin_detail();">+ 더보기</a></span>		
	</div>
	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="">
			<col width="">
			<col width="">
			<col width="">			
		</colgroup>
		<thead>
			<tr>
				<th>접속</th>
				<th>타입</th>
                <th>Coin</th>
				<th>획득일</th>
			</tr>
		</thead>
		<tbody>
<?
    for ($i=0; $i<sizeof($freecoin_user_list); $i++)
    {
        $type = $freecoin_user_list[$i]["category"];
        $freecoin_inbox_type = $freecoin_user_list[$i]["inbox_type"];
        $category = $freecoin_user_list[$i]["type"];        
        $amount = $freecoin_user_list[$i]["amount"];        
        $writedate = $freecoin_user_list[$i]["writedate"];
        
        $typename = "";
        
        if($type == 0)
        	$typename = "Web";
        else if($type == 1)
        	$typename = "IOS";
        else if($type == 2)
        	$typename = "Android";
        else if($type == 3)
        	$typename = "Amazon";
        
        $categoryname = "";
        
        for($j=0; $j<sizeof($freecoinlist); $j++)
        {
        	if ($freecoin_inbox_type == 301)
        	{
        		$categoryname = "Ty Point";
        		$amount = 1;
        	}
	        else if($freecoinlist[$j]["type"] == $category)
	        {
	            if($freecoin_inbox_type == $freecoinlist[$j]["inbox_type"])
    	        	$categoryname = $freecoinlist[$j]["name"];
	            else
	                $categoryname = $freecoinlist[$j]["name"];
	            
				break;
	        }
	        else
	        {
	        	$categoryname = "Unkown Type";
	        }
        }
?>
			<tr>
				<td class="tdc point"><?= $typename ?></td>
				<td class="tdc"><?= $categoryname ?></td>
				<td class="tdc"><?= number_format($amount) ?></td>
				<td class="tdc"><?= $writedate ?></td>
			</tr>
<?
    }
?>
		</tbody>
	</table>
	
	<div class="clear"></div>
	
	<div class="h2_title pt20">
              최근  inbox 획득 정보<span style="padding-left:10px;font:11px dotum;"><a href="javascript:pop_inbox_detail();">+ 더보기</a></span>		
	</div>
	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="">
			<col width="">
			<col width="">
			<col width="">			
		</colgroup>
		<thead>
			<tr>
				<th>접속</th>
				<th>타입</th>
                <th>Coin</th>
				<th>획득일</th>
			</tr>
		</thead>
		<tbody>
<?
    for ($i=0; $i<sizeof($inbox_user_list); $i++)
    {
        $os_type = $inbox_user_list[$i]["os_type"];
        $inbox_type = $inbox_user_list[$i]["inbox_type"];        
        $amount = $inbox_user_list[$i]["coin"];        
        $writedate = $inbox_user_list[$i]["writedate"];
        
        $typename = "";
        
        if($os_type == 0)
        	$typename = "Web";
        else if($os_type == 1)
        	$typename = "IOS";
        else if($os_type == 2)
        	$typename = "Android";
        else if($os_type == 3)
        	$typename = "Amazon";
        
        $categoryname = "";
        
        for($j=0; $j<sizeof($inbox_list); $j++)
        {
	        if($inbox_list[$j]["inbox_type"] == $inbox_type)
	        {
	        	$categoryname = $inbox_list[$j]["name"];
					break;
	        }
	        else
	        {
	        	$categoryname = "Unkown Type";
	        }
        }
?>
			<tr>
				<td class="tdc point"><?= $typename ?></td>
				<td class="tdc"><?= $categoryname ?></td>
				<td class="tdc"><?= number_format($amount) ?></td>
				<td class="tdc"><?= $writedate ?></td>
			</tr>
<?
    }
?>
		</tbody>
	</table>
	
	<div class="clear"></div>
	
	<div class="h2_title pt20">
	게임정보 - Web
	</div>

	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="20%">
			<col width="20%">
			<col width="20%">
			<col width="20%">
			<col width="20%">
		</colgroup>
		<thead>
            <tr>
                <th>Slot</th>
                <th class="tdr">IN</th>
                <th class="tdr">OUT</th>
                <th class="tdr">수익(OUT-IN)</th>
                <th class="tdr">수익률</th>
            </tr>
		</thead>
		<tbody>
		<?
    $sum_money_in = 0;
    $sum_money_out = 0;
    $sum_money_diff = 0;
    
    for($i=0;$i<sizeof($slotinfolist);$i++)
    { 
        $slottype = $slotinfolist[$i]["slottype"];
        $mode = $slotinfolist[$i]["mode"];
        $money_in = $slotinfolist[$i]["moneyin"];
        $money_out = $slotinfolist[$i]["moneyout"];
        
        $money_diff = (string)($money_out - $money_in);
        
        if ($money_in > 0)
            $money_rate = round($money_out/$money_in*10000)/100;
        else
            $money_rate = 0; 
        
        $sum_money_in += $money_in;
        $sum_money_out += $money_out;
        $sum_money_diff += $money_diff;
        
        if ($sum_money_in > 0)
            $sum_money_rate = round($sum_money_out/$sum_money_in*10000)/100;
        else
            $sum_money_rate = 0;
        
        $tutorial = "";
        
        if($mode == "3")
        	$tutorial = "(트리트 금액)";
        else if($mode == "4")
        	$tutorial = " (Tutorial)";
        else if($mode == "5")
        	$tutorial = " (승률 부양)";
        
?>
            <tr>
                <th>
<?

		for($j=0; $j<sizeof($slottype_list); $j++)
		{
			if($slottype_list[$j]["slottype"] == $slottype)
			{
				$slot_name = $slottype_list[$j]["slotname"];
				break;
			}
			else
			{
				$slot_name = "Unkown";
			}
		}
		
				echo("$slot_name$tutorial");
?>
                </th>
                <td class="tdr"><?= number_format($money_in) ?></td>
                <td class="tdr"><?= number_format($money_out) ?></td>
                <td class="tdr"><?= number_format($money_diff) ?></td>
                <td class="tdr"><?= $money_rate ?>%</td> 
            
                <td></td>
            </tr>
<?
    } 
?>
		</tbody>
		<tfoot>
            <tr>
                <th colspan="1">총합</th>
                <td class="tdr"><?= number_format($sum_money_in) ?></td>
                <td class="tdr"><?= number_format($sum_money_out) ?></td>
                <td class="tdr"><?= number_format($sum_money_diff) ?></td>
                <td class="tdr"><?= $sum_money_rate ?>%</td>
            </tr>
		</tfoot>
	</table>
	
	<div class="clear"></div>
	
	<div class="h2_title pt20">
	게임정보 - Ios
	</div>

	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="20%">
			<col width="20%">
			<col width="20%">
			<col width="20%">
			<col width="20%">
		</colgroup>
		<thead>
            <tr>
                <th>Slot</th>
                <th class="tdr">IN</th>
                <th class="tdr">OUT</th>
                <th class="tdr">수익(OUT-IN)</th>
                <th class="tdr">수익률</th>
            </tr>
		</thead>
		<tbody>
		<?
    $sum_money_in = 0;
    $sum_money_out = 0;
    $sum_money_diff = 0;
    
    for($i=0;$i<sizeof($slotinfolist_ios);$i++)
    { 
        $slottype = $slotinfolist_ios[$i]["slottype"];
        $mode = $slotinfolist_ios[$i]["mode"];
        $money_in = $slotinfolist_ios[$i]["moneyin"];
        $money_out = $slotinfolist_ios[$i]["moneyout"];
        
        $money_diff = (string)($money_out - $money_in);
        
        if ($money_in > 0)
            $money_rate = round($money_out/$money_in*10000)/100;
        else
            $money_rate = 0; 
        
        $sum_money_in += $money_in;
        $sum_money_out += $money_out;
        $sum_money_diff += $money_diff;
        
        if ($sum_money_in > 0)
            $sum_money_rate = round($sum_money_out/$sum_money_in*10000)/100;
        else
            $sum_money_rate = 0;
        
        $tutorial = "";
        
        if($mode == "3")
        	$tutorial = "(트리트 금액)";
        else if($mode == "4")
        	$tutorial = " (Tutorial)";
        else if($mode == "5")
        	$tutorial = " (승률 부양)";
        
?>
            <tr>
                <th>
<?

		for($j=0; $j<sizeof($slottype_list); $j++)
		{
			if($slottype_list[$j]["slottype"] == $slottype)
			{
				$slot_name = $slottype_list[$j]["slotname"];
				break;
			}
			else
			{
				$slot_name = "Unkown";
			}
		}
		
				echo("$slot_name$tutorial");
?>
                </th>
                <td class="tdr"><?= number_format($money_in) ?></td>
                <td class="tdr"><?= number_format($money_out) ?></td>
                <td class="tdr"><?= number_format($money_diff) ?></td>
                <td class="tdr"><?= $money_rate ?>%</td> 
            
                <td></td>
            </tr>
<?
    } 
?>
		</tbody>
		<tfoot>
            <tr>
                <th colspan="1">총합</th>
                <td class="tdr"><?= number_format($sum_money_in) ?></td>
                <td class="tdr"><?= number_format($sum_money_out) ?></td>
                <td class="tdr"><?= number_format($sum_money_diff) ?></td>
                <td class="tdr"><?= $sum_money_rate ?>%</td>
            </tr>
		</tfoot>
	</table>
	
	<div class="clear"></div>
	
	<div class="h2_title pt20">
	게임정보 - Android
	</div>

	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="20%">
			<col width="20%">
			<col width="20%">
			<col width="20%">
			<col width="20%">
		</colgroup>
		<thead>
            <tr>
                <th>Slot</th>
                <th class="tdr">IN</th>
                <th class="tdr">OUT</th>
                <th class="tdr">수익(OUT-IN)</th>
                <th class="tdr">수익률</th>
            </tr>
		</thead>
		<tbody>
		<?
    $sum_money_in = 0;
    $sum_money_out = 0;
    $sum_money_diff = 0;
    
    for($i=0;$i<sizeof($slotinfolist_android);$i++)
    { 
        $slottype = $slotinfolist_android[$i]["slottype"];
        $mode = $slotinfolist_android[$i]["mode"];
        $money_in = $slotinfolist_android[$i]["moneyin"];
        $money_out = $slotinfolist_android[$i]["moneyout"];
        
        $money_diff = (string)($money_out - $money_in);
        
        if ($money_in > 0)
            $money_rate = round($money_out/$money_in*10000)/100;
        else
            $money_rate = 0; 
        
        $sum_money_in += $money_in;
        $sum_money_out += $money_out;
        $sum_money_diff += $money_diff;
        
        if ($sum_money_in > 0)
            $sum_money_rate = round($sum_money_out/$sum_money_in*10000)/100;
        else
            $sum_money_rate = 0;
        
        $tutorial = "";
        
        if($mode == "3")
        	$tutorial = "(트리트 금액)";
        else if($mode == "4")
        	$tutorial = " (Tutorial)";
        else if($mode == "5")
        	$tutorial = " (승률 부양)";
        
?>
            <tr>
                <th>
<?

		for($j=0; $j<sizeof($slottype_list); $j++)
		{
			if($slottype_list[$j]["slottype"] == $slottype)
			{
				$slot_name = $slottype_list[$j]["slotname"];
				break;
			}
			else
			{
				$slot_name = "Unkown";
			}
		}
		
				echo("$slot_name$tutorial");
?>
                </th>
                <td class="tdr"><?= number_format($money_in) ?></td>
                <td class="tdr"><?= number_format($money_out) ?></td>
                <td class="tdr"><?= number_format($money_diff) ?></td>
                <td class="tdr"><?= $money_rate ?>%</td> 
            
                <td></td>
            </tr>
<?
    } 
?>
		</tbody>
		<tfoot>
            <tr>
                <th colspan="1">총합</th>
                <td class="tdr"><?= number_format($sum_money_in) ?></td>
                <td class="tdr"><?= number_format($sum_money_out) ?></td>
                <td class="tdr"><?= number_format($sum_money_diff) ?></td>
                <td class="tdr"><?= $sum_money_rate ?>%</td>
            </tr>
		</tfoot>
	</table>
	
	<div class="clear"></div>
	
	<div class="h2_title pt20">
	게임정보 - Amazon
	</div>

	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="20%">
			<col width="20%">
			<col width="20%">
			<col width="20%">
			<col width="20%">
		</colgroup>
		<thead>
            <tr>
                <th>Slot</th>
                <th class="tdr">IN</th>
                <th class="tdr">OUT</th>
                <th class="tdr">수익(OUT-IN)</th>
                <th class="tdr">수익률</th>
            </tr>
		</thead>
		<tbody>
		<?
    $sum_money_in = 0;
    $sum_money_out = 0;
    $sum_money_diff = 0;
    
    for($i=0;$i<sizeof($slotinfolist_amazon);$i++)
    { 
        $slottype = $slotinfolist_amazon[$i]["slottype"];
        $mode = $slotinfolist_amazon[$i]["mode"];
        $money_in = $slotinfolist_amazon[$i]["moneyin"];
        $money_out = $slotinfolist_amazon[$i]["moneyout"];
        
        $money_diff = (string)($money_out - $money_in);
        
        if ($money_in > 0)
            $money_rate = round($money_out/$money_in*10000)/100;
        else
            $money_rate = 0; 
        
        $sum_money_in += $money_in;
        $sum_money_out += $money_out;
        $sum_money_diff += $money_diff;
        
        if ($sum_money_in > 0)
            $sum_money_rate = round($sum_money_out/$sum_money_in*10000)/100;
        else
            $sum_money_rate = 0;
        
        $tutorial = "";
        
        if($mode == "3")
        	$tutorial = "(트리트 금액)";
        else if($mode == "4")
        	$tutorial = " (Tutorial)";
        else if($mode == "5")
        	$tutorial = " (승률 부양)";
        
?>
            <tr>
                <th>
<?

		for($j=0; $j<sizeof($slottype_list); $j++)
		{
			if($slottype_list[$j]["slottype"] == $slottype)
			{
				$slot_name = $slottype_list[$j]["slotname"];
				break;
			}
			else
			{
				$slot_name = "Unkown";
			}
		}
		
				echo("$slot_name$tutorial");
?>
                </th>
                <td class="tdr"><?= number_format($money_in) ?></td>
                <td class="tdr"><?= number_format($money_out) ?></td>
                <td class="tdr"><?= number_format($money_diff) ?></td>
                <td class="tdr"><?= $money_rate ?>%</td> 
            
                <td></td>
            </tr>
<?
    } 
?>
		</tbody>
		<tfoot>
            <tr>
                <th colspan="1">총합</th>
                <td class="tdr"><?= number_format($sum_money_in) ?></td>
                <td class="tdr"><?= number_format($sum_money_out) ?></td>
                <td class="tdr"><?= number_format($sum_money_diff) ?></td>
                <td class="tdr"><?= $sum_money_rate ?>%</td>
            </tr>
		</tfoot>
	</table>
	
	<div class="clear"></div>
	
	<div class="h2_title pt20">
              최근 Jackpot 정보<span style="padding-left:10px;font:11px dotum;"><a href="javascript:pop_jackpot_detail();">+ 더보기</a></span>		
	</div>
	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="">
			<col width="">
			<col width="">
			<col width="">			
			<col width="">	
			<col width="">			
			<col width="">		
		</colgroup>
		<thead>
			<tr>
				<th>OS</th>
				<th>Jackpot Name</th>
				<th>Slot</th>
				<th>Onwer</th>
				<th>Jackpot 금액</th>				
				<th>총 Jackpot 금액</th>
				<th>획득일</th>
			</tr>
		</thead>
		<tbody>
<?
    for ($i=0; $i<sizeof($jackpot_list); $i++)
    {
    	$devicetype = $jackpot_list[$i]["devicetype"];
    	$slottype = $jackpot_list[$i]["slottype"];
    	$amount = $jackpot_list[$i]["amount"];
        $jackpot_owner = $jackpot_list[$i]["jackpot_owner"];
        $jackpot_member_count = $jackpot_list[$i]["jackpot_member_count"];
        $total_jackpot_amount = $jackpot_list[$i]["total_jackpot_amount"];
        $writedate = $jackpot_list[$i]["writedate"];
        $fiestaidx = $jackpot_list[$i]["fiestaidx"];
        
        $os_name = "";
        
        if($devicetype == 0)
        	$os_name = "Web";
        else if($devicetype == 1)
        	$os_name = "Ios";
        else if($devicetype == 2)
        	$os_name = "Android";
    	else if($devicetype = 3)
        	$os_name = "Amazon";
        
        $jackpot_name = "";
        
        if($fiestaidx > 0)
			$jackpot_name = "Fiesta JACKPOT";
        else if($jackpot_member_count == 1)
			$jackpot_name = "DECENT JACKPOT";
		else if($jackpot_member_count == 2)
			$jackpot_name = "FABULOUS JACKPOT";
		else if($jackpot_member_count == 3)
			$jackpot_name = "AWESOME JACKPOT";
		else if($jackpot_member_count == 4)
			$jackpot_name = "AMAZING JACKPOT";
		else if($jackpot_member_count == 5)
			$jackpot_name = "TAKE5 JACKPOT";
		
		$jackpot_onwer_name = "N";
		
		if($jackpot_owner == 1)
			$jackpot_onwer_name = "Y";			
        
    	for($j=0; $j<sizeof($slottype_list); $j++)
		{
			if($slottype_list[$j]["slottype"] == $slottype)
			{
				$slot_name = $slottype_list[$j]["slotname"];
				break;
			}
			else
			{
				$slot_name = "Unkown";
			}
		}
?>
			<tr>
				<td class="tdc point"><?= $os_name ?></td>
				<td class="tdc point"><?= $jackpot_name ?></td>
				<td class="tdc"><?= $slot_name ?></td>
				<td class="tdc"><?= $jackpot_onwer_name ?></td>
				<td class="tdc"><?= number_format($amount) ?></td>
				<td class="tdc"><?= number_format($total_jackpot_amount) ?></td>
				<td class="tdc"><?= $writedate ?></td>
			</tr>
<?
    }
?>
		</tbody>
	</table>
	
	<div class="clear"></div>
		
	<div class="h2_title pt20">
              최근 BigWin 정보<span style="padding-left:10px;font:11px dotum;"><a href="javascript:pop_bigwin_detail();">+ 더보기</a></span>		
	</div>
	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="">
			<col width="">
			<col width="">
			<col width="">			
			<col width="">			
		</colgroup>
		<thead>
			<tr>
				<th>BigWin Name</th>
				<th>Slot</th>
				<th>BigWin Amount</th>
				<th>BigWin Location</th>                
				<th>획득일</th>
			</tr>
		</thead>
		<tbody>
<?
    for ($i=0; $i<sizeof($bigwin_list); $i++)
    {
    	$slottype = $bigwin_list[$i]["slottype"];
    	$objectidx = $bigwin_list[$i]["objectidx"];
        $grade = $bigwin_list[$i]["grade"];
        $amount = $bigwin_list[$i]["amount"];
        $writedate = $bigwin_list[$i]["writedate"];
        
        $gradename = "";
        
        if($objectidx == 0)
        	$bigwin_location = "Ultra";
        else if($objectidx < 1000000)
        	$bigwin_location = "레귤러";
        else if($objectidx >= 1000000)
        	$bigwin_location = "하이롤러";
        
        if($grade == 1)
        	$gradename = "BIG WIN";
        else if($grade == 2)
        	$gradename = "SUPER WIN";
        else if($grade == 3)
        	$gradename = "MEGA WIN";
        else if($grade == 4)
        	$gradename = "ULTRA WIN";
        else if($grade == 5)
        	$gradename = "GRAND WIN";
        else if($grade == 6)
        	$gradename = "GRAND WIN(6)";
        else if($grade == 7)
        	$gradename = "GRAND WIN(7)";
        else if($grade == 8)
        	$gradename = "GRAND WIN(8)";
        else if($grade == 9)
        	$gradename = "GRAND WIN(9)";
        else if($grade == 10)
        	$gradename = "GRAND WIN(10)";
        
    	for($j=0; $j<sizeof($slottype_list); $j++)
		{
			if($slottype_list[$j]["slottype"] == $slottype)
			{
				$slot_name = $slottype_list[$j]["slotname"];
				break;
			}
			else
			{
				$slot_name = "Unkown";
			}
		}
?>
			<tr>
				<td class="tdc point"><?= $gradename ?></td>
				<td class="tdc"><?= $slot_name ?></td>
				<td class="tdc"><?= number_format($amount) ?></td>
				<td class="tdc"><?= $bigwin_location ?></td>				
				<td class="tdc"><?= $writedate ?></td>
			</tr>
<?
    }
?>
		</tbody>
	</table>
	
	<div class="clear"></div>
	
	<div class="h2_title pt20">
		Share 현황
        <input type="button" class="btn_03" style="margin-bottom:5px;" value="Share 현황 보기" onclick="pop_share_info()">
	 </div>
		
	<div class="clear"></div>	
	
	<div class="h2_title pt20">
		Unknown 코인 정보<span style="padding-left:10px;font:11px dotum;"><a href="javascript:pop_unknown_coin_detail();">+ 더보기</a></span>
	</div>
<?
    if (sizeof($unknownlist) != 0)
    { 
?>
	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="55%">
            <col width="25%">            
            <col width="20%">
		</colgroup>
		<thead>
            <tr>
                <th>종류</th>
                <th>금액</th>
                <th>일시</th>
            </tr>
		</thead>
		<tbody>
<?
                
        for($i=0;$i<sizeof($unknownlist);$i++)
        { 
            $unknown = $unknownlist[$i];
            
            $category = $unknown["category"];
            $coin_change = $unknown["coin_change"];
            $writedate = $unknown["writedate"];
            
            if($writedate <= "2016-02-24 08:22:00")
	        {
	            if ($category == "0")
	            	$category = "Slot Exit (동기화)";
				else if ($category == "1")
					$category = "Login";
				else if ($category == "2")
					$category = "Slot Exit";
				else if ($category == "3")
					$category = "Login (한도초과)";
				else if ($category == "4")
					$category = "Slot Exit (한도초과)";
				else if ($category == "5")
					$category = "Slot Exit (20분내 결제 사용자, 로그)";
				else if ($category == "6")
					$category = "Slot Exit (Big Wheel)";
				else if ($category == "7")
					$category = "Web, Mobile 동시 로그인";				
				else if ($category == "12")
					$category = "IOS";
				else if ($category == "14")
					$category = "IOS (로그)";
				else if ($category == "22")
					$category = "Login시 코인정보 강제 업데이트(서버와 정보 다름)";
				else if ($category == "24")
					$category = "Login시 코인정보 강제 업데이트(범위 초과, 로그)";
				else if ($category == "25")
					$category = "Login시 코인정보 강제 업데이트(20분내 결제 사용자, 로그)";
				else if ($category == "26")
					$category = "Mypot, Megaspin 게이지 조정(로그)";
				else if ($category == "112")
					$category = "Android";
				else if ($category == "114")
					$category = "Android (로그)";
				else if ($category == "122")
					$category = "Amazon";
				else if ($category == "124")
					$category = "Amazon (로그)";
			}
			else
			{
				if ($category == "1")
					$category = "게임 로그인 직후(코인 변화량 : -1,000,000,000 ~ -2,000, 최근 20분내 결제 X)";
				else if ($category == "2")
					$category = "게임 로그인 직후(코인 변화량 : -1,000,000,000 ~ -2,000, 최근 20분내 결제 O, 로그)";
				else if ($category == "3")
					$category = "게임 로그인 직후(코인 변화량 : -2,000, 200,000,000 사이)";
				else if ($category == "4")
					$category = "게임 로그인 직후(코인 변화량 : 200,000,000 크거나 -1,000,000,000 작을 때, 로그)";
				else if ($category == "11")
					$category = "슬롯에서 exit(코인 변화량 : -1,000,000,000 ~ -2,000, 최근 20분내 결제 X)";
				else if ($category == "12")
					$category = "슬롯에서 exit(코인 변화량 : -1,000,000,000 ~ -2,000, 최근 20분내 결제 O, 로그)";
				else if ($category == "13")
					$category = "슬롯에서 exit(코인 변화량 : -2,000, 200,000,000 사이)";
				else if ($category == "14")
					$category = "슬롯에서 exit(코인 변화량 : 200,000,000 크거나 -1,000,000,000 작을 때, 로그)";
				else if ($category == "22")
				    $category = "Login시 코인정보 강제 업데이트(서버와 정보 다름)";
			}
?>
            <tr>
                <td class="tdc"><?= $category ?></td>
                <td class="tdc"><?= number_format($coin_change) ?></td>                
                <td class="tdc"><?= $writedate ?></td>
            </tr>
<?
        } 
?>
		</tbody>
           
	</table>
<?
    } 
?>
		
	<div class="clear"></div>		
	
	<div class="h2_title pt20">
		최근 행동 로그 - Web
		<span style="padding-left:10px;font:11px dotum;"><a href="javascript:pop_user_actionlist('web')">+ 더보기</a></span>
	</div>

	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="120">  
		</colgroup>
		<thead>
			<tr>
				<th>IP 주소</th>
				<th>category</th>
				<th>category_info</th>
				<th>action</th>
				<th>action_info</th>
				<th>staytime</th>
				<th>isplaynow</th>
				<th>coin</th>
				<th>version</th>
				<th>접속일시</th>
			</tr>
		</thead>
		<tbody>
<?
    
    for($i=0; $i<sizeof($action_log_list); $i++)
    {
        $ipaddress = $action_log_list[$i]["ipaddress"];
        $category = $action_log_list[$i]["category"];
        $category_info = $action_log_list[$i]["category_info"];
        $action = $action_log_list[$i]["action"];
        $action_info = $action_log_list[$i]["action_info"];
        $staytime = $action_log_list[$i]["staytime"];
        $isplaynow = $action_log_list[$i]["isplaynow"];
        $coin = $action_log_list[$i]["coin"];
        $is_v2 = $action_log_list[$i]["is_v2"];
        $writedate = $action_log_list[$i]["writedate"];
        
        if ($coin == "-1")
            $coin = "-";
        else 
            $coin = make_price_format($coin);
        
        if($is_v2 == 1)
        	$is_v2 = "2.0";
		else
			$is_v2 = "1.0";
?>
				<tr>
					<td class="tdc"><?= $ipaddress ?></td>
					<td class="tdc"><?= $category ?></td>
					<td class="tdc"><?= $category_info ?></td>
					<td class="tdc"><?= $action ?></td>
					<td class="tdc"><?= $action_info ?></td>
					<td class="tdc"><?= $staytime ?></td>
					<td class="tdc"><?= $isplaynow ?></td>
					<td class="tdc"><?= $coin ?></td>
					<td class="tdc"><?= $is_v2 ?></td>
					<td class="tdc"><?= $writedate ?></td>
				</tr>
<?
    } 
?>
		</tbody>
	</table>
	
	<div class="clear"></div>		
	
	<div class="h2_title pt20">
		최근 행동 로그 - Ios
		<span style="padding-left:10px;font:11px dotum;"><a href="javascript:pop_user_actionlist('ios')">+ 더보기</a></span>
	</div>

	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="120">  
		</colgroup>
		<thead>
			<tr>
				<th>IP 주소</th>
				<th>category</th>
				<th>category_info</th>
				<th>action</th>
				<th>action_info</th>
				<th>staytime</th>
				<th>isplaynow</th>
				<th>coin</th>
				<th>접속일시</th>
			</tr>
		</thead>
		<tbody>
<?
    
    for($i=0; $i<sizeof($action_log_ios_list); $i++)
    {
        $ipaddress = $action_log_ios_list[$i]["ipaddress"];
        $category = $action_log_ios_list[$i]["category"];
        $category_info = $action_log_ios_list[$i]["category_info"];
        $action = $action_log_ios_list[$i]["action"];
        $action_info = $action_log_ios_list[$i]["action_info"];
        $staytime = $action_log_ios_list[$i]["staytime"];
        $isplaynow = $action_log_ios_list[$i]["isplaynow"];
        $coin = $action_log_ios_list[$i]["coin"];
        $writedate = $action_log_ios_list[$i]["writedate"];
        
        if ($coin == "-1")
            $coin = "-";
        else 
            $coin = make_price_format($coin);
?>
				<tr>
					<td class="tdc"><?= $ipaddress ?></td>
					<td class="tdc"><?= $category ?></td>
					<td class="tdc"><?= $category_info ?></td>
					<td class="tdc"><?= $action ?></td>
					<td class="tdc"><?= $action_info ?></td>
					<td class="tdc"><?= $staytime ?></td>
					<td class="tdc"><?= $isplaynow ?></td>
					<td class="tdc"><?= $coin ?></td>
					<td class="tdc"><?= $writedate ?></td>
				</tr>
<?
    } 
?>
		</tbody>
	</table>
	
	<div class="clear"></div>		
	
	<div class="h2_title pt20">
		최근 행동 로그 - Android
		<span style="padding-left:10px;font:11px dotum;"><a href="javascript:pop_user_actionlist('android')">+ 더보기</a></span>
	</div>

	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="120">  
		</colgroup>
		<thead>
			<tr>
				<th>IP 주소</th>
				<th>category</th>
				<th>category_info</th>
				<th>action</th>
				<th>action_info</th>
				<th>staytime</th>
				<th>isplaynow</th>
				<th>coin</th>
				<th>접속일시</th>
			</tr>
		</thead>
		<tbody>
<?
    
    for($i=0; $i<sizeof($action_log_android_list); $i++)
    {
        $ipaddress = $action_log_android_list[$i]["ipaddress"];
        $category = $action_log_android_list[$i]["category"];
        $category_info = $action_log_android_list[$i]["category_info"];
        $action = $action_log_android_list[$i]["action"];
        $action_info = $action_log_android_list[$i]["action_info"];
        $staytime = $action_log_android_list[$i]["staytime"];
        $isplaynow = $action_log_android_list[$i]["isplaynow"];
        $coin = $action_log_android_list[$i]["coin"];
        $writedate = $action_log_android_list[$i]["writedate"];
        
        if ($coin == "-1")
            $coin = "-";
        else 
            $coin = make_price_format($coin);
?>
				<tr>
					<td class="tdc"><?= $ipaddress ?></td>
					<td class="tdc"><?= $category ?></td>
					<td class="tdc"><?= $category_info ?></td>
					<td class="tdc"><?= $action ?></td>
					<td class="tdc"><?= $action_info ?></td>
					<td class="tdc"><?= $staytime ?></td>
					<td class="tdc"><?= $isplaynow ?></td>
					<td class="tdc"><?= $coin ?></td>
					<td class="tdc"><?= $writedate ?></td>
				</tr>
<?
    } 
?>
		</tbody>
	</table>
	
	<div class="clear"></div>		
	
	<div class="h2_title pt20">
		최근 행동 로그 - Amazon
		<span style="padding-left:10px;font:11px dotum;"><a href="javascript:pop_user_actionlist('amazon')">+ 더보기</a></span>
	</div>

	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="120">  
		</colgroup>
		<thead>
			<tr>
				<th>IP 주소</th>
				<th>category</th>
				<th>category_info</th>
				<th>action</th>
				<th>action_info</th>
				<th>staytime</th>
				<th>isplaynow</th>
				<th>coin</th>
				<th>접속일시</th>
			</tr>
		</thead>
		<tbody>
<?
    
    for($i=0; $i<sizeof($action_log_amazon_list); $i++)
    {
        $ipaddress = $action_log_amazon_list[$i]["ipaddress"];
        $category = $action_log_amazon_list[$i]["category"];
        $category_info = $action_log_amazon_list[$i]["category_info"];
        $action = $action_log_amazon_list[$i]["action"];
        $action_info = $action_log_amazon_list[$i]["action_info"];
        $staytime = $action_log_amazon_list[$i]["staytime"];
        $isplaynow = $action_log_amazon_list[$i]["isplaynow"];
        $coin = $action_log_amazon_list[$i]["coin"];
        $writedate = $action_log_amazon_list[$i]["writedate"];
        
        if ($coin == "-1")
            $coin = "-";
        else 
            $coin = make_price_format($coin);
?>
				<tr>
					<td class="tdc"><?= $ipaddress ?></td>
					<td class="tdc"><?= $category ?></td>
					<td class="tdc"><?= $category_info ?></td>
					<td class="tdc"><?= $action ?></td>
					<td class="tdc"><?= $action_info ?></td>
					<td class="tdc"><?= $staytime ?></td>
					<td class="tdc"><?= $isplaynow ?></td>
					<td class="tdc"><?= $coin ?></td>
					<td class="tdc"><?= $writedate ?></td>
				</tr>
<?
    } 
?>
		</tbody>
	</table>
	
	<div class="clear"></div>	
		<div class="h2_title pt20">
		 최근 로그인 정보<span style="padding-left:10px;font:11px dotum;"><a href="/m2_user/user_login_log.php?useridx=<?= $useridx ?>">+ 더보기</a></span>
		 <input type="button" class="btn_03" value="Excel 다운로드 " onclick="window.location.href='login_log_excel.php?useridx=<?= $useridx ?>'">		
	</div>

	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="120">
			<col width="">
			<col width="80">
			<col width="100">			
			<col width="120">  
		</colgroup>
		<thead>
			<tr>
				<th>IP 주소</th>
				<th>브라우저</th>
				<th>ad flag</th>				
				<th>보유코인</th>
				<th>접속일시</th>
			</tr>
		</thead>
		<tbody>
<?
    
    for($i=0; $i<sizeof($login_list); $i++)
    {
        $ipaddress = $login_list[$i]["ipaddress"];
        $useragent = $login_list[$i]["useragent"];
        $adflag = $login_list[$i]["adflag"];        
        $coin = $login_list[$i]["coin"];
        $writedate = $login_list[$i]["writedate"];
?>
			<tr>
				<td class="tdc"><?= $ipaddress ?></td>
				<td class="tdl"><?= $useragent ?></td>
				<td class="tdc"><?= $adflag ?></td>				
				<td class="tdc"><?= number_format($coin) ?></td>
				<td class="tdl"><?= $writedate ?></td>
			</tr>
<?
    } 
?>
		</tbody>
	</table>
	
	<div class="clear"></div>	
	<div class="h2_title pt20">
                최근 클라이언트 로그 - Web<span style="padding-left:10px;font:11px dotum;"><a href="/m8_monitoring/client_log_mng.php?facebookid=<?= $userid ?>">+ 더보기</a></span>
	</div>

    <table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="120">
			<col width="150">
			<col width="150">
			<col width="150">
			<col width="150">  
		</colgroup>
		<thead>
			<tr>
				<th>IP 주소</th>
				<th>cmd</th>
				<th>에러코드</th>
				<th>반응시간</th>
				<th>접속일시</th>
			</tr>
		</thead>
		<tbody>
<?
    
    for($i=0; $i<sizeof($client_log_list); $i++)
    {
        $ipaddress = $client_log_list[$i]["ipaddress"];
        $cmd = $client_log_list[$i]["cmd"];
        $errcode = $client_log_list[$i]["errcode"];
        $latency = $client_log_list[$i]["latency"];
        $writedate = $client_log_list[$i]["writedate"];
?>
			<tr>
				<td class="tdc"><?= $ipaddress ?></td>
				<td class="tdc"><?= $cmd ?></td>
				<td class="tdc"><?= $errcode ?></td>
				<td class="tdc"><?= $latency ?></td>
				<td class="tdc"><?= $writedate ?></td>
			</tr>
<?
    } 
?>
		</tbody>
	</table>
	
		<div class="clear"></div>	
	<div class="h2_title pt20">
                최근 클라이언트 로그 - Ios<span style="padding-left:10px;font:11px dotum;"><a href="/m8_monitoring/client_log_mng_ios.php?useridx=<?= $useridx ?>">+ 더보기</a></span>
	</div>

    <table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="120">
			<col width="150">
			<col width="150">
			<col width="150">
			<col width="150">  
		</colgroup>
		<thead>
			<tr>
				<th>IP 주소</th>
				<th>cmd</th>
				<th>에러코드</th>
				<th>반응시간</th>
				<th>접속일시</th>
			</tr>
		</thead>
		<tbody>
<?
    
    for($i=0; $i<sizeof($client_log_ios_list); $i++)
    {
        $ipaddress = $client_log_ios_list[$i]["ipaddress"];
        $cmd = $client_log_ios_list[$i]["cmd"];
        $errcode = $client_log_ios_list[$i]["errcode"];
        $latency = $client_log_ios_list[$i]["latency"];
        $writedate = $client_log_ios_list[$i]["writedate"];
?>
			<tr>
				<td class="tdc"><?= $ipaddress ?></td>
				<td class="tdc"><?= $cmd ?></td>
				<td class="tdc"><?= $errcode ?></td>
				<td class="tdc"><?= $latency ?></td>
				<td class="tdc"><?= $writedate ?></td>
			</tr>
<?
    } 
?>
		</tbody>
	</table>
	
	<div class="clear"></div>	
	<div class="h2_title pt20">
                최근 클라이언트 로그 - Android<span style="padding-left:10px;font:11px dotum;"><a href="/m8_monitoring/client_log_mng_android.php?useridx=<?= $useridx ?>">+ 더보기</a></span>
	</div>

    <table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="120">
			<col width="150">
			<col width="150">
			<col width="150">
			<col width="150">  
		</colgroup>
		<thead>
			<tr>
				<th>IP 주소</th>
				<th>cmd</th>
				<th>에러코드</th>
				<th>반응시간</th>
				<th>접속일시</th>
			</tr>
		</thead>
		<tbody>
<?
    
    for($i=0; $i<sizeof($client_log_android_list); $i++)
    {
        $ipaddress = $client_log_android_list[$i]["ipaddress"];
        $cmd = $client_log_android_list[$i]["cmd"];
        $errcode = $client_log_android_list[$i]["errcode"];
        $latency = $client_log_android_list[$i]["latency"];
        $writedate = $client_log_android_list[$i]["writedate"];
?>
			<tr>
				<td class="tdc"><?= $ipaddress ?></td>
				<td class="tdc"><?= $cmd ?></td>
				<td class="tdc"><?= $errcode ?></td>
				<td class="tdc"><?= $latency ?></td>
				<td class="tdc"><?= $writedate ?></td>
			</tr>
<?
    } 
?>
		</tbody>
	</table>
	
		<div class="clear"></div>	
	<div class="h2_title pt20">
                최근 클라이언트 로그 - Amazon<span style="padding-left:10px;font:11px dotum;"><a href="/m8_monitoring/client_log_mng_amazon.php?useridx=<?= $useridx ?>">+ 더보기</a></span>
	</div>

    <table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="120">
			<col width="150">
			<col width="150">
			<col width="150">
			<col width="150">  
		</colgroup>
		<thead>
			<tr>
				<th>IP 주소</th>
				<th>cmd</th>
				<th>에러코드</th>
				<th>반응시간</th>
				<th>접속일시</th>
			</tr>
		</thead>
		<tbody>
<?
    
    for($i=0; $i<sizeof($client_log_amazon_list); $i++)
    {
        $ipaddress = $client_log_amazon_list[$i]["ipaddress"];
        $cmd = $client_log_amazon_list[$i]["cmd"];
        $errcode = $client_log_amazon_list[$i]["errcode"];
        $latency = $client_log_amazon_list[$i]["latency"];
        $writedate = $client_log_amazon_list[$i]["writedate"];
?>
			<tr>
				<td class="tdc"><?= $ipaddress ?></td>
				<td class="tdc"><?= $cmd ?></td>
				<td class="tdc"><?= $errcode ?></td>
				<td class="tdc"><?= $latency ?></td>
				<td class="tdc"><?= $writedate ?></td>
			</tr>
<?
    } 
?>
		</tbody>
	</table>
	
	<div class="clear"></div>	
	<div class="h2_title pt20">
		일별 슬롯통계(최근1주일) - Web
	<span style="padding-left:10px;font:11px dotum;"><a href="javascript:pop_slotdetail_view('web')">+ 슬롯별 승률상세보기</a></span>
	</div>
            
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="15%">
			<col width="25%">
			<col width="25%">
			<col width="10%">
			<col width="25%">
		</colgroup>
		<thead>
            <tr>
                <th class="tdㅊ">날짜</th>
                <th class="tdr">money_in</th>
                <th class="tdr">money_out</th>
                <th class="tdr">승률</th>
                <th class="tdr">승리금액</th>
            </tr>
		</thead>
		<tbody>
<?
		$summoney_in = 0;
		$summoney_out = 0;
	
		for($i=0; $i<sizeof($dailystatlist); $i++)
		{
			$today = $dailystatlist[$i]["today"];
			$money_in = $dailystatlist[$i]["money_in"];
			$money_out = $dailystatlist[$i]["money_out"];
			$slot_winrate = round($dailystatlist[$i]["slot_winrate"]);
			$winmoney = $dailystatlist[$i]["winmoney"];
			
			$summoney_in += $money_in;
			$summoney_out += $money_out;
?>
				<tr>
        			<td class="tdc"><?= $today ?></td>
            		<td class="tdr"><?= number_format($money_in) ?></td>
            		<td class="tdr"><?= number_format($money_out) ?></td>
            		<td class="tdr"><?= $slot_winrate ?>%</td>
            		<td class="tdr"><?= number_format($winmoney) ?></td>
        		</tr>
<?
		}
		
		if(sizeof($dailystatlist) > 0)
		{
			if($summoney_in > 0)
				$totalwinrate = ($summoney_in == 0)?0:round($summoney_out / $summoney_in * 100, 2);
			else
				$totalwinrate = 0;
?>
				<tr>
					<td class="tdc point">합계</td>
            		<td class="tdr point"><?= number_format($summoney_in) ?></td>
            		<td class="tdr point"><?= number_format($summoney_out) ?></td>
            		<td class="tdr point"><?= $totalwinrate ?>%</td>
            		<td class="tdr point"><?= number_format($summoney_out-$summoney_in) ?></td>
				</tr>
<?
		}
?>
		</tbody>
	</table>
	
	<div class="clear"></div>	
	<div class="h2_title pt20">
		일별 슬롯통계(최근1주일) - Ios
	<span style="padding-left:10px;font:11px dotum;"><a href="javascript:pop_slotdetail_view('ios')">+ 슬롯별 승률상세보기</a></span>
	</div>
            
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="15%">
			<col width="25%">
			<col width="25%">
			<col width="10%">
			<col width="25%">
		</colgroup>
		<thead>
            <tr>
                <th class="tdㅊ">날짜</th>
                <th class="tdr">money_in</th>
                <th class="tdr">money_out</th>
                <th class="tdr">승률</th>
                <th class="tdr">승리금액</th>
            </tr>
		</thead>
		<tbody>
<?
		$summoney_in = 0;
		$summoney_out = 0;
	
		for($i=0; $i<sizeof($dailystatlist_ios); $i++)
		{
			$today = $dailystatlist_ios[$i]["today"];
			$money_in = $dailystatlist_ios[$i]["money_in"];
			$money_out = $dailystatlist_ios[$i]["money_out"];
			$slot_winrate = round($dailystatlist_ios[$i]["slot_winrate"]);
			$winmoney = $dailystatlist_ios[$i]["winmoney"];
			
			$summoney_in += $money_in;
			$summoney_out += $money_out;
?>
				<tr>
        			<td class="tdc"><?= $today ?></td>
            		<td class="tdr"><?= number_format($money_in) ?></td>
            		<td class="tdr"><?= number_format($money_out) ?></td>
            		<td class="tdr"><?= $slot_winrate ?>%</td>
            		<td class="tdr"><?= number_format($winmoney) ?></td>
        		</tr>
<?
		}
		
		if(sizeof($dailystatlist_ios) > 0)
		{
			if($summoney_in > 0)
				$totalwinrate = ($summoney_in == 0)?0:round($summoney_out / $summoney_in * 100, 2);
			else
				$totalwinrate = 0;
?>
				<tr>
					<td class="tdc point">합계</td>
            		<td class="tdr point"><?= number_format($summoney_in) ?></td>
            		<td class="tdr point"><?= number_format($summoney_out) ?></td>
            		<td class="tdr point"><?= $totalwinrate ?>%</td>
            		<td class="tdr point"><?= number_format($summoney_out-$summoney_in) ?></td>
				</tr>
<?
		}
?>
		</tbody>
	</table>
	
	<div class="clear"></div>	
	<div class="h2_title pt20">
		일별 슬롯통계(최근1주일) - Android
	<span style="padding-left:10px;font:11px dotum;"><a href="javascript:pop_slotdetail_view('android')">+ 슬롯별 승률상세보기</a></span>
	</div>
            
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="15%">
			<col width="25%">
			<col width="25%">
			<col width="10%">
			<col width="25%">
		</colgroup>
		<thead>
            <tr>
                <th class="tdㅊ">날짜</th>
                <th class="tdr">money_in</th>
                <th class="tdr">money_out</th>
                <th class="tdr">승률</th>
                <th class="tdr">승리금액</th>
            </tr>
		</thead>
		<tbody>
<?
		$summoney_in = 0;
		$summoney_out = 0;
	
		for($i=0; $i<sizeof($dailystatlist_android); $i++)
		{
			$today = $dailystatlist_android[$i]["today"];
			$money_in = $dailystatlist_android[$i]["money_in"];
			$money_out = $dailystatlist_android[$i]["money_out"];
			$slot_winrate = round($dailystatlist_android[$i]["slot_winrate"]);
			$winmoney = $dailystatlist_android[$i]["winmoney"];
			
			$summoney_in += $money_in;
			$summoney_out += $money_out;
?>
				<tr>
        			<td class="tdc"><?= $today ?></td>
            		<td class="tdr"><?= number_format($money_in) ?></td>
            		<td class="tdr"><?= number_format($money_out) ?></td>
            		<td class="tdr"><?= $slot_winrate ?>%</td>
            		<td class="tdr"><?= number_format($winmoney) ?></td>
        		</tr>
<?
		}
		
		if(sizeof($dailystatlist_android) > 0)
		{
			if($summoney_in > 0)
				$totalwinrate = ($summoney_in == 0)?0:round($summoney_out / $summoney_in * 100, 2);
			else
				$totalwinrate = 0;
?>
				<tr>
					<td class="tdc point">합계</td>
            		<td class="tdr point"><?= number_format($summoney_in) ?></td>
            		<td class="tdr point"><?= number_format($summoney_out) ?></td>
            		<td class="tdr point"><?= $totalwinrate ?>%</td>
            		<td class="tdr point"><?= number_format($summoney_out-$summoney_in) ?></td>
				</tr>
<?
		}
?>
		</tbody>
	</table>
	
	<div class="clear"></div>	
	<div class="h2_title pt20">
		일별 슬롯통계(최근1주일) - Amazon
	<span style="padding-left:10px;font:11px dotum;"><a href="javascript:pop_slotdetail_view('amazon')">+ 슬롯별 승률상세보기</a></span>
	</div>
            
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="15%">
			<col width="25%">
			<col width="25%">
			<col width="10%">
			<col width="25%">
		</colgroup>
		<thead>
            <tr>
                <th class="tdㅊ">날짜</th>
                <th class="tdr">money_in</th>
                <th class="tdr">money_out</th>
                <th class="tdr">승률</th>
                <th class="tdr">승리금액</th>
            </tr>
		</thead>
		<tbody>
<?
		$summoney_in = 0;
		$summoney_out = 0;
	
		for($i=0; $i<sizeof($dailystatlist_amazon); $i++)
		{
			$today = $dailystatlist_amazon[$i]["today"];
			$money_in = $dailystatlist_amazon[$i]["money_in"];
			$money_out = $dailystatlist_amazon[$i]["money_out"];
			$slot_winrate = round($dailystatlist_amazon[$i]["slot_winrate"]);
			$winmoney = $dailystatlist_amazon[$i]["winmoney"];
			
			$summoney_in += $money_in;
			$summoney_out += $money_out;
?>
				<tr>
        			<td class="tdc"><?= $today ?></td>
            		<td class="tdr"><?= number_format($money_in) ?></td>
            		<td class="tdr"><?= number_format($money_out) ?></td>
            		<td class="tdr"><?= $slot_winrate ?>%</td>
            		<td class="tdr"><?= number_format($winmoney) ?></td>
        		</tr>
<?
		}
		
		if(sizeof($dailystatlist_amazon) > 0)
		{
			if($summoney_in > 0)
				$totalwinrate = ($summoney_in == 0)?0:round($summoney_out / $summoney_in * 100, 2);
			else
				$totalwinrate = 0;
?>
				<tr>
					<td class="tdc point">합계</td>
            		<td class="tdr point"><?= number_format($summoney_in) ?></td>
            		<td class="tdr point"><?= number_format($summoney_out) ?></td>
            		<td class="tdr point"><?= $totalwinrate ?>%</td>
            		<td class="tdr point"><?= number_format($summoney_out-$summoney_in) ?></td>
				</tr>
<?
		}
?>
		</tbody>
	</table>
		
	<div class="clear"></div>
	
	<div class="h2_title pt20">
		이벤트 참여목록(30일 이내)
	<span style="padding-left:10px;font:11px dotum;"><a href="javascript:pop_user_event_detail()">+ 더보기</a></span>
	</div>
	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="">
			<col width="200">
			<col width="200">
		</colgroup>
		<thead>
			<tr>
				<th class="tbl">이벤트 제목</th>
				<th class="tdr">지급량</th>
				<th>참여일</th>
			</tr>
		</thead>
		<tbody>
<?
    for ($i=0; $i<sizeof($event_resultlist); $i++)
    {
    	$eventidx = $event_resultlist[$i]["eventidx"];
    	$category = $event_resultlist[$i]["category"];
        $title = $event_resultlist[$i]["title"];
        $reward_type = $event_resultlist[$i]["reward_type"];
        $reward_amount = $event_resultlist[$i]["reward_amount"];
        $writedate = $event_resultlist[$i]["writedate"];
        
        $reward_name = "";
        
        if($reward_type == "1")
        	$reward_name = "Coin";
        else if($reward_type == "2")
        	$reward_name = "TY Point";
?> 
			<tr onmouseover="className='tr_over'" onmouseout="className=''" >
				<td class="tbl point"><?= $title ?></td>
				<td class="tdr point"><?= number_format($reward_amount) ?> <?= $reward_name ?></td>
				<td class="tdc"><?= $writedate ?></td>
			</tr>
<?
    }
?>
		</tbody>
	</table>
	
	<div class="clear"></div>
	
	<div class="h2_title pt20">
               관리자 무료 자원 발급 정보
	<span style="padding-left:10px;font:11px dotum;"><a href="javascript:pop_free_detail()">+ 더보기</a></span>
	</div>
	<table class="tbl_list_basic1" style="width:100%;">
		<colgroup>
			<col width="">
			<col width="">
			<col width="">
			<col width="">			
		</colgroup>
		<thead>
			<tr>
				<th>발급 사유</th>
				<th>발급 메세지</th>
				<th>Coin</th>
				<th>발급일</th>
			</tr>
		</thead>
		<tbody>
<?
    for ($i=0; $i<sizeof($freecoin_list); $i++)
    {
        $reason = $freecoin_list[$i]["reason"];
        $message = $freecoin_list[$i]["message"];        
        $free_coin = $freecoin_list[$i]["freecoin"];        
        $writedate = $freecoin_list[$i]["writedate"];
?>
			<tr>
				<td class="tdc point"><?= $reason ?></td>
				<td class="tdc point"><?= $message ?></td>
				<td class="tdc point_title"><?= make_price_format($free_coin) ?></td>
				<td class="tdc point"><?= substr($writedate,0,10) ?></td>
			</tr>
<?
    }
?>
		</tbody>
	</table>
	
	<div class="clear"></div>
            
	<div class="h2_title pt20">
                친구목록 (<?= sizeof($friendlist) ?>명)
<? 
	if ($useridx != HELPER_FB_ID)
	{
?> 
		<input type="button" class="btn_03" style="margin-bottom:5px;" value="친구목록  자세히 보기" onclick="get_friend_list()">
<?
	}

	if($dbaccesstype == 1 || $dbaccesstype == 2)
	{
?>

		<input type="button" class="btn_03" style="margin-bottom:5px;" value="친구목록 갱신" onclick="update_friend()">
<?
	}
?> 
	</div>
	<div class="friendlist_wrap" id="friendlist_wrap">
	</div>
		
	<div class="clear"></div>
            
	<div class="button_wrap tdr" style="margin-top:15px;">
		<input type="button" class="btn_setting_01" value="목록" onclick="go_page('<?= $list_page ?>')">
	</div>
</div>
<!--  //CONTENTS WRAP -->
<?        
    $db_main->end();
    $db_main2->end();
    $db_analysis->end();
    $db_friend->end();
    $db_other->end();
    $db_mobile->end();  
    $db_livestats->end();
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
		
    
