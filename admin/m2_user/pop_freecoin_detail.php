<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	$useridx = $_POST["useridx"];
	
	$db_main2 = new CDatabase_Main2();
	
	$sql = "SELECT COUNT(*) FROM tbl_user_freecoin_log_".($useridx % 10)." WHERE useridx = $useridx";
	$list_totalcnt = $db_main2->getvalue($sql);
	
	$db_main2->end();
?>
<script type="text/javascript">
	var g_page = 1;

	$(document).ready(function()
	{
		refresh();
	});

	function refresh()
	{
		var param = {};
		param.page = g_page;
		param.useridx = "<?= $useridx ?>";

		WG_ajax_list("user/get_freecoinlog_list", param, fnContentLoad_callback, true);
	}

	function fnContentLoad_callback(result, reason, totalcount, list)
	{
		if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            update_pagenation(g_page, <?= $list_totalcnt?>, 10, 10);
            
            var tbody = document.getElementById("list_contents");
            
            while (tbody.childNodes.length > 0)
                tbody.removeChild(tbody.childNodes[0]);

            for (var i=0; i<list.length; i++)
            {
                var tr = document.createElement("tr");
                var td = document.createElement("td");

				var typename = "";
				var amount = 0;
				var categoryname = "";

				if(list[i].category == 0)
					typename = "Web";
				else if(list[i].category == 1)
					typename = "IOS";
				else if(list[i].category == 2)
					typename = "Android";
				else if(list[i].category == 3)
					typename = "Amazon";		        
		        
		        amount = list[i].amount;	
		        categoryname = list[i].name;
		        	        	
				if(list[i].inbox_type == 301)
					categoryname = "Ty Point";		        
                
                tr.appendChild(td);
                td.className = "tdc point";
                td.innerText = typename;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = categoryname;
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                
                if(list[i].inbox_type == 301)
                	td.innerText = make_price_format(1);
                else	
                	td.innerText = make_price_format(amount);

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].writedate;
                
                tbody.appendChild(tr);
            }
        }
	}
</script>
<div id="layer_wrap">
	<div class="layer_header" >
        <div class="layer_title">무료자원 로그</div>
    	<img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>
    
    <div class="layer_contents_wrap" style="width:700px;height:435px;">
    	 <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
	            <table class="tbl_list_basic1" width="1200px;">
	                <colgroup>
	                    <col width="">
	                    <col width="">
	                    <col width="">	                    
	                    <col width="">	                    
	                </colgroup>
	                <thead>
	                    <tr>
							<th>접속</th>
							<th>타입</th>
                			<th>Coin</th>
							<th>획득일</th>
	                    </tr>
	                </thead>
	                <tbody id="list_contents">
	                </tbody>
	            </table>  
            	<div class="pagenation mt20" id=pagenation></div>
            </form>  
         </div>
    </div>
</div>