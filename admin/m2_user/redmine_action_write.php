<?
    $top_menu = "user";
    $sub_menu = "redmine_action_write";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    function save_actionlog_blackbox()
    {
        var input_form = document.input_form;

        if (input_form.redmine.value == "")
        {
            alert("레드마인 이슈번호를 입력하세요.");
            input_form.redmine.focus();
            return;
        }

        if (input_form.useridx.value == "")
        {
            alert("useridx를 입력하세요.");
            input_form.useridx.focus();
            return;
        }

        if (get_radio("category") == "")
        {
            alert("os을 선택해주세요.");
            return;
        }

        /* if (input_form.writedate_date.value == "")
        {
            alert("날짜를 선택해주세요.");
            input_form.writedate_date.focus();
            return;
        }

        if (input_form.writedate_hour.value == "")
        {
            alert("시간을 입력해주세요.");
            input_form.writedate_hour.focus();
            return;
        }

        if (input_form.writedate_min.value == "")
        {
            alert("분을 입력해주세요.");
            input_form.writedate_min.focus();
            return;
        } */
              
        var param = {};
        param.redmine = input_form.redmine.value;
        param.useridx = input_form.useridx.value;
        param.os = get_radio("category");
        
        //param.put("writedate_date", input_form.writedate_date.value);
        //param.put("writedate_hour", input_form.writedate_hour.value);
        //param.put("writedate_min", input_form.writedate_min.value);
        WG_ajax_list("user/get_actionlog_blackbox", param, get_actionlog_blackbox_callback, true);
    }

    function get_actionlog_blackbox_callback(result, reason, totalcount, list)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            if(list.length > 0)
            {
            	var param = {};
                param.redmine = input_form.redmine.value;
                param.useridx = input_form.useridx.value;
                param.os = get_radio("category");
                //param.put("writedate_date", input_form.writedate_date.value);
                //param.put("writedate_hour", input_form.writedate_hour.value);
                //param.put("writedate_min", input_form.writedate_min.value);
                WG_ajax_execute("user/insert_actionlog_blackbox", param, insert_actionlog_blackbox_callback);
            }
            else
                alert("해당 시간에 행동로그가 없습니다.");
        }
    }

    function insert_actionlog_blackbox_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
			alert("정상적으로 저장하였습니다.");
			window.location.href = "redmine_action.php";
        }
    }

    $(function() {
        $("#writedate_date").datepicker({ });
    });
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 레드마인 이슈 행동로그 추가</div>
            </div>
            <!-- //title_warp -->
            
            <form name="input_form" id="input_form" onsubmit="return false">
            
            <div class="h2_title pt20">[보관할 행동로그 설정]</div>
            <table class="tbl_view_basic">
                <colgroup>
                    <col width="180">
                    <col width="">
                </colgroup>
                <tbody>
                	<tr>
                    	<th>레드마인 이슈 번호</th>
            			<td>
            				<input type="text" class="search_text" name="redmine" id="redmine" style="width:100px" onkeypress="return checkonlynum()" />
            			</td>
                    </tr>
                	<tr>
                    	<th>Useridx</th>
            			<td>
            				<input type="text" class="search_text" name="useridx" id="useridx" style="width:200px" onkeypress="return checkonlynum()" />
            			</td>
                    </tr>
                	<tr>
                		<th>OS</th>
                		<td>
                			<input type="radio" id="category_0" name="category" value="0" >Web</input>
                			<input type="radio" id="category_1" name="category" value="1" >IOS</input>
                			<input type="radio" id="category_2" name="category" value="2" >And</input>
                			<input type="radio" id="category_3" name="category" value="3" >Amazon</input>
                		</td>
                	</tr>
                                        
                    <!-- <tr>
                        <th>이슈 발생 시점</th>
                        <td>
                            <input type="input" class="search_text" id="writedate_date" name="writedate_date" style="width:65px" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
                            <select id="writedate_hour" name="writedate_hour">
                                <option value="">선택</option>
<?
    for ($i=0;$i<24;$i++)
    {
?>
                                <option value="<?= $i ?>"><?= $i ?></option>
<?
    }
?>
                            </select>시&nbsp;
                            -
                            <select id="writedate_min" name="writedate_min">
                                <option value="">선택</option>
<?
    for ($i=0;$i<60;$i++)
    {
?>
                                <option value="<?= $i ?>"><?= $i ?></option>
<?
    }
?>
                            </select>분&nbsp;
                        </td>
                    </tr>  -->
                    
                </tbody>
            </table>
            </form>
            
            <div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="저장" onclick="save_actionlog_blackbox()">
                <input type="button" class="btn_setting_02" value="취소" onclick="go_page('redmine_action.php')">            
            </div>
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>