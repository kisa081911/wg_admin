<?
    $top_menu = "user";
    $sub_menu = "user_login_log";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $search_ipaddress = trim($_GET["ipaddress"]);
    $search_useragent = trim($_GET["useragent"]);
    $search_adflag = $_GET["adflag"];
    $search_start_writedate = $_GET["start_writedate"];
    $search_end_writedate = $_GET["end_writedate"];
    $search_useridx = $_GET["useridx"];
    
    $search_order_by = ($_GET["orderby"] == "") ? "writedate desc" : $_GET["orderby"];
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];

    $listcount = "10";
    $pagename = "user_login_log.php";
    $pagefield = "orderby=$search_order_by&ipaddress=$search_ipaddress&useragent=$search_useragent&adflag=$search_adflag&start_writedate=$search_start_writedate&end_writedate=$search_end_writedate&useridx=$search_useridx";
    
    $tail =" WHERE 1=1 ";
    $order_by = "ORDER BY ".str_replace(":", " ", $search_order_by);
     
    if ($search_useridx != "")
    	$tail .= "AND useridx=$search_useridx";
     
    if ($search_adflag != "")
    {
    	$searchadflag = ($search_adflag == "0") ? "" : $search_adflag;
    	$tail .= " AND adflag = '$searchadflag' ";
    }
    
    if ($search_ipaddress != "")
    	$tail .= " AND ipaddress LIKE '%$search_ipaddress%' ";
    
    if ($search_useragent != "")
    	$tail .= " AND useragent LIKE '%$search_useragent%' ";
    
    if ($search_start_writedate != "")
    	$tail .= " AND writedate >= '$search_start_writedate 00:00:00' ";
    
    if ($search_end_writedate != "")
    	$tail .= " AND writedate <= '$search_end_writedate 23:59:59' ";
    
    $db_analysis = new CDatabase_Analysis();
    
    $totalcount = $db_analysis->getvalue("SELECT COUNT(*) FROM user_login_log $tail");
    
    $sql = "SELECT logidx, useridx, ipaddress, useragent, adflag, coin, writedate FROM user_login_log $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    $list = $db_analysis->gettotallist($sql);
    
    $db_analysis->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
    	$page = floor(($totalcount + $listcount - 1) / $listcount);    
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>  
<script>
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;
            
        search_form.submit();
    }

    $(function() {
        $("#start_writedate").datepicker({ });
    });
    
    $(function() {
        $("#end_writedate").datepicker({ });
    });
</script>
	<!-- CONTENTS WRAP -->
	<div class="contents_wrap">        
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 사용자 로그인로그 <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
		</div>
		<!-- //title_warp -->
		
		<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="user_login_log.php">
			<div class="detail_search_wrap">
				<table>
					<tr>
						<td>IP 주소</td>
						<td><input type="input" class="search_text" id="ipaddress" name="ipaddress" style="width:120px" value="<?= encode_html_attribute($search_ipaddress) ?>" onkeypress="search_press(event)" /></td>
						<td>브라우저</td>
						<td><input type="input" class="search_text" id="useragent" name="useragent" style="width:120px" value="<?= encode_html_attribute($search_useragent) ?>" onkeypress="search_press(event)" /></td>
						<td>유입경로</td>
						<td>
							<select name="adflag" id="adflag">
								<option value="" <?= ($search_adflag == "") ? "selected": "" ?>>선택</option>
								<option value="0" <?= ($search_adflag == "0") ? "selected": "" ?>>바이럴</option>
								<option value="maudau" <?= ($search_adflag == "maudau") ? "selected": "" ?>>maudau</option>
								<option value="maudau1" <?= ($search_adflag == "maudau1") ? "selected": "" ?>>maudau1</option>
								<option value="maudau2" <?= ($search_adflag == "maudau2") ? "selected": "" ?>>maudau2</option>
								<option value="userad" <?= ($search_adflag == "userad") ? "selected": "" ?>>userad</option>
							</select>
						</td>
						<td>접속일시</td>
						<td>
							<input type="input" class="search_text" id="start_writedate" name="start_writedate" style="width:65px" value="<?= $search_start_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
							<input type="input" class="search_text" id="end_writedate" name="end_writedate" style="width:65px" value="<?= $search_end_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
						</td>
						<td><input type="button" class="btn_search" value="검색" onclick="search()" /></td>
					</tr>
				</table>
			</div>
            
			<div style="float:right;padding-bottom:5px;">
				<select name="orderby" id="orderby" onchange="search()">
					<option value="" <?= ($search_order_by == "") ? "selected" : "" ?>>정렬선택</option>
					<option value="ipaddress:asc" <?= ($search_order_by == "ipaddress:asc") ? "selected" : "" ?>>ip주소 오름차순</option>
					<option value="ipaddress:desc" <?= ($search_order_by == "ipaddress:desc") ? "selected" : "" ?>>ip주소 내림차순</option>
					<option value="useragent:asc" <?= ($search_order_by == "useragent:asc") ? "selected" : "" ?>>브라우저 오름차순</option>
					<option value="useragent:desc" <?= ($search_order_by == "useragent:desc") ? "selected" : "" ?>>브라우저 내림차순</option>
					<option value="adflag:asc" <?= ($search_order_by == "adflag:asc") ? "selected" : "" ?>>ad flag 오름차순</option>
					<option value="adflag:desc" <?= ($search_order_by == "adflag:desc") ? "selected" : "" ?>>ad flag 내림차순</option>
					<option value="writedate:asc" <?= ($search_order_by == "writedate:asc") ? "selected" : "" ?>>접속일시 오름차순</option>
					<option value="writedate:desc" <?= ($search_order_by == "writedate:desc") ? "selected" : "" ?>>접속일시 내림차순</option>
				</select>
			</div>
		</form>
            
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="80">
				<col width="120">
				<col width="">
				<col width="80">				
				<col width="80">
				<col width="120">               
			</colgroup>
			<thead>
				<tr>
					<th>번호</th>
					<th>IP 주소</th>
					<th>브라우저</th>
					<th>유입경로</th>					
					<th>보유코인</th>
					<th>접속일시</th>
				</tr>
			</thead>
			<tbody>
<?
    for($i=0; $i<sizeof($list); $i++)
    {
        $logidx = $list[$i]["logidx"];
        $useridx = $list[$i]["useridx"];
        $ipaddress = $list[$i]["ipaddress"];
        $useragent = $list[$i]["useragent"];
        $adflag = $list[$i]["adflag"];        
        $coin = $list[$i]["coin"];
        $writedate = $list[$i]["writedate"];
?>
				<tr class="" onmouseover="className='tr_over'" onmouseout="className=''" onclick="view_user_dtl(<?= $useridx ?>,'loginlog')">
					<td class="tdc"><?=  $totalcount - (($page-1) * $listcount) - $i ?></td>
					<td class="tdc point"><?= $ipaddress ?></td>
					<td class="tdl"><?= $useragent ?></td>
					<td class="tdc point"><?= $adflag ?></td>                        
					<td class="tdc"><?= make_price_format($coin) ?></td>
					<td class="tdl"><?= $writedate ?></td>
				</tr>
<?
    } 
?>
			</tbody>
		</table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
	</div>
    <!--  //CONTENTS WRAP -->
        
	<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>