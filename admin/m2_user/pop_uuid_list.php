<? 
    include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
    
    $uuid = $_POST["uuid"];
	$db_mobile = new CDatabase_Mobile();
	
	$totalcount = $db_mobile->getvalue("SELECT COUNT(*) FROM `tbl_mobile` t1 JOIN `tbl_user_mobile_connection_log` t2 ON t1.device_id = t2.device_id WHERE uuid = '$uuid'");

    $db_mobile->end();
?>
<script type="text/javascript">
	var g_page = 1;

	$(document).ready(function()
	{
		refresh();
	});

	function refresh()
	{
		var param = {};
		param.page = g_page;
		param.uuid = "<?= $uuid ?>";

		WG_ajax_list("user/get_uuid_list", param, fnContentLoad_callback, true);
	}

	function fnContentLoad_callback(result, reason, totalcount, list)
	{
		if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            update_pagenation(g_page, <?= $totalcount?>, 10, 10);
            
            var tbody = document.getElementById("list_contents");
            
            while (tbody.childNodes.length > 0)
                tbody.removeChild(tbody.childNodes[0]);

            for (var i=0; i<list.length; i++)
            {
                var tr = document.createElement("tr");
                var td = document.createElement("td");
                var userid = list[i].userid;
                
                if(userid < 10)
                	var photourl = "https://take5-dev.doubleugames.com/mobile/images/avatar_profile/guest_profile_"+userid+".png";
                else
                	var photourl = "http://graph.facebook.com/"+userid+"/picture?type=square&access_token=<?=$client_accesstoken?>";
                               
                var nickname = list[i].nickname;
                var obj = "<span style='float:left;'><img src='"+photourl+"' height='25' width='25' align='absmiddle' hspace='3'></img></span>"+nickname;
                var useridx = list[i].useridx;
                var button_obj = "<input type='button' class='btn_03' value='user 상세보기' onclick='go_detail("+useridx+")'>";
                
                tr.addEventListener("mouseover",myhandler_over, false);
                tr.addEventListener("mouseout",myhandler_out, false);
                tr.appendChild(td);
                
                td.className = "point_title";
                td.innerHTML = obj;
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc point";
                td.innerText = make_price_format(list[i].coin);
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc point";
                td.innerText =list[i].level;
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc point";
                td.innerText =list[i].logindate;
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc point";
                td.innerText =list[i].createdate;
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc point";
                td.innerHTML = button_obj;
               
                tbody.appendChild(tr);
            }
        }
	}
	var myhandler_over = function(){
	  this.className = "tr_over";
	}
	var myhandler_out = function(){
	  this.className = "";
	}
	
	function go_detail(useridx)
	{
	  window.location.href = "/m2_user/user_view.php?useridx="+useridx+"&category=";
	}
</script>
<div id="layer_wrap">   
    <div class="layer_header" >
        <div class="layer_title">동일 UUID 사용자 상세보기</div>
         <img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>        
    <div class="layer_contents_wrap" style="width: 1170px;height:650px;padding-bottom:0px;">
         <!--  레이어 내용  -->
         
         <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
             <table class="tbl_list_basic1" style="width:100%;">
                <colgroup>
                    <col width="">
                    <col width="">
                    <col width="">
                    <col width="">
                    <col width="">
                    <col width="">
                </colgroup>
                <thead>
                <tr>
                	<th>이름</th>                    
                    <th>코인</th>
                    <th>레벨</th>
                    <th>최근 로그인</th>
                    <th>가입일</th>  
                    <th>상세보기</th>                  
                </tr>
                </thead>
               <tbody id="list_contents">
	                </tbody>
                

            </table>
            <div class="pagenation mt20" id=pagenation></div>
            </form>  
         </div>
    </div>
</div>
</body>
</html>

