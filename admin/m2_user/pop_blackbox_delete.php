<? 
    include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
    
    check_login_layer();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>코인 수동 발급</title>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script>
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
	var tmp_hosturl = "<?= HOST_URL ?>";
    function layer_close()
    {
        window.parent.close_layer_popup("<?= $_GET["layer_no"] ?>");
    }
    
    function delete_blackbox()
    {
        var input_form = document.input_form;

        if (input_form.redmine.value == "")
        {			         
            alert("레드마인 이슈 번호를 입력해주세요.");
            input_form.redmine.focus();
            return;
        }
		        
        var param = {};
        param.redmine = input_form.redmine.value;
        
        WG_ajax_execute("user/delete_blackbox", param, delete_blackbox_callback, false);
    }
    
    function delete_blackbox_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            alert("삭제되었습니다.");
            layer_close();
            window.parent.location.reload(false);
        }
    } 
</script>
</head>
<body class="layer_body" onload="window.focus()" onkeyup="if (getEventKeycode(event) == 27) { layer_close(); }">
<div id="layer_wrap">   
    <div class="layer_header" >
        <div class="layer_title">블랙박스 삭제</div>
        <img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="layer_close()" />
    </div>        
    <div class="layer_contents_wrap" style="width:550px">
         <!--  레이어 내용  -->
         
         <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
            <table class="tbl_view_basic" style="width:540px">
                <colgroup>
                    <col width="120">
                    <col width="">
                </colgroup>
                    <tbody>
                         <tr>
                             <th>이슈 번호</th>
                             <td><input type="text" class="view_tbl_text" style="width:250px" name="redmine" id="redmine" value="" onkeypress="return checknum()"/></td>
                         </tr>
                     </tbody>
             </table>  
             </form>  
         </div>
         
         <!-- 확인 버튼 -->
         <div class="layer_button_wrap" style="width:500px;text-align:right;">
            <input type="button" class="btn_02" value="삭제" onclick="delete_blackbox()" />
            <input type="button" class="btn_02" value="닫기" onclick="layer_close()" />
         </div>
         <!-- 확인 버튼 -->
         <!--  //레이어 내용  -->
    </div>
</div>
</body>
</html>

