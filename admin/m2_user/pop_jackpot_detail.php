<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	$useridx = $_POST["useridx"];
	
	$db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	
	$sql = "SELECT COUNT(*) FROM tbl_jackpot_log WHERE useridx = $useridx";
	$list_totalcnt = $db_main->getvalue($sql);
	
	$sql = "SELECT slottype, slotname FROM tbl_slot_list";
    $slottype_list = $db_main2->gettotallist($sql);
	
    $db_main->end();
	$db_main2->end();
?>
<script type="text/javascript">
	var g_page = 1;
	var slotlist = [];
	<?
		for($i=0; $i<sizeof($slottype_list); $i++)
		{
			$slottype = $slottype_list[$i]["slottype"];
			$slotname = $slottype_list[$i]["slotname"];
	?>
			slotlist[<?= $slottype?>] = "<?= $slotname?>";
	<?
		}
	?>
		

	$(document).ready(function()
	{
		refresh();
	});

	function refresh()
	{
		var param = {};
		param.page = g_page;
		param.useridx = "<?= $useridx ?>";

		WG_ajax_list("user/get_jackpot_list", param, fnContentLoad_callback, true);
	}

	function fnContentLoad_callback(result, reason, totalcount, list)
	{
		if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            update_pagenation(g_page, <?= $list_totalcnt?>, 10, 10);
            
            var tbody = document.getElementById("list_contents");
            
            while (tbody.childNodes.length > 0)
                tbody.removeChild(tbody.childNodes[0]);

            for (var i=0; i<list.length; i++)
            {
                var tr = document.createElement("tr");
                var td = document.createElement("td");

                var os_name = "";
                var slotname = "";
				var jackpot_name = "";
				var jackpot_onwer_name = "";
				var amount = 0;				
				var total_jackpot_amount = 0;

				if(list[i].devicetype == 0)
					os_name = "Web";
				else if(list[i].devicetype == 1)
					os_name = "Ios";
				else if(list[i].devicetype == 2)
					os_name = "Android";
				else if(list[i].devicetype == 3)
					os_name = "Amazon";
				
		        if(list[i].fiestaidx > 0)
		        	jackpot_name = "Fiesta JACKPOT";
		        else if(list[i].jackpot_member_count == 1)
		        	jackpot_name = "DECENT JACKPOT";
		        else if(list[i].jackpot_member_count == 2)
		        	jackpot_name = "FABULOUS JACKPOT";
		        else if(list[i].jackpot_member_count == 3)
		        	jackpot_name = "AWESOME JACKPOT";
		        else if(list[i].jackpot_member_count == 4)
		        	jackpot_name = "AMAZING JACKPOT";
		        else if(list[i].jackpot_member_count == 5)
		        	jackpot_name = "TAKE5 JACKPOT";
				
		        jackpot_onwer_name = "N";
				
				if(list[i].jackpot_owner == 1)
					jackpot_onwer_name = "Y";	        
		        
		        amount = list[i].amount;		            	
		        total_jackpot_amount = list[i].total_jackpot_amount;

		        tr.appendChild(td);
                td.className = "tdc point";
                td.innerText = os_name;		        

		        td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc point";
                td.innerText = jackpot_name;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = slotlist[list[i].slottype];

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc point";
                td.innerText = jackpot_onwer_name;
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = make_price_format(amount);

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = make_price_format(total_jackpot_amount);

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].writedate;
                
                tbody.appendChild(tr);
            }
        }
	}
</script>
<div id="layer_wrap">
	<div class="layer_header" >
        <div class="layer_title">BigWin 로그</div>
    	<img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>
    
    <div class="layer_contents_wrap" style="width:950px;height:500px;">
    	 <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
	            <table class="tbl_list_basic1" width="1200px;">
	                <colgroup>
	                	<col width="">
	                    <col width="">
	                    <col width="">
	                    <col width="">	                    
	                    <col width="">
	                    <col width="">	                    
	                    <col width="">	                    	                    
	                </colgroup>
	                <thead>
	                    <tr>
	                    	<th>Os</th>
							<th>Jackpot Name</th>
							<th>Slot</th>
							<th>Onwer</th>
							<th>Jackpot 금액</th>				
							<th>총 Jackpot 금액</th>
							<th>획득일</th>
	                    </tr>
	                </thead>
	                <tbody id="list_contents">
	                </tbody>
	            </table>  
            	<div class="pagenation mt20" id=pagenation></div>
            </form>  
         </div>
    </div>
</div>