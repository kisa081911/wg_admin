<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	$useridx = $_POST["useridx"];
	
	$db_livestats = new CDatabase_Livestats();
	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT COUNT(*) FROM tbl_user_inbox_collect_".($useridx % 20)." WHERE useridx = $useridx AND category != 301;";
	$list_totalcnt = $db_livestats->getvalue($sql);
	
	$sql = "SELECT inbox_type, name FROM tbl_total_freecoin_type WHERE type = 100";
	$inbox_list = $db_analysis->gettotallist($sql);
	
	$db_livestats->end();
	$db_analysis->end();
?>
<script type="text/javascript">
	var g_page = 1;
	var inboxlist = [];
	<?
		for($i=0; $i<sizeof($inbox_list); $i++)
		{
			$inbox_type = $inbox_list[$i]["inbox_type"];
			$inboxname = $inbox_list[$i]["name"];
	?>
			inboxlist[<?= $inbox_type?>] = "<?= $inboxname?>";
	<?
		}
	?>

	$(document).ready(function()
	{
		refresh();
	});

	function refresh()
	{
		var param = {};
		param.page = g_page;
		param.useridx = "<?= $useridx ?>";

		WG_ajax_list("user/get_inbox_list", param, fnContentLoad_callback, true);
	}

	function fnContentLoad_callback(result, reason, totalcount, list)
	{
		if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            update_pagenation(g_page, <?= $list_totalcnt?>, 10, 10);
            
            var tbody = document.getElementById("list_contents");
            
            while (tbody.childNodes.length > 0)
                tbody.removeChild(tbody.childNodes[0]);

            for (var i=0; i<list.length; i++)
            {
                var tr = document.createElement("tr");
                var td = document.createElement("td");

				var typename = "";
				var amount = 0;
				var categoryname = "";

				if(list[i].os_type == 0)
					typename = "Web";
				else if(list[i].os_type == 1)
					typename = "IOS";
				else if(list[i].os_type == 2)
					typename = "Android";
				else if(list[i].os_type == 3)
					typename = "Amazon";		        
		        
		        amount = list[i].amount;	
		        categoryname = inboxlist[list[i].inbox_type];	        	
                
                tr.appendChild(td);
                td.className = "tdc point";
                td.innerText = typename;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = categoryname;
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = make_price_format(amount);

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].writedate;
                
                tbody.appendChild(tr);
            }
        }
	}
</script>
<div id="layer_wrap">
	<div class="layer_header" >
        <div class="layer_title">Inbox 획득 로그</div>
    	<img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>
    
    <div class="layer_contents_wrap" style="width:700px;height:435px;">
    	 <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
	            <table class="tbl_list_basic1" width="1200px;">
	                <colgroup>
	                    <col width="">
	                    <col width="">
	                    <col width="">	                    
	                    <col width="">	                    
	                </colgroup>
	                <thead>
	                    <tr>
							<th>접속</th>
							<th>타입</th>
                			<th>Coin</th>
							<th>획득일</th>
	                    </tr>
	                </thead>
	                <tbody id="list_contents">
	                </tbody>
	            </table>  
            	<div class="pagenation mt20" id=pagenation></div>
            </form>  
         </div>
    </div>
</div>