<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	$useridx = $_POST["useridx"];
	
	$db_main = new CDatabase_Main();
	
	$sql = "SELECT COUNT(*) FROM tbl_unknown_coin WHERE useridx=$useridx";
	$list_totalcnt = $db_main->getvalue($sql);
	
	$db_main->end();
?>
<script type="text/javascript">
	var g_page = 1;

	$(document).ready(function()
	{
		refresh();
	});

	function refresh()
	{
		var param = {};
		param.page = g_page;
		param.useridx = "<?= $useridx ?>";

		WG_ajax_list("user/get_user_unkown_coin_list", param, fnContentLoad_callback, true);
	}

	function fnContentLoad_callback(result, reason, totalcount, list)
	{
		if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            update_pagenation(g_page, <?= $list_totalcnt?>, 10, 10);
            
            var tbody = document.getElementById("list_contents");
            
            while (tbody.childNodes.length > 0)
                tbody.removeChild(tbody.childNodes[0]);

            for (var i=0; i<list.length; i++)
            {
                var tr = document.createElement("tr");
                var td = document.createElement("td");

				var category_name = "";

				if(list[i].writedate <= "2016-02-24 08:22:00")
				{	
					if(list[i].category == "0")
						category_name = "Slot Exit (동기화)";
					else if(list[i].category == "1")
						category_name = "Login";
					else if(list[i].category == "2")
						category_name = "Slot Exit";
					else if(list[i].category == "3")
						category_name = "Login (한도초과)";
					else if(list[i].category == "4")
						category_name = "Slot Exit (한도초과)";
					else if(list[i].category == "5")
						category_name = "Slot Exit (20분내 결제 사용자, 로그)";
					else if(list[i].category == "6")
						category_name = "Slot Exit (Big Wheel)";
					else if(list[i].category == "7")
						category_name = "Web, Mobile 동시 로그인";
					else if(list[i].category == "12")
						category_name = "IOS";
					else if(list[i].category == "14")
						category_name = "IOS (로그)";
					else if(list[i].category == "22")
						category_name = "Login시 코인정보 강제 업데이트(서버와 정보 다름)";
					else if(list[i].category == "24")
						category_name = "Login시 코인정보 강제 업데이트(범위 초과, 로그)";
					else if(list[i].category == "25")
						category_name = "Login시 코인정보 강제 업데이트(20분내 결제 사용자, 로그)";
					else if(list[i].category == "26")
						category_name = "Mypot, Megaspin 게이지 조정(로그)";
					else if(list[i].category == "112")
						category_name = "Android";
					else if(list[i].category == "114")
						category_name = "Android (로그)";
					else if(list[i].category == "122")
						category_name = "Amazon";
					else if(list[i].category == "124")
						category_name = "Amazon (로그)";
				}
				else
				{
					if (list[i].category == "1")
						category_name = "게임 로그인 직후(코인 변화량 : -1,000,000,000 ~ -2,000, 최근 20분내 결제 X)";
					else if (list[i].category == "2")
						category_name = "게임 로그인 직후(코인 변화량 : -1,000,000,000 ~ -2,000, 최근 20분내 결제 O, 로그)";
					else if (list[i].category == "3")
						category_name = "게임 로그인 직후(코인 변화량 : -2,000, 200,000,000 사이)";
					else if (list[i].category == "4")
						category_name = "게임 로그인 직후(코인 변화량 : 200,000,000 크거나 -1,000,000,000 작을 때, 로그)";
					else if (list[i].category == "11")
						category_name = "슬롯에서 exit(코인 변화량 : -1,000,000,000 ~ -2,000, 최근 20분내 결제 X)";
					else if (list[i].category == "12")
						category_name = "슬롯에서 exit(코인 변화량 : -1,000,000,000 ~ -2,000, 최근 20분내 결제 O, 로그)";
					else if (list[i].category == "13")
						category_name = "슬롯에서 exit(코인 변화량 : -2,000, 200,000,000 사이)";
					else if (list[i].category == "14")
						category_name = "슬롯에서 exit(코인 변화량 : 200,000,000 크거나 -1,000,000,000 작을 때, 로그)";
					else if(list[i].category == "22")
						category_name = "Login시 코인정보 강제 업데이트(서버와 정보 다름)";
				}				

                tr.appendChild(td);
                td.className = "tdl point";
                td.innerText = category_name;				
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdr";
                td.innerText = make_price_format(list[i].coin_change);

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].writedate;
                
                tbody.appendChild(tr);
            }
        }
	}
</script>
<div id="layer_wrap">
	<div class="layer_header" >
        <div class="layer_title">이벤트 참여 목록</div>
    	<img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>
    
    <div class="layer_contents_wrap" style="width:800px;height:500px;">
    	 <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
	            <table class="tbl_list_basic1" width="780px;">
	                <colgroup>
	                    <col width="*">
						<col width="150">
						<col width="150">
	                </colgroup>
	                <thead>
	                    <tr>
	                        <th>종류</th>
                			<th>금액</th>
                			<th>일시</th>
	                    </tr>
	                </thead>
	                <tbody id="list_contents">
	                </tbody>
	            </table>  
            	<div class="pagenation mt20" id=pagenation></div>
            </form>  
         </div>
    </div>
</div>