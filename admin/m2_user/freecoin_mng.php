<?
	$top_menu = "user";
	$sub_menu = "user_freecoin_mng";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    
    $search_nickname = trim($_GET["nickname"]);
    $search_userid = trim($_GET["userid"]); 
    
    check_xss($search_nickname.$search_userid);
    
	$listcount = "10";
	$pagename = "freecoin_mng.php";
	$pagefield = "nickname=$search_nickname&userid=$search_userid";
	
	$tail = "WHERE 1=1 ";
    
	if ($search_nickname != "")
		$tail .= " AND useridx IN (SELECT useridx FROM tbl_user WHERE useridx=A.useridx AND nickname LIKE '%$search_nickname%')";
	
	if ($search_userid != "")
		$tail .= " AND useridx IN (SELECT useridx FROM tbl_user WHERE useridx=A.useridx AND userid LIKE '%$search_userid%')";
	
	$db_main = new CDatabase_Main();
	
	$totalcount = $db_main->getvalue("SELECT COUNT(*) FROM tbl_freecoin_admin_log AS A $tail");
	
	$sql = "SELECT logidx,useridx,freecoin,reason,admin_id,writedate, ".
		"(SELECT userid FROM tbl_user WHERE useridx=A.useridx) AS userid, (SELECT nickname FROM tbl_user WHERE useridx=A.useridx) AS nickname ".	
		"FROM tbl_freecoin_admin_log A $tail ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
	
	$freecoinlist = $db_main->gettotallist($sql);
	
	$db_main->end();
	
	if ($totalcount < ($page-1) * $listcount && page != 1)
		$page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<link type="text/css" href="/js/themes/base/jquery.ui.datepicker.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery-ui.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script>
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}
	
	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
    	
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 관리자발급 자원관리 <span class="totalcount">(<?= number_format($totalcount) ?>)</span></div>
	</div>
	<!-- //title_warp -->
	    	
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="freecoin_mng.php">
		<div class="detail_search_wrap">
			<span class="search_lbl">이름</span>
			<input type="text" class="search_text" id="nickname" name="nickname" style="width:120px" value="<?= encode_html_attribute($search_nickname) ?>" onkeypress="search_press(event)" />
					
			<span class="search_lbl ml20">Facebook ID</span>
			<input type="text" class="search_text" id="userid" name="userid" style="width:120px" value="<?= encode_html_attribute($search_userid) ?>" onkeypress="search_press(event)" />

			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>
            
	<table class="tbl_list_basic1"> 
		<colgroup>
			<col width="50">
			<col width="250">		
			<col width="90">			
			<col width="">
			<col width=110">		
			<col width="120">			
		</colgroup>
		<thead>
    		<tr>
    			<th>번호</th>
    			<th class="tdl">사용자</th>
    			<th class="tdr">Coin</th>
    			<th>발급사유</th>
    			<th>발급자</th>
    			<th>발급일시</th>
    		</tr>
		</thead>
		<tbody>
<?
	for ($i=0;$i<sizeof($freecoinlist);$i++)
	{
		$logidx = $freecoinlist[$i]["logidx"];
		$useridx = $freecoinlist[$i]["useridx"];
		$userid = $freecoinlist[$i]["userid"];
		$nickname = $freecoinlist[$i]["nickname"];
		$freecoin = $freecoinlist[$i]["freecoin"];;
		$reason = $freecoinlist[$i]["reason"];
		$admin_id = $freecoinlist[$i]["admin_id"];		
		$writedate = $freecoinlist[$i]["writedate"];
		
		$photourl = get_fb_pictureURL($userid,$client_accesstoken);
		
		if ($vip_level > 0)
			$vip_level_image = "&nbsp;<img src=\"/images/membership/vp_level_".$vip_level.".png\" height=\"30\" width=\"30\" align=\"absmiddle\" />";
		else
			$vip_level_image = "";
?>
			<tr  class="">
				<td class="tdc"><?=  $totalcount - (($page-1) * $listcount) - $i ?></td>
				<td class="point_title"><a href="javascript:view_user_dtl(<?= $useridx ?>,'')"><img src="<?= $photourl ?>" width="25" height="25" align="absmiddle"> <?= $nickname ?></a>&nbsp;&nbsp;<img src="/images/icon/facebook.png" height="20" width="20" align="absmiddle" style="cursor:pointer" onclick="event.cancelBubble=true;window.open('http://www.facebook.com/<?= $userid ?>')" /><?= $vip_level_image?></td>
				<td class="tdr point_title">$<?= number_format($freecoin)?></td>
				<td class="tdc point"><?= $reason ?></td>
				<td class="tdc point"><?= $admin_id ?></td>
				<td class="tdc"><?= $writedate ?></td>
			</tr>
<?
	} 
?>
		</tbody>
	</table>
<?
	include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
</div>
<!--  //CONTENTS WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>