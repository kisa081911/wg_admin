<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	$os_type = $_POST["os_type"];
	$useridx = $_POST["useridx"];
	
	$db_main2 = new CDatabase_Main2();
	$db_other = new CDatabase_Other();
	
	if($os_type == "web")
	{			
		$sql = "SELECT SUM(cnt) ".
				"FROM ".
				"( ".
				"	SELECT COUNT(logidx) AS cnt FROM `tbl_user_gamelog` WHERE useridx = $useridx AND MODE NOT IN (4, 5, 9, 26, 29) AND slottype != 0 ".
				"	AND DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 7 DAY), '%Y-%m-%d') <= writedate AND writedate <= NOW()".
				") t1;";
	}
	else if($os_type == "ios")
	{
		$sql = "SELECT SUM(cnt) ".
				"FROM ".
				"( ".
				"	SELECT COUNT(logidx) AS cnt FROM `tbl_user_gamelog_ios` WHERE useridx = $useridx AND MODE NOT IN (4, 5, 9, 26, 29) AND slottype != 0 ".
				"	AND DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 7 DAY), '%Y-%m-%d') <= writedate AND writedate <= NOW()".
				") t1;";		
	}
	else if($os_type == "android")
	{
		$sql = "SELECT SUM(cnt) ".
				"FROM ".
				"( ".
				"	SELECT COUNT(logidx) AS cnt FROM `tbl_user_gamelog_android` WHERE useridx = $useridx AND MODE NOT IN (4, 5, 9, 26, 29) AND slottype != 0 ".
				"	AND DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 7 DAY), '%Y-%m-%d') <= writedate AND writedate <= NOW()".
				") t1;";
	}
	else if($os_type == "amazon")
	{
		$sql = "SELECT SUM(cnt) ".
				"FROM ".
				"( ".
				"	SELECT COUNT(logidx) AS cnt FROM `tbl_user_gamelog_amazon` WHERE useridx = $useridx AND MODE NOT IN (4, 5, 9, 26, 29) AND slottype != 0 ".
				"	AND DATE_FORMAT(DATE_SUB(NOW(),INTERVAL 7 DAY), '%Y-%m-%d') <= writedate AND writedate <= NOW()".
				") t1;";
	}	
	
	$list_totalcnt = $db_other->getvalue($sql);
	
	$sql = "SELECT slottype, slotname FROM tbl_slot_list";
    $slottype_list = $db_main2->gettotallist($sql);
	
	$db_main2->end();
	$db_other->end();
?>
<script type="text/javascript">
	var g_page = 1;
	var slotlist = [];
	<?
		for($i=0; $i<sizeof($slottype_list); $i++)
		{
			$slottype = $slottype_list[$i]["slottype"];
			$slotname = $slottype_list[$i]["slotname"];
	?>
			slotlist[<?= $slottype?>] = "<?= $slotname?>";
	<?
		}
	?>
		

	$(document).ready(function()
	{
		refresh();
	});

	function refresh()
	{
		var param = {};
		param.page = g_page;
		param.os_type = "<?= $os_type ?>";
		param.useridx = "<?= $useridx ?>";

		WG_ajax_list("user/get_slotdetail_list", param, fnContentLoad_callback, true);
	}

	function fnContentLoad_callback(result, reason, totalcount, list)
	{
		if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            update_pagenation(g_page, <?= $list_totalcnt?>, 10, 10);
            
            var tbody = document.getElementById("list_contents");
            
            while (tbody.childNodes.length > 0)
                tbody.removeChild(tbody.childNodes[0]);

            for (var i=0; i<list.length; i++)
            {
                var tr = document.createElement("tr");
                var td = document.createElement("td");

                var slotname = "";	
                var winrate = "";	        

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = slotlist[list[i].slottype];
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = make_price_format(list[i].moneyin);

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = make_price_format(list[i].moneyout);

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].slot_winrate+"%";

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].writedate;
                
                tbody.appendChild(tr);
            }
        }
	}
</script>
<div id="layer_wrap">
	<div class="layer_header" >
        <div class="layer_title">슬롯별 승률상세보기</div>
    	<img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>
    
    <div class="layer_contents_wrap" style="width:700px;height:435px;">
    	 <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
	            <table class="tbl_list_basic1" width="1200px;">
	                <colgroup>
	                    <col width="">
	                    <col width="">
	                    <col width="">	                    
	                    <col width="">	                    
	                </colgroup>
	                <thead>
	                    <tr>
							<th>Slot</th>
							<th>Money_In</th>
							<th>Money_Out</th>
							<th>승률</th>                
							<th>등록일</th>
	                    </tr>
	                </thead>
	                <tbody id="list_contents">
	                </tbody>
	            </table>  
            	<div class="pagenation mt20" id=pagenation></div>
            </form>  
         </div>
    </div>
</div>