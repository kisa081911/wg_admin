<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	$useridx = $_POST["useridx"];
	
	$db_main = new CDatabase_Main();
	
	$sql = "SELECT COUNT(*) FROM tbl_freecoin_admin_log WHERE useridx=$useridx";
	$list_totalcnt = $db_main->getvalue($sql);
	
	$db_main->end();
?>
<script type="text/javascript">
	var g_page = 1;

	$(document).ready(function()
	{
		refresh();
	});

	function refresh()
	{
		var param = {};
		param.page = g_page;
		param.useridx = "<?= $useridx ?>";

		WG_ajax_list("user/get_free_resource_list", param, fnContentLoad_callback, true);
	}

	function fnContentLoad_callback(result, reason, totalcount, list)
	{
		if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            update_pagenation(g_page, <?= $list_totalcnt?>, 10, 10);
            
            var tbody = document.getElementById("list_contents");
            
            while (tbody.childNodes.length > 0)
                tbody.removeChild(tbody.childNodes[0]);

            for (var i=0; i<list.length; i++)
            {
                var tr = document.createElement("tr");
                var td = document.createElement("td");

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].reason;
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].message;               

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = make_price_format(list[i].coin);

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].writedate;
                
                tbody.appendChild(tr);
            }
        }
	}
</script>
<div id="layer_wrap">
	<div class="layer_header" >
        <div class="layer_title">관리자 무료 발급 자원 로그</div>
    	<img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>
    
    <div class="layer_contents_wrap" style="width:950px;height:420px;">
    	 <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
	            <table class="tbl_list_basic1" width="930px;">
	                <colgroup>
	                	<col width="150">
	                    <col width="">
	                    <col width="80">	                    
	                    <col width="140">
	                </colgroup>
	                <thead>
	                    <tr>
	                    	<th>발급 사유</th>
	                        <th>발급 메세지</th>							
							<th>Coin</th>
							<th>발급일</th>
	                    </tr>
	                </thead>
	                <tbody id="list_contents">
	                </tbody>
	            </table>  
            	<div class="pagenation mt20" id=pagenation></div>
            </form>  
         </div>
    </div>
</div>