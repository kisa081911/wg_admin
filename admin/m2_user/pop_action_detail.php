<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");

	$os_type = $_POST["os_type"];
	$useridx = $_POST["useridx"];
	$userid = $_POST["userid"];
?>
<script type="text/javascript">
    $(function() {
    	$("#search_date").datepicker({ });
    });

	var g_page = 1;
	var g_totalcount = 0;

	$(document).ready(function()
	{
		refresh();
	});

	function searchclear()
	{
		document.getElementById("search_date").value = "";
		$("#search_hour").val("").change();
		$("#search_minute").val("").change();
		g_page = 1;
		refresh();
	}
	
	function refresh()
	{
		var param = {};
		var search_date = document.getElementById("search_date").value;
		var search_hour = document.getElementById("search_hour").value;
		var search_minute = document.getElementById("search_minute").value;
		var search_version = document.getElementById("search_version").value;

		param.page = g_page;
		param.os_type = "<?= $os_type ?>";
		param.useridx = "<?= $useridx ?>";
		param.userid = "<?= $userid ?>";

		if(search_date != "")
		{
    		param.search_date = search_date;
    		param.search_hour = search_hour;
    		param.search_minute = search_minute;    		
    		
    		if(search_hour=="" && search_minute != "")
        	{
    			alert("날짜을 선택 하세요");
    			return;
        	}
		}

		param.search_version = search_version;

		WG_ajax_query("user/get_actionlog_totalcount", param, fnContentLoad_callback1, false);
		
	}
	
	function fnContentLoad_callback1(result, reason, map)
    {
    	if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
        	var param = {};
    		var search_date = document.getElementById("search_date").value;
    		var search_hour = document.getElementById("search_hour").value;
    		var search_minute = document.getElementById("search_minute").value;
    		var search_version = document.getElementById("search_version").value;

    		param.page = g_page;
    		param.os_type = "<?= $os_type ?>";
    		param.useridx = "<?= $useridx ?>";
    		param.userid = "<?= $userid ?>";

    		if(search_date != "")
    		{
        		param.search_date = search_date;
        		param.search_hour = search_hour;
        		param.search_minute = search_minute;
    		}

    		param.search_version = search_version;
    		
            var totalcount = map.totalcount;
            
            if(totalcount!=g_totalcount)
            {
                g_totalcount = totalcount;
                g_page = 1;
                param.page = g_page;
            }
            
            update_pagenation(g_page, totalcount, 10, 10);

            WG_ajax_list("user/get_actionlog_list", param, fnContentLoad_callback, true);
        }
    }

		

	function fnContentLoad_callback(result, reason, totalcount, list)
	{
		if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            var tbody = document.getElementById("list_contents");
            
            while (tbody.childNodes.length > 0)
                tbody.removeChild(tbody.childNodes[0]);

            for (var i=0; i<list.length; i++)
            {
                var tr = document.createElement("tr");
                var td = document.createElement("td");

                tr.appendChild(td);
                td.className = "tdc point";
                td.innerText = list[i].ipaddress;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].category;
                
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].category_info;
                    
                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].action;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].action_info;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].staytime;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].isplaynow;

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = make_price_format(list[i].coin);

              <?
               	if($os_type == 0)
                {
              ?>
              	  if(list[i].is_v2 == 1)
                  	  is_v2_str = "2.0";
              	  else
              		  is_v2_str = "1.0";
            		
	              td = document.createElement("td");
	              tr.appendChild(td);
	              td.className = "tdc";
	              td.innerText = is_v2_str;
              <?
                }
              ?>

                td = document.createElement("td");
                tr.appendChild(td);
                td.className = "tdc";
                td.innerText = list[i].writedate;
                
                tbody.appendChild(tr);
            }
        }
	}
</script>
<div id="layer_wrap">
	<div class="layer_header" >
        <div class="layer_title">행동 로그</div>
    	<img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>
    
    
    <div class="layer_contents_wrap" style="width:1250px;height:600px;">
    	<div class="search_box" style="margin-bottom: 13px;">
    		<span class="search_lbl ml20">기준일&nbsp;&nbsp;&nbsp;</span>
    		<input type="text" class="search_text" id="search_date" name="search_date" value="<?= $search_date?>" maxlength="10" style="width:65px"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
			<select id="search_hour" name="search_hour">
							<option value="">선택</option>
<?
    for ($i=0;$i<24;$i++)
    {
?>
							<option value="<?= $i ?>"><?= $i ?></option>
<?
    }
?>
						</select>시&nbsp;
						<select id="search_minute" name="search_minute">
							<option value="">선택</option>
<?
    for ($i=0;$i<60;$i++)
    {
?>
							<option value="<?= $i ?>"><?= $i ?></option>
<?
    }
?>
						</select>분
			<span class="search_lbl ml20">버전&nbsp;&nbsp;&nbsp;</span>
			<input type="text" class="search_version" id="search_version" name="search_version" value="<?= $search_version?>" maxlength="10" style="width:65px"" />    		 
    		<input type="button" class="btn_search" value="검색" onclick="refresh()" /> <input type="button" class="btn_search" value="초기화" onclick="searchclear()" />
    	</div>
    	 <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
	            <table class="tbl_list_basic1" width="1230px;">
	                <colgroup>
	                    <col width="100">
						<col width="100">
						<col width="100">
						<col width="100">
						<col width="100">
						<col width="80">
						<col width="80">
						<col width="80">
				<?
					if($os_type == 0)
					{
				?>
						<col width="80">	
				<?
					}
				?>
						<col width="120">
	                </colgroup>
	                <thead>
	                    <tr>
	                        <th>IP 주소</th>
							<th>category</th>
							<th>category_info</th>
							<th>action</th>
							<th>action_info</th>
							<th>staytime</th>
							<th>isplaynow</th>
							<th>coin</th>
				<?
					if($os_type == 0)
					{
				?>
							<th>version</th>	
				<?
					}
				?>
							<th>접속일시</th>
	                    </tr>
	                </thead>
	                <tbody id="list_contents">
	                </tbody>
	            </table>  
            	<div class="pagenation mt20" id=pagenation></div>
            </form>  
         </div>
    </div>
</div>