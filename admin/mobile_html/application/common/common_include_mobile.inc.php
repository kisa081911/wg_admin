<?
	set_time_limit(60 * 60);

	session_start();

	include_once("config.inc.php");
	include_once("common_util.inc.php");
	include_once("fbsdk/facebook.inc.php");
	include_once("bitly/bitly.php");
	
	$login_adminidx = $_SESSION["adminidx"];
	$login_adminid = $_SESSION["adminid"];
	$login_adminname = $_SESSION["adminname"];
	$login_adminmenu = $_SESSION["adminmenu"];
	$login_customerservice = $_SESSION["admincustomerservice"];
	$login_dbaccesstype = $_SESSION["dbaccesstype"];
	$client_accesstoken = $_SESSION["client_accesstoken"];

	function check_login()
	{
		global $login_adminidx, $login_adminid, $login_adminname;

		if ($login_adminidx == "" || $login_adminid == "" || $login_adminname == "")
		{
			$current_url = $_SERVER["PHP_SELF"];
			error_go("로그인이 필요한 기능입니다.", "/mobile_html/login?url=$current_url");	
		}
	}
	
	function check_login_layer()
	{
		global $login_adminidx, $login_adminid, $login_adminname;
		
		if ($login_adminidx == "" || $login_adminid == "" || $login_adminname == "")
		{
			error_close_layer("로그인이 필요한 기능입니다.", "/mobile_html/login");
		}
	}
?>