<?
	if (!defined ("__magic_quotes_gpc_check_"))
	{
		define("__magic_quotes_gpc_check_", 1);
		
		if (!get_magic_quotes_gpc()) 
		{
	    	function add_array($var) 
	    	{
	 	       return is_array($var)? array_map("add_array", $var):addslashes($var);
	    	}
	
	    	$_POST = add_array($_POST);
	    	$_GET = add_array($_GET);
		}
	}
	
	if (!defined ("__common_util_inc_"))
	{
		define("__common_util_inc_", 1);
        
		$error_handler_count = 0;
		
        function error_handler($errno, $errstr, $errfile, $errline, $errcontext)
        {
        	global $error_handler_count;
        	
            switch ($errno) {
                case E_ERROR:
                    $errors = "ERROR";
                    break;
                case E_WARNING:
                    $errors = "WARNING";
                    break;
                case E_PARSE:
                    $errors = "PARSING ERROR";
                    break;
                case E_NOTICE:
                    $errors = "NOTICE";
                    return false;
                case E_CORE_ERROR:
                    $errors = "CORE ERROR";
                    break;
                case E_CORE_WARNING:
                    $errors = "CORE WARNING";
                    break;
                case E_COMPILE_ERROR:
                    $errors = "COMPILE ERROR";
                    break;
                case E_COMPILE_WARNING:
                    $errors = "COMPILE WARNING";
                    break;
                case E_USER_ERROR:
                    $errors = "USER ERROR";
                    break;
                case E_COMPILE_WARNING:
                    $errors = "USER WARNING";
                    break;
                case E_USER_NOTICE:
                    $errors = "USER NOTICE";
                    break;
                case E_STRICT:
                    $errors = "STRICT NOTICE";
                    break;
                case E_RECOVERABLE_ERROR:
                    $errors = "RECOVERABLE ERROR";
                    break;
                default:
                    $errors = "UNKNOWN";
                break;
            }
        
            $contents = $errors . " : " . $errfile . " : line " . $errline . "\r\n" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] . "\r\n" . $errstr.  "\r\n" . $errcontext;
        
            if ($error_handler_count < 3)
            {
            	write_log($contents);
	        	$error_handler_count++;
            }
        	
            return true;
        }
        
        // 에러 핸들러 등록
        set_error_handler("error_handler");
		
		function error_back($message)
		{
			echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>alert(\"$message\");history.go(-1)</script>");
			exit();
		}
		
		function error_go($message, $url)
		{
			echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>alert(\"$message\");window.location.href='$url';</script>");
			exit();
		}

		function error_close($message)
		{
			echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>alert(\"$message\");window.close();</script>");
			exit();
		}
		
		function error_close_layer($message)
		{
			echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>alert(\"$message\");window.parent.close_layer_popup('".$_GET['layer_no']."')</script>");
			exit();
		}
		
		function just_go($url)
		{
			echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>window.location.href='$url';</script>");
			exit();
		}
		
		function just_close()
		{
			echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>window.close();</script>");
			exit();
		}
		
		function just_top_go($url)
		{
			echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>window.top.location.href='$url';</script>");
			exit();
		}

		function error_top_go($message, $url)
		{
			echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>alert(\"$message\");window.top.location.href='$url';</script>");
			exit();
		}

		function error_parent_go($message, $url)
		{
			echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>alert(\"$message\");try {window.parent.location.href='$url'} catch(e) {};</script>");
			exit();
		}

		function error_close_parent_go($message, $url)
		{
			echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>alert(\"$message\");try {window.parent.location.href='$url'} catch(e) {};window.close();</script>");
			exit();
		}

		function error_close_opener_go($message, $url)
		{
			echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>alert(\"$message\");try {window.opener.location.href='$url'} catch(e) {};window.close();</script>");
			exit();
		}

		function error_close_opener_parent_go($message, $url)
		{
			echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>alert(\"$message\");try {window.opener.parent.location.href='$url'} catch(e) {};window.close();</script>");
			exit();
		}

		function error_close_opener_parent_refresh($message)
		{
			echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>alert(\"$message\");try {window.opener.parent.location.reload(false)} catch(e) {};window.close();</script>");
			exit();
		}

		function error_parent_refresh($message)
		{
			echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>alert(\"$message\");try {window.parent.location.reload(false);} catch(e) {}</script>");
			exit();
		}

		function error_close_opener_refresh($message)
		{
			echo("<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script>alert(\"$message\");try {window.opener.location.reload(false)} catch(e) {};window.close()</script>");
			exit();
		}
	
		function opener_go($url)
		{
			echo("<script>try {window.opener.location.href='$url'} catch(e) {};window.close();</script>");
			exit();
		}

		function file_read($filepath)
		{
			$content = '';

			if (file_exists($filepath))
			{
				$fp = fopen($filepath, "r");
				$contents = fread($fp, 3000000);
				fclose($fp);
			}
			
			return $contents;
		}
	
		function file_write($filepath, $data)
		{
			$fp = fopen($filepath, "w");
			
			if (!$fp)
				return;
				
    		fwrite($fp, $data);
			fclose($fp);
		}
            
        function file_write_append($filepath, $data)
        {
            $fp = fopen($filepath, "a+");

            if (!$fp)
				return;
				
            fwrite($fp, $data);
            fclose($fp);
        }
        
        function write_log($data)
        {
            global $DEBUG_LOG_PATH;
            
            if ($DEBUG_LOG_PATH == "")
            	return;
            	
            file_write_append($DEBUG_LOG_PATH."LOG_".date("Y-m-d").".txt", "[".date("Y-m-d H:i:s")."] ".$data."\r\n\r\n");
        }

		function directory_size($parentpath)
		{
			$size = 0;
			
			if (!file_exists($parentpath))
				return 0;
				
			$d = dir($parentpath);

			while (false !== ($entry = $d->read()))
			{
				$path = $parentpath.'/'.$entry;
			
				if (lfs_is_dir($path) && $entry != '..' && $entry != '.')
					$size += directory_size($path);
				else if (!lfs_is_dir($path) && $entry != '..' && $entry != '.')
					$size += lfs_filesize($path);
			}		
			
			return $size;
		}

		function encode_excel_field($content)
		{
			return "\"".str_replace("\n", ' ', str_replace("\"", "\"\"", $content))."\"";
		}		
		
		function encode_html_attribute($content)
		{
			return str_replace('"', '&quot;', str_replace("<", "&lt;", str_replace('>', '&gt;', str_replace('&', '&amp;', $content))));
		}

		function encode_script_value($content)
		{
			return str_replace("\"", "\\\"", str_replace("\r", "\\r", str_replace("\n", "\\n", str_replace("\\", "\\\\", $content))));
		}
		
		function encode_db_field($content)
		{
			return str_replace("'", "''", str_replace('\\', '\\\\', $content));
		}
		
		function encode_xml_field($content)
		{
			return str_replace("<", "&lt;", str_replace('>', '&gt;', str_replace('&', '&amp;', $content)));
		}
		
		function encode_json_value($content)
		{
			$content = str_replace("\"", "\\\"", $content);
			$content = str_replace("\n", "\\n", $content);
			$content = str_replace("\r", "\\r", $content);
			
			return $content;
		}
		
		function get_left_title_utf8($title, $length)
		{
			$title = iconv("utf-8", "euc-kr", $title);
			
			$newtitle = "";
			
			for ($i=0; $i<strlen($title); $i++)
			{
				$char = substr($title, $i, 1);
				
				if (ord($char) < 127)
				{
					$length -= 0.6;
					$newtitle .= $char;
				}
				else
				{
					$length -= 1;
					$newtitle .= substr($title, $i, 2);
					$i++;
				}
				
				if ($length <= 0)
					break;
			}

			if ($newtitle != $title)
				$newtitle .= "..";
							
			$newtitle = iconv("euc-kr", "utf-8", $newtitle);

			return $newtitle;
		}
		
		function get_left_title($title, $length)
		{
			$newtitle = "";
			
			for ($i=0; $i<strlen($title); $i++)
			{
				$char = substr($title, $i, 1);
				
				if (ord($char) < 127)
				{
					$length -= 0.7;
					$newtitle .= $char;
				}
				else
				{
					$length -= 1;
					$newtitle .= substr($title, $i, 2);
					$i++;
				}
				
				if ($length <= 0)
					break;
			}

			if ($newtitle != $title)
				$newtitle .= "..";
							
			return $newtitle;
		}
		
		function make_price_format($content)
		{
			$price = "";
			
			while ($content != "")
			{
				if (strlen($content) < 4)
				{
					$price = $content.$price;	
					break;
				}
				
				$price = ",".substr($content, strlen($content)-3, 3).$price;
				$content = substr($content, 0, strlen($content)-3);
			}
			
			return $price;
		}
		
		function make_time_format($minute)
		{
			$time = "";
		
			if ($minute > 60)
				$time = str_pad(floor($minute / 60),2,'0',STR_PAD_LEFT).":".str_pad(($minute % 60),2,'0',STR_PAD_LEFT);
			else
				$time = "00:".str_pad($minute,2,'0',STR_PAD_LEFT);
		
			return $time;
		}
		
		function remove_tag($content)
		{
			while (true)
			{
				if (strpos($content, "<") === false)
					break;
					
				$pos1 = strpos($content, "<");
				
				if (strpos($content, ">", $pos1) === false)
					break;
				
				$pos2 = strpos($content, ">", $pos1);
				
				$content = substr($content, 0, $pos1).substr($content, $pos2+1);
			}
			
			return $content;
		}
		
		function format_size($size)
		{
			if ($size == 0)
				return '0KB';
			else if ($size < 1024)
				return '1KB';
			else if ($size < 1024 * 1024)
				return floor($size / 1024).'KB';	
			else if ($size < 1024 * 1024 * 1024)
				return floor($size / 1024 / 1024).'.'.(floor($size / 1024 / 102.4) % 10).'MB';	
			else
				return floor($size / 1024 / 1024 / 1024).'.'.(floor($size / 1024 / 1024 / 102.4) % 10).'GB';	
		}	
		
		function check_number($param)
		{
			$checks = "0123456789";
			
			for ($i=0; $i<strlen($param); $i++)
			{
				if (strpos($checks, substr($param, $i, 1)) === false)
				{
					error_back("정수형 인자에 오류가 있습니다.");	
				}
			}
		}	
		
		function check_number_list($param)
		{
			$checks = "0123456789,";
			
			for ($i=0; $i<strlen($param); $i++)
			{
				if (strpos($checks, substr($param, $i, 1)) === false)
				{
					error_back("정수형 인자에 오류가 있습니다.");	
				}
			}
		}
		
		function is_number($param)
		{
			$checks = "0123456789";
						
			for ($i=0; $i<strlen($param); $i++)
			{
				if (strpos($checks, substr($param, $i, 1)) === false)
				{
					return false;
				}
			}
			
			return true;
		}
		
		function is_number_list($param)
		{
			$checks = "0123456789,";
						
			for ($i=0; $i<strlen($param); $i++)
			{
				if (strpos($checks, substr($param, $i, 1)) === false)
				{
					return false;
				}
			}
			
			return true;
		}
		
		function check_database_field($param)
		{
			$checks = "0123456789_abcdefghijklmnopqrstuvwxyz";
			
			for ($i=0; $i<strlen($param); $i++)
			{
				if (strpos($checks, substr($param, $i, 1)) === false)
				{
					error_back("필드명에 오류가 있습니다.");	
				}
			}
		}
		
		function check_xss($param)
		{
			$param = strtolower($param);
			$keyword = "javascript|vbscript|jscript|script|history.go|document.cookie|location.href|document.location|window.location|window.open|alert(|settimeout|setinterval|confirm(|object|embed|applet|iframe|frameset|onclick|onload|onmouseover|onmouseout|onfocus|onblur|onabort|onafterprint|onbeforecopy|onbeforeunload|onbounce|oncopy|oncut|ondblclick|ondrag|ondrop";
			$list = explode("|", $keyword);
			
			foreach ($list as $item)
			{
				if (strpos($param, $item) !== false)
				{
					error_back("Script input is not allowed.");
					return;
				}
			}
		}
		
		function sendmail($to, $from, $title, $content)
		{
			$header = "From: $from\nTo: $to\nSubject: $title\nContent-Type: text/html;charset=utf-8";
			mail($to, '=?UTF-8?B?'.base64_encode($title).'?=', $content, $header);
		}		
		
		function get_url()
		{   
			$server=getenv("SERVER_NAME");   
			$file=getenv("SCRIPT_NAME");   
			$query=getenv("QUERY_STRING");   
			$url="http://$server$file";   

			if ($query) 
				$url .= "?$query";   

			return $url;  
		}   
		
		function pad_point($point)
		{
			if (strpos($point, ".") === false)
				$point .= ".00";
			else if (strpos($point, ".") == strlen($point)-2)
				$point .= "0";
			
			return $point;
		}
		
		function build_xml_from_map($list, $tagname)
		{
			$xml = "<$tagname TYPE='MAP'>";
			
			foreach ($list as $key => $value)
			{
				if (!is_number($key))
				{
					$xml .= "<$key>".encode_xml_field($value)."</$key>";
				}
			}
			
			$xml .= "</$tagname>";
			
			return $xml;
		}
		
		function build_xml_from_list($list, $totalcount1, $totalcount2, $tagname)
		{
			$xml = "<$tagname TYPE='LIST' TOTALCOUNT='$totalcount1' TOTALCOUNT2='$totalcount2'>";
			
			for ($i=0; $i<sizeof($list); $i++)
			{
				$xml .= build_xml_from_map($list[$i], "LISTITEM");
			}
			
			$xml .= "</$tagname>";
			
			return $xml;
		}
		
		function get_bar_chart_value($max_width, $max_value, $value)
		{
			if ($max_value == 0 || $value == 0)
				return 0;
		
			$width = ceil(($max_width/$max_value) * $value);
		
			return $width;
		}
				
		function common_image_resize( $srcimg, $dstimg, $rewidth, $reheight, $fixed = true, $usecrop = true)
		{
			$src_info = getimagesize($srcimg);
			
			if (!$fixed)
			{
				if($rewidth < $src_info[0] || $reheight < $src_info[1])
				{
					if(($src_info[0] - $rewidth) > ($src_info[1] - $reheight))
						$reheight = round(($src_info[1]*$rewidth)/$src_info[0]);
					else
						$rewidth = round(($src_info[0]*$reheight)/$src_info[1]);
				}
				else
				{
					copy($srcimg, $dstimg);
					return;
				}
			}
			else if ($rewidth == $src_info[0] && $reheight == $src_info[1])
			{
				copy($srcimg, $dstimg);
				return;
			}
			
			if ($usecrop)
			{
				$width = $src_info[0];
				$height = $src_info[1];

				if ($width > $rewidth * $height / $reheight)
				{
					$width = $rewidth * $height / $reheight;
				}				
				else
				{
					$height = $reheight * $width / $rewidth;
				}
			}
			else
			{
				$width = $src_info[0];
				$height = $src_info[1];
			}
			
			$dst = imageCreatetrueColor($rewidth, $reheight);
		
			if ($src_info[2] == 1)
			{
				$src = ImageCreateFromGIF("$srcimg");
				
				if (!$src)
					return;
				
				imagecopyResampled($dst, $src,0,0,0,0,$rewidth,$reheight,$width,$height);
			
				imagepng($dst, "$dstimg");
			}
			else if ($src_info[2] == 2)
			{
				$src = ImageCreateFromJPEG("$srcimg");
				
				if (!$src)
					return;
				
				imagecopyResampled($dst, $src,0,0,0,0,$rewidth,$reheight,$width,$height);
			
				imagepng($dst, "$dstimg");
			}
			else if ($src_info[2] == 3)
			{
				$src = ImageCreateFromPNG("$srcimg");
				
				if (!$src)
					return;
				
				imagecopyResampled($dst, $src,0,0,0,0,$rewidth,$reheight,$width,$height);
			
				imagepng($dst, "$dstimg");
			}
			else if ($src_info[2] == 6)
			{
				$src = ImageCreateFromBMP($srcimg);
				
				if (!$src)
					return;
				
				imagecopyResampled($dst, $src,0,0,0,0,$rewidth,$reheight,$width,$height);
			
				imagepng($dst, "$dstimg");
			}
			else
			{
				$src = ImageCreateFromJPEG("$srcimg");
				
				if (!$src)
					return;
				
				imagecopyResampled($dst, $src,0,0,0,0,$rewidth,$reheight,$width,$height);
			
				imagepng($dst, "$dstimg");
			}
		
			imageDestroy($src);
			imageDestroy($dst);
		}	
		
		function ImageCreateFromBMP($filename)
		{
			//Ouverture du fichier en mode binaire
			if (! $f1 = fopen($filename,"rb")) 
				return FALSE;
		
			$FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread($f1,14));
			if ($FILE['file_type'] != 19778) 
				return FALSE;
		
			$BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel'.
		                 '/Vcompression/Vsize_bitmap/Vhoriz_resolution'.
		                 '/Vvert_resolution/Vcolors_used/Vcolors_important', fread($f1,40));
			$BMP['colors'] = pow(2,$BMP['bits_per_pixel']);
			if ($BMP['size_bitmap'] == 0) $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
			$BMP['bytes_per_pixel'] = $BMP['bits_per_pixel']/8;
			$BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
			$BMP['decal'] = ($BMP['width']*$BMP['bytes_per_pixel']/4);
			$BMP['decal'] -= floor($BMP['width']*$BMP['bytes_per_pixel']/4);
			$BMP['decal'] = 4-(4*$BMP['decal']);
			if ($BMP['decal'] == 4) $BMP['decal'] = 0;
		
			$PALETTE = array();
			if ($BMP['colors'] < 16777216)
			{
				$PALETTE = unpack('V'.$BMP['colors'], fread($f1,$BMP['colors']*4));
			}
		
			$IMG = fread($f1,$BMP['size_bitmap']);
			$VIDE = chr(0);
		
			$res = imagecreatetruecolor($BMP['width'],$BMP['height']);
			$P = 0;
			$Y = $BMP['height']-1;
			while ($Y >= 0)
			{
				$X=0;
				while ($X < $BMP['width'])
				{
					if ($BMP['bits_per_pixel'] == 24)
					$COLOR = unpack("V",substr($IMG,$P,3).$VIDE);
					elseif ($BMP['bits_per_pixel'] == 16)
					{
						$COLOR = unpack("n",substr($IMG,$P,2));
						$COLOR[1] = $PALETTE[$COLOR[1]+1];
					}
					elseif ($BMP['bits_per_pixel'] == 8)
					{
						$COLOR = unpack("n",$VIDE.substr($IMG,$P,1));
						$COLOR[1] = $PALETTE[$COLOR[1]+1];
					}
					elseif ($BMP['bits_per_pixel'] == 4)
					{
						$COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
						if (($P*2)%2 == 0) $COLOR[1] = ($COLOR[1] >> 4) ; else $COLOR[1] = ($COLOR[1] & 0x0F);
						$COLOR[1] = $PALETTE[$COLOR[1]+1];
					}
					elseif ($BMP['bits_per_pixel'] == 1)
					{
						$COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
						if     (($P*8)%8 == 0) $COLOR[1] =  $COLOR[1]        >>7;
						elseif (($P*8)%8 == 1) $COLOR[1] = ($COLOR[1] & 0x40)>>6;
						elseif (($P*8)%8 == 2) $COLOR[1] = ($COLOR[1] & 0x20)>>5;
						elseif (($P*8)%8 == 3) $COLOR[1] = ($COLOR[1] & 0x10)>>4;
						elseif (($P*8)%8 == 4) $COLOR[1] = ($COLOR[1] & 0x8)>>3;
						elseif (($P*8)%8 == 5) $COLOR[1] = ($COLOR[1] & 0x4)>>2;
						elseif (($P*8)%8 == 6) $COLOR[1] = ($COLOR[1] & 0x2)>>1;
						elseif (($P*8)%8 == 7) $COLOR[1] = ($COLOR[1] & 0x1);
						$COLOR[1] = $PALETTE[$COLOR[1]+1];
					}
					else
					return FALSE;
					imagesetpixel($res,$X,$Y,$COLOR[1]);
					$X++;
					$P += $BMP['bytes_per_pixel'];
				}
				$Y--;
				$P+=$BMP['decal'];
			}
		
			fclose($f1);
		
			return $res;
		}		
	}
	
	function get_diff_date($date1, $date2, $term)
	{
		$_date1 = explode("-", $date1);
		$_date2 = explode("-", $date2);
	
		if ($term == "d")
		{
			$tm1 = mktime(0,0,0,(float)$_date1[1],(float)$_date1[2],(float)$_date1[0]);
			$tm2 = mktime(0,0,0,(float)$_date2[1],(float)$_date2[2],(float)$_date2[0]);
	
			$result = floor(($tm1 - $tm2) / 86400);
		}
		else if ($term == "w")
		{
			$week1 = date("W", mktime(0,0,0,$_date1[1],$_date1[2],$_date1[0]));
			$week2 = date("W", mktime(0,0,0,$_date2[1],$_date2[2],$_date2[0]));
				
			$firstday1 = date("Y-m-d", strtotime($_date1[0]."W".$week1."0"));
			$firstday2 = date("Y-m-d", strtotime($_date2[0]."W".$week2."0"));
				
			$_firstday1 = explode("-", $firstday1);
			$_firstday2 = explode("-", $firstday2);
				
			$tm1 = mktime(0,0,0,$_firstday1[1],$_firstday1[2],$_firstday1[0]);
			$tm2 = mktime(0,0,0,$_firstday2[1],$_firstday2[2],$_firstday2[0]);
				
			$result = floor(($tm1 - $tm2) / 604800);
		}
		else if ($term == "m")
		{
			$tm1 = mktime(0,0,0,$_date1[1],15,$_date1[0]);
			$tm2 = mktime(0,0,0,$_date2[1],15,$_date2[0]);
	
			$result = round(($tm1 - $tm2) / 2592000);
		}
	
		return $result;
	}
	
	function get_past_date($date, $duration, $term)
	{
		$_date = explode("-", $date);
	
		if ($term == "d")
		{
			$_duration = $duration * 86400;
			$tm = mktime(0,0,0,$_date[1],$_date[2],$_date[0]);
	
			$tm_past = $tm - $_duration;
	
			$past = date("Y-m-d", $tm_past);
		}
		else if ($term == "w")
		{
			$_duration = $duration * 604800;
			$tm = mktime(0,0,0,$_date[1],$_date[2],$_date[0]);
			 
			$tm_past = $tm - $_duration;
			 
			$past = date("Y-m-d", $tm_past);
		}
		else if ($term == "m")
		{
			$_duration = $duration * 2592000;
			$tm = mktime(0,0,0,$_date[1],15,$_date[0]);
	
			$tm_past = $tm - $_duration;
			 
			$past = date("Y-m", $tm_past);
		}
	
		return $past;
	}
	
	function convert_savepath($virtualpath)
	{
		global $CONST_UPLOAD_PATH;
		
		return $CONST_UPLOAD_PATH.substr($virtualpath, 12);	
	}
    
	function get_week_date($year, $week, $day)
	{
		if (strlen($week) == "1")
			$week = "0".$week;
	
		$str = $year."W".$week.$day;
	
		return date("Y-m-d",strtotime("$str"));
	}
	
	function get_week($date)
	{
		$_date = explode("-", $date);
	
		$week = date("W", mktime(0,0,0,$_date[1],$_date[2],$_date[0]));
	
		return $week;
	}
	
	function get_percentage($val, $total)
	{
		if ($total == 0)
			return 0;
	
		$percentage = floor((($val * 100) / $total) * 100) / 100;
	
		return $percentage;
	}
	
    function getShortURL($longURL)
    {
        $shorturl = bitly_v3_shorten($longURL, '855fc6213e36e1913d7183c14344022b390f00b5', 'bit.ly');	
	
		$surl = $shorturl['url'];
    
    	return $surl;
    }
    
    function remote_file_exist($url)
    {
    	$ch = curl_init();
    
    	curl_setopt($ch, CURLOPT_URL,$url);
    
    	// don't download content
    	curl_setopt($ch, CURLOPT_NOBODY, 1);
    	curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    
    	if(curl_exec($ch)!==FALSE)
    	{
    		return TRUE;
    	}
    	else
    	{
    		return FALSE;
    	}
    }

    function curl_request_async($url, $params)
    {
    	$type='POST';
    	 
    	foreach ($params as $key => &$val)
    	{
    		if (is_array($val))
    			$val = implode(',', $val);
    		$post_params[] = $key.'='.urlencode($val);
    	}
    	$post_string = implode('&', $post_params);
    	 
    	$parts=parse_url($url);
    	 
    	if ($parts['scheme'] == 'http')
    	{
    		$fp = fsockopen($parts['host'], isset($parts['port'])?$parts['port']:80, $errno, $errstr, 10);
    	}
    	else if ($parts['scheme'] == 'https')
    	{
    		$fp = fsockopen("ssl://" . $parts['host'], isset($parts['port'])?$parts['port']:443, $errno, $errstr, 10);
    	}
    	 
    	// Data goes in the path for a GET request
    	if('GET' == $type)
    		$parts['path'] .= '?'.$post_string;
    	 
    	$out = "$type ".$parts['path']." HTTP/1.1\r\n";
    	$out.= "Host: ".$parts['host']."\r\n";
    	$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
    	$out.= "Content-Length: ".strlen($post_string)."\r\n";
    	$out.= "Connection: Close\r\n\r\n";
    	// Data goes in the request body for a POST request
    	if ('POST' == $type && isset($post_string))
    		$out.= $post_string;
    	 
    	fwrite($fp, $out);
    	fclose($fp);
    }
    
    function https_post($uri, $postdata)
    {
    	$ch = curl_init($uri);
    	curl_setopt($ch, CURLOPT_POST, true);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    	$result = curl_exec($ch);
    	curl_close($ch);
    	 
    	return $result;
    }
    
    function log_node_async($path, $params)
    {
    	$url = NODE_HOST_URL."/".$path;
    	curl_request_async($url, $params);
    }
    
    $key = "djqmfdbthfflxpdj!@#2014";
    
    function encrypt($value)
    {
    	try
    	{
    		$padSize = 16 - (strlen ($value) % 16) ;
    		$value = $value . str_repeat (chr ($padSize), $padSize);
    		$output = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $value, MCRYPT_MODE_CBC, str_repeat(chr(0),16)) ;

    		return base64_encode ($output) ;
    	}
    	catch(Exception $e)
    	{
    		return "";
    	}
    }
    
    function decrypt($value)
    {
    	try
    	{
    		$value = base64_decode ($value) ;
    		$output = mcrypt_decrypt (MCRYPT_RIJNDAEL_128, $key, $value, MCRYPT_MODE_CBC, str_repeat(chr(0),16)) ;
    		 
    		$valueLen = strlen ($output) ;
    		if ( $valueLen % 16 > 0 )
    			$output = "";
    		 
    		$padSize = ord ($output{$valueLen - 1});
    		
    		if ( ($padSize < 1) or ($padSize > 16) )
    		{
    			return "";
    		}
    		
    		for ($i = 0; $i < $padSize; $i++)
    		{
    		 
    			if ( ord ($output{$valueLen - $i - 1}) != $padSize )
    				$output = "";
    		}
    		
    		$output = substr ($output, 0, $valueLen - $padSize) ;
    
    		return $output;
    	}
    	catch(Exception $e)
    	{
    		return "";
    	}
    }
    
    function get_ipaddress()
    {
    	if (getenv('HTTP_CLIENT_IP'))
    		$ip = getenv('HTTP_CLIENT_IP');
    	else if (getenv('HTTP_X_FORWARDED_FOR'))
    		$ip = getenv('HTTP_X_FORWARDED_FOR');
    	else if (getenv('HTTP_X_FORWARDED'))
    		$ip = getenv('HTTP_X_FORWARDED');
    	else if (getenv('HTTP_FORWARDED_FOR'))
    		$ip = getenv('HTTP_FORWARDED_FOR');
    	else if (getenv('HTTP_FORWARDED'))
    		$ip = getenv('HTTP_FORWARDED');
    	else
    		$ip = $_SERVER['REMOTE_ADDR'];
    	return $ip;
    }
    
    function num_ordinal($num)
    {
    	$suffix = "";
    
    	if(is_number($num))
    	{
    		if($num%10 == 1 && $num != 11)
    			$suffix = "st";
    		else if($num%10 == 2 && $num != 12)
    			$suffix = "nd";
    		else if($num%10 == 3 && $num != 13)
    			$suffix = "rd";
    		else
    			$suffix = "th";
    	}
    
    	return $num.$suffix;
    }
    
    function sigma($n)
    { 
    	return ($n*($n+1))/2; 
    }
    
    function get_vip_level($vip_point)
    {
    	$ret = 0; //Bronze
    
    	if($vip_point >= 19999)
    	{
    		$ret = 10; //Chairman
    	}
    	else if($vip_point >= 9999)
    	{
    		$ret = 9; //Premier
    	}
    	else if($vip_point >= 4999)
    	{
    		$ret = 8; //Black
    	}
    	else if($vip_point >= 1999)
    	{
    		$ret = 7; //Ruby
    	}
    	else if($vip_point >= 999)
    	{
    		$ret = 6; //Diamond
    	}
    	else if($vip_point >= 499)
    	{
    		$ret = 5; //Emerald
    	}
    	else if($vip_point >= 99)
    	{
    		$ret = 4; //sapphire
    	}
    	else if($vip_point >= 49)
    	{
    		$ret = 3; //Platium
    	}
    	else if($vip_point >= 19)
    	{
    		$ret = 2; //Gold
    	}
    	else if($vip_point >= 9)
    	{
    		$ret = 1; //Silver
    	}
    
    	return $ret;
    }
    
    function get_fb_pictureURL($value1,$value2)
    {
        return "http://graph.facebook.com/".$value1."/picture?type=square&access_token=$value2";
    }
    
    function slack_send_message($to, $message)
    {
        if(sizeof($to) > 0)
        {
            for($i=0; $i<sizeof($to); $i++)
            {
                $postData = array(
                    'token'    => 'xoxb-587336671202-1567125281427-AbSQ6SuB4BjV1wSRBslkpaGP',
                    'channel' => $to[$i],
                    'text'     => $message
                );
                
                $ch = curl_init("https://slack.com/api/chat.postMessage");
                
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
                $result = curl_exec($ch);
            }
            
            curl_close($ch);
        }
        else
        {
            if($channel == "")
            {
                $channel = "platform_monitoring";
            }
            
            $postData = array(
                'token'    => 'xoxb-587336671202-1567125281427-AbSQ6SuB4BjV1wSRBslkpaGP',
                'channel'  => $channel,
                'text'     => $message
            );
            
            $ch = curl_init("https://slack.com/api/chat.postMessage");
            
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            $result = curl_exec($ch);
            
            curl_close($ch);
        }
    }
?>