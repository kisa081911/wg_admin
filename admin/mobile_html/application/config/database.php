<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Main 
$active_group = 'main';
$active_record = TRUE;

$db['main']['hostname'] = 'take5-dub-dev.cluster-ck4cizw45kga.ap-northeast-2.rds.amazonaws.com';
$db['main']['username'] = 'w_main';
$db['main']['password'] = '$Broqkftjqj@0420';
$db['main']['database'] = 'takefive';
$db['main']['dbdriver'] = 'mysql';
$db['main']['dbprefix'] = '';
$db['main']['pconnect'] = FALSE;
$db['main']['db_debug'] = TRUE;
$db['main']['cache_on'] = FALSE;
$db['main']['cachedir'] = '';
$db['main']['char_set'] = 'utf8';
$db['main']['dbcollat'] = 'utf8_general_ci';
$db['main']['swap_pre'] = '';
$db['main']['autoinit'] = TRUE;
$db['main']['stricton'] = FALSE;

// Main2 
$db['main2']['hostname'] = 'take5-dub-dev.cluster-ck4cizw45kga.ap-northeast-2.rds.amazonaws.com';
$db['main2']['username'] = 'w_main2';
$db['main2']['password'] = '$Broqkftjqj@0420';
$db['main2']['database'] = 'takefive2';
$db['main2']['dbdriver'] = 'mysql';
$db['main2']['dbprefix'] = '';
$db['main2']['pconnect'] = FALSE;
$db['main2']['db_debug'] = TRUE;
$db['main2']['cache_on'] = FALSE;
$db['main2']['cachedir'] = '';
$db['main2']['char_set'] = 'utf8';
$db['main2']['dbcollat'] = 'utf8_general_ci';
$db['main2']['swap_pre'] = '';
$db['main2']['autoinit'] = TRUE;
$db['main2']['stricton'] = FALSE;

// Analysis
$db['analysis']['hostname'] = 'take5-dub-dev.cluster-ck4cizw45kga.ap-northeast-2.rds.amazonaws.com';
$db['analysis']['username'] = 'w_analysis';
$db['analysis']['password'] = '$Broqkftjqj@0420';
$db['analysis']['database'] = 'takefive_analysis';
$db['analysis']['dbdriver'] = 'mysql';
$db['analysis']['dbprefix'] = '';
$db['analysis']['pconnect'] = FALSE;
$db['analysis']['db_debug'] = TRUE;
$db['analysis']['cache_on'] = FALSE;
$db['analysis']['cachedir'] = '';
$db['analysis']['char_set'] = 'utf8';
$db['analysis']['dbcollat'] = 'utf8_general_ci';
$db['analysis']['swap_pre'] = '';
$db['analysis']['autoinit'] = TRUE;
$db['analysis']['stricton'] = FALSE;

// Friend
$db['friend']['hostname'] = 'take5-dub-dev.cluster-ck4cizw45kga.ap-northeast-2.rds.amazonaws.com';
$db['friend']['username'] = 'w_friend';
$db['friend']['password'] = '$Broqkftjqj@0420';
$db['friend']['database'] = 'takefive_friend';
$db['friend']['dbdriver'] = 'mysql';
$db['friend']['dbprefix'] = '';
$db['friend']['pconnect'] = FALSE;
$db['friend']['db_debug'] = TRUE;
$db['friend']['cache_on'] = FALSE;
$db['friend']['cachedir'] = '';
$db['friend']['char_set'] = 'utf8';
$db['friend']['dbcollat'] = 'utf8_general_ci';
$db['friend']['swap_pre'] = '';
$db['friend']['autoinit'] = TRUE;
$db['friend']['stricton'] = FALSE;

// Game
$db['game']['hostname'] = 'take5-dub-dev.cluster-ck4cizw45kga.ap-northeast-2.rds.amazonaws.com';
$db['game']['username'] = 'w_game';
$db['game']['password'] = '$Broqkftjqj@0420';
$db['game']['database'] = 'takefive_game';
$db['game']['dbdriver'] = 'mysql';
$db['game']['dbprefix'] = '';
$db['game']['pconnect'] = FALSE;
$db['game']['db_debug'] = TRUE;
$db['game']['cache_on'] = FALSE;
$db['game']['cachedir'] = '';
$db['game']['char_set'] = 'utf8';
$db['game']['dbcollat'] = 'utf8_general_ci';
$db['game']['swap_pre'] = '';
$db['game']['autoinit'] = TRUE;
$db['game']['stricton'] = FALSE;

// Inbox
$db['inbox']['hostname'] = 'take5-dub-dev.cluster-ck4cizw45kga.ap-northeast-2.rds.amazonaws.com';
$db['inbox']['username'] = 'w_inbox';
$db['inbox']['password'] = '$Broqkftjqj@0420';
$db['inbox']['database'] = 'takefive_inbox';
$db['inbox']['dbdriver'] = 'mysql';
$db['inbox']['dbprefix'] = '';
$db['inbox']['pconnect'] = FALSE;
$db['inbox']['db_debug'] = TRUE;
$db['inbox']['cache_on'] = FALSE;
$db['inbox']['cachedir'] = '';
$db['inbox']['char_set'] = 'utf8';
$db['inbox']['dbcollat'] = 'utf8_general_ci';
$db['inbox']['swap_pre'] = '';
$db['inbox']['autoinit'] = TRUE;
$db['inbox']['stricton'] = FALSE;

// Livestats
$db['livestats']['hostname'] = 'take5-dub-dev.cluster-ck4cizw45kga.ap-northeast-2.rds.amazonaws.com';
$db['livestats']['username'] = 'w_livestats';
$db['livestats']['password'] = '$Broqkftjqj@0420';
$db['livestats']['database'] = 'takefive_livestats';
$db['livestats']['dbdriver'] = 'mysql';
$db['livestats']['dbprefix'] = '';
$db['livestats']['pconnect'] = FALSE;
$db['livestats']['db_debug'] = TRUE;
$db['livestats']['cache_on'] = FALSE;
$db['livestats']['cachedir'] = '';
$db['livestats']['char_set'] = 'utf8';
$db['livestats']['dbcollat'] = 'utf8_general_ci';
$db['livestats']['swap_pre'] = '';
$db['livestats']['autoinit'] = TRUE;
$db['livestats']['stricton'] = FALSE;

// Mobile
$db['mobile']['hostname'] = 'take5-dub-dev.cluster-ck4cizw45kga.ap-northeast-2.rds.amazonaws.com';
$db['mobile']['username'] = 'w_mobile';
$db['mobile']['password'] = '$Broqkftjqj@0420';
$db['mobile']['database'] = 'takefive_mobile';
$db['mobile']['dbdriver'] = 'mysql';
$db['mobile']['dbprefix'] = '';
$db['mobile']['pconnect'] = FALSE;
$db['mobile']['db_debug'] = TRUE;
$db['mobile']['cache_on'] = FALSE;
$db['mobile']['cachedir'] = '';
$db['mobile']['char_set'] = 'utf8';
$db['mobile']['dbcollat'] = 'utf8_general_ci';
$db['mobile']['swap_pre'] = '';
$db['mobile']['autoinit'] = TRUE;
$db['mobile']['stricton'] = FALSE;


// Other
$db['other']['hostname'] = 'take5-dub-dev.cluster-ck4cizw45kga.ap-northeast-2.rds.amazonaws.com';
$db['other']['username'] = 'w_other';
$db['other']['password'] = '$Broqkftjqj@0420';
$db['other']['database'] = 'takefive_other';
$db['other']['dbdriver'] = 'mysql';
$db['other']['dbprefix'] = '';
$db['other']['pconnect'] = FALSE;
$db['other']['db_debug'] = TRUE;
$db['other']['cache_on'] = FALSE;
$db['other']['cachedir'] = '';
$db['other']['char_set'] = 'utf8';
$db['other']['dbcollat'] = 'utf8_general_ci';
$db['other']['swap_pre'] = '';
$db['other']['autoinit'] = TRUE;
$db['other']['stricton'] = FALSE;
