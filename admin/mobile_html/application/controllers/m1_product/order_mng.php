<?php 
	include($_SERVER['DOCUMENT_ROOT']."/mobile_html/application/common/common_include_mobile.inc.php");
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
class Order_Mng extends CI_Controller {
    
    function __construct()
	{
		parent::__construct();
       

	}

    public function index()
    {
        log_message("info", "Order_Mng");
        $data = [
            'resource_url' => RESOURCE_URL
        ];
        
        $this->load->view('/m1_product/order_mng',$data);
    }
}
