<?php 
    include($_SERVER['DOCUMENT_ROOT']."/mobile_html/application/common/common_include_mobile.inc.php");
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
    
    function __construct()
	{
		parent::__construct();
		
		// Cookie Helper Load
		$this->load->helper('cookie');
		// Url Helper Load
		$this->load->helper('url');
	}
    
    public function index()
    {
        $data = [
            'saved_userid' => get_cookie("saved_userid")
            ,'current_url' => $this->input->get('url')
            ,'resource_url' => RESOURCE_URL
        ];

        log_message("info", "login");
        $this->load->view('login',$data);
    }
}
