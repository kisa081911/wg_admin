<?php 
	include($_SERVER['DOCUMENT_ROOT']."/mobile_html/application/common/common_include_mobile.inc.php");
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Login_check extends CI_Controller {
    
    function __construct()
	{
		parent::__construct();

        $this->output->set_header("Content-Type: text/html; charset=UTF-8;");

	}

    public function index()
    {
        log_message("info", "login_check");

        $current_url = $this->input->get('url');
        $saveid = $this->input->post('saveid');

        $this->load->model('admin_class');
        $adminid =$this->input->post('adminid');
        $password = $this->input->post('password');
        $password = encrypt($password);

        $data_list= $this->admin_class->get_userinfo($adminid,$password);
        $adminidx = $data_list->adminidx;
        $adminname = $data_list->adminname;
        $adminid = $data_list->adminid;
        $status = $data_list->status;
        $dbaccesstype = $data_list->dbaccesstype;
        $menu = $data_list->menu;
        $iscustomerservice = $data_list->iscustomerservice;
        $client_accesstoken = $this->admin_class->get_client_accesstoken()->accesstoken;

    log_message("info", "$adminidx");

	if ($adminidx == "")
		error_back("아이디 혹은 비밀번호가 일치하지 않습니다.");
			
	$session_data = array(    //로그인 성공시 session 생성
            'adminidx' => $adminidx,
            'adminid' => $adminid,
            'adminname' => $adminname,
            'adminmenu' => $menu,
            'admincustomerservice' => $iscustomerservice,
            'dbaccesstype' => $dbaccesstype,
            'client_accesstoken' => $client_accesstoken
       );
	   
    $this->session->set_userdata($session_data);	

	// 아이디 저장 (5일)
	if ($saveid == "1")
	{
		setcookie("saved_userid", $adminid, time() + 86400 * 365, "/");
	}
	else
	{
		setcookie("saved_userid", "",  time()-86400 * 365, "/");
	}
	
	if($current_url != NULL)
		just_go($current_url);
	
	if (strpos($menu, "[product]") !== FALSE)
        just_go("/mobile_html/m1_product/order_mng");
	else if (strpos($menu, "[dashboard]") !== FALSE)
		just_go("/mobile_html/m_dashboard/dashboard_all");
	else if (strpos($menu, "[user]") !== FALSE)
		just_go("/mobile_html/m2_user/user_mng");
	else if (strpos($menu, "[user_static]") !== FALSE)
		just_go("/mobile_html/m3_user_static/user_stats");
	else if (strpos($menu, "[pay_static]") !== FALSE)
		just_go("/mobile_html/m4_pay_static/pay_static");
	else if (strpos($menu, "[game_stats]") !== FALSE)
		just_go("/mobile_html/m5_game_stats/game_total_stats");
	else if (strpos($menu, "[game]") !== FALSE)
		just_go("/mobile_html/m6_game/event");
	else if (strpos($menu, "[customer]") !== FALSE)
		just_go("/mobile_html/m7_customer/qa_dashboard");
	else if (strpos($menu, "[monitoring]") !== FALSE)
		just_go("/mobile_html/m8_monitoring/total_stats");
	else if (strpos($menu, "[management]") !== FALSE)
		just_go("/mobile_html/m10_management/daily_game_indicator");
	else if (strpos($menu, "[admin]") !== FALSE)
		just_go("/mobile_html/m11_admin/admin_mng");
	else if (strpos($menu, "[marketing]") !== FALSE)
		just_go("/mobile_html/m12_marketing/marketing_dashboard");
    }
}
