<?php 
	include($_SERVER['DOCUMENT_ROOT']."/mobile_html/application/common/common_include_mobile.inc.php");
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Dashboard_All extends CI_Controller {
    
    function __construct()
	{
		parent::__construct();       

	}

    public function index()
    {
        $data['resource_url'] = RESOURCE_URL;
        
        log_message("info", "Dashboard_All");
        $this->load->view('m_dashboard/dashboard_all',$data);

    }
}
