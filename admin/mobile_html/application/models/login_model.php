<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Login_Model extends CI_Model {
 
    public function __construct()
    {
        parent::__construct();

        $this->main = $this->load->database('main', TRUE);
        $this->analysis = $this->load->database('analysis', TRUE);
	    $this->main2 = $this->load->database('main2', TRUE);
    }
 
    function get_userinfo($adminid,$password){

        if ($adminid == "" || $password == "")
		error_back("잘못된 접근입니다.");

        $sql="SELECT adminidx,adminid,adminname,menu,status,dbaccesstype,iscustomerservice FROM tbl_admin WHERE adminid='$adminid' AND password ='$password' AND status=1";
        return $this->analysis->query($sql)->row();
    }

    function get_client_accesstoken(){
    log_message("info", "get_client_accesstoken");
        $sql="SELECT accesstoken FROM tbl_client_accesstoken";
        return $this->main2->query($sql)->row();
    }
}
