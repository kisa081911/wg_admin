<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Admin_Class extends CI_Model {
 
    public function __construct()
    {
        parent::__construct();

        $this->main = $this->load->database('main', TRUE);
        $this->analysis = $this->load->database('analysis', TRUE);
	    $this->main2 = $this->load->database('main2', TRUE);
    }
 
    function get_userinfo($adminid,$password){

        if ($adminid == "" || $password == "")
		error_back("잘못된 접근입니다.");

        $sql="SELECT adminidx,adminid,adminname,menu,status,dbaccesstype,iscustomerservice FROM tbl_admin WHERE adminid='$adminid' AND password ='$password' AND status=1";
        return $this->analysis->query($sql)->row();
    }

    function get_client_accesstoken(){
    log_message("info", "get_client_accesstoken");
        $sql="SELECT accesstoken FROM tbl_client_accesstoken";
        return $this->main2->query($sql)->row();
    }

    // function admin_save_admin()
	// {
	// 	global $db_analysis;
	
	// 	$adminname = $_POST["adminname"];
	// 	$adminid = $_POST["adminid"];
	// 	$password = $_POST["password"];
	// 	$menu = $_POST["menu"];
	// 	$status = $_POST["status"];
	// 	$dbaccesstype = $_POST["dbaccesstype"];
	// 	$iscustomerservice = $_POST["iscustomerservice"];
	
	// 	if ($adminname == "" || $adminid == "" || $password == "" || $status == "" || $dbaccesstype == "")
	// 		return "필수 항목이 빠졌습니다.";
	
	// 	check_number($status);
	// 	$password = encrypt($password);
	
	// 	if ($iscustomerservice == "")
	// 		$iscustomerservice = "0";
	
	// 	//아이디 중복 검사
	// 	$sql = "SELECT COUNT(*) FROM tbl_admin WHERE adminid='$adminid'";
	// 	$exsit_id = $db_analysis->getvalue($sql);
	
	// 	if ($exsit_id > 0)
	// 		return "중복되는 아이디가 존재합니다.";
	
	// 	$sql = "INSERT INTO tbl_admin(adminname, adminid, password, menu, iscustomerservice, status, dbaccesstype, writedate) VALUES('$adminname', '$adminid', '$password', '$menu', $iscustomerservice, $status, $dbaccesstype, NOW())";
	
	// 	$db_analysis->execute($sql);
	// }

	// function admin_update_admin()
	// {
	// 	global $db_analysis;
	
	// 	$adminidx = $_POST["adminidx"];
	// 	$adminname = $_POST["adminname"];
	// 	$adminid = $_POST["adminid"];
	// 	$password = $_POST["password"];
	// 	$menu = $_POST["menu"];
	// 	$status = $_POST["status"];
	// 	$dbaccesstype = $_POST["dbaccesstype"];
	// 	$iscustomerservice = $_POST["iscustomerservice"];
	
	// 	if ($adminidx == "" || $adminname == "" || $adminid == "" || $password == "" || $status == "" || $dbaccesstype == "")
	// 		return "필수 항목이 빠졌습니다.";
	
	// 	check_number($adminidx);
	// 	check_number($status);
		
	// 	$password = encrypt($password);
	
	// 	if ($iscustomerservice == "")
	// 		$iscustomerservice = "0";
	
	// 	//존재하는 관리자인지 확인
	// 	$adminidx = $db_analysis->getvalue("SELECT adminidx FROM tbl_admin WHERE adminidx=$adminidx");
	
	// 	if ($adminidx == "")
	// 		return "삭제되었거나 존재하지 않는 관리자입니다.";
	
	// 	$sql = "UPDATE tbl_admin SET adminname='$adminname',adminid='$adminid',password='$password',menu='$menu',iscustomerservice='$iscustomerservice',status='$status', dbaccesstype='$dbaccesstype' WHERE adminidx=$adminidx";
	
	// 	$db_analysis->execute($sql);
	// }

	// function admin_delete_admin()
	// {
	// 	global $db_analysis;
	
	// 	$adminidx = $_POST["adminidx"];
	
	// 	if ($adminidx == "")
	// 		return "필수 항목이 빠졌습니다.";
	
	// 	check_number($adminidx);
	
	// 	//존재하는 관리자인지 확인
	// 	$adminidx = $db_analysis->getvalue("SELECT adminidx FROM tbl_admin WHERE adminidx=$adminidx");
	
	// 	if ($adminidx == "")
	// 		return "삭제되었거나 존재하지 않는 관리자입니다.";
	
	// 	$sql = "DELETE FROM tbl_admin WHERE adminidx=$adminidx";
	
	// 	$db_analysis->execute($sql);
	// }
	
	// function admin_update_resource()
	// {
	// 	global $db_main2;
			
	// 	$configidx = $_POST["configidx"];
	// 	$variance = $_POST["variance"];
			
	// 	if ($configidx == "" || $variance == "")
	// 		return "필수 항목이 빠졌습니다.";
			
	// 	$currentversion = $db_main2->getvalue("SELECT currentversion FROM tbl_config_version_info WHERE configidx=$configidx");
			
	// 	$newcurrentversion = str_pad($currentversion+$variance, 4,'0',STR_PAD_LEFT);
			
	// 	$sql = "UPDATE tbl_config_version_info SET beforeversion=currentversion, currentversion='$newcurrentversion', writedate=now()  WHERE configidx=$configidx";
			
	// 	$db_main2->execute($sql);
	// }
	
	// function admin_tmpdeploy_resource()
	// {
	// 	$ret = "";
			
	// 	$webservercnt = $_POST["webservercnt"];
			
	// 	for($i=1; $i<=$webservercnt; $i++)
	// 	{
	// 		// URL 페이지 읽기
	// 		$host = "http://web".str_pad($i, 2,'0',STR_PAD_LEFT).".take5slots.com/";
	// 		$url = $host."facebook/deploy/configversion_create.php";
	// 		$info = parse_url($url);
							
	// 		$host = $info["host"];
	// 		$port = $info["port"];
							
	// 		if($port == 0)
	// 			$port = 80;
	
	// 		$path = $info["path"];
	
	// 		if($info["query"] != "")
	// 			$path .= "?".$info["query"];
	
	// 		$out = "GET $path HTTP/1.0\r\nHost: $host\r\n\r\n";
								
	// 		$fp = fsockopen($host, $port, $error, $errstr, 30);
								
	// 		if(!$fp)
	// 		{
	// 			if($ret == "")
	// 				$ret .= $errstr;
	// 			else
	// 				$ret .= "|".$errstr;
	// 		}
	// 		else
	// 		{
	// 			fputs($fp, $out);
	// 			$start = false;
	// 			$retVal = "";
									
	// 			while(!feof($fp))
	// 			{
	// 				$tmp = fgets($fp, 1024);
										
	// 				if($start == true)
	// 					$retVal .= $tmp;
	
	// 				if($tmp == "\r\n")
	// 					$start = true;
	// 			}
	
	// 			fclose($fp);
								
	// 			if($ret == "")
	// 				$ret .= $retVal;
	// 			else
	// 				$ret .= "|".$retVal;
	// 		}
	// 	}	
	
	// 	return $ret;
	// }
	
	// function admin_file_check()
	// {
	// 	global $db_main2;
			
	// 	$sql = "SELECT filename, currentversion FROM tbl_config_version_info WHERE filename is not null ORDER BY configidx";
			
	// 	$resourcelist = $db_main2->gettotallist($sql);
	
	// 	$ret = "";
			
	// 	$webservercnt = $_POST["webservercnt"];
			
	// 	for($i=1; $i<=$webservercnt; $i++)
	// 	{
	// 		$existret = "";
	// 		$retexist = true;
	
	// 		for($j=0; $j<sizeof($resourcelist); $j++)
	// 		{
	// 			$objectname = $resourcelist[$j]["filename"];
	// 			$objectcurrentversion = $resourcelist[$j]["currentversion"];
		
	// 			$checkurl = "http://web".str_pad($i, 2,'0',STR_PAD_LEFT).".take5slots.com/resource/".$objectname."_".$objectcurrentversion.".swf";
		
	// 			if(!remote_file_exist($checkurl))
	// 			{
	// 				$existret .= $objectname."_".$objectcurrentversion.".swf - Fail"."<br/>";
	// 				$retexist = false;
	// 			}
	// 		}
		
	// 		if($retexist)
	// 		{
	// 			if($ret == "")
	// 				$ret .= "OK";
	// 			else
	// 				$ret .= "|"."OK";
	// 		}
	// 		else
	// 		{
	// 			if($ret == "")
	// 				$ret .= $existret;
	// 			else
	// 				$ret .= "|".$existret;
	// 		}
	// 	}
	
	// 	return $ret;
	// }
	
	// function admin_file_md5check()
	// {
	// 	global $db_main2;
	
	// 	$sql = "SELECT filename, currentversion FROM tbl_config_version_info WHERE filename is not null ORDER BY configidx";
	
	// 	$resourcelist = $db_main2->gettotallist($sql);
		
	// 	$ret = "";
	
	// 	$webservercnt = $_POST["webservercnt"];
				
	// 	$md5array = array();
				
	// 	for($i=1; $i<=$webservercnt; $i++)
	// 	{
	// 		$md5ret = "";
	// 		$retmd5 = true;
				
	// 		for($j=0; $j<sizeof($resourcelist); $j++)
	// 		{
	// 			$objectname = $resourcelist[$j]["filename"];
	// 			$objectcurrentversion = $resourcelist[$j]["currentversion"];
				
	// 			$checkurl = "http://web".str_pad($i, 2,'0',STR_PAD_LEFT).".take5slots.com/resource/".$objectname."_".$objectcurrentversion.".swf";
	
	// 			//web01 체크섬 추출
	// 			if($i == 1)
	// 			{
	// 				array_push($md5array, md5_file($checkurl));
	// 			}
	// 			//web01 체크섬 기준으로 체크
	// 			else
	// 			{
	// 				if($md5array[$j] != md5_file($checkurl))
	// 				{
	// 					$md5ret .= $objectname."_".$objectcurrentversion.".swf - Fail"."<br/>";
	// 					$retmd5 = false;
	// 				}
	// 			}
	// 		}
	
	// 		if($retmd5)
	// 		{
	// 			if($ret == "")
	// 				$ret .= "OK";
	// 			else
	// 				$ret .= "|"."OK";
	// 		}
	// 		else
	// 		{
	// 			if($ret == "")
	// 				$ret .= $md5ret;
	// 			else
	// 				$ret .= "|".$md5ret;
	// 		}
	// 	}
	
	// 	return $ret;
	// }
	
	// function admin_beta_deploy_resource()
	// {
	// 	$ret = "";
				
	// 	$webservercnt = $_POST["webservercnt"];
				
	// 	for($i=1; $i<=$webservercnt; $i++)
	// 	{
	// 		//URL 페이지 읽기
	// 		$host = "http://web".str_pad($i, 2,'0',STR_PAD_LEFT).".take5slots.com/";
	// 		$url = $host."facebook/deploy/configversion_beta_deploy.php";
	// 		$info = parse_url($url);
	
	// 		$host = $info["host"];
	// 		$port = $info["port"];
	
	// 		if($port == 0)
	// 			$port = 80;
	
	// 		$path = $info["path"];
	
	// 		if($info["query"] != "")
	// 			$path .= "?".$info["query"];
	
	// 		$out = "GET $path HTTP/1.0\r\nHost: $host\r\n\r\n";
	
	// 		$fp = fsockopen($host, $port, $error, $errstr, 30);
	
	// 		if(!$fp)
	// 		{
	// 			if($ret == "")
	// 				$ret .= $errstr;
	// 			else
	// 				$ret .= "|".$errstr;
	// 		}
	// 		else
	// 		{
	// 			fputs($fp, $out);
	// 			$start = false;
	// 			$retVal = "";
	
	// 			while(!feof($fp))
	// 			{
	// 				$tmp = fgets($fp, 1024);
					
	// 				if($start == true)
	// 					$retVal .= $tmp;
		
	// 				if($tmp == "\r\n")
	// 					$start = true;
	// 			}
		
	// 			fclose($fp);
		
	// 			if($ret == "")
	// 				$ret .= $retVal;
	// 			else
	// 				$ret .= "|".$retVal;
	// 		}
	// 	}
				
	// 	return $ret;
	// }
	
	// function admin_deploy_resource()
	// {
	// 	$ret = "";
	
	// 	$webservercnt = $_POST["webservercnt"];
	
	// 	for($i=1; $i<=$webservercnt; $i++)
	// 	{
	// 		//URL 페이지 읽기
	// 		$host = "http://web".str_pad($i, 2,'0',STR_PAD_LEFT).".take5slots.com/";
	// 		$url = $host."facebook/deploy/configversion_deploy.php";
	// 		$info = parse_url($url);
	
	// 		$host = $info["host"];
	// 		$port = $info["port"];
	
	// 		if($port == 0)
	// 			$port = 80;
	
	// 		$path = $info["path"];
	
	// 		if($info["query"] != "")
	// 			$path .= "?".$info["query"];
	
	// 		$out = "GET $path HTTP/1.0\r\nHost: $host\r\n\r\n";
						
	// 		$fp = fsockopen($host, $port, $error, $errstr, 30);
						
	// 		if(!$fp)
	// 		{
	// 			if($ret == "")
	// 				$ret .= $errstr;
	// 			else
	// 				$ret .= "|".$errstr;
	// 		}
	// 		else
	// 		{
	// 			fputs($fp, $out);
	// 			$start = false;
	// 			$retVal = "";
	
	// 			while(!feof($fp))
	// 			{
	// 				$tmp = fgets($fp, 1024);
	
	// 				if($start == true)
	// 					$retVal .= $tmp;
	
	// 				if($tmp == "\r\n")
	// 					$start = true;
	// 			}
	
	// 			fclose($fp);
	
	// 			if($ret == "")
	// 				$ret .= $retVal;
	// 			else
	// 				$ret .= "|".$retVal;
	// 		}
	// 	}
	
	// 	return $ret;
	// }
	
	// function admin_maintenance_save()
	// {
	// 	global $db_main2;
	
	// 	$webservercnt = $_POST["webservercnt"];
	
	// 	if ($webservercnt == "")
	// 		return "필수항목이 빠졌습니다.";
			
	// 	for($i=1; $i<=$webservercnt; $i++)
	// 	{
	// 		$servername = "WebServer".str_pad($i, 2,'0',STR_PAD_LEFT);
	// 		$web_data = $servername."_web_maintenance";
	// 		$mobile_data = $servername."_mobile_maintenance";			
				
	// 		$web_maintenance_flag = $_POST["$web_data"];
	// 		$mobile_maintenance_flag = $_POST["$mobile_data"];
				
	// 		if ($web_maintenance_flag == "" && $mobile_maintenance_flag="")
	// 			return "필수항목이 빠졌습니다.";	
			
	// 		$sql = "INSERT INTO tbl_maintenance_xml_setting (web_server, xml_web_maintenance, xml_mobile_maintenance) VALUES('$servername','$web_maintenance_flag', '$mobile_maintenance_flag') ON DUPLICATE KEY UPDATE xml_web_maintenance = VALUES(xml_web_maintenance), xml_mobile_maintenance = VALUES(xml_mobile_maintenance);";
	// 		$db_main2->execute($sql);
	// 	}
	// }
	
	// function admin_maintenance_tmpdeploy_resource()
	// {
	// 	$ret = "";
			
	// 	$webservercnt = $_POST["webservercnt"];
			
	// 	for($i=1; $i<=$webservercnt; $i++)
	// 	{
	// 		// URL 페이지 읽기
	// 		$host = "http://web".str_pad($i, 2,'0',STR_PAD_LEFT).".take5slots.com/";
	// 		$url = $host."facebook/deploy/maintenance_create.php";
	// 		$info = parse_url($url);
					
	// 		$host = $info["host"];
	// 		$port = $info["port"];
	
	// 		if($port == 0)
	// 			$port = 80;
	
	// 		$path = $info["path"];
	
	// 		if($info["query"] != "")
	// 			$path .= "?".$info["query"];
	
	// 		$out = "GET $path HTTP/1.0\r\nHost: $host\r\n\r\n";
	
	// 		$fp = fsockopen($host, $port, $error, $errstr, 30);
	
	// 		if(!$fp)
	// 		{
	// 			if($ret == "")
	// 				$ret .= $errstr;
	// 			else
	// 				$ret .= "|".$errstr;
	// 		}
	// 		else
	// 		{
	// 			fputs($fp, $out);
	// 			$start = false;
	// 			$retVal = "";
	
	// 			while(!feof($fp))
	// 			{
	// 				$tmp = fgets($fp, 1024);

	// 				if($start == true)
	// 					$retVal .= $tmp;
	
	// 				if($tmp == "\r\n")
	// 					$start = true;
	// 			}
	
	// 			fclose($fp);
	
	
	// 			if($ret == "")
	// 				$ret .= $retVal;
	// 			else
	// 				$ret .= "|".$retVal;
	// 		}
	// 	}
	
	// 	return $ret;
	// }
	
	// function admin_maintenance_deploy_resource()
	// {
	// 	$ret = "";

	// 	$webservercnt = $_POST["webservercnt"];

	// 	for($i=1; $i<=$webservercnt; $i++)
	// 	{
	// 		//URL 페이지 읽기
	// 		$host = "http://web".str_pad($i, 2,'0',STR_PAD_LEFT).".take5slots.com/";
	// 		$url = $host."facebook/deploy/maintenance_deploy.php";
	// 		$info = parse_url($url);

	// 		$host = $info["host"];
	// 		$port = $info["port"];

	// 		if($port == 0)
	// 			$port = 80;

	// 		$path = $info["path"];

	// 		if($info["query"] != "")
	// 			$path .= "?".$info["query"];

	// 		$out = "GET $path HTTP/1.0\r\nHost: $host\r\n\r\n";

	// 		$fp = fsockopen($host, $port, $error, $errstr, 30);

	// 		if(!$fp)
	// 		{
	// 			if($ret == "")
	// 				$ret .= $errstr;
	// 			else
	// 				$ret .= "|".$errstr;
	// 		}
	// 		else
	// 		{
	// 			fputs($fp, $out);
	// 			$start = false;
	// 			$retVal = "";

	// 			while(!feof($fp))
	// 			{
	// 				$tmp = fgets($fp, 1024);

	// 				if($start == true)
	// 					$retVal .= $tmp;

	// 				if($tmp == "\r\n")
	// 					$start = true;
	// 			}

	// 			fclose($fp);

	// 			if($ret == "")
	// 				$ret .= $retVal;
	// 			else
	// 				$ret .= "|".$retVal;
	// 		}
	// 	}

	// 	return $ret;
	// }
	
	// function admin_update_resource_data()
	// {
	//     global $db_main2;
	    
	//     $config_name = $_POST["config_name"];
	//     $resource_name = $_POST["resource_name"];
	//     $resource_type = $_POST["resource_type"];
	//     $resource_version = $_POST["resource_version"];
	//     $update_type = $_POST["update_type"];
	    
	//     if($config_name == "" || $resource_name == "" || $resource_type == "" || $resource_version == "")
	//         return "필수항목이 빠졌습니다.";
	        
	//         if($update_type == 1) // Update
	//         {
	//             $sql = "UPDATE tbl_resource_version_info SET configname = '$config_name', filename = '$resource_name', type = $resource_type, beforeversion = currentversion, currentversion = '$resource_version' WHERE configname = '$config_name' AND type = $resource_type";
	//             $db_main2->execute($sql);
	//         }
	//         else if($update_type == 2) // Insert
	//         {
	//             $sql = "INSERT INTO tbl_resource_version_info(configname, filename, type, beforeversion, currentversion, writedate) VALUES('$config_name', '$resource_name', $resource_type, '0.0.0.00', '$resource_version', NOW())";
	//             $db_main2->execute($sql);
	//         }
	// }
	
	// function admin_update_config_setting()
	// {
	//     global $db_main2;
	    
	//     $type = $_POST["type"];
	//     $settings = $_POST["settings"];
	    
	//     if($type == "" || $settings == "")
	//         return "필수항목이 빠졌습니다.";
	        
	//         $sql = "UPDATE tbl_common_settings_info SET settings = '$settings' WHERE type = $type";
	//         $db_main2->execute($sql);
	// }
	// function admin_copy_resource_pord_data()
	// {
	//     global $db_main2;
	    
	//     $sql = "INSERT INTO tbl_resource_version_info (configname, filename, type, beforeversion, currentversion, writedate) ";
	//     $sql .= "SELECT * FROM (SELECT t1.configname, t1.filename, 1, t1.beforeversion, t1.currentversion, NOW() ";
	//     $sql .= "FROM tbl_resource_version_info t1 ";
	//     $sql .= "WHERE t1.`type` = 2 AND EXISTS (SELECT * FROM tbl_resource_version_info WHERE configname = t1.configname AND `type` = 1 AND currentversion != t1.currentversion) ";
	//     $sql .= "UNION ALL ";
	//     $sql .= "SELECT t2.configname, t2.filename, 1, t2.beforeversion, t2.currentversion, NOW() ";
	//     $sql .= "FROM tbl_resource_version_info t2 ";
	//     $sql .= "WHERE t2.`type` = 2 AND NOT EXISTS (SELECT * FROM tbl_resource_version_info WHERE configname = t2.configname AND `type` = 1)) t3 ";
	//     $sql .= "ON DUPLICATE KEY UPDATE beforeversion = VALUES(beforeversion), currentversion = VALUES(currentversion), writedate = VALUES(writedate) ";
	//     $db_main2->execute($sql);
	// }
	
}
