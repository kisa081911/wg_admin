<?
	$top_menu = "product";
	$sub_menu = "product";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$productidx = $_GET["productidx"];
	
	check_number($productidx);
	
	$db_main = new CDatabase_Main();
	
	$sql = "SELECT product_type FROM tbl_product GROUP BY product_type";
	$product_type_list = $db_main->gettotallist($sql);
	
	if ($productidx != "")
	{
		$sql = "SELECT productidx,productname,category,description,imageurl,amount,facebookcredit,vip_point,special_more,special_discount,status,writedate,productkey,product_type FROM tbl_product WHERE productidx=$productidx";
				
		$product = $db_main->getarray($sql);
		
		$productidx = $product["productidx"];				
		$productname = $product["productname"];
		$category = $product["category"];
		$description = $product["description"];
		$imageurl = $product["imageurl"];
		$amount = $product["amount"];
		$facebookcredit = $product["facebookcredit"];
		$vip_point = $product["vip_point"];
		$special_more = $product["special_more"];
		$special_discount = $product["special_discount"];
		$status = $product["status"];
		$writedate = $product["writedate"];
		$productkey = $product["productkey"];
		$product_type = $product["product_type"];

		if ($productidx == "")
			error_back("잘못된 접근 입니다.");
		
		if ($status == "3")
			error_back("삭제된 상품입니다.");
	}

	$db_main->end();
?>
<script type="text/javascript">
	function save_product()
	{
		var product_form = document.product_form;

		if (product_form.productname.value == "")
		{
			alert("상품명을 입력하세요.");
			product_form.productname.focus();
			return;
		}

		if (product_form.imageurl.value == "")
		{
			alert("상품 이미지 주소를 입력하세요.");
			product_form.imageurl.focus();
			return;
		}

		if (product_form.description.value == "")
		{
			alert("상품 설명을 입력하세요.");
			product_form.description.focus();
			return;
		}

		if (product_form.amount.value == "")
		{
			alert("충전 금액을 입력하세요.");
			product_form.amount.focus();
			return;
		}

		if(get_radio("category") == 8)
		{
			if(product_form.more_amount.value == "")
			{
				alert("More % 수치를 입력하세요.");
				product_form.more_amount.focus();
				return;
			}
		}

		if(get_radio("category") == 9)
		{
			if(product_form.discount_amount.value == "")
			{
				alert("Discount % 수치를 입력하세요.");
				product_form.discount_amount.focus();
				return;
			}
		}

		if (product_form.facebookcredit.value == "")
		{
			alert("페이스북 Credit을 입력하세요.");
			product_form.facebookcredit.focus();
			return;
		}

		if (product_form.vip_point.value == "")
		{
			alert("VIP Point를 입력하세요.");
			product_form.vip_point.focus();
			return;
		}

		if (!IsNumber(product_form.amount.value == ""))
		{
			alert("충전금액은 숫자만 입력할 수 있습니다.");
			product_form.amount.focus();
			return;
		}

		if (!IsNumber(product_form.facebookcredit.value == ""))
		{
			alert("페이스북 Credit은 숫자만 입력할 수 있습니다.");
			product_form.facebookcredit.focus();
			return;
		}	

		if (!IsNumber(product_form.vip_point.value == ""))
		{
			alert("VIP Point 숫자만 입력할 수 있습니다.");
			product_form.vip_point.focus();
			return;
		}		

		var param = {};

		param.productidx = product_form.productidx.value;
		param.productname = product_form.productname.value;
		param.imageurl = product_form.imageurl.value;
		param.description = product_form.description.value;
		param.category = get_radio("category");
		param.amount = product_form.amount.value;
		param.facebookcredit = product_form.facebookcredit.value;
		param.vip_point = product_form.vip_point.value;
		param.special_more = product_form.more_amount.value;
		param.special_discount = product_form.discount_amount.value;
		param.status = get_radio("status");
		param.product_type = get_radio("product_type");
		
		if (product_form.productidx.value == "")
			WG_ajax_execute("product/save_product", param, save_product_callback);
		else
			WG_ajax_execute("product/update_product", param, save_product_callback);
	}

	function save_product_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
           	window.location.href = "product_mng.php";
        }
    }

    function delete_product()
    {
        var product_form = document.product_form;

        if (!confirm("상품을 삭제 하시겠습니까?"))
            return;

        var param = {};

        param.productidx = product_form.productidx.value;

        WG_ajax_execute("product/delete_product", param, delete_product_callback);
    }

    function delete_product_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.href = "product_mng.php";
        }
    }

    $(document).ready(function()
	{
    	$("input[name=category]").click(function() 
		{
    		var clickidx = $(this).val();

    		if(clickidx == 8)
    		{
				$("#more_row").show();
    		}
    		else
    		{
    			$("#more_row").hide();
    		}

    		if(clickidx == 9)
    		{
				$("#discount_row").show();
    		}
    		else
    		{
    			$("#discount_row").hide();
    		}
        });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
    	
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 상품상세</div>
<?
	if ($productidx != "")
	{ 
?>
		<div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_product()"></div>
<?
	} 
?>
	</div>
	<!-- //title_warp -->

	<form name="product_form" id="product_form">
			<input type="hidden" name="productidx" id="productidx" value="<?= $productidx ?>"/>
			<table class="tbl_view_basic">
				<colgroup>
					<col width="150">
					<col width="">
				</colgroup>
				<tbody>
					<tr>
						<th><span>*</span> 상품명</th>
						<td><input type="text" class="view_tbl_text" name="productname" id="productname" value="<?= encode_html_attribute($productname) ?>" maxlength="100"></td>
					</tr>
					<tr>
						<th><span>*</span> 이미지 주소</th>
						<td><input type="text" class="view_tbl_text" name="imageurl" id="imageurl" value="<?= encode_html_attribute($imageurl) ?>" maxlength="100"></td>
					</tr>
					<tr>
						<th><span>*</span> 설명</th>
						<td><input type="text" class="view_tbl_text" name="description" id="description" value="<?= encode_html_attribute($description) ?>" maxlength="500"></td>
					</tr>
					<tr>
						<th><span>*</span> 종류</th>
						<td>
							<input type="radio" class="radio" name="category" value="0" <?= ($category == "0") ? "checked" : ""?>> Coin
							<input type="radio" class="radio ml10" name="category" value="1" <?= ($category == "1") ? "checked" : "" ?>> First
							<input type="radio" class="radio ml10" name="category" value="2" <?= ($category == "2") ? "checked" : "" ?>> Second
							<input type="radio" class="radio ml10" name="category" value="8" <?= ($category == "8") ? "checked" : "" ?>> More Chips
							<!--<input type="radio" class="radio ml10" name="category" value="4" <?= ($category == "4") ? "checked" : "" ?>> FreeSpin
							<input type="radio" class="radio ml10" name="category" value="6" <?= ($category == "6") ? "checked" : "" ?>> First
							<input type="radio" class="radio ml10" name="category" value="7" <?= ($category == "7") ? "checked" : "" ?>> Jackpot Bingo
							<input type="radio" class="radio ml10" name="category" value="8" <?= ($category == "8") ? "checked" : "" ?>> More
							<input type="radio" class="radio ml10" name="category" value="9" <?= ($category == "9") ? "checked" : "" ?>> Discount + Item
							<input type="radio" class="radio ml10" name="category" value="100" <?= ($category == "100") ? "checked" : "" ?>> etc  -->
						</td>
					</tr>
					<tr>
						<th><span>*</span> 상품군</th>
						<td>
<?
						for($i=0; $i<sizeof($product_type_list); $i++)
						{							
							$product_type_num = $product_type_list[$i]["product_type"];
							
							if($product_type_num == -1)
								$product_type_name = "사용안함";
							else if($product_type == 1)
								$product_type_name = "Basic";
							else if($product_type == 2)
								$product_type_name = "Season";
							else if($product_type == 3)
								$product_type_name = "Threadhold";
							else if($product_type == 4)
								$product_type_name = "Whale";
							else if($product_type == 5)
								$product_type_name = "28 Retention";
							else if($product_type == 6)
								$product_type_name = "Frist";
							else if($product_type == 7)
								$product_type_name = "Lucky";
							else if($product_type == 8)
								$product_type_name = "Monthly";
							else if($product_type == 9)
								$product_type_name = "Piggypot";
							else if($product_type == 10)
								$product_type_name = "Buyerleave";
							else if($product_type == 11)
								$product_type_name = "NoPayer";
							else if($product_type == 12)
								$product_type_name = "Primedeal_1";
							else if($product_type == 13)
								$product_type_name = "Primedeal_2";
							else if($product_type == 14)
								$product_type_name = "Attend";
							else if($product_type == 15)
								$product_type_name = "Platinum Deal";
							else if($product_type == 16)
								$product_type_name = "Amazing Deal";
							else if($product_type == 17)
								$product_type_name = "First Attend";
							
							if($i==0)
							{	
							
?>
								<input type="radio" class="radio" name="product_type" value="<?=$product_type_num?>" <?= ($product_type_num == "$product_type") ? "checked" : ""?>><?=$product_type_name?>
<?
							}
							else 
							{
?>
								<input type="radio" class="radio ml10" name="product_type" value="<?=$product_type_num?>" <?= ($product_type_num == "$product_type") ? "checked" : "" ?>><?=$product_type_name?>
<?
							}
						}
?>								
						</td>
					</tr>			
					<tr>
						<th><span>*</span> 충전금액</th>
						<td><input type="text" class="view_tbl_text" name="amount" id="amount" value="<?= $amount ?>" onkeypress="return checkonlynum();" style="width:150px"></td>
					</tr>
					<tr id="more_row" style="display:<?= ($category == 8) ? "" : "none" ?>;">
						<th><span>*</span> More</th>
						<td>
							<input type="text" class="view_tbl_text" name="more_amount" id="more_amount" value="<?= ($special_more == "") ? 0 : $special_more ?>" onkeypress="return checkonlynum();" style="width:50px"> %
						</td>
					</tr>
					<tr id="discount_row" style="display:<?= ($category == 9) ? "" : "none" ?>;">
						<th><span>*</span> Discount</th>
						<td>
							<input type="text" class="view_tbl_text" name="discount_amount" id="discount_amount" value="<?= ($special_discount == "") ? 0 : $special_discount ?>" onkeypress="return checkonlynum();" style="width:50px"> %
						</td>
					</tr>
					<tr>
						<th><span>*</span> 페이스북 Credit</th>
						<td><input type="text" class="view_tbl_text" name="facebookcredit" id="facebookcredit" value="<?= $facebookcredit ?>" onkeypress="return checkonlynum();" style="width:150px"> <span style="display:inline-block; padding:5px 0 0 15px;"> credit은 0.1 달러입니다</span> 
						</td>
					</tr>
					<tr>
						<th><span>*</span> VIP Point</th>
						<td><input type="text" class="view_tbl_text" name="vip_point" id="vip_point" value="<?= $vip_point ?>" onkeypress="return checkonlynum();" style="width:150px"> 
						</td>
					</tr>
			
					<tr>
						<th><span>*</span> 사용 여부</th>
						<td><input type="radio" class="radio" name="status" value="1" <?= ($status == "1" || $status == "") ? "checked" : ""?>> 사용 <input type="radio" class="radio ml20" name="status" value="2" <?= ($status == "2") ? "checked" : "" ?> > 미사용</td>
					</tr>
					<tr>
						<th>제품키</th>
						<td><?= $productkey ?></td>
					</tr>
				</tbody>
			</table>    
		</form>	
    		
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="저장" onclick="save_product()">
		<input type="button" class="btn_setting_02" value="취소" onclick="go_page('product_mng.php')">    		
	</div>
</div>
<!--  //CONTENTS WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>