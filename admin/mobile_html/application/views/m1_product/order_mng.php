<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_date").datepicker({ });
	});
	
	$(function() {
	    $("#end_date").datepicker({ });
	});

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();	
	}
    
    function check_sleeptime()
    {
        setTimeout("window.location.reload(false)",60000);
    }
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Web 주문목록 <span class="totalcount">(<?= number_format($totalcount) ?>건)</span></div>
	</div>
	<!-- //title_warp -->
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="order_mng.php">
		<div class="detail_search_wrap">
			<span class="search_lbl">사용자&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" value="<?= encode_html_attribute($username) ?>" id="username" name="username"  onkeypress="search_press(event)" style="width:130px;" />
				<span class="search_lbl ml20">페이스북주문번호</span>
				<input type="text" class="search_text" value="<?= encode_html_attribute($orderno) ?>" id="orderno" name="orderno"  onkeypress="search_press(event)" style="width:130px;" /> 
				<span class="search_lbl ml20">주문상태</span>
				<select name="status" id="status">
					<option value="" <?= ($serarch_status == "") ? "selected" : "" ?>>선택하세요</option>
					<option value="0" <?= ($serarch_status == "0") ? "selected" : "" ?>>결제진행중</option>
					<option value="1" <?= ($serarch_status == "1") ? "selected" : "" ?>>주문완료</option>
					<option value="2" <?= ($serarch_status == "2") ? "selected" : "" ?>>결제취소</option>
				</select>   
				<span class="search_lbl ml20">주문일시</span>
				<input type="text" class="search_text" id="start_date" name="start_date" value="<?= $search_start_date ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_date" name="end_date" value="<?= $search_end_date ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
				<div class="clear"  style="padding-top:10px"></div>
				<span class="search_lbl">페이스북 크레딧</span>
				<input type="text" class="search_text" value="<?= $start_facebookcredit ?>" id="start_facebookcredit" name="start_facebookcredit" onkeypress="search_press(event); return checkonlynum();" style="width:110px;" /> ~ 
				<input type="text" class="search_text" value="<?= $end_facebookcredit ?>" id="end_facebookcredit" name="end_facebookcredit"  onkeypress="search_press(event); return checkonlynum();" style="width:110px;" />
				<span class="search_lbl ml20">
					<input type="checkbox" value="1" name="disputed" <?= ($search_disputed == "1") ? "checked" : "" ?> align="absmiddle" />Disputed
					<input type="checkbox" value="1" name="usecoupon" <?= ($search_usecoupon == "1") ? "checked" : "" ?> align="absmiddle" />쿠폰사용
					<input type="checkbox" value="1" name="usediscount" <?= ($search_usediscount == "1") ? "checked" : "" ?> align="absmiddle" />SpecialOffer
				</span>
				<span class="search_lbl ml20">상품군</span> 
		    		<select name="product_type" id="product_type">
		    			<option value="">선택</option>
	    			
		    		</select>
		    	</span>
				<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="50">
			<col width="">
			<col width="100">
			<col width="80">
			<col width="60">
			<col width="60">
			<col width="40">
			<col width="40">
			<col width="90">
			<col width="120">
		</colgroup>
		<thead>
			<tr>
    			<th>번호</th>
    			<th>사용자</th>
    			<th>유입경로</th>
                <th>가입일</th>
    			<th class="tdr">구입금액</th>
    			<th>Credit</th>
    			<th>쿠폰</th>
    			<th>Special</th>
    			<th>주문상태</th>
    			<th>상품군</th>
    			<th>주문일시<?= ($serarch_status == "2") ? "<br/>취소일시" : "" ?></th>
    		</tr>
		</thead>
		<tbody>

			<tr class="<?= ($status=="2") ? "tr_disabled" : "" ?>" onmouseover="className='tr_over'" onmouseout="className='<?= ($status=="2") ? "tr_disabled" : "" ?>'" onclick="view_order_dtl(<?= $orderidx ?>)">
				<td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
				<td class="point_title"><span style="float:left;padding-top:8px;padding-right:12px;"><img src="/images/icon/<?= ($online_status == "1") ? "status_1.png" : "status_3.png" ?>" align="absmiddle" /></span><img src="<?= $photourl ?>" height="25" width="25" align="absmiddle" /> <?= $username ?>&nbsp;&nbsp;<img src="/images/icon/facebook.png" height="20" width="20" align="absmiddle" style="cursor:pointer" onclick="event.cancelBubble=true;window.open('http://www.facebook.com/<?= $userid ?>')" /></td>
				<td class="tdc point"><?= $adflag ?></td>
				<td class="tdc point"><?= $createdate  ?></td>
				<td class="tdr point_title"><?= number_format($amount) ?></td>
				<td class="tdc point"><?= $facebookcredit ?></td>
				<td class="tdc point"><?= ($couponidx == "0") ? "" : "O" ?></td>
				<td class="tdc point"><?= ($special == "0") ? "" : "O" ?></td>
				<td class="tdc">
				</td>
				<td class="tdc"><?= $product_type_name ?></td>
				<td class="tdc"><?= $writedate.$canceldate ?></td>
			</tr>

		</tbody>
    </table>


</div>
