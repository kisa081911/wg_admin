<?
	$top_menu = "product";
	$sub_menu = "order_dispute";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$username = trim($_GET["username"]);
    $orderno = trim($_GET["orderno"]);
    $serarch_iscomplete = $_GET["iscomplete"];
	$page = ($_GET["page"] == "") ? "1" : $_GET["page"];
	
    check_xss($username.$orderno);
    
	$pagename = "order_dispute.php";
	$listcount = 10;
	$pagefield = "username=$username&orderno=$orderno&iscomplete=$serarch_iscomplete";
	
	$tail = " WHERE true=true ";
	$order_by = "ORDER BY disputeidx DESC ";
	
	if ($orderno != "")
		$tail .= "AND orderno='$orderno'";
	
	if ($serarch_iscomplete != "")
	{
		$tail .= " AND iscomplete IN ($serarch_iscomplete)";
	}
	
	if ($username != "")
		$tail .= " AND B.nickname LIKE '%".encode_db_field($username)."%' ";
	
	$db_main = new CDatabase_Main();
	
	$totalcount = $db_main->getvalue("SELECT COUNT(*) FROM tbl_product_order_dispute A JOIN tbl_user B ON A.useridx=B.useridx $tail");
	
	$sql = "SELECT * FROM tbl_product_order_dispute $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;
	
	$sql = "SELECT A.useridx, B.userid, nickname, orderno, amount, A.coin, user_email, status, iscomplete, writedate, ".
			"(SELECT COUNT(*) FROM tbl_user_online_sync2 WHERE useridx=B.useridx) AS online_status, ".
			"IFNULL((SELECT orderidx FROM tbl_product_order WHERE orderno=A.orderno), 0) AS orderidx ".			
			"FROM tbl_product_order_dispute A JOIN tbl_user B ON A.useridx=B.useridx $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;
	
	$disputelist = $db_main->gettotallist($sql);
	
	$db_main->end();
	
	if ($totalcount < ($page-1) * $listcount && page != 1)
		$page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script>
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();	
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">    	
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 주문 dispute 목록 <span class="totalcount">(<?= make_price_format($totalcount) ?>건)</div>
	</div>
	<!-- //title_warp -->
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="order_dispute.php">
		<div class="detail_search_wrap">
			<span class="search_lbl">사용자&nbsp;&nbsp;&nbsp;</span>
			<input type="text" class="search_text" value="<?= encode_html_attribute($username) ?>" id="username" name="username"  onkeypress="search_press(event)" style="width:130px;" />
			<span class="search_lbl ml20">페이스북주문번호</span>
			<input type="text" class="search_text" value="<?= encode_html_attribute($orderno) ?>" id="orderno" name="orderno"  onkeypress="search_press(event)" style="width:130px;" /> 
			<span class="search_lbl ml20">처리상태</span>
			<select name="iscomplete" id="iscomplete">
				<option value="" <?= ($serarch_iscomplete == "") ? "selected" : "" ?>>선택하세요</option>
				<option value="0" <?= ($serarch_iscomplete == "0") ? "selected" : "" ?>>미처리</option>
				<option value="3" <?= ($serarch_iscomplete == "3") ? "selected" : "" ?>>처리</option>
				<option value="1,2" <?= ($serarch_iscomplete == "1,2") ? "selected" : "" ?>>환불</option>
			</select>   
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="50">
			<col width="">
			<col width="80">
			<col width="80">
			<col width="150">
			<col width="120">
		</colgroup>
		<thead>
    		<tr>
				<th>번호</th>
    			<th>사용자</th>
    			<th Credit="tdr">Credit</th>
    			<th>처리상태</th>
    			<th>email</th>
    			<th>등록일시</th>
    		</tr>
		</thead>
		<tbody>
<?
	for($i=0; $i<sizeof($disputelist); $i++)
	{ 
		$orderidx = $disputelist[$i]["orderidx"];
        $productidx = $disputelist[$i]["productidx"];
        $userid = $disputelist[$i]["userid"];
        $amount = round($disputelist[$i]["amount"] * 10);
        $user_email = $disputelist[$i]["user_email"];
		$username = $disputelist[$i]["nickname"];
		$photourl = $photourl = get_fb_pictureURL($userid,$client_accesstoken);
		$writedate = $disputelist[$i]["writedate"];
        
        $iscomplete = $disputelist[$i]["iscomplete"];
        $vip_level = $disputelist[$i]["vip_level"];
       
        if ($serarch_status == "2")
            $canceldate = "<br/><span style='color:red'>$canceldate</span>";
        else
            $canceldate = "";        
?>
			<tr class="<?= ($status=="2") ? "tr_disabled" : "" ?>" onmouseover="className='tr_over'" onmouseout="className='<?= ($status=="2") ? "tr_disabled" : "" ?>'" onclick="view_order_dtl(<?= $orderidx ?>)">
				<td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
				<td class="point_title"><span style="float:left;padding-top:8px;padding-right:12px;"><img src="/images/icon/<?= ($online_status == "1") ? "status_1.png" : "status_3.png" ?>" align="absmiddle" /></span><img src="<?= $photourl ?>" height="25" width="25" align="absmiddle" /> <?= $username ?>&nbsp;&nbsp;<img src="/images/icon/facebook.png" height="20" width="20" align="absmiddle" style="cursor:pointer" onclick="event.cancelBubble=true;go_facebook('<?= $userid ?>');" /></td>
				<td class="tdc point_title"><?= make_price_format($amount) ?></td>
				<td class="tdc">
<?
					switch ($iscomplete)
					{
						case 0:
							echo("미처리");
							break;
						case 1:
							echo("환불");
							break;
						case 2:
							echo("환불");
							break;
						case 3:
							echo("처리");
							break;
					}
?>
				</td>
				<td><?= $user_email ?></td>
				<td class="tdc"><?= $writedate ?></td>
			</tr>
<?
	} 
?>
		</tbody>
    </table>
<? 
	include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
</div>
<!--  //CONTENTS WRAP -->
    	
<div class="clear"></div>
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>



