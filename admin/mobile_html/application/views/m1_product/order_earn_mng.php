<?
	$top_menu = "product";
	$sub_menu = "order_earn";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$username = trim($_GET["username"]);
    $orderno = trim($_GET["orderno"]);        
    $search_start_date = ($_GET["start_date"] == "") ? date("Y-m-d", time() - 14 * 24 * 60 * 60) : $_GET["start_date"];
    $search_end_date = ($_GET["end_date"] == "") ? date("Y-m-d", time()) : $_GET["end_date"];
	$page = ($_GET["page"] == "") ? "1" : $_GET["page"];
	
    check_xss($username.$orderno); 
    
	$pagename = "order_earn_mng.php";
	$listcount = 10;
	$pagefield = "username=$username&orderno=$orderno&start_date=$search_start_date&end_date=$search_end_date";
	
		
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
		$tail = "WHERE A.status=1 AND A.useridx > 10000 ";
	else
		$tail = "WHERE A.status=1 AND A.useridx > 20000 ";	
	
	$order_by = "ORDER BY orderidx DESC ";
    
    if ($orderno != "")
        $tail .= " AND A.orderno='$orderno'";

    if ($username != "")
        $tail .= " AND B.nickname LIKE '%$username%' ";
    
    if ($search_start_date != "")
    	$tail .= " AND A.writedate >= '$search_start_date 00:00:00'";
    
    if ($search_end_date != "")
    	$tail .= " AND A.writedate <= '$search_end_date 23:59:59'";
                        	
	$db_main = new CDatabase_main();
	
	$totalcount = $db_main->getvalue("SELECT COUNT(*) FROM tbl_product_order_earn A JOIN tbl_user B ON A.useridx=B.useridx $tail");
	
	$sql = "SELECT A.orderidx,A.coin,A.writedate,B.nickname,userid,LEFT(createdate,10) AS createdate, ".
			"(SELECT adflag FROM tbl_user_ext WHERE useridx=A.useridx) AS adflag, ".
			"CASE WHEN A.coin != 0 THEN A.coin ELSE 0 END AS amount, ".
			"(SELECT COUNT(*) FROM tbl_user_online_sync2 WHERE useridx=B.useridx) AS online_status ".
			"FROM tbl_product_order_earn A JOIN tbl_user B ON A.useridx=B.useridx $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;

	$orderlist = $db_main->gettotallist($sql);
     
	$is_search = false;

	if(strcmp($tail, " WHERE B.useridx > 10000 ") == 0)
	{
		$today = date("Y-m-d");
		$sql = "SELECT COUNT(*) FROM tbl_product_order_earn A JOIN tbl_user B ON A.useridx=B.useridx $tail AND A.useridx>10090 AND A.writedate >= '$today 00:00:00' ";
		$today_count = $db_main->getvalue($sql);
		
		$sql = "SELECT IFNULL(SUM(coin),0) FROM tbl_product_order_earn A JOIN tbl_user B ON A.useridx=B.useridx $tail AND A.useridx>10090 AND A.writedate >= '$today 00:00:00' ";
		$today_credit = $db_main->getvalue($sql);
	}
	else
	{
		$is_search = true;
	}
	
	$db_main->end();
	
	if ($totalcount < ($page-1) * $listcount && page != 1)
		$page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_date").datepicker({ });
	});
	
	$(function() {
	    $("#end_date").datepicker({ });
	});

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();	
	}
    
    function check_sleeptime()
    {
        setTimeout("window.location.reload(false)",60000);
    }
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
    	
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 주문목록 <span class="totalcount">(<?= number_format($totalcount) ?>건)
<?
		if(!$is_search)
		{
?>
			&nbsp;&nbsp;오늘 주문 내역 (<?= number_format($today_count) ?>건, $<?= number_format($today_credit, 1) ?>)</span>
<?
		}
?>
		</div>
	</div>
	<!-- //title_warp -->	
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="order_earn_mng.php">
		<div class="detail_search_wrap">
			<span class="search_lbl">사용자&nbsp;&nbsp;&nbsp;</span>
			<input type="text" class="search_text" value="<?= encode_html_attribute($username) ?>" id="username" name="username"  onkeypress="search_press(event)" style="width:130px;" />
			<span class="search_lbl ml20">Earn 주문번호</span>
			<input type="text" class="search_text" value="<?= encode_html_attribute($orderno) ?>" id="orderno" name="orderno"  onkeypress="search_press(event)" style="width:130px;" />
			&nbsp;&nbsp;&nbsp; 
			<span class="search_lbl">주문일시</span>
			<input type="text" class="search_text" id="start_date" name="start_date" value="<?= $search_start_date ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_date" name="end_date" value="<?= $search_end_date ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />	
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="120">
			<col width="">
			<col width="120">
			<col width="120">			
			<col width="120">
			<col width="120">
			<col width="150">
		</colgroup>
		<thead>
			<tr>
				<th>번호</th>
				<th>사용자</th>
				<th>유입경로</th>
				<th>가입일</th>				
				<th>주문 Coin</th>
				<th>주문상태</th>
				<th>주문일시</th>
			</tr>
		</thead>
		<tbody>
<?
	for($i=0; $i<sizeof($orderlist); $i++)
	{ 
		$orderidx = $orderlist[$i]["orderidx"];
        $userid = $orderlist[$i]["userid"];
        $amount = $orderlist[$i]["amount"];
		$username = $orderlist[$i]["nickname"];
		$photourl = get_fb_pictureURL($userid,$client_accesstoken);
		$money = $orderlist[$i]["coin"];
        $adflag = $orderlist[$i]["adflag"];
        $createdate = $orderlist[$i]["createdate"];
		$writedate = $orderlist[$i]["writedate"];    
        $online_status = $orderlist[$i]["online_status"];

        if ($adflag == "")
            $adflag = "바이럴";
        else if ($adflag == "1")
            $adflag = "maudau";
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''" onclick="view_earn_order_dtl(<?= $orderidx ?>)">
				<td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
				<td class="point_title"><span style="float:left;padding-top:8px;padding-right:12px;"><img src="/images/icon/<?= ($online_status == "1") ? "status_1.png" : "status_3.png" ?>" align="absmiddle" /></span><img src="<?= $photourl ?>" height="25" width="25" align="absmiddle" /> <?= $username ?>&nbsp;&nbsp;<img src="/images/icon/facebook.png" height="20" width="20" align="absmiddle" style="cursor:pointer" onclick="event.cancelBubble=true;window.open('http://www.facebook.com/<?= $userid ?>')" /></td>
				<td class="tdc point"><?= $adflag ?></td>
				<td class="tdc point"><?= $createdate  ?></td>
				<td class="tdc point">$&nbsp;<?= $money ?></td>
				<td class="tdc">주문 완료</td>
				<td class="tdc"><?= $writedate ?></td>
			</tr>
<?
	} 
?>
		</tbody>
   </table>
<? 
	include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
</div>
<!--  //CONTENTS WRAP -->    	
    
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>