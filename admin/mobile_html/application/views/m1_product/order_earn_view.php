<?
	$top_menu = "product";
	$sub_menu = "order_earn";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$orderidx = $_GET["orderidx"];
	
	if ($orderidx == "")
		error_back("잘못된 접근입니다.");
	
	check_number($orderidx);
	
    $db_main = new CDatabase_Main();
    
	//주문정보조회
	$sql="SELECT orderidx,useridx,orderno,coin,writedate,usercoin, ".
		 "CASE WHEN coin != 0 THEN coin ELSE 0 END AS amount, ".
		 "CASE WHEN coin != 0 THEN 'coin' ELSE 0 END AS producttype ".
		 "FROM tbl_product_order_earn WHERE orderidx=$orderidx";
	$order = $db_main->getarray($sql);
	
	$orderidx = $order["orderidx"];
	$useridx = $order["useridx"];
	$orderno = $order["orderno"];
	$order_coin = $order["coin"];
	$orderdate = $order["writedate"];
	$orderamount = $order["amount"];
    $orderproducttype = $order["producttype"];	
    $usercoin = $order["usercoin"];
                	
	if ($orderidx == "")
		error_back("존재하지 않는 주문입니다.");
	
	// 사용자정보 조회
	$sql = "SELECT userid,B.email,A.nickname,coin FROM tbl_user AS A JOIN tbl_user_ext AS B ON A.useridx=B.useridx WHERE A.useridx=$useridx";
	
	$user = $db_main->getarray($sql);
	
	$userid = $user["userid"];
	$email = $user["email"];
	$nickname = $user["nickname"];
	$photourl = get_fb_pictureURL($userid,$client_accesstoken);
	$coin = $user["coin"];	
?>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
    	
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 주문상세</div>
	</div>
	<!-- //title_warp -->
	    	
	<div class="h2_title">사용자정보</div>
		<div class="user_info_summary" onmouseover="className='user_info_summary_over'" onmouseout="className='user_info_summary'" onclick="view_user_dtl(<?= $useridx ?>)">
			<img src="<?= $photourl ?>" height="50" width="50" class="summary_user_image"/>
			<div class="sumary_username_wrap">
				<div class="summary_username">[<?= $useridx ?>] <?= $nickname ?></div>
				<div class="summary_user_info"><?= $email ?></div>
			</div>
			<div class="summary_user_coin"><?= number_format($coin) ?></div>			
		</div>
    	<div class="h2_title">주문정보</div>
    	<div class="h2_cont">
    		
    		<div class="summary_item">
    			<div class="summary_item_lbl">상품종류</div>
    			<div class="summary_item_value"><?= $orderproducttype ?></div>
    		</div>
    		
    		<div class="summary_item">
    			<div class="summary_item_lbl">주문금액</div>
    			<div class="summary_item_value"><?= number_format($orderamount) ?></div>
    		</div>
    		
    		<div class="summary_item">
    			<div class="summary_item_lbl">사용자 보유 칩</div>
    			<div class="summary_item_value"><?= number_format($usercoin) ?> (주문 시점의 사용자 보유 칩 - 상품 칩 더해지기 이전)</div>
    		</div>
    		
    		<div class="summary_item">
    			<div class="summary_item_lbl">주문번호</div>
    			<div class="summary_item_value"><?= $orderno ?></div>
    		</div>
    		
    		<div class="summary_item">
    			<div class="summary_item_lbl">주문 Coin</div>
    			<div class="summary_item_value">$ <?= $order_coin ?></div>
    		</div> 
    		
    		<div class="summary_item">
	    		<div class="summary_item_lbl">상태</div>
	    		<div class="summary_item_value">주문완료</div>
	    	</div>
	    	
	    	<div class="summary_item">
	    		<div class="summary_item_lbl">주문일시</div>
	    		<div class="summary_item_value"><?= $orderdate ?></div>
	    	</div>
	    		         
			<div class="button_wrap tdr">
				<input type="button" class="btn_setting_01" value="목록" onclick="go_page('order_earn_mng.php')">
			</div>
		</div>	
	</div>	
</div>
<!--  //CONTENTS WRAP -->    		
<?
	$db_main->end();

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>