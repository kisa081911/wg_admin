<?
	$top_menu = "product";
	$sub_menu = "product_android";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$page = ($_GET["page"] == "") ? "1" : $_GET["page"];
	$category = $_GET["category"];
	$status = $_GET["status"];
	$search_value = trim($_GET["search_value"]);
	$start_amount = $_GET["start_amount"];
	$end_amount = $_GET["end_amount"];
	$start_money = $_GET["start_money"];
	$end_money = $_GET["end_money"];
			
    check_xss($search_value);
    
	$pagename = "product_android_mng.php";
	$pagefield = "category=$category&status=$status&search_value=$search_value&start_amount=$start_amount&end_amount=$end_amount&start_money=$start_money&end_money=$end_money";
	$listcount = "10";
	
	$tail = " WHERE os_type = 2 AND status!=3 ";
	
	if ($category != "")
		$tail .= " AND category=$category";
	
	if ($status != "")
		$tail .= " AND status=$status";
	
	if ($start_amount != "")
		$tail .= " AND amount >= $start_amount";
	
	if ($end_amount != "")
		$tail .= " AND amount <= $end_amount";
	
	if ($start_money != "")
		$tail .= " AND money >= $start_money";
	
	if ($end_money != "")
		$tail .= " AND money <= $end_money";
	
	if ($search_value != "")
		$tail .= " AND title LIKE '%$search_value%'";
			
    $db_main = new CDatabase_Main();
	
	$totalcount = $db_main->getvalue("SELECT COUNT(*) FROM tbl_product_mobile $tail");
	
	$sql = "SELECT productidx,productkey,category,productname,description,amount,money,special_more,special_discount,status,writedate FROM tbl_product_mobile $tail ORDER BY productidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
	$productlist = $db_main->gettotallist($sql);
	
	$db_main->end();
	
	if ($totalcount < ($page-1) * $listcount && page != 1)
		$page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script>
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}
	
	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}
</script>    	
    	<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; Android 상품목록 <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
	        </div>
	    	<!-- //title_warp -->
	    	
	    	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="product_android_mng.php">
		    	<div class="detail_search_wrap">
		    		<span class="search_lbl">충전금액</span> 
		    		<input type="text" class="search_text" name="start_amount" id="start_amount" style="width:60px" value="<?= $start_amount ?>" onkeypress="search_press(event); return checkonlynum();" />
		    		 ~  
		    		<input type="text" class="search_text" name="end_amount" id="end_amount" style="width:60px" value="<?= $end_amount ?>" onkeypress="search_press(event); return checkonlynum();" />  
		    		<span class="search_lbl ml20">구매금액($)</span> 
		    		<input type="text" class="search_text" name="start_money" id="start_money" style="width:60px" value="<?= $start_money ?>" onkeypress="search_press(event); return checkonlynum();" />
		    		 ~  
		    		<input type="text" class="search_text" name="end_money" id="end_money" style="width:60px" value="<?= $end_money ?>" onkeypress="search_press(event); return checkonlynum();" />  
		    		<span class="search_lbl ml20">패키지</span> 
		    		<select name="category" id="category">
		    			<option value="">선택</option>
		    			<option value="0" <?= ($category == "0") ? "selected" : ""?>>Basic</option>
		    			<option value="1" <?= ($category == "1") ? "selected" : ""?>>1st Time</option>
		    			<option value="2" <?= ($category == "2") ? "selected" : ""?>>Season Offer</option>
		    			<option value="3" <?= ($category == "3") ? "selected" : ""?>>Special Offer</option>		    			
		    		</select>
		    		<span class="search_lbl ml20">상태</span> 
		    		<select name="status" id="status">
		    			<option value="">선택</option>
		    			<option value="1" <?= ($status == "1") ? "selected" : ""?>>사용</option>
		    			<option value="2" <?= ($status == "2") ? "selected" : ""?>>미사용</option>
		    		</select>
		    		<span class="search_lbl ml20">상품명</span>
		    		<input type="text" class="search_text" id="serach_value" name="search_value"  value="<?= encode_html_attribute($search_value) ?>"  onkeypress="search_press(event)" style="width:130px;" /> 
		            <div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		    	</div>
	    	</form>
    	
    		<table class="tbl_list_basic1">
    		<thead>
    		<tr>
    			<th>번호</th>
    			<th>상품명</th>
    			<th>패키지</th>
    			<th>충전금액</th>
    			<th>More</th>
    			<th>Discount</th>
    			<th>구매금액($)</th>
    			<th>상태</th>
    			<th>등록일</th>
    		</tr>
			</thead>
			<tbody>
<?
	for ($i=0; $i<sizeof($productlist); $i++)
	{ 
		$productidx = $productlist[$i]["productidx"];
		$category = $productlist[$i]["category"];
		$productname = $productlist[$i]["productname"];
		$description = $productlist[$i]["description"];
		$amount = $productlist[$i]["amount"];
		$special_more = $productlist[$i]["special_more"];
		$special_discount = $productlist[$i]["special_discount"];
		$money = $productlist[$i]["money"];
		$status = $productlist[$i]["status"];
		$writedate = $productlist[$i]["writedate"];
		
		if ($category == "0")
			$package = "Basic";
		else if ($category == "1")
			$package = "1st Time";
		else if ($category == "2")
			$package = "Season Offer";
		else if ($category == "3")
			$package = "Special Offer";	
		
?>
			<tr class="<?= ($status=="2") ? "tr_disabled" : "" ?>" onmouseover="className='tr_over'" onmouseout="className='<?= ($status=="2") ? "tr_disabled" : "" ?>'" onclick="view_product_android_dtl(<?= $productidx ?>)">
				<td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
				<td class="tdl point_title"><?= $productname ?></td>
				<td class="tdc point"><?= $package ?></td>
				<td class="tdc point"><?= make_price_format($amount) ?></td>
				<td class="tdc point"><?= $special_more ?>%</td>
				<td class="tdc point"><?= $special_discount ?>%</td>
				<td class="tdc point"><?= $money ?></td>
				<td class="tdc point"><?= ($status == "1") ? "사용" : "미사용" ?></td>
				<td class="tdc"><?= $writedate ?></td>
			</tr>
<?
	} 
?>
			</tbody>
    		</table>
    		
<?
	include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
    		<div class="button_warp tdr">
				<input type="button" class="btn_setting_01" value="상품 추가" onclick="window.location.href='product_ios_write.php'">    		
    		</div>
    	</div>
    	<!--  //CONTENTS WRAP -->
    	
    	<div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>