<script type="text/javascript">
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}
	
	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}
</script>    	
    	<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; 상품목록 <span class="totalcount">(<?= number_format($totalcount) ?>)</span></div>
	        </div>
	    	<!-- //title_warp -->
	    	
	    	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="product_mng.php">
		    	<div class="detail_search_wrap">
		    		<span class="search_lbl">금액/개수</span> 
		    		<input type="text" class="search_text" name="start_amount" id="start_amount" style="width:60px" value="<?= $start_amount ?>" onkeypress="search_press(event); return checkonlynum();" />
		    		 ~  
		    		<input type="text" class="search_text" name="end_amount" id="end_amount" style="width:60px" value="<?= $end_amount ?>" onkeypress="search_press(event); return checkonlynum();" />  
		    		<span class="search_lbl ml20">페이스북 creidt</span> 
		    		<input type="text" class="search_text" name="start_facebookcredit" id="start_facebookcredit" style="width:60px" value="<?= $start_facebookcredit ?>" onkeypress="search_press(event); return checkonlynum();" />
		    		 ~  
		    		<input type="text" class="search_text" name="end_facebookcredit" id="end_facebookcredit" style="width:60px" value="<?= $end_facebookcredit ?>" onkeypress="search_press(event); return checkonlynum();" />
		    		<span class="search_lbl ml20">상태</span> 
		    		<select name="status" id="status">
		    			<option value="">선택</option>
		    			<option value="1" <?= ($status == "1") ? "selected" : ""?>>사용</option>
		    			<option value="2" <?= ($status == "2") ? "selected" : ""?>>미사용</option>
		    		</select>
		    		<span class="search_lbl ml20">상품군</span> 
		    		<select name="product_type" id="product_type">
		    			<option value="">선택</option>
		    		</select>		    		
		    		<span class="search_lbl ml20">상품명</span>
		    		<input type="text" class="search_text" id="serach_value" name="search_value"  value="<?= encode_html_attribute($search_value) ?>"  onkeypress="search_press(event)" style="width:130px;" /> 
		            <div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		    	</div>
	    	</form>
    	
    		<table class="tbl_list_basic1">
    		<thead>
    		<tr>
    			<th>번호</th>
    			<th>상품이미지</th>
    			<th class="tdl">상품명</th>
    			<th>금액/개수</th>
    			<th>페이스북 Credit</th>
    			<th>상품군</th>
    			<th>상태</th>
    			<th>등록일</th>
    		</tr>
			</thead>
			<tbody>
			<tr class="<?= ($status=="2") ? "tr_disabled" : "" ?>" onmouseover="className='tr_over'" onmouseout="className='<?= ($status=="2") ? "tr_disabled" : "" ?>'" onclick="view_product_dtl(<?= $productidx ?>)">
				<td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
				<td class="tdc"><img src="<?= $imageurl ?>" width="25" height="25"></td>
				<td class="point_title"><?= $productname ?></td>
				<td class="tdc point"><?= ($category == 7) ? "-" : number_format($amount)." $count_name" ?></td>
				<td class="tdc point"><?= $facebookcredit ?></td>
				<td class="tdc point"><?= $product_type_name ?></td>
				<td class="tdc point"><?= ($status == "1") ? "사용" : "미사용" ?></td>
				<td class="tdc"><?= $writedate ?></td>
			</tr>
			</tbody>
    		</table>

    		<div class="button_warp tdr">
				<input type="button" class="btn_setting_01" value="상품 추가" onclick="window.location.href='product_write.php'">    		
    		</div>
    	</div>
    	<!--  //CONTENTS WRAP -->
