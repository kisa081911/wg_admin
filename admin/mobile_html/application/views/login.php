<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="user-scalable=yes, initial-scale=1.0, minimum-scale=1.0,maximum-scale=1.0, width=device-width" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <title>Take5 Free Slots - 더블유게임즈 어드민</title>

    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico">
    <link rel="stylesheet" href="<?=$resource_url?>/styles/reset.css">
    <link rel="stylesheet" href="<?=$resource_url?>/styles/layout.css">
    <link rel="stylesheet" href="<?=$resource_url?>/styles/modules.css">
    <link rel="stylesheet" href="<?=$resource_url?>/styles/contents.css">

    <script src="<?=$resource_url?>/scripts/jquery/jquery-1.9.1.min.js"></script>
    <script src="<?=$resource_url?>/scripts/jquery/jquery-ui.min.js"></script>
    <script src="<?=$resource_url?>/scripts/jquery/plugins/slick.min.js"></script>
    <script src="<?=$resource_url?>/scripts/jquery/plugins/swiper.min.js"></script>
    <script src="<?=$resource_url?>/scripts/jquery/plugins/iscroll.min.js"></script>
    <script src="<?=$resource_url?>/scripts/MUI.js"></script>
    <script src="<?=$resource_url?>/scripts/common.js"></script>
	<script type="text/javascript" src="/js/common_util.js"></script>
	<script>
		function show_pwd()
		{
			document.getElementById("password_cover").style.display = "none";
			document.getElementById("password").style.display = "";	
			document.getElementById("password").focus();
		}

		function login_press(e)
		{
			if (((e.which) ? e.which : e.keyCode) == 13)
			{
				form_submit();
				return false;
			}
		}

		function form_submit()
		{
			var login_form = document.login_form;

			if (login_form.adminid.value == "" || login_form.adminid.value == "아이디")
			{
				alert("아이디를 입력하세요");
				login_form.adminid.focus();
				return;
			}

			if (login_form.password.value == "")
			{
				alert("비밀번호를 입력하세요");

				if (login_form.password.style.display != "none")
					login_form.password.focus();
				else
					login_form.password_cover.focus();
				
				return;
			}

			login_form.submit();
		}
	</script>
</head>
<body>
	<form id="login_form" class="form" name="login_form" method="post" action="login_check" onsubmit="return false" style="height:100%">
	<input type="hidden" name="currenturl" value="<?=$current_url?>">
		<!-- sign in -->
		<div class="wrap wrap--sign">
			<div class="wrap__inner">       
				<!-- 헤더 -->     
				<header class="header">
					<div class="header__inner">
						<a href="javascript:;" class="logo">
							<img src="<?=$resource_url?>/images/common/logo.png" alt="">
							<div class="logo__text">
								DOUBLEU GAMES
								<span>admin</span>
							</div>
						</a>
						<h1>SIGN IN</h1>
					</div>
				</header>
				<!-- //헤더 -->  

				<!-- 주요 컨텐츠 -->            
				<main class="container">
					<div class="from__cont">
						<div class="input-row">
							<div class="input-row__tit">
								<label for="userName" class="input-row__text">
									이름<span class="hidden">을 입력해주세요.</span>
								</label>
							</div>
							<div class="input-row__cont">
								<input type="text"   name="adminid" maxlength="50" onkeypress="login_press(event)" value="<?= ($saved_userid != "") ? $saved_userid : "" ?>"  placeholder="e.g.) 홍길동" class="form-control form-control--default">
							</div>
						</div>
						<div class="input-row">
							<div class="input-row__tit input-row__tit--between">
								<label for="userPw" class="input-row__text">
									비밀번호<span class="hidden">를 입력해주세요.</span>
								</label>
								<a href="javascript:;" class="link link--small">
								비밀번호를 잊으셨나요?
								</a>
							</div>
							<div class="input-row__cont">
								<input type="password" name="password" id="password" maxlength="100" onkeypress="login_press(event)"  class="form-control form-control--default" placeholder="e.g.) 숫자 + 영문 + 특수기호 조합의 비밀번호">
							</div>
						</div>
						<div class="box-between login-box">
							<div class="checkbox checkbox--default">
								<input type="checkbox" id="checkboxExample1" class="checkbox__input" name="saveid" id="saveid" value="1" <?= ($saved_userid != "") ? "checked" : "" ?>>
								<label for="checkboxExample1" class="checkbox__label">ID 저장</label>                                
							</div>
							<div class="btns">
								<button type="submit" class="btn btn--primary" onclick="form_submit()">로그인</button>
							</div>
						</div>
					</div>
					<div class="form__footer">
						<p class="text text--align-c">
							아직 회원이 아니신가요? <a href="" class="link link--small">회원가입하러 가기!</a>
						</p>
					</div>					
				</main>
				<!-- //주요 컨텐츠 -->

				<!-- 푸터 -->     
				<footer class="footer">
					<div class="footer__inner">
						<p class="text text--align-c">&copy; Copyright 2021. All rights reserved. </p>
					</div>
				</footer>
				<!-- //푸터 -->     
			</div>
		</div>
		<!-- //sign in -->
	</form>
</body>
</html>