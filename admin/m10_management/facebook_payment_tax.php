<?
	$top_menu = "management";
	$sub_menu = "facebook_payment_tax";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");	
		
	$search_start_year = $_GET["search_start_year"];
	$search_start_month = $_GET["search_start_month"]; 
	$search_end_year = $_GET["search_end_year"];
	$search_end_month = $_GET["search_end_month"];	
	
			
	if($search_start_year == "" && $search_start_month == "")
	{
		$search_start_year = date("Y", strtotime("-2 month"));
		$search_start_month = date("m", strtotime("-2 month"));
	}
	
	if($search_end_year == "" && $search_end_month == "")
	{
		$search_end_year = date("Y");
		$search_end_month = date("m");
	}
	
	$search_start_createdate = $search_start_year."-".$search_start_month;
	$search_end_createdate = $search_end_year."-".$search_end_month;
	
	$db_main = new CDatabase_Main();
	
	$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m') AS date_month, status, AVG(payout_foreign_exchange_rate) AS fx_rate, SUM(facebookcredit/10) AS money, SUM(currency_amount) AS currency_amount, SUM(tax_amount*payout_foreign_exchange_rate) AS commission, SUM(tax_amount) AS tax_amount
			FROM tbl_product_order tt WHERE useridx > 20000 AND '$search_start_year-$search_start_month' <= DATE_FORMAT(writedate, '%Y-%m') AND DATE_FORMAT(writedate, '%Y-%m') <= '$search_end_year-$search_end_month' ".
			"AND status IN (1, 2) GROUP BY DATE_FORMAT(writedate, '%Y-%m'), status";
	$facebook_tax_data = $db_main->gettotallist($sql);
	
	$db_main->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="facebook_payment_tax.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Facebook 매출 및 세금 지표</div>
		<div class="search_box">			
			<select name="search_start_year" id="search_start_year">
			 <?for ($i=2019; $i <= 2030; $i++ ) {?>
				<option value="<?=$i?>" <?= ($search_start_year == $i) ? "selected" : "" ?>><?=$i?></option>				
			<?}?>
			</select>년
			<select name="search_start_month" id="search_start_month">
			<?for( $i = 1; $i <= 12; $i++ ) { 
				$month_start_num = str_pad( $i, 2, 0, STR_PAD_LEFT );
			?>			
				<option value="<?=$month_start_num?>" <?= ($search_start_month == $month_start_num) ? "selected" : "" ?>><?=$month_start_num?></option>			
			<?}?>
			</select>월				
			 ~
			<select name="search_end_year" id="search_end_year">
			<?for ($i=2014; $i <= 2020; $i++ ) {?>
				<option value="<?=$i?>" <?= ($search_end_year == $i) ? "selected" : "" ?>><?=$i?></option>				
			<?}?>
			</select>년
			<select name="search_end_month" id="search_end_month">
			<?for( $i = 1; $i <= 12; $i++ ) { 
				$month_end_num = str_pad( $i, 2, 0, STR_PAD_LEFT );
			?>			
				<option value="<?=$month_end_num?>" <?= ($search_end_month == $month_end_num) ? "selected" : "" ?>><?=$month_end_num?></option>			
			<?}?>
			</select>월
			
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
		<input type="button" class="btn_03" value="일별 자료 Excel 다운로드" onclick="window.location.href='facebook_payment_tax_excel.php?start_createdate=<?= $search_start_createdate ?>&end_createdate=<?= $search_end_createdate ?>'">				
	</div>
	
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="100">
				<col width="200">
				<col width="">
				<col width="">
				<col width="">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">구분</th>					
					<th class="tdl">총 결제 금액</th>
					<th class="tdl">Fx Rate 평균</th>
					<th class="tdl">총 세금(국가 화폐)</th>					
				</tr>
			</thead>
			<tbody>
			
<?
			for($i=0; $i<sizeof($facebook_tax_data); $i++)
			{
				$date_month = $facebook_tax_data[$i]["date_month"];
				$status = $facebook_tax_data[$i]["status"];
				
				if($status == 1)
					$status_str = "결제";
				else
					$status_str = "취소";
				
				$fx_rate = $facebook_tax_data[$i]["fx_rate"];
				$currency_amount = $facebook_tax_data[$i]["currency_amount"];
				$commission = $facebook_tax_data[$i]["commission"];
				$tax_amount = $facebook_tax_data[$i]["tax_amount"];
?>
			<tr>
<? 
				if($i == 0 || $i % 2 == 0)
				{
?>
				<td class="tdc point_title" rowspan="2"><?= $date_month ?></td>
<?
				}	
?>
				<td class="tdc point"><?= $status_str?></td>				
				<td class="tdl point">$<?= number_format($currency_amount, 2)?></td>
				<td class="tdl point"><?= number_format($fx_rate, 4)?></td>
				<td class="tdl point">$<?= number_format($tax_amount, 2)?></td>				
			</tr>
<?
			}
?>
			</tbody>
		</table>
 		</div>
        
     </form>
	
</div>

<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>