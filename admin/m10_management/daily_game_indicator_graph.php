<?php
	$top_menu = "management";
	$sub_menu = "daily_game_indicator_graph";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
	$pagename = "daily_game_indicator_graph.php";
	
	$today = date("Y-m-d");
	$before_day = get_past_date($today,28,"d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db_analysis = new CDatabase_Analysis();

	//1.신규회원 가입 수
	$sql = "SELECT writedate, ".
			"SUM(IF(typeid=10, today, 0)) AS total_today, ".
			"SUM(IF(typeid=11, today, 0)) AS facebook_today, ".
			"SUM(IF(typeid=12, today, 0)) AS apple_today, ".
			"SUM(IF(typeid=13, today, 0)) AS google_today, ". 
			"SUM(IF(typeid=14, today, 0)) AS amazon_today ".
			"FROM game_indicator_daily ".
			"WHERE typeid IN (10,11,12,13,14) AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate;";
	$indicator_todayjoin_data = $db_analysis->gettotallist($sql);
	
	//2.활동 유저 수
	$sql = "SELECT writedate, ".
			"SUM(IF(typeid=20, today, 0)) AS total_today, ".
			"SUM(IF(typeid=21, today, 0)) AS facebook_today, ".
			"SUM(IF(typeid=22, today, 0)) AS apple_today, ".
			"SUM(IF(typeid=23, today, 0)) AS google_today, ". 
			"SUM(IF(typeid=24, today, 0)) AS amazon_today ".
			"FROM game_indicator_daily ".
			"WHERE typeid IN (20,21,22,23,24) AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate;";
	$indicator_todayactive_data = $db_analysis->gettotallist($sql);
	
	//3.결제 유저 수
	$sql = "SELECT writedate, ".
			"SUM(IF(typeid=30, today, 0)) AS total_today, ".
			"SUM(IF(typeid=31, today, 0)) AS facebook_today, ".
			"SUM(IF(typeid=32, today, 0)) AS apple_today, ".
			"SUM(IF(typeid=33, today, 0)) AS google_today, ". 
			"SUM(IF(typeid=34, today, 0)) AS amazon_today ".
			"FROM game_indicator_daily ".
			"WHERE typeid IN (30,31,32,33,34) AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate;";
	$indicator_todaypay_data = $db_analysis->gettotallist($sql);
	
	//4.결제 금액
	$sql = "SELECT writedate, ".
			"SUM(IF(typeid=40, today, 0)) AS total_today, ".
			"SUM(IF(typeid=41, today, 0)) AS facebook_today, ".
			"SUM(IF(typeid=42, today, 0)) AS apple_today, ".
			"SUM(IF(typeid=43, today, 0)) AS google_today, ". 
			"SUM(IF(typeid=44, today, 0)) AS amazon_today, ".
			"SUM(IF(typeid=45, today, 0)) AS trialpay_today ".
			"FROM game_indicator_daily ".
			"WHERE typeid IN (40,41,42,43,44,45) AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate;";
	$indicator_todaypay_amount_data = $db_analysis->gettotallist($sql);	
	
	//5.ARPU
	$sql = "SELECT writedate, ".
			"SUM(IF(typeid=50, today, 0)) AS total_today, ".
			"SUM(IF(typeid=51, today, 0)) AS facebook_today, ".
			"SUM(IF(typeid=52, today, 0)) AS apple_today, ".
			"SUM(IF(typeid=53, today, 0)) AS google_today, ". 
			"SUM(IF(typeid=54, today, 0)) AS amazon_today ".
			"FROM game_indicator_daily ".
			"WHERE typeid IN (50,51,52,53,54) AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate;";
	$indicator_arpu_data = $db_analysis->gettotallist($sql);
	
	//6.ARPPU
	$sql = "SELECT writedate, ".
			"SUM(IF(typeid=60, today, 0)) AS total_today, ".
			"SUM(IF(typeid=61, today, 0)) AS facebook_today, ".
			"SUM(IF(typeid=62, today, 0)) AS apple_today, ".
			"SUM(IF(typeid=63, today, 0)) AS google_today, ". 
			"SUM(IF(typeid=64, today, 0)) AS amazon_today ".
			"FROM game_indicator_daily ".
			"WHERE typeid IN (60,61,62,63,64) AND writedate BETWEEN '$startdate' AND '$enddate' GROUP BY writedate;";
	$indicator_arppu_data = $db_analysis->gettotallist($sql);
	
	
	$date_todayjoin_list = array();
	$date_todayactive_list = array();
	$date_todaypay_list = array();
	$date_todaypay_amount_list = array();
	$date_arpu_list = array();
	$date_arppu_list = array();		
	
	$todayjoin_total_today_list = array();
	$todayjoin_facebook_today_list = array();
	$todayjoin_apple_today_list = array();
	$todayjoin_google_today_list = array();
	$todayjoin_amazon_today_list = array();
	
	$todayactive_total_today_list = array();
	$todayactive_facebook_today_list = array();
	$todayactive_apple_today_list = array();
	$todayactive_google_today_list = array();
	$todayactive_amazon_today_list = array();	
	
	$todaypay_total_today_list = array();
	$todaypay_facebook_today_list = array();
	$todaypay_apple_today_list = array();
	$todaypay_google_today_list = array();
	$todaypay_amazon_today_list = array();
		
	$todaypay_total_amount_today_list = array();
	$todaypay_facebook_amount_today_list = array();
	$todaypay_apple_amount_today_list = array();
	$todaypay_google_amount_today_list = array();
	$todaypay_amazon_amount_today_list = array();
	$todaypay_trialpay_amount_today_list = array();
	
	$arpu_total_today_list = array();
	$arpu_facebook_today_list = array();
	$arpu_apple_today_list = array();
	$arpu_google_today_list = array();
	$arpu_amazon_today_list = array();
	
	
	$arppu_total_today_list = array();
	$arppu_facebook_today_list = array();
	$arppu_apple_today_list = array();
	$arppu_google_today_list = array();
	$arppu_amazon_today_list = array();
	
	
	$todayjoin_list_pointer = sizeof($indicator_todayjoin_data);
	$todayactive_list_pointer = sizeof($indicator_todayactive_data);
	$todaypay_list_pointer = sizeof($indicator_todaypay_data);
	$todaypay_amount_list_pointer = sizeof($indicator_todaypay_amount_data);
	$arpu_list_pointer = sizeof($indicator_arpu_data);
	$arppu_list_pointer = sizeof($indicator_arppu_data);	
	
	$date_pointer = $enddate;	
	$loop_count = get_diff_date($enddate, $startdate, "d") + 1;

	//1.신규회원 가입 수
	for ($i=0; $i < $loop_count; $i++)
	{
		if ($todayjoin_list_pointer > 0)
			$today = $indicator_todayjoin_data[$todayjoin_list_pointer-1]["writedate"];
	
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$todayjoin_list = $indicator_todayjoin_data[$todayjoin_list_pointer-1];
		
			$date_todayjoin_list[$i] = $date_pointer;
			$todayjoin_total_today_list[$i] = $todayjoin_list["total_today"];
			$todayjoin_facebook_today_list[$i] = $todayjoin_list["facebook_today"];
			$todayjoin_apple_today_list[$i] = $todayjoin_list["apple_today"];
			$todayjoin_google_today_list[$i] = $todayjoin_list["google_today"];
			$todayjoin_amazon_today_list[$i] = $todayjoin_list["amazon_today"];
		
			$todayjoin_list_pointer--;
		}
		else
		{
			$date_todayjoin_list[$i] = $date_pointer;
			$todayjoin_total_today_list[$i] = 0;
			$todayjoin_facebook_today_list[$i] = 0;
			$todayjoin_apple_today_list[$i] = 0;
			$todayjoin_google_today_list[$i] = 0;
			$todayjoin_amazon_today_list[$i] = 0;
		}
	
			$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	$date_pointer = $enddate;
	
	//2.활동 유저 수
	for ($i=0; $i < $loop_count; $i++)
	{
		if ($todayactive_list_pointer > 0)
			$today = $indicator_todayactive_data[$todayactive_list_pointer-1]["writedate"];
	
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$todayactive_list = $indicator_todayactive_data[$todayactive_list_pointer-1];
		
			$date_todayactive_list[$i] = $date_pointer;
			$todayactive_total_today_list[$i] = $todayactive_list["total_today"];
			$todayactive_facebook_today_list[$i] = $todayactive_list["facebook_today"];
			$todayactive_apple_today_list[$i] = $todayactive_list["apple_today"];
			$todayactive_google_today_list[$i] = $todayactive_list["google_today"];
			$todayactive_amazon_today_list[$i] = $todayactive_list["amazon_today"];
		
			$todayactive_list_pointer--;
		}
		else
		{
			$date_todayactive_list[$i] = $date_pointer;
			$todayactive_total_today_list[$i] = 0;
			$todayactive_facebook_today_list[$i] = 0;
			$todayactive_apple_today_list[$i] = 0;
			$todayactive_google_today_list[$i] = 0;
			$todayactive_amazon_today_list[$i] = 0;	
		}
	
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	$date_pointer = $enddate;
	
	//3.결제 유저 수
	for ($i=0; $i < $loop_count; $i++)
	{
		if ($todaypay_list_pointer > 0)
			$today = $indicator_todaypay_data[$todaypay_list_pointer-1]["writedate"];
	
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$todaypay_list = $indicator_todaypay_data[$todaypay_list_pointer-1];
		
			$date_todaypay_list[$i] = $date_pointer;
			$todaypay_total_today_list[$i] = $todaypay_list["total_today"];
			$todaypay_facebook_today_list[$i] = $todaypay_list["facebook_today"];
			$todaypay_apple_today_list[$i] = $todaypay_list["apple_today"];
			$todaypay_google_today_list[$i] = $todaypay_list["google_today"];
			$todaypay_amazon_today_list[$i] = $todaypay_list["amazon_today"];
		
			$todaypay_list_pointer--;
		}
		else
		{
			$date_todaypay_list[$i] = $date_pointer;
			$todaypay_total_today_list[$i] = 0;
			$todaypay_facebook_today_list[$i] = 0;
			$todaypay_apple_today_list[$i] = 0;
			$todaypay_google_today_list[$i] = 0;
			$todaypay_amazon_today_list[$i] = 0;
		}
	
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	$date_pointer = $enddate;
	
	//4.결제 금액
	for ($i=0; $i < $loop_count; $i++)
	{
		if ($todaypay_amount_list_pointer > 0)
			$today = $indicator_todaypay_amount_data[$todaypay_amount_list_pointer-1]["writedate"];
	
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$todaypay_amount_list = $indicator_todaypay_amount_data[$todaypay_amount_list_pointer-1];
		
			$sub_point_count = 1;
			
			$date_todaypay_amount_list[$i] = $date_pointer;			
			
			$todaypay_total_amount_today_list[$i] = round($todaypay_amount_list["total_today"]/10, $sub_point_count);
			$todaypay_facebook_amount_today_list[$i] = round($todaypay_amount_list["facebook_today"]/10, $sub_point_count);
			$todaypay_apple_amount_today_list[$i] = round($todaypay_amount_list["apple_today"]/10, $sub_point_count);
			$todaypay_google_amount_today_list[$i] = round($todaypay_amount_list["google_today"]/10, $sub_point_count);
			$todaypay_amazon_amount_today_list[$i] = round($todaypay_amount_list["amazon_today"]/10, $sub_point_count);
			$todaypay_trialpay_amount_today_list[$i] = round($todaypay_amount_list["trialpay_today"]/10, $sub_point_count);
				
			$todaypay_amount_list_pointer--;
		}
		else
		{
			$date_todaypay_amount_list[$i] = $date_pointer;
			$todaypay_total_amount_today_list[$i] = 0;
			$todaypay_facebook_amount_today_list[$i] = 0;
			$todaypay_apple_amount_today_list[$i] = 0;
			$todaypay_google_amount_today_list[$i] = 0;
			$todaypay_amazon_amount_today_list[$i] = 0;
			$todaypay_trialpay_amount_today_list[$i] = 0;
		}
	
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	$date_pointer = $enddate;
	
	//5.ARPU
	for ($i=0; $i < $loop_count; $i++)
	{
		if ($arpu_list_pointer > 0)
			$today = $indicator_arpu_data[$arpu_list_pointer-1]["writedate"];
	
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$todayarpu_list = $indicator_arpu_data[$arpu_list_pointer-1];
	
			$sub_point_count = 3;
			
			$date_arpu_list[$i] = $date_pointer;				
			$arpu_total_today_list[$i] = round($todayarpu_list["total_today"]/1000, $sub_point_count);
			$arpu_facebook_today_list[$i] = round($todayarpu_list["facebook_today"]/1000, $sub_point_count);
			$arpu_apple_today_list[$i] = round($todayarpu_list["apple_today"]/1000, $sub_point_count);
			$arpu_google_today_list[$i] = round($todayarpu_list["google_today"]/1000, $sub_point_count);
			$arpu_amazon_today_list[$i] = round($todayarpu_list["amazon_today"]/1000, $sub_point_count);
			
	
			$arpu_list_pointer--;
		}
		else
		{
			$date_arpu_list[$i] = $date_pointer;
			$arpu_total_today_list[$i] = 0;
			$arpu_facebook_today_list[$i] = 0;
			$arpu_apple_today_list[$i] = 0;
			$arpu_google_today_list[$i] = 0;
			$arpu_amazon_today_list[$i] = 0;
		}
	
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	$date_pointer = $enddate;
	
	//6. ARPPU
	for ($i=0; $i < $loop_count; $i++)
	{
		if ($arppu_list_pointer > 0)
			$today = $indicator_arppu_data[$arppu_list_pointer-1]["writedate"];
	
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$todayarppu_list = $indicator_arppu_data[$arppu_list_pointer-1];
	
			$sub_point_count = 3;
			
			$date_arppu_list[$i] = $date_pointer;
			$arppu_total_today_list[$i] = round($todayarppu_list["total_today"]/1000, $sub_point_count);
			$arppu_facebook_today_list[$i] = round($todayarppu_list["facebook_today"]/1000, $sub_point_count);
			$arppu_apple_today_list[$i] = round($todayarppu_list["apple_today"]/1000, $sub_point_count);
			$arppu_google_today_list[$i] = round($todayarppu_list["google_today"]/1000, $sub_point_count);
			$arppu_amazon_today_list[$i] = round($todayarppu_list["amazon_today"]/1000, $sub_point_count);
	
			$arppu_list_pointer--;
		}
		else
		{
			$date_arppu_list[$i] = $date_pointer;
			$arppu_total_today_list[$i] = 0;
			$arppu_facebook_today_list[$i] = 0;
			$arppu_apple_today_list[$i] = 0;
			$arppu_google_today_list[$i] = 0;
			$arppu_amazon_today_list[$i] = 0;
		}
	
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}

	$db_analysis->end();
?>
	<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
	<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
	<script type="text/javascript">
		google.load("visualization", "1", {packages:["corechart"]});

		function drawChart() 
		{
			//1. 신규 회원
			var data_today_join = new google.visualization.DataTable();

			data_today_join.addColumn('string', '날짜');
			data_today_join.addColumn('number', '전체');
			data_today_join.addColumn('number', 'Facebook');
			data_today_join.addColumn('number', 'Apple');
			data_today_join.addColumn('number', 'Google');
			data_today_join.addColumn('number', 'Amazon');
			data_today_join.addRows([
<?
				for ($i=sizeof($todayjoin_total_today_list); $i>0; $i--)
				{
					$date = $date_todayjoin_list[$i-1];
					$todayjoin_total_today_count = $todayjoin_total_today_list[$i-1];
					$todayjoin_facebook_today_count = $todayjoin_facebook_today_list[$i-1];
					$todayjoin_apple_today_count = $todayjoin_apple_today_list[$i-1];
					$todayjoin_google_today_count = $todayjoin_google_today_list[$i-1];
					$todayjoin_amazon_today_count = $todayjoin_amazon_today_list[$i-1];					
					
					echo("['".$date."'");
					
					if ($todayjoin_total_today_count != "")
						echo(",{v:".$todayjoin_total_today_count.",f:'".number_format($todayjoin_total_today_count,1)."'}");
					else
						echo(",0");
					
					if ($todayjoin_facebook_today_count != "")
						echo(",{v:".$todayjoin_facebook_today_count.",f:'".number_format($todayjoin_facebook_today_count,1)."'}");
					else
						echo(",0");
					
					if ($todayjoin_apple_today_count != "")
						echo(",{v:".$todayjoin_apple_today_count.",f:'".number_format($todayjoin_apple_today_count,1)."'}");
					else
						echo(",0");
					
					if ($todayjoin_google_today_count != "")
						echo(",{v:".$todayjoin_google_today_count.",f:'".number_format($todayjoin_google_today_count,1)."'}");
					else
						echo(",0");
					
					if ($todayjoin_amazon_today_count != "")
						echo(",{v:".$todayjoin_amazon_today_count.",f:'".number_format($todayjoin_amazon_today_count,1)."'}]");
					else
						echo(",0]");
					
					if ($i > 1)
						echo(",");
				}			
?>
			]);

		    //2. 활동유전 		
			var data_today_ative = new google.visualization.DataTable();
				
			data_today_ative.addColumn('string', '날짜');
			data_today_ative.addColumn('number', '전체');
			data_today_ative.addColumn('number', 'Facebook');
			data_today_ative.addColumn('number', 'Apple');
			data_today_ative.addColumn('number', 'Google');
			data_today_ative.addColumn('number', 'Amazon');
			data_today_ative.addRows([
<?
				for ($i=sizeof($todayactive_total_today_list); $i>0; $i--)
				{
					$date = $date_todayactive_list[$i-1];
					$todayactive_total_today_count = $todayactive_total_today_list[$i-1];
					$todayactive_facebook_today_count = $todayactive_facebook_today_list[$i-1];
					$todayactive_apple_today_count = $todayactive_apple_today_list[$i-1];
					$todayactive_google_today_count = $todayactive_google_today_list[$i-1];
					$todayactive_amazon_today_count = $todayactive_amazon_today_list[$i-1];
					
					echo("['".$date."'");
					
					if ($todayactive_total_today_count != "")
						echo(",{v:".$todayactive_total_today_count.",f:'".number_format($todayactive_total_today_count,1)."'}");
					else
						echo(",0");
					
					if ($todayactive_facebook_today_count != "")
						echo(",{v:".$todayactive_facebook_today_count.",f:'".number_format($todayactive_facebook_today_count,1)."'}");
					else
						echo(",0");
					
					if ($todayactive_apple_today_count != "")
						echo(",{v:".$todayactive_apple_today_count.",f:'".number_format($todayactive_apple_today_count,1)."'}");
					else
						echo(",0");
					
					if ($todayactive_google_today_count != "")
						echo(",{v:".$todayactive_google_today_count.",f:'".number_format($todayactive_google_today_count,1)."'}");
					else
						echo(",0");
					
					if ($todayactive_amazon_today_count != "")
						echo(",{v:".$todayactive_amazon_today_count.",f:'".number_format($todayactive_amazon_today_count,1)."'}]");
					else
						echo(",0]");
							
					if ($i > 1)
						echo(",");
				}
?>

			]);

			//3. 결제 유저 		
			var data_today_pay = new google.visualization.DataTable();
				
			data_today_pay.addColumn('string', '날짜');
			data_today_pay.addColumn('number', '전체');
			data_today_pay.addColumn('number', 'Facebook');
			data_today_pay.addColumn('number', 'Apple');
			data_today_pay.addColumn('number', 'Google');
			data_today_pay.addColumn('number', 'Amazon');
			data_today_pay.addRows([
<?
				for ($i=sizeof($todaypay_total_today_list); $i>0; $i--)
				{
					$date = $date_todaypay_list[$i-1];
					$todaypay_total_today_count = $todaypay_total_today_list[$i-1];
					$todaypay_facebook_today_count = $todaypay_facebook_today_list[$i-1];
					$todaypay_apple_today_count = $todaypay_apple_today_list[$i-1];
					$todaypay_google_today_count = $todaypay_google_today_list[$i-1];
					$todaypay_amazon_today_count = $todaypay_amazon_today_list[$i-1];					
					
					echo("['".$date."'");
					
					if ($todaypay_total_today_count != "")
						echo(",{v:".$todaypay_total_today_count.",f:'".number_format($todaypay_total_today_count,1)."'}");
					else
						echo(",0");
					
					if ($todaypay_facebook_today_count != "")
						echo(",{v:".$todaypay_facebook_today_count.",f:'".number_format($todaypay_facebook_today_count,1)."'}");
					else
						echo(",0");
					
					if ($todaypay_apple_today_count != "")
						echo(",{v:".$todaypay_apple_today_count.",f:'".number_format($todaypay_apple_today_count,1)."'}");
					else
						echo(",0");
					
					if ($todaypay_google_today_count != "")
						echo(",{v:".$todaypay_google_today_count.",f:'".number_format($todaypay_google_today_count,1)."'}");
					else
						echo(",0");
					
					if ($todaypay_amazon_today_count != "")
						echo(",{v:".$todaypay_amazon_today_count.",f:'".number_format($todaypay_amazon_today_count,1)."'}]");
					else
						echo(",0]");
							
					if ($i > 1)
						echo(",");
				}
?>
			]);

			//4. 결제 금액		
			var data_today_pay_amount = new google.visualization.DataTable();
				
			data_today_pay_amount.addColumn('string', '날짜');
			data_today_pay_amount.addColumn('number', '전체');
			data_today_pay_amount.addColumn('number', 'Facebook');
			data_today_pay_amount.addColumn('number', 'Apple');
			data_today_pay_amount.addColumn('number', 'Google');
			data_today_pay_amount.addColumn('number', 'Amazon');
			data_today_pay_amount.addColumn('number', 'Trialpay');
			data_today_pay_amount.addRows([
<?
				
				for ($i=sizeof($todaypay_total_amount_today_list); $i>0; $i--)
				{
					
					$date = $date_todaypay_amount_list[$i-1];					
								
					$todaypay_total_amount_count = $todaypay_total_amount_today_list[$i-1];					
					$todaypay_facebook_amount_count = $todaypay_facebook_amount_today_list[$i-1];
					$todaypay_apple_amount_count = $todaypay_apple_amount_today_list[$i-1];
					$todaypay_google_amount_count = $todaypay_google_amount_today_list[$i-1];
					$todaypay_amazon_amount_count = $todaypay_amazon_amount_today_list[$i-1];					
					$todaypay_trialpay_amount_count = $todaypay_trialpay_amount_today_list[$i-1];
					
					echo("['".$date."'");
					
					if ($todaypay_total_amount_count != "")
						echo(",{v:".$todaypay_total_amount_count.",f:'".number_format($todaypay_total_amount_count,1)."'}");
					else
						echo(",0");
		
					if ($todaypay_facebook_amount_count != "")
						echo(",{v:".$todaypay_facebook_amount_count.",f:'".number_format($todaypay_facebook_amount_count,1)."'}");
					else
						echo(",0");
					
					if ($todaypay_apple_amount_count != "")
						echo(",{v:".$todaypay_apple_amount_count.",f:'".number_format($todaypay_apple_amount_count,1)."'}");
					else
						echo(",0");
					
					if ($todaypay_google_amount_count != "")
						echo(",{v:".$todaypay_google_amount_count.",f:'".number_format($todaypay_google_amount_count,1)."'}");
					else
						echo(",0");
					
					if ($todaypay_amazon_amount_count != "")
						echo(",{v:".$todaypay_amazon_amount_count.",f:'".number_format($todaypay_amazon_amount_count,1)."'}");
					else
						echo(",0");
		
					if ($todaypay_trialpay_amount_count != "")
						echo(",{v:".$todaypay_trialpay_amount_count.",f:'".number_format($todaypay_trialpay_amount_count,1)."'}]");
					else
						echo(",0]");

					if ($i > 1)
						echo(",");
				}

?>

			]);

			//5. ARPU 		
			var data_today_arpu = new google.visualization.DataTable();
				
			data_today_arpu.addColumn('string', '날짜');
			data_today_arpu.addColumn('number', '전체');
			data_today_arpu.addColumn('number', 'Facebook');
			data_today_arpu.addColumn('number', 'Apple');
			data_today_arpu.addColumn('number', 'Google');
			data_today_arpu.addColumn('number', 'Amazon');
			data_today_arpu.addRows([
<?
				for ($i=sizeof($arpu_total_today_list); $i>0; $i--)
				{
					$date = $date_arpu_list[$i-1];
					$todayarpu_total_today_count = $arpu_total_today_list[$i-1];
					$todayarpu_facebook_today_count = $arpu_facebook_today_list[$i-1];
					$todayarpu_apple_today_count = $arpu_apple_today_list[$i-1];
					$todayarpu_google_today_count = $arpu_google_today_list[$i-1];
					$todayarpu_amazon_today_count = $arpu_amazon_today_list[$i-1];
					
					echo("['".$date."'");
					
					if ($todayarpu_total_today_count != "")
						echo(",{v:".$todayarpu_total_today_count.",f:'".$todayarpu_total_today_count."'}");
					else
						echo(",0");
					
					if ($todayarpu_facebook_today_count != "")
						echo(",{v:".$todayarpu_facebook_today_count.",f:'".$todayarpu_facebook_today_count."'}");
					else
						echo(",0");
					
					if ($todayarpu_apple_today_count != "")
						echo(",{v:".$todayarpu_apple_today_count.",f:'".$todayarpu_apple_today_count."'}");
					else
						echo(",0");
					
					if ($todayarpu_google_today_count != "")
						echo(",{v:".$todayarpu_google_today_count.",f:'".$todayarpu_google_today_count."'}");
					else
						echo(",0");
					
					if ($todayarpu_amazon_today_count != "")
						echo(",{v:".$todayarpu_amazon_today_count.",f:'".$todayarpu_amazon_today_count."'}]");
					else
						echo(",0]");
							
					if ($i > 1)
						echo(",");
				}
?>

			]);

			//6. ARPPU 		
			var data_today_arppu = new google.visualization.DataTable();
				
			data_today_arppu.addColumn('string', '날짜');
			data_today_arppu.addColumn('number', '전체');
			data_today_arppu.addColumn('number', 'Facebook');
			data_today_arppu.addColumn('number', 'Apple');
			data_today_arppu.addColumn('number', 'Goole');
			data_today_arppu.addColumn('number', 'Amazon');
			data_today_arppu.addRows([
<?
				for ($i=sizeof($arppu_total_today_list); $i>0; $i--)
				{
					$date = $date_arppu_list[$i-1];
					$todayarppu_total_today_count = $arppu_total_today_list[$i-1];
					$todayarppu_facebook_today_count = $arppu_facebook_today_list[$i-1];
					$todayarppu_apple_today_count = $arppu_apple_today_list[$i-1];
					$todayarppu_google_today_count = $arppu_google_today_list[$i-1];
					$todayarppu_amazon_today_count = $arppu_amazon_today_list[$i-1];
					
					echo("['".$date."'");
					
					if ($todayarppu_total_today_count != "")
						echo(",{v:".$todayarppu_total_today_count.",f:'".$todayarppu_total_today_count."'}");
					else
						echo(",0");
					
					if ($todayarppu_facebook_today_count != "")
						echo(",{v:".$todayarppu_facebook_today_count.",f:'".$todayarppu_facebook_today_count."'}");
					else
						echo(",0");
					
					if ($todayarppu_apple_today_count != "")
						echo(",{v:".$todayarppu_apple_today_count.",f:'".$todayarppu_apple_today_count."'}");
					else
						echo(",0");
					
					if ($todayarppu_google_today_count != "")
						echo(",{v:".$todayarppu_google_today_count.",f:'".$todayarppu_google_today_count."'}");
					else
						echo(",0");
					
					if ($todayarppu_amazon_today_count != "")
						echo(",{v:".$todayarppu_amazon_today_count.",f:'".$todayarppu_amazon_today_count."'}]");
					else
						echo(",0]");
							
					if ($i > 1)
						echo(",");
				}
?>
			]);

			var options = {
	                title:'',                                                      
	                width:1050,                         
	                height:200,
	                axisTitlesPosition:'in',
	                curveType:'none',
	                focusTarget:'category',
	                interpolateNulls:'true',
	                legend:'top',
	                fontSize : 12,
	                chartArea:{left:80,top:40,width:1020,height:130}
	        };

			var chart = new google.visualization.LineChart(document.getElementById('chart_divTodayJoin'));
	        chart.draw(data_today_join, options);

	        var chart = new google.visualization.LineChart(document.getElementById('chart_divTodayActive'));
	        chart.draw(data_today_ative, options);	

	        var chart = new google.visualization.LineChart(document.getElementById('chart_divTodayPay'));
	        chart.draw(data_today_pay, options);	

	       var chart = new google.visualization.LineChart(document.getElementById('chart_divTodayPayAmount'));
	       chart.draw(data_today_pay_amount, options);	

	        var chart = new google.visualization.LineChart(document.getElementById('chart_divTodayArpu'));
	        chart.draw(data_today_arpu, options);	

	        var chart = new google.visualization.LineChart(document.getElementById('chart_divTodayArppu'));
	        chart.draw(data_today_arppu, options);				
		}

		google.setOnLoadCallback(drawChart);
		
		function search_press(e)
		{
		    if (((e.which) ? e.which : e.keyCode) == 13)
		    {
		        search();
		    }
		}
		
		function search()
		{
		    var search_form = document.search_form;
		
		    search_form.submit();
		}
		
		$(function() {
		    $("#startdate").datepicker({ });
		});
		
		$(function() {
		    $("#enddate").datepicker({ });
		});
	</script>
	
		<!-- CONTENTS WRAP -->
	<div class="contents_wrap">
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 일일 게임 지표 그래프</div>
			<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
				<div class="search_box">
					<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
					<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
					<input type="button" class="btn_search" value="검색" onclick="search()" />
				</div>
			</form>
		</div>
		<!-- //title_warp -->
		
		<div class="search_result">
			<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다
		</div>
		
		<div class="h2_title">[신규회원 가입수]</div>
		<div id="chart_divTodayJoin" style="height:230px; min-width: 500px"></div>
		
		<div class="h2_title">[활동 유저수]</div>
		<div id="chart_divTodayActive" style="height:230px; min-width: 500px"></div>	
		
		<div class="h2_title">[결제 유저수]</div>
		<div id="chart_divTodayPay" style="height:230px; min-width: 500px"></div>	
		
		<div class="h2_title">[결제 금액]</div>
		<div id="chart_divTodayPayAmount" style="height:230px; min-width: 500px"></div>	
		
		<div class="h2_title">[ARPU]</div>
		<div id="chart_divTodayArpu" style="height:230px; min-width: 500px"></div>	
		
		<div class="h2_title">[ARPPU]</div>
		<div id="chart_divTodayArppu" style="height:230px; min-width: 500px"></div>			
	</div>
	<!--  //CONTENTS WRAP -->
	        
	<div class="clear"></div>
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>