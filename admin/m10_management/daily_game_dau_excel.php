<?
	include("../common/common_include.inc.php");
	
	$today = date("Y-m-d", strtotime("-1 day"));
	
	$search_start_searchdate = $_GET["start_searchdate"];
	$search_end_searchdate = $_GET["end_searchdate"];
	
	if($search_start_searchdate == "")
		$search_start_searchdate = date("Y-m-d", strtotime("-15 day"));

	if($search_end_searchdate == "")
		$search_end_searchdate = $today;

	header("Pragma: public");
	header("Expires: 0");
	header("Content-type: application/vnd.ms-excel");
	header( "Content-Disposition: attachment; filename=DAU_$search_start_searchdate"._."$search_end_searchdate.xls" );
	header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
	header("Content-Description: PHP5 Generated Data");

	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT LEFT(today,10) AS today, todayactivecount, todayfacebookactivecount, todayiosactivecount, todayandroidactivecount, todayamazonactivecount ".
			"FROM user_join_log WHERE today >= '$search_start_searchdate' AND today <= '$search_end_searchdate' GROUP BY LEFT(today, 10) ORDER BY today ASC ";

	$dau_list = $db_analysis->gettotallist($sql);

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<div id="tab_content_1">
		<table class="tbl_list_basic1" border="1">
			<colgroup>
				<col width="100">
				<col width="150">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">전체활동회원수</th>
					<th class="tdc">Facebook 활동회원수</th>
					<th class="tdc">iOS 활동회원수</th>
					<th class="tdc">Android 활동회원수</th>
					<th class="tdc">Amazon 활동회원수</th>
				</tr>
			</thead>
			<tbody>
<?
			for($i=0; $i<sizeof($dau_list); $i++)
			{
				$today = $dau_list[$i]["today"];
				$todayactivecount = $dau_list[$i]["todayactivecount"];
				$todayfacebookactivecount = $dau_list[$i]["todayfacebookactivecount"];				
				$todaymobileactivecount = $dau_list[$i]["todayiosactivecount"];
				$todayandroidactivecount = $dau_list[$i]["todayandroidactivecount"];
				$todayamazonactivecount = $dau_list[$i]["todayamazonactivecount"];
?>
				<tr>
					<td class="tdc"><?= $today?></td>
					<td class="tdc"><?= number_format($todayactivecount) ?></td>
					<td class="tdc"><?= number_format($todayfacebookactivecount) ?></td>
					<td class="tdc"><?= number_format($todaymobileactivecount) ?></td>
					<td class="tdc"><?= number_format($todayandroidactivecount) ?></td>
					<td class="tdc"><?= number_format($todayamazonactivecount) ?></td>
				</tr>
<?
			}
?>
			</tbody>
		</table>
 		</div>
        
     </form>
	
</div>

<!--  //CONTENTS WRAP -->
<?
	$db_analysis->end();
?>