<?	
	include($_SERVER["DOCUMENT_ROOT"]."/common/common_include.inc.php");
	
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	
	$today = date("Y-m", strtotime("-1 month"));
	
	if($search_start_createdate == "")
		$search_start_createdate = date("Y-m", strtotime("-2 month"));
	
	if($search_end_createdate == "")
		$search_end_createdate = $today;
	
	$db_main = new CDatabase_Main();
	
	$sql = "SELECT writedate, status, (SELECT country FROM tbl_user_ext WHERE useridx = tt.useridx) AS country, currency, payout_foreign_exchange_rate AS fx_rate, (facebookcredit/10) AS money, currency_amount, tax_amount*payout_foreign_exchange_rate AS commission, tax_amount, tax_country
			FROM tbl_product_order tt WHERE  useridx > 20000 AND '$search_start_createdate-01 00:00:00' <= writedate AND writedate <= '$search_end_createdate-31 23:59:59' ORDER BY writedate ASC";	
	$facebook_tax_data = $db_main->gettotallist($sql);

	header("Pragma: public");
	header("Expires: 0");
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=facebook_payment_tax_$search_start_createdate"._."$search_end_createdate.xls");
	header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
	header("Content-Description: PHP5 Generated Data");
?>
 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<div id="tab_content_1">
		<table class="tbl_list_basic1" border="1">
			<colgroup>
				<col width="100">
				<col width="150">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">구분</th>
					<th class="tdc">국가</th>
					<th class="tdc">구매통화</th>
					<th class="tdc">Fx rate</th>
					<th class="tdc">결제액</th>
					<th class="tdc">세금</th>
					<th class="tdc">세금국가</th>					
				</tr>
			</thead>
<?
	for($i=0; $i<sizeof($facebook_tax_data); $i++)
	{
		$writedate = $facebook_tax_data[$i]["writedate"];
		$status = $facebook_tax_data[$i]["status"];
		$country = $facebook_tax_data[$i]["country"];
		$currency = $facebook_tax_data[$i]["currency"];
		$fx_rate = $facebook_tax_data[$i]["fx_rate"];
		$currency_amount = $facebook_tax_data[$i]["currency_amount"];
		$commission = $facebook_tax_data[$i]["commission"];
		$tax_amount = $facebook_tax_data[$i]["tax_amount"];
		$tax_country = $facebook_tax_data[$i]["tax_country"];
		
		if($status == 1)
			$status_str = "결제";
		else
			$status_str = "취소";
		
		
?>

				<tr>
					<td class="tdc point_title"><?= $writedate ?></td>
					<td class="tdc point"><?= $status_str?></td>
					<td class="tdc"><?= $country ?></td>
					<td class="tdc"><?= $currency ?></td>
					<td class="tdc"><?= $fx_rate ?></td>
					<td class="tdc"><?= $currency_amount ?></td>
					<td class="tdc"><?= $tax_amount ?></td>
					<td class="tdc"><?= $tax_country ?></td>			
				</tr>

<?
	}
?>
			</tbody>
		</table>
 		</div>
        
     </form>
	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main->end();
?>