<?
	$top_menu = "management";
	$sub_menu = "user_week_pay_stat";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
	
	$search_type = ($_GET["search_type"] == "") ? 0 : $_GET["search_type"];
	$search_data = ($_GET["search_data"] == "") ? 0 : $_GET["search_data"];
	$search_adflag = ($_GET["search_adflag"] == "") ? 0 : $_GET["search_adflag"];
	$search_view = ($_GET["search_view"] == "") ? 0 : $_GET["search_view"];
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today,120,"d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$pagename = "user_week_pay_stat.php";
	
	$db_otherdb = new CDatabase_Other();
	
	$stamp_sql = "";
	$createdate_sql = "";
	
	if($search_view == 0)
	{
		$stamp_sql = "(FLOOR(UNIX_TIMESTAMP(createdate)/86400) - (FLOOR(UNIX_TIMESTAMP(createdate)/86400) - FLOOR(UNIX_TIMESTAMP('$startdate')/86400)%7)%7)";
		$createdate_sql = "MIN(createdate)";
	}
	else if($search_view == 1)
	{
		$stamp_sql = "DATE_FORMAT(createdate, '%Y-%m')";
		$createdate_sql = "DATE_FORMAT(createdate, '%Y-%m')";
	}
	else if($search_view == 2)
	{
		$stamp_sql = "DATE_FORMAT(createdate, '%Y-%m-%d')";
		$createdate_sql = "DATE_FORMAT(createdate, '%Y-%m-%d')";
	}
	
	$sql = "SELECT adflag FROM tbl_user_week_pay_daily WHERE '$startdate' <= createdate AND createdate <= '$enddate' AND adflag NOT LIKE ('mobile%') GROUP BY adflag";
	$adflag_list = $db_otherdb->gettotallist($sql);
	
	if($search_adflag == "0")
	{
		//Web
		$sql = "SELECT $stamp_sql AS stamp, $createdate_sql AS createdate, SUM(join_count) AS join_count, ".
				"	SUM(CASE WHEN week_0 > 0 THEN join_count ELSE 0 END) AS week_0_reg_count, SUM(CASE WHEN week_1 > 0 THEN join_count ELSE 0 END) AS week_1_reg_count, SUM(CASE WHEN week_2 > 0 THEN join_count ELSE 0 END) AS week_2_reg_count, SUM(CASE WHEN week_3 > 0 THEN join_count ELSE 0 END) AS week_3_reg_count, SUM(CASE WHEN week_4 > 0 THEN join_count ELSE 0 END) AS week_4_reg_count, ".
				"	SUM(CASE WHEN week_5 > 0 THEN join_count ELSE 0 END) AS week_5_reg_count, SUM(CASE WHEN week_6 > 0 THEN join_count ELSE 0 END) AS week_6_reg_count, SUM(CASE WHEN week_7 > 0 THEN join_count ELSE 0 END) AS week_7_reg_count, ".
				"	SUM(CASE WHEN week_13 > 0 THEN join_count ELSE 0 END) AS week_13_reg_count, SUM(CASE WHEN week_26 > 0 THEN join_count ELSE 0 END) AS week_26_reg_count, SUM(CASE WHEN week_39 > 0 THEN join_count ELSE 0 END) AS week_39_reg_count, SUM(CASE WHEN week_51 > 0 THEN join_count ELSE 0 END) AS week_51_reg_count, ".
				"	FLOOR(DATEDIFF('$enddate', createdate)/7) AS std_week, ".
				"	SUM(week_0) AS week_0, SUM(week_1) AS week_1, SUM(week_2) AS week_2, SUM(week_3) AS week_3, SUM(week_4) AS week_4, SUM(week_5) AS week_5, SUM(week_6) AS week_6, SUM(week_7) AS week_7, SUM(week_8) AS week_8, SUM(week_9) AS week_9, ". 
				"	SUM(week_10) AS week_10, SUM(week_11) AS week_11, SUM(week_12) AS week_12, SUM(week_13) AS week_13, SUM(week_14) AS week_14, SUM(week_15) AS week_15, SUM(week_16) AS week_16, SUM(week_17) AS week_17, SUM(week_18) AS week_18, SUM(week_19) AS week_19, ".
				"	SUM(week_20) AS week_20, SUM(week_21) AS week_21, SUM(week_22) AS week_22, SUM(week_23) AS week_23, SUM(week_24) AS week_24, SUM(week_25) AS week_25, SUM(week_26) AS week_26, SUM(week_27) AS week_27, SUM(week_28) AS week_28, SUM(week_29) AS week_29, ".
				"	SUM(week_30) AS week_30, SUM(week_31) AS week_31, SUM(week_32) AS week_32, SUM(week_33) AS week_33, SUM(week_34) AS week_34, SUM(week_35) AS week_35, SUM(week_36) AS week_36, SUM(week_37) AS week_37, SUM(week_38) AS week_38, SUM(week_39) AS week_39, ".
				"	SUM(week_40) AS week_40, SUM(week_41) AS week_41, SUM(week_42) AS week_42, SUM(week_43) AS week_43, SUM(week_44) AS week_44, SUM(week_45) AS week_45, SUM(week_46) AS week_46, SUM(week_47) AS week_47, SUM(week_48) AS week_48, SUM(week_49) AS week_49, ".
				"	SUM(week_50) AS week_50, SUM(week_51) AS week_51 ".
				"FROM tbl_user_week_pay_daily ".
				"WHERE '$startdate' <= createdate AND createdate < '$enddate' AND category = 0 ".
				"GROUP BY stamp ".
				"ORDER BY stamp ASC, createdate ASC";
		

		$week_pay_list = array(array("name" => "Web", "data" => $db_otherdb->gettotallist($sql)));

		/*
		//IOS
		$sql = "SELECT $stamp_sql AS stamp, $createdate_sql AS createdate, SUM(join_count) AS join_count, ".
				"	SUM(CASE WHEN week_0 > 0 THEN join_count ELSE 0 END) AS week_0_reg_count, SUM(CASE WHEN week_1 > 0 THEN join_count ELSE 0 END) AS week_1_reg_count, SUM(CASE WHEN week_2 > 0 THEN join_count ELSE 0 END) AS week_2_reg_count, SUM(CASE WHEN week_3 > 0 THEN join_count ELSE 0 END) AS week_3_reg_count, SUM(CASE WHEN week_4 > 0 THEN join_count ELSE 0 END) AS week_4_reg_count, ".
				"	SUM(CASE WHEN week_5 > 0 THEN join_count ELSE 0 END) AS week_5_reg_count, SUM(CASE WHEN week_6 > 0 THEN join_count ELSE 0 END) AS week_6_reg_count, SUM(CASE WHEN week_7 > 0 THEN join_count ELSE 0 END) AS week_7_reg_count, ".
				"	SUM(CASE WHEN week_13 > 0 THEN join_count ELSE 0 END) AS week_13_reg_count, SUM(CASE WHEN week_26 > 0 THEN join_count ELSE 0 END) AS week_26_reg_count, SUM(CASE WHEN week_39 > 0 THEN join_count ELSE 0 END) AS week_39_reg_count, SUM(CASE WHEN week_51 > 0 THEN join_count ELSE 0 END) AS week_51_reg_count, ".
				"	FLOOR(DATEDIFF('$enddate', createdate)/7) AS std_week, ".
				"	SUM(week_0) AS week_0, SUM(week_1) AS week_1, SUM(week_2) AS week_2, SUM(week_3) AS week_3, SUM(week_4) AS week_4, SUM(week_5) AS week_5, SUM(week_6) AS week_6, SUM(week_7) AS week_7, SUM(week_8) AS week_8, SUM(week_9) AS week_9, ". 
				"	SUM(week_10) AS week_10, SUM(week_11) AS week_11, SUM(week_12) AS week_12, SUM(week_13) AS week_13, SUM(week_14) AS week_14, SUM(week_15) AS week_15, SUM(week_16) AS week_16, SUM(week_17) AS week_17, SUM(week_18) AS week_18, SUM(week_19) AS week_19, ".
				"	SUM(week_20) AS week_20, SUM(week_21) AS week_21, SUM(week_22) AS week_22, SUM(week_23) AS week_23, SUM(week_24) AS week_24, SUM(week_25) AS week_25, SUM(week_26) AS week_26, SUM(week_27) AS week_27, SUM(week_28) AS week_28, SUM(week_29) AS week_29, ".
				"	SUM(week_30) AS week_30, SUM(week_31) AS week_31, SUM(week_32) AS week_32, SUM(week_33) AS week_33, SUM(week_34) AS week_34, SUM(week_35) AS week_35, SUM(week_36) AS week_36, SUM(week_37) AS week_37, SUM(week_38) AS week_38, SUM(week_39) AS week_39, ".
				"	SUM(week_40) AS week_40, SUM(week_41) AS week_41, SUM(week_42) AS week_42, SUM(week_43) AS week_43, SUM(week_44) AS week_44, SUM(week_45) AS week_45, SUM(week_46) AS week_46, SUM(week_47) AS week_47, SUM(week_48) AS week_48, SUM(week_49) AS week_49, ".
				"	SUM(week_50) AS week_50, SUM(week_51) AS week_51 ".
				"FROM tbl_user_week_pay_daily ".
				"WHERE '$startdate' <= createdate AND createdate < '$enddate' AND category = 1 ".
				"GROUP BY stamp ".
				"ORDER BY stamp ASC, createdate ASC";
		
		$ios_list = array(array("name" => "IOS", "data" => $db_otherdb->gettotallist($sql)));
		
		$week_pay_list = array_merge($web_list, $ios_list);
		
		//Android
		$sql = "SELECT $stamp_sql AS stamp, $createdate_sql AS createdate, SUM(join_count) AS join_count, ".
				"	SUM(CASE WHEN week_0 > 0 THEN join_count ELSE 0 END) AS week_0_reg_count, SUM(CASE WHEN week_1 > 0 THEN join_count ELSE 0 END) AS week_1_reg_count, SUM(CASE WHEN week_2 > 0 THEN join_count ELSE 0 END) AS week_2_reg_count, SUM(CASE WHEN week_3 > 0 THEN join_count ELSE 0 END) AS week_3_reg_count, SUM(CASE WHEN week_4 > 0 THEN join_count ELSE 0 END) AS week_4_reg_count, ".
				"	SUM(CASE WHEN week_5 > 0 THEN join_count ELSE 0 END) AS week_5_reg_count, SUM(CASE WHEN week_6 > 0 THEN join_count ELSE 0 END) AS week_6_reg_count, SUM(CASE WHEN week_7 > 0 THEN join_count ELSE 0 END) AS week_7_reg_count, ".
				"	SUM(CASE WHEN week_13 > 0 THEN join_count ELSE 0 END) AS week_13_reg_count, SUM(CASE WHEN week_26 > 0 THEN join_count ELSE 0 END) AS week_26_reg_count, SUM(CASE WHEN week_39 > 0 THEN join_count ELSE 0 END) AS week_39_reg_count, SUM(CASE WHEN week_51 > 0 THEN join_count ELSE 0 END) AS week_51_reg_count, ".
				"	FLOOR(DATEDIFF('$enddate', createdate)/7) AS std_week, ".
				"	SUM(week_0) AS week_0, SUM(week_1) AS week_1, SUM(week_2) AS week_2, SUM(week_3) AS week_3, SUM(week_4) AS week_4, SUM(week_5) AS week_5, SUM(week_6) AS week_6, SUM(week_7) AS week_7, SUM(week_8) AS week_8, SUM(week_9) AS week_9, ". 
				"	SUM(week_10) AS week_10, SUM(week_11) AS week_11, SUM(week_12) AS week_12, SUM(week_13) AS week_13, SUM(week_14) AS week_14, SUM(week_15) AS week_15, SUM(week_16) AS week_16, SUM(week_17) AS week_17, SUM(week_18) AS week_18, SUM(week_19) AS week_19, ".
				"	SUM(week_20) AS week_20, SUM(week_21) AS week_21, SUM(week_22) AS week_22, SUM(week_23) AS week_23, SUM(week_24) AS week_24, SUM(week_25) AS week_25, SUM(week_26) AS week_26, SUM(week_27) AS week_27, SUM(week_28) AS week_28, SUM(week_29) AS week_29, ".
				"	SUM(week_30) AS week_30, SUM(week_31) AS week_31, SUM(week_32) AS week_32, SUM(week_33) AS week_33, SUM(week_34) AS week_34, SUM(week_35) AS week_35, SUM(week_36) AS week_36, SUM(week_37) AS week_37, SUM(week_38) AS week_38, SUM(week_39) AS week_39, ".
				"	SUM(week_40) AS week_40, SUM(week_41) AS week_41, SUM(week_42) AS week_42, SUM(week_43) AS week_43, SUM(week_44) AS week_44, SUM(week_45) AS week_45, SUM(week_46) AS week_46, SUM(week_47) AS week_47, SUM(week_48) AS week_48, SUM(week_49) AS week_49, ".
				"	SUM(week_50) AS week_50, SUM(week_51) AS week_51 ".
				"FROM tbl_user_week_pay_daily ".
				"WHERE '$startdate' <= createdate AND createdate < '$enddate' AND category = 2 ".
				"GROUP BY stamp ".
				"ORDER BY stamp ASC, createdate ASC";
		
		$and_list = array(array("name" => "Android", "data" => $db_otherdb->gettotallist($sql)));
		
		$week_pay_list = array_merge($week_pay_list, $and_list);
		
		//Amazon
		$sql = "SELECT $stamp_sql AS stamp, $createdate_sql AS createdate, SUM(join_count) AS join_count, ".
				"	SUM(CASE WHEN week_0 > 0 THEN join_count ELSE 0 END) AS week_0_reg_count, SUM(CASE WHEN week_1 > 0 THEN join_count ELSE 0 END) AS week_1_reg_count, SUM(CASE WHEN week_2 > 0 THEN join_count ELSE 0 END) AS week_2_reg_count, SUM(CASE WHEN week_3 > 0 THEN join_count ELSE 0 END) AS week_3_reg_count, SUM(CASE WHEN week_4 > 0 THEN join_count ELSE 0 END) AS week_4_reg_count, ".
				"	SUM(CASE WHEN week_5 > 0 THEN join_count ELSE 0 END) AS week_5_reg_count, SUM(CASE WHEN week_6 > 0 THEN join_count ELSE 0 END) AS week_6_reg_count, SUM(CASE WHEN week_7 > 0 THEN join_count ELSE 0 END) AS week_7_reg_count, ".
				"	SUM(CASE WHEN week_13 > 0 THEN join_count ELSE 0 END) AS week_13_reg_count, SUM(CASE WHEN week_26 > 0 THEN join_count ELSE 0 END) AS week_26_reg_count, SUM(CASE WHEN week_39 > 0 THEN join_count ELSE 0 END) AS week_39_reg_count, SUM(CASE WHEN week_51 > 0 THEN join_count ELSE 0 END) AS week_51_reg_count, ".
				"	FLOOR(DATEDIFF('$enddate', createdate)/7) AS std_week, ".
				"	SUM(week_0) AS week_0, SUM(week_1) AS week_1, SUM(week_2) AS week_2, SUM(week_3) AS week_3, SUM(week_4) AS week_4, SUM(week_5) AS week_5, SUM(week_6) AS week_6, SUM(week_7) AS week_7, SUM(week_8) AS week_8, SUM(week_9) AS week_9, ". 
				"	SUM(week_10) AS week_10, SUM(week_11) AS week_11, SUM(week_12) AS week_12, SUM(week_13) AS week_13, SUM(week_14) AS week_14, SUM(week_15) AS week_15, SUM(week_16) AS week_16, SUM(week_17) AS week_17, SUM(week_18) AS week_18, SUM(week_19) AS week_19, ".
				"	SUM(week_20) AS week_20, SUM(week_21) AS week_21, SUM(week_22) AS week_22, SUM(week_23) AS week_23, SUM(week_24) AS week_24, SUM(week_25) AS week_25, SUM(week_26) AS week_26, SUM(week_27) AS week_27, SUM(week_28) AS week_28, SUM(week_29) AS week_29, ".
				"	SUM(week_30) AS week_30, SUM(week_31) AS week_31, SUM(week_32) AS week_32, SUM(week_33) AS week_33, SUM(week_34) AS week_34, SUM(week_35) AS week_35, SUM(week_36) AS week_36, SUM(week_37) AS week_37, SUM(week_38) AS week_38, SUM(week_39) AS week_39, ".
				"	SUM(week_40) AS week_40, SUM(week_41) AS week_41, SUM(week_42) AS week_42, SUM(week_43) AS week_43, SUM(week_44) AS week_44, SUM(week_45) AS week_45, SUM(week_46) AS week_46, SUM(week_47) AS week_47, SUM(week_48) AS week_48, SUM(week_49) AS week_49, ".
				"	SUM(week_50) AS week_50, SUM(week_51) AS week_51 ".
				"FROM tbl_user_week_pay_daily ".
				"WHERE '$startdate' <= createdate AND createdate < '$enddate' AND category = 3 ".
				"GROUP BY stamp ".
				"ORDER BY stamp ASC, createdate ASC";
		
		$amazon_list = array(array("name" => "Amazon", "data" => $db_otherdb->gettotallist($sql)));
		
		$week_pay_list = array_merge($week_pay_list, $amazon_list);
		*/
	}
	else
	{
		$sql = "SELECT $stamp_sql AS stamp, $createdate_sql AS createdate, SUM(join_count) AS join_count, ".
				"	SUM(CASE WHEN week_0 > 0 THEN join_count ELSE 0 END) AS week_0_reg_count, SUM(CASE WHEN week_1 > 0 THEN join_count ELSE 0 END) AS week_1_reg_count, SUM(CASE WHEN week_2 > 0 THEN join_count ELSE 0 END) AS week_2_reg_count, SUM(CASE WHEN week_3 > 0 THEN join_count ELSE 0 END) AS week_3_reg_count, SUM(CASE WHEN week_4 > 0 THEN join_count ELSE 0 END) AS week_4_reg_count, ".
				"	SUM(CASE WHEN week_5 > 0 THEN join_count ELSE 0 END) AS week_5_reg_count, SUM(CASE WHEN week_6 > 0 THEN join_count ELSE 0 END) AS week_6_reg_count, SUM(CASE WHEN week_7 > 0 THEN join_count ELSE 0 END) AS week_7_reg_count, ".
				"	SUM(CASE WHEN week_13 > 0 THEN join_count ELSE 0 END) AS week_13_reg_count, SUM(CASE WHEN week_26 > 0 THEN join_count ELSE 0 END) AS week_26_reg_count, SUM(CASE WHEN week_39 > 0 THEN join_count ELSE 0 END) AS week_39_reg_count, SUM(CASE WHEN week_51 > 0 THEN join_count ELSE 0 END) AS week_51_reg_count, ".
				"	FLOOR(DATEDIFF('$enddate', createdate)/7) AS std_week, ".
				"	SUM(week_0) AS week_0, SUM(week_1) AS week_1, SUM(week_2) AS week_2, SUM(week_3) AS week_3, SUM(week_4) AS week_4, SUM(week_5) AS week_5, SUM(week_6) AS week_6, SUM(week_7) AS week_7, SUM(week_8) AS week_8, SUM(week_9) AS week_9, ". 
				"	SUM(week_10) AS week_10, SUM(week_11) AS week_11, SUM(week_12) AS week_12, SUM(week_13) AS week_13, SUM(week_14) AS week_14, SUM(week_15) AS week_15, SUM(week_16) AS week_16, SUM(week_17) AS week_17, SUM(week_18) AS week_18, SUM(week_19) AS week_19, ".
				"	SUM(week_20) AS week_20, SUM(week_21) AS week_21, SUM(week_22) AS week_22, SUM(week_23) AS week_23, SUM(week_24) AS week_24, SUM(week_25) AS week_25, SUM(week_26) AS week_26, SUM(week_27) AS week_27, SUM(week_28) AS week_28, SUM(week_29) AS week_29, ".
				"	SUM(week_30) AS week_30, SUM(week_31) AS week_31, SUM(week_32) AS week_32, SUM(week_33) AS week_33, SUM(week_34) AS week_34, SUM(week_35) AS week_35, SUM(week_36) AS week_36, SUM(week_37) AS week_37, SUM(week_38) AS week_38, SUM(week_39) AS week_39, ".
				"	SUM(week_40) AS week_40, SUM(week_41) AS week_41, SUM(week_42) AS week_42, SUM(week_43) AS week_43, SUM(week_44) AS week_44, SUM(week_45) AS week_45, SUM(week_46) AS week_46, SUM(week_47) AS week_47, SUM(week_48) AS week_48, SUM(week_49) AS week_49, ".
				"	SUM(week_50) AS week_50, SUM(week_51) AS week_51 ".
				"FROM tbl_user_week_pay_daily ".
				"WHERE '$startdate' <= createdate AND createdate < '$enddate' AND adflag = '".($search_adflag == "viral" ? "" : $search_adflag)."' ".
				"GROUP BY stamp ".
				"ORDER BY stamp ASC, createdate ASC";
		
		write_log($sql);

		$week_pay_list = array(array("name" => "$search_adflag", "data" => $db_otherdb->gettotallist($sql)));
	}
	
	$db_otherdb->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript">
	$(function() {
    	$("#startdate").datepicker({ });
	});

	$(function() {
    	$("#enddate").datepicker({ });
	});

	function search()
    {
        var search_form = document.search_form;
            
        if (search_form.startdate.value == "")
        {
            alert("기준일을 입력하세요.");
            search_form.startdate.focus();
            return;
        } 

        if (search_form.enddate.value == "")
        {
            alert("기준일을 입력하세요.");
            search_form.enddate.focus();
            return;
        } 

        search_form.submit();
    }
</script>
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 가입자 주별 결제 현황</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<span class="search_lbl">보기&nbsp;</span>
				<select name="search_type" id="search_type">
					<option value="0" <?= ($search_type == "0") ? "selected" : "" ?>>ARPI</option>
					<option value="1" <?= ($search_type == "1") ? "selected" : "" ?>>매출</option>
				</select>
				<span class="search_lbl ml20">데이터&nbsp;</span>
				<select name="search_data" id="search_data">
					<option value="0" <?= ($search_data == "0") ? "selected" : "" ?>>누적</option>
					<option value="1" <?= ($search_data == "1") ? "selected" : "" ?>>기간</option>
				</select>
				<span class="search_lbl ml20">구분&nbsp;</span>
				<select name="search_adflag" id="search_adflag">
					<option value="0" <?= ($search_adflag == "0") ? "selected" : "" ?>>전체</option>
<?
				for($i=0; $i<sizeof($adflag_list); $i++)
				{
					$adflag = $adflag_list[$i]["adflag"];
					
					$adflag = ($adflag == "") ? "viral" : $adflag;
?>
					<option value="<?= $adflag ?>" <?= ("$search_adflag" == "$adflag") ? "selected" : "" ?>><?= $adflag ?></option>
<?
				}
?>
				</select>
				<span class="search_lbl ml20">기간&nbsp;&nbsp;&nbsp;</span>
				<select name="search_view" id="search_view">
					<option value="2" <?= ($search_view == "2") ? "selected" : "" ?>>일</option>
					<option value="0" <?= ($search_view == "0") ? "selected" : "" ?>>주</option>
					<option value="1" <?= ($search_view == "1") ? "selected" : "" ?>>월</option>
				</select>
				<span class="search_lbl ml20">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" /> ~
				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $startdate ?> ~ <?= $enddate ?></span> 통계입니다
	</div>

<?
	for($i=0; $i<sizeof($week_pay_list); $i++)
	{
		if($i > 0)
		{
?>
		<br/><br/>
<?
		}
?>
		<div class="search_result"><?= $week_pay_list[$i]["name"]; ?></div>
		<table class="tbl_list_basic1" style="width: 1150px;">
		<colgroup>
			<col width="70">
			<col width="70">
			<col width="70">
			<col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
            <col width="70">
		</colgroup>
        <thead>
         	<th>가입일</th>
			<th>가입자수</th>
			<th>1주</th>
			<th>2주</th>
			<th>3주</th>
			<th>4주</th>
			<th>5주</th>
			<th>6주</th>
			<th>7주</th>
			<th>8주</th>
			<th>3개월</th>
			<th>6개월</th>
			<th>9개월</th>
			<th>12개월</th>
        </thead>
        <tbody>
<?
		$week_pay_detail = $week_pay_list[$i]["data"];
		
		$sum_join_count = 0;
		$sum_week_0 = 0;
		$sum_week_1 = 0;
		$sum_week_2 = 0;
		$sum_week_3 = 0;
		$sum_week_4 = 0;
		$sum_week_5 = 0;
		$sum_week_6 = 0;
		$sum_week_7 = 0;
		$sum_week_13 = 0;
		$sum_week_26 = 0;
		$sum_week_39 = 0;
		$sum_week_51 = 0;
		
		$sum_reg_count_week_0 = 0;
		$sum_reg_count_week_1 = 0;
		$sum_reg_count_week_2 = 0;
		$sum_reg_count_week_3 = 0;
		$sum_reg_count_week_4 = 0;
		$sum_reg_count_week_5 = 0;
		$sum_reg_count_week_6 = 0;
		$sum_reg_count_week_7 = 0;
		$sum_reg_count_week_13 = 0;
		$sum_reg_count_week_26 = 0;
		$sum_reg_count_week_39 = 0;
		$sum_reg_count_week_51 = 0;

		for($j=0; $j<sizeof($week_pay_detail); $j++)
		{
			$week_1 = 0;
			$week_2 = 0;
			$week_3 = 0;
			$week_4 = 0;
			$week_5 = 0;
			$week_6 = 0;
			$week_7 = 0;
			$week_13 = 0;
			$week_26 = 0;
			$week_39 = 0;
			$week_51 = 0;
			
			$createdate = $week_pay_detail[$j]["createdate"];
			$join_count = $week_pay_detail[$j]["join_count"];
			
			$std_week = $week_pay_detail[$j]["std_week"];
			$week_0 = $week_pay_detail[$j]["week_0"];
			
			if($search_data == 0) // 누적
			{
				for($k=0; $k<=1; $k++)
				{
					if($k <= $std_week)
						$week_1 += $week_pay_detail[$j]["week_$k"];
					else
						$week_1 = 0;
				}
					
				for($k=0; $k<=2; $k++)
				{
					if($k <= $std_week)
						$week_2 += $week_pay_detail[$j]["week_$k"];
					else
						$week_2 = 0;
				}
					
				for($k=0; $k<=3; $k++)
				{
					if($k <= $std_week)
						$week_3 += $week_pay_detail[$j]["week_$k"];
					else
						$week_3 = 0;
				}
					
				for($k=0; $k<=4; $k++)
				{
					if($k <= $std_week)
						$week_4 += $week_pay_detail[$j]["week_$k"];
					else
						$week_4 = 0;
				}
					
				for($k=0; $k<=5; $k++)
				{
					if($k <= $std_week)
						$week_5 += $week_pay_detail[$j]["week_$k"];
					else
						$week_5 = 0;
				}
					
				for($k=0; $k<=6; $k++)
				{
					if($k <= $std_week)
						$week_6 += $week_pay_detail[$j]["week_$k"];
					else
						$week_6 = 0;
				}
					
				for($k=0; $k<=7; $k++)
				{
					if($k <= $std_week)
						$week_7 += $week_pay_detail[$j]["week_$k"];
					else
						$week_7 = 0;
				}
					
				for($k=0; $k<=13; $k++)
				{
					if($k <= $std_week)
						$week_13 += $week_pay_detail[$j]["week_$k"];
					else
						$week_13 = 0;
				}
					
				for($k=0; $k<=26; $k++)
				{
					if($k <= $std_week)
						$week_26 += $week_pay_detail[$j]["week_$k"];
					else
						$week_26 = 0;
				}
					
				for($k=0; $k<=39; $k++)
				{
					if($k <= $std_week)
						$week_39 += $week_pay_detail[$j]["week_$k"];
					else
						$week_39 = 0;
				}
					
				for($k=0; $k<=51; $k++)
				{
					if($k <= $std_week)
						$week_51 += $week_pay_detail[$j]["week_$k"];
					else
						$week_51 = 0;
				}
			}
			else	// 기간
			{
				$week_0 = $week_pay_detail[$j]["week_0"];
				$week_1 = $week_pay_detail[$j]["week_1"];
				$week_2 = $week_pay_detail[$j]["week_2"];
				$week_3 = $week_pay_detail[$j]["week_3"];
				$week_4 = $week_pay_detail[$j]["week_4"];
				$week_5 = $week_pay_detail[$j]["week_5"];
				$week_6 = $week_pay_detail[$j]["week_6"];
				$week_7 = $week_pay_detail[$j]["week_7"];
				$week_13 = $week_pay_detail[$j]["week_13"];
				$week_26 = $week_pay_detail[$j]["week_26"];
				$week_39 = $week_pay_detail[$j]["week_39"];
				$week_51 = $week_pay_detail[$j]["week_51"];
			}
			
			if($std_week >= 51)
				$sum_reg_count_week_51 += $join_count;
				
			if($std_week >= 39)
				$sum_reg_count_week_39 += $join_count;
				
			if($std_week >= 26)
				$sum_reg_count_week_26 += $join_count;
				
			if($std_week >= 13)
				$sum_reg_count_week_13 += $join_count;
				
			if($std_week >= 7)
				$sum_reg_count_week_7 += $join_count;
				
			if($std_week >= 6)
				$sum_reg_count_week_6 += $join_count;
				
			if($std_week >= 5)
				$sum_reg_count_week_5 += $join_count;
				
			if($std_week >= 4)
				$sum_reg_count_week_4 += $join_count;
				
			if($std_week >= 3)
				$sum_reg_count_week_3 += $join_count;
				
			if($std_week >= 2)
				$sum_reg_count_week_2 += $join_count;
				
			if($std_week >= 1)
				$sum_reg_count_week_1 += $join_count;
				
			if($std_week >= 0)
				$sum_reg_count_week_0 += $join_count;
			
			$sum_join_count += $join_count;
			$sum_week_0 += $week_0;
			$sum_week_1 += $week_1;
			$sum_week_2 += $week_2;
			$sum_week_3 += $week_3;
			$sum_week_4 += $week_4;
			$sum_week_5 += $week_5;
			$sum_week_6 += $week_6;
			$sum_week_7 += $week_7;
			$sum_week_13 += $week_13;
			$sum_week_26 += $week_26;
			$sum_week_39 += $week_39;
			$sum_week_51 += $week_51;
			
			write_log($week_3."|".$join_count."|".number_format($week_3/$join_count,2));
			write_log($week_4."|".$join_count."|".number_format($week_4/$join_count,2));
			write_log($week_5."|".$join_count."|".number_format($week_5/$join_count,2));
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
	            <td class="tdc"><?= $createdate ?></td>
	            <td class="tdc"><?= number_format($join_count) ?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format(($join_count == 0) ? 0 : $week_0/$join_count, 2) : "$".number_format($week_0)?></td>
	           	<td class="tdc"><?= ($search_type == 0) ? number_format(($join_count == 0) ? 0 : $week_1/$join_count, 2) : "$".number_format($week_1)?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format(($join_count == 0) ? 0 : $week_2/$join_count, 2) : "$".number_format($week_2)?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format(($join_count == 0) ? 0 : $week_3/$join_count, 2) : "$".number_format($week_3)?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format(($join_count == 0) ? 0 : $week_4/$join_count, 2) : "$".number_format($week_4)?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format(($join_count == 0) ? 0 : $week_5/$join_count, 2) : "$".number_format($week_5)?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format(($join_count == 0) ? 0 : $week_6/$join_count, 2) : "$".number_format($week_6)?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format(($join_count == 0) ? 0 : $week_7/$join_count, 2) : "$".number_format($week_7)?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format(($join_count == 0) ? 0 : $week_13/$join_count, 2) : "$".number_format($week_13)?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format(($join_count == 0) ? 0 : $week_26/$join_count, 2) : "$".number_format($week_26)?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format(($join_count == 0) ? 0 : $week_39/$join_count, 2) : "$".number_format($week_39)?></td>
	            <td class="tdc"><?= ($search_type == 0) ? number_format(($join_count == 0) ? 0 : $week_51/$join_count, 2) : "$".number_format($week_51)?></td>
	        </tr>
<?
	}
    if(sizeof($week_pay_list) == 0)
    {
?>
   			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   				<td class="tdc" colspan="14">검색 결과가 없습니다.</td>
   			</tr>
<?
   	}
   	else
   	{
?>
			<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc point_title">합계</td>
	        	<td class="tdc point"><?= number_format($sum_join_count) ?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format(($sum_reg_count_week_0 == 0) ? 0 : $sum_week_0/$sum_reg_count_week_0, 2) : "$".number_format($sum_week_0)?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format(($sum_reg_count_week_1 == 0) ? 0 : $sum_week_1/$sum_reg_count_week_1, 2) : "$".number_format($sum_week_1)?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format(($sum_reg_count_week_2 == 0) ? 0 : $sum_week_2/$sum_reg_count_week_2, 2) : "$".number_format($sum_week_2)?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format(($sum_reg_count_week_3 == 0) ? 0 : $sum_week_3/$sum_reg_count_week_3, 2) : "$".number_format($sum_week_3)?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format(($sum_reg_count_week_4 == 0) ? 0 : $sum_week_4/$sum_reg_count_week_4, 2) : "$".number_format($sum_week_4)?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format(($sum_reg_count_week_5 == 0) ? 0 : $sum_week_5/$sum_reg_count_week_5, 2) : "$".number_format($sum_week_5)?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format(($sum_reg_count_week_6 == 0) ? 0 : $sum_week_6/$sum_reg_count_week_6, 2) : "$".number_format($sum_week_6)?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format(($sum_reg_count_week_7 == 0) ? 0 : $sum_week_7/$sum_reg_count_week_7, 2) : "$".number_format($sum_week_7)?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format(($sum_reg_count_week_13 == 0) ? 0 : $sum_week_13/$sum_reg_count_week_13, 2) : "$".number_format($sum_week_13)?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format(($sum_reg_count_week_26 == 0) ? 0 : $sum_week_26/$sum_reg_count_week_26, 2) : "$".number_format($sum_week_26)?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format(($sum_reg_count_week_39 == 0) ? 0 : $sum_week_39/$sum_reg_count_week_39, 2) : "$".number_format($sum_week_39)?></td>
	            <td class="tdc point"><?= ($search_type == 0) ? number_format(($sum_reg_count_week_51 == 0) ? 0 : $sum_week_51/$sum_reg_count_week_51, 2) : "$".number_format($sum_week_51)?></td>
	        </tr>
<?
   	}
?>
        </tbody>
	</table>
<?
	}
?>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>