<?	
	include($_SERVER["DOCUMENT_ROOT"]."/common/common_include.inc.php");
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$std_useridx = 10000;
	}
	
	$category = $_GET["category"];
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	
	$today = date("Y-m-d");
	
	if($search_start_createdate == "")
		$search_start_createdate = date("Y-m-d", strtotime("-5 day"));
	
	if($search_end_createdate == "")
		$search_end_createdate = $today;
	
	$db_main = new CDatabase_Main();
	
	$sql = "";
	
	if($category == 0) 
	{
	$sql = "SELECT writedate, SUM(pay_money) AS pay_money, SUM(refund_pay_money) AS refund_pay_money, SUM(earn_money) AS earn_money ".
			"FROM ".
			"( ".
				"SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, IFNULL(SUM(facebookcredit), 0) AS pay_money, 0 AS refund_pay_money, 0 AS earn_money ".
				"FROM tbl_product_order ". 
				"WHERE useridx > $std_useridx AND status IN (1, 3) AND writedate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59' ".
				"GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
				"UNION ALL ".
				"SELECT DATE_FORMAT(canceldate, '%Y-%m-%d') AS writedate, 0 AS pay_money, IFNULL(SUM(facebookcredit), 0) AS refund_pay_money, 0 AS earn_money ".
				"FROM tbl_product_order ". 
				"WHERE useridx > $std_useridx AND status = 2 AND canceldate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59' ".
				"GROUP BY DATE_FORMAT(canceldate, '%Y-%m-%d') ".
				"UNION ALL ".
				"SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 0 AS pay_money, 0 AS refund_pay_money, IFNULL(ROUND(SUM(money*10)), 0) AS earn_money ".
				"FROM tbl_product_order_earn ". 
				"WHERE useridx > $std_useridx AND status = 1 AND writedate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59' ".
				"GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
			") t1 ". 
			"GROUP BY writedate ORDER BY writedate DESC ";
	}
	else if($category == 1)
	{
		$sql = "SELECT writedate, SUM(pay_money) AS pay_money, SUM(refund_pay_money) AS refund_pay_money, SUM(earn_money) AS earn_money
				FROM
				(
					SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, ROUND(SUM(money*10)) AS pay_money, 0 AS refund_pay_money, 0 AS earn_money
					FROM tbl_product_order_mobile WHERE  useridx > $std_useridx AND  os_type = 1 AND STATUS = 1 AND writedate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59'
					GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
					UNION ALL
					SELECT DATE_FORMAT(canceldate, '%Y-%m-%d') AS writedate, 0 AS pay_money, ROUND(SUM(money*10)) AS refund_pay_money, 0 AS earn_money
					FROM tbl_product_order_mobile WHERE  useridx > $std_useridx AND  os_type = 1 AND STATUS = 2 AND canceldate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59'
					GROUP BY DATE_FORMAT(canceldate, '%Y-%m-%d')
				) t1
				GROUP BY writedate ORDER BY writedate DESC";
	}
	else if($category == 2)
	{
		$sql = "SELECT writedate, SUM(pay_money) AS pay_money, SUM(refund_pay_money) AS refund_pay_money, SUM(earn_money) AS earn_money
				FROM
				(
					SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, ROUND(SUM(money*10)) AS pay_money, 0 AS refund_pay_money, 0 AS earn_money
					FROM tbl_product_order_mobile WHERE  useridx > $std_useridx AND  os_type = 2 AND STATUS = 1 AND writedate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59'
					GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
					UNION ALL
					SELECT DATE_FORMAT(canceldate, '%Y-%m-%d') AS writedate, 0 AS pay_money, ROUND(SUM(money*10)) AS refund_pay_money, 0 AS earn_money
					FROM tbl_product_order_mobile WHERE  useridx > $std_useridx AND  os_type = 2 AND STATUS = 2 AND canceldate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59'
					GROUP BY DATE_FORMAT(canceldate, '%Y-%m-%d')
				) t1
				GROUP BY writedate ORDER BY writedate DESC";
	}
	else if($category == 3)
	{
		$sql = "SELECT writedate, SUM(pay_money) AS pay_money, SUM(refund_pay_money) AS refund_pay_money, SUM(earn_money) AS earn_money
				FROM
				(
					SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, ROUND(SUM(money*10)) AS pay_money, 0 AS refund_pay_money, 0 AS earn_money
					FROM tbl_product_order_mobile WHERE  useridx > $std_useridx AND  os_type = 3 AND STATUS = 1 AND writedate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59'
					GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
					UNION ALL
					SELECT DATE_FORMAT(canceldate, '%Y-%m-%d') AS writedate, 0 AS pay_money, ROUND(SUM(money*10)) AS refund_pay_money, 0 AS earn_money
					FROM tbl_product_order_mobile WHERE  useridx > $std_useridx AND  os_type = 3 AND STATUS = 2 AND canceldate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59'
					GROUP BY DATE_FORMAT(canceldate, '%Y-%m-%d')
				) t1
				GROUP BY writedate ORDER BY writedate DESC";
	}
	
	if($sql != "")
	$pay_data = $db_main->gettotallist($sql);
?>

<?
header("Pragma: public");
header("Expires: 0");
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=indicator_$search_start_createdate"._."$search_end_createdate.xls");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Content-Description: PHP5 Generated Data");
?>
 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<div id="tab_content_1">
		<table class="tbl_list_basic1" border="1">
			<colgroup>
				<col width="150">
				<col width="100">
				<col width="100">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">결제금액</th>
					<th class="tdc">환불금액</th>	
				</tr>
			</thead>
			<tbody>
	<?
					$sum_money = 0;
					$sum_refund = 0;
					
					$sub_point_count = 1;
						
					for($i=0; $i<sizeof($pay_data); $i++)
					{
						$writedate = $pay_data[$i]["writedate"];
							
						$pay_money = $pay_data[$i]["pay_money"];
						$earn_money = $pay_data[$i]["earn_money"];
						$money = $pay_money + $earn_money;						
							
						$refund_pay_money = $pay_data[$i]["refund_pay_money"];							
							
						$sum_money += $money;
						$sum_refund += $refund_pay_money;
						
						$money = round($money/10, $sub_point_count);
						$refund_pay_money = round($refund_pay_money/10, $sub_point_count);
	?>
					
					<tr>
						<td class="tdc point_title"><?= $writedate ?></td>
						<td class="tdc">$<?= number_format($money, $sub_point_count) ?></td>
						<td class="tdc">$<?= number_format($refund_pay_money, $sub_point_count) ?></td>											
					</tr>
	<?	
					}					

					$sum_money = round($sum_money/10, $sub_point_count);
					$sum_refund = round($sum_refund/10, $sub_point_count);
	?>				
					<tr>
						<td class="tdc point_title">TOTAL</td>
						<td class="tdc">$<?= number_format($sum_money, $sub_point_count) ?></td>
						<td class="tdc">$<?= number_format($sum_refund, $sub_point_count) ?></td>										
					</tr>
			</tbody>
		</table>
 	</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main->end();
?>