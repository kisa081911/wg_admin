<?
	$top_menu = "management";
	$sub_menu = "month_game_purchase_join";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");	
		
	$search_start_year = $_GET["search_start_year"];
	$search_start_month = $_GET["search_start_month"];
	$search_end_year = $_GET["search_end_year"];
	$search_end_month = $_GET["search_end_month"];
	$search_mode = ($_GET["search_mode"] == "") ? 0:$_GET["search_mode"];
	$search_platform = ($_GET["search_platform"] == "") ? 0 : $_GET["search_platform"];
	
	if($search_start_year == "" && $search_start_month == "")
	{
		$search_start_year = date("Y", strtotime("-6 month"));
		$search_start_month = date("m", strtotime("-6 month"));
	}
	
	if($search_end_year == "" && $search_end_month == "")
	{
		$search_end_year = date("Y");
		$search_end_month = date("m");
	}
	
	$search_start_createdate = $search_start_year."-".$search_start_month;
	$search_end_createdate = $search_end_year."-".$search_end_month;
	
	$db_other = new CDatabase_Other();
	$db_analysis = new CDatabase_Analysis();
	
	if($search_mode == 0)
	{
		$str_mode = "매출월 기준";
	}
	else if($search_mode == 1)
	{
		$str_mode = "가입월 기준";
	}
	
	if($search_platform == 0)
	{
		$str_platform = " WHERE platform = 0";
		$plaform_name = "Web";

		if($search_mode == 1)
		{
			$sql = "SELECT writedate, month ".
					"FROM ".
					"( ".
					"	SELECT writedate, month FROM game_indicator_month WHERE typeid = 11 AND writedate <= '2015-12' ".
					"	UNION ALL	".	
					"	SELECT DATE_FORMAT(writedate, '%Y-%m') AS writedate, SUM(today) AS month FROM game_indicator_daily WHERE typeid = 11 AND writedate >= '2016-01-01' GROUP BY DATE_FORMAT(writedate, '%Y-%m') ".
					") t1;";
			$cnt_month_user_data = $db_analysis->gettotallist($sql);
			
			$cnt_month_user = array();
			
			for($i = 0; $i < sizeof($cnt_month_user_data); $i++)
			{
				$cnt_month_user[$cnt_month_user_data[$i]["writedate"]] = $cnt_month_user_data[$i]["month"];
			}
		}		
	}
	else if($search_platform == 1)
	{
		$str_platform = " WHERE platform = 1";
		$plaform_name = "IOS";
		
		if($search_mode == 1)
		{		
			$sql = "SELECT writedate, month ".
					"FROM ".
					"( ".
					"	SELECT writedate, month FROM game_indicator_month WHERE typeid = 12 AND writedate <= '2015-12' ".
					"	UNION ALL	".
					"	SELECT DATE_FORMAT(writedate, '%Y-%m') AS writedate, SUM(today) AS month FROM game_indicator_daily WHERE typeid = 12 AND writedate >= '2016-01-01' GROUP BY DATE_FORMAT(writedate, '%Y-%m') ".
					") t1;";
			$cnt_month_user_data = $db_analysis->gettotallist($sql);
			
			$cnt_month_user = array();
			
			for($i = 0; $i < sizeof($cnt_month_user_data); $i++)
			{
				$cnt_month_user[$cnt_month_user_data[$i]["writedate"]] = $cnt_month_user_data[$i]["month"];
			}
		}		
	}
	else if($search_platform == 2)
	{
		$str_platform = " WHERE platform = 2";
		$plaform_name = "Android";
		
		if($search_mode == 1)
		{
			$sql = "SELECT writedate, month ".
					"FROM ".
					"( ".
					"	SELECT writedate, month FROM game_indicator_month WHERE typeid = 13 AND writedate <= '2015-12' ".
					"	UNION ALL	".	
					"	SELECT DATE_FORMAT(writedate, '%Y-%m') AS writedate, SUM(today) AS month FROM game_indicator_daily WHERE typeid = 13 AND writedate >= '2016-01-01' GROUP BY DATE_FORMAT(writedate, '%Y-%m') ".
					") t1;";
			$cnt_month_user_data = $db_analysis->gettotallist($sql);
			
			$cnt_month_user = array();
			
			for($i = 0; $i < sizeof($cnt_month_user_data); $i++)
			{
				$cnt_month_user[$cnt_month_user_data[$i]["writedate"]] = $cnt_month_user_data[$i]["month"];
			}
		}
	}
	else if($search_platform == 3)
	{
		$str_platform = " WHERE platform = 3";
		$plaform_name = "Amazon";
		
		if($search_mode == 1)
		{
			$sql = "SELECT writedate, month ".
					"FROM ".
					"( ".
					"	SELECT writedate, month FROM game_indicator_month WHERE typeid = 14 AND writedate <= '2015-12' ".
					"	UNION ALL	".	
					"	SELECT DATE_FORMAT(writedate, '%Y-%m') AS writedate, SUM(today) AS month FROM game_indicator_daily WHERE typeid = 14 AND writedate >= '2016-01-01' GROUP BY DATE_FORMAT(writedate, '%Y-%m') ".
					") t1;";
			$cnt_month_user_data = $db_analysis->gettotallist($sql);
			
			$cnt_month_user = array();
			
			for($i = 0; $i < sizeof($cnt_month_user_data); $i++)
			{
				$cnt_month_user[$cnt_month_user_data[$i]["writedate"]] = $cnt_month_user_data[$i]["month"];
			}
		}
	}
	
	if($search_mode == 0)
	{	
		$sql = "SELECT purchase_month FROM tbl_user_month_purchases_statics ".
				" $str_platform AND purchase_month BETWEEN '$search_start_createdate' AND '$search_end_createdate' GROUP BY purchase_month";		
		$total_pay_date = $db_other->gettotallist($sql);
		
		$purchase_width_size = sizeof($total_pay_date) * 162;
		
		$sql = "SELECT join_month FROM tbl_user_month_purchases_statics $str_platform GROUP BY join_month";
		$total_join_date = $db_other->gettotallist($sql);
		
		
		$sql = "SELECT join_month, purchase_month, total_credit, user_count, ".
				" (SELECT SUM(total_credit) FROM tbl_user_month_purchases_statics $str_platform AND purchase_month = t1.purchase_month) AS total_month_credit ".
				"FROM tbl_user_month_purchases_statics t1 ".
				" $str_platform AND purchase_month BETWEEN '$search_start_createdate' AND '$search_end_createdate' ";
		$data_purchase_list = $db_other->gettotallist($sql);
		
		for($i = 0; $i < sizeof($data_purchase_list); $i++)
		{
			$join_month = $data_purchase_list[$i]["join_month"];
			$purchase_month = $data_purchase_list[$i]["purchase_month"];
			
			$total_data_purchase[$join_month][$purchase_month]["total_credit"] = $data_purchase_list[$i]["total_credit"];
			$total_data_purchase[$join_month][$purchase_month]["user_count"] = $data_purchase_list[$i]["user_count"];
			$total_data_purchase[$join_month][$purchase_month]["total_month_credit"] = $data_purchase_list[$i]["total_month_credit"];			
		}
	}
	else if ($search_mode == 1)
	{
		$sql = "SELECT join_month FROM tbl_user_month_join_statics ".
				" $str_platform AND join_month BETWEEN '$search_start_createdate' AND '$search_end_createdate' GROUP BY join_month";
		$total_join_date = $db_other->gettotallist($sql);
		
		$join_width_size = sizeof($total_join_date) * 200;
		
		$sql = "SELECT purchase_month FROM tbl_user_month_join_statics $str_platform AND purchase_month BETWEEN '$search_start_createdate' AND '$search_end_createdate' GROUP BY purchase_month";
		$total_pay_date = $db_other->gettotallist($sql);
		
		$sql = "SELECT purchase_month, join_month, total_credit, user_count, (SELECT total_credit FROM tbl_user_month_join_statics $str_platform AND join_month = t1.join_month AND purchase_month > t1.join_month ORDER BY purchase_month ASC LIMIT 1) AS max_month_credit ".
				" FROM tbl_user_month_join_statics t1 ".
				" $str_platform AND join_month  BETWEEN '$search_start_createdate' AND '$search_end_createdate' ";
		$data_join_list = $db_other->gettotallist($sql);
		
		for($i = 0; $i < sizeof($data_join_list); $i++)
		{
			$purchase_month = $data_join_list[$i]["purchase_month"];
			$join_month = $data_join_list[$i]["join_month"];			
		
			$total_data_join[$purchase_month][$join_month]["total_credit"] = $data_join_list[$i]["total_credit"];
			$total_data_join[$purchase_month][$join_month]["user_count"] = $data_join_list[$i]["user_count"];
			$total_data_join[$purchase_month][$join_month]["max_month_credit"] = $data_join_list[$i]["max_month_credit"];
		}
	}
?>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="month_game_purchase_join.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; <?= $str_mode?>&nbsp;<?=$plaform_name ?> 가입자 분포 </div>
			<div class="search_box">			
			기준 : &nbsp;
			<select name="search_mode" id="search_mode">
				<option value = "0" <?= ($search_mode == 0) ? "selected" : "" ?>>매출월 기준</option>
				<option value = "1" <?= ($search_mode == 1) ? "selected" : "" ?>>가입월 기준</option>				
			</select>
			&nbsp;&nbsp;&nbsp;
			플랫폼 : &nbsp;
			<select name="search_platform" id="search_platform">
				<option value = "0" <?= ($search_platform == 0) ? "selected" : "" ?>>Web</option>
				<option value = "1" <?= ($search_platform == 1) ? "selected" : "" ?>>Ios</option>
				<option value = "2" <?= ($search_platform == 2) ? "selected" : "" ?>>Android</option>
				<option value = "3" <?= ($search_platform == 3) ? "selected" : "" ?>>Amazon</option>
			</select>	
			&nbsp;&nbsp;&nbsp;		
			<select name="search_start_year" id="search_start_year">
			 <?for ($i=2014; $i <= 2020; $i++ ) {?>
				<option value="<?=$i?>" <?= ($search_start_year == $i) ? "selected" : "" ?>><?=$i?></option>				
			<?}?>
			</select>년
			<select name="search_start_month" id="search_start_month">
			<?for( $i = 1; $i <= 12; $i++ ) { 
				$month_start_num = str_pad( $i, 2, 0, STR_PAD_LEFT );
			?>			
				<option value="<?=$month_start_num?>" <?= ($search_start_month == $month_start_num) ? "selected" : "" ?>><?=$month_start_num?></option>			
			<?}?>
			</select>월				
			 ~
			<select name="search_end_year" id="search_end_year">
			<?for ($i=2015; $i <= 2020; $i++ ) {?>
				<option value="<?=$i?>" <?= ($search_end_year == $i) ? "selected" : "" ?>><?=$i?></option>				
			<?}?>
			</select>년
			<select name="search_end_month" id="search_end_month">
			<?for( $i = 1; $i <= 12; $i++ ) { 
				$month_end_num = str_pad( $i, 2, 0, STR_PAD_LEFT );
			?>			
				<option value="<?=$month_end_num?>" <?= ($search_end_month == $month_end_num) ? "selected" : "" ?>><?=$month_end_num?></option>			
			<?}?>
			</select>월
			
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->      
    </form>    	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	<div id="tab_content_1">
<?
		if($search_mode == 0)
		{
?>
			<table class="tbl_list_basic1" style="min-width:1088px; width:<?=$purchase_width_size?>px">
				<colgroup>
					<col width="70">
<?
					for($i = 0; $i < sizeof($total_pay_date); $i++)
					{
?>
						<col width="">
<?
					}
?>
				</colgroup>
				<thead>
					<tr>
						<th class="tdc" colspan="1">매출월</th>
					
<?
					for($i = 0; $i < sizeof($total_pay_date); $i++)
					{
?>
						<th class="tdc" colspan="3"><?=$total_pay_date[$i]["purchase_month"]?></th>
<?
					}
?>
					</tr>
					<tr>
						<th class="tdc">가입월</th>
<?
					for($i = 0; $i < sizeof($total_pay_date); $i++)
					{
?>
						<th class="tdc">매출액</th>
						<th class="tdc">비율</th>
						<th class="tdc">결제자</th>
<?
					}
?>
					</tr>
				</thead>
				<tbody>
	
<?
				for($i = 0; $i<sizeof($total_join_date); $i++)
				{
					$join_date = $total_join_date[$i]["join_month"];
					
?>
					<tr>
						<td class="tdc point_title"><?=  $join_date ?></td>
<?
					for($j = 0; $j < sizeof($total_pay_date); $j++)
					{
						$pay_date = $total_pay_date[$j]["purchase_month"];

						$total_credit = $total_data_purchase[$join_date][$pay_date]["total_credit"];
						$user_count = $total_data_purchase[$join_date][$pay_date]["user_count"];
						$total_month_credit = $total_data_purchase[$join_date][$pay_date]["total_month_credit"];
						
						$sum_purchase_total_credit[$pay_date] += $total_credit;
						
						if($total_credit == "")
							$total_credit = 0;
						
						if($user_count == "")
							$user_count = 0;
						
			
?>
						<td class="tdc">$<?= number_format($total_credit, 1) ?></td>
						<td class="tdc"><?= round((($total_month_credit == 0) ? 0 : $total_credit/$total_month_credit)*100,1) ?>%</td>
						<td class="tdc"><?= number_format($user_count) ?></td>
<?
					}
?>
					</tr>
<?
				}
?>		
					<tr>
						<td class="tdc point_title">합계</td>
<?					
					for($j = 0; $j < sizeof($total_pay_date); $j++)
					{
						$pay_date = $total_pay_date[$j]["purchase_month"];
?>
						<td class="tdc point_title">$<?= number_format($sum_purchase_total_credit[$pay_date], 1) ?></td>
						<td class="tdc point_title">100%</td>
						<td class="tdc point_title"></td>
<?
					}
?>					
					</tr>
				</tbody>
			</table>
<?
		}
		else if($search_mode == 1)
		{
?>

			<table class="tbl_list_basic1" style="min-width:1088px; width:<?=$join_width_size?>px">
				<colgroup>
					<col width="70">
<?
					for($i = 0; $i < sizeof($total_join_date); $i++)
					{
?>
						<col width="">
<?
					}
?>
				</colgroup>
				<thead>
					<tr>
						<th class="tdc" colspan="1">가입월</th>
					
<?
					for($i = 0; $i < sizeof($total_join_date); $i++)
					{
?>
						<th class="tdc" colspan="4"><?=$total_join_date[$i]["join_month"]?></th>
<?
					}
?>
					</tr>
					<tr>
						<th class="tdc">매출월</th>
<?
					for($i = 0; $i < sizeof($total_join_date); $i++)
					{
?>
						<th class="tdc">매출액</th>
						<th class="tdc">감소율</th>
						<th class="tdc">결제자</th>
						<th class="tdc">ARPI</th>
<?
					}
?>
					</tr>
				</thead>
				<tbody>
<?
				for($i = 0; $i<sizeof($total_pay_date); $i++)
				{
					$pay_date = $total_pay_date[$i]["purchase_month"];
?>
					<tr>
						<td class="tdc point_title"><?= $pay_date ?></td>
<?
					for($j = 0; $j < sizeof($total_join_date); $j++)
					{
						$join_date = $total_join_date[$j]["join_month"];	
						
						$total_credit = $total_data_join[$pay_date][$join_date]["total_credit"];
						$user_count = $total_data_join[$pay_date][$join_date]["user_count"];
						$max_month_credit = $total_data_join[$pay_date][$join_date]["max_month_credit"];

						$cnt_month_user_data = $cnt_month_user[$join_date];
						
						$sum_join_total_credit[$join_date] += $total_credit;
						
						if($total_credit == "")
							$total_credit = 0;
						
						if($user_count == "")
							$user_count = 0;
?>
						<td class="tdc">$<?= number_format($total_credit, 1) ?></td>
						<td class="tdc"><?= round((($max_month_credit == 0) ? 0 : $total_credit/$max_month_credit)*100,1) ?>%</td>
						<td class="tdc"><?= number_format($user_count) ?></td>
						<td class="tdc"><?= round(($cnt_month_user_data == 0) ? 0 : $total_credit/$cnt_month_user_data,1) ?></td>
<?
					}
?>
					</tr>
<?
				}
?>

					<tr>
						<td class="tdc point_title">합계</td>
<?					
					for($j = 0; $j < sizeof($total_join_date); $j++)
					{
						$join_date = $total_join_date[$j]["join_month"];
?>
						<td class="tdc point_title">$<?= number_format($sum_join_total_credit[$join_date], 1) ?></td>
						<td class="tdc point_title"></td>
						<td class="tdc point_title"></td>
						<td class="tdc point_title"></td>
<?
					}
?>					
					</tr>
				</tbody>
			</table>
<?
		}
?>

	</div>	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_other->end();
	$db_analysis->end();
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>