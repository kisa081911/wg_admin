<?
    $top_menu = "management";
	$sub_menu = "daily_game_mobile_arpu_stat";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $term = ($_GET["term"] == "") ? "ALL" : $_GET["term"];
    
    if ($term != "ALL" && $term != "0" && $term != "1")
    	error_back("잘못된 접근입니다.");
    
    $pagename = "daily_game_mobile_arpu_stat.php";
    
    $startdate = ($_GET["startdate"] == "") ? get_past_date(date("Y-m-d"),14,"d") : $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
    
    if($term == "ALL")
    {
    	$column = "today, ios_logincount + guest_ios_logincount AS ios_logincount, and_logincount + guest_and_logincount AS and_logincount, ama_logincount + guest_ama_logincount AS ama_logincount,
	    				ios_payercount + guest_ios_payercount AS ios_payercount, and_payercount + guest_and_payercount AS and_payercount, ama_payercount + guest_ama_payercount AS ama_payercount,
	    				ios_money + guest_ios_money AS ios_money, and_money + guest_and_money AS and_money, ama_money + guest_ama_money AS ama_money ";
    }
    else if($term == 0)
    {
    	$column = "today, ios_logincount, and_logincount, ama_logincount,
    				ios_payercount, and_payercount, ama_payercount,
    				ios_money, and_money, ama_money ";
    }
    else
    {
    	$column = "today, guest_ios_logincount AS ios_logincount, guest_and_logincount AS and_logincount, guest_ama_logincount AS ama_logincount,
	    				guest_ios_payercount AS ios_payercount, guest_and_payercount AS and_payercount, guest_ama_payercount AS ama_payercount,
	    				guest_ios_money AS ios_money, guest_and_money AS and_money, guest_ama_money AS ama_money ";
    }
    	
    $db_other = new CDatabase_Other();
    
   	$sql = "SELECT $column FROM tbl_user_guest_stat_daily WHERE today BETWEEN '$startdate' AND '$enddate'";
   	$guest_arpu_stat = $db_other->gettotallist($sql);

    $db_other->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});

    function drawChart() 
    {
        var data1 = new google.visualization.DataTable();
        
        data1.addColumn('string', '날짜');
        data1.addColumn('number', '전체');
        data1.addColumn('number', 'iOS');
        data1.addColumn('number', 'Android');
        data1.addColumn('number', 'Amazon');
        data1.addRows([
<?
    for ($i=0; $i<sizeof($guest_arpu_stat); $i++)
    {
    	$date = $guest_arpu_stat[$i]["today"];
    	
    	$all_logincount = $guest_arpu_stat[$i]["ios_logincount"] + $guest_arpu_stat[$i]["and_logincount"] + $guest_arpu_stat[$i]["ama_logincount"];
    	$ios_logincount = $guest_arpu_stat[$i]["ios_logincount"];
    	$and_logincount = $guest_arpu_stat[$i]["and_logincount"];
    	$ama_logincount = $guest_arpu_stat[$i]["ama_logincount"];
    	
    	$all_money = $guest_arpu_stat[$i]["ios_money"] + $guest_arpu_stat[$i]["and_money"] + $guest_arpu_stat[$i]["ama_money"];
    	$ios_money = $guest_arpu_stat[$i]["ios_money"];
    	$and_money = $guest_arpu_stat[$i]["and_money"];
    	$ama_money = $guest_arpu_stat[$i]["ama_money"];

    	$all_arpu = ($all_logincount == 0) ? 0 : $all_money / $all_logincount;  
    	$ios_arpu = ($ios_logincount == 0) ? 0 : $ios_money / $ios_logincount;  
    	$and_arpu = ($and_logincount == 0) ? 0 : $and_money / $and_logincount;  
    	$ama_arpu = ($ama_logincount == 0) ? 0 : $ama_money / $ama_logincount;  
    	
        echo("['".$date."'");   
        
        if ($all_arpu != "")
        	echo(",{v:".$all_arpu."}");
        else
        	echo(",0");
        
       	if ($ios_arpu != "")
        	echo(",{v:".$ios_arpu."}");
        else
        	echo(",0");
        
       	if ($and_arpu != "")
        	echo(",{v:".$and_arpu."}");
        else
        	echo(",0");
        
       	if ($ama_arpu != "")
        	echo(",{v:".$ama_arpu."}]");
        else
        	echo(",0]");
        
        if ($i < sizeof($guest_arpu_stat))
			echo(",");
    }
?>     
        ]);

        var options1 = {
                title:'',                                                      
                width:1050,                         
                height:200,
                axisTitlesPosition:'in',
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize : 12,
                chartArea:{left:80,top:40,width:1020,height:130}
        };
       
        chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
        chart.draw(data1, options1);
    }
  
    google.setOnLoadCallback(drawChart);
    
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }

    function search()
    {
        var search_form = document.search_form;

        search_form.submit();
    }
    
    $(function() {
        $("#startdate").datepicker({ });
    });
    
    $(function() {
        $("#enddate").datepicker({ });
    });

    function change_term(term)
	{
		var search_form = document.search_form;

		var all = document.getElementById("term_all");
		var fb = document.getElementById("term_fb");
		var guest = document.getElementById("term_guest");
		
		document.search_form.term.value = term;

		if (term == "ALL")
		{
			all.className="btn_schedule_select";
			fb.className="btn_schedule";
			guest.className="btn_schedule";
		}	
		else if (term == "0")
		{
			all.className="btn_schedule";
			fb.className="btn_schedule_select";
			guest.className="btn_schedule";
		}
		else if (term == "1")
		{
			all.className="btn_schedule";
			fb.className="btn_schedule";
			guest.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>
        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 모바일 ARPU 현황</div>
                
                <form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
	                <div class="search_box">
	                	<input type="hidden" name="term" id="term" value="<?= $term ?>" />
	                	
	                	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
					
				</span>
	                	
	                    <input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
	                    <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
	                     <input type="button" class="btn_search" value="검색" onclick="search()" />
	                </div>
                </form>
            </div>
            <!-- //title_warp -->
            
            <div class="search_result">
            	<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다.
            </div>
            
            <input type="button" class="<?= ($term == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" value="전체" id="term_all" onclick="change_term('ALL')"    />
            <input type="button" class="<?= ($term == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="FB유저" id="term_fb" onclick="change_term('0')"    />
			<input type="button" class="<?= ($term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="Guest유저" id="term_guest" onclick="change_term('1')" />
        
	    	<div id="chart_div1" style="height:230px; min-width: 500px"></div>

			<table class="tbl_list_basic1">
                <colgroup>
					<col width="70">
					
					<col width="">
					<col width="60">
					<col width="">
					<col width=50>
					
		            <col width="">
		            <col width="60">
		            <col width="">
		            <col width="50">
		            
		            <col width="">
		            <col width="60">
		            <col width="">
		            <col width="50">
		            
		            <col width="">
		            <col width="60">
		            <col width="">
		            <col width="50">
				</colgroup>
		        <thead>
		        	<tr>
		        		<th rowspan="2">일자</th>
						<th colspan="4" style="border-right: 1px dotted #dbdbdb;">전체</th>
		            	<th colspan="4" style="border-right: 1px dotted #dbdbdb;">iOS</th>
		               	<th colspan="4" style="border-right: 1px dotted #dbdbdb;">Android</th>
		             	<th colspan="4" style="border-right: 1px dotted #dbdbdb;">Amazon</th>
		         	</tr>
		        	<tr>
		        		<th>유저수</th>
		               	<th>결제자수</th>
		             	<th>결제금액</th>
		             	<th style="border-right: 1px dotted #dbdbdb;">ARPU</th>
		             	
		        		<th>유저수</th>
		               	<th>결제자수</th>
		             	<th>결제금액</th>
		             	<th style="border-right: 1px dotted #dbdbdb;">ARPU</th>
		             	
		        		<th>유저수</th>
		               	<th>결제자수</th>
		             	<th>결제금액</th>
		             	<th style="border-right: 1px dotted #dbdbdb;">ARPU</th>
		             	
		        		<th>유저수</th>
		               	<th>결제자수</th>
		             	<th>결제금액</th>
		             	<th>ARPU</th>
		         	</tr>
		        </thead>
                <tbody>
                	<tr>
<?
	for($i=0; $i<sizeof($guest_arpu_stat); $i++)
	{
		$today = $guest_arpu_stat[$i]["today"];
		$ios_logincount = $guest_arpu_stat[$i]["ios_logincount"];
		$and_logincount = $guest_arpu_stat[$i]["and_logincount"];
		$ama_logincount = $guest_arpu_stat[$i]["ama_logincount"];
		$all_logincount = $ios_logincount + $and_logincount + $ama_logincount;
		
		$ios_payercount = $guest_arpu_stat[$i]["ios_payercount"];
		$and_payercount = $guest_arpu_stat[$i]["and_payercount"];
		$ama_payercount = $guest_arpu_stat[$i]["ama_payercount"];
		$all_payercount = $ios_payercount + $and_payercount + $ama_payercount;
		
		$ios_money = $guest_arpu_stat[$i]["ios_money"];
		$and_money = $guest_arpu_stat[$i]["and_money"];
		$ama_money = $guest_arpu_stat[$i]["ama_money"];
		$all_money = $ios_money + $and_money + $ama_money;
		
		$ios_arpu = ($ios_logincount == 0) ? 0 : number_format($ios_money / $ios_logincount, 3);
		$and_arpu = ($and_logincount == 0) ? 0 : number_format($and_money / $and_logincount, 3);
		$ama_arpu = ($ama_logincount == 0) ? 0 : number_format($ama_money / $ama_logincount, 3);
		$all_arpu = ($all_logincount == 0) ? 0 : number_format($all_money / $all_logincount, 3);
		
		if($all_payercount == 0)
			continue;
		
		$total_all_logincount += $all_logincount;
		$total_ios_logincount += $ios_logincount;
		$total_and_logincount += $and_logincount;
		$total_ama_logincount += $ama_logincount;
		
		$total_all_payercount += $all_payercount;
		$total_ios_payercount += $ios_payercount;
		$total_and_payercount += $and_payercount;
		$total_ama_payercount += $ama_payercount;
		
		$total_all_money += $all_money;
		$total_ios_money += $ios_money;
		$total_and_money += $and_money;		
		$total_ama_money += $ama_money;
?>
						<td class="tdc point"><?= $today ?></td>
						<td class="tdr"><?= number_format($all_logincount) ?></td>
						<td class="tdr"><?= number_format($all_payercount) ?></td>
						<td class="tdr">$ <?= number_format($all_money) ?></td>
						<td class="tdr" style="border-right: 1px dotted #dbdbdb;"><?= $all_arpu ?></td>
						
						<td class="tdr"><?= number_format($ios_logincount) ?></td>
						<td class="tdr"><?= number_format($ios_payercount) ?></td>
						<td class="tdr">$ <?= number_format($ios_money) ?></td>
						<td class="tdr" style="border-right: 1px dotted #dbdbdb;"><?= $ios_arpu ?></td>
						
						<td class="tdr"><?= number_format($and_logincount) ?></td>
						<td class="tdr"><?= number_format($and_payercount) ?></td>
						<td class="tdr">$ <?= number_format($and_money) ?></td>
						<td class="tdr" style="border-right: 1px dotted #dbdbdb;"><?= $and_arpu ?></td>
						
						<td class="tdr"><?= number_format($ama_logincount) ?></td>
						<td class="tdr"><?= number_format($ama_payercount) ?></td>
						<td class="tdr">$ <?= number_format($ama_money) ?></td>
						<td class="tdr"><?= $ama_arpu ?></td>
					</tr>
<?
	}
?>
					<tr>
						<td class="tdc point_title">Total</td>
						<td class="tdr point"><?= number_format($total_all_logincount) ?></td>
						<td class="tdr point"><?= number_format($total_all_payercount) ?></td>
						<td class="tdr point">$ <?= number_format($total_all_money) ?></td>
						<td class="tdr point" style="border-right: 1px dotted #dbdbdb;"><?= ($total_all_logincount == 0)?0:number_format($total_all_money / $total_all_logincount, 3) ?></td>
						
						<td class="tdr point"><?= number_format($total_ios_logincount) ?></td>
						<td class="tdr point"><?= number_format($total_ios_payercount) ?></td>
						<td class="tdr point">$ <?= number_format($total_ios_money) ?></td>
						<td class="tdr point" style="border-right: 1px dotted #dbdbdb;"><?= ($total_ios_logincount == 0)?0:number_format($total_ios_money / $total_ios_logincount, 3) ?></td>
						
						<td class="tdr point"><?= number_format($total_and_logincount) ?></td>
						<td class="tdr point"><?= number_format($total_and_payercount) ?></td>
						<td class="tdr point">$ <?= number_format($total_and_money) ?></td>
						<td class="tdr point" style="border-right: 1px dotted #dbdbdb;"><?= ($total_and_logincount == 0)?0:number_format($total_and_money / $total_and_logincount, 3) ?></td>
						
						<td class="tdr point"><?= number_format($total_ama_logincount) ?></td>
						<td class="tdr point"><?= number_format($total_ama_payercount) ?></td>
						<td class="tdr point">$ <?= number_format($total_ama_money) ?></td>
						<td class="tdr point"><?= ($total_ama_logincount == 0)?0:number_format($total_ama_money / $total_ama_logincount, 3) ?></td>
					</tr>
				</tbody>
			</table>
        </div>
        
        <!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>