<?
	$top_menu = "management";
	$sub_menu = "daily_game_pay_indicator";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$std_useridx = 10000;
	}
	
	$today = date("Y-m-d");
	
	$category = ($_GET["search_tab"] == "") ? "0" : $_GET["search_tab"];
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	
	if($search_start_createdate == "")
		$search_start_createdate = date("Y-m-d", strtotime("-5 day"));
	
	if($search_end_createdate == "")
		$search_end_createdate = $today;
	
	$db_main = new CDatabase_Main();
	
	$sql = "";	
	$category_value = "";
	
	if($category == 0) 
	{
		$category_value = "WEB";
	$sql = "SELECT writedate, SUM(pay_money) AS pay_money, SUM(refund_pay_money) AS refund_pay_money, SUM(earn_money) AS earn_money ".
			"FROM ".
			"( ".
				"SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, IFNULL(SUM(facebookcredit), 0) AS pay_money, 0 AS refund_pay_money, 0 AS earn_money ".
				"FROM tbl_product_order ". 
				"WHERE useridx > $std_useridx AND status IN (1, 3) AND writedate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59' ".
				"GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
				"UNION ALL ".
				"SELECT DATE_FORMAT(canceldate, '%Y-%m-%d') AS writedate, 0 AS pay_money, IFNULL(SUM(facebookcredit), 0) AS refund_pay_money, 0 AS earn_money ".
				"FROM tbl_product_order ". 
				"WHERE useridx > $std_useridx AND status = 2 AND canceldate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59' ".
				"GROUP BY DATE_FORMAT(canceldate, '%Y-%m-%d') ".
				"UNION ALL ".
				"SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 0 AS pay_money, 0 AS refund_pay_money, IFNULL(ROUND(SUM(money*10)), 0) AS earn_money ".
				"FROM tbl_product_order_earn ". 
				"WHERE useridx > $std_useridx AND status = 1 AND writedate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59' ".
				"GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
			") t1 ". 
			"GROUP BY writedate ORDER BY writedate DESC ";
	}
	else if($category == 1)
	{
		$category_value = "IOS";		
		
		$sql = "SELECT writedate, SUM(pay_money) AS pay_money, SUM(refund_pay_money) AS refund_pay_money, SUM(earn_money) AS earn_money
				FROM
				(
					SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, ROUND(SUM(money*10)) AS pay_money, 0 AS refund_pay_money, 0 AS earn_money
					FROM tbl_product_order_mobile WHERE  useridx > $std_useridx AND  os_type = 1 AND STATUS = 1 AND writedate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59'
					GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
					UNION ALL
					SELECT DATE_FORMAT(canceldate, '%Y-%m-%d') AS writedate, 0 AS pay_money, ROUND(SUM(money*10)) AS refund_pay_money, 0 AS earn_money
					FROM tbl_product_order_mobile WHERE  useridx > $std_useridx AND  os_type = 1 AND STATUS = 2 AND canceldate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59'
					GROUP BY DATE_FORMAT(canceldate, '%Y-%m-%d')
				) t1
				GROUP BY writedate ORDER BY writedate DESC";
	}
	else if($category == 2)
	{
		$category_value = "Android";
		
		$sql = "SELECT writedate, SUM(pay_money) AS pay_money, SUM(refund_pay_money) AS refund_pay_money, SUM(earn_money) AS earn_money
				FROM
				(
					SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, ROUND(SUM(money*10)) AS pay_money, 0 AS refund_pay_money, 0 AS earn_money
					FROM tbl_product_order_mobile WHERE  useridx > $std_useridx AND  os_type = 2 AND STATUS = 1 AND writedate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59'
					GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
					UNION ALL
					SELECT DATE_FORMAT(canceldate, '%Y-%m-%d') AS writedate, 0 AS pay_money, ROUND(SUM(money*10)) AS refund_pay_money, 0 AS earn_money
					FROM tbl_product_order_mobile WHERE  useridx > $std_useridx AND  os_type = 2 AND STATUS = 2 AND canceldate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59'
					GROUP BY DATE_FORMAT(canceldate, '%Y-%m-%d')
				) t1
				GROUP BY writedate ORDER BY writedate DESC";
	
	}
	else if($category == 3)
	{
		$category_value = "Amazon";
		
		$sql = "SELECT writedate, SUM(pay_money) AS pay_money, SUM(refund_pay_money) AS refund_pay_money, SUM(earn_money) AS earn_money
				FROM
				(
					SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, ROUND(SUM(money*10)) AS pay_money, 0 AS refund_pay_money, 0 AS earn_money
					FROM tbl_product_order_mobile WHERE  useridx > $std_useridx AND  os_type = 3 AND STATUS = 1 AND writedate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59'
					GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d')
					UNION ALL
					SELECT DATE_FORMAT(canceldate, '%Y-%m-%d') AS writedate, 0 AS pay_money, ROUND(SUM(money*10)) AS refund_pay_money, 0 AS earn_money
					FROM tbl_product_order_mobile WHERE  useridx > $std_useridx AND  os_type = 3 AND STATUS = 2 AND canceldate BETWEEN '$search_start_createdate 00:00:00' AND '$search_end_createdate 23:59:59'
					GROUP BY DATE_FORMAT(canceldate, '%Y-%m-%d')
				) t1
				GROUP BY writedate ORDER BY writedate DESC";
	}
	
	if($sql != "")
		$pay_data = $db_main->gettotallist($sql);	
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="daily_game_pay_indicator.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; <?=$category_value?>&nbsp;게임 결제 현황</div>
		<div class="search_box">
		           플랫폼 : &nbsp;&nbsp; 
             <select name="search_tab" id="search_tab">
             	<option value="0" <?= ($category=="0") ? "selected" : "" ?>>WEB</option>
				<option value="1" <?= ($category=="1") ? "selected" : "" ?>>IOS</option>
				<option value="2" <?= ($category=="2") ? "selected" : "" ?>>Android</option>
				<option value="3" <?= ($category=="3") ? "selected" : "" ?>>Amazon</option>								
			</select>&nbsp;&nbsp; 
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
		<input type="button" class="btn_03" value="Excel 다운로드" onclick="window.location.href='daily_game_pay_indicator_excel.php?category=<?=$category?>&start_createdate=<?= $search_start_createdate ?>&end_createdate=<?= $search_end_createdate ?>'">		
	</div>
	
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">				
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">결제금액</th>
					<th class="tdc">환불금액</th>					
				</tr>
			</thead>
			<tbody>
<?
					$sum_money = 0;
					$sum_refund = 0;
					
					$sub_point_count = 1;
						
					for($i=0; $i<sizeof($pay_data); $i++)
					{
						$writedate = $pay_data[$i]["writedate"];
							
						$pay_money = $pay_data[$i]["pay_money"];
						$earn_money = $pay_data[$i]["earn_money"];
						$money = $pay_money + $earn_money;
						
						
							
						$refund_pay_money = $pay_data[$i]["refund_pay_money"];							
							
						$sum_money += $money;
						$sum_refund += $refund_pay_money;
						
						$money = round($money/10, $sub_point_count);
						$refund_pay_money = round($refund_pay_money/10, $sub_point_count);
?>
					
					<tr>
						<td class="tdc point_title"><?= $writedate ?></td>
						<td class="tdc">$<?= number_format($money, $sub_point_count) ?></td>
						<td class="tdc">$<?= number_format($refund_pay_money, $sub_point_count) ?></td>											
					</tr>
<?	
					}					

					$sum_money = round($sum_money/10, $sub_point_count);
					$sum_refund = round($sum_refund/10, $sub_point_count);
?>				
					<tr>
						<td class="tdc point_title">TOTAL</td>
						<td class="tdc">$<?= number_format($sum_money, $sub_point_count) ?></td>
						<td class="tdc">$<?= number_format($sum_refund, $sub_point_count) ?></td>										
					</tr>
			</tbody>
		</table>
 	</div>
        
     </form>
	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main->end();

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>