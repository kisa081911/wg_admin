<?
	$top_menu = "management";
	$sub_menu = "daily_game_dau_stat";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d", strtotime("-1 day"));
	
	$search_start_searchdate = $_GET["start_searchdate"];
	$search_end_searchdate = $_GET["end_searchdate"];
	
	if($search_start_searchdate == "")
		$search_start_searchdate = date("Y-m-d", strtotime("-15 day"));
	
	if($search_end_searchdate == "")
		$search_end_searchdate = $today;
		
	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT LEFT(today,10) AS today, todayactivecount, todayfacebookactivecount, todayiosactivecount, todayandroidactivecount, todayamazonactivecount ".
			"FROM user_join_log WHERE today >= '$search_start_searchdate' AND today <= '$search_end_searchdate' GROUP BY LEFT(today, 10) ORDER BY today ASC ";
	
	$dau_list = $db_analysis->gettotallist($sql);
?>

<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_searchdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_searchdate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="daily_game_dau_stat.php">
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; DAU 현황</div>
			<div class="search_box">
				<input type="text" class="search_text" id="start_searchdate" name="start_searchdate" value="<?= $search_start_searchdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_searchdate" name="end_searchdate" value="<?= $search_end_searchdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
		</div>
		<!-- //title_warp -->
	
		<div class="search_result">
			<span><?= $search_start_searchdate ?></span> ~ <span><?= $search_end_searchdate ?></span> 통계입니다
			<input type="button" class="btn_03" value="Excel 다운로드" onclick="window.location.href='daily_game_dau_excel.php?start_searchdate=<?= $search_start_searchdate ?>&end_searchdate=<?= $search_end_searchdate ?>'">
		</div>
	</form>
	
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="100">
				<col width="150">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">전체활동회원수</th>
					<th class="tdc">Facebook 활동회원수</th>
					<th class="tdc">iOS 활동회원수</th>
					<th class="tdc">Android 활동회원수</th>
					<th class="tdc">Amazon 활동회원수</th>
				</tr>
			</thead>
			<tbody>
<?
			for($i=0; $i<sizeof($dau_list); $i++)
			{
				$today = $dau_list[$i]["today"];
				$todayactivecount = $dau_list[$i]["todayactivecount"];
				$todayfacebookactivecount = $dau_list[$i]["todayfacebookactivecount"];				
				$todaymobileactivecount = $dau_list[$i]["todayiosactivecount"];
				$todayandroidactivecount = $dau_list[$i]["todayandroidactivecount"];
				$todayamazonactivecount = $dau_list[$i]["todayamazonactivecount"];
?>
				<tr>
					<td class="tdc point_title"><?= $today?></td>
					<td class="tdc"><?= number_format($todayactivecount) ?></td>
					<td class="tdc"><?= number_format($todayfacebookactivecount) ?></td>
					<td class="tdc"><?= number_format($todaymobileactivecount) ?></td>
					<td class="tdc"><?= number_format($todayandroidactivecount) ?></td>
					<td class="tdc"><?= number_format($todayamazonactivecount) ?></td>
				</tr>
<?
			}
?>
			</tbody>
		</table>
	</div>
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_analysis->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>