<?	
	include($_SERVER["DOCUMENT_ROOT"]."/common/common_include.inc.php");
	
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	
	if($search_start_createdate == "")
		$search_start_createdate = date("Y-m-d", strtotime("-2 day"));
	
	if($search_end_createdate == "")
		$search_end_createdate = $today;
	
	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT t1.writedate, t1.main_type, typeid, today, week, month, month, quarter, row_cnt, sub_row_cnt ".
			"FROM ( ".
			"	SELECT writedate, FLOOR(typeid/10) AS main_type, typeid, today, week, month, quarter ".
			"	FROM game_indicator_daily ".
			"	WHERE writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate' ".
			"	ORDER BY writedate DESC, typeid ASC ".
			") t1 LEFT JOIN ( ".
			"	SELECT writedate, COUNT(*) AS row_cnt ".
			"	FROM game_indicator_daily ".
			"	WHERE writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate' ".
			"	GROUP BY writedate ".
			") t2 ON t1.writedate = t2.writedate LEFT JOIN ( ".
			"	SELECT writedate, FLOOR(typeid/10) AS main_type, COUNT(*) AS sub_row_cnt ".
			"	FROM game_indicator_daily ".
			"	WHERE writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate' ".
			"	GROUP BY writedate, FLOOR(typeid/10) ".
			") t3 ON t1.writedate = t3.writedate AND t1.main_type = t3.main_type";
	
	$indicator_data = $db_analysis->gettotallist($sql);
?>

<?
header("Pragma: public");
header("Expires: 0");
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=indicator_$search_start_createdate"._."$search_end_createdate.xls");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Content-Description: PHP5 Generated Data");
?>
 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<div id="tab_content_1">
		<table class="tbl_list_basic1" border="1">
			<colgroup>
				<col width="100">
				<col width="150">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">구분</th>
					<th class="tdc">타입</th>
					<th class="tdc">당일</th>
					<th class="tdc">전주</th>
					<th class="tdc">전월 평균</th>
					<th class="tdc">전분기 평균</th>
				</tr>
			</thead>
			<tbody>
<?
			$tmp_row_cnt = 0;
			$tmp_sub_row_cnt = 0;

			for($i=0; $i<sizeof($indicator_data); $i++)
			{
				$writedate = $indicator_data[$i]["writedate"];
				$typeid = $indicator_data[$i]["typeid"];
				$main_type = floor($typeid/10);
				$sub_type = $typeid%10;
				$today = $indicator_data[$i]["today"];				
				$week = $indicator_data[$i]["week"];
				$month = $indicator_data[$i]["month"];
				$quarter = $indicator_data[$i]["quarter"];
				$row_cnt = $indicator_data[$i]["row_cnt"];
				$sub_row_cnt = $indicator_data[$i]["sub_row_cnt"];
				
				if($main_type < 5)
				{
					$month = round($month/4);
					$quarter = round($quarter/12);
				}
								
				$week_rate = ($week == 0) ? 0 : round(($today / $week) * 100 - 100, 2);
				$month_rate = ($month == 0) ? 0 : round(($today / $month) * 100 - 100, 2);
				$quarter_rate = ($quarter == 0) ? 0 : round(($today / $quarter) * 100 - 100, 2);
				
				$main_name = "";
				
				$sub_point_count = 0;
				
				if($main_type == 1)
					$main_name = "1. 신규회원 가입수";
				else if($main_type == 2)
					$main_name = "2. 활동 유저수";
				else if($main_type == 3)
					$main_name = "3. 결제 유저수";
				else if($main_type == 4)
				{
					$main_name = "4. 결제 금액";
					
					$sub_point_count = 1;
					
					$today = round($today/10, $sub_point_count);					
					$week = round($week/10, $sub_point_count);
					$month = round($month/10, $sub_point_count);
					$quarter = round($quarter/10, $sub_point_count);
				}
				else if($main_type == 5)
				{
					$main_name = "5. ARPU";
					
					$sub_point_count = 3;
					
					$today = round($today/1000, $sub_point_count);					
					$week = round($week/1000, $sub_point_count);
					$month = round($month/1000, $sub_point_count);
					$quarter = round($quarter/1000, $sub_point_count);
				}
				else if($main_type == 6)
				{
					$main_name = "6. ARPPU";
						
					$sub_point_count = 3;
						
					$today = round($today/1000, $sub_point_count);					
					$week = round($week/1000, $sub_point_count);
					$month = round($month/1000, $sub_point_count);
					$quarter = round($quarter/1000, $sub_point_count);
				}
				
				$sub_name = "";
				
				if($sub_type == 0)
					$sub_name = "전체";
				else if($sub_type == 1)
					$sub_name = "Facebook";
				else if($sub_type == 2)
					$sub_name = "Apple";
				else if($sub_type == 3)
					$sub_name = "Google";
				else if($sub_type == 4)
					$sub_name = "Amazon";
				else if($sub_type == 5)
					$sub_name = "Trialpay";				
				
?>
				<tr>

					<td class="tdc point_title"><?= $writedate ?></td>
					<td class="tdc point"><?= $main_name?></td>
					<td class="tdc"><?= $sub_name ?></td>
					<td class="tdc"><?= ($main_type == 4) ? "$ " : "" ?><?= number_format($today, $sub_point_count) ?></td>					
					<td class="tdc"><?= ($main_type == 4) ? "$ " : "" ?><?= number_format($week, $sub_point_count) ?></td>
					<td class="tdc"><?= ($main_type == 4) ? "$ " : "" ?><?= number_format($month, $sub_point_count) ?></td>
					<td class="tdc"><?= ($main_type == 4) ? "$ " : "" ?><?= number_format($quarter, $sub_point_count) ?></td>
				</tr>
<?
				$tmp_row_cnt--;
				$tmp_sub_row_cnt--;
			}
?>
			</tbody>
		</table>
 		</div>
        
     </form>
	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_analysis->end();
?>