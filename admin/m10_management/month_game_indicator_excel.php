<?	
	include($_SERVER["DOCUMENT_ROOT"]."/common/common_include.inc.php");
	
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	
	$today = date("Y-m", strtotime("-1 month"));
	
	if($search_start_createdate == "")
		$search_start_createdate = date("Y-m", strtotime("-2 month"));
	
	if($search_end_createdate == "")
		$search_end_createdate = $today;
	
	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT t1.writedate, t1.main_type, typeid, month, before_month, before_two_month, row_cnt, sub_row_cnt ".
			"FROM ( ".
			"	SELECT writedate, FLOOR(typeid/10) AS main_type, typeid, month, before_month, before_two_month ".
			"	FROM game_indicator_month ".
			"	WHERE typeid NOT LIKE '7%' AND writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate' ".
			"	ORDER BY writedate DESC, typeid ASC ".
			") t1 LEFT JOIN ( ".
			"	SELECT writedate, COUNT(*) AS row_cnt ".
			"	FROM game_indicator_month ".
			"	WHERE typeid NOT LIKE '7%' AND writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate' ".
			"	GROUP BY writedate ".
			") t2 ON t1.writedate = t2.writedate LEFT JOIN ( ".
			"	SELECT writedate, FLOOR(typeid/10) AS main_type, COUNT(*) AS sub_row_cnt ".
			"	FROM game_indicator_month ".
			"	WHERE typeid NOT LIKE '7%' AND writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate' ".
			"	GROUP BY writedate, FLOOR(typeid/10) ".
			") t3 ON t1.writedate = t3.writedate AND t1.main_type = t3.main_type";
	
	$indicator_data = $db_analysis->gettotallist($sql);
?>

<?
header("Pragma: public");
header("Expires: 0");
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=month_indicator_$search_start_createdate"._."$search_end_createdate.xls");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Content-Description: PHP5 Generated Data");
?>
 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<div id="tab_content_1">
		<table class="tbl_list_basic1" border="1">
			<colgroup>
				<col width="100">
				<col width="150">
				<col width="100">
				<col width="100">
				<col width="100">
				<col width="100">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">구분</th>
					<th class="tdc">타입</th>
					<th class="tdc">당월</th>
					<th class="tdc">전월</th>
					<th class="tdc">전전월</th>					
				</tr>
			</thead>
			<tbody>
<?
			$tmp_row_cnt = 0;
			$tmp_sub_row_cnt = 0;

			for($i=0; $i<sizeof($indicator_data); $i++)
			{
				$writedate = $indicator_data[$i]["writedate"];
				$typeid = $indicator_data[$i]["typeid"];
				$main_type = floor($typeid/10);
				$sub_type = $typeid%10;				
				$month = $indicator_data[$i]["month"];
				$before_month = $indicator_data[$i]["before_month"];
				$before_two_month = $indicator_data[$i]["before_two_month"];
				$row_cnt = $indicator_data[$i]["row_cnt"];
				$sub_row_cnt = $indicator_data[$i]["sub_row_cnt"];
				
				$sub_point_count = 0;
				
				if($main_type == 1)
					$main_name = "1. 신규회원 가입수";
				else if($main_type == 2)
					$main_name = "2. 활동 유저수";
				else if($main_type == 3)
					$main_name = "3. 결제 유저수";
				else if($main_type == 4)
				{
					$main_name = "4. 결제 금액";
					
					$sub_point_count = 1;
										
					$month = round($month/10, $sub_point_count);
					$before_month = round($before_month/10, $sub_point_count);
					$before_two_month = round($before_two_month/10, $sub_point_count);
				}
				else if($main_type == 5)
				{
					$main_name = "5. ARPU";
					
					$sub_point_count = 3;
					
					$month = round($month/1000, $sub_point_count);
					$before_month = round($before_month/1000, $sub_point_count);
					$before_two_month = round($before_two_month/1000, $sub_point_count);
				}
				else if($main_type == 6)
				{
					$main_name = "6. ARPPU";
						
					$sub_point_count = 3;
						
					$month = round($month/1000, $sub_point_count);
					$before_month = round($before_month/1000, $sub_point_count);
					$before_two_month = round($before_two_month/1000, $sub_point_count);
				}
				
				$sub_name = "";
				
				if($sub_type == 0)
					$sub_name = "전체";
				else if($sub_type == 1)
					$sub_name = "Facebook";
				else if($sub_type == 2)
					$sub_name = "Apple";
				else if($sub_type == 3)
					$sub_name = "Google";
				else if($sub_type == 4)
					$sub_name = "Amazon";
				else if($sub_type == 5)
					$sub_name = "Trialpay";	
				
?>
				<tr>
					<td class="tdc point_title"><?= date('Y년 m월', strtotime($writedate)) ?></td>
					<td class="tdc point"><?= $main_name?></td>
					<td class="tdc"><?= $sub_name ?></td>
					<td class="tdc"><?= ($main_type == 4) ? "$ " : "" ?><?= number_format($month, $sub_point_count) ?></td>
					<td class="tdc"><?= ($main_type == 4) ? "$ " : "" ?><?= number_format($before_month, $sub_point_count) ?></td>
					<td class="tdc"><?= ($main_type == 4) ? "$ " : "" ?><?= number_format($before_two_month, $sub_point_count) ?></td>					
				</tr>
<?
				$tmp_row_cnt--;
				$tmp_sub_row_cnt--;
			}
?>
			</tbody>
		</table>
 		</div>
        
     </form>
	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_analysis->end();
?>