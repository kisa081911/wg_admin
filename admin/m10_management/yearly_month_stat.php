<?
	$top_menu = "management";
	$sub_menu = "yearly_month_stat";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_end_year = $_GET["search_end_year"];
	$search_end_month = $_GET["search_end_month"];	
	
	if($search_end_year == "" && $search_end_month == "")
	{
		$search_end_year = date("Y");
		$search_end_month = date("m");
	}
	
	$end_month = date('Y-m-d', strtotime($search_end_year."-".$search_end_month."-01"));
	
	$view_month = $end_month = date('Y-m', strtotime($search_end_year."-".$search_end_month));;
	
	$db_other = new CDatabase_Other();	
	
	$end_month = date("m", strtotime($end_month."+1 month"));
	
	$search_end_createdate = $search_end_year."-".$end_month."-01";
	
	$web_sql = "SELECT statcode, AVG( usercount * purchase_usd ) AS A, AVG( purchase_passed ) AS B 
				FROM `takefive_other`.`tbl_user_playstat_gather_daily` 
				WHERE statcode IN ( 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211) AND '2014-08-20' <= today AND today < '$search_end_createdate' AND purchase_passed >= 0
				GROUP BY statcode;";
	$web_result = $db_other->gettotallist($web_sql);
	
	$android_sql = "SELECT statcode, AVG( usercount * purchase_usd ) AS A, AVG( purchase_passed ) AS B
					FROM `takefive_other`.`tbl_user_playstat_gather_daily_android`
					WHERE statcode IN ( 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211) AND '2014-08-20' <= today AND today < '$search_end_createdate' AND purchase_passed >= 0
					GROUP BY statcode;";
	$android_result = $db_other->gettotallist($android_sql);
	
	$ios_sql = "SELECT statcode, AVG( usercount * purchase_usd ) AS A, AVG( purchase_passed ) AS B
				FROM `takefive_other`.`tbl_user_playstat_gather_daily_ios`
				WHERE statcode IN ( 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211) AND '2014-08-20' <= today AND today < '$search_end_createdate' AND purchase_passed >= 0
				GROUP BY statcode;";
	$ios_result = $db_other->gettotallist($ios_sql);
	
	$amazon_sql = "SELECT statcode, AVG( usercount * purchase_usd ) AS A, AVG( purchase_passed ) AS B
					FROM `takefive_other`.`tbl_user_playstat_gather_daily_amazon`
					WHERE statcode IN ( 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211) AND '2014-08-20' <= today AND today < '$search_end_createdate' AND purchase_passed >= 0
					GROUP BY statcode;";
	$amazon_result = $db_other->gettotallist($amazon_sql);
	
	$db_other->end();
?>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="yearly_month_stat.php">
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 게임 매출 이연 </div>
			<div class="search_box">			
				<select name="search_end_year" id="search_end_year">
				<?for ($i=2015; $i <= 2020; $i++ ) {?>
					<option value="<?=$i?>" <?= ($search_end_year == $i) ? "selected" : "" ?>><?=$i?></option>				
				<?}?>
				</select>년
				<select name="search_end_month" id="search_end_month">
				<?for( $i = 1; $i <= 12; $i++ ) { 
					$month_end_num = str_pad( $i, 2, 0, STR_PAD_LEFT );
				?>			
					<option value="<?=$month_end_num?>" <?= ($search_end_month == $month_end_num) ? "selected" : "" ?>><?=$month_end_num?></option>			
				<?}?>
				</select>월			
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
			</div>
		</div>
		<!-- //title_warp -->      
    </form>    	
	<div class="search_result">
		<span><?= $view_month ?></span>까지 통계입니다
	</div>
	<div class="h2_title">[Web]</div>
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">			
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">statcode</th>
					<th class="tdc">AVG( usercount * purchase_usd )</th>
					<th class="tdc">AVG( purchase_passed )</th>
				</tr>
			</thead>
			<tbody> 
	<?
			for($i=0; $i<sizeof($web_result); $i++)
			{
				$statcode = $web_result[$i]["statcode"];
				$A = $web_result[$i]["A"];
				$B = $web_result[$i]["B"];
	?>
				<tr>
					<td class="tdc point_title"><?= $statcode?></td>
					<td class="tdc"><?= number_format($A, 4) ?></td>
					<td class="tdc"><?= number_format($B, 4) ?></td>				
				</tr>
	<?
			}
	?>
			</tbody>
		</table>
	</div>	
	
	<br/><br/>
	
	<div class="h2_title">[Android]</div>
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">			
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">statcode</th>
					<th class="tdc">AVG( usercount * purchase_usd )</th>
					<th class="tdc">AVG( purchase_passed )</th>
				</tr>
			</thead>
			<tbody> 
	<?
			for($i=0; $i<sizeof($android_result); $i++)
			{
				$statcode = $android_result[$i]["statcode"];
				$A = $android_result[$i]["A"];
				$B = $android_result[$i]["B"];
	?>
				<tr>
					<td class="tdc point_title"><?= $statcode?></td>
					<td class="tdc"><?= number_format($A, 4) ?></td>
					<td class="tdc"><?= number_format($B, 4) ?></td>				
				</tr>
	<?
			}
	?>
			</tbody>
		</table>
	</div>
	
	<br/><br/>
	
	<div class="h2_title">[IOS]</div>
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">			
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">statcode</th>
					<th class="tdc">AVG( usercount * purchase_usd )</th>
					<th class="tdc">AVG( purchase_passed )</th>
				</tr>
			</thead>
			<tbody> 
	<?
			for($i=0; $i<sizeof($ios_result); $i++)
			{
				$statcode = $ios_result[$i]["statcode"];
				$A = $ios_result[$i]["A"];
				$B = $ios_result[$i]["B"];
	?>
				<tr>
					<td class="tdc point_title"><?= $statcode?></td>
					<td class="tdc"><?= number_format($A, 4) ?></td>
					<td class="tdc"><?= number_format($B, 4) ?></td>				
				</tr>
	<?
			}
	?>
			</tbody>
		</table>
	</div>
	
	<br/><br/>
	
	<div class="h2_title">[Amazon]</div>
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">			
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">statcode</th>
					<th class="tdc">AVG( usercount * purchase_usd )</th>
					<th class="tdc">AVG( purchase_passed )</th>
				</tr>
			</thead>
			<tbody> 
	<?
			for($i=0; $i<sizeof($amazon_result); $i++)
			{
				$statcode = $amazon_result[$i]["statcode"];
				$A = $amazon_result[$i]["A"];
				$B = $amazon_result[$i]["B"];
	?>
				<tr>
					<td class="tdc point_title"><?= $statcode?></td>
					<td class="tdc"><?= number_format($A, 4) ?></td>
					<td class="tdc"><?= number_format($B, 4) ?></td>				
				</tr>
	<?
			}
	?>
			</tbody>
		</table>
	</div>	

</div>

<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>	