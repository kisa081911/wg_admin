<?
	$top_menu = "management";
	$sub_menu = "daily_game_indicator";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d", strtotime("-1 day"));
	
	$search_viewmode = ($_GET["search_viewmode"] == "") ? 0 : $_GET["search_viewmode"];
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	
	if($search_start_createdate == "")
		$search_start_createdate = date("Y-m-d", strtotime("-2 day"));
	
	if($search_end_createdate == "")
		$search_end_createdate = $today;
	
	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT t1.writedate, t1.main_type, typeid, today, week, month, quarter, row_cnt, sub_row_cnt ".
			"FROM ( ".
			"	SELECT writedate, FLOOR(typeid/10) AS main_type, typeid, today, week, month, quarter ".
			"	FROM game_indicator_daily ".
			"	WHERE writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate' ".
			"	ORDER BY writedate DESC, typeid ASC ".
			") t1 LEFT JOIN ( ".
			"	SELECT writedate, COUNT(*) AS row_cnt ".
			"	FROM game_indicator_daily ".
			"	WHERE writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate' ".
			"	GROUP BY writedate ".
			") t2 ON t1.writedate = t2.writedate LEFT JOIN ( ".
			"	SELECT writedate, FLOOR(typeid/10) AS main_type, COUNT(*) AS sub_row_cnt ".
			"	FROM game_indicator_daily ".
			"	WHERE writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate' ".
			"	GROUP BY writedate, FLOOR(typeid/10) ".
			") t3 ON t1.writedate = t3.writedate AND t1.main_type = t3.main_type";
	
	$indicator_data = $db_analysis->gettotallist($sql);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="daily_game_indicator.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 일일 게임 지표</div>
		<div class="search_box">
			가감 표시&nbsp;
			<select name="search_viewmode" id="search_viewmode">
				<option value="0" <?= ($search_viewmode == "0") ? "selected" : "" ?>>퍼센트</option>
				<option value="1" <?= ($search_viewmode == "1") ? "selected" : "" ?>>수치</option>
			</select>&nbsp;&nbsp;
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
		<input type="button" class="btn_03" value="Excel 다운로드" onclick="window.location.href='daily_game_indicator_excel.php?start_createdate=<?= $search_start_createdate ?>&end_createdate=<?= $search_end_createdate ?>'">		
	</div>
	
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="80">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">구분</th>
					<th class="tdc">타입</th>
					<th class="tdc">당일</th>					
					<th class="tdc">전주</th>
					<th class="tdc">전월 평균</th>
					<th class="tdc">전분기 평균</th>
				</tr>
			</thead>
			<tbody>
<?
			$tmp_row_cnt = 0;
			$tmp_sub_row_cnt = 0;

			for($i=0; $i<sizeof($indicator_data); $i++)
			{
				$writedate = $indicator_data[$i]["writedate"];
				$typeid = $indicator_data[$i]["typeid"];
				$main_type = floor($typeid/10);
				$sub_type = $typeid%10;
				$today = $indicator_data[$i]["today"];				
				$week = $indicator_data[$i]["week"];
				$month = $indicator_data[$i]["month"];
				$quarter = $indicator_data[$i]["quarter"];
				$row_cnt = $indicator_data[$i]["row_cnt"];
				$sub_row_cnt = $indicator_data[$i]["sub_row_cnt"];
				
				if($main_type < 5)
				{
					$month = round($month/4);
					$quarter = round($quarter/12);
				}
							
				$week_rate = ($week == 0) ? 0 : round(($today / $week) * 100 - 100, 2);
				$month_rate = ($month == 0) ? 0 : round(($today / $month) * 100 - 100, 2);
				$quarter_rate = ($quarter == 0) ? 0 : round(($today / $quarter) * 100 - 100, 2);
				
				$main_name = "";
				
				$sub_point_count = 0;
				
				if($main_type == 1)
					$main_name = "1. 신규회원 가입수";
				else if($main_type == 2)
					$main_name = "2. 활동 유저수";
				else if($main_type == 3)
					$main_name = "3. 결제 유저수";
				else if($main_type == 4)
				{
					$main_name = "4. 결제 금액";
					
					$sub_point_count = 1;
					
					$today = round($today/10, $sub_point_count);					
					$week = round($week/10, $sub_point_count);
					$month = round($month/10, $sub_point_count);
					$quarter = round($quarter/10, $sub_point_count);
				}
				else if($main_type == 5)
				{
					$main_name = "5. ARPU";
					
					$sub_point_count = 3;
					
					$today = round($today/1000, $sub_point_count);					
					$week = round($week/1000, $sub_point_count);
					$month = round($month/1000, $sub_point_count);
					$quarter = round($quarter/1000, $sub_point_count);
				}
				else if($main_type == 6)
				{
					$main_name = "6. ARPPU";
						
					$sub_point_count = 3;
						
					$today = round($today/1000, $sub_point_count);					
					$week = round($week/1000, $sub_point_count);
					$month = round($month/1000, $sub_point_count);
					$quarter = round($quarter/1000, $sub_point_count);
				}
				
				$sub_name = "";
				
				if($sub_type == 0)
					$sub_name = "전체";
				else if($sub_type == 1)
					$sub_name = "Facebook";
				else if($sub_type == 2)
					$sub_name = "Apple";
				else if($sub_type == 3)
					$sub_name = "Google";
				else if($sub_type == 4)
					$sub_name = "Amazon";
				else if($sub_type == 5)
					$sub_name = "Trialpay";
				
				$week_style = "";
				$week_arrow = "";
				
				if(($today - $week) > 0)
				{
					$week_style = "color:red;";
					$week_arrow = ($search_viewmode == 0) ? "▲" : "+";
				}
				else if (($today - $week) < 0)
				{
					$week_style = "color:blue;";
					$week_arrow = ($search_viewmode == 0) ? "▼" : "";
				}
				
				$month_style = "";
				$month_arrow = "";
				
				if(($today - $month) > 0)
				{
					$month_style = "color:red;";
					$month_arrow = ($search_viewmode == 0) ? "▲" : "+";
				}
				else if (($today - $month) < 0)
				{
					$month_style = "color:blue;";
					$month_arrow = ($search_viewmode == 0) ? "▼" : "";
				}
				
				$quarter_style = "";
				$quarter_arrow = "";
				
				if(($today - $quarter) > 0)
				{
					$quarter_style = "color:red;";
					$quarter_arrow = ($search_viewmode == 0) ? "▲" : "+";
				}
				else if (($today - $quarter) < 0)
				{
					$quarter_style = "color:blue;";
					$quarter_arrow = ($search_viewmode == 0) ? "▼" : "";
				}
				
				if($search_viewmode == 0)
				{
					$week_value = $week_rate."%";
					$month_value = $month_rate."%";
					$quarter_value = $quarter_rate."%";
				}
				else
				{
					$week_value = number_format($today - $week, $sub_point_count);
					$month_value = number_format($today - $month, $sub_point_count);
					$quarter_value = number_format($today - $quarter, $sub_point_count);
				}
?>
				<tr>
<?
				if($tmp_row_cnt == 0)
				{
					$tmp_row_cnt = $row_cnt;
?>
					<td class="tdc point_title" rowspan="<?= $tmp_row_cnt?>"><?= $writedate ?></td>
<?
				}
				
				if($tmp_sub_row_cnt == 0)
				{
					$tmp_sub_row_cnt = $sub_row_cnt;
?>
					<td class="tdc point" rowspan="<?= $tmp_sub_row_cnt?>"><?= $main_name?></td>
<?
				}
?>
					<td class="tdc"><?= $sub_name ?></td>
					<td class="tdc"><?= ($main_type == 4) ? "$ " : "" ?><?= number_format($today, $sub_point_count) ?></td>
					<td class="tdc"><?= ($main_type == 4) ? "$ " : "" ?><?= number_format($week, $sub_point_count) ?>&nbsp;(<span style="<?= $week_style?>"><?= $week_arrow.$week_value ?></span>)</td>
					<td class="tdc"><?= ($main_type == 4) ? "$ " : "" ?><?= number_format($month, $sub_point_count) ?>&nbsp;(<span style="<?= $month_style?>"><?= $month_arrow.$month_value ?></span>)</td>
					<td class="tdc"><?= ($main_type == 4) ? "$ " : "" ?><?= number_format($quarter, $sub_point_count) ?>&nbsp;(<span style="<?= $quarter_style?>"><?= $quarter_arrow.$quarter_value ?></span>)</td>
				</tr>
<?
				$tmp_row_cnt--;
				$tmp_sub_row_cnt--;
			}
?>
			</tbody>
		</table>
 		</div>
        
     </form>
	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_analysis->end();

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>