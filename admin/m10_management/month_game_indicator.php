<?
	$top_menu = "management";
	$sub_menu = "month_game_indicator";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");	
	

	
	$search_viewmode = ($_GET["search_viewmode"] == "") ? 0 : $_GET["search_viewmode"];
	$search_start_year = $_GET["search_start_year"];
	$search_start_month = $_GET["search_start_month"]; 
	$search_end_year = $_GET["search_end_year"];
	$search_end_month = $_GET["search_end_month"];	
	
			
	if($search_start_year == "" && $search_start_month == "")
	{
		$search_start_year = date("Y", strtotime("-2 month"));
		$search_start_month = date("m", strtotime("-2 month"));
	}
	
	if($search_end_year == "" && $search_end_month == "")
	{
		$search_end_year = date("Y", strtotime("-1 month"));
		$search_end_month = date("m", strtotime("-1 month"));
	}
	
	$search_start_createdate = $search_start_year."-".$search_start_month;
	$search_end_createdate = $search_end_year."-".$search_end_month;
	
	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT t1.writedate, t1.main_type, typeid, month, before_month, before_two_month, row_cnt, sub_row_cnt ".
			"FROM ( ".
			"	SELECT writedate, FLOOR(typeid/10) AS main_type, typeid, month, before_month, before_two_month ".
			"	FROM game_indicator_month ".
			"	WHERE typeid NOT LIKE '7%' AND writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate' ".
			"	ORDER BY writedate DESC, typeid ASC ".
			") t1 LEFT JOIN ( ".
			"	SELECT writedate, COUNT(*) AS row_cnt ".
			"	FROM game_indicator_month ".
			"	WHERE typeid NOT LIKE '7%' AND writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate' ".
			"	GROUP BY writedate ".
			") t2 ON t1.writedate = t2.writedate LEFT JOIN ( ".
			"	SELECT writedate, FLOOR(typeid/10) AS main_type, COUNT(*) AS sub_row_cnt ".
			"	FROM game_indicator_month ".
			"	WHERE typeid NOT LIKE '7%' AND writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate' ".
			"	GROUP BY writedate, FLOOR(typeid/10) ".
			") t3 ON t1.writedate = t3.writedate AND t1.main_type = t3.main_type";
	
	$indicator_data = $db_analysis->gettotallist($sql);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="month_game_indicator.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 월별 게임 지표</div>
		<div class="search_box">
			가감 표시&nbsp;
			<select name="search_viewmode" id="search_viewmode">
				<option value="0" <?= ($search_viewmode == "0") ? "selected" : "" ?>>퍼센트</option>
				<option value="1" <?= ($search_viewmode == "1") ? "selected" : "" ?>>수치</option>
			</select>&nbsp;&nbsp;&nbsp;&nbsp;
			
			<select name="search_start_year" id="search_start_year">
			 <?for ($i=2014; $i <= 2020; $i++ ) {?>
				<option value="<?=$i?>" <?= ($search_start_year == $i) ? "selected" : "" ?>><?=$i?></option>				
			<?}?>
			</select>년
			<select name="search_start_month" id="search_start_month">
			<?for( $i = 1; $i <= 12; $i++ ) { 
				$month_start_num = str_pad( $i, 2, 0, STR_PAD_LEFT );
			?>			
				<option value="<?=$month_start_num?>" <?= ($search_start_month == $month_start_num) ? "selected" : "" ?>><?=$month_start_num?></option>			
			<?}?>
			</select>월				
			 ~
			<select name="search_end_year" id="search_end_year">
			<?for ($i=2014; $i <= 2020; $i++ ) {?>
				<option value="<?=$i?>" <?= ($search_end_year == $i) ? "selected" : "" ?>><?=$i?></option>				
			<?}?>
			</select>년
			<select name="search_end_month" id="search_end_month">
			<?for( $i = 1; $i <= 12; $i++ ) { 
				$month_end_num = str_pad( $i, 2, 0, STR_PAD_LEFT );
			?>			
				<option value="<?=$month_end_num?>" <?= ($search_end_month == $month_end_num) ? "selected" : "" ?>><?=$month_end_num?></option>			
			<?}?>
			</select>월
			
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
		<input type="button" class="btn_03" value="Excel 다운로드" onclick="window.location.href='month_game_indicator_excel.php?start_createdate=<?= $search_start_createdate ?>&end_createdate=<?= $search_end_createdate ?>'">				
	</div>
	
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="100">
				<col width="150">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">구분</th>
					<th class="tdc">타입</th>
					<th class="tdc">당월</th>
					<th class="tdc">전월</th>
					<th class="tdc">전전월</th>					
				</tr>
			</thead>
			<tbody>
<?
			$tmp_row_cnt = 0;
			$tmp_sub_row_cnt = 0;			

			for($i=0; $i<sizeof($indicator_data); $i++)
			{
				$writedate = $indicator_data[$i]["writedate"];
				$typeid = $indicator_data[$i]["typeid"];
				$main_type = floor($typeid/10);
				$sub_type = $typeid%10;
				$month = $indicator_data[$i]["month"];
				$before_month = $indicator_data[$i]["before_month"];
				$before_two_month = $indicator_data[$i]["before_two_month"];
				$row_cnt = $indicator_data[$i]["row_cnt"];
				$sub_row_cnt = $indicator_data[$i]["sub_row_cnt"];
				
				$before_month_rate = ($before_month == 0) ? 0 : round(($month / $before_month) * 100 - 100, 2);
				$before_two_month_rate = ($before_two_month == 0) ? 0 : round(($month / $before_two_month) * 100 - 100, 2);
				
				$main_name = "";
				
				$sub_point_count = 0;
				
				if($main_type == 1)
					$main_name = "1. 신규회원 가입수";
				else if($main_type == 2)
					$main_name = "2. 활동 유저수";
				else if($main_type == 3)
					$main_name = "3. 결제 유저수";
				else if($main_type == 4)
				{
					$main_name = "4. 결제 금액";
					
					$sub_point_count = 1;
										
					$month = round($month/10, $sub_point_count);
					$before_month = round($before_month/10, $sub_point_count);
					$before_two_month = round($before_two_month/10, $sub_point_count);
				}
				else if($main_type == 5)
				{
					$main_name = "5. ARPU";
					
					$sub_point_count = 3;
					
					$month = round($month/1000, $sub_point_count);
					$before_month = round($before_month/1000, $sub_point_count);
					$before_two_month = round($before_two_month/1000, $sub_point_count);
				}
				else if($main_type == 6)
				{
					$main_name = "6. ARPPU";
						
					$sub_point_count = 3;
						
					$month = round($month/1000, $sub_point_count);
					$before_month = round($before_month/1000, $sub_point_count);
					$before_two_month = round($before_two_month/1000, $sub_point_count);
				}
				
				$sub_name = "";
				
				if($sub_type == 0)
					$sub_name = "전체";
				else if($sub_type == 1)
					$sub_name = "Facebook";
				else if($sub_type == 2)
					$sub_name = "Apple";
				else if($sub_type == 3)
					$sub_name = "Google";
				else if($sub_type == 4)
					$sub_name = "Amazon";
				else if($sub_type == 5)
					$sub_name = "Trialpay";
				
				$before_month_style = "";
				$before_month_arrow = "";
				
				if(($month - $before_month) > 0)
				{
					$before_month_style = "color:red;";
					$before_month_arrow = ($search_viewmode == 0) ? "▲" : "+";
				}
				else if (($month - $before_month) < 0)
				{
					$before_month_style = "color:blue;";
					$before_month_arrow = ($search_viewmode == 0) ? "▼" : "";
				}
				
				$before_two_month_style = "";
				$before_two_month_arrow = "";
				
				if(($month - $before_two_month) > 0)
				{
					$before_two_month_style = "color:red;";
					$before_two_month_arrow = ($search_viewmode == 0) ? "▲" : "+";
				}
				else if (($month - $before_two_month) < 0)
				{
					$before_two_month_style = "color:blue;";
					$before_two_month_arrow = ($search_viewmode == 0) ? "▼" : "";
				}
				
				if($search_viewmode == 0)
				{
					$before_month_value = $before_month_rate."%";
					$before_two_month_value = $before_two_month_rate."%";
				}
				else
				{
					$before_month_value = number_format($month - $before_month, $sub_point_count);
					$before_two_month_value = number_format($month - $before_two_month, $sub_point_count);
				}
?>
				<tr>
<?
				if($tmp_row_cnt == 0)
				{
					$tmp_row_cnt = $row_cnt;
?>
					<td class="tdc point_title" rowspan="<?= $tmp_row_cnt?>"><?= $writedate ?></td>
<?
				}
				
				if($tmp_sub_row_cnt == 0)
				{
					$tmp_sub_row_cnt = $sub_row_cnt;
?>
					<td class="tdc point" rowspan="<?= $tmp_sub_row_cnt?>"><?= $main_name?></td>
<?
				}
?>
					<td class="tdc"><?= $sub_name ?></td>
					<td class="tdc"><?= ($main_type == 4) ? "$ " : "" ?><?= number_format($month, $sub_point_count) ?></td>
					<td class="tdc"><?= ($main_type == 4) ? "$ " : "" ?><?= number_format($before_month, $sub_point_count) ?>&nbsp;(<span style="<?= $before_month_style?>"><?= $before_month_arrow.$before_month_value ?></span>)</td>					
					<td class="tdc"><?= ($main_type == 4) ? "$ " : "" ?><?= number_format($before_two_month, $sub_point_count) ?>&nbsp;(<span style="<?= $before_two_month_style?>"><?= $before_two_month_arrow.$before_two_month_value ?></span>)</td>
				</tr>
<?
				$tmp_row_cnt--;
				$tmp_sub_row_cnt--;
			}
?>
			</tbody>
		</table>
 		</div>
        
     </form>
	
</div>

<!--  //CONTENTS WRAP -->
<?
	$db_analysis->end();

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>