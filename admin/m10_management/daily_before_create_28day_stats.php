<?
	$top_menu = "management";
	$sub_menu = "daily_before_create_28day_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d", strtotime("-1 day"));
	
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	
	if($search_start_createdate == "")
		$search_start_createdate = date("Y-m-d", strtotime("-7 day"));
	
	if($search_end_createdate == "")
		$search_end_createdate = $today;
	
	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT today, activecount, money, purchasecount FROM tbl_user_std_28day_daily ".
			"WHERE today BETWEEN '$search_start_createdate' AND '$search_end_createdate' ORDER BY today DESC;";
	
	$std_28day_data = $db_analysis->gettotallist($sql);
	
	$db_analysis->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="daily_before_create_28day_stats.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 가입 기준 28일 이후 통계</div>
		<div class="search_box">
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다.		
	</div>	
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">방문자</th>
					<th class="tdc">결제금액</th>
					<th class="tdc">ARPU</th>					
					<th class="tdc">ARPPU</th>					
				</tr>
			</thead>
			<tbody>
	<?
					$sum_activecount = 0;
					$sum_facebookcredit = 0;
					$sum_purchasecount = 0;
						
					for($i=0; $i<sizeof($std_28day_data); $i++)
					{
						$today = $std_28day_data[$i]["today"];							
						$activecount = $std_28day_data[$i]["activecount"];
						$money = $std_28day_data[$i]["money"];
						$purchasecount = $std_28day_data[$i]["purchasecount"];						
							
						$sum_activecount += $activecount;
						$sum_money += $money;
						$sum_purchasecount += $purchasecount;
						
						$money = round($money, 2);
						$arpu =  ($activecount == 0) ? 0 : round($money / $activecount, 3);
						$arppu = ($purchasecount == 0) ? 0 : round($money / $purchasecount, 3);
						
	?>
					
					<tr>
						<td class="tdc point_title"><?= $today ?></td>
						<td class="tdc"><?= number_format($activecount) ?></td>
						<td class="tdc">$<?= number_format($money, 2) ?></td>
						<td class="tdc"><?= number_format($arpu, 3) ?></td>
						<td class="tdc"><?= number_format($arppu, 3) ?></td>											
					</tr>
	<?	
					}					

					$sum_money = round($sum_money, 2);
					$sum_arpu =  ($sum_activecount == 0) ? 0 : round($sum_money / $sum_activecount, 3);
					$sum_arppu = ($sum_purchasecount == 0) ? 0 : round($sum_money / $sum_purchasecount, 3);
	?>				
					<tr>
						<td class="tdc point_title">TOTAL</td>
						<td class="tdc"><?= number_format($sum_activecount) ?></td>
						<td class="tdc">$<?= number_format($sum_money, 2) ?></td>
						<td class="tdc"><?= number_format($sum_arpu, 3) ?></td>
						<td class="tdc"><?= number_format($sum_arppu, 3) ?></td>											
					</tr>
			</tbody>
		</table>
 	</div>
        
     </form>
	
</div>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>