<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	check_login();
    
    if (strpos($login_adminmenu, "[".$top_menu."]") === false)
        error_back("접근 권한이 없습니다.");
        
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Take 5 Admin</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9, IE=8, IE=7" />
<meta name="viewport" content="width=1280px, initial-scale=1.0, minimum-scale=0.25,user-scalable=yes, target-densitydpi=medium-dpi" />
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script>
<script type="text/javascript" src="/js/jquery.bpopup.min.js"></script>
<script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/js/jquery.easing.compatibility.js"></script>
<script type="text/javascript" >
	var tmp_hosturl = "<?= HOST_URL ?>";

	function fnPopupClose()
	{
		$('#to_pop_up').bPopup().close();
	}
</script>
</head>

<body class="bg_body" onload="try{check_sleeptime_dashboard();} catch(e){}; try{window_onload();} catch(e){}">
<!-- FULL WRAP -->
<div class="full_wrap" style="position:relative;">
<div id="file_progress_info" style="position:absolute;z-index:500;left:350px;top:300px;" ></div>
	<!-- TOP -->
	<div class="top_area">
	
		<!--LOGO-->
		<div class="logo">
			<a href="javascript:window.location.href='/m_dashboard/dashboard_all.php'"><img src="/images/logo.png" style="width:159px;height:55px;margin-left:-10px;"></a>
<?
	if($login_adminid == "jumyung.choi" || $login_adminid == "sbkim" || $login_adminid == "kisa0819" || $login_adminid == "minewat")
	{
		$db_analysis = new CDatabase_Analysis();
	
		// 시스템 장애 알림 메일 발송 스케쥴러 체크
		$sql = "SELECT COUNT(*) FROM tbl_scheduler_log WHERE TYPE = 400 AND STATUS = 1 AND writedate >= DATE_SUB(NOW(), INTERVAL 11 MINUTE)";
		$alarm_scheduler_check = $db_analysis->getvalue($sql);
	
		$total_cnt = $system_alarm_cnt_info["total_cnt"];
		$send_cnt = $system_alarm_cnt_info["send_cnt"];
	
		$db_analysis->end();
?>
			<br/>알람 스케줄러 정상 동작 유무 ( <?= ($alarm_scheduler_check > 0) ? "<font style='font-weight:bold;color:blue'>O</font>" : "<font style='font-weight:bold;color:red'>X</font>" ?> )
<?
	}
?>
		</div>	
		<!-- // LOGO-->
	
		<!-- RIGHT LINK -->
		<div class="top_right_link_area">
			<div class="user_info">
				<a href="javascript:view_admin_dtl('<?= $login_adminidx ?>')" title="개인 정보 수정"><img src="/images/icon/user.png" align="absmiddle" /><?= $login_adminname ?>님(<?= $login_adminid ?>)</a> . <a href="/logout.php" title="로그아웃">LOGOUT</a>
			</div>
			<div class="user_info">
				현재서버시간 : <?= date("Y-m-d H:i:s"); ?><br/>
				미국동부시간 : <?= date("Y-m-d H:i:s", mktime(date("H")-5,date("i"),date("s"),date("m"),date("d"),date("Y"))); ?><br/>
				PST : <?= date("Y-m-d H:i:s", mktime(date("H")-8,date("i"),date("s"),date("m"),date("d"),date("Y"))); ?><br/>
				서울시간 : <?= date("Y-m-d H:i:s", mktime(date("H")+9,date("i"),date("s"),date("m"),date("d"),date("Y"))); ?>
			</div>
	  	</div>
		<!-- //RIGHT LINK -->
  	
  	</div>
  	<!-- //TOP -->
	  
    <!-- GNB -->
    <div class="gnb_wrap clear">
    	<ul class="gnb">
        	<li <?= ($top_menu == "product") ? "class='select'" : "" ?> <?= (strpos($login_adminmenu, "[product]") === false) ? "style='display:none'" : "" ?>><a href="/m1_product/order_mng.php?status=1" hidefocus><span>상품/주문 관리</span></a></li>
        	<li <?= ($top_menu == "user") ? "class='select'" : "" ?> <?= (strpos($login_adminmenu, "[user]") === false) ? "style='display:none'" : "" ?>><a href="/m2_user/user_mng.php?status=1" hidefocus><span>사용자 관리</span></a></li>
        	<li <?= ($top_menu == "user_static") ? "class='select'" : "" ?> <?= (strpos($login_adminmenu, "[user_static]") === false) ? "style='display:none;width:130px;'" : "style='width:130px;'" ?>><a href="/m3_user_static/user_stats.php" hidefocus><span>사용자 통계</span></a></li>
        	<li <?= ($top_menu == "pay_static") ? "class='select'" : "" ?> <?= (strpos($login_adminmenu, "[pay_static]") === false) ? "style='display:none;width:130px;'" : "style='width:130px;'" ?>><a href="/m4_pay_static/pay_stat/pay_stat_all.php" hidefocus><span>결제 통계</span></a></li>
        	<li <?= ($top_menu == "game_stats") ? "class='select'" : "" ?> <?= (strpos($login_adminmenu, "[game_stats]") === false) ? "style='display:none'" : "" ?>><a href="/m5_game_stats/game_total_stats.php" hidefocus><span>게임 통계</span></a></li>
        	<li <?= ($top_menu == "marketing") ? "class='select'" : "" ?> <?= (strpos($login_adminmenu, "[marketing]") === false) ? "style='display:none'" : "" ?>><a href="/m12_marketing/marketing_dashboard.php" hidefocus><span>마케팅 통계</span></a></li>
        	<li <?= ($top_menu == "game") ? "class='select'" : "" ?> <?= (strpos($login_adminmenu, "[game]") === false) ? "style='display:none'" : "" ?>><a href="/m6_game/event.php" hidefocus><span>게임 관리</span></a></li>
        	<li <?= ($top_menu == "customer") ? "class='select'" : "" ?> <?= (strpos($login_adminmenu, "[customer]") === false) ? "style='display:none'" : "" ?>><a href="/m7_customer/qa_dashboard.php" hidefocus><span>고객 서비스</span></a></li>
        	<li <?= ($top_menu == "monitoring") ? "class='select'" : "" ?> <?= (strpos($login_adminmenu, "[monitoring]") === false) ? "style='display:none'" : "" ?>><a href="/m8_monitoring/total_stats.php" hidefocus><span>시스템 모니터링</span></a></li>        	
        	<li <?= ($top_menu == "management") ? "class='select'" : "" ?> <?= (strpos($login_adminmenu, "[management]") === false) ? "style='display:none'" : "" ?>><a href="/m10_management/daily_game_indicator.php" hidefocus><span>경영기획 관리</span></a></li>
        	<li <?= ($top_menu == "admin") ? "class='select'" : "" ?> <?= (strpos($login_adminmenu, "[admin]") === false) ? "style='display:none'" : "" ?>><a href="/m11_admin/admin_mng.php" hidefocus><span>시스템 관리</span></a></li>
        	<li <?= ($top_menu == "deploy") ? "class='select'" : "" ?> <?= (strpos($login_adminmenu, "[deploy]") === false) ? "style='display:none'" : "" ?>><a href="/m11_admin/resource_mng.php" hidefocus><span>배포 관리</span></a></li>
        </ul>
    </div>
    <!-- //GNB -->

	<!-- CONTENTS WRAP -->
	<div class="gnb_title_wrap">
	    <div class="gnb_title">
<?
	if ($top_menu == "product")
		$top_menu_txt = "상품/주문 관리";
	else if ($top_menu == "user")
		$top_menu_txt = "사용자 관리";
	else if ($top_menu == "user_static")
		$top_menu_txt = "사용자 통계";
    else if ($top_menu == "pay_static")
        $top_menu_txt = "결제 통계"; 
    else if ($top_menu == "monitoring")
        $top_menu_txt = "시스템 모니터링";
    else if ($top_menu == "game")
        $top_menu_txt = "게임 관리";
    else if ($top_menu == "game_stats")
        $top_menu_txt = "게임 통계";
    else if ($top_menu == "management")
        $top_menu_txt = "경영기획 관리";
    else if ($top_menu == "customer")
        $top_menu_txt = "고객 서비스";
    else if ($top_menu == "admin")
    	$top_menu_txt = "시스템 관리";
    else if ($top_menu == "marketing")
    	$top_menu_txt = "마케팅 통계";
    else if ($top_menu == "deploy")
    	$top_menu_txt = "리소스 관리";
    echo($top_menu_txt);
?>
	   </div>
	</div>
	
	<!-- MAIN WRAP -->
    <div class="main_wrap">
<?
    if ($top_menu != "dashboard")
    {
?>
    	<div class="lnb_wrap">
<?
	if ($top_menu == "product")
	{
?>
    		<div class="lnb<?= ($sub_menu == "order") ? "_select" : "" ?>" onclick="go_page('/m1_product/order_mng.php?status=1')">Web 주문관리</div>    		
    		<div class="lnb<?= ($sub_menu == "product") ? "_select" : "" ?>" onclick="go_page('/m1_product/product_mng.php?status=1')">Web 상품관리</div>
    		<div class="lnb<?= ($sub_menu == "order_earn") ? "_select" : "" ?>" onclick="go_page('/m1_product/order_earn_mng.php?status=1')">Earn 주문관리</div>
            <div class="lnb<?= ($sub_menu == "order_dispute") ? "_select" : "" ?>" onclick="go_page('/m1_product/order_dispute.php?iscomplete=0')">주문 dispute 관리</div>
            <div class="lnb<?= ($sub_menu == "order_ios") ? "_select" : "" ?>" onclick="go_page('/m1_product/order_ios_mng.php?status=1')">Ios 주문관리</div>    		
    		<div class="lnb<?= ($sub_menu == "product_ios") ? "_select" : "" ?>" onclick="go_page('/m1_product/product_ios_mng.php?status=1')">Ios 상품관리</div>
    		<div class="lnb<?= ($sub_menu == "order_android") ? "_select" : "" ?>" onclick="go_page('/m1_product/order_android_mng.php?status=1')">Android 주문관리</div>    		
    		<div class="lnb<?= ($sub_menu == "product_android") ? "_select" : "" ?>" onclick="go_page('/m1_product/product_android_mng.php?status=1')">Android 상품관리</div>
    		<div class="lnb<?= ($sub_menu == "order_amazon") ? "_select" : "" ?>" onclick="go_page('/m1_product/order_amazon_mng.php?status=1')">Amazon 주문관리</div>    		
    		<div class="lnb<?= ($sub_menu == "product_amazon") ? "_select" : "" ?>" onclick="go_page('/m1_product/product_amazon_mng.php?status=1')">Amazon 상품관리</div>
<?
	}
	else if ($top_menu == "user")
	{
?>
    		<div class="lnb<?= ($sub_menu == "user_mng") ? "_select" : "" ?>" onclick="go_page('/m2_user/user_mng.php')">사용자관리</div>
    		<div class="lnb<?= ($sub_menu == "redmine_action") ? "_select" : "" ?>" onclick="go_page('/m2_user/redmine_action.php')">행동로그 블랙박스 관리</div>
    		<div class="lnb<?= ($sub_menu == "user_login_log") ? "_select" : "" ?>" onclick="go_page('/m2_user/user_login_log.php')">사용자 로그인로그</div>
    		<div class="lnb<?= ($sub_menu == "user_freecoin_mng") ? "_select" : "" ?>" onclick="go_page('/m2_user/freecoin_mng.php')">관리자발급 자원관리</div>
<?
	}
	else if ($top_menu == "user_static")
	{
?>
			<div class="lnb<?= ($sub_menu == "user_stats") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/user_stats.php')">사용자 통계</div>
			<div class="lnb<?= ($sub_menu == "bgu_featuring_stat") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/bgu_featuring_stat.php')">BGU 피처링 현황</div>
			<div class="lnb_sub<?= ($sub_menu == "bgu_featuring_stat_ext") ? "_select_end" : "_end" ?>" onclick="go_page('/m3_user_static/bgu_featuring_stat_ext.php')">BGU 피처링 상세현황</div>
			<div class="lnb<?= ($sub_menu == "user_maintain") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/user_maintain.php')">사용자 유지현황(Web)</div>
			<div class="lnb<?= ($sub_menu == "user_maintain_new") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/user_inflowroute_stats.php')">사용자 유지현황(전체)</div>
			<div class="lnb<?= ($sub_menu == "user_inflowroute_stats_daily") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/user_inflowroute_stats_daily.php')">사용자 유지현황(일간)</div>			
			<div class="lnb<?= ($sub_menu == "user_retention_detail") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/user_retention_detail.php')">사용자 유지현황(세부)</div>
			<div class="lnb<?= ($sub_menu == "user_retention_detail_daily" || $sub_menu == "user_pay_retention_daily" || $sub_menu == "user_retention_detail_daily_qa" ) ? "_select" : "" ?>" onclick="go_page('/m3_user_static/user_retention_detail_daily.php')">Retention</div>
            <div class="lnb_sub<?= ($sub_menu == "user_retention_detail_daily") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/user_retention_detail_daily.php')">가입자</div>
            <div class="lnb_sub<?= ($sub_menu == "user_pay_retention_daily") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/user_pay_retention_daily.php')">첫 결제자</div>
            <div class="lnb_sub<?= ($sub_menu == "user_30day_retention_daily") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/user_30day_retention_daily.php')">30일 이탈자</div>
	                <div class="lnb_sub<?= ($sub_menu == "user_retention_detail_daily_qa") ? "_select_end" : "_end" ?>" onclick="go_page('/m3_user_static/user_retention_detail_daily_qa.php')">일일 가입자</div>
			<div class="lnb<?= ($sub_menu == "adflag_detail") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/adflag_detail.php')">유입경로별 유입 현황</div>
			<div class="lnb<?= ($sub_menu == "adflag_detail_analysis") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/adflag_detail_analysis.php')">유입경로 유입 상세분석</div>
			<div class="lnb<?= ($sub_menu == "adflag_viral_detail") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/adflag_viral_detail.php')">Viral 유입 현황</div>
			<div class="lnb<?= ($sub_menu == "adflag_game_viral_detail") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/adflag_game_viral_detail.php')">Game Viral 유입 현황</div>
			<div class="lnb<?= ($sub_menu == "adflag_mobile_detail") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/adflag_mobile_detail.php')">Mobile 유입 현황</div>				
			<div class="lnb<?= ($sub_menu == "adflag_fbretention_retention" || $sub_menu == "adflag_fbretention_a2u" || $sub_menu == "adflag_fbretention_stats" || $sub_menu == "adflag_fbretention_mobile_stats") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/adflag_fbretention_retention.php')">Retention 복귀현황</div>
            <div class="lnb_sub<?= ($sub_menu == "adflag_fbretention_retention") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/adflag_fbretention_retention.php')">Retention</div>
            <div class="lnb_sub<?= ($sub_menu == "adflag_fbretention_a2u") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/adflag_fbretention_a2u.php')">A2U</div>
            <!--div class="lnb_sub<?= ($sub_menu == "adflag_fbretention_stats") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/adflag_fbretention_stats.php')">통합 - Web</div-->
			<div class="lnb_sub<?= ($sub_menu == "adflag_fbretention_mobile_stats") ? "_select_end" : "_end" ?>" onclick="go_page('/m3_user_static/adflag_fbretention_mobile_stats.php')">통합 - Mobile</div>
			<div class="lnb<?= ($sub_menu == "user_coin_stats") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/user_coin_stats.php')">보유 코인 통계</div>
			<div class="lnb<?= ($sub_menu == "user_unpay_avg_coin") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/user_unpay_avg_coin.php')">미결제 평균 코인</div>
			<div class="lnb<?= ($sub_menu == "user_vip_level_stats") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/user_vip_level_stats.php')">Vip 인원 통계</div>
			<div class="lnb<?= ($sub_menu == "user_honor_level_stats") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/user_honor_level_stats.php')">Fame 인원 통계</div>
			<div class="lnb<?= ($sub_menu == "join_leave_stats" || $sub_menu == "leave_stats") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/join_leave_stats.php')">이탈자 통계</div>
			<div class="lnb_sub<?= ($sub_menu == "join_leave_stats") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/join_leave_stats.php')">신규유입 이탈</div>
            <div class="lnb_sub<?= ($sub_menu == "leave_stats") ? "_select_end" : "_end" ?>" onclick="go_page('/m3_user_static/leave_stats.php')">기존유저 이탈</div>	
            <div class="lnb<?= ($sub_menu == "mobile_push_enable_day_stats") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/mobile_push_enable_day_stats.php')">Mobile Push Enable</div>
			<div class="lnb_sub<?= ($sub_menu == "mobile_push_enable_day_stats") ? "_select_end" : "_end" ?>" onclick="go_page('/m3_user_static/mobile_push_enable_day_stats.php')">일별 통계</div>	
			<div class="lnb<?= ($sub_menu == "wakeup_user_stat") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/wakeup_user_stat.php')">Wakeup 복귀 유저 통계</div>
			<div class="lnb<?= ($sub_menu == "push_event_stat") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/push_event_stat.php')">Push Event 통계</div>
			<div class="lnb<?= ($sub_menu == "pay_user_stats") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/pay_user_stats.php')">결제 이탈/복귀 현황</div>
			<div class="lnb<?= ($sub_menu == "mobile_version_daily") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/mobile_version_daily.php')">모바일 버전 통계</div>
			<div class="lnb<?= ($sub_menu == "mobile_os_version_daily") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/mobile_os_version_daily.php')">모바일 OS버전 통계</div>
			<div class="lnb<?= ($sub_menu == "conversion_rate_stat") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/conversion_rate_stat.php')">모바일 전환율 현황</div>
			<div class="lnb<?= ($sub_menu == "userpayment_stats") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/userpayment_stats.php')">사용자 유형별 통계</div>
			<div class="lnb<?= ($sub_menu == "user_switch_daily_stat") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/user_switch_daily_stat.php')">2.0 전환율 통계</div>
							
<?
	}
	else if ($top_menu == "pay_static")
	{
?>
			<div class="lnb<?= ($sub_menu == "pay_stat") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/pay_stat/pay_stat_all.php')">결제 통계(New)</div>
			<div class="lnb<?= ($sub_menu == "pay_static" || $sub_menu == "pay_time" || $sub_menu == "pay_month" || $sub_menu == "pay_nation" || $sub_menu == "pay_time") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/pay_static_new.php')">결제 통계</div>
			<div class="lnb_sub<?= ($sub_menu == "pay_static") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/pay_static_new.php')">기간별 통계</div>
			<div class="lnb_sub<?= ($sub_menu == "pay_month_new") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/pay_month_new.php')">월별 통계</div>
			<div class="lnb_sub<?= ($sub_menu == "pay_time") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/pay_time.php')">시간, 요일별 통계</div>
			<div class="lnb_sub<?= ($sub_menu == "pay_nation") ? "_select_end" : "_end" ?>" onclick="go_page('/m4_pay_static/pay_nation.php')">국가, 일별 통계</div>
			<div class="lnb<?= ($sub_menu == "daily_product_stat" || $sub_menu == "daily_product_stats_mobile") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/daily_product_stats.php')">일별 상품 통계</div>
			<div class="lnb_sub<?= ($sub_menu == "daily_product_stat") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/daily_product_stats.php')">Web</div>
			<div class="lnb_sub<?= ($sub_menu == "daily_product_stats_mobile") ? "_select_end" : "_end" ?>" onclick="go_page('/m4_pay_static/daily_product_stats_mobile.php')">Mobile</div>
			<!-- div class="lnb<?= ($sub_menu == "pay_static_adflag_mobile") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/pay_static_adflag_mobile.php')">모바일 유입 결제 통계</div>
			<div class="lnb<?= ($sub_menu == "pay_static_mobile_guest") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/pay_static_mobile_guest.php')">모바일 게스트 결제 통계</div-->
			<div class="lnb<?= ($sub_menu == "payer_stats") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/payer_stats.php')">결제 경험자 결제 현황</div>
			<div class="lnb<?= ($sub_menu == "pay_month_join") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/pay_month_join.php')">월별결제 가입자 분포</div>
			<div class="lnb<?= ($sub_menu == "pay_month_first") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/pay_month_first.php')">월별 첫결제 분포</div>		
			<div class="lnb<?= ($sub_menu == "daily_repayment_stats") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/daily_repayment_stats.php')">재결제 현황</div>
			<div class="lnb<?= ($sub_menu == "buyer_grade_stat") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/buyer_grade_stat.php')">결제자 세그먼트 현황</div>
			<div class="lnb<?= ($sub_menu == "pay_4week_stats") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/pay_4week_stats.php')">최근 28일 결제 통계</div>
			<div class="lnb<?= ($sub_menu == "payer_4w_stat") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/payer_stat/payer_4w_daily_stat_all.php')">최근 28일 결제자 통계</div>
			<div class="lnb<?= ($sub_menu == "pay_100per_morechips_stats") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/pay_100per_morechips_stats.php')">100% More Chips 통계</div>
			<div class="lnb<?= ($sub_menu == "pay_attendoffer_static_daily" || $sub_menu == "pay_attendoffer_bonus_static_daily") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/pay_attendoffer_static_daily.php')">기간제 오퍼 통계</div>
			<div class="lnb_sub<?= ($sub_menu == "pay_attendoffer_static_daily") ? "_select" : "" ?>" onclick="go_page('/m4_pay_static/pay_attendoffer_static_daily.php')">대상자 통계</div>
			<div class="lnb_sub<?= ($sub_menu == "pay_attendoffer_bonus_static_daily") ? "_select_end" : "_end" ?>" onclick="go_page('/m4_pay_static/pay_attendoffer_bonus_static_daily.php')">보너스 통계</div>
<?
	}
    else if ($top_menu == "game_stats")
    {
?>
            <div class="lnb<?= ($sub_menu == "game_total_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_total_stats.php')">금일 게임 접속/활동 </div>            
            <div class="lnb<?= ($sub_menu == "slot_rank_type0" ||$sub_menu == "slot_rank_type1" || $sub_menu == "slot_rank_type2"||$sub_menu == "slot_rank_type3") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/slot_rank/slot_rank_type.php?type=0')">주간 슬롯 순위</div>
            <div class="lnb_sub<?= ($sub_menu == "slot_rank_type0") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/slot_rank/slot_rank_type.php?type=0')">최근 1주 결제자</div>
            <div class="lnb_sub<?= ($sub_menu == "slot_rank_type1") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/slot_rank/slot_rank_type.php?type=1')">최근 4주 가입자</div>
            <div class="lnb_sub<?= ($sub_menu == "slot_rank_type2") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/slot_rank/slot_rank_type.php?type=2')">최근 4주 복귀자</div>
            <div class="lnb_sub<?= ($sub_menu == "slot_rank_type3") ? "_select_end" : "_end" ?>" onclick="go_page('/m5_game_stats/slot_rank/slot_rank_type.php?type=3')">최근 1주 결제복귀자</div>
            <div class="lnb<?= ($sub_menu == "game_slot_playcount_stats" || $sub_menu == "game_slot_money_stats" || $sub_menu == "game_slot_daily_stats" || $sub_menu == "game_slot_summarystate_daily" || $sub_menu == "game_slot_daily_mode_stats" || $sub_menu == "game_slot_online_log_stats" || $sub_menu == "daily_free_stat" || $sub_menu == "daily_freecoin_detail_stats" || $sub_menu == "daily_treat_ticket_stats" || $sub_menu == "daily_ty_stats" || $sub_menu == "daily_treat_loyalty_stats" || $sub_menu == "game_24hour_slot_rate" || $sub_menu == "game_slot_winrate_term" || $sub_menu == "recent_slot_unitbet_stats"|| $sub_menu == "loyaltypot_treat_stat" || $sub_menu == "game_action_stats3_mode2528"|| $sub_menu == "game_slot_daily_statsV2") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_slot_playcount_stats.php')">게임 활동 추이</div>
            <div class="lnb_sub<?= ($sub_menu == "game_slot_playcount_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_slot_playcount_stats.php')">게임 횟수 추이</div>
            <div class="lnb_sub<?= ($sub_menu == "game_slot_money_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_slot_money_stats.php')">게임 금액별 추이</div>
            <div class="lnb_sub<?= ($sub_menu == "game_slot_daily_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_slot_daily_stats.php')">게임 일별 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "game_action_stats3_mode2528") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_action_stats3_mode2528.php')">일별 통계(mode25,28)</div>
            <div class="lnb_sub<?= ($sub_menu == "game_action_stats3_d3_all") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_action_stats3_d3_all.php')">일별 통계 D3</div>
            <div class="lnb_sub<?= ($sub_menu == "game_slot_vip_daily_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_slot_vip_daily_stats.php')">게임 Vip 일별 통계</div>            
            <div class="lnb_sub<?= ($sub_menu == "game_slot_daily2_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_slot_daily2_stats.php')">게임 일별 승률 통계</div>
            <!--div class="lnb_sub<?= ($sub_menu == "game_slot_summarystate_daily") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_slot_summarystate_daily.php')">게임 일별 요약통계</div-->
            <div class="lnb_sub<?= ($sub_menu == "game_slot_daily_mode_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_slot_daily_mode_stats.php')">Mode별 승률 비중</div>
            <div class="lnb_sub<?= ($sub_menu == "game_24hour_slot_rate") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_24hour_slot_rate.php')">최근 단위 승률 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "recent_slot_unitbet_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/recent_slot_unitbet_stats.php')">단위 승률 (승률 변동)</div>                     
            <div class="lnb_sub<?= ($sub_menu == "game_slot_winrate_term") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_slot_winrate_term.php')">게임 어제/1주일 통계</div>              
          	<div class="lnb_sub<?= ($sub_menu == "game_slot_online_log_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_slot_online_log_stats.php')">사용자 접속 통계</div>
          	<div class="lnb_sub<?= ($sub_menu == "daily_free_stat") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/daily_free_stat.php')">무료코인 일별 통계</div>          	
          	<div class="lnb_sub<?= ($sub_menu == "daily_freecoin_detail_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/daily_freecoin_detail_stats.php')">무료코인 상세 통계</div>
          	<div class="lnb_sub<?= ($sub_menu == "daily_inbox_free_stat") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/daily_inbox_free_stat.php')">Inbox 통계</div>
          	<div class="lnb_sub<?= ($sub_menu == "daily_treat_ticket_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/daily_treat_ticket_stats.php')">Treat Ticket 통계</div>
			<div class="lnb_sub<?= ($sub_menu == "loyaltypot_treat_stat") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/loyaltypot_treat_stat.php')">Treat loyalty 밸런스</div>
          	<div class="lnb_sub<?= ($sub_menu == "daily_treat_loyalty_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/daily_treat_loyalty_stats.php')">Treat Loyalty 통계</div>
          	<div class="lnb_sub<?= ($sub_menu == "daily_ty_stats") ? "_select_end" : "" ?>" onclick="go_page('/m5_game_stats/daily_ty_stats.php')">TY Point 통계</div>
          	<!--div class="lnb_sub<?= ($sub_menu == "game_bigwin_event_stats") ? "_select_end" : "_end" ?>" onclick="go_page('/m5_game_stats/game_bigwin_event_stats.php')">빅윈 이벤트 통계</div-->           	
          	<div class="lnb<?= ($sub_menu == "daily_free_stat_graph") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/daily_free_stat_graph.php')">일별 통계(Graph)</div>          	          	
          	<div class="lnb_sub<?= ($sub_menu == "daily_free_stat_graph") ? "_select_end" : "_end" ?>" onclick="go_page('/m5_game_stats/daily_free_stat_graph.php')">무료코인 통계</div>          	
          	<div class="lnb<?= ($sub_menu == "game_top_jp_winners_stats" || $sub_menu == "game_daily_jackpot_stats" || $sub_menu == "game_jackpot_save_amount_detail" || $sub_menu == "game_jackpot_save_amount_highroller_detail" || $sub_menu == "game_jackpot_detail_save_daily" || $sub_menu == "game_jackpot_deduction_info") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_top_jp_winners_stats.php')">잭팟 현황</div>          	
          	<div class="lnb_sub<?= ($sub_menu == "game_top_jp_winners_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_top_jp_winners_stats.php')">잭팟 상세 현황</div>
          	<div class="lnb_sub<?= ($sub_menu == "game_daily_jackpot_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_daily_jackpot_stats.php')">잭팟 일별 현황</div>
          	<div class="lnb_sub<?= ($sub_menu == "game_jackpot_fiesta_playcount_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_jackpot_fiesta_playcount_stats.php')">잭팟피에스타</div>
         	<div class="lnb_sub<?= ($sub_menu == "game_jackpot_fiesta_ratio_change_new") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_jackpot_fiesta_ratio_change_new.php')">잭팟피에스타 확률조정</div>
          	<div class="lnb_sub<?= ($sub_menu == "game_fiesta_jp_winners_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_fiesta_jp_winners_stats.php')">잭팟피에스타 현황</div>          	
          	<div class="lnb_sub<?= ($sub_menu == "game_jackpot_save_amount_detail") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_jackpot_save_amount_detail.php')">누적금액(레귤러)</div>
          	<div class="lnb_sub<?= ($sub_menu == "game_jackpot_save_amount_highroller_detail") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_jackpot_save_amount_highroller_detail.php')">누적금액(하이롤러)</div>
			<div class="lnb_sub<?= ($sub_menu == "game_jackpot_detail_save_daily") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_jackpot_detail_save_daily.php')">일별 누적 금액</div>
<?
		if($login_adminid == "admin" || $login_adminid == "yhjung")
		{
?>
		
          	<div class="lnb_sub<?= ($sub_menu == "game_jackpot_deduction_info") ? "_select_end" : "_end" ?>" onclick="go_page('/m5_game_stats/game_jackpot_deduction_info.php')">Jackpot Deduction</div>
<?
		}
		else
		{
?>
		<div class="lnb_sub<?= ($sub_menu == "game_jackpot_save_amount_highroller_detail") ? "_select_end" : "_end" ?>" onclick="go_page('/m5_game_stats/game_jackpot_save_amount_highroller_detail.php')">누적금액(하이롤러)</div>
<?
		}
?>
          	<div class="lnb<?= ($sub_menu == "game_matching_coin") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_matching_coin_new.php')">코인 정합성 목록</div>          	          	          	
          	<div class="lnb<?= ($sub_menu == "game_winrate_change_stat_slot"||$sub_menu == "game_winrate_change_stat_slot_ios"||$sub_menu == "game_winrate_change_stat_slot_android"||$sub_menu == "game_winrate_change_stat_slot_amazon") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_winrate_change_stat_slot.php')">승률 조정 후 추이</div>
          	<div class="lnb_sub<?= ($sub_menu == "game_winrate_change_stat_slot") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_winrate_change_stat_slot.php')">Web</div>
          	<div class="lnb_sub<?= ($sub_menu == "game_winrate_change_stat_slot_ios") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_winrate_change_stat_slot_ios.php')">Ios</div>
          	<div class="lnb_sub<?= ($sub_menu == "game_winrate_change_stat_slot_android") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_winrate_change_stat_slot_android.php')">Android</div>
          	<div class="lnb_sub<?= ($sub_menu == "game_winrate_change_stat_slot_amazon") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_winrate_change_stat_slot_amazon.php')">Amazon</div>
          	<div class="lnb<?= ($sub_menu == "game_slot_empty_seat") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_slot_empty_seat.php')">슬롯 빈자리 현황</div>
          	<div class="lnb<?= ($sub_menu == "game_mobile_push_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_mobile_push_stats.php')">Mobile Push 통계</div>
          	<div class="lnb_sub<?= ($sub_menu == "game_jackpot_save_amount_detail") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_jackpot_save_amount_detail.php')">누적금액(레귤러)</div>          	
<?
		if($login_adminid == "admin" || $login_adminid == "yhjung")
		{
?>
		<div class="lnb_sub<?= ($sub_menu == "game_jackpot_save_amount_highroller_detail") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_jackpot_save_amount_highroller_detail.php')">누적금액(하이롤러)</div>
          	<div class="lnb_sub<?= ($sub_menu == "game_jackpot_deduction_info") ? "_select_end" : "_end" ?>" onclick="go_page('/m5_game_stats/game_jackpot_deduction_info.php')">Jackpot Deduction</div>
<?
		}
		else
		{
?>
		<div class="lnb_sub<?= ($sub_menu == "game_jackpot_save_amount_highroller_detail") ? "_select_end" : "_end" ?>" onclick="go_page('/m5_game_stats/game_jackpot_save_amount_highroller_detail.php')">누적금액(하이롤러)</div>
<?
		}
?>
          	<!--div class="lnb<?= ($sub_menu == "game_matching_coin") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_matching_coin.php')">코인 정합성 목록</div>
          	<div class="lnb<?= ($sub_menu == "game_winrate_change_stat_slot") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_winrate_change_stat_slot.php')">승률 조정 후 추이</div>
          	<div class="lnb<?= ($sub_menu == "game_slot_empty_seat") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_slot_empty_seat.php')">슬롯 빈자리 현황</div-->          	
          	<div class="lnb<?= ($sub_menu == "game_coupon_stat" || $sub_menu == "game_coupon_stat_graph") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_coupon_stat.php')">쿠폰 현황</div>
            <div class="lnb_sub<?= ($sub_menu == "game_coupon_stat") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_coupon_stat.php')">일별 쿠폰 현황</div>
            <div class="lnb_sub<?= ($sub_menu == "game_coupon_stat_graph") ? "_select_end" : "_end" ?>" onclick="go_page('/m5_game_stats/game_coupon_stat_graph.php')">일별 쿠폰 현황(그래프)</div>
            <div class="lnb<?= ($sub_menu == "daily_highroller_join_week_stats" || $sub_menu == "daily_highroller_currentcoin_stats" || $sub_menu == "daily_highroller_famelevel_stats" || $sub_menu == "daily_highroller_viplevel_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/daily_highroller_join_week_stats.php')">하이롤러 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "daily_highroller_join_week_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/daily_highroller_join_week_stats.php')">가입 주 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "daily_highroller_currentcoin_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/daily_highroller_currentcoin_stats.php')">현재 유저 코인 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "daily_highroller_famelevel_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/daily_highroller_famelevel_stats.php')">FameLevel 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "daily_highroller_viplevel_stats") ? "_select_end" : "_end" ?>" onclick="go_page('/m5_game_stats/daily_highroller_viplevel_stats.php')">VipLevel 통계</div>
            <div class="lnb<?= ($sub_menu == "guest_connect_facebook_stat") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/guest_connect_facebook_stat.php')">게스트 페북 연동 현황</div>
            <div class="lnb<?= ($sub_menu == "game_super_wheel_party_stat") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_super_wheel_party_stat.php')">Super Wheel 통계</div>
            <!-- <div class="lnb<?= ($sub_menu == "action_log_stats") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/action_log_stats.php')">행동 분석 통계</div> -->

            <div class="lnb<?= ($sub_menu == "game_mega_coupon_stat") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/game_mega_coupon_stat.php')">메가 쿠폰 현황</div>          	
         	<div class="lnb<?= ($sub_menu == "daily_freespin_stat") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/daily_freespin_stat.php')">DailyFreespin 현황</div>       
         	<div class="lnb<?= ($sub_menu == "metaevent_stat_daily_detail") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/metaevent_stat_daily_detail.php')">Meta Event 통계</div>          	          	
		    <!-- 	    <div class="lnb<?= ($sub_menu == "daily_hocus_pocus_stat") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/daily_hocus_pocus_stat.php')">Hocus Pocus 통계</div>
          	<div class="lnb<?= ($sub_menu == "christmas_stat_daily" || $sub_menu == "christmas_stat_daily_detail") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/christmas_stat_daily.php')">Christmas Event 통계</div>          	          	
         	<div class="lnb_sub<?= ($sub_menu == "christmas_stat_daily_detail" ) ? "_select_end" : "" ?>" onclick="go_page('/m5_game_stats/christmas_stat_daily_detail.php')">Christmas Event 통계(상세)</div-->          	          	
          	<!-- div class="lnb_sub<?= ($sub_menu == "thanksgiving_stat_daily") ? "_select_end" : "_end" ?>" onclick="go_page('/m5_game_stats/thanksgiving_stat_daily.php')">Thanksgiving <br/>상세 통계</div -->
			<div class="lnb<?= ($sub_menu == "ab_test") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/ab_test/ab_test_list.php')">AB Test 현황</div>    
			<div class="lnb<?= ($sub_menu == "collectionevent_stat_daily_detail") ? "_select" : "" ?>" onclick="go_page('/m5_game_stats/collectionevent_stat_daily_detail.php')">Collection 통계</div>
<?
    } 
    else if ($top_menu == "marketing")
    {
?>
			<div class="lnb<?= ($sub_menu == "dashboard") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/marketing_dashboard.php')">대시보드</div>
			<div class="lnb<?= ($sub_menu == "ua_marketing_roi") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/marketing_ad_d28_roi.php')">신규 마케팅 ROI</div>
			<div class="lnb<?= ($sub_menu == "user_stats") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/user_stats.php')">사용자 통계</div>
			<div class="lnb<?= ($sub_menu == "user_maintain_new") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/user_inflowroute_stats.php')">사용자 유지현황(전체)</div>			
			<div class="lnb<?= ($sub_menu == "user_inflowroute_stats_daily") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/user_inflowroute_stats_daily.php')">사용자 유지현황(일간)</div>
			<div class="lnb<?= ($sub_menu == "user_inflowroute_stats_marketing") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/user_inflowroute_stats_marketing.php')">사용자 유지현황(광고)</div>	  
    		<div class="lnb<?= ($sub_menu == "user_retention_detail_daily" || $sub_menu == "user_pay_retention_daily") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/user_retention_detail_daily.php')">Retention</div>
            <div class="lnb_sub<?= ($sub_menu == "user_retention_detail_daily") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/user_retention_detail_daily.php')">가입자</div>
            <div class="lnb_sub<?= ($sub_menu == "user_pay_retention_daily") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/user_pay_retention_daily.php')">첫 결제자</div>
            <div class="lnb_sub<?= ($sub_menu == "user_30day_retention_daily") ? "_select_end" : "_end" ?>" onclick="go_page('/m12_marketing/user_30day_retention_daily.php')">30일 이탈자</div>
            <div class="lnb<?= ($sub_menu == "adflag_detail") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/adflag_detail.php')">유입경로별 유입 현황</div>
			<div class="lnb<?= ($sub_menu == "adflag_detail_analysis") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/adflag_detail_analysis.php')">유입경로 유입 상세분석</div>
			<div class="lnb<?= ($sub_menu == "adflag_viral_detail") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/adflag_viral_detail.php')">Viral 유입 현황</div>
			<div class="lnb<?= ($sub_menu == "adflag_game_viral_detail") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/adflag_game_viral_detail.php')">Game Viral 유입 현황</div>
			<div class="lnb<?= ($sub_menu == "adflag_mobile_detail") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/adflag_mobile_detail.php')">Mobile 유입 현황</div>
			<div class="lnb<?= ($sub_menu == "adflag_fbretention_retention" || $sub_menu == "adflag_fbretention_a2u" || $sub_menu == "adflag_fbretention_stats" || $sub_menu == "adflag_fbretention_mobile_stats") ? "_select" : "" ?>" onclick="go_page('/m3_user_static/adflag_fbretention_retention.php')">Retention 복귀현황</div>
            <div class="lnb_sub<?= ($sub_menu == "adflag_fbretention_retention") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/adflag_fbretention_retention.php')">Retention</div>
            <div class="lnb_sub<?= ($sub_menu == "adflag_fbretention_a2u") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/adflag_fbretention_a2u.php')">A2U</div>
            <div class="lnb_sub<?= ($sub_menu == "adflag_fbretention_stats") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/adflag_fbretention_stats.php')">통합 - Web</div>
			<div class="lnb_sub<?= ($sub_menu == "adflag_fbretention_mobile_stats") ? "_select_end" : "_end" ?>" onclick="go_page('/m12_marketing/adflag_fbretention_mobile_stats.php')">통합 - Mobile</div>
			<div class="lnb<?= ($sub_menu == "retention_stat") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/retention_stat/retention_stat_all.php')">Retention 유입 현황</div>
			<div class="lnb<?= ($sub_menu == "mobile_organic_IAP_download" || $sub_menu == "mobile_IAP_download" || $sub_menu == "mobile_install_download") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/mobile_IAP_download.php')">IAP 추출</div>
			<div class="lnb_sub<?= ($sub_menu == "mobile_IAP_download") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/mobile_IAP_download.php')">모바일 IAP 추출</div>
			<div class="lnb_sub<?= ($sub_menu == "mobile_organic_IAP_download") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/mobile_organic_IAP_download.php')">모바일 IAP 추출(Organic)</div>
			<div class="lnb_sub<?= ($sub_menu == "mobile_install_download") ? "_select_end" : "_end" ?>" onclick="go_page('/m12_marketing/mobile_install_download.php')">모바일 Install 추출</div>
			<div class="lnb<?= ($sub_menu == "marketing_spend_stat" ) ? "_select" : "" ?>" onclick="go_page('/m12_marketing/marketing_spend_stat.php')">마케팅 비용 현황</div>
			<div class="lnb<?= ($sub_menu == "marketing_newuser_stat_daily" || $sub_menu == "marketing_retention_stat" ||$sub_menu == "marketing_organic_stat_daily") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/marketing_newuser_stat_daily.php')">마케팅 일일 현황</div>
			<div class="lnb_sub<?= ($sub_menu == "marketing_newuser_stat_daily") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/marketing_newuser_stat_daily.php')">신규</div>
			<div class="lnb_sub<?= ($sub_menu == "marketing_retention_stat") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/marketing_retention_stat.php')">리텐션</div>
			<div class="lnb_sub<?= ($sub_menu == "marketing_organic_stat_daily") ? "_select_end" : "_end" ?>" onclick="go_page('/m12_marketing/marketing_organic_stat_daily.php')">Organic</div>
			<div class="lnb<?= ($sub_menu == "marketing_roi_factor_week_stat") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/marketing_roi_factor_week_stat2.php')">마케팅 ROI 예측</div>
			<div class="lnb<?= ($sub_menu == "marketing_fraud_check") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/marketing_fraud_check.php')">마케팅 Fraud 체크</div>
			<div class="lnb<?= ($sub_menu == "app_performance") ? "_select" : "" ?>" onclick="go_page('/m12_marketing/compare_stat/app_performance.php')">앱 퍼포먼스 비교(신규)</div>
    		
<?
    }    
    else if ($top_menu == "game")
    {
?>
            <div class="lnb<?= ($sub_menu == "event") ? "_select" : "" ?>" onclick="go_page('/m6_game/event.php')">이벤트관리</div>
            <div class="lnb<?= ($sub_menu == "cross_promotion") ? "_select" : "" ?>" onclick="go_page('/m6_game/cross_promotion.php')">Cross Promotion 관리</div>
            <div class="lnb<?= ($sub_menu == "fanpage") ? "_select" : "" ?>" onclick="go_page('/m6_game/fanpage.php')">팬페이지 게시물 관리</div>
            <div class="lnb<?= ($sub_menu == "aws_s3_fileupload") ? "_select" : "" ?>" onclick="go_page('/m6_game/aws_s3_fileupload.php')">Image 관리</div>
            <div class="lnb<?= ($sub_menu == "and_push") ? "_select" : "" ?>" onclick="go_page('/m6_game/and_push.php')">모바일 PUSH 관리</div>
            <div class="lnb<?= ($sub_menu == "mobile_push_event") ? "_select" : "" ?>" onclick="go_page('/m6_game/mobile_push_event.php')">M PUSH 이벤트관리</div>						
           	<div class="lnb<?= ($sub_menu == "apk_download_dev" || $sub_menu == "apk_download_release") ? "_select" : "" ?>" onclick="go_page('/m6_game/apk_download_dev.php')">APK 다운로드</div>
            <div class="lnb_sub<?= ($sub_menu == "apk_download_dev") ? "_select" : "" ?>" onclick="go_page('/m6_game/apk_download_dev.php')">APK-dev</div>
            <div class="lnb_sub<?= ($sub_menu == "apk_download_debug") ? "_select" : "" ?>" onclick="go_page('/m6_game/apk_download_debug.php')">APK-debug</div>
            <div class="lnb_sub<?= ($sub_menu == "apk_download_beta") ? "_select" : "" ?>" onclick="go_page('/m6_game/apk_download_beta.php')">APK-Beta</div>    
            <div class="lnb_sub<?= ($sub_menu == "apk_download_release_end") ? "_select_end" : "_end" ?>" onclick="go_page('/m6_game/apk_download_release.php')">APK-Release</div>
            <div class="lnb<?= ($sub_menu == "email_retention_stat") ? "_select" : "" ?>" onclick="go_page('/m6_game/email_retention_stat.php')">이메일 리텐션 현황</div>
	    <div class="lnb<?= ($sub_menu == "youtube_event_stat") ? "_select" : "" ?>" onclick="go_page('/m6_game/youtube_event_stat.php')">YouTube 리텐션 현황</div>            
	    <!--div class="lnb<?= ($sub_menu == "Instagram_event_stat") ? "_select" : "" ?>" onclick="go_page('/m6_game/Instagram_event_stat.php')">Instagram 리텐션 현황</div-->
	    <div class="lnb<?= ($sub_menu == "notice_popup" || $sub_menu == "notice_popup_write") ? "_select" : "" ?>" onclick="go_page('/m6_game/notice_popup.php')">공지사항 관리</div>            
	    <div class="lnb<?= ($sub_menu == "chat_block_list" ) ? "_select" : "" ?>" onclick="go_page('/m6_game/chat_block_list.php')">채팅차단 관리</div>            
   	    <div class="lnb<?= ($sub_menu == "vip_user_manage" || $sub_menu == "vip_retention_stat" ) ? "_select" : "" ?>" onclick="go_page('/m6_game/vip_manage/vip_user_manage.php')">VIP 유저 이탈 관리</div>
   	    <div class="lnb_sub<?= ($sub_menu == "vip_retention_stat" ) ? "_select_end" : "_end" ?>" onclick="go_page('/m6_game/vip_retention_stat.php')">복귀자 통계</div>
   	    <div class="lnb<?= ($sub_menu == "regular_push" ) ? "_select" : "" ?>" onclick="go_page('/m6_game/regular_push.php')">서버 자동 PUSH 관리</div>            
    	    <div class="lnb<?= ($sub_menu == "mobile_push_detail" ) ? "_select" : "" ?>" onclick="go_page('/m6_game/mobile_push_detail.php')">모바일 PUSH현황</div>
	    <div class="lnb<?= ($sub_menu == "notice_lobby" ) ? "_select" : "" ?>" onclick="go_page('/m6_game/notice_lobby.php')">전광판 문구관리</div>
<?    
    } 
    else if ($top_menu == "customer")
    {
?>
            <div class="lnb<?= ($sub_menu == "qa_dashboard") ? "_select" : "" ?>" onclick="go_page('/m7_customer/qa_dashboard.php')">QA 현황</div>
            <div class="lnb<?= ($sub_menu == "qa") ? "_select" : "" ?>" onclick="go_page('/m7_customer/qa.php')">QA 관리</div>
            <div class="lnb<?= ($sub_menu == "bug_issue_report_web") ? "_select" : "" ?>" onclick="go_page('/m7_customer/bug_issue_report.php?platform=0')">버그/이슈 리포트 관리</div>
            <div class="lnb<?= ($sub_menu == "qa_template") ? "_select" : "" ?>" onclick="go_page('/m7_customer/qa_template.php')">QA 답변 템플릿 관리</div>
            <div class="lnb<?= ($sub_menu == "faq") ? "_select" : "" ?>" onclick="go_page('/m7_customer/faq.php')">FAQ 관리</div>
            <div class="lnb<?= ($sub_menu == "qa_ios_dashboard") ? "_select" : "" ?>" onclick="go_page('/m7_customer/qa_ios_dashboard.php')">IOS QA 현황</div>
            <div class="lnb<?= ($sub_menu == "qa_ios") ? "_select" : "" ?>" onclick="go_page('/m7_customer/qa_ios.php')">IOS QA 관리</div>
            <div class="lnb<?= ($sub_menu == "bug_issue_report_ios") ? "_select" : "" ?>" onclick="go_page('/m7_customer/bug_issue_report.php?platform=1')">버그/이슈 리포트 관리</div>
            <div class="lnb<?= ($sub_menu == "mobile_ios_template") ? "_select" : "" ?>" onclick="go_page('/m7_customer/mobile_ios_template.php')">IOS QA 답변템플릿 관리</div>
            <div class="lnb<?= ($sub_menu == "mobile_ios_category") ? "_select" : "" ?>" onclick="go_page('/m7_customer/mobile_ios_category.php')">IOS QA 분류 관리</div>
            <div class="lnb<?= ($sub_menu == "qa_android_dashboard") ? "_select" : "" ?>" onclick="go_page('/m7_customer/qa_android_dashboard.php')">Android QA 현황</div>
            <div class="lnb<?= ($sub_menu == "qa_android") ? "_select" : "" ?>" onclick="go_page('/m7_customer/qa_android.php')">Android QA 관리</div>
            <div class="lnb<?= ($sub_menu == "bug_issue_report_android") ? "_select" : "" ?>" onclick="go_page('/m7_customer/bug_issue_report.php?platform=2')">버그/이슈 리포트 관리</div>
            <div class="lnb<?= ($sub_menu == "mobile_android_template") ? "_select" : "" ?>" onclick="go_page('/m7_customer/mobile_android_template.php')">And QA 답변템플릿 관리</div>
            <div class="lnb<?= ($sub_menu == "mobile_android_category") ? "_select" : "" ?>" onclick="go_page('/m7_customer/mobile_android_category.php')">And QA 분류 관리</div>
            <div class="lnb<?= ($sub_menu == "qa_amazon_dashboard") ? "_select" : "" ?>" onclick="go_page('/m7_customer/qa_amazon_dashboard.php')">Amazon QA 현황</div>
            <div class="lnb<?= ($sub_menu == "qa_amazon") ? "_select" : "" ?>" onclick="go_page('/m7_customer/qa_amazon.php')">Amazon QA 관리</div>
            <div class="lnb<?= ($sub_menu == "bug_issue_report_amazon") ? "_select" : "" ?>" onclick="go_page('/m7_customer/bug_issue_report.php?platform=3')">버그/이슈 리포트 관리</div>
            <div class="lnb<?= ($sub_menu == "mobile_amazon_template") ? "_select" : "" ?>" onclick="go_page('/m7_customer/mobile_amazon_template.php')">Ama QA 답변템플릿 관리</div>
            <div class="lnb<?= ($sub_menu == "mobile_amazon_category") ? "_select" : "" ?>" onclick="go_page('/m7_customer/mobile_amazon_category.php')">Ama QA 분류 관리</div>
            <div class="lnb<?= ($sub_menu == "appstore_support") ? "_select" : "" ?>" onclick="go_page('/m7_customer/appstore_support.php')">Appstore 서포트</div>
            <div class="lnb" style="cursor:default;" >---------------------</div>
            <div class="lnb<?= ($sub_menu == "claim_review") ? "_select" : "" ?>" onclick="go_page('/m7_customer/claim_review.php')">클레임 리뷰</div>            
            <div class="lnb<?= ($sub_menu == "qa_special") ? "_select" : "" ?>" onclick="go_page('/m7_customer/qa_special.php')">Web 특별 대응 문의</div>
            <div class="lnb<?= ($sub_menu == "qa_ios_special") ? "_select" : "" ?>" onclick="go_page('/m7_customer/qa_ios_special.php')">Ios 특별 대응 문의</div>
            <div class="lnb<?= ($sub_menu == "qa_android_special") ? "_select" : "" ?>" onclick="go_page('/m7_customer/qa_android_special.php')">And 특별 대응 문의</div>            
            <div class="lnb<?= ($sub_menu == "qa_amazon_special") ? "_select" : "" ?>" onclick="go_page('/m7_customer/qa_amazon_special.php')">Ama 특별 대응 문의</div>
            
<?
    } 
    else if ($top_menu == "monitoring")
    {
?>
            <div class="lnb<?= ($sub_menu == "total_stats") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/total_stats.php')">모니터링 총괄</div>
            <div class="lnb<?= ($sub_menu == "online_log_stats") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/online_log_stats.php')">사용자 접속 통계</div>
            <div class="lnb<?= ($sub_menu == "online_user_mng") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/online_user_mng.php')">온라인 사용자 목록</div>
            <div class="lnb<?= ($sub_menu == "client_log" || $sub_menu == "client_log_stats"  || $sub_menu == "client_log_stats_ios"  || $sub_menu == "client_log_ios"  
    		|| $sub_menu == "client_log_stats_android"  || $sub_menu == "client_log_android" || $sub_menu == "client_log_stats_amazon" || $sub_menu == "client_log_amazon") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/client_log_stats.php')">클라이언트 통신로그</div>
            <div class="lnb_sub<?= ($sub_menu == "client_log_stats") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/client_log_stats.php')"> - Web 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "client_log") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/client_log_mng.php')"> - Web 로그</div>
            <div class="lnb_sub<?= ($sub_menu == "client_log_stats_ios") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/client_log_stats_ios.php')"> - Ios 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "client_log_ios") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/client_log_mng_ios.php')"> - Ios 로그</div>
            <div class="lnb_sub<?= ($sub_menu == "client_log_stats_android") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/client_log_stats_android.php')"> - Android 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "client_log_android") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/client_log_mng_android.php')"> - Android 로그</div>
            <div class="lnb_sub<?= ($sub_menu == "client_log_stats_amazon") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/client_log_stats_amazon.php')"> - Amazon 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "client_log_amazon") ? "_select_end" : "_end" ?>" onclick="go_page('/m8_monitoring/client_log_mng_amazon.php')"> - Amazon 로그</div>
            <div class="lnb<?= ($sub_menu == "server_log_stats" || $sub_menu == "server_log" || $sub_menu == "server_log_stats_v2" || $sub_menu == "server_log_v2" || $sub_menu == "server_log_stats_mobile"  || $sub_menu == "server_log_mobile") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/server_log_stats.php')">서버 통신로그</div>
            <div class="lnb_sub<?= ($sub_menu == "server_log_stats") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/server_log_stats.php')"> - Web 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "server_log") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/server_log_mng.php')"> - Web 로그</div>
            <div class="lnb_sub<?= ($sub_menu == "server_log_stats_v2") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/server_log_stats_v2.php')"> - Web 통계(v2)</div>
            <div class="lnb_sub<?= ($sub_menu == "server_log_v2") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/server_log_mng_v2.php')"> - Web 로그(v2)</div>
            <div class="lnb_sub<?= ($sub_menu == "server_log_stats_mobile") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/server_log_stats_mobile.php')"> - Mobile 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "server_log_mobile") ? "_select_end" : "_end" ?>" onclick="go_page('/m8_monitoring/server_log_mng_mobile.php')"> - Mobile 로그</div>
            <div class="lnb<?= ($sub_menu == "serverevent_log_stats" || $sub_menu == "serverevent_log" || $sub_menu == "serverevent_log_stats_v2" || $sub_menu == "serverevent_log_v2" || $sub_menu == "serverevent_log_stats_mobile"  || $sub_menu == "serverevent_log_mobile") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/serverevent_log_stats.php')">서버 이벤트 로그</div>
            <div class="lnb_sub<?= ($sub_menu == "serverevent_log_stats") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/serverevent_log_stats.php')"> - Web 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "serverevent_log") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/serverevent_log_mng.php')"> - Web 로그</div>
            <div class="lnb_sub<?= ($sub_menu == "serverevent_log_stats_v2") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/serverevent_log_stats_v2.php')"> - Web 통계(v2)</div>
            <div class="lnb_sub<?= ($sub_menu == "serverevent_log_v2") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/serverevent_log_mng_v2.php')"> - Web 로그(v2)</div>
            <div class="lnb_sub<?= ($sub_menu == "serverevent_log_stats_mobile") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/serverevent_log_stats_mobile.php')"> - Mobile 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "serverevent_log_mobile") ? "_select_end" : "_end" ?>" onclick="go_page('/m8_monitoring/serverevent_log_mng_mobile.php')"> - Mobile 로그</div>
            <div class="lnb<?= ($sub_menu == "facebook_log_stats") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/facebook_log_stats.php')">Facebook 로그</div>
            <div class="lnb_sub<?= ($sub_menu == "facebook_log_stats") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/facebook_log_stats.php')"> - 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "facebook_log") ? "_select_end" : "_end" ?>" onclick="go_page('/m8_monitoring/facebook_log_mng.php')"> - 로그</div>
            <div class="lnb<?= ($sub_menu == "slowquery_log_stats" || $sub_menu == "slowquery_log" || $sub_menu == "slowquery_log_stats_mobile" || $sub_menu == "slowquery_log_mobile") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/slowquery_log_stats.php')">Slow Query 로그</div>
            <div class="lnb_sub<?= ($sub_menu == "slowquery_log_stats") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/slowquery_log_stats.php')"> - Web 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "slowquery_log") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/slowquery_log_mng.php')"> - Web 로그</div>
            <div class="lnb_sub<?= ($sub_menu == "slowquery_log_stats_mobile") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/slowquery_log_stats_mobile.php')"> - Mobile 통계</div>
            <div class="lnb_sub<?= ($sub_menu == "slowquery_log_mobile") ? "_select_end" : "_end" ?>" onclick="go_page('/m8_monitoring/slowquery_log_mng_mobile.php')"> - Mobile 로그</div>
			
			 <div class="lnb<?= ($sub_menu == "download_faillog_stat_daily" || $sub_menu == "graph_download_faillog_stat" ) ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/download_faillog/download_faillog_stat_daily.php')">리소스 다운실패</div>            
            <div class="lnb_sub<?= ($sub_menu == "download_faillog_stat_daily") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/download_faillog/download_faillog_stat_daily.php')"> - 상세</div>
            <div class="lnb_sub<?= ($sub_menu == "graph_download_faillog_stat") ? "_select_end" : "_end" ?>" onclick="go_page('/m8_monitoring/download_faillog/graph_download_faillog_stat.php')"> - 그래프</div>
			
            <div class="lnb<?= ($sub_menu == "scheduler_stats") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/scheduler_stats.php')">스케줄러 모니터링</div>
            <div class="lnb<?= ($sub_menu == "blocklist") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/blocklist.php')"> 차단 유저 목록</div>
            <div class="lnb<?= ($sub_menu == "coin_increase_user" || $sub_menu == "coin_increase_user_check" || $sub_menu == "coin_increase_user_reserve" || $sub_menu == "coin_increase_user_new"
    		|| $sub_menu == "unknowncoin_user" || $sub_menu == "unknowncoin_user_check" || $sub_menu == "unknowncoin_user_reserve" || $sub_menu == "unknowncoin_user_new"
    		|| $sub_menu == "abuse_suspicion_user" || $sub_menu == "abuse_suspicion_user_reserve" || $sub_menu == "abuse_suspicion_user_check" 
			|| $sub_menu == "abuse_debug_list_web" || $sub_menu == "slot_gain_high_coin_user_list"||$sub_menu=="slot_gain_high_coin_user_list_reserve"||$sub_menu=="slot_gain_high_coin_user_list_check" || $sub_menu == "abuse_slot_user_new") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/coin_increase_user.php?check=0')">어뷰즈 관리</div>
            <div class="lnb_sub<?= ($sub_menu == "coin_increase_user") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/coin_increase_user.php?check=0')"> - 비결제 코인 급증</div>
            <div class="lnb_sub<?= ($sub_menu == "coin_increase_user_reserve") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/coin_increase_user.php?check=2')"> &nbsp;└ 보류 중</div>
            <div class="lnb_sub<?= ($sub_menu == "coin_increase_user_new") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/coin_increase_user_new.php')"> &nbsp;└ 보류 중(New)</div>
            <div class="lnb_sub<?= ($sub_menu == "coin_increase_user_check") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/coin_increase_user.php?check=1')"> &nbsp;└ 확인 완료</div>
            <div class="lnb_sub<?= ($sub_menu == "unknowncoin_user") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/unknowncoin_user.php?check=0')"> - Unknown코인 급증</div>
            <div class="lnb_sub<?= ($sub_menu == "unknowncoin_user_reserve") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/unknowncoin_user.php?check=2')"> &nbsp;└ 보류 중</div>
            <div class="lnb_sub<?= ($sub_menu == "unknowncoin_user_new") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/unknowncoin_user_new.php')"> &nbsp;└ 보류 중(New)</div>
            <div class="lnb_sub<?= ($sub_menu == "unknowncoin_user_check") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/unknowncoin_user.php?check=1')"> &nbsp;└ 확인 완료</div>
            <div class="lnb_sub<?= ($sub_menu == "abuse_suspicion_user") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/abuse_suspicion_user.php?check=0')"> - 어뷰즈 의심 유저</div>
            <div class="lnb_sub<?= ($sub_menu == "abuse_suspicion_user_reserve") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/abuse_suspicion_user.php?check=2')"> &nbsp;└ 보류 중</div>
            <div class="lnb_sub<?= ($sub_menu == "abuse_suspicion_user_check") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/abuse_suspicion_user.php?check=1')"> &nbsp;└ 확인 완료</div>
            <div class="lnb_sub<?= ($sub_menu == "slot_gain_high_coin_user_list") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/slot_gain_high_coin_user_list.php?check=1')">슬롯별 최고 획득</div>
            <div class="lnb_sub<?= ($sub_menu == "slot_gain_high_coin_user_list_reserve") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/slot_gain_high_coin_user_list.php?check=4')"> &nbsp;└ 보류 중</div>
            <div class="lnb_sub<?= ($sub_menu == "abuse_slot_user_new") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/abuse_slot_user_new.php')"> &nbsp;└ 보류 중(New)</div>
            <div class="lnb_sub<?= ($sub_menu == "slot_gain_high_coin_user_list_check") ? "_select" : "_end" ?>" onclick="go_page('/m8_monitoring/slot_gain_high_coin_user_list.php?check=3')"> &nbsp;└ 확인 완료</div>  
            <div class="lnb_sub<?= ($sub_menu == "abuse_debug_list_web") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/abuse_debug_list.php?platform=0')"> &nbsp;디버그 등록(Mobile)</div>
                      
			<div class="lnb<?= ($sub_menu == "betrange") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/betrange.php')">베팅레인지관리</div>
			<div class="lnb<?= ($sub_menu == "redshift_data_dashboard") ? "_select" : "" ?>" onclick="go_page('/m8_monitoring/redshift_data_dashboard.php')">Redshift Data 체크</div>
<?
    }
    else if ($top_menu == "management")
    {
?>
            <div class="lnb<?= ($sub_menu == "daily_game_indicator") ? "_select" : "" ?>" onclick="go_page('/m10_management/daily_game_indicator.php')">일일 게임 지표</div>
            <div class="lnb<?= ($sub_menu == "daily_game_indicator_graph") ? "_select" : "" ?>" onclick="go_page('/m10_management/daily_game_indicator_graph.php')">일일 게임 지표(Graph)</div>
            <div class="lnb<?= ($sub_menu == "month_game_indicator") ? "_select" : "" ?>" onclick="go_page('/m10_management/month_game_indicator.php')">월별 게임 지표</div>
            <div class="lnb<?= ($sub_menu == "daily_game_pay_indicator") ? "_select" : "" ?>" onclick="go_page('/m10_management/daily_game_pay_indicator.php')">게임 결제 현황</div>
            <div class="lnb<?= ($sub_menu == "daily_game_dau_stat") ? "_select" : "" ?>" onclick="go_page('/m10_management/daily_game_dau_stat.php')">DAU 현황</div>
            <div class="lnb<?= ($sub_menu == "month_game_purchase_join") ? "_select" : "" ?>" onclick="go_page('/m10_management/month_game_purchase_join.php')">월별 결제 가입자 지표</div>
            <div class="lnb<?= ($sub_menu == "user_week_pay_stat") ? "_select" : "" ?>" onclick="go_page('/m10_management/user_week_pay_stat.php')">가입자 주별 결제 현황</div>
            <div class="lnb<?= ($sub_menu == "daily_before_create_28day_stats") ? "_select" : "" ?>" onclick="go_page('/m10_management/daily_before_create_28day_stats.php')">가입기준 28일 이후 통계</div>
            <div class="lnb<?= ($sub_menu == "daily_game_mobile_arpu_stat") ? "_select" : "" ?>" onclick="go_page('/m10_management/daily_game_mobile_arpu_stat.php')">모바일 ARPU 현황</div>
            <div class="lnb<?= ($sub_menu == "yearly_month_stat") ? "_select" : "" ?>" onclick="go_page('/m10_management/yearly_month_stat.php')">게임 매출 이연</div>
            <div class="lnb<?= ($sub_menu == "facebook_payment_tax") ? "_select" : "" ?>" onclick="go_page('/m10_management/facebook_payment_tax.php')">Facebook 세금 지표</div>
            
<?
    }  
    else if ($top_menu == "admin")
    {
?>
            <div class="lnb<?= ($sub_menu == "admin") ? "_select" : "" ?>" onclick="go_page('/m11_admin/admin_mng.php')">관리자관리</div>				
			<div class="lnb<?= ($sub_menu == "maintenance_deploy") ? "_select" : "" ?>" onclick="go_page('/m11_admin/maintenance_deploy.php')">Web 메인터넌스</div>
<?
    }  
    else if ($top_menu == "deploy")
    {
?>
            <div class="lnb<?= ($sub_menu == "resource_mng") ? "_select" : "" ?>" onclick="go_page('/m11_admin/resource_mng.php')">리소스관리</div>
	    	<div class="lnb<?= ($sub_menu == "html5_resource_mng") ? "_select" : "" ?>" onclick="go_page('/m11_admin/html5_resource_mng.php')">2.0 리소스 관리</div>
			<div class="lnb<?= ($sub_menu == "common_setting") ? "_select" : "" ?>" onclick="go_page('/m11_admin/common_setting.php')">2.0 Config 관리</div>
<?
	}  
?>
    	</div>
<?
    }
?>
