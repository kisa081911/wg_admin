﻿// 문자열 치환
function ReplaceText(orgStr, findStr, replaceStr)
{
	var re = new RegExp(findStr, "gi");
	
	return orgStr.replace(re, replaceStr);
}

// 텍스트 앞뒤의 공백문자 제거
function TrimText(orgStr)
{
	var copyStr = "";
	var strIndex;

	for ( strIndex = 0; strIndex < orgStr.length; strIndex ++ ) {
		if ( orgStr.charAt(strIndex) == ' ' ) continue;
		else {
			copyStr = orgStr.substr( strIndex );
			break;
		}
	}
	
	for ( strIndex = copyStr.length - 1; strIndex >= 0; strIndex -- ) {
		if ( copyStr.charAt(strIndex) == ' ' ) continue;
		else {
			copyStr = copyStr.substr( 0, strIndex + 1 );
			break;
		}
	}
	
	return copyStr;
}

var layer_no = 1;
var layer_zindex = 10;
var layer_count = 0;
var layer_check = true;
var layer_cover_no = "";
var layer_parent_map = null;

// 레이어 팝업 출력
function open_layer_popup(url, width, height, multiwindow, original_parent)
{
	var clientHeight = document.body.clientHeight;
	var clientWidth = document.body.clientWidth;
	
	if (document.documentElement)
	{
		clientHeight = document.documentElement.clientHeight;
		clientWidth = document.documentElement.clientWidth;
	}
	
	if (clientHeight > screen.availHeight - 100);
		clientHeight = screen.availHeight - 100;
		
	if (window.innerHeight)
		clientHeight = window.innerHeight;
	
	if (window.innerWidth)
		clientWidth = window.innerWidth;
	
    var left = (clientWidth - width) / 2 - 6;
    var top = (clientHeight - height) / 2 + document.body.scrollTop;
    
    if ((clientWidth - width) / 2 - 6 <= 0 || (clientHeight - height) / 2 <= 0)
    {
        if (window.parent != null && window.parent != window)
        {
            if (original_parent == null)
                original_parent = window;
            
            window.parent.open_layer_popup(url, width, height, "1", original_parent);
            return;
        }
    }
    
    if (multiwindow == "1")
    {
        var date = new Date();
        var variation = 30 - date.getSeconds();

        left += variation;
        top += variation;
    }
    
    if (left < 0)
        left = 0;
        
    if (top < 0)
        top = 0;
    
    if (multiwindow != "1")
    {
        layer_cover_no = layer_no;

        var div = document.createElement("div");
        div.id = "layer_popup_cover_" + layer_cover_no;
        div.style.position = "absolute";
        div.style.display = "block";
        div.style.top = "0px";
        div.style.left = "0px";
        div.style.width = document.body.scrollWidth + "px";
        div.style.height = document.body.scrollHeight + "px";
        div.style.zIndex = layer_zindex++;
        div.style.filter = "alpha(opacity=15)";
        div.style.opacity = "0.15";

        document.body.appendChild(div);

        // 깜빡임을 없애기 위해 타이머를 이용해 배경색 지정
        window.setTimeout("set_background_layer_popup()", 1);
    }
        
    var div = document.createElement("div");
    div.id = "layer_popup_" + layer_no;
    div.style.position = "absolute";
    div.style.display = "block";
    div.style.top = top + "px";
    div.style.left = left + "px";
    div.style.width = width + "px";
    div.style.height = height + "px";
    div.style.zIndex = layer_zindex;
    div.style.border = "1px solid #818181";

    layer_zindex++;
    
    if (url.indexOf("?") == -1)
        url += "?layer_no=" + layer_no;
    else
        url += "&layer_no=" + layer_no;
        
    div.innerHTML = "<iframe id='iframe_form_" + layer_no + "' src='" + url + "' scrolling='no' frameborder='0' width='" + width + "' height='" + height + "'></iframe>";

    document.body.appendChild(div);

    if (layer_parent_map == null)
        layer_parent_map = new HashMap2();
        
    if (original_parent != null)
    {
        layer_parent_map.put(layer_no, original_parent);
    }
    else
    {
        layer_parent_map.put(layer_no, window);
    }
    
    layer_no++;
    layer_count++;
}

function get_layer_parent(layer_no)
{
    return layer_parent_map.get(layer_no);
}

function set_background_layer_popup()
{
    var div = document.getElementById("layer_popup_cover_" + layer_cover_no);
    
    if (div != null)
        div.style.backgroundColor = "black";
}

// 레이어 팝업 닫기
function close_layer_popup(no)
{
    // 포커스 lost 문제를 막기 위해 Timer 사용
    window.setTimeout("close_layer_popup_action('" + no + "')", 1);
}

function close_layer_popup_action(no)
{
    var iframe = document.getElementById("iframe_form_" + no);
    
    if (iframe != null)
    {
        iframe.src = "about:blank";
        iframe.parentElement.removeChild(iframe);
    }
        
    var div = document.getElementById("layer_popup_" + no);

    if (div != null)
        div.parentElement.removeChild(div);
    
    div = document.getElementById("layer_popup_cover_" + no);
    
    if (div != null)
        div.parentElement.removeChild(div);
    
    layer_count--;
    
    window.focus();
}

// 페이징 정보 출력
function update_pagenation(page, totalcount, listcount, pagecount, pagenation)
{
    if (pagenation == null)
        pagenation = "";
        
    var pageinfo = "";

    var firstpage = Math.floor(((page-1) / pagecount)) * pagecount + 1;
    var endpage = firstpage + pagecount - 1;
    var finalpage = Math.floor((totalcount-1) / listcount + 1);
    
    if (finalpage == 0)
    {
        document.getElementById("pagenation" + pagenation).innerHTML = "<div style='padding:20px 0'>검색결과가 없습니다.</div>";
        return;
    }
    
    if (endpage > finalpage)
        endpage = finalpage;
        
    if (firstpage > 1)
        pageinfo += "<a href='javascript:g_page" + pagenation + "=1;refresh" + pagenation + "()' onclick='layer_check=false' class='pprev' onmouseover=\"className='pprev_over'\" onmouseout=\"className='pprev'\"></a> <a href='javascript:g_page" + pagenation + "=" + (firstpage-1) + ";refresh" + pagenation + "()' onclick='layer_check=false' class='prev' onmouseover=\"className='prev_over'\" onmouseout=\"className='prev'\"></a>"
    
    for (var i=firstpage; i<=endpage; i++)
    {
        if (i == page)
            pageinfo += " <a href='javascript:g_page" + pagenation + "=" + i + ";refresh" + pagenation + "()' onclick='layer_check=false' class='select'>" + i + "</a> "
        else
            pageinfo += " <a href='javascript:g_page" + pagenation + "=" + i + ";refresh" + pagenation + "()' onclick='layer_check=false' onmouseover=\"className='over'\" onmouseout=\"className=''\">" + i + "</a> "
    }
    
    if (endpage < firstpage)
        pageinfo += " ";
        
    if (endpage < finalpage)
        pageinfo += "<a href='javascript:g_page" + pagenation + "=" + (endpage+1) + ";refresh" + pagenation + "()' onclick='layer_check=false' class='next' onmouseover=\"className='next_over'\" onmouseout=\"className='next'\"></a> <a href='javascript:g_page" + pagenation + "=" + finalpage + ";refresh" + pagenation + "()' onclick='layer_check=false' class='nnext' onmouseover=\"className='nnext_over'\" onmouseout=\"className='nnext'\"></a>"
        
    document.getElementById("pagenation" + pagenation).innerHTML = pageinfo;
}

// 분을 시+분으로 변환
function make_time_format(minute)
{
    var time = "";
    
    if (minute > 60)
        time = Math.floor(minute / 60) + "시간 " + (minute % 60) + "분";
    else
        time = minute + "분";
        
    return time;
}

function make_time_second_format(second)
{
	var time = "";
    
    if (second > 60)
    {
        var minute = Math.floor(second / 60);      
        
        if (minute > 60)
        {
            var hour = Math.floor(minute / 60);
            
            if (hour > 24)
            {
            	time = Math.floor(hour / 24) + "일 " + (((hour % 24) > 0) ? (hour % 24) + "시간" : "");
            }
            else
            {
            	minute = (hour % 60);
                second = second % 60;   
                time = hour + "시간 " + minute + "분 " + second + "초";
            }
        }
        else
        {
        	time = minute + "분 " + (second % 60) + "초";
        }
    }
    else
    {
    	time = second + "초";   
    }
        
    return time;
}

// 문자열을 길이에 따라 자르기
function get_left_title(title, length)
{
    var newtitle = "";
    var len = 0;
    
    for (var i=0; i<title.length; i++)
    {
        var chr = title.substr(i, 1);
        
        if (chr.charCodeAt(0) > 128)
            len = len + 1;
        else
            len = len + 0.7;
            
        if (len > length)
            break;

        newtitle += chr;
    }
    
    if (newtitle != title)
        newtitle += "..";
        
    return newtitle;
}

// 금액 표현을 숫자로 변환
function strip_price_format(price)
{
    return ReplaceText(price, ",", "");
}

// 숫자 문자열을 금액 표현으로 변환
function make_price_format(price)
{
    price = price + "";
    
    var newprice = "";
    var isminus = false;
    
    var checklist = "0123456789-";
    
    for (var i=0; i<price.length; i++)
    {
        if (checklist.indexOf(price.substr(i, 1)) != -1)
            newprice += price.substr(i, 1);
    }

    price = newprice;
    newprice = "";
    
    if (price.substr(0, 1) == "-")
    {
        isminus = true;
        price = price.substr(1);
    }
            
    while (price != "")
    {
        if (price.length > 3)
        {
            newprice = "," + price.substr(price.length-3) + newprice;
            price = price.substr(0, price.length-3);
        }
        else
        {
            newprice = price + newprice;
            price = "";
        }
    }
    
    if (isminus)
        return "-" + newprice;
    else
        return newprice;
}

// 금액 표현을 숫자로 변환
function strip_price_format(price)
{
    return ReplaceText(price, ",", "");
}

// html을 text로 변환
function encode_html_value(str)
{
    return ReplaceText(ReplaceText(ReplaceText(str, "\\&", "&amp;"), "\\<", "&lt;"), "\\>", "&gt;");
}

function encode_html_attribute(str)
{	
	return ReplaceText(ReplaceText(ReplaceText(ReplaceText(str, '\\&', '&amp;'),'\\>', '&gt;'),"\\<", "&lt;"), '\\"', '&quot;');
}

// input 태그의 값을 금액 표현으로 유지
function keep_price_format(elem)
{
    var value = make_price_format(strip_price_format(elem.value));
    
    if (elem.value != value)
        elem.value = value;
}

// date Object로 변환
function convert_date(datestr)
{
    var year = datestr.substr(0, 4);
    var month = datestr.substr(5, 2);
    var day = datestr.substr(8, 2);
    
    if (month.substr(0,1) == "0")
        month = month.substr(1);
    
    if (day.substr(0,1) == "0")
        day = day.substr(1);
        
    var date = new Date(parseInt(year), parseInt(month)-1, parseInt(day));
    
    return date;
}

// date 문자열(yyyy-mm-dd) 포맷으로 변환
function convert_date_str(date)
{
    var datestr = date.getFullYear() + "-" + 
        ((date.getMonth() + 1 < 10) ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1)) + "-" +
        ((date.getDate() < 10) ? "0" + date.getDate() : date.getDate());
        
    return datestr;
}

//짧은 date 문자열(yyyy.mm.dd)로 문자열 변환
function short_date_format(date)
{
    return ReplaceText(date.substr(0,10), "-", ".");
}

function manual_date_focus(elem)
{
    elem.setAttribute("old_date", elem.value);
}

function manual_date_blur(elem)
{
    var date = ReplaceText(elem.value, "-", "");
    date = ReplaceText(date, " ", "");
     
    date = date.substr(0, 4) + "-" + date.substr(4, 2) + "-" + date.substr(6, 2);
    
    if (convert_date_str(convert_date(date)) == date)
        elem.value = date;
    else
    {
        if (elem.value != "")
        {
            alert("올바르지 않은 날짜 포맷입니다.");

            if (elem.getAttribute("old_date") != "" && (elem.getAttribute("old_date") == null || convert_date_str(convert_date(elem.getAttribute("old_date"))) != elem.getAttribute("old_date")))
                return;
            else
                elem.value = elem.getAttribute("old_date");
        }
    }
}

function get_split_data(data, seperator, index)
{
    if (data.split(seperator).length > index)
        return data.split(seperator)[index];
    else
        return "";
}

function format_size(size)
{
    if (size > 1024 * 1024 * 1024)
    {
        size = (Math.round(size / 102.4 / 1024 / 1024) / 10) + "GB";
    }
    else if (size > 1024 * 1024)
    {
        size = (Math.round(size / 102.4 / 1024) / 10) + "MB";
    }
    else if (size > 1024)
    {
        size = (Math.round(size / 102.4) / 10) + "KB";
    }
    else if (size == 0)
    	size = "0KB";
    else
        size = "1KB";
        
    return size;
}

function setPng24(obj) 
{
    obj.width=obj.height=1;
    obj.className=obj.className.replace(/\bpng24\b/i,'');
    obj.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+obj.src+"',sizingMethod='image');"
    obj.src='/images/blank.gif';
    return '';
}  

function get_radio(name)
{
	var radios = document.getElementsByName(name);
		
	for (var i=0; i<radios.length; i++)
	{
		if (radios[i].checked)
		{
			return radios[i].value;
		}
	}

	return "";
}	

function checknum()
{
    e = window.event;
    if (e.keyCode >= 48 && e.keyCode <= 57)
    {    
        var keypress = String.fromCharCode(e.keyCode); 
        var numkey = /\d/;
        return numkey.test(keypress);
    }
    else if (e.keyCode == 8 || e.keyCode == 45 || e.keyCode == 46 || e.keyCode == 37 || e.keyCode == 39) // backspace, -, delete, <-, ->
    {
        var keypress = String.fromCharCode(e.keyCode); 
        if (keypress == "%")
        {
            e.returnValue = false;
            return false;
        }
        else
        {
            e.returnValue = true;
            return true;
        }
    }
    else
    {
        e.returnValue = false;
        return false;
    }   
}

function checkonlynum()
{
    e = window.event;
    if (e.keyCode >= 48 && e.keyCode <= 57)
    {    
        var keypress = String.fromCharCode(e.keyCode); 
        var numkey = /\d/;
        return numkey.test(keypress);
    }
    else if (e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 37 || e.keyCode == 39) // backspace, delete, <-, ->
    {
        var keypress = String.fromCharCode(e.keyCode); 
        if (keypress == "%")
        {
            e.returnValue = false;
            return false;
        }
        else
        {
            e.returnValue = true;
            return true;
        }
    }
    else
    {
        e.returnValue = false;
        return false;
    }
}

function IsNumber(str)
{
	var Number = '1234567890';
	var kiki = Number;
	var i;
	for (i=0; i<str.length; i++){	
		if(kiki.indexOf(str.substring(i,i+1))<0) {
			return false; 
		}	
	}//for
	return true;
}

function addEvent(element, eventName, callback)
{
	if (typeof(element) == "string")
		element = document.getElementById(element);
	
	if (element == null)
		return;
	
	if (element.addEventListener)
		element.addEventListener(eventName, callback, false);
	else if(element.attachEvent)
		element.attachEvent("on" + eventName, callback);
}
	
function getEventTarget(e)
{
	if(!e)
		e = window.event;
	
	if(e.target)
		return e.target;
	
	return e.srcElement;
}

function getEventKeycode(e)
{
	if (window.event)
	{
		return window.event.keyCode;
	}
	else
	{
		return e.which;
	}	
}

function checkCitizenNumber(szCitizenBasic, szCitizenDetail)
{
	if (!IsNumber(szCitizenBasic))
		return false;

	if (!IsNumber(szCitizenBasic))
		return false;

		if (szCitizenBasic.length != 6) {
			return false;
		} else if (szCitizenDetail.length != 7) {
			return false;
		} else {
			var str_serial1 = szCitizenBasic;
			var str_serial2 = szCitizenDetail;
			var digit=0
			for (var i=0;i<str_serial1.length;i++) {
				var str_dig=str_serial1.substring(i,i+1);
				if (str_dig<'0' || str_dig>'9') { 
					digit=digit+1 
				}
			}
			if ((str_serial1 == '') || ( digit != 0 )) {
				return false;
			}
			
			var digit1=0
			for (var i=0;i<str_serial2.length;i++) {
				var str_dig1=str_serial2.substring(i,i+1);
				if (str_dig1<'0' || str_dig1>'9'){
					digit1=digit1+1
				}
			}
			
			if ((str_serial2 == '') || ( digit1 != 0 ))	{
				return false;   
			}
			if (str_serial1.substring(2,3) > 1) {
				return false;
			}
			if (str_serial1.substring(4,5) > 3) {
				return false;
			} 
			if (str_serial2.substring(0,1) > 8 || str_serial2.substring(0,1) == 0) {
				return false;   
			} 
			if (str_serial2.substring(0,1) >= 5 ) {
				return check_fgnno(szCitizenBasic, szCitizenDetail); 
			}
			
			var a1=str_serial1.substring(0,1)
			var a2=str_serial1.substring(1,2)
			var a3=str_serial1.substring(2,3)
			var a4=str_serial1.substring(3,4)
			var a5=str_serial1.substring(4,5)
			var a6=str_serial1.substring(5,6)
			var check_digit=a1*2+a2*3+a3*4+a4*5+a5*6+a6*7
			var b1=str_serial2.substring(0,1)
			var b2=str_serial2.substring(1,2)
			var b3=str_serial2.substring(2,3)
			var b4=str_serial2.substring(3,4)
			var b5=str_serial2.substring(4,5)
			var b6=str_serial2.substring(5,6)
			var b7=str_serial2.substring(6,7)
			var check_digit=check_digit+b1*8+b2*9+b3*2+b4*3+b5*4+b6*5 
			
			check_digit = check_digit%11
			check_digit = 11 - check_digit
			check_digit = check_digit%10
			
			if (check_digit != b7) {
				return false;
			} else {
				return true;
			}
		}
}	

function check_fgnno(szCitizenBasic, szCitizenDetail) {
    var fgnno=szCitizenBasic+szCitizenDetail;
    var sum=0;
    var odd=0;
    buf = new Array(13);
    for(i=0; i<13; i++) { buf[i]=parseInt(fgnno.charAt(i)); }
    odd = buf[7]*10 + buf[8];
    if(odd%2 != 0) { return false; }
    if( (buf[11]!=6) && (buf[11]!=7) && (buf[11]!=8) && (buf[11]!=9) ) {
            return false;
    }
    multipliers = [2,3,4,5,6,7,8,9,2,3,4,5];
    for(i=0, sum=0; i<12; i++) { sum += (buf[i] *= multipliers[i]); }
    sum = 11 - (sum%11);
    if(sum >= 10) { sum -= 10; }
    sum += 2;
    if(sum >= 10) { sum -= 10; }
    if(sum != buf[12]) { return false }
    return true;
}

var HashMap2 = function()
{
    var mapVal = {};
    var pos = new Array();

    this.get = function(key)
    {
        return mapVal[key];
    }

    this.getPos = function(n)
    {
        return mapVal[pos[n]];
    }

    this.getKey = function(n)
    {
        return pos[n];
    }
    
    this.remove = function(n)
    {
        var ary = new Array();
        
        for (var i=0; i<map.size(); i++)
        {
            if (i != n)
            {
                ary.push(pos[i]);
            }
        }
        
        pos = ary;
    }

    this.put = function(key, val)
    {
        mapVal[key] = val;

        var flg = true;
        
        for (var i=0; i<pos.length; i++)
        {
            if (key == pos[i])
                flg = false;
        }

        if (flg)
            pos.push(key);
    }

    this.size = function()
    {
        return pos.length;
    }

    this.length = function()
    {
        return pos.length;
    }
}

function setCookie(c_name, value, exdays)
{
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name)
{
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++)
	{
		x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		x=x.replace(/^\s+|\s+$/g,"");
		if (x==c_name)
		{
			return unescape(y);
		}
	}
}