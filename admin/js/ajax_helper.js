function WG_ajax_execute(cmd, param, executecallback, isshow)
{
	var loading = $('<div id="loading" class="loading"></div><img id="loading_img" alt="loading" src="' + tmp_hosturl + '/images/loading.gif" />').appendTo(document.body).hide();
	
	if (isshow)
        loading.show();
        
	var request = $.ajax({
		url: "/helper/execute_helper.php?cmd=" + cmd + "&date=" + (new Date()),
		type: "POST",
		data: param,
		dataType: "json"
	});
		
	request.done(function(jsonData){
		WG_ajax_execute_CallBack(jsonData, executecallback, cmd);
		loading.remove();
	});
	
	request.fail(function(jqXHR, textStatus){
		executecallback(false, "Helper Error - " + textStatus);
        	
		loading.remove();
	})
}

function WG_ajax_execute_CallBack(jsonData, executecallback, cmd)
{   
	if (jsonData.result == "0")
    {
        executecallback(false, jsonData.reason);
    }
    else
    {
    	executecallback(true, jsonData.reason);
    }
}

function WG_ajax_query(cmd, param, querycallback, isshow)
{
	var loading = $('<div id="loading" class="loading"></div><img id="loading_img" alt="loading" src="' + tmp_hosturl + '/images/loading.gif" />').appendTo(document.body).hide();
	
	if (isshow)
        loading.show();

	var request = $.ajax({
		url: "/helper/query_helper.php?cmd=" + cmd + "&date=" + (new Date()),
		type: "POST",
		data: param,
		dataType: "json"
	});

	request.done(function(jsonData){
		WG_ajax_query_CallBack(jsonData, querycallback, cmd);
		loading.remove();
	});
	
	request.fail(function(jqXHR, textStatus){
		WG_ajax_query_CallBack(false, "Helper Error - " + textStatus);
		loading.remove();
	})
}

function WG_ajax_query_CallBack(jsonData, querycallback, cmd)
{   
	if (jsonData.result == "0")
    {
        querycallback(false, jsonData.reason);
    }
    else
    {
    	querycallback(true, "", jsonData.data);
    }
}

function WG_ajax_list(cmd, param, querycallback, isshow)
{
	var loading = $('<div id="loading" class="loading"></div><img id="loading_img" alt="loading" src="' + tmp_hosturl + '/images/loading.gif" />').appendTo(document.body).hide();
	
	if (isshow)
        loading.show();

	var request = $.ajax({
		url: "/helper/list_helper.php?cmd=" + cmd + "&date=" + (new Date()),
		type: "POST",
		data: param,
		dataType: "json"
	});

	request.done(function(jsonData){
		WG_ajax_list_CallBack(jsonData, querycallback, cmd);
		loading.remove();
	});
	
	request.fail(function(jqXHR, textStatus){
		WG_ajax_list_CallBack(false, "Helper Error - " + textStatus);
		loading.remove();
	})
}

function WG_ajax_list_CallBack(jsonData, listcallback, cmd)
{   
	if (jsonData.result == "0")
    {
        listcallback(false, jsonData.reason);
    }
    else
    {
    	listcallback(true, "", jsonData.data.length, jsonData.data);
    }
}