﻿function go_page(url)
{
	window.location.href = url;
}

function go_facebook(userid)
{
	window.open("https://www.facebook.com/"+userid);
}

function view_admin_dtl(adminidx)
{
	window.location.href = "/m11_admin/admin_write.php?adminidx=" + adminidx;
}

function view_product_dtl(productidx)
{
	window.location.href = "/m1_product/product_write.php?productidx=" + productidx;
}

function view_product_ios_dtl(productidx)
{
	window.location.href = "/m1_product/product_ios_write.php?productidx=" + productidx;
}

function view_product_android_dtl(productidx)
{
	window.location.href = "/m1_product/product_android_write.php?productidx=" + productidx;
}

function view_product_amazon_dtl(productidx)
{
	window.location.href = "/m1_product/product_amazon_write.php?productidx=" + productidx;
}

function view_order_dtl(orderidx)
{
	parent.window.location.href = "/m1_product/order_view.php?orderidx=" + orderidx;
}

function view_ios_order_dtl(orderidx)
{
	parent.window.location.href = "/m1_product/order_ios_view.php?orderidx=" + orderidx;
}

function view_android_order_dtl(orderidx)
{
	parent.window.location.href = "/m1_product/order_android_view.php?orderidx=" + orderidx;
}

function view_amazon_order_dtl(orderidx)
{
	parent.window.location.href = "/m1_product/order_amazon_view.php?orderidx=" + orderidx;
}

function view_user_dtl(useridx,category)
{
	window.location.href = "/m2_user/user_view.php?useridx=" + useridx + "&category=" + category;
}

function view_user_dtl_new(useridx,category)
{	
	window.open("/m2_user/user_view.php?useridx=" + useridx + "&category=" + category);
}

function view_event_dtl(eventidx)
{
    window.location.href = "/m6_game/event_write.php?eventidx="+eventidx;
}

function view_earn_order_dtl(orderidx)
{
	parent.window.location.href = "/m1_product/order_earn_view.php?orderidx=" + orderidx;
}

function view_coin_increase_user_dtl(logidx)
{
	window.location.href = "/m8_monitoring/coin_increase_user_write.php?logidx="+logidx;
}

function view_coin_increase_user_new_dtl(logidx)
{
	window.location.href = "/m8_monitoring/coin_increase_user_new_write.php?logidx="+logidx;
}

function view_unknowncoin_user_dtl(logidx)
{
	window.location.href = "/m8_monitoring/unknowncoin_user_write.php?logidx="+logidx;
}

function view_unknowncoin_user_new_dtl(logidx)
{
	window.location.href = "/m8_monitoring/unknowncoin_user_new_write.php?logidx="+logidx;
}

function view_abuse_slot_user_dtl(logidx)
{
	window.location.href = "/m8_monitoring/abuse_slot_user_new_write.php?logidx="+logidx;
}

function view_abuse_suspicion_user_dtl(logidx)
{
	window.location.href = "/m8_monitoring/abuse_suspicion_user_write.php?logidx="+logidx;
}

function view_gain_high_coin_user_dtl(today, useridx, slottype)
{
	window.location.href = "/m8_monitoring/slot_gain_high_coin_user_write.php?today=" + today + "&useridx=" + useridx+"&slottype="+slottype;
}

function view_and_push_dtl(mesgidx)
{
	window.location.href = "/m6_game/and_push_write.php?mesgidx="+mesgidx;
}

function view_mobile_push_event_dtl(eventidx)
{
	window.location.href = "/m6_game/mobile_push_event_write.php?eventidx="+eventidx;
}

function view_cross_promotion_dtl(eventidx)
{
	window.location.href = "/m6_game/cross_promotion_write.php?eventidx="+eventidx;
}

function view_ab_test_detail(markingidx)
{
	window.location.href = "/m5_game_stats/ab_test/ab_test_list_detail.php?markingidx="+markingidx;
}