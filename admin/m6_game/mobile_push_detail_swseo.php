<?
    $top_menu = "game";
    $sub_menu = "mobile_push_detail";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $os = ($_GET["os"] == "") ? "ALL" : $_GET["os"];
    $category = $_GET["category"];
    $startdate = ($_GET["startdate"] == "") ? date("Y-m-d", mktime(0,0,0,date("m"),date("d")-7,date("Y"))) : $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
    $image_view = ($_GET["image_view"] == "") ? 0 : $_GET["image_view"];
    
    if($os == "1")
    	$os_tail = "_ios";
    else if($os == "2")
    	$os_tail = "_android";
    else if($os == "3")
    	$os_tail = "_amazon";
    
    $pagename = "mobile_push_detail.php";
    $pagefield = "os=$os&category=$category&startdate=$startdate&enddate=$enddate";
    
    $tail = "";
    
    if($category != "")
    	$tail .= " AND push_code LIKE '".$category."_%' ";
    	
	$db_main2 = new CDatabase_Main2();
	$db_mobile = new CDatabase_Mobile();
    
    if($os == "ALL")
    {
	    $sql = "SELECT writedate, push_code, send_count, invalid_count,
					(engage_count_ios + engage_count_android + engage_count_amazon) AS engage_count, 
					(return_count_ios + return_count_android + return_count_amazon) AS return_count,
					(return_count_ios_2week + return_count_android_2week + return_count_amazon_2week) AS return_count_2week,
	    			(return_count_ios_3week + return_count_android_3week + return_count_amazon_3week) AS return_count_3week,
	    			(return_count_ios_4week + return_count_android_4week + return_count_amazon_4week) AS return_count_4week,
                    REPLACE(SUBSTRING_INDEX(push_code, '_', 1), '2', '') AS CODE
	    		FROM tbl_push_event_stat
	    		WHERE writedate BETWEEN '$startdate' AND '$enddate' $tail
	    		ORDER BY writedate DESC, code ASC";
    }
    else
    {
    	$sql = "SELECT writedate, push_code, send_count$os_tail, invalid_count$os_tail, engage_count$os_tail AS engage_count, 
				return_count$os_tail AS return_count,
				return_count".$os_tail."_week AS return_count_week,
    			return_count".$os_tail."_2week AS return_count_2week,
    			return_count".$os_tail."_3week AS return_count_3week,
    			return_count".$os_tail."_4week AS return_count_4week,
    			REPLACE(SUBSTRING_INDEX(push_code, '_', 1), '2', '') AS CODE
    			FROM tbl_push_event_stat
    			WHERE writedate BETWEEN '$startdate' AND '$enddate' $tail
    			ORDER BY writedate DESC, code ASC";
    }
    
    $push_event_list = $db_main2->gettotallist($sql);
    
    $sql = "SELECT writedate, COUNT(code) AS rowspan
			FROM (
				SELECT writedate, REPLACE(SUBSTRING_INDEX(push_code, '_', 1), '2', '') AS code
				FROM tbl_push_event_stat
				WHERE writedate BETWEEN '$startdate' AND '$enddate' $tail
				ORDER BY writedate DESC, code ASC
			) t1
			GROUP BY writedate, code
			ORDER BY writedate DESC, code ASC";
    $push_code_rowspan_list = $db_main2->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) ".
      		"FROM tbl_push_event_stat ".
      		"WHERE writedate BETWEEN '$startdate' AND '$enddate' $tail ";
    $totalcount = $db_main2->getvalue($sql);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<link type="text/css" href="/js/themes/base/jquery.ui.datepicker.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery-ui.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	    search_form.submit();
	}

	function pop_push_image(eventcode)
	{
		open_layer_popup("pop_push_image.php?pushcode=" + eventcode, 1000, 550, event.pageY);
	}

	$(function() {
        $("#startdate").datepicker({ });
    });

    $(function() {
        $("#enddate").datepicker({ });
    });
</script>

	<!-- CONTENTS WRAP -->
	<div class="contents_wrap">
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt;모바일 PUSH 현황<span class="totalcount">(<?= number_format($totalcount) ?>)</span></div>
		</div>
		<!-- //title_warp -->
		
		<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
		    <div class="detail_search_wrap">
		    	<span class="search_lbl">플랫폼</span>
		    	<select id="os" name="os">
		            <option value="">전체</option>
		            <option value="1" <?= ($os=="1") ? "selected" : "" ?>>iOS</option>
		            <option value="2" <?= ($os=="2") ? "selected" : "" ?>>Android</option>
		            <option value="3" <?= ($os=="3") ? "selected" : "" ?>>Amazon</option>
		        </select>&nbsp;
		    	<span class="search_lbl">이벤트 종류</span>
		    	<select id="category" name="category">
		            <option value="">전체</option>
		            <option value="PU_lk" <?= ($category=="PU_lk") ? "selected" : "" ?>>Lucky Wheel</option>
					<option value="PU_fc" <?= ($category=="PU_fc") ? "selected" : "" ?>>Free Coin</option>
					<option value="PU_ga" <?= ($category=="PU_ga") ? "selected" : "" ?>>Give A Way</option>
					<option value="PU_sc" <?= ($category=="PU_sc") ? "selected" : "" ?>>Scratch Card</option>
					<option value="PU_sof" <?= ($category=="PU_sof") ? "selected" : "" ?>>Slot of Fortune</option>
					<option value="PU_ge" <?= ($category=="PU_ge") ? "selected" : "" ?>>Golden Egg</option>							
					<option value="PU_fs" <?= ($category=="PU_fs") ? "selected" : "" ?>>Free Spins</option>
		        </select>&nbsp;
		        <span class="search_lbl">등록일</span>
		        <input type="text" class="search_text" id="startdate" name="startdate" style="width:70px" value="<?= $startdate ?>" 
					onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
		        <input type="text" class="search_text" id="enddate" name="enddate" style="width:70px" value="<?= $enddate ?>" 
		        	onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
		        <div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		        &nbsp;&nbsp;
		        <input type="checkbox" name="image_view" id="image_view" value="1" <?= ($image_view == 1) ? "checked" :"" ?>> 이미지 리스트에서 보기
		    </div>
		</form>
		
		<table class="tbl_list_basic1">
		<colgroup>
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
		        <col width="">
		        <col width="">
		        <col width="">
		        <col width="">
		</colgroup>
			
		<thead>
			<tr>
				<th rowspan="2">등록일</th>
				<th rowspan="2">이벤트 타입</th>
				<th rowspan="2">전송된 푸시수</th>
				<th rowspan="2">총 참여인원</th>
				<th rowspan="2">참여비율</th>
				<th rowspan="2">앱 삭제 사용자수</th>
				<th colspan="5">이탈 복귀수</th>
				<th rowspan="2">이미지보기</th>
			</tr>
			<tr>
				<th>0~6 일</th>
				<th>7~13 일</th>
				<th>14~20 일</th>
				<th>21~27 일</th>
				<th>28이상</th>			
			</tr>
		</thead>
		<tbody>
<?
	$date_tmp = "";
	
	$rowcount_list = array();
	$rowcount = 1;
	$rowcount_index = 0;
	
	for($i=0; $i<sizeof($push_event_list); $i++)
	{
		if($push_event_list[$i]["writedate"] != $push_event_list[$i+1]["writedate"])
		{
			$rowcount_list[$rowcount_index++] = $rowcount;
			$rowcount = 1;
		}
		if($push_event_list[$i]["writedate"] == $push_event_list[$i+1]["writedate"])
			$rowcount++;
	}
	
	$rowcount_index = 0;
	$eventtype_rowspan_index = 0;
	
	for ($i=0; $i<sizeof($push_event_list); $i++)
	{
	    $push_code = $push_event_list[$i]["push_code"];
	    $origin_pushcode = $push_event_list[$i]["push_code"];
		$send_count = $push_event_list[$i]["send_count$os_tail"];
		$invalid_count = $push_event_list[$i]["invalid_count$os_tail"];
		$writedate = $push_event_list[$i]["writedate"];
		$engage_count = $push_event_list[$i]["engage_count"];
		$return_count = $push_event_list[$i]["return_count"];
		$return_count_week = $push_event_list[$i]["return_count_week"];
		$return_count_2week = $push_event_list[$i]["return_count_2week"];
		$return_count_3week = $push_event_list[$i]["return_count_3week"];
		$return_count_4week = $push_event_list[$i]["return_count_4week"];
		$code = $push_event_list[$i]["CODE"];
		
		$imageurl = "";
		
		// ImageURL
		if(substr($push_code, 0, 5) == "PU_lk" || substr($push_code, 0, 5) == "PU_fc" || substr($push_code, 0, 5) == "PU_ga" || substr($push_code, 0, 5) == "PU_ge" || substr($push_code, 0, 5) == "PU_fs" || substr($push_code, 0, 6) == "PU_sof" || substr($push_code, 0, 5) == "PU_sc")
		{
			$sql = "SELECT image_url FROM tbl_push_event WHERE push_code = '$push_code'";
			$imageurl = $db_main2->getvalue($sql);
		}
		else if(substr($push_code, 0, 5) == "PU_BA" || substr($push_code, 0, 2) == "BA")
			$imageurl = "https://d3n41vkwtxfepl.cloudfront.net/take5/server_push_image/push_image/comeback_fix.png";
		else if(substr($push_code, 0, 5) == "PU_FV" || substr($push_code, 0, 2) == "FV")
			$imageurl = "https://d3n41vkwtxfepl.cloudfront.net/take5/2019_oct_mobile/extradaily.png";
		else if(substr($push_code, 0, 5) == "PU_QA" || substr($push_code, 0, 2) == "QA")
			$imageurl = "https://d3n41vkwtxfepl.cloudfront.net/take5/server_push_image/push_image/inquiryanswered.png";
		else if(substr($push_code, 0, 5) == "PU_WP" || substr($push_code, 0, 2) == "WP")
			$imageurl = "https://d3n41vkwtxfepl.cloudfront.net/take5/server_push_image/push_image/timetowakeup.png";
		else if(substr($push_code, 0, 5) == "PU_NP" || substr($push_code, 0, 2) == "NP")
			$imageurl = "https://d3n41vkwtxfepl.cloudfront.net/take5/server_push_image/push_image/newuser_push.png";
		else if(substr($push_code, 0, 5) == "PU_CP" || substr($push_code, 0, 2) == "CP")
			$imageurl = "https://d3n41vkwtxfepl.cloudfront.net/take5/server_push_image/push_image/vipcoupon_push.png";
		else if(substr($push_code, 0, 5) == "PU_MS" || substr($push_code, 0, 2) == "MS")
			$imageurl = "메세지 푸시";
		else if(substr($push_code, 0, 5) == "PU_FG" || substr($push_code, 0, 2) == "FG")
			$imageurl = "https://d3n41vkwtxfepl.cloudfront.net/take5/server_push_image/push_image/firstbuy_fix.png";
		else if(substr($push_code, 0, 5) == "PU_jf" || substr($push_code, 0, 5) == "PU_sa")
		{
			$sql = "SELECT image_url FROM tbl_message WHERE push_code = '$push_code'";
			$imageurl = $db_mobile->getvalue($sql);
		}
        else if(substr($push_code, 0, 3) == "vip")
            $imageurl = "https://d3n41vkwtxfepl.cloudfront.net/take5/server_push_image/push_image/vvipunder10000.png";

		if(substr($push_code, 0, 5) == "PU_lk")
		    $push_code = "Lucky Wheel";
	    else if(substr($push_code, 0, 5) == "PU_fc")
	        $push_code = "Free Coin";
        else if(substr($push_code, 0, 5) == "PU_ga")
            $push_code = "Give A Way";
        else if(substr($push_code, 0, 5) == "PU_sc") 
            $push_code = "Scratch Card";
        else if(substr($push_code, 0, 6) == "PU_sof")
            $push_code = "Slot of Fortune";
        else if(substr($push_code, 0, 5) == "PU_ge")
            $push_code = "Golden Egg";
        else if(substr($push_code, 0, 5) == "PU_fs")
            $push_code = "Free Spin";
        else if(substr($push_code, 0, 5) == "PU_BA" || substr($push_code, 0, 2) == "BA")
            $push_code = "3~9이탈자";
        else if(substr($push_code, 0, 5) == "PU_FV" || substr($push_code, 0, 2) == "FV")
            $push_code = "$5이상결제";
        else if(substr($push_code, 0, 5) == "PU_QA" || substr($push_code, 0, 2) == "QA")
            $push_code = "QA답변";
        else if(substr($push_code, 0, 5) == "PU_WP" || substr($push_code, 0, 2) == "WP")
            $push_code = "WakeUp";
        else if(substr($push_code, 0, 5) == "PU_NP" || substr($push_code, 0, 2) == "NP")
            $push_code = "신규가입자";
        else if(substr($push_code, 0, 5) == "PU_CP" || substr($push_code, 0, 2) == "CP")
            $push_code = "쿠폰푸시";
        else if(substr($push_code, 0, 5) == "PU_MS" || substr($push_code, 0, 2) == "MS")
            $push_code = "메세지 푸시";
        else if(substr($push_code, 0, 5) == "PU_FG" || substr($push_code, 0, 2) == "FG")
            $push_code = "생애첫결제";
		else if(substr($push_code, 0, 5) == "PU_jf")
            $push_code = "잭팟 피에스타";
        else if(substr($push_code, 0, 5) == "PU_sa")
            $push_code = "시즌 세일";
        else if(substr($push_code, 0, 3) == "vip")
            $push_code = "VIP 이탈관리";
        else
            $push_code = " ";
		
?>
            	
<?
		if($i == 0 || $date_tmp != $writedate)
		{
?>
			<tr onmouseover="className='tr_over'" onmouseout="className=''" style="border-top:1px double;">
				<td class="tdc point" rowspan="<?= $rowcount_list[$rowcount_index] ?>"><?= $writedate ?></td>
<?
			$rowcount_index++;
		}
		
		if($i == 0 || $code != $push_event_list[$i-1]["code"] || $date_tmp != $writedate)
		{
?>
				<td class="tdc point" rowspan="<?= $eventcode_rowspan_list[$eventtype_rowspan_index++]["rowspan"] ?>"><?= $push_code ?></td>
<?
		}
?>
				<td class="tdr"><?= ($send_count == "") ? "0" : number_format($send_count) ?></td>
				<td class="tdr"><?= ($engage_count == "") ? "0" : number_format($engage_count) ?></td>
				<td class="tdr"><?= ($send_count == 0) ? "0" : round(($engage_count / $send_count) * 100, 1) ?>%</td>
				<td class="tdr"><?= ($invalid_count == "") ? "0" : number_format($invalid_count) ?></td>
				<td class="tdr"><?= ($return_count == "") ? "0" : number_format($return_count) ?></td>
				<td class="tdr"><?= ($return_count_week == "") ? "0" : number_format($return_count_week) ?></td>
				<td class="tdr"><?= ($return_count_2week == "") ? "0" : number_format($return_count_2week) ?></td>
				<td class="tdr"><?= ($return_count_3week == "") ? "0" : number_format($return_count_3week) ?></td>
				<td class="tdr"><?= ($return_count_4week == "") ? "0" : number_format($return_count_4week) ?></td>
<? 
		if($image_view == 1)
		{
?>
				<td class="tbc"><img width="100" height="50" src="<?= $imageurl?>" /></td>
<? 
		}
		else
		{
?>
				<td class="tdc"><input type="button" class="btn_search" value="보기" onclick="pop_push_image('<?= $origin_pushcode?>')" /></td>
<? 
		}
?>
			</tr>
<?
		$date_tmp = $writedate;
	}
?>
		</tbody>
		</table>
    </div>
<!-- CONTENTS WRAP -->

<?
	$db_main2->end();
	$db_mobile->end();

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?> 