<?php
	error_reporting(E_ALL);
	require '../common/aws_sdk/vendor/autoload.php';
	use Aws\Common\Aws;
	use Aws\S3\Enum\CannedAcl;
	
	ini_set('display_errors','On');
	$uploaddir = '/tmp/';
	$path = $_POST['path'];
	try{
	    $aws = Aws::factory('../common/aws_sdk/aws_config.php');
		$s3 = $aws->get('s3');
		
		foreach($_FILES['userfile']['name'] as $key => $val){
			
			$uploadfile = $uploaddir . basename($_FILES['userfile']['name'][$key]);
			
			if (move_uploaded_file($_FILES['userfile']['tmp_name'][$key], $uploadfile)) 
			{
				$result = $s3->putObject(array(
						'Bucket' => 'dug-event-img',
						'Key'    => $path.$_FILES['userfile']['name'][$key],
						'Body'   => fopen($uploadfile, 'r'),
						'ACL'    => CannedAcl::PUBLIC_READ,
						'ContentType'=>mime_content_type($uploadfile)
				));
				
			} 
			else 
			{
			    die('업로드에 실패 했습니다');
			}
		}
	} catch(S3Exception $e){
		print_r($e);
	}
	exit();
?>
