<?
    $top_menu = "game";
    $sub_menu = "cross_promotion";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $db_main2 = new CDatabase_Main2();
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_title = $_GET["title"];
    $game_type = $_GET["game_type"];
    
    $listcount = 10;
    $pagename = "cross_promotion.php";
    $pagefield = "title=$search_title";
    
    $tail = "WHERE 1=1 ";
    
    if ($search_title != "")
    	$tail .= " AND title LIKE '%".encode_db_field($search_title)."%' ";
    
    if ($game_type != "")
    	$tail .= " AND game_type='$game_type' ";
    
    $sql = "SELECT  eventidx, game_type, title, ENABLE, reward_amount, logo_path, title_path, popup_path, writedate ".
			"FROM `tbl_cross_promotion_event` $tail ORDER BY eventidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;

    $cross_promotioneventlist = $db_main2->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) FROM tbl_cross_promotion_event $tail";
    $totalcount = $db_main2->getvalue($sql);
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
    	$page = floor(($totalcount + $listcount - 1) / $listcount);
    
    $db_main2->end();
?>
<script type="text/javascript">
function search_press(e)
{
    if (((e.which) ? e.which : e.keyCode) == 13)
    {
        search();
    }
}

function search()
{
    var search_form = document.search_form;
    search_form.submit();
}

function go_result(eventidx)
{
    window.location.href = "cross_promotion_result.php?eventidx="+eventidx;
} 

function tab_change(fanpage)
{
    var search_form = document.search_form;
    search_form.fanpage.value = fanpage;
    search_form.submit();
}    
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
    
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Cross Promotion 관리 (<?= make_price_format($totalcount) ?>)</div>
	</div>
	<!-- //title_warp -->
        
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
		<div class="detail_search_wrap">
			<span class="search_lbl">게임 타입</span>
			<select id="game_type" name="game_type">
				<option value="">전체</option>
				<option value="1" <?= ($game_type=="1") ? "selected" : "" ?>>DUC</option>
				<option value="2" <?= ($game_type=="2") ? "selected" : "" ?>>DUB</option>
				<option value="3" <?= ($game_type=="3") ? "selected" : "" ?>>OES</option>
				<option value="4" <?= ($game_type=="4") ? "selected" : "" ?>>HVS</option>
			</select>&nbsp;
			<span class="search_lbl">Cross Promotion 제목</span>
			<input type="text" class="search_text" id="title" name="title" style="width:250px" value="<?= encode_html_attribute($search_title) ?>" onkeypress="search_press(event)" />&nbsp;
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>
        
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="40">
			<col width="70">
			<col width="">
			<col width="70">
			<col width="80">
		</colgroup>
		<thead>
			<tr>
				<th>번호</th>
				<th>게임 타입</th>
				<th class="tbl">Cross Promotion 제목</th>
				<th>상태</th>
				<th>등록일</th>
				<!--th>결과</th-->
			</tr>
		</thead>
		<tbody>
<?
for ($i=0; $i<sizeof($cross_promotioneventlist); $i++)
{
    $eventidx = $cross_promotioneventlist[$i]["eventidx"];
    $game_type = $cross_promotioneventlist[$i]["game_type"];
    $title = $cross_promotioneventlist[$i]["title"];
    $ENABLE = $cross_promotioneventlist[$i]["ENABLE"];
    $writedate = $cross_promotioneventlist[$i]["writedate"];
    
    if($game_type =='1')
    	$game_type_str = "DUC";
    else if($game_type =='2')
    	$game_type_str = "DUB";
    else if($game_type =='3')
    	$game_type_str = "OES";
    else if($game_type =='4')
    	$game_type_str = "HVS";
    else 
    	$game_type_str = "none";
?>
        <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="view_cross_promotion_dtl(<?= $eventidx ?>)">
            <td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
            <td class="tbl point_title"><?= $game_type_str ?></td>
            <td class="tbl point_title"><?= $title ?></td>
            <td class="tdc"><?= ($ENABLE == "1") ? "활성" : "비활성" ?></td> 
            <td class="tdc"><?= $writedate ?></td> 
            <!--td class="tdc"><input type="button" class="btn_03" value="보기" style="cursor:pointer" onclick="event.cancelBubble=true;go_result(<?= $eventidx ?>)" /></td-->
        </tr>
<?
} 
?>
	</tbody>
</table>
<?
include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>

<div class="button_warp tdr">
	<input type="button" class="btn_setting_01" value="Cross Promotion 추가" onclick="window.location.href='cross_promotion_write.php'">           
</div>            
</div>
<!--  //CONTENTS WRAP -->
    
<div class="clear"></div>
<?
include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>