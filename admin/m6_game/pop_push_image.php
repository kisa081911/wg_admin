<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/common_include.inc.php");
    
    check_login_layer();
    
    $push_code = $_GET["pushcode"];
    
    if ($push_code == "")
        error_close_layer("잘못된 접근입니다.");
    
	$db_main2 = new CDatabase_Main2();
	$db_mobile = new CDatabase_Mobile();
	
	$imageurl = "";
    
	if(substr($push_code, 0, 5) == "PU_lk" || substr($push_code, 0, 5) == "PU_fc" || substr($push_code, 0, 5) == "PU_ga" || substr($push_code, 0, 5) == "PU_ge" || substr($push_code, 0, 5) == "PU_fs" || substr($push_code, 0, 6) == "PU_sof" || substr($push_code, 0, 5) == "PU_sc")
	{
		$sql = "SELECT image_url FROM tbl_push_event WHERE push_code = '$push_code'";
		$imageurl = $db_main2->getvalue($sql);
	}
	else if(substr($push_code, 0, 5) == "PU_BA" || substr($push_code, 0, 2) == "BA")
		$imageurl = "https://dug-event-img.doubleugames.com/take5/server_push_image/push_image/comeback_fix.png";
	else if(substr($push_code, 0, 5) == "PU_FV" || substr($push_code, 0, 2) == "FV")
		$imageurl = "https://dug-event-img.doubleugames.com/take5/2019_oct_mobile/extradaily.png";
	else if(substr($push_code, 0, 5) == "PU_QA" || substr($push_code, 0, 2) == "QA")
		$imageurl = "https://dug-event-img.doubleugames.com/take5/server_push_image/push_image/inquiryanswered.png";
	else if(substr($push_code, 0, 5) == "PU_WP" || substr($push_code, 0, 2) == "WP")
		$imageurl = "https://dug-event-img.doubleugames.com/take5/server_push_image/push_image/timetowakeup.png";
	else if(substr($push_code, 0, 5) == "PU_NP" || substr($push_code, 0, 2) == "NP")
		$imageurl = "https://dug-event-img.doubleugames.com/take5/server_push_image/push_image/newuser_push.png";
	else if(substr($push_code, 0, 5) == "PU_CP" || substr($push_code, 0, 2) == "CP")
		$imageurl = "https://dug-event-img.doubleugames.com/take5/server_push_image/push_image/vipcoupon_push.png";
	else if(substr($push_code, 0, 5) == "PU_MS" || substr($push_code, 0, 2) == "MS")
		$imageurl = "메세지 푸시";
	else if(substr($push_code, 0, 5) == "PU_FG" || substr($push_code, 0, 2) == "FG")
		$imageurl = "https://dug-event-img.doubleugames.com/take5/server_push_image/push_image/firstbuy_fix.png";
	else if(substr($push_code, 0, 5) == "PU_jf" || substr($push_code, 0, 5) == "PU_sa")
	{
		$sql = "SELECT image_url FROM tbl_message WHERE push_code = '$push_code'";
		$imageurl = $db_mobile->getvalue($sql);
	}
	else if(substr($push_code, 0, 3) == "vip")
		$imageurl = "https://dug-event-img.doubleugames.com/take5/server_push_image/push_image/vvipunder10000.png";	
		
	$db_main2->end();
	$db_mobile->end();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Push Image</title>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script>
<script type="text/javascript">
    function layer_close()
    {
        window.parent.close_layer_popup("<?= $_GET["layer_no"] ?>");
    }
</script>
</head>
<body class="layer_body" onload="window.focus()" onkeyup="if (getEventKeycode(event) == 27) { layer_close(); }">
<div id="layer_wrap">   
    <div class="layer_header" >
        <div class="layer_title">Push Image</div>
        <img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="layer_close()" />
    </div>        
    <div class="layer_contents_wrap" style="width:100%; height:100%">
         <!--  레이어 내용  -->
         <img width="100%" height="100%" src="<?= $imageurl?>"></img>
         <div class="layer_user_fix_height">
         </div>
         <!--  //레이어 내용  -->
    </div>
</div>
</body>
</html>

