<?
    $top_menu = "game";
    $sub_menu = "fanpage";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
        
    $articleid = $_GET["articleid"];
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_title = $_GET["title"];
    
    $listcount = 30;

    $pagename = "fanpage_comment.php";
    $pagefield = "articleid=$articleid&title=$search_title&";
	
	if ($articleid == '')
		error_back("잘못된 접근입니다.");

    $tail = "WHERE articleid='$articleid' ";

    if ($search_title != "")
        $tail .= " AND message LIKE '%".encode_db_field($search_title)."%' ";
    
    $db_analysis = new CDatabase_Analysis();
    
    $sql = "SELECT commentid,useridx,message,likes,writedate,gifttype,(SELECT IFNULL(SUM(giftamount),0) FROM fanpage_article_comment WHERE articleid='$articleid' AND useridx=A.useridx) AS giftamount,(SELECT IFNULL(SUM(giftamount),0) FROM fanpage_article_comment WHERE useridx=A.useridx) AS totalgiftamount ".
        "FROM fanpage_article_comment A $tail and useridx<>0 ORDER BY writedate LIMIT ".(($page-1) * $listcount).", ".$listcount;

    $commentlist = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) FROM fanpage_article_comment $tail and useridx<>0";
    $totalcount = $db_analysis->getvalue($sql);
	
    $db_analysis->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script type="text/javascript">
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	    search_form.submit();
	}

	function check_all(checked)
	{
		var checks = document.getElementsByName("user_check");
		
		for (var i=0; i<checks.length; i++)
		{
			if (!checks[i].disabled)
				checks[i].checked = checked;
		}
	}

	function send_gift()
	{
		var input_form = document.input_form;
		var userlist = "";
		var commentlist = "";
		var userchecklist = "";
				
		var checks = document.getElementsByName("user_check");
		
		for (var i=0; i<checks.length; i++)
		{
			if (checks[i].checked)
			{
				if (userchecklist.indexOf("[" + checks[i].value + "]") != -1)
					continue;
					
				if (userlist == "")
				{
					userlist = checks[i].value;
					commentlist = checks[i].getAttribute("commentid");
				}
				else		
				{
					userlist += "," + checks[i].value;
					commentlist += "," + checks[i].getAttribute("commentid");
				}
					
				userchecklist += "[" + checks[i].value + "]";
			}
		}
		
		if (input_form.reward_amount.value == "")
		{
			alert("선물 금액을 입력해주세요.")
			input_form.reward_amount.focus();
			return;
		}
		
	    if (input_form.reason.value == "")
	    {
	        alert("발급 사유를 입력해주세요.");
	        input_form.reason.focus();
	        return;
	    }
	    
	    if (input_form.reward_amount.value.indexOf("-") == -1)
	    {
	        if (input_form.message.value == "")
	        {
	            alert("보내는 메시지를 입력해주세요.");
	            input_form.message.focus();
	            return;
	        }
		}

		if(userlist == "")
		{
			alert("유저를 선택해주세요.");
			return;
		}
		
		if (!confirm("선택한 유저에게 선물을 발급하시겠습니까?"))
			return;
			
        var param = {};
		param.articleid = "<?= $articleid ?>";
		param.userlist = userlist;
		param.commentlist = commentlist;
		param.reward_type = input_form.reward_type.value;
		param.reward_amount = strip_price_format(input_form.reward_amount.value);
		param.memo = ((input_form.reward_amount.value == "") ? "" : input_form.reason.value);
		param.message = ((input_form.reward_amount.value == "") ? "" : input_form.message.value);
		param.adminid = "<?= $login_adminid?>";

		WG_ajax_execute("game/send_comment_gift", param, send_comment_gift_callback, true);	
	}

	function send_comment_gift_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
        	alert("선물을 발급했습니다.");
        	window.location.reload(false);
        }
    }

	function send_gift_random()
	{
		alert('준비중');
	}

	function change_rewardtype(type)
    {
        if(type == "")
        {
            $("#row_reward_amount").hide();
        }
        else
        {
        	$("#row_reward_amount").show();

			var rewardname = "";
			var message = "";
        	
        	if(type == 1)
        	{
        		rewardname = " Coins";
        		message = "Take5 Free Slots Support Center has sent you Coins!";
        	}
    		
        	$("#reward_name").html(rewardname);
        	$("#message").val(message);
        }
    }
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 팬페이지 게시물 관리 - 댓글 목록 (<?= number_format($totalcount) ?>)</div>
	</div>
	<!-- //title_warp -->
	
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
		<input type=hidden name="articleid" value="<?= $articleid ?>"/>
		<div class="detail_search_wrap">
			<span class="search_lbl">댓글 내용</span>
			<input type="text" class="search_text" id="title" name="title" style="width:250px" value="<?= encode_html_attribute($search_title) ?>" onkeypress="search_press(event)" />
                    
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>
	
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="40">
			<col width="70">
			<col width="250">
			<col width="70">
			<col width="100">
			<col width="120">
			<col width="">
			<col width="60">
			<col width="120">
		</colgroup>
		<thead>
			<tr>
				<th><input type=checkbox onclick="check_all(this.checked)"></th>
				<th class="tbc">순번</th>
				<th class="tbl">사용자</th>
				<th class="tbl">선물타입</th>
				<th class="tdr">댓글 선물금액</th>
				<th class="tdr">총 댓글 선물금액</th>
				<th class="tbc">내용</th>
				<th class="tdc">Like</th>
				<th>등록일</th>
			</tr>
		</thead>
		<tbody>
<?
	$db_main = new CDatabase_Main();
	
    for ($i=0; $i<sizeof($commentlist); $i++)
    {
        $commentid = $commentlist[$i]["commentid"];
        $useridx = $commentlist[$i]["useridx"];
        $message = $commentlist[$i]["message"];
        $likes = $commentlist[$i]["likes"];
        $writedate = $commentlist[$i]["writedate"];
        $gifttype = $commentlist[$i]["gifttype"];
        $giftamount = $commentlist[$i]["giftamount"];
        $totalgiftamount = $commentlist[$i]["totalgiftamount"];
		
		$sql = "SELECT nickname,userid FROM tbl_user WHERE useridx=$useridx";
		$data = $db_main->getarray($sql);
		
		$name = $data["nickname"];
		$facebookid = $data["userid"];
 
		$sql = "SELECT COUNT(*) FROM tbl_user_online_sync2 WHERE useridx=$useridx";
		$online_status = $db_main->getvalue($sql);
		
		$gift_name = "";
		
		if($gifttype == 1)
			$gift_name = "Coin";
?>
            <tr>
                <td class="tdc"><input type=checkbox name="user_check" value="<?= $useridx ?>" commentid="<?= $commentid ?>" <?= ($giftamount != 0) ? "disabled" : "" ?>></td>
                <td class="tdc"><?= make_price_format(($page-1) * $listcount + $i + 1) ?></td>
                <td class="tbl"><span style="float:left;padding-top:8px;padding-right:2px;"><img src="/images/icon/<?= ($online_status == "1") ? "status_1.png" : "status_3.png" ?>" align="absmiddle" /></span>  <span style="float:left;"><img src="https://graph.facebook.com/<?= $facebookid ?>/picture?type=square&access_token=<?=$client_accesstoken?>" height="25" width="25" align="absmiddle" hspace="3"></span> <?= $name ?></td>
                <td class="tdc"><?= $gift_name ?></td>
                <td class="tdr"><?= make_price_format($giftamount) ?></td>
                <td class="tdr"><?= make_price_format($totalgiftamount) ?></td>
                <td class="tbl point_title"><?= $message ?></td>
                <td class="tdc point"><?= make_price_format($likes) ?></td>
                <td class="tdc"><?= $writedate ?></td> 
            </tr>
<?
    } 

	$db_main->end();
?>
		</tbody>
	</table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="선물 시상 내역" onclick="window.location.href='fanpage_comment_summary.php?articleid=<?= $articleid ?>'">           
	</div>            
	<br/>

	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title">선택 사용자 선물 발급</div>
	</div>
	<!-- //title_warp -->
	
	<form name="input_form" id="input_form" onsubmit="return false">
		<table class="tbl_view_basic">
            <colgroup>
                <col width="180">
                <col width="">
            </colgroup>
            <tbody>
            	<tr>
					<th>선물 타입</th>
					<td>
						<select id="reward_type" name="reward_type" onchange="change_rewardtype(this.value);">
							<option value="1">Coins</option>
						</select>
					</td>
				</tr>
				<tr>
					<th>선물 지급량</th>
					<td><input type="text" class="view_tbl_text" style="width:250px" name="reward_amount" id="reward_amount" value="" onkeydown="return keep_price_format(this)" onkeyup="return keep_price_format(this)" onblur="return keep_price_format(this)" onkeypress="return checknum()"/><span id="reward_name" style="font-weight:bold;"> Coins</span></td>
				</tr>
				<tr>
					<th>발급 사유</th>
					<td><input type="text" class="view_tbl_text" style="width:520px" name="reason" id="reason" maxlength="200" value="" /></td>
				</tr>
				<tr>
					<th>선물 보내는 메시지</th>
                    <td><input type="text" class="view_tbl_text" style="width:520px" name="message" id="message" maxlength="200" value="Take5 Free Slots Support Center has sent you Coins!" /></td>
				</tr>
			</tbody>
		</table>

		<div class="button_warp tdr">
			<input type="button" class="btn_setting_01" value="선물 발급" onclick="send_gift()">           
		</div>            

		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title">단체 선물 발급 - 댓글 내용으로 검색한 경우 검색된 댓글만이 발급 대상</div>
		</div>
		<!-- //title_warp -->

		<table class="tbl_view_basic">
			<colgroup>
				<col width="180">
                <col width="">
            </colgroup>
            <tbody>
				<tr>
					<th>발급 방식</th>
					<td>
						<input type=radio value="1" name="category" checked> 랜덤 Pick <input type=radio value="2" name="category"> 선착순 <input type=radio value="3" name="category"> 시간제한 
					</td>
				</tr>
				<tr>
					<th>발급 회원수</th>
					<td><input type="text" class="view_tbl_text" style="width:100px" name="totalcount" id="totalcount" value="" onkeydown="return keep_price_format(this)" onkeyup="return keep_price_format(this)" onblur="return keep_price_format(this)" onkeypress="return checknum()"/> 명 (시간 제한일 경우 발급 회원수가 아니고 [   ] 분 이내 댓글 단 사용자)</td>
				</tr>
				<tr>
					<th>선물 금액(Coin)</th>
					<td><input type="text" class="view_tbl_text" style="width:250px" name="freecoin2" id="freecoin2" value="" onkeydown="return keep_price_format(this)" onkeyup="return keep_price_format(this)" onblur="return keep_price_format(this)" onkeypress="return checknum()"/></td>
				</tr>
				<tr>
					<th>발급 사유</th>
					<td><input type="text" class="view_tbl_text" style="width:520px" name="reason2" id="reason2" maxlength="200" value="" /></td>
				</tr>
				<tr>
					<th>선물 보내는 메시지</th>
                    <td><input type="text" class="view_tbl_text" style="width:520px" name="message2" id="message2" maxlength="200" value="Take5 Free Slots Support Center has sent you Coins!" /></td>
				</tr>
            </tbody>
		</table>
	</form>
           	
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="랜덤 Pick 발급" onclick="send_gift_random()">           
	</div>
</div>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>