<?
    $top_menu = "game";
    $sub_menu = "regular_push";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $group_no = ($_GET["group_no"] == "") ? "" : $_GET["group_no"];
    $search_message = ($_GET["search_message"] == "") ? "" : $_GET["search_message"];

    $listcount = 10;
    $pagename = "regular_push.php";

    $tail = "AND 1 = 1";
    
    if($group_no != "")
        $tail .= " AND group_no = $group_no ";
    
    if($search_message != "")
    	$tail .= " AND message LIKE '%$search_message%' ";    
    
    $pagefield = "search_message=$search_message";    
    
    $db_mobile = new CDatabase_Mobile();
    
    $sql = "SELECT * FROM `tbl_push_imgpath` ".
      		"WHERE 1=1 $tail ".
      		"ORDER BY group_no ".
    		"DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
	
    $messagelist = $db_mobile->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) FROM `tbl_push_imgpath`".
      		"WHERE 1=1 $tail";
    $totalcount = $db_mobile->getvalue($sql);    
    
    $db_mobile->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script type="text/javascript">  
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    } 
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 서버 자동 PUSH 관리 (<?= make_price_format($totalcount) ?>)</div>
            </div>
            <!-- //title_warp -->
            
            <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
                <div class="detail_search_wrap">
                    <span class="search_lbl ml20">Message</span>
                    <input type="text" class="search_text" id="search_message" name="search_message" style="width:250px" value="<?= encode_html_attribute($search_message) ?>" onkeypress="search_press(event)" />
                    
                  	<span class="search_lbl ml20">푸시 타입</span>
                    <select id="group_no" name="group_no">

                      <option value="" <?= ($group_no == "") ? "selected" : "" ?>>전체</option>
                      <option value="1" <?= ($group_no == 1) ? "selected" : "" ?>>신규 가입자</option>
                      <option value="2" <?= ($group_no == 2) ? "selected" : "" ?>>VIP Coupon</option>
                      <option value="3" <?= ($group_no == 3) ? "selected" : "" ?>>Wake Up Event</option>
                      <option value="4" <?= ($group_no == 4) ? "selected" : "" ?>>QA 답변완료</option>
                      <option value="5" <?= ($group_no == 5) ? "selected" : "" ?>>QA 답변완료(리텐션)</option>
                      <option value="6" <?= ($group_no == 6) ? "selected" : "" ?>>$5이상 첫 결제자</option>
                      <option value="7" <?= ($group_no == 7) ? "selected" : "" ?>>3~9일 이탈자</option>
                      <option value="8" <?= ($group_no == 8) ? "selected" : "" ?>>VIP 유저 이탈</option>
                      <option value="8" <?= ($group_no == 9) ? "selected" : "" ?>>생애 첫 결제</option>
                     </select>					
					<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
                </div>
            </form>
                        
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="5%">
                <col width="10%">
                <col width="30%">
            </colgroup>
            <thead>
                <tr>
                    <th>번호</th>
                    <th>푸시타입</th>
                    <th class="tbl">Message</th>
                </tr>
            </thead>
            <tbody>
<?
    for ($i=0; $i<sizeof($messagelist); $i++)
    {
        $group_no = $messagelist[$i]["group_no"];
        $message = $messagelist[$i]["message"];
        $group_no_str="";

        if($group_no == 1)
            $group_no_str = "신규 가입자";
        else if($group_no == 2)
            $group_no_str = "VIP Coupon";
        else if($group_no == 3)
            $group_no_str = "Wake Up Event";
        else if($group_no == 4)
            $group_no_str = "QA 답변완료";
        else if($group_no == 5)
            $group_no_str = "QA 답변완료(리텐션)";
        else if($group_no == 6)
            $group_no_str = "$5이상 생애 첫 결제";
        else if($group_no == 7)
            $group_no_str = "3~7일 이탈자";
        else if($group_no == 8)
            $group_no_str = "VIP 유저 이탈";
        else if($group_no == 9)
            $group_no_str = "생애 첫 결제";
?>
            <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="window.location.href = '/m6_game/regular_push_write.php?group_no=<?=$group_no?>'">
                <td class="tdc"><?= $group_no ?></td>
                <td class="tdc"><?= $group_no_str ?></td>
                <td class="tbl point_title"><?= $message ?></td>
            </tr>
<?
    } 
?>
            </tbody>
            </table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>

            <!-- >div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="PUSH 추가" onclick="window.location.href='regular_push_write.php'">           
            </div-->            
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>