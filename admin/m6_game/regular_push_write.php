<?
    $top_menu = "game";
    $sub_menu = "regular_push";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $group_no = $_GET["group_no"];
    
    check_number($group_no);

    $db_mobile = new CDatabase_Mobile();
    
    if ($group_no != "")
    {
        $sql = "SELECT * FROM tbl_push_imgpath WHERE group_no=$group_no";        
        $message = $db_mobile->getarray($sql);
        
        $group_no = $message["group_no"];
        $msg = $message["message"];
        $title = $message["title"];
        $and_imgpath = $message["and_imgpath"];
        $ios_imgpath = $message["ios_imgpath"];
        $and_banner = $message["and_banner"];
        $text_visible = $message["text_visible"];
        		
        if ($group_no == "")
            error_back("삭제되었거나 등록되지 않은 push입니다.");
    }   
   
    $db_mobile->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript">
    function save_push()
    {
        var input_form = document.input_form;

        if (input_form.message.value == "" )
        {
            alert("Message를 입력해주세요.");
            input_form.message.focus();
            return;
        }
        
        if (input_form.title.value == "" )
        {
            alert("Title 를 입력해주세요.");
            input_form.title.focus();
            return;
        }
      
        var param = {};
        param.group_no = "<?= $group_no ?>";
        param.message = input_form.message.value;
        param.title = input_form.title.value;
        param.and_imgpath = input_form.and_imgpath.value;
        param.ios_imgpath = input_form.ios_imgpath.value;
        param.and_banner = input_form.and_banner.value;
        param.text_visible = input_form.text_visible.value;

        if ("<?= $group_no ?>" == "")
        	WG_ajax_execute("game/insert_regular_push", param, save_push_callback);
        else
        	WG_ajax_execute("game/update_regular_push", param, save_push_callback);
    }

    function save_push_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            window.location.href = "regular_push.php";
        }
    }
    
    function delete_push()
    {
        if (!confirm("해당 push를 삭제 하시겠습니까?"))
            return;

        var param = {};

        param.group_no =  "<?= $group_no ?>";

        WG_ajax_execute("game/delete_regular_push", param, delete_push_callback);
    }

    function delete_push_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.href = "regular_push.php";
        }
    }

    $(function() {
        $("#reservation_date").datepicker({ });
        $("#startdate_date").datepicker({ });
        $("#enddate_date").datepicker({ });
    });    
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 서버 자동 PUSH 관리</div>
<?
if ($group_no != "")
    { 
?>
            <!-- div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_push()"></div-->
<?
    } 
?>
            </div>
            <!-- //title_warp -->
            
            <form name="input_form" id="input_form" onsubmit="return false">
            
            <div class="h2_title pt20">[모바일 PUSH 설정]</div>
            <table class="tbl_view_basic">
                <colgroup>
                    <col width="180">
                    <col width="">
                </colgroup>
                <tbody>
                    <tr>
                        <th>Message</th>
                        <td>
                            <textarea style="width:700px;height:100px;" name="message" id="message"><?= $msg ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>Title</th>
                        <td>
                            <input type="text" class="view_tbl_text" name="title" id="title" maxlength="200" value="<?= $title ?>" /><br/>
                        </td>
                    </tr>
                    <tr>
                        <th>Android 푸시 이미지</th>
                        <td>
                            <input type="text" class="view_tbl_text" name="and_imgpath" id="and_imgpath" maxlength="200" value="<?= $and_imgpath ?>" /><br/>
<?
    if ($and_imgpath != "")
    {
?>
                            <img src="<?= $and_imgpath ?>">
<?
    } 
?>
   
                        </td>
                    </tr>
                    <tr>
                        <th>IOS 푸시 이미지</th>
                        <td>
                            <input type="text" class="view_tbl_text" name="ios_imgpath" id="ios_imgpath" maxlength="200" value="<?= $ios_imgpath ?>" /><br/>
<?
    if ($ios_imgpath != "")
    {
?>
                            <img src="<?= $ios_imgpath ?>">
<?
    } 
?>
   
                        </td>
                    </tr>
                    <tr>
                        <th>Android 배너 이미지</th>
                        <td>
                            <input type="text" class="view_tbl_text" name="and_banner" id="and_banner" maxlength="200" value="<?= $and_banner ?>" /><br/>
<?
    if ($and_banner  != "")
    {
?>
                            <img src="<?= $and_banner?>">
<?
    } 
?>
   
                        </td>
                    </tr>
                    <tr>
                        <th>Text Visible </th>
                            <td>
                            <select id="text_visible" name="text_visible" >
                                <option value="">선택</option>
                                <option value="1" <?= ($text_visible=="1") ? "selected" : "" ?>>사용</option>
                                <option value="0" <?= ($text_visible=="0") ? "selected" : "" ?>>미사용</option>
                            </select>
                        </td>
                    </tr>

                </tbody>
            </table>
            </form>
            
            <div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="저장" onclick="save_push()">
                <input type="button" class="btn_setting_02" value="취소" onclick="go_page('regular_push.php')">            
            </div>
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>