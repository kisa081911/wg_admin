<?
    $top_menu = "game";
    $sub_menu = "cross_promotion";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $eventidx = $_GET["eventidx"];
    
    check_number($eventidx);
    
    $db_main2 = new CDatabase_Main2();
    
    if ($eventidx != "")
    {
        $sql = "SELECT  eventidx, game_type, title, enable , reward_amount, logo_path, title_path, popup_path, writedate ".
				" FROM `tbl_cross_promotion_event` WHERE eventidx= $eventidx";
        
        $cross_promotion_info = $db_main2->getarray($sql);
        
        $eventidx = $cross_promotion_info["eventidx"];
        $game_type = $cross_promotion_info["game_type"];
        $title = $cross_promotion_info["title"];
        $enable = $cross_promotion_info["enable"];
        $reward_amount = $cross_promotion_info["reward_amount"]; 
        $logo_path = $cross_promotion_info["logo_path"]; 
        $title_path = $cross_promotion_info["title_path"]; 
        $popup_path = $cross_promotion_info["popup_path"]; 
    }  
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript">

    function save_cross_promotion()
    {
    	debugger;
        var input_form = document.input_form;
 		   
        var param = {};
        param.eventidx = "<?= $eventidx ?>";
        param.game_type = input_form.game_type.value;
        param.title = input_form.title.value;
        param.enable =  $("input[name='enable']:checked"). val();
        param.reward_amount = input_form.reward_amount.value;
        param.logo_path = input_form.logo_path.value;
        param.title_path = input_form.title_path.value;
        param.popup_path = input_form.popup_path.value;

        if ("<?= $eventidx ?>" == "")
        	WG_ajax_execute("game/insert_cross_promotion", param, cross_promotion_callback);
        else
        	WG_ajax_execute("game/update_cross_promotion", param, cross_promotion_callback);
    }

    function cross_promotion_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            window.location.href = "cross_promotion.php";
        }
    }
    
    function delete_cross_promotion()
    {
        var input_form = document.input_form;

        if (!confirm("Cross Promotion를 삭제 하시겠습니까?"))
            return;

        var param = {};

        param.eventidx = "<?= $eventidx ?>";

        WG_ajax_execute("game/delete_cross_promotion", param, delete_cross_promotion_callback);
    }

    function delete_cross_promotion_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.href = "cross_promotion.php";
        }
    }

    function pop_img_upload(uploadtype,webserver)
    {
        window.open("pop_img_upload.php?uploadtype="+uploadtype+"&webserver="+webserver, "","width=450,height=350");
    }
    
    function pop_img_upload_callback(domid,imgurl)
    {
    	$('#'+domid+'_img').remove();
    	$('#'+domid).after('<img id="'+domid+'_img"src="'+imgurl+'">');
    }
    
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Cross Promotion 관리</div>
<?
    if ($eventidx != "")
    { 
?>
            <div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_cross_promotion()"></div>
<?
    } 
?>
	</div>
	<!-- //title_warp -->
            
	<form name="input_form" id="input_form" onsubmit="return false">
		<div class="h2_title">[Cross Promotion 정보]</div>
		<table class="tbl_view_basic">
			<colgroup>
				<col width="150">
				<col width="">
			</colgroup>
			<tbody>
				<tr>
					<th>게임 타입</th>
					<td>
						<select id="game_type" name="game_type">
							<option value="">선택</option>
							<option value="1" <?= ($game_type=="1") ? "selected" : "" ?>>DUC</option>
							<option value="2" <?= ($game_type=="2") ? "selected" : "" ?>>DUB</option>
							<option value="3" <?= ($game_type=="3") ? "selected" : "" ?>>OES</option>
							<option value="4" <?= ($game_type=="4") ? "selected" : "" ?>>HVS</option>
						</select>
					</td>
				</tr>
				<tr>
					<th>상태</th>
					<td>
							<input type="radio" id="enable" name="enable" value="1" <?= ($enable=="1") ? "checked" : "" ?>>활성
							<input type="radio" id="enable" name="enable" value="0" <?= ($enable=="0" || $enable=="") ? "checked" : "" ?>>비활성
					</td>
				</tr>
				<tr>
					<th >이벤트 Title</th>
					<td>
						<input type="text" class="view_tbl_text" name="title" id="title" value="<?= $title ?>"  style="width:200px"/>
					</td>
				</tr>
				<tr>
					<th>지급 Coin</th>
					<td>
						<input type="text" class="view_tbl_text" name="reward_amount" id="reward_amount" value="<?= $reward_amount ?>"  style="width:200px"/> Coin
					</td>
				</tr>
				<tr id="row_event_img">
					<th colspan="3">Logo 이미지</th>
				<tr>
					<td style="width: 950px;" colspan="3">
						<input type="text" class="view_tbl_text" name="logo_path" id="logo_path" value="<?= encode_html_attribute($logo_path) ?>" /> 
<?
    if ($logo_path != "")
    {
?>
						<div><img src="<?= $logo_path ?>" class="mt5" id="imgurl_img"/></div>
<?
    }
?>
					</td>	                    	
				</tr>
				<tr id="row_event_img">					    
					<th colspan="3">Title 이미지</th>
				</tr>
				<tr>
					<td colspan="3">
						<input type="text" class="view_tbl_text" name="title_path" id="title_path" value="<?= encode_html_attribute($title_path) ?>"  /> 
<?
    if ($title_path != "")
    {
?>
						<div><img src="<?= $title_path ?>" class="mt5" id="imgurl_answer_img" /></div>
<?
    }
?>
					</td>	                    	
				</tr>
				<tr id="row_event_img">					    
					<th colspan="3">Popup 이미지</th>
				</tr>
				<tr>
					<td colspan="3">
						<input type="text" class="view_tbl_text" name="popup_path" id="popup_path" value="<?= encode_html_attribute($popup_path) ?>" />
<?
    if ($popup_path != "")
    {
?>
						<div><img src="<?= $popup_path ?>" class="mt5" id="answer_imgurl_1_img" /></div>
<?
    }
?>
					</td>	                    	
				</tr>					    
			</tbody>
		</table>
            
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="저장" onclick="save_cross_promotion()">
		<input type="button" class="btn_setting_02" value="취소" onclick="go_page('cross_promotion.php')">            
	</div>
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main->end();
	$db_main2->end();
	$db_game->end();

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>