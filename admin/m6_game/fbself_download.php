<?
    $top_menu = "game";
    $sub_menu = "fbself_download";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
?>
<script type="text/javascript">

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function isInt(value) {
	  return !isNaN(value) && 
	         parseInt(Number(value)) == value && 
	         !isNaN(parseInt(value, 10));
	}

function downloadUserIndex() {
	var startidx = document.getElementById("startidx").value.replace(/,/g, "");
	var endidx = document.getElementById("endidx").value.replace(/,/g, "");
	
	if( !isInt(startidx) || !isInt(endidx) ){
		alert("숫자를 입력해 주세요.");
		return;
	}
	if(parseInt(endidx) - parseInt(startidx) > 1000000) {
		alert("범위는 백만이하로 입력해 주세요");
		return;
	}
	if(parseInt(startidx) <= 20000) {
		alert("useridx 20001이상으로 입력해 주세요");
		return;
	}
	
	window.location.href = 'fbself_download_user.php?startidx=' + startidx + '&endidx=' + endidx;
}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 마케팅용 자료 추출</div>
	</div>
	<!-- //title_warp -->
	
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="">
	    	<col width="100">
		</colgroup>
		<thead>
	    	<tr>
	    		<th class="tdl" style="padding-left:20px">종류</th>
	    		<th>신규 회차</th>
	    	</tr>
		</thead>
		<tbody>
	    	<tr>
	    		<td class="point_title" style="padding-left:20px">
	    			Useridx 추출: &nbsp;&nbsp;<input type="text" onchange="value=numberWithCommas(value);" class="view_tbl_text" name="startidx" id="startidx" style="width:100px;">
	    			~ <input type="text" onchange="value=numberWithCommas(value);" class="view_tbl_text" name="endidx" id="endidx" style="width:100px;"> 
	    			(useridx는 20,001이상, 범위는 백만 이하로 해주세요.)
	    		</td>
	    		<td class="tdc"><input type="button" class="btn_03" value="다운로드" onclick="downloadUserIndex();"></td>
	    	</tr>
		</tbody>
	</table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>