<?
    $top_menu = "game";
    $sub_menu = "notice_popup";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $db_main2 = new CDatabase_Main2();
    $db_analysis = new CDatabase_Analysis();

    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_title = $_GET["title"];
    $category = ($_GET["category"] == "")? "4":$_GET["category"];
    
    $listcount = 10;
    $pagename = "notice_popup.php";
    $pagefield = "message=$search_title";
    
    $tail = "WHERE 1=1 ";
    
    if ($search_title != "")
    	$tail .= " AND message LIKE '%".encode_db_field($search_title)."%' ";
    
    if($category != 4)
        $tail .= " AND platform = $category";
    
    $sql = "SELECT * FROM `tbl_notice_popup_info` $tail ORDER BY mesgidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    $eventlist = $db_main2->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) FROM tbl_notice_popup_info $tail";
    $totalcount = $db_main2->getvalue($sql);
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
    	$page = floor(($totalcount + $listcount - 1) / $listcount);
    
    $db_main2->end();
    $db_analysis->end();
?>
<script type="text/javascript">
function search_press(e)
{
    if (((e.which) ? e.which : e.keyCode) == 13)
    {
        search();
    }
}

function search()
{
    var search_form = document.search_form;
    search_form.submit();
}

function go_result(eventidx)
{
    window.location.href = "event_result.php?eventidx="+eventidx;
} 

function tab_change(fanpage)
{
    var search_form = document.search_form;
    search_form.fanpage.value = fanpage;
    search_form.submit();
}    
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
    
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 공지사항 관리 (<?= make_price_format($totalcount) ?>)</div>
	</div>
	<!-- //title_warp -->
        
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
		<div class="detail_search_wrap">
			<select id="category" name="category">
				<option value="4">전체</option>
				<option value="0" <?= ($category=="0") ? "selected" : "" ?>>Web</option>
				<option value="1" <?= ($category=="1") ? "selected" : "" ?>>IOS</option>
				<option value="2" <?= ($category=="2") ? "selected" : "" ?>>Android</option>
				<option value="3" <?= ($category=="3") ? "selected" : "" ?>>Amazon</option>
			</select>&nbsp;
			
			<span class="search_lbl">message</span>
			<input type="text" class="search_text" id="title" name="title" style="width:250px" value="<?= encode_html_attribute($search_title) ?>" onkeypress="search_press(event)" />&nbsp;
                                    
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>
        
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="40">
			<col width="">
 			<col width="80">
 			<col width="80">
			<col width="80">
		</colgroup>
		<thead>
			<tr>
				<th>번호</th>
				<th class="tbl">message</th>
				<th class="tbc">platform</th>
				<th class="tbc">상태</th>
				<th class="tbc">기간</th>
			</tr>
		</thead>
		<tbody>
<?
for ($i=0; $i<sizeof($eventlist); $i++)
{
    $mesgidx = $eventlist[$i]["mesgidx"];
    $platform = $eventlist[$i]["platform"];
    $enable = $eventlist[$i]["enable"];
    $message = $eventlist[$i]["message"];
    $startdate = $eventlist[$i]["startdate"];
    $enddate = $eventlist[$i]["enddate"];
    
    $platform_str="";
    $enable_str="";
    
    if($platform == 0)
        $platform_str = 'WEB';
    else if($platform == 1)
        $platform_str = 'IOS';
    else if($platform == 2)
        $platform_str = 'Android';
    else if($platform == 3)
        $platform_str = 'Amazon';
    else if($platform == 4)
        $platform_str = '공통';
    
    if($enable == 0)
        $enable_str = "비 활성";
    else 
        $enable_str = "활성";
?>
        <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="window.location.href='notice_popup_write.php?mesgidx=<?= $mesgidx ?>'">
            <td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
            <td class="tbl point_title"><?= mb_strimwidth($message , '0', '170', '...', 'utf-8');?></td>
            <td class="tdc point"><?= $platform_str ?></td>
            <td class="tdc"><?= $enable_str ?></td> 
            <td class="tdc"><?= $startdate ?> ~ <?= $enddate ?></td> 
        </tr>
<?
} 
?>
	</tbody>
</table>
<?
include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>

<div class="button_warp tdr">
	<input type="button" class="btn_setting_01" value="추가" onclick="window.location.href='notice_popup_write.php'">           
</div>            
</div>
<!--  //CONTENTS WRAP -->
    
<div class="clear"></div>
<?
include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
