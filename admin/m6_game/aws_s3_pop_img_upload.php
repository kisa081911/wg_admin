<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<?
	$path = $_GET["path"];
?>
<head>
	<title>DOUBLEU GAMES 관리자</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="robots" content="all" />
	<link rel="stylesheet" type="text/css" media="all" href="../css/style.css" />
	<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
	<style>
	.progress 
	{
	  display:none; 
	  position:relative; 
      width: 220px;
      border: 1px solid #ddd;
      padding: 1px;
      border-radius: 3px;
      height: 35px;
      background: #fff;
	}
	.percent 
	{ 
	     position: absolute;
	    display: inline-block;
	    top: 10px;
	    left: 26%;
	    font-size: 11pt;
	    font-style: inherit;
	    font-weight: bolder;
	}
</style>	
	<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/js/ajax_helper.js"></script>
	<script type="text/javascript" src="/js/common_util.js"></script>
	<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="/js/jquery.bpopup.min.js"></script>
	<script src="http://malsup.github.com/jquery.form.js"></script> 
	<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="/js/jquery.easing.compatibility.js"></script>
	<script type="text/javascript">

	var uploadtype = '<?=$uploadtype?>';
	var webserver = '<?=$webserver?>';
	
	function img_upload()
	{
	    var input_form = document.input_form;
	    var fileInput = document.getElementById("userfile[]");
        var files = fileInput.files;
        var file;
        
	    var a = /[ #\&\+\-%@=\/\\\:;,'\"\^`~|\!\?\*$#<>()\[\]\{\}]/i;
	    if (files.length < 1)
	    {
	        alert("파일을 선택하세요.");
	        fileInput.focus();
	        $("#input_form").submit(function(){
  	    	  return false;
  	    	});
	        return false;
	    }
        for (var i = 0; i < files.length; i++) {
            file = files[i];
		    if( file.name.search(/\s/g) > -1 )
			{
		    	    alert( '파일명 (" '+file.name+' ") 에 공백이 포함되어 있습니다.' );
		    	    fileInput.focus();
		    	    $("#input_form").submit(function(){
		    	    	  return false;
		    	    	});
		    	    return false;
		    }
		    else if( a.test(file.name) == true)
			{
		    	    alert( '파일명(" '+file.name+' ")에 사용할 수 없는 특수 문자가 포함되어 있습니다.' );
		    	    fileInput.focus();
		    	    $("#input_form").submit(function(){
		    	    	  return false;
		    	    	});
		    	    return false;
		    }
        }
        return true;
	}
	
	$(document).ready( function() {
		 
        $("input[type=file]").change(function () {
             
            var fileInput = document.getElementById("userfile[]");
            var files = fileInput.files;
            var file;
             
            $('#filelist tbody').html('');
            for (var i = 0; i < files.length; i++) {
                file = files[i];

                var html = "<tr id='uploadfilenum_"+i+"'>";
                	html +="<td class='tbc point h3_title' style='font: bold 13px 'Malgun Gothic';'>"+file.name+"</td>";
                	html +="<td class='tdc'>"+Number(file.size).toLocaleString('en')+"</td>";
                	html +="</tr>";
                $('#filelist tbody').append(html);
            }
             
        });
 
    });

	function upload_image() 
	{

		  if(img_upload())
		  {	
		 	  var bar = $('#bar1');
			  var percent = $('#percent1');
			  $('#progress_div').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
				
			  $('#input_form').ajaxForm({
			    beforeSubmit: function() {
			      var percentVal = '0%';
			      bar.width(percentVal);
			      percent.html(percentVal);
			    },
		
			    uploadProgress: function(event, position, total, percentComplete) {
			      percent.html('업로드 중입니다...');
			    },
			    complete: function(xhr) {
				    alert("업로드 완료!");
			    	opener.location.reload();
			    	location.reload();
			    }
			  }); 
		 }
	}
	
	</script>
</head>
<body> 
	<form enctype="multipart/form-data" action="aws_s3_fileupload_receiver.php" method="post" id="input_form" name="input_form">
           <p><input name="userfile[]" id="userfile[]" type="file" class="btn_03" style=" margin-left: 6%;margin-top: 1%;" multiple/></p>
           <div id="filelist" style="width: 90%;margin-left: 6%;">
           		<table class="tbl_list_basic1">
				<colgroup>
					<col width="120">
					<col width="70">
				</colgroup>
				<thead>
					<tr>
						<th class="tbl">파일 이름</th>
						<th class="tbc">사이즈</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
	   		</table>
	           <input name="path" id="path" type="hidden" value="<?=$path?>"/>
	           <p><input type="submit" class="btn_02"  value="업로드" onclick="upload_image()"><input type="button" class="btn_02" style="margin-bottom:5px;margin-left:3px;" value="닫기" onclick="self.close();"></p>
           </div>
    </form>
    <div class='progress' id="progress_div">
    	<div class='uploading' id='uploading'>
    		<img src="../images/loading.gif" style="margin-top: 2px;margin-left: 6px;"></br>
		<div class='percent' id='percent1'>0%</div>
    	</div>
	</div>
</body>
</html>