<?
    $top_menu = "game";
    $sub_menu = "fanpage";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    
    $listcount = 10;
    $pagename = "fanpage.php";
    $search_title = $_GET["title"];
    
    $pagefield = "title=$search_title";
    
    $tail = "WHERE message<>'' ";
    
    if ($search_title != "")
    	$tail .= " AND message LIKE '%".encode_db_field($search_title)."%' ";
    
    $db_analysis = new CDatabase_Analysis();
    
    $sql = "SELECT articleid,message,shares,likes,comments,writedate ".
    		"FROM fanpage_article $tail ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    $fanpagelist = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) FROM fanpage_article $tail";
    $totalcount = $db_analysis->getvalue($sql);
    
    $db_analysis->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
    	$page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script type="text/javascript">  
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }    
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 팬페이지 게시물 관리 (<?= number_format($totalcount) ?>)</div>
	</div>
	<!-- //title_warp -->
	
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
		<div class="detail_search_wrap">
			<span class="search_lbl">게시 내용</span>
			<input type="text" class="search_text" id="title" name="title" style="width:250px" value="<?= encode_html_attribute($search_title) ?>" onkeypress="search_press(event)" />
	                    
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>
	
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="40">
			<col width="">
			<col width="80">
			<col width="100">
			<col width="120">
		</colgroup>
		<thead>
			<tr>
				<th>번호</th>
				<th class="tbl">내용</th>
				<th class="tdc">Like</th>
				<th class="tdc">Comment</th>
				<th>등록일</th>
			</tr>
		</thead>
		<tbody>
<?
    for ($i=0; $i<sizeof($fanpagelist); $i++)
    {
        $articleid = $fanpagelist[$i]["articleid"];
        $message = $fanpagelist[$i]["message"];
        $likes = $fanpagelist[$i]["likes"];
        $comments = $fanpagelist[$i]["comments"];
        $writedate = $fanpagelist[$i]["writedate"];
?>
			<tr>
				<td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
				<td class="tbl point_title"><?= get_left_title($message, 1000) ?></td>
				<td class="tdc point"><?= make_price_format($likes) ?></td>
				<td class="tdc point"><?= make_price_format($comments) ?> <input type="button" class="btn_03" value="보기" style="cursor:pointer" onclick="event.cancelBubble=true;window.location.href='fanpage_comment.php?articleid=<?= $articleid ?>'" /></td>
				<td class="tdc"><?= $writedate ?></td> 
			</tr>
<?
    } 
?>
		</tbody>
	</table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>