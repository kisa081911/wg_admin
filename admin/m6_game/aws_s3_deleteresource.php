<?
	require '../common/aws_sdk/vendor/autoload.php';
    use Aws\Common\Aws;
    ini_set("memory_limit", "-1");
    include_once("../common/common_util.inc.php");
    
   	$filedeletekeylist =  $_POST['filedeletekeylist'];
   	$folderdeletekeylist = $_POST['folderdeletekeylist'];
   	
    try
    {
    	$aws = Aws::factory('../common/aws_sdk/aws_config.php');
    	$s3Client = $aws->get('s3');
    	$bucket = "dug-event-img";
    	
    	//파일삭제
     	for ($i=0;$i<sizeof($filedeletekeylist);$i++)
     	{
	    	delete_file_resource($s3Client,$bucket,$filedeletekeylist[$i]);
     	}
     	
     	for ($i=0;$i<sizeof($folderdeletekeylist);$i++)
     	{
     		delete_folder_resource($s3Client,$bucket,$folderdeletekeylist[$i]);
     	}
    	echo "삭제 완료";
    	
    }
    catch(\Aws\S3\Exception\S3Exception $e)
    {
    	write_log($e->getMessage());
    }
    
    function delete_file_resource($s3Client,$bucket,$resourcekey)
    {
    		$result = $s3Client->deleteObject(array(
    				'Bucket' => $bucket,
    				'Key'    => $resourcekey
    		));
    }
    
    function delete_folder_resource($s3Client,$bucket,$folderdeletekey)
    {
    		$result = $s3Client->listObjects(array('Bucket' => $bucket,'Prefix' => $folderdeletekey));
    		$files = $result->getPath('Contents');
    		
    		if(sizeof($files) > 1)
    		{
    			foreach ($files as $file)
				{
					$filename = $file['Key'];
					$size = $file['Size'];
						delete_file_resource($s3Client,$bucket,$filename);
				}
    		}
    		else
    		{
    			delete_file_resource($s3Client,$bucket,$folderdeletekey);
    		}
    		 
    }
?>
