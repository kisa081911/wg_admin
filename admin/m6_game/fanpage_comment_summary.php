<?
    $top_menu = "game";
    $sub_menu = "fanpage";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $articleid = $_GET["articleid"];
    
    if ($articleid == '')
    	error_back("잘못된 접근입니다.");
    
    $db_main = new CDatabase_Main();
    $db_analysis = new CDatabase_Analysis();
    
    $sql = "SELECT gifttype, giftamount FROM fanpage_article_comment WHERE articleid='$articleid' AND giftamount>0 GROUP BY gifttype, giftamount ORDER BY gifttype ASC, giftamount DESC";
    $amountlist = $db_analysis->gettotallist($sql);
?>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 팬페이지 게시물 관리 - 댓글 시상 내역</div>
	</div>
	<!-- //title_warp -->
	
	<table class="tbl_list_basic1">
            <colgroup>
                <col width="150">
                <col width="150">
                <col width="">
            </colgroup>
            <thead>
                <tr>
                	<th class="tbc">시상 내용</th>
                    <th class="tbc">시상 금액</th>
                    <th class="tbl">사용자</th>
                </tr>
            </thead>
            <tbody>
<?
    for ($i=0; $i<sizeof($amountlist); $i++)
    {
    	$gifttype = $amountlist[$i]["gifttype"];
    	$giftamount = $amountlist[$i]["giftamount"];
		
		$sql = "SELECT useridx FROM fanpage_article_comment WHERE articleid='$articleid' and gifttype=$gifttype AND giftamount=$giftamount";
		$userlist = $db_analysis->gettotallist($sql);
		
		$totaluser = "";
		
		for ($j=0; $j<sizeof($userlist); $j++)
		{
			$useridx = $userlist[$j]["useridx"];
			$username = $db_main->getvalue("SELECT nickname FROM tbl_user WHERE useridx=$useridx");
			$finalname = "";
			
			$name_info = explode(' ', $username);

			if(sizeof($name_info) == 1)
			{
				if ($totaluser == "")
					$totaluser = $username;
				else
					$totaluser .= " / ".$username;
			}
			else
			{
				for($k=0; $k<sizeof($name_info); $k++)
				{
					if($k+1 == sizeof($name_info))
					{
						$initial = substr($name_info[$k], 0, 1);
						$initial = strtoupper($initial);
						$finalname .= " ".$initial;
					}
					else
					{
						if($finalname == "")
						{
							$finalname = $name_info[$k];
						}
						else
						{
							$finalname .= " ".$name_info[$k];
						}
					}
				}
				
				if ($totaluser == "")
					$totaluser = $finalname;
				else
					$totaluser .= " / ".$finalname;
			}
		}
		
		$gift_name = "";
		
		if($gifttype == 1)
			$gift_name = "Coin"
?>
            <tr>
            	<td class="tdc"><?= $gift_name ?></td>
                <td class="tdc"><?= make_price_format($giftamount) ?></td>
                <td class="tbl point_title"><?= $totaluser ?></td>
            </tr>
<?
    } 
?>
		</tbody>
	</table> 
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main->end();
	$db_analysis->end();
	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>