<?
    $top_menu = "game";
    $sub_menu = "and_push";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $os_type = ($_GET["os_type"] == "") ? "" : $_GET["os_type"];
    $search_message = ($_GET["search_message"] == "") ? "" : $_GET["search_message"];
    $search_sent = ($_GET["search_sent"] == "") ? "" : $_GET["search_sent"];

    $listcount = 10;
    $pagename = "and_push.php";

    $tail = "AND 1 = 1";
    
    if($os_type != "")
    	$tail .= " AND os_type = $os_type ";
    
    if($search_message != "")
    	$tail .= " AND message LIKE '%$search_message%' ";    
    
    if($search_sent != "")
    	$tail .= " AND sent = '$search_sent' ";
    
    $pagefield = "search_message=$search_message&search_sent=$search_sent";    
    
    $db_mobile = new CDatabase_Mobile();
    
    $sql = "SELECT * FROM `tbl_message` ".
      		"WHERE 1=1 $tail ".
      		"ORDER BY mesgidx ".
    		"DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
	
    $messagelist = $db_mobile->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) FROM `tbl_message`".
      		"WHERE 1=1 $tail";
    $totalcount = $db_mobile->getvalue($sql);    
    
    $db_mobile->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script type="text/javascript">  
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    } 
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; PUSH 관리 (<?= make_price_format($totalcount) ?>)</div>
            </div>
            <!-- //title_warp -->
            
            <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
                <div class="detail_search_wrap">
                    <span class="search_lbl ml20">Message</span>
                    <input type="text" class="search_text" id="search_message" name="search_message" style="width:250px" value="<?= encode_html_attribute($search_message) ?>" onkeypress="search_press(event)" />
                    
                    <span class="search_lbl ml20">PUSH 여부</span>
                    <select name="search_sent" id="search_sent">
                    	<option value="" <?= ($search_sent=="") ? "selected" : "" ?>>전체</option>
						<option value="0" <?= ($search_sent=="0") ? "selected" : "" ?>>미전송</option>
						<option value="1" <?= ($search_sent=="1") ? "selected" : "" ?>>전송</option>
					</select>	
                  	<span class="search_lbl ml20">플랫폼 선택</span>
                    <select id="os_type" name="os_type">
                      <option value="">선택</option>
                      <option value="0" <?= ($os_type == 0) ? "selected" : "" ?>>공통</option>
                      <option value="1" <?= ($os_type == 1) ? "selected" : "" ?>>IOS</option>
                      <option value="2" <?= ($os_type == 2) ? "selected" : "" ?>>Android</option>
                      <option value="3" <?= ($os_type == 3) ? "selected" : "" ?>>Amazon</option>
                     </select>					
					<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
                </div>
            </form>
                        
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="5%">
                <col width="10%">
                <col width="30%">
                <col width="10%">
                <col width="10%">
                <col width="15%">
            </colgroup>
            <thead>
                <tr>
                    <th>번호</th>
                    <th>Platform</th>
                    <th class="tbl">Message</th>
					<th>전송 상태</th>  
                    <th>예약 게시 여부</th>
                    <th>예약게시일</th>
                </tr>
            </thead>
            <tbody>
<?
    for ($i=0; $i<sizeof($messagelist); $i++)
    {
        $mesgidx = $messagelist[$i]["mesgidx"];
        $platform = $messagelist[$i]["os_type"];
        $message = $messagelist[$i]["message"];
        $sent = $messagelist[$i]["sent"];
        $ios_sent = $messagelist[$i]["ios_sent"];
        $ama_sent = $messagelist[$i]["ama_sent"];
        
        $enabled = $messagelist[$i]["enabled"];
        $reservedate = $messagelist[$i]["reservedate"];    

        if($platform == 0)
        	$platform_str = "ALL";
        else if($platform == 1)
        {
        	$sent = $ios_sent;
        	$platform_str = "IOS";
        }
        else if($platform == 2)
        	$platform_str = "Android";
        else if($platform == 3)
        {
        	$sent=$ama_sent;
        	$platform_str = "Amazon";
    	}
?>
            <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="view_and_push_dtl(<?= $mesgidx ?>)">
                <td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
                <td class="tdc"><?= $platform_str ?></td>
                <td class="tbl point_title"><?= $message ?></td>
                <td class="tdc"><?= ($sent == "1") ? "전송" : "미전송" ?></td>
                <td class="tdc"><?= ($enabled == "1") ? "예약 게시" : "-" ?></td>
                <td class="tdc"><?= $reservedate ?></td>                
            </tr>
<?
    } 
?>
            </tbody>
            </table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>

            <div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="PUSH 추가" onclick="window.location.href='and_push_write.php'">           
            </div>            
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>