<? 
    include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
    
    check_login_layer();
    
    $useridx = $_GET["useridx"];
    $fb_id = $_GET["fb_id"];
    $send_type = "";
    $nickname = ($_GET["nickname"] == "") ? "#nickname#" : $_GET["nickname"];
    $welcome_coin = ($_GET["welcome_coin"] == "") ? "0" : $_GET["welcome_coin"];
	$sql_money = ($_GET["money"] == "") ? "" : $_GET["money"];
	$sql_leavedays = ($_GET["leavedays"] == "") ? "" : $_GET["leavedays"];
	$sql_contactdays = ($_GET["contactdays"] == "") ? "" : $_GET["contactdays"];
	$bundle_flag = 0;
    
    if($sql_money == "")
	{
		$bundle_flag = 0;
		
		if ($fb_id == "")
			error_close_layer("잘못된 접근입니다."); 
	}
	else
	{
		$bundle_flag = 1;
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FB A2U 발송</title>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	var tmp_hosturl = "<?= HOST_URL ?>";

	$(function() {
        $("#reservation_date").datepicker({ });
        $("#startdate_date").datepicker({ });
        $("#enddate_date").datepicker({ });
    });
	
    function layer_close()
    {
        window.parent.close_layer_popup("<?= $_GET["layer_no"] ?>");
    }
    
    function fn_send_a2u()
    {
        var input_form = document.input_form;
        
        var radioLength = input_form.send_type.length;
        var radio_value = "";

        for(var i = 0; i < radioLength; i++) 
		{
			if(input_form.send_type[i].checked) 
			{
				radio_value =  input_form.send_type[i].value;
    		}
    	}
		
		if(radio_value == 1)
		{
			if (input_form.fb_a2u_msg.value == "")
			{
				alert("A2U 메세지를 입력해주세요.");
				input_form.fb_a2u_msg.focus();
				return;
			}
		}
		else if(radio_value == 0)
		{
			if (input_form.fb_a2u_txt.value == "")
			{
				alert("A2U 메세지를 입력해주세요.");
				input_form.fb_a2u_txt.focus();
				return;
			}
		}  

		var param = {};
        param.useridx = '<?= $useridx ?>';
        param.fb_id  = '<?= $fb_id ?>';
        param.fb_a2u_msg=input_form.fb_a2u_msg.value;
		param.fb_a2u_txt=input_form.fb_a2u_txt.value;
		param.bundle_flag='<?= $bundle_flag?>';
		param.sql_money='<?= $sql_money?>';
		param.sql_leavedays='<?= $sql_leavedays?>';
		param.sql_contactdays='<?= $sql_contactdays?>';
        param.send_type=radio_value;
		param.reservation_date=input_form.reservation_date.value;
        param.reservation_hour=input_form.reservation_hour.value;
        param.reservation_minute=input_form.reservation_minute.value;
        
        WG_ajax_execute("user/send_a2u", param, send_a2u_callback);
    }
    
    function send_a2u_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            alert("A2U 노티를 발송하였습니다.");
            window.parent.close_layer_popup("<?= $_GET["layer_no"] ?>");
            window.parent.location.reload(true);
        }
    }
	
	function fn_insert_name()
    {
		var input_form = document.input_form;
        
		input_form.fb_a2u_msg.value += "<?= $nickname?> ";
		input_form.fb_a2u_txt.value += "<?= $nickname?> ";
    }

    function fn_insert_emoji()
    {
    	showCenterPopup("emoji.html", "Emoji", 700, 500);
    } 

    function fn_txt_insert_emoji(emoji, code)
    {
    	var input_form = document.input_form;

    	input_form.fb_a2u_msg.value += emoji;
		input_form.fb_a2u_txt.value += code;
		
    }

    function fn_insert_coin()
    {
		var input_form = document.input_form;
<?
	if($bundle_flag == "0")
	{
?> 
		input_form.fb_a2u_msg.value += "$ <?= number_format($welcome_coin) ?>";
		input_form.fb_a2u_txt.value += "$ <?= number_format($welcome_coin) ?>";
<?
	}
	else
	{
?>
		input_form.fb_a2u_msg.value += "#welcome_coin#";
		input_form.fb_a2u_txt.value += "#welcome_coin#";
<?
	}
?>
    }

    function showCenterPopup(url, title, width, height) 
    {
    	var popupX = (window.screen.width / 2) - (width / 2);
    	var popupY = (window.screen.height / 2) - (height / 2);

    	var wnd = window.open(url, title, "status=no," + "width=" + width + "," + "height=" + height+ "," + "left=" + popupX + "," + "top=" + popupY + "," + "screenX=" + popupX + "," + "screenY=" + popupY + "," + "scrollbars=yes");

    	return wnd;
	}
	
	function fn_show_a2u()
	{
		var total_msg = "";
		var utf8_emoji = "";
		var double_emoji = "";
		var input_form = document.input_form;
		var msg = input_form.fb_a2u_txt.value;
		var agent = navigator.userAgent.toLowerCase();
		
		document.getElementById("fb_a2u_show").style.display = "";
		var msg_temp = msg.split("]");
		
		if(1 < msg_temp.length)
		{
			for(var i = 0;i < msg_temp.length;i++)
			{
				var msg_emoji = msg_temp[i].split("[");
				
				if(1 < msg_emoji.length)
				{
					emoji_code = msg_emoji[1].replace('U+','0x');
					
					var double_code = emoji_code.split(" ");
					
					if(1 < double_code.length)
					{
						for(var j = 0;j < double_code.length;j++)
						{
							double_code_temp = double_code[j].replace('U+','0x');
							
							if (agent.indexOf("chrome") != -1) 
							{
								utf8_emoji_temp = String.fromCodePoint(double_code_temp);
							}
							else
							{
								H = Math.floor((double_code_temp - 0x10000) / 0x400) + 0xD800;
								L = (double_code_temp - 0x10000) % 0x400 + 0xDC00;
								
								utf8_emoji_temp = String.fromCharCode(H, L);
							}
							
							double_emoji += utf8_emoji_temp;
							//alert(double_emoji);
						}
						
						urf8_emoji = double_emoji;
						
					}
					else 
					{
						if (agent.indexOf("chrome") != -1) 
						{
							utf8_emoji = String.fromCodePoint(emoji_code);
						}
						else
						{
							H = Math.floor((emoji_code - 0x10000) / 0x400) + 0xD800;
							L = (emoji_code - 0x10000) % 0x400 + 0xDC00;
							
							utf8_emoji = String.fromCharCode(H, L);
							
						}
					}						
					
					total_msg += msg_emoji[0];
					total_msg += utf8_emoji;
				}
				else
				{
					total_msg = total_msg + msg_temp[i];
				}
				
			}
			document.getElementById("fb_a2u_show").value = total_msg;
		}
		else
		{
			document.getElementById("fb_a2u_show").value = msg;
		}
		
	}
	
	function show_msg_input(type)
	{
		var input_form = document.input_form;
		
		var type_0 = document.getElementById("reserve_send");
		var type_1 = document.getElementById("immediately_send");
		var show_a2u = document.getElementById("show_a2u");
		var show_a2u_button = document.getElementById("show_a2u_button");
		var reserve_date = document.getElementById("reserve_send_date");
		
		if(type == 0)
		{
			input_form.fb_a2u_msg.value = "";
			input_form.fb_a2u_txt.value = "";
			type_0.style.display = "";
			type_1.style.display = "none";
			show_a2u.style.display = "";
			show_a2u_button.value = "예약발송";
			reserve_date.style.display = "";
		}
		else if(type == 1)
		{
			input_form.fb_a2u_msg.value = "";
			input_form.fb_a2u_txt.value = "";
			type_0.style.display = "none";
			type_1.style.display = "";
			show_a2u.style.display = "none";
			show_a2u_button.value = "즉시발송";
			reserve_date.style.display = "none";
		}
		
<?
	if($bundle_flag == "1")
	{
?>
		show_a2u_button.value = "일괄 예약발송";
<?
	}
?>
	}

</script>
</head>
<body class="layer_body" onload="window.focus()" onkeyup="if (getEventKeycode(event) == 27) { layer_close(); }">
<div id="layer_wrap">   
    <div class="layer_header" >
        <div class="layer_title">FB A2U 발송 </div>
        <img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="layer_close()" />
    </div>        
    <div class="layer_contents_wrap" style="width:740px">
         <!--  레이어 내용  -->
         
         <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
			<input type="hidden" name="emoji_code" id="emoji_code">
            <table class="tbl_view_basic" style="width:730px">
                <colgroup>
                    <col width="120">
                    <col width="">
                </colgroup>
                    <tbody>
						<tr>
							<th>발송 타입</th>
							<td>
								<input type="radio" value="0" name="send_type" id="send_type_0" <?= ($send_type == "0") ? "checked=\"true\"" : ""?> onclick="show_msg_input(0);"/> 예약 발송
								<?
									if($bundle_flag == "0")
									{
								?>
                				<input type="radio" value="1" name="send_type" id="send_type_1" <?= ($send_type == "1") ? "checked=\"true\"" : ""?>/ onclick="show_msg_input(1);"> 즉시 발송
								<?
									}
								?>
                				<!-- <input type="radio" value="2" name="send_type" id="send_type_2" <?= ($send_type == "2") ? "checked=\"true\"" : ""?>/> 나에게 발송  -->
                				<br/><br/>
                				*예약 발송은 유저 로그인 시간대로 발송<br/>
								*일괄 발송의 경우 마지막 컨택일이 7일 이상인 유저들만 발송</br>
								<font color="red">* 미리보기 기능은 크롬브라우져를 권장</font>
							</td>
                         </tr>
                         <tr style="display:none;" id="immediately_send">
                            <th>A2U 메세지(즉시발송)</th>
							<td>
								<input type="text" class="view_tbl_text" style="width:550px" name="fb_a2u_msg" id="fb_a2u_msg" maxlength="200" value="" />
								<br/>
								<div class="layer_button_wrap" style="width:562px;text-align:right;">
									<input type="button" class="btn_02" value="유저 이름 추가" onclick="fn_insert_name();" />
									<input type="button" class="btn_02" value="웰컴백 보상 금액 추가" onclick="fn_insert_coin();" />
									<input type="button" class="btn_02" value="이모티콘 삽입" onclick="fn_insert_emoji();" />
								</div>
                            </td>
                         </tr>
						 <tr style="display:none;" id="reserve_send_date">
                            <th>예약발송 시간</th>
							<td>
								<input type="input" class="search_text" id="reservation_date" name="reservation_date" style="width:65px" value="" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
								<select id="reservation_hour" name="reservation_hour">
									<option value="">선택</option>
	<?
		for ($i=0;$i<24;$i++)
		{
	?>
									<option value="<?= $i ?>" <?= ($i == $reservation_hour) ? "selected" : "" ?>><?= $i ?></option>
	<?
		}
	?>
								</select>시&nbsp;
								<select id="reservation_minute" name="reservation_minute">
									<option value="">선택</option>
	<?
		for ($i=0;$i<60;$i++)
		{
	?>
									<option value="<?= $i ?>" <?= ($i == $reservation_minute) ? "selected" : "" ?>><?= $i ?></option>
	<?
		}
	?>
								</select>분
							</td>
                         </tr>
                         <tr style="display:none;" id="reserve_send">
                            <th>A2U 메세지(예약발송)</th>
							<td>
								<input type="text" class="view_tbl_text" style="width:550px" name="fb_a2u_txt" id="fb_a2u_txt" maxlength="200" value="" />
								<br/>
								<input type="text" class="view_tbl_text" style="width:550px;display:none;" name="fb_a2u_show" id="fb_a2u_show" maxlength="200" value="" />
								<div class="layer_button_wrap" style="width:562px;text-align:right;">
									<input type="button" class="btn_02" value="유저 이름 추가" onclick="fn_insert_name();" />
									<input type="button" class="btn_02" value="웰컴백 보상 금액 추가" onclick="fn_insert_coin();" />
									<input type="button" class="btn_02" value="이모티콘 삽입" onclick="fn_insert_emoji();" />
								</div>
                            </td>
                         </tr>
                     </tbody>
             </table>  
             </form>  
         </div>
        <!-- 확인 버튼 -->
        <div class="button_warp tdr"> 
			<input type="button" class="btn_setting_01" style="display:none;" value="미리보기" id="show_a2u" onclick="fn_show_a2u();">
			<input type="button" class="btn_setting_01" value="발송" id="show_a2u_button" onclick="fn_send_a2u();">
		</div>
         <!-- 확인 버튼 -->
         <!--  //레이어 내용  -->
    </div>
</div>
</body>
</html>

