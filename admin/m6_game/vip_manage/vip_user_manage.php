<?
    $top_menu = "game";
    $sub_menu = "vip_user_manage";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_start_value = ($_GET["start_value"] == "") ? "499" : $_GET["start_value"];
    $search_end_value = ($_GET["end_value"] == "") ? "" : $_GET["end_value"];
    $search_leave_std = ($_GET["search_leave_std"] == "") ? "more" : $_GET["search_leave_std"];
    $search_leavedays1 = ($_GET["search_leavedays1"] == "") ? "1" : $_GET["search_leavedays1"];
    $search_leavedays2 = ($_GET["search_leavedays2"] == "") ? "100" : $_GET["search_leavedays2"];
    $search_contact_std = ($_GET["search_contact_std"] == "") ? "more" : $_GET["search_contact_std"];
    $search_contactdays = ($_GET["search_contactdays"] == "") ? "" : $_GET["search_contactdays"];
    $search_order_by = ($_GET["orderby"] == "") ? "total_money DESC" : $_GET["orderby"];
    $search_send_type = $_GET["send_type"];
    
    $listcount = 20;
    $pagename = "vip_user_manage.php";
    $pagefield = "start_value=$search_start_value&end_value=$search_end_value&search_leavedays1=$search_leavedays1&search_leavedays2=$search_leavedays2&search_leave_std=$search_leave_std&search_contactdays=$search_contactdays&search_contact_std=$search_contact_std&orderby=$search_order_by&send_type=$search_send_type";
    
    $db_main= new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    
    ini_set("memory_limit", "-1");
    
    $sql_money = "";
    
    if($search_start_value != "")
    {
        $sql_money .= " AND $search_start_value <= total_money ";
    }

    if($search_end_value != "")
    {
        $sql_money .= " AND $search_end_value >= total_money ";
    }
    
    $sql_leavedays = "";
    
    $sql_leavedays .= " AND $search_leavedays1 <= leavedays AND $search_leavedays2 >= leavedays";
    
    $sql_contactdays = "";
    
    if($search_contact_std == "more")
    {
        if($search_contactdays != "")
        {
            $sql_contactdays .= " AND DATE_SUB(NOW(), INTERVAL $search_contactdays DAY) >= IF(contact_1 >= contact_2, contact_1, contact_2)";
        }
    }
    else
    {
        if($search_contactdays != "")
        {
            $sql_contactdays .= " AND DATE_SUB(NOW(), INTERVAL $search_contactdays DAY) < IF(contact_1 >= contact_2, contact_1, contact_2)";
        }
    }
    
    $sql_is_a2u = "";
    $sql_is_email = "";
    $sql_is_push = "";
    $sql_is_msg = "";
	$bundle_sql = "";
    
    if($search_send_type == "a2u")
    {
        $sql_is_a2u = " AND fb_id > 1000 AND is_app_delete = 0";
    }
    else if($search_send_type == "email")
    {
        $sql_is_email = " AND email != '' AND is_email_disable = 0 ";
    }
    else if($search_send_type == "push")
    {
        $sql_is_push = " AND is_push_disable = 0 ";
    }
    else if($search_send_type == "msg")
    {
        $sql_is_msg = " AND fb_id > 0 ";
    }
    
    $sql = "SELECT useridx, fb_id, nickname, total_money, coin, isblock, leavedays, logindate, createdate, welcome_coin, last_a2u_send_date, last_email_send_date, last_push_send_date, last_fb_msg_send_date, IF(contact_1 >= contact_2, contact_1, contact_2) AS max_contact ". 
            "FROM ( ".
            "   SELECT useridx, fb_id, nickname, total_money, coin, isblock, leavedays, logindate, createdate, welcome_coin, last_a2u_send_date, last_email_send_date, last_push_send_date, last_fb_msg_send_date, IF(last_a2u_send_date >= last_email_send_date, last_a2u_send_date, last_email_send_date) AS contact_1, IF(last_push_send_date >= last_fb_msg_send_date, last_push_send_date, last_fb_msg_send_date) AS contact_2 ". 
            "   FROM tbl_vip_leave_user ". 
            "   WHERE 1=1 AND status = 0  $sql_money $sql_leavedays $sql_is_a2u $sql_is_email $sql_is_push $sql_is_msg".  
            ") t1 ".
            "WHERE 1=1 $sql_contactdays ".
            "ORDER BY $search_order_by ". 
            "LIMIT ".(($page-1) * $listcount).", ".$listcount;
    $vip_list = $db_main2->gettotallist($sql);
	
	//일괄발송 기능시 필요한 대상자 sql
	if($search_send_type == "a2u"  || $search_send_type == "push")
	{
		$bundle_sql = 	"SELECT useridx, fb_id, nickname, total_money, email,  coin, isblock, leavedays, logindate, createdate, welcome_coin, last_a2u_send_date, last_email_send_date, last_push_send_date, last_fb_msg_send_date, IF(contact_1 >= contact_2, contact_1, contact_2) AS max_contact ". 
							"FROM ( ".
							"   SELECT useridx, fb_id, nickname, total_money, email, coin, isblock, leavedays, logindate, createdate, welcome_coin, last_a2u_send_date, last_email_send_date, last_push_send_date, last_fb_msg_send_date, IF(last_a2u_send_date >= last_email_send_date, last_a2u_send_date, last_email_send_date) AS contact_1, IF(last_push_send_date >= last_fb_msg_send_date, last_push_send_date, last_fb_msg_send_date) AS contact_2 ". 
							"   FROM tbl_vip_leave_user ". 
							"   WHERE 1=1 AND status = 0 $sql_money $sql_leavedays $sql_is_a2u $sql_is_email $sql_is_push $sql_is_msg".  
							") t1 ".
							"WHERE 1=1 $sql_contactdays ";
	}
    
    $sql = "SELECT COUNT(*) ".
            "FROM ( ".
            "   SELECT useridx, nickname, total_money, coin, isblock, leavedays, logindate, createdate, IF(last_a2u_send_date >= last_email_send_date, last_a2u_send_date, last_email_send_date) AS contact_1, IF(last_push_send_date >= last_fb_msg_send_date, last_push_send_date, last_fb_msg_send_date) AS contact_2 ". 
            "   FROM tbl_vip_leave_user ". 
            "   WHERE 1=1 AND status = 0 $sql_money $sql_leavedays $sql_is_a2u $sql_is_email $sql_is_push $sql_is_msg".  
            ") t1 ".
            "WHERE 1=1 $sql_contactdays ";
    $totalcount = $db_main2->getvalue($sql);
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
    {
        $page = floor(($totalcount + $listcount - 1) / $listcount);
    }
    
    $db_main2->end();
?>
<script type="text/javascript">
function go_view(useridx)
{
    window.open("../../m2_user/user_view.php?useridx="+useridx);
}

function search_press(e)
{
    if (((e.which) ? e.which : e.keyCode) == 13)
    {
        search();
    }
}

function search()
{
    var search_form = document.search_form;
        
    search_form.submit();
}

function pop_a2u_write(useridx, fb_id, nickname, welcome_coin)
{
    open_layer_popup("pop_a2u_write.php?fb_id="+fb_id+"&useridx="+useridx+"&nickname="+nickname+"&welcome_coin="+welcome_coin, 750, 370, event.pageY+100);
}

function pop_a2u_write_bundle(money, leavedays, contactdays)
{
    open_layer_popup("pop_a2u_write.php?money="+money+"&leavedays="+leavedays+"&contactdays="+contactdays, 750, 320, event.pageY-500);
}

function pop_email_file_export(start_value, end_value, leave_std, contact_std, leavedays, contactdays, count)
{
	
	var date = new Date(); 
	var year = date.getFullYear(); 
	var month = new String(date.getMonth()+1); 
	var day = new String(date.getDate()); 

	// 한자리수일 경우 0을 채워준다. 
	if(month.length == 1)
	{ 
	  month = "0" + month; 
	} 
	
	if(day.length == 1)
	{ 
	  day = "0" + day; 
	} 

	var alert_message = "총 "+count+"명의 유저데이터를 추출하시겠습니까?";
	var filename = "vip_leave_emaillist_"+year+month+day+".csv";
	
	if(confirm(alert_message))
	{
		window.location.href = 'vip_email_csv_export_download.php?start_value='+start_value+'&end_value='+end_value+'&leave_std='+leave_std+'&contact_std='+contact_std+'&leavedays='+leavedays+'&contactdays='+contactdays+'&filename='+filename;
	}
}

function pop_push_write(useridx, nickname, welcome_coin)
{
    open_layer_popup("pop_push_write.php?useridx="+useridx+"&nickname="+nickname+"&welcome_coin="+welcome_coin, 750, 400, event.pageY+100);
}

function pop_push_write_bundle(money, leavedays, contactdays)
{
    open_layer_popup("pop_push_write.php?money="+money+"&leavedays="+leavedays+"&contactdays="+contactdays, 750, 400, event.pageY+100);
}
</script>  

	<!-- CONTENTS WRAP -->
	<div class="contents_wrap">
        
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; VIP 유저 이탈 관리 (<?= number_format($totalcount) ?>)</div>
		</div>
		<!-- //title_warp -->
		
		<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="vip_user_manage.php">
                <div class="detail_search_wrap">
                    <span class="search_lbl ml20">누적결제금액</span>
                    <input type="text" class="search_text" id="start_value" name="start_value" style="width:60px" value="<?= $search_start_value ?>" onkeypress="search_press(event); return checknum();" /> ~
                    <input type="text" class="search_text" id="end_value" name="end_value" style="width:60px" value="<?= $search_end_value ?>" onkeypress="search_press(event); return checknum();" />

                    <span class="search_lbl ml20">이탈일</span>
                	   <input type="text" class="search_text" id="search_leavedays1" name="search_leavedays1" style="width:30px" value="<?= $search_leavedays1 ?>" onkeypress="search_press(event); return checknum();" />일 ~
                	   <input type="text" class="search_text" id="search_leavedays2" name="search_leavedays2" style="width:30px" value="<?= $search_leavedays2 ?>" onkeypress="search_press(event); return checknum();" />일
                    </select>
                    
                    <span class="search_lbl ml20">컨택일</span>
                    <input type="text" class="search_text" id="search_contactdays" name="search_contactdays" style="width:30px" value="<?= $search_contactdays ?>" onkeypress="search_press(event); return checknum();" />일 
					<select name="search_contact_std" id="search_contact_std"> 
                        <option value="more" <?= ($search_contact_std == "more") ? "selected" : "" ?>>이상</option>
                        <option value="less" <?= ($search_contact_std == "less") ? "selected" : "" ?>>미만</option>
                    </select>
          
          			<span class="search_lbl ml20">컨택 종류</span>
          			<select name="send_type" id="send_type">
						<option value="" <?= ($search_send_type=="") ? "selected" : "" ?>>없음</option>
						<option value="a2u" <?= ($search_send_type=="a2u") ? "selected" : "" ?>>A2U 발송 가능 유저</option>
						<option value="email" <?= ($search_send_type=="email") ? "selected" : "" ?>>Email 발송 가능 유저</option>
						<option value="push" <?= ($search_send_type=="push") ? "selected" : "" ?>>Push 발송 가능 유저</option>
						<!-- <option value="msg" <?= ($search_send_type=="msg") ? "selected" : "" ?>>FB 메신저 가능 유저</option>  -->
					</select>

                    <div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
                </div>
            
                <div style="float:right;padding-bottom:5px;">
                    <select name="orderby" id="orderby" onchange="search()">
                        <option value="total_money ASC" <?= ($search_order_by == "total_money ASC") ? "selected" : "" ?>>누적 결제금액 오름차순</option>
                        <option value="total_money DESC" <?= ($search_order_by == "total_money DESC") ? "selected" : "" ?>>누적 결제금액 내림차순</option>
                    </select>
                </div>
            </form>
                        
		<table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
<?
            if($search_send_type == "a2u" && $search_send_type == "email" && $search_send_type == "push" && $search_send_type == "msg")
            {
?>
				<col width="">
<?
            }
?>
            </colgroup>
	            
            <thead>
                <tr>
                	<th>번호</th>
                    <th>이름</th>
                    <th>useridx</th>
                    <th>누적 결제금액</th>
                    <th>보유코인</th>                    
                    <th>차단여부</th>
<?
            if($search_send_type == "a2u")
            {
?>
                    <th>최근 A2U 발송일</th>
<?
            }
            else if($search_send_type == "email")
            {
?>
                    <th>최근 Email 발송일</th>
<?
            }
            else if($search_send_type == "push")
            {
?>
                    <th>최근 Push 발송일</th>
<?
            }
            else if($search_send_type == "msg")
            {
                ?>
                    <th>최근 메신저 컨택일</th>
<?
            }
            else
            {
?>
					<th>최근 컨택일</th>
<?
            }
?>
                    <th>이탈일</th>
                    <th>Web</th>
                    <th>IOS</th>
                    <th>Android</th>
                    <th>Amazon</th>
                    <th>가입일</th>
                    <th>상세정보</th>
                </tr>
            </thead>
	            
			<tbody>
<?
            for ($i=0; $i<sizeof($vip_list); $i++)
		    {
		        $useridx = $vip_list[$i]["useridx"];
		        $fb_id = $vip_list[$i]["fb_id"];
		        $nickname = $vip_list[$i]["nickname"];
		        $total_money = $vip_list[$i]["total_money"];
		        $coin = $vip_list[$i]["coin"];
		        $isblock = $vip_list[$i]["isblock"];
		        $last_a2u_send_date = $vip_list[$i]["last_a2u_send_date"];
		        $last_email_send_date = $vip_list[$i]["last_email_send_date"];
		        $last_push_send_date = $vip_list[$i]["last_push_send_date"];
		        $last_fb_msg_send_date = $vip_list[$i]["last_fb_msg_send_date"];
		        $max_contact = $vip_list[$i]["max_contact"];
		        $leavedays = $vip_list[$i]["leavedays"];
		        $logindate = $vip_list[$i]["logindate"];
		        $createdate = $vip_list[$i]["createdate"];
		        $welcome_coin = $vip_list[$i]["welcome_coin"];
		        
		        $sql ="SELECT * FROM tbl_user_platform_logindate WHERE useridx = $useridx";
		        $platform_logindate = $db_main->getarray($sql);
		        $web = $platform_logindate['web']; 
		        $ios = $platform_logindate['ios']; 
		        $android = $platform_logindate['android']; 
		        $amazon = $platform_logindate['amazon']; 
?>
				<tr onmouseover="className='tr_over'" onmouseout="className=''">
					<td class="tdc"><?=  (($page-1) * $listcount) + $i + 1 ?></td>
	                <td class="tdc"><?= $nickname ?></td>
	                <td class="tdc"><?= $useridx ?></td>
	                <td class="tdc">$<?= number_format($total_money) ?></td>
	                <td class="tdc"><?= number_format($coin) ?></td>
	                <td class="tdc"><?= ($isblock == 0) ? "X" : "O" ?></td>
<?
            if($search_send_type == "a2u")
            {
?>
                    <td class="tdc"><?= $last_a2u_send_date ?></td>
<?
            }
            else if($search_send_type == "email")
            {
?>
                    <td class="tdc"><?= $last_email_send_date ?></td>
<?
            }
            else if($search_send_type == "push")
            {
                ?>
                    <td class="tdc"><?= $last_push_send_date ?></td>
<?
            }
            else if($search_send_type == "msg")
            {
                ?>
                    <td class="tdc"><?= $last_fb_msg_send_date ?></td>
<?
            }
            else
            {
?>
					<td class="tdc"><?= $max_contact ?></td>
<?
            }
?>
	                <td class="tdc"><?= number_format($leavedays) ?></td>
	                <td class="tdc"><?= $web ?></td>
	                <td class="tdc"><?= $ios ?></td>
	                <td class="tdc"><?= $android ?></td>
	                <td class="tdc"><?= $amazon ?></td>
	                <td class="tdc"><?= $createdate ?></td>
					<td class="tdc"><input type="button" class="btn_03" value="보기" style="cursor:pointer" onclick="event.cancelBubble=true;go_view(<?= $useridx ?>)" /></td>
				</tr>
<?
    		} 
?>
			</tbody>
		</table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
    
    if($search_send_type == "push")
    {
?>
		<div class="button_warp tdr">
			<input type="button" class="btn_setting_01" value="Push 조건 내 일괄 예약 발송" onclick="pop_push_write_bundle('<?= $sql_money?>','<?= $sql_leavedays?>','<?= $sql_contactdays?>');">    		
		</div>
<?
    }
?>
	</div>
	<!--  //CONTENTS WRAP -->
        
	<div class="clear"></div>
        
<?
    $db_main->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
