<? 
    include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
    
    check_login_layer();
    
    $useridx = $_GET["useridx"];
    $send_type = "";
    $nickname = ($_GET["nickname"] == "") ? "#nickname#" : $_GET["nickname"];
    $welcome_coin = ($_GET["welcome_coin"] == "") ? "0" : $_GET["welcome_coin"];
	$sql_money = ($_GET["money"] == "") ? "" : $_GET["money"];
	$sql_leavedays = ($_GET["leavedays"] == "") ? "" : $_GET["leavedays"];
	$sql_contactdays = ($_GET["contactdays"] == "") ? "" : $_GET["contactdays"];
	$bundle_flag = 0;
	
	if($sql_money == "")
	{
		$bundle_flag = 0;
		
		if ($useridx == "")
			error_close_layer("잘못된 접근입니다."); 
	}
	else
	{
		$bundle_flag = 1;
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FB A2U 발송</title>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script> 
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	var tmp_hosturl = "<?= HOST_URL ?>";

	$(function() {
        $("#reservation_date").datepicker({ });
        $("#startdate_date").datepicker({ });
        $("#enddate_date").datepicker({ });
    });

    function layer_close()
    {
        window.parent.close_layer_popup("<?= $_GET["layer_no"] ?>");
    }
    
    function fn_send_push()
    {
        var input_form = document.input_form;
        
        var radioLength = input_form.send_type.length;
        var radio_value = "";

        for(var i = 0; i < radioLength; i++) 
		{
			if(input_form.send_type[i].checked) 
			{
				radio_value =  input_form.send_type[i].value;
    		}
    	}
		
		if(radio_value == 1)
		{
			if (input_form.fb_push_msg.value == "")
			{
				alert("Push 메세지를 입력해주세요.");
				input_form.fb_push_msg.focus();
				return;
			}
		}
		else if(radio_value == 0)
		{
			if (input_form.fb_push_txt.value == "")
			{
				alert("Push 메세지를 입력해주세요.");
				input_form.fb_push_txt.focus();
				return;
			}
		}
		var param = {};
        param.useridx = '<?= $useridx ?>';
        param.fb_push_msg = input_form.fb_push_msg.value;
		param.fb_push_txt = input_form.fb_push_txt.value;
		param.bundle_flag = '<?= $bundle_flag?>';
		param.sql_money = '<?= $sql_money?>';
		param.sql_leavedays = '<?= $sql_leavedays?>';
		param.sql_contactdays = '<?= $sql_contactdays?>';
        param.send_type = radio_value;
        param.reservation_date = input_form.reservation_date.value;
        param.reservation_hour = input_form.reservation_hour.value;
        param.reservation_minute = input_form.reservation_minute.value;
		param.amount = input_form.amount.value;
        
		WG_ajax_execute("user/send_push", param, send_push_callback);
    }
    
    function send_push_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            alert("Push를 발송하였습니다.");
            window.parent.close_layer_popup("<?= $_GET["layer_no"] ?>");
            window.parent.location.reload(true);
        }
    }
	
	function fn_insert_name()
    {
		var input_form = document.input_form;
        
		input_form.fb_push_msg.value += "<?= $nickname?> ";
		input_form.fb_push_txt.value += "<?= $nickname?> ";
    }

    function fn_insert_coin()
    {
		var input_form = document.input_form;
<?
	if($bundle_flag == "0")
	{
?> 
		input_form.fb_push_msg.value += "$ <?= number_format($welcome_coin) ?>";
		input_form.fb_push_txt.value += "$ <?= number_format($welcome_coin) ?>";
<?
	}
	else
	{
?>
		input_form.fb_push_msg.value += "#welcome_coin#";
		input_form.fb_push_txt.value += "#welcome_coin#";
<?
	}
?>
    }

    function showCenterPopup(url, title, width, height) 
    {
    	var popupX = (window.screen.width / 2) - (width / 2);
    	var popupY = (window.screen.height / 2) - (height / 2);

    	var wnd = window.open(url, title, "status=no," + "width=" + width + "," + "height=" + height+ "," + "left=" + popupX + "," + "top=" + popupY + "," + "screenX=" + popupX + "," + "screenY=" + popupY + "," + "scrollbars=yes");

    	return wnd;
	}
	
	function show_msg_input(type)
	{
		var input_form = document.input_form;
		
		var type_0 = document.getElementById("reserve_send");
		var type_1 = document.getElementById("immediately_send");
		var show_push_button = document.getElementById("show_push_button");
		var push_imageurl = document.getElementById("reserve_imageurl");
		var reserve_date = document.getElementById("reserve_send_date");
		
		if(type == 0)
		{
			input_form.fb_push_msg.value = "";
			input_form.fb_push_txt.value = "";
			type_0.style.display = "";
			type_1.style.display = "none";
			show_push_button.value = "예약발송";
			push_imageurl.style.display = "";
			reserve_date.style.display = "";
		}
		else if(type == 1)
		{
			input_form.fb_push_msg.value = "";
			input_form.fb_push_txt.value = "";
			type_0.style.display = "none";
			type_1.style.display = "";
			show_push_button.value = "즉시발송";
			push_imageurl.style.display = "";
			reserve_date.style.display = "none";
		}
		
<?
	if($bundle_flag == "1")
	{
?>
			show_push_button.value = "일괄 예약발송";
<?
	}
?>
	}

</script>
</head>
<body class="layer_body" onload="window.focus()" onkeyup="if (getEventKeycode(event) == 27) { layer_close(); }">
<div id="layer_wrap">   
    <div class="layer_header" >
        <div class="layer_title">Push 발송 </div>
        <img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="layer_close()" />
    </div>        
    <div class="layer_contents_wrap" style="width:740px">
         <!--  레이어 내용  -->
         
         <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
			<table class="tbl_view_basic" style="width:730px">
                <colgroup>
                    <col width="120">
                    <col width="">
                </colgroup>
                    <tbody>
						<tr>
							<th>발송 타입</th>
							<td>
								<input type="radio" value="0" name="send_type" id="send_type_0" <?= ($send_type == "0") ? "checked=\"true\"" : ""?> onclick="show_msg_input(0);"/> 예약 발송
								<?
									if($bundle_flag == "0")
									{
								?>
                				<input type="radio" value="1" name="send_type" id="send_type_1" <?= ($send_type == "1") ? "checked=\"true\"" : ""?>/ onclick="show_msg_input(1);"> 즉시 발송
								<?
									}
								?>
                				<br/><br/>
								*일괄 발송의 경우 마지막 컨택일이 7일 이상인 유저들만 발송</br>
							</td>
                         </tr>
                         <tr style="display:none;" id="immediately_send">
                            <th>Push 메세지(즉시발송)</th>
							<td>
								<!--input type="text" class="view_tbl_text" style="width:550px" name="fb_push_msg" id="fb_push_msg" maxlength="200" value="" /-->
								<textarea style="width:550px;height:50px;" name="fb_push_msg" id="fb_push_msg"></textarea>
								<br/>
								<div class="layer_button_wrap" style="width:562px;text-align:right;">
									<input type="button" class="btn_02" value="유저 이름 추가" onclick="fn_insert_name();" />
									<input type="button" class="btn_02" value="웰컴백 보상 금액 추가" onclick="fn_insert_coin();" />
								</div>
                            </td>
                         </tr>
                         <tr style="display:none;" id="reserve_send_date">
                            <th>예약발송 시간</th>
							<td>
								<input type="input" class="search_text" id="reservation_date" name="reservation_date" style="width:65px" value="" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
								<select id="reservation_hour" name="reservation_hour">
									<option value="">선택</option>
	<?
		for ($i=0;$i<24;$i++)
		{
	?>
									<option value="<?= $i ?>" <?= ($i == $reservation_hour) ? "selected" : "" ?>><?= $i ?></option>
	<?
		}
	?>
								</select>시&nbsp;
								<select id="reservation_minute" name="reservation_minute">
									<option value="">선택</option>
	<?
		for ($i=0;$i<60;$i++)
		{
	?>
									<option value="<?= $i ?>" <?= ($i == $reservation_minute) ? "selected" : "" ?>><?= $i ?></option>
	<?
		}
	?>
								</select>분
							</td>
                         </tr>
                         <tr style="display:none;" id="reserve_send">
                            <th>Push 메세지(예약발송)</th>
							<td>
								<!--input type="text" class="view_tbl_text" style="width:550px" name="fb_push_msg" id="fb_push_msg" maxlength="200" value="" /-->
								<textarea style="width:550px;height:50px;" name="fb_push_txt" id="fb_push_txt"></textarea>
								<div class="layer_button_wrap" style="width:562px;text-align:right;">
									<input type="button" class="btn_02" value="유저 이름 추가" onclick="fn_insert_name();" />
									<input type="button" class="btn_02" value="웰컴백 보상 금액 추가" onclick="fn_insert_coin();" />
								</div>
                            </td>
                         </tr>
						 <tr style="display:none;" id="reserve_imageurl">
                        <th>보상금액</th>
                        <td>
							<input type="text" class="view_tbl_text" name="amount" id="amount" style="width:200px" value="" onkeypress="return checkonlynum()">
                        </td>
                    </tr>
                     </tbody>
             </table>  
             </form>  
         </div>
        <!-- 확인 버튼 -->
        <div class="button_warp tdr"> 
			<input type="button" class="btn_setting_01" value="발송" id="show_push_button" onclick="fn_send_push();">
		</div>
         <!-- 확인 버튼 -->
         <!--  //레이어 내용  -->
    </div>
</div>
</body>
</html>

