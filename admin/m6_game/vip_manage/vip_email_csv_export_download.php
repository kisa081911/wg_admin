<?
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	ini_set("memory_limit", "-1");
	check_login();
	
	$search_start_value = $_GET["start_value"];
	$search_end_value = $_GET["end_value"];
	$search_leave_std = $_GET["leave_std"];
	$search_contact_std = $_GET["contact_std"];
	$search_leavedays = $_GET["leavedays"];
	$sql_contactdays = $_GET["contactdays"];
	$filename = $_GET["filename"];
	
	function outputCSV($data)
	{
		$output = fopen("php://output", "w");
	
		foreach ($data as $row)
		{
			fputcsv($output, $row);
		}
	
		fclose($output);
	}
	
	if($login_adminid == "admin")
	{
		$db = new CDatabase();
		$db2 = new CDatabase2();
		$db_main2_slave = new CDatabase_Slave_Main2();
		
		if($search_start_value != "")
		{
			$sql_money .= " AND $search_start_value <= total_money ";
		}

		if($search_end_value != "")
		{
			$sql_money .= " AND $search_end_value >= total_money ";
		}
		
		$sql_leavedays = "";
		
		if($search_leave_std == "more")
		{
			if($search_leavedays != "")
			{
				$sql_leavedays .= " AND $search_leavedays <= leavedays ";
			}
		}
		else
		{
			if($search_leavedays != "")
			{
				$sql_leavedays .= " AND $search_leavedays > leavedays ";
			}
		}
		
		$sql_contactdays = "";
		
		if($search_contact_std == "more")
		{
			if($search_contactdays != "")
			{
				$sql_contactdays .= " AND DATE_SUB(NOW(), INTERVAL $search_contactdays DAY) >= IF(contact_1 >= contact_2, contact_1, contact_2)";
			}
		}
		else
		{
			if($search_contactdays != "")
			{
				$sql_contactdays .= " AND DATE_SUB(NOW(), INTERVAL $search_contactdays DAY) < IF(contact_1 >= contact_2, contact_1, contact_2)";
			}
		}
		
		$sql = "SELECT useridx, fb_id, nickname, total_money, email, coin, isblock, leavedays, logindate, createdate, welcome_coin, last_a2u_send_date, last_email_send_date, last_push_send_date, last_fb_msg_send_date, IF(contact_1 >= contact_2, contact_1, contact_2) AS max_contact ". 
				"FROM ( ".
				"   SELECT useridx, fb_id, nickname, total_money, email, coin, isblock, leavedays, logindate, createdate, welcome_coin, last_a2u_send_date, last_email_send_date, last_push_send_date, last_fb_msg_send_date, IF(last_a2u_send_date >= last_email_send_date, last_a2u_send_date, last_email_send_date) AS contact_1, IF(last_push_send_date >= last_fb_msg_send_date, last_push_send_date, last_fb_msg_send_date) AS contact_2 ". 
				"   FROM tbl_vip_leave_user ". 
				"   WHERE 1=1 $sql_money $sql_leavedays AND email != '' AND is_email_disable = 0 ".  
				") t1 ".
				"WHERE 1=1 $sql_contactdays ";
		
		$email_user_list = $db_main2_slave->gettotallist($sql);
		
		$email_useridx = "";
		$insert_sql = "";
		
		for($i=0; $i<sizeof($email_user_list); $i++)
		{
			$useridx = $email_user_list[$i]["useridx"];
			
			if($email_useridx == "")
				$email_useridx = $useridx;
			else
				$email_useridx .= ",".$useridx;	
		}
		
		$sql = "SELECT firstname FROM tbl_user WHERE useridx IN ($email_useridx) ORDER BY useridx ASC";
		$firstname_list = $db->gettotallist($sql);
		
		$sql = "UPDATE tbl_vip_leave_user SET last_email_send_date = now() WHERE useridx IN ($email_useridx) AND email != '' AND is_email_disable = 0 ";
		$db2->execute($sql);
		 
		$db->end();
		$db2->end();
		$db_main2_slave->end();
		
		if (sizeof($email_user_list) == 0)
			error_go("저장할 데이터가 없습니다.", "vip_user_manage.php");
		
		$csv_output_list = array();
		
		$csv_output_list[0] = ['email', 'firstname', 'useridx', 'logindate', 'money'];
		
		for($i=0; $i<sizeof($email_user_list); $i++)
		{
			$email = $email_user_list[$i]["email"];
			$firstname = $firstname_list[$i]["firstname"];
			$useridx = $email_user_list[$i]["useridx"];
			$logindate = $email_user_list[$i]["logindate"];
			$money = $email_user_list[$i]["total_money"];
			
			$csv_output_list[$i+1] = [$email, $firstname, $useridx, $logindate, $money];
		}
		
		header('Content-Encoding: UTF-8');
		header("Content-Type: text/csv; charset=UTF-8");
		header("Content-Disposition: attachment; filename=".$filename.".csv");
		
		echo "\xEF\xBB\xBF";
		
		outputCSV($csv_output_list);
	}
	else
	{
		error_go("해당 기능은 admin 유저만 가능합니다.", "vip_user_manage.php");
	}
	
	
	
	
?>