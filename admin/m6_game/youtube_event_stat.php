<?
    $top_menu = "game";
    $sub_menu = "youtube_event_stat";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    include("../common/dbconnect/db_util_redshift.inc.php");
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    
    $listcount = 10;
    $pagename = "youtube_event_stat.php";

    
    $startdate = ($_GET["startdate"] == "") ? date("Y-m-d", mktime(0,0,0,date("m"),date("d")-100,date("Y"))) : $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
    $pagefield = "startdate=$startdate&enddate=$enddate";    
    $db_redshift = new CDatabase_Redshift();
    $db_main2 = new CDatabase_Main2();
    
    $sql = "SELECT COUNT(*) FROM t5_event WHERE category = 23 ";
    $totalcount = $db_redshift->getvalue($sql);
    
    $sql = "SELECT eventidx,category,reward_amount,event_imagepath,start_eventdate,end_eventdate,limit_join,share_enabled,reservation_fanpage_status,writedate,
			(SELECT NVL(SUM(reward_amount),0) AS total_reward_amount FROM t5_event_result WHERE eventidx=t5_event.eventidx AND writedate >='$startdate 00:00:00' AND  writedate <= '$enddate 23:59:59') AS total_reward_amount,
			(SELECT COUNT(*) FROM t5_event_result WHERE eventidx=t5_event.eventidx AND writedate >='$startdate 00:00:00' AND  writedate <= '$enddate 23:59:59') AS total_user,
			(SELECT COUNT(*) FROM t5_event_result WHERE eventidx=t5_event.eventidx AND isnew=1 AND writedate >='$startdate 00:00:00' AND  writedate <= '$enddate 23:59:59') AS new_user,
			(SELECT COUNT(*) FROM t5_event_result WHERE eventidx=t5_event.eventidx AND isreturn=1 AND ordercredit = 0 AND writedate >='$startdate 00:00:00' AND  writedate <= '$enddate 23:59:59') AS return_user,
			(SELECT COUNT(*) FROM t5_event_result WHERE eventidx=t5_event.eventidx AND ordercredit > 0 AND isreturn=1 AND writedate >='$startdate 00:00:00' AND  writedate <= '$enddate 23:59:59') AS return_pay_user,
		    (SELECT COUNT(*) FROM t5_event_result WHERE eventidx=t5_event.eventidx AND writedate BETWEEN t5_event.writedate AND dateadd(min, 180, date(getdate())) AND writedate >='$startdate 00:00:00' AND  writedate <= '$enddate 23:59:59') AS hour3_user
			FROM t5_event WHERE category = 23
			ORDER BY eventidx DESC LIMIT $listcount OFFSET ".(($page-1) * $listcount);
    $eventlist = $db_redshift->gettotallist($sql);
    
    $sql = "SELECT SUM(case when isreturn = 1 then 1  else 0 end) AS isreturn, SUM(case when ordercredit > 0 AND isreturn = 1  then 1  else 0 end ) AS isreturn_payer FROM t5_event_result WHERE reward_type = 23 AND writedate >='$startdate 00:00:00' AND  writedate <= '$enddate 23:59:59'";
    $return_user_data = $db_redshift->getarray($sql);
    
    $isreturn = $return_user_data["isreturn"];
    $isreturn_payer = $return_user_data["isreturn_payer"];
    
    $db_redshift->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">    
    function go_result(eventidx)
    {
        window.location.href = "event_result.php?eventidx="+eventidx;
    }
    $(function() {
        $("#startdate").datepicker({ });
    });

    $(function() {
        $("#enddate").datepicker({ });
    });

    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; YouTube 이벤트 현황 (<?= make_price_format($totalcount) ?>)</div>
            </div>
            <!-- //title_warp -->
            
            <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
            	<input type=hidden name="fanpage" value="<?= $fanpage ?>">
				<input type="input" class="search_text" id="startdate" name="startdate" style="width:65px;margin-left: 859px;" value="<?= $startdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
				<input type="input" class="search_text" id="enddate" name="enddate" style="width:65px" value="<?= $enddate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
				<input type="button" class="btn_search" value="검색" onclick="search()" />
				<br/><br/><br/>
            		<div style="text-align: right;">
	            		<span style="font-weight: bold;">2주 이탈 결제자 수 : <?= number_format($isreturn_payer)?>&nbsp;명, &nbsp;&nbsp; 2주 이탈 비결제자 수 : <?= number_format($isreturn)?>&nbsp;명</span>
            		</div>
            		<br/>
            	
            </form>
            
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="40">
                <col width="">
                <col width="65">
                <col width="65">
                <col width="65">
                <col width="65">
                <col width="65">
                <col width="65">
                <col width="65">
                <col width="90">
                <col width="80">
                <col width="80">
                <col width="80">
                <col width="80">
                <col width="80">
            </colgroup>
            <thead>
                <tr>
                    <th>번호</th>
                    <th class="tbl">이벤트 제목</th>
                    <th class="tbc">3H 내<br>참여</th>
                    <th class="tbc">총<br>참여인원</th>
                    <th class="tbc">신규가입</th>
                    <th class="tbc">2주이탈<br>복귀<br>비결제자</th>
                    <th class="tbc">2주이탈<br>복귀<br>결제자</th>
                    <th class="tdr">발행코인</th>
                    <th>뉴스공지</th> 
                    <th>팬페이지</br/>등록완료</th>
                    <th>이벤트기간</th>
                    <th>등록일</th>
                    <th>결과</th>
                </tr>
            </thead>
            <tbody>
<?
    for ($i=0; $i<sizeof($eventlist); $i++)
    {
        $eventidx = $eventlist[$i]["eventidx"];
        $title = $db_main2->getvalue("SELECT title FROM tbl_event WHERE eventidx = $eventidx");
        $category = $eventlist[$i]["category"];
        $reward_amount = $eventlist[$i]["reward_amount"];
        $total_user = $eventlist[$i]["total_user"];
        $total_reward_amount = $eventlist[$i]["total_reward_amount"];
        $event_imagepath = $eventlist[$i]["event_imagepath"];
        $start_eventdate = $eventlist[$i]["start_eventdate"];
        $end_eventdate = $eventlist[$i]["end_eventdate"];
        $writedate = $eventlist[$i]["writedate"];
        $share_enabled = $eventlist[$i]["share_enabled"];
        $new_user = $eventlist[$i]["new_user"];
        $return_user = $eventlist[$i]["return_user"];
        $return_pay_user = $eventlist[$i]["return_pay_user"];
        $hour3_user = $eventlist[$i]["hour3_user"];
        $reservation_fanpage_status = $eventlist[$i]["reservation_fanpage_status"];
?>
            <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="view_event_dtl(<?= $eventidx ?>)">
                <td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
                <td class="tbl point_title"><?= $title ?></td>
                <td class="tdc"><?= make_price_format($hour3_user) ?></td> 
                <td class="tdc point"><?= make_price_format($total_user) ?></td>
                <td class="tdc"><?= make_price_format($new_user) ?></td>
                <td class="tdc"><?= make_price_format($return_user) ?></td>
                <td class="tdc"><?= make_price_format($return_pay_user) ?></td>
                <td class="tdr point"><?= make_price_format($total_reward_amount) ?></td>
                <td class="tdc"><?= ($share_enabled == "1") ? "공지" : "-" ?></td> 
                <td class="tdc"><?= ($reservation_fanpage_status == "1") ? "완료" : "-" ?></td> 
                <td class="tdc"><?= $start_eventdate ?> ~ <?= $end_eventdate ?></td> 
                <td class="tdc"><?= $writedate ?></td> 
                <td class="tdc"><input type="button" class="btn_03" value="보기" style="cursor:pointer" onclick="event.cancelBubble=true;go_result(<?= $eventidx ?>)" /></td>
            </tr>
<?
    } 
?>
            </tbody>
            </table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>           
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    $db_main2->end();
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
