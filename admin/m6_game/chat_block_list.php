<?
    $top_menu = "game";
    $sub_menu = "chat_block_list";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $db_main2 = new CDatabase_Main2();
    $db_analysis = new CDatabase_Analysis();

    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_useridx = $_GET["search_useridx"];
    $category = $_GET["category"];
    
    $listcount = 10;
    $pagename = "chat_block_list.php";
    
    $tail = "WHERE 1=1 ";
    
    if ($search_useridx != "")
        $tail .= " AND useridx = $search_useridx";
    
    $sql = "SELECT *,(SELECT image_url FROM `tbl_user_report` WHERE report_useridx =tbl_user_profile_block.useridx ORDER BY writedate DESC LIMIT 1) as img_url FROM`tbl_user_profile_block` $tail AND is_block = 1 AND report_count >= 15 ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    $userlist = $db_main2->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) FROM`tbl_user_profile_block`  $tail AND is_block = 1 AND report_count >= 15 ";
    $totalcount = $db_main2->getvalue($sql);
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
    	$page = floor(($totalcount + $listcount - 1) / $listcount);
    
    $db_main2->end();
    $db_analysis->end();
?>
<script type="text/javascript">
function search_press(e)
{
    if (((e.which) ? e.which : e.keyCode) == 13)
    {
        search();
    }
}

function search()
{
    var search_form = document.search_form;
    search_form.submit();
}

function chat_block_update(item)
{
    
    var param = {};
    param.useridx = item;

   	WG_ajax_execute("game/update_chat_block", param, chat_block_update_callback);
}

function chat_block_update_callback(result, reason)
{
    if (!result)
    {
        alert("오류 발생 - " + reason);
    }
    else
    {
        window.location.href = "chat_block_list.php";
    }
}

</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
    
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 채팅 차단 관리 (<?= make_price_format($totalcount) ?>)</div>
	</div>
	<!-- //title_warp -->
        
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
		<div class="detail_search_wrap">
			<span class="search_lbl">useridx</span>
			<input type="text" class="search_useridx" id="search_useridx" name="search_useridx" style="width:250px" value="<?= encode_html_attribute($search_useridx) ?>" onkeypress="search_press(event)" />&nbsp;
                                    
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>
        
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="70">
			<col width="70">
			<col width="70">
			<col width="70">
		</colgroup>
		<thead>
			<tr>
				<th class="tbl">프로필 이미지(useridx)</th>
				<th class="tbc">신고 수</th>
				<th class="tbc">block 일자</th>
				<th>block 해제</th>
			</tr>
		</thead>
		<tbody>
<?
for ($i=0; $i<sizeof($userlist); $i++)
{
    $useridx = $userlist[$i]["useridx"];
    $report_count = $userlist[$i]["report_count"];
    $writedate = $userlist[$i]["writedate"];
    $img_url = $userlist[$i]["img_url"];
?>
        <tr onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="td1"><img src="<?= $img_url?>"  height="40" width="40" class="summary_user_image"/>(<?=$useridx?>)</td>
            <td class="tbc" style = "text-align: center;"><?= $report_count ?></td>
            <td class="tdc"><?= $writedate ?></td>
            <td class="tdc"><input type="button" class="btn_03" value="해제" style="cursor:pointer" onclick="chat_block_update(<?= $useridx ?>)" /></td>
        </tr>
<?
} 
?>
	</tbody>
</table>
<?
include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>

</div>
<!--  //CONTENTS WRAP -->
    
<div class="clear"></div>
<?
include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
