<?
    $top_menu = "game";
    $sub_menu = "event";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $eventidx = $_GET["eventidx"];
    $category = $_GET["category"];
    
    check_number($eventidx);
    
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    $db_mobile = new CDatabase_Mobile();	
    
    if ($eventidx != "")
    {
        $sql = "SELECT eventidx,title,category,reward_type,reward_amount,reward_amount_mobile,limit_join,slotidx,start_eventdate,end_eventdate,event_imagepath,eventcode,shorturl, ".
            "reservation_date,reservation_fanpage,reservation_fanpage_date,reservation_fanpage_status,reservation_fanpage_date,reservation_imagepath,reservation_contents, ".
            "fbid,share_enabled ".
            "FROM tbl_event WHERE eventidx=$eventidx";
        
        $event = $db_main2->getarray($sql);
        
        $eventidx = $event["eventidx"];
        $title = $event["title"];
        $category = $event["category"];
        $reward_type = $event["reward_type"];
        $reward_amount = $event["reward_amount"];
        $reward_amount_mobile = $event["reward_amount_mobile"];
        $limit_join = $event["limit_join"];
        $start_eventdate = $event["start_eventdate"];
        $end_eventdate = $event["end_eventdate"];
        $event_imagepath = $event["event_imagepath"]; 
        $eventcode = $event["eventcode"];
        $slotidx = $event["slotidx"];
        $shorturl = $event["shorturl"];
        $reservation_date = substr($event["reservation_date"],0,10);
        
        $reward_name = "";
        
        if($reward_type == 1 || $reward_type == 7)
        	$reward_name = " Coins";
        else if($reward_type == 2)
        	$reward_name = " TY Points";
        
        if($reservation_date == "0000-00-00")
        	$reservation_date = "";
        
        $reservation_hour = substr($event["reservation_date"],11,2);
        $reservation_minute = substr($event["reservation_date"],14,2); 
        $reservation_fanpage = $event["reservation_fanpage"];
        $reservation_fanpage_date  = $event["reservation_fanpage_date"];
        $reservation_fanpage_status = $event["reservation_fanpage_status"];         
        $reservation_imagepath = $event["reservation_imagepath"];   
        $reservation_contents = $event["reservation_contents"];
		$fbid = $event["fbid"];
		$share_enabled = $event["share_enabled"];
		
		if($category == "6")
		{
			$sql = "SELECT * FROM tbl_ultra_freespin_event WHERE eventidx=$eventidx";
			$slotfreeevent = $db_main2->getarray($sql);
		
			$slotbetcoin = $slotfreeevent["totalbet"];
			$freespincnt = $slotfreeevent["freespin_cnt"];
			$freeslot = $slotfreeevent["slot_type"];
			$start_hour = substr($slotfreeevent["startdate"], 11, 2);
			$end_hour = substr($slotfreeevent["enddate"], 11, 2);
		}
		else if($category == "7" || $category == "8" || $category == "9"|| $category == "10")
		{
			$sql = "SELECT agencyidx, sendidx, promotion_text, contents_image_url, email_width, target_type FROM tbl_email_retention_setting WHERE eventidx = $eventidx";
			$email_retention_info = $db_main2->getarray($sql);
			
			$agencyidx = $email_retention_info["agencyidx"];
			$sendidx = $email_retention_info["sendidx"];
			$promotion_text = $email_retention_info["promotion_text"];
			$contents_image_url = $email_retention_info["contents_image_url"];
			$email_width = $email_retention_info["email_width"];
			$target_type = $email_retention_info["target_type"];
		}
        if ($eventidx == "")
            error_back("삭제되었거나 등록되지 않은 이벤트입니다.");
    }  
    else
    {
        $eventcode = uniqid();
    }
    
    $sql = "SELECT slottype,slotname FROM tbl_slot_list";
    $totalslotlist = $db_main2->gettotallist($sql);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript">
    function save_event()
    {
        var input_form = document.input_form;
        var category = input_form.category.value;
        var agencyidx = document.getElementById("row_agencyidx");
        var sendidx = document.getElementById("row_sendidx");
        var promotion_text = document.getElementById("row_promotion_text");
        var contents_image_url = document.getElementById("row_contents_image_url");
        var target_type = document.getElementById("row_target_type");
        
        
        if (input_form.title.value == "")
        {
            alert("이벤트 제목을 입력하세요.");
            input_form.title.focus();
            return;
        }

        if ((category == "1" || category == "4" || category == "7" || category == "10") && input_form.reward_amount.value == "")
        {
            alert("리워드 코인을 입력하세요.");
            input_form.reward_amount.focus();
            return;
        }

        if(input_form.reward_amount.value > 300000)
        {
        	if(confirm("리워드 코인이 300,000 이상입니다. 저장하시겠습니까?") == 0)
    			return;
        }
        
        if(input_form.reward_amount_mobile.value > 300000)
        {
        	if(confirm("리워드 코인이 300,000 이상입니다. 저장하시겠습니까?") == 0)
    			return;
        }

        if (category == "2" && input_form.reward_amount.value == "")
        {
            alert("리워드  TY을 입력하세요.");
            input_form.reward_amount.focus();
            return;
        }


        if (category == "4" && input_form.total_coin.value == "")
        {
            alert("전체 리워드 코인을 입력하세요.");
            input_form.reward_amount.focus();
            return;
        }

        if (category == "6" && input_form.slotbetcoin.value == "")
        {
            alert("토탈벳 금액을 입력하세요.");
            input_form.slotbetcoin.focus();
            return;
        }

        if(category == "6" && input_form.slotbetcoin.value > 2000)
        {
    		if(confirm("토탈벳 금액이 2000 이상입니다. 저장하시겠습니까?") == 0)
    			return;
        }

        if (category == "6" && input_form.freespincnt.value == "")
        {
            alert("슬롯 프리스핀 수를 입력하세요.");
            input_form.freespincnt.focus();
            return;
        }
        
        if (input_form.start_eventdate.value == "")
        {
            alert("이벤트 기간을 입력하세요.");
            input_form.start_eventdate.focus();
            return;
        }
        
        if (input_form.end_eventdate.value == "")
        {
            alert("이벤트 기간을 입력하세요.");
            input_form.end_eventdate.focus();
            return;
        }
        
        if (input_form.reservation_date.value != "" && input_form.reservation_hour.value == "")
        {
            alert("예약게시 시간을 선택해주세요.");
            input_form.reservation_hour.focus();
            return;
        }
        
        if (input_form.reservation_date.value != "" && input_form.reservation_minute.value == "")
        {
            alert("예약게시 시간을 선택해주세요.");
            input_form.reservation_minute.focus();
            return;
        }

        if(category == 1 || category == 4 || category == 7 || category == 10)
        {
            if(input_form.reward_amount_mobile.value > 0 )
			{
            	reward_type = 3;
            }
            else
            {
            	reward_type = 1;
            }
        }
     	else if(category == 2)
     		reward_type = 2;
     	else
     		reward_type = 0;
 		   
        var param = {};
        param.eventidx = "<?= $eventidx ?>";
        param.title = input_form.title.value;
        param.category = category;
        param.reward_type = reward_type;
        param.reward_amount = input_form.reward_amount.value; 
        param.reward_amount_mobile = input_form.reward_amount_mobile.value; 
        param.total_coin =  input_form.total_coin.value;       
        param.eventcode = input_form.eventcode.value;
        param.slotidx = input_form.slot_type.value;
        param.slotbetcoin = input_form.slotbetcoin.value;
        param.freespincnt = input_form.freespincnt.value;        
        param.start_eventdate = input_form.start_eventdate.value;
        param.end_eventdate = input_form.end_eventdate.value;
        param.event_imagepath = input_form.event_imagepath.value;
        param.reservation_date = input_form.reservation_date.value;
        param.reservation_hour = input_form.reservation_hour.value;
        param.reservation_minute = input_form.reservation_minute.value;
        param.reservation_fanpage = ((input_form.reservation_fanpage.checked) ? "1" : "0");
        param.reservation_imagepath = input_form.reservation_imagepath.value;
        param.reservation_contents = input_form.reservation_contents.value;
        param.share_enabled = get_radio("share_enabled");
        param.shorturl = input_form.shorturl.value;

	    // Email Retention (category : 8,9,10)
        param.agencyidx=input_form.agencyidx.value;
        param.sendidx=input_form.sendidx.value;
        param.promotion_text=input_form.promotion_text.value;
        param.contents_image_url=input_form.contents_image_url.value;
        param.email_width=input_form.email_width.value;
        param.target_type=input_form.target_type.value;
        

        if ("<?= $eventidx ?>" == "")
        	WG_ajax_execute("game/insert_event", param, save_event_callback);
        else
        	WG_ajax_execute("game/update_event", param, save_event_callback);
    }

    function save_event_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            window.location.href = "event.php";
        }
    }
    
    function delete_event()
    {
        var input_form = document.input_form;

        if (!confirm("이벤트를 삭제 하시겠습니까?"))
            return;

        var param = {};

        param.eventidx = "<?= $eventidx ?>";
        param.category = "<?= $category ?>";

        WG_ajax_execute("game/delete_event", param, delete_event_callback);
    }

    function delete_event_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.href = "event.php";
        }
    }
    
    function get_event_info(category)
    {
    	 if(category == "")
         {
             $("#row_reward_amount").hide();
             $("#row_total_coin").hide();
         }
         else
         {
        	 var rewardname = "";
        	 
 	      	 if (category == "1" || category == "2")
 	      	 {
 	      		$("#row_reward_amount").show();
 	      		$("#row_slot_list").show();
 	      	    $("#row_total_coin").hide();
 	      	    $("#row_slotbetcoin").hide();
 	      	 	$("#row_freespincnt").hide();
 	         	
 	         	if(category == "1" || category == "7")
 	         		rewardname = " Coins";
 	         	else if(category == "2")
 	         		rewardname = " TY Points";

 	         	$("#reward_name").html(rewardname); 	         	 
 	      	 } 
 	      	 else if (category == "3" || category == "5")
             {
         		$("#row_reward_amount").hide();
         		$("#row_event_img").hide();
         		$("#row_total_coin").hide();
         		$("#row_slotbetcoin").hide();
         		$("#row_freespincnt").hide();
         		$("#row_slot_list").hide();
             }
 	      	 else if(category == "4")
             {
         		$("#row_reward_amount").show();
                $("#row_total_coin").show();
                $("#row_slot_list").show();
                $("#row_slotbetcoin").hide();
                $("#row_freespincnt").hide();

                rewardname = " Coins";
                
                $("#reward_name").html(rewardname);
                $("#total_reward_name").html(rewardname);
             }
 	      	 else if(category == "6")
 	      	 {
 	      		$("#row_reward_amount").hide();
         		$("#row_event_img").hide();
 	      		$("#row_total_coin").hide();
 	      		$("#row_slotbetcoin").show();
 	      		$("#row_freespincnt").show();
 	      		$("#row_slot_list").show();
 	      	 }
 	      	//이메일 리텐션 업체명
 	      	 else if(category == "7" || category == "8" || category == "9"|| category == "10")
 	      	 {
 				row_agencyidx.style.display = "";
 				row_sendidx.style.display = "";
 				row_promotion_text.style.display = "";
 				row_contents_image_url.style.display = "";
 				row_email_width.style.display = "";
 				row_target_type.style.display = "";
 			 }
 			 else
 			 {
 				row_agencyidx.style.display = "none";
 				row_sendidx.style.display = "none";
 				row_promotion_text.style.display = "none";
 				row_contents_image_url.style.display = "none";
 				row_email_width.style.display = "none";
 				row_target_type.style.display = "none";
 			 }
         }
         
        var param = {};
        param.category = category;        
        
        WG_ajax_query("game/get_event_info", param, get_event_info_callback, false);
    }
    
    function get_event_info_callback(result, reason, map)
    {
        if (!result)
        {
        }
        else
        {
            if (map.check_event == 0)
            	return;

            var info_category = map.category;
			var info_title = map.title;
			var reward_type = map.coin;			
			var event_imagepath = map.event_imagepath;
        	
            $("#title").val(info_title);
            $("#reward_amount").val(map.reward_amount);            
			$("#event_imagepath").val(event_imagepath);            
            $("#reservation_imagepath").val(map.reservation_imagepath);
            $("#reservation_contents").val(map.reservation_contents);            
        }
    }
    
    function update_shorturl()
    {
        var param = {};
        param.eventidx = "<?= $eventidx ?>";
        
        WG_ajax_execute("game/update_shorturl", param, update_shorturl_callback);
    }

    function update_shorturl_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            window.location.reload(false);
        }
    }
    
    $(function() {
        $("#start_eventdate").datepicker({ defaultDate : <?= date("y-m-d"); ?> });
    });
    
    $(function() {
        $("#end_eventdate").datepicker({ defaultDate : <?= date("y-m-d"); ?> });
    });
   
    $(function() {
        $("#reservation_date").datepicker({ });
    }); 
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 이벤트 관리</div>
<?
    if ($eventidx != "")
    { 
?>
            <div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_event()"></div>
<?
    } 
?>
	</div>
	<!-- //title_warp -->
            
	<form name="input_form" id="input_form" onsubmit="return false">
		<div class="h2_title">[이벤트 정보]</div>
		<table class="tbl_view_basic">
			<colgroup>
				<col width="150">
				<col width="">
			</colgroup>
			<tbody>
				<tr>
					<th>이벤트 종류</th>
					<td>
						<select id="category" name="category" onchange="get_event_info(this.value);">
							<option value="">선택</option>
							<option value="1" <?= ($category=="1") ? "selected" : "" ?>>Free Coins Event</option>
							<option value="2" <?= ($category=="2") ? "selected" : "" ?>>Free TY Points Event</option>
							<option value="3" <?= ($category=="3") ? "selected" : "" ?>>Extra Lucky Wheel Event</option>
							<option value="4" <?= ($category=="4") ? "selected" : "" ?>>Giveaway Event</option>
							<option value="5" <?= ($category=="5") ? "selected" : "" ?>>WelcomeBack 이벤트</option>
							<option value="6" <?= ($category=="6") ? "selected" : "" ?>>Free Spin Event</option>
							<option value="7" <?= ($category=="7") ? "selected" : "" ?>>Email Retention Event</option>
							<option value="10" <?= ($category == "10") ? "selected" : "" ?>>챗봇 이벤트</option>
							<option value="20" <?= ($category == "20") ? "selected" : "" ?>>팬페이지 스크래치 카드</option>
							<option value="21" <?= ($category == "21") ? "selected" : "" ?>>팬페이지 슬롯오브 포춘</option>
							<option value="22" <?= ($category == "22") ? "selected" : "" ?>>팬페이지 이벤트 황금알</option>
							<option value="23" <?= ($category == "23") ? "selected" : "" ?>>YouTube 리텐션</option>
							<option value="24" <?= ($category == "24") ? "selected" : "" ?>>Instagram 리텐션</option>
						</select> <?= ($eventidx != "") ? "(변경 시 저장 후 short url 재발급 필요)" : "" ?>
					</td>
				</tr>
				<tr>
					<th>이벤트 제목</th>
					<td><input type="text" class="view_tbl_text" name="title" id="title" maxlength="200" value="<?= $title ?>" /></td>
				</tr>				
				<tr id="row_agencyidx"  style="<?= ($category == "7" || $category == "8" || $category == "9" || $category == "10") ? "" : "display:none"?>">
					<th>업체명</th>
	                <td>
	                 	<select name="agencyidx" id="agencyidx">
							<option value="">선택하세요</option>

							<option value="3" <?= ($agencyidx == "3") ? "selected" : "" ?>>SES</option>
						</select>
	                </td>
				</tr>
				<tr id="row_sendidx" style="<?= ($category == "7" || $category == "8" || $category == "9" || $category == "10") ? "" : "display:none"?>">
                   	<th>회차</th>
                   	<td><input type="text" class="view_tbl_text" name="sendidx" id="sendidx" style="width:200px" value="<?= ($sendidx == "") ? "" : $sendidx ?>" onkeypress="return checkonlynum()" /></td>
				</tr>
                <tr id="row_target_type" style="<?= ($category == "7" || $category == "8" || $category == "9" || $category == "10") ? "" : "display:none"?>">
                   	<th>결제조건</th>
                   	<td>
                   		<select name="target_type" id="target_type">
							<option value="1" <?= ($target_type == "1") ? "selected" : "" ?>>결제자</option>
							<option value="0" <?= ($target_type == "0") ? "selected" : "" ?>>비결제자</option>
						</select>
					</td>
                </tr>
                <tr id="row_promotion_text" style="<?= ($category == "7" || $category == "8" || $category == "9") || $category == "10" ? "" : "display:none"?>">
                    	<th>프로모션 문구</th>
                    	<td><input type="text" class="view_tbl_text" name="promotion_text" id="promotion_text" maxlength="200" value="<?= $promotion_text ?>" /></td>
                </tr>
				<tr id="row_email_width" style="<?= ($category == "7" || $category == "8" || $category == "9" || $category == "10") ? "" : "display:none"?>">
                   	<th>가로 영역 사이즈</th>
                   	<td><input type="text" class="view_tbl_text" name="email_width" id="email_width" style="width:200px" value="<?= ($email_width == "") ? "600" : $email_width ?>" onkeypress="return checkonlynum()" /></td>
                </tr>
               	<tr id="row_contents_image_url" style="<?= ($category == "7" || $category == "8" || $category == "9" || $category == "10") ? "" : "display:none"?>">
					<th>증앙 이미지 경로</th>
                    <td>
                    	<input type="text" class="view_tbl_text" name="contents_image_url" id="contents_image_url" value="<?= encode_html_attribute($contents_image_url) ?>" />
<?
    if ($contents_image_url != "")
    {
?>
                           <div><img src="<?= $contents_image_url ?>" class="mt5" /></div>
<?
    }
?>
					</td>
                </tr>
                <tr id="row_reward_amount" style="display:<?= ($category == "3" || $category == "5" || $category == "6") ? "none;" : ""?>">
				<th>리워드 코인</th>
                        <td><input type="text" class="view_tbl_text" name="reward_amount" id="reward_amount" style="width:200px" value="<?= $reward_amount ?>" onkeypress="return checkonlynum()" /><span id="reward_name" style="font-weight:bold;"><?= $reward_name?></span> (Web) &nbsp;&nbsp;&nbsp;<input type="text" class="view_tbl_text" name="reward_amount_mobile" id="reward_amount_mobile" style="width:200px" value="<?= $reward_amount_mobile ?>" onkeypress="return checkonlynum()" /><span id="reward_name_mobile" style="font-weight:bold;"><?= $reward_name?></span> (Mobile)</td>
				</tr>
				<tr id="row_total_coin" style="display:<?= ($category == "4") ? "" : "none;"?>">
                        <th>전체 지급 칩</th>
                        <td><input type="text" class="view_tbl_text" name="total_coin" id="total_coin" style="width:200px" value="<?= ($category == "4") ? $limit_join : "" ?>" onkeypress="return checkonlynum()" /><span id="total_reward_name" style="font-weight:bold;"><?= $reward_name?></span></td>
				</tr>
				<tr id="row_event_img" style="display: <?= ($category=="3" || $category == "5"  || $category == "6") ? "none;" : "" ?>">
					<th>이벤트 이미지</th>
					<td><input type="text" class="view_tbl_text" name="event_imagepath" id="event_imagepath" value="<?= encode_html_attribute($event_imagepath) ?>" />
<?
    if ($event_imagepath != "")
    {
?>
						<div><img src="<?= $event_imagepath ?>" class="mt5" /></div>
<?
    }
?>
					</td>	                    	
				</tr>					    
				<tr>
					<th>이벤트기간</th>
					<td>
						<input type="input" class="search_text" id="start_eventdate" name="start_eventdate" style="width:65px" value="<?= $start_eventdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
						-
						<input type="input" class="search_text" id="end_eventdate" name="end_eventdate" style="width:65px" value="<?= $end_eventdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
					</td>
				</tr>
				<tr id="row_slotbetcoin" style="<?= ($category == "6") ? "" : "display:none"?>">
	            	<th>토탈벳금액</th>
	            	<td>
	            		<input type="text" class="view_tbl_text" name="slotbetcoin" id="slotbetcoin" style="width:200px" value="<?= $slotbetcoin ?>" onkeypress="return checkonlynum()" />
	            	</td>
	            </tr>
	            <tr id="row_freespincnt" style="<?= ($category == "6") ? "" : "display:none"?>">
	            	<th>슬롯프리스핀횟수</th>
	            	<td>
	            		<input type="text" class="view_tbl_text" name="freespincnt" id="freespincnt" style="width:200px" value="<?= $freespincnt ?>" onkeypress="return checkonlynum()" />
	            	</td>
	            </tr>                    
           		<tr id="row_slot_list">
            		<th>슬롯</th>
            		<td>
            			<select name="slot_type" id="slot_type">
							<option value="">선택하세요</option>
<?
	for ($i=0; $i<sizeof($totalslotlist); $i++)
	{
		$slottype = $totalslotlist[$i]["slottype"];
		$slotname = $totalslotlist[$i]["slotname"];
?>	
							<option value="<?= $slottype ?>" <?= ($slottype == $slotidx) ? "selected=\"true\"" : "" ?>><?= $slotname ?></option>
<?						
	}
?>						
						</select>* 슬롯 바로가기 기능 (변경 시 저장 후 short url 재발급 필요)
            		</td>
            	</tr>
				<tr>
					<th>이벤트 코드</th>
					<td><input type="text" class="view_tbl_text" style="width:150px;" name="eventcode" id="eventcode" value="<?= encode_html_attribute($eventcode) ?>" readonly /></td>
				</tr>
				<tr>
					<th>short url</th>					
					<td><input type="text" name="shorturl" id="shorturl" class="view_tbl_text" style="width:400px;" value="<?= ($shorturl == "") ? LINK_URL : $shorturl ?>"/></td>
				</tr>
				<tr id="row_event_isshare" style="display: <?= ($category=="3") ? "none;" : "" ?>">
					<th>Share전파여부</th>
					<td>
						<input type="radio" value="0" name="share_enabled" id="share_enabled_0" <?= ($share_enabled == "" || $share_enabled == "0") ? "checked=\"true\"" : ""?>/> 미전파 
						<input type="radio" value="1" name="share_enabled" id="share_enabled_1" <?= ($share_enabled == "1") ? "checked=\"true\"" : ""?>/> 전파 
					</td>
				</tr>                                                        	
			</tbody>
		</table>
		<div class="h2_title pt20">[예약 게시 설정]</div>
		<table class="tbl_view_basic">
			<colgroup>
				<col width="180">
				<col width="">
			</colgroup>
			<tbody>
				<tr>
					<th>예약게시시간</th>
					<td>
						<input type="input" class="search_text" id="reservation_date" name="reservation_date" style="width:65px" value="<?= $reservation_date ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
						<select id="reservation_hour" name="reservation_hour">
							<option value="">선택</option>
<?
    for ($i=0;$i<24;$i++)
    {
?>
							<option value="<?= $i ?>" <?= ($i == $reservation_hour) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
						</select>시&nbsp;
						<select id="reservation_minute" name="reservation_minute">
							<option value="">선택</option>
<?
    for ($i=0;$i<60;$i++)
    {
?>
							<option value="<?= $i ?>" <?= ($i == $reservation_minute) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
						</select>분
					</td>
				</tr>
				<tr>
					<th>팬페이지 게시 여부</th>
					<td>
						<input type="checkbox" name="reservation_fanpage" id="reservation_fanpage" value="1" <?= ($reservation_fanpage == 1) ? "checked" :"" ?>> 게시
						<?= ($reservation_fanpage_status == "1") ? ",&nbsp;예약게시완료일 : ".$reservation_fanpage_date." 성공" : "" ?>
					</td>
				</tr>
                    
				<tr>
					<th>이미지 게시 URL</th>
					<td>
						<input type="text" class="view_tbl_text" name="reservation_imagepath" id="reservation_imagepath" maxlength="200" value="<?= $reservation_imagepath ?>" /><br/>
<?
    if ($reservation_imagepath != "")
    {
?>
						<img src="<?= $reservation_imagepath ?>">
<?
    } 
?>
   
					</td>
				</tr>
				<tr>
					<th>내용</th>
					<td>
						<textarea style="width:700px;height:350px;" name="reservation_contents" id="reservation_contents"><?= $reservation_contents ?></textarea>
						<br/>* URL은 [URL]로 입력
					</td>
				</tr>
			</tbody>
		</table>
	</form>
            
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="저장" onclick="save_event()">
		<input type="button" class="btn_setting_02" value="취소" onclick="go_page('event.php')">            
	</div>
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main->end();
	$db_main2->end();
	$db_mobile->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>