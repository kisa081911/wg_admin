<?
    $top_menu = "game";
    $sub_menu = "notice_lobby";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $mesgidx = $_GET["mesgidx"];
    check_number($mesgidx);
    
    $db_main2 = new CDatabase_Main2();
    
    if ($mesgidx != "")
    {
        $sql = "SELECT * FROM tbl_notice_lobby_info WHERE mesgidx=$mesgidx";
        
        $event = $db_main2->getarray($sql);
        
        $mesgidx = $event["mesgidx"];
        $groups = $event["groups"];
        $message = $event["message"];
        $enable = $event["enabled"];

    }  
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    function save_event()
    {
        var input_form = document.input_form;
        
        if (input_form.message.value == "")
        {
        	alert("문구 내용을 입력하세요.");
            input_form.message.focus();
            return;
        }

        if (input_form.enable.value == "")
        {
            alert("상태 값을 설정 하세요");
            input_form.enable.focus();
            return;
        }
        
        if (input_form.groups.value == "" )
        {
            alert("그룹을 선택해주세요.");
            input_form.groups.focus();
            return;
        }

        var param = {};
        param.mesgidx = "<?= $mesgidx ?>";
        param.groups = input_form.groups.value;
        param.enable = input_form.enable.value;
        param.message = input_form.message.value;
        
        if ("<?= $mesgidx ?>" == "")
        	WG_ajax_execute("game/insert_notice_lobby", param, save_event_callback);
        else
        	WG_ajax_execute("game/update_notice_lobby", param, save_event_callback);
    }

    function save_event_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            window.location.href = "notice_lobby.php";
        }
    }
    
    function delete_event()
    {
        var input_form = document.input_form;

        if (!confirm("문구를 삭제 하시겠습니까?"))
            return;

        var param = {};

        param.mesgidx = "<?= $mesgidx ?>";

        WG_ajax_execute("game/delete_notice_lobby", param, delete_event_callback);
    }

    function delete_event_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.href = "notice_lobby.php";
        }
    }
    
    
    $(function() {
        $("#startdate_day").datepicker({ defaultDate : <?= date("y-m-d"); ?> });
    });
    
    $(function() {
        $("#enddate_day").datepicker({ defaultDate : <?= date("y-m-d"); ?> });
    });

    function fnChkByte(obj, maxByte) {
        var str = obj.value;
        var str_len = str.length;
        var rbyte = 0;
        var rlen = 0;
        var one_char = "";
        var str2 = "";
        for (var i = 0; i < str_len; i++) {
            one_char = str.charAt(i);
            if (escape(one_char).length > 4) {
                rbyte += 2; //한글2Byte
            } else {
                rbyte++; //영문 등 나머지 1Byte
            }
            if (rbyte <= maxByte) {
                rlen = i + 1; //return할 문자열 갯수
            }
        }
        if (rbyte > maxByte) {
            alert("한글 " + (maxByte / 2) + "자 / 영문 " + maxByte + "자를 초과 입력할 수 없습니다.");
            str2 = str.substr(0, rlen); //문자열 자르기
            obj.value = str2;
            fnChkByte(obj, maxByte);
        }
    }
        
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 전광판 문구 상세</div>
<?
if ($mesgidx != "")
    { 
?>
            <div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_event()"></div>
<?
    } 
?>
	</div>
	<!-- //title_warp -->
            
	<form name="input_form" id="input_form" onsubmit="return false">
		<div class="h2_title">[전광판 문구]</div>
		<table class="tbl_view_basic">
			<colgroup>
				<col width="150">
				<col width="">
			</colgroup>
			<tbody>
				<tr>
					<th>Groups</th>
					<td>
						<select id="groups" name="groups">
							<option value="">선택</option>
								<option value="0" <?= ($groups=="0") ? "selected" : "" ?>>groups 0 (신규 유저 그룹 (가입 1주일 이내))</option>
                				<option value="1" <?= ($groups=="1") ? "selected" : "" ?>>groups 1 (2주 이상 이탈 복귀자 그룹)</option>
                				<option value="2" <?= ($groups=="2") ? "selected" : "" ?>>groups 2 (결제경험 유저 그룹)</option>
                				<option value="3" <?= ($groups=="3") ? "selected" : "" ?>>groups 3 (비결제 유저 그룹 (위 3개 그룹 제외한 나머지))</option>
                				<option value="10" <?= ($groups=="10") ? "selected" : "" ?>>groups 10 (격려, 유머, 격언 문구)</option>
                				<option value="11" <?= ($groups=="11") ? "selected" : "" ?>>groups 11 (게임 팁 문구)</option>
                				<option value="12" <?= ($groups=="12") ? "selected" : "" ?>>groups 12 (시즌 성 문구)</option>
                				<option value="13" <?= ($groups=="13") ? "selected" : "" ?>>groups 13 (슬롯 홍보)</option>
						</select> 
					</td>
				</tr>
				<tr>
					<th><span>*</span> messages</th>
                    <td>
                        <textarea style="width:700px;height:350px;" name="message" id="message" onKeyUp="javascript:fnChkByte(this,'1000')"><?= $message ?></textarea>
                    </td>
				</tr>				
				<tr>
					<th>enable</th>
					<td>
						<select id="enable" name="enable">
							<option value="">선택</option>
							<option value="0" <?= ($enable=="0") ? "selected" : "" ?>>비활성</option>
							<option value="1" <?= ($enable=="1") ? "selected" : "" ?>>활성</option>
						</select> 
					</td>
				</tr>				
			
			</tbody>
		</table>
	</form>
            
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="저장" onclick="save_event()">
		<input type="button" class="btn_setting_02" value="취소" onclick="go_page('notice_lobby.php')">            
	</div>
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main2->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>