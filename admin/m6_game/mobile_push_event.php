<?
    $top_menu = "game";
    $sub_menu = "mobile_push_event";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $os_type = ($_GET["os_type"] == "") ? "" : $_GET["os_type"];
    $reward_type = ($_GET["reward_type"] == "") ? "" : $_GET["reward_type"];
    $search_sent = ($_GET["search_sent"] == "") ? "" : $_GET["search_sent"];

    $listcount = 10;
    $pagename = "mobile_push_event.php";

    $tail = " ";
    
    if($os_type != "")
    	$tail .= " AND os_type = $os_type ";
    
    if($reward_type != "")
    	$tail .= " AND reward_type  = $reward_type ";    
    
    if($search_sent != "")
    	$tail .= " AND sent = '$search_sent' ";
    
    $pagefield = "os_type=$os_type&reward_type=$reward_type&search_sent=$search_sent";    
    
    $db_main2 = new CDatabase_Main2();
    
    $sql = "SELECT eventidx, os_type, message, reward_type, reward_amount, sent, enabled, reservedate, ".
      		"(SELECT IFNULL(SUM(reward_amount),0) AS total_amount FROM tbl_push_event_result WHERE eventidx=tbl_push_event.eventidx) AS total_amount,".
    		"(SELECT COUNT(*) FROM tbl_push_event_result WHERE eventidx=tbl_push_event.eventidx) AS total_user ".    		
    		"FROM `tbl_push_event` ".
      		"WHERE 1=1 $tail ".
      		"ORDER BY eventidx ".
    		"DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;	
    write_log($sql);
    $messagelist = $db_main2->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) FROM `tbl_push_event`".
      		"WHERE 1=1 $tail";
    $totalcount = $db_main2->getvalue($sql);    
    
    $db_main2->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script type="text/javascript">  
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    } 
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; PUSH Event 관리 (<?= make_price_format($totalcount) ?>)</div>
            </div>
            <!-- //title_warp -->
            
            <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
                <div class="detail_search_wrap">
                    <span class="search_lbl ml20">Message</span>
                    <input type="text" class="search_text" id="search_message" name="search_message" style="width:250px" value="<?= encode_html_attribute($search_message) ?>" onkeypress="search_press(event)" />
                    
                    <span class="search_lbl ml20">PUSH 여부</span>
                    <select name="search_sent" id="search_sent">
                    	<option value="" <?= ($search_sent=="") ? "selected" : "" ?>>전체</option>
						<option value="0" <?= ($search_sent=="0") ? "selected" : "" ?>>미전송</option>
						<option value="1" <?= ($search_sent=="1") ? "selected" : "" ?>>전송</option>
					</select>	
                  	<span class="search_lbl ml20">플랫폼 선택</span>
                    <select id="os_type" name="os_type">
                      <option value="">선택</option>
                      <option value="0" <?= ($os_type == 0) ? "selected" : "" ?>>공통</option>
                      <option value="1" <?= ($os_type == 1) ? "selected" : "" ?>>IOS</option>
                      <option value="2" <?= ($os_type == 2) ? "selected" : "" ?>>Android</option>
                      <option value="3" <?= ($os_type == 3) ? "selected" : "" ?>>Amazon</option>
                     </select>
                    <span class="search_lbl ml20">Event Reward</span>
                    <select id="reward_type" name="reward_type">
                      <option value="">선택</option>
                      <option value="1" <?= ($reward_type == 1) ? "selected" : "" ?>>Lucky Wheel</option>
                      <option value="2" <?= ($reward_type == 2) ? "selected" : "" ?>>Free Coin</option>
                      <option value="3" <?= ($reward_type == 3) ? "selected" : "" ?>>Give a Way</option>
					  <option value="10" <?= ($reward_type == 10) ? "selected" : "" ?>>Scratch Card</option>
					  <option value="11" <?= ($reward_type == 11) ? "selected" : "" ?>>Slot of Fortune</option>
					  <option value="12" <?= ($reward_type == 12) ? "selected" : "" ?>>Golden Egg</option>							
					  <option value="14" <?= ($reward_type == 14) ? "selected" : "" ?>>Free Spins</option>
                     </select>						
					<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
                </div>
            </form>
                        
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
            </colgroup>
            <thead>
                <tr>
                    <th>번호</th>
                    <th>Platform</th>
                    <th class="tbl">Message</th>                    
                    <th>리워드 종류</th>
                    <th>리워드 금액</th>  
					<th>전송 상태</th>  
                    <th>예약 게시 여부</th>
                    <th>예약게시일</th>
                    <th class="tbc">총<br>참여인원</th>
                    <th class="tdr">리워드지급</th>                    
                </tr>
            </thead>
            <tbody>
<?
    for ($i=0; $i<sizeof($messagelist); $i++)
    {
        $eventidx = $messagelist[$i]["eventidx"];
        $platform = $messagelist[$i]["os_type"];
        $message = $messagelist[$i]["message"];        
        $reward_type = $messagelist[$i]["reward_type"];
        $reward_amount = $messagelist[$i]["reward_amount"];
        $total_amount = $messagelist[$i]["total_amount"];
        $total_user = $messagelist[$i]["total_user"];        
        $sent = $messagelist[$i]["sent"];
        $enabled = $messagelist[$i]["enabled"];
        $reservedate = $messagelist[$i]["reservedate"];    

        if($platform == 0)
        	$platform_str = "ALL";
        else if($platform == 1)
        	$platform_str = "IOS";
        else if($platform == 2)
        	$platform_str = "Android";
        else if($platform == 3)
        	$platform_str = "Amazon";
        
        if($reward_type == 1)
        	$reward_str = "Lucky Wheel";
        else if($reward_type == 2)
        	$reward_str = "Free Coin";
        else if($reward_type == 3)
        	$reward_str = "Give A Way";
       	else if($reward_type == 10)
       		$reward_str = "Scratch Card";
       	else if($reward_type == 11)
       		$reward_str = "Slot of Fortune";
       	else if($reward_type == 12)
       		$reward_str = "Golden Egg";
		else if($reward_type == 14)
       		$reward_str = "Free Spins";
        
?>
            <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="view_mobile_push_event_dtl(<?= $eventidx ?>)">
                <td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
                <td class="tdc"><?= $platform_str ?></td>
                <td class="tbl point_title"><?= $message ?></td>
                <td class="tdc"><?= $reward_str ?></td>
                <td class="tdc"><?= $reward_amount ?></td>
                <td class="tdc"><?= ($sent == "1") ? "전송" : "미전송" ?></td>
                <td class="tdc"><?= ($enabled == "1" || $enabled == "2" || $enabled == "3") ? "예약 게시" : "-" ?></td>
                <td class="tdc"><?= $reservedate ?></td>
                <td class="tdc"><?= $total_user ?></td>  
                <td class="tdc"><?= $total_amount ?></td>                  
            </tr>
<?
    } 
?>
            </tbody>
            </table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>

            <div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="PUSH EVENT 추가" onclick="window.location.href='mobile_push_event_write.php'">           
            </div>            
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
