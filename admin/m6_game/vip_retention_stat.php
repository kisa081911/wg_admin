<?
    $top_menu = "game";
    $sub_menu = "vip_retention_stat";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    
    $listcount = 10;
    $pagename = "vip_retention_stat.php";

    
    $startdate = ($_GET["startdate"] == "") ? date("Y-m-d", mktime(0,0,0,date("m"),date("d")-100,date("Y"))) : $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
    
    $pagefield = "startdate=$startdate&enddate=$enddate";
    
    $db_main2 = new CDatabase_Main2();
    $db_mobile = new CDatabase_Mobile();
    
    $sql = "SELECT count(*) FROM tbl_individual_push WHERE STATUS = 1 GROUP BY DATE_FORMAT(senddate,'%Y-%m-%d')";
    $totalcount = $db_mobile->getvalue($sql);
    
    $sql = " SELECT DATE_FORMAT(senddate,'%Y-%m-%d') AS senddate ,COUNT(useridx) AS sendcount FROM tbl_individual_push". 
           " WHERE STATUS = 1 AND DATE_FORMAT(senddate,'%Y-%m-%d') BETWEEN '$startdate' AND '$enddate' GROUP BY DATE_FORMAT(senddate,'%Y-%m-%d') ORDER BY  DATE_FORMAT(senddate,'%Y-%m-%d') DESC";
    $eventlist = $db_mobile->gettotallist($sql);
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">    
    function go_result(eventidx)
    {
        window.location.href = "event_result.php?eventidx="+eventidx;
    }
    $(function() {
        $("#startdate").datepicker({ });
    });

    $(function() {
        $("#enddate").datepicker({ });
    });

    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; VIP 유저 이탈 관리 복귀자 통계</div>
            </div>
            <!-- //title_warp -->
            
            <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
            	<input type=hidden name="fanpage" value="<?= $fanpage ?>">
				<input type="input" class="search_text" id="startdate" name="startdate" style="width:65px;margin-left: 859px;" value="<?= $startdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
				<input type="input" class="search_text" id="enddate" name="enddate" style="width:65px" value="<?= $enddate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
				<input type="button" class="btn_search" value="검색" onclick="search()" />
				<br/><br/><br/>
            </form>
            
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="100">
                <col width="100">
                <col width="100">
                <col width="">
                <col width="100">
                <col width="100">
                <col width="100">
                <col width="100">
                <col width="100">
                <col width="100">
            </colgroup>
            <thead>
                <tr>
                    <th class="tbl" rowspan="2">발송일</th>
                    <th class="tbl" rowspan="2">총 발송 수</th>
                    <th class="tbl" rowspan="2">총 복귀 수</th>
                    <th class="tbc" colspan = "3">복귀 일별 회원 수</th>
                    <th class="tbc" rowspan="2">비결제자</th>
                    <th class="tbc" rowspan="2">결제자</th>
                    <th class="tbc" rowspan="2">복귀 후 누적<br>결제금액</th>
                    <th class="tbc" rowspan="2">총 결제 횟수</th>
                    <th class="tdr" rowspan="2">평균결제금액</th>
                </tr>
                 <tr>
	                <th class="tdc">7일 미만 </th>
	                <th class="tdc">7일~28일미만 </th>
	                <th class="tdc">28일이상 </th>
	               
	            </tr>
            </thead>
            <tbody>
<?
    for ($i=0; $i<sizeof($eventlist); $i++)
    {
        $senddate = $eventlist[$i]["senddate"];
        $sendcount = $eventlist[$i]["sendcount"];
        
        $sql = " SELECT IFNULL(SUM(usercount),0) AS usercount ".
                        " , IFNULL(SUM(return7count),0) AS return7count ".
                        " , IFNULL(SUM(return720count),0) AS return720count ".
                        " , IFNULL(SUM(return20count),0) AS return20count ".
                        " , IFNULL(SUM(payer),0) AS payer, IFNULL(SUM(nopayer),0) AS nopayer ".
                        " ,IFNULL(SUM(buycredit),0) AS buycredit ".
                        " ,IFNULL(SUM(buycount),0) AS buycount ".
                " FROM `tbl_individual_stat` WHERE senddate = '$senddate'";
        $push_arr = $db_main2-> getarray($sql);
        
        $usercount = $push_arr["usercount"];
        $return7count = $push_arr["return7count"];
        $return720count = $push_arr["return720count"];
        $return20count = $push_arr["return20count"];
        $payer= $push_arr["payer"];
        $nopayer = $push_arr["nopayer"];
        $buycredit = $push_arr["buycredit"];
        $buycount = $push_arr["buycount"];
        $avg_credit = ($buycount == 0 )? 0 : round($buycredit/$buycount,2);
?>
            <tr onmouseover="className='tr_over'" onmouseout="className=''">
                <td class="tbc point_title"><?= $senddate ?></td>
                <td class="tdc"><?= number_format($sendcount) ?></td> 
                <td class="tdc"><?= number_format($usercount) ?></td> 
                <td class="tdc point"><?= number_format($return7count) ?></td>
                <td class="tdc point"><?= number_format($return720count) ?></td>
                <td class="tdc point"><?= number_format($return20count) ?></td>
                <td class="tdc"><?= number_format($nopayer) ?></td>
                <td class="tdc"><?= number_format($payer) ?></td>
                <td class="tdc">$<?= number_format($buycredit) ?></td>
                <td class="tdr point"><?= number_format($buycount) ?></td>
                <td class="tdr point">$<?= number_format($avg_credit) ?></td>
            </tr>
<?
    } 
?>
            </tbody>
            </table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>           
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    $db_main2->end();
    $db_mobile->end();
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
