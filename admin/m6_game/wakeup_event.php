<?
    $top_menu = "game";
    $sub_menu = "wakeup_event";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $db_main2 = new CDatabase_Main2();
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    
    $listcount = 10;
    $pagename = "wakeup_event.php";
    
    $tail = "WHERE 1=1 ";
    
    $sql =" SELECT * FROM tbl_wakeup_setting ORDER BY wakeupidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    $eventlist = $db_main2->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) FROM tbl_wakeup_setting $tail";
    $totalcount = $db_main2->getvalue($sql);
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
    	$page = floor(($totalcount + $listcount - 1) / $listcount);
    
    $db_main2->end();
?>
<script type="text/javascript">
function search_press(e)
{
    if (((e.which) ? e.which : e.keyCode) == 13)
    {
        search();
    }
}

function go_result(eventidx)
{
    window.location.href = "wakeup_event_write.php?eventidx="+eventidx;
} 

</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
    
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Wake Up 이벤트 관리 (<?= make_price_format($totalcount) ?>)</div>
	</div>
	<!-- //title_warp -->
        
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="120">
			<col width="90">
			<col width="80">
		</colgroup>
		<thead>
			<tr>
				<th>번호</th>
				<th>이벤트기간</th>
				<th>t상태</th>
			</tr>
		</thead>
		<tbody>
<?
for ($i=0; $i<sizeof($eventlist); $i++)
{
    $wakeupidx = $eventlist[$i]["wakeupidx"];
    $status = $eventlist[$i]["status"];
    $startdate = $eventlist[$i]["startdate"];
    $enddate= $eventlist[$i]["enddate"];
    
?>
        <tr onmouseover="className='tr_over'" onmouseout="className=''" >
            <td class="tdc"><?= $wakeupidx ?></td>
            <td class="tdc"><?= $start_eventdate ?> ~ <?= $end_eventdate ?></td> 
            <td class="tbl point_title"><?= $status ?></td>
            <td class="tdc"><input type="button" class="btn_03" value="수정" style="cursor:pointer" onclick="event.cancelBubble=true;go_result(<?= $eventidx ?>)" /></td>
        </tr>
<?
} 
?>
	</tbody>
</table>
<?
include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>

<div class="button_warp tdr">
	<input type="button" class="btn_setting_01" value="이벤트 추가" onclick="window.location.href='wakeup_event_write.php'">           
</div>            
</div>
<!--  //CONTENTS WRAP -->
    
<div class="clear"></div>
<?
include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
