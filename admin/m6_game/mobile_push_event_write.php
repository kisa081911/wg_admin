<?
    $top_menu = "game";
    $sub_menu = "mobile_push_event_write";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $eventidx = $_GET["eventidx"];
    
    check_number($eventidx);
    
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    
    if ($eventidx != "")
    {
        $sql = "SELECT eventidx, os_type, title, message, reward_type, reward_amount, limit_join, image_url, image_url2,image_url_short,text_visible, push_type, push_code, sent, enabled, start_eventdate, end_eventdate, reservedate ".
            	"FROM tbl_push_event WHERE eventidx=$eventidx";
        $event = $db_main2->getarray($sql);
        
        $eventidx = $event["eventidx"];
        $os_type = $event["os_type"];
        $message = $event["message"];
        $title = $event["title"];
        $reward_type = $event["reward_type"];
        $reward_amount = $event["reward_amount"];
        $limit_join = $event["limit_join"];
        $image_url = $event["image_url"];
        $image_url2 = $event["image_url2"];


        $push_type = $event["push_type"];       
        $push_code = $event["push_code"];
        $sent = $event["sent"];
        $enabled = $event["enabled"];
        $image_url_short = $event["image_url_short"];
        $text_visible = $event["text_visible"];
        $reservation_date = substr($event["reservedate"],0,10);        
        $reservation_hour = substr($event["reservedate"],11,2);
        $reservation_minute = substr($event["reservedate"],14,2);
        
        $start_eventdate = substr($event["start_eventdate"],0,10);        
        $start_eventdate_hour = substr($event["start_eventdate"],11,2);
        $start_eventdate_minute = substr($event["start_eventdate"],14,2);
        
        $end_eventdate = substr($event["end_eventdate"],0,10);        
        $end_eventdate_hour = substr($event["end_eventdate"],11,2);
        $end_eventdate_minute = substr($event["end_eventdate"],14,2); 
        
        if($reward_type == 14)
        {
        	$sql="SELECT * FROM tbl_ultra_freespin_event WHERE eventidx = $eventidx";
        	$freespin_arr=$db_main2->getarray($sql);
        	
        	$slot_type =$freespin_arr["slot_type"];
        	$totalbet  =$freespin_arr["totalbet"];
        	$freespin_cnt =$freespin_arr["freespin_cnt"];
        }
		
        
        if ($eventidx == "")
            error_back("삭제되었거나 등록되지 않은 이벤트입니다.");
    }  

    $sql = "SELECT slottype,slotname FROM tbl_slot_list";
    $totalslotlist = $db_main2->gettotallist($sql);
    
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript">
function save_push()
{
    var input_form = document.input_form;

    if (input_form.os_type.value == "" )
    {
        alert("OS를 선택해주세요.");
        input_form.os_type.focus();
        return;
    }

    if (input_form.reward_type.value == "" )
    {
        alert("Reward Type를 선택해주세요.");
        input_form.reward_type.focus();
        return;
    }

    if (input_form.start_eventdate.value == "")
    {
        alert("이벤트 기간을 입력하세요.");
        input_form.start_eventdate.focus();
        return;
    }
    
    if (input_form.end_eventdate.value == "")
    {
        alert("이벤트 기간을 입력하세요.");
        input_form.end_eventdate.focus();
        return;
    }

    if (input_form.reservation_date.value == "")
    {
        alert("예약게시 시간을 선택해주세요.");
        input_form.reservation_date.focus();
        return;
    }
    
    if (input_form.message.value == "" )
    {
        alert("Message를 입력해주세요.");
        input_form.message.focus();
        return;
    }

    if (input_form.push_type.value == "" )
    {
        alert("Push Type를 선택해주세요.");
        input_form.push_type.focus();
        return;
    }   
  
    if(input_form.push_code.value == "")
    {
        alert("Push 코드를 입력해주세요.");
        input_form.push_code.focus();
        return;
    }

    if (input_form.reward_type.value == "4" && input_form.slotbetcoin.value == "")
    {
        alert("토탈벳 금액을 입력하세요.");
        input_form.slotbetcoin.focus();
        return;
    }

    if(input_form.reward_type.value == "4" && input_form.slotbetcoin.value > 2000)
    {
		if(confirm("토탈벳 금액이 2000 이상입니다. 저장하시겠습니까?") == 0)
			return;
    }

    if (input_form.reward_type.value == "4" && input_form.freespincnt.value == "")
    {
        alert("슬롯 프리스핀 수를 입력하세요.");
        input_form.freespincnt.focus();
        return;
    }

    if (input_form.text_visible.value == "" )
    {
        alert("Text Visible를 선택해주세요.");
        input_form.text_visible.focus();
        return;
    }
  
    var param = {};
    param.eventidx  = "<?= $eventidx ?>";
    param.reservation_date = input_form.reservation_date.value;
    param.reservation_hour = input_form.reservation_hour.value;
    param.reservation_minute = input_form.reservation_minute.value;
           

    param.start_eventdate_hour = input_form.start_eventdate_hour.value;
    param.start_eventdate_minute = input_form.start_eventdate_minute.value;
           

    param.end_eventdate_hour = input_form.end_eventdate_hour.value;
    param.end_eventdate_minute = input_form.end_eventdate_minute.value;
           
    param.reward_type = input_form.reward_type.value;
    param.reward_amount = input_form.reward_amount.value;
    param.limit_join = input_form.limit_join.value;
    param.message = input_form.message.value;
    param.title = input_form.title.value;
    param.reservation_enabled = ((input_form.enabled.checked) ? "1" : "0");
    param.push_code = input_form.push_code.value;
    param.push_type = input_form.push_type.value;
    param.os_type = input_form.os_type.value;
    param.image_url = input_form.image_url.value;
    param.image_url2 = input_form.image_url2.value;
    param.start_eventdate = input_form.start_eventdate.value;
    param.end_eventdate = input_form.end_eventdate.value;

    param.slotidx = input_form.slot_type.value;
    param.slotbetcoin = input_form.slotbetcoin.value;
    param.freespincnt = input_form.freespincnt.value;

    param.image_url_short = input_form.image_url_short.value;
    param.text_visible = input_form.text_visible.value;
    

    if ("<?= $eventidx ?>" == "")
    	WG_ajax_execute("game/insert_mobile_push_event", param, save_push_callback);
    else
    	WG_ajax_execute("game/update_mobile_push_event", param, save_push_callback);
}

function save_push_callback(result, reason)
{
    if (!result)
    {
        alert("오류 발생 - " + reason);
    }
    else
    {
        window.location.href = "mobile_push_event.php";
    }
}

function delete_push()
{
    if (!confirm("해당  push event 를 삭제 하시겠습니까?"))
        return;

    var param = {};

    param.eventidx =  "<?= $eventidx ?>";

    WG_ajax_execute("game/delete_mobile_push_event", param, delete_push_callback);
}

function delete_push_callback(result, reason)
{
    if (!result)
    {
        alert("오류 발생 - " + reason);
    } 
    else
    {
        window.location.href = "mobile_push_event.php";
    }
}

function get_event_info(category)
{
	if(category == "1")
    {
		$("#row_reward_amount").hide();
		$("#row_limit_join").hide();
		$("#row_slotbetcoin").hide();
  		$("#row_freespincnt").hide();
  		$("#row_slot_list").hide();
    }
	else if(category == "2")
	{
		$("#row_reward_amount").show();
		$("#row_limit_join").hide();
		$("#row_slotbetcoin").hide();
  		$("#row_freespincnt").hide();
  		$("#row_slot_list").hide();
	}
	else if(category == "3")
	{
		$("#row_reward_amount").show();
		$("#row_limit_join").show();
		$("#row_slotbetcoin").hide();
  		$("#row_freespincnt").hide();
  		$("#row_slot_list").hide();
	}
	else if(category == "14")
  	 {
		$("#row_reward_amount").hide();
		$("#row_limit_join").hide();
  		$("#row_slotbetcoin").show();
  		$("#row_freespincnt").show();
  		$("#row_slot_list").show();
  	}
	else if(category == "10" || category == "11" || category == "12")
  	{
		$("#row_reward_amount").show();
		$("#row_limit_join").hide();
		$("#row_slotbetcoin").hide();
  		$("#row_freespincnt").hide();
  		$("#row_slot_list").hide();
  	}
}

$(function() {
    $("#reservation_date").datepicker({ });
    $("#start_eventdate").datepicker({ });
    $("#end_eventdate").datepicker({ });
}); 

</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; PUSH Event 관리</div>
<?
    if ($eventidx != "")
    { 
?>
            <div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_push()"></div>
<?
    } 
?>
            </div>
            <!-- //title_warp -->
            
            <form name="input_form" id="input_form" onsubmit="return false">
            
            <div class="h2_title pt20">[모바일 PUSH 설정]</div>
            <table class="tbl_view_basic">
                <colgroup>
                    <col width="180">
                    <col width="">
                </colgroup>
                <tbody>
                    <tr>
                  <th>플랫폼 선택</th>
                  <td>
                    <select id="os_type" name="os_type">
                      <option value="">선택</option>
                      <option value="0" <?= ($os_type == 0) ? "selected" : "" ?>>공통</option>
                      <option value="1" <?= ($os_type == 1) ? "selected" : "" ?>>IOS</option>
                      <option value="2" <?= ($os_type == 2) ? "selected" : "" ?>>Android</option>
                      <option value="3" <?= ($os_type == 3) ? "selected" : "" ?>>Amazon</option>
                      
                  </td>
                </tr>
                <tr>
					<th>이벤트 종류</th>
					<td>
						<select id="reward_type" name="reward_type" onchange="get_event_info(this.value);">
							<option value="">선택</option>
							<option value="1" <?= ($reward_type=="1") ? "selected" : "" ?>>Lucky Wheel</option>
							<option value="2" <?= ($reward_type=="2") ? "selected" : "" ?>>Free Coin</option>
							<option value="3" <?= ($reward_type=="3") ? "selected" : "" ?>>Give A Way</option>
							<option value="10" <?= ($reward_type=="10") ? "selected" : "" ?>>Scratch Card</option>
							<option value="11" <?= ($reward_type=="11") ? "selected" : "" ?>>Slot of Fortune</option>
							<option value="12" <?= ($reward_type=="12") ? "selected" : "" ?>>Golden Egg</option>							
							<option value="14" <?= ($reward_type=="14") ? "selected" : "" ?>>Free Spins</option>
						</select>
					</td>
				</tr>
				 <tr id="row_reward_amount" style="display:<?= ($reward_type == "2" || $reward_type == "3"|| $reward_type == "10"|| $reward_type == "11"|| $reward_type == "12") ? "" : "none;"?>">
					<th>Reward Amount</th>
                        <td><input type="text" class="view_tbl_text" name="reward_amount" id="reward_amount" style="width:200px" value="<?= $reward_amount ?>" onkeypress="return checkonlynum()" /></td>
				</tr>
				<tr id="row_limit_join" style="display:<?= ($reward_type == "3") ? "" : "none;"?>">
                        <th>Limit Join</th>
                        <td><input type="text" class="view_tbl_text" name="limit_join" id="limit_join" style="width:200px" value="<?= ($reward_type == "3") ? $limit_join : "" ?>" onkeypress="return checkonlynum()" /></td>
				</tr>
				<tr>
					<th>이벤트기간</th>
					<td>
						<input type="input" class="search_text" id="start_eventdate" name="start_eventdate" style="width:65px" value="<?= $start_eventdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
						   <select id="start_eventdate_hour" name="start_eventdate_hour">
                                <option value="">선택</option>
<?
    for ($i=0;$i<24;$i++)
    {
?>
                                <option value="<?= $i ?>" <?= ($i == $start_eventdate_hour) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
                            </select>시&nbsp;
                            <select id="start_eventdate_minute" name="start_eventdate_minute">
                                <option value="">선택</option>
<?
    for ($i=0;$i<60;$i++)
    {
?>
                                <option value="<?= $i ?>" <?= ($i ==  $start_eventdate_minute) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
                            </select>분
						-
						<input type="input" class="search_text" id="end_eventdate" name="end_eventdate" style="width:65px" value="<?= $end_eventdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
					   <select id="end_eventdate_hour" name="end_eventdate_hour">
                                <option value="">선택</option>
<?
    for ($i=0;$i<24;$i++)
    {
?>
                                <option value="<?= $i ?>" <?= ($i == $end_eventdate_hour) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
                            </select>시&nbsp;
                            <select id="end_eventdate_minute" name="end_eventdate_minute">
                                <option value="">선택</option>
<?
    for ($i=0;$i<60;$i++)
    {
?>
                                <option value="<?= $i ?>" <?= ($i == $end_eventdate_minute) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
                            </select>분
					</td>
				</tr>
				<tr id="row_slotbetcoin" style="<?= ($reward_type == "14") ? "" : "display:none"?>">
	            	<th>토탈벳금액</th>
	            	<td>
	            		<input type="text" class="view_tbl_text" name="slotbetcoin" id="slotbetcoin" style="width:200px" value="<?= $totalbet ?>" onkeypress="return checkonlynum()" />
	            	</td>
	            </tr>
	            <tr id="row_freespincnt" style="<?= ($reward_type == "14") ? "" : "display:none"?>">
	            	<th>슬롯프리스핀횟수</th>
	            	<td>
	            		<input type="text" class="view_tbl_text" name="freespincnt" id="freespincnt" style="width:200px" value="<?= $freespin_cnt ?>" onkeypress="return checkonlynum()" />
	            	</td>
	            </tr>                    
           		<tr id="row_slot_list" style="display:<?= ($reward_type == "14") ? "" : "none;"?>">
            		<th>슬롯</th>
            		<td>
            			<select name="slot_type" id="slot_type">
							<option value="">선택하세요</option>
<?
	for ($i=0; $i<sizeof($totalslotlist); $i++)
	{
		$slottype = $totalslotlist[$i]["slottype"];
		$slotname = $totalslotlist[$i]["slotname"];
?>	
							<option value="<?= $slottype ?>" <?= ($slottype == $slot_type) ? "selected=\"true\"" : "" ?>><?= $slotname ?></option>
<?						
	}
?>						
						</select>* 슬롯 바로가기 기능 (변경 시 저장 후 short url 재발급 필요)
            		</td>
            	</tr>
            	
                    <tr>
                        <th>예약 게시 시간</th>
                        <td>
                            <input type="input" class="search_text" id="reservation_date" name="reservation_date" style="width:65px" value="<?= $reservation_date ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
                            <select id="reservation_hour" name="reservation_hour">
                                <option value="">선택</option>
<?
    for ($i=0;$i<24;$i++)
    {
?>
                                <option value="<?= $i ?>" <?= ($i == $reservation_hour) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
                            </select>시&nbsp;
                            <select id="reservation_minute" name="reservation_minute">
                                <option value="">선택</option>
<?
    for ($i=0;$i<60;$i++)
    {
?>
                                <option value="<?= $i ?>" <?= ($i == $reservation_minute) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
                            </select>분
                        </td>
                    </tr>
                     
                    <tr>
                    	<th>알림 예약 게시 여부</th>
                    	<td>
                            <input type="checkbox" name="enabled" id="enabled" value="1" <?= ($enabled == 1 || $enabled == 2 || $enabled == 3) ? "checked" : "" ?>> 게시
                        </td>
                    </tr>
                    <tr>
                        <th>Message</th>
                        <td>
                            <textarea style="width:700px;height:100px;" name="message" id="message"><?= $message ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>Title</th>
                        <td>
                            <input type="text" class="view_tbl_text" name="title" id="title" maxlength="200" value="<?= $title ?>" /><br/>
                        </td>
                    </tr>
                    <tr>
                        <th>이미지 URL</th>
                        <td>
                            <input type="text" class="view_tbl_text" name="image_url" id="image_url" maxlength="200" value="<?= $image_url ?>" /><br/>
<?
    if ($image_url != "")
    {
?>
                            <img src="<?= $image_url ?>">
<?
    } 
?>
   
                        </td>
                    </tr>
                    <tr>
                        <th>이미지 URL2</th>
                        <td>
                            <input type="text" class="view_tbl_text" name="image_url2" id="image_url2" maxlength="200" value="<?= $image_url2 ?>" /><br/>
<?
    if ($image_url2 != "")
    {
?>
                            <img src="<?= $image_url2 ?>">
<?
    } 
?>
   
                        </td>
                    </tr>
                    <tr>
                        <th>BG이미지 URL</th>
                        <td>
                            <input type="text" class="view_tbl_text" name="image_url_short" id="image_url_short" maxlength="200" value="<?= $image_url_short  ?>" /><br/>
<?
    if ($image_url_short  != "")
    {
?>
                            <img src="<?= $image_url_short  ?>">
<?
    } 
?>
   
                        </td>
                    </tr>
                    <tr>
                        <th>PUSH 코드</th>
                            <td><input type="text" class="view_tbl_text" style="width:150px;" name="push_code" id="push_code" value="<?= encode_html_attribute($push_code) ?>" />
                            </td>
                    </tr>

                    <tr>
                        <th>push 타입</th>
                            <td>
                            <select id="push_type" name="push_type" >
                                <option value="">선택</option>
                                <option value="1" <?= ($push_type=="1") ? "selected" : "" ?>>reward</option>
                            </select>
                        </td>
                    </tr>
					<tr>
                        <th>Text Visible </th>
                            <td>
                            <select id="text_visible" name="text_visible" >
                                <option value="">선택</option>
                                <option value="1" <?= ($text_visible=="1") ? "selected" : "" ?>>사용</option>
                                <option value="0" <?= ($text_visible=="0") ? "selected" : "" ?>>미사용</option>
                            </select>
                        </td>
                    </tr>

                </tbody>
            </table>
            </form>
            
            <div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="저장" onclick="save_push()">
                <input type="button" class="btn_setting_02" value="취소" onclick="go_page('mobile_push_event.php')">            
            </div>
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>