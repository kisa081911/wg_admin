<?
    $top_menu = "game";
    $sub_menu = "event";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $eventidx = $_GET["eventidx"];
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    
    $listcount = 10;
    $pagename = "event_result.php";
    $pagefield = "eventidx=$eventidx";
    
    $tail = "WHERE 1=1 ";
    
    if ($eventidx == "")
        error_back("잘못된 접근입니다.");
    
    check_number($eventidx);        
    
    $db_main2 = new CDatabase_Main2();
    
    // 이벤트 정보
    $sql = "SELECT eventidx,title,category,reward_type,reward_amount,start_eventdate,end_eventdate ".
            "FROM tbl_event WHERE eventidx='$eventidx'";
        
    $event = $db_main2->getarray($sql);
    
    $eventidx = $event["eventidx"];
    $title = $event["title"];
    $category = $event["category"];
    $reward_type = $event["reward_type"];
    $reward_amount = $event["reward_amount"];
    $start_eventdate = $event["start_eventdate"];
    $end_eventdate = $event["end_eventdate"];

    if($reward_type == "1")
    	$reward_name = number_format($reward_amount)." Coins";
    else if($reward_type == "0")
    	$reward_name = "None";
    
	// 이벤트 경과 시간별 참여 summary
    $sql = "SELECT COUNT(*) AS totalcount, IFNULL(SUM(B.reward_amount),0) AS total_amount, FLOOR((UNIX_TIMESTAMP(B.writedate)-UNIX_TIMESTAMP(A.writedate))/(60 * 60 * 3)) AS term ".
            "FROM tbl_event A JOIN tbl_event_result B ON A.eventidx=B.eventidx ".
            "WHERE A.eventidx=$eventidx ".
            "GROUP BY term";
			
	$event_timelist = $db_main2->gettotallist($sql);
	
	// 이벤트 경과 시간별 참여 summary
    $sql = "SELECT COUNT(*) AS totalcount, IFNULL(SUM(B.reward_amount),0) AS reward_amount, FLOOR((UNIX_TIMESTAMP(B.writedate)-UNIX_TIMESTAMP(A.writedate))/(60 * 30)) AS term ".
            "FROM tbl_event A JOIN tbl_event_result B ON A.eventidx=B.eventidx ".
            "WHERE A.eventidx=$eventidx AND UNIX_TIMESTAMP(B.writedate)-UNIX_TIMESTAMP(A.writedate)<60*60*3 ".
            "GROUP BY term ";
			
	$event_timelist3hr = $db_main2->gettotallist($sql);
	
	// 총 참여자
	$sql = "SELECT COUNT(*) FROM tbl_event_result WHERE eventidx=$eventidx";
	$eventcount = $db_main2->getvalue($sql);
		
	// 신규 가입자
	$sql = "SELECT COUNT(*) FROM tbl_event_result WHERE eventidx=$eventidx AND isnew=1";
	$newcount = $db_main2->getvalue($sql);

	// 신규 가입자 분석 대상 수
	$sql = "SELECT COUNT(*) FROM tbl_event_result WHERE eventidx=$eventidx AND isnew>-1";
	$newtotalcount = $db_main2->getvalue($sql);
	
	// 이탈자 복귀
	$sql = "SELECT COUNT(*) FROM tbl_event_result WHERE eventidx=$eventidx AND isreturn=1";
	$returncount = $db_main2->getvalue($sql);
	
	// 이탈자 복귀 분석 대상 수
	$sql = "SELECT COUNT(*) FROM tbl_event_result WHERE eventidx=$eventidx AND isreturn>-1";
	$returntotalcount = $db_main2->getvalue($sql);
	
	// 결제 금액
	$sql = "SELECT IFNULL(SUM(ordercredit),0) FROM tbl_event_result WHERE eventidx=$eventidx AND ordercredit>-1";
	$ordercredit = $db_main2->getvalue($sql);
	$ordercredit = number_format($ordercredit, 1);
	
	// 결제 금액 분석 대상 수
	$sql = "SELECT COUNT(*) FROM tbl_event_result WHERE eventidx=$eventidx AND ordercredit>-1";
	$ordertotalcount = $db_main2->getvalue($sql);
	
	if ($newtotalcount == 0)
		$newratio = 0;
	else
		$newratio = round($newcount * 10000 / $newtotalcount) / 100;
			
	if ($returntotalcount == 0)
		$returnratio = 0;
	else
		$returnratio = round($returncount * 10000 / $returntotalcount) / 100;
			
	if ($ordertotalcount == 0)
		$avgcredit = 0;
	else
		$avgcredit = round($ordercredit * 100 / $ordertotalcount) / 100;
	
	$db_main2->end();
?>

<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
</script>
	<!-- CONTENTS WRAP -->
	<div class="contents_wrap">
        
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 이벤트 결과</div>
		</div>
		<!-- //title_warp -->
            
		<div class="h2_title">[이벤트 정보]</div>
		<table class="tbl_view_basic">
			<colgroup>
				<col width="230">
				<col width="">
			</colgroup>
			<tbody>
				<tr>
					<th>이벤트 제목</th>
					<td><?= $title ?></td>
				</tr>
				<tr>
					<th>리워드 지급</th>
					<td><?= $reward_name  ?></td>
				</tr>
				<tr>
					<th>이벤트 기간</th>
					<td><?= $start_eventdate ?> - <?= $end_eventdate ?></td>
				</tr>                  
			</tbody>
		</table>
            
		<div class="h2_title pt20">[ 이벤트 경과 시간별 참여 리스트 ]</div>
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="300">
				<col width="300">
			</colgroup>
			<thead>
				<tr>
					<th class="tbl">경과시간</th>
					<th class="tdr">참여자수</th>
					<th class="tdr">지급된 자원</th>
				</tr>
			</thead>
		<tbody>
<?
    for ($i=0; $i<sizeof($event_timelist); $i++)
    {
        $term = $event_timelist[$i]["term"];
        $totalcount = $event_timelist[$i]["totalcount"];
        $total_amount = $event_timelist[$i]["total_amount"];
?>                  
			<tr onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="point_title"><?= $term * 3 ?> ~ <?= ($term + 1) * 3 ?>시간</td>
				<td class="tdr point"><?= number_format($totalcount) ?></td>
				<td class="tdr point"><?= number_format($total_amount) ?></td>
			</tr>
<?
    }
?>
		</tbody>
	</table>
            
	<div class="h2_title pt20">[ 이벤트 경과 시간별 참여 리스트 - 3시간내 상세]</div>
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="300">
				<col width="300">
			</colgroup>
			<thead>
				<tr>
					<th class="tbl">경과시간</th>
					<th class="tdr">참여자수</th>
					<th class="tdr">지급된 자원</th>
				</tr>
			</thead>
			<tbody>
<?
    for ($i=0; $i<sizeof($event_timelist3hr); $i++)
    {
        $term = $event_timelist3hr[$i]["term"];
        $totalcount = $event_timelist3hr[$i]["totalcount"];
        $totalcoin = $event_timelist3hr[$i]["reward_amount"];
?>                  
				<tr onmouseover="className='tr_over'" onmouseout="className=''">
					<td class="point_title"><?= $term * 30 ?> ~ <?= ($term + 1) * 30 ?>분</td>
					<td class="tdr point"><?= number_format($totalcount) ?></td>
					<td class="tdr point"><?= number_format($totalcoin) ?></td>
				</tr>

<?
    }
?>
     
			</tbody>
		</table>
            
		<div class="h2_title pt20">[ 이벤트 효과 지표 ]</div>
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="300">
				<col width="300">
			</colgroup>
			<thead>
				<tr>
					<th class="tbl">항목</th>
					<th class="tdr">지표</th>
					<th class="tdr">비율(or 평균)</th>
				</tr>
			</thead>
			<tbody>
				<tr onmouseover="className='tr_over'" onmouseout="className=''">
					<td class="point_title">총 참여자수</td>
					<td class="tdr point"><?= number_format($eventcount) ?>명</td>
					<td class="tdr point"></td>
				</tr>
				<tr onmouseover="className='tr_over'" onmouseout="className=''">
					<td class="point_title">신규 가입자수 (1시간에 한번 분석)</td>
					<td class="tdr point"><?= number_format($newcount) ?> / <?= number_format($newtotalcount) ?> 명 분석</td>
					<td class="tdr point"><?= $newratio ?>%</td>
				</tr>
				<tr onmouseover="className='tr_over'" onmouseout="className=''">
					<td class="point_title">2주 이탈 복귀자수 (1시간에 한번 분석)</td>
					<td class="tdr point"><?= number_format($returncount) ?> / <?= number_format($returntotalcount) ?> 명 분석</td>
					<td class="tdr point"><?= $returnratio ?>%</td>
				</tr>
				<tr onmouseover="className='tr_over'" onmouseout="className=''">
					<td class="point_title">참여 후 24시간 이내 결제금액 (참여 24시간 경과 후 분석)</td>
					<td class="tdr point">$ <?= number_format($ordercredit, 1) ?> / <?= number_format($ordertotalcount) ?> 명 분석</td>
					<td class="tdr point">평균 $ <?= $avgcredit ?></td>
				</tr>
			</tbody>
		</table>
                      
		<div class="button_warp tdr">
			<input type="button" class="btn_setting_02" value="목록" onclick="go_page('event.php')">            
		</div>
	</div>
	<!--  //CONTENTS WRAP -->
        
	<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>