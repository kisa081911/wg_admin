<?
    $top_menu = "game";
    $sub_menu = "and_push";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $mesgidx = $_GET["mesgidx"];
    
    check_number($mesgidx);

    $db_mobile = new CDatabase_Mobile();
    
    if ($mesgidx != "")
    {
        $sql = "SELECT * FROM tbl_message WHERE mesgidx=$mesgidx";        
        $message = $db_mobile->getarray($sql);
        
        $mesgidx = $message["mesgidx"];
        $reservation_date = substr($message["reservedate"],0,10); 
        $reservation_hour = substr($message["reservedate"],11,2);
        $reservation_minute = substr($message["reservedate"],14,2);
        $sent = $message["sent"];
        $msg = $message["message"];
        $title = $message["title"];
        $enabled = $message["enabled"];
        $eventcode = $message["push_code"];
        $pushtype = $message["push_type"];
        $os_type = $message["os_type"];
        $image_url = $message["image_url"];
        $image_url2 = $message["image_url2"];
        $image_url_short = $message["image_url_short"];
        $text_visible = $message["text_visible"];
        		
        if ($mesgidx == "")
            error_back("삭제되었거나 등록되지 않은 push입니다.");
    }   
   
    $db_mobile->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript">
    function save_push()
    {
        var input_form = document.input_form;

        if (input_form.reservation_date.value == "")
        {
            alert("예약게시 시간을 선택해주세요.");
            input_form.reservation_date.focus();
            return;
        }
        
        if (input_form.message.value == "" )
        {
            alert("Message를 입력해주세요.");
            input_form.message.focus();
            return;
        }

        if (input_form.pushtype.value == "" )
        {
            alert("Push Type를 선택해주세요.");
            input_form.pushtype.focus();
            return;
        }
        
        if (input_form.text_visible.value == "" )
        {
            alert("Text Visible를 선택해주세요.");
            input_form.text_visible.focus();
            return;
        }
      
        if(input_form.pushtype.value == "10"){
          if(input_form.eventcode.value == ""){
            alert("이벤트 코드를 입력해주세요.");
            input_form.eventcode.focus();
            return;
          }
        }
      
        var param = {};
        param.mesgidx = "<?= $mesgidx ?>";
        param.reservation_date = input_form.reservation_date.value;
        param.reservation_hour = input_form.reservation_hour.value;
        param.reservation_minute = input_form.reservation_minute.value;       
        param.message = input_form.message.value;
        param.title = input_form.title.value;
        param.reservation_enabled = ((input_form.enabled.checked) ? "1" : "0");
        param.eventcode = input_form.eventcode.value;
        param.pushtype = input_form.pushtype.value;
        param.os_type = input_form.os_type.value;
        param.image_url = input_form.image_url.value;
        param.image_url2 = input_form.image_url2.value;
        param.image_url_short = input_form.image_url_short.value;
        param.text_visible = input_form.text_visible.value;

        if ("<?= $mesgidx ?>" == "")
        	WG_ajax_execute("game/insert_and_push", param, save_push_callback);
        else
        	WG_ajax_execute("game/update_and_push", param, save_push_callback);
    }

    function save_push_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            window.location.href = "and_push.php";
        }
    }
    
    function delete_push()
    {
        if (!confirm("해당 push를 삭제 하시겠습니까?"))
            return;

        var param = {};

        param.mesgidx =  "<?= $mesgidx ?>";

        WG_ajax_execute("game/delete_and_push", param, delete_push_callback);
    }

    function delete_push_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.href = "and_push.php";
        }
    }

    $(function() {
        $("#reservation_date").datepicker({ });
        $("#startdate_date").datepicker({ });
        $("#enddate_date").datepicker({ });
    });    
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; PUSH 관리</div>
<?
    if ($mesgidx != "")
    { 
?>
            <div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_push()"></div>
<?
    } 
?>
            </div>
            <!-- //title_warp -->
            
            <form name="input_form" id="input_form" onsubmit="return false">
            
            <div class="h2_title pt20">[모바일 PUSH 설정]</div>
            <table class="tbl_view_basic">
                <colgroup>
                    <col width="180">
                    <col width="">
                </colgroup>
                <tbody>
                    <tr>
                  <th>플랫폼 선택</th>
                  <td>
                    <select id="os_type" name="os_type">
                      <option value="">선택</option>
                      <option value="0" <?= ($os_type == 0) ? "selected" : "" ?>>공통</option>
                      <option value="1" <?= ($os_type == 1) ? "selected" : "" ?>>IOS</option>
                      <option value="2" <?= ($os_type == 2) ? "selected" : "" ?>>Android</option>
                      <option value="3" <?= ($os_type == 3) ? "selected" : "" ?>>Amazon</option>
                      
                  </td>
                </tr>
                    <tr>
                        <th>예약 게시 시간</th>
                        <td>
                            <input type="input" class="search_text" id="reservation_date" name="reservation_date" style="width:65px" value="<?= $reservation_date ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
                            <select id="reservation_hour" name="reservation_hour">
                                <option value="">선택</option>
<?
    for ($i=0;$i<24;$i++)
    {
?>
                                <option value="<?= $i ?>" <?= ($i == $reservation_hour) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
                            </select>시&nbsp;
                            <select id="reservation_minute" name="reservation_minute">
                                <option value="">선택</option>
<?
    for ($i=0;$i<60;$i++)
    {
?>
                                <option value="<?= $i ?>" <?= ($i == $reservation_minute) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
                            </select>분
                        </td>
                    </tr>
                     
                    <tr>
                    	<th>알림 예약 게시 여부</th>
                    	<td>
                            <input type="checkbox" name="enabled" id="enabled" value="1" <?= ($enabled == 1) ? "checked" : "" ?>> 게시
                        </td>
                    </tr>
                    <tr>
                        <th>Message</th>
                        <td>
                            <textarea style="width:700px;height:100px;" name="message" id="message"><?= $msg ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>Title</th>
                        <td>
                            <input type="text" class="view_tbl_text" name="title" id="title" maxlength="200" value="<?= $title ?>" /><br/>
                        </td>
                    </tr>
                    <tr>
                        <th>이미지 URL</th>
                        <td>
                            <input type="text" class="view_tbl_text" name="image_url" id="image_url" maxlength="200" value="<?= $image_url ?>" /><br/>
<?
    if ($image_url != "")
    {
?>
                            <img src="<?= $image_url ?>">
<?
    } 
?>
   
                        </td>
                    </tr>
                    <tr>
                        <th>이미지 URL2</th>
                        <td>
                            <input type="text" class="view_tbl_text" name="image_url2" id="image_url2" maxlength="200" value="<?= $image_url2 ?>" /><br/>
<?
    if ($image_url2 != "")
    {
?>
                            <img src="<?= $image_url2 ?>">
<?
    } 
?>
   
                        </td>
                    </tr>
                    <tr>
                        <th>BG이미지 URL</th>
                        <td>
                            <input type="text" class="view_tbl_text" name="image_url_short" id="image_url_short" maxlength="200" value="<?= $image_url_short  ?>" /><br/>
<?
    if ($image_url_short  != "")
    {
?>
                            <img src="<?= $image_url_short  ?>">
<?
    } 
?>
   
                        </td>
                    </tr>

                    <tr>
                        <th>PUSH 코드</th>
                            <td><input type="text" class="view_tbl_text" style="width:150px;" name="eventcode" id="eventcode" value="<?= encode_html_attribute($eventcode) ?>" />
                            </td>
                    </tr>

                    <tr>
                        <th>push 타입</th>
                            <td>
                            <select id="pushtype" name="pushtype" >
                                <option value="">선택</option>
                                <option value="0" <?= ($pushtype=="0") ? "selected" : "" ?>>message</option>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <th>Text Visible </th>
                            <td>
                            <select id="text_visible" name="text_visible" >
                                <option value="">선택</option>
                                <option value="1" <?= ($text_visible=="1") ? "selected" : "" ?>>사용</option>
                                <option value="0" <?= ($text_visible=="0") ? "selected" : "" ?>>미사용</option>
                            </select>
                        </td>
                    </tr>

                </tbody>
            </table>
            </form>
            
            <div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="저장" onclick="save_push()">
                <input type="button" class="btn_setting_02" value="취소" onclick="go_page('and_push.php')">            
            </div>
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>