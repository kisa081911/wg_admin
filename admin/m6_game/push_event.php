<?
    $top_menu = "game";
    $sub_menu = "push_event";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_reward_type = ($_GET["reward_type"] == "") ? "" : $_GET["reward_type"];
        
    $listcount = 10;
    $pagename = "push_event.php";
    $pagefield = "reward_type=$search_reward_type";
    
    $tail = "WHERE 1=1 ";
    		
	if ($search_reward_type != "")
		$tail .= " AND reward_type=$search_reward_type ";

	$db_main2 = new CDatabase_Main2();
    
	$sql = "SELECT * 
			FROM tbl_push_event $tail ORDER BY eventidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
	$push_event_list = $db_main2->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) FROM tbl_push_event $tail";
    $totalcount = $db_main2->getvalue($sql);
    
    $db_main2->end();
?>

<script type="text/javascript">  
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    
    function go_result(eventidx)
    {
        window.location.href = "event_result.php?eventidx="+eventidx;
    }
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 모바일 Push 이벤트 관리 (<?= make_price_format($totalcount) ?>)</div>
            </div>
            <!-- //title_warp -->
            
            <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
            	<input type=hidden name="fanpage" value="<?= $fanpage ?>">
                <div class="detail_search_wrap">
                	<span class="search_lbl">이벤트 종류</span>
                	<select id="reward_type" name="reward_type">
                        <option value="">전체</option>
                        <option value="1" <?= ($search_reward_type=="1") ? "selected" : "" ?>>럭키휠</option>
                        <option value="2" <?= ($search_reward_type=="2") ? "selected" : "" ?>>프리코인</option>
                        <option value="3" <?= ($search_reward_type=="3") ? "selected" : "" ?>>Giveaway</option>
                    </select>&nbsp;
                    <div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
                </div>
            </form>
            
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="120">
                <col width="">
                <col width="100">
                <col width="">
                <col width="280">
            </colgroup>
            <thead>
                <tr>
                    <th>번호</th>
                    <th class="tbl">이벤트 코드</th>
                    <th class="tbc">종류</th>
                    <th class="tdr">발행코인</th>
                    <th>이벤트기간</th>
                </tr>
            </thead>
            <tbody>
<?
    for ($i=0; $i<sizeof($push_event_list); $i++)
    {
        $eventidx = $push_event_list[$i]["eventidx"];
        $title = $push_event_list[$i]["push_code"];
        $search_reward_type = $push_event_list[$i]["reward_type"];
        $coin = $push_event_list[$i]["reward_amount"];
        $start_eventdate = $push_event_list[$i]["start_eventdate"];
        $end_eventdate = $push_event_list[$i]["end_eventdate"];
        
        if($search_reward_type == 1)
        	$search_reward_type = "럭키휠";
        else if($search_reward_type == 2)
        	$search_reward_type = "프리코인";
        else if($search_reward_type == 3)
        	$search_reward_type = "Giveaway";
?>
            <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="view_push_event_dtl(<?= $eventidx ?>)">
                <td class="tdc"><?= $eventidx ?></td>
                <td class="tbl point_title"><?= $title ?></td>
                <td class="tbl point_title"><?= $search_reward_type ?></td>
                <td class="tdr point"><?= make_price_format($coin) ?></td>
                <td class="tdc"><?= $start_eventdate ?> ~ <?= $end_eventdate ?></td> 
            </tr>
<?
    } 
?>
            </tbody>
            </table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>

            <div class="button_warp tdr">
                <input type="button" class="btn_setting_01" value="이벤트 추가" onclick="window.location.href='push_event_write.php'">           
            </div>            
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>