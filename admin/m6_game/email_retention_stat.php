<?
	$top_menu = "game";
	$sub_menu = "email_retention_stat";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_startdate = ($_GET["startdate"] == "") ? date("Y-m-d",strtotime("-7 day")) : $_GET["startdate"];
	$search_enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"]; 
	$target_type = ($_GET["target_type"] == "") ? "target_type" : $_GET["target_type"];
	$agencyidx = ($_GET["agencyidx"] == "") ? "agencyidx" : $_GET["agencyidx"];
	
	$db_main2 = new CDatabase_Main2();
	
	$sql = "SELECT sendidx, eventidx, category, target_type,
			start_eventdate, end_eventdate, send_cnt, SUM(money) AS money, SUM(user_cnt) AS user_cnt, SUM(nopay_return_cnt) AS nopay_return_cnt, SUM(pay_return_cnt) AS pay_return_cnt,
			SUM(target_email_retention_user_cnt + target_other_retention_user_cnt + non_target_email_retention_user_cnt + non_target_other_retention_user_cnt) AS total_retention,
			SUM(target_email_active_user_cnt + target_other_active_user_cnt + non_target_email_active_user_cnt + non_target_other_active_user_cnt) AS total_active
			FROM tbl_email_retention_stat
			WHERE today BETWEEN '$search_startdate' AND '$search_enddate' AND category <> 25 AND target_type = $target_type AND agencyidx = $agencyidx and eventidx not in (281,282,283)
			GROUP BY sendidx, eventidx, category
			ORDER BY sendidx DESC, category ASC";
	
	$email_retention_stat_list = $db_main2->gettotallist($sql);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>  
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	function view_event_dtl(eventidx)
	{
		window.open("../m6_game/event_write.php?eventidx=" + eventidx, "이벤트 관리", "width=1000, height=850, toolbar=no, menubar=no, scrollbars=yes" );
	}
	
	function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    
    $(function() {
        $("#startdate").datepicker({ });
    });

    $(function() {
        $("#enddate").datepicker({ });
    });
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 이메일 리텐션 현황 </div>
		<div class="search_box">
			<select name="target_type" id="target_type">
				<option value="" <?= ($target_type == "") ? "selected" : "" ?>>결제조건선택</option>
				<option value="1" <?= ($target_type == "1") ? "selected" : "" ?>>결제자</option>
				<option value="0" <?= ($target_type == "0") ? "selected" : "" ?>>비결제자</option>
			</select>
			&nbsp;&nbsp;&nbsp;
			<select name="agencyidx" id="agencyidx">
				<option value="" <?= ($agencyidx == "") ? "selected" : "" ?>>업체선택</option>
				<option value="1" <?= ($agencyidx == "1") ? "selected" : "" ?>>SocketLabs</option>
				<option value="3" <?= ($agencyidx == "3") ? "selected" : "" ?>>SES</option>
			</select>
			&nbsp;&nbsp;&nbsp;
			날짜 : <input type="input" class="search_text" id="startdate" name="startdate" style="width:75px" value="<?= $search_startdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />~
            <input type="input" class="search_text" id="enddate" name="enddate" style="width:75px" value="<?= $search_enddate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
            <input type="button" class="btn_search" value="검색" onclick="search()" />
        </div>
	</div>
	<!-- //title_warp -->
		<div id="tab_content_1">
			<table class="tbl_list_basic1">
	            <colgroup>
	                <col width="">
	                <col width="">
	                <col width="170">
	                <col width="">
	                
	                <col width="90">
	                <col width="">
	                <col width="">
	                
	                <col width="">
	                <col width="">
	                <col width="">
	                <col width="">
	                <col width="">
	                <col width="">
	            </colgroup>
		            
	            <thead>
	            	<tr>
	            		<th>회차</th>
	            		<th>발송 대상자</th>
	            		<th>발송시간</th>
	            		<th>대상자수</th>
	            		
	            		<th class="tdr">참여자수</th>
	            		<th class="tdr">1주일 미만</th>
	            		<th class="tdr">1주일 이상</th>
	            		
	            		<th class="tdr">비용</th>
	            		<th class="tdr">CPI</th>
	            		<th class="tdr">결제금액</th>
	            		<th class="tdr">ROI</th>
	            		<th class="tdr">ARPPU</th>
	            		<th class="tdr">ARPU</th>
	            		<th>정보</th>
	            	</tr>
	            </thead>
				<tbody>
<?
		$tmp_sendidx = 0;
		
		for($i=0; $i<sizeof($email_retention_stat_list); $i++)
		{
			$eventidx = $email_retention_stat_list[$i]["eventidx"];
			
			$sendidx = $email_retention_stat_list[$i]["sendidx"];
			$category = $email_retention_stat_list[$i]["category"];
			$target_type = $email_retention_stat_list[$i]["target_type"];
			
			$user_cnt = $email_retention_stat_list[$i]["user_cnt"];
			$return_user_2week_cnt = $email_retention_stat_list[$i]["total_retention"];
			$return_user_1week_cnt = $email_retention_stat_list[$i]["total_active"];
			
			$start_eventdate = $email_retention_stat_list[$i]["start_eventdate"];
			$end_eventdate = $email_retention_stat_list[$i]["end_eventdate"];
			$send_cnt = $email_retention_stat_list[$i]["send_cnt"];
			
			$spend = $send_cnt * 0.000637;
			$cpi = ($user_cnt == 0) ? 0 : number_format($spend / $user_cnt, 2);
			$money = $email_retention_stat_list[$i]["money"];
			$roi = ($spend == 0) ? 0 : round($money / $spend * 100, 2);
			$arppu = ($pay_user_count == 0) ? 0 : round($money/$pay_user_count, 2);
			$arpu = ($user_cnt == 0) ? 0 : round($money/$user_cnt, 2);
			
			$total_user_cnt += $user_cnt;
			$total_return_user_2week_cnt += $return_user_2week_cnt;
			$total_return_user_1week_cnt += $return_user_1week_cnt;
			
			$total_send_cnt += $send_cnt;
			$total_money += $money;
			
			$total_spend += $spend;
			
			if($target_type == 0)
				$category = "1주이상 이탈 비결제자";
			else if($target_type == 1)
				$category = "1주이상 이탈 결제자";
			
			if($tmp_sendidx != $sendidx)
			{
				$sql = "SELECT COUNT(DISTINCT eventidx)
						FROM tbl_email_retention_stat
						WHERE sendidx = $sendidx AND start_eventdate BETWEEN '$search_startdate' AND '$search_enddate'  AND category <> 25";
				$rowspan = $db_main2->getvalue($sql);
?>
				<tr class onmouseover="className='tr_over'" onmouseout="className=''" style="border-top: double 1px;">
					<td class="tdc point" rowspan="<?= ($rowspan==0)?1:$rowspan ?>"><?= $sendidx ?>회</td>
<?
			}
			else
			{
?>
				<tr onmouseover="className='tr_over'" onmouseout="className=''">
<?
			}
?>
					
					<td class="tdc point" style="font-size:13px"><?= $category ?></td>
					<td class="tdc point" style="font-size:13px"><?= $start_eventdate ?> 17:00:00</td>
					<td class="tdr point"><?= number_format($send_cnt) ?></td>
					
					<td class="tdr"><?= number_format($user_cnt) ?></td>
					<td class="tdr"><?= number_format($return_user_1week_cnt) ?></td>
					<td class="tdr"><?= number_format($return_user_2week_cnt) ?></td>
					
					<td class="tdr"><?= ($spend == 0) ? "-" : "$ ".number_format($spend, 2)?></td>
					<td class="tdr"><?= ($cpi == 0) ? "-" : "$ ".number_format($cpi, 2)?></td>
					<td class="tdr"><?= ($money == 0) ? "-" : "$ ".number_format($money)?></td>
					
					<td class="tdr"><?= ($roi == 0) ? "-" : $roi." %"?></td>
					<td class="tdr"><?= ($arppu == 0) ? "-" : number_format($arppu, 2)?></td>
					<td class="tdr"><?= ($arpu == 0) ? "-" : number_format($arpu, 2)?></td>
					<td class="tdc"><input type="button" class="btn_03" value="상세보기" style="cursor:pointer" onclick="view_event_dtl(<?= $eventidx ?>)" /></td>
				</tr>
<?
			$tmp_sendidx = $sendidx;
		}
		
		$total_cpi = ($total_user_cnt == 0) ? 0 : number_format($total_spend / $total_user_cnt, 2);
		$total_roi = ($total_spend == 0) ? 0 : round($total_money / $total_spend * 100, 2);
		$total_arppu = ($total_pay_user_count == 0) ? 0 : round($total_money / $total_pay_user_count, 2);
		$total_arpu = ($total_user_cnt == 0) ? 0 : round($total_money / $total_user_cnt, 2);
?>
				<tr style="border-top:1px double;">
					<td class="tdc point" colspan="3">Total</td>
					<td class="tdr point"><?= number_format($total_send_cnt) ?></td>
					
					<td class="tdr point"><?= number_format($total_user_cnt) ?></td>
					<td class="tdr point"><?= number_format($total_return_user_1week_cnt) ?></td>
					<td class="tdr point"><?= number_format($total_return_user_2week_cnt) ?></td>
					
					<td class="tdr"><?= ($total_spend == 0) ? "-" : "$ ".number_format($total_spend, 2)?></td>
					<td class="tdr"><?= ($total_cpi == 0) ? "-" : "$ ".number_format($total_cpi, 2)?></td>
					<td class="tdr"><?= ($total_money == 0) ? "-" : "$ ".number_format($total_money)?></td>
					
					<td class="tdr"><?= ($total_roi == 0) ? "-" : $total_roi." %"?></td>
					<td class="tdr"><?= ($total_arppu == 0) ? "-" : number_format($total_arppu, 2)?></td>
					<td class="tdr"><?= ($total_arpu == 0) ? "-" : number_format($total_arpu, 2)?></td>
					<td class="tdr point"></td>
				</tr>
			</tbody>
		</table>
	</div>
	</form>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
        
<?
	$db_main2->end();

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>