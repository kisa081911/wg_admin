<?
    $top_menu = "game";
    $sub_menu = "notice_popup_write";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $mesgidx = $_GET["mesgidx"];
    $platform = $_GET["platform"];
    
    check_number($mesgidx);
    
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    $db_mobile = new CDatabase_Mobile();	
    
    if ($mesgidx != "")
    {
        $sql = "SELECT *,DATE_FORMAT(startdate,'%Y-%m-%d') AS startdate_day ,DATE_FORMAT(enddate,'%Y-%m-%d') AS enddate_day 
                        , DATE_FORMAT(startdate,'%H') AS startdate_hour ,DATE_FORMAT(enddate,'%H') AS enddate_hour
                        , DATE_FORMAT(startdate,'%i') AS startdate_min ,DATE_FORMAT(enddate,'%i') AS enddate_min  FROM tbl_notice_popup_info WHERE mesgidx=$mesgidx";
        
        $event = $db_main2->getarray($sql);
        
        $mesgidx = $event["mesgidx"];
        $message = $event["message"];
        $platform = $event["platform"];
        $enable = $event["enable"];
        $startdate_day = $event["startdate_day"];
        $enddate_day = $event["enddate_day"];
        $startdate_hour = $event["startdate_hour"];
        $enddate_hour = $event["enddate_hour"];
        $startdate_min = $event["startdate_min"];
        $enddate_min = $event["enddate_min"];
    }  
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    function save_event()
    {
        var input_form = document.input_form;
        var platform = input_form.platform.value;
        
        if (input_form.message.value == "")
        {
            alert("공지 내용을 입력하세요.");
            input_form.message.focus();
            return;
        }

        if (input_form.startdate_day.value == "")
        {
            alert("공지  기간을 입력하세요.");
            input_form.start_eventdate.focus();
            return;
        }
        
        if (input_form.enddate_day.value == "")
        {
            alert("공지  기간을 입력하세요.");
            input_form.end_eventdate.focus();
            return;
        }
        if (input_form.enable.value == "")
        {
            alert("상태 값을 설정 하세요");
            input_form.enable.focus();
            return;
        }
        
        if (input_form.startdate_hour.value != "" && input_form.startdate_hour.value == "")
        {
            alert("공지 시간을 선택해주세요.");
            input_form.startdate_hour.focus();
            return;
        }
        
        if (input_form.enddate_hour.value != "" && input_form.enddate_hour.value == "")
        {
            alert("공지  시간을 선택해주세요.");
            input_form.enddate_hour.focus();
            return;
        }

        var param = {};
        param.mesgidx = "<?= $mesgidx ?>";
        param.platform = platform;
        param.startdate_day = input_form.startdate_day.value;
        param.startdate_hour = input_form.startdate_hour.value;
        param.startdate_min = input_form.startdate_min.value;
        param.enddate_day = input_form.enddate_day.value;
        param.enddate_hour = input_form.enddate_hour.value;
        param.enddate_min = input_form.enddate_min.value;
        param.message = input_form.message.value;
        
	param.enable = input_form.enable.value;

        if ("<?= $mesgidx ?>" == "")
        	WG_ajax_execute("game/insert_notice_popup", param, save_event_callback);
        else
        	WG_ajax_execute("game/update_notice_popup", param, save_event_callback);
    }

    function save_event_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            window.location.href = "notice_popup.php";
        }
    }
    
    function delete_event()
    {
        var input_form = document.input_form;

        if (!confirm("공지를 삭제 하시겠습니까?"))
            return;

        var param = {};

        param.mesgidx = "<?= $mesgidx ?>";

        WG_ajax_execute("game/delete_notice_popup", param, delete_event_callback);
    }

    function delete_event_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.href = "notice_popup.php";
        }
    }
    
    
    $(function() {
        $("#startdate_day").datepicker({ defaultDate : <?= date("y-m-d"); ?> });
    });
    
    $(function() {
        $("#enddate_day").datepicker({ defaultDate : <?= date("y-m-d"); ?> });
    });

    function fnChkByte(obj, maxByte) {
        var str = obj.value;
        var str_len = str.length;
        var rbyte = 0;
        var rlen = 0;
        var one_char = "";
        var str2 = "";
        for (var i = 0; i < str_len; i++) {
            one_char = str.charAt(i);
            if (escape(one_char).length > 4) {
                rbyte += 2; //한글2Byte
            } else {
                rbyte++; //영문 등 나머지 1Byte
            }
            if (rbyte <= maxByte) {
                rlen = i + 1; //return할 문자열 갯수
            }
        }
        if (rbyte > maxByte) {
            alert("한글 " + (maxByte / 2) + "자 / 영문 " + maxByte + "자를 초과 입력할 수 없습니다.");
            str2 = str.substr(0, rlen); //문자열 자르기
            obj.value = str2;
            fnChkByte(obj, maxByte);
        }
    }
        
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 이벤트 관리</div>
<?
if ($mesgidx != "")
    { 
?>
            <div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_event()"></div>
<?
    } 
?>
	</div>
	<!-- //title_warp -->
            
	<form name="input_form" id="input_form" onsubmit="return false">
		<div class="h2_title">[이벤트 정보]</div>
		<table class="tbl_view_basic">
			<colgroup>
				<col width="150">
				<col width="">
			</colgroup>
			<tbody>
				<tr>
					<th>Platform</th>
					<td>
						<select id="platform" name="platform">
							<option value="">선택</option>
							<option value="4" <?= ($platform=="4") ? "selected" : "" ?>>공통</option>
							<option value="0" <?= ($platform=="0") ? "selected" : "" ?>>Web</option>
							<option value="1" <?= ($platform=="1") ? "selected" : "" ?>>IOS</option>
							<option value="2" <?= ($platform=="2") ? "selected" : "" ?>>Android</option>
							<option value="3" <?= ($platform=="3") ? "selected" : "" ?>>Amazon</option>
						</select> 
					</td>
				</tr>
				<tr>
					<th><span>*</span> messages</th>
                    <td>
                        <textarea style="width:700px;height:350px;" name="message" id="message" onKeyUp="javascript:fnChkByte(this,'1000')"><?= $message ?></textarea>
                    </td>
				</tr>				
				<tr>
					<th>enable</th>
					<td>
						<select id="enable" name="enable">
							<option value="">선택</option>
							<option value="0" <?= ($enable=="0") ? "selected" : "" ?>>비활성</option>
							<option value="1" <?= ($enable=="1") ? "selected" : "" ?>>활성</option>
						</select> 
					</td>
				</tr>				
				<tr>
					<th>기간</th>
					<td>
						<input type="input" class="search_text" id="startdate_day" name="startdate_day" style="width:65px" value="<?= $startdate_day ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
						<select id="startdate_hour" name="startdate_hour">
							<option value="">선택</option>
<?
    for ($i=0;$i<24;$i++)
    {
?>
							<option value="<?= $i ?>" <?= ($i == $startdate_hour) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
						</select>시&nbsp;
						<select id="startdate_min" name="startdate_min">
						<option value="">선택</option>
<?
    for ($i=0;$i<60;$i++)
    {
?>
							<option value="<?= $i ?>" <?= ($i == $startdate_min) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
						</select>분&nbsp;
						-
						<input type="input" class="search_text" id="enddate_day" name="enddate_day" style="width:65px" value="<?= $enddate_day ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
						<select id="enddate_hour" name="enddate_hour">
							<option value="">선택</option>
<?
    for ($i=0;$i<24;$i++)
    {
?>
							<option value="<?= $i ?>" <?= ($i == $enddate_hour) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
						</select>시&nbsp;
						<select id="enddate_min" name="enddate_min">
							<option value="">선택</option>
<?
    for ($i=0;$i<60;$i++)
    {
?>
							<option value="<?= $i ?>" <?= ($i == $enddate_min) ? "selected" : "" ?>><?= $i ?></option>
<?
    }
?>
						</select>분&nbsp;
						
					</td>
				</tr>
			</tbody>
		</table>
	</form>
            
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="저장" onclick="save_event()">
		<input type="button" class="btn_setting_02" value="취소" onclick="go_page('notice_popup.php')">            
	</div>
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main->end();
	$db_main2->end();
	$db_mobile->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>