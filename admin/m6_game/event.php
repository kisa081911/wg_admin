<?
    $top_menu = "game";
    $sub_menu = "event";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $db_main2 = new CDatabase_Main2();
    $db_analysis = new CDatabase_Analysis();
    
    $sql = "SELECT accesstoken FROM page_accesstoken WHERE userid='1015660465162708' AND writedate>DATE_ADD(NOW(), INTERVAL -14 DAY) ORDER BY writedate DESC LIMIT 1";
    $accesstoken = $db_analysis->getvalue($sql);

    $sql = "SELECT accesstoken FROM page_accesstoken WHERE userid='1015660465162708' AND writedate>DATE_ADD(NOW(), INTERVAL -7 DAY) ORDER BY writedate DESC LIMIT 1";
    $recent_accesstoken = $db_analysis->getvalue($sql);
/*
    // fan page manage용 access token 저장  || $_SESSION["adminid"] == "csmanager")
    if ($_GET["page"] == "" && $recent_accesstoken == "" && $SERVER_NAME == "admin" && $_SESSION["adminid"] == "admin")
    {
   		session_start();
	                
	    $facebook = new Facebook(array(
	        'appId'  => FACEBOOK_APP_ID,
	        'secret' => FACEBOOK_SECRET_KEY,
	        'cookie' => true,
	    ));

	    $_SESSION['facebook_session'] = "";
	    $session = $facebook->getUser();
	    if (!$session) 
	    {
	        $url = $facebook->getLoginUrl(array(
					'canvas' => 1,
					'fbconnect' => 0,
	        		'req_perms' => 'manage_pages'
	               ));

	        echo ('<script type="text/javascript">top.location.href=\''.$url.'\';</script>');
	        die();
	    } 
	    else 
	    {
	        $_SESSION['facebook_session'] = $session;

	        try 
	        {
	            $uid = $facebook->getUser();
	        } 
	        catch (FacebookApiException $e) 
	        {
	            $url = $facebook->getLoginUrl(array(
	                    'canvas' => 1,
	                    'fbconnect' => 0,
	                    'req_perms' => 'manage_pages'
	                ));
	        } 
	        $admin_userid = $uid;

			if ($admin_userid == "1015660465162708")
			{
				$longlivedtoken = $facebook->getExtendedAccessToken();
				$facebook->manual_accesstoken = $longlivedtoken;
				
				$page_id = "1514356795528809";
				
				$page_info = $facebook->api("/$page_id?fields=access_token"); 
				$accesstoken = $page_info["access_token"];

				if ($accesstoken != "" )
				{
					$sql = "DELETE FROM page_accesstoken WHERE userid='$admin_userid';".
						"INSERT INTO page_accesstoken(userid,accesstoken,writedate) VALUES('$admin_userid','$accesstoken',NOW())";
						
					$db_analysis->execute($sql);
				}
			}
	    }
    }
  */  
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_title = $_GET["title"];
    $category = $_GET["category"];
    
    $listcount = 10;
    $pagename = "event.php";
    $pagefield = "title=$search_title&category=$category";
    
    $tail = "WHERE 1=1 ";
    
    if ($search_title != "")
    	$tail .= " AND title LIKE '%".encode_db_field($search_title)."%' ";
    
    if ($category != "")
    	$tail .= " AND category='$category' ";
    
    $sql = "SELECT eventidx,title,category,reward_type,start_eventdate,end_eventdate,reservation_fanpage_status,writedate, ".
    		"(SELECT IFNULL(SUM(reward_amount),0) AS total_amount FROM tbl_event_result WHERE eventidx=tbl_event.eventidx) AS total_amount,".
    		"(SELECT COUNT(*) FROM tbl_event_result WHERE eventidx=tbl_event.eventidx) AS total_user, ".
    		"(SELECT COUNT(*) FROM tbl_ultra_freespin_event_result WHERE eventidx=tbl_event.eventidx) AS total_ultra_freespin_user, ".
    		"(SELECT COUNT(*) FROM tbl_event_result WHERE eventidx=tbl_event.eventidx AND isnew=1) AS new_user, ".
    		"(SELECT COUNT(*) FROM tbl_event_result WHERE eventidx=tbl_event.eventidx AND isreturn=1) AS return_user ".
    		"FROM tbl_event $tail ORDER BY eventidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    $eventlist = $db_main2->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) FROM tbl_event $tail";
    $totalcount = $db_main2->getvalue($sql);
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
    	$page = floor(($totalcount + $listcount - 1) / $listcount);
    
    $db_main2->end();
    $db_analysis->end();
?>
<script type="text/javascript">
function search_press(e)
{
    if (((e.which) ? e.which : e.keyCode) == 13)
    {
        search();
    }
}

function search()
{
    var search_form = document.search_form;
    search_form.submit();
}

function go_result(eventidx)
{
    window.location.href = "event_result.php?eventidx="+eventidx;
} 

function tab_change(fanpage)
{
    var search_form = document.search_form;
    search_form.fanpage.value = fanpage;
    search_form.submit();
}    
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
    
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 이벤트 관리 (<?= make_price_format($totalcount) ?>)</div>
	</div>
	<!-- //title_warp -->
        
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
		<div class="detail_search_wrap">
			<span class="search_lbl">이벤트 종류</span>
			<select id="category" name="category">
				<option value="">전체</option>
				<option value="1" <?= ($category=="1") ? "selected" : "" ?>>Free Coins Event</option>
				<option value="2" <?= ($category=="2") ? "selected" : "" ?>>Free TY Points Event</option>
				<option value="3" <?= ($category=="3") ? "selected" : "" ?>>Extra Lucky Wheel Event</option>
				<option value="4" <?= ($category=="4") ? "selected" : "" ?>>Giveaway Event</option>
				<option value="5" <?= ($category=="5") ? "selected" : "" ?>>WelcomeBack 이벤트</option>
				<option value="6" <?= ($category=="6") ? "selected" : "" ?>>Ultra FreeSpin</option>
				<option value="7" <?= ($category=="7") ? "selected" : "" ?>>Email Retention Event</option>
				<option value="10" <?= ($category=="10") ? "selected" : "" ?>>챗봇 이벤트</option>
				<option value="20" <?= ($category == "20") ? "selected" : "" ?>>팬페이지 스크래치 카드</option>
				<option value="21" <?= ($category == "21") ? "selected" : "" ?>>팬페이지 슬롯오브 포춘</option>
				<option value="22" <?= ($category == "22") ? "selected" : "" ?>>팬페이지 이벤트 황금알</option>
				<option value="23" <?= ($category == "23") ? "selected" : "" ?>>YouTube 리텐션</option>
			</select>&nbsp;
			<span class="search_lbl">이벤트 제목</span>
			<input type="text" class="search_text" id="title" name="title" style="width:250px" value="<?= encode_html_attribute($search_title) ?>" onkeypress="search_press(event)" />&nbsp;
                                    
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>
        
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="40">
			<col width="">
			<col width="70">
			<col width="70">
			<col width="70">
			<col width="120">
			<col width="90">
			<col width="80">
 			<col width="80">
			<col width="80">
		</colgroup>
		<thead>
			<tr>
				<th>번호</th>
				<th class="tbl">이벤트 제목</th>
				<th class="tbc">총<br>참여인원</th>
				<th class="tbc">신규가입</th>
				<th class="tbc">2주이탈<br>복귀</th>
				<th class="tdr">리워드지급</th>
				<th>팬페이지</br/>등록완료</th>
				<th>이벤트기간</th>
				<th>등록일</th>
				<th>결과</th>
			</tr>
		</thead>
		<tbody>
<?
for ($i=0; $i<sizeof($eventlist); $i++)
{
    $eventidx = $eventlist[$i]["eventidx"];
    $title = $eventlist[$i]["title"];
    $category = $eventlist[$i]["category"];
    $reward_type = $eventlist[$i]["reward_type"];
    $total_amount = $eventlist[$i]["total_amount"];
    $total_user = $eventlist[$i]["total_user"]; 
    $total_ultra_freespin_user = $eventlist[$i]["total_ultra_freespin_user"];
    $total_coin = $eventlist[$i]["total_coin"];
    $start_eventdate = $eventlist[$i]["start_eventdate"];
    $end_eventdate = $eventlist[$i]["end_eventdate"];
    $writedate = $eventlist[$i]["writedate"];
    $new_user = $eventlist[$i]["new_user"];
    $return_user = $eventlist[$i]["return_user"];
    $reservation_fanpage_status = $eventlist[$i]["reservation_fanpage_status"];
    
    if($reward_type == "1" || $reward_type == "0")
    	$reward_str = "Coins";
    else if($reward_type == "2")
    	$reward_str = "TY Points";
    
    if($category == 6)
    	$total_user = $total_ultra_freespin_user;
    
?>
        <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="view_event_dtl(<?= $eventidx ?>)">
            <td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
            <td class="tbl point_title"><?= $title ?></td>
            <td class="tdc point"><?= make_price_format($total_user) ?></td>
            <td class="tdc"><?= make_price_format($new_user) ?></td>
            <td class="tdc"><?= make_price_format($return_user) ?></td>
             <td class="tdr point"><?= make_price_format($total_amount)." $reward_str" ?></td>
            <td class="tdc"><?= ($reservation_fanpage_status == "1") ? "완료" : "-" ?></td> 
            <td class="tdc"><?= $start_eventdate ?> ~ <?= $end_eventdate ?></td> 
            <td class="tdc"><?= $writedate ?></td> 
            <td class="tdc"><input type="button" class="btn_03" value="보기" style="cursor:pointer" onclick="event.cancelBubble=true;go_result(<?= $eventidx ?>)" /></td>
        </tr>
<?
} 
?>
	</tbody>
</table>
<?
include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>

<div class="button_warp tdr">
	<input type="button" class="btn_setting_01" value="이벤트 추가" onclick="window.location.href='event_write.php'">           
</div>            
</div>
<!--  //CONTENTS WRAP -->
    
<div class="clear"></div>
<?
include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
