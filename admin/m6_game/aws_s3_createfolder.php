<?
	require '../common/aws_sdk/vendor/autoload.php';
    use Aws\Common\Aws;
    
    ini_set("memory_limit", "-1");
    
    $create_folder_path = $_POST['createfolderpath'];
    
    try
    {
	    if($create_folder_path=="")
	    {
			echo "폴더 명을 입력 하세요";
	    }
	    else 
	    {
	    	$aws = Aws::factory('../common/aws_sdk/aws_config.php');
	    	$s3Client = $aws->get('s3');
	    	$bucket = "dug-event-img";
	    	//폴더 생성
	    	$s3Client->putObject(array(
	    				'Bucket'       => $bucket, // Defines name of Bucket
	    				'Key'          => $create_folder_path, //Defines Folder name
	    				'Body'         => "",
	    				'ACL'          => 'public-read' // Defines Permission to that folder
	    		));
	    	echo "폴더 생성 완료";
	    }
    	
    }
    catch(\Aws\S3\Exception\S3Exception $e)
    {
    	write_log($e->getMessage());
    }
?>
