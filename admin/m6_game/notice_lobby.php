<?
    $top_menu = "game";
    $sub_menu = "notice_lobby";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    $db_main2 = new CDatabase_Main2();
    $db_analysis = new CDatabase_Analysis();

    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_title = $_GET["title"];
    $search_groups = ($_GET["search_groups"] == "")? "ALL":$_GET["search_groups"];
    
    $listcount = 10;
    $pagename = "notice_lobby.php";
    $pagefield = "message=$search_title";
    
    $tail = "WHERE 1=1 ";
    
    if ($search_title != "")
    	$tail .= " AND message LIKE '%".encode_db_field($search_title)."%' ";
    
    if($search_groups != "ALL")
        $tail .= " AND groups = $search_groups";
    
    $sql = "SELECT * FROM tbl_notice_lobby_info $tail ORDER BY mesgidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    $notice_lobby_list = $db_main2->gettotallist($sql);
    
    $sql = "SELECT count(*) FROM tbl_notice_lobby_info $tail";
    $totalcount = $db_main2->getvalue($sql);
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
    	$page = floor(($totalcount + $listcount - 1) / $listcount);
    
    $db_main2->end();
    $db_analysis->end();
?>
<script type="text/javascript">
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    
    function tab_change(fanpage)
    {
        var search_form = document.search_form;
        search_form.fanpage.value = fanpage;
        search_form.submit();
    }    
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
    
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 전광판 관리 (<?= make_price_format($totalcount) ?>)</div>
	</div>
	<!-- //title_warp -->
        
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
		<div class="detail_search_wrap">
			<select id="search_groups" name="search_groups">
				<option value="ALL">전체</option>
				<option value="0" <?= ($search_groups=="0") ? "selected" : "" ?>>groups 0 (신규 유저 그룹 (가입 1주일 이내))</option>
				<option value="1" <?= ($search_groups=="1") ? "selected" : "" ?>>groups 1 (2주 이상 이탈 복귀자 그룹)</option>
				<option value="2" <?= ($search_groups=="2") ? "selected" : "" ?>>groups 2 (결제경험 유저 그룹)</option>
				<option value="3" <?= ($search_groups=="3") ? "selected" : "" ?>>groups 3 (비결제 유저 그룹 (위 3개 그룹 제외한 나머지))</option>
				<option value="10" <?= ($search_groups=="10") ? "selected" : "" ?>>groups 10 (격려, 유머, 격언 문구)</option>
				<option value="11" <?= ($search_groups=="11") ? "selected" : "" ?>>groups 11 (게임 팁 문구)</option>
				<option value="12" <?= ($search_groups=="12") ? "selected" : "" ?>>groups 12 (시즌 성 문구)</option>
				<option value="13" <?= ($search_groups=="13") ? "selected" : "" ?>>groups 13 (슬롯 홍보)</option>
			</select>&nbsp;
			
			<span class="search_lbl">message</span>
			<input type="text" class="search_text" id="title" name="title" style="width:250px" value="<?= encode_html_attribute($search_title) ?>" onkeypress="search_press(event)" />&nbsp;
                                    
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>
        
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="">
 			<col width="280">
 			<col width="80">
		</colgroup>
		<thead>
			<tr>
				<th class="tbl">문구내용</th>
				<th class="tbc">그룹</th>
				<th class="tbc">사용여부</th>
			</tr>
		</thead>
		<tbody>
<?
for ($i=0; $i<sizeof($notice_lobby_list); $i++)
{
    $mesgidx = $notice_lobby_list[$i]["mesgidx"];
    $groups = $notice_lobby_list[$i]["groups"];
    $message = $notice_lobby_list[$i]["message"];
    $enable = $notice_lobby_list[$i]["enabled"];
    
    $groups_str="";
    $enable_str="";
    
    if($groups == 0)
        $groups_str = 'groups 0 (신규 유저 그룹 (가입 1주일 이내))';
    else if($groups == 1)
        $groups_str = 'groups 1 (2주 이상 이탈 복귀자 그룹)';
    else if($groups == 2)
        $groups_str = 'groups 2 (결제경험 유저 그룹)';
    else if($groups == 3)
        $groups_str = 'groups 3 (비결제 유저 그룹 (위 3개 그룹 제외한 나머지))';
    else if($groups == 10)
        $groups_str = 'groups 10 (격려, 유머, 격언 문구)';
    else if($groups == 11)
        $groups_str = 'groups 11 (게임 팁 문구)';
    else if($groups == 12)
        $groups_str = 'groups 12 (시즌 성 문구)';
    else if($groups == 13)
        $groups_str = 'groups 13 (슬롯 홍보)';
    
    if($enable == 0)
        $enable_str = "비 활성";
    else 
        $enable_str = "활성";
?>
        <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="window.location.href='notice_lobby_write.php?mesgidx=<?= $mesgidx ?>'">
            <td class="tbl point_title"><?= mb_strimwidth($message , '0', '170', '...', 'utf-8');?></td>
            <td class="tdc point"><?= $groups_str ?></td>
            <td class="tdc"><?= $enable_str ?></td> 
        </tr>
<?
} 
?>
	</tbody>
</table>
<?
include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>

<div class="button_warp tdr">
	<input type="button" class="btn_setting_01" value="추가" onclick="window.location.href='notice_lobby_write.php'">           
</div>            
</div>
<!--  //CONTENTS WRAP -->
    
<div class="clear"></div>
<?
include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
