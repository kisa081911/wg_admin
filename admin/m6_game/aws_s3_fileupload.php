<?
    $top_menu = "game";
    $sub_menu = "aws_s3_fileupload";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    require '../common/aws_sdk/vendor/autoload.php';

    use Aws\Common\Aws;
    
    $fullpath = ($_GET['path']=="")?'take5/' : $_GET['path'];
    $prev_path = ($_GET['prev_path']=="")?'' : $_GET['prev_path'];
    ini_set("memory_limit", "-1");
    try
    {
    	// Create the AWS service builder, providing the path to the config file
    
    	$aws = Aws::factory('../common/aws_sdk/aws_config.php');
    	$s3Client = $aws->get('s3');
    	$bucket = "dug-event-img";
    	$result = $s3Client->listObjects(array('Bucket' => $bucket,'Prefix' => $fullpath,"Delimiter" => "/"));
    	$files = $result->getPath('Contents');
    }
    catch(\Aws\S3\Exception\S3Exception $e)
    {
    	write_log($e->getMessage());
    }
?>

<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/js/jquery.bpopup.min.js"></script>
<script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/js/jquery.easing.compatibility.js"></script>
	<link type="text/css" rel="stylesheet" href="/css/common.css" />
		<link type="text/css" rel="stylesheet" href="/css/popup.css" />
<script type="text/javascript">
function create_folder()
{	
	if( $('#createfolderpath').val().search(/\W|\s/g) > -1 ){
	    alert( '특수문자 또는 공백을 입력할 수 없습니다.' );
	    $('#createfolderpath').focus();
	    return false;
	} 
	var createfolderpath = "<?=$fullpath?>"+$('#createfolderpath').val()+"/";
	try
	{
		var request = $.ajax({
    		url: "aws_s3_createfolder.php",
			type: "POST",
			data: {createfolderpath:createfolderpath},
			cache: false,
			dataType: "html"
		});
    	request.done(function( data ) {
	    	alert(data);
    		location.reload();
    		$('#create_folder').bPopup().close();
		});
    	request.fail(function( jqXHR, textStatus ) {
		});
	}
	catch (e)
    {
    }
}
	
function show_create_folder()
{
	
	try
	{
		$('#create_folder').bPopup({
			easing: 'easeOutBack',
			speed: 850,
			opacity: 0.3,
			transition: 'slideDown',
			modalClose: false
		});
	}
	catch (e)
	{
	}
}

function hide_create_folder()
{
	$('#create_folder').bPopup().close();
}

function show_fileupload_popup()
{
	window.open("aws_s3_pop_img_upload.php?path=<?=$fullpath?>", "fileupload_popup", "top=300,left=600,width=362,height=150");
}

function show_imageview(filename,filepath,size,date)
{
	var html ="<div style='text-align: center;padding-left: 30px;padding-top: 15px;'>";
	html +=" <div class='h2_title' name='div_normal'> 이미지명  : "+filename+"</div>"; 
	html +=" <div class='h2_title' name='div_normal'> 이미지 경로  </br> https://dug-event-img.doubleugames.com/"+filepath+"</div>"; 
	html +=" <div class='h2_title' name='div_normal'>사이즈  : "+(size/1024).toFixed(1)+"KB</div>"; 
	html +=" <div class='h2_title' name='div_normal'>생성일/수정일  : "+date+"</div>"; 
	html +=" <div class='h2_title' name='div_normal'>미리보기</div>";
	html +=" <img src='https://dug-event-img.doubleugames.com/"+filepath+"' class='img-thumbnail'  width='304' height='236'/></br></div>";
	html +=" <input type='button' class='btn_03' style='margin-bottom: 5px;margin-top: 35px;margin-left: 44%;' value='닫기' onclick='hide_imageview()'>";
	
	$('#imageview').html(html);
	
	try
	{
		$('#imageview').bPopup({
			easing: 'easeOutBack',
			speed: 850,
			opacity: 0.3,
			transition: 'slideDown',
			modalClose: false
		});
	}
	catch (e)
	{
	}		
}
function hide_imageview()
{
	$('#imageview').bPopup().close();
}

function delete_resource()
{		
	if(confirm("선택 된 파일 및 폴더 를 삭제 하시겠습니까? \n *폴더 삭제 시 하위 파일 및 폴더 포함 삭제"))
	{
		try
		{
			var file_deletekeylist = new Array();
			var folder_deletekeylist = new Array();
			
			$("input[name='filelist']:checkbox:checked").map(function(){
					file_deletekeylist.push($(this).val());
			    });
		    
			$("input[name='folderlist']:checkbox:checked").map(function(){
					folder_deletekeylist.push($(this).val());
			    });
			    
			var request = $.ajax({
	    		url: "aws_s3_deleteresource.php",
				type: "POST",
				data: {filedeletekeylist:file_deletekeylist,folderdeletekeylist:folder_deletekeylist},
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
		    	alert(data);
	    		location.reload();
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}
}

</script>
	<!-- CONTENTS WRAP -->
	<div class="contents_wrap">
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 이미지 업로더 </div>
		</div>
		<div class="h2_title" name="div_normal">[ https://dug-event-img.doubleugames.com/<?=$fullpath?> ]</div> 
		<input type="button" class="btn_03" style="margin-bottom:5px;" value="폴더 생성" onclick="show_create_folder()">
		<input type="button" class="btn_03" style="margin-bottom:5px;" value="파일 업로드" onclick="show_fileupload_popup()">
<?
	if($_SESSION["dbaccesstype"]<=2)
	{
?>		
		<input type="button" class="btn_03" style="margin-bottom:5px;" value="삭제" onclick="delete_resource()">
<?
	}
?>		
		<!-- //title_warp -->
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="10">
				<col width="320">
				<col width="70">
				<col width="120">
			</colgroup>
			<thead>
				<tr>
					<th class="tbc">&nbsp;</th>
					<th class="tbl">경로</th>
					<th class="tbc">사이즈</th>
					<th class="tbc">생성/수정일</th>
				</tr>
			</thead>
			<tbody>
<?
	if($fullpath != "take5/")
	{
?>		
				<tr onmouseover="className='tr_over'" onmouseout="className=''">
					<td class="tbc">&nbsp;</td>
		            <td class="tbl point_title" onclick="go_page('aws_s3_fileupload.php?path=<?=$prev_path?>')" >. . /</td>
		            <td class="tdc">-</td>
		            <td class="tdc">-</td>
		        </tr>
<?		        
	}
?>			
<?
		if(sizeof($result['CommonPrefixes']) > 0)
		{
			foreach ($result['CommonPrefixes'] as $object)
			{
				$name = $object['Prefix'];
?>
			<tr onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc"><input type="checkbox" name="folderlist" value="<?=$name?>"/></td>
	            <td class="tbl h2_title" style="font: bold 16px 'Malgun Gothic';color:#65a8ce;" onclick="go_page('aws_s3_fileupload.php?path=<?=$name?>&prev_path=<?=$fullpath?>')" ><?=str_ireplace($fullpath,"",$name)?></td>
	            <td class="tdc">-</td>
	            <td class="tdc">-</td>
	        </tr>
<?				
			}
		}
?>
<?
		if(sizeof($files) > 0)
		{
			foreach ($files as $file)
			{
				$filename = $file['Key'];
				$size = $file['Size'];
				$date = $file['LastModified'];
				if($size>0)
				{
?>
			<tr onmouseover="className='tr_over'" onmouseout="className=''">
				<td class="tdc"><input type="checkbox" name="filelist"value="<?=$filename?>"/></td>
	            <td class="tbl point h3_title" style="font: bold 13px 'Malgun Gothic';" onclick="show_imageview('<?=str_ireplace($fullpath,"",$filename)?>','<?=$filename?>','<?=$size?>','<?=date('Y-m-d H:i:s',strtotime($date))?>')" ><?=str_ireplace($fullpath,"",$filename)?></td>
	            <td class="tdc"><?=number_format($size)?></td>
	            <td class="tdc"><?=date('Y-m-d H:i:s',strtotime($date))?></td>
	        </tr>
<?				
				}
			}
		}
?>
			</tbody>
		</table>
	    <div id="create_folder" style="display:none;width: 381px;background: #fff;height: 50px;padding-top: 27px;padding-left: 25px;">
			폴더명 : <input type="text" name="createfolderpath" id="createfolderpath">
			<input type="button" class="btn_03" style="margin-bottom:5px;" value="만들기" onclick="create_folder()">
			<input type="button" class="btn_03" style="margin-bottom:5px;" value="닫기" onclick="hide_create_folder()">
		</div>    
	    <div id="imageview" style="display:none; width: 1024px;height:500px;background:#fff;">
		</div>    
	</div>
	<!--  //CONTENTS WRAP -->
	<div class="clear"></div>
	
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
