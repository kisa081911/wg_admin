<?
	$top_menu = "product";
	$sub_menu = "product";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$page = ($_GET["page"] == "") ? "1" : $_GET["page"];
	$status = $_GET["status"];
	$search_value = trim($_GET["search_value"]);
	$start_amount = $_GET["start_amount"];
	$end_amount = $_GET["end_amount"];
	$start_facebookcredit = $_GET["start_facebookcredit"];
	$end_facebookcredit = $_GET["end_facebookcredit"];
	$search_product_type = $_GET["product_type"];
			
    check_xss($search_value);
    
	$pagename = "product_mng.php";
	$pagefield = "status=$status&search_value=$search_value&start_amount=$start_amount&end_amount=$end_amount&start_facebookcredit=$start_facebookcredit&end_facebookcredit=$end_facebookcredit&product_type=$search_product_type";
	$listcount = "10";
	
	$tail = " WHERE status!=3 ";
	
	if ($status != "")
		$tail .= " AND status=$status";
	
	if ($start_amount != "")
		$tail .= " AND amount >= $start_amount";
	
	if ($end_amount != "")
		$tail .= " AND amount <= $end_amount";
	
	if ($start_facebookcredit != "")
		$tail .= " AND facebookcredit >= $start_facebookcredit";
	
	if ($end_facebookcredit != "")
		$tail .= " AND facebookcredit <= $end_facebookcredit";
	
	if ($search_value != "")
		$tail .= " AND productname LIKE '%$search_value%'";
	
	if($search_product_type != "")
		$tail .= " AND product_type=$search_product_type";
			
    $db_main = new CDatabase_Main();
	
	$totalcount = $db_main->getvalue("SELECT COUNT(*) FROM tbl_product $tail");
	
	$sql = "SELECT productidx,productkey,category,productname,description,imageurl,amount,facebookcredit,special_more,special_discount,status,product_type,writedate FROM tbl_product $tail ORDER BY productidx DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
	$productlist = $db_main->gettotallist($sql);
	
	$sql = "SELECT product_type FROM tbl_product GROUP BY product_type";
	$product_type_list = $db_main->gettotallist($sql);
	
	$db_main->end();
	
	if ($totalcount < ($page-1) * $listcount && page != 1)
		$page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script type="text/javascript">
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}
	
	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}
</script>    	
    	<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; 상품목록 <span class="totalcount">(<?= number_format($totalcount) ?>)</span></div>
	        </div>
	    	<!-- //title_warp -->
	    	
	    	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="product_mng.php">
		    	<div class="detail_search_wrap">
		    		<span class="search_lbl">금액/개수</span> 
		    		<input type="text" class="search_text" name="start_amount" id="start_amount" style="width:60px" value="<?= $start_amount ?>" onkeypress="search_press(event); return checkonlynum();" />
		    		 ~  
		    		<input type="text" class="search_text" name="end_amount" id="end_amount" style="width:60px" value="<?= $end_amount ?>" onkeypress="search_press(event); return checkonlynum();" />  
		    		<span class="search_lbl ml20">페이스북 creidt</span> 
		    		<input type="text" class="search_text" name="start_facebookcredit" id="start_facebookcredit" style="width:60px" value="<?= $start_facebookcredit ?>" onkeypress="search_press(event); return checkonlynum();" />
		    		 ~  
		    		<input type="text" class="search_text" name="end_facebookcredit" id="end_facebookcredit" style="width:60px" value="<?= $end_facebookcredit ?>" onkeypress="search_press(event); return checkonlynum();" />
		    		<span class="search_lbl ml20">상태</span> 
		    		<select name="status" id="status">
		    			<option value="">선택</option>
		    			<option value="1" <?= ($status == "1") ? "selected" : ""?>>사용</option>
		    			<option value="2" <?= ($status == "2") ? "selected" : ""?>>미사용</option>
		    		</select>
		    		<span class="search_lbl ml20">상품군</span> 
		    		<select name="product_type" id="product_type">
		    			<option value="">선택</option>
<?
		    			for ($i=0; $i<sizeof($product_type_list); $i++)
		    			{
		    				$product_type = $product_type_list[$i]["product_type"];
		    				
		    				if($product_type == -1)
		    					$product_type_name = "사용안함";
		    				else if($product_type == 1)
								$product_type_name = "Basic";
							else if($product_type == 2)
								$product_type_name = "Season";
							else if($product_type == 3)
								$product_type_name = "Threadhold";
							else if($product_type == 4)
								$product_type_name = "Whale";
							else if($product_type == 5)
								$product_type_name = "28 Retention";
							else if($product_type == 6)
								$product_type_name = "Frist";
							else if($product_type == 7)
								$product_type_name = "Lucky";
							else if($product_type == 8)
								$product_type_name = "Monthly";
							else if($product_type == 9)
								$product_type_name = "Piggypot";
							else if($product_type == 10)
								$product_type_name = "Buyerleave";
							else if($product_type == 11)
								$product_type_name = "NoPayer";
							else if($product_type == 12)
								$product_type_name = "Primedeal_1";
							else if($product_type == 13)
								$product_type_name = "Primedeal_2";
							else if($product_type == 14)
								$product_type_name = "Attend";
							else if($product_type == 15)
								$product_type_name = "Platinum Deal";
							else if($product_type == 16)
								$product_type_name = "Amazing Deal";
							else if($product_type == 17)
								$product_type_name = "First Attend";
?>
		    				<option value="<?=$product_type?>" <?= ($product_type == "$search_product_type") ? "selected" : ""?>><?=$product_type_name?></option>		    			
<?
		    			}
?>
		    			
		    		</select>		    		
		    		<span class="search_lbl ml20">상품명</span>
		    		<input type="text" class="search_text" id="serach_value" name="search_value"  value="<?= encode_html_attribute($search_value) ?>"  onkeypress="search_press(event)" style="width:130px;" /> 
		            <div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		    	</div>
	    	</form>
    	
    		<table class="tbl_list_basic1">
    		<thead>
    		<tr>
    			<th>번호</th>
    			<th>상품이미지</th>
    			<th class="tdl">상품명</th>
    			<th>금액/개수</th>
    			<th>페이스북 Credit</th>
    			<th>상품군</th>
    			<th>상태</th>
    			<th>등록일</th>
    		</tr>
			</thead>
			<tbody>
<?
	for ($i=0; $i<sizeof($productlist); $i++)
	{ 
		$category = $productlist[$i]["category"];
		$productidx = $productlist[$i]["productidx"];
		$productname = $productlist[$i]["productname"];
		$description = $productlist[$i]["description"];
		$imageurl = $productlist[$i]["imageurl"];
		$amount = $productlist[$i]["amount"];
		$facebookcredit = $productlist[$i]["facebookcredit"];
		$special_more = $productlist[$i]["special_more"];
		$special_discount = $productlist[$i]["special_discount"];
		$status = $productlist[$i]["status"];
		$product_type = $productlist[$i]["product_type"];
		$writedate = $productlist[$i]["writedate"];
		
		$count_name = "개";
		
		if($category == 0)
			$count_name = "Coins";
		else if($category == 1)
			$count_name = "Coins";
		else if($category == 5)
			$count_name = "Credit + Item";
		else if($category == 6)
			$count_name = "Credit + Item";
		else if($category == 8)
		{
			$amount = $amount + ($amount * ($special_more) / 100);
			$count_name = "Coins ($special_more% More)";
		}
		else if($category == 9)
			$count_name = "Coins ($special_discount% Discount)";
		
		if($product_type == -1)
			$product_type_name = "사용안함";
		else if($product_type == 1)
			$product_type_name = "Basic";
		else if($product_type == 2)
			$product_type_name = "Season";
		else if($product_type == 3)
			$product_type_name = "Threadhold";
		else if($product_type == 4)
			$product_type_name = "Whale";
		else if($product_type == 5)
			$product_type_name = "28 Retention";
		else if($product_type == 6)
			$product_type_name = "Frist";
		else if($product_type == 7)
			$product_type_name = "Lucky";
		else if($product_type == 8)
			$product_type_name = "Monthly";
		else if($product_type == 9)
			$product_type_name = "Piggypot";
		else if($product_type == 10)
			$product_type_name = "Buyerleave";
		else if($product_type == 11)
			$product_type_name = "NoPayer";
		else if($product_type == 12)
			$product_type_name = "Primedeal_1";
		else if($product_type == 13)
			$product_type_name = "Primedeal_2";
		else if($product_type == 14)
			$product_type_name = "Attend";
		else if($product_type == 15)
			$product_type_name = "Platinum Deal";
		else if($product_type == 16)
			$product_type_name = "Amazing Deal";
		else if($product_type == 17)
			$product_type_name = "First Attend";
?>
			<tr class="<?= ($status=="2") ? "tr_disabled" : "" ?>" onmouseover="className='tr_over'" onmouseout="className='<?= ($status=="2") ? "tr_disabled" : "" ?>'" onclick="view_product_dtl(<?= $productidx ?>)">
				<td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
				<td class="tdc"><img src="<?= $imageurl ?>" width="25" height="25"></td>
				<td class="point_title"><?= $productname ?></td>
				<td class="tdc point"><?= ($category == 7) ? "-" : number_format($amount)." $count_name" ?></td>
				<td class="tdc point"><?= $facebookcredit ?></td>
				<td class="tdc point"><?= $product_type_name ?></td>
				<td class="tdc point"><?= ($status == "1") ? "사용" : "미사용" ?></td>
				<td class="tdc"><?= $writedate ?></td>
			</tr>
<?
	} 
?>
			</tbody>
    		</table>
    		
<?
	include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
    		<div class="button_warp tdr">
				<input type="button" class="btn_setting_01" value="상품 추가" onclick="window.location.href='product_write.php'">    		
    		</div>
    	</div>
    	<!--  //CONTENTS WRAP -->
    	
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>