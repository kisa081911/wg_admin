<?
	$top_menu = "product";
	$sub_menu = "order";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$username = trim($_GET["username"]);
	$orderno = trim($_GET["orderno"]);
	$serarch_status = $_GET["status"];
	$start_facebookcredit = $_GET["start_facebookcredit"];
	$end_facebookcredit = $_GET["end_facebookcredit"];
	$search_disputed = $_GET["disputed"];
	$search_usecoupon = $_GET["usecoupon"];
	$search_usediscount = $_GET["usediscount"];
	$search_start_date = ($_GET["start_date"] == "") ? get_past_date(date("Y-m-d"),8,"d") : $_GET["start_date"];
	$search_end_date = ($_GET["end_date"] == "") ? date("Y-m-d") : $_GET["end_date"];
	$page = ($_GET["page"] == "") ? "1" : $_GET["page"];
	$search_product_type = $_GET["product_type"];

	check_xss($username.$orderno);
    check_number($serarch_status.$start_facebookcredit.$end_facebookcredit.$search_disputed);
    
	$pagename = "order_mng.php";
	$listcount = 10;
	$pagefield = "username=$username&orderno=$orderno&status=$serarch_status&start_date=$search_start_date&end_date=$search_end_date&start_facebookcredit=$start_facebookcredit&end_facebookcredit=$end_facebookcredit&disputed=$search_disputed&usecoupon=$search_usecoupon&usediscount=$search_usediscount&product_type=$product_type";
	

	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
		$tail = "WHERE A.status != 3 ";
	else
		$tail = "WHERE A.status != 3 ";
	
	$order_by = "ORDER BY orderidx DESC ";
    
    if ($orderno != "")
        $tail .= "AND A.orderno='$orderno'";
    
    if ($serarch_status != "")
        $tail .= " AND A.status=$serarch_status";

    if ($start_facebookcredit != "")
        $tail .= " AND A.facebookcredit >= $start_facebookcredit";
        
    if ($end_facebookcredit != "")
        $tail .= " AND A.facebookcredit <= $end_facebookcredit";

    if ($username != "")
        $tail .= " AND B.nickname LIKE '%$username%' ";
    
    if ($search_disputed != "")
        $tail .= " AND disputed = '$search_disputed' ";
    
    if ($search_usecoupon != "")
        $tail .= " AND couponidx<>0 ";
    
    if ($search_usediscount != "")
    	$tail .= " AND (special_discount>0 OR special_more>0 AND couponidx = 0) ";
    
    if ($search_start_date != "")
    	$tail .= " AND A.writedate >= '$search_start_date 00:00:00'";
    
    if ($search_end_date != "")
    	$tail .= " AND A.writedate <= '$search_end_date 23:59:59'";
    
    if($search_product_type != "")
    	$tail .= " AND product_type=$search_product_type";
                        	
	$db_main = new CDatabase_Main();
	
	$totalcount = $db_main->getvalue("SELECT COUNT(*) FROM ".
										"(	SELECT orderidx, useridx, t1.productidx, t1.facebookcredit, t1.status, t1.writedate, t1.canceldate, couponidx, t1.special_discount, t1.special_more, t2.product_type, t1.coin	".
										"	FROM tbl_product_order t1 JOIN tbl_product t2 ON t1.productidx = t2.productidx	".
										") A JOIN tbl_user B ON A.useridx=B.useridx $tail");
	
	$sql = "SELECT A.orderidx, productidx, A.facebookcredit, A.status, A.writedate, A.canceldate, B.nickname, userid, LEFT(createdate,10) AS createdate, couponidx, special_discount, special_more, product_type, ". 
			"(SELECT adflag FROM tbl_user_ext WHERE useridx=A.useridx) AS adflag,	".	   		
			"CASE WHEN A.coin != 0 THEN A.coin ELSE 0 END AS amount,	". 
			"(SELECT COUNT(*) FROM tbl_user_online_sync2 WHERE useridx=B.useridx) AS online_status	". 
			"FROM	".
			"(	".
			"	SELECT orderidx, useridx, t1.productidx, t1.facebookcredit, t1.status, t1.writedate, t1.canceldate, couponidx, t1.special_discount, t1.special_more, t2.product_type, t1.coin	".
			"	FROM tbl_product_order t1 JOIN tbl_product t2 ON t1.productidx = t2.productidx WHERE t2.product_type != -1	".
			") A JOIN tbl_user B ON A.useridx=B.useridx $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;
	$orderlist = $db_main->gettotallist($sql);
	
	$sql = "SELECT product_type FROM tbl_product WHERE product_type != -1 GROUP BY product_type";
	$product_type_list = $db_main->gettotallist($sql);
     
	$db_main->end();
	
	if ($totalcount < ($page-1) * $listcount && page != 1)
		$page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_date").datepicker({ });
	});
	
	$(function() {
	    $("#end_date").datepicker({ });
	});

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();	
	}
    
    function check_sleeptime()
    {
        setTimeout("window.location.reload(false)",60000);
    }
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Web 주문목록 <span class="totalcount">(<?= number_format($totalcount) ?>건)</span></div>
	</div>
	<!-- //title_warp -->
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="order_mng.php">
		<div class="detail_search_wrap">
			<span class="search_lbl">사용자&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" value="<?= encode_html_attribute($username) ?>" id="username" name="username"  onkeypress="search_press(event)" style="width:130px;" />
				<span class="search_lbl ml20">페이스북주문번호</span>
				<input type="text" class="search_text" value="<?= encode_html_attribute($orderno) ?>" id="orderno" name="orderno"  onkeypress="search_press(event)" style="width:130px;" /> 
				<span class="search_lbl ml20">주문상태</span>
				<select name="status" id="status">
					<option value="" <?= ($serarch_status == "") ? "selected" : "" ?>>선택하세요</option>
					<option value="0" <?= ($serarch_status == "0") ? "selected" : "" ?>>결제진행중</option>
					<option value="1" <?= ($serarch_status == "1") ? "selected" : "" ?>>주문완료</option>
					<option value="2" <?= ($serarch_status == "2") ? "selected" : "" ?>>결제취소</option>
				</select>   
				<span class="search_lbl ml20">주문일시</span>
				<input type="text" class="search_text" id="start_date" name="start_date" value="<?= $search_start_date ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="end_date" name="end_date" value="<?= $search_end_date ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
				<div class="clear"  style="padding-top:10px"></div>
				<span class="search_lbl">페이스북 크레딧</span>
				<input type="text" class="search_text" value="<?= $start_facebookcredit ?>" id="start_facebookcredit" name="start_facebookcredit" onkeypress="search_press(event); return checkonlynum();" style="width:110px;" /> ~ 
				<input type="text" class="search_text" value="<?= $end_facebookcredit ?>" id="end_facebookcredit" name="end_facebookcredit"  onkeypress="search_press(event); return checkonlynum();" style="width:110px;" />
				<span class="search_lbl ml20">
					<input type="checkbox" value="1" name="disputed" <?= ($search_disputed == "1") ? "checked" : "" ?> align="absmiddle" />Disputed
					<input type="checkbox" value="1" name="usecoupon" <?= ($search_usecoupon == "1") ? "checked" : "" ?> align="absmiddle" />쿠폰사용
					<input type="checkbox" value="1" name="usediscount" <?= ($search_usediscount == "1") ? "checked" : "" ?> align="absmiddle" />SpecialOffer
				</span>
				<span class="search_lbl ml20">상품군</span> 
		    		<select name="product_type" id="product_type">
		    			<option value="">선택</option>
<?
		    			for ($i=0; $i<sizeof($product_type_list); $i++)
		    			{
		    				$product_type = $product_type_list[$i]["product_type"];
							
							if($product_type == 1)
		    					$product_type_name = "Basic";
		    				else if($product_type == 2)
		    					$product_type_name = "Season";
		    				else if($product_type == 3)
		    					$product_type_name = "Threadhold";
		    				else if($product_type == 4)
		    					$product_type_name = "Whale";
		    				else if($product_type == 5)
		    					$product_type_name = "28 Retention";
		    				else if($product_type == 6)
		    					$product_type_name = "Frist";
		    				else if($product_type == 7)
		    					$product_type_name = "Lucky";
		    				else if($product_type == 8)
		    					$product_type_name = "Monthly";
		    				else if($product_type == 9)
		    					$product_type_name = "Piggypot";
		    				else if($product_type == 10)
		    					$product_type_name = "Buyerleave";
		    				else if($product_type == 11)
		    					$product_type_name = "NoPayer";
		    				else if($product_type == 12)
		    					$product_type_name = "Primedeal_1";
		    				else if($product_type == 13)
		    					$product_type_name = "Primedeal_2";
		    				else if($product_type == 14)
		    					$product_type_name = "Attend";
		    				else if($product_type == 15)
		    					$product_type_name = "Platinum Deal";
		    				else if($product_type == 16)
		    					$product_type_name = "Amazing Deal";
		    				else if($product_type == 17)
		    					$product_type_name = "First Attend";
	    					else if($product_type == 18)
	    					    $product_type_name = "Super Deal";
    					    else if($product_type == 19)
    					        $product_type_name = "Speed Wheel";
					        else if($product_type == 20)
					            $product_type_name = "Collection Deal";
?>
		    				<option value="<?=$product_type?>" <?= ($product_type == "$search_product_type") ? "selected" : ""?>><?=$product_type_name?></option>		    			
<?
		    			}
?>
		    			
		    		</select>
		    	</span>
				<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="50">
			<col width="">
			<col width="100">
			<col width="80">
			<col width="60">
			<col width="60">
			<col width="40">
			<col width="40">
			<col width="90">
			<col width="120">
		</colgroup>
		<thead>
			<tr>
    			<th>번호</th>
    			<th>사용자</th>
    			<th>유입경로</th>
                <th>가입일</th>
    			<th class="tdr">구입금액</th>
    			<th>Credit</th>
    			<th>쿠폰</th>
    			<th>Special</th>
    			<th>주문상태</th>
    			<th>상품군</th>
    			<th>주문일시<?= ($serarch_status == "2") ? "<br/>취소일시" : "" ?></th>
    		</tr>
		</thead>
		<tbody>
<?
	for($i=0; $i<sizeof($orderlist); $i++)
	{ 
		$orderidx = $orderlist[$i]["orderidx"];
		$productidx = $orderlist[$i]["productidx"];
		$userid = $orderlist[$i]["userid"];
		$amount = $orderlist[$i]["amount"];
		$username = $orderlist[$i]["nickname"];
		$photourl = get_fb_pictureURL($userid,$client_accesstoken);
		$facebookcredit = $orderlist[$i]["facebookcredit"];
		$status = $orderlist[$i]["status"];
		$product_type = $orderlist[$i]["product_type"];
		$adflag = $orderlist[$i]["adflag"];
		$createdate = $orderlist[$i]["createdate"];
		$writedate = $orderlist[$i]["writedate"];
		$canceldate = $orderlist[$i]["canceldate"];
		$online_status = $orderlist[$i]["online_status"];	
		$couponidx = $orderlist[$i]["couponidx"];
		$special = 0;
		
		if($couponidx == 0)
			$special = $orderlist[$i]["special_discount"] + $orderlist[$i]["special_more"];
		
		if ($adflag == "")
			$adflag = "바이럴";
		else if ($adflag == "1")
			$adflag = "maudau";
		
		if ($serarch_status == "2")
			$canceldate = "<br/><span style='color:red'>$canceldate</span>";
		else
			$canceldate = "";
		
		if($product_type == 1)
			$product_type_name = "Basic";
		else if($product_type == 2)
			$product_type_name = "Season";
		else if($product_type == 3)
			$product_type_name = "Threadhold";
		else if($product_type == 4)
			$product_type_name = "Whale";
		else if($product_type == 5)
			$product_type_name = "28 Retention";
		else if($product_type == 6)
			$product_type_name = "Frist";
		else if($product_type == 7)
			$product_type_name = "Lucky";
		else if($product_type == 8)
			$product_type_name = "Monthly";
		else if($product_type == 9)
			$product_type_name = "Piggypot";
		else if($product_type == 10)
			$product_type_name = "Buyerleave";
		else if($product_type == 11)
			$product_type_name = "NoPayer";
		else if($product_type == 12)
			$product_type_name = "Primedeal_1";
		else if($product_type == 13)
			$product_type_name = "Primedeal_2";
		else if($product_type == 14)
			$product_type_name = "Attend";
		else if($product_type == 15)
			$product_type_name = "Platinum Deal";
		else if($product_type == 16)
			$product_type_name = "Amazing Deal";
		else if($product_type == 17)
			$product_type_name = "First Attend";
		else if($product_type == 18)
		    $product_type_name = "Super Deal";
	    else if($product_type == 19)
	        $product_type_name = "Speed Wheel";
        else if($product_type == 20)
            $product_type_name = "Collection Deal";
?>
			<tr class="<?= ($status=="2") ? "tr_disabled" : "" ?>" onmouseover="className='tr_over'" onmouseout="className='<?= ($status=="2") ? "tr_disabled" : "" ?>'" onclick="view_order_dtl(<?= $orderidx ?>)">
				<td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
				<td class="point_title"><span style="float:left;padding-top:8px;padding-right:12px;"><img src="/images/icon/<?= ($online_status == "1") ? "status_1.png" : "status_3.png" ?>" align="absmiddle" /></span><img src="<?= $photourl ?>" height="25" width="25" align="absmiddle" /> <?= $username ?>&nbsp;&nbsp;<img src="/images/icon/facebook.png" height="20" width="20" align="absmiddle" style="cursor:pointer" onclick="event.cancelBubble=true;window.open('http://www.facebook.com/<?= $userid ?>')" /></td>
				<td class="tdc point"><?= $adflag ?></td>
				<td class="tdc point"><?= $createdate  ?></td>
				<td class="tdr point_title"><?= number_format($amount) ?></td>
				<td class="tdc point"><?= $facebookcredit ?></td>
				<td class="tdc point"><?= ($couponidx == "0") ? "" : "O" ?></td>
				<td class="tdc point"><?= ($special == "0") ? "" : "O" ?></td>
				<td class="tdc">
<?
					switch ($status)
					{
						case 0:
							echo("결제진행중");
							break;
						case 1:
							echo("주문완료");
							break;
						case 2:
							echo("결제취소");
							break;
					}
?>
				</td>
				<td class="tdc"><?= $product_type_name ?></td>
				<td class="tdc"><?= $writedate.$canceldate ?></td>
			</tr>
<?
	}
?>
		</tbody>
    </table>
<? 
	include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>

</div>
<!-- //CONTENTS WRAP -->

    
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>