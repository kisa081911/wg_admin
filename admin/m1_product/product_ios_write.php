<?
	$top_menu = "product";
	$sub_menu = "product_ios";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$productidx = $_GET["productidx"];
	
	check_number($productidx);
	
	if ($productidx != "")
	{
		$db_main = new CDatabase_Main();
		
		$sql = "SELECT productidx,productname,description,amount,facebookcredit,money,category,status,writedate,productkey,vip_point,special_more,special_discount,imageurl FROM tbl_product_mobile WHERE productidx=$productidx";
				
		$product = $db_main->getarray($sql);
		
		$productidx = $product["productidx"];				
		$productname = $product["productname"];
		$description = $product["description"];		
		$imageurl = $product["imageurl"];
		$amount = $product["amount"];
		$facebookcredit = $product["facebookcredit"];
		$money = $product["money"];
		$category = $product["category"];
		$status = $product["status"];
		$writedate = $product["writedate"];
		$vip_point = $product["vip_point"];
		$productkey = $product["productkey"];
		$special_more = $product["special_more"];
		$special_discount = $product["special_discount"];
		
				
		if ($productidx == "")
			error_back("잘못된 접근 입니다.");
		
		if ($status == "3")
			error_back("삭제된 상품입니다.");
		
		$db_main->end();
	}
?>
<script>
	function save_product_mobile()
	{
		var product_form = document.product_form;

		if (product_form.productnaem.value == "")
		{
			alert("상품명을 입력하세요.");
			product_form.title.focus();
			return;
		}

		if (product_form.description.value == "")
		{
			alert("상품 설명을 입력하세요.");
			product_form.description1.focus();
			return;
		}

		if (product_form.amount.value == "")
		{
			alert("충전 금액을 입력하세요.");
			product_form.amount.focus();
			return;
		}

		if (product_form.money.value == "")
		{
			alert("구매금액($)을 입력하세요.");
			product_form.money.focus();
			return;
		}

		if (product_form.facebookcredit.value == "")
		{
			alert("facebookcredit($)을 입력하세요.");
			product_form.money.focus();
			return;
		}

		if (!IsNumber(product_form.amount.value == ""))
		{
			alert("충전금액은 숫자만 입력할 수 있습니다.");
			product_form.amount.focus();
			return;
		}

		if (!IsNumber(product_form.money.value == ""))
		{
			alert("구매금액($)은 숫자만 입력할 수 있습니다.");
			product_form.money.focus();
			return;
		}

		if (!IsNumber(product_form.facebookcredit.value == ""))
		{
			alert("facebookcredit($)은 숫자만 입력할 수 있습니다.");
			product_form.facebookcredit.focus();
			return;
		}

		if (product_form.productkey.value == "")
		{
			alert("제품키를 입력하세요.");
			product_form.productkey.focus();
			return;
		}

		var param = {};
		
		param.productidx = product_form.productidx.value;
		param.productname = product_form.productname.value;
		param.description = product_form.description.value;		
		param.imageurl = product_form.imageurl.value;
		param.os_type = 1;
		param.amount = product_form.amount.value;
		param.facebookcredit = product_form.facebookcredit.value;
		param.money = product_form.money.value;		
		param.category = get_radio("category");
		param.status = get_radio("status");
		param.productkey = product_form.productkey.value;
		param.vip_point = product_form.vip_point.value;
		param.special_more = product_form.more_amount.value;
		param.special_discount = product_form.discount_amount.value;

		if (product_form.productidx.value == "")
			WG_ajax_execute("product/save_product_mobile", param, save_product_mobile_callback);
		else
			WG_ajax_execute("product/update_product_mobile", param, save_product_mobile_callback);
	}

	function save_product_mobile_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
           	window.location.href = "product_ios_mng.php";
        }
    }

    function delete_product_mobile()
    {
        var product_form = document.product_form;

        if (!confirm("상품을 삭제 하시겠습니까?"))
            return;

        var param = {};

        param.productidx = product_form.productidx.value;

        WG_ajax_execute("product/delete_product_mobile", param, delete_product_mobile_callback);
    }

    function delete_product_mobile_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.href = "product_ios_mng.php";
        }
    }
</script>
<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; 상품상세</div>
<?
	if ($productidx != "")
	{ 
?>
	   		<div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_product_mobile()"></div>
<?
	} 
?>
	        </div>
	    	<!-- //title_warp -->

			<form name="product_form" id="product_form">
			<input type="hidden" name="productidx" id="productidx" value="<?= $productidx ?>">
			<table class="tbl_view_basic">
			<colgroup>
				<col width="150">
				<col width="">
			</colgroup>
			<tbody>
			<tr>
				<th><span>*</span> 상품명</th>
				<td><input type="text" class="view_tbl_text" name="productname" id="productname" value="<?= encode_html_attribute($productname) ?>" maxlength="50"></td>
			</tr>
			<tr>
				<th><span>*</span> 설명</th>
				<td><input type="text" class="view_tbl_text" name="description" id="description" value="<?= encode_html_attribute($description) ?>" maxlength="500"></td>
			</tr>
			<tr>
				<th><span></span> 이미지 주소</th>
				<td><input type="text" class="view_tbl_text" name="imageurl" id="imageurl" value="<?= encode_html_attribute($imageurl) ?>" maxlength="100"></td>
			</tr>
			<tr>
				<th><span>*</span> category</th>
				<td>
					<input type="radio" class="radio" name="package" value="0" <?= ($package == "0" || $package == "") ? "checked" : ""?>> Basic 
					<input type="radio" class="radio ml20" name="package" value="1" <?= ($package == "1") ? "checked" : "" ?> > 1st Time  
					<input type="radio" class="radio ml20" name="package" value="2" <?= ($package == "2") ? "checked" : "" ?> > Season Offer 
					<input type="radio" class="radio ml20" name="package" value="3" <?= ($package == "3") ? "checked" : "" ?> > Special Offer					
				</td>
			</tr>
			<tr>
				<th><span>*</span> 충전금액</th>
				<td><input type="text" class="view_tbl_text" name="amount" id="amount" value="<?= $amount ?>" onkeypress="return checkonlynum();" style="width:150px"></td>
			</tr>
			<tr>
				<th><span>*</span> 구매금액($)</th>
				<td><input type="text" class="view_tbl_text" name="money" id="money" value="<?= $money ?>" onkeypress="return checkonlynum();" style="width:150px"></td>
			</tr>
			<tr>
				<th><span>*</span> Facebookcredit($)</th>
				<td><input type="text" class="view_tbl_text" name="facebookcredit " id="facebookcredit " value="<?= $facebookcredit ?>" onkeypress="return checkonlynum();" style="width:150px">
				</td>
			</tr>
			<tr>
				<th><span>*</span> VIP Point</th>
				<td><input type="text" class="view_tbl_text" name="vip_point" id="vip_point" value="<?= $vip_point ?>" onkeypress="return checkonlynum();" style="width:150px"> 
				</td>
			</tr>
			<tr>
				<th><span>*</span> More</th>
				<td><input type="text" class="view_tbl_text" name="more_amount" id="more_amount" value="<?= ($special_more == "") ? 0 : $special_more ?>" onkeypress="return checkonlynum();" style="width:50px"> %</td>
			</tr>
			<tr>
				<th><span>*</span> Discount</th>
				<td><input type="text" class="view_tbl_text" name="discount_amount" id="discount_amount" value="<?= ($special_discount == "") ? 0 : $special_discount ?>" onkeypress="return checkonlynum();" style="width:50px"> %	</td>
			</tr>
			<tr>
				<th><span>*</span> 상태</th>
				<td><input type="radio" class="radio" name="status" value="1" <?= ($status == "1" || $status == "") ? "checked" : "" ?>> 사용 <input type="radio" class="radio ml20" name="status" value="2" <?= ($status == "2") ? "checked" : "" ?> > 미사용</td>
			</tr>
			<tr>
				<th>제품키</th>
				<td>
					<input type="text" class="view_tbl_text" name="productkey" id="productkey" value="<?= encode_html_attribute($productkey) ?>" maxlength="100">
				</td>
			</tr>
			</tbody>
			</table>    
			</form>	
    		
    		<div class="button_warp tdr">
				<input type="button" class="btn_setting_01" value="저장" onclick="save_product_mobile()">
				<input type="button" class="btn_setting_02" value="취소" onclick="go_page('product_ios_mng.php')">    		
    		</div>
    	</div>
    	<!--  //CONTENTS WRAP -->  
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>