<?
	$top_menu = "product";
	$sub_menu = "order_android";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$orderidx = $_GET["orderidx"];
	
	if ($orderidx == "")
		error_back("잘못된 접근입니다.");
	
	check_number($orderidx);
	
    $db_main = new CDatabase_Main();
	$db_main2 = new CDatabase_Main2();
	$db_friend = new CDatabase_Friend();
	
	//주문정보조회
	$sql= "SELECT orderidx, useridx, a.productidx, orderno, a.money, a.status, a.writedate, canceldate, cancelleftcoin, ". 
			"disputed, couponidx, usercoin, a.special_discount, a.special_more, ".
			"CASE WHEN coin != 0 THEN coin ELSE 0 END AS amount, ". 
			"CASE WHEN coin != 0 THEN 'coin' ELSE 0 END AS producttype, basecoin	". 
			"FROM ( SELECT * FROM tbl_product_order_mobile WHERE orderidx=$orderidx ) a LEFT JOIN tbl_product_mobile b ON a.productidx = b.productidx";	
	$order = $db_main->getarray($sql);
	
	$orderidx = $order["orderidx"];
	$useridx = $order["useridx"];
	$productidx = $order["productidx"];
	$couponidx = $order["couponidx"];
	$orderno = $order["orderno"];
	$order_money = $order["money"];
	$order_status = $order["status"];
	$orderdate = $order["writedate"];
	$orderamount = $order["amount"];
	$orderproducttype = $order["producttype"];
	$canceldate = $order["canceldate"];
	$cancelleftcoin = $order["cancelleftcoin"];
	$disputed = $order["disputed"];	
	$usercoin = $order["usercoin"];
	$gift_coin = $order["gift_coin"];
	$special_discount = $order["special_discount"];
	$special_more = $order["special_more"];
                	
	if ($orderidx == "")
		error_back("존재하지 않는 주문입니다.");
	
	// 사용자정보 조회
	$sql = "SELECT userid, B.email, A.nickname, A.useridx, coin FROM tbl_user AS A JOIN tbl_user_ext AS B ON A.useridx=B.useridx WHERE A.useridx=$useridx";	
	$user = $db_main->getarray($sql);
	
	$useridx = $user["useridx"];
	$userid = $user["userid"];
	$email = $user["email"];
	$nickname = $user["nickname"];
	
	if($userid < 10)
		$photourl = "https://".WEB_HOST_NAME."/mobile/images/avatar_profile/guest_profile_".$userid.".png";
	else
	    $photourl = get_fb_pictureURL($userid,$client_accesstoken);
	
	$coin = $user["coin"];
		
	// 상품정보 조회
	$sql = "SELECT category, productname, description, imageurl, amount, money, status, writedate FROM tbl_product_mobile WHERE productidx=$productidx";	
	$product = $db_main->getarray($sql);
	
	$category = $product["category"];
	$productname = $product["productname"];
	$description = $product["description"];
	$imageurl = $product["imageurl"];
	$amount = $product["amount"];
	$product_money = $product["money"];
	$product_status = $product["status"];
	
	// 쿠폰 정보 조회
	$sql = "SELECT discount,coupon_more FROM tbl_coupon WHERE couponidx=$couponidx";
	$coupon = $db_main2->getarray($sql);
	
	// 접속 상태 여부
	$sql = "SELECT COUNT(*) FROM tbl_user_online_sync2 WHERE useridx=$useridx";
	$online_status = $db_friend->getvalue($sql);
		
	$db_main->end();
	$db_main2->end();
	$db_friend->end();
?>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
    	
		<!-- title_warp -->
	    <div class="title_wrap">
	    	<div class="title"><?= $top_menu_txt ?> &gt; 주문상세</div>
	    </div>
	    <!-- //title_warp -->
	    
	    <div class="h2_title">사용자정보</div>
		<div class="user_info_summary" onmouseover="className='user_info_summary_over'" onmouseout="className='user_info_summary'" onclick="view_user_dtl(<?= $useridx ?>)">
			<img src="<?= $photourl ?>" height="50" width="50" class="summary_user_image">
			<div class="sumary_username_wrap">
				<div class="summary_username">[<?= $useridx ?>] <?= $nickname ?></div>
				<div class="summary_user_info"><?= $email ?></div>
			</div>
			<div class="summary_user_coin"><?= number_format($coin) ?></div>	
		</div>
<?
    if ($productidx != "0")
    {
?>
	    	<div class="h2_title">상품정보</div>
			<div class="product_info_summary" onmouseover="className='product_info_summary_over'" onmouseout="className='product_info_summary'" onclick="view_product_android_dtl(<?= $productidx ?>)">
				<img src="<?= $imageurl ?>" width="50" height="50" class="summary_product_image">
				<div class="summary_productname">[<?= $productidx ?>] <?= $productname ?></div>
				<div class="summary_product_description"><?= $description ?></div>
			    		
				<div class="summary_item">
					<div class="summary_item_lbl">종류</div>
					<div class="summary_item_value"><?= ($category == "0") ? "코인" : "캐쉬" ?></div>
				</div>
			    		
				<div class="summary_item">
					<div class="summary_item_lbl">충전금액</div>
					<div class="summary_item_value"><?= number_format($amount) ?></div>
				</div>
			    		
				<div class="summary_item">
					<div class="summary_item_lbl">결제 금액</div>
					<div class="summary_item_value"><?= $product_money ?></div>
				</div>
			    		
				<div class="summary_item">
					<div class="summary_item_lbl">상태</div>
					<div class="summary_item_value">
						<?= ($product_status == "1") ? "사용" : "미사용" ?>
					</div>
				</div>
			</div>
<?
    }
?>

	<div class="h2_title">주문정보</div>
	<div class="h2_cont">
	    		
		<div class="summary_item">
			<div class="summary_item_lbl">상품종류</div>
			<div class="summary_item_value"><?= $orderproducttype ?></div>
		</div>
		    		
		<div class="summary_item">
			<div class="summary_item_lbl">주문금액</div>
			<div class="summary_item_value"><?= number_format($orderamount) ?></div>
		</div>
		    		
		<div class="summary_item">
			<div class="summary_item_lbl">선물발행 Chip</div>
			<div class="summary_item_value"><?= number_format($gift_coin) ?></div>
		</div>
		    		
		<div class="summary_item">
			<div class="summary_item_lbl">사용자 보유 칩</div>
			<div class="summary_item_value"><?= number_format($usercoin) ?> (주문 시점의 사용자 보유 칩 - 상품 칩 더해지기 이전)</div>
		</div>
		    		
		<div class="summary_item">
			<div class="summary_item_lbl">주문번호</div>
			<div class="summary_item_value"><?= $orderno ?></div>
		</div>
		    		
		<div class="summary_item">
			<div class="summary_item_lbl">페이스북 Credit</div>
			<div class="summary_item_value"><?= $order_facebookcredit ?></div>
		</div>
<?
	if ($special_discount != 0)
	{
?>
		<div class="summary_item">
			<div class="summary_item_lbl">Special Offer Discount</div>
			<div class="summary_item_value"><?= $special_discount ?> %</div>
		</div>
<?
	}	    		

	if ($special_more != 0)
	{
?>
		<div class="summary_item">
			<div class="summary_item_lbl">Special Offer More</div>
			<div class="summary_item_value"><?= $special_more ?> %</div>
		</div>
<?
	}	    		
?>	
		<div class="summary_item">
			<div class="summary_item_lbl">상태</div>
			<div class="summary_item_value">
<?
	switch ($order_status)
	{
		case 0:
			echo("진행중");
			break;
		case 1:
			echo("완료");
			break;
		case 2:
			echo("실패");
			break;
		case 3:
			echo("유효성 실패");
			break;
	}
?>
			</div>
		</div>
	    		
		<div class="summary_item">
			<div class="summary_item_lbl">주문일시</div>
			<div class="summary_item_value"><?= $orderdate ?></div>
		</div>
<?
    if ($order_status == "2")
    {
?>
		<div class="summary_item">
			<div class="summary_item_lbl">취소일시</div>
			<div class="summary_item_value"><?= $canceldate ?></div>
		</div>
		<div class="summary_item">
			<div class="summary_item_lbl">차감잔액</div>
			<div class="summary_item_value"><?= number_format($cancelleftcoin) ?></div>
		</div>
<?
    }
?>
	</div>
    
<?
	if ($couponidx != "0")
	{
?>		
    <div class="h2_title">쿠폰 사용 정보</div>
    <div class="h2_cont">
       	<div class="summary_item">
    		<div class="summary_item_lbl">Discount</div>
    		<div class="summary_item_value"><?= $coupon["discount"] ?> %</div>
    	</div>
    		
    	<div class="summary_item">
    		<div class="summary_item_lbl">More Chips</div>
    		<div class="summary_item_value"><?= $coupon["coupon_more"] ?> %</div>
	    </div>

    </div>
<?            
	}
?>  
	    	<div class="button_wrap tdr">
	    		<input type="button" class="btn_setting_01" value="목록" onclick="go_page('order_android_mng.php')">
	    	</div>
    	</div>
    	<!--  //CONTENTS WRAP -->
    	
    	<div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>