<?
	$top_menu = "pay_static";
	$sub_menu = "order_daily_stat";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$platform = ($_GET["platform"] == "") ? "ALL" : $_GET["platform"];
	$dayofweek = ($_GET["dayofweek"] == "") ? "0" : $_GET["dayofweek"];
	$is_sale = ($_GET["is_sale"] == "") ? "ALL" : $_GET["is_sale"];
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
		
	check_xss($startdate);
	check_xss($enddate);
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today, 29, "d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db_analysis = new CDatabase_Analysis();
	
	$platform_sql = "";
	
	if($platform != "ALL")
	{
	    $platform_sql = "AND platform=$platform ";
	}
	
	$week_sql = "";
	
	if($dayofweek != 0)
	{
	    $week_sql = " AND DAYOFWEEK(today) = $dayofweek ";
	}
	
	$is_sale_sql = "";
	
	if($is_sale != "ALL")
	{
	    $is_sale_sql = " AND is_sale = $is_sale ";
	}
	
	$sql = "SELECT today, SUM(basic_money) AS basic_money, SUM(coupon_money) AS coupon_money, SUM(offer_money) AS offer_money, ".
            "   ROUND(SUM(basic_buy_coin)/SUM(basic_base_coin)*100, 2) AS basic_more_rate, ".
            "   ROUND(SUM(coupon_buy_coin)/SUM(coupon_base_coin)*100, 2) AS coupon_more_rate, ".
            "   ROUND(SUM(offer_buy_coin)/SUM(offer_base_coin)*100, 2) AS offer_more_rate ".
            "FROM ( ".
            "   SELECT today, platform, ". 
            "       SUM(IF(category_name = 'basic', total_money, 0)) AS basic_money, ".  
            "       SUM(IF(category_name = 'basic', payer_cnt, 0)) AS basic_payer_cnt, ". 
            "       SUM(IF(category_name = 'basic', pay_cnt, 0)) AS basic_pay_cnt, ". 
            "       SUM(IF(category_name = 'basic', base_coin, 0)) AS basic_base_coin, ". 
            "       SUM(IF(category_name = 'basic', buy_coin, 0)) AS basic_buy_coin, ". 
            "       SUM(IF(category_name = 'coupon', total_money, 0)) AS coupon_money, ". 
            "       SUM(IF(category_name = 'coupon', payer_cnt, 0)) AS coupon_payer_cnt, ". 
            "       SUM(IF(category_name = 'coupon', pay_cnt, 0)) AS coupon_pay_cnt, ". 
            "       SUM(IF(category_name = 'coupon', base_coin, 0)) AS coupon_base_coin, ". 
            "       SUM(IF(category_name = 'coupon', buy_coin, 0)) AS coupon_buy_coin, ". 
            "       SUM(IF(category_name = 'offer', total_money, 0)) AS offer_money, ".	
            "       SUM(IF(category_name = 'offer', payer_cnt, 0)) AS offer_payer_cnt, ".
            "       SUM(IF(category_name = 'offer', pay_cnt, 0)) AS offer_pay_cnt, ".
            "       SUM(IF(category_name = 'offer', base_coin, 0)) AS offer_base_coin, ". 
            "       SUM(IF(category_name = 'offer', buy_coin, 0)) AS offer_buy_coin ".
            "   FROM tbl_order_daily_stat ".
            "   WHERE '$startdate' <= today AND today <= '$enddate' $platform_sql $week_sql $is_sale_sql ".
            "   GROUP BY today, platform ".
            ") t1 ".
            "GROUP BY today";
	$order_list = $db_analysis->gettotallist($sql);

	$db_analysis->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>

<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

	google.load("visualization", "1", {packages:["corechart"]});

    function drawChart() 
    {
    	var data0 = new google.visualization.DataTable();
        data0.addColumn('string', '날짜');
        data0.addColumn('number', '총 구매금액($)');
        data0.addRows([
<?
    for ($i=0; $i<sizeof($order_list); $i++)
	{
	    $today = $order_list[$i]["today"];
	    $basic_money = $order_list[$i]["basic_money"];
	    $coupon_money = $order_list[$i]["coupon_money"];
	    $offer_money = $order_list[$i]["offer_money"];
	    $total_money = $basic_money + $coupon_money + $offer_money;

		
	    echo("['".$today."',".$total_money."]");
		
	    if ($i != sizeof($order_list) - 1)
			echo(",");
	}
?>
        ]);
        
        var data1 = new google.visualization.DataTable();
        data1.addColumn('string', '날짜');
        data1.addColumn('number', '정상상품 구매금액($)');
        data1.addColumn('number', '쿠폰 구매금액($)');
        data1.addColumn('number', '오퍼 구매금액($)');
        data1.addRows([
<?
    for ($i=0; $i<sizeof($order_list); $i++)
	{
	    $today = $order_list[$i]["today"];
	    $basic_money = $order_list[$i]["basic_money"];
	    $coupon_money = $order_list[$i]["coupon_money"];
	    $offer_money = $order_list[$i]["offer_money"];

		
	    echo("['".$today."',".$basic_money.", ".$coupon_money.", ".$offer_money."]");
		
	    if ($i != sizeof($order_list) - 1)
			echo(",");
	}
?>
        ]);

        var data2 = new google.visualization.DataTable();
        data2.addColumn('string', '날짜');
        data2.addColumn('number', '정상상품 구매비중(%)');
        data2.addColumn('number', '쿠폰 구매비중(%)');
        data2.addColumn('number', '오퍼 구매비중(%)');
        data2.addRows([
<?
    for ($i=0; $i<sizeof($order_list); $i++)
	{
	    $today = $order_list[$i]["today"];
	    $basic_money = $order_list[$i]["basic_money"];
	    $coupon_money = $order_list[$i]["coupon_money"];
	    $offer_money = $order_list[$i]["offer_money"];
	    $total_money = $basic_money + $coupon_money + $offer_money;

	    $basic_rate = round($basic_money/$total_money*100, 2);
	    $coupon_rate = round($coupon_money/$total_money*100, 2);
	    $offer_rate = round($offer_money/$total_money*100, 2);
		
	    echo("['".$today."',".$basic_rate.", ".$coupon_rate.", ".$offer_rate."]");
		
	    if ($i != sizeof($order_list) - 1)
			echo(",");
	}
?>
        ]);

        var data3 = new google.visualization.DataTable();
        data3.addColumn('string', '날짜');
        data3.addColumn('number', '정상상품 more(%)');
        data3.addColumn('number', '쿠폰 more(%)');
        data3.addColumn('number', '오퍼 more(%)');
        data3.addRows([
<?
    for ($i=0; $i<sizeof($order_list); $i++)
	{
	    $today = $order_list[$i]["today"];
	    $basic_more_rate = $order_list[$i]["basic_more_rate"];
	    $coupon_more_rate = $order_list[$i]["coupon_more_rate"];
	    $offer_more_rate = $order_list[$i]["offer_more_rate"];

	    echo("['".$today."',".$basic_more_rate.", ".$coupon_more_rate.", ".$offer_more_rate."]");
		
	    if ($i != sizeof($order_list) - 1)
			echo(",");
	}
?>
        ]);
            
        var options1 = {
            width:1050,                         
            height:470,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:300}
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div0'));
        chart.draw(data0, options1);
        
        var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
        chart.draw(data1, options1);

        var chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
        chart.draw(data2, options1);

        var chart = new google.visualization.LineChart(document.getElementById('chart_div3'));
        chart.draw(data3, options1);             
    }
        
	google.setOnLoadCallback(drawChart);	

	function change_platform(term)
	{
		var search_form = document.search_form;
		
		var term_all = document.getElementById("term_all");
		var term_fb = document.getElementById("term_fb");
		var term_ios = document.getElementById("term_ios");
		var term_and = document.getElementById("term_and");
		var term_ama = document.getElementById("term_ama");

		document.search_form.platform.value = term;
		
		if (term == "ALL")
		{
			term_all.className="btn_schedule_select";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 0)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule_select";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 1)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule_select";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 2)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule_select";
			term_ama.className="btn_schedule";
		}
		else if (term == 3)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule_select";
		}

		search_form.submit();
	}

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}
    
    function check_sleeptime()
    {
    }
</script>
		<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; 기간별 통계</div>
	    		<form name="search_form" id="search_form"  method="get" action="order_daily_stat.php">
	    		<input type="hidden" name="platform" id="platform" value="<?= $platform ?>" />
	    		<div class="search_box">
	    			요일&nbsp;
					<select name="dayofweek" id="dayofweek">										
						<option value="0" <?= ($dayofweek=="0") ? "selected" : "" ?>>전체</option>
						<option value="1" <?= ($dayofweek=="1") ? "selected" : "" ?>>일요일</option>                       
						<option value="2" <?= ($dayofweek=="2") ? "selected" : "" ?>>월요일</option>					
						<option value="3" <?= ($dayofweek=="3") ? "selected" : "" ?>>화요일</option>					
						<option value="4" <?= ($dayofweek=="4") ? "selected" : "" ?>>수요일</option>
						<option value="5" <?= ($dayofweek=="5") ? "selected" : "" ?>>목요일</option>
						<option value="6" <?= ($dayofweek=="6") ? "selected" : "" ?>>금요일</option>
						<option value="7" <?= ($dayofweek=="7") ? "selected" : "" ?>>토요일</option>					
					</select>&nbsp;
					
					시즌세일여부&nbsp;
					<select name="is_sale" id="is_sale">										
						<option value="ALL" <?= ($is_sale=="ALL") ? "selected" : "" ?>>전체</option>
						<option value="0" <?= ($is_sale=="0") ? "selected" : "" ?>>시즌세일기간아님</option>
						<option value="1" <?= ($is_sale=="1") ? "selected" : "" ?>>시즌세일기간</option>                       				
					</select>&nbsp;
	    		
	    			<input type="button" class="<?= ($platform == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_all" onclick="change_platform('ALL')" value="통합" />
	    			<input type="button" class="<?= ($platform == "0") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_fb" onclick="change_platform('0')" value="Facebook" />
	    			<input type="button" class="<?= ($platform == "1") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ios" onclick="change_platform('1')" value="iOS" />
	    			<input type="button" class="<?= ($platform == "2") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_and" onclick="change_platform('2')" value="Android" />
	    			<input type="button" class="<?= ($platform == "3") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ama" onclick="change_platform('3')" value="Amazon" />
	    			 
	    			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
		            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
					<input type="button" class="btn_search" value="검색" onclick="search()" />
	    		</div>
	    		</form>
	        </div>
	    	<!-- //title_warp -->
	    	
            <div class="search_result">
            	<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다
            </div>
	    	
	    	<div id="chart_div0" style="height:480px; min-width: 500px"></div>
	    	<div id="chart_div1" style="height:480px; min-width: 500px"></div>
	    	<div id="chart_div2" style="height:480px; min-width: 500px"></div>
	    	<div id="chart_div3" style="height:480px; min-width: 500px"></div>

    	</div>
    	<!--  //CONTENTS WRAP -->
    	
    	<div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>