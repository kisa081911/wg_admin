<?
	$top_menu = "pay_static";
	$sub_menu = "daily_repayment_stats";
	
	$pagename = "daily_repayment_stats.php";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$search_sdate = ($_GET["search_sdate"] == "") ? date("Y-m-d", strtotime("-7 day")) : $_GET["search_sdate"];	
	$search_edate = ($_GET["search_edate"] == "") ? $today : $_GET["search_edate"];
	$alllist = $_GET["alllist"];
	$usertype_value = $_GET["userlist"];
	$std_div_date = ($_GET["std_div_date"] == "") ? 0: $_GET["std_div_date"];
	$new_user = ($_GET["new_user"] == "") ? 0: $_GET["new_user"];
	$platform = ($_GET["platform"] == "") ? 0: $_GET["platform"];

	if(is_array($usertype_value))
	{
		$sel_userlist = implode(",", $usertype_value);
	}
	
	$mod_str = "";
	
	if($sel_userlist != "")
		$mod_str = " AND t1.useridx % 10 IN ($sel_userlist) ";
	
	if($alllist == "all")
		$mod_str = " AND 1=1 ";	
	
	if($std_div_date == "0")
		$std_div_str = " AND 1=1 ";	
	else if($std_div_date == "1")
		$std_div_str = " AND t2.createdate > DATE_SUB(DATE_FORMAT(writedate, '%Y-%m-%d 00:00:00'), INTERVAL 28 DAY) ";
	else if($std_div_date == "2")
		$std_div_str = " AND t2.createdate <= DATE_SUB(DATE_FORMAT(writedate, '%Y-%m-%d 00:00:00'), INTERVAL 28 DAY) ";
	
	$db_slave_main = new CDatabase_Main();
	
	$str_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}
	
	if($new_user == 0)
	{		
		if($platform == 0)
		{
			$sql = "SELECT today, COUNT(useridx) AS uniq_user, ROUND(SUM(sum_money), 2) AS total_money, SUM(uniq_7day_user) AS uniq_7day_user, ROUND(SUM(total_7day_money),2) AS total_7day_money ".
					"FROM ".
					"	( ".
					"		SELECT ". 
					"		DATE_FORMAT(writedate, '%Y-%m-%d') AS today, ".
					"		t1.useridx AS useridx, ".
					"		SUM(facebookcredit)/10 AS sum_money, ".
					"		( ".
					"			SELECT IFNULL(SUM(facebookcredit), 0)/10 ". 
					"			FROM tbl_product_order ". 
					"			WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND useridx = t1.useridx ".
					"			AND DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00') , INTERVAL 1 DAY) <= writedate AND writedate < DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 8 DAY) ".
					"		) AS total_7day_money, ".
					"		( ".
					"			SELECT COUNT(DISTINCT useridx) ". 
					"			FROM tbl_product_order ". 
					"			WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND useridx = t1.useridx ".
					"			AND DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 1 DAY) <= writedate AND writedate < DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 8 DAY) ".
					"		) AS uniq_7day_user ".
					"		FROM tbl_product_order t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx ".
					"		WHERE t1.status IN (1, 3) AND t1.useridx > $str_useridx $mod_str AND '$search_sdate 00:00:00' <= writedate AND writedate <= '$search_edate 23:59:59'	". 
					"		$std_div_str ".
					"	GROUP BY today, t1.useridx ".
					") t3 ".
					"GROUP BY today ORDER BY today DESC; ";
		}
		else if($platform == 1)
		{		
			$sql = "SELECT today, COUNT(useridx) AS uniq_user, ROUND(SUM(sum_money), 2) AS total_money, SUM(uniq_7day_user) AS uniq_7day_user, ROUND(SUM(total_7day_money),2) AS total_7day_money ".
					"FROM ".
					"	( ".
					"		SELECT ".
					"		DATE_FORMAT(writedate, '%Y-%m-%d') AS today, ".
					"		t1.useridx AS useridx, ".
					"		SUM(facebookcredit)/10 AS sum_money, ".
					"		( ".
					"			SELECT IFNULL(SUM(facebookcredit), 0)/10 ".
					"			FROM tbl_product_order_mobile ".
					"			WHERE STATUS = 1 AND useridx > $str_useridx AND useridx = t1.useridx ".
					"			AND DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00') , INTERVAL 1 DAY) <= writedate AND writedate < DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 8 DAY) ".
					"		) AS total_7day_money, ".
					"		( ".
					"			SELECT COUNT(DISTINCT useridx) ".
					"			FROM tbl_product_order_mobile ".
					"			WHERE STATUS = 1 AND useridx > $str_useridx AND useridx = t1.useridx ".
					"			AND DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 1 DAY) <= writedate AND writedate < DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 8 DAY) ".
					"		) AS uniq_7day_user ".
					"		FROM tbl_product_order_mobile t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx ".
					"		WHERE t1.status = 1 AND t1.useridx > $str_useridx $mod_str AND '$search_sdate 00:00:00' <= writedate AND writedate <= '$search_edate 23:59:59'	".
					"		$std_div_str ".
					"	GROUP BY today, t1.useridx ".
					") t3 ".
					"GROUP BY today ORDER BY today DESC; ";
		}
	}
	else if($new_user == 1)
	{
		if($platform == 0)
		{
			$sql = "SELECT today, COUNT(useridx) AS uniq_user, ROUND(SUM(sum_money), 2) AS total_money, SUM(uniq_7day_user) AS uniq_7day_user, ROUND(SUM(total_7day_money),2) AS total_7day_money, SUM(new_user) AS new_user ".
					"FROM ".
					"	( ".
					"		SELECT ".
					"		DATE_FORMAT(writedate, '%Y-%m-%d') AS today, ".
					"		t1.useridx AS useridx, ".
					"		SUM(facebookcredit)/10 AS sum_money, ".
					"		( ".
					"			SELECT IFNULL(SUM(facebookcredit), 0)/10 ".
					"			FROM tbl_product_order ".
					"			WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND useridx = t1.useridx ".
					"			AND DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00') , INTERVAL 1 DAY) <= writedate AND writedate < DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 8 DAY) ".
					"		) AS total_7day_money, ".
					"		( ".
					"			SELECT COUNT(DISTINCT useridx) ".
					"			FROM tbl_product_order ".
					"			WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND useridx = t1.useridx ".
					"			AND DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 1 DAY) <= writedate AND writedate < DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 8 DAY) ".
					"		) AS uniq_7day_user, ".
					"		( ".
					"			SELECT COUNT(*) FROM tbl_user WHERE useridx = t1.useridx AND DATE_FORMAT(createdate, '%Y-%m-%d') = DATE_FORMAT(writedate, '%Y-%m-%d') ".
					"		)  AS new_user ".
					"		FROM tbl_product_order t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx ".
					"		WHERE t1.status IN (1, 3) AND t1.useridx > $str_useridx $mod_str AND '$search_sdate 00:00:00' <= writedate AND writedate <= '$search_edate 23:59:59'	".
					"		$std_div_str ".
					"	GROUP BY today, t1.useridx ".
					") t3 ".
					"GROUP BY today, new_user HAVING new_user > 0 ORDER BY today DESC; ";
		}
		else if($platform == 1)
		{		
			$sql = "SELECT today, COUNT(useridx) AS uniq_user, ROUND(SUM(sum_money), 2) AS total_money, SUM(uniq_7day_user) AS uniq_7day_user, ROUND(SUM(total_7day_money),2) AS total_7day_money, SUM(new_user) AS new_user ".
					"FROM ".
					"	( ".
					"		SELECT ".
					"		DATE_FORMAT(writedate, '%Y-%m-%d') AS today, ".
					"		t1.useridx AS useridx, ".
					"		SUM(facebookcredit)/10 AS sum_money, ".
					"		( ".
					"			SELECT IFNULL(SUM(facebookcredit), 0)/10 ".
					"			FROM tbl_product_order_mobile ".
					"			WHERE STATUS = 1 AND useridx > $str_useridx AND useridx = t1.useridx ".
					"			AND DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00') , INTERVAL 1 DAY) <= writedate AND writedate < DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 8 DAY) ".
					"		) AS total_7day_money, ".
					"		( ".
					"			SELECT COUNT(DISTINCT useridx) ".
					"			FROM tbl_product_order_mobile ".
					"			WHERE STATUS = 1 AND useridx > $str_useridx AND useridx = t1.useridx ".
					"			AND DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 1 DAY) <= writedate AND writedate < DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 8 DAY) ".
					"		) AS uniq_7day_user, ".
					"		( ".
					"			SELECT COUNT(*) FROM tbl_user WHERE useridx = t1.useridx AND DATE_FORMAT(createdate, '%Y-%m-%d') = DATE_FORMAT(writedate, '%Y-%m-%d') ".
					"		)  AS new_user ".
					"		FROM tbl_product_order_mobile t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx ".
					"		WHERE t1.status = 1 AND t1.useridx > $str_useridx $mod_str AND '$search_sdate 00:00:00' <= writedate AND writedate <= '$search_edate 23:59:59'	".
					"		$std_div_str ".
					"	GROUP BY today, t1.useridx ".
					") t3 ".
					"GROUP BY today, new_user HAVING new_user > 0 ORDER BY today DESC; ";
		}
	}
	else if($new_user == 2)
	{
		if($platform == 0)
		{
			$sql = "SELECT today, COUNT(useridx) AS uniq_user, ROUND(SUM(sum_money), 2) AS total_money, SUM(uniq_7day_user) AS uniq_7day_user, ROUND(SUM(total_7day_money),2) AS total_7day_money, SUM(new_user) AS new_user ".
					"FROM ".
					"	( ".
					"		SELECT ".
					"		DATE_FORMAT(writedate, '%Y-%m-%d') AS today, ".
					"		t1.useridx AS useridx, ".
					"		SUM(facebookcredit)/10 AS sum_money, ".
					"		( ".
					"			SELECT IFNULL(SUM(facebookcredit), 0)/10 ".
					"			FROM tbl_product_order ".
					"			WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND useridx = t1.useridx ".
					"			AND DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00') , INTERVAL 1 DAY) <= writedate AND writedate < DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 8 DAY) ".
					"		) AS total_7day_money, ".
					"		( ".
					"			SELECT COUNT(DISTINCT useridx) ".
					"			FROM tbl_product_order ".
					"			WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND useridx = t1.useridx ".
					"			AND DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 1 DAY) <= writedate AND writedate < DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 8 DAY) ".
					"		) AS uniq_7day_user, ".
					"		( ".
					"			SELECT COUNT(*) FROM tbl_user WHERE useridx = t1.useridx AND DATE_FORMAT(createdate, '%Y-%m-%d') = DATE_FORMAT(writedate, '%Y-%m-%d') ".
					"		)  AS new_user ".
					"		FROM tbl_product_order t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx ".
					"		WHERE t1.status IN (1, 3) AND t1.useridx > $str_useridx $mod_str AND '$search_sdate 00:00:00' <= writedate AND writedate <= '$search_edate 23:59:59'	".
					"		$std_div_str ".
					"	GROUP BY today, t1.useridx ".
					") t3 ".
					"GROUP BY today, new_user HAVING new_user = 0 ORDER BY today DESC; ";
		}
		else if($platform == 1)
		{		
			$sql = "SELECT today, COUNT(useridx) AS uniq_user, ROUND(SUM(sum_money), 2) AS total_money, SUM(uniq_7day_user) AS uniq_7day_user, ROUND(SUM(total_7day_money),2) AS total_7day_money, SUM(new_user) AS new_user ".
					"FROM ".
					"	( ".
					"		SELECT ".
					"		DATE_FORMAT(writedate, '%Y-%m-%d') AS today, ".
					"		t1.useridx AS useridx, ".
					"		SUM(facebookcredit)/10 AS sum_money, ".
					"		( ".
					"			SELECT IFNULL(SUM(facebookcredit), 0)/10 ".
					"			FROM tbl_product_order_mobile ".
					"			WHERE STATUS = 1 AND useridx > $str_useridx AND useridx = t1.useridx ".
					"			AND DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00') , INTERVAL 1 DAY) <= writedate AND writedate < DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 8 DAY) ".
					"		) AS total_7day_money, ".
					"		( ".
					"			SELECT COUNT(DISTINCT useridx) ".
					"			FROM tbl_product_order_mobile ".
					"			WHERE STATUS = 1 AND useridx > $str_useridx AND useridx = t1.useridx ".
					"			AND DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 1 DAY) <= writedate AND writedate < DATE_ADD(DATE_FORMAT( t1.writedate, '%Y-%m-%d 00:00:00'), INTERVAL 8 DAY) ".
					"		) AS uniq_7day_user, ".
					"		( ".
					"			SELECT COUNT(*) FROM tbl_user WHERE useridx = t1.useridx AND DATE_FORMAT(createdate, '%Y-%m-%d') = DATE_FORMAT(writedate, '%Y-%m-%d') ".
					"		)  AS new_user ".
					"		FROM tbl_product_order_mobile t1 JOIN tbl_user t2 ON t1.useridx = t2.useridx ".
					"		WHERE t1.status = 1 AND t1.useridx > $str_useridx $mod_str AND '$search_sdate 00:00:00' <= writedate AND writedate <= '$search_edate 23:59:59'	".
					"		$std_div_str ".
					"	GROUP BY today, t1.useridx ".
					") t3 ".
					"GROUP BY today, new_user HAVING new_user = 0 ORDER BY today DESC; ";
		}
	}

	$data_list = $db_slave_main->gettotallist($sql);	
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    
    $(function() {
        $("#search_sdate").datepicker({ });
    });

    $(function() {
        $("#search_edate").datepicker({ });
    });
</script>
<!-- CONTENTS WRAP -->
	<div class="contents_wrap">        
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<!-- title_warp -->
			<div class="title_wrap">
				<div class="title"><?= $top_menu_txt ?> &gt; 재결제 현황</div>
				<div class="search_box">
                    	Useridx%10 :
                    	<input type="checkbox"  name="alllist"  value="all"  <?= ($alllist == "all") ? "checked" : "" ?>/>ALL</br>
						<tr>				
				
<?	
	for ($i=0; $i<10; $i++)
	{
?>
						
							<td><input type="checkbox" name="userlist[]"  value="<?= $i?>"/><?= $i ?></td>			
						
<?		
		
	}
?>
						</tr>
	<script>
						var sel_userlist_array = new Array();						
						var sel_userlist = "<?=$sel_userlist?>";
						
						sel_userlist_array = sel_userlist.split(',');
														
						$("input:checkbox[name='userlist[]']").each(function(index){
						    if(sel_userlist_array.indexOf($(this).val()) > -1){
						        $(this).attr("checked", true);						        
						    }
						});

						$("input:checkbox[name='alllist']").click(function(){
							if($("input:checkbox[name='alllist']").is(":checked"))
							{
								$("input:checkbox[name='userlist[]']").attr("checked", true);
							}
							else
							{	
								$("input:checkbox[name='userlist[]']").attr("checked", false);
							}								
						});

						$("input:checkbox[name='userlist[]']").click(function(){
							if($("input:checkbox[name='userlist[]']").is(":unchecked"))
							{
								$("input:checkbox[name='alllist']").attr("checked", false);
							}							
						});
	</script>
				&nbsp;&nbsp;&nbsp;&nbsp;
				platfrom : 
				<select name="platform" id="platform">					
					<option value="0" <?= ($platform=="0" || $platform == "") ? "selected" : "" ?>>Web</option>
					<option value="1" <?= ($platform=="1") ? "selected" : "" ?>>Mobile</option>
				</select>
				<br>
				&nbsp;&nbsp;
				가입 기준 : 
				<select name="std_div_date" id="std_div_date">					
					<option value="0" <?= ($std_div_date=="0" || $std_div_date == "") ? "selected" : "" ?>>전체</option>
					<option value="1" <?= ($std_div_date=="1") ? "selected" : "" ?>>가입28일 미만</option>
					<option value="2" <?= ($std_div_date=="2") ? "selected" : "" ?>>가입28일 이상</option>
				</select>
				&nbsp;&nbsp;
				신규 기준 : 
				<select name="new_user" id="new_user">					
					<option value="0" <?= ($new_user=="0" || $new_user == "") ? "selected" : "" ?>>전체</option>
					<option value="1" <?= ($new_user=="1") ? "selected" : "" ?>>신규사용자</option>
					<option value="2" <?= ($new_user=="2") ? "selected" : "" ?>>신규사용자 제외</option>					
				</select>							
				<input type="text" class="search_text" id="search_sdate" name="search_sdate" value="<?= $search_sdate ?>" style="width:65px" maxlength="10" onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="search_edate" name="search_edate" value="<?= $search_edate ?>" style="width:65px" maxlength="10" onkeypress="search_press(event)" />		 
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />				
				</div>
			</div>

		<!-- //title_warp -->

		<div class="search_result">
			<span><?= $search_sdate ?></span> ~ <span><?= $search_edate ?></span> 통계입니다
		</div>
	
		<div id="tab_content_1">
            <table class="tbl_list_basic1">
	            <colgroup>
	                <col width="200">
	                <col width="">
	                <col width="">
	                <col width="">
	                <col width="">
	                <col width="">
	                <col width="">                
	            </colgroup>
	            <thead>
		            <tr>
		                <th>날짜</th>
		                <th class="tdc">결제 총액(A)</th>
		                <th class="tdc">결제자 수(B)</th>                
		                <th class="tdc">D+1~D+7 결제 총액(C)</th>
		                <th class="tdc">D+1~D+7 결제자 수(D)</th>
		                <th class="tdc">C/A</th>
		                <th class="tdc">D/B</th>
		            </tr>
	            </thead>
	            <tbody>
<?
			$sum_uniq_user = 0;
			$sum_total_money = 0;
			$sum_7day_uniq_user = 0;
			$sum_7day_total_money = 0;				
			
			for($i=0; $i<sizeof($data_list); $i++)
			{
				$today = $data_list[$i]["today"];
				$uniq_user = $data_list[$i]["uniq_user"];
				$total_money = $data_list[$i]["total_money"];
				$uniq_7day_user = $data_list[$i]["uniq_7day_user"];
				$total_7day_money = $data_list[$i]["total_7day_money"];
				
				$cnt_user_rate = ($uniq_user == 0) ? "0" : ($uniq_7day_user/$uniq_user) * 100;
				$cnt_money_rate = ($total_money == 0) ? "0" : ($total_7day_money/$total_money) * 100;
				
				$sum_uniq_user += $uniq_user;
				$sum_total_money += $total_money;
				$sum_7day_uniq_user += $uniq_7day_user;
				$sum_7day_total_money += $total_7day_money;
?>
				
				<tr>
					<td class="tdc point_title"><?= $today ?></td>
					<td class="tdc">$ <?= number_format($total_money) ?></td>
					<td class="tdc"><?= number_format($uniq_user) ?></td>					
					<td class="tdc">$ <?= number_format($total_7day_money) ?></td>
					<td class="tdc"><?= number_format($uniq_7day_user) ?></td>					
					<td class="tdc"><?= round($cnt_money_rate, 2) ?> %</td>
					<td class="tdc"><?= round($cnt_user_rate, 2) ?> %</td>
				</tr>
				
<?
			}			
?>
				<tr>
					<td class="tdc point_title">Total</td>
					<td class="tdc">$ <?= number_format($sum_total_money) ?></td>
					<td class="tdc"><?= number_format($sum_uniq_user) ?></td>					
					<td class="tdc">$ <?= number_format($sum_7day_total_money) ?></td>
					<td class="tdc"><?= number_format($sum_7day_uniq_user) ?></td>
					<td class="tdc"><?= ($sum_total_money == 0) ? "0" : round(($sum_7day_total_money/$sum_total_money) * 100, 2) ?> %</td>					
					<td class="tdc"><?= ($sum_uniq_user == 0) ? "0" : round(($sum_7day_uniq_user/$sum_uniq_user) * 100, 2) ?> %</td>					
				</tr>
	            </tbody>
			</table>
		</div>		        
     </form>	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>