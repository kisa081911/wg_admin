<?
	$top_menu = "pay_static";
	$sub_menu = "pay_refund";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
	
	check_xss($startdate);
	check_xss($enddate);
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today,15,"d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db = new CDatabase_Main();
    		
	$sql = "SELECT DATE_FORMAT(canceldate, '%Y-%m-%d') AS today, ROUND(SUM(facebookcredit)/10, 1) AS cancel_money, COUNT(*) AS cancel_count, COUNT(DISTINCT useridx) AS cancel_usercount ".
			"FROM tbl_product_order ".
			"WHERE canceldate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' AND `status` = 2 AND useridx > 20000 AND disputeclearreason = 'chargeback' ".
			"GROUP BY DATE_FORMAT(canceldate, '%Y-%m-%d')";
	$chargeback_list = $db->gettotallist($sql);
	
	$sql = "SELECT DATE_FORMAT(canceldate, '%Y-%m-%d') AS today, ROUND(SUM(facebookcredit)/10, 1) AS cancel_money, COUNT(*) AS cancel_count, COUNT(DISTINCT useridx) AS cancel_usercount ".
			"FROM tbl_product_order ".
			"WHERE canceldate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' AND `status` = 2 AND useridx > 20000 AND disputeclearreason = 'refund' ".
			"GROUP BY DATE_FORMAT(canceldate, '%Y-%m-%d')";
	$refund_list = $db->gettotallist($sql);
	
	$sql = "SELECT DATE_FORMAT(canceldate, '%Y-%m-%d') AS today, ROUND(SUM(facebookcredit)/10, 1) AS chargeback_90day_money, COUNT(*) AS chargeback_90day_count, COUNT(DISTINCT useridx) AS chargeback_90day_usercount ".
			"FROM tbl_product_order ".
			"WHERE canceldate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' AND `status` = 3 AND useridx > 20000 AND disputeclearreason = 'chargeback' ".
			"GROUP BY DATE_FORMAT(canceldate, '%Y-%m-%d')";
	$chargeback_90day_list = $db->gettotallist($sql);
	
	$chargeback_cancel_money_list = array();
	$chargeback_cancel_count_list = array();
	$chargeback_cancel_usercount_list = array();
	
	$refund_cancel_money_list = array();
	$refund_cancel_count_list = array();
	$refund_cancel_usercount_list = array();
	
	$chargeback_90day_cancel_money_list = array();
	$chargeback_90day_cancel_count_list = array();
	$chargeback_90day_cancel_usercount_list = array();
	
	$date_list = array();
	
	// chargeback
	$list_pointer = sizeof($chargeback_list);
		
	$date_pointer = $enddate;
	
	for ($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
	{
		$chargeback_today = $chargeback_list[$list_pointer-1]["today"];
		
		if (get_diff_date($date_pointer, $chargeback_today, "d") == 0)
		{
			$chargeback = $chargeback_list[$list_pointer-1];
			
			$chargeback_cancel_money_list[$i] = $chargeback["cancel_money"];
			$chargeback_cancel_count_list[$i] = $chargeback["cancel_count"];
			$chargeback_cancel_usercount_list[$i] = $chargeback["cancel_usercount"];
			$date_list[$i] = $date_pointer;
			
			$list_pointer--;
		}
		else 
		{
			$chargeback_cancel_money_list[$i] = 0;
			$chargeback_cancel_count_list[$i] = 0;
			$chargeback_cancel_usercount_list[$i] = 0;
			$date_list[$i] = $date_pointer;				
		}		
		
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	// refund
	$list_pointer = sizeof($refund_list);
		
	$date_pointer = $enddate;
	
	for ($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
	{
		$refund_today = $refund_list[$list_pointer-1]["today"];
		
		if (get_diff_date($date_pointer, $refund_today, "d") == 0)
		{
			$refund = $refund_list[$list_pointer-1];
			
			$refund_cancel_money_list[$i] = $refund["cancel_money"];
			$refund_cancel_count_list[$i] = $refund["cancel_count"];
			$refund_cancel_usercount_list[$i] = $refund["cancel_usercount"];
			$list_pointer--;
		}
		else 
		{
			$refund_cancel_money_list[$i] = 0;
			$refund_cancel_count_list[$i] = 0;
			$refund_cancel_usercount_list[$i] = 0;
		}
		
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	// chaegeback 90day
	$list_pointer = sizeof($chargeback_90day_list);
	
	$date_pointer = $enddate;
	
	for ($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
	{
		$chargeback_90day_today = $chargeback_90day_list[$list_pointer-1]["today"];
	
		if (get_diff_date($date_pointer, $chargeback_90day_today, "d") == 0)
		{
			$chargeback = $chargeback_90day_list[$list_pointer-1];
				
			$chargeback_90day_cancel_money_list[$i] = $chargeback["chargeback_90day_money"];
			$chargeback_90day_cancel_count_list[$i] = $chargeback["chargeback_90day_count"];
			$chargeback_90day_cancel_usercount_list[$i] = $chargeback["chargeback_90day_usercount"];
			$list_pointer--;
		}
		else
		{
			$chargeback_90day_cancel_money_list[$i] = 0;
			$chargeback_90day_cancel_count_list[$i] = 0;
			$chargeback_90day_cancel_usercount_list[$i] = 0;
		}
	
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}

	$db->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="/js/ui/ui.core.js"></script>
<script type="text/javascript" src="/js/ui/ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

	google.load("visualization", "1", {packages:["corechart"]});

    function drawChart() 
    {
        // cancel money
        var data1 = new google.visualization.DataTable();
        data1.addColumn('string', '날짜');
        data1.addColumn('number', 'chargeback 금액($)');
        data1.addColumn('number', 'refund 금액($)');
        data1.addColumn('number', 'chargeback 90일 금액($)');
        data1.addRows([
<?
	for ($i=sizeof($date_list); $i>0; $i--)
	{
		$_chargeback_cancel_money = $chargeback_cancel_money_list[$i-1];
		$_refund_cancel_money = $refund_cancel_money_list[$i-1];
		$_chargeback_90day_cancel_money = $chargeback_90day_cancel_money_list[$i-1];
        $_date = $date_list[$i-1];
		
		echo("['".$_date."',".$_chargeback_cancel_money.",".$_refund_cancel_money.",".$_chargeback_90day_cancel_money."]");
		
		if ($i > 1)
			echo(",");
	}
?>
        ]);

        // cancel count
        var data2 = new google.visualization.DataTable();
        data2.addColumn('string', '날짜');
        data2.addColumn('number', 'chargeback 횟수');
        data2.addColumn('number', 'refund 횟수');
        data2.addColumn('number', 'chargeback 90일  횟수');
        data2.addRows([
<?
	for ($i=sizeof($date_list); $i>0; $i--)
	{
		$_chargeback_cancel_count = $chargeback_cancel_count_list[$i-1];
		$_refund_cancel_count = $refund_cancel_count_list[$i-1];
		$_chargeback_90day_cancel_count = $chargeback_90day_cancel_count_list[$i-1];
        $_date = $date_list[$i-1];
		
		echo("['".$_date."',".$_chargeback_cancel_count.",".$_refund_cancel_count.",".$_chargeback_90day_cancel_count."]");
		
		if ($i > 1)
			echo(",");
	}
?>
        ]);

        // cancel usercount
        var data3 = new google.visualization.DataTable();
        data3.addColumn('string', '날짜');
        data3.addColumn('number', 'chargeback 사용자수(명)');
        data3.addColumn('number', 'refund 사용자수(명)');
        data3.addColumn('number', 'chargeback 90일 사용자수(명)');
        data3.addRows([
<?
	for ($i=sizeof($date_list); $i>0; $i--)
	{
		$_chargeback_cancel_usercount = $chargeback_cancel_usercount_list[$i-1];
		$_refund_cancel_usercount = $refund_cancel_usercount_list[$i-1];
		$_chargeback_90day_cancel_usercount = $chargeback_90day_cancel_usercount_list[$i-1];
        $_date = $date_list[$i-1];
		
		echo("['".$_date."',".$_chargeback_cancel_usercount.",".$_refund_cancel_usercount.",".$_chargeback_90day_cancel_usercount."]");
		
		if ($i > 1)
			echo(",");
	}
?>
        ]);
    
        var options1 = {
            width:1050,                         
            height:470,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:300}
        };
        
        var options2 = {
            width:1050,                         
            height:470,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:300}
        };
        
        var options3 = {
            width:1050,                         
            height:470,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:300}
        };
                        
        var chart1 = new google.visualization.LineChart(document.getElementById('chart_div1'));
        var chart2 = new google.visualization.LineChart(document.getElementById('chart_div2'));
        var chart3 = new google.visualization.LineChart(document.getElementById('chart_div3'));
        
        chart1.draw(data1, options1);
        chart2.draw(data2, options2);
        chart3.draw(data3, options3);
    }
        
	google.setOnLoadCallback(drawChart);	

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}
</script>
		<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; 기간별 통계</div>
	    		<form name="search_form" id="search_form"  method="get" action="pay_refund.php">
	    		<div class="search_box">
	    			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
		            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
		             <input type="button" class="btn_search" value="검색" onclick="search()" />
	    		</div>
	    		</form>
	        </div>
	    	<!-- //title_warp -->
	    	
            <div class="search_result">
            	<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span>통계입니다
            </div>
	    	
	    	<div class="h2_title">[환불 금액]</div>
	    	<div id="chart_div1" style="height:480px; min-width: 500px"></div>
	    	<div class="h2_title">[환불 횟수]</div>
	    	<div id="chart_div2" style="height:480px; min-width: 500px"></div>
	    	<div class="h2_title">[환불 사용자수]</div>
	    	<div id="chart_div3" style="height:480px; min-width: 500px"></div>

    	</div>
    	<!--  //CONTENTS WRAP -->
    	
    	<div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>