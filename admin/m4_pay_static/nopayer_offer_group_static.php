<?
	$top_menu = "pay_static";
	$sub_menu = "nopayer_offer_group_static";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_type = ($_GET["search_type"] == "") ? "" : $_GET["search_type"];
	
	if($_GET["start_date"] == "")
		$search_sdate = date("Y-m-d",strtotime("-13 days"));
	else
		$search_sdate = $_GET["start_date"];
	
	if($_GET["end_date"] == "")
		$search_edate = date("Y-m-d");
	else
		$search_edate = $_GET["end_date"];
	
	$start_date = $search_date;
	
	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT  today ".
				" , groupidx ".
				" , productidx ".
				" , usercount ".
				" , offerbuycount ".
				" , offerbuyusercount ".
				" , (SELECT COUNT(*) FROM tbl_nopayer_stat_daily WHERE today = t1.today AND groupidx != 0  GROUP BY today) AS today_rowcount ".
				" , (SELECT COUNT(*) FROM tbl_nopayer_stat_daily WHERE today = t1.today AND t1.groupidx = groupidx) AS group_rowcount ".
			" FROM tbl_nopayer_stat_daily t1 WHERE  today BETWEEN '$search_sdate' AND '$search_edate' AND groupidx != 0 ORDER BY today DESC , groupidx ASC, productidx ASC";
	
	$nopayer_static_group_list = $db_analysis->gettotallist($sql);
	
	$db_analysis->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#start_date").datepicker({ });
		$("#end_date").datepicker({ });
	});
	
	function search()
	{
		var search_form = document.search_form;
	    
		if (search_form.start_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.start_date.focus();
	    	return;
		} 
	
		if (search_form.end_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.end_date.focus();
	    	return;
		} 
	
		search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 비결제자 오퍼 결제 통계</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<span class="search_lbl">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="start_date" name="start_date" style="width:65px" readonly="readonly" value="<?= $search_sdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/> ~
				<input type="text" class="search_text" id="end_date" name="end_date" style="width:65px" readonly="readonly" value="<?= $search_edate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/>
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $search_sdate ?></span> ~ <span><?= $search_edate ?></span> 현황입니다
	</div>

	<table class="tbl_list_basic1">
		<colgroup>
            <col width="60">
            <col width="70">
            <col width="100">
            <col width="100">
            <col width="100">
            <col width="100">
            <col width="50">    
		</colgroup>
        <thead>
        	<tr>
        		<th>일자</th>
        		<th>그룹 </th>
        		<th>그룹 인원</th>
        		<th>상품 idx</th>
        		<th>상품 구매 건수</th>
        		<th>상품 구매 인원수</th>
         	</tr>
        </thead>
        <tbody>
<?
	$row_count = sizeof($nopayer_static_group_list);
	$today_total_usercount=0;
	$today_total_offerbuycount=0;
	$today_total_offerbuyusercount=0;
	
    for($i=0; $i<$row_count; $i++)
    {
    	$today = $nopayer_static_group_list[$i]["today"];
    	$groupidx = $nopayer_static_group_list[$i]["groupidx"];
    	$productidx = $nopayer_static_group_list[$i]["productidx"];
    	$usercount = $nopayer_static_group_list[$i]["usercount"];
    	$offerbuycount = $nopayer_static_group_list[$i]["offerbuycount"];
    	$offerbuyusercount = $nopayer_static_group_list[$i]["offerbuyusercount"];
    	$today_rowcount = $nopayer_static_group_list[$i]["today_rowcount"];
    	$group_rowcount = $nopayer_static_group_list[$i]["group_rowcount"];
    	
    	$today_total_usercount +=$usercount;
    	$today_total_offerbuycount +=$offerbuycount;
    	$today_total_offerbuyusercount +=$offerbuyusercount;
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
		if($i == 0 || $today !=$nopayer_static_group_list[$i-1]["today"])
		{
?>
			<td class="tdc point" rowspan="<?=$today_rowcount?>"><?= $today ?></td>
<?
		}
		
		if($i == 0 || $groupidx !=$nopayer_static_group_list[$i-1]["groupidx"] || $today !=$nopayer_static_group_list[$i-1]["today"])
		{
?>
			<td class="tdc point" rowspan="<?=$group_rowcount?>"><?= $groupidx ?></td>
<?
		}
?>
            <td class="tdc"><?= number_format($usercount) ?></td>
            <td class="tdc"><?= number_format($productidx) ?></td>
            <td class="tdc"><?= number_format($offerbuycount) ?></td>
            <td class="tdc"><?= number_format($offerbuyusercount) ?></td>
        </tr>
<?
	
		if($today !=$nopayer_static_group_list[$i+1]["today"])
		{
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
		    <td class="tdc point">Total</td>
		    <td class="tdc"> - </td>
		    <td class="tdc"><?= number_format($today_total_usercount) ?></td>
            <td class="tdc"> - </td>
            <td class="tdc"><?= number_format($today_total_offerbuycount) ?></td>
            <td class="tdc"><?= number_format($today_total_offerbuyusercount) ?></td>
        </tr>
<?		
		$today_total_usercount=0;
		$today_total_offerbuycount=0;
		$today_total_offerbuyusercount=0;
		}
    }
    
    if($row_count == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="5">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>
		
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>