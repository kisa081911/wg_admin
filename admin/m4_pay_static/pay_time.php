<?
	$top_menu = "pay_static";
	$sub_menu = "pay_time";
	
	include("../common/dbconnect/db_util_redshift.inc.php");
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$term = ($_GET["term"] == "") ? "hour" : $_GET["term"];
	$platform = ($_GET["platform"] == "") ? "ALL" : $_GET["platform"];
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
	
// 	$db = new CDatabase();
	$db_redshift = new CDatabase_Redshift();
    
	$date_tail = "";
	
	if ($startdate != "")
		$date_tail .= " AND writedate >= '$startdate 00:00:00' ";
	if ($enddate != "")
		$date_tail .= " AND writedate <= '$enddate 23:59:59'";
	
	if($platform == "ALL")
		$tail = " os_type = os_type ";
	else
		$tail = " os_type = $platform ";
		
	if ($term == "hour")
	{
		$sql = "SELECT to_char(writedate::timestamp, 'HH24') AS hour,
						COUNT(*) AS totalcount,
						COUNT(CASE WHEN status=1 THEN 1 ELSE null END) AS status1,
						SUM(CASE WHEN status=1 THEN money ELSE 0 END) AS money
				FROM t5_product_order_all
				WHERE status=1 AND useridx > 20000 AND $tail $date_tail
				GROUP BY to_char(writedate::timestamp, 'HH24')
				ORDER BY hour ASC";
		$hourlist = $db_redshift->gettotallist($sql);
	}
	else if ($term == "day")
	{
		$sql = "SELECT date_part(day, writedate) AS day,
						COUNT(*) AS totalcount,
						COUNT(CASE WHEN status=1 THEN 1 ELSE null END) AS status1,
						SUM(CASE WHEN status=1 THEN money ELSE 0 END) AS money
				FROM t5_product_order_all
				WHERE status=1 AND useridx > 20000 AND $tail $date_tail
				GROUP BY date_part(day, writedate)
				ORDER BY day ASC";
		$daylist = $db_redshift->gettotallist($sql);
	}
// 	$db->end();
	$db_redshift->end();
	
	$day_list = array();
	$status1_list = array();
	$money_list = array();
	$totalcount_list = array();
	
	$list_pointer = 0;
	
	if ($term == "hour")
	{		
		for ($hour=0; $hour<24; $hour++)
		{
			if ($list_pointer < sizeof($hourlist))
				$write_hour = $hourlist[$list_pointer]["hour"];
			
			if ($hour == $write_hour)
			{
				$status1_list[$hour] = $hourlist[$list_pointer]["status1"];
				$money_list[$hour] = $hourlist[$list_pointer]["money"];
				$totalcount_list[$hour] = $hourlist[$list_pointer]["totalcount"];
				
				$list_pointer++;
			}
			else 
			{
				$status1_list[$hour] = 0;
				$money_list[$hour] = 0;
				$totalcount_list[$hour] = 0;
			}
			
			$day_list[$hour] = $hour."시";
		}
	}
	else if ($term == "day")
	{		
		for ($day=0; $day<7; $day++)
		{
			if ($list_pointer < sizeof($daylist))
				$write_day = $daylist[$list_pointer]["day"];
	
			if ($day == $write_day-1)
			{
				$status1_list[$day] = $daylist[$list_pointer]["status1"];
				$money_list[$day] = $daylist[$list_pointer]["money"];
				$totalcount_list[$day] = $daylist[$list_pointer]["totalcount"];
				
				$list_pointer++;
			}
			else
			{
				$status1_list[$day] = 0;
				$money_list[$day] = 0;
				$totalcount_list[$day] = 0;
			}
			
			if ($day == 0)
				$day_list[$day] = "일";
			else if ($day == 1)
				$day_list[$day] = "월";
			else if ($day == 2)
				$day_list[$day] = "화";
			else if ($day == 3)
				$day_list[$day] = "수";
			else if ($day == 4)
				$day_list[$day] = "목";
			else if ($day == 5)
				$day_list[$day] = "금";
			else if ($day == 6)
				$day_list[$day] = "토";
		}
	}
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>

<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});
	
	function change_platform(term)
	{
		var search_form = document.search_form;
		
		var term_all = document.getElementById("term_all");
		var term_fb = document.getElementById("term_fb");
		var term_ios = document.getElementById("term_ios");
		var term_and = document.getElementById("term_and");
		var term_ama = document.getElementById("term_ama");

		document.search_form.platform.value = term;
		
		if (term == "ALL")
		{
			term_all.className="btn_schedule_select";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		
		}
		else if (term == 0)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule_select";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 1)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule_select";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 2)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule_select";
			term_ama.className="btn_schedule";
		}
		else if (term == 3)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule_select";
		}

		search_form.submit();
	}
	
	google.load("visualization", "1", {packages:["corechart"]});

	function drawChart() 
    {
        var data1 = new google.visualization.DataTable();
        
        data1.addColumn('string', '시간대');
        data1.addColumn('number', '결제완료금액($)');
        data1.addRows([
<? 
	$list_size = sizeof($day_list);
	
	for($i=0; $i<$list_size; $i++)
	{
		$hour = $day_list[$i];
		$money = $money_list[$i];
		
		echo("['".$hour."',".$money."]");
		
		if ($i<$list_size-1)
			echo(",");
	}
?>
        ]);

		var data2 = new google.visualization.DataTable();
        
        data2.addColumn('string', '시간대');
        data2.addColumn('number', '결제완료건');
        data2.addRows([
<? 
	$list_size = sizeof($day_list);
	
	for($i=0; $i<$list_size; $i++)
	{
		$hour = $day_list[$i];
		$status1 = $status1_list[$i];
		
		echo("['".$hour."',".$status1."]");
		
		if ($i<$list_size-1)
			echo(",");
	}
?>
        ]);

        var options1 = {
            width:1050,                         
            height:200,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:130}
        };
        
        var options2 = {
            width:1050,                         
            height:200,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:130}
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
        chart.draw(data1, options1);      

        var chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
        chart.draw(data2, options2);        
    }
        
	google.setOnLoadCallback(drawChart);

	function change_term(term)
	{
		var search_form = document.search_form;

		var hour = document.getElementById("term_hour");
		var day = document.getElementById("term_day");
		
		document.search_form.term.value = term;

		if (term == "hour")
		{
			hour.className = "btn_schedule_select";
			day.className = "btn_schedule";
		}
		else if (term == "day")
		{
			hour.className = "btn_schedule";
			day.className = "btn_schedule_select";
		}

		search_form.submit();
	}

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}    
</script>
		<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; <?= ($term == "hour") ? "시간별" : "요일별" ?></div>
	    		<form name="search_form" id="search_form"  method="get" action="pay_time.php">
	    		
	    		<div class="search_box">
	    			<input type="hidden" name="term" id="term" value="<?= $term ?>">
	    			<input type="hidden" name="platform" id="platform" value="<?= $platform ?>" />
	    			
	    			<input type="button" class="<?= ($platform == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_all" onclick="change_platform('ALL')" value="통합" />
	    			<input type="button" class="<?= ($platform == "0") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_fb" onclick="change_platform('0')" value="Facebook" />
	    			<input type="button" class="<?= ($platform == "1") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ios" onclick="change_platform('1')" value="iOS" />
	    			<input type="button" class="<?= ($platform == "2") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_and" onclick="change_platform('2')" value="Android" />
	    			<input type="button" class="<?= ($platform == "3") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ama" onclick="change_platform('3')" value="Amazon" />
	    			&nbsp;&nbsp;
	    			<input type="button" class="<?= ($term == "hour") ? "btn_schedule_select" : "btn_schedule" ?>" value="시간별" id="term_hour" onclick="change_term('hour')" /><input type="button" class="<?= ($term == "day") ? "btn_schedule_select" : "btn_schedule" ?>" value="요일별" id="term_day" onclick="change_term('day')" />
	    			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
		            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
		            <input type="button" class="btn_search" value="검색" onclick="search()" />
	    		</div>
	    		</form>
	        </div>
	    	<!-- //title_warp -->
	    	
	    	<div class="search_result">
<?
	if ($startdate == "" && $enddate == "")
		echo("<span>전체</span>");
	else if ($startdate != "" && $enddate == "")
		echo("<span>$startdate</span> ~ <span>현재</span>");
	else if ($startdate == "" && $enddate != "")
		echo("<span>$enddate 까지</span>");
	else if ($startdate != "" && $enddate != "")
		echo("<span>$startdate</span> ~ <span>$enddate</span>");
	
	if ($term == "hour")
		echo(" 시간별");
	else if ($term == "day")
		echo(" 요일별");
?>
            	 통계입니다
            </div>
	    	
	    	<div id="chart_div1" style="height:230px; min-width: 500px"></div>
	    	<div id="chart_div2" style="height:230px; min-width: 500px"></div>
    	</div>
    	<!--  //CONTENTS WRAP -->
    	
    	<div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>