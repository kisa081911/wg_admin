<?
    $top_menu = "pay_static";
    $sub_menu = "pay_month";
    
    include("../common/dbconnect/db_util_redshift.inc.php");
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
	
    $platform = ($_GET["platform"] == "") ? "ALL" : $_GET["platform"];
    
	$startdate = ($_GET["startdate"] == "") ? date("Y-m-01", strtotime("-11 month")) : $_GET["startdate"];
	$enddate = ($_GET["enddate"] == "") ? date("Y-m-d", strtotime("-1 day")) : $_GET["enddate"];
	
// 	$db = new CDatabase();
	$db_redshift = new CDatabase_Redshift();
	
	if($platform == "ALL")
		$tail = " AND os_type = os_type ";
	else
		$tail = " AND os_type = $platform ";
	
	$sql = "SELECT to_char(writedate::timestamp, 'YYYY년 MM월') AS today,
			COUNT(CASE WHEN STATUS=1 THEN 1 ELSE NULL END) AS total_cnt,
			COUNT(CASE WHEN STATUS=1 AND couponidx<>0 THEN 1 ELSE NULL END) AS coupon_buy_cnt,
			COUNT(CASE WHEN STATUS=1 AND special_more<>0 THEN 1 ELSE NULL END) AS more_buy_cnt,
			COUNT(CASE WHEN STATUS=2 THEN 1 ELSE NULL END) AS cancel_cnt, 
			SUM(CASE WHEN STATUS=1 THEN money ELSE 0 END) AS total_money,
			SUM(CASE WHEN STATUS=1 AND couponidx<>0 THEN money ELSE 0 END) AS coupon_buy_money,
			SUM(CASE WHEN STATUS=1 AND special_more<>0 THEN money ELSE 0 END) AS more_buy_money,
			SUM(CASE WHEN STATUS=2 THEN money ELSE 0 END) AS cancel_money
			FROM t5_product_order_all
			WHERE useridx > 20000 AND '$startdate 00:00:00' <= writedate AND writedate <= '$enddate 23:59:59' $tail
			GROUP BY to_char(writedate::timestamp, 'YYYY년 MM월')
			ORDER BY today DESC";
	$product_order_list = $db_redshift->gettotallist($sql);
	
	$prev_startdate = date("Y-m-01", strtotime($startdate."-1 month"));
	$prev_enddate = date("Y-m-01", strtotime($enddate));
	
	$sql = "SELECT to_char(writedate::timestamp, 'YYYY년 MM월') AS today,
			SUM(CASE WHEN STATUS=1 THEN money ELSE 0 END) AS prev_total_money
			FROM t5_product_order_all
			WHERE useridx > 20000 AND '$prev_startdate 00:00:00' <= writedate AND writedate < '$prev_enddate 00:00:00' $tail
			GROUP BY to_char(writedate::timestamp, 'YYYY년 MM월')
			ORDER BY today DESC;";
	$prev_month_data = $db_redshift->gettotallist($sql);
	
// 	$db->end();
	$db_redshift->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>

<script type="text/javascript">
	$(function() {
		$("#startdate").datepicker({});
		$("#enddate").datepicker({});
	});
	
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	    
	    if (search_form.startdate.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    }
	
	    if (search_form.enddate.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    }
	
	    search_form.submit();
	}

	function change_platform(term)
	{
		var search_form = document.search_form;
		
		var term_all = document.getElementById("term_all");
		var term_fb = document.getElementById("term_fb");
		var term_ios = document.getElementById("term_ios");
		var term_and = document.getElementById("term_and");
		var term_ama = document.getElementById("term_ama");

		document.search_form.platform.value = term;
		
		if (term == "ALL")
		{
			term_all.className="btn_schedule_select";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 0)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule_select";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 1)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule_select";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 2)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule_select";
			term_ama.className="btn_schedule";
		}
		else if (term == 3)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule_select";
		}

		search_form.submit();
	}
</script>

        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
        <form name="search_form" id="search_form"  method="get" action="pay_month_new.php">
            <input type=hidden name=issearch value="1">
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 월별 통계</div>
                <input type="hidden" name="platform" id="platform" value="<?= $platform ?>" />
                
				<div class="search_box">
					<input type="button" class="<?= ($platform == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_all" onclick="change_platform('ALL')" value="통합" />
					<input type="button" class="<?= ($platform == "0") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_fb" onclick="change_platform('0')" value="Facebook" />
	    			<input type="button" class="<?= ($platform == "1") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ios" onclick="change_platform('1')" value="iOS" />
	    			<input type="button" class="<?= ($platform == "2") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_and" onclick="change_platform('2')" value="Android" />
	    			<input type="button" class="<?= ($platform == "3") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ama" onclick="change_platform('3')" value="Amazon" />
	    			&nbsp;&nbsp;
					날짜 : &nbsp;&nbsp;
					<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" />~
					<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" />
					<input type="button" class="btn_search" value="검색" onclick="search()" />	
				</div>
            </div>
            <!-- //title_warp -->
            
            <div class="search_result">
                <span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다
            </div>
            
            <div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                
                <col width="">
                <col width="">
                
                <col width="">
                
                <col width="">
                <col width="">
                
                <col width="">
                <col width="">
                
                <col width="">
                <col width="">
            </colgroup>
            
            <thead>
            <tr>
                <th>년월</th>
                
                <th class="tdr">구매건수</th>
                <th class="tdr">구매액</th>
                
                <th class="tdr" style="border-right:dotted 1px;">전월대비</th>
                
                <th class="tdr">More 상품<br>구매건수</th>
                <th class="tdr" style="border-right:dotted 1px;">More 상품<br>구매액</th>
                
                <th class="tdr">쿠폰<br>구매건수</th>
                <th class="tdr" style="border-right:dotted 1px;">쿠폰<br>구매액</th>
                
                <th class="tdr">취소건수<br/>(결제일 기준)</th>
                <th class="tdr">취소금액<br/>(결제일 기준)</th>
            </tr>
            </thead>
            
            <tbody>
<?
    
    
    for ($i=0; $i<sizeof($product_order_list); $i++)
    {
    	$today = $product_order_list[$i]["today"];
    	
    	$total_cnt = $product_order_list[$i]["total_cnt"];
    	$total_money = $product_order_list[$i]["total_money"];
    	
    	$cancel_cnt = $product_order_list[$i]["cancel_cnt"];
    	$cancel_money = $product_order_list[$i]["cancel_money"];
    	
    	$coupon_buy_cnt = $product_order_list[$i]["coupon_buy_cnt"];
    	$coupon_buy_money = $product_order_list[$i]["coupon_buy_money"];
    	
    	$more_buy_cnt = $product_order_list[$i]["more_buy_cnt"];
    	$more_buy_money = $product_order_list[$i]["more_buy_money"];
		
    	$prev_total_money = $prev_month_data[$i]["prev_total_money"];
    	
		if ($prev_total_money == 0)
			$ratio = "N/A";
		else
			$ratio = number_format($total_money * 100 / $prev_total_money, 1)."%";
?>
                <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
                    <td class="tdc point_title"><?= $today ?></td>
                    
                    <td class="tdr point"><?= number_format($total_cnt) ?></td>
                    <td class="tdr point">$<?= number_format(($total_money), 1) ?></td>
                    
                    <td class="tdr point" style="border-right:dotted 1px;"><?= $ratio ?></td>
                    
                    <td class="tdr point"><?= number_format($more_buy_cnt) ?></td>
                    <td class="tdr point" style="border-right:dotted 1px;">$<?= number_format(($more_buy_money), 1) ?></td>
                    
                    <td class="tdr point"><?= ($coupon_buy_cnt == 0) ? "-" : "$ ".number_format($coupon_buy_cnt) ?></td>
                    <td class="tdr point" style="border-right:dotted 1px;"><?= ($coupon_buy_money == 0) ? "-" : "$ ".number_format(($coupon_buy_money), 1) ?></td>
                    
                    <td class="tdr point"><?= ($cancel_cnt == 0) ? "-" : number_format($cancel_cnt) ?></td>
                    <td class="tdr point"><?= ($cancel_money == 0) ? "-" : "$ ".number_format(($cancel_money), 1) ?></td>
                </tr>
<?
    }
?>    
            </tbody>
            </table>
        </div>
        
        </form>
</div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>