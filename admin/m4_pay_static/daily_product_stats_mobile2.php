<?
	$top_menu = "pay_static";
	$sub_menu = "daily_product_stats_mobile";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	$os_type = ($_GET["os_type"] =="")? 0 :$_GET["os_type"];
	
	if($search_start_createdate == "")
		$search_start_createdate = date("Y-m-d", strtotime("-4 day"));
	
	if($search_end_createdate == "")
		$search_end_createdate = $today;
	
	$tab = ($_GET["tab"] == "") ? "0" : $_GET["tab"];
	
	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();
	
	$std_useridx = 20000;
	
	if($os_type == 0)
	{
		$os_tail="";
		$platform_name = "Total";
	}
	else if($os_type == 1)
	{
		$os_tail="AND os_type = 1";
		$platform_name = "IOS";
	}
	else if($os_type == 2)
	{
		$os_tail="AND os_type = 2";
		$platform_name = "Android";
	}
	else if($os_type == 3)
	{
		$os_tail="AND os_type = 3";
		$platform_name = "Amazon";
	}
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}
	
	if($tab == 0) 
	{
		$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, ".
				"	SUM(facebookcredit) AS sum_total_credit, ".
				"	SUM(IF(special_discount = 0 AND special_more = 0 AND couponidx = 0, facebookcredit, 0)) AS sum_basic_credit, ".
				"	SUM(IF(special_discount != 0 OR special_more != 0 AND couponidx = 0, facebookcredit, 0)) AS sum_special_credit, ".
				"	SUM(IF(couponidx != 0, facebookcredit, 0)) AS sum_coupon_credit ".
				"FROM tbl_product_order_mobile ".
				"WHERE useridx > $std_useridx AND status = 1 $os_tail AND writedate >= '$search_start_createdate 00:00:00' AND writedate <= '$search_end_createdate 23:59:59' ".
				"GROUP BY LEFT(writedate,10) ORDER BY writedate ASC";
		
		$ratelist = $db_main->gettotallist($sql);
		
		$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, ".
				"	COUNT(*) AS sum_total_count, ".
				"	COUNT(IF(special_discount = 0 AND special_more = 0 AND couponidx = 0, 1, NULL)) AS sum_basic_count, ".
				"	COUNT(IF(special_discount != 0 OR special_more != 0 AND couponidx = 0, 1, NULL)) AS sum_special_count, ".
				"	COUNT(IF(couponidx != 0, 1, NULL)) AS sum_coupon_count ".
				"FROM tbl_product_order_mobile ".
				"WHERE useridx > $std_useridx AND status = 1 $os_tail AND writedate >= '$search_start_createdate 00:00:00' AND writedate <= '$search_end_createdate 23:59:59' ".
				"GROUP BY LEFT(writedate,10) ORDER BY writedate ASC";
		
		$countlist = $db_main->gettotallist($sql);
	}
	else
	{
		
		$sql = "SELECT t1.writedate, productidx, product_type, productname, imgurl, pay_count, money, coin, day_total_facebookcredit,day_row_count
                FROM ( 
                	SELECT writedate, productidx, product_type, productname, imgurl, SUM(pay_count) AS pay_count, SUM(money) AS money, SUM(coin) AS coin
                	FROM tbl_product_stat_mobile_daily_new 
                	WHERE writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate' $os_tail GROUP BY writedate, productidx, product_type 
                ) t1 LEFT JOIN ( 
                	SELECT writedate, SUM(money) AS day_total_facebookcredit, COUNT(writedate) AS day_row_count 
                	FROM tbl_product_stat_mobile_daily_new 
                	WHERE  writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate' $os_tail 
                	GROUP BY writedate 
                ) t2 ON t1.writedate = t2.writedate 
                ORDER BY writedate DESC, day_total_facebookcredit DESC";
		$product_data = $db_analysis->gettotallist($sql);
	}
	
	$db_main->end();
	$db_analysis->end();
?>

<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
<?
	if($tab == 0)
	{
?>
		google.load("visualization", "1", {packages:["corechart"]});
		google.setOnLoadCallback(drawChart);

		function drawChart()
		{
			var data1 = google.visualization.arrayToDataTable([
				['날짜','정상구매', '할인구매', '쿠폰구매'],
<?
				for($i=0; $i<sizeof($ratelist); $i++)
				{
					
					$writedate = $ratelist[$i]["writedate"];
					$total_facebookcredit = $ratelist[$i]["sum_total_credit"];
					$basic_facebookcredit = $ratelist[$i]["sum_basic_credit"];
					$special_facebookcredit = $ratelist[$i]["sum_special_credit"];
					$coupon_facebookcredit = $ratelist[$i]["sum_coupon_credit"];
					
					$basic_rate = $total_facebookcredit == 0 ? 0 : round($basic_facebookcredit/$total_facebookcredit * 100, 1);
					$special_rate = $total_facebookcredit == 0 ? 0 : round($special_facebookcredit/$total_facebookcredit * 100, 1);
					$coupon_rate = $total_facebookcredit == 0 ? 0 : round($coupon_facebookcredit/$total_facebookcredit * 100, 1);
					
					if($i == sizeof($ratelist))
					{
						echo "['$writedate', ".$basic_rate.",  ".$special_rate.", ".$coupon_rate."]";
					}
					else
					{
						echo "['$writedate', ".$basic_rate.",  ".$special_rate.", ".$coupon_rate."],";
					}
				}
?>
			]);

			var data2 = google.visualization.arrayToDataTable([
				['날짜','정상구매', '할인구매', '쿠폰구매'],
<?
				for($i=0; $i<sizeof($countlist); $i++)
				{
					$writedate = $countlist[$i]["writedate"];
					$total_count = $countlist[$i]["sum_total_count"];
					$basic_count = $countlist[$i]["sum_basic_count"];
					$special_count = $countlist[$i]["sum_special_count"];
					$coupon_count = $countlist[$i]["sum_coupon_count"];
						
					if($i == sizeof($countlist))
					{
						echo "['$writedate', ".$basic_count.",  ".$special_count.", ".$coupon_count."]";
					}
					else
					{
						echo "['$writedate', ".$basic_count.",  ".$special_count.", ".$coupon_count."],";
					}					
				}
?>
			]);
			
			var options1 = {
				    title: '1.구매비율 현황',
				    hAxis: {title: '', titleTextStyle: {color: 'black'}},
				    vAxis: {title: '%', titleTextStyle: {color: 'black'}}
				  };
			  
			var options2 = {
				    title: '2.구매건수 현황',
				    hAxis: {title: '', titleTextStyle: {color: 'black'}},
				    vAxis: {title: '건수', titleTextStyle: {color: 'black'}}
				  };
			
			var chart1 = new google.visualization.ColumnChart(document.getElementById('chart1_div'));
			var chart2 = new google.visualization.ColumnChart(document.getElementById('chart2_div'));

			chart1.draw(data1, options1);
			chart2.draw(data2, options2);
		}
<?
	}
?>
	function tab_change(tab)
	{
		var search_form = document.search_form;
		search_form.tab.value = tab;
		search_form.submit();
	}

	function change_os_type(type)
	{
		var search_form = document.search_form;
		
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
	
	$(function() {
	    $("#start_createdate").datepicker({ });
	});

	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	
	<form name="search_form" id="search_form"  method="get" action="daily_product_stats_mobile.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> <?=$platform_name?> &gt; 일별 상품 통계</div>
		<div class="search_box">
			<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />
        		
        	<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
				<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Total" id="type_web" onclick="change_os_type('0')"    />
				<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
				<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
				<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
			</span>
			
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type=hidden name="tab" value="<?= $tab ?>">
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	
	<ul class="tab">
		<li id="tab_1" class="<?= ($tab == "0") ? "select" : "" ?>" style="width:50px;text-align:center;font-size:11px;" onclick="tab_change('0')">그래프</li>
		<li id="tab_2" class="<?= ($tab == "1") ? "select" : "" ?>" style="width:50px;text-align:center;font-size:11px;" onclick="tab_change('1')">상세보기</li>
	</ul>
	
<?
	if($tab == 0)
	{
?>
		<div id="chart1_div" style="width: 1100px; height: 500px;"></div>
		<div id="chart2_div" style="width: 1100px; height: 500px;"></div>
<?
	}
	else 
	{
?> 
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">상품명</th>
					<th class="tdc">구매 수</th>
					<th class="tdc">구매 금액</th>
					<th class="tdc">구매 Coin</th>
					<th class="tdc">구매비율</th>
				</tr>
			</thead>
			<tbody>
<?
			$tmp_row_count = 1;
			
			$day_total_count = 0;
			$day_total_facebookcredit = 0;
			
			$all_total_count = 0;
			$all_total_facebookcredit = 0;
			
			for($i=0; $i<sizeof($product_data); $i++)
			{
				$row_count = $product_data[$i]["day_row_count"];
				$writedate = $product_data[$i]["writedate"];
				$product_type = $product_data[$i]["product_type"];
				$productidx = $product_data[$i]["productidx"];
				$product = $product_data[$i]["productname"];
				$total_count = $product_data[$i]["pay_count"];
				$total_facebookcredit = $product_data[$i]["money"];
				$total_coin = $product_data[$i]["coin"];
				$imageurl = $product_data[$i]["imgurl"];
				$total_purchase_rate = ($product_data[$i]["day_total_facebookcredit"] == 0) ? 0 : round(($product_data[$i]["money"] / $product_data[$i]["day_total_facebookcredit"] * 100), 2);
				
				$day_total_count += $total_count;
				$day_total_facebookcredit += $total_facebookcredit;
				$day_total_coin += $total_coin;
				
				$all_total_count += $total_count;
				$all_total_facebookcredit += $total_facebookcredit;
				$all_total_coin += $total_coin;
				$productname = "";
				
				if($productidx == "0")
				{
				    $productname = "facebok earn";
				    $imageurl = "/images/icon/facebook.png";
				}
				else if($product_type == 1)
				{
				    $productname = $product."(Basic)";
				}
				else if($product_type == 2)
				{
				    $productname = $product."(Season)";
				}
				else if($product_type == 3)
				{
				    $productname = $product."(Threshold)";
				}
				else if($product_type == 4)
				{
				    $productname = $product."(Whale)";
				}
				else if($product_type == 5)
				{
				    $productname = $product."(28 Retention)";
				}
				else if($product_type == 6)
				{
				    $productname = $product."(First)";
				}
				else if($product_type == 7)
				{
				    $productname = $product."(Lucky)";
				}
				else if($product_type == 8)
				{
				    $productname = $product."(Monthly)";
				}
				else if($product_type == 9)
				{
				    $productname = $product."(Piggypot)";
				}
				else if($product_type == 10)
				{
				    $productname = $product."(Buyerleave)";
				}
				else if($product_type == 11)
				{
				    $productname = $product."(Nopayer)";
				}
				else if($product_type == 12 || $product_type == 13)
				{
				    $productname = $product."(Prime)";
				}
				else if($product_type == 14)
				{
				    $productname = $product."(Attend)";
				}
				else if($product_type == 15)
				{
				    $productname = $product."(Platinum Deal)";
				}
				else if($product_type == 16)
				{
				    $productname = $product."(Amazing Deal)";
				}
				else if($product_type == 17)
				{
				    $productname = $product."(First Attend)";
				}
				  else if($product_type == 18)
	         {
	             $productname = $product."(Super Deal)";
	         }
	         else if($product_type == 19)
	         {
	             $productname = $product."(Speed Wheel)";
	         }
	         else if($product_type == 20)
	         {
	             $productname = $product."(Collection Deal)";
	         }
?>
				<tr>
<?
				if($tmp_row_count == 1)
				{
					$tmp_row_count = $row_count;
?>
					<td class="tdc point_title" rowspan="<?= $row_count + 1?>"><?= $writedate ?></td>
<?
				}
				else
				{
					$tmp_row_count--;
				}
?>
					<td class="tdc point"><img src="<?=$imageurl?>" align="absmiddle" width="25" height="25"> <?= $productname?></td>
					<td class="tdc"><?= number_format($total_count) ?></td>
					<td class="tdc">$<?= number_format($total_facebookcredit, 1) ?></td>
					<td class="tdc"><?= number_format($total_coin) ?></td>
					<td class="tdc"><?= number_format($total_purchase_rate, 2) ?>%</td>
				</tr>
<?
				if($tmp_row_count == 1)
				{
?>
					<tr>
						<td class="tdc point">Total</td>
						<td class="tdc point"><?= number_format($day_total_count) ?></td>
						<td class="tdc point">$<?= number_format($day_total_facebookcredit, 1) ?></td>
						<td class="tdc point"><?= number_format($day_total_coin) ?></td>
						<td class="tdc point">100%</td>
					</tr>
<? 
					$day_total_count = 0;
					$day_total_facebookcredit = 0;
					$day_total_coin = 0;
				}
			}
?>
				<tr>
					<td class="tdc point_title" colspan="2">Total</td>
					<td class="tdc point"><?= number_format($all_total_count) ?></td>
					<td class="tdc point">$<?= number_format($all_total_facebookcredit, 1) ?></td>
					<td class="tdc point"><?= number_format($all_total_coin) ?></td>
					<td class="tdc point"></td>
				</tr>
			</tbody>
		</table>
	</div>
<?
	}
?>
     </form>
</div>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>