<?
	$top_menu = "pay_static";
	$sub_menu = "pay_product_type_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];	
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today,15,"d");
	
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db_main = new CDatabase_Main();
	
	$web_sql = "SELECT writedate, SUM(basic_credit) AS basic_credit, SUM(season_credit) AS season_credit, SUM(threadhold_credit) AS threadhold_credit, SUM(whale_credit) AS whale_credit, ".
			"SUM(retention_credit) AS retention_credit, SUM(frist_credit) AS frist_credit, SUM(lucky_credit) AS lucky_credit, SUM(monthly_credit) AS monthly_credit, SUM(piggypot_credit) AS piggypot_credit, SUM(buyerleave_credit) AS buyerleave_credit, SUM(nopayer_credit) AS nopayer_credit , SUM(total_credit) AS total_credit	".
			"FROM	".
			"(	".
			"	SELECT LEFT(t1.writedate,10) AS writedate,	". 
			"	SUM(IF(t1.product_type = 1, t1.facebookcredit, 0)) AS basic_credit,	".
			"	SUM(IF(t1.product_type = 2, t1.facebookcredit, 0)) AS season_credit,	".
			"	SUM(IF(t1.product_type = 3, t1.facebookcredit, 0)) AS threadhold_credit,	".
			"	SUM(IF(t1.product_type = 4, t1.facebookcredit, 0)) AS whale_credit,	".
			"	SUM(IF(t1.product_type = 5, t1.facebookcredit, 0)) AS retention_credit,	".
			"	SUM(IF(t1.product_type = 6, t1.facebookcredit, 0)) AS frist_credit,	".
			"	SUM(IF(t1.product_type = 7, t1.facebookcredit, 0)) AS lucky_credit,	".
			"	SUM(IF(t1.product_type = 8, t1.facebookcredit, 0)) AS monthly_credit,	".
			"	SUM(IF(t1.product_type = 9, t1.facebookcredit, 0)) AS piggypot_credit,	".
			"	SUM(IF(t1.product_type = 10, t1.facebookcredit, 0)) AS buyerleave_credit,	".
			"	SUM(IF(t1.product_type = 11, t1.facebookcredit, 0)) AS nopayer_credit,	".
			"   IFNULL(SUM(t1.facebookcredit), 0) AS total_credit ".
			"	FROM tbl_product_order t1 JOIN tbl_product t2 ON t1.productidx = t2.productidx	". 
			"	WHERE useridx > 20000 AND t1.status IN (1, 3) AND t1.writedate >= '$startdate 00:00:00' AND t1.writedate <= '$enddate 23:59:59'	". 
			"	GROUP BY LEFT(t1.writedate,10), t1.product_type	".
			") t3 GROUP BY writedate ORDER BY writedate ASC;";
	$web_product_set_info = $db_main->gettotallist($web_sql);
	
	$web_basic_list = array();
	$web_season_list = array();
	$web_thresholdcredit_list = array();
	$web_whale_list = array();
	$web_retention_list = array();
	$web_frist_list = array();
	$web_lucky_list = array();
	$web_monthly_list = array();
	$web_piggypot_list = array();
	$web_buyerleave_list = array();
	$web_nopayer_list = array();
	$web_total_list = array();
	$date_list = array();
	
	$list_pointer = sizeof($web_product_set_info);
		
	
	$date_pointer = $enddate;
	
	for ($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
	{
		$writedate = $web_product_set_info[$list_pointer-1]["writedate"];
				
		if (get_diff_date($date_pointer, $writedate, "d") == 0)
		{
			$static = $web_product_set_info[$list_pointer-1];
	
			$web_basic_list[$i] = $static["basic_credit"] * 0.1;
			$web_season_list[$i] = $static["season_credit"] * 0.1;
			$web_thresholdcredit_list[$i] = $static["threadhold_credit"] * 0.1;
			$web_whale_list[$i] = $static["whale_credit"] * 0.1;
			$web_retention_list[$i] = $static["retention_credit"] * 0.1;
			$web_frist_list[$i] = $static["frist_credit"] * 0.1;
			$web_lucky_list[$i] = $static["lucky_credit"] * 0.1;
			$web_monthly_list[$i] = $static["monthly_credit"] * 0.1;
			$web_piggypot_list[$i] = $static["piggypot_credit"] * 0.1;
			$web_buyerleave_list[$i] = $static["buyerleave_credit"] * 0.1;
			$web_nopayer_list[$i] = $static["nopayer_credit"] * 0.1;
			$web_total_list[$i] = ($static["basic_credit"] + $static["season_credit"] + $static["threadhold_credit"] + $static["whale_credit"] + $static["retention_credit"] + $static["frist_credit"] +$static["lucky_credit"] +  $static["monthly_credit"] + $static["piggypot_credit"]+ $static["buyerleave_credit"]+ $static["nopayer_credit"]) * 0.1;
			$date_list[$i] = $date_pointer;
	
			$list_pointer--;
		}
		else
		{
			$web_basic_list[$i] = 0;
			$web_season_list[$i] = 0;
			$web_thresholdcredit_list[$i] = 0;
			$web_whale_list[$i] = 0;
			$web_retention_list[$i] = 0;
			$web_frist_list[$i] = 0;
			$web_lucky_list[$i] = 0;
			$web_monthly_list[$i] = 0;
			$web_piggypot_list[$i] = 0;
			$web_buyerleave_list[$i] = 0;
			$web_nopayer_list[$i] = 0;
			$web_total_list[$i] = 0;
			$date_list[$i] = $date_pointer;
		}
			
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	$ios_sql = "SELECT writedate, SUM(basic_credit) AS basic_credit, SUM(frist_credit) AS frist_credit, SUM(season_credit) AS season_credit, SUM(special_credit) AS special_credit, SUM(total_credit) AS total_credit ".
				"FROM	".
				"(	".
				"	SELECT LEFT(t1.writedate,10) AS writedate,	". 
				"	SUM(IF(category = 0, t1.money, 0)) AS basic_credit,	".
				"	SUM(IF(category = 1, t1.money, 0)) AS frist_credit,	".
				"	SUM(IF(category = 2, t1.money, 0)) AS season_credit,	".
				"	SUM(IF(category = 3, t1.money, 0)) AS special_credit,	".
				"   IFNULL(SUM(t1.money), 0) AS total_credit ".
				"	FROM tbl_product_order_mobile t1 JOIN tbl_product_mobile t2 ON t1.productidx = t2.productidx	 ".
				"	WHERE useridx > 20000 AND t1.os_type = 1 AND t1.status = 1 AND t1.writedate >= '$startdate 00:00:00' AND t1.writedate <= '$enddate 23:59:59'	". 
				"	GROUP BY LEFT(t1.writedate,10), category	".
				") t3 GROUP BY writedate ORDER BY writedate ASC;";
	
	$ios_product_set_info = $db_main->gettotallist($ios_sql);
	
	$ios_basic_list = array();
	$ios_frist_list = array();
	$ios_season_list = array();
	$ios_special_list = array();
	$ios_total_list = array();
	$date_list = array();
	
	$list_pointer = sizeof($ios_product_set_info);
	
	
	$date_pointer = $enddate;
	
	for ($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
	{
		$writedate = $ios_product_set_info[$list_pointer-1]["writedate"];
		
		if (get_diff_date($date_pointer, $writedate, "d") == 0)
		{
			$static = $ios_product_set_info[$list_pointer-1];
		
			$ios_basic_list[$i] = $static["basic_credit"];
			$ios_frist_list[$i] = $static["season_credit"];
			$ios_season_list[$i] = $static["threadhold_credit"];
			$ios_special_list[$i] = $static["special_credit"];
			$ios_total_list[$i] =  $static["basic_credit"] + $static["season_credit"] + $static["threadhold_credit"] + $static["special_credit"];
			$date_list[$i] = $date_pointer;
		
			$list_pointer--;
		}
		else
		{
			$ios_basic_list[$i] = 0;
			$ios_frist_list[$i] = 0;
			$ios_season_list[$i] = 0;
			$ios_special_list[$i] = 0;
			$ios_total_list[$i] = 0;
			$date_list[$i] = $date_pointer;
		}
		
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	$and_sql = "SELECT writedate, SUM(basic_credit) AS basic_credit, SUM(frist_credit) AS frist_credit, SUM(season_credit) AS season_credit, SUM(special_credit) AS special_credit, SUM(total_credit) AS total_credit ".
				"FROM	".
				"(	".
				"	SELECT LEFT(t1.writedate,10) AS writedate,	".
				"	SUM(IF(category = 0, t1.money, 0)) AS basic_credit,	".
				"	SUM(IF(category = 1, t1.money, 0)) AS frist_credit,	".
				"	SUM(IF(category = 2, t1.money, 0)) AS season_credit,	".
				"	SUM(IF(category = 3, t1.money, 0)) AS special_credit,	".
				"   IFNULL(SUM(t1.money), 0) AS total_credit ".
				"	FROM tbl_product_order_mobile t1 JOIN tbl_product_mobile t2 ON t1.productidx = t2.productidx	 ".
				"	WHERE useridx > 20000 AND t1.os_type = 2 AND t1.status = 1 AND t1.writedate >= '$startdate 00:00:00' AND t1.writedate <= '$enddate 23:59:59'	".
				"	GROUP BY LEFT(t1.writedate,10), category	".
				") t3 GROUP BY writedate ORDER BY writedate ASC;";
	
	$and_product_set_info = $db_main->gettotallist($and_sql);
	
	$and_basic_list = array();
	$and_frist_list = array();
	$and_season_list = array();
	$and_special_list = array();
	$and_total_list = array();
	$date_list = array();
	
	$list_pointer = sizeof($and_product_set_info);
	
	
	$date_pointer = $enddate;
	
	for ($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
	{
		$writedate = $and_product_set_info[$list_pointer-1]["writedate"];
		
		if (get_diff_date($date_pointer, $writedate, "d") == 0)
		{
			$static = $and_product_set_info[$list_pointer-1];
		
			$and_basic_list[$i] = $static["basic_credit"];
			$and_frist_list[$i] = $static["season_credit"];
			$and_season_list[$i] = $static["threadhold_credit"];
			$and_special_list[$i] = $static["special_credit"];
			$and_total_list[$i] = $static["basic_credit"] + $static["season_credit"] + $static["threadhold_credit"] + $static["special_credit"];
			$date_list[$i] = $date_pointer;
		
			$list_pointer--;
		}
		else
		{
			$and_basic_list[$i] = 0;
			$and_frist_list[$i] = 0;
			$and_season_list[$i] = 0;
			$and_special_list[$i] = 0;
			$and_total_list[$i] = 0;
			$date_list[$i] = $date_pointer;
		}
		
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
			
	$ama_sql = "SELECT writedate, SUM(basic_credit) AS basic_credit, SUM(frist_credit) AS frist_credit, SUM(season_credit) AS season_credit, SUM(special_credit) AS special_credit, SUM(total_credit) AS total_credit ".
				"FROM	".
				"(	".
				"	SELECT LEFT(t1.writedate,10) AS writedate,	".
				"	SUM(IF(category = 0, t1.money, 0)) AS basic_credit,	".
				"	SUM(IF(category = 1, t1.money, 0)) AS frist_credit,	".
				"	SUM(IF(category = 2, t1.money, 0)) AS season_credit,	".
				"	SUM(IF(category = 3, t1.money, 0)) AS special_credit,	".
				"   IFNULL(SUM(t1.money), 0) AS total_credit ".
				"	FROM tbl_product_order_mobile t1 JOIN tbl_product_mobile t2 ON t1.productidx = t2.productidx	 ".
				"	WHERE useridx > 20000 AND t1.os_type = 3 AND t1.status = 1 AND t1.writedate >= '$startdate 00:00:00' AND t1.writedate <= '$enddate 23:59:59'	".
				"	GROUP BY LEFT(t1.writedate,10), category	".
				") t3 GROUP BY writedate ORDER BY writedate ASC;";
			
	$ama_product_set_info = $db_main->gettotallist($ama_sql);
	
	$ama_basic_list = array();
	$ama_frist_list = array();
	$ama_season_list = array();
	$ama_special_list = array();
	$ama_total_list = array();
	$date_list = array();
	
	$list_pointer = sizeof($ama_product_set_info);
	
	
	$date_pointer = $enddate;
	
	for ($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
	{
		$writedate = $ama_product_set_info[$list_pointer-1]["writedate"];
		
		if (get_diff_date($date_pointer, $writedate, "d") == 0)
		{
			$static = $ama_product_set_info[$list_pointer-1];
		
			$ama_basic_list[$i] = $static["basic_credit"];
			$ama_frist_list[$i] = $static["season_credit"];
			$ama_season_list[$i] = $static["threadhold_credit"];
			$ama_special_list[$i] = $static["special_credit"];
			$ama_total_list[$i] = $static["basic_credit"] + $static["season_credit"] + $static["threadhold_credit"] + $static["special_credit"];
			$date_list[$i] = $date_pointer;
		
			$list_pointer--;
		}
		else
		{
			$ama_basic_list[$i] = 0;
			$ama_frist_list[$i] = 0;
			$ama_season_list[$i] = 0;
			$ama_special_list[$i] = 0;
			$ama_total_list[$i] = 0;
			$date_list[$i] = $date_pointer;
		}
	
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}	
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

	google.load("visualization", "1", {packages:["corechart"]});
	
    function drawChart() 
    {
        var data1 = new google.visualization.DataTable();
        data1.addColumn('string', '날짜');
        data1.addColumn('number', 'Basic 결제율(%)');
        data1.addColumn('number', 'Season 결제율(%)');
        data1.addColumn('number', 'Threshold 결제율(%)');
        data1.addColumn('number', 'Whale 결제율(%)');
        data1.addColumn('number', '28 Retention 결제율(%)');        
        data1.addColumn('number', 'First 결제율(%)');
        data1.addColumn('number', 'Lucky 결제율(%)');
        data1.addColumn('number', 'Monthly 결제율(%)');
        data1.addColumn('number', 'Piggypot 결제율(%)');
        data1.addColumn('number', 'Buyerleave 결제율(%)');
	data1.addColumn('number', 'Nopayer 결제율(%)');
        data1.addRows([
<?
			for ($i=sizeof($date_list); $i>0; $i--)
			{
				$_bascicredit = ($web_total_list[$i-1]==0)?0:round($web_basic_list[$i-1]/$web_total_list[$i-1]*100, 1);
				$_seasoncredit = ($web_total_list[$i-1]==0)?0:round($web_season_list[$i-1]/$web_total_list[$i-1]*100, 1);
				$_thresholdcredit = ($web_total_list[$i-1]==0)?0:round($web_thresholdcredit_list[$i-1]/$web_total_list[$i-1]*100, 1);
				$_whalecredit = ($web_total_list[$i-1]==0)?0:round($web_whale_list[$i-1]/$web_total_list[$i-1]*100, 1);
				$_retentioncredit = ($web_total_list[$i-1]==0)?0:round($web_retention_list[$i-1]/$web_total_list[$i-1]*100, 1);
				$_firstcredit = ($web_total_list[$i-1]==0)?0:round($web_frist_list[$i-1]/$web_total_list[$i-1]*100, 1);
				$_luckycredit = ($web_total_list[$i-1]==0)?0:round($web_lucky_list[$i-1]/$web_total_list[$i-1]*100, 1);
				$_monthlycredit = ($web_total_list[$i-1]==0)?0:round($web_monthly_list[$i-1]/$web_total_list[$i-1]*100, 1);
				$_piggypotcredit = ($web_total_list[$i-1]==0)?0:round($web_piggypot_list[$i-1]/$web_total_list[$i-1]*100, 1);
				$_buyerleavecredit = ($web_total_list[$i-1]==0)?0:round($web_buyerleave_list[$i-1]/$web_total_list[$i-1]*100, 1);
				$_nopayercredit = ($web_total_list[$i-1]==0)?0:round($web_nopayer_list[$i-1]/$web_total_list[$i-1]*100, 1);
		        $_date = $date_list[$i-1];
				
				echo("['".$_date."',".$_bascicredit.",".$_seasoncredit.",".$_thresholdcredit.",".$_whalecredit.",".$_retentioncredit.",".$_firstcredit.",".$_luckycredit.",".$_monthlycredit.",".$_piggypotcredit.",".$_buyerleavecredit.",".$_nopayercredit."]");
				
				if ($i > 1)
					echo(",");
			}
?>
        ]);

        var data2 = new google.visualization.DataTable();
        data2.addColumn('string', '날짜');
        data2.addColumn('number', 'Basic 결제율(%)');
        data2.addColumn('number', 'First 결제율(%)');
        data2.addColumn('number', 'Season 결제율(%)');
        data2.addColumn('number', 'Special 결제율(%)');        
        data2.addRows([
<?
			for ($i=sizeof($date_list); $i>0; $i--)
			{
				$_bascicredit = ($ios_total_list[$i-1]==0)?0:round($ios_basic_list[$i-1]/$ios_total_list[$i-1]*100, 1);
				$_firstcredit = ($ios_total_list[$i-1]==0)?0:round($ios_frist_list[$i-1]/$ios_total_list[$i-1]*100, 1);
				$_seasoncredit = ($ios_total_list[$i-1]==0)?0:round($ios_season_list[$i-1]/$ios_total_list[$i-1]*100, 1);
				$_specialcredit = ($ios_total_list[$i-1]==0)?0:round($ios_special_list[$i-1]/$ios_total_list[$i-1]*100, 1);
		        $_date = $date_list[$i-1];
				
				echo("['".$_date."',".$_bascicredit.",".$_firstcredit.",".$_seasoncredit.",".$_specialcredit."]");
				
				if ($i > 1)
					echo(",");
			}
?>
        ]);

        var data3 = new google.visualization.DataTable();
        data3.addColumn('string', '날짜');
        data3.addColumn('number', 'Basic 결제율(%)');
        data3.addColumn('number', 'First 결제율(%)');
        data3.addColumn('number', 'Season 결제율(%)');
        data3.addColumn('number', 'Special 결제율(%)');        
        data3.addRows([
<?
			for ($i=sizeof($date_list); $i>0; $i--)
			{
				$_bascicredit = ($and_total_list[$i-1]==0)?0:round($and_basic_list[$i-1]/$and_total_list[$i-1]*100, 1);
				$_firstcredit = ($and_total_list[$i-1]==0)?0:round($and_frist_list[$i-1]/$and_total_list[$i-1]*100, 1);
				$_seasoncredit = ($and_total_list[$i-1]==0)?0:round($and_season_list[$i-1]/$and_total_list[$i-1]*100, 1);
				$_specialcredit = ($and_total_list[$i-1]==0)?0:round($and_special_list[$i-1]/$and_total_list[$i-1]*100, 1);
		        $_date = $date_list[$i-1];
				
				echo("['".$_date."',".$_bascicredit.",".$_firstcredit.",".$_seasoncredit.",".$_specialcredit."]");
				
				if ($i > 1)
					echo(",");
			}
?>
        ]);

        var data4 = new google.visualization.DataTable();
        data4.addColumn('string', '날짜');
        data4.addColumn('number', 'Basic 결제율(%)');
        data4.addColumn('number', 'First 결제율(%)');
        data4.addColumn('number', 'Season 결제율(%)');
        data4.addColumn('number', 'Special 결제율(%)');        
        data4.addRows([
<?
			for ($i=sizeof($date_list); $i>0; $i--)
			{
				$_bascicredit = ($ama_total_list[$i-1]==0)?0:round($ama_basic_list[$i-1]/$ama_total_list[$i-1]*100, 1);
				$_firstcredit = ($ama_total_list[$i-1]==0)?0:round($ama_frist_list[$i-1]/$ama_total_list[$i-1]*100, 1);
				$_seasoncredit = ($ama_total_list[$i-1]==0)?0:round($ama_season_list[$i-1]/$ama_total_list[$i-1]*100, 1);
				$_specialcredit = ($ama_total_list[$i-1]==0)?0:round($ama_special_list[$i-1]/$ama_total_list[$i-1]*100, 1);
		        $_date = $date_list[$i-1];
				
				echo("['".$_date."',".$_bascicredit.",".$_firstcredit.",".$_seasoncredit.",".$_specialcredit."]");
				
				if ($i > 1)
					echo(",");
			}
?>
        ]);        
        
        var options = {
            width:1050,                         
            height:470,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,            
        	chartArea:{left:100,top:40,width:1000,height:300}
        };

        var chart1 = new google.visualization.LineChart(document.getElementById('chart_div1'));
		chart1.draw(data1, options);
		
		var chart2 = new google.visualization.LineChart(document.getElementById('chart_div2'));
		chart2.draw(data2, options);
		
		var chart3 = new google.visualization.LineChart(document.getElementById('chart_div3'));
		chart3.draw(data3, options);
		
		var chart4 = new google.visualization.LineChart(document.getElementById('chart_div4'));
		chart4.draw(data4, options);     
    }
        
	google.setOnLoadCallback(drawChart);

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">

	<!-- title_warp -->
	<div class="title_wrap">
		<form name="search_form" id="search_form"  method="get" action="pay_product_type_stats.php">
			<div class="title_wrap">
				<div class="title"><?= $top_menu_txt ?> &gt; 상품군 통계</div>
				<div class="search_box"> 
					<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
		            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
		            <input type=hidden name="tab" value="<?= $tab ?>">
		            <input type="button" class="btn_search" value="검색" onclick="search()" />
				</div>		
		    </div>
	    </form>	
    </div>
	<!-- //title_warp -->
	
    <div class="search_result">
    	<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다
    </div>
    	
	<div class="h2_title">[Web]</div>
	<div id="chart_div1" style="height:480px; min-width: 500px"></div>
	
	<div class="h2_title">[iOS]</div>
	<div id="chart_div2" style="height:480px; min-width: 500px"></div>
	
	<div class="h2_title">[Android]</div>
	<div id="chart_div3" style="height:480px; min-width: 500px"></div>
	
	<div class="h2_title">[Amazon]</div>
	<div id="chart_div4" style="height:480px; min-width: 500px"></div>
    

</div>
    	<!--  //CONTENTS WRAP -->
    	
	<div class="clear"></div>
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
