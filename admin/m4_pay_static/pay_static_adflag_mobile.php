<?
	$top_menu = "pay_static";
	$sub_menu = "pay_static_adflag_mobile";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_type = ($_GET["search_type"] == "") ? "" : $_GET["search_type"];
	
	if($_GET["start_date"] == "")
		$search_sdate = date("Y-m-d",strtotime("-13 days"));
	else
		$search_sdate = $_GET["start_date"];
	
	if($_GET["end_date"] == "")
		$search_edate = date("Y-m-d");
	else
		$search_edate = $_GET["end_date"];
	
	$start_date = $search_date;
	
	$db_other = new CDatabase_Other();
	
	$sql = "SELECT * FROM tbl_user_adflag_purchase_stat ".
			"WHERE '$search_sdate' <= today AND today <= '$search_edate' ".
			"ORDER BY today DESC";
	
	$log_list = $db_other->gettotallist($sql);
	
	$db_other->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#start_date").datepicker({ });
		$("#end_date").datepicker({ });
	});
	
	function search()
	{
		var search_form = document.search_form;
	    
		if (search_form.start_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.start_date.focus();
	    	return;
		} 
	
		if (search_form.end_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.end_date.focus();
	    	return;
		} 
	
		search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 모바일 유입자 결제 통계</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<span class="search_lbl">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="start_date" name="start_date" style="width:65px" readonly="readonly" value="<?= $search_sdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/> ~
				<input type="text" class="search_text" id="end_date" name="end_date" style="width:65px" readonly="readonly" value="<?= $search_edate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/>
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $search_sdate ?></span> ~ <span><?= $search_edate ?></span> 현황입니다
	</div>

	<table class="tbl_list_basic1">
		<colgroup>
            <col width="60">
            <col width="80">
            <col width="50">
            <col width="50">
            <col width="50">    
		</colgroup>
        <thead>
        	<tr>
        		<th>일자</th>
        		<th>페이스북 결제</th>
        		<th>아이폰 결제</th>
            	<th>안드로이드 결제</th>
            	<th>아마존 결제</th>            	
         	</tr>
        </thead>
        <tbody>
<?
	$row_count = sizeof($log_list);
	
    for($i=0; $i<$row_count; $i++)
    {
    	$today = $log_list[$i]["today"];
    	$facebook_total = $log_list[$i]["facebook_total"];
    	$ios_total = $log_list[$i]["mobile_total"];
    	$and_total = $log_list[$i]["android_total"];
    	$ama_total = $log_list[$i]["amazon_total"];
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="tdc point"><?= $today ?></td>
            <td class="tdc">$<?= number_format($facebook_total, 1) ?></td>
            <td class="tdc">$<?= number_format($ios_total, 1) ?></td>
            <td class="tdc">$<?= number_format($and_total, 1) ?></td>
            <td class="tdc">$<?= number_format($ama_total, 1) ?></td>
        </tr>
<?
    }
    
    if($row_count == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="5">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>
		
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>