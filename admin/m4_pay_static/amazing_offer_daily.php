<?
    $top_menu = "pay_static";
    $sub_menu = "amazing_offer_daily";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $today = date("Y-m-d");
    
    $os_type = ($_GET["os_type"] == "") ? "4" :$_GET["os_type"];
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    
    if($search_start_createdate == "")
        $search_start_createdate = date("Y-m-d", strtotime("-3 day"));
        
    if($search_end_createdate == "")
        $search_end_createdate = $today;
        
    $tail = "";
    
    if($os_type == "0")
    {
        $os_txt = "Web";
        $os_type_sql = "AND os_type = 0 ";
        
    }
    else if($os_type == "1")
    {
        $os_txt = "MOBILE";
        $os_type_sql = "AND os_type = 1 ";
    }
    else if($os_type == "2")
    {
        $os_txt = "Android";
        $os_type_sql = "AND os_type = 2 ";
    }
    else if($os_type == "3")
    {
        $os_txt = "Amazon";
        $os_type_sql = "AND os_type = 3 ";
    }
    else if($os_type == "4")
    {
        $os_txt = "Total";
    }
    
        
    $db_main2 = new CDatabase_Main2();
    
    $sql = " SELECT ". 
            " today ".
            " , TYPE as type".
            " ,SUM(target_usercount) AS target_usercount ".
            " ,SUM(offer_pay_count_group1_5) AS offer_pay_count_group1_5 ".
            " ,SUM(offer_pay_count_group1_19) AS offer_pay_count_group1_19 ".
            " ,SUM(offer_pay_count_group2_19) AS offer_pay_count_group2_19 ".
            " ,SUM(offer_pay_count_group2_99) AS offer_pay_count_group2_99 ".
            " ,SUM(non_offer_pay_count) as non_offer_pay_count".
            " ,SUM(non_offer_usercount) as non_offer_usercount,SUM(non_offer_user_paymoney) as non_offer_user_paymoney".
            " FROM tbl_retention_offer_daily t1 ".
            " WHERE TYPE != 3 AND today BETWEEN '$search_start_createdate' AND '$search_end_createdate' $os_type_sql ".
            " GROUP BY today, TYPE ";
    $amazing_data = $db_main2->gettotallist($sql);
    
    $today_arr = $db_main2->gettotallist("SELECT DISTINCT today FROM `tbl_retention_offer_daily` WHERE TYPE != 3 and today BETWEEN '$search_start_createdate' AND '$search_end_createdate' $os_type_sql ORDER BY today DESC");
    
    $db_main2->end();
    
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#start_createdate").datepicker({ });
	});
	
	$(function() {
		$("#end_createdate").datepicker({ });
	});
	
	function search()
	{
	    var search_form = document.search_form;
	        
	    if (search_form.startdate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    } 
	
	    if (search_form.enddate.value == "")
	    {
	        alert("기준일을 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    } 
	
	    search_form.submit();
	}
	function change_os_type(type)
	{
		var search_form = document.search_form;

		var total = document.getElementById("type_total");
		var web = document.getElementById("type_web");
		var ios = document.getElementById("type_ios");
		var android = document.getElementById("type_android");
		var amazon = document.getElementById("type_amazon");
		
		document.search_form.os_type.value = type;
	
		if (type == "0")
		{
			web.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "1")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "2")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
			total.className="btn_schedule";
		}
		else if (type == "3")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}
		else if (type == "4")
		{
			web.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
			total.className="btn_schedule_select";
		}
	
		search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap" style="height:76px;">
		<div class="title"><?= $top_menu_txt ?> &gt; <?=$platform_name?> 어메이징 딜 통계</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
			<input type="hidden" name="os_type" id="os_type" value="<?= $os_type ?>" />

        		<span style="font:12px;color:#000;font-weight:bold;cursor:ponter;"><?= $title ?><br/>
					<input type="button" class="<?= ($os_type == "4") ? "btn_schedule_select" : "btn_schedule" ?>" value="ALL" id="type_total" onclick="change_os_type('4')"    />
					<input type="button" class="<?= ($os_type == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="type_web" onclick="change_os_type('0')"    />
					<input type="button" class="<?= ($os_type == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="type_ios" onclick="change_os_type('1')" />
					<input type="button" class="<?= ($os_type == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="type_android" onclick="change_os_type('2')"    />
					<input type="button" class="<?= ($os_type == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="type_amazon" onclick="change_os_type('3')"    />
				</span>
				<span class="search_lbl ml20">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" /> ~
				<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	<div class="search_result"><?= $os_txt?></div>
			<div class="search_result">
        		<span><?= $search_start_createdate ?> ~ <?= $search_end_createdate ?></span> 통계입니다
        	</div>
    		<table class="tbl_list_basic1">
    		<colgroup>
    			<col width="70">
    			<col width="120">
    			<col width="120">
                <col width="120">
                <col width="120">
                <col width="120">
                <col width="50">
                <col width="50">
                <col width="50">
    		</colgroup>
            <thead>
            <tr>
             	<th rowspan="2">날짜</th>
    			<th colspan="2">어메이징 딜 대상 인원</th>
    			<th colspan="4">어메이징 딜 구매 인원</th>
    			<!-- th rowspan="2">다른 상품 </br>구매 인원</th -->
    			<!-- th colspan="2">어메이징 딜 대상자 비적용 인원</th -->
    		</tr>
            <tr>
            	<th>1 타입</br>(누적 결제금 액이 </br>$5이상~$19미만)</th>
            	<th>2 타입</br>(누적 결제금 액이 </br>$19이상~$499미만)</th>
            	<th>1 타입</br>$5($4.99)</br>상품 구매</th>
            	<th>1 타입</br>$19($19.99)</br>상품 구매</th>
            	<th>2 타입</br>$19($19.99)</br>상품 구매</th>
            	<th>2 타입</br>$99($99.99)</br>상품 구매</th>
        		<!-- th>인원</th>
        		<th>결제 금액</th -->
        	</tr>
            </thead>
            <tbody>
	
<?
    $amazing_data_stat = array();
    
    for($i=0; $i<sizeof($amazing_data); $i++)
    {
        $today = $amazing_data[$i]["today"];
	$type = $amazing_data[$i]["type"];
	
        $target_usercount = $amazing_data[$i]["target_usercount"];
        $offer_pay_count_group1_5 = $amazing_data[$i]["offer_pay_count_group1_5"];
        $offer_pay_count_group1_19 = $amazing_data[$i]["offer_pay_count_group1_19"];
        $offer_pay_count_group2_19 = $amazing_data[$i]["offer_pay_count_group2_19"];
        $offer_pay_count_group2_99 = $amazing_data[$i]["offer_pay_count_group2_99"];
        $non_offer_pay_count = $amazing_data[$i]["non_offer_pay_count"];
        $non_offer_usercount = $amazing_data[$i]["non_offer_usercount"];
        $non_offer_user_paymoney = $amazing_data[$i]["non_offer_user_paymoney"];
        
        $amazing_data_stat [$today][$type]['target_usercount'] = $target_usercount;
        if($type == 1)
        {
            $amazing_data_stat [$today][$type]['offer_pay_count_group1_5'] = $offer_pay_count_group1_5;
            $amazing_data_stat [$today][$type]['offer_pay_count_group1_19'] = $offer_pay_count_group1_19;
        }
        else if ($type == 2)
        {
            $amazing_data_stat [$today][$type]['offer_pay_count_group2_19'] = $offer_pay_count_group2_19;
            $amazing_data_stat [$today][$type]['offer_pay_count_group2_99'] = $offer_pay_count_group2_99;
        }
        
        $amazing_data_stat [$today]['non_offer_pay_count'] = $non_offer_pay_count;
        $amazing_data_stat [$today]['non_offer_usercount'] = $non_offer_usercount;
        $amazing_data_stat [$today]['non_offer_user_paymoney'] = $non_offer_user_paymoney;
    }

    for($i=0; $i<sizeof($today_arr); $i++)
    {
        $today = $today_arr[$i]["today"];
        
        
        ?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="tdc point"><?= $today ?></td>
            <td class="tdc"><?= number_format($amazing_data_stat[$today][1]['target_usercount']) ?></td>
            <td class="tdc"><?= number_format($amazing_data_stat[$today][2]['target_usercount']) ?></td>
            <td class="tdc"><?= number_format($amazing_data_stat[$today][1]['offer_pay_count_group1_5']) ?></td>
            <td class="tdc"><?= number_format($amazing_data_stat[$today][1]['offer_pay_count_group1_19']) ?></td>
            <td class="tdc"><?= number_format($amazing_data_stat[$today][2]['offer_pay_count_group2_19']) ?></td>
            <td class="tdc"><?= number_format($amazing_data_stat[$today][2]['offer_pay_count_group2_99']) ?></td>
            <!-- td class="tdc"><?= number_format($amazing_data_stat[$today]['non_offer_pay_count']) ?></td -->
            <!-- td class="tdc"><?= number_format($amazing_data_stat[$today]['non_offer_usercount']) ?></td>
            <td class="tdc"><?= number_format($amazing_data_stat[$today]['non_offer_user_paymoney']) ?></td -->
        </tr>
<?
    }
    
    if(sizeof($amazing_data_stat) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="5">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
            </tbody>
    	</table>
</div>
<!--  //CONTENTS WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>