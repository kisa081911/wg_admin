<?
	$top_menu = "pay_static";
	$sub_menu = "pay_stat";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$platform = ($_GET["platform"] == "") ? "ALL" : $_GET["platform"];
	$dayofweek = ($_GET["dayofweek"] == "") ? "0" : $_GET["dayofweek"];
	$is_sale = ($_GET["is_sale"] == "") ? "ALL" : $_GET["is_sale"];
	$viewmode = ($_GET["viewmode"] == "") ? "0" : $_GET["viewmode"];
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
	
	check_xss($platform);
	check_xss($dayofweek);
	check_xss($is_sale);
	check_xss($viewmode);
	check_xss($startdate);
	check_xss($enddate);
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today, 28, "d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db_other = new CDatabase_Other();
	
	$platform_sql = "";
	
	if($platform != "ALL")
	{
	    $platform_sql = "AND platform=$platform ";
	}
	
	$week_sql = "";
	
	if($dayofweek != 0)
	{
	    $week_sql = " AND dayweek = $dayofweek ";
	}
	
	$is_sale_sql = "";
	
	if($is_sale != "ALL")
	{
	    $is_sale_sql = " AND is_sale = $is_sale ";
	}
	
	if($viewmode == 0)
		$order_str = "ASC";
	else
		$order_str = "DESC";
	
	$sql = "SELECT today, ". 
            "   IFNULL(SUM(IF(category = 'offer_coinbox_basic', total_money, 0)), 0) AS basic_money, IFNULL(SUM(IF(category = 'offer_coinbox_basic', payer_cnt, 0)), 0) AS basic_payer_cnt, ". 
            "   IFNULL(SUM(IF(category = 'offer_coinbox_basic', base_coin, 0)), 0) AS basic_base_coin,IFNULL(SUM(IF(category = 'offer_coinbox_basic', buy_coin, 0)), 0) AS basic_buy_coin, ".
            "   IFNULL(SUM(IF(category = 'offer_coinbox_coupon_basic', total_money, 0)), 0) AS coupon_money, IFNULL(SUM(IF(category = 'offer_coinbox_coupon_basic', payer_cnt, 0)), 0) AS coupon_payer_cnt, ". 
            "   IFNULL(SUM(IF(category = 'offer_coinbox_coupon_basic', base_coin, 0)), 0) AS coupon_base_coin,IFNULL(SUM(IF(category = 'offer_coinbox_coupon_basic', buy_coin, 0)), 0) AS coupon_buy_coin, ".
            "   IFNULL(SUM(IF(category = 'offer_coinbox_coupon_mobile', total_money, 0)), 0) AS coupon_mobile_money, IFNULL(SUM(IF(category = 'offer_coinbox_coupon_mobile', payer_cnt, 0)), 0) AS coupon_mobile_payer_cnt, ". 
            "   IFNULL(SUM(IF(category = 'offer_coinbox_coupon_mobile', base_coin, 0)), 0) AS coupon_mobile_base_coin,IFNULL(SUM(IF(category = 'offer_coinbox_coupon_mobile', buy_coin, 0)), 0) AS coupon_mobile_buy_coin, ".
            "   IFNULL(SUM(IF(category = 'offer_coinbox_coupon_season', total_money, 0)), 0) AS coupon_season_money, IFNULL(SUM(IF(category = 'offer_coinbox_coupon_season', payer_cnt, 0)), 0) AS coupon_season_payer_cnt, ". 
            "   IFNULL(SUM(IF(category = 'offer_coinbox_coupon_season', base_coin, 0)), 0) AS coupon_season_base_coin,IFNULL(SUM(IF(category = 'offer_coinbox_coupon_season', buy_coin, 0)), 0) AS coupon_season_buy_coin, ".
            "   IFNULL(SUM(IF(category = 'offer_coinbox_coupon_incentive', total_money, 0)), 0) AS coupon_incentive_money, IFNULL(SUM(IF(category = 'offer_coinbox_coupon_incentive', payer_cnt, 0)), 0) AS coupon_incentive_payer_cnt, ". 
            "   IFNULL(SUM(IF(category = 'offer_coinbox_coupon_incentive', base_coin, 0)), 0) AS coupon_incentive_base_coin,IFNULL(SUM(IF(category = 'offer_coinbox_coupon_incentive', buy_coin, 0)), 0) AS coupon_incentive_buy_coin, ".
            "   IFNULL(SUM(IF(category = 'offer_coinbox_coupon_vip', total_money, 0)), 0) AS coupon_vip_money, IFNULL(SUM(IF(category = 'offer_coinbox_coupon_vip', payer_cnt, 0)), 0) AS coupon_vip_payer_cnt, ". 
            "   IFNULL(SUM(IF(category = 'offer_coinbox_coupon_vip', base_coin, 0)), 0) AS coupon_vip_base_coin,IFNULL(SUM(IF(category = 'offer_coinbox_coupon_vip', buy_coin, 0)), 0) AS coupon_vip_buy_coin ".
            "FROM ( ".
            "   SELECT today, type_name AS category, SUM(money) AS total_money, COUNT(DISTINCT useridx) AS payer_cnt, COUNT(*) AS pay_cnt, SUM(basecoin) AS base_coin, SUM(coin) AS buy_coin ".
            "   FROM ( ".
            "       SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, orderidx, platform, useridx, product_type, coupon_type, money, basecoin, coin ".
            "       FROM `tbl_product_order_detail` ".
            "       WHERE useridx > 20000 AND '$startdate 00:00:00' <= writedate AND writedate <= '$enddate 23:59:59' AND status = 1 $platform_sql $week_sql $is_sale_sql ".
            "   ) t1 LEFT JOIN `tbl_product_type` t2 ON t1.product_type = t2.product_type AND t1.coupon_type = t2.coupon_type ".
            "   WHERE type_name LIKE 'offer_coinbox%' ".
            "   GROUP BY today, category ".
            ") total ".
            "GROUP BY today ".
            "ORDER BY today $order_str";
	$order_list = $db_other->gettotallist($sql);

	$db_other->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>

<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

	function change_platform(term)
	{
		var search_form = document.search_form;
		
		var term_all = document.getElementById("term_all");
		var term_fb = document.getElementById("term_fb");
		var term_ios = document.getElementById("term_ios");
		var term_and = document.getElementById("term_and");
		var term_ama = document.getElementById("term_ama");

		document.search_form.platform.value = term;
		
		if (term == "ALL")
		{
			term_all.className="btn_schedule_select";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 0)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule_select";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 1)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule_select";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 2)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule_select";
			term_ama.className="btn_schedule";
		}
		else if (term == 3)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule_select";
		}

		search_form.submit();
	}

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}

	function tab_change(tab)
    {
		window.location.href = "/m4_pay_static/pay_stat/pay_stat_" + tab + ".php";
    }
    
	google.load("visualization", "1", {packages:["corechart"]});

    function drawChart() 
    {
    	var data0 = new google.visualization.DataTable();
        data0.addColumn('string', '날짜');
        data0.addColumn('number', '정상구매($)');
        data0.addColumn('number', '정기쿠폰구매($)');
        data0.addColumn('number', '시즌쿠폰구매($)');
        data0.addColumn('number', '인센티브쿠폰구매($)');
        data0.addColumn('number', '모바일유도쿠폰구매($)');
        data0.addColumn('number', 'VIP쿠폰구매($)');
        data0.addRows([
<?
    for ($i=0; $i<sizeof($order_list); $i++)
	{
	    $today = $order_list[$i]["today"];
	    $basic_money = $order_list[$i]["basic_money"];
	    $coupon_money = $order_list[$i]["coupon_money"];
	    $coupon_season_money = $order_list[$i]["coupon_season_money"];
	    $coupon_incentive_money = $order_list[$i]["coupon_incentive_money"];
	    $coupon_mobile_money = $order_list[$i]["coupon_mobile_money"];
	    $coupon_vip_money = $order_list[$i]["coupon_vip_money"];
	    		
	    echo("['".$today."',".$basic_money.",".$coupon_money.",".$coupon_season_money.",".$coupon_incentive_money.",".$coupon_mobile_money.",".$coupon_vip_money."]");
		
	    if ($i != sizeof($order_list) - 1)
			echo(",");
	}
?>
        ]);

        var data1 = new google.visualization.DataTable();
        data1.addColumn('string', '날짜');
        data1.addColumn('number', '정상구매(%)');
        data1.addColumn('number', '정기쿠폰구매(%)');
        data1.addColumn('number', '시즌쿠폰구매(%)');
        data1.addColumn('number', '인센티브쿠폰구매(%)');
        data1.addColumn('number', '모바일유도쿠폰구매(%)');
        data1.addColumn('number', '쿠폰구매오퍼(%)');
        data1.addRows([
<?
    for ($i=0; $i<sizeof($order_list); $i++)
	{
	    $today = $order_list[$i]["today"];
	    $basic_money = $order_list[$i]["basic_money"];
	    $coupon_money = $order_list[$i]["coupon_money"];
	    $coupon_season_money = $order_list[$i]["coupon_season_money"];
	    $coupon_incentive_money = $order_list[$i]["coupon_incentive_money"];
	    $coupon_mobile_money = $order_list[$i]["coupon_mobile_money"];
	    $coupon_vip_money = $order_list[$i]["coupon_vip_money"];
	    $total_money = $basic_money + $coupon_money + $coupon_season_money + $coupon_incentive_money + $coupon_mobile_money + $coupon_vip_money;
	    
	    $basic_rate = ($total_money == 0) ? 0 : round($basic_money/$total_money*100, 2);
	    $coupon_rate = ($total_money == 0) ? 0 : round($coupon_money/$total_money*100, 2);
	    $coupon_season_rate = ($total_money == 0) ? 0 : round($coupon_season_money/$total_money*100, 2);
	    $coupon_incentive_rate = ($total_money == 0) ? 0 : round($coupon_incentive_money/$total_money*100, 2);
	    $coupon_mobile_rate = ($total_money == 0) ? 0 : round($coupon_mobile_money/$total_money*100, 2);
	    $coupon_vip_rate = ($total_money == 0) ? 0 : round($coupon_vip_money/$total_money*100, 2);
		
	    echo("['".$today."',".$basic_rate.",".$coupon_rate.",".$coupon_season_rate.",".$coupon_incentive_rate.",".$coupon_mobile_rate.",".$coupon_vip_rate."]");
		
	    if ($i != sizeof($order_list) - 1)
			echo(",");
	}
?>
        ]);

        var data2 = new google.visualization.DataTable();
        data2.addColumn('string', '날짜');
        data2.addColumn('number', '정상구매(%)');
        data2.addColumn('number', '정기쿠폰구매(%)');
        data2.addColumn('number', '시즌쿠폰구매(%)');
        data2.addColumn('number', '인센티브쿠폰구매(%)');
        data2.addColumn('number', '모바일유도쿠폰구매(%)');
        data2.addColumn('number', '쿠폰구매오퍼(%)');
        data2.addRows([
<?
    for ($i=0; $i<sizeof($order_list); $i++)
	{
	    $today = $order_list[$i]["today"];
	    $basic_base_coin = $order_list[$i]["basic_base_coin"];
	    $basic_buy_coin = $order_list[$i]["basic_buy_coin"];
	    $coupon_base_coin = $order_list[$i]["coupon_base_coin"];
	    $coupon_buy_coin = $order_list[$i]["coupon_buy_coin"];
	    $coupon_season_base_coin = $order_list[$i]["coupon_season_base_coin"];
	    $coupon_season_buy_coin = $order_list[$i]["coupon_season_buy_coin"];
	    $coupon_incentive_base_coin = $order_list[$i]["coupon_incentive_base_coin"];
	    $coupon_incentive_buy_coin = $order_list[$i]["coupon_incentive_buy_coin"];
	    $coupon_mobile_base_coin = $order_list[$i]["coupon_mobile_base_coin"];
	    $coupon_mobile_buy_coin = $order_list[$i]["coupon_mobile_buy_coin"];
	    $coupon_vip_base_coin = $order_list[$i]["coupon_vip_base_coin"];
	    $coupon_vip_buy_coin = $order_list[$i]["coupon_vip_buy_coin"];
	    
	    $basic_morerate = ($basic_base_coin == 0) ? 100 : round($basic_buy_coin/$basic_base_coin*100, 2);
	    $coupon_morerate = ($coupon_base_coin == 0) ? 100 : round($coupon_buy_coin/$coupon_base_coin*100, 2);
	    $coupon_season_morerate = ($coupon_season_base_coin == 0) ? 100 : round($coupon_season_buy_coin/$coupon_season_base_coin*100, 2);
	    $coupon_incentive_morerate = ($coupon_incentive_base_coin == 0) ? 100 : round($coupon_incentive_buy_coin/$coupon_incentive_base_coin*100, 2);
	    $coinbox_morerate = ($coupon_mobile_base_coin == 0) ? 100 : round($coupon_mobile_buy_coin/$coupon_mobile_base_coin*100, 2);
	    $coupon_vip_morerate = ($coupon_vip_base_coin == 0) ? 100 : round($coupon_vip_buy_coin/$coupon_vip_base_coin*100, 2);
		
	    echo("['".$today."',".$basic_morerate.",".$coupon_morerate.",".$coupon_season_morerate.",".$coupon_incentive_morerate.",".$coinbox_morerate.",".$coupon_vip_morerate."]");
		
	    if ($i != sizeof($order_list) - 1)
			echo(",");
	}
?>
        ]);

    	var options1 = {
            width:1050,                         
            height:470,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:300}
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div0'));
        chart.draw(data0, options1);

        chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
        chart.draw(data1, options1);

        chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
        chart.draw(data2, options1);
    }
        
	google.setOnLoadCallback(drawChart);
</script>
		<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; 코인박스 상세 통계</div>
	        </div>
	    	<!-- //title_warp -->
	    	
	    	<ul class="tab">
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('all')">전체</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub')">분류별</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_coupon')">쿠폰상세</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_offer')">오퍼상세</li>
				<li class="select" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_offer_coinbox')">코인박스상세</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_offer_period')">기간제오퍼상세</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_recent_4w')">최근 28일 결제금액별</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_accrue')">누적 결제금액별</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_daypurchase')">재결제일별</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_group')">그룹별</li>
			</ul>
	    	
	    	<form name="search_form" id="search_form"  method="get" action="pay_stat_sub_offer_coinbox.php">
	    		<div class="detail_search_wrap">
	    			<input type="hidden" name="platform" id="platform" value="<?= $platform ?>" />
	    			
	    			<input type="button" class="<?= ($platform == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_all" onclick="change_platform('ALL')" value="통합" />
	    			<input type="button" class="<?= ($platform == "0") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_fb" onclick="change_platform('0')" value="Facebook" />
	    			<input type="button" class="<?= ($platform == "1") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ios" onclick="change_platform('1')" value="iOS" />
	    			<input type="button" class="<?= ($platform == "2") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_and" onclick="change_platform('2')" value="Android" />
	    			<input type="button" class="<?= ($platform == "3") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ama" onclick="change_platform('3')" value="Amazon" />
	    			
	    			<span class="search_lb2 ml10">요일</span>
					<select name="dayofweek" id="dayofweek">										
						<option value="0" <?= ($dayofweek=="0") ? "selected" : "" ?>>전체</option>
						<option value="1" <?= ($dayofweek=="1") ? "selected" : "" ?>>일요일</option>                       
						<option value="2" <?= ($dayofweek=="2") ? "selected" : "" ?>>월요일</option>					
						<option value="3" <?= ($dayofweek=="3") ? "selected" : "" ?>>화요일</option>					
						<option value="4" <?= ($dayofweek=="4") ? "selected" : "" ?>>수요일</option>
						<option value="5" <?= ($dayofweek=="5") ? "selected" : "" ?>>목요일</option>
						<option value="6" <?= ($dayofweek=="6") ? "selected" : "" ?>>금요일</option>
						<option value="7" <?= ($dayofweek=="7") ? "selected" : "" ?>>토요일</option>					
					</select>
	    			
	    			<span class="search_lb2 ml10">시즌세일여부</span>
	    			<select name="is_sale" id="is_sale">										
						<option value="ALL" <?= ($is_sale=="ALL") ? "selected" : "" ?>>전체</option>
						<option value="0" <?= ($is_sale=="0") ? "selected" : "" ?>>시즌세일기간아님</option>
						<option value="1" <?= ($is_sale=="1") ? "selected" : "" ?>>시즌세일기간</option>                       				
					</select>
					
					<span class="search_lb2 ml10">View</span>
	    			<select name="viewmode" id="viewmode">										
						<option value="0" <?= ($viewmode=="0") ? "selected" : "" ?>>그래프</option>
						<option value="1" <?= ($viewmode=="1") ? "selected" : "" ?>>리스트</option>                       				
					</select>
	    			
	    			<span class="search_lb2 ml10">날짜</span>
	    			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
		            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
					<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
				</div>
    		</form>
<?
	if($viewmode == 0)
	{
?>
	    	<div class="h2_title">[결제금액]</div>
	    	<div id="chart_div0" style="height:480px; min-width: 500px"></div>
	    	
	    	<div class="h2_title">[결제비중]</div>
	    	<div id="chart_div1" style="height:480px; min-width: 500px"></div>
	    	
	    	<div class="h2_title">[할인율]</div>
	    	<div id="chart_div2" style="height:480px; min-width: 500px"></div>
<?          
	}
	else if($viewmode == 1)
	{
?>
			<div id="tab_content_1" style="width:1500px;">
	    		<table class="tbl_list_basic1">
	    			<colgroup>
	    				<col width="">
	    				<col width="">
	    				<col width="">
	    				<col width="">
	    			</colgroup>
	    			<thead>
	    				<tr>
	    					<th class="tdc" rowspan = 2>날짜</th>
	    					<th class="tdc" colspan = 6>결제 금액($)</th>
	    					<th class="tdc" colspan = 6>결제 비중(%)</th>
	    					<th class="tdc" colspan = 6>할인율(%)</th>
	    				</tr>
	    				<tr>
	    					<th class="tdc">정상</th>
	    					<th class="tdc">정기쿠폰</th>
	    					<th class="tdc">시즌쿠폰</th>
	    					<th class="tdc">인센티브쿠폰</th>
	    					<th class="tdc">모바일유도쿠폰</th>
	    					<th class="tdc">VIP쿠폰</th>	    					
	    					<th class="tdc">정상</th>
	    					<th class="tdc">정기쿠폰</th>
	    					<th class="tdc">시즌쿠폰</th>
	    					<th class="tdc">인센티브폰</th>
	    					<th class="tdc">모바일유도쿠폰</th>
	    					<th class="tdc">VIP쿠폰</th>	    					
	    					<th class="tdc">정상</th>
	    					<th class="tdc">정기쿠폰</th>
	    					<th class="tdc">시즌쿠폰</th>
	    					<th class="tdc">인센티브쿠폰</th>
	    					<th class="tdc">모바일유도쿠폰</th>
	    					<th class="tdc">VIP쿠폰</th>   
	    				</tr>
	    			</thead>
	    			<tbody>
<?
    			for($i=0; $i<sizeof($order_list); $i++)
    			{
    			    $today = $order_list[$i]["today"];
    			    
    			   	$basic_money = $order_list[$i]["basic_money"];
				    $coupon_money = $order_list[$i]["coupon_money"];
				    $coupon_season_money = $order_list[$i]["coupon_season_money"];
				    $coupon_incentive_money = $order_list[$i]["coupon_incentive_money"];
				    $coupon_mobile_money = $order_list[$i]["coupon_mobile_money"];
				    $coupon_vip_money = $order_list[$i]["coupon_vip_money"];	   
				    
				    $total_money = $basic_money + $coupon_money + $coupon_season_money + $coupon_incentive_money + $coupon_mobile_money + $coupon_vip_money;
				     
				    $basic_rate = ($total_money == 0) ? 0 : round($basic_money/$total_money*100, 2);
				    $coupon_rate = ($total_money == 0) ? 0 : round($coupon_money/$total_money*100, 2);
				    $coupon_season_rate = ($total_money == 0) ? 0 : round($coupon_season_money/$total_money*100, 2);
				    $coupon_incentive_rate = ($total_money == 0) ? 0 : round($coupon_incentive_money/$total_money*100, 2);
				    $coupon_mobile_rate = ($total_money == 0) ? 0 : round($coupon_mobile_money/$total_money*100, 2);
				    $coupon_vip_rate = ($total_money == 0) ? 0 : round($coupon_vip_money/$total_money*100, 2);
				    
				    $basic_base_coin = $order_list[$i]["basic_base_coin"];
				    $basic_buy_coin = $order_list[$i]["basic_buy_coin"];
				    $coupon_base_coin = $order_list[$i]["coupon_base_coin"];
				    $coupon_buy_coin = $order_list[$i]["coupon_buy_coin"];
				    $coupon_season_base_coin = $order_list[$i]["coupon_season_base_coin"];
				    $coupon_season_buy_coin = $order_list[$i]["coupon_season_buy_coin"];
				    $coupon_incentive_base_coin = $order_list[$i]["coupon_incentive_base_coin"];
				    $coupon_incentive_buy_coin = $order_list[$i]["coupon_incentive_buy_coin"];
				    $coupon_mobile_base_coin = $order_list[$i]["coupon_mobile_base_coin"];
				    $coupon_mobile_buy_coin = $order_list[$i]["coupon_mobile_buy_coin"];
				    $coupon_vip_base_coin = $order_list[$i]["coupon_vip_base_coin"];
				    $coupon_vip_buy_coin = $order_list[$i]["coupon_vip_buy_coin"];
				     
				    $basic_morerate = ($basic_base_coin == 0) ? 100 : round($basic_buy_coin/$basic_base_coin*100, 2);
				    $coupon_morerate = ($coupon_base_coin == 0) ? 100 : round($coupon_buy_coin/$coupon_base_coin*100, 2);
				    $coupon_season_morerate = ($coupon_season_base_coin == 0) ? 100 : round($coupon_season_buy_coin/$coupon_season_base_coin*100, 2);
				    $coupon_incentive_morerate = ($coupon_incentive_base_coin == 0) ? 100 : round($coupon_incentive_buy_coin/$coupon_incentive_base_coin*100, 2);
				    $coinbox_morerate = ($coupon_mobile_base_coin == 0) ? 100 : round($coupon_mobile_buy_coin/$coupon_mobile_base_coin*100, 2);
				    $coupon_vip_morerate = ($coupon_vip_base_coin == 0) ? 100 : round($coupon_vip_buy_coin/$coupon_vip_base_coin*100, 2);
?>
					<tr>
    					<td class="tdc point_title"><?= $today?></td>    					
    					<td class="tdc">$<?= number_format($basic_money, 2) ?></td>
    					<td class="tdc">$<?= number_format($coupon_money, 2) ?></td>
    					<td class="tdc">$<?= number_format($coupon_season_money, 2) ?></td>
    					<td class="tdc">$<?= number_format($coupon_incentive_money, 2) ?></td>
    					<td class="tdc">$<?= number_format($coupon_mobile_money, 2) ?></td>
    					<td class="tdc">$<?= number_format($coupon_vip_money, 2) ?></td>
    					
    					
    					<td class="tdc"><?= number_format($basic_rate, 2) ?>%</td>
    					<td class="tdc"><?= number_format($coupon_rate, 2) ?>%</td>
    					<td class="tdc"><?= number_format($coupon_season_rate, 2) ?>%</td>
    					<td class="tdc"><?= number_format($coupon_incentive_rate, 2) ?>%</td>
    					<td class="tdc"><?= number_format($coupon_mobile_rate, 2) ?>%</td> 
    					<td class="tdc"><?= number_format($coupon_vip_rate, 2) ?>%</td> 
    					
    					<td class="tdc"><?= number_format($basic_morerate, 2) ?>%</td>
    					<td class="tdc"><?= number_format($coupon_morerate, 2) ?>%</td>
    					<td class="tdc"><?= number_format($coupon_season_morerate, 2) ?>%</td>
    					<td class="tdc"><?= number_format($coupon_incentive_morerate, 2) ?>%</td>
    					<td class="tdc"><?= number_format($coinbox_morerate, 2) ?>%</td> 
    					<td class="tdc"><?= number_format($coupon_vip_morerate, 2) ?>%</td> 
    				</tr>
<?
    			}
?>
    			</tbody>
    		</table>
    	</div>
<?
	}
?>
    	</div>
    	<!--  //CONTENTS WRAP -->
    	
    	<div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>