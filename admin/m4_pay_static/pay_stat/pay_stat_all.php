<?
    $top_menu = "pay_static";
    $sub_menu = "pay_stat";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $platform = ($_GET["platform"] == "") ? "ALL" : $_GET["platform"];
    $dayofweek = ($_GET["dayofweek"] == "") ? "0" : $_GET["dayofweek"];
    $viewmode = ($_GET["viewmode"] == "") ? "0" : $_GET["viewmode"];
    $is_sale = ($_GET["is_sale"] == "") ? "ALL" : $_GET["is_sale"];
    
    $startdate = $_GET["startdate"];
    $enddate = $_GET["enddate"];
    
    check_xss($platform);
    check_xss($dayofweek);
    check_xss($is_sale);
    check_xss($viewmode);
    check_xss($startdate);
    check_xss($enddate);
    
    //오늘 날짜 정보
    $today = date("Y-m-d");
    $before_day = get_past_date($today, 28, "d");
    $startdate = ($startdate == "") ? $before_day : $startdate;
    $enddate = ($enddate == "") ? $today : $enddate;
    
    $db_other = new CDatabase_Other();
    
    $platform_sql = "";
    
    if($platform != "ALL")
    {
        $platform_sql = "AND platform=$platform ";
    }
    
    $week_sql = "";
    
    if($dayofweek != 0)
    {
        $week_sql = " AND dayweek = $dayofweek ";
    }
    
    $is_sale_sql = "";
    
    if($is_sale != "ALL")
    {
        $is_sale_sql = " AND is_sale = $is_sale ";
    }
    
    if($viewmode == 0)
        $order_str = "ASC";
    else
        $order_str = "DESC";
        
    $sql = "SELECT today, SUM(money) AS total_money, COUNT(DISTINCT useridx) AS payer_cnt, COUNT(*) AS pay_cnt, ROUND(SUM(money)/COUNT(DISTINCT useridx), 2) AS arppu, ROUND(SUM(coin)/SUM(basecoin)*100, 2) AS more_rate ".
        "FROM ( ".
        "   SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, orderidx, platform, useridx, ".
        "       product_type, coupon_type, money, basecoin, coin ".
        "   FROM `tbl_product_order_detail` ".
        "   WHERE useridx > 20000 AND '$startdate 00:00:00' <= writedate AND writedate <= '$enddate 23:59:59' AND status = 1 $platform_sql $week_sql $is_sale_sql ".
        ") t1 LEFT JOIN `tbl_product_type` t2 ON t1.product_type = t2.product_type AND t1.coupon_type = t2.coupon_type ".
        "GROUP BY today ".
        "ORDER BY today $order_str";
    $order_list = $db_other->gettotallist($sql);
    
    $db_other->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>

<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

	function change_platform(term)
	{
		var search_form = document.search_form;
		
		var term_all = document.getElementById("term_all");
		var term_fb = document.getElementById("term_fb");
		var term_ios = document.getElementById("term_ios");
		var term_and = document.getElementById("term_and");
		var term_ama = document.getElementById("term_ama");

		document.search_form.platform.value = term;
		
		if (term == "ALL")
		{
			term_all.className="btn_schedule_select";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 0)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule_select";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 1)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule_select";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 2)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule_select";
			term_ama.className="btn_schedule";
		}
		else if (term == 3)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule_select";
		}

		search_form.submit();
	}

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}

	function tab_change(tab)
    {
		window.location.href = "/m4_pay_static/pay_stat/pay_stat_" + tab + ".php";
    }
    
	google.load("visualization", "1", {packages:["corechart"]});

    function drawChart() 
    {
    	var data0 = new google.visualization.DataTable();
        data0.addColumn('string', '날짜');
        data0.addColumn('number', '총 결제금액($)');
        data0.addRows([
<?
    for ($i=0; $i<sizeof($order_list); $i++)
	{
	    $today = $order_list[$i]["today"];
	    $total_money = $order_list[$i]["total_money"];
		
	    echo("['".$today."',".$total_money."]");
		
	    if ($i != sizeof($order_list) - 1)
			echo(",");
	}
?>
        ]);

    	var data1 = new google.visualization.DataTable();
        data1.addColumn('string', '날짜');
        data1.addColumn('number', '총 결제자수(명)');
        data1.addRows([
<?
    for ($i=0; $i<sizeof($order_list); $i++)
	{
	    $today = $order_list[$i]["today"];
	    $payer_cnt = $order_list[$i]["payer_cnt"];
		
	    echo("['".$today."',".$payer_cnt."]");
		
	    if ($i != sizeof($order_list) - 1)
			echo(",");
	}
?>
        ]);

        var data2 = new google.visualization.DataTable();
        data2.addColumn('string', '날짜');
        data2.addColumn('number', '총 할인율(%)');
        data2.addRows([
<?
    for ($i=0; $i<sizeof($order_list); $i++)
	{
	    $today = $order_list[$i]["today"];
	    $more_rate = $order_list[$i]["more_rate"];
		
	    echo("['".$today."',".$more_rate."]");
		
	    if ($i != sizeof($order_list) - 1)
			echo(",");
	}
?>
        ]);

        var data3 = new google.visualization.DataTable();
        data3.addColumn('string', '날짜');
        data3.addColumn('number', 'ARPPU($)');
        data3.addRows([
<?
    for ($i=0; $i<sizeof($order_list); $i++)
	{
	    $today = $order_list[$i]["today"];
	    $arppu = $order_list[$i]["arppu"];
		
	    echo("['".$today."',".$arppu."]");
		
	    if ($i != sizeof($order_list) - 1)
			echo(",");
	}
?>
        ]);

    	var options1 = {
            width:1050,                         
            height:470,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:300}
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div0'));
        chart.draw(data0, options1);

        chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
        chart.draw(data1, options1);

        chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
        chart.draw(data2, options1);

        chart = new google.visualization.LineChart(document.getElementById('chart_div3'));
        chart.draw(data3, options1);
    }
        
	google.setOnLoadCallback(drawChart);
</script>
		<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; 전체 통계</div>
	        </div>
	    	<!-- //title_warp -->
	    	
	    	<ul class="tab">
				<li class="select" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('all')">전체</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub')">분류별</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_coupon')">쿠폰상세</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_offer')">오퍼상세</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_offer_coinbox')">코인박스상세</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_offer_period')">기간제오퍼상세</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_recent_4w')">최근 28일 결제금액별</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_accrue')">누적 결제금액별</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_daypurchase')">재결제일별</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_group')">그룹별</li>
			</ul>
	    	
	    	<form name="search_form" id="search_form"  method="get" action="pay_stat_all.php">
	    		<div class="detail_search_wrap">
	    			<input type="hidden" name="platform" id="platform" value="<?= $platform ?>" />
	    			
	    			<input type="button" class="<?= ($platform == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_all" onclick="change_platform('ALL')" value="통합" />
	    			<input type="button" class="<?= ($platform == "0") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_fb" onclick="change_platform('0')" value="Facebook" />
	    			<input type="button" class="<?= ($platform == "1") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ios" onclick="change_platform('1')" value="iOS" />
	    			<input type="button" class="<?= ($platform == "2") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_and" onclick="change_platform('2')" value="Android" />
	    			<input type="button" class="<?= ($platform == "3") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ama" onclick="change_platform('3')" value="Amazon" />
	    			
	    			<span class="search_lb2 ml10">요일</span>
					<select name="dayofweek" id="dayofweek">										
						<option value="0" <?= ($dayofweek=="0") ? "selected" : "" ?>>전체</option>
						<option value="1" <?= ($dayofweek=="1") ? "selected" : "" ?>>일요일</option>                       
						<option value="2" <?= ($dayofweek=="2") ? "selected" : "" ?>>월요일</option>					
						<option value="3" <?= ($dayofweek=="3") ? "selected" : "" ?>>화요일</option>					
						<option value="4" <?= ($dayofweek=="4") ? "selected" : "" ?>>수요일</option>
						<option value="5" <?= ($dayofweek=="5") ? "selected" : "" ?>>목요일</option>
						<option value="6" <?= ($dayofweek=="6") ? "selected" : "" ?>>금요일</option>
						<option value="7" <?= ($dayofweek=="7") ? "selected" : "" ?>>토요일</option>					
					</select>
	    			
	    			<span class="search_lb2 ml10">시즌세일여부</span>
	    			<select name="is_sale" id="is_sale">										
						<option value="ALL" <?= ($is_sale=="ALL") ? "selected" : "" ?>>전체</option>
						<option value="0" <?= ($is_sale=="0") ? "selected" : "" ?>>시즌세일기간아님</option>
						<option value="1" <?= ($is_sale=="1") ? "selected" : "" ?>>시즌세일기간</option>                       				
					</select>
					
					<span class="search_lb2 ml10">View</span>
	    			<select name="viewmode" id="viewmode">										
						<option value="0" <?= ($viewmode=="0") ? "selected" : "" ?>>그래프</option>
						<option value="1" <?= ($viewmode=="1") ? "selected" : "" ?>>리스트</option>                       				
					</select>
	    			
	    			<span class="search_lb2 ml10">날짜</span>
	    			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
		            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
					<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
				</div>
    		</form>
<?
	if($viewmode == 0)
	{
?>
	    	<div class="h2_title">[결제금액]</div>
	    	<div id="chart_div0" style="height:480px; min-width: 500px"></div>
	    	
	    	<div class="h2_title">[결제자]</div>
	    	<div id="chart_div1" style="height:480px; min-width: 500px"></div>
	    	
	    	<div class="h2_title">[할인율]</div>
	    	<div id="chart_div2" style="height:480px; min-width: 500px"></div>
	    	
	    	<div class="h2_title">[ARPPU]</div>
	    	<div id="chart_div3" style="height:480px; min-width: 500px"></div>
    	</div>
    	<!--  //CONTENTS WRAP -->
<?          
	}
	else if($viewmode == 1)
	{
?>    	
        <div id="tab_content_1">
    		<table class="tbl_list_basic1">
    			<colgroup>
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    			</colgroup>
    			<thead>
    				<tr>
    					<th class="tdc">날짜</th>
    					<th class="tdc">결제금액</th>
    					<th class="tdc">결제자</th>
    					<th class="tdc">결제액</th>
    					<th class="tdc">할인율</th>
    					<th class="tdc">ARPPU</th>
    				</tr>
    			</thead>
    			<tbody>    				
<?
    			for($i=0; $i<sizeof($order_list); $i++)
    			{
    			    $today = $order_list[$i]["today"];
    			    $total_money = $order_list[$i]["total_money"];
    			    $payer_cnt = $order_list[$i]["payer_cnt"];
    			    $pay_cnt = $order_list[$i]["pay_cnt"];
    			    $arppu = $order_list[$i]["arppu"];
    			    $more_rate = $order_list[$i]["more_rate"];
?>
					<tr>
    					<td class="tdc point_title"><?= $today?></td>
    					<td class="tdc">$<?= number_format($total_money, 2) ?></td>
    					<td class="tdc"><?= number_format($payer_cnt) ?></td>
    					<td class="tdc"><?= number_format($pay_cnt,2) ?></td>
    					<td class="tdc"><?= number_format($more_rate,2) ?>%</td>
    					<td class="tdc"><?= number_format($arppu, 2) ?></td>
    				</tr>
<?
			}
?>
    			</tbody>
    		</table>
    	</div>						
<?
            }
?>
    	<div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>