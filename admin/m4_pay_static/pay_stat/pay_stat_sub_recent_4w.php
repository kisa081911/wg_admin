<?
	$top_menu = "pay_static";
	$sub_menu = "pay_stat";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$platform = ($_GET["platform"] == "") ? "ALL" : $_GET["platform"];
	$dayofweek = ($_GET["dayofweek"] == "") ? "0" : $_GET["dayofweek"];
	$is_sale = ($_GET["is_sale"] == "") ? "ALL" : $_GET["is_sale"];
	$viewmode = ($_GET["viewmode"] == "") ? "0" : $_GET["viewmode"];
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
	
	check_xss($platform);
	check_xss($dayofweek);
	check_xss($is_sale);
	check_xss($viewmode);
	check_xss($startdate);
	check_xss($enddate);
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today, 28*6, "d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db_other = new CDatabase_Other();
	
	$platform_sql = "";
	
	if($platform != "ALL")
	{
	    $platform_sql = "AND platform=$platform ";
	}
	
	$week_sql = "";
	
	if($dayofweek != 0)
	{
	    $week_sql = " AND dayweek = $dayofweek ";
	}
	
	$is_sale_sql = "";
	
	if($is_sale != "ALL")
	{
	    $is_sale_sql = " AND is_sale = $is_sale ";
	}
	
	if($viewmode == 0)
		$order_str = "ASC";
	else
		$order_str = "DESC";
	
	$sql = "SELECT today, IFNULL(SUM(IF(recent_4w_grade = 1, total_money, 0)), 0) AS grade_1_money, IFNULL(SUM(IF(recent_4w_grade = 1, payer_cnt, 0)), 0) AS grade_1_payer_cnt, IFNULL(SUM(IF(recent_4w_grade = 1, buy_cnt, 0)), 0) AS grade_1_buy_cnt, ".
            "   IFNULL(SUM(IF(recent_4w_grade = 1, base_coin, 0)), 0) AS grade_1_base_coin, IFNULL(SUM(IF(recent_4w_grade = 1, buy_coin, 0)), 0) AS grade_1_buy_coin, ".
            "   IFNULL(SUM(IF(recent_4w_grade = 2, total_money, 0)), 0) AS grade_2_money, IFNULL(SUM(IF(recent_4w_grade = 2, payer_cnt, 0)), 0) AS grade_2_payer_cnt, IFNULL(SUM(IF(recent_4w_grade = 2, buy_cnt, 0)), 0) AS grade_2_buy_cnt, ".
            "   IFNULL(SUM(IF(recent_4w_grade = 2, base_coin, 0)), 0) AS grade_2_base_coin, IFNULL(SUM(IF(recent_4w_grade = 2, buy_coin, 0)), 0) AS grade_2_buy_coin, ".
            "   IFNULL(SUM(IF(recent_4w_grade = 3, total_money, 0)), 0) AS grade_3_money, IFNULL(SUM(IF(recent_4w_grade = 3, payer_cnt, 0)), 0) AS grade_3_payer_cnt, IFNULL(SUM(IF(recent_4w_grade = 3, buy_cnt, 0)), 0) AS grade_3_buy_cnt, ".
            "   IFNULL(SUM(IF(recent_4w_grade = 3, base_coin, 0)), 0) AS grade_3_base_coin, IFNULL(SUM(IF(recent_4w_grade = 3, buy_coin, 0)), 0) AS grade_3_buy_coin, ".
            "   IFNULL(SUM(IF(recent_4w_grade = 4, total_money, 0)), 0) AS grade_4_money, IFNULL(SUM(IF(recent_4w_grade = 4, payer_cnt, 0)), 0) AS grade_4_payer_cnt, IFNULL(SUM(IF(recent_4w_grade = 4, buy_cnt, 0)), 0) AS grade_4_buy_cnt, ".
            "   IFNULL(SUM(IF(recent_4w_grade = 4, base_coin, 0)), 0) AS grade_4_base_coin, IFNULL(SUM(IF(recent_4w_grade = 4, buy_coin, 0)), 0) AS grade_4_buy_coin, ".
            "   IFNULL(SUM(IF(recent_4w_grade = 5, total_money, 0)), 0) AS grade_5_money, IFNULL(SUM(IF(recent_4w_grade = 5, payer_cnt, 0)), 0) AS grade_5_payer_cnt, IFNULL(SUM(IF(recent_4w_grade = 5, buy_cnt, 0)), 0) AS grade_5_buy_cnt, ".
            "   IFNULL(SUM(IF(recent_4w_grade = 5, base_coin, 0)), 0) AS grade_5_base_coin, IFNULL(SUM(IF(recent_4w_grade = 5, buy_coin, 0)), 0) AS grade_5_buy_coin ".
            "FROM ( ".
            "   SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, ". 
            "       (CASE WHEN recent_4w_money >= 499 THEN 5 WHEN recent_4w_money >= 199 AND recent_4w_money < 499 THEN 4 WHEN recent_4w_money >= 59 AND recent_4w_money < 199 THEN 3 WHEN recent_4w_money >= 9 AND recent_4w_money < 59 THEN 2 WHEN recent_4w_money < 9 THEN 1 END ".
            "   ) AS recent_4w_grade, COUNT(DISTINCT useridx) AS payer_cnt, COUNT(*) AS buy_cnt, SUM(money) AS total_money, SUM(basecoin) AS base_coin, SUM(coin) AS buy_coin ".
            "   FROM `tbl_product_order_detail` ".
            "   WHERE useridx > 20000 AND '$startdate 00:00:00' <= writedate AND writedate <= '$enddate 23:59:59' AND status = 1 $platform_sql $week_sql $is_sale_sql ".
            "   GROUP BY today, recent_4w_grade ".
            ") t1 ".
            "GROUP BY today ".
            "ORDER BY today $order_str";
	$order_list = $db_other->gettotallist($sql);

	$db_other->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 

<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

	function change_platform(term)
	{
		var search_form = document.search_form;
		
		var term_all = document.getElementById("term_all");
		var term_fb = document.getElementById("term_fb");
		var term_ios = document.getElementById("term_ios");
		var term_and = document.getElementById("term_and");
		var term_ama = document.getElementById("term_ama");

		document.search_form.platform.value = term;
		
		if (term == "ALL")
		{
			term_all.className="btn_schedule_select";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 0)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule_select";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 1)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule_select";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 2)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule_select";
			term_ama.className="btn_schedule";
		}
		else if (term == 3)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule_select";
		}

		search_form.submit();
	}

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}

	function tab_change(tab)
    {
		window.location.href = "/m4_pay_static/pay_stat/pay_stat_" + tab + ".php";
    }
    
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart);

    function drawChart() 
    {
    	var data0 = google.visualization.arrayToDataTable([
		['날짜', '매출액', '결제자수'],
<?
            for ($i=0; $i<sizeof($order_list); $i++)
		    {
		        $today = $order_list[$i]["today"];
		        $grade_5_money = $order_list[$i]["grade_5_money"];
		        $grade_5_payer_cnt = $order_list[$i]["grade_5_payer_cnt"];
		
		        echo("['".$today."'");
		
		        if ($grade_5_money != "")
		            echo(",{v:".$grade_5_money.",f:'".number_format($grade_5_money)."'}");
				else
					echo(",0");
		
				if ($grade_5_payer_cnt != "")
				    echo(",{v:".$grade_5_payer_cnt.",f:'".number_format($grade_5_payer_cnt)."'}]");
				else
					echo(",0]");
		        
				if ($i != sizeof($order_list) - 1)
				    echo(",");
		    }
?>
		]);

    	var data1 = google.visualization.arrayToDataTable([
    		['날짜', '매출액', '결제자수'],
<?
            for ($i=0; $i<sizeof($order_list); $i++)
		    {
		        $today = $order_list[$i]["today"];
		        $grade_4_money = $order_list[$i]["grade_4_money"];
		        $grade_4_payer_cnt = $order_list[$i]["grade_4_payer_cnt"];
		
		        echo("['".$today."'");
		
		        if ($grade_4_money != "")
		            echo(",{v:".$grade_4_money.",f:'".number_format($grade_4_money)."'}");
				else
					echo(",0");
		
				if ($grade_4_payer_cnt != "")
				    echo(",{v:".$grade_4_payer_cnt.",f:'".number_format($grade_4_payer_cnt)."'}]");
				else
					echo(",0]");
		        
				if ($i != sizeof($order_list) - 1)
				    echo(",");
		    }
?>
		]);

    	var data2 = google.visualization.arrayToDataTable([
    		['날짜', '매출액', '결제자수'],
<?
            for ($i=0; $i<sizeof($order_list); $i++)
		    {
		        $today = $order_list[$i]["today"];
		        $grade_3_money = $order_list[$i]["grade_3_money"];
		        $grade_3_payer_cnt = $order_list[$i]["grade_3_payer_cnt"];
		
		        echo("['".$today."'");
		
		        if ($grade_3_money != "")
		            echo(",{v:".$grade_3_money.",f:'".number_format($grade_3_money)."'}");
				else
					echo(",0");
		
				if ($grade_3_payer_cnt != "")
				    echo(",{v:".$grade_3_payer_cnt.",f:'".number_format($grade_3_payer_cnt)."'}]");
				else
					echo(",0]");
		        
				if ($i != sizeof($order_list) - 1)
				    echo(",");
		    }
?>
		]);

    	var data3 = google.visualization.arrayToDataTable([
    		['날짜', '매출액', '결제자수'],
<?
            for ($i=0; $i<sizeof($order_list); $i++)
		    {
		        $today = $order_list[$i]["today"];
		        $grade_2_money = $order_list[$i]["grade_2_money"];
		        $grade_2_payer_cnt = $order_list[$i]["grade_2_payer_cnt"];
		
		        echo("['".$today."'");
		
		        if ($grade_2_money != "")
		            echo(",{v:".$grade_2_money.",f:'".number_format($grade_2_money)."'}");
				else
					echo(",0");
		
				if ($grade_2_payer_cnt != "")
				    echo(",{v:".$grade_2_payer_cnt.",f:'".number_format($grade_2_payer_cnt)."'}]");
				else
					echo(",0]");
		        
				if ($i != sizeof($order_list) - 1)
				    echo(",");
		    }
?>
		]);

    	var data4 = google.visualization.arrayToDataTable([
    		['날짜', '매출액', '결제자수'],
<?
            for ($i=0; $i<sizeof($order_list); $i++)
		    {
		        $today = $order_list[$i]["today"];
		        $grade_1_money = $order_list[$i]["grade_1_money"];
		        $grade_1_payer_cnt = $order_list[$i]["grade_1_payer_cnt"];
		
		        echo("['".$today."'");
		
		        if ($grade_1_money != "")
		            echo(",{v:".$grade_1_money.",f:'".number_format($grade_1_money)."'}");
				else
					echo(",0");
		
				if ($grade_1_payer_cnt != "")
				    echo(",{v:".$grade_1_payer_cnt.",f:'".number_format($grade_1_payer_cnt)."'}]");
				else
					echo(",0]");
		        
				if ($i != sizeof($order_list) - 1)
				    echo(",");
		    }
?>
		]);

        var options = {
    			width:1100,                         
        	    height:500,
        	    isStacked: 'true',
    			seriesType: 'steppedArea',
    			series: 
    			{
    				1: {
    					type: 'line',
    					targetAxisIndex:1
    				}
    			},
    			fontSize : 12,
    			colors:['#003399','red']
    		};

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div0'));
        chart.draw(data0, options);

        chart = new google.visualization.ComboChart(document.getElementById('chart_div1'));
        chart.draw(data1, options);

        chart = new google.visualization.ComboChart(document.getElementById('chart_div2'));
        chart.draw(data2, options);

        chart = new google.visualization.ComboChart(document.getElementById('chart_div3'));
        chart.draw(data3, options);

        chart = new google.visualization.ComboChart(document.getElementById('chart_div4'));
        chart.draw(data4, options);
    }
</script>
		<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; 최근 28일 결제금액별 통계</div>
	        </div>
	    	<!-- //title_warp -->
	    	
	    	<ul class="tab">
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('all')">전체</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub')">분류별</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_coupon')">쿠폰상세</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_offer')">오퍼상세</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_offer_coinbox')">코인박스상세</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_offer_period')">기간제오퍼상세</li>
				<li class="select" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_recent_4w')">최근 28일 결제금액별</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_accrue')">누적 결제금액별</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_daypurchase')">재결제일별</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('sub_group')">그룹별</li>
			</ul>
	    	
	    	<form name="search_form" id="search_form"  method="get" action="pay_stat_sub_recent_4w.php">
	    		<div class="detail_search_wrap">
	    			<input type="hidden" name="platform" id="platform" value="<?= $platform ?>" />
	    			
	    			<input type="button" class="<?= ($platform == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_all" onclick="change_platform('ALL')" value="통합" />
	    			<input type="button" class="<?= ($platform == "0") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_fb" onclick="change_platform('0')" value="Facebook" />
	    			<input type="button" class="<?= ($platform == "1") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ios" onclick="change_platform('1')" value="iOS" />
	    			<input type="button" class="<?= ($platform == "2") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_and" onclick="change_platform('2')" value="Android" />
	    			<input type="button" class="<?= ($platform == "3") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ama" onclick="change_platform('3')" value="Amazon" />
	    			
	    			<span class="search_lb2 ml10">요일</span>
					<select name="dayofweek" id="dayofweek">										
						<option value="0" <?= ($dayofweek=="0") ? "selected" : "" ?>>전체</option>
						<option value="1" <?= ($dayofweek=="1") ? "selected" : "" ?>>일요일</option>                       
						<option value="2" <?= ($dayofweek=="2") ? "selected" : "" ?>>월요일</option>					
						<option value="3" <?= ($dayofweek=="3") ? "selected" : "" ?>>화요일</option>					
						<option value="4" <?= ($dayofweek=="4") ? "selected" : "" ?>>수요일</option>
						<option value="5" <?= ($dayofweek=="5") ? "selected" : "" ?>>목요일</option>
						<option value="6" <?= ($dayofweek=="6") ? "selected" : "" ?>>금요일</option>
						<option value="7" <?= ($dayofweek=="7") ? "selected" : "" ?>>토요일</option>					
					</select>
	    			
	    			<span class="search_lb2 ml10">시즌세일여부</span>
	    			<select name="is_sale" id="is_sale">										
						<option value="ALL" <?= ($is_sale=="ALL") ? "selected" : "" ?>>전체</option>
						<option value="0" <?= ($is_sale=="0") ? "selected" : "" ?>>시즌세일기간아님</option>
						<option value="1" <?= ($is_sale=="1") ? "selected" : "" ?>>시즌세일기간</option>                       				
					</select>
					
					<span class="search_lb2 ml10">View</span>
	    			<select name="viewmode" id="viewmode">										
						<option value="0" <?= ($viewmode=="0") ? "selected" : "" ?>>그래프</option>
						<option value="1" <?= ($viewmode=="1") ? "selected" : "" ?>>리스트</option>                       				
					</select>
	    			
	    			<span class="search_lb2 ml10">날짜</span>
	    			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
		            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
					<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
				</div>
    		</form>
<?
	if($viewmode == 0)
	{
?>
	    	<div class="h2_title">[Killer Whale] 최근 28일 결제금액 $499 이상</div>
	    	<div id="chart_div0" name="div_normal" style="height:500px; min-width: 1000px"></div>
	    	
	    	<div class="h2_title mt30" name="div_normal">[Whale] 최근 28일 결제금액 $199 ~ $499</div>
	    	<div id="chart_div1" name="div_normal" style="height:500px; min-width: 1000px"></div>
	    	
	    	<div class="h2_title mt30" name="div_normal">[Sea Lion] 최근 28일 결제금액 $59 ~ $199</div>
	    	<div id="chart_div2" name="div_normal" style="height:500px; min-width: 1000px"></div>
	    	
	    	<div class="h2_title mt30" name="div_normal">[Dolphin] 최근 28일 결제금액 $9 ~ $59</div>
	    	<div id="chart_div3" name="div_normal" style="height:500px; min-width: 1000px"></div>
	    	
	    	<div class="h2_title mt30" name="div_normal">[Minnow] 최근 28일 결제금액 $0 ~ $9</div>
	    	<div id="chart_div4" name="div_normal" style="height:500px; min-width: 1000px"></div>
<?          
	}
	else if($viewmode == 1)
	{
?>
			<span>※ 최근 28일 결제금액 기준으로  <br> &nbsp;&nbsp;&nbsp;&nbsp;Killer Whale : $499 이상, Whale : $199 ~ $499, Sea Lion : $59 ~ $199, Dolphin : $9 ~ $59, Minnow : $0 ~ $9 </span><br>
			<div id="tab_content_1">
	    		<table class="tbl_list_basic1">
	    			<colgroup>
	    				<col width="">
	    				<col width="">
	    				<col width="">
	    				<col width="">
	    				<col width="">
	    			</colgroup>
	    			<thead>
	    				<tr>
	    					<th class="tdc" rowspan = 2>날짜</th>
	    					<th class="tdc" colspan = 2>Killer Whale</th>
	    					<th class="tdc" colspan = 2>Whale</th>
	    					<th class="tdc" colspan = 2>Sea Lion</th>
	    					<th class="tdc" colspan = 2>Dolphin</th>
	    					<th class="tdc" colspan = 2>Minnow</th>
	    				</tr>
	    				<tr>
	    					<th class="tdc">매출액</th>
	    					<th class="tdc">결제자수</th>
	    					<th class="tdc">매출액</th>
	    					<th class="tdc">결제자수</th>
	    					<th class="tdc">매출액</th>
	    					<th class="tdc">결제자수</th>
	    					<th class="tdc">매출액</th>
	    					<th class="tdc">결제자수</th>
	    					<th class="tdc">매출액</th>
	    					<th class="tdc">결제자수</th>    					
	    				</tr>
	    			</thead>
	    			<tbody>
<?
    			for($i=0; $i<sizeof($order_list); $i++)
    			{
    			    $today = $order_list[$i]["today"];
    			    
    			    $grade_5_money = $order_list[$i]["grade_5_money"];
		        	$grade_5_payer_cnt = $order_list[$i]["grade_5_payer_cnt"];
		        	$grade_4_money = $order_list[$i]["grade_4_money"];
		        	$grade_4_payer_cnt = $order_list[$i]["grade_4_payer_cnt"];
		        	$grade_3_money = $order_list[$i]["grade_3_money"];
		        	$grade_3_payer_cnt = $order_list[$i]["grade_3_payer_cnt"];
		        	$grade_2_money = $order_list[$i]["grade_2_money"];
		        	$grade_2_payer_cnt = $order_list[$i]["grade_2_payer_cnt"];
		        	$grade_1_money = $order_list[$i]["grade_1_money"];
		        	$grade_1_payer_cnt = $order_list[$i]["grade_1_payer_cnt"];
?>
					<tr>
    					<td class="tdc point_title"><?= $today?></td>    					
    					<td class="tdc">$<?= number_format($grade_5_money, 2) ?></td>
    					<td class="tdc"><?= number_format($grade_5_payer_cnt) ?></td>
    					<td class="tdc">$<?= number_format($grade_4_money, 2) ?></td>
    					<td class="tdc"><?= number_format($grade_4_payer_cnt) ?></td>
    					<td class="tdc">$<?= number_format($grade_3_money, 2) ?></td>
    					<td class="tdc"><?= number_format($grade_3_payer_cnt) ?></td>
    					<td class="tdc">$<?= number_format($grade_2_money, 2) ?></td>
    					<td class="tdc"><?= number_format($grade_2_payer_cnt) ?></td>
    					<td class="tdc">$<?= number_format($grade_1_money, 2) ?></td>
    					<td class="tdc"><?= number_format($grade_1_payer_cnt) ?></td>    				
    				</tr>
<?
    			}
?>
    			</tbody>
    		</table>
    	</div>	
<?
	}
?>
    	</div>
    	<!--  //CONTENTS WRAP -->
    	
    	<div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>