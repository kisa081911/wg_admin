<?
	$top_menu = "pay_static";
	$sub_menu = "pay_static";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$term = ($_GET["term"] == "") ? "day" : $_GET["term"];
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
	
	if ($term != "day" && $term != "week" && $term != "month")
		error_back("잘못된 접근입니다.");
	
	check_xss($startdate);
	check_xss($enddate);
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today,15,"d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db_main = new CDatabase_Main();
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$std_useridx = 10000;
	}
    
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
		$tail = "WHERE status IN (1, 3) AND useridx > 10000 ";
	else
		$tail = "WHERE status IN (1, 3) AND useridx > 20000 ";
	
	if ($term == "day")
	{
		$tail .= "AND writedate >= '$startdate 00:00:00' AND writedate <= '$enddate 23:59:59'";
		$group_by = "GROUP BY LEFT(writedate,10)";
				
		$sql = "SELECT writedate, SUM(web_count) AS web_count, SUM(earn_count) AS earn_count, SUM(total_credit) AS total_credit ".
				"FROM ( ".
				"	SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, COUNT(orderidx) AS web_count, 0 AS earn_count, SUM(facebookcredit) AS total_credit ".
				"	FROM tbl_product_order ".
				"	$tail ".
				"	$group_by ".
				"	UNION ALL ".
				"	SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 0 AS web_count, COUNT(orderidx) AS earn_count, ROUND(SUM(money)/10) AS total_credit ".
				"	FROM tbl_product_order_earn ".
				"	$tail ".
				"	$group_by ".
				") t1 ".
				"GROUP BY writedate ".
				"ORDER BY writedate ASC";
		
		$staticlist = $db_main->gettotallist($sql);
		
		$sql = "SELECT DATE_FORMAT(t1.writedate, '%Y-%m-%d') AS writedate, ".
				"	   SUM(t1.facebookcredit) AS sum_total_credit, ".
				"	   SUM(IF(t2.special_discount = 0 AND t2.special_more = 0 AND t1.couponidx = 0, t1.facebookcredit, 0)) AS sum_basic_credit, ".
				"      SUM(IF(t1.couponidx != 0, t1.facebookcredit, 0)) AS sum_coupon_credit, ".
				"	   SUM(IF(t1.special_discount != 0 OR t1.special_more != 0 AND t1.couponidx = 0, t1.facebookcredit, 0)) AS sum_special_credit ".
				"FROM tbl_product_order t1 LEFT JOIN tbl_product t2 ON t1.productidx = t2.productidx ".
				"WHERE t1.useridx > $std_useridx AND t1.status IN (1, 3) AND t1.writedate >= '$startdate 00:00:00' AND t1.writedate <= '$enddate 23:59:59' ".
				"GROUP BY LEFT(t1.writedate,10) ORDER BY writedate ASC";
		$ratelist = $db_main->gettotallist($sql);
	}
	else if ($term == "week")
	{
		$tail .= " AND writedate >= '$startdate 00:00:00' AND writedate <= '$enddate 23:59:59'";
		$group_by = "GROUP BY CONCAT(LEFT(writedate,4),WEEK(writedate)) ";
				
		$sql = "SELECT WEEK(writedate) AS week,LEFT(writedate,4) AS year, SUM(web_count) AS web_count, SUM(earn_count) AS earn_count, SUM(total_credit) AS total_credit ".
				"FROM ( ".
				"	SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, COUNT(orderidx) AS web_count, 0 AS earn_count, SUM(facebookcredit) AS total_credit ".
				"	FROM tbl_product_order ".
				"	$tail ".
				"	GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
				"	UNION ALL ".
				"	SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 0 AS web_count, COUNT(orderidx) AS earn_count, ROUND(SUM(money)/10) AS total_credit ".
				"	FROM tbl_product_order_earn ".
				"	$tail ".
				"	GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
				") t1 ".
				"GROUP BY CONCAT(LEFT(writedate,4),WEEK(writedate)) ".
				"ORDER BY writedate ASC";

		$staticlist = $db_main->gettotallist($sql);
		
		$sql = "SELECT WEEK(t1.writedate) AS week,LEFT(t1.writedate,4) AS year, ".
				"	   SUM(t1.facebookcredit) AS sum_total_credit, ".
				"	   SUM(IF(t2.special_discount = 0 AND t2.special_more = 0 AND t1.couponidx = 0, t1.facebookcredit, 0)) AS sum_basic_credit, ".
				"      SUM(IF(t1.couponidx != 0, t1.facebookcredit, 0)) AS sum_coupon_credit, ".
				"	   SUM(IF(t1.special_discount != 0 OR t1.special_more != 0 AND t1.couponidx = 0, t1.facebookcredit, 0)) AS sum_special_credit ".
				"FROM tbl_product_order t1 LEFT JOIN tbl_product t2 ON t1.productidx = t2.productidx ".
				"WHERE t1.useridx > $std_useridx AND t1.status IN (1, 3) AND t1.writedate >= '$startdate 00:00:00' AND t1.writedate <= '$enddate 23:59:59' ".
				"GROUP BY CONCAT(LEFT(t1.writedate,4),WEEK(t1.writedate)) ORDER BY t1.writedate ASC";
		$ratelist = $db_main->gettotallist($sql);
	}
	else if ($term == "month")
	{
		$tail .= " AND writedate >= '$startdate 00:00:00' AND writedate <= '$enddate 23:59:59'";
		$group_by = "GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ";
		
		$sql = "SELECT LEFT(writedate,7) AS writedate, SUM(web_count) AS web_count, SUM(earn_count) AS earn_count, SUM(total_credit) AS total_credit ".
				"FROM ( ".
				"	SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, COUNT(orderidx) AS web_count, 0 AS earn_count, SUM(facebookcredit) AS total_credit ".
				"	FROM tbl_product_order ".
				"	$tail ".
				"	$group_by ".
				"	UNION ALL ".
				"	SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, 0 AS web_count, COUNT(orderidx) AS earn_count, ROUND(SUM(money)/10) AS total_credit ".
				"	FROM tbl_product_order_earn ".
				"	$tail ".
				"	$group_by ".
				") t1 ".
				"GROUP BY LEFT(writedate,7) ".
				"ORDER BY writedate ASC";

		$staticlist = $db_main->gettotallist($sql);
		
		$sql = "SELECT LEFT(t1.writedate,7) AS writedate, ".
				"	   SUM(t1.facebookcredit) AS sum_total_credit, ".
				"	   SUM(IF(t2.special_discount = 0 AND t2.special_more = 0 AND t1.couponidx = 0, t1.facebookcredit, 0)) AS sum_basic_credit, ".
				"      SUM(IF(t1.couponidx != 0, t1.facebookcredit, 0)) AS sum_coupon_credit, ".
				"	   SUM(IF(t1.special_discount != 0 OR t1.special_more != 0 AND t1.couponidx = 0, t1.facebookcredit, 0)) AS sum_special_credit ".
				"FROM tbl_product_order t1 LEFT JOIN tbl_product t2 ON t1.productidx = t2.productidx ".
				"WHERE t1.useridx > $std_useridx AND t1.status IN (1, 3) AND t1.writedate >= '$startdate 00:00:00' AND t1.writedate <= '$enddate 23:59:59' ".
				"GROUP BY LEFT(t1.writedate,7) ORDER BY t1.writedate ASC";
		$ratelist = $db_main->gettotallist($sql);
	}
	
	$start_week = get_week($startdate);
	$end_week = get_week($enddate);
	$start_month = substr($startdate,0,7);
	$end_month = substr($enddate,0,7);
	$end_year = substr($enddate,0,4);
		
	//결제금액, 결제 건수, 결제비율 array로 구성
	$facebookcredit_list= array();
	$status1_list= array();
	$totalcount_list = array();
	$total_facebookcredit_list = array();
	$basic_facebookcredit_list = array();
	$coupon_facebookcredit_list = array();
	$special_facebookcredit_list = array();
	$date_list = array();
	
	$list_pointer = sizeof($staticlist);
		
	if ($term == "day")
	{
		$date_pointer = $enddate;
		
		for ($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
		{
			$writedate = $staticlist[$list_pointer-1]["writedate"];
			
			if (get_diff_date($date_pointer, $writedate, "d") == 0)
			{
				$static = $staticlist[$list_pointer-1];
				$rate = $ratelist[$list_pointer-1];
				
				$facebookcredit_list[$i] = $static["total_credit"] * 0.1;
				$status1_list[$i] = $static["status1"];
				$totalcount_list[$i] = $static["web_count"] + $static["earn_count"];
				
				$total_facebookcredit_list[$i]  = $rate["sum_total_credit"];
				$basic_facebookcredit_list[$i]  = $rate["sum_basic_credit"];
				$coupon_facebookcredit_list[$i]  = $rate["sum_coupon_credit"];
				$special_facebookcredit_list[$i] = $rate["sum_special_credit"];
				
				$date_list[$i] = $date_pointer;
				
				$list_pointer--;
			}
			else 
			{
				$facebookcredit_list[$i] = 0;
				$status1_list[$i] = 0;
				$totalcount_list[$i] = 0;
				$total_facebookcredit_list[$i] = 0;
				$basic_facebookcredit_list[$i] = 0;
				$coupon_facebookcredit_list[$i] = 0;
				$special_facebookcredit_list[$i] = 0;
				$date_list[$i] = $date_pointer;				
			}
			
			$date_pointer = get_past_date($date_pointer, 1, "d");
		}
	}
	else if ($term == "week")
	{
		$date_pointer = get_week_date($end_year, $end_week, 0);
		
		for ($i=0; $i<=get_diff_date($enddate, $startdate, "w"); $i++)
		{
			$week = $staticlist[$list_pointer-1]["week"];
			$year = $staticlist[$list_pointer-1]["year"];
			
			$write_week = get_week_date($year,$week,0);
			
			if ($date_pointer == $write_week)
			{
				$static = $staticlist[$list_pointer-1];
				$rate = $ratelist[$list_pointer-1];
				
				$facebookcredit_list[$i] = $static["total_credit"] * 0.1;
				$status1_list[$i] = $static["status1"];
				$totalcount_list[$i] = $static["web_count"] + $static["earn_count"];
				
				$total_facebookcredit_list[$i]  = $rate["sum_total_credit"];
				$basic_facebookcredit_list[$i]  = $rate["sum_basic_credit"];
				$coupon_facebookcredit_list[$i]  = $rate["sum_coupon_credit"];
				$special_facebookcredit_list[$i] = $rate["sum_special_credit"];
				
				$list_pointer--;
			}
			else
			{
				$facebookcredit_list[$i] = 0;
				$status1_list[$i] = 0;
				$totalcount_list[$i] = 0;
				$total_facebookcredit_list[$i] = 0;
				$basic_facebookcredit_list[$i] = 0;
				$coupon_facebookcredit_list[$i] = 0;
				$special_facebookcredit_list[$i] = 0;				
			}
			
			if (get_diff_date($startdate, $date_pointer, "d") > 0)
				$_date = $startdate;
			else 
				$_date = $date_pointer;
			
			$date_list[$i] = $_date." ~";
			
			$date_pointer = get_past_date($date_pointer, 1, "w");
		}
	}
	else if ($term == "month")
	{
		$date_pointer = $end_month;

		for($i=0; $i<=get_diff_date($end_month, $start_month, "m"); $i++)
		{
			$write_month = $staticlist[$list_pointer-1]["writedate"];
			
			if (get_diff_date($date_pointer, $write_month, "m") == 0)
			{
				$static = $staticlist[$list_pointer-1];
				$rate = $ratelist[$list_pointer-1];
				
				$facebookcredit_list[$i] = $static["total_credit"] * 0.1;
				$status1_list[$i] = $static["status1"];
				$totalcount_list[$i] = $static["web_count"] + $static["earn_count"];
				
				$total_facebookcredit_list[$i]  = $rate["sum_total_credit"];
				$basic_facebookcredit_list[$i]  = $rate["sum_basic_credit"];
				$coupon_facebookcredit_list[$i]  = $rate["sum_coupon_credit"];
				$special_facebookcredit_list[$i] = $rate["sum_special_credit"];
				
				$date_list[$i] = $date_pointer;
				
				$list_pointer--;
			}
			else
			{
				$facebookcredit_list[$i] = 0;
				$status1_list[$i] = 0;
				$totalcount_list[$i] = 0;
				$total_facebookcredit_list[$i] = 0;
				$basic_facebookcredit_list[$i] = 0;
				$coupon_facebookcredit_list[$i] = 0;
				$date_list[$i] = $date_pointer;
			}
			
			$date_pointer = get_past_date($date_pointer, 1, "m");
		}
	}

	$db_main->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

	google.load("visualization", "1", {packages:["corechart"]});

    function drawChart() 
    {
        var data1 = new google.visualization.DataTable();
        data1.addColumn('string', '날짜');
        data1.addColumn('number', '총 결제완료금액($)');
        data1.addRows([
<?
	for ($i=sizeof($date_list); $i>0; $i--)
	{
		$_facebookcredit = $facebookcredit_list[$i-1];
        $_date = $date_list[$i-1];
		
		echo("['".$_date."',".$_facebookcredit."]");
		
		if ($i > 1)
			echo(",");
	}
?>
        ]);
        
        var data2 = new google.visualization.DataTable();
        data2.addColumn('string', '날짜');
        data2.addColumn('number', '결제완료건');
        data2.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_status1 = $totalcount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."',".$_status1."]");
        
        if ($i > 1)
            echo(",");
    }
?>
        ]);

        var data3 = new google.visualization.DataTable();
        data3.addColumn('string', '날짜');
        data3.addColumn('number', '정상구매(%)');
        data3.addColumn('number', '세일구매(%)');
        data3.addColumn('number', '쿠폰구매(%)');
        data3.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_total_facebookcredit = $total_facebookcredit_list[$i-1];
        $_basic_facebookcredit = $basic_facebookcredit_list[$i-1];
        $_coupon_facebookcredit = $coupon_facebookcredit_list[$i-1];
        $_special_facebookcredit = $special_facebookcredit_list[$i-1];
        
        $_basic_rate = $_total_facebookcredit == 0 ? 0:round($_basic_facebookcredit/$_total_facebookcredit * 100, 1);
        $_coupon_rate = $_total_facebookcredit == 0 ? 0:round($_coupon_facebookcredit/$_total_facebookcredit * 100, 1);
        $_special_rate = $_total_facebookcredit == 0 ? 0:round($_special_facebookcredit/$_total_facebookcredit * 100, 1);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."',".$_basic_rate.",".$_special_rate.",".$_coupon_rate."]");
        
        if ($i > 1)
            echo(",");
    }
?>
        ]);
    
        var options1 = {
            width:1050,                         
            height:470,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:300}
        };
        
        var options2 = {
            width:1050,                         
            height:470,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:300}
        };
                
        var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
        chart.draw(data1, options1);        
        
        chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
        chart.draw(data2, options2);   

        chart = new google.visualization.LineChart(document.getElementById('chart_div3'));
        chart.draw(data3, options2);      
    }
        
	google.setOnLoadCallback(drawChart);	

	function change_term(term)
	{
		var search_form = document.search_form;
		
		var day = document.getElementById("term_day");
		var week = document.getElementById("term_week");
		var month = document.getElementById("term_month");
		
		document.search_form.term.value = term;

		if (term == "day")
		{
			day.className="btn_schedule_select";
			week.className="btn_schedule";
			month.className="btn_schedule";
		}
		else if (term == "week")
		{
			day.className="btn_schedule";
			week.className="btn_schedule_select";
			month.className="btn_schedule";			
		}
		else if (term == "month")
		{
			day.className="btn_schedule";
			week.className="btn_schedule";
			month.className="btn_schedule_select";	
		}

		search_form.submit();
	}

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}
    
    function check_sleeptime()
    {
    }
</script>
	<!-- CONTENTS WRAP -->
	<div class="contents_wrap">
    	
		<!-- title_warp -->
		<div class="title_wrap">
    		<div class="title"><?= $top_menu_txt ?> &gt; 기간별 통계</div>
    		<form name="search_form" id="search_form"  method="get" action="pay_static.php">
    		<input type="hidden" name="term" id="term" value="<?= $term ?>" />
    		<div class="search_box">
    			<input type="button" class="<?= ($term == "day") ? "btn_schedule_select" : "btn_schedule" ?>" value="일" id="term_day" onclick="change_term('day')" /><input type="button" class="<?= ($term == "week") ? "btn_schedule_select" : "btn_schedule" ?>" value="주"  id="term_week"  onclick="change_term('week')" /><input type="button" class="<?= ($term == "month") ? "btn_schedule_select" : "btn_schedule" ?>" value="월"  onclick="change_term('month')"   id="term_month" /> 
    			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
	            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
	             <input type="button" class="btn_search" value="검색" onclick="search()" />
    		</div>
    		</form>
        </div>
    	<!-- //title_warp -->
	    	
		<div class="search_result">
			<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span>
<?
	if ($term == "day")
		echo("일별");
	else if ($term == "week")
		echo("주별");
	else if ($term == "month")
		echo("월별");
?>
				통계입니다
		</div>
	    	
		<div id="chart_div1" style="height:480px; min-width: 500px"></div>
		<div id="chart_div2" style="height:480px; min-width: 500px"></div>
		<div id="chart_div3" style="height:480px; min-width: 500px"></div>
	</div>
	<!--  //CONTENTS WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>