<?
    $top_menu = "pay_static";
    $sub_menu = "payer_stats";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $pagename = "payer_stats.php";
    
    $startdate = ($_GET["startdate"] == "") ? get_past_date(date("Y-m-d"),7,"d") : $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
    
    $db_other = new CDatabase_Other();
    
   	$sql = "SELECT * FROM tbl_user_payer_stat_daily WHERE today BETWEEN '$startdate' AND '$enddate'";
   	$user_payer_stat = $db_other->gettotallist($sql);

    $db_other->end();
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    google.charts.load('44', {
        	'packages':['line', 'corechart'],
			'callback' : drawChart
			}
	);
    
    //google.charts.setOnLoadCallback(drawChart);

    function drawChart() 
    {
    	var chartDiv1 = document.getElementById('chart_div1');
    	var chartDiv2 = document.getElementById('chart_div2');
    	var chartDiv3 = document.getElementById('chart_div3');
    	var chartDiv4 = document.getElementById('chart_div4');
    	var chartDiv5 = document.getElementById('chart_div5');
        
        var data1 = new google.visualization.DataTable();
        
        data1.addColumn('string', '날짜');
        data1.addColumn('number', '결제 경험 회원수');
        data1.addColumn('number', '결제 회원수');
        data1.addColumn('number', '비율(%)');
        data1.addRows([
<?
    for ($i=0; $i<sizeof($user_payer_stat); $i++)
    {
    	$date = $user_payer_stat[$i]["today"];
    	
    	$all_payer_logincnt = $user_payer_stat[$i]["payer_logincount"] + $user_payer_stat[$i]["ios_payer_logincount"] + $user_payer_stat[$i]["and_payer_logincount"] + $user_payer_stat[$i]["ama_payer_logincount"];
    	$all_payer_cnt = $user_payer_stat[$i]["payer_count"] + $user_payer_stat[$i]["ios_payer_count"] + $user_payer_stat[$i]["and_payer_count"] + $user_payer_stat[$i]["ama_payer_count"];
    	$all_payer_percent = ($all_payer_cnt/$all_payer_logincnt)*100;
    	
        echo("['".$date."'");   
        
        if ($all_payer_logincnt != "")
        	echo(",{v:".$all_payer_logincnt."}");
        else
        	echo(",0");
        
        if ($all_payer_cnt != "")
        	echo(",{v:".$all_payer_cnt."}");
        else
        	echo(",0");
        
        if ($all_payer_percent != "")
        	echo(",{v:".$all_payer_percent."}]");
        else
        	echo(",0]");
        
        if ($i < sizeof($user_payer_stat))
			echo(",");
    }
?>     
        ]);

        var data2 = new google.visualization.DataTable();
        
        data2.addColumn('string', '날짜');
        data2.addColumn('number', '결제 경험 회원수');
        data2.addColumn('number', '결제 회원수');
        data2.addColumn('number', '비율(%)');
        data2.addRows([
<?
    for ($i=0; $i<sizeof($user_payer_stat); $i++)
    {
    	$date = $user_payer_stat[$i]["today"];
    	
    	$web_payer_logincnt = $user_payer_stat[$i]["payer_logincount"];
    	$web_payer_cnt = $user_payer_stat[$i]["payer_count"];
    	$web_payer_percent = ($web_payer_cnt/$web_payer_logincnt)*100;
    	
        echo("['".$date."'");   
        
        if ($web_payer_logincnt != "")
        	echo(",{v:".$web_payer_logincnt."}");
        else
        	echo(",0");
        
        if ($web_payer_cnt != "")
        	echo(",{v:".$web_payer_cnt."}");
        else
        	echo(",0");
        
        if ($web_payer_percent != "")
        	echo(",{v:".$web_payer_percent."}]");
        else
        	echo(",0]");
        
        if ($i < sizeof($user_payer_stat))
			echo(",");
    }
?>     
        ]);

        var data3 = new google.visualization.DataTable();
        
        data3.addColumn('string', '날짜');
        data3.addColumn('number', '결제 경험 회원수');
        data3.addColumn('number', '결제 회원수');
        data3.addColumn('number', '비율(%)');
        data3.addRows([
<?
    for ($i=0; $i<sizeof($user_payer_stat); $i++)
    {
    	$date = $user_payer_stat[$i]["today"];
    	
    	$ios_payer_logincnt = $user_payer_stat[$i]["ios_payer_logincount"];
    	$ios_payer_cnt = $user_payer_stat[$i]["ios_payer_count"];
    	$ios_payer_percent = ($ios_payer_logincnt == 0)?0:($ios_payer_cnt/$ios_payer_logincnt)*100;
    	
        echo("['".$date."'");   
        
        if ($ios_payer_logincnt != "")
        	echo(",{v:".$ios_payer_logincnt."}");
        else
        	echo(",0");
        
        if ($ios_payer_cnt != "")
        	echo(",{v:".$ios_payer_cnt."}");
        else
        	echo(",0");
        
        if ($ios_payer_percent != "")
        	echo(",{v:".$ios_payer_percent."}]");
        else
        	echo(",0]");
        
        if ($i < sizeof($user_payer_stat))
			echo(",");
    }
?>     
        ]);

        var data4 = new google.visualization.DataTable();
        
        data4.addColumn('string', '날짜');
        data4.addColumn('number', '결제 경험 회원수');
        data4.addColumn('number', '결제 회원수');
        data4.addColumn('number', '비율(%)');
        data4.addRows([
<?
    for ($i=0; $i<sizeof($user_payer_stat); $i++)
    {
    	$date = $user_payer_stat[$i]["today"];
    	
    	$and_payer_logincnt = $user_payer_stat[$i]["and_payer_logincount"];
    	$and_payer_cnt = $user_payer_stat[$i]["and_payer_count"];
    	$and_payer_percent = ($and_payer_logincnt == 0)?0:($and_payer_cnt/$and_payer_logincnt)*100;
    	
        echo("['".$date."'");   
        
        if ($and_payer_logincnt != "")
        	echo(",{v:".$and_payer_logincnt."}");
        else
        	echo(",0");
        
        if ($and_payer_cnt != "")
        	echo(",{v:".$and_payer_cnt."}");
        else
        	echo(",0");
        
        if ($and_payer_percent != "")
        	echo(",{v:".$and_payer_percent."}]");
        else
        	echo(",0]");
        
        if ($i < sizeof($user_payer_stat))
			echo(",");
    }
?>     
        ]);

        var data5 = new google.visualization.DataTable();
        
        data5.addColumn('string', '날짜');
        data5.addColumn('number', '결제 경험 회원수');
        data5.addColumn('number', '결제 회원수');
        data5.addColumn('number', '비율(%)');
        data5.addRows([
<?
    for ($i=0; $i<sizeof($user_payer_stat); $i++)
    {
    	$date = $user_payer_stat[$i]["today"];
    	
    	$ama_payer_logincnt = $user_payer_stat[$i]["ama_payer_logincount"];
    	$ama_payer_cnt = $user_payer_stat[$i]["ama_payer_count"];
    	$ama_payer_percent = ($ama_payer_logincnt == 0)?0:($ama_payer_cnt/$ama_payer_logincnt)*100;
    	
        echo("['".$date."'");   
        
        if ($ama_payer_logincnt != "")
        	echo(",{v:".$ama_payer_logincnt."}");
        else
        	echo(",0");
        
        if ($ama_payer_cnt != "")
        	echo(",{v:".$ama_payer_cnt."}");
        else
        	echo(",0"); 
        
        if ($ama_payer_percent != "")
        	echo(",{v:".$ama_payer_percent."}]");
        else
        	echo(",0]");
        
        if ($i < sizeof($user_payer_stat))
			echo(",");
    }
?>     
        ]);

        var materialOptions = {
			chart: {},
			width: 1050,
			height: 200,
			series: {
				// Gives each series an axis name that matches the Y-axis below.
				0: {axis: '사용자수'},
				1: {axis: '비율(%)'}
			},
			axes: {
				// Adds labels to each axis; they don't have to match the axis names.
				y: {
					'사용자수': {label: '사용자수'},
					'비율(%)': {label: '비율(%)'}
                  }
                }
		};

        var materialChart = new google.charts.Line(chartDiv1);
        materialChart.draw(data1, materialOptions);

        var materialChart = new google.charts.Line(chartDiv2);
        materialChart.draw(data2, materialOptions);

        var materialChart = new google.charts.Line(chartDiv3);
        materialChart.draw(data3, materialOptions);

        var materialChart = new google.charts.Line(chartDiv4);
        materialChart.draw(data4, materialOptions);

        var materialChart = new google.charts.Line(chartDiv5);
        materialChart.draw(data5, materialOptions);

    }
    
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }

    function search()
    {
        var search_form = document.search_form;

        search_form.submit();
    }
    
    $(function() {
        $("#startdate").datepicker({ });
    });
    
    $(function() {
        $("#enddate").datepicker({ });
    });
</script>
        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 결제 경험자 결제 현황</div>
                
                <form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
	                <div class="search_box">
	                    <input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
	                    <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
	                     <input type="button" class="btn_search" value="검색" onclick="search()" />
	                </div>
                </form>
            </div>
            <!-- //title_warp -->
            
            <div class="search_result">
            	<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다.
            </div>
        
            <div class="h2_title">[전체]</div>
	    	<div id="chart_div1" style="height:230px; min-width: 500px"></div>
            
            <div class="h2_title">[Web]</div>
	    	<div id="chart_div2" style="height:230px; min-width: 500px"></div>
	    	
	    	<div class="h2_title">[iOS]</div>
	    	<div id="chart_div3" style="height:230px; min-width: 500px"></div>
	    	
	    	<div class="h2_title">[Android]</div>
	    	<div id="chart_div4" style="height:230px; min-width: 500px"></div>
	    	
	    	<div class="h2_title">[Amazon]</div>
	    	<div id="chart_div5" style="height:230px; min-width: 500px"></div>
        </div>
        
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>

<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>