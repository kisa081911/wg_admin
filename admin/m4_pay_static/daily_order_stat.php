<?
	$top_menu = "pay_static";
	$sub_menu = "pay_static";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];

	check_xss($startdate);
	check_xss($enddate);
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today,15,"d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db_main = new CDatabase_Main();
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$std_useridx = 10000;
	}
    
	$sql = "SELECT today, ". 
            "   SUM(IF(ptype = 'ons', total_money, 0)) AS ons_money, ".
            "   SUM(IF(ptype = 'ocp', total_money, 0)) AS ocp_money, ".
            "   SUM(IF(ptype = 'ocv', total_money, 0)) AS ocv_money, ".
            "   SUM(IF(ptype = 'ocs', total_money, 0)) AS ocs_money, ".
            "   SUM(IF(ptype = 'ocm', total_money, 0)) AS ocm_money, ".
            "   SUM(IF(ptype = 'oci', total_money, 0)) AS oci_money, ".
            "   SUM(IF(ptype = 'oth', total_money, 0)) AS oth_money, ".
            "   SUM(IF(ptype = 'ofo', total_money, 0)) AS ofo_money, ".
            "   SUM(IF(ptype = 'olk', total_money, 0)) AS olk_money, ".
            "   SUM(IF(ptype = 'opg', total_money, 0)) AS opg_money, ".
            "   SUM(IF(ptype = 'olo', total_money, 0)) AS olo_money, ".
            "   SUM(IF(ptype = 'opd', total_money, 0)) AS opd_money, ".
            "   SUM(IF(ptype = 'oto', total_money, 0)) AS oto_money, ".
            "   SUM(IF(ptype = 'ohu', total_money, 0)) AS ohu_money, ".
            "   SUM(IF(ptype = 'oad', total_money, 0)) AS oad_money, ".
            "   SUM(IF(ptype = 'unknown', total_money, 0)) AS unknown_money ".
            "FROM ( ".
            "   SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, ". 
            "       (CASE WHEN product_type = 1 AND coupon_type = -1 THEN 'ons' ". // 기본 상품, 쿠폰 X
            "       WHEN product_type = 1 AND coupon_type = 0 THEN 'ocp' ". // 기본 상품, 정기 쿠폰
            "       WHEN product_type = 1 AND coupon_type = 1 THEN 'ocv' ". // 기본 상품, 100% 쿠폰
            "       WHEN product_type = 1 AND coupon_type IN (2,3,4) THEN 'ocs' ". // 기본 상품, 시즌 쿠폰 
            "       WHEN product_type = 1 AND coupon_type = 5 THEN 'ocm' ". // 기본 상품, 모바일 유도 쿠폰
            "       WHEN product_type = 1 AND coupon_type IN (10, 11, 12) THEN 'oci' ". // 기본 상품, 인센티브 쿠폰
            "       WHEN product_type = 3 AND coupon_type = -1 THEN 'oth' ". // TH 오퍼 
            "       WHEN product_type = 6 AND coupon_type = -1 THEN 'ofo' ". // 첫 결제 오퍼 
            "       WHEN product_type = 7 AND coupon_type = -1 THEN 'olk' ". // 럭키 오퍼 
            "       WHEN product_type = 9 THEN 'opg' ". // 피기 팟
            "       WHEN product_type = 10 AND coupon_type = -1 THEN 'olo' ". // 결제 이탈자 오퍼 
            "       WHEN product_type = 12 AND coupon_type = -1 THEN 'opd' ". // 프라임 딜 
            "       WHEN product_type = 13 AND coupon_type = -1 THEN 'opd' ". // 프라임 딜 v2 
            "       WHEN product_type = 14 AND coupon_type = -1 THEN 'oto' ". // 기간제 오퍼 
            "       WHEN product_type = 15 AND coupon_type = -1 THEN 'ohu' ". // 하이롤러 언락오퍼 
            "       WHEN product_type = 16 AND coupon_type = -1 THEN 'oad' ". // 어메이징 딜 
            "       WHEN product_type = 17 AND coupon_type = -1 THEN 'oto' ". // 기간제 오퍼(비결제자)) 
            "       ELSE 'unknown' END) AS ptype, ROUND(SUM(money), 2) AS total_money, COUNT(DISTINCT useridx), ".
            "       COUNT(*) AS pay_cnt, ". 
            "       ROUND(SUM(coin)/SUM(basecoin)*100, 2) AS morerate ".
            "   FROM ( ".
            "       SELECT 0 AS platform, useridx, (facebookcredit/10) AS money, couponidx, special_more, special_discount, basecoin, coin, product_type, coupon_type, writedate ".
            "       FROM tbl_product_order ".
            "       WHERE useridx > $std_useridx AND status IN (1, 3) AND writedate >= '$startdate 00:00:00' AND writedate < '$enddate 23:59:59' ".
            "       UNION ALL ".
            "       SELECT os_type AS platform, useridx, money, couponidx, special_more, special_discount, basecoin, coin, product_type, coupon_type, writedate ".
            "       FROM tbl_product_order_mobile ".
            "       WHERE useridx > $std_useridx AND status IN (1, 3) AND writedate >= '$startdate 00:00:00' AND writedate < '$enddate 23:59:59' ".
            "   ) t1 ".
            "   GROUP BY today, ptype ".
            ") total ".
            "GROUP BY today ".
            "ORDER BY today ASC";
	$data_list = $db_main->gettotallist($sql);

	$db_main->end();
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	    $("#enddate").datepicker({ });
	});
	
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart);

    function drawChart() 
    {
        var data1 = google.visualization.arrayToDataTable([
            ['날짜', '일반(쿠폰X)', '일반(정기C)', '일반(100%C)', '일반(시즌C)', '일반(모바일유도C)', '일반(인센티브C)', 'TH 오퍼', '첫 결제 오퍼', '럭키 오퍼', '피기팟', '결제이탈자 오퍼',
                '프라임딜', '기간제 오퍼', '하이롤러 언락 오퍼', '어메이징딜', 'unknown'],
<?
        for($i=0;$i<sizeof($data_list); $i++)
        {
            $today = $data_list[$i]["today"];
            $ons_money = $data_list[$i]["ons_money"];
            $ocp_money = $data_list[$i]["ocp_money"];
            $ocv_money = $data_list[$i]["ocv_money"];
            $ocs_money = $data_list[$i]["ocs_money"];
            $ocm_money = $data_list[$i]["ocm_money"];
            $oci_money = $data_list[$i]["oci_money"];
            $oth_money = $data_list[$i]["oth_money"];
            $ofo_money = $data_list[$i]["ofo_money"];
            $olk_money = $data_list[$i]["olk_money"];
            $opg_money = $data_list[$i]["opg_money"];
            $olo_money = $data_list[$i]["olo_money"];
            $opd_money = $data_list[$i]["opd_money"];
            $oto_money = $data_list[$i]["oto_money"];
            $ohu_money = $data_list[$i]["ohu_money"];
            $oad_money = $data_list[$i]["oad_money"];
            $unknown_money = $data_list[$i]["unknown_money"];
            
            echo "['$today', $ons_money, $ocp_money, $ocv_money, $ocs_money, $ocm_money, $oci_money, $oth_money, $ofo_money, $olk_money, $opg_money, $olo_money, 
                    $opd_money, $oto_money, $ohu_money, $oad_money, $unknown_money]";
            
            if($i != sizeof($data_list) - 1)
            {
                echo(",");
            }
        }
?>
          ]);
        
        var options = {
                hAxis: {titleTextStyle: {color: '#333'}},
                vAxis: {minValue: 0},
                fontSize : 12,
                isStacked: true
              };

      	var chart = new google.visualization.AreaChart(document.getElementById('chart_div1'));
      	chart.draw(data1, options);
    }
        
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}
    
    function check_sleeptime()
    {
    }
</script>
	<!-- CONTENTS WRAP -->
	<div class="contents_wrap">
    	
		<!-- title_warp -->
		<div class="title_wrap">
    		<div class="title"><?= $top_menu_txt ?> &gt; Test</div>
    		<form name="search_form" id="search_form"  method="get" action="daily_order_stat.php">
    		<div class="search_box"> 
    			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
	            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
	             <input type="button" class="btn_search" value="검색" onclick="search()" />
    		</div>
    		</form>
        </div>
    	<!-- //title_warp -->
	    	
		<div id="chart_div1" style="height:480px; min-width: 500px"></div>
	</div>
	<!--  //CONTENTS WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>