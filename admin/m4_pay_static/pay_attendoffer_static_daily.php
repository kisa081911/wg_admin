<?
	$top_menu = "pay_static";
	$sub_menu = "pay_attendoffer_static_daily";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_type = ($_GET["search_type"] == "") ? "" : $_GET["search_type"];
	
	if($_GET["start_date"] == "")
		$search_sdate = date("Y-m-d",strtotime("-13 days"));
	else
		$search_sdate = $_GET["start_date"];
	
	if($_GET["end_date"] == "")
		$search_edate = date("Y-m-d");
	else
		$search_edate = $_GET["end_date"];
	
	$start_date = $search_date;
	
	$db_main2 = new CDatabase_Main2();
	
	$sql = "SELECT * FROM `tbl_subscribe_stat_daily` WHERE TYPE != 0 and today BETWEEN '$search_sdate' AND '$search_edate'  ORDER BY today DESC, TYPE ASC";
	$subscribe_stat_list = $db_main2->gettotallist($sql);
	
	$today_arr = $db_main2->gettotallist("SELECT DISTINCT today FROM `tbl_subscribe_stat_daily` WHERE TYPE != 0 and today BETWEEN '$search_sdate' AND '$search_edate' ORDER BY today DESC");
	
	$db_main2->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#start_date").datepicker({ });
		$("#end_date").datepicker({ });
	});
	
	function search()
	{
		var search_form = document.search_form;
	    
		if (search_form.start_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.start_date.focus();
	    	return;
		} 
	
		if (search_form.end_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.end_date.focus();
	    	return;
		} 
	
		search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 기간제 오퍼 통계</div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<span class="search_lbl">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="start_date" name="start_date" style="width:65px" readonly="readonly" value="<?= $search_sdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/> ~
				<input type="text" class="search_text" id="end_date" name="end_date" style="width:65px" readonly="readonly" value="<?= $search_edate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/>
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $search_sdate ?></span> ~ <span><?= $search_edate ?></span> 현황입니다.(D+0 07:00 - D+1 07:00 기준이 D+0 통계 입니다. (갱신 시간은 서버시간 기준 07:00 입니다.))
	</div>
	<table class="tbl_list_basic1" style="font: 11px dotum;width: 74%;margin-left: 12%;">
		<colgroup>
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">    
            <col width="">
            <col width="">
            <col width="">
            <col width="">
		</colgroup>
        <thead>
        	<tr>
        		<th rowspan=2>오퍼타입</th>
        		<th colspan=4>오퍼 대상 조건</th>
        		<th colspan=2>오퍼 상품 리스트</th>
         	</tr>
        	<tr>
        		<th >최근 30일  </br> 플레이 일 수</th>
        		<th >누적 </br> 결제 금액</th>
        		<th >최근 30일 </br> 결제 금액</th>
        		<th >최근 30일 결제 금액 평균</br>($4.99 이상 상품만 수집 계산)</th>
        		<th >오퍼 </br>상품 금액</th>
        		<th >휠 보너스 일 수</th>
         	</tr>
        </thead>
        <tbody>
        	<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
                <td class="tdc point"> Type 1 </td>
                <td class="tdc" rowspan = 2>10일 이하</td>
                <td class="tdc" rowspan = 2>$5이상 </br> $99 미만</td>
                <td class="tdc" rowspan = 2>$25 이하</td>
                <td class="tdc">$9 미만</td>
                <td class="tdc">$5</td>
                <td class="tdc">14일(14회)</td>
           	</tr>
        	<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
        		<td class="tdc point"> Type 2 </td>
                <td class="tdc">$9 이상</td>
                <td class="tdc">$9</td>
                <td class="tdc">14일(14회)</td>
           	</tr>
        	<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
                <td class="tdc point"> Type 3 </td>
                <td class="tdc" rowspan = 2>10일 이하</td>
                <td class="tdc" rowspan = 2>$99 이상</td>
                <td class="tdc" rowspan = 2>$70 이하</td>
                <td class="tdc">$59 미만</td>
                <td class="tdc">$19</td>
                <td class="tdc">14일(14회)</td>
           	</tr>
        	<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
        		<td class="tdc point"> Type 4 </td>
                <td class="tdc">$59 이상</td>
                <td class="tdc">$59</td>
                <td class="tdc">14일(14회)</td>
            </tr>
        	<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
                <td class="tdc point"> Type 5 </td>
                <td class="tdc" rowspan = 2>11~25일</td>
                <td class="tdc" rowspan = 2>$5이상 </br> $99 미만</td>
                <td class="tdc" rowspan = 2>$25 이하</td>
                <td class="tdc">$9 미만</td>
                <td class="tdc">$5</td>
                <td class="tdc">7일(7회)</td>
          	</tr>
        	<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
        		<td class="tdc point"> Type 6 </td>
                <td class="tdc">$9 이상</td>
                <td class="tdc">$9</td>
                <td class="tdc">7일(7회)</td>
            </tr>
        	<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
                <td class="tdc point"> Type 7 </td>
                <td class="tdc" rowspan = 2>11~25일</td>
                <td class="tdc" rowspan = 2>$99 이상</td>
                <td class="tdc" rowspan = 2>$70 이하</td>
                <td class="tdc">$59 미만</td>
                <td class="tdc">$19</td>
                <td class="tdc">7일(7회)</td>
          	</tr>
        	<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
        		<td class="tdc point"> Type 8 </td>
                <td class="tdc">$59 이상</td>
                <td class="tdc">$59</td>
                <td class="tdc">7일(7회)</td>
            </tr>
        </tbody>
    </table>
    </br></br></br>
	
	<div class="h2_title">[기간제 오퍼 대상자 통계]</div>
	<table class="tbl_list_basic1">
		<colgroup>
            <col width="">
            <col width="">
            <col width="">
            <col width="">
            <col width="">    
            <col width="">
            <col width="">
            <col width="">
            <col width="">
		</colgroup>
        <thead>
        	<tr>
        		<th rowspan = 2>날짜</th>
        		<th>구분</th>
        		<th colspan = 9>대상인원</th>
        		<th colspan = 9>구매인원</th>
         	</tr>
        	<tr>
        	    <th>오퍼 타입</th>
        		<th>Type 1</th>
        		<th>Type 2</th>
        		<th>Type 3</th>
        		<th>Type 4</th>
        		<th>Type 5</th>
        		<th>Type 6</th>
        		<th>Type 7</th>
        		<th>Type 8</th>
        		<th>합계</th>
        		<th>Type 1</th>
        		<th>Type 2</th>
        		<th>Type 3</th>
        		<th>Type 4</th>
        		<th>Type 5</th>
        		<th>Type 6</th>
        		<th>Type 7</th>
        		<th>Type 8</th>
        		<th>합계</th>
         	</tr>
        </thead>
        <tbody>
<?
	
    $subscribe_stat = array();
    
    for($i=0; $i<sizeof($subscribe_stat_list); $i++)
    {
        $today = $subscribe_stat_list[$i]["today"];
        $type = $subscribe_stat_list[$i]["type"];
        $usercount = $subscribe_stat_list[$i]["usercount"];
        $buycount = $subscribe_stat_list[$i]["buycount"];
        $bonus_usercount = $subscribe_stat_list[$i]["bonus_usercount"];
        $bonus_getusercount = $subscribe_stat_list[$i]["bonus_getusercount"];
        
        $subscribe_stat[$today][$type]['usercount'] = $usercount;
        $subscribe_stat[$today][$type]['buycount'] = $buycount;
        $subscribe_stat[$today][$type]['bonus_getusercount'] = $bonus_getusercount;
        $subscribe_stat[$today][$type]['bonus_usercount'] = $bonus_usercount;
    }
    
    for($i=0; $i<sizeof($today_arr); $i++)
    {
        $today = $today_arr[$i]["today"];
        
        $total_usercount=$subscribe_stat[$today][1]['usercount']+$subscribe_stat[$today][2]['usercount']+$subscribe_stat[$today][3]['usercount']
        +$subscribe_stat[$today][4]['usercount']+$subscribe_stat[$today][5]['usercount']+$subscribe_stat[$today][6]['usercount']+$subscribe_stat[$today][7]['usercount']+$subscribe_stat[$today][8]['usercount'];
        
        $total_buycount=$subscribe_stat[$today][1]['buycount']+$subscribe_stat[$today][2]['buycount']+$subscribe_stat[$today][3]['buycount']
        +$subscribe_stat[$today][4]['buycount']+$subscribe_stat[$today][5]['buycount']+$subscribe_stat[$today][6]['buycount']+$subscribe_stat[$today][7]['buycount']+$subscribe_stat[$today][8]['buycount'];
        
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="tdc point"><?= $today ?></td>
            <td class="tdc">오퍼인원</td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][1]['usercount']) ?></td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][2]['usercount']) ?></td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][3]['usercount']) ?></td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][4]['usercount']) ?></td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][5]['usercount']) ?></td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][6]['usercount']) ?></td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][7]['usercount']) ?></td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][8]['usercount']) ?></td>
            <td class="tdc"><?= number_format($total_usercount) ?></td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][1]['buycount']) ?></td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][2]['buycount']) ?></td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][3]['buycount']) ?></td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][4]['buycount']) ?></td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][5]['buycount']) ?></td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][6]['buycount']) ?></td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][7]['buycount']) ?></td>
            <td class="tdc"><?= number_format($subscribe_stat[$today][8]['buycount']) ?></td>
            <td class="tdc"><?= number_format($total_buycount) ?></td>
        </tr>
<?
    }
    
    if(sizeof($subscribe_stat_list) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="5">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
