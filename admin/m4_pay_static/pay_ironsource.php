<?
	$top_menu = "pay_static";
	$sub_menu = "pay_ironsource";
	
	$pagename = "pay_ironsource.php";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$platform = ($_GET["platform"] == "") ? "ALL" : $_GET["platform"];
	$search_sdate = ($_GET["search_sdate"] == "") ? date("Y-m-d", strtotime("-7 day")) : $_GET["search_sdate"];	
	$search_edate = ($_GET["search_edate"] == "") ? $today : $_GET["search_edate"];

	
    $platform_sql = $platform;
    
    if($platform == "ALL")
    {
        $platform_sql = "platform";
    }
	
	$db_slave_main2 = new CDatabase_Main2();
	
	$str_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$str_useridx = 10000;
	}
	
	$sql = "SELECT event_timestamp, platform, SUM(revenue) AS sum_money, COUNT(DISTINCT user_id) AS cnt_user, SUM(impression) AS sum_impression
			FROM `tbl_iron_ad_revenue_measurement_user_level` 
			WHERE platform = $platform_sql AND '$search_sdate 00:00:00' <= event_timestamp AND event_timestamp <= '$search_edate 23:59:59'
			GROUP BY event_timestamp ORDER BY event_timestamp DESC;";
	$data_list = $db_slave_main2->gettotallist($sql);	
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
    
    $(function() {
        $("#search_sdate").datepicker({ });
    });

    $(function() {
        $("#search_edate").datepicker({ });
    });

    function change_platform(term)
	{
		var search_form = document.search_form;
		
		var term_all = document.getElementById("term_all");
		var term_ios = document.getElementById("term_ios");
		var term_and = document.getElementById("term_and");

		document.search_form.platform.value = term;
		
		if (term == "ALL")
		{
			term_all.className="btn_schedule_select";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
		}
		else if (term == 1)
		{
			term_all.className="btn_schedule";
			term_ios.className="btn_schedule_select";
			term_and.className="btn_schedule";
		}
		else if (term == 2)
		{
			term_all.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule_select";
		}

		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
	<div class="contents_wrap">        
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<!-- title_warp -->
			<div class="title_wrap">
				<div class="title"><?= $top_menu_txt ?> &gt; IronSource 수익화</div>
				<div class="search_box">
					<input type="hidden" name="platform" id="platform" value="<?= $platform ?>" />	    			
	    			<input type="button" class="<?= ($platform == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_all" onclick="change_platform('ALL')" value="통합" />
	 	    		<input type="button" class="<?= ($platform == "1") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ios" onclick="change_platform('1')" value="iOS" />
	    			<input type="button" class="<?= ($platform == "2") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_and" onclick="change_platform('2')" value="Android" />						
					<input type="text" class="search_text" id="search_sdate" name="search_sdate" value="<?= $search_sdate ?>" style="width:65px" maxlength="10" onkeypress="search_press(event)" /> ~
					<input type="text" class="search_text" id="search_edate" name="search_edate" value="<?= $search_edate ?>" style="width:65px" maxlength="10" onkeypress="search_press(event)" />		 
					<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />				
				</div>
			</div>

		<!-- //title_warp -->

		<div class="search_result">
			<span><?= $search_sdate ?></span> ~ <span><?= $search_edate ?></span> 통계입니다
		</div>
	
		<div id="tab_content_1">
            <table class="tbl_list_basic1">
	            <colgroup>
	                <col width="200">
	                <col width="300">
	                <col width="300">
	                <col width="300">              
	            </colgroup>
	            <thead>
		            <tr>
		                <th>날짜</th>
		                <th class="tdc">금액</th>
		                <th class="tdc">유저수</th>
		                <th class="tdc">Impression</th>
		           	</tr>
	            </thead>
	            <tbody>
<?
			$total_sum_money = 0;
			$total_cnt_user = 0;
			
			for($i=0; $i<sizeof($data_list); $i++)
			{
				$today = $data_list[$i]["event_timestamp"];
				$sum_money = $data_list[$i]["sum_money"];
				$cnt_user = $data_list[$i]["cnt_user"];
				$sum_impression= $data_list[$i]["sum_impression"];

				
				$total_sum_money += $sum_money;
				$total_cnt_user += $cnt_user;
				$total_sum_impression += $sum_impression;
?>
				
				<tr>
					<td class="tdc point_title"><?= $today ?></td>
					<td class="tdc">$ <?= number_format($sum_money) ?></td>
					<td class="tdc"><?= number_format($cnt_user) ?></td>
					<td class="tdc"><?= number_format($sum_impression) ?></td>
				</tr>
				
<?
			}			
?>
				<tr>
					<td class="tdc point_title">Total</td>
					<td class="tdc">$ <?= number_format($total_sum_money) ?></td>
					<td class="tdc"><?= number_format($total_cnt_user) ?></td>
					<td class="tdc"><?= number_format($total_sum_impression) ?></td>
				</tr>
	            </tbody>
			</table>
		</div>		        
     </form>	
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>