<?
    $top_menu = "pay_static";
    $sub_menu = "payer_4w_stat";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $platform = ($_GET["platform"] == "") ? "ALL" : $_GET["platform"];
    $viewmode = ($_GET["viewmode"] == "") ? "0" : $_GET["viewmode"];
    
    $startdate = $_GET["startdate"];
    $enddate = $_GET["enddate"];
    
    check_xss($platform);
    check_xss($viewmode);
    check_xss($startdate);
    check_xss($enddate);
    
    //오늘 날짜 정보
    $today = date("Y-m-d");
    $before_day = get_past_date($today, 365, "d");
    $startdate = ($startdate == "") ? $before_day : $startdate;
    $enddate = ($enddate == "") ? $today : $enddate;
    
    $db_analysis = new CDatabase_Analysis();
    
    $platform_sql = "";
    
    if($platform != "ALL")
    {
        $platform_sql = "AND platform=$platform ";
    }

    $sql = "SELECT today, SUM(IF(TYPE = 1, payer_cnt, 0)) AS lasted_payer, SUM(IF(TYPE = 2, payer_cnt, 0)) AS new_payer, SUM(IF(TYPE = 3, payer_cnt, 0)) AS return_payer,
            	SUM(IF(TYPE = 1, web_money+ios_money+and_money+ama_money, 0)) AS lasted_money, SUM(IF(TYPE = 2, web_money+ios_money+and_money+ama_money, 0)) AS new_money, SUM(IF(TYPE = 3, web_money+ios_money+and_money+ama_money, 0)) AS return_money,
                SUM(IF(TYPE = 1, web_coin+ios_coin+and_coin+ama_coin, 0)) AS lasted_coin, SUM(IF(TYPE = 2, web_coin+ios_coin+and_coin+ama_coin, 0)) AS new_coin, SUM(IF(TYPE = 3, web_coin+ios_coin+and_coin+ama_coin, 0)) AS return_coin,
                SUM(IF(TYPE = 1, web_basecoin+ios_basecoin+and_basecoin+ama_basecoin, 0)) AS lasted_basecoin, SUM(IF(TYPE = 2, web_basecoin+ios_basecoin+and_basecoin+ama_basecoin, 0)) AS new_basecoin, SUM(IF(TYPE = 3, web_basecoin+ios_basecoin+and_basecoin+ama_basecoin, 0)) AS return_basecoin
            FROM tbl_payer_4w_daily_stat
            WHERE '$startdate' <= today AND today <= '$enddate' $platform_sql
            GROUP BY today
            ORDER BY today ASC";
    $payer_list = $db_analysis->gettotallist($sql);
    
    $db_analysis->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>

<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

	function change_platform(term)
	{
		var search_form = document.search_form;
		
		var term_all = document.getElementById("term_all");
		var term_fb = document.getElementById("term_fb");
		var term_ios = document.getElementById("term_ios");
		var term_and = document.getElementById("term_and");
		var term_ama = document.getElementById("term_ama");

		document.search_form.platform.value = term;
		
		if (term == "ALL")
		{
			term_all.className="btn_schedule_select";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 0)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule_select";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 1)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule_select";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 2)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule_select";
			term_ama.className="btn_schedule";
		}
		else if (term == 3)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule_select";
		}

		search_form.submit();
	}

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}

	function tab_change(tab)
    {
		window.location.href = "/m4_pay_static/payer_stat/payer_4w_daily_stat_" + tab + ".php";
    }
    
	google.load("visualization", "1", {packages:["corechart"]});

    function drawChart() 
    {
    	var data0 = new google.visualization.DataTable();
        data0.addColumn('string', '날짜');
        data0.addColumn('number', '지속 결제자(명)');
        data0.addColumn('number', '신규 결제자(명)');
        data0.addColumn('number', '복귀 결제자(명)');
        data0.addRows([
<?
    for ($i=0; $i<sizeof($payer_list); $i++)
	{
	    $today = $payer_list[$i]["today"];
	    $lasted_payer = $payer_list[$i]["lasted_payer"];
	    $new_payer = $payer_list[$i]["new_payer"];
	    $return_payer = $payer_list[$i]["return_payer"];
		
	    echo("['".$today."',".$lasted_payer.",".$new_payer.",".$return_payer."]");
		
	    if ($i != sizeof($payer_list) - 1)
			echo(",");
	}
?>
        ]);

    	var data1 = new google.visualization.DataTable();
        data1.addColumn('string', '날짜');
        data1.addColumn('number', '지속 결제액($)');
        data1.addColumn('number', '신규 결제액($)');
        data1.addColumn('number', '복귀 결제액($)');
        data1.addRows([
<?
    for ($i=0; $i<sizeof($payer_list); $i++)
	{
	    $today = $payer_list[$i]["today"];
	    $lasted_money = $payer_list[$i]["lasted_money"];
	    $new_money = $payer_list[$i]["new_money"];
	    $return_money = $payer_list[$i]["return_money"];
	    
	    echo("['".$today."',".$lasted_money.",".$new_money.",".$return_money."]");
		
	    if ($i != sizeof($payer_list) - 1)
			echo(",");
	}
?>
        ]);

        var data2 = new google.visualization.DataTable();
        data2.addColumn('string', '날짜');
        data2.addColumn('number', '지속 결제 할인율(%)');
        data2.addColumn('number', '신규 결제 할인율(%)');
        data2.addColumn('number', '복귀 결제 할인율(%)');
        data2.addRows([
<?
    for ($i=0; $i<sizeof($payer_list); $i++)
    {
        $today = $payer_list[$i]["today"];
        $lasted_coin = $payer_list[$i]["lasted_coin"];
        $lasted_basecoin = $payer_list[$i]["lasted_basecoin"];
        $new_coin = $payer_list[$i]["new_coin"];
        $new_basecoin = $payer_list[$i]["new_basecoin"];
        $return_coin = $payer_list[$i]["return_coin"];
        $return_basecoin = $payer_list[$i]["return_basecoin"];
        
        $lasted_rate = round($lasted_coin / $lasted_basecoin * 100, 2);
        $new_rate = round($new_coin / $new_basecoin * 100, 2);
        $return_rate = round($return_coin / $return_basecoin * 100, 2);
        
        echo("['".$today."',".$lasted_rate.",".$new_rate.",".$return_rate."]");
        
        if ($i != sizeof($payer_list) - 1)
            echo(",");
    }
?>
        ]);

    	var options1 = {
            width:1050,                         
            height:470,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:300}
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div0'));
        chart.draw(data0, options1);

        chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
        chart.draw(data1, options1);

        chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
        chart.draw(data2, options1);
    }
        
	google.setOnLoadCallback(drawChart);
</script>
		<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; 최근 28일 전체 결제자 통계</div>
	        </div>
	    	<!-- //title_warp -->
	    	
	    	<ul class="tab">
				<li class="select" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('all')">전체</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('lasted')">지속 결제자</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('new')">신규 결제자</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('return')">복귀 결제자</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('leave')">결제 이탈자</li>
			</ul>
	    	
	    	<form name="search_form" id="search_form"  method="get" action="payer_4w_daily_stat_all.php">
	    		<div class="detail_search_wrap">
	    			<input type="hidden" name="platform" id="platform" value="<?= $platform ?>" />
	    			
	    			<input type="button" class="<?= ($platform == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_all" onclick="change_platform('ALL')" value="통합" />
	    			<input type="button" class="<?= ($platform == "0") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_fb" onclick="change_platform('0')" value="Facebook" />
	    			<input type="button" class="<?= ($platform == "1") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ios" onclick="change_platform('1')" value="iOS" />
	    			<input type="button" class="<?= ($platform == "2") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_and" onclick="change_platform('2')" value="Android" />
	    			<input type="button" class="<?= ($platform == "3") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ama" onclick="change_platform('3')" value="Amazon" />
	    			
					<span class="search_lb2 ml10">View</span>
	    			<select name="viewmode" id="viewmode">										
						<option value="0" <?= ($viewmode=="0") ? "selected" : "" ?>>그래프</option>
						<option value="1" <?= ($viewmode=="1") ? "selected" : "" ?>>리스트</option>                       				
					</select>
	    			
	    			<span class="search_lb2 ml10">날짜</span>
	    			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
		            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
					<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
				</div>
    		</form>
<?
	if($viewmode == 0)
	{
?>
			<div>*지속 결제자: 기준일로부터 D-56 ~ D-28 기간 결제자가 D-28 ~ D-0에도 결제한 유저</div>
			<div>*신규 결제자: 기준일로부터 D-28 ~ D-0 기간 첫 결제한 유저</div>
			<div>*복귀 결제자: 기준일로부터 D-56 ~ D-28 기간 결제가 없으나 D-28 ~ D-0에 결제한 유저</div>
			<br/>
	    	<div class="h2_title">[결제자수]</div>
	    	<div id="chart_div0" style="height:480px; min-width: 500px"></div>
	    	
	    	<div class="h2_title">[결제금액]</div>
	    	<div id="chart_div1" style="height:480px; min-width: 500px"></div>
	    	
	    	<div class="h2_title">[할인율]</div>
	    	<div id="chart_div2" style="height:480px; min-width: 500px"></div>
    	</div>
    	<!--  //CONTENTS WRAP -->
<?          
	}
	else if($viewmode == 1)
	{
?>    	
		<div>*지속 결제자: 기준일로부터 D-56 ~ D-28 기간 결제자가 D-28 ~ D-0에도 결제한 유저</div>
		<div>*신규 결제자: 기준일로부터 D-28 ~ D-0 기간 첫 결제한 유저</div>
		<div>*복귀 결제자: 기준일로부터 D-56 ~ D-28 기간 결제가 없으나 D-28 ~ D-0에 결제한 유저</div>
		<br/>
        <div id="tab_content_1">
    		<table class="tbl_list_basic1">
    			<colgroup>
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    				<col width="">
    			</colgroup>
    			<thead>
    				<tr>
    					<th class="tdc">날짜</th>
    					<th class="tdc">지속결제자</th>
    					<th class="tdc">신규결제자</th>
    					<th class="tdc">복귀결제자</th>
						<th class="tdc">전체결제자</th>
						<th class="tdc">지속결제금액</th>
    					<th class="tdc">신규결제금액</th>
    					<th class="tdc">복귀결제금액</th>
						<th class="tdc">전체결제금액</th>
						<th class="tdc">지속결제할인율</th>
    					<th class="tdc">신규결제할인율</th>
    					<th class="tdc">복귀결제할인율</th>
						<th class="tdc">전체결제할인율</th>
    				</tr>
    			</thead>
    			<tbody>    				
<?
                for($i=0; $i<sizeof($payer_list); $i++)
    			{
    			    $today = $payer_list[$i]["today"];
    			    $lasted_payer = $payer_list[$i]["lasted_payer"];
    			    $new_payer = $payer_list[$i]["new_payer"];
    			    $return_payer = $payer_list[$i]["return_payer"];
    			    $total_payer = $lasted_payer + $new_payer + $return_payer;
    			    
    			    $lasted_money = $payer_list[$i]["lasted_money"];
    			    $new_money = $payer_list[$i]["new_money"];
    			    $return_money = $payer_list[$i]["return_money"];
    			    $total_money = $lasted_money + $new_money + $return_money;
    			    
    			    $lasted_coin = $payer_list[$i]["lasted_coin"];
    			    $new_coin = $payer_list[$i]["new_coin"];
    			    $return_coin = $payer_list[$i]["return_coin"];
    			    $total_coin = $lasted_coin + $new_coin + $return_coin;
    			    
    			    $lasted_basecoin = $payer_list[$i]["lasted_basecoin"];
    			    $new_basecoin = $payer_list[$i]["new_basecoin"];
    			    $return_basecoin = $payer_list[$i]["return_basecoin"];
    			    $total_basecoin = $lasted_basecoin + $new_basecoin + $return_basecoin;
    			    
    			    $lasted_rate = round($lasted_coin / $lasted_basecoin * 100, 1);
    			    $new_rate = round($new_coin / $new_basecoin * 100, 1);
    			    $return_rate = round($return_coin / $return_basecoin * 100, 1);
    			    $total_rate = round($total_coin / $total_basecoin * 100, 1);
?>
					<tr>
    					<td class="tdc point_title"><?= $today?></td>
    					<td class="tdc"><?= number_format($lasted_payer) ?></td>
    					<td class="tdc"><?= number_format($new_payer) ?></td>
    					<td class="tdc"><?= number_format($return_payer) ?></td>
    					<td class="tdc"><?= number_format($total_payer) ?></td>
    					<td class="tdc">$<?= number_format($lasted_money) ?></td>
    					<td class="tdc">$<?= number_format($new_money) ?></td>
    					<td class="tdc">$<?= number_format($return_money) ?></td>
    					<td class="tdc">$<?= number_format($total_money) ?></td>
    					<td class="tdc"><?= number_format($lasted_rate, 1) ?>%</td>
    					<td class="tdc"><?= number_format($new_rate, 1) ?>%</td>
    					<td class="tdc"><?= number_format($return_rate, 1) ?>%</td>
    					<td class="tdc"><?= number_format($total_rate, 1) ?>%</td>
    				</tr>
<?
			}
?>
    			</tbody>
    		</table>
    	</div>						
<?
            }
?>
    	<div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>