<?
    $top_menu = "pay_static";
    $sub_menu = "payer_4w_stat";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $platform = ($_GET["platform"] == "") ? "ALL" : $_GET["platform"];
    $viewmode = ($_GET["viewmode"] == "") ? "0" : $_GET["viewmode"];
    
    $startdate = $_GET["startdate"];
    $enddate = $_GET["enddate"];
    
    check_xss($platform);
    check_xss($viewmode);
    check_xss($startdate);
    check_xss($enddate);
    
    //오늘 날짜 정보
    $today = date("Y-m-d");
    $before_day = get_past_date($today, 365, "d");
    $startdate = ($startdate == "") ? $before_day : $startdate;
    $enddate = ($enddate == "") ? $today : $enddate;
    
    $db_analysis = new CDatabase_Analysis();
    
    $platform_sql = "";
    
    if($platform != "ALL")
    {
        $platform_sql = "AND platform=$platform ";
        
        $sql = "SELECT today, payer_cnt, web_money, web_coin, web_basecoin, ios_money, ios_coin, ios_basecoin, and_money, and_coin, and_basecoin, ama_money, ama_coin, ama_basecoin
                FROM tbl_payer_4w_daily_stat
                WHERE '$startdate' <= today AND today <= '$enddate' AND type = 4 $platform_sql ";
    }
    else
    {
        $sql = "SELECT today, SUM(IF(platform = 0, payer_cnt, 0)) AS web_payer_cnt, SUM(IF(platform = 0, web_money+ios_money+and_money+ama_money, 0)) AS web_money, SUM(IF(platform = 0, web_coin+ios_coin+and_coin+ama_coin, 0)) AS web_coin, SUM(IF(platform = 0, web_basecoin+ios_basecoin+and_basecoin+ama_basecoin, 0)) AS web_basecoin,
                	SUM(IF(platform = 1, payer_cnt, 0)) AS ios_payer_cnt, SUM(IF(platform = 1, web_money+ios_money+and_money+ama_money, 0)) AS ios_money, SUM(IF(platform = 1, web_coin+ios_coin+and_coin+ama_coin, 0)) AS ios_coin, SUM(IF(platform = 1, web_basecoin+ios_basecoin+and_basecoin+ama_basecoin, 0)) AS ios_basecoin,
                	SUM(IF(platform = 2, payer_cnt, 0)) AS and_payer_cnt, SUM(IF(platform = 2, web_money+ios_money+and_money+ama_money, 0)) AS and_money, SUM(IF(platform = 2, web_coin+ios_coin+and_coin+ama_coin, 0)) AS and_coin, SUM(IF(platform = 2, web_basecoin+ios_basecoin+and_basecoin+ama_basecoin, 0)) AS and_basecoin,
                	SUM(IF(platform = 3, payer_cnt, 0)) AS ama_payer_cnt, SUM(IF(platform = 3, web_money+ios_money+and_money+ama_money, 0)) AS ama_money, SUM(IF(platform = 3, web_coin+ios_coin+and_coin+ama_coin, 0)) AS ama_coin, SUM(IF(platform = 3, web_basecoin+ios_basecoin+and_basecoin+ama_basecoin, 0)) AS ama_basecoin
                FROM tbl_payer_4w_daily_stat
                WHERE '$startdate' <= today AND today <= '$enddate' AND type = 4
                GROUP BY today
                ORDER BY today ASC";
    }

    $payer_list = $db_analysis->gettotallist($sql);
    
    $db_analysis->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>

<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

	function change_platform(term)
	{
		var search_form = document.search_form;
		
		var term_all = document.getElementById("term_all");
		var term_fb = document.getElementById("term_fb");
		var term_ios = document.getElementById("term_ios");
		var term_and = document.getElementById("term_and");
		var term_ama = document.getElementById("term_ama");

		document.search_form.platform.value = term;
		
		if (term == "ALL")
		{
			term_all.className="btn_schedule_select";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 0)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule_select";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 1)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule_select";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 2)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule_select";
			term_ama.className="btn_schedule";
		}
		else if (term == 3)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule_select";
		}

		search_form.submit();
	}

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}

	function tab_change(tab)
    {
		window.location.href = "/m4_pay_static/payer_stat/payer_4w_daily_stat_" + tab + ".php";
    }
    
	google.load("visualization", "1", {packages:["corechart"]});

    function drawChart() 
    {

    	var data0 = new google.visualization.DataTable();
        data0.addColumn('string', '날짜');
<?
    $platform_title = "";
    
    if($platform == 0)
    {
        $platform_title = "web";
    }
    else if($platform == 1)
    {
        $platform_title = "ios";
    }
    else if($platform == 2)
    {
        $platform_title = "android";
    }
    else if($platform == 3)
    {
        $platform_title = "amazon";
    }

    if($platform != "ALL")
    {
?>
		data0.addColumn('number', '<?= $platform_title ?>(명)');
<?
    }
    else
    {
?>
		data0.addColumn('number', 'web(명)');
		data0.addColumn('number', 'ios(명)');
		data0.addColumn('number', 'android(명)');
		data0.addColumn('number', 'amazon(명)');
<?
    }
?>
        data0.addRows([
<?
    for ($i=0; $i<sizeof($payer_list); $i++)
	{
	    
	    if($platform != "ALL")
	    {
	        $today = $payer_list[$i]["today"];
	        $payer_cnt = $payer_list[$i]["payer_cnt"];
	        
	        echo("['".$today."',".$payer_cnt."]");
	        
	    }
	    else
	    {
	        $today = $payer_list[$i]["today"];
	        $web_payer_cnt = $payer_list[$i]["web_payer_cnt"];
	        $ios_payer_cnt = $payer_list[$i]["ios_payer_cnt"];
	        $and_payer_cnt = $payer_list[$i]["and_payer_cnt"];
	        $ama_payer_cnt = $payer_list[$i]["ama_payer_cnt"];
	        
	        echo("['".$today."',".$web_payer_cnt.",".$ios_payer_cnt.",".$and_payer_cnt.",".$ama_payer_cnt."]");
	        
	    }
	    
	    if ($i != sizeof($payer_list) - 1)
			echo(",");
	}
?>
        ]);

    	var options1 = {
            width:1050,                         
            height:470,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:300}
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div0'));
        chart.draw(data0, options1);
    }
        
	google.setOnLoadCallback(drawChart);
</script>
		<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; 최근 28일 결제 이탈자 통계</div>
	        </div>
	    	<!-- //title_warp -->
	    	
	    	<ul class="tab">
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('all')">전체</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('lasted')">지속 결제자</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('new')">신규 결제자</li>
				<li class="" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('return')">복귀 결제자</li>
				<li class="select" style="padding-left:3px;padding-right:3px;font-size:9px;" onclick="tab_change('leave')">결제 이탈자</li>
			</ul>
	    	
	    	<form name="search_form" id="search_form"  method="get" action="payer_4w_daily_stat_leave.php">
	    		<div class="detail_search_wrap">
	    			<input type="hidden" name="platform" id="platform" value="<?= $platform ?>" />
	    			
	    			<input type="button" class="<?= ($platform == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_all" onclick="change_platform('ALL')" value="통합" />
	    			<input type="button" class="<?= ($platform == "0") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_fb" onclick="change_platform('0')" value="Facebook" />
	    			<input type="button" class="<?= ($platform == "1") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ios" onclick="change_platform('1')" value="iOS" />
	    			<input type="button" class="<?= ($platform == "2") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_and" onclick="change_platform('2')" value="Android" />
	    			<input type="button" class="<?= ($platform == "3") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ama" onclick="change_platform('3')" value="Amazon" />
	    			
					<span class="search_lb2 ml10">View</span>
	    			<select name="viewmode" id="viewmode">										
						<option value="0" <?= ($viewmode=="0") ? "selected" : "" ?>>그래프</option>
						<option value="1" <?= ($viewmode=="1") ? "selected" : "" ?>>리스트</option>                       				
					</select>
	    			
	    			<span class="search_lb2 ml10">날짜</span>
	    			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
		            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
					<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
				</div>
    		</form>
<?
    $sub_fix_title = "가입플랫폼 기준";
    $sub_title = "가입플랫폼 기준";
    
    if($platform != "ALL")
    {
        $sub_title = "결제플랫폼 기준";
    }

	if($viewmode == 0)
	{
?>
			<div>*지속 결제자: 기준일로부터 D-56 ~ D-28 기간 결제자가 D-28 ~ D-0에도 결제한 유저</div>
			<div>*신규 결제자: 기준일로부터 D-28 ~ D-0 기간 첫 결제한 유저</div>
			<div>*복귀 결제자: 기준일로부터 D-56 ~ D-28 기간 결제가 없으나 D-28 ~ D-0에 결제한 유저</div>
			<br/>
	    	<div class="h2_title">[결제이탈자(<?= $sub_fix_title ?>)]</div>
	    	<div id="chart_div0" style="height:480px; min-width: 500px"></div>
    	</div>
    	<!--  //CONTENTS WRAP -->
<?          
	}
	else if($viewmode == 1)
	{
?>    	
		<div>*지속 결제자: 기준일로부터 D-56 ~ D-28 기간 결제자가 D-28 ~ D-0에도 결제한 유저</div>
		<div>*신규 결제자: 기준일로부터 D-28 ~ D-0 기간 첫 결제한 유저</div>
		<div>*복귀 결제자: 기준일로부터 D-56 ~ D-28 기간 결제가 없으나 D-28 ~ D-0에 결제한 유저</div>
		<br/>
        <div id="tab_content_1">
    		<table class="tbl_list_basic1">
    			<colgroup>
    				<col width="">
<?
                if($platform == "ALL")
                {
?>
					<col width="">
    				<col width="">
    				<col width="">
<?
                }
?>
    				
    				<col width="">
    			</colgroup>
    			<thead>
    				<tr>
    					<th class="tdc" rowspan="2">날짜</th>
    					<th class="tdc" colspan="<?= ($platform == "ALL") ? 4 : 1 ?>">결제이탈자(<?= $sub_fix_title?>)</th>
    				</tr>
    				<tr>
<?
                if($platform == "ALL")
                {
?>
						<th class="tdc">web</th>
    					<th class="tdc">ios</th>
    					<th class="tdc">android</th>
						<th class="tdc">amazon</th>
<?
                }
                else
                {
?>
						<th class="tdc"><?= $platform_title?></th>
<?
                }
?>
    				</tr>
    			</thead>
    			<tbody>    				
<?
                for($i=0; $i<sizeof($payer_list); $i++)
    			{
    			    $today = $payer_list[$i]["today"];
    			    
    			    if($platform == "ALL")
    			    {
    			        $web_payer_cnt = $payer_list[$i]["web_payer_cnt"];
    			        $ios_payer_cnt = $payer_list[$i]["ios_payer_cnt"];
    			        $and_payer_cnt = $payer_list[$i]["and_payer_cnt"];
    			        $ama_payer_cnt = $payer_list[$i]["ama_payer_cnt"];
    			    }
    			    else
    			    {
    			        $payer_cnt = $payer_list[$i]["payer_cnt"];
    			    }
?>
					<tr>
    					<td class="tdc point_title"><?= $today?></td>
<?
                    if($platform == "ALL")
                    {
?>
						<td class="tdc"><?= number_format($web_payer_cnt) ?></td>
    					<td class="tdc"><?= number_format($ios_payer_cnt) ?></td>
    					<td class="tdc"><?= number_format($and_payer_cnt) ?></td>
    					<td class="tdc"><?= number_format($ama_payer_cnt) ?></td>
<?
                    }
                    else
                    {
?>
						<td class="tdc"><?= number_format($payer_cnt) ?></td>
<?
                    }
?>
    				</tr>
<?
			}
?>
    			</tbody>
    		</table>
    	</div>						
<?
            }
?>
    	<div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>