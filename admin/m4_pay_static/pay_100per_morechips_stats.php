<?
	$top_menu = "pay_static";
	$sub_menu = "pay_100per_morechips_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
	
	$tab = ($_GET["tab"] == "") ? "0" : $_GET["tab"];
	
	check_xss($startdate);
	check_xss($enddate);
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today,15,"d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db_slave = new CDatabase_Slave_Main();
	$db_main2 = new CDatabase_Main2();
	
	$str_useridx = 20000;	
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$str_useridx = 10000;
	}
	
	if($tab == 0)
	{
		$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, COUNT(orderidx) AS buy_cnt, SUM(facebookcredit/10) AS money 
				FROM tbl_product_order
				WHERE useridx > $str_useridx AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' AND couponidx <> 0 AND special_more = 100 AND status IN (1, 3)
				GROUP BY today
				ORDER BY today DESC";
	}
	else
	{
		$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, COUNT(orderidx) AS buy_cnt, SUM(money) AS money, os_type 
				FROM tbl_product_order_mobile 
				WHERE useridx > $str_useridx AND writedate BETWEEN '$startdate 00:00:00' AND '$enddate 23:59:59' AND couponidx <> 0 AND special_more >= 100 AND status = 1
				GROUP BY today, os_type
				ORDER BY today DESC";
	}
	$product_stat_list = $db_slave->gettotallist($sql);
	
	function get_today_row_array($summarylist, $today)
	{
	    $row_array = array();
	    
	    for ($i=0; $i<sizeof($summarylist); $i++)
	    {
	        if ($summarylist[$i]["today"] == $today)
	        {
	            array_push($row_array, $summarylist[$i]["os_type"]);
	        }
	    }
	    
	    return $row_array;
	}
	
	
	$db_slave->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

	function tab_change(tab)
	{
		var search_form = document.search_form;
		search_form.tab.value = tab;
		search_form.submit();
	}

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">

	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="pay_100per_morechips_stats.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 100% More Chips 통계</div>
		<div class="search_box"> 
			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
            <input type=hidden name="tab" value="<?= $tab ?>">
             <input type="button" class="btn_search" value="검색" onclick="search()" />
		</div>
    </div>
	<!-- //title_warp -->
	
    <div class="search_result">
    	<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다
    </div>
	
	<ul class="tab">
		<li id="tab_1" class="<?= ($tab == "0") ? "select" : "" ?>" style="width:50px;text-align:center;font-size:11px;" onclick="tab_change('0')">Web</li>
		<li id="tab_2" class="<?= ($tab == "1") ? "select" : "" ?>" style="width:50px;text-align:center;font-size:11px;" onclick="tab_change('1')">Mobile</li>
	</ul>

	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
<?
				if($tab == 1)
				{
?>
				<col width="">
<?
				}
?>
				<col width="">
				<col width="">
				<col width="">				
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
<?
				if($tab == 1)
				{
?>
					<th class="tdc">OS</th>
<?
				}
?>
					<th class="tdc">구매 수</th>
					<th class="tdc">구매 금액</th>
					<th class="tdc">평균 구매 금액</th>
					<th class="tdc">발급 량</th>

				</tr>
			</thead>
			<tbody>
<?
			$total_buy_cnt = 0;
			$total_money = 0;
			
			for($i=0; $i<sizeof($product_stat_list); $i++)
			{
				$today = $product_stat_list[$i]["today"];
				$buy_cnt = $product_stat_list[$i]["buy_cnt"];
				$money = $product_stat_list[$i]["money"];

				$add_count=$db_main2->getvalue("select use_count+nouse_count from `tbl_100coupon_daily` where today = '$today';");
				
				if($tab == 1)
				{
					$os_type = $product_stat_list[$i]["os_type"];
					
					if($os_type == 1)
						$os_name = "IOS";
					else if($os_type == 2)
						$os_name = "AND";
					else if($os_type == 3)
						$os_name = "AMA";
				}

				
				
				$total_buy_cnt += $buy_cnt;
				$total_money += $money;
				
?>
				<tr>
					<td class="tdc point_title"><?= $today ?></td>
<?
				if($tab == 1)
				{
?>
					<td class="tdc"><?= $os_name ?></td>
<?
				}
?>
					<td class="tdc"><?= number_format($buy_cnt) ?></td>
					<td class="tdc">$<?= number_format($money, 1) ?></td>
					<td class="tdc">$<?= ($buy_cnt == 0) ? 0 : number_format($money / $buy_cnt, 1) ?></td>
					<td class="tdc"><?= number_format(($add_count=="")?0:$add_count) ?></td>
					
				</tr>
<?
			}
?>
				<tr>
					<td class="tdc point_title">Total</td>
<?
				if($tab == 1)
				{
?>
					<td class="tdc point">-</td>
<?
				}
?>
					<td class="tdc point"><?= number_format($total_buy_cnt) ?></td>
					<td class="tdc point">$<?= number_format($total_money, 1) ?></td>
					<td class="tdc point">$<?= ($total_buy_cnt == 0) ? 0 : number_format($total_money / $total_buy_cnt, 1) ?></td>
					<td class="tdc point"></td>
				</tr>
			</tbody>
		</table>
	</div>
	</form>	
</div>
    	<!--  //CONTENTS WRAP -->    	
	<div class="clear"></div>

<?
    $db_main2->end();
    
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>