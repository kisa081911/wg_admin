<?
	$top_menu = "pay_static";
	$sub_menu = "pay_static";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$term = ($_GET["term"] == "") ? "day" : $_GET["term"];
	$platform = ($_GET["platform"] == "") ? "ALL" : $_GET["platform"];
	
	if ($term != "day" && $term != "week" && $term != "month")
		error_back("잘못된 접근입니다.");
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
		
	check_xss($startdate);
	check_xss($enddate);
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today, 15, "d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db = new CDatabase_Main();
	
	if($platform == "ALL")
	{
		$table = "SELECT orderidx,facebookcredit / 10 AS money, useridx, couponidx, special_more, special_discount, status, writedate
					FROM tbl_product_order
					WHERE status IN (1,3) AND useridx > 20000 AND writedate >= '$startdate 00:00:00' AND writedate <= '$enddate 23:59:59'
					UNION ALL
					SELECT orderidx,money, useridx, couponidx, special_more, special_discount, STATUS, writedate
					FROM tbl_product_order_mobile
					WHERE status=1 AND useridx > 20000 AND writedate >= '$startdate 00:00:00' AND writedate <= '$enddate 23:59:59'";
	}
	else if($platform == 0 || $platform == 4)
	{
	
			
		$table = "SELECT orderidx,facebookcredit / 10 AS money, useridx, couponidx, special_more, special_discount, status, writedate
					FROM tbl_product_order
					WHERE status IN (1,3) AND useridx > 20000 AND writedate >= '$startdate 00:00:00' AND writedate <= '$enddate 23:59:59' $tail";
	
		
	}
	else if($platform == 1 || $platform == 2 || $platform == 3)
	{
		$table = "SELECT orderidx,money, useridx, couponidx, special_more, special_discount, status, writedate
					FROM tbl_product_order_mobile
					WHERE status=1 AND useridx > 20000 AND writedate >= '$startdate 00:00:00' AND writedate <= '$enddate 23:59:59' AND os_type = $platform ";
	}
    
	$sql = "SELECT LEFT(writedate, 10) as writedate, COUNT(IF(status=1, 1, NULL)) AS status1,
			SUM(IF(status=1, money, 0)) AS money,
			COUNT(*) AS totalcount,
			SUM(IF(status=1 AND vip_point <= 499, money, 0)) AS normal_money,
			SUM(IF(status=1 AND (couponidx=0 AND (special_discount > 0 OR special_more > 0)), money,0)) AS special_money,
			SUM(IF(status=1 AND couponidx<>0, money, 0)) AS coupon_money,
			SUM(IF(status=1 AND (couponidx=0 AND (special_discount > 0 OR special_more > 0)), 1, 0)) AS special_totalcount,
			SUM(IF(status=1 AND couponidx <> 0, 1, 0)) AS coupon_totalcount,
			SUM(IF(status=1 AND vip_point > 499, money, 0)) AS vip_money,
			SUM(IF(status=1 AND vip_point > 499, 1, 0)) AS vip_totalcount,
			SUM(IF(STATUS=1 AND ispremium=1 AND vip_point < 499,money,0)) AS premium_money
			FROM (
				$table
			) t1 JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx
			GROUP BY LEFT(writedate, 10)
			ORDER BY writedate ASC";
	
	if ($term == "day")
	{
		$sql = "SELECT LEFT(writedate, 10) as writedate, COUNT(IF(status=1, 1, NULL)) AS status1,
				SUM(IF(status=1, money, 0)) AS money,
				COUNT(*) AS totalcount,
				SUM(IF(status=1 AND vip_point <= 499, money, 0)) AS normal_money,
				SUM(IF(status=1 AND (couponidx=0 AND (special_discount > 0 OR special_more > 0)), money, 0)) AS special_money,
				SUM(IF(status=1 AND couponidx <> 0, money, 0)) AS coupon_money,
				SUM(IF(status=1 AND (couponidx=0 AND (special_discount>0 OR special_more>0)), 1, 0)) AS special_totalcount,
				SUM(IF(status=1 AND couponidx <> 0, 1, 0)) AS coupon_totalcount,
				SUM(IF(status=1 AND vip_point > 499,money, 0)) AS vip_money,
				SUM(IF(status=1 AND vip_point > 499, 1, 0)) AS vip_totalcount,
                SUM(IF(STATUS=1 AND ispremium=1 AND vip_point < 499,money,0)) AS premium_money
	  			FROM (
	  				$table
	  			) t1 JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx
				GROUP BY LEFT(writedate, 10)
				ORDER BY writedate ASC";
	}
	else if ($term == "week")
	{
		$sql = "SELECT WEEK(writedate) AS week, LEFT(writedate,4) AS year, COUNT(IF(status=1, 1, NULL)) AS status1,
				SUM(IF(status=1, money, 0)) AS money,
				COUNT(orderidx) AS totalcount,
				SUM(IF(status=1 AND vip_point <= 499, money, 0)) AS normal_money,
				SUM(IF(status=1 AND (couponidx=0 AND (special_discount>0 OR special_more>0)), money, 0)) AS special_money,
				SUM(IF(status=1 AND couponidx <> 0, money, 0)) AS coupon_money,
				SUM(IF(status=1 AND (couponidx=0 AND (special_discount>0 OR special_more>0)), 1, 0)) AS special_totalcount,
				SUM(IF(status=1 AND couponidx <> 0, 1, 0)) AS coupon_totalcount,
				SUM(IF(status=1 AND vip_point > 499, money, 0)) AS vip_money,
				SUM(IF(status=1 AND vip_point > 499, 1, 0)) AS vip_totalcount,
                SUM(IF(STATUS=1 AND ispremium=1 AND vip_point < 499,money,0)) AS premium_money
	  			FROM (
	  				$table
	  			) t1 JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx 
				GROUP BY CONCAT(LEFT(writedate,4),WEEK(writedate))
				ORDER BY writedate ASC";
	}
	else if ($term == "month")
	{
		$sql = "SELECT LEFT(writedate,7) AS writedate, COUNT(IF(status=1,1,NULL)) AS status1,
				SUM(IF(status=1,money,0)) AS money,
				COUNT(orderidx) AS totalcount,
				SUM(IF(status=1 AND vip_point <= 499, money, 0)) AS normal_money,
				SUM(IF(status=1 AND (couponidx=0 AND (special_discount>0 OR special_more>0)), money, 0)) AS special_money,
				SUM(IF(status=1 AND couponidx <> 0, money, 0)) AS coupon_money,
				SUM(IF(status=1 AND (couponidx=0 AND (special_discount>0 OR special_more>0)), 1, 0)) AS special_totalcount,
				SUM(IF(status=1 AND couponidx <> 0, 1, 0)) AS coupon_totalcount,
				SUM(IF(status=1 AND vip_point > 499, money, 0)) AS vip_money,
				SUM(IF(status=1 AND vip_point > 499, 1, 0)) AS vip_totalcount,
                SUM(IF(STATUS=1 AND ispremium=1 AND vip_point < 499,money,0)) AS premium_money
  				FROM (
  					$table
  				)t1 JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx
				GROUP BY LEFT(writedate,7)
				ORDER BY writedate ASC";
	}
	
	$staticlist = $db->gettotallist($sql);
	
	$start_week = get_week($startdate);
	$end_week = get_week($enddate);
	$start_month = substr($startdate,0,7);
	$end_month = substr($enddate,0,7);
	$end_year = substr($enddate,0,4);
		
	//결제금액, 결제 건수, 결제비율 array로 구성
	$money_list = array();
	$vip_money_list = array();
	$normal_money_list = array();
	$special_money_list = array();
	$coupon_money_list = array();
	$status1_list = array();
	$totalcount_list = array();
	$special_totalcount_list = array();
	$coupon_totalcount_list = array();
	$vip_totalcount_list = array();
	$date_list = array();
	$premium_money_list = array();
	
	$list_pointer = sizeof($staticlist);
		
	if ($term == "day")
	{
		$date_pointer = $enddate;
		
		for ($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
		{
			$writedate = $staticlist[$list_pointer-1]["writedate"];
			
			if (get_diff_date($date_pointer, $writedate, "d") == 0)
			{
				$static = $staticlist[$list_pointer-1];
				
				$money_list[$i] = $static["money"];
				$vip_money_list[$i] = $static["vip_money"];
				$normal_money_list[$i] = $static["normal_money"];
				$special_money_list[$i] = $static["special_money"];
				$coupon_money_list[$i] = $static["coupon_money"];
				$premium_money_list[$i] = $static["premium_money"];
				$status1_list[$i] = $static["status1"];
				$totalcount_list[$i] = $static["totalcount"];
				$special_totalcount_list[$i] = $static["special_totalcount"];
				$coupon_totalcount_list[$i] = $static["coupon_totalcount"];
				$vip_totalcount_list[$i] = $static["vip_totalcount"];
				$date_list[$i] = $date_pointer;
				
				$list_pointer--;
			}
			else 
			{
				$money_list[$i] = 0;
				$vip_money_list[$i] = 0;
				$normal_money_list[$i] = 0;
				$special_money_list[$i] = 0;
				$premium_money_list[$i] = 0;
				$coupon_money_list[$i] = 0;
				$status1_list[$i] = 0;
				$totalcount_list[$i] = 0;
				$special_totalcount_list[$i] = 0;
				$coupon_totalcount_list[$i] = 0;
				$vip_totalcount_list[$i] = 0;
				$date_list[$i] = $date_pointer;				
			}
			
			$date_pointer = get_past_date($date_pointer, 1, "d");
		}
	}
	else if ($term == "week")
	{
		$date_pointer = get_week_date($end_year, $end_week, 0);
		
		for ($i=0; $i<=get_diff_date($enddate, $startdate, "w"); $i++)
		{
			$week = $staticlist[$list_pointer-1]["week"];
			$year = $staticlist[$list_pointer-1]["year"];
			
			$write_week = get_week_date($year,$week,0);
			
			if ($date_pointer == $write_week)
			{
				$static = $staticlist[$list_pointer-1];
				
				$money_list[$i] = $static["money"];
				$vip_money_list[$i] = $static["vip_money"];
				$normal_money_list[$i] = $static["normal_money"];
				$special_money_list[$i] = $static["special_money"];
				$coupon_money_list[$i] = $static["coupon_money"];
				$premium_money_list[$i] = $static["premium_money"];
				$status1_list[$i] = $static["status1"];
				$totalcount_list[$i] = $static["totalcount"];
				$special_totalcount_list[$i] = $static["special_totalcount"];
				$coupon_totalcount_list[$i] = $static["coupon_totalcount"];
				$vip_totalcount_list[$i] = $static["vip_totalcount"];
				
				$list_pointer--;
			}
			else
			{
				$money_list[$i] = 0;
				$vip_money_list[$i] = 0;
				$normal_money_list[$i] = 0;
				$special_money_list[$i] = 0;
				$coupon_money_list[$i] = 0;
				$premium_money_list[$i] = 0;
				$status1_list[$i] = 0;
				$totalcount_list[$i] = 0;
				$special_totalcount_list[$i] = 0;
				$coupon_totalcount_list[$i] = 0;
				$vip_totalcount_list[$i] = 0;
			}
			
			if (get_diff_date($startdate, $date_pointer, "d") > 0)
				$_date = $startdate;
			else 
				$_date = $date_pointer;
			
			$date_list[$i] = $_date." ~";
			
			$date_pointer = get_past_date($date_pointer, 1, "w");
		}
	}
	else if ($term == "month")
	{
		$date_pointer = $end_month;

		for($i=0; $i<=get_diff_date($end_month, $start_month, "m"); $i++)
		{
			$write_month = $staticlist[$list_pointer-1]["writedate"];
			
			if (get_diff_date($date_pointer, $write_month, "m") == 0)
			{
				$static = $staticlist[$list_pointer-1];
				
				$money_list[$i] = $static["money"];
				$vip_money_list[$i] = $static["vip_money"];
				$normal_money_list[$i] = $static["normal_money"];
				$special_money_list[$i] = $static["special_money"];
				$coupon_money_list[$i] = $static["coupon_money"];
				$premium_money_list[$i] = $static["premium_money"];
				$status1_list[$i] = $static["status1"];
				$totalcount_list[$i] = $static["totalcount"];
				$special_totalcount_list[$i] = $static["special_totalcount"];
				$coupon_totalcount_list[$i] = $static["coupon_totalcount"];
				$vip_totalcount_list[$i] = $static["vip_totalcount"];
				$date_list[$i] = $date_pointer;
				
				$list_pointer--;
			}
			else
			{
				$money_list[$i] = 0;
				$vip_money_list[$i] = 0;
				$normal_money_list[$i] = 0;
				$special_money_list[$i] = 0;
				$coupon_money_list[$i] = 0;
				$premium_money_list[$i] = 0;
				$status1_list[$i] = 0;
				$totalcount_list[$i] = 0;
				$special_totalcount_list[$i] = 0;
				$coupon_totalcount_list[$i] = 0;
				$vip_totalcount_list[$i] = 0;
				$date_list[$i] = $date_pointer;
			}
			
			$date_pointer = get_past_date($date_pointer, 1, "m");
		}
	}

	$db->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>

<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

	google.load("visualization", "1", {packages:["corechart"]});

    function drawChart() 
    {
        var data1 = new google.visualization.DataTable();
        data1.addColumn('string', '날짜');
        data1.addColumn('number', '총 결제완료금액($)');
        data1.addColumn('number', 'VIP회원($)');
        data1.addColumn('number', '일반회원($)');
        data1.addColumn('number', '스페셜오퍼($)');
        data1.addColumn('number', '쿠폰($)');
        data1.addColumn('number', 'Premium회원/VIP회원아님($)');
        data1.addRows([
<?
	for ($i=sizeof($date_list); $i>0; $i--)
	{
		$_money = $money_list[$i-1];
		$_vip_money = $vip_money_list[$i-1];
		$_normal_money = $normal_money_list[$i-1];
		$_special_money = $special_money_list[$i-1];
		$_coupon_money = $coupon_money_list[$i-1];
		$_premium_money = $premium_money_list[$i-1];
        $_date = $date_list[$i-1];
		
        echo("['".$_date."',".$_money.",".$_vip_money.",".$_normal_money.",".$_special_money.",".$_coupon_money.",".$_premium_money."]");
		
		if ($i > 1)
			echo(",");
	}
?>
        ]);
        
        var data2 = new google.visualization.DataTable();
        data2.addColumn('string', '날짜');
        data2.addColumn('number', '결제완료건');
        data2.addColumn('number', '스페셜오퍼 결제완료건');
        data2.addColumn('number', '쿠폰 결제완료건');
        data2.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_status1 = $totalcount_list[$i-1];
        $_status3 = $special_totalcount_list[$i-1];
        $_status4 = $coupon_totalcount_list[$i-1];
        $_date = $date_list[$i-1];
        
        echo("['".$_date."',".$_status1.",".$_status3.",".$_status4."]");
        
        if ($i > 1)
            echo(",");
    }
?>
        ]);

        var data3 = new google.visualization.DataTable();
        data3.addColumn('string', '날짜');
        data3.addColumn('number', '정상 결제금액(%)');
        data3.addColumn('number', '스페셜오퍼 결제금액(%)');
        data3.addColumn('number', '쿠폰 결제금액(%)');
        data3.addRows([
<?
    for ($i=sizeof($date_list); $i>0; $i--)
    {
        $_total_credit = $money_list[$i-1];
        $_special_credit = $special_money_list[$i-1];
        $_coupon_credit = $coupon_money_list[$i-1];
        $_normal_credit = $_total_credit - ($_special_credit + $_coupon_credit);
        
        $_special_credit = ($_total_credit == 0) ? 0 : round($_special_credit/$_total_credit * 100, 1);
        $_coupon_credit = ($_total_credit == 0) ? 0 : round($_coupon_credit/$_total_credit * 100, 1);
        $_normal_credit = ($_total_credit == 0) ? 0 : round($_normal_credit/$_total_credit * 100, 1);
        $_date = $date_list[$i-1];
        
        echo("['".$_date."',".$_normal_credit.",".$_special_credit.",".$_coupon_credit."]");
        
        if ($i > 1)
            echo(",");
    }
?>
        ]);
    
        var options1 = {
            width:1050,                         
            height:470,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:300}
        };
        
        var options2 = {
            width:1050,                         
            height:470,
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize : 12,
            chartArea:{left:60,top:40,width:1040,height:300}
        };

        var options3 = {
                width:1050,                         
                height:470,
                axisTitlesPosition:'in',
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize : 12,
                chartArea:{left:60,top:40,width:1040,height:300}
            };
                
        var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
        chart.draw(data1, options1);        
        
        chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
        chart.draw(data2, options2);

        chart = new google.visualization.LineChart(document.getElementById('chart_div3'));
        chart.draw(data3, options3);      
    }
        
	google.setOnLoadCallback(drawChart);	

	function change_term(term)
	{
		var search_form = document.search_form;
		
		var day = document.getElementById("term_day");
		var week = document.getElementById("term_week");
		var month = document.getElementById("term_month");
		
		document.search_form.term.value = term;

		if (term == "day")
		{
			day.className="btn_schedule_select";
			week.className="btn_schedule";
			month.className="btn_schedule";
		}
		else if (term == "week")
		{
			day.className="btn_schedule";
			week.className="btn_schedule_select";
			month.className="btn_schedule";			
		}
		else if (term == "month")
		{
			day.className="btn_schedule";
			week.className="btn_schedule";
			month.className="btn_schedule_select";	
		}

		search_form.submit();
	}
	
	function change_platform(term)
	{
		var search_form = document.search_form;
		
		var term_all = document.getElementById("term_all");
		var term_fb = document.getElementById("term_fb");
		var term_ios = document.getElementById("term_ios");
		var term_and = document.getElementById("term_and");
		var term_ama = document.getElementById("term_ama");

		document.search_form.platform.value = term;
		
		if (term == "ALL")
		{
			term_all.className="btn_schedule_select";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 0)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule_select";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 1)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule_select";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule";
		}
		else if (term == 2)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule_select";
			term_ama.className="btn_schedule";
		}
		else if (term == 3)
		{
			term_all.className="btn_schedule";
			term_fb.className="btn_schedule";
			term_ios.className="btn_schedule";
			term_and.className="btn_schedule";
			term_ama.className="btn_schedule_select";
		}

		search_form.submit();
	}

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}
    
    function check_sleeptime()
    {
    }
</script>
		<!-- CONTENTS WRAP -->
    	<div class="contents_wrap">
    	
	    	<!-- title_warp -->
	    	<div class="title_wrap">
	    		<div class="title"><?= $top_menu_txt ?> &gt; 기간별 통계</div>
	    		<form name="search_form" id="search_form"  method="get" action="pay_static_new.php">
	    		<input type="hidden" name="term" id="term" value="<?= $term ?>" />
	    		<input type="hidden" name="platform" id="platform" value="<?= $platform ?>" />
	    		<div class="search_box">
	    			<input type="button" class="<?= ($platform == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_all" onclick="change_platform('ALL')" value="통합" />
	    			<input type="button" class="<?= ($platform == "0") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_fb" onclick="change_platform('0')" value="Facebook" />
	    			<input type="button" class="<?= ($platform == "1") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ios" onclick="change_platform('1')" value="iOS" />
	    			<input type="button" class="<?= ($platform == "2") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_and" onclick="change_platform('2')" value="Android" />
	    			<input type="button" class="<?= ($platform == "3") ? "btn_schedule_select" : "btn_schedule" ?>" id="term_ama" onclick="change_platform('3')" value="Amazon" />
	    			&nbsp;&nbsp;
	    			<input type="button" class="<?= ($term == "day") ? "btn_schedule_select" : "btn_schedule" ?>" value="일" id="term_day" onclick="change_term('day')" />
	    			<input type="button" class="<?= ($term == "week") ? "btn_schedule_select" : "btn_schedule" ?>" value="주"  id="term_week"  onclick="change_term('week')" />
	    			<input type="button" class="<?= ($term == "month") ? "btn_schedule_select" : "btn_schedule" ?>" value="월"  onclick="change_term('month')"   id="term_month" />
	    			 
	    			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:70px"  onkeypress="search_press(event)" /> ~
		            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:70px" maxlength="10"  onkeypress="search_press(event)" />
		             <input type="button" class="btn_search" value="검색" onclick="search()" />
	    		</div>
	    		</form>
	        </div>
	    	<!-- //title_warp -->
	    	
            <div class="search_result">
            	<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span>
<?
	if ($term == "day")
		echo("일별");
	else if ($term == "week")
		echo("주별");
	else if ($term == "month")
		echo("월별");
?>
				통계입니다
            </div>
	    	
	    	<div id="chart_div1" style="height:480px; min-width: 500px"></div>
	    	<div id="chart_div2" style="height:480px; min-width: 500px"></div>
	    	<div id="chart_div3" style="height:480px; min-width: 500px"></div>

    	</div>
    	<!--  //CONTENTS WRAP -->
    	
    	<div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>