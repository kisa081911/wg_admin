<?
	$top_menu = "pay_static";
	$sub_menu = "buyer_grade_stat";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$startdate = ($_GET["startdate"] == "") ? date("Y-m-d", strtotime("-180 day")) : $_GET["startdate"];
	$enddate = ($_GET["enddate"] == "") ? date("Y-m-d", strtotime("-1 day")) : $_GET["enddate"];
	
	$term = ($_GET["term"] == "") ? "0" : $_GET["term"];
	
	$db_other = new CDatabase_Other();
	
	$sql = "SELECT * FROM tbl_buyer_grade_stat WHERE today BETWEEN '$startdate' AND '$enddate' AND type = 4";
	$killer_whale_grade_stat = $db_other->gettotallist($sql);
	
	$sql = "SELECT * FROM tbl_buyer_grade_stat WHERE today BETWEEN '$startdate' AND '$enddate' AND type = 3";
	$whale_grade_stat = $db_other->gettotallist($sql);
	
	$sql = "SELECT * FROM tbl_buyer_grade_stat WHERE today BETWEEN '$startdate' AND '$enddate' AND type = 2";
	$sea_lion_grade_stat = $db_other->gettotallist($sql);
	
	$sql = "SELECT * FROM tbl_buyer_grade_stat WHERE today BETWEEN '$startdate' AND '$enddate' AND type = 1";
	$dolphin_grade_stat = $db_other->gettotallist($sql);
	
	$sql = "SELECT * FROM tbl_buyer_grade_stat WHERE today BETWEEN '$startdate' AND '$enddate' AND type = 0";
	$minnow_whale_grade_stat = $db_other->gettotallist($sql);
	
	$db_other->end();
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
		$("#startdate").datepicker({});
		$("#enddate").datepicker({});
	});
	
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	    
	    if (search_form.startdate.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.startdate.focus();
	        return;
	    }
	
	    if (search_form.enddate.value == "")
	    {
	        alert("날짜를 입력하세요.");
	        search_form.enddate.focus();
	        return;
	    }
	
	    search_form.submit();
	}
	
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart);
	
	function drawChart() 
	{
		var data1 = google.visualization.arrayToDataTable([
		['날짜', 'Web', 'iOS', 'And', 'Ama', 'Usercount'],
<?
		    for ($i=0; $i<sizeof($killer_whale_grade_stat); $i++)
		    {
		        $_today = $killer_whale_grade_stat[$i]["today"];
		        $_web_paymoney = $killer_whale_grade_stat[$i]["web_paymoney"];
		        $_ios_paymoney = $killer_whale_grade_stat[$i]["ios_paymoney"];
		        $_and_paymoney = $killer_whale_grade_stat[$i]["and_paymoney"];
		        $_ama_paymoney = $killer_whale_grade_stat[$i]["ama_paymoney"];
		        $_usercount = $killer_whale_grade_stat[$i]["usercount"];
		
		        echo("['".$_today."'");
		
				if ($_web_paymoney != "")
					echo(",{v:".$_web_paymoney.",f:'".number_format($_web_paymoney)."'}");
				else
					echo(",0");
		
				if ($_ios_paymoney != "")
					echo(",{v:".$_ios_paymoney.",f:'".number_format($_ios_paymoney)."'}");
				else
					echo(",0");
		
				if ($_and_paymoney != "")
					echo(",{v:".$_and_paymoney.",f:'".number_format($_and_paymoney)."'}");
				else
					echo(",0");
				
				if ($_ama_paymoney != "")
					echo(",{v:".$_ama_paymoney.",f:'".number_format($_ama_paymoney)."'}");
				else
					echo(",0");
		
				if ($_usercount != "")
					echo(",{v:".$_usercount.",f:'".number_format($_usercount)."'}]");
				else
					echo(",0]");
		        
		        if ($i < sizeof($killer_whale_grade_stat))
		        	echo(",");
		        
		    }
?>
		]);
		
		var data2 = google.visualization.arrayToDataTable([
		['날짜', 'Web', 'iOS', 'And', 'Ama', 'Usercount'],
<?
		    for ($i=0; $i<sizeof($killer_whale_grade_stat); $i++)
		    {
		        $_today = $whale_grade_stat[$i]["today"];
		        $_web_paymoney = $whale_grade_stat[$i]["web_paymoney"];
		        $_ios_paymoney = $whale_grade_stat[$i]["ios_paymoney"];
		        $_and_paymoney = $whale_grade_stat[$i]["and_paymoney"];
		        $_ama_paymoney = $whale_grade_stat[$i]["ama_paymoney"];
		        $_usercount = $whale_grade_stat[$i]["usercount"];
		
		        echo("['".$_today."'");
		
				if ($_web_paymoney != "")
					echo(",{v:".$_web_paymoney.",f:'".number_format($_web_paymoney)."'}");
				else
					echo(",0");
		
				if ($_ios_paymoney != "")
					echo(",{v:".$_ios_paymoney.",f:'".number_format($_ios_paymoney)."'}");
				else
					echo(",0");
		
				if ($_and_paymoney != "")
					echo(",{v:".$_and_paymoney.",f:'".number_format($_and_paymoney)."'}");
				else
					echo(",0");
				
				if ($_ama_paymoney != "")
					echo(",{v:".$_ama_paymoney.",f:'".number_format($_ama_paymoney)."'}");
				else
					echo(",0");
		
				if ($_usercount != "")
					echo(",{v:".$_usercount.",f:'".number_format($_usercount)."'}]");
				else
					echo(",0]");
		        
		        if ($i < sizeof($killer_whale_grade_stat))
		        	echo(",");
		        
		    }
?>
		]);
		
		var data3 = google.visualization.arrayToDataTable([
		['날짜', 'Web', 'iOS', 'And', 'Ama', 'Usercount'],
<?
		    for ($i=0; $i<sizeof($sea_lion_grade_stat); $i++)
		    {
		        $_today = $sea_lion_grade_stat[$i]["today"];
		        $_web_paymoney = $sea_lion_grade_stat[$i]["web_paymoney"];
		        $_ios_paymoney = $sea_lion_grade_stat[$i]["ios_paymoney"];
		        $_and_paymoney = $sea_lion_grade_stat[$i]["and_paymoney"];
		        $_ama_paymoney = $sea_lion_grade_stat[$i]["ama_paymoney"];
		        $_usercount = $sea_lion_grade_stat[$i]["usercount"];
		
		        echo("['".$_today."'");
		
				if ($_web_paymoney != "")
					echo(",{v:".$_web_paymoney.",f:'".number_format($_web_paymoney)."'}");
				else
					echo(",0");
		
				if ($_ios_paymoney != "")
					echo(",{v:".$_ios_paymoney.",f:'".number_format($_ios_paymoney)."'}");
				else
					echo(",0");
		
				if ($_and_paymoney != "")
					echo(",{v:".$_and_paymoney.",f:'".number_format($_and_paymoney)."'}");
				else
					echo(",0");
				
				if ($_ama_paymoney != "")
					echo(",{v:".$_ama_paymoney.",f:'".number_format($_ama_paymoney)."'}");
				else
					echo(",0");
		
				if ($_usercount != "")
					echo(",{v:".$_usercount.",f:'".number_format($_usercount)."'}]");
				else
					echo(",0]");
		        
		        if ($i < sizeof($killer_whale_grade_stat))
		        	echo(",");
		        
		    }
?>
		]);
		
		var data4 = google.visualization.arrayToDataTable([
		['날짜', 'Web', 'iOS', 'And', 'Ama', 'Usercount'],
<?
		    for ($i=0; $i<sizeof($dolphin_grade_stat); $i++)
		    {
		        $_today = $dolphin_grade_stat[$i]["today"];
		        $_web_paymoney = $dolphin_grade_stat[$i]["web_paymoney"];
		        $_ios_paymoney = $dolphin_grade_stat[$i]["ios_paymoney"];
		        $_and_paymoney = $dolphin_grade_stat[$i]["and_paymoney"];
		        $_ama_paymoney = $dolphin_grade_stat[$i]["ama_paymoney"];
		        $_usercount = $dolphin_grade_stat[$i]["usercount"];
		
		        echo("['".$_today."'");
		
				if ($_web_paymoney != "")
					echo(",{v:".$_web_paymoney.",f:'".number_format($_web_paymoney)."'}");
				else
					echo(",0");
		
				if ($_ios_paymoney != "")
					echo(",{v:".$_ios_paymoney.",f:'".number_format($_ios_paymoney)."'}");
				else
					echo(",0");
		
				if ($_and_paymoney != "")
					echo(",{v:".$_and_paymoney.",f:'".number_format($_and_paymoney)."'}");
				else
					echo(",0");
				
				if ($_ama_paymoney != "")
					echo(",{v:".$_ama_paymoney.",f:'".number_format($_ama_paymoney)."'}");
				else
					echo(",0");
		
				if ($_usercount != "")
					echo(",{v:".$_usercount.",f:'".number_format($_usercount)."'}]");
				else
					echo(",0]");
		        
		        if ($i < sizeof($killer_whale_grade_stat))
		        	echo(",");
		        
		    }
?>
		]);
		
		var data5 = google.visualization.arrayToDataTable([
		['날짜', 'Web', 'iOS', 'And', 'Ama', 'Usercount'],
<?
		    for ($i=0; $i<sizeof($minnow_whale_grade_stat); $i++)
		    {
		        $_today = $minnow_whale_grade_stat[$i]["today"];
		        $_web_paymoney = $minnow_whale_grade_stat[$i]["web_paymoney"];
		        $_ios_paymoney = $minnow_whale_grade_stat[$i]["ios_paymoney"];
		        $_and_paymoney = $minnow_whale_grade_stat[$i]["and_paymoney"];
		        $_ama_paymoney = $minnow_whale_grade_stat[$i]["ama_paymoney"];
		        $_usercount = $minnow_whale_grade_stat[$i]["usercount"];
		
		        echo("['".$_today."'");
		
				if ($_web_paymoney != "")
					echo(",{v:".$_web_paymoney.",f:'".number_format($_web_paymoney)."'}");
				else
					echo(",0");
		
				if ($_ios_paymoney != "")
					echo(",{v:".$_ios_paymoney.",f:'".number_format($_ios_paymoney)."'}");
				else
					echo(",0");
		
				if ($_and_paymoney != "")
					echo(",{v:".$_and_paymoney.",f:'".number_format($_and_paymoney)."'}");
				else
					echo(",0");
				
				if ($_ama_paymoney != "")
					echo(",{v:".$_ama_paymoney.",f:'".number_format($_ama_paymoney)."'}");
				else
					echo(",0");
		
				if ($_usercount != "")
					echo(",{v:".$_usercount.",f:'".number_format($_usercount)."'}]");
				else
					echo(",0]");
		        
		        if ($i < sizeof($killer_whale_grade_stat))
		        	echo(",");
		        
		    }
?>
		]);

		var options = {
			width:1100,                         
    	    height:500,
    	    isStacked: 'true',
			seriesType: 'steppedArea',
			series: 
			{
				4: {
					type: 'line',
					targetAxisIndex:1
				}
			},
			fontSize : 12,
			colors:['#D2691E','gray','#147814','#003399','red']
		};

        var chart1 = new google.visualization.ComboChart(document.getElementById('chart_div1'));
        var chart2 = new google.visualization.ComboChart(document.getElementById('chart_div2'));
        var chart3 = new google.visualization.ComboChart(document.getElementById('chart_div3'));
        var chart4 = new google.visualization.ComboChart(document.getElementById('chart_div4'));
        var chart5 = new google.visualization.ComboChart(document.getElementById('chart_div5'));
        
	    chart1.draw(data1, options);
	    chart2.draw(data2, options);
	    chart3.draw(data3, options);
	    chart4.draw(data4, options);
	    chart5.draw(data5, options);
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 결제자 세그먼트 현황 </div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<div style="float: right; margin-right: 2px">
					날짜 : &nbsp;&nbsp;
					<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" />~
					<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" />
					<input type="button" class="btn_search" value="검색" onclick="search()" />
				</div>
			</div>
		</form>
	</div>
	<!-- //title_warp -->
	<div class="h2_title mt30" name="div_normal">
		Killer Whale (28일 결제 금액 $499$)
	</div>
	<div id="chart_div1" name="div_normal" style="height:500px; min-width: 1000px"></div>
	
	<div class="h2_title mt30" name="div_normal">
		Whale (28일 결제 금액 $199 ~ $499)
	</div>
	<div id="chart_div2" name="div_normal" style="height:500px; min-width: 1000px"></div>
	
	<div class="h2_title mt30" name="div_normal">
		Sea Lion (28일 결제 금액 $59 ~ $199)
	</div>
	<div id="chart_div3" name="div_normal" style="height:500px; min-width: 1000px"></div>
	
	<div class="h2_title mt30" name="div_normal">
		Dolphin (28일 결제 금액 $9 ~ $59)
	</div>
	<div id="chart_div4" name="div_normal" style="height:500px; min-width: 1000px"></div>
	
	<div class="h2_title mt30" name="div_normal">
		Minnow (28일 결제 금액 $0 ~ $9)
	</div>
	<div id="chart_div5" name="div_normal" style="height:500px; min-width: 1000px"></div>
</div>
        
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>