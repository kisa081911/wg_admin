<?
	$top_menu = "pay_static";
	$sub_menu = "pay_4week_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
	
	//오늘 날짜 정보
	$today = get_past_date(date("Y-m-d"),1,"d");
	$before_day = get_past_date($today,40,"d");
	
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT today, ROUND(SUM(total_money)) AS total_money, (SUM(total_coin) / SUM(total_basecoin) - 1) * 100 AS total_salerate ".
			"FROM `tbl_order_4week_daily` ".
			"WHERE '$startdate' <= today AND today <= '$enddate' GROUP BY today";
	
	$order_4week_info = $db_analysis->gettotallist($sql);
	
	$total_money_list = array();
	$money_rate_list = array();
	$date_list = array();
	
	$list_pointer = sizeof($order_4week_info);
	
	$date_pointer = $enddate;
	
	for ($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
	{
		$today = $order_4week_info[$list_pointer-1]["today"];
		
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$static = $order_4week_info[$list_pointer-1];
		
			$total_money_list[$i] = $static["total_money"];	
			$money_rate_list[$i] = ($static["total_salerate"] == "")? 0 : $static["total_salerate"];
			$date_list[$i] = $date_pointer;
	
			$list_pointer--;
		}
		else
		{
			$total_money_list[$i] = 0;
			$money_rate_list[$i] = 0;
			$date_list[$i] = $date_pointer;
		}
			
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}

	$sql = "select today, ROUND(sum(web)) as web , ROUND(sum(ios)) as ios, ROUND(sum(android)) as android, ROUND(sum(amazon)) as amazon ".
			"from	".
			"(	".
			"	SELECT today, if(platform = 0, sum(total_money), 0) as web, if(platform = 1, ".
			"	sum(total_money), 0) as ios, if(platform = 2, sum(total_money), 0) as android, if(platform = 3, sum(total_money), 0) as amazon	".
			"	FROM `tbl_order_4week_daily`	".
			"	WHERE '$startdate' <= today AND today <= '$enddate' group by today, platform	".
			")tt  GROUP BY today ";
	
	$order_4week_info = $db_analysis->gettotallist($sql);
	
	$total_web_list = array();
	$total_ios_list = array();
	$total_android_list = array();
	$total_amazon_list = array();
	$date_list = array();
	
	$list_pointer = sizeof($order_4week_info);
	
	$date_pointer = $enddate;
	
	for ($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
	{
		$today = $order_4week_info[$list_pointer-1]["today"];
		
		if (get_diff_date($date_pointer, $today, "d") == 0)
		{
			$static = $order_4week_info[$list_pointer-1];
			
			$total_web_list[$i] = $static["web"];
			$total_ios_list[$i] = $static["ios"];
			$total_android_list[$i] = $static["android"];
			$total_amazon_list[$i] = $static["amazon"];
			$date_list[$i] = $date_pointer;
			
			$list_pointer--;
		}
		else
		{
			$total_web_list[$i] = 0;
			$total_ios_list[$i] = 0;
			$total_android_list[$i] = 0;
			$total_amazon_list[$i] = 0;
			$date_list[$i] = $date_pointer;
		}
			
		$date_pointer = get_past_date($date_pointer, 1, "d");
	}
	
	$sql = "select today,
					ROUND(sum(facebook)) as facebook,
					ROUND(sum(ios)) as ios,
					ROUND(sum(android)) as android,
					ROUND(sum(amazon)) as amazon,
					sum(facebook_salerate) * 100 as facebook_salerate,
					sum(ios_salerate) * 100 as ios_salerate,
					sum(android_salerate) * 100 as android_salerate,
					sum(amazon_salerate) * 100 as amazon_salerate
			from
			(
				SELECT today,
					if(platform = 0, sum(total_money), 0) as facebook,
					if(platform = 1, sum(total_money), 0) as ios,
					if(platform = 2, sum(total_money), 0) as android,
					if(platform = 3, sum(total_money), 0) as amazon,
					if(platform = 0, SUM(total_coin) / SUM(total_basecoin) - 1, 0) as facebook_salerate,
					if(platform = 1, SUM(total_coin) / SUM(total_basecoin) - 1 , 0) as ios_salerate,
					if(platform = 2, SUM(total_coin) / SUM(total_basecoin) - 1, 0) as android_salerate,
					if(platform = 3, SUM(total_coin) / SUM(total_basecoin) - 1, 0) as amazon_salerate
				FROM tbl_order_4week_daily
				WHERE '$startdate' <= today AND today <= '$enddate' group by today, platform
			)tt  GROUP BY today ";
	$order_4week_info = $db_analysis->gettotallist($sql);
	
	$total_facebook_list = array();
	$total_ios_list = array();
	$total_android_list = array();
	$total_amazon_list = array();
	$date_list = array();
	
	$facebook_salerate_list = array();
	$ios_salerate_list = array();
	$android_salerate_list = array();
	$amazon_salerate_list = array();
	
	$list_pointer = sizeof($order_4week_info);
	
	$date_pointer = $enddate;
	
	for ($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
	{
	    $today = $order_4week_info[$list_pointer-1]["today"];
	    
	    if (get_diff_date($date_pointer, $today, "d") == 0)
	    {
	        $static = $order_4week_info[$list_pointer-1];
	        
	        $total_facebook_list[$i] = $static["facebook"];
	        $total_ios_list[$i] = $static["ios"];
	        $total_android_list[$i] = $static["android"];
	        $total_amazon_list[$i] = $static["amazon"];
	        
	        $facebook_salerate_list[$i] = $static["facebook_salerate"];
	        $ios_salerate_list[$i] = $static["ios_salerate"];
	        $android_salerate_list[$i] = $static["android_salerate"];
	        $amazon_salerate_list[$i] = $static["amazon_salerate"];
	        
	        $date_list[$i] = $date_pointer;
	        
	        $list_pointer--;
	    }
	    else
	    {
	        $total_facebook_list[$i] = 0;
	        $total_ios_list[$i] = 0;
	        $total_android_list[$i] = 0;
	        $total_amazon_list[$i] = 0;
	        
	        $facebook_salerate_list[$i] = 0;
	        $ios_salerate_list[$i] = 0;
	        $android_salerate_list[$i] = 0;
	        $amazon_salerate_list[$i] = 0;
	        
	        $date_list[$i] = $date_pointer;
	    }
	    
	    $date_pointer = get_past_date($date_pointer, 1, "d");
	}
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

	google.load("visualization", "1", {packages:["corechart"]});

	function drawChart() 
    {
        var data1 = new google.visualization.DataTable();
        data1.addColumn('string', '날짜');
        data1.addColumn('number', '총합');
        data1.addRows([
<?
			for ($i=sizeof($date_list); $i>0; $i--)
			{
				$_total_money = ($total_money_list[$i-1]);
		        $_date = $date_list[$i-1];
				
				echo("['".$_date."',".$_total_money."]");
				
				if ($i > 1)
					echo(",");
			}
?>
        ]);

        var data2 = new google.visualization.DataTable();
        data2.addColumn('string', '날짜');
        data2.addColumn('number', 'Web');
        data2.addColumn('number', 'Ios');
        data2.addColumn('number', 'Android');
        data2.addColumn('number', 'Amazon');
        data2.addRows([
<?
			for ($i=sizeof($date_list); $i>0; $i--)
			{
				$_total_web_money = ($total_web_list[$i-1]);
				$_total_ios_money = ($total_ios_list[$i-1]);
				$_total_android_money = ($total_android_list[$i-1]);
				$_total_amazon_money = ($total_amazon_list[$i-1]);
		        $_date = $date_list[$i-1];
				
				echo("['".$_date."',".$_total_web_money.",".$_total_ios_money.",".$_total_android_money.",".$_total_amazon_money."]");
				
				if ($i > 1)
					echo(",");
			}
?>
        ]);


        var data3 = new google.visualization.DataTable();
        data3.addColumn('string', '날짜');
        data3.addColumn('number', '할인율(%)');
        data3.addRows([
<?
			for ($i=sizeof($date_list); $i>0; $i--)
			{
				$_money_salerate = ($money_rate_list[$i-1]);
		        $_date = $date_list[$i-1];
				
				echo("['".$_date."',".$_money_salerate."]");
				
				if ($i > 1)
					echo(",");
			}
?>
        ]);

        var data4 = new google.visualization.DataTable();
        data4.addColumn('string', '날짜');
        data4.addColumn('number', 'Facebook');
        data4.addColumn('number', 'iOS');
        data4.addColumn('number', 'Android');
        data4.addColumn('number', 'Amazon');
        data4.addRows([
<?
			for ($i=sizeof($date_list); $i>0; $i--)
			{
				$_facebook_salerate_money = ($facebook_salerate_list[$i-1]);
				$_ios_salerate_money = ($ios_salerate_list[$i-1]);
				$_android_salerate_money = ($android_salerate_list[$i-1]);
				$_amazon_salerate_money = ($amazon_salerate_list[$i-1]);
		        $_date = $date_list[$i-1];
				
				echo("['".$_date."',".$_facebook_salerate_money.",".$_ios_salerate_money.",".$_android_salerate_money.",".$_amazon_salerate_money."]");
				
				if ($i > 1)
					echo(",");
			}
?>
        ]);

        var options = {
			width:1050,                         
			height:400,
			axisTitlesPosition:'in',
			curveType:'none',
			focusTarget:'category',
			interpolateNulls:'true',
			legend:'top',
			fontSize : 12,            
			chartArea:{left:100,top:40,width:1000,height:300}
		};

		var chart1 = new google.visualization.LineChart(document.getElementById('chart_div1'));
		chart1.draw(data1, options);

		var chart2 = new google.visualization.LineChart(document.getElementById('chart_div2'));
		chart2.draw(data2, options);

		var chart3 = new google.visualization.LineChart(document.getElementById('chart_div3'));
		chart3.draw(data3, options);

		var chart4 = new google.visualization.LineChart(document.getElementById('chart_div4'));
		chart4.draw(data4, options);
	}
        
	google.setOnLoadCallback(drawChart);

	function search_press(e)
	{
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
    	    search();
        }
    }

    function search()
    {
    	var search_form = document.search_form;

    	search_form.submit();
    }
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">

<!-- title_warp -->
<div class="title_wrap">
	<form name="search_form" id="search_form"  method="get" action="pay_4week_stats.php">
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 최근 28일 결제</div>
			<div class="search_box"> 
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />			
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>		
		</div>
	</form>	
</div>
<!-- //title_warp -->
    	
<div class="search_result">
	<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다
</div>
	<div class="h2_title" style="font-size: 18px;">1. 최근 28일 결제 금액</div>
	<div class="h2_title">[Total]</div>
	<div id="chart_div1" style="height:420px; min-width: 500px"></div>
	
	<div class="h2_title">[OS]</div>
	<div id="chart_div2" style="height:420px; min-width: 500px"></div>
	
	<br/><br/>
	<div class="h2_title" style="font-size: 18px;">2. 최근 28일 결제 할인율</div>
	<br/>
	<div class="h2_title">[Total]</div>
	<div id="chart_div3" style="height:420px; min-width: 500px"></div>
	
	<div class="h2_title">[OS]</div>
	<div id="chart_div4" style="height:420px; min-width: 500px"></div>        
</div>
<!--  //CONTENTS WRAP -->
        	
<div class="clear"></div>
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
