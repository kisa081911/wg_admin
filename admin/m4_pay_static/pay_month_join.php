<?
	$top_menu = "pay_static";
	$sub_menu = "pay_month_join";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_start_year = $_GET["search_start_year"];
	$search_start_month = $_GET["search_start_month"];
	$search_end_year = $_GET["search_end_year"];
	$search_end_month = $_GET["search_end_month"];	
	$search_plaform = ($_GET["search_platform"] == "") ? 0 : $_GET["search_platform"];
	
	if($search_start_year == "" && $search_start_month == "")
	{
		$search_start_year = date("Y", strtotime("-6 month"));
		$search_start_month = date("m", strtotime("-6 month"));
	}
	
	if($search_end_year == "" && $search_end_month == "")
	{
		$search_end_year = date("Y");
		$search_end_month = date("m");
	}
	
	$search_start_createdate = $search_start_year."-".$search_start_month;
	$search_end_createdate = $search_end_year."-".$search_end_month;
	
	$db_other = new CDatabase_Other();
	
	if($search_plaform == 0)
	{	
		$sql = "SELECT pay_date FROM tbl_user_month_purchase_stats ".
				" WHERE pay_date BETWEEN '$search_start_createdate' AND '$search_end_createdate' GROUP BY pay_date";		
		$pay_date_max = $db_other->gettotallist($sql);
		
		$sql = "SELECT join_date FROM tbl_user_month_purchase_stats  GROUP BY join_date";
		$join_date_max = $db_other->gettotallist($sql);
		
		$sql_statment = "tbl_user_month_purchase_stats";
		$plaform_name = "Web";
	}
	else if($search_plaform == 1)
	{	
		$sql = "SELECT pay_date FROM tbl_user_month_purchase_stats_ios ".
				" WHERE pay_date BETWEEN '$search_start_createdate' AND '$search_end_createdate' GROUP BY pay_date";
		$pay_date_max = $db_other->gettotallist($sql);
		
		$sql = "SELECT join_date FROM tbl_user_month_purchase_stats_ios  GROUP BY join_date";
		$join_date_max = $db_other->gettotallist($sql);
		
		$sql_statment = "tbl_user_month_purchase_stats_ios";
		$plaform_name = "IOS";
	}
	else if($search_plaform == 2)
	{	
		$sql = "SELECT pay_date FROM tbl_user_month_purchase_stats_android ".
				" WHERE pay_date BETWEEN '$search_start_createdate' AND '$search_end_createdate' GROUP BY pay_date";
		$pay_date_max = $db_other->gettotallist($sql);
	
		$sql = "SELECT join_date FROM tbl_user_month_purchase_stats_android  GROUP BY join_date";
		$join_date_max = $db_other->gettotallist($sql);
		
		$sql_statment = "tbl_user_month_purchase_stats_android";
		$plaform_name = "Android";
	}
	else if($search_plaform == 3)
	{	
		$sql = "SELECT pay_date FROM tbl_user_month_purchase_stats_amazon ".
				" WHERE pay_date BETWEEN '$search_start_createdate' AND '$search_end_createdate' GROUP BY pay_date";
		$pay_date_max = $db_other->gettotallist($sql);
	
		$sql = "SELECT join_date FROM tbl_user_month_purchase_stats_amazon  GROUP BY join_date";
		$join_date_max = $db_other->gettotallist($sql);
		
		$sql_statment = "tbl_user_month_purchase_stats_amazon";
		$plaform_name = "Amazon";
	}
	
	$total_credit = 0;
	
	$data_list[0][0] = "Date";
	
	for($i = 0; $i<sizeof($join_date_max); $i++)
	{
		$data_list[0][$i+1] = $join_date_max[$i]["join_date"];
	}	
		
	
	for($i=0; $i<sizeof($pay_date_max); $i++)
	{
		$pay_date = $pay_date_max[$i]["pay_date"];
		
		$data_list[$i+1][0] = $pay_date;
		
		for($j=0; $j<sizeof($join_date_max); $j++)
		{
			$join_date = $join_date_max[$j]["join_date"];
			
			$sql = "SELECT total_credit FROM $sql_statment WHERE pay_date = '$pay_date' AND join_date = '$join_date'";
			$total_credit = $db_other->getvalue($sql);
			
			if($total_credit > 0)
				$data_list[$i+1][$j+1] = $total_credit;
			else
				$data_list[$i+1][$j+1] = 0;
		}
	}
		
	$db_other->end();	
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">

	google.load("visualization", "1", {packages:["corechart"]});
	
	function drawChart() 
	{
		
 		 var data = google.visualization.arrayToDataTable([
<?
			for($i = 0; $i < sizeof($data_list); $i++)
			{				
				echo "['";
				for($j = 0; $j < sizeof($data_list[$i]); $j++)
				{
					if($i == 0 && $j+1 == sizeof($data_list[$i]))
						echo $data_list[$i][$j]."'";
					else if($j+1 == sizeof($data_list[$i]))
						echo $data_list[$i][$j];
					else if($i == 0)
						echo $data_list[$i][$j]."','";
					else if($j == 0)						
						echo $data_list[$i][$j]."',";
					else 
						echo $data_list[$i][$j].",";				

					if($j+1 == sizeof($data_list[$i]))
					 	echo "],";
				}
			}
?>
 		 ]);

		 var options = {
	        legend: { position: 'top'},
	        isStacked: true,
	        chartArea:{left:80,top:40,width:1020,height:500}
	      };

		 var chart = new google.visualization.ColumnChart(document.getElementById('chart_data0'));
		 chart.draw(data, options);
		                                           		
	}

	google.setOnLoadCallback(drawChart);
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="pay_month_join.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 월별결제 가입자 분포 </div>
			<div class="search_box">
			플랫폼 : &nbsp;
			<select name="search_platform" id="search_platform">
				<option value = "0" <?= ($search_plaform == 0) ? "selected" : "" ?>>Web</option>
				<option value = "1" <?= ($search_plaform == 1) ? "selected" : "" ?>>Ios</option>
				<option value = "2" <?= ($search_plaform == 2) ? "selected" : "" ?>>Android</option>
				<option value = "3" <?= ($search_plaform == 3) ? "selected" : "" ?>>Amazon</option>
			</select>	
			&nbsp;&nbsp;&nbsp;			
			<select name="search_start_year" id="search_start_year">
			 <?for ($i=2014; $i <= 2020; $i++ ) {?>
				<option value="<?=$i?>" <?= ($search_start_year == $i) ? "selected" : "" ?>><?=$i?></option>				
			<?}?>
			</select>년
			<select name="search_start_month" id="search_start_month">
			<?for( $i = 1; $i <= 12; $i++ ) { 
				$month_start_num = str_pad( $i, 2, 0, STR_PAD_LEFT );
			?>			
				<option value="<?=$month_start_num?>" <?= ($search_start_month == $month_start_num) ? "selected" : "" ?>><?=$month_start_num?></option>			
			<?}?>
			</select>월				
			 ~
			<select name="search_end_year" id="search_end_year">
			<?for ($i=2014; $i <= 2020; $i++ ) {?>
				<option value="<?=$i?>" <?= ($search_end_year == $i) ? "selected" : "" ?>><?=$i?></option>				
			<?}?>
			</select>년
			<select name="search_end_month" id="search_end_month">
			<?for( $i = 1; $i <= 12; $i++ ) { 
				$month_end_num = str_pad( $i, 2, 0, STR_PAD_LEFT );
			?>			
				<option value="<?=$month_end_num?>" <?= ($search_end_month == $month_end_num) ? "selected" : "" ?>><?=$month_end_num?></option>			
			<?}?>
			</select>월
			
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
     	<div class="h2_title">[ <?= $plaform_name ?> 월별결제 가입자 분포 ]</div>
	 	<div id="chart_data0" style="height:700px; min-width: 500px"></div>	       
    </form>	
</div>
<!--  //CONTENTS WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
