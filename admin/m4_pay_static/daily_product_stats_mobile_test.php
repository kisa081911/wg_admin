<?
	$top_menu = "pay_static";
	$sub_menu = "daily_product_stats_mobile";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$today = date("Y-m-d");
	
	$search_start_createdate = $_GET["start_createdate"];
	$search_end_createdate = $_GET["end_createdate"];
	
	if($search_start_createdate == "")
		$search_start_createdate = date("Y-m-d", strtotime("-4 day"));
	
	if($search_end_createdate == "")
		$search_end_createdate = $today;
	
	$tab = ($_GET["tab"] == "") ? "0" : $_GET["tab"];
	
	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();
	
	$std_useridx = 20000;
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
	{
		$port = ":8081";
		$std_useridx = 10000;
	}
	
	if($tab == 0) 
	{
		$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, ".
				"	SUM(facebookcredit) AS sum_total_credit, ".
				"	SUM(IF(special_discount = 0 AND special_more = 0 AND couponidx = 0, facebookcredit, 0)) AS sum_basic_credit, ".
				"	SUM(IF(special_discount != 0 OR special_more != 0 AND couponidx = 0, facebookcredit, 0)) AS sum_special_credit, ".
				"	SUM(IF(couponidx != 0, facebookcredit, 0)) AS sum_coupon_credit ".
				"FROM tbl_product_order_mobile ".
				"WHERE useridx > $std_useridx AND status = 1 AND writedate >= '$search_start_createdate 00:00:00' AND writedate <= '$search_end_createdate 23:59:59' ".
				"GROUP BY LEFT(writedate,10) ORDER BY writedate ASC";
		
		$ratelist = $db_main->gettotallist($sql);
		
		$sql = "SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS writedate, ".
				"	COUNT(*) AS sum_total_count, ".
				"	COUNT(IF(special_discount = 0 AND special_more = 0 AND couponidx = 0, 1, NULL)) AS sum_basic_count, ".
				"	COUNT(IF(special_discount != 0 OR special_more != 0 AND couponidx = 0, 1, NULL)) AS sum_special_count, ".
				"	COUNT(IF(couponidx != 0, 1, NULL)) AS sum_coupon_count ".
				"FROM tbl_product_order_mobile ".
				"WHERE useridx > $std_useridx AND status = 1 AND writedate >= '$search_start_createdate 00:00:00' AND writedate <= '$search_end_createdate 23:59:59' ".
				"GROUP BY LEFT(writedate,10) ORDER BY writedate ASC";
		
		$countlist = $db_main->gettotallist($sql);
	}
	else
	{
		$sql = "SELECT productidx, os_type, category, amount, money, imageurl FROM tbl_product_mobile";
		$product_list = $db_main->gettotallist($sql);		

		$sql = "SELECT t1.writedate, day_row_count, t1.os_type, day_total_facebookcredit, productidx, category, total_count, total_facebookcredit, total_coin 
				FROM ( 
					SELECT writedate, os_type, productidx, category, SUM(total_count) AS total_count, SUM(total_money) AS total_facebookcredit, SUM(total_coin) AS total_coin 
					FROM tbl_product_stat_os_mobile_daily 
					WHERE TYPE=1 AND writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate' GROUP BY writedate, productidx, category, os_type 
				) t1 LEFT JOIN ( 
					SELECT writedate, os_type, SUM(total_money) AS day_total_facebookcredit, COUNT(DISTINCT productidx) AS day_row_count 
					FROM tbl_product_stat_os_mobile_daily 
					WHERE TYPE=1 AND writedate BETWEEN '$search_start_createdate' AND '$search_end_createdate'
					GROUP BY writedate, os_type 
				) t2 ON t1.writedate = t2.writedate AND  t1.os_type = t2.os_type
				ORDER BY writedate DESC, t1.os_type ASC, total_facebookcredit DESC";
		$product_data = $db_analysis->gettotallist($sql);
	}
?>

<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
<?
	if($tab == 0)
	{
?>
		google.load("visualization", "1", {packages:["corechart"]});
		google.setOnLoadCallback(drawChart);

		function drawChart()
		{
			var data1 = google.visualization.arrayToDataTable([
				['날짜','정상구매', '할인구매', '쿠폰구매'],
<?
				for($i=0; $i<sizeof($ratelist); $i++)
				{
					
					$writedate = $ratelist[$i]["writedate"];
					$total_facebookcredit = $ratelist[$i]["sum_total_credit"];
					$basic_facebookcredit = $ratelist[$i]["sum_basic_credit"];
					$special_facebookcredit = $ratelist[$i]["sum_special_credit"];
					$coupon_facebookcredit = $ratelist[$i]["sum_coupon_credit"];
					
					$basic_rate = $total_facebookcredit == 0 ? 0 : round($basic_facebookcredit/$total_facebookcredit * 100, 1);
					$special_rate = $total_facebookcredit == 0 ? 0 : round($special_facebookcredit/$total_facebookcredit * 100, 1);
					$coupon_rate = $total_facebookcredit == 0 ? 0 : round($coupon_facebookcredit/$total_facebookcredit * 100, 1);
					
					if($i == sizeof($ratelist))
					{
						echo "['$writedate', ".$basic_rate.",  ".$special_rate.", ".$coupon_rate."]";
					}
					else
					{
						echo "['$writedate', ".$basic_rate.",  ".$special_rate.", ".$coupon_rate."],";
					}
				}
?>
			]);

			var data2 = google.visualization.arrayToDataTable([
				['날짜','정상구매', '할인구매', '쿠폰구매'],
<?
				for($i=0; $i<sizeof($countlist); $i++)
				{
					$writedate = $countlist[$i]["writedate"];
					$total_count = $countlist[$i]["sum_total_count"];
					$basic_count = $countlist[$i]["sum_basic_count"];
					$special_count = $countlist[$i]["sum_special_count"];
					$coupon_count = $countlist[$i]["sum_coupon_count"];
						
					if($i == sizeof($countlist))
					{
						echo "['$writedate', ".$basic_count.",  ".$special_count.", ".$coupon_count."]";
					}
					else
					{
						echo "['$writedate', ".$basic_count.",  ".$special_count.", ".$coupon_count."],";
					}					
				}
?>
			]);
			
			var options1 = {
				    title: '1.구매비율 현황',
				    hAxis: {title: '', titleTextStyle: {color: 'black'}},
				    vAxis: {title: '%', titleTextStyle: {color: 'black'}}
				  };
			  
			var options2 = {
				    title: '2.구매건수 현황',
				    hAxis: {title: '', titleTextStyle: {color: 'black'}},
				    vAxis: {title: '건수', titleTextStyle: {color: 'black'}}
				  };
			
			var chart1 = new google.visualization.ColumnChart(document.getElementById('chart1_div'));
			var chart2 = new google.visualization.ColumnChart(document.getElementById('chart2_div'));

			chart1.draw(data1, options1);
			chart2.draw(data2, options2);
		}
<?
	}
?>
	function tab_change(tab)
	{
		var search_form = document.search_form;
		search_form.tab.value = tab;
		search_form.submit();
	}

	$(function() {
	    $("#start_createdate").datepicker({ });
	});

	$(function() {
	    $("#end_createdate").datepicker({ });
	});
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	
	<form name="search_form" id="search_form"  method="get" action="daily_product_stats_mobile.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 일별 상품 통계</div>
		<div class="search_box">
			<input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			<input type=hidden name="tab" value="<?= $tab ?>">
			<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
		</div>
	</div>
	<!-- //title_warp -->
	<div class="search_result">
		<span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span> 통계입니다
	</div>
	
	<ul class="tab">
		<li id="tab_1" class="<?= ($tab == "0") ? "select" : "" ?>" style="width:50px;text-align:center;font-size:11px;" onclick="tab_change('0')">그래프</li>
		<li id="tab_2" class="<?= ($tab == "1") ? "select" : "" ?>" style="width:50px;text-align:center;font-size:11px;" onclick="tab_change('1')">상세보기</li>
	</ul>
	
<?
	if($tab == 0)
	{
?>
		<div id="chart1_div" style="width: 1100px; height: 500px;"></div>
		<div id="chart2_div" style="width: 1100px; height: 500px;"></div>
<?
	}
	else 
	{
?> 
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">OS</th>
					<th class="tdc">상품명</th>
					<th class="tdc">구매 수</th>
					<th class="tdc">구매 금액</th>
					<th class="tdc">구매 Coin</th>
					<th class="tdc">구매비율</th>
				</tr>
			</thead>
			<tbody>
<?
			$tmp_row_count = 1;
			
			$day_total_count = 0;
			$day_total_facebookcredit = 0;
			
			$all_total_count = 0;
			$all_total_facebookcredit = 0;
			
			$before_writedate = "";
			$before_os_type = "";
			
			for($i=0; $i<sizeof($product_data); $i++)
			{
				$row_count = $product_data[$i]["day_row_count"];
				$writedate = $product_data[$i]["writedate"];
				$os_type = $product_data[$i]["os_type"];
				$productidx = $product_data[$i]["productidx"];
				$total_count = $product_data[$i]["total_count"];
				$total_facebookcredit = round($product_data[$i]["total_facebookcredit"]/10, 1);
				$total_coin = $product_data[$i]["total_coin"];
				$total_purchase_rate = ($product_data[$i]["day_total_facebookcredit"] == 0) ? 0 : round(($product_data[$i]["total_facebookcredit"] / $product_data[$i]["day_total_facebookcredit"] * 100), 2);
				
				$day_total_count += $total_count;
				$day_total_facebookcredit += $total_facebookcredit;
				$day_total_coin += $total_coin;
				
				$all_total_count += $total_count;
				$all_total_facebookcredit += $total_facebookcredit;
				$all_total_coin += $total_coin;
				
				$imageurl = "";
				$productname = "";				
				
				if($before_writedate != $writedate)
					$writedate_row = $db_analysis->getvalue("SELECT COUNT(*) FROM tbl_product_stat_os_mobile_daily WHERE TYPE=1 AND writedate = '$writedate'");	
				
				if($productidx == "100")
				{
					$imageurl = "/images/icon/facebook.png";
					$productname = "Earn";
				}
				else
				{
					for($j=0; $j<sizeof($product_list); $j++)
					{
						if($product_list[$j]["productidx"] == $productidx)
						{
							$imageurl = $product_list[$j]["imageurl"];
							$category = $product_list[$j]["category"];							
							$productname = $product_list[$j]["facebookcredit"];
							
							 if($category == 0)
					         	$productname = $productname."(Basic)";
					         else if($category == 1)
					         	$productname = $productname."(First)";
					         else if($category == 2)
					         	$productname = $productname."(Season)";
					         else if($category == 3)
					         	$productname = $productname."(Special)";
					         else if($category == 4)
					         	$productname = $productname."(PiggyPot)";
					         else if($category == 5)
					         	$productname = $productname."(Season)";
									
							break;
						}
					}
				}
?>
				<tr>
<?
				if($before_writedate != $writedate)
				{
?>
					<td class="tdc point_title" rowspan="<?= $writedate_row ?>"><?= $writedate ?></td>
					
<?
					$tmp_row_count++;
				}	

				if($tmp_row_count == 2 || $before_os_type != $os_type)
				{
?>
					<td class="tdc" rowspan="<?= $row_count ?>"><?= $os_type ?></td>
<?
				}
?>
					<td class="tdc point"><img src="<?=$imageurl?>" align="absmiddle" width="25" height="25"> <?= $productname ?></td>
					<td class="tdc"><?= number_format($total_count) ?></td>
					<td class="tdc">$<?= number_format($total_facebookcredit, 1) ?></td>
					<td class="tdc"><?= number_format($total_coin) ?></td>
					<td class="tdc"><?= number_format($total_purchase_rate, 2) ?>%</td>
				</tr>
<?
				if($tmp_row_count ==100)
				{
?>
					<tr>
						<td class="tdc point">Total</td>
						<td class="tdc point"><?= number_format($day_total_count) ?></td>
						<td class="tdc point">$<?= number_format($day_total_facebookcredit, 1) ?></td>
						<td class="tdc point"><?= number_format($day_total_coin) ?></td>
						<td class="tdc point">100%</td>
					</tr>
<? 
					$day_total_count = 0;
					$day_total_facebookcredit = 0;
					$day_total_coin = 0;
				}
				
				$before_writedate = $writedate;
				$before_os_type = $os_type;
				
				if($before_writedate != $writedate)
					$tmp_row_count == 1;
			}
?>
				<tr>
					<td class="tdc point_title" colspan="2">Total</td>
					<td class="tdc point"><?= number_format($all_total_count) ?></td>
					<td class="tdc point">$<?= number_format($all_total_facebookcredit, 1) ?></td>
					<td class="tdc point"><?= number_format($all_total_coin) ?></td>
					<td class="tdc point"></td>
				</tr>
			</tbody>
		</table>
	</div>
<?
	}
?>
     </form>
</div>
<!--  //CONTENTS WRAP -->
<?
	$db_main->end();
	$db_analysis->end();

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>