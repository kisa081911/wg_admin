<?
    $top_menu = "pay_static";
    $sub_menu = "pay_month";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	$search_start_year = $_GET["search_start_year"];
    $search_start_month = $_GET["search_start_month"];
    $search_end_year = $_GET["search_end_year"];
    $search_end_month = $_GET["search_end_month"];
	
	if($search_start_year == "" && $search_start_month == "")
    {
    	$search_start = date("Y-m", strtotime("-11 month"));
		$search_start_year = substr($search_start, 0, 4);  
		$search_start_month = substr($search_start, -2);
    }
    
    if($search_end_year == "" && $search_end_month == "")
    {
    	$search_end_year = date("Y");
    	$search_end_month = date("m");
    }	
		
	$size = (($search_end_year-$search_start_year)*12 + ($search_end_month - $search_start_month) + 1);		
	
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
		$std_useridx = 10000;
	else
		$std_useridx = 20000;
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
</script>

        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
        <form name="search_form" id="search_form"  method="get" action="pay_month.php">
            <input type=hidden name=issearch value="1">
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 월별 통계</div>
				<div class="search_box">			
					<select name="search_start_year" id="search_start_year">
			 			<?for ($i=2014; $i <= 2020; $i++ ) {?>
							<option value="<?=$i?>" <?= ($search_start_year == $i) ? "selected" : "" ?>><?=$i?></option>				
						<?}?>
					</select>년
					<select name="search_start_month" id="search_start_month">
						<?for( $i = 1; $i <= 12; $i++ ) { 
							$month_start_num = str_pad( $i, 2, 0, STR_PAD_LEFT );
						?>			
							<option value="<?=$month_start_num?>" <?= ($search_start_month == $month_start_num) ? "selected" : "" ?>><?=$month_start_num?></option>			
						<?}?>
					</select>월				
			 		~
					<select name="search_end_year" id="search_end_year">
						<?for ($i=2014; $i <= 2020; $i++ ) {?>
							<option value="<?=$i?>" <?= ($search_end_year == $i) ? "selected" : "" ?>><?=$i?></option>				
						<?}?>
					</select>년
					<select name="search_end_month" id="search_end_month">
						<?for( $i = 1; $i <= 12; $i++ ) { 
							$month_end_num = str_pad( $i, 2, 0, STR_PAD_LEFT );
						?>			
							<option value="<?=$month_end_num?>" <?= ($search_end_month == $month_end_num) ? "selected" : "" ?>><?=$month_end_num?></option>			
						<?}?>
					</select>월			
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
				</div>
            </div>
            <!-- //title_warp -->
            
            <div class="search_result">
                <span><?= $search_start_year."-".$search_start_month ?></span> ~ <span><?= $search_end_year."-".$search_end_month ?></span> 통계입니다
            </div>
            
            <div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
            </colgroup>
            <thead>
            <tr>
                <th>년월</th>
                <th class="tdr">구매건수</th>
                <th class="tdr">구매액</th>
                <th class="tdr">전월대비</th>
                <th class="tdr">취소건수<br>(결제일기준)</th>
                <th class="tdr">취소금액<br>(결제일기준)</th>
            </tr>
            </thead>
            <tbody>
<?
    $db_main = new CDatabase_Main();
    
	$year = $search_end_year;
	$month = $search_end_month;
	
    for ($i=0; $i<$size; $i++)
    {
    	$nextmonth = $month + 1;
		$nextyear = $year;
		
		if ($nextmonth == 13)
		{
			$nextmonth = 1;
			$nextyear += 1;
		}
		
		$showmonth = $month;
		$shownextmonth = $nextmonth;
		
		if ($showmonth < 10 && $i > 0)
			$showmonth = "0".$showmonth;
		
		if ($shownextmonth < 10)
			$shownextmonth = "0".$shownextmonth;
			
		$date_month = $showmonth;
		
    	$datetail = " useridx>$std_useridx AND '$year-$showmonth-01 00:00:00' <= writedate AND writedate<'$nextyear-$shownextmonth-01 00:00:00' ";		
			
		$prevmonth = $month - 1;
		$prevyear = $year;
		
		if ($prevmonth == 0)
		{
			$prevmonth = 12;
			$prevyear = $prevyear - 1;
		}
		
		$showmonth = $prevmonth;
		$shownextmonth = $month;
		$nextyear = $year;
		
		if ($showmonth < 10)
			$showmonth = "0".$showmonth;
		
		if ($shownextmonth < 10  && $i > 0)
			$shownextmonth = "0".$shownextmonth;
		
    	$prevdatetail = " useridx>$std_useridx AND '$prevyear-$showmonth-01 00:00:00' <=  writedate AND writedate<'$nextyear-$shownextmonth-01 00:00:00' ";		
		
    	$sql = "SELECT COUNT(*) AS totalcount,IFNULL(ROUND(SUM(facebookcredit)),0) AS totalcredit ". 
				"FROM ( ".
				"	SELECT facebookcredit, writedate ".
				"	FROM tbl_product_order ". 
				"	WHERE status IN (1, 3) AND $datetail ".
				"	UNION ALL ".
				"	SELECT ROUND(money/10) AS facebookcredit, writedate ".
				"	FROM tbl_product_order_earn ".
				"	WHERE status=1 AND $datetail ".
				") t1 ".
				"GROUP BY DATE_FORMAT(writedate, '%Y-%m')";
		$data = $db_main->getarray($sql);
		
		$totalcount = $data["totalcount"];
		$totalcredit = $data["totalcredit"];
		
    	$sql = "SELECT COUNT(*) AS totalcount,IFNULL(ROUND(SUM(facebookcredit)),0) AS totalcredit ".
    			"FROM ( ".
    			"	SELECT facebookcredit, writedate ".
    			"	FROM tbl_product_order ".
    			"	WHERE status IN (1, 3) AND $prevdatetail ".
    			"	UNION ALL ".
    			"	SELECT ROUND(money/10) AS facebookcredit, writedate ".
    			"	FROM tbl_product_order_earn ".
    			"	WHERE status=1 AND $prevdatetail ".
    			") t1 ".
    			"GROUP BY DATE_FORMAT(writedate, '%Y-%m')";
		$data = $db_main->getarray($sql);
		
		$prevcount = $data["totalcount"];
		$prevcredit = $data["totalcredit"];
		
    	$sql = "SELECT COUNT(*) AS totalcount,IFNULL(SUM(facebookcredit),0) AS totalcredit FROM tbl_product_order WHERE status=2 AND ".$datetail;
		$data = $db_main->getarray($sql);
		
		$cancelcount = $data["totalcount"];
		$cancelcredit = $data["totalcredit"];
		
		if ($prevcredit == 0)
			$ratio = "N/A";
		else
		{
			$ratio = (round($totalcredit * 1000 / $prevcredit) / 10)."%";
		}
?>
                <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
                    <td class="tdc point"><?= $year ?>년 <?= $date_month ?>월</td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>
                    <td class="tdr point">$<?= number_format(($totalcredit*0.1), 1) ?></td>
                    <td class="tdr point"><?= $ratio ?></td>                   
                    <td class="tdr point"><?= number_format($cancelcount) ?></td>
                    <td class="tdr point">$<?= number_format(($cancelcredit*0.1), 1) ?></td>
                </tr>
<?
		$year = $prevyear;
		$month = $prevmonth;
    }

	$db_main->end();
?>    
            </tbody>
            </table>
        </div>
        
        </form>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>