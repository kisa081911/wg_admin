<?	
	include($_SERVER["DOCUMENT_ROOT"]."/common/common_include.inc.php");
	
	$search_sdate = $_GET["start_date"];
	$search_edate = $_GET["end_date"];
	
	$filename = "pay_nation_".$search_sdate."-".$search_edate.".xls";
	
	if($_GET["start_date"] == "")
		$search_sdate = date("Y-m-d",strtotime("-9 days"));
	if($_GET["end_date"] == "")
		$search_edate = date("Y-m-d");
				
	$db_other = new CDatabase_Other();
	
	$sql = "SELECT * FROM tbl_user_nationstat_daily WHERE '$search_sdate' <= today AND today <= '$search_edate' ORDER BY today DESC, usercnt DESC";
	
	$nationlist = $db_other->gettotallist($sql);
	
	$db_other->end();
	
	if (sizeof($nationlist) == 0)
		error_go("저장할 데이터가 없습니다.", "pay_nation.php");
	
		$excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'>".
				"<table border=1>".
				"<tr>".
				"<td style='font-weight:bold;'>일자</td>".
				"<td style='font-weight:bold;'>국가</td>".
				"<td style='font-weight:bold;'>국가별 유입수</td>".
				"<td style='font-weight:bold;'>일일 유입율</td>".
				"<td style='font-weight:bold;'>국가별 결제자수</td>".
				"<td style='font-weight:bold;'>일일 결제비율</td>".
				"<td style='font-weight:bold;'>일일 결제금액</td>".
				"<td style='font-weight:bold;'>결제 금액비율</td>".
				"<td style='font-weight:bold;'>결제자 평균 결제금액</td>".
				"</tr>";
	
		for ($i=0; $i < sizeof($nationlist); $i++)
		{
			$today = $nationlist[$i]["today"];
	    	$nation = $nationlist[$i]["nation"];
	    	$totalusercnt = $nationlist[$i]["totalusercnt"];
	    	$usercnt = $nationlist[$i]["usercnt"];
	    	
	    	if($usercnt > 0)
	    		$usercntrate = round($usercnt / $totalusercnt * 100, 2);
	    	else-
	    		$usercntrate = 0;
	    	
	    	$totalpurchasercnt = $nationlist[$i]["totalpurchasercnt"];
	    	$purchasercnt = $nationlist[$i]["purchasercnt"];
	    	
	    	if($purchasercnt > 0)
	    		$purchasercntrate = round($purchasercnt / $totalpurchasercnt * 100, 2);
	    	else
	    		$purchasercntrate = 0;
	    	
	    	$totalpurchase = round($nationlist[$i]["totalpurchase"]/10);
	    	$purchase = round($nationlist[$i]["purchase"]/10);
	    	
	    	if($purchase > 0)
	    		$purchaserate = round($purchase / $totalpurchase * 100, 2);
	    	else
	    		$purchaserate = 0;
	    	
	    	if($purchasercnt > 0)
	    		$avrpurchase = round($purchase / $purchasercnt);
	    	else
	    		$avrpurchase = 0;
				
			$excel_contents .= "<tr>";
			
			if($i % 10 == "0")
				$excel_contents .= "<td rowspan='10'>".$today."</td>";
			
			$excel_contents .= "<td>".$nation."</td>".
				"<td>".number_format($usercnt)."</td>".
				"<td>".number_format($usercntrate, 2)."%</td>".
				"<td>".number_format($purchasercnt)."</td>".
				"<td>".number_format($purchasercntrate, 2)."%</td>".
				"<td>$".number_format($purchase, 1)."</td>".
				"<td>".number_format($purchaserate, 2)."%</td>".
				"<td>$".number_format($avrpurchase, 1)."</td>".
				"</tr>";
		}
	
		$excel_contents .= "</table>";
	
		Header("Content-type: application/x-msdownload");
		Header("Content-type: application/x-msexcel");
		Header("Content-Disposition: attachment; filename=$filename");
	
		echo($excel_contents);
?>