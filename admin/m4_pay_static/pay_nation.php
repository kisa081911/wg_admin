<?
	$top_menu = "pay_static";
	$sub_menu = "pay_nation";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
		
	if($_GET["start_date"] == "")
		$search_sdate = date("Y-m-d",strtotime("-9 days"));
	else
		$search_sdate = $_GET["start_date"];
	
	if($_GET["end_date"] == "")
		$search_edate = date("Y-m-d");
	else
		$search_edate = $_GET["end_date"];
	
	$db_other = new CDatabase_Other();

	$sql = "SELECT * FROM tbl_user_nationstat_daily WHERE '$search_sdate' <= today AND today <= '$search_edate' ORDER BY today DESC, usercnt DESC";
	
	$nationlist = $db_other->gettotallist($sql);
	
	$db_other->end();
?>
<link type="text/css" href="/js/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="/js/ui/ui.core.js"></script>
<script type="text/javascript" src="/js/ui/ui.datepicker.js"></script>  
<script type="text/javascript">
	$(function() {
		$("#start_date").datepicker({ });
		$("#end_date").datepicker({ });
	});

	function search()
	{
		var search_form = document.search_form;
	    
		if (search_form.start_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.start_date.focus();
	    	return;
		} 
	
		if (search_form.end_date.value == "")
		{
	    	alert("기준일을 입력하세요.");
	    	search_form.end_date.focus();
	    	return;
		} 
	
		search_form.submit();
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">     
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 국가,일별 통계 </div>
		<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
			<div class="search_box">
				<span class="search_lbl">기준일&nbsp;&nbsp;&nbsp;</span>
				<input type="text" class="search_text" id="start_date" name="start_date" style="width:65px" readonly="readonly" value="<?= $search_sdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/> ~
				<input type="text" class="search_text" id="end_date" name="end_date" style="width:65px" readonly="readonly" value="<?= $search_edate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)"/>
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->	
	<div class="search_result">
		<span><?= $search_sdate ?></span> ~ <span><?= $search_edate ?></span> 현황입니다
		<input type="button" class="btn_03" value="Excel 다운로드" onclick="window.location.href='pay_nation_excel.php?start_date=<?= $search_sdate ?>&end_date=<?= $search_edate ?>'">
	</div>
	
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="50">
			<col width="40">
			<col width="70">
			<col width="70">
			<col width="70">
			<col width="70">
			<col width="70">
			<col width="70">
			<col width="70">
			<col width="70">
		</colgroup>
        <thead>
        	<tr>
        		<th>일자</th>
        		<th>국가</th>
        		<th>국가별유입수</th>
        		<th>일일유입율</th>
        		<th>국가별결제자수</th>
        		<th>일일결제비율</th>
        		<th>일일결제금액</th>
        		<th>결제금액비율</th>
        		<th>결제자평균결제금액</th>
         	</tr>
        </thead>
        <tbody>
<?
    for($i=0; $i<sizeof($nationlist); $i++)
    {
    	$today = $nationlist[$i]["today"];
    	$nation = $nationlist[$i]["nation"];
    	$totalusercnt = $nationlist[$i]["totalusercnt"];
    	$usercnt = $nationlist[$i]["usercnt"];
    	
    	if($usercnt > 0)
    		$usercntrate = round($usercnt / $totalusercnt * 100, 2);
    	else-
    		$usercntrate = 0;
    	
    	$totalpurchasercnt = $nationlist[$i]["totalpurchasercnt"];
    	$purchasercnt = $nationlist[$i]["purchasercnt"];
    	
    	if($purchasercnt > 0)
    		$purchasercntrate = round($purchasercnt / $totalpurchasercnt * 100, 2);
    	else
    		$purchasercntrate = 0;
    	
    	$totalpurchase = round($nationlist[$i]["totalpurchase"]/10);
    	$purchase = round($nationlist[$i]["purchase"]/10);
    	
    	if($purchase > 0)
    		$purchaserate = round($purchase / $totalpurchase * 100, 2);
    	else
    		$purchaserate = 0;
    	
    	if($purchasercnt > 0)
    		$avrpurchase = round($purchase / $purchasercnt);
    	else
    		$avrpurchase = 0;
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
			if($i % 10 == "0")
			{
?>
				<td class="tdc point_title" rowspan="10"><?= $today?></td>
<?
			}
?>
			<td class="tdc point"><?= $nation?></td>
			<td class="tdc"><?= number_format($usercnt)?></td>
			<td class="tdc"><?= number_format($usercntrate, 2)?>%</td>
			<td class="tdc"><?= number_format($purchasercnt)?></td>
			<td class="tdc"><?= number_format($purchasercntrate, 2)?>%</td>
			<td class="tdc">$<?= number_format($purchase, 1)?></td>
			<td class="tdc"><?= number_format($purchaserate, 2)?>%</td>
			<td class="tdc">$<?= number_format($avrpurchase, 1)?></td>
        </tr>
<?
    }
    
    if(sizeof($nationlist) == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="8">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>
	
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>