<?
    $top_menu = "pay_static";
    $sub_menu = "pay_month_mobile";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$search_start_year = $_GET["search_start_year"];
    $search_start_month = $_GET["search_start_month"];
    $search_end_year = $_GET["search_end_year"];
    $search_end_month = $_GET["search_end_month"];
    
    $db_main = new CDatabase_Main();
     
    if($search_start_year == "" && $search_start_month == "")
    {
    	$search_start_year = date("Y", strtotime("-1 year"));
    	$search_start_month = date("m", strtotime("-11 month"));
    }
    
    if($search_end_year == "" && $search_end_month == "")
    {
    	$search_end_year = date("Y");
    	$search_end_month = date("m");
    }
    
    $search_start_createdate = $search_start_year."-".$search_start_month."-01";
    $end_month = $search_end_month + 1;
    $search_end_createdate = $search_end_year."-".$end_month."-01";
    
    $datetail = " useridx>20000 AND '$search_start_createdate 00:00:00' <= writedate AND writedate < '$search_end_createdate 00:00:00' ";
    $ios_datetail = " useridx>20000 AND '$search_start_createdate 00:00:00' <= writedate AND writedate < '$search_end_createdate 00:00:00' AND os_type = 1 ";
    $and_datetail = " useridx>20000 AND '$search_start_createdate 00:00:00' <= writedate AND writedate < '$search_end_createdate 00:00:00' AND os_type = 2 ";
    $ama_datetail = " useridx>20000 AND '$search_start_createdate 00:00:00' <= writedate AND writedate < '$search_end_createdate 00:00:00' AND os_type = 3 ";
    
    $sql = "SELECT LEFT(writedate,7) AS writedate, ".
    		"SUM(IF(STATUS=1, 1, 0)) AS totalcount, ".
    		"SUM(IF(STATUS=1, money, 0)) AS totalcredit, ".
    		"SUM(IF(STATUS=1 AND special_more<>0, 1, 0)) AS specialcount, ".
    		"SUM(IF(STATUS=1 AND special_more<>0, money, 0)) AS specialcredit, ".
    		"SUM(IF(STATUS=2, 1, 0)) AS cancelcount, ".
    		"SUM(IF(STATUS=2, money, 0)) AS cancelcredit ".
    		"FROM tbl_product_order_mobile WHERE ".$datetail."GROUP BY LEFT(writedate,7) ORDER BY writedate DESC;";
    $data = $db_main->gettotallist($sql);
    
    $sql = "SELECT LEFT(writedate,7) AS writedate, ".
    		"SUM(IF(STATUS=1, 1, 0)) AS totalcount, ".
    		"SUM(IF(STATUS=1, money, 0)) AS totalcredit, ".
    		"SUM(IF(STATUS=1 AND special_more<>0, 1, 0)) AS specialcount, ".
    		"SUM(IF(STATUS=1 AND special_more<>0, money, 0)) AS specialcredit, ".
    		"SUM(IF(STATUS=2, 1, 0)) AS cancelcount, ".
    		"SUM(IF(STATUS=2, money, 0)) AS cancelcredit ".
    		"FROM tbl_product_order_mobile WHERE ".$ios_datetail."GROUP BY LEFT(writedate,7) ORDER BY writedate DESC;";
    $ios_data = $db_main->gettotallist($sql);
    
    $sql = "SELECT LEFT(writedate,7) AS writedate, ".
    		"SUM(IF(STATUS=1, 1, 0)) AS totalcount, ".
    		"SUM(IF(STATUS=1, money, 0)) AS totalcredit, ".
    		"SUM(IF(STATUS=1 AND special_more<>0, 1, 0)) AS specialcount, ".
    		"SUM(IF(STATUS=1 AND special_more<>0, money, 0)) AS specialcredit, ".
    		"SUM(IF(STATUS=2, 1, 0)) AS cancelcount, ".
    		"SUM(IF(STATUS=2, money, 0)) AS cancelcredit ".
    		"FROM tbl_product_order_mobile WHERE ".$and_datetail."GROUP BY LEFT(writedate,7) ORDER BY writedate DESC;";
    $and_data = $db_main->gettotallist($sql);
    
    $sql = "SELECT LEFT(writedate,7) AS writedate, ".
    		"SUM(IF(STATUS=1, 1, 0)) AS totalcount, ".
    		"SUM(IF(STATUS=1, money, 0)) AS totalcredit, ".
    		"SUM(IF(STATUS=1 AND special_more<>0, 1, 0)) AS specialcount, ".
    		"SUM(IF(STATUS=1 AND special_more<>0, money, 0)) AS specialcredit, ".
    		"SUM(IF(STATUS=2, 1, 0)) AS cancelcount, ".
    		"SUM(IF(STATUS=2, money, 0)) AS cancelcredit ".
    		"FROM tbl_product_order_mobile WHERE ".$ama_datetail."GROUP BY LEFT(writedate,7) ORDER BY writedate DESC;";
    $ama_data = $db_main->gettotallist($sql);
    
    $prev_start_year = $search_start_year;
    $prev_start_month = $search_start_month - 1;
    
    if($prev_start_month == 0)
    {
    	$prev_start_month = 12;
    	$prev_start_year = $prev_start_year - 1;
    }
    
    $prev_end_year = $search_end_year;
    $prev_end_month = $search_end_month;
    
    if($prev_end_month == 0)
    {
    	$prev_end_month = 12;
    	$prev_end_year = $prev_end_year - 1;
    }
    
    $prev_start_createdate = $prev_start_year."-".$prev_start_month."-01";
    $prev_end_createdate = $prev_end_year."-".$prev_end_month."-01";
    
    $pretail = " useridx>20000 AND '$prev_start_createdate 00:00:00' <= writedate AND writedate < '$prev_end_createdate 00:00:00' ";
    $ios_pretail = " useridx>20000 AND '$prev_start_createdate 00:00:00' <= writedate AND writedate < '$prev_end_createdate 00:00:00' AND os_type = 1 ";
    $and_pretail = " useridx>20000 AND '$prev_start_createdate 00:00:00' <= writedate AND writedate < '$prev_end_createdate 00:00:00' AND os_type = 2 ";
    $ama_pretail = " useridx>20000 AND '$prev_start_createdate 00:00:00' <= writedate AND writedate < '$prev_end_createdate 00:00:00' AND os_type = 3 ";

    $sql = "SELECT LEFT(writedate,7) AS writedate, ".    		
     	  "SUM(IF(STATUS=1, money, 0)) AS prevcredit ".
    	  "FROM tbl_product_order_mobile WHERE ".$pretail."GROUP BY LEFT(writedate,7) ORDER BY writedate DESC;";    
    $pre_data = $db_main->gettotallist($sql);
    
    $sql = "SELECT LEFT(writedate,7) AS writedate, ".
    		"SUM(IF(STATUS=1, money, 0)) AS prevcredit ".
    		"FROM tbl_product_order_mobile WHERE ".$ios_pretail."GROUP BY LEFT(writedate,7) ORDER BY writedate DESC;";
    $ios_pre_data = $db_main->gettotallist($sql);
    
    $sql = "SELECT LEFT(writedate,7) AS writedate, ".
    		"SUM(IF(STATUS=1, money, 0)) AS prevcredit ".
    		"FROM tbl_product_order_mobile WHERE ".$and_pretail."GROUP BY LEFT(writedate,7) ORDER BY writedate DESC;";
    $and_pre_data = $db_main->gettotallist($sql);
    
    $sql = "SELECT LEFT(writedate,7) AS writedate, ".
    		"SUM(IF(STATUS=1, money, 0)) AS prevcredit ".
    		"FROM tbl_product_order_mobile WHERE ".$ama_pretail."GROUP BY LEFT(writedate,7) ORDER BY writedate DESC;";
    $ama_pre_data = $db_main->gettotallist($sql);
    
    $db_main->end();
    
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
</script>

        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
        <form name="search_form" id="search_form"  method="get" action="pay_month_mobile.php">
            <input type=hidden name=issearch value="1">
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 월별 통계</div>
				<div class="search_box">			
					<select name="search_start_year" id="search_start_year">
			 			<?for ($i=2014; $i <= 2020; $i++ ) {?>
							<option value="<?=$i?>" <?= ($search_start_year == $i) ? "selected" : "" ?>><?=$i?></option>				
						<?}?>
					</select>년
					<select name="search_start_month" id="search_start_month">
						<?for( $i = 1; $i <= 12; $i++ ) { 
							$month_start_num = str_pad( $i, 2, 0, STR_PAD_LEFT );
						?>			
							<option value="<?=$month_start_num?>" <?= ($search_start_month == $month_start_num) ? "selected" : "" ?>><?=$month_start_num?></option>			
						<?}?>
					</select>월				
			 		~
					<select name="search_end_year" id="search_end_year">
						<?for ($i=2014; $i <= 2020; $i++ ) {?>
							<option value="<?=$i?>" <?= ($search_end_year == $i) ? "selected" : "" ?>><?=$i?></option>				
						<?}?>
					</select>년
					<select name="search_end_month" id="search_end_month">
						<?for( $i = 1; $i <= 12; $i++ ) { 
							$month_end_num = str_pad( $i, 2, 0, STR_PAD_LEFT );
						?>			
							<option value="<?=$month_end_num?>" <?= ($search_end_month == $month_end_num) ? "selected" : "" ?>><?=$month_end_num?></option>			
						<?}?>
					</select>월			
				<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
				</div>
            </div>
            <!-- //title_warp -->
            
            <div class="search_result">
                <span><?= $search_start_year."-".$search_start_month ?></span> ~ <span><?= $search_end_year."-".$search_end_month ?></span> 통계입니다
            </div>
            
            <div class="h2_title">[전체]</div>
            
            <div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
                <col width="">
                <col width=""> 
                 
            </colgroup>
            <thead>
            <tr>
                <th>년월</th>
                <th class="tdr">구매건수</th>
                <th class="tdr">구매액</th>
                <th class="tdr">전월대비</th>
                
                <th class="tdr">SpecialOffer<br>구매건수</th>
                <th class="tdr">SpecialOffer<br>구매액</th>
                
                <th class="tdr">취소건수<br>(결제일기준)</th>
                <th class="tdr">취소금액<br>(결제일기준)</th>
            </tr>
            </thead>
            <tbody>
<?
	for($i=0; $i<sizeof($data); $i++)
	{
		$writedate = $data[$i]["writedate"];
		$totalcount = $data[$i]["totalcount"];
		$totalcredit = $data[$i]["totalcredit"];
		$specialcount = $data[$i]["specialcount"];
		$specialcredit = $data[$i]["specialcredit"];
		$cancelcount = $data[$i]["cancelcount"];
		$cancelcredit = $data[$i]["cancelcredit"];
		$prevcredit = $pre_data[$i]["prevcredit"];
		
		if ($prevcredit == 0)
			$ratio = "N/A";
		else
		{
			$ratio = (round($totalcredit * 1000 / $prevcredit) / 10)."%";
		}
?>
                <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
                    <td class="tdc point"><?=$writedate?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>
                    <td class="tdr point">$<?= number_format(($totalcredit), 1) ?></td>
                    <td class="tdr point"><?= $ratio ?></td>
                    
                    <td class="tdr point"><?= number_format($specialcount) ?></td>
                    <td class="tdr point">$<?= number_format(($specialcredit), 1) ?></td>
                    
                    <td class="tdr point"><?= number_format($cancelcount) ?></td>
                    <td class="tdr point">$<?= number_format(($cancelcredit), 1) ?></td>
                </tr>
<?
    }
?>    
            </tbody>
            </table>
        </div>
        <br/><br/>
        	
            <div class="h2_title">[iOS]</div>
            
            <div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
                <col width="">
                <col width=""> 
                 
            </colgroup>
            <thead>
            <tr>
                <th>년월</th>
                <th class="tdr">구매건수</th>
                <th class="tdr">구매액</th>
                <th class="tdr">전월대비</th>
                
                <th class="tdr">SpecialOffer<br>구매건수</th>
                <th class="tdr">SpecialOffer<br>구매액</th>
                
                <th class="tdr">취소건수<br>(결제일기준)</th>
                <th class="tdr">취소금액<br>(결제일기준)</th>
            </tr>
            </thead>
            <tbody>
<?
	for($i=0; $i<sizeof($ios_data); $i++)
	{
		$writedate = $ios_data[$i]["writedate"];
		$totalcount = $ios_data[$i]["totalcount"];
		$totalcredit = $ios_data[$i]["totalcredit"];
		$specialcount = $ios_data[$i]["specialcount"];
		$specialcredit = $ios_data[$i]["specialcredit"];
		$cancelcount = $ios_data[$i]["cancelcount"];
		$cancelcredit = $ios_data[$i]["cancelcredit"];
		$prevcredit = $ios_pre_data[$i]["prevcredit"];
		
		if ($prevcredit == 0)
			$ratio = "N/A";
		else
		{
			$ratio = (round($totalcredit * 1000 / $prevcredit) / 10)."%";
		}
?>
                <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
                    <td class="tdc point"><?=$writedate?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>
                    <td class="tdr point">$<?= number_format(($totalcredit), 1) ?></td>
                    <td class="tdr point"><?= $ratio ?></td>
                    
                    <td class="tdr point"><?= number_format($specialcount) ?></td>
                    <td class="tdr point">$<?= number_format(($specialcredit), 1) ?></td>
                    
                    <td class="tdr point"><?= number_format($cancelcount) ?></td>
                    <td class="tdr point">$<?= number_format(($cancelcredit), 1) ?></td>
                </tr>
<?
    }

	
?>    
            </tbody>
            </table>
        </div>
        <br/><br/>
        
        <div class="h2_title">[Android]</div>
            
            <div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
                <col width="">
                <col width=""> 
                 
            </colgroup>
            <thead>
            <tr>
                <th>년월</th>
                <th class="tdr">구매건수</th>
                <th class="tdr">구매액</th>
                <th class="tdr">전월대비</th>
                
                <th class="tdr">SpecialOffer<br>구매건수</th>
                <th class="tdr">SpecialOffer<br>구매액</th>
                
                <th class="tdr">취소건수<br>(결제일기준)</th>
                <th class="tdr">취소금액<br>(결제일기준)</th>
            </tr>
            </thead>
            <tbody>
<?
	for($i=0; $i<sizeof($and_data); $i++)
	{
		$writedate = $and_data[$i]["writedate"];
		$totalcount = $and_data[$i]["totalcount"];
		$totalcredit = $and_data[$i]["totalcredit"];
		$specialcount = $and_data[$i]["specialcount"];
		$specialcredit = $and_data[$i]["specialcredit"];
		$cancelcount = $and_data[$i]["cancelcount"];
		$cancelcredit = $and_data[$i]["cancelcredit"];
		$prevcredit = $and_pre_data[$i]["prevcredit"];
		
		if ($prevcredit == 0)
			$ratio = "N/A";
		else
		{
			$ratio = (round($totalcredit * 1000 / $prevcredit) / 10)."%";
		}
?>
                <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
                    <td class="tdc point"><?=$writedate?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>
                    <td class="tdr point">$<?= number_format(($totalcredit), 1) ?></td>
                    <td class="tdr point"><?= $ratio ?></td>
                    
                    <td class="tdr point"><?= number_format($specialcount) ?></td>
                    <td class="tdr point">$<?= number_format(($specialcredit), 1) ?></td>
                    
                    <td class="tdr point"><?= number_format($cancelcount) ?></td>
                    <td class="tdr point">$<?= number_format(($cancelcredit), 1) ?></td>
                </tr>
<?
    }

	
?>    
            </tbody>
            </table>
        </div>
        <br/><br/>
        
        <div class="h2_title">[Amazon]</div>
            
            <div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
                <col width="">
                <col width=""> 
                 
            </colgroup>
            <thead>
            <tr>
                <th>년월</th>
                <th class="tdr">구매건수</th>
                <th class="tdr">구매액</th>
                <th class="tdr">전월대비</th>
                
                <th class="tdr">SpecialOffer<br>구매건수</th>
                <th class="tdr">SpecialOffer<br>구매액</th>
                
                <th class="tdr">취소건수<br>(결제일기준)</th>
                <th class="tdr">취소금액<br>(결제일기준)</th>
            </tr>
            </thead>
            <tbody>
<?
	for($i=0; $i<sizeof($ama_data); $i++)
	{
		$writedate = $ama_data[$i]["writedate"];
		$totalcount = $ama_data[$i]["totalcount"];
		$totalcredit = $ama_data[$i]["totalcredit"];
		$specialcount = $ama_data[$i]["specialcount"];
		$specialcredit = $ama_data[$i]["specialcredit"];
		$cancelcount = $ama_data[$i]["cancelcount"];
		$cancelcredit = $ama_data[$i]["cancelcredit"];
		$prevcredit = $ama_pre_data[$i]["prevcredit"];
		
		if ($prevcredit == 0)
			$ratio = "N/A";
		else
		{
			$ratio = (round($totalcredit * 1000 / $prevcredit) / 10)."%";
		}
?>
                <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
                    <td class="tdc point"><?=$writedate?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>
                    <td class="tdr point">$<?= number_format(($totalcredit), 1) ?></td>
                    <td class="tdr point"><?= $ratio ?></td>
                    
                    <td class="tdr point"><?= number_format($specialcount) ?></td>
                    <td class="tdr point">$<?= number_format(($specialcredit), 1) ?></td>
                    
                    <td class="tdr point"><?= number_format($cancelcount) ?></td>
                    <td class="tdr point">$<?= number_format(($cancelcredit), 1) ?></td>
                </tr>
<?
    }

	
?>    
            </tbody>
            </table>
        </div>
        
        </form>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>