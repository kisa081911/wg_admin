<?
	$top_menu = "pay_static";
	$sub_menu = "pay_winrate_boost_stats";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$startdate = $_GET["startdate"];
	$enddate = $_GET["enddate"];
	
	$tab = ($_GET["tab"] == "") ? "0" : $_GET["tab"];
	
	check_xss($startdate);
	check_xss($enddate);
	
	//오늘 날짜 정보
	$today = date("Y-m-d");
	$before_day = get_past_date($today,15,"d");
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;
	
	$db_main = new CDatabase_Main();
	$db_analysis = new CDatabase_Analysis();
	
	if($tab == 0)
	{
		$sql = "SELECT LEFT(writedate,10) as writedate, ".
				" SUM(IF(STATUS=1 AND (productidx BETWEEN 24 AND 33 OR productidx BETWEEN 87 AND 90 OR productidx BETWEEN 125 AND 134),facebookcredit,0)) AS thresholdcredit, ". 
				" SUM(IF(STATUS=1 AND (productidx BETWEEN 34 AND 39 OR productidx BETWEEN 135 AND 137),facebookcredit,0)) AS whalecredit, ".
				" SUM(IF(STATUS=1 AND (productidx BETWEEN 40 AND 45 OR productidx BETWEEN 138 AND 140),facebookcredit,0)) AS retentioncredit, ".
				" SUM(IF(STATUS=1 AND (productidx BETWEEN 46 AND 50 OR productidx = 141),facebookcredit,0)) AS firstcredit ".
	  			" FROM tbl_product_order A JOIN tbl_user_ext B ON A.useridx=B.useridx ".
	  			" WHERE A.status IN (1, 3) AND A.useridx > 20000 AND writedate >= '$startdate 00:00:00' AND writedate <= '$enddate 23:59:59' ".
	  			" GROUP BY LEFT(writedate,10) ".
	  			" ORDER BY writedate ASC";
		$staticlist = $db_main->gettotallist($sql);
		
		$thresholdcredit_list = array();
		$retentioncredit_list = array();
		$whalecredit_list = array();
		$firstcredit_list = array();
		$date_list = array();
		
		$list_pointer = sizeof($staticlist);
			
	
		$date_pointer = $enddate;
		
		for ($i=0; $i<=get_diff_date($enddate, $startdate, "d"); $i++)
		{
			$writedate = $staticlist[$list_pointer-1]["writedate"];
			
			if (get_diff_date($date_pointer, $writedate, "d") == 0)
			{
				$static = $staticlist[$list_pointer-1];
				
				$thresholdcredit_list[$i] = $static["thresholdcredit"] * 0.1;
				$retentioncredit_list[$i] = $static["retentioncredit"] * 0.1;
				$whalecredit_list[$i] = $static["whalecredit"] * 0.1;
				$firstcredit_list[$i] = $static["firstcredit"] * 0.1;
				$date_list[$i] = $date_pointer;
				
				$list_pointer--;
			}
			else 
			{
				$thresholdcredit_list[$i] = 0;
				$retentioncredit_list[$i] = 0;
				$whalecredit_list[$i] = 0;
				$firstcredit_list[$i] = 0;
				$date_list[$i] = $date_pointer;				
			}
			
			$date_pointer = get_past_date($date_pointer, 1, "d");
		}
	}
	else
	{
		$sql = "SELECT productidx, amount, facebookcredit, imageurl FROM tbl_product WHERE productidx BETWEEN 24 AND 50 ";
		$product_list = $db_main->gettotallist($sql);
		
		$sql = "SELECT t1.writedate, day_row_count, day_total_facebookcredit, productidx, total_count, total_facebookcredit, total_coin ".
				"FROM ( ".
				"	SELECT writedate, productidx, total_count, total_facebookcredit, total_coin ".
				"	FROM tbl_product_stat_daily ".
				"	WHERE writedate BETWEEN '$startdate' AND '$enddate' AND productidx BETWEEN 24 AND 50 ".
				") t1 LEFT JOIN ( ".
				"	SELECT writedate, SUM(total_facebookcredit) AS day_total_facebookcredit, COUNT(productidx) AS day_row_count ".
				"	FROM tbl_product_stat_daily ".
				"	WHERE writedate BETWEEN '$startdate' AND '$enddate' AND productidx BETWEEN 24 AND 50 ".
				"	GROUP BY writedate ".
				") t2 ON t1.writedate = t2.writedate ".
				"ORDER BY writedate DESC, total_facebookcredit DESC";
		$product_data = $db_analysis->gettotallist($sql);
	}
	
	$db_main->end();
	$db_analysis->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});

<?
	if($tab == 0)
	{
?>
		google.load("visualization", "1", {packages:["corechart"]});
	
	    function drawChart() 
	    {
	        var data1 = new google.visualization.DataTable();
	        data1.addColumn('string', '날짜');
	        data1.addColumn('number', 'Threshold 결제금액($)');
	        data1.addColumn('number', '28 Retention 결제금액($)');
	        data1.addColumn('number', 'Whale 결제금액($)');
	        data1.addColumn('number', 'First 결제금액($)');
	        data1.addRows([
<?
			for ($i=sizeof($date_list); $i>0; $i--)
			{
				$_thresholdcredit = $thresholdcredit_list[$i-1];
				$_retentioncredit = $retentioncredit_list[$i-1];
				$_whalecredit = $whalecredit_list[$i-1];
				$_firstcredit = $firstcredit_list[$i-1];
		        $_date = $date_list[$i-1];
				
				echo("['".$_date."',".$_thresholdcredit.",".$_retentioncredit.",".$_whalecredit.",".$_firstcredit."]");
				
				if ($i > 1)
					echo(",");
			}
?>
	        ]);
	        
	        var options1 = {
	            width:1050,                         
	            height:470,
	            axisTitlesPosition:'in',
	            curveType:'none',
	            focusTarget:'category',
	            interpolateNulls:'true',
	            legend:'top',
	            fontSize : 12,
	            chartArea:{left:60,top:40,width:1040,height:300}
	        };
	        
	        var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
	        chart.draw(data1, options1);      
	    }
	        
		google.setOnLoadCallback(drawChart);
<?
	}
?>
	
	function tab_change(tab)
	{
		var search_form = document.search_form;
		search_form.tab.value = tab;
		search_form.submit();
	}

	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
		    search();
	    }
	}

	function search()
	{
		var search_form = document.search_form;

		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">

	<!-- title_warp -->
	<form name="search_form" id="search_form"  method="get" action="pay_winrate_boost_stats.php">
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; V4부양 스페셜오퍼 통계</div>
		<div class="search_box"> 
			<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
            <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
            <input type=hidden name="tab" value="<?= $tab ?>">
             <input type="button" class="btn_search" value="검색" onclick="search()" />
		</div>
    </div>
	<!-- //title_warp -->
	
    <div class="search_result">
    	<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다
    </div>
	
	<ul class="tab">
		<li id="tab_1" class="<?= ($tab == "0") ? "select" : "" ?>" style="width:50px;text-align:center;font-size:11px;" onclick="tab_change('0')">그래프</li>
		<li id="tab_2" class="<?= ($tab == "1") ? "select" : "" ?>" style="width:50px;text-align:center;font-size:11px;" onclick="tab_change('1')">상세보기</li>
	</ul>
	    	
<?
	if($tab == 0)
	{
?>
	<div id="chart_div1" style="height:480px; min-width: 500px"></div>
<?
	}
	else
	{
?>
	<div id="tab_content_1">
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">날짜</th>
					<th class="tdc">상품명</th>
					<th class="tdc">구매 수</th>
					<th class="tdc">구매 금액</th>
					<th class="tdc">구매 Coin</th>
					<th class="tdc">구매비율</th>
				</tr>
			</thead>
			<tbody>
<?
			$tmp_row_count = 1;
			
			$day_total_count = 0;
			$day_total_facebookcredit = 0;
			
			$all_total_count = 0;
			$all_total_facebookcredit = 0;
			
			for($i=0; $i<sizeof($product_data); $i++)
			{
				$row_count = $product_data[$i]["day_row_count"];
				$writedate = $product_data[$i]["writedate"];
				$productidx = $product_data[$i]["productidx"];
				$total_count = $product_data[$i]["total_count"];
				$total_facebookcredit = round($product_data[$i]["total_facebookcredit"]/10, 1);
				$total_coin = $product_data[$i]["total_coin"];
				$total_purchase_rate = ($product_data[$i]["day_total_facebookcredit"] == 0) ? 0 : round(($product_data[$i]["total_facebookcredit"] / $product_data[$i]["day_total_facebookcredit"] * 100), 2);
				
				$day_total_count += $total_count;
				$day_total_facebookcredit += $total_facebookcredit;
				$day_total_coin += $total_coin;
				
				$all_total_count += $total_count;
				$all_total_facebookcredit += $total_facebookcredit;
				$all_total_coin += $total_coin;
				
				$imageurl = "";
				$productname = "";
				
				if($productidx == "0")
				{
					$imageurl = "/images/icon/facebook.png";
					$productname = "Earn";
				}
				else
				{
					for($j=0; $j<sizeof($product_list); $j++)
					{
						if($product_list[$j]["productidx"] == $productidx)
						{
							//if((24 <= $productidx && $productidx <= 50))
							//{
								$imageurl = $product_list[$j]["imageurl"];
								
								$productname = $product_list[$j]["facebookcredit"];
								
								if((24 <= $productidx && $productidx <= 33) || (87 <= $productidx && $productidx <= 90) || (125 <= $productidx && $productidx <= 134))
								{
									$productname = $productname."(Threshold)";
								}
								else if((34 <= $productidx && $productidx <= 39) || (135 <= $productidx && $productidx <= 137))
								{
									$productname = $productname."(Whale)";
								}
								else if((40 <= $productidx && $productidx <= 45) || (138 <= $productidx && $productidx <= 140))
								{
									$productname = $productname."(28 Retention)";
								}
								else if((46 <= $productidx && $productidx <= 50) || $productidx == 141)
								{
									$productname = $productname."(First)";
								}
								
								break;
							//}						
						}
					}
				}
				
?>
				<tr>
<?
				if($tmp_row_count == 1)
				{
					$tmp_row_count = $row_count;
?>
					<td class="tdc point_title" rowspan="<?= $row_count + 1?>"><?= $writedate ?></td>
<?
				}
				else
				{
					$tmp_row_count--;
				}
?>
					<td class="tdl point"><img src="<?=$imageurl?>" align="absmiddle" width="25" height="25"> <?= $productname ?></td>
					<td class="tdc"><?= number_format($total_count) ?></td>
					<td class="tdc">$<?= number_format($total_facebookcredit, 1) ?></td>
					<td class="tdc"><?= number_format($total_coin) ?></td>
					<td class="tdc"><?= number_format($total_purchase_rate, 2) ?>%</td>
				</tr>
<?
				if($tmp_row_count == 1)
				{
?>
				<tr>
					<td class="tdc point">Total</td>
					<td class="tdc point"><?= number_format($day_total_count) ?></td>
					<td class="tdc point">$<?= number_format($day_total_facebookcredit, 1) ?></td>
					<td class="tdc point"><?= number_format($day_total_coin) ?></td>
					<td class="tdc point">100%</td>
				</tr>
<? 
					$day_total_count = 0;
					$day_total_facebookcredit = 0;
					$day_total_coin = 0;
				}
			}
?>
				<tr>
					<td class="tdc point_title" colspan="2">Total</td>
					<td class="tdc point"><?= number_format($all_total_count) ?></td>
					<td class="tdc point">$<?= number_format($all_total_facebookcredit, 1) ?></td>
					<td class="tdc point"><?= number_format($all_total_coin) ?></td>
					<td class="tdc point"></td>
				</tr>
			</tbody>
		</table>
	</div>
<?
	}
?>
	</form>	
</div>
    	<!--  //CONTENTS WRAP -->
    	
	<div class="clear"></div>
</div>
<!-- //MAIN WRAP -->
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>