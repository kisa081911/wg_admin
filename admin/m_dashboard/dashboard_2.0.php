<?
    $top_menu = "dashboard";
    $sub_menu = "";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    // 최근 6시간 3분 term
    $term = "4";
    $os_term = 4;
    
    $end_date = date("Y-m-d", time());
    $end_time = date("H:i:s", time());
    
    $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time))-6,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
    $start_time = date("H:i:s", mktime(date("H", strtotime($end_time))-6,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
    
    $user_where = " WHERE isnpc=0 ";
    $late_where = " WHERE latency > 0  AND errcode = 0";
    $error_where = " WHERE errcode != '0' ";
    $event_where = "WHERE 1=1 ";
    $warningevent_where = " WHERE category=2 ";
    $errorevent_where = " WHERE category=3 ";
    
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    $db_analysis = new CDatabase_Analysis();
    
    // 현재 사용자 접속 상태
    $sql = "SELECT SUM(webusercount) AS webusercount, SUM(iosusercount) AS iosusercount, SUM(androidusercount) AS androidusercount, SUM(amazonusercount) AS amazonusercount, SUM(group1) AS group1, SUM(group2) AS group2, SUM(group3) AS group3, minute ".
        "FROM ( ".
        "	SELECT SUM(totalcount)/COUNT(DISTINCT logkey) AS webusercount, 0 AS iosusercount, 0 AS androidusercount, 0 AS amazonusercount, SUM(group1)/COUNT(DISTINCT logkey) AS group1, SUM(group2)/COUNT(DISTINCT logkey) AS group2, SUM(group3)/COUNT(DISTINCT logkey) AS group3, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "	FROM user_online_log ".
        "	$user_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        "	GROUP BY minute ".
        "	UNION ALL ".
        "	SELECT 0 AS webusercount, SUM(totalcount)/COUNT(DISTINCT logkey) AS iosusercount, 0 AS androidusercount, 0 AS amazonusercount, SUM(group1)/COUNT(DISTINCT logkey) AS group1, SUM(group2)/COUNT(DISTINCT logkey) AS group2, SUM(group3)/COUNT(DISTINCT logkey) AS group3, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "	FROM user_online_mobile_log ".
        "	$user_where AND os_type=1 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        " 	GROUP BY minute ".
        "	UNION ALL ".
        "	SELECT 0 AS webusercount, 0 AS iosusercount, SUM(totalcount)/COUNT(DISTINCT logkey) AS androidusercount, 0 AS amazonusercount, SUM(group1)/COUNT(DISTINCT logkey) AS group1, SUM(group2)/COUNT(DISTINCT logkey) AS group2, SUM(group3)/COUNT(DISTINCT logkey) AS group3, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "	FROM user_online_mobile_log ".
        "	$user_where AND os_type=2 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        " 	GROUP BY minute ".
        "	UNION ALL ".
        "	SELECT 0 AS webusercount, 0 AS iosusercount, 0 AS androidusercount, SUM(totalcount)/COUNT(DISTINCT logkey) AS amazonusercount, SUM(group1)/COUNT(DISTINCT logkey) AS group1, SUM(group2)/COUNT(DISTINCT logkey) AS group2, SUM(group3)/COUNT(DISTINCT logkey) AS group3, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "	FROM user_online_mobile_log ".
        "	$user_where AND os_type=3 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        " 	GROUP BY minute ".
        ") t1 ".
        "GROUP BY minute ";
    
    $userlist = $db_analysis->gettotallist($sql);
    
    // web일반 유저
    $sql = "SELECT SUM(webusercount_v1) AS webusercount_v1, SUM(webusercount_v2) AS webusercount_v2, minute ".
        "FROM ( ".
        "	SELECT SUM(totalcount)/COUNT(DISTINCT logkey) AS webusercount_v1, 0 AS webusercount_v2, SUM(group1)/COUNT(DISTINCT logkey) AS group1, SUM(group2)/COUNT(DISTINCT logkey) AS group2, SUM(group3)/COUNT(DISTINCT logkey) AS group3, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "	FROM user_online_log ".
        "	$user_where AND is_v2 = 0 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        "	GROUP BY minute ".
        "	UNION ALL ".
        "	SELECT 0 AS webusercount, SUM(totalcount)/COUNT(DISTINCT logkey) AS webusercount_v2, SUM(group1)/COUNT(DISTINCT logkey) AS group1, SUM(group2)/COUNT(DISTINCT logkey) AS group2, SUM(group3)/COUNT(DISTINCT logkey) AS group3, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "	FROM user_online_log ".
        "	$user_where AND is_v2 = 1 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        "	GROUP BY minute ".
        ") t1 ".
        "GROUP BY minute ";
    $web_userlist = $db_analysis->gettotallist($sql);
    
    
    // 클라이언트 통신 로그
    // 늦은 응답 건수
    $sql = "SELECT IFNULL(COUNT(*), 0) AS latecount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "FROM client_communication_log $late_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        "GROUP BY minute ";
    
    $latelist = $db_analysis->gettotallist($sql);
    
    // 에러 건수
    $sql = "SELECT IFNULL(COUNT(*), 0) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "FROM client_communication_log $error_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        "GROUP BY minute ";
    
    $errorlist = $db_analysis->gettotallist($sql);
    
    // 서버 통신 로그
    // 늦은 응답 건수
    $sql = "SELECT IFNULL(COUNT(*), 0) AS latecount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "FROM server_communication_log_v2 $late_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        "GROUP BY minute ";
    
    $server_latelist = $db_analysis->gettotallist($sql);
    
    // 에러 건수
    $sql = "SELECT IFNULL(COUNT(*), 0) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "FROM server_communication_log_v2 $error_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        "GROUP BY minute ";
    
    $server_errorlist = $db_analysis->gettotallist($sql);
    
    // 서버 이벤트 로그
    // 서버 이벤트 warning로그
    $sql = "SELECT IFNULL(COUNT(*), 0) AS warningcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "FROM server_log_v2 $warningevent_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        "GROUP BY minute ";
    
    $warningevent_list = $db_analysis->gettotallist($sql);
    
    // 서버 이벤트 error로그
    $sql = "SELECT IFNULL(COUNT(*), 0) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "FROM server_log_v2 $errorevent_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        "GROUP BY minute ";
    
    $errorevent_list = $db_analysis->gettotallist($sql);
    
    $saved_isrefresh = $_COOKIE["saved_isrefresh"];
?>
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
	<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});

	function drawChart() 
    {
		var datatable1 = new google.visualization.DataTable();
        
        datatable1.addColumn('string', '시간');
        datatable1.addColumn('number', '전체');
        datatable1.addColumn('number', 'web');
        datatable1.addColumn('number', 'ios');
        datatable1.addColumn('number', 'android');
        datatable1.addColumn('number', 'amazon');
        datatable1.addColumn('number', 'VIP 1~3');
        datatable1.addColumn('number', 'VIP 4~6');
        datatable1.addColumn('number', 'VIP 7이상');
        datatable1.addRows([
<?
    $start = round(mktime(date("H",strtotime($start_time)), date("i",strtotime($start_time)), date("s", strtotime($start_time)), date("m",strtotime($start_date)), date("d", strtotime($start_date)), date("Y", strtotime($start_date)))/($term*60));
    $end = round(mktime(date("H",strtotime($end_time)), date("i",strtotime($end_time)), date("s", strtotime($end_time)), date("m",strtotime($end_date)), date("d", strtotime($end_date)), date("Y", strtotime($end_date)))/($term*60)) + 1;
    
    for ($i=$start; $i<$end; $i=$i+1)
    {
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        if ($term > 10080)
            echo("['".substr($list_date, 0, 7)."'");  
        else if ($term > 720)
            echo("['$list_date'");
        else
            echo("['".make_time_format($temp_minute)."'");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
            if ($userlist[$j]["minute"] == $i)
            {
                echo(",{v:".round($userlist[$j]["webusercount"] + $userlist[$j]["iosusercount"] + $userlist[$j]["androidusercount"] + $userlist[$j]["amazonusercount"]).",f:'".number_format(round($userlist[$j]["webusercount"] + $userlist[$j]["iosusercount"] + $userlist[$j]["androidusercount"] + $userlist[$j]["amazonusercount"]))."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
	        if ($userlist[$j]["minute"] == $i)
	        {
	        	echo(",{v:".round($userlist[$j]["webusercount"]).",f:'".number_format(round($userlist[$j]["webusercount"]))."'}");
	        	$print = true;
	        			break;
	        }
        }
        
        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
	        if ($userlist[$j]["minute"] == $i)
	        {
	        	echo(",{v:".round($userlist[$j]["iosusercount"]).",f:'".number_format(round($userlist[$j]["iosusercount"]))."'}");
	        	$print = true;
	        			break;
	        }
        }
        
        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
	        if ($userlist[$j]["minute"] == $i)
	        {
	        	echo(",{v:".round($userlist[$j]["androidusercount"]).",f:'".number_format(round($userlist[$j]["androidusercount"]))."'}");
	        	$print = true;
	        			break;
	        }
        }
        
        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
        	if ($userlist[$j]["minute"] == $i)
        	{
        		echo(",{v:".round($userlist[$j]["amazonusercount"]).",f:'".number_format(round($userlist[$j]["amazonusercount"]))."'}");
        		$print = true;
        		break;
        	}
        }
        
        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
	        if ($userlist[$j]["minute"] == $i)
	        {
	        	echo(",{v:".round($userlist[$j]["group1"]).",f:'".number_format(round($userlist[$j]["group1"]))."'}");
	        	$print = true;
	        			break;
	        }
        }
        
        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
        	if ($userlist[$j]["minute"] == $i)
        	{
        		echo(",{v:".round($userlist[$j]["group2"]).",f:'".number_format(round($userlist[$j]["group2"]))."'}");
        		$print = true;
        		break;
        	}
        }
        
        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
        	if ($userlist[$j]["minute"] == $i)
        	{
        		echo(",{v:".round($userlist[$j]["group3"]).",f:'".number_format(round($userlist[$j]["group3"]))."'}");
        		$print = true;
        		break;
        	}
        }
        
        if (!$print)
           echo(",0");
        
		           
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");                    
    }    
?>          
        ]);

var datatable1_web = new google.visualization.DataTable();
        
        datatable1_web.addColumn('string', '시간');
        datatable1_web.addColumn('number', '전체');
        datatable1_web.addColumn('number', 'v1');
        datatable1_web.addColumn('number', 'v2');
        datatable1_web.addRows([
<?
    $start = round(mktime(date("H",strtotime($start_time)), date("i",strtotime($start_time)), date("s", strtotime($start_time)), date("m",strtotime($start_date)), date("d", strtotime($start_date)), date("Y", strtotime($start_date)))/($term*60));
    $end = round(mktime(date("H",strtotime($end_time)), date("i",strtotime($end_time)), date("s", strtotime($end_time)), date("m",strtotime($end_date)), date("d", strtotime($end_date)), date("Y", strtotime($end_date)))/($term*60)) + 1;
    
    for ($i=$start; $i<$end; $i=$i+1)
    {
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        if ($term > 10080)
            echo("['".substr($list_date, 0, 7)."'");  
        else if ($term > 720)
            echo("['$list_date'");
        else
            echo("['".make_time_format($temp_minute)."'");
        
        $print = false;
        for ($j=0; $j<sizeof($web_userlist); $j++)
        {
            if ($web_userlist[$j]["minute"] == $i)
            {
                echo(",{v:".round($web_userlist[$j]["webusercount_v1"] + $web_userlist[$j]["webusercount_v2"]).",f:'".make_price_format(round($web_userlist[$j]["webusercount_v1"] + $web_userlist[$j]["webusercount_v2"]))."'}");
                $print = true;      
                break;
            }
        }

        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($web_userlist); $j++)
        {
	        if ($web_userlist[$j]["minute"] == $i)
	        {
	        	echo(",{v:".round($web_userlist[$j]["webusercount_v1"]).",f:'".number_format(round($web_userlist[$j]["webusercount_v1"]))."'}");
	        	$print = true;
	        	break;
	        }
        }
        
        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($web_userlist); $j++)
        {
	        if ($web_userlist[$j]["minute"] == $i)
	        {
	        	echo(",{v:".round($web_userlist[$j]["webusercount_v2"]).",f:'".number_format(round($web_userlist[$j]["webusercount_v2"]))."'}");
        		$print = true;
        		break;
	        }
        }
        
       if (!$print)
        	echo(",0");        
		           
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");                    
    }    
?>          
    ]);

        var datatable2 = new google.visualization.DataTable();
        
        datatable2.addColumn('string', '시간');
        datatable2.addColumn('number', '늦은응답건수');
        datatable2.addColumn('number', '에러건수');
        datatable2.addRows([
<?
	for ($i=$start; $i<$end; $i=$i+1)
	{      
		$temp_time = explode(":",date("H:i:s", $i*($term*60)));        
		$temp_minute = $temp_time[0]*60 + $temp_time[1];
      
		$list_date = date("Y-m-d", $i*($term*60));
        
		if ($term > 10080)
			echo("['".substr($list_date, 0, 7)."'");  
		else if ($term > 720)
			echo("['$list_date'");        
		else
			echo("['".make_time_format($temp_minute)."'");
        
		$print = false;
		for ($j=0; $j<sizeof($latelist); $j++)
		{
			if ($latelist[$j]["minute"] == $i)
			{
				echo(",{v:".$latelist[$j]["latecount"].",f:'".number_format($latelist[$j]["latecount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
        
		$print = false;
		for ($j=0; $j<sizeof($errorlist); $j++)
		{
			if ($errorlist[$j]["minute"] == $i)
			{
				echo(",{v:".$errorlist[$j]["errorcount"].",f:'".number_format($errorlist[$j]["errorcount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
        
		if ($i+1 >= $end)
			echo("]");  
		else
			echo("],");               
	}   
?>          
        ]);
        
        var datatable3 = new google.visualization.DataTable();
        
        datatable3.addColumn('string', '시간');
        datatable3.addColumn('number', '늦은응답건수');
        datatable3.addColumn('number', '에러건수');
        datatable3.addRows([
<?
	for ($i=$start; $i<$end; $i=$i+1)
	{      
		$temp_time = explode(":",date("H:i:s", $i*($term*60)));        
		$temp_minute = $temp_time[0]*60 + $temp_time[1];
      
		$list_date = date("Y-m-d", $i*($term*60));
        
		if ($term > 10080)
			echo("['".substr($list_date, 0, 7)."'");  
		else if ($term > 720)
			echo("['$list_date'");        
		else
			echo("['".make_time_format($temp_minute)."'");
        
		$print = false;
		for ($j=0; $j<sizeof($server_latelist); $j++)
		{
			if ($server_latelist[$j]["minute"] == $i)
			{
				echo(",{v:".$server_latelist[$j]["latecount"].",f:'".number_format($server_latelist[$j]["latecount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
           
		$print = false;
		for ($j=0; $j<sizeof($server_errorlist); $j++)
		{
			if ($server_errorlist[$j]["minute"] == $i)
			{
				echo(",{v:".$server_errorlist[$j]["errorcount"].",f:'".number_format($server_errorlist[$j]["errorcount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
        
		if ($i+1 >= $end)
			echo("]");  
		else
			echo("],");                
	}
?>          
        ]);
        
        var datatable4 = new google.visualization.DataTable();
        
        datatable4.addColumn('string', '시간');
        datatable4.addColumn('number', 'Waring 건수');
        datatable4.addColumn('number', 'Error 건수');
        datatable4.addRows([
<?
	for ($i=$start; $i<$end; $i=$i+1)
	{      
		$temp_time = explode(":",date("H:i:s", $i*($term*60)));        
		$temp_minute = $temp_time[0]*60 + $temp_time[1];
      
		$list_date = date("Y-m-d", $i*($term*60));
        
		if ($term > 10080)
			echo("['".substr($list_date, 0, 7)."'");  
		else if ($term > 720)
			echo("['$list_date'");        
		else
			echo("['".make_time_format($temp_minute)."'");
        
		$print = false;
		for ($j=0; $j<sizeof($warningevent_list); $j++)
		{
			if ($warningevent_list[$j]["minute"] == $i)
			{
				echo(",{v:".$warningevent_list[$j]["warningcount"].",f:'".number_format($warningevent_list[$j]["warningcount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
        
		$print = false;
		for ($j=0; $j<sizeof($errorevent_list); $j++)
		{
			if ($errorevent_list[$j]["minute"] == $i)
			{
				echo(",{v:".$errorevent_list[$j]["errorcount"].",f:'".number_format($errorevent_list[$j]["errorcount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
        
		if ($i+1 >= $end)
			echo("]");  
		else
			echo("],");         
	}
?>          
        ]);

        var options1 = {          
                axisTitlesPosition:'in',  
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize:12,
                colors: ['#0000CD', '#008000', '#D2691E', '#C0C0C0', '#FFD700'],
                chartArea:{left:50,top:30,width:595,height:200}
            };
        
		var options2 = {          
                axisTitlesPosition:'in',
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize:12,
                chartArea:{left:85,top:30,width:370,height:200}
            };
        
		var options3 = {      
                axisTitlesPosition:'in',
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize:12,
                chartArea:{left:85,top:30,width:370,height:200}
            };
        
		var options4 = {       
                axisTitlesPosition:'in',  
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize:12,
                chartArea:{left:85,top:30,width:370,height:200}
            };

		var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
		chart.draw(datatable1, options1);

		chart = new google.visualization.LineChart(document.getElementById('chart_div1_web'));
		chart.draw(datatable1_web, options1);
            
		chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
		chart.draw(datatable2, options2);
            
		chart = new google.visualization.LineChart(document.getElementById('chart_div3'));
		chart.draw(datatable3, options3);
            
		chart = new google.visualization.LineChart(document.getElementById('chart_div4'));
		chart.draw(datatable4, options4);
	}

	google.setOnLoadCallback(drawChart);

	function check_sleeptime_dashboard()
    {
        if ("<?= $saved_isrefresh ?>" == "1")
            setTimeout("window.location.reload(false)", 20 * 60 * 1000);
    }
	    
    function refresh()
    {
        window.location.reload(false);
    }
	 
    function set_cookie(name, checked, days) 
    {
        if (days) 
        {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString(); 
        } 
        else 
        {
           var expires = "";
        }
        
        if (checked)
            value = "1";
        else
            value = "0";
	            
        document.cookie = name + "=" + value + expires + "; path=/";   
        window.location.reload(false);     
    }
    function change_os_term(term)
	{
		if (term == "ALL")
			window.location.href = 'dashboard_all.php';
		else if (term == "0")
			window.location.href = 'dashboard.php';
		else if (term == "1")
			window.location.href = 'dashboard_ios.php';
		else if (term == "2")
			window.location.href = 'dashboard_android.php';
		else if (term == "3")
			window.location.href = 'dashboard_amazon.php';
		else if (term == "4")
			window.location.href = 'dashboard_2.0.php';
	}
	</script>	
	<!-- CONTENTS WRAP -->
	<div class="contents_wrap_wide">
		<!-- title_warp -->
		<div class="title_wrap">
			<input type="checkbox" name="isrefresh" id="isrefresh" value="1" align="absmiddle" onclick="set_cookie('saved_isrefresh', this.checked, '5')" <?= ($saved_isrefresh == "1") ? "checked" : "" ?> /> 20분마다 새로고침
                    <img src="/images/icon/refresh.png" align="absmiddle" style="cursor:pointer" align="absmiddle" width="23px" height="23px" onclick="refresh()"/>
                    
                    &nbsp;&nbsp;&nbsp;
                    
			<input type="button" class="<?= ($os_term == "6") ? "btn_schedule_select" : "btn_schedule" ?>" value="통합" id="term_all" onclick="change_os_term('ALL')"    />
			<input type="button" class="<?= ($os_term == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="term_web" onclick="change_os_term('0')"    />
			<input type="button" class="<?= ($os_term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_os_term('1')" />
			<input type="button" class="<?= ($os_term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_os_term('2')"    />
			<input type="button" class="<?= ($os_term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_os_term('3')"    />
			<input type="button" class="<?= ($os_term == "4") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web_v2" id="term_web_v2" onclick="change_os_term('4')"    />
		</div>
		<!-- //title_warp -->

<?
	$str_useridx = 20000;
	// 개발 서버용
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
		$str_useridx = 10000;
	
	$today = date("Y-m-d");
	$yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));
	
	$sql = "SELECT (SELECT value FROM tbl_dashboard_stat WHERE type = 501 AND subtype = 1) AS order_28day, ".
			"(SELECT value FROM tbl_dashboard_stat WHERE type = 501 AND subtype = 2) AS order_cancel_28day, ".
			"ROUND((SELECT value FROM tbl_dashboard_stat WHERE type = 501 AND subtype = 3)) AS count_28day, ".
			"(SELECT value FROM tbl_dashboard_stat WHERE type = 501 AND subtype = 4) AS order_7day, ".
			"(SELECT value FROM tbl_dashboard_stat WHERE type = 501 AND subtype = 5) AS order_cancel_7day, ".
			"ROUND((SELECT value FROM tbl_dashboard_stat WHERE type = 501 AND subtype = 6)) AS count_7day, ".
			"(SELECT value FROM tbl_dashboard_stat WHERE type = 501 AND subtype = 7) AS order_yesterday, ".
			"(SELECT value FROM tbl_dashboard_stat WHERE type = 501 AND subtype = 8) AS order_cancel_yesterday, ".
			"ROUND((SELECT value FROM tbl_dashboard_stat WHERE type = 501 AND subtype = 9)) AS count_yesterday ";	
	$order_info = $db_analysis->getarray($sql);
	
	$sql = "SELECT SUM(order_today) AS order_today, SUM(order_cancel_today) AS order_cancel_today, SUM(count_today) AS count_today ".
			"FROM ( ".
			"	SELECT ROUND(IFNULL(SUM(facebookcredit),0)/10, 1) AS order_today, 0 AS order_cancel_today, COUNT(orderidx) AS count_today ".
			"	FROM tbl_product_order ".
			"	WHERE useridx > $str_useridx AND status IN (1, 3) AND is_v2 = 1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' ".
			"	UNION ALL ".
			"	SELECT 0 AS order_today, ROUND(IFNULL(SUM(facebookcredit),0)/10, 1) AS order_cancel_today, 0 AS count_today ".
			"	FROM tbl_product_order ".
			"	WHERE useridx > $str_useridx AND status = 2  AND is_v2 = 1 AND canceldate BETWEEN '$today 00:00:00' AND '$today 23:59:59' ".
			") t1";
	
	$today_order_info = $db_main->getarray($sql);	
	
	$basedate = date("Y-m-d", time() - 14 * 24 * 60 * 60);
	$basedate2 = date("Y-m-d", time());
	
	$sql = "SELECT SUM(time_credit) ".
			"FROM ( ".
			"	SELECT IFNULL(SUM(facebookcredit),0) AS time_credit ".
			"	FROM tbl_product_order ".
			"	WHERE useridx>$str_useridx AND status IN (1, 3) AND is_v2 = 1  AND writedate >= '$basedate 00:00:00' AND writedate<'$basedate2' AND UNIX_TIMESTAMP(writedate) % (24 * 60 * 60) <= UNIX_TIMESTAMP(NOW()) % (24 * 60 * 60) ".			
			") t1";
	$time_credit = $db_main->getvalue($sql);
	
	$sql = "SELECT SUM(total_credit1) ".
			"FROM ( ".
			"	SELECT IFNULL(SUM(facebookcredit),0) AS total_credit1 ".
			"	FROM tbl_product_order ".
			"	WHERE useridx>$str_useridx AND status IN (1, 3) AND is_v2 = 1  AND writedate >= '$basedate 00:00:00' AND writedate<'$basedate2' ".
			") t1";
	$total_credit1 = $db_main->getvalue($sql);

	if ($time_credit != 0 && $today_total_credit != 0)
	{
		$estimated_credit = $today_total_credit * $total_credit1 / $time_credit;
	}
	else
		$estimated_credit = 0;
	
	// 기간별 할인율 계산
	$sql = "SELECT  ROUND((IFNULL(SUM(coin), 0)-IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
			"FROM tbl_product_order ".
			"WHERE basecoin > 0 AND STATUS IN (1, 3) AND is_v2 = 1 AND useridx > $str_useridx AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
	$salerate_today = $db_main->getvalue($sql);
		
	$sql = "SELECT (SELECT value FROM tbl_dashboard_stat WHERE type = 502 AND subtype = 1) AS salerate_28days, ".
			"(SELECT value FROM tbl_dashboard_stat WHERE type = 502 AND subtype = 2) AS salerate_7days, ".
			"(SELECT value FROM tbl_dashboard_stat WHERE type = 502 AND subtype = 3) AS salerate_yesterday";
	$sale_info = $db_analysis->getarray($sql);
	
	$salerate_yesterday = $sale_info["salerate_yesterday"];
	$salerate_7days = $sale_info["salerate_7days"];
	$salerate_28days = $sale_info["salerate_28days"];
	
	// 기간별 비할인 구매율 계산
	$sql = "SELECT SUM(t1.facebookcredit)/10 ".
			"FROM ( ".
			"	SELECT * FROM tbl_product_order ".
			"	WHERE STATUS IN (1, 3) AND useridx > $str_useridx AND is_v2 = 1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND couponidx = 0 AND special_discount = 0 AND special_more = 0".
			") t1 LEFT JOIN tbl_product t2 ON t1.productidx = t2.productidx ";
	$normalpurchase_today = $db_main->getvalue($sql);
	
	$sql = "SELECT (SELECT value FROM tbl_dashboard_stat WHERE type = 503 AND subtype = 1) AS normalpurchase_28days, ".
			"(SELECT value FROM tbl_dashboard_stat WHERE type = 503 AND subtype = 2) AS normalpurchase_7days, ".
			"(SELECT value FROM tbl_dashboard_stat WHERE type = 503 AND subtype = 3) AS normalpurchase_yesterday";	
	$normalpurchase_info = $db_analysis->getarray($sql);
	
	$normalpurchase_yesterday = $normalpurchase_info["normalpurchase_yesterday"];
	$normalpurchase_7days = $normalpurchase_info["normalpurchase_7days"];
	$normalpurchase_28days = $normalpurchase_info["normalpurchase_28days"];
?>
		<div style="width:600px;float:left;margin-left:10px;">
			<a href="/m1_product/order_mng.php?status=1" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">결제내역</a>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="60">
					<col width="50">
					<col width="120"> 
					<col width="120"> 
					<col width="100"> 
					<col width="60">                
				</colgroup>
				<thead>
					<tr>
						<th class="tdc">기간</th>
						<th>건수</th>
						<th class="tdr">구매금액(할인율%)</th>
						<th class="tdr">비할인구매금액(%)</th>
						<th class="tdr">취소금액(%)</th>
						<th class="tdr">예상금액</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="tdc point">최근28일</td>
						<td class="tdc"><?= number_format($order_info["count_28day"]) ?></td>
						<td class="tdr">$<?= number_format($order_info["order_28day"], 1) ?> (<?= round($salerate_28days, 1)?>%)</td>
						<td class="tdr">$<?= number_format($normalpurchase_28days, 1) ?> (<?= ($order_info["order_28day"] == 0 ? 0 : round($normalpurchase_28days*100/$order_info["order_28day"], 1))?>%)</td>
						<td class="tdr">$<?= number_format($order_info["order_cancel_28day"], 1)." (".($order_info["order_28day"] == 0 ? 0 : (round($order_info["order_cancel_28day"] / $order_info["order_28day"] * 100, 1)))."%)" ?></td>
						<td class="tdr">-</td>
					</tr>
					<tr>
						<td class="tdc point">최근7일</td>
						<td class="tdc"><?= number_format($order_info["count_7day"]) ?></td>
						<td class="tdr">$<?= number_format($order_info["order_7day"], 1) ?> (<?= round($salerate_7days, 1)?>%)</td>
						<td class="tdr">$<?= number_format($normalpurchase_7days, 1) ?> (<?= ($order_info["order_7day"] == 0 ? 0 : round($normalpurchase_7days*100/$order_info["order_7day"], 1))?>%)</td>
						<td class="tdr">$<?= number_format($order_info["order_cancel_7day"], 1)." (".($order_info["order_7day"] == 0 ? 0 : (round($order_info["order_cancel_7day"] / $order_info["order_7day"] * 100, 1)))."%)" ?></td>
						<td class="tdr">-</td>
					</tr>
					<tr>
						<td class="tdc point">어제</td>
						<td class="tdc"><?= number_format($order_info["count_yesterday"]) ?></td>
						<td class="tdr">$<?= number_format($order_info["order_yesterday"], 1) ?> (<?= round($salerate_yesterday, 1)?>%)</td>
						<td class="tdr">$<?= number_format($normalpurchase_yesterday, 1) ?> (<?= ($order_info["order_yesterday"] == 0 ? 0 : round($normalpurchase_yesterday*100/$order_info["order_yesterday"], 1))?>%)</td>
						<td class="tdr">$<?= number_format($order_info["order_cancel_yesterday"], 1)." (".($order_info["order_yesterday"] == 0 ? 0 : (round($order_info["order_cancel_yesterday"] / $order_info["order_yesterday"] * 100, 1)))."%)" ?></td>
						<td class="tdr">-</td>
					</tr>
					<tr>
						<td class="tdc point_title">오늘</td>
						<td class="tdc point_title"><?= number_format($today_order_info["count_today"]) ?></td>
						<td class="tdr point_title">$<?= number_format($today_order_info["order_today"], 1) ?> (<?= round($salerate_today, 1)?>%)</td>
						<td class="tdr">$<?= number_format($normalpurchase_today, 1) ?> (<?= ($today_order_info["order_today"] == 0 ? 0 : round($normalpurchase_today*100/$today_order_info["order_today"], 1))?>%)</td>
						<td class="tdr point_title">$<?= number_format($today_order_info["order_cancel_today"], 1)." (".($today_order_info["order_today"] == 0 ? 0 : (round($today_order_info["order_cancel_today"] / $today_order_info["order_today"] * 100, 1)))."%)" ?></td>
						<td class="tdr point_title">$<?= number_format(($estimated_credit*0.1), 1) ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div style="width: 600px; margin-left: 10px; float: left;">
			<a href="/m1_product/product_mng.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">오늘 상품별 결제 내역</a>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="120"> 
					<col width="80"> 
					<col width="80"> 
					<col width="120"> 
					<col width="80"> 
					<col width="80">               
				</colgroup>
				<thead>
					<tr>
						<th>상품명</th>
						<th class="tdc">건수</th>
						<th class="tdr">금액</th>
						<th style="padding-left:8px;">상품명</th>
						<th class="tdc">건수</th>
						<th class="tdr">금액</th>
					</tr>
				</thead>
				<tbody>
<?
    $sql = "SELECT t1.productidx, t2.facebookcredit AS productname, imageurl, paycount, t1.facebookcredit, t2.product_type ".
    		"FROM ( ".
    		"	SELECT productidx, COUNT(productidx) AS paycount, ROUND(IFNULL(SUM(facebookcredit),0))/10 AS facebookcredit ".
    		"	FROM tbl_product_order ".
    		"	WHERE status=1 AND is_v2 = 1 AND useridx>$str_useridx AND writedate >= '$today 00:00:00' ".
    		"	GROUP BY productidx ".
    		"	UNION ALL ".
    		"	SELECT 0 AS productidx, COUNT(orderidx) AS paycount, ROUND(IFNULL(SUM(money), 0), 1) AS facebookcredit ".
    		"	FROM tbl_product_order_earn ".
    		"	WHERE status=1 AND useridx>$str_useridx AND writedate >= '$today 00:00:00' ".
    		") t1 LEFT JOIN tbl_product t2 on t1.productidx = t2.productidx ".
    		"WHERE paycount > 0 ".
    		"ORDER BY facebookcredit DESC";
    
    $productlist = $db_main->gettotallist($sql);
       
    for($i=0; $i<sizeof($productlist); $i++)
    {
         $productidx = $productlist[$i]["productidx"];
         $productname = $productlist[$i]["productname"];
         $imageurl = $productlist[$i]["imageurl"];
         $facebookcredit = $productlist[$i]["facebookcredit"];
         $paycount = $productlist[$i]["paycount"];
         $product_type = $productlist[$i]["product_type"];
         
         if($productidx == "0")
         {
         	$productname = "facebok earn";
         	$imageurl = "/images/icon/facebook.png";
         }
         else if($product_type == 1)
         {
         	$productname = $productname."(Basic)";
         }         
         else if($product_type == 2)
         {
         	$productname = $productname."(Season)";
         }         
         else if($product_type == 3)
         {
         	$productname = $productname."(Threshold)";
         }
         else if($product_type == 4)
         {
         	$productname = $productname."(Whale)";
         }
         else if($product_type == 5)
         {
         	$productname = $productname."(28 Retention)";
         }
         else if($product_type == 6)
         {
         	$productname = $productname."(First)";
         }
         else if($product_type == 7)
         {
         	$productname = $productname."(Lucky)";
         }
         else if($product_type == 8)
         {
         	$productname = $productname."(Monthly)";
         }
         else if($product_type == 9)
         {
         	$productname = $productname."(Piggypot)";
         }
         else if($product_type == 10)
         {
         	$productname = $productname."(Buyerleave)";
         }
         else if($product_type == 11)
         {
         	$productname = $productname."(Nopayer)";
         }
         else if($product_type == 12)
         {
         	$productname = $productname."(primedeal_1)";
         }
         else if($product_type == 13)
         {
         	$productname = $productname."(primedeal_2)";
         }
         else if($product_type == 14)
         {
             $productname = $productname."(Attend)";
         }
         else if($product_type == 15)
         {
             $productname = $productname."(Platinum Deal)";
         }
         else if($product_type == 16)
         {
         	$productname = $productname."(Amazing deal)";
         }
         else if($product_type == 17)
         {
         	$productname = $productname."(First Attend)";
         }
	 else if($product_type == 19)
         {
         	$productname = $productname."(Speed Wheel)";
         }
?>
					<tr>
						<td class="tdl point"><img src="<?=$imageurl?>" align="absmiddle" width="25" height="25"> <?= $productname ?></td>
						<td class="tdc"><?= number_format($paycount) ?>건</td>
						<td class="tdr">$<?= number_format($facebookcredit, 1) ?></td>
<?
        $i++;
        
        if (sizeof($productlist) > $i)
        {
            $productidx = $productlist[$i]["productidx"];
            $productname = $productlist[$i]["productname"];
            $imageurl = $productlist[$i]["imageurl"];
            $facebookcredit = $productlist[$i]["facebookcredit"];
            $paycount = $productlist[$i]["paycount"]; 
	    	$product_type = $productlist[$i]["product_type"];
            
            if($productidx == "0")
            {
            	$productname = "facebok earn";
            	$imageurl = "/images/icon/facebook.png";
            }
	        else if($product_type == 1)
	         {
	         	$productname = $productname."(Basic)";
	         }
	         else if($product_type == 2)
	         {
	         	$productname = $productname."(Season)";
	         }
	         else if($product_type == 3)
	         {
	         	$productname = $productname."(Threshold)";
	         }
	         else if($product_type == 4)
	         {
	         	$productname = $productname."(Whale)";
	         }
	         else if($product_type == 5)
	         {
	         	$productname = $productname."(28 Retention)";
	         }
	         else if($product_type == 6)
	         {
	         	$productname = $productname."(First)";
	         }
	         else if($product_type == 7)
	         {
	         	$productname = $productname."(Lucky)";
	         }
	         else if($product_type == 8)
	         {
	         	$productname = $productname."(Monthly)";
	         }
	         else if($product_type == 9)
	         {
	         	$productname = $productname."(Piggypot)";
	         }
	         else if($product_type == 10)
	         {
	         	$productname = $productname."(Buyerleave)";
	         }
	         else if($product_type == 12)
	         {
	         	$productname = $productname."(primedeal_1)";
	         }
	         else if($product_type == 13)
	         {
	         	$productname = $productname."(primedeal_2)";
	         }
	         else if($product_type == 14)
	         {
	             $productname = $productname."(Attend)";
	         }
	         else if($product_type == 15)
	         {
	             $productname = $productname."(Platinum Deal)";
	         }
	         else if($product_type == 16)
	         {
	         	$productname = $productname."(Amazing deal)";
	         }
	         else if($product_type == 17)
	         {
	         	$productname = $productname."(First Attend)";
	         }
?>
						<td class="tdl point" style="border-left:1px solid #e7e7e7;padding-left:8px;"><img src="<?=$imageurl?>" align="absmiddle" width="25" height="25"> <?= $productname ?></td>
						<td class="tdc"><?= number_format($paycount) ?>건</td>
						<td class="tdr">$<?= number_format($facebookcredit, 1) ?></td>
<?
        }
?>
					</tr>
<?
    }
?>
				</tbody>
			</table>
		</div>
		
		<!-- <div style="width:600px;float:left;margin-left:10px;margin-top:20px;">
			<a href="/m2_user/user_mng.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">오늘 회원 유입경로별</a>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="120"> 
					<col width="50">   
					<col width="">  
					<col width="120"> 
					<col width="50">   
					<col width="">                   
				</colgroup>
				<thead>
					<tr>
						<th>유입경로</th>
						<th>건수</th>
						<th class="tdr">미게임건수(%)</th>
						<th style="padding-left:5px;">유입경로</th>
						<th>건수</th>
						<th class="tdr">미게임건수(%)</th>
					</tr>
				</thead>
				<tbody>
<?
	$sql = "SELECT * ".
			"FROM ( ".
     		"	SELECT COUNT(*) AS adflag_count, platform, adflag, COUNT(IF(NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx),1,NULL)) AS nogame_adflag ".
     		"	FROM tbl_user_ext ". 
     		"	WHERE useridx>$str_useridx AND createdate >= '$today 00:00:00' AND platform NOT IN (1, 2, 3) ".
     		"	GROUP BY adflag ". 
			"	UNION ALL ".
     		"	SELECT COUNT(*) AS adflag_count, platform, adflag, COUNT(IF(NOT EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx),1,NULL)) AS nogame_adflag ".
     		"	FROM tbl_user_ext ". 
     		"	WHERE useridx>$str_useridx AND createdate >= '$today 00:00:00' AND platform = 1 ".
     		"	GROUP BY adflag ". 
			"	UNION ALL ".
     		"	SELECT COUNT(*) AS adflag_count, platform, adflag, COUNT(IF(NOT EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx),1,NULL)) AS nogame_adflag ".
     		"	FROM tbl_user_ext ". 
     		"	WHERE useridx>$str_useridx AND createdate >= '$today 00:00:00' AND platform = 2 ".
     		"	GROUP BY adflag ".
     		"	UNION ALL ".
     		"	SELECT COUNT(*) AS adflag_count, platform, adflag, COUNT(IF(NOT EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx),1,NULL)) AS nogame_adflag ".
     		"	FROM tbl_user_ext ".
     		"	WHERE useridx>$str_useridx AND createdate >= '$today 00:00:00' AND platform = 3 ".
     		"	GROUP BY adflag ".
			") t1 ".
			"ORDER BY adflag_count DESC";
	
    $user_adflag_list = $db_main->gettotallist($sql);
    
    for ($i=0; $i<sizeof($user_adflag_list); $i++)
    {
        $adflag = $user_adflag_list[$i]["adflag"];
        $platform = $user_adflag_list[$i]["platform"];
        $adflag_count = $user_adflag_list[$i]["adflag_count"];
        $nogame_adflag = $user_adflag_list[$i]["nogame_adflag"];
        $os_str = "";
        
        if ($adflag == "" && $platform == 0)
            $adflag = "web_viral";
        else if ($adflag == "" && $platform == 1)
        	$adflag = "ios_viral";
        else if ($adflag == "" && $platform == 2)
        	$adflag = "android_viral";
        else if ($adflag == "" && $platform == 3)
        	$adflag = "amazon_viral";
		else if ($adflag != "" )
		{
			if ($platform == 0)
				$os_str = "(web)";
			else if ($platform == 1)
				$os_str = "(ios)";
			else if ($platform == 2)
				$os_str = "(android)";
			else if ($platform == 3)
				$os_str = "(amazon)";
		}
?>
					<tr>
						<td class="tdc"><?= $os_str.$adflag ?></td>
						<td class="tdc"><?= number_format($adflag_count) ?></td>
						<td class="tdr"><?= $nogame_adflag." (".(round($nogame_adflag / $adflag_count * 100))."%)" ?></td>
<?
        $i++;
        
        if (sizeof($user_adflag_list) > $i)
        {
            $adflag = $user_adflag_list[$i]["adflag"];
            $platform = $user_adflag_list[$i]["platform"];
            $adflag_count = $user_adflag_list[$i]["adflag_count"];
            $nogame_adflag = $user_adflag_list[$i]["nogame_adflag"];
            $os_str = "";
            
            if ($adflag == "" && $platform == 0)
           		$adflag = "web_viral";
	        else if ($adflag == "" && $platform == 1)
	        	$adflag = "ios_viral";
	        else if ($adflag == "" && $platform == 2)
	        	$adflag = "android_viral";
	        else if ($adflag == "" && $platform == 3)
	        	$adflag = "amazon_viral";
	        else if ($adflag != "" )
	        {
	        		if ($platform == 0)
	        			$os_str = "(web)";
	        		else if ($platform == 1)
	        			$os_str = "(ios)";
	        		else if ($platform == 2)
	        			$os_str = "(android)";
	        		else if ($platform == 3)
	        			$os_str = "(amazon)";
	        }
?>
						<td class="tdc" style="padding-left:5px;"><?= $os_str.$adflag ?></td>
						<td class="tdc"><?= number_format($adflag_count) ?></td>
						<td class="tdr"><?= $nogame_adflag." (".(round($nogame_adflag / $adflag_count * 100))."%)" ?></td>
<?      
		}
?>
					</tr>
<?      
    }
?>
				</tbody>
			</table>
		</div>	 -->
		
		<div class="clear"></div>	
		
		<div style="width: 600px; margin-top: 20px; margin-left: 10px; float: left;">
			<a href="/m5_monitoring/online_log_stats.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">현재 사용자 접속 상태</a>
			<div id="chart_div1" style="height:300px;width:590px;"></div>
		</div>
		
		<div style="width: 600px; margin-top: 20px; margin-left: 10px; float: left;">
			<a href="/m5_monitoring/online_log_stats.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">현재 사용자 접속 상태(web)</a>
			<div id="chart_div1_web" style="height:300px;width:590px;"></div>
		</div>
		
		<div class="clear"></div>
		
		<div style="width:600px;float:left;margin-left:10px;margin-top:20px;">
			<a href="/m8_game_stats/game_action_stats3.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">오늘 게임 활동 추이</a>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="100">
					<col width="100">
					<col width="100"> 
					<col width="100">               
				</colgroup>
				<tbody>
<?
    $sql = "SELECT IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout),0) AS moneyout ".
			"FROM tbl_game_cash_stats_daily2 ".
			"WHERE writedate = '$today' AND is_v2 = 1 AND MODE NOT IN (5, 9, 26, 29, 30) ";
    
    $action_info = $db_main2->getarray($sql);
    
    $jackpot_amount = $db_analysis->getvalue("SELECT IFNULL(SUM(amount),0) FROM tbl_jackpot_stat_daily WHERE devicetype=0 AND today = '$today'");
    $rate = round($action_info["moneyin"]==0 ? 0 : ($action_info["moneyout"]+$jackpot_amount) / $action_info["moneyin"] * 100, 2);
    
    $user_count = $db_analysis->getvalue("SELECT SUM(totalcount) FROM user_online_game_log WHERE is_v2 = 1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
    $game_profit = $action_info["moneyin"] - ($action_info["moneyout"] + $jackpot_amount);
    $free_amount = $db_analysis->getvalue("SELECT SUM(freeamount) FROM tbl_user_freecoin_stat WHERE category = 0 AND is_v2 = 1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
    
    $sql = "SELECT IFNULL(SUM(order_coin),0) AS order_coin ".
			"FROM ( ".
			"	SELECT SUM(coin+gift_coin) AS order_coin ".
			"	FROM tbl_product_order ". 
			"	WHERE STATUS=1 AND useridx>$str_useridx AND is_v2 = 1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' ".
			") t1";
    
    $order_amount = $db_main->getvalue($sql);
    
    $total_margin = $game_profit - ($free_amount + $purchase_amount + $free_amount_play);
    if($total_margin < 0 )
        $total_margin ="-".make_price_format(abs($total_margin));
    else
	$total_margin =make_price_format(abs($total_margin));
    
    $purchase_amount = $order_amount;
?>
					<tr>
						<th class="tdc" style="border-top:1px solid #e7e7e7;">money_in (A)</th>
						<td class="tdr" style="border-top:1px solid #e7e7e7;"><?= number_format($action_info["moneyin"]) ?></td>
						<th class="tdc" style="border-top:1px solid #e7e7e7;">접속자수</th>
						<td class="tdr" style="border-top:1px solid #e7e7e7;"><?= number_format($user_count) ?></td>
					</tr>
					<tr>
						<th class="tdc" style="border-top:1px solid #e7e7e7;">money_out (B)</th>
						<td class="tdr" style="border-top:1px solid #e7e7e7;"><?= number_format($action_info["moneyout"]) ?></td>
						<th class="tdc">승률 ((B+C)/A)</th>
						<td class="tdr"><?= $rate."%" ?></td>
					</tr>
					<tr>
						<th class="tdc">jackpot (C)</th>
						<td class="tdr"><?= number_format($jackpot_amount) ?></td>
						<th class="tdc">게임횟수</th>
						<td class="tdr"><?= number_format($action_info["playcount"]) ?></td>
					</tr>
					<tr>
						<th class="tdc">게임이익 (A-(B+C)=D)</th>
						<td class="tdr"><?= number_format($game_profit) ?></td>
					</tr>
					<tr>
						<th class="tdc">구매코인 (E)</th>
						<td class="tdr"><?= number_format($purchase_amount) ?></td>
					</tr>
					<tr>
						<th class="tdc">무료코인 (F)</th>
						<td class="tdr"><?= number_format($free_amount) ?></td>
					</tr>
					<tr>
						<th class="tdc">총 이익 (D-(E+F))</th>
						<td class="tdr"><?= $total_margin ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div class="clear"></div>
		
		<div style="width:1200px;float:left;margin:20px 0 0 20px;">
			<a style="font:12px;color:#000;font-weight:bold;cursor:ponter;">서버 상태 정보</a>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="">   
					<col width="">  
                    <col width="">  
                    <col width="">  
                    <col width="">  
                    <col width=""> 
                    <col width="">   
                    <col width="">  
                    <col width="">  
                    <col width="">   
                    <col width="">            
                    <col width="">            
                </colgroup>
				<thead>
					<tr>
						<th class="tdc">IP(서버명)</th>
						<th class="tdc">CPU</th>
						<th class="tdc">디스크</th>
						<th class="tdc">memtotal</th>
						<th class="tdc">memfree</th>
						<th class="tdc">메모리가용량</th>
						<th class="tdc">vmem</th>
						<th class="tdc">Connection</th>
						<th class="tdc">TIME_WAIT</th>
						<th class="tdc">CLOSE_WAIT</th>
						<th class="tdc">SYNC_RECV</th>
						<th class="tdc">UpdateDate</th>
					</tr>
				</thead>
				<tbody>
<?
	$sql = "SELECT servername,serverip,cpu,diskuse,memtotal,memfree,vmem,established,timewait,closewait,sync_recv,UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(writedate) AS seconds FROM tbl_server_status ORDER BY servername";
	$server_list = $db_main2->gettotallist($sql);
	
	$sql = "SELECT MAX(cpu) as maxcpu, serverip ".
			"FROM tbl_server_status_log ".
			"WHERE '". $start_date ." 00:00:00' <= writedate AND writedate <= '". $end_date ." 23:59:59' ".
			"GROUP BY serverip";
	$maxcpu_list = $db_analysis->gettotallist($sql);

	for ($i=0; $i<sizeof($server_list); $i++)
	{
		$servername = $server_list[$i]["servername"];
		$serverip = $server_list[$i]["serverip"];
		$cpu = $server_list[$i]["cpu"];
		
		$maxcpu = "";
		
		for($j=0; $j<sizeof($maxcpu_list); $j++)
		{
			if($serverip == $maxcpu_list[$j]["serverip"])
				$maxcpu = $maxcpu_list[$j]["maxcpu"];
		}
		
		$diskuse = $server_list[$i]["diskuse"];
		$memtotal = $server_list[$i]["memtotal"];
		$memfree = $server_list[$i]["memfree"];
		$vmem = $server_list[$i]["vmem"];
		$established = $server_list[$i]["established"];
		$timewait = $server_list[$i]["timewait"];
        $closewait = $server_list[$i]["closewait"];
        $sync_recv = $server_list[$i]["sync_recv"];
        $mem_rate = round($memfree / $memtotal * 10000) / 100;
        $minutes = round($server_list[$i]["seconds"]/60);

        if ($diskuse >= "90" || $mem_rate < "10" || $cpu > "90" || $minutes > 5)
        	$style = "style='color:#fb7878;font-weight:bold;'";
        else
            $style = "";
?>
						<tr>
                            <td class="tdc" <?= $style ?>><?= $serverip."($servername)" ?></td>
                            <td class="tdc" <?= $style ?>><?= $cpu ?>%(<?= ($maxcpu=="")? "-" : $maxcpu."%" ?>)</td>
                            <td class="tdc" <?= $style ?>><?= $diskuse ?>%</td>
                            <td class="tdc" <?= $style ?>><?= number_format($memtotal) ?>MB</td>
                            <td class="tdc" <?= $style ?>><?= number_format($memfree) ?>MB</td>
                            <td class="tdc" <?= $style ?>><?= $mem_rate ?>%</td>
                            <td class="tdc" <?= $style ?>><?= number_format($vmem) ?>MB</td>
                            <td class="tdc" <?= $style ?>><?= number_format($established) ?></td>
                            <td class="tdc" <?= $style ?>><?= number_format($timewait) ?></td>
                            <td class="tdc" <?= $style ?>><?= number_format($closewait) ?></td>
                            <td class="tdc" <?= $style ?>><?= number_format($sync_recv) ?></td>
                            <td class="tdc" <?= $style ?>><?= $minutes ?>분전</td>
                        </tr>
<?
    }
?>
				</tbody>
			</table>
		</div>
		
		<div class="clear"></div>

		<div style="width:390px;float:left;margin:20px 0 0 15px;">
			<a href="/m8_monitoring/client_log_stats.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">클라이언트 통신 로그</a>
			<div id="chart_div2" style="height:300px;width:390px;"></div>
		</div>
		<div style="width:390px;float:left;margin:20px 20px 0 20px;">
			<a href="/m8_monitoring/server_log_stats.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">서버 통신 로그_v2</a>
			<div id="chart_div3" style="height:300px;width:390px;"></div>
		</div>
		<div style="width:390px;float:left;margin:20px 15px 0 0;">
			<a href="/m8_monitoring/serverevent_log_stats.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">서버 이벤트 로그_v2</a>
			<div id="chart_div4" style="height:300px;width:390px;"></div>
		</div>
	</div>
	<!-- //CONTENTS WRAP -->
<? 
	$db_main->end();
	$db_main2->end();
	$db_analysis->end();

    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>