<?
    $top_menu = "dashboard";
    $sub_menu = "";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    if (strpos($login_adminmenu, "[dashboard]") === false)
    	error_back("접근 권한이 없습니다.");
    
    $os_term = "3";
    $term = "3";
    
    // 최근 6시간 3분 term
    $end_date = date("Y-m-d", time());
    $end_time = date("H:i:s", time());  
    
    $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time))-5,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
    $start_time = date("H:i:s", mktime(date("H", strtotime($end_time))-5,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));      
      
    $user_where = " WHERE isnpc=0 ";  
    $late_where = " WHERE latency > 0  AND errcode = 0";
    $error_where = " WHERE errcode != '0' ";
	$client_zero_where = "WHERE errcode != '0' AND cmd = 0";
	$client_not_zero_where = "WHERE errcode != '0' AND cmd != 0";
    $event_where = "WHERE 1=1 ";
    $warningevent_where = " WHERE category=2 ";
    $errorevent_where = " WHERE category=3 ";
    
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    $db_analysis = new CDatabase_Analysis();
    $db_friend = new CDatabase_Friend();
    
    /************************* 결제 내역 ***************************/
    $today = date("Y-m-d");
    $yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));
    
    $sql = "SELECT (SELECT value FROM tbl_dashboard_stat WHERE type = 301 AND subtype = 1) AS order_28day,
    		(SELECT value FROM tbl_dashboard_stat WHERE type = 301 AND subtype = 2) AS order_cancel_28day,
    		ROUND((SELECT value FROM tbl_dashboard_stat WHERE type = 301 AND subtype = 3)) AS count_28day,
    		
    		(SELECT value FROM tbl_dashboard_stat WHERE type = 301 AND subtype = 4) AS order_7day,
    		(SELECT value FROM tbl_dashboard_stat WHERE type = 301 AND subtype = 5) AS order_cancel_7day,
    		ROUND((SELECT value FROM tbl_dashboard_stat WHERE type = 301 AND subtype = 6)) AS count_7day,
    		
    		(SELECT value FROM tbl_dashboard_stat WHERE type = 301 AND subtype = 7) AS order_yesterday,
    		(SELECT value FROM tbl_dashboard_stat WHERE type = 301 AND subtype = 8) AS order_cancel_yesterday,
    		ROUND((SELECT value FROM tbl_dashboard_stat WHERE type = 301 AND subtype = 9)) AS count_yesterday ";
    $order_info = $db_analysis->getarray($sql);
    
    $order_today = $db_main->getvalue("SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE os_type=3 AND useridx > 20000 AND status=1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
    $order_cancel_today = $db_main->getvalue("SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile_cancel WHERE useridx > 20000 AND os_type=3 AND canceldate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
    $order_count_today =  $db_main->getvalue("SELECT COUNT(*) FROM tbl_product_order_mobile WHERE os_type=3 AND useridx > 20000 AND status=1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
    
    $today_total_credit = round($order_today);
    
    $basedate = date("Y-m-d", time() - 14 * 24 * 60 * 60);
    $basedate2 = date("Y-m-d", time());
    
    $sql = "SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE os_type=3 AND useridx>20000 AND status = 1 AND writedate >= '$basedate 00:00:00' AND writedate<'$basedate2' AND UNIX_TIMESTAMP(writedate) % (24 * 60 * 60) <= UNIX_TIMESTAMP(NOW()) % (24 * 60 * 60)";
    $time_credit = $db_main->getvalue($sql);
    
    $sql = "SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE os_type=3 AND useridx>20000 AND status = 1 AND writedate >= '$basedate 00:00:00' AND writedate<'$basedate2'";
    $total_credit1 = $db_main->getvalue($sql);
    
    if ($time_credit != 0 && $today_total_credit != 0)
    {
    	$estimated_credit = $today_total_credit * $total_credit1 / $time_credit;
    }
    else
    	$estimated_credit = 0;
    
    // 기간별 할인율 계산
    $sql = 	"SELECT ROUND((SUM(coin) - SUM(basecoin)) / SUM(basecoin) * 100, 2) ".
    		"FROM tbl_product_order_mobile ".
    		"WHERE status = 1 AND useridx > 20000 AND os_type = 3 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
    $salerate_today = $db_main->getvalue($sql);
    
    $sql = "SELECT (SELECT value FROM tbl_dashboard_stat WHERE type = 302 AND subtype = 1) AS salerate_28days, ".
    		"(SELECT value FROM tbl_dashboard_stat WHERE type = 302 AND subtype = 2) AS salerate_7days, ".
    		"(SELECT value FROM tbl_dashboard_stat WHERE type = 302 AND subtype = 3) AS salerate_yesterday";
    
    $sale_info = $db_analysis->getarray($sql);
    
    $salerate_yesterday = $sale_info["salerate_yesterday"];
    $salerate_7days = $sale_info["salerate_7days"];
    $salerate_28days = $sale_info["salerate_28days"];
    
    // 기간별 비할인 구매율 계산
    $sql = 	"SELECT SUM(money) ".
    		"FROM tbl_product_order_mobile t1 ".
    		"WHERE STATUS = 1 AND useridx > 20000 AND os_type = 3 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND couponidx = 0 AND special_discount = 0 AND special_more = 0;";    		
	$normalpurchase_today = $db_main->getvalue($sql);
    
    $sql = "SELECT (SELECT value FROM tbl_dashboard_stat WHERE type = 303 AND subtype = 1) AS normalpurchase_28days, ".
    		"(SELECT value FROM tbl_dashboard_stat WHERE type = 303 AND subtype = 2) AS normalpurchase_7days, ".
    		"(SELECT value FROM tbl_dashboard_stat WHERE type = 303 AND subtype = 3) AS normalpurchase_yesterday";    
    $normalpurchase_info = $db_analysis->getarray($sql);
    
    $normalpurchase_yesterday = $normalpurchase_info["normalpurchase_yesterday"];
    $normalpurchase_7days = $normalpurchase_info["normalpurchase_7days"];
    $normalpurchase_28days = $normalpurchase_info["normalpurchase_28days"];
    
    /************************ 회원 가입 ************************/
    $sql = "SELECT (SELECT SUM(VALUE) FROM tbl_dashboard_stat WHERE TYPE = 306 AND subtype = 1) AS join_28days,
					(SELECT SUM(VALUE) FROM tbl_dashboard_stat WHERE TYPE = 306 AND subtype = 2) AS join_7days,
					(SELECT SUM(VALUE) FROM tbl_dashboard_stat WHERE TYPE = 306 AND subtype = 3) AS join_yesterday,
					(SELECT SUM(VALUE) FROM tbl_dashboard_stat WHERE TYPE = 307 AND subtype = 1) AS install_28days,
					(SELECT SUM(VALUE) FROM tbl_dashboard_stat WHERE TYPE = 307 AND subtype = 2) AS install_7days,
					(SELECT SUM(VALUE) FROM tbl_dashboard_stat WHERE TYPE = 307 AND subtype = 3) AS install_yesterday,
					(SELECT SUM(VALUE) FROM tbl_dashboard_stat WHERE TYPE = 308 AND subtype = 1) AS tutorial_28days,
					(SELECT SUM(VALUE) FROM tbl_dashboard_stat WHERE TYPE = 308 AND subtype = 2) AS tutorial_7days,
					(SELECT SUM(VALUE) FROM tbl_dashboard_stat WHERE TYPE = 308 AND subtype = 3) AS tutorial_yesterday,
					(SELECT SUM(VALUE) FROM tbl_dashboard_stat WHERE TYPE = 309 AND subtype = 1) AS nogame_28days,
					(SELECT SUM(VALUE) FROM tbl_dashboard_stat WHERE TYPE = 309 AND subtype = 2) AS nogame_7days,
					(SELECT SUM(VALUE) FROM tbl_dashboard_stat WHERE TYPE = 309 AND subtype = 3) AS nogame_yesterday";
    $today_join_info_arr = $db_analysis->getarray($sql);
    
    $join_28days = $today_join_info_arr["join_28days"];
    $join_7days = $today_join_info_arr["join_7days"];
    $join_yesterday = $today_join_info_arr["join_yesterday"];
    $install_28days = $today_join_info_arr["install_28days"];
    $install_7days = $today_join_info_arr["install_7days"];
    $install_yesterday = $today_join_info_arr["install_yesterday"];
    $tutorial_28days = $today_join_info_arr["tutorial_28days"];
    $tutorial_7days = $today_join_info_arr["tutorial_7days"];
    $tutorial_yesterday = $today_join_info_arr["tutorial_yesterday"];
    $nogame_28days = $today_join_info_arr["nogame_28days"];
    $nogame_7days = $today_join_info_arr["nogame_7days"];
    $nogame_yesterday = $today_join_info_arr["nogame_yesterday"];
    
    $sql = "SELECT IFNULL(
		    SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END)
		    - IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_ios WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)
		    - IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_android WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)
		    - IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END),0)
		    ,0) AS join_nogame_today
		    FROM tbl_user_ext
		    WHERE createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND platform = 3";
    $join_nogame_today = $db_main->getvalue($sql);
     
    $sql = "SELECT platform, COUNT(*) AS join_count, SUM(IF(IFNULL(tutorial,0) < 4, 1, 0)) AS tutorial_count
		    FROM (
		    		SELECT useridx, platform
		    		FROM tbl_user_ext
		    		WHERE createdate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND logindate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND platform = 3
    		) t1 LEFT JOIN tbl_user_detail t2 ON t1.useridx = t2.useridx ";
    $today_data = $db_main->getarray($sql);
    
    $join_today = $today_data["join_count"];
    $install_today = $today_data["install_count"];
    $tutorial_today = $today_data["tutorial_count"];
    
    $basedate = date("Y-m-d", time() - 14 * 24 * 60 * 60);
    $basedate2 = date("Y-m-d", time());
     
    $sql = "SELECT COUNT(*)
		    FROM tbl_user_ext
		    WHERE createdate >= '$basedate 00:00:00' AND createdate<'$basedate2' AND platform = 3
		    AND UNIX_TIMESTAMP(createdate) % (24 * 60 * 60) <= UNIX_TIMESTAMP(NOW()) % (24 * 60 * 60)";
    $time_count = $db_main->getvalue($sql);
    
    $sql = "SELECT COUNT(*)
		    FROM tbl_user_ext
		    WHERE createdate >= '$basedate 00:00:00' AND createdate<'$basedate2' AND platform = 3";
    $total_count = $db_main->getvalue($sql);
    
    if ($time_count != 0 && $join_today != 0)
    	$estimated_count = $join_today * $total_count / $time_count;
    else
    	$estimated_count = 0;;
    	
    /************************ 현재 사용자 접속 상태 ************************/
    // 일반 유저
    $sql = "SELECT SUM(iosusercount) AS iosusercount, SUM(androidusercount) AS androidusercount, SUM(amazonusercount) AS amazonusercount, minute ".
    		"FROM ( ".
    		"	SELECT SUM(totalcount)/COUNT(DISTINCT logkey) AS iosusercount, 0 AS androidusercount, 0 AS amazonusercount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"	FROM user_online_mobile_log ".
    		"	$user_where AND os_type=1 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		" 	GROUP BY minute ".
    		"	UNION ALL ".
    		"	SELECT 0 AS iosusercount, SUM(totalcount)/COUNT(DISTINCT logkey) AS androidusercount, 0 AS amazonusercount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"	FROM user_online_mobile_log ".
    		"	$user_where AND os_type=2 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		" 	GROUP BY minute ".
    		"	UNION ALL ".
    		"	SELECT 0 AS iosusercount, 0 AS androidusercount, SUM(totalcount)/COUNT(DISTINCT logkey) AS amazonusercount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"	FROM user_online_mobile_log ".
    		"	$user_where AND os_type=3 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		" 	GROUP BY minute ".
    		") t1 ".
    		"GROUP BY minute ";
    $userlist = $db_analysis->gettotallist($sql);
       
     // 클라이언트 통신 로그
    // 늦은 응답 건수, 에러 건수
        
    // 에러 0
    $sql = "SELECT IFNULL(COUNT(*), 0) AS latecount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM client_communication_log_amazon $client_zero_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $latelist = $db_analysis->gettotallist($sql);
    
    // 에러 0 제외
    $sql = "SELECT IFNULL(COUNT(*), 0) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM client_communication_log_amazon $client_not_zero_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
     
    $errorlist = $db_analysis->gettotallist($sql);
    
    
    // 서버 통신 로그
    // 늦은 응답 건수, 에러 건수
    $sql = "SELECT IFNULL(COUNT(*), 0) AS latecount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_communication_log_mobile $late_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $server_latelist = $db_analysis->gettotallist($sql);
    
    // 에러 건수
    $sql = "SELECT IFNULL(COUNT(*), 0) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_communication_log_mobile $error_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $server_errorlist = $db_analysis->gettotallist($sql);
    
    // 서버 이벤트 로그
   // 서버 이벤트 warning로그
    $sql = "SELECT IFNULL(COUNT(*), 0) AS warningcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_log_mobile $warningevent_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $warningevent_list = $db_analysis->gettotallist($sql);
     
    // 서버 이벤트 error로그
    $sql = "SELECT IFNULL(COUNT(*), 0) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_log_mobile $errorevent_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $errorevent_list = $db_analysis->gettotallist($sql);
    
    $saved_isrefresh = $_COOKIE["saved_isrefresh"];
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
      
    function drawChart() 
    {
        var datatable1 = new google.visualization.DataTable();
        
        datatable1.addColumn('string', '시간');
        datatable1.addColumn('number', 'iOS');
        datatable1.addColumn('number', 'Android');
        datatable1.addColumn('number', 'Amazon');
        datatable1.addRows([
<?
    $start = round(mktime(date("H",strtotime($start_time)), date("i",strtotime($start_time)), date("s", strtotime($start_time)), date("m",strtotime($start_date)), date("d", strtotime($start_date)), date("Y", strtotime($start_date)))/($term*60));
    $end = round(mktime(date("H",strtotime($end_time)), date("i",strtotime($end_time)), date("s", strtotime($end_time)), date("m",strtotime($end_date)), date("d", strtotime($end_date)), date("Y", strtotime($end_date)))/($term*60)) + 1;
    
    for ($i=$start; $i<$end; $i=$i+1)
    {      
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        if ($term > 10080)
            echo("['".substr($list_date, 0, 7)."'");  
        else if ($term > 720)
            echo("['$list_date'");        
        else
            echo("['".make_time_format($temp_minute)."'");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
        	if ($userlist[$j]["minute"] == $i)
        	{
        		echo(",{v:".round($userlist[$j]["iosusercount"]).",f:'".make_price_format(round($userlist[$j]["iosusercount"]))."'}");
        		$print = true;
        		break;
        	}
        }
        
        if (!$print)
        	echo(",0");
         
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
        	if ($userlist[$j]["minute"] == $i)
        	{
        		echo(",{v:".round($userlist[$j]["androidusercount"]).",f:'".make_price_format(round($userlist[$j]["androidusercount"]))."'}");
        		$print = true;
        		break;
        	}
        }
        
        if (!$print)
        	echo(",0");
         
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
	        if ($userlist[$j]["minute"] == $i)
	        {
	        	echo(",{v:".round($userlist[$j]["amazonusercount"]).",f:'".make_price_format(round($userlist[$j]["amazonusercount"]))."'}");
	        	$print = true;
	        			break;
	        }
        }
        
        if (!$print)
        	echo(",0");
                   
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");                    
    }    
?>          
        ]);

        var datatable2 = new google.visualization.DataTable();
        
        datatable2.addColumn('string', '시간');
        datatable2.addColumn('number', '에러건수(0)');
        datatable2.addColumn('number', '에러건수(0 제외)');
        datatable2.addRows([
<?
	for ($i=$start; $i<$end; $i=$i+1)
	{      
		$temp_time = explode(":",date("H:i:s", $i*($term*60)));        
		$temp_minute = $temp_time[0]*60 + $temp_time[1];
      
		$list_date = date("Y-m-d", $i*($term*60));
        
		if ($term > 10080)
			echo("['".substr($list_date, 0, 7)."'");  
		else if ($term > 720)
			echo("['$list_date'");        
		else
			echo("['".make_time_format($temp_minute)."'");
        
		$print = false;
		for ($j=0; $j<sizeof($latelist); $j++)
		{
			if ($latelist[$j]["minute"] == $i)
			{
				echo(",{v:".$latelist[$j]["latecount"].",f:'".number_format($latelist[$j]["latecount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
        
		$print = false;
		for ($j=0; $j<sizeof($errorlist); $j++)
		{
			if ($errorlist[$j]["minute"] == $i)
			{
				echo(",{v:".$errorlist[$j]["errorcount"].",f:'".number_format($errorlist[$j]["errorcount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
        
		if ($i+1 >= $end)
			echo("]");  
		else
			echo("],");               
	}   
?>          
        ]);
        
        var datatable3 = new google.visualization.DataTable();
        
        datatable3.addColumn('string', '시간');
        datatable3.addColumn('number', '늦은응답건수');
        datatable3.addColumn('number', '에러건수');
        datatable3.addRows([
<?
	for ($i=$start; $i<$end; $i=$i+1)
	{      
		$temp_time = explode(":",date("H:i:s", $i*($term*60)));        
		$temp_minute = $temp_time[0]*60 + $temp_time[1];
      
		$list_date = date("Y-m-d", $i*($term*60));
        
		if ($term > 10080)
			echo("['".substr($list_date, 0, 7)."'");  
		else if ($term > 720)
			echo("['$list_date'");        
		else
			echo("['".make_time_format($temp_minute)."'");
        
		$print = false;
		for ($j=0; $j<sizeof($server_latelist); $j++)
		{
			if ($server_latelist[$j]["minute"] == $i)
			{
				echo(",{v:".$server_latelist[$j]["latecount"].",f:'".number_format($server_latelist[$j]["latecount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
           
		$print = false;
		for ($j=0; $j<sizeof($server_errorlist); $j++)
		{
			if ($server_errorlist[$j]["minute"] == $i)
			{
				echo(",{v:".$server_errorlist[$j]["errorcount"].",f:'".number_format($server_errorlist[$j]["errorcount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
        
		if ($i+1 >= $end)
			echo("]");  
		else
			echo("],");                
	}
?>          
        ]);
        
        var datatable4 = new google.visualization.DataTable();
        
        datatable4.addColumn('string', '시간');
        datatable4.addColumn('number', 'Waring 건수');
        datatable4.addColumn('number', 'Error 건수');
        datatable4.addRows([
<?
	for ($i=$start; $i<$end; $i=$i+1)
	{      
		$temp_time = explode(":",date("H:i:s", $i*($term*60)));        
		$temp_minute = $temp_time[0]*60 + $temp_time[1];
      
		$list_date = date("Y-m-d", $i*($term*60));
        
		if ($term > 10080)
			echo("['".substr($list_date, 0, 7)."'");  
		else if ($term > 720)
			echo("['$list_date'");        
		else
			echo("['".make_time_format($temp_minute)."'");
        
		$print = false;
		for ($j=0; $j<sizeof($warningevent_list); $j++)
		{
			if ($warningevent_list[$j]["minute"] == $i)
			{
				echo(",{v:".$warningevent_list[$j]["warningcount"].",f:'".number_format($warningevent_list[$j]["warningcount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
        
		$print = false;
		for ($j=0; $j<sizeof($errorevent_list); $j++)
		{
			if ($errorevent_list[$j]["minute"] == $i)
			{
				echo(",{v:".$errorevent_list[$j]["errorcount"].",f:'".number_format($errorevent_list[$j]["errorcount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
        
		if ($i+1 >= $end)
			echo("]");  
		else
			echo("],");         
	}
?>          
        ]);

        
        var options1 = {          
            axisTitlesPosition:'in',  
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize:12,
            chartArea:{left:85,top:30,width:590,height:200}
        };
    
        var options2 = {          
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize:12,
            chartArea:{left:85,top:30,width:370,height:200}
        };

        var options3 = {          
                axisTitlesPosition:'in',
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize:12,
                chartArea:{left:85,top:30,width:370,height:200}
		};

        var options4 = {          
                axisTitlesPosition:'in',
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize:12,
                chartArea:{left:85,top:30,width:370,height:200}
		};
    
        var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
        chart.draw(datatable1, options1);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
        chart.draw(datatable2, options2);

        chart = new google.visualization.LineChart(document.getElementById('chart_div3'));
        chart.draw(datatable3, options3);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_div4'));
        chart.draw(datatable4, options4);
    }
  
<?
	if ($login_adminmenu != "")
	{
?>
    google.setOnLoadCallback(drawChart);
<?
	}
?>
    
    function check_sleeptime_dashboard()
    {
        if ("<?= $saved_isrefresh ?>" == "1")
            setTimeout("window.location.reload(false)", 20 * 60 * 1000);
    }
    
    function refresh()
    {
        window.location.reload(false);
    }
 
    function set_cookie(name, checked, days) 
    {
        if (days) 
        {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString(); 
        } 
        else 
        {
           var expires = "";
        }
        
        if (checked)
            value = "1";
        else
            value = "0";
            
        document.cookie = name + "=" + value + expires + "; path=/";   
        window.location.reload(false);     
    }

    function change_os_term(term)
	{
		if (term == "ALL")
			window.location.href = 'dashboard_all.php';
		else if (term == "0")
			window.location.href = 'dashboard.php';
		else if (term == "1")
			window.location.href = 'dashboard_ios.php';
		else if (term == "2")
			window.location.href = 'dashboard_android.php';
		else if (term == "3")
			window.location.href = 'dashboard_amazon.php';
		else if (term == "4")
			window.location.href = 'dashboard_2.0.php';
	}

    function pop_product_payment_history()
	{
    	 window.open('/m_dashboard/pop_product_payment_history.php?platform=3', '', 'width=650,height=750,toolbar=no,menubar=no,scrollbars=yes,resizable=no');
	}
</script>
<!-- CONTENTS WRAP -->
        <div class="contents_wrap_wide" style="width:1300px">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div style="float:left;padding-left:5px;padding-top:7px;">
                    <input type="checkbox" name="isrefresh" id="isrefresh" value="1" align="absmiddle" onclick="set_cookie('saved_isrefresh', this.checked, '5')" <?= ($saved_isrefresh == "1") ? "checked" : "" ?> /> 20분마다 새로고침
                    <img src="/images/icon/refresh.png" align="absmiddle" style="cursor:pointer" align="absmiddle" width="23px" height="23px" onclick="refresh()"/>
                    
                &nbsp;&nbsp;&nbsp;
                    
                	<input type="button" class="<?= ($os_term == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" value="통합" id="term_web" onclick="change_os_term('ALL')"    />
					<input type="button" class="<?= ($os_term == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web_v2" id="term_web" onclick="change_os_term('0')"    />
					<input type="button" class="<?= ($os_term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_os_term('1')" />
					<input type="button" class="<?= ($os_term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_os_term('2')"    />
					<input type="button" class="<?= ($os_term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_os_term('3')"    />
				</div>
            </div>
            <!-- //title_warp -->
			
            <div style="width:640px;float:left;margin-left:20px;">
                <a href="/m1_product/order_ios_mng.php?status=1" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">결제내역</a>
                <table class="tbl_list_basic1" style="margin-top:5px">
                    <colgroup>
                        <col width="60">
                        <col width="50">
                        <col width="120"> 
						<col width="120"> 
                        <col width="100"> 
                        <col width="60">                
                    </colgroup>
                    <thead>
                        <tr>
                            <th class="tdc">기간</th>
                            <th>건수</th>
                            <th class="tdr">구매금액(할인율%)</th>
							<th class="tdr">비할인구매금액(%)</th>
                            <th class="tdr">취소금액(%)</th>
                            <th class="tdr">예상금액</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="tdc point">최근28일</td>
                            <td class="tdc"><?= make_price_format($order_info["count_28day"]) ?></td>
                            <td class="tdr">$<?= number_format(round($order_info["order_28day"], 1), 1) ?> (<?= round($salerate_28days, 1)?>%)</td>
							<td class="tdr">$<?= number_format(round($normalpurchase_28days, 1), 1) ?> (<?= round($normalpurchase_28days*100/$order_info["order_28day"], 1)?>%)</td>
                            <td class="tdr">$<?= number_format(round($order_info["order_cancel_28day"], 1), 1)." (".round($order_info["order_cancel_28day"] / $order_info["order_28day"] * 100, 2)."%)" ?></td>
                            <td class="tdr">-</td>
                        </tr>
                        <tr>
                            <td class="tdc point">최근7일</td>
                            <td class="tdc"><?= make_price_format($order_info["count_7day"]) ?></td>
                            <td class="tdr">$<?= number_format(round($order_info["order_7day"], 1), 1) ?> (<?= round($salerate_7days, 1)?>%)</td>
							<td class="tdr">$<?= number_format(round($normalpurchase_7days, 1), 1) ?> (<?= round($normalpurchase_7days*100/$order_info["order_7day"], 1)?>%)</td>
                            <td class="tdr">$<?= number_format(round($order_info["order_cancel_7day"], 1), 1)." (".round($order_info["order_cancel_7day"] / $order_info["order_7day"] * 100, 2)."%)" ?></td>
                            <td class="tdr">-</td>
                        </tr>
                        <tr>
                            <td class="tdc point">어제</td>
                            <td class="tdc"><?= make_price_format($order_info["count_yesterday"]) ?></td>
                            <td class="tdr">$<?= number_format(round($order_info["order_yesterday"], 1), 1) ?> (<?= round($salerate_yesterday, 1)?>%)</td>
							<td class="tdr">$<?= number_format(round($normalpurchase_yesterday, 1), 1) ?> (<?= round($normalpurchase_yesterday*100/$order_info["order_yesterday"], 1)?>%)</td>
                            <td class="tdr">$<?= number_format(round($order_info["order_cancel_yesterday"], 1), 1)." (".round($order_info["order_cancel_yesterday"] / $order_info["order_yesterday"] * 100, 2)."%)" ?></td>
                            <td class="tdr">-</td>
                        </tr>
                        <tr>
                            <td class="tdc point_title">오늘</td>
                            <td class="tdc point_title"><?= number_format($order_count_today) ?></td>
                            <td class="tdr point_title">$<?= number_format(round($order_today, 1),1) ?> (<?= round($salerate_today, 1)?>%)</td>
							<td class="tdr">$<?= number_format(round($normalpurchase_today, 1), 1) ?> (<?= round($normalpurchase_today*100/$order_today, 1)?>%)</td>
                            <td class="tdr point_title">$<?= number_format(round($order_cancel_today, 1), 1)." (".round($order_cancel_today / $order_today * 100, 2)."%)" ?></td>
                            <td class="tdr point_title">$<?= number_format($estimated_credit, 1) ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <div style="width:590px;float:left;margin-left:40px;">
                <a href="/m2_user/user_mng.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">회원가입</a>
                <table class="tbl_list_basic1" style="margin-top:5px">
                    <colgroup>
                        <col width="60">
                        <col width="50">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                    </colgroup>
                    <thead>
                        <tr>
                            <th class="tdc">기간</th>
                            <th class="tdr">건수</th>
                            <th class="tdr">미게임(%)</th>
                            <th class="tdr">튜토리얼(%)</th>
                            <th class="tdr">예상인원</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="tdc point">최근28일</td>
                            <td class="tdr"><?= make_price_format(round($join_28days)) ?></td>
                            <td class="tdr"><?= make_price_format(round($nogame_28days))." (".(round($nogame_28days / $join_28days * 10000) / 100)."%)" ?></td>
                            <td class="tdr"><?= make_price_format(round($tutorial_28days))." (".(round($tutorial_28days / $join_28days * 10000) / 100)."%)" ?></td>                            
                            <td class="tdr"></td>
                        </tr>
                        <tr>
                            <td class="tdc point">최근7일</td>
                            <td class="tdr"><?= make_price_format(round($join_7days)) ?></td>
                            <td class="tdr"><?= make_price_format(round($nogame_7days))." (".(round($nogame_7days / $join_7days * 10000) / 100)."%)" ?></td>
                            <td class="tdr"><?= make_price_format(round($tutorial_7days))." (".(round($tutorial_7days / $join_7days * 10000) / 100)."%)" ?></td>                            
                            <td class="tdr"></td>
                        </tr>
                        <tr>
                            <td class="tdc point">어제</td>
                            <td class="tdr"><?= make_price_format(round($join_yesterday)) ?></td>
                            <td class="tdr"><?= make_price_format(round($nogame_yesterday))." (".(round($nogame_yesterday / $join_yesterday * 10000) / 100)."%)" ?></td>
                            <td class="tdr"><?= make_price_format(round($tutorial_yesterday))." (".(round($tutorial_yesterday / $join_yesterday * 10000) / 100)."%)" ?></td>                            
                            <td class="tdr"></td>
                        </tr>
                        <tr>
                            <td class="tdc point_title">오늘</td>
                            <td class="tdr point_title"><?= make_price_format($join_today) ?></td>
                            <td class="tdr point_title"><?= make_price_format($join_nogame_today)." (".(round($join_nogame_today / $join_today * 10000) / 100)."%)" ?></td>
                            <td class="tdr point_title"><?= make_price_format($tutorial_today)." (".(round($tutorial_today / $join_today * 10000) / 100)."%)" ?></td>                            
                            <td class="tdr point_title"><?= number_format($estimated_count) ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <div class="clear"></div>
            <div class="clear"></div>
            
            <div style="width:640px;float:left;margin:20px 0 0 20px;">
                <a href="/m1_product/product_mng.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">오늘 상품별 결제 내역 (Top 10)</a>
                <a href="#" style="font:9px;color:#28AEFF;font-weight:bold;cursor:ponter;" onclick="pop_product_payment_history()"> - 전체 보기(새창)</a>
                <table class="tbl_list_basic1" style="margin-top:5px">
                    <colgroup>
                        <col width="160"> 
                        <col width="60"> 
                        <col width="60"> 
                        <col width="160"> 
                        <col width="60"> 
                        <col width="60">               
                    </colgroup>
                    <thead>
                        <tr>
                            <th>상품명</th>
                            <th class="tdc">건수</th>
                            <th class="tdr">금액</th>
                            <th style="padding-left:8px;">상품명</th>
                            <th class="tdc">건수</th>
                            <th class="tdr">금액</th>
                        </tr>
                    </thead>
                    <tbody>
<?
    $sql = "SELECT *
			FROM (
				SELECT os_type AS category, t1.productidx, t1.product_type, FORMAT(t2.money, 2)	 AS product, FORMAT(t2.money, 2) AS productname, imageurl, paycount, t1.money, is_discount
				FROM (
					SELECT productidx, COUNT(productidx) AS paycount, ROUND(IFNULL(SUM(money),0)) AS money, IF(special_discount <> 0, 1, 0) AS is_discount, product_type
					FROM tbl_product_order_mobile
					WHERE STATUS = 1 AND useridx>20000 AND writedate >= '$today 00:00:00' AND os_type = 3
					GROUP BY productidx,product_type, is_discount
				) t1 LEFT JOIN tbl_product_mobile t2 ON t1.productidx = t2.productidx
				WHERE paycount > 0
			) t1
			ORDER BY money DESC
    		LIMIT 10";
    
    $productlist = $db_main->gettotallist($sql);
       
    for($i=0; $i<sizeof($productlist); $i++)
    {
         $category = $productlist[$i]["category"];
         $productidx = $productlist[$i]["productidx"];
         $product = $productlist[$i]["product"];
         $productname = $productlist[$i]["productname"];
         $imageurl = $productlist[$i]["imageurl"];
         $facebookcredit = $productlist[$i]["money"];
         $paycount = $productlist[$i]["paycount"];
         $is_discount = $productlist[$i]["is_discount"];
         $product_type = $productlist[$i]["product_type"];
         
         
     if($productidx == "0")
         {
         	$productname = "facebok earn";
         	$imageurl = "/images/icon/facebook.png";
         }
         else if($product_type == 1)
         {
         	$productname = $product."(Basic)";
         }         
         else if($product_type == 2)
         {
         	$productname = $product."(Season)";
         }         
         else if($product_type == 3)
         {
         	$productname = $product."(Threshold)";
         }
         else if($product_type == 4)
         {
         	$productname = $product."(Whale)";
         }
         else if($product_type == 5)
         {
         	$productname = $product."(28 Retention)";
         }
         else if($product_type == 6)
         {
         	$productname = $product."(First)";
         }
         else if($product_type == 7)
         {
         	$productname = $product."(Lucky)";
         }
         else if($product_type == 8)
         {
         	$productname = $product."(Monthly)";
         }
         else if($product_type == 9)
         {
         	$productname = $product."(Piggypot)";
         }
         else if($product_type == 10)
         {
         	$productname = $product."(Buyerleave)";
         }
         else if($product_type == 11)
         {
         	$productname = $product."(Nopayer)";
         }
         else if($product_type == 12)
         {
         	$productname = $product."(primedeal_1)";
         }
         else if($product_type == 13)
         {
         	$productname = $product."(primedeal_2)";
         }
    	 else if($product_type == 14)
         {
             $productname = $product."(Attend)";
         }
         else if($product_type == 15)
         {
             $productname = $product."(Platinum Deal)";
         }
         else if($product_type == 16)
         {
         	$productname = $product."(Amazing deal)";
         }
         else if($product_type == 17)
         {
         	$productname = $product."(First Attend)";
         }
         else if($product_type == 18)
         {
             $productname = $productname."(Super Deal)";
         }
         else if($product_type == 19)
         {
             $productname = $productname."(Speed Wheel)";
         }
         else if($product_type == 20)
         {
             $productname = $productname."(Collection Deal)";
         }
         
         if($category == 0)
         	$category = "/images/icon/facebook.png";
         else if($category == 1)
         	$category = "/images/icon/ios.png";
         else if($category == 2)
         	$category = "/images/icon/android.png";
         else if($category == 3)
         	$category = "/images/icon/ama.png";
?>
                        <tr>
                            <td class="tdl point"><img src="<?=$imageurl?>" align="absmiddle" width="25" height="25"> <?= $productname ?> <img src="<?=$category?>" align="absmiddle" width="15" height="15"></td>
                            <td class="tdc"><?= make_price_format($paycount) ?>건</td>
                            <td class="tdr">$<?= number_format($facebookcredit, 1) ?></td>
<?
        $i++;
        
        if (sizeof($productlist) > $i)
        {
        	$category = $productlist[$i]["category"];
            $productidx = $productlist[$i]["productidx"];
            $product = $productlist[$i]["product"];
            $productname = $productlist[$i]["productname"];
            $imageurl = $productlist[$i]["imageurl"];
            $facebookcredit = $productlist[$i]["money"];
            $paycount = $productlist[$i]["paycount"]; 
            $is_discount = $productlist[$i]["is_discount"];
            $product_type = $productlist[$i]["product_type"];
            
	         if($productidx == "0")
	         {
	         	$productname = "facebok earn";
	         	$imageurl = "/images/icon/facebook.png";
	         }
	         else if($product_type == 1)
	         {
	         	$productname = $product."(Basic)";
	         }         
	         else if($product_type == 2)
	         {
	         	$productname = $product."(Season)";
	         }         
	         else if($product_type == 3)
	         {
	         	$productname = $product."(Threshold)";
	         }
	         else if($product_type == 4)
	         {
	         	$productname = $product."(Whale)";
	         }
	         else if($product_type == 5)
	         {
	         	$productname = $product."(28 Retention)";
	         }
	         else if($product_type == 6)
	         {
	         	$productname = $product."(First)";
	         }
	         else if($product_type == 7)
	         {
	         	$productname = $product."(Lucky)";
	         }
	         else if($product_type == 8)
	         {
	         	$productname = $product."(Monthly)";
	         }
	         else if($product_type == 9)
	         {
	         	$productname = $product."(Piggypot)";
	         }
	         else if($product_type == 10)
	         {
	         	$productname = $product."(Buyerleave)";
	         }
	         else if($product_type == 11)
	         {
	         	$productname = $product."(Nopayer)";
	         }
	         else if($product_type == 12)
	         {
	         	$productname = $product."(primedeal_1)";
	         }
	         else if($product_type == 13)
	         {
	         	$productname = $product."(primedeal_2)";
	         }
        	 else if($product_type == 14)
	         {
	             $productname = $product."(Attend)";
	         }
	         else if($product_type == 15)
	         {
	             $productname = $product."(Platinum Deal)";
	         }
	         else if($product_type == 16)
	         {
	         	$productname = $product."(Amazing deal)";
	         }
	         else if($product_type == 17)
	         {
	         	$productname = $product."(First Attend)";
	         }
	         else if($product_type == 18)
	         {
	             $productname = $productname."(Super Deal)";
	         }
	         else if($product_type == 19)
	         {
	             $productname = $productname."(Speed Wheel)";
	         }
	         else if($product_type == 20)
	         {
	             $productname = $productname."(Collection Deal)";
	         }
	         
	         if($category == 0)
	         	$category = "/images/icon/facebook.png";
	         else if($category == 1)
         		$category = "/images/icon/ios.png";
	         else if($category == 2)
       			$category = "/images/icon/android.png";
	         else if($category == 3)
	        	$category = "/images/icon/ama.png";
?>
                            <td class="tdl point" style="border-left:1px solid #e7e7e7;padding-left:8px;"><img src="<?=$imageurl?>" align="absmiddle" width="25" height="25"> <?= $productname ?> <img src="<?=$category?>" align="absmiddle" width="15" height="15"></td>
                            <td class="tdc"><?= number_format($paycount) ?>건</td>
                            <td class="tdr">$<?= number_format($facebookcredit, 1) ?></td>
<?
        }
?>
                        </tr>
<?
    }
    
?>
                    </tbody>
                </table>
            </div>
            <div style="width:590px;float:left;margin:20px 0 0 40px;">
                <a href="/m2_user/user_mng.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">오늘 회원 유입경로별 현황</a>
                <table class="tbl_list_basic1" style="margin-top:5px">
                    <colgroup>
                        <col width="120"> 
                        <col width="50">   
                        <col width="">  
                        <col width="120"> 
                        <col width="50">   
                        <col width="">                   
                    </colgroup>
                    <thead>
                        <tr>
                            <th>유입경로</th>
                            <th>건수</th>
                            <th class="tdr">미게임건수(%)</th>
                            <th style="padding-left:5px;">유입경로</th>
                            <th>건수</th>
                            <th class="tdr">미게임건수(%)</th>
                        </tr>
                    </thead>
                    <tbody>
<?
	$sql = "SELECT * ".
			"FROM ( ".
     		"	SELECT COUNT(*) AS adflag_count, platform, adflag, COUNT(IF(NOT EXISTS (SELECT * FROM tbl_user_stat_amazon WHERE useridx=tbl_user_ext.useridx),1,NULL)) AS nogame_adflag ".
     		"	FROM tbl_user_ext ". 
     		"	WHERE useridx>20000 AND createdate >= '$today 00:00:00' AND platform = 3 ".
     		"	GROUP BY adflag ". 
			") t1 ".
			"ORDER BY adflag_count DESC";
	
    $user_adflag_list = $db_main->gettotallist($sql);
    
    for ($i=0; $i<sizeof($user_adflag_list); $i++)
    {
        $adflag = $user_adflag_list[$i]["adflag"];
        $platform = $user_adflag_list[$i]["platform"];
        $adflag_count = $user_adflag_list[$i]["adflag_count"];
        $nogame_adflag = $user_adflag_list[$i]["nogame_adflag"];
        $os_str = "";
        
        if ($adflag == "" && $platform == 0)
            $adflag = "web_viral";
        else if ($adflag == "" && $platform == 1)
        	$adflag = "ios_viral";
        else if ($adflag == "" && $platform == 2)
        	$adflag = "android_viral";
        else if ($adflag == "" && $platform == 3)
        	$adflag = "amazon_viral";
		else if ($adflag != "" )
		{
			if ($platform == 0)
				$os_str = "(web)";
			else if ($platform == 1)
				$os_str = "(ios)";
			else if ($platform == 2)
				$os_str = "(android)";
			else if ($platform == 3)
				$os_str = "(amazon)";
		}
?>
					<tr>
						<td class="tdc"><?= $os_str.$adflag ?></td>
						<td class="tdc"><?= number_format($adflag_count) ?></td>
						<td class="tdr"><?= $nogame_adflag." (".(round($nogame_adflag / $adflag_count * 100))."%)" ?></td>
<?
        $i++;
        
        if (sizeof($user_adflag_list) > $i)
        {
            $adflag = $user_adflag_list[$i]["adflag"];
            $platform = $user_adflag_list[$i]["platform"];
            $adflag_count = $user_adflag_list[$i]["adflag_count"];
            $nogame_adflag = $user_adflag_list[$i]["nogame_adflag"];
            $os_str = "";
            
            if ($adflag == "" && $platform == 0)
           		$adflag = "web_viral";
	        else if ($adflag == "" && $platform == 1)
	        	$adflag = "ios_viral";
	        else if ($adflag == "" && $platform == 2)
	        	$adflag = "android_viral";
	        else if ($adflag == "" && $platform == 3)
	        	$adflag = "amazon_viral";
	        else if ($adflag != "" )
	        {
	        		if ($platform == 0)
	        			$os_str = "(web)";
	        		else if ($platform == 1)
	        			$os_str = "(ios)";
	        		else if ($platform == 2)
	        			$os_str = "(android)";
	        		else if ($platform == 3)
	        			$os_str = "(amazon)";
	        }
?>
						<td class="tdc" style="padding-left:5px;"><?= $os_str.$adflag ?></td>
						<td class="tdc"><?= number_format($adflag_count) ?></td>
						<td class="tdr"><?= $nogame_adflag." (".(round($nogame_adflag / $adflag_count * 100))."%)" ?></td>
<?      
		}
?>
					</tr>
<?      
    }
?>
                    </tbody>
                </table>
            </div>
            
            <div class="clear"></div>
            
            <div style="width:640px;float:left;margin:20px 0 0 20px;">
                <a style="font:12px;color:#000;font-weight:bold;cursor:ponter;">게임 접속/활동</a>
                <table class="tbl_list_basic1" style="margin-top:5px">
                    <colgroup>
                        <col width="">   
                        <col width="90">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="60">            
                    </colgroup>
                    <thead>
                        <tr>
                            <th class="tdl">게임명</th>
                            <th class="tdc">접속자수</th>
                            <th class="tdr">money in</th>
                            <th class="tdr">money out</th>
                            <th class="tdr">jackpot</th>
                            <th class="tdc">승률</th>
                        </tr>
                    </thead>
                    <tbody>
<?
    
    $total_user_count = 0;
    $total_moneyin = 0;
    $total_moneyout = 0;
    $total_jackpot = 0;
    $megajackpot_out = 0;
	$slotfreespin_out = 0;

		$game_name = 'Slot';
		$user_count = 0;
		$platform = 3;
        
        if($platform == 0)
        {
        	$table = "tbl_game_cash_stats_daily2";
        	$tail = "AND devicetype = 0";
        	$user_count = $db_analysis->getvalue("SELECT IFNULL(SUM(totalcount),0) FROM user_online_game_log_daily WHERE today = '$today'");
        }
        else if($platform == 1)
        {
        	$table = "tbl_game_cash_stats_ios_daily2";
        	$tail = "AND devicetype = 1";
        	$user_count = $db_analysis->getvalue("SELECT IFNULL(SUM(totalcount),0) FROM user_online_game_mobile_log_daily WHERE today = '$today' AND os_type = $platform");
        }
        else if($platform == 2)
        {
        	$table = "tbl_game_cash_stats_android_daily2";
        	$tail = "AND devicetype = 2";
        	$user_count = $db_analysis->getvalue("SELECT IFNULL(SUM(totalcount),0) FROM user_online_game_mobile_log_daily WHERE today = '$today' AND os_type = $platform");
        }
        else if($platform == 3)
        {
        	$table = "tbl_game_cash_stats_amazon_daily2";
        	$tail = "AND devicetype = 3";
        	$user_count = $db_analysis->getvalue("SELECT IFNULL(SUM(totalcount),0) FROM user_online_game_mobile_log_daily WHERE today = '$today' AND os_type = $platform");
        }
        
       	$jackpot = $db_analysis->getvalue("SELECT IFNULL(SUM(amount), 0) AS jackpotamount FROM tbl_jackpot_stat_daily WHERE today = '$today'  $tail");
        	
	    $sql = "SELECT IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout),0) AS moneyout
	       		FROM $table
	       		WHERE writedate = '$today' AND MODE NOT IN (5, 29,30,31)";
		$data = $db_main2->getarray($sql);
        

		
		if($platform == 0)
			$platform = "/images/icon/facebook.png";
		else if($platform == 1)
			$platform = "/images/icon/ios.png";
		else if($platform == 2)
			$platform = "/images/icon/android.png";
		else if($platform == 3)
			$platform = "/images/icon/ama.png";
		
        $moneyin = $data["moneyin"];
        $moneyout = $data["moneyout"];

		if ($moneyin == 0)
			$rate = 0;
		else
        	$rate = round(($moneyout+$jackpot) / $moneyin * 10000) / 100;
        
		$total_moneyin += $moneyin;
		$total_moneyout += $moneyout;

		$total_user_count += $user_count;
		$total_jackpot += $jackpot;

        
        if ($rate <= "80" || $rate >= "120")
        {
            	$style = "style='color:#fb7878;font-weight:bold;'";
        }
        else
            $style = "";
?>
                        <tr>
                            <td class="tdl" <?= $style ?>><?= $game_name ?> <img src="<?= $platform ?>" align="absmiddle" width="15" height="15"></td>
                            <td class="tdc" <?= $style ?>><?= make_price_format($user_count) ?></td>
                            <td class="tdr" <?= $style ?>><?= make_price_format($moneyin) ?></td>
                            <td class="tdr" <?= $style ?>><?= make_price_format($moneyout) ?></td>
                            <td class="tdr" <?= $style ?>><?= make_price_format($jackpot) ?></td>
                            <td class="tdc" <?= $style ?>><?= $rate."%" ?></td>
                        </tr>
<?
    $total_rate = round(($total_moneyout+$total_jackpot) / $total_moneyin * 10000) / 100;
?>
                        <tr>
                            <td class="tdl" style='font-weight:bold;'>합계</td>
                            <td class="tdc" style='font-weight:bold;'><?= make_price_format($total_user_count) ?></td>
                            <td class="tdr"><?= make_price_format($total_moneyin) ?></td>
                            <td class="tdr"><?= make_price_format($total_moneyout) ?></td>
                            <td class="tdr"><?= make_price_format($total_jackpot) ?></td>
                            <td class="tdc" style='font-weight:bold;'><?= $total_rate."%" ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <div style="width:590px;float:left;margin:20px 0 0 40px;">
                <a href="/m5_monitoring/online_log_stats.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">현재 사용자 접속 상태</a>
                <div id="chart_div1" style="height:300px;width:590px;"></div>
            </div>
            
            <div style="width:585px;float:left;margin:20px 0 0 20px;">
                <a style="font:12px;color:#000;font-weight:bold;cursor:ponter;">오늘 게임 활동 추이</a>
                <table class="tbl_list_basic1" style="margin-top:5px">
                    <colgroup>
                        <col width="100">
                        <col width="100">
                        <col width="100"> 
                        <col width="100">               
                    </colgroup>
                    <tbody>
<?
    $sql = "SELECT IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout),0) AS moneyout, LEFT(writedate, 10) AS writedate ".
            "FROM tbl_game_cash_stats_amazon WHERE writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";    
    $action_info = $db_main2->getarray($sql);    
        
    $jackpot_amount = $db_analysis->getvalue("SELECT IFNULL(SUM(amount),0) FROM tbl_jackpot_stat_daily WHERE devicetype=3 AND today = '$today'");
    $rate = round($action_info["moneyin"]==0 ? 0 : ($action_info["moneyout"]+$jackpot_amount) / $action_info["moneyin"] * 100, 2);
    
    $user_count = $db_analysis->getvalue("SELECT SUM(totalcount) FROM user_online_game_mobile_log WHERE os_type = 3 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
    $game_profit = $action_info["moneyin"] - ($action_info["moneyout"] + $jackpot_amount);
    $free_amount = $db_analysis->getvalue("SELECT SUM(freeamount) FROM tbl_user_freecoin_stat WHERE category = 1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
    $order_amount = $db_main->getvalue("SELECT SUM(coin) FROM tbl_product_order_mobile WHERE os_type = 3 AND status=1 AND useridx>20000 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
    $purchase_amount = $order_amount;
    
    $total_margin = $game_profit - ($free_amount + $purchase_amount + $free_amount_play);
     if($total_margin < 0 )
        $total_margin ="-".make_price_format(abs($total_margin));
    else
	$total_margin =make_price_format(abs($total_margin));
    
	$free_amount_play = 0;
?>
                        <tr>
                            <th class="tdc" style="border-top:1px solid #e7e7e7;">money_in (A)</th>
                            <td class="tdr" style="border-top:1px solid #e7e7e7;"><?= make_price_format($action_info["moneyin"]) ?></td>
                            <th class="tdc" style="border-top:1px solid #e7e7e7;">접속자수</th>
                            <td class="tdr" style="border-top:1px solid #e7e7e7;"><?= make_price_format($user_count) ?></td>
                        </tr>
                        <tr>
                            <th class="tdc" style="border-top:1px solid #e7e7e7;">money_out (B)</th>
                            <td class="tdr" style="border-top:1px solid #e7e7e7;"><?= make_price_format($action_info["moneyout"]) ?></td>
                            <th class="tdc">승률 ((B+C)/A)</th>
                            <td class="tdr"><?= $rate."%" ?></td>
                        </tr>
                        <tr>
                            <th class="tdc">jackpot (C)</th>
                            <td class="tdr"><?= make_price_format($jackpot_amount) ?></td>
                            <th class="tdc">게임횟수</th>
                            <td class="tdr"><?= make_price_format($action_info["playcount"]) ?></td>
                        </tr>
                        <tr>
                            <th class="tdc">게임이익 (A-(B+C)=D)</th>
                            <td class="tdr"><?= make_price_format($game_profit) ?></td>
                        </tr>
                        <tr>
                            <th class="tdc">구매코인 (E)</th>
                            <td class="tdr"><?= make_price_format($purchase_amount) ?></td>
                        </tr>
                        <tr>
                            <th class="tdc">활동 무료코인 (F)</th>
                            <td class="tdr"><?= make_price_format($free_amount_play) ?></td>
                        </tr>
                        <tr>
                            <th class="tdc">무료코인 (G)</th>
                            <td class="tdr"><?= make_price_format($free_amount) ?></td>
                        </tr>
                        <tr>
                            <th class="tdc">총 이익 (D-(E+F+G))</th>
                            <td class="tdr"><?=$total_margin?></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="clear"></div>
            
            <div style="width:380px;float:left;margin:20px 0 0 20px;">
                <a href="/m5_monitoring/client_log_stats_ios.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">클라이언트 통신 로그</a>
                <div id="chart_div2" style="height:300px;width:380px;"></div>
            </div>
            
            <div style="width:380px;float:left;margin:20px 0 0 30px;">
                <a href="/m5_monitoring/server_log_stats_ios.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">서버 통신 로그</a>
                <div id="chart_div3" style="height:300px;width:380px;"></div>
            </div>
            
            <div style="width:380px;float:left;margin:20px 0 0 30px;">
                <a href="/m5_monitoring/serverevent_log_stats_mobile.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">서버 이벤트 로그</a>
                <div id="chart_div4" style="height:300px;width:380px;"></div>
            </div>

        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?  
    $db_main->end();
    $db_main2->end();
    $db_analysis->end();
    $db_friend->end();
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
