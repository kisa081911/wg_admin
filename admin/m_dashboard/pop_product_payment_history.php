<? 
    include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
    
    check_login_layer();
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    
    $pagename = "pop_user_jackpot_count.php";
    
    $startdate = ($_GET["startdate"] == "") ? date("Y-m-d") : $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
    $platform = ($_GET["platform"] == "") ? "ALL" : $_GET["platform"];
    
    $db_main = new CDatabase_Main();
    
    $str_useridx=20000;
    
    if($platform != "ALL")
    	$platform_tail = "WHERE category = $platform";
    
    $today = date("Y-m-d", time());	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>오늘 상품별 결제 내역</title>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>  
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	function search()
	{
	    var search_form = document.search_form;
	    search_form.submit();
	}

	$(function() {
        $("#startdate").datepicker({ });
    });

    $(function() {
        $("#enddate").datepicker({ });
    });

    function layer_close()
    {
        window.parent.close_layer_popup("<?= $_GET["layer_no"] ?>");
    }
</script>
</head>
<body class="layer_body"  onload="try { window_onload() } catch(e) {}" onkeyup="if (getEventKeycode(event) == 27) { layer_close(); }">
<div id="layer_wrap">   
    <div class="layer_header" >
        <div class="layer_title">오늘 상품별 결제 내역</div>
        <img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="layer_close()" />
    </div>
    <div style="width:640;height:690px;overflow-y:auto;">
         <!--  레이어 내용  -->
         
         <div class="layer_user_fix_height">
         	<form name="search_form" id="search_form" onsubmit="return false">
            	플랫폼
		   	<select name="platform" id="platform">
		    	<option value="ALL" value="ALL" <?= ($platform == "ALL") ? "selected" : "" ?>>전체</option>
		        <option value="0" value="0" <?= ($platform == "0") ? "selected" : "" ?>>Web</option>
		        <option value="1" value="1" <?= ($platform == "1") ? "selected" : "" ?>>iOS</option>
		        <option value="2" value="2" <?= ($platform == "2") ? "selected" : "" ?>>Android</option>
		        <option value="3" value="3" <?= ($platform == "3") ? "selected" : "" ?>>Amazon</option>
		    </select>
		    
				날짜 : <input type="input" class="search_text" id="startdate" name="startdate" style="width:70px" value="<?= $startdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
					- <input type="input" class="search_text" id="enddate" name="enddate" style="width:70px" value="<?= $enddate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
		    </form>
		    <input style="float: right;"type="button" class="btn_search" value="검색" onclick="search()" />
		    
            <table class="tbl_list_basic1" style="margin-top:5px">
                    <colgroup>
                        <col width="160"> 
                        <col width="60"> 
                        <col width="60"> 
                        <col width="160"> 
                        <col width="60"> 
                        <col width="60">               
                    </colgroup>
                    <thead>
                        <tr>
                            <th>상품명</th>
                            <th class="tdc">건수</th>
                            <th class="tdr">금액</th>
                            <th style="padding-left:8px;">상품명</th>
                            <th class="tdc">건수</th>
                            <th class="tdr">금액</th>
                        </tr>
                    </thead>
                    <tbody>
<?
			
	$sql = "SELECT *
			FROM (
				SELECT 0 AS category, t1.productidx, t2.product_type, FORMAT(t2.facebookcredit / 10, 2) AS product, FORMAT(t2.facebookcredit / 10, 2) AS productname, imageurl, paycount, t1.facebookcredit AS money, is_discount
				FROM (
				SELECT productidx, COUNT(productidx) AS paycount, ROUND(IFNULL(SUM(facebookcredit),0))/10 AS facebookcredit, IF(special_discount <> 0, 1, 0) AS is_discount
				FROM tbl_product_order
				WHERE STATUS IN (1,3) AND useridx>$str_useridx  AND writedate >= '$today 00:00:00'
				GROUP BY productidx, is_discount
				UNION ALL
				SELECT 0 AS productidx, COUNT(orderidx) AS paycount, ROUND(IFNULL(SUM(money), 0), 1) AS facebookcredit, 0 AS is_discount
				FROM tbl_product_order_earn
				WHERE STATUS=1 AND useridx>$str_useridx AND writedate >= '$today 00:00:00'
			) t1 LEFT JOIN tbl_product t2 ON t1.productidx = t2.productidx
			WHERE paycount > 0
			UNION ALL
			SELECT os_type as category, t1.productidx, t1.product_type, FORMAT(t2.money, 2)	 AS product, FORMAT(t2.money, 2) AS productname, imageurl, paycount, t1.money, is_discount
				FROM (
						SELECT productidx, COUNT(productidx) AS paycount, ROUND(IFNULL(SUM(money),0)) AS money, IF(special_discount <> 0, 1, 0) AS is_discount,product_type
						FROM tbl_product_order_mobile
						WHERE STATUS = 1 AND useridx>$str_useridx AND writedate >= '$today 00:00:00'
						GROUP BY productidx,product_type, is_discount
				) t1 LEFT JOIN tbl_product_mobile t2 ON t1.productidx = t2.productidx
			WHERE paycount > 0
			) t1
			$platform_tail
			ORDER BY money DESC ";

    $productlist = $db_main->gettotallist($sql);
       
    for($i=0; $i<sizeof($productlist); $i++)
    {
         $category = $productlist[$i]["category"];
         $productidx = $productlist[$i]["productidx"];
         $product = $productlist[$i]["product"];
         $productname = $productlist[$i]["productname"];
         $imageurl = $productlist[$i]["imageurl"];
         $facebookcredit = $productlist[$i]["money"];
         $paycount = $productlist[$i]["paycount"];
         $is_discount = $productlist[$i]["is_discount"];
         $product_type = $productlist[$i]["product_type"];
         
         
     if($productidx == "0")
         {
         	$productname = "facebok earn";
         	$imageurl = "/images/icon/facebook.png";
         }
         else if($product_type == 1)
         {
         	$productname = $product."(Basic)";
         }         
         else if($product_type == 2)
         {
         	$productname = $product."(Season)";
         }         
         else if($product_type == 3)
         {
         	$productname = $product."(Threshold)";
         }
         else if($product_type == 4)
         {
         	$productname = $product."(Whale)";
         }
         else if($product_type == 5)
         {
         	$productname = $product."(28 Retention)";
         }
         else if($product_type == 6)
         {
         	$productname = $product."(First)";
         }
         else if($product_type == 7)
         {
         	$productname = $product."(Lucky)";
         }
         else if($product_type == 8)
         {
         	$productname = $product."(Monthly)";
         }
         else if($product_type == 9)
         {
         	$productname = $product."(Piggypot)";
         }
         else if($product_type == 10)
         {
         	$productname = $product."(Buyerleave)";
         }
         else if($product_type == 11)
         {
         	$productname = $product."(Nopayer)";
         }
         else if($product_type == 12)
         {
         	$productname = $product."(primedeal_1)";
         }
         else if($product_type == 13)
         {
         	$productname = $product."(primedeal_2)";
         }
    	 else if($product_type == 14)
         {
             $productname = $product."(Attend)";
         }
         else if($product_type == 15)
         {
             $productname = $product."(Platinum Deal)";
         }
         else if($product_type == 16)
         {
         	$productname = $product."(Amazing deal)";
         }
         else if($product_type == 17)
         {
         	$productname = $product."(First Attend)";
         }
         else if($product_type == 18)
         {
             $productname = $product."(Super Deal)";
         }
         else if($product_type == 19)
         {
             $productname = $product."(Speed Wheel)";
         }
         else if($product_type == 20)
         {
             $productname = $product."(Collection Deal)";
         }
         
         if($category == 0)
         	$category = "/images/icon/facebook.png";
         else if($category == 1)
         	$category = "/images/icon/ios.png";
         else if($category == 2)
         	$category = "/images/icon/android.png";
         else if($category == 3)
         	$category = "/images/icon/ama.png";
?>
                        <tr>
                            <td class="tdl point"><img src="<?=$imageurl?>" align="absmiddle" width="25" height="25"> <?= $productname ?> <img src="<?=$category?>" align="absmiddle" width="15" height="15"></td>
                            <td class="tdc"><?= make_price_format($paycount) ?>건</td>
                            <td class="tdr">$<?= number_format($facebookcredit, 1) ?></td>
<?
        $i++;
        
        if (sizeof($productlist) > $i)
        {
        	$category = $productlist[$i]["category"];
            $productidx = $productlist[$i]["productidx"];
            $product = $productlist[$i]["product"];
            $productname = $productlist[$i]["productname"];
            $imageurl = $productlist[$i]["imageurl"];
            $facebookcredit = $productlist[$i]["money"];
            $paycount = $productlist[$i]["paycount"]; 
            $is_discount = $productlist[$i]["is_discount"];
            $product_type = $productlist[$i]["product_type"];
             
             
	         if($productidx == "0")
	         {
	         	$productname = "facebok earn";
	         	$imageurl = "/images/icon/facebook.png";
	         }
	         else if($product_type == 1)
	         {
	         	$productname = $product."(Basic)";
	         }         
	         else if($product_type == 2)
	         {
	         	$productname = $product."(Season)";
	         }         
	         else if($product_type == 3)
	         {
	         	$productname = $product."(Threshold)";
	         }
	         else if($product_type == 4)
	         {
	         	$productname = $product."(Whale)";
	         }
	         else if($product_type == 5)
	         {
	         	$productname = $product."(28 Retention)";
	         }
	         else if($product_type == 6)
	         {
	         	$productname = $product."(First)";
	         }
	         else if($product_type == 7)
	         {
	         	$productname = $product."(Lucky)";
	         }
	         else if($product_type == 8)
	         {
	         	$productname = $product."(Monthly)";
	         }
	         else if($product_type == 9)
	         {
	         	$productname = $product."(Piggypot)";
	         }
	         else if($product_type == 10)
	         {
	         	$productname = $product."(Buyerleave)";
	         }
	         else if($product_type == 11)
	         {
	         	$productname = $product."(Nopayer)";
	         }
	         else if($product_type == 12)
	         {
	         	$productname = $product."(primedeal_1)";
	         }
	         else if($product_type == 13)
	         {
	         	$productname = $product."(primedeal_2)";
	         }
        	 else if($product_type == 14)
	         {
	             $productname = $product."(Attend)";
	         }
	         else if($product_type == 15)
	         {
	             $productname = $product."(Platinum Deal)";
	         }
	         else if($product_type == 16)
	         {
	         	$productname = $product."(Amazing deal)";
	         }
	         else if($product_type == 17)
	         {
	         	$productname = $product."(First Attend)";
	         }
	         else if($product_type == 18)
	         {
	             $productname = $product."(Super Deal)";
	         }
	         else if($product_type == 19)
	         {
	             $productname = $product."(Speed Wheel)";
	         }
	         else if($product_type == 20)
	         {
	             $productname = $product."(Collection Deal)";
	         }
	         
	         
	         if($productidx == 47 || $productidx == 48 || $productidx == 49 || $productidx == 50 )
	             $productname = $productname."(First A/B)";
	         
	         if($category == 0)
	         	$category = "/images/icon/facebook.png";
	         else if($category == 1)
         		$category = "/images/icon/ios.png";
	         else if($category == 2)
       			$category = "/images/icon/android.png";
	         else if($category == 3)
	        	$category = "/images/icon/ama.png";
?>
                            <td class="tdl point" style="border-left:1px solid #e7e7e7;padding-left:8px;"><img src="<?=$imageurl?>" align="absmiddle" width="25" height="25"> <?= $productname ?> <img src="<?=$category?>" align="absmiddle" width="15" height="15"></td>
                            <td class="tdc"><?= number_format($paycount) ?>건</td>
                            <td class="tdr">$<?= number_format($facebookcredit, 1) ?></td>
<?
        }
?>
                        </tr>
<?
    }
    
    $db_main->end();
?>
                    </tbody>
                </table> 
            </form>  
         </div>
    </div>
</div>
</body>
</html>

