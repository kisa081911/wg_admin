<?
    $top_menu = "dashboard";
    $sub_menu = "";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

    // 최근 6시간 3분 term
    $term = "3";
    $os_term = 1;
    
    $end_date = date("Y-m-d", time());
    $end_time = date("H:i:s", time());
    
    $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time))-6,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
    $start_time = date("H:i:s", mktime(date("H", strtotime($end_time))-6,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));

    $user_where = " WHERE isnpc=0 ";
    $late_where = " WHERE latency > 0  AND errcode = 0";
    $error_where = " WHERE errcode != '0' ";
    $event_where = "WHERE 1=1 ";
    $warningevent_where = " WHERE category=2 ";
    $errorevent_where = " WHERE category=3 ";
    
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();
    $db_analysis = new CDatabase_Analysis();
    
    // 현재 사용자 접속 상태
    
    // 일반 유저
    $sql = "SELECT SUM(iosusercount) AS iosusercount, SUM(androidusercount) AS androidusercount, SUM(amazonusercount) AS amazonusercount, minute ".
    		"FROM ( ".
    		"	SELECT SUM(totalcount)/COUNT(DISTINCT logkey) AS iosusercount, 0 AS androidusercount, 0 AS amazonusercount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"	FROM user_online_mobile_log ".
    		"	$user_where AND os_type=1 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		" 	GROUP BY minute ".
    		"	UNION ALL ".
    		"	SELECT 0 AS iosusercount, SUM(totalcount)/COUNT(DISTINCT logkey) AS androidusercount, 0 AS amazonusercount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"	FROM user_online_mobile_log ".
    		"	$user_where AND os_type=2  AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		" 	GROUP BY minute ".
    		"	UNION ALL ".
    		"	SELECT 0 AS iosusercount, 0 AS androidusercount, SUM(totalcount)/COUNT(DISTINCT logkey) AS amazonusercount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"	FROM user_online_mobile_log ".
    		"	$user_where AND os_type=3  AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		" 	GROUP BY minute ".
    		") t1 ".
    		"GROUP BY minute ";    
    $userlist = $db_analysis->gettotallist($sql);
     
    // 클라이언트 통신 로그
    // 늦은 응답 건수, 에러 건수
        
    // 늦은 응답 건수
    $sql = "SELECT IFNULL(COUNT(*), 0) AS latecount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM client_communication_log_ios $late_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $latelist = $db_analysis->gettotallist($sql);
    
    // 에러 건수
    $sql = "SELECT IFNULL(COUNT(*), 0) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM client_communication_log_ios $error_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
     
    $errorlist = $db_analysis->gettotallist($sql);
    
    
    // 서버 통신 로그
    // 늦은 응답 건수, 에러 건수
    $sql = "SELECT IFNULL(COUNT(*), 0) AS latecount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_communication_log_mobile $late_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $server_latelist = $db_analysis->gettotallist($sql);
    
    // 에러 건수
    $sql = "SELECT IFNULL(COUNT(*), 0) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_communication_log_mobile $error_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $server_errorlist = $db_analysis->gettotallist($sql);
    
    // 서버 이벤트 로그
   // 서버 이벤트 warning로그
    $sql = "SELECT IFNULL(COUNT(*), 0) AS warningcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_log_mobile $warningevent_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $warningevent_list = $db_analysis->gettotallist($sql);
     
    // 서버 이벤트 error로그
    $sql = "SELECT IFNULL(COUNT(*), 0) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_log_mobile $errorevent_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $errorevent_list = $db_analysis->gettotallist($sql);
    
    $saved_isrefresh = $_COOKIE["saved_isrefresh"];
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
      
    function drawChart() 
    {
        var datatable1 = new google.visualization.DataTable();
        
        datatable1.addColumn('string', '시간');
        datatable1.addColumn('number', 'IOS');
        datatable1.addColumn('number', 'Android');
        datatable1.addColumn('number', 'Amazon');
        datatable1.addRows([
<?
    $start = round(mktime(date("H",strtotime($start_time)), date("i",strtotime($start_time)), date("s", strtotime($start_time)), date("m",strtotime($start_date)), date("d", strtotime($start_date)), date("Y", strtotime($start_date)))/($term*60));
    $end = round(mktime(date("H",strtotime($end_time)), date("i",strtotime($end_time)), date("s", strtotime($end_time)), date("m",strtotime($end_date)), date("d", strtotime($end_date)), date("Y", strtotime($end_date)))/($term*60)) + 1;
    
    for ($i=$start; $i<$end; $i=$i+1)
    {      
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        if ($term > 10080)
            echo("['".substr($list_date, 0, 7)."'");  
        else if ($term > 720)
            echo("['$list_date'");        
        else
            echo("['".make_time_format($temp_minute)."'");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
        	if ($userlist[$j]["minute"] == $i)
        	{
        		echo(",{v:".round($userlist[$j]["iosusercount"]).",f:'".make_price_format(round($userlist[$j]["iosusercount"]))."'}");
        		$print = true;
        		break;
        	}
        }
        
        if (!$print)
        	echo(",0");
         
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
        	if ($userlist[$j]["minute"] == $i)
        	{
        		echo(",{v:".round($userlist[$j]["androidusercount"]).",f:'".make_price_format(round($userlist[$j]["androidusercount"]))."'}");
        		$print = true;
        		break;
        	}
        }
        
        if (!$print)
        	echo(",0");
         
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
	        if ($userlist[$j]["minute"] == $i)
	        {
	        	echo(",{v:".round($userlist[$j]["amazonusercount"]).",f:'".make_price_format(round($userlist[$j]["amazonusercount"]))."'}");
	        	$print = true;
	        			break;
	        }
        }
        
        if (!$print)
        	echo(",0");
                   
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");                    
    }    
?>          
        ]);

        var datatable2 = new google.visualization.DataTable();
        
        datatable2.addColumn('string', '시간');
        datatable2.addColumn('number', '늦은응답건수');
        datatable2.addColumn('number', '에러건수');
        datatable2.addRows([
<?
	for ($i=$start; $i<$end; $i=$i+1)
	{      
		$temp_time = explode(":",date("H:i:s", $i*($term*60)));        
		$temp_minute = $temp_time[0]*60 + $temp_time[1];
      
		$list_date = date("Y-m-d", $i*($term*60));
        
		if ($term > 10080)
			echo("['".substr($list_date, 0, 7)."'");  
		else if ($term > 720)
			echo("['$list_date'");        
		else
			echo("['".make_time_format($temp_minute)."'");
        
		$print = false;
		for ($j=0; $j<sizeof($latelist); $j++)
		{
			if ($latelist[$j]["minute"] == $i)
			{
				echo(",{v:".$latelist[$j]["latecount"].",f:'".number_format($latelist[$j]["latecount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
        
		$print = false;
		for ($j=0; $j<sizeof($errorlist); $j++)
		{
			if ($errorlist[$j]["minute"] == $i)
			{
				echo(",{v:".$errorlist[$j]["errorcount"].",f:'".number_format($errorlist[$j]["errorcount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
        
		if ($i+1 >= $end)
			echo("]");  
		else
			echo("],");               
	}   
?>          
        ]);
        
        var datatable3 = new google.visualization.DataTable();
        
        datatable3.addColumn('string', '시간');
        datatable3.addColumn('number', '늦은응답건수');
        datatable3.addColumn('number', '에러건수');
        datatable3.addRows([
<?
	for ($i=$start; $i<$end; $i=$i+1)
	{      
		$temp_time = explode(":",date("H:i:s", $i*($term*60)));        
		$temp_minute = $temp_time[0]*60 + $temp_time[1];
      
		$list_date = date("Y-m-d", $i*($term*60));
        
		if ($term > 10080)
			echo("['".substr($list_date, 0, 7)."'");  
		else if ($term > 720)
			echo("['$list_date'");        
		else
			echo("['".make_time_format($temp_minute)."'");
        
		$print = false;
		for ($j=0; $j<sizeof($server_latelist); $j++)
		{
			if ($server_latelist[$j]["minute"] == $i)
			{
				echo(",{v:".$server_latelist[$j]["latecount"].",f:'".number_format($server_latelist[$j]["latecount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
           
		$print = false;
		for ($j=0; $j<sizeof($server_errorlist); $j++)
		{
			if ($server_errorlist[$j]["minute"] == $i)
			{
				echo(",{v:".$server_errorlist[$j]["errorcount"].",f:'".number_format($server_errorlist[$j]["errorcount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
        
		if ($i+1 >= $end)
			echo("]");  
		else
			echo("],");                
	}
?>          
        ]);
        
        var datatable4 = new google.visualization.DataTable();
        
        datatable4.addColumn('string', '시간');
        datatable4.addColumn('number', 'Waring 건수');
        datatable4.addColumn('number', 'Error 건수');
        datatable4.addRows([
<?
	for ($i=$start; $i<$end; $i=$i+1)
	{      
		$temp_time = explode(":",date("H:i:s", $i*($term*60)));        
		$temp_minute = $temp_time[0]*60 + $temp_time[1];
      
		$list_date = date("Y-m-d", $i*($term*60));
        
		if ($term > 10080)
			echo("['".substr($list_date, 0, 7)."'");  
		else if ($term > 720)
			echo("['$list_date'");        
		else
			echo("['".make_time_format($temp_minute)."'");
        
		$print = false;
		for ($j=0; $j<sizeof($warningevent_list); $j++)
		{
			if ($warningevent_list[$j]["minute"] == $i)
			{
				echo(",{v:".$warningevent_list[$j]["warningcount"].",f:'".number_format($warningevent_list[$j]["warningcount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
        
		$print = false;
		for ($j=0; $j<sizeof($errorevent_list); $j++)
		{
			if ($errorevent_list[$j]["minute"] == $i)
			{
				echo(",{v:".$errorevent_list[$j]["errorcount"].",f:'".number_format($errorevent_list[$j]["errorcount"])."'}");
				$print = true;      
				break;
			}
		}
        
		if (!$print)
			echo(",0");
        
		if ($i+1 >= $end)
			echo("]");  
		else
			echo("],");         
	}
?>          
        ]);

        var options1 = {          
                axisTitlesPosition:'in',  
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize:12,
                colors: ['#0000CD', '#008000', '#D2691E', '#C0C0C0', '#FFD700'],
                chartArea:{left:50,top:30,width:595,height:200}
            };
        
		var options2 = {          
                axisTitlesPosition:'in',
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize:12,
                chartArea:{left:85,top:30,width:370,height:200}
            };
        
		var options3 = {      
                axisTitlesPosition:'in',
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize:12,
                chartArea:{left:85,top:30,width:370,height:200}
            };
        
		var options4 = {       
                axisTitlesPosition:'in',  
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize:12,
                chartArea:{left:85,top:30,width:370,height:200}
            };

		var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
		chart.draw(datatable1, options1);
            
		chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
		chart.draw(datatable2, options2);
            
		chart = new google.visualization.LineChart(document.getElementById('chart_div3'));
		chart.draw(datatable3, options3);
            
		chart = new google.visualization.LineChart(document.getElementById('chart_div4'));
		chart.draw(datatable4, options4);
	}

	google.setOnLoadCallback(drawChart);

	function check_sleeptime_dashboard()
    {
        if ("<?= $saved_isrefresh ?>" == "1")
            setTimeout("window.location.reload(false)", 20 * 60 * 1000);
    }
	    
    function refresh()
    {
        window.location.reload(false);
    }
	 
    function set_cookie(name, checked, days) 
    {
        if (days) 
        {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString(); 
        } 
        else 
        {
           var expires = "";
        }
        
        if (checked)
            value = "1";
        else
            value = "0";
            
        document.cookie = name + "=" + value + expires + "; path=/";   
        window.location.reload(false);     
    }
    
    function change_os_term(term)
	{
		if (term == "ALL")
			window.location.href = 'dashboard_all.php';
		else if (term == "0")
			window.location.href = 'dashboard.php';
		else if (term == "1")
			window.location.href = 'dashboard_ios.php';
		else if (term == "2")
			window.location.href = 'dashboard_android.php';
		else if (term == "3")
			window.location.href = 'dashboard_amazon.php';
	}
</script>
<!-- CONTENTS WRAP -->
	<div class="contents_wrap_wide">        
		<!-- title_warp -->
		<div class="title_wrap">
			<input type="checkbox" name="isrefresh" id="isrefresh" value="1" align="absmiddle" onclick="set_cookie('saved_isrefresh', this.checked, '5')" <?= ($saved_isrefresh == "1") ? "checked" : "" ?> /> 20분마다 새로고침
                    <img src="/images/icon/refresh.png" align="absmiddle" style="cursor:pointer" align="absmiddle" width="23px" height="23px" onclick="refresh()"/>
                    
                    &nbsp;&nbsp;&nbsp;
			
			<input type="button" class="<?= ($os_term == "ALL") ? "btn_schedule_select" : "btn_schedule" ?>" value="통합" id="term_all" onclick="change_os_term('ALL')"    />
			<input type="button" class="<?= ($os_term == "0") ? "btn_schedule_select" : "btn_schedule" ?>" value="Web" id="term_web" onclick="change_os_term('0')"    />
			<input type="button" class="<?= ($os_term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_os_term('1')" />
			<input type="button" class="<?= ($os_term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_os_term('2')"    />
			<input type="button" class="<?= ($os_term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_os_term('3')"    />
		</div>
		<!-- //title_warp -->
<?
	$str_useridx = 20000;
	// 개발 서버용
	if (WEB_HOST_NAME == "take5-dev.doubleugames.com")
		$str_useridx = 10000;
	
    $today = date("Y-m-d");
    $yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));
    
    $sql = "SELECT (SELECT value FROM tbl_dashboard_stat WHERE type = 101 AND subtype = 1) AS order_28day, ".
    		"(SELECT value FROM tbl_dashboard_stat WHERE type = 101 AND subtype = 2) AS order_cancel_28day, ".
    		"ROUND((SELECT value FROM tbl_dashboard_stat WHERE type = 101 AND subtype = 3)) AS count_28day, ".
    		"(SELECT value FROM tbl_dashboard_stat WHERE type = 101 AND subtype = 4) AS order_7day, ".
    		"(SELECT value FROM tbl_dashboard_stat WHERE type = 101 AND subtype = 5) AS order_cancel_7day, ".
    		"ROUND((SELECT value FROM tbl_dashboard_stat WHERE type = 101 AND subtype = 6)) AS count_7day, ".
    		"(SELECT value FROM tbl_dashboard_stat WHERE type = 101 AND subtype = 7) AS order_yesterday, ".
    		"(SELECT value FROM tbl_dashboard_stat WHERE type = 101 AND subtype = 8) AS order_cancel_yesterday, ".
    		"ROUND((SELECT value FROM tbl_dashboard_stat WHERE type = 101 AND subtype = 9)) AS count_yesterday ";    
    $order_info = $db_analysis->getarray($sql);
    
    $order_today = $db_main->getvalue("SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE os_type=1 AND useridx > $str_useridx AND status=1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
    $order_cancel_today = $db_main->getvalue("SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile_cancel WHERE os_type=1 AND useridx > $str_useridx AND canceldate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
    $order_count_today =  $db_main->getvalue("SELECT COUNT(*) FROM tbl_product_order_mobile WHERE os_type=1 AND useridx > $str_useridx AND status=1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
    
    $today_total_credit = round($order_today);
    
    $basedate = date("Y-m-d", time() - 14 * 24 * 60 * 60);
    $basedate2 = date("Y-m-d", time());
        
    $sql = "SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE os_type=1 AND useridx > $str_useridx AND status=1 AND writedate >= '$basedate 00:00:00' AND writedate<'$basedate2' AND UNIX_TIMESTAMP(writedate) % (24 * 60 * 60) <= UNIX_TIMESTAMP(NOW()) % (24 * 60 * 60)";
    $time_credit = $db_main->getvalue($sql);
    
    $sql = "SELECT IFNULL(SUM(money),0) FROM tbl_product_order_mobile WHERE os_type=1 AND useridx > $str_useridx AND status=1 AND writedate >= '$basedate 00:00:00' AND writedate<'$basedate2'";
    $total_credit1 = $db_main->getvalue($sql);

    if ($time_credit != 0 && $today_total_credit != 0)
    {
        $estimated_credit = $today_total_credit * $total_credit1 / $time_credit;
    }
    else 
        $estimated_credit = 0;
    
    // 기간별 할인율 계산
    $sql = 	"SELECT  ROUND((IFNULL(SUM(coin), 0)- IFNULL(SUM(basecoin), 0))/IFNULL(SUM(basecoin), 0)*100, 2) ".
    		"FROM tbl_product_order_mobile ".
    		"WHERE os_type = 1 AND useridx > $str_useridx AND status = 1  AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";
    $salerate_today = $db_main->getvalue($sql);
    
    $sql = "SELECT (SELECT value FROM tbl_dashboard_stat WHERE type = 102 AND subtype = 1) AS salerate_28days, ".
    		"(SELECT value FROM tbl_dashboard_stat WHERE type = 102 AND subtype = 2) AS salerate_7days, ".
    		"(SELECT value FROM tbl_dashboard_stat WHERE type = 102 AND subtype = 3) AS salerate_yesterday";  


    $sale_info = $db_analysis->getarray($sql);
    
    $salerate_yesterday = $sale_info["salerate_yesterday"];
    $salerate_7days = $sale_info["salerate_7days"];
    $salerate_28days = $sale_info["salerate_28days"];
    
    // 기간별 비할인 구매율 계산
    $sql = 	"SELECT SUM(money) ".
    		"FROM tbl_product_order_mobile t1 ".
    		"WHERE STATUS = 1 AND useridx > $str_useridx AND os_type = 1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59' AND couponidx = 0 AND special_discount = 0 AND special_more = 0;";    		
	$normalpurchase_today = $db_main->getvalue($sql);
    
    $sql = "SELECT (SELECT value FROM tbl_dashboard_stat WHERE type = 103 AND subtype = 1) AS normalpurchase_28days, ".
    		"(SELECT value FROM tbl_dashboard_stat WHERE type = 103 AND subtype = 2) AS normalpurchase_7days, ".
    		"(SELECT value FROM tbl_dashboard_stat WHERE type = 103 AND subtype = 3) AS normalpurchase_yesterday";    
    $normalpurchase_info = $db_analysis->getarray($sql);
    
    $normalpurchase_yesterday = $normalpurchase_info["normalpurchase_yesterday"];
    $normalpurchase_7days = $normalpurchase_info["normalpurchase_7days"];
    $normalpurchase_28days = $normalpurchase_info["normalpurchase_28days"];
?>
            <div style="width:585px;float:left;margin-left:20px;">
                <a href="/m1_product/order_mobile_ios.php?status=1" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">결제내역</a>
                <table class="tbl_list_basic1" style="margin-top:5px">
                    <colgroup>
                        <col width="60">
                        <col width="50">
                        <col width="120"> 
						<col width="120"> 
                        <col width="100"> 
                        <col width="60">                
                    </colgroup>
                    <thead>
                        <tr>
                            <th class="tdc">기간</th>
                            <th>건수</th>
                            <th class="tdr">구매금액(할인율%)</th>
							<th class="tdr">비할인구매금액(%)</th>
                            <th class="tdr">취소금액(%)</th>
                            <th class="tdr">예상금액</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="tdc point">최근28일</td>
                            <td class="tdc"><?= make_price_format($order_info["count_28day"]) ?></td>
                            <td class="tdr">$<?= number_format(round($order_info["order_28day"], 1), 1) ?> (<?= round($salerate_28days, 1)?>%)</td>
							<td class="tdr">$<?= number_format(round($normalpurchase_28days, 1), 1) ?> (<?= ($order_info["order_28day"] == 0)?0:round($normalpurchase_28days*100/$order_info["order_28day"], 1)?>%)</td>
                            <td class="tdr">$<?= number_format(round($order_info["order_cancel_28day"], 1), 1)?>(<?=($order_info["order_28day"] == 0)?0:round($order_info["order_cancel_28day"] / $order_info["order_28day"] * 100, 2)?>%)</td>
                            <td class="tdr">-</td>
                        </tr>
                        <tr>
                            <td class="tdc point">최근7일</td>
                            <td class="tdc"><?= make_price_format($order_info["count_7day"]) ?></td>
                            <td class="tdr">$<?= number_format(round($order_info["order_7day"], 1), 1) ?> (<?= round($salerate_7days, 1)?>%)</td>
							<td class="tdr">$<?= number_format(round($normalpurchase_7days, 1), 1) ?> (<?= ($order_info["order_7day"] == 0)?0:round($normalpurchase_7days*100/$order_info["order_7day"], 1)?>%)</td>
                            <td class="tdr">$<?= number_format(round($order_info["order_cancel_7day"], 1), 1)?>(<?=($order_info["order_7day"] == 0)?0:round($order_info["order_cancel_7day"] / $order_info["order_7day"] * 100, 2)?>%)</td>
                            <td class="tdr">-</td>
                        </tr>
                        <tr>
                            <td class="tdc point">어제</td>
                            <td class="tdc"><?= make_price_format($order_info["count_yesterday"]) ?></td>
                            <td class="tdr">$<?= number_format(round($order_info["order_yesterday"], 1), 1) ?> (<?= round($salerate_yesterday, 1)?>%)</td>
							<td class="tdr">$<?= number_format(round($normalpurchase_yesterday, 1), 1) ?> (<?= ($order_info["order_yesterday"] == 0)?0:round($normalpurchase_yesterday*100/$order_info["order_yesterday"], 1)?>%)</td>
                            <td class="tdr">$<?= number_format(round($order_info["order_cancel_yesterday"], 1), 1)?>(<?=($order_info["order_yesterday"] == 0)?0:round($order_info["order_cancel_yesterday"] / $order_info["order_yesterday"] * 100, 2)?>%)</td>
                            <td class="tdr">-</td>
                        </tr>
                        <tr>
                            <td class="tdc point_title">오늘</td>
                            <td class="tdc point_title"><?= number_format($order_count_today) ?></td>
                            <td class="tdr point_title">$<?= number_format(round($order_today, 1),1) ?> (<?= round($salerate_today, 1)?>%)</td>
							<td class="tdr">$<?= number_format(round($normalpurchase_today, 1), 1) ?> (<?= ($order_today == 0)?0:round($normalpurchase_today*100/$order_today, 1)?>%)</td>
                            <td class="tdr point_title">$<?= number_format(round($order_cancel_today, 1), 1)?>(<?=($order_today == 0)?0:round($order_cancel_today / $order_today * 100, 2)?>%)</td>
                            <td class="tdr point_title">$<?= number_format($estimated_credit, 1) ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div style="width:585px;float:left;margin-left:40px;">
               <a href="/m1_product/product_ios_mng.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">오늘 상품별 결제 내역</a>
                <table class="tbl_list_basic1" style="margin-top:5px">
                    <colgroup>
                        <col width="120"> 
                        <col width="80"> 
                        <col width="80"> 
                        <col width="120"> 
                        <col width="80"> 
                        <col width="80">               
                    </colgroup>
                    <thead>
                        <tr>
                            <th>상품명</th>
                            <th class="tdc">건수</th>
                            <th class="tdr">금액</th>
                            <th style="padding-left:8px;">상품명</th>
                            <th class="tdc">건수</th>
                            <th class="tdr">금액</th>
                        </tr>
                    </thead>
                    <tbody>
<?
	$sql = "SELECT t1.productidx, t2.category, t2.money AS productname, imageurl, paycount, t1.money AS money ".
			"FROM ( ".
			"	SELECT productidx, COUNT(*) AS paycount, IFNULL(SUM(money),0) AS money ".
			"	FROM tbl_product_order_mobile ".
			"	WHERE os_type=1 AND STATUS=1 AND useridx>20000 AND writedate >= '$today 00:00:00' ".
			"	GROUP BY productidx ".
			") t1 JOIN tbl_product_mobile t2 ON t1.productidx = t2.productidx ".
			"ORDER BY t1.money DESC	";
    $productlist = $db_main->gettotallist($sql);
    
    for($i=0; $i<sizeof($productlist); $i++)
    {
         $productidx = $productlist[$i]["productidx"];
         $productname = $productlist[$i]["productname"];
         $category = $productlist[$i]["category"];
         $imageurl = $productlist[$i]["imageurl"];
         $money = $productlist[$i]["money"];
         $paycount = $productlist[$i]["paycount"];
         	
	 	 if($productidx == 178)
         	$productname = "199.99(Prime)";         
         else if($category == 0)
         	$productname = $productname."(Basic)";
         else if($category == 1)
         	$productname = $productname."(First)";
         else if($category == 2 || $category == 5)
         	$productname = $productname."(Season)";
         else if($category == 3)
         	$productname = $productname."(Special)";
         else if($category == 8)
         	$productname = $productname."(Buyerleave)";

?>
                        <tr>
                            <td class="tdl point"><img src="<?=$imageurl?>" align="absmiddle" width="25" height="25"> <?= $productname ?></td>
                            <td class="tdc"><?= make_price_format($paycount) ?>건</td>
                            <td class="tdr">$<?= number_format($money, 2) ?></td>
<?
        $i++;
        
        if (sizeof($productlist) > $i)
        {
            $productidx = $productlist[$i]["productidx"];
            $category = $productlist[$i]["category"];
            $productname = $productlist[$i]["productname"];
            $imageurl = $productlist[$i]["imageurl"];
            $money = $productlist[$i]["money"];
            $paycount = $productlist[$i]["paycount"]; 
            if($productidx == 178)
            	$productname = $productname."(Prime)";
            else if($category == 0)
            	$productname = $productname."(Basic)";
            else if($category == 1)
            	$productname = $productname."(First)";
            else if($category == 2 || $category == 5)
         		$productname = $productname."(Season)";
            else if($category == 3 || $category == 8 || $category == 9)
            	$productname = $productname."(Special)";
            else if($category == 4)
            	$productname = $productname."(Piggypot)";
?>
                            <td class="tdl point" style="border-left:1px solid #e7e7e7;padding-left:8px;"><img src="<?=$imageurl?>" align="absmiddle" width="25" height="25"> <?= $productname ?></td>
                            <td class="tdc"><?= make_price_format($paycount) ?>건</td>
                            <td class="tdr">$<?= number_format($money, 2) ?></td>
<?
        }
?>
                        </tr>
<?
    }
    
?>
                    </tbody>
                </table>
            </div>
            
            <div class="clear"></div>
            
            <div style="width:595px;float:left;margin:20px 0 0 20px;">
                <a href="/m5_monitoring/online_log_stats.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">현재 사용자 접속 상태</a>
                <div id="chart_div1" style="height:300px;width:590px;"></div>
            </div>

            <div style="width:585px;float:left;margin:20px 0 0 40px;">
            	<a href="/m8_game_stats/game_action_stats3_ios.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">오늘 게임 활동 추이</a>
                <table class="tbl_list_basic1" style="margin-top:5px">
                    <colgroup>
                        <col width="100">
                        <col width="100">
                        <col width="100"> 
                        <col width="100">               
                    </colgroup>
                    <tbody>
<?
    $sql = "SELECT IFNULL(SUM(playcount),0) AS playcount, IFNULL(SUM(moneyin),0) AS moneyin, IFNULL(SUM(moneyout),0) AS moneyout, LEFT(writedate, 10) AS writedate ".
            "FROM tbl_game_cash_stats_ios WHERE writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'";    
    $action_info = $db_main2->getarray($sql);    
        
    $jackpot_amount = $db_analysis->getvalue("SELECT IFNULL(SUM(amount),0) FROM tbl_jackpot_stat_daily WHERE devicetype=1 AND today = '$today'");
    $rate = round($action_info["moneyin"]==0 ? 0 : ($action_info["moneyout"]+$jackpot_amount) / $action_info["moneyin"] * 100, 2);
    
    $user_count = $db_analysis->getvalue("SELECT SUM(totalcount) FROM user_online_game_mobile_log WHERE os_type = 1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
    $game_profit = $action_info["moneyin"] - ($action_info["moneyout"] + $jackpot_amount);
    $free_amount = $db_analysis->getvalue("SELECT SUM(freeamount) FROM tbl_user_freecoin_stat WHERE category = 1 AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
    $order_amount = $db_main->getvalue("SELECT SUM(coin) FROM tbl_product_order_mobile WHERE os_type = 1 AND status=1 AND useridx>$str_useridx AND writedate BETWEEN '$today 00:00:00' AND '$today 23:59:59'");
    $purchase_amount = $order_amount;
    
    $total_margin = $game_profit - ($free_amount + $purchase_amount + $free_amount_play);
     if($total_margin < 0 )
        $total_margin ="-".make_price_format(abs($total_margin));
    else
	$total_margin =make_price_format(abs($total_margin));
    
	$free_amount_play = 0;
?>
                        <tr>
                            <th class="tdc" style="border-top:1px solid #e7e7e7;">money_in (A)</th>
                            <td class="tdr" style="border-top:1px solid #e7e7e7;"><?= make_price_format($action_info["moneyin"]) ?></td>
                            <th class="tdc" style="border-top:1px solid #e7e7e7;">접속자수</th>
                            <td class="tdr" style="border-top:1px solid #e7e7e7;"><?= make_price_format($user_count) ?></td>
                        </tr>
                        <tr>
                            <th class="tdc" style="border-top:1px solid #e7e7e7;">money_out (B)</th>
                            <td class="tdr" style="border-top:1px solid #e7e7e7;"><?= make_price_format($action_info["moneyout"]) ?></td>
                            <th class="tdc">승률 ((B+C)/A)</th>
                            <td class="tdr"><?= $rate."%" ?></td>
                        </tr>
                        <tr>
                            <th class="tdc">jackpot (C)</th>
                            <td class="tdr"><?= make_price_format($jackpot_amount) ?></td>
                            <th class="tdc">게임횟수</th>
                            <td class="tdr"><?= make_price_format($action_info["playcount"]) ?></td>
                        </tr>
                        <tr>
                            <th class="tdc">게임이익 (A-(B+C)=D)</th>
                            <td class="tdr"><?= make_price_format($game_profit) ?></td>
                        </tr>
                        <tr>
                            <th class="tdc">구매코인 (E)</th>
                            <td class="tdr"><?= make_price_format($purchase_amount) ?></td>
                        </tr>
                        <tr>
                            <th class="tdc">활동 무료코인 (F)</th>
                            <td class="tdr"><?= make_price_format($free_amount_play) ?></td>
                        </tr>
                        <tr>
                            <th class="tdc">무료코인 (G)</th>
                            <td class="tdr"><?= make_price_format($free_amount) ?></td>
                        </tr>
                        <tr>
                            <th class="tdc">총 이익 (D-(E+F+G))</th>
                            <td class="tdr"><?=$total_margin?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <div class="clear"></div> 
            
            <div style="width:380px;float:left;margin:20px 0 0 20px;">
                <a href="/m5_monitoring/client_log_stats_ios.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">클라이언트 통신 로그</a>
                <div id="chart_div2" style="height:300px;width:380px;"></div>
            </div>
            
            <div style="width:380px;float:left;margin:20px 0 0 30px;">
                <a href="/m5_monitoring/server_log_stats_ios.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">서버 통신 로그</a>
                <div id="chart_div3" style="height:300px;width:380px;"></div>
            </div>
            
            <div style="width:380px;float:left;margin:20px 0 0 30px;">
                <a href="/m5_monitoring/serverevent_log_stats_mobile.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">서버 이벤트 로그</a>
                <div id="chart_div4" style="height:300px;width:380px;"></div>
            </div>

        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?  
   $db_main->end();
    $db_main2->end();
    $db_analysis->end();
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
		
