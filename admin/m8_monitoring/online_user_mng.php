<?
    $top_menu = "monitoring";
    $sub_menu = "online_user_mng";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    
    $search_nickname = trim($_GET["nickname"]);
    $search_userid = trim($_GET["userid"]);
    $search_serveridx = $_GET["serveridx"];
    
    check_xss($search_nickname.$search_userid);
    
    $listcount = "10";
    $pagename = "online_user_mng.php";
    $pagefield = "nickname=$search_nickname&userid=$search_userid&serveridx=$search_serveridx";
    
    $tail = " WHERE B.useridx > 10000 ";
    $order_by = "ORDER BY A.heartbeatdate desc";
    
    if ($search_serveridx != "")
        $tail .= " AND serveridx=$search_serveridx";
        
    if ($search_nickname != "")
        $tail .= " AND nickname LIKE '%$search_nickname%'";
    
    if ($search_userid != "")
        $tail .= " AND userid LIKE '%$search_userid%'";
    
    $db_main = new CDatabase_Main();
    
    $totalcount = $db_main->getvalue("SELECT COUNT(*) FROM tbl_user_online_sync2 AS A JOIN tbl_user AS B ON A.useridx=B.useridx $tail");
    
    $sql = "SELECT A.useridx,serveridx,heartbeatdate,A.heartbeatdate,B.nickname,B.userid,coin,B.honor_level,B.honor_point,totalspin, totalamount ".
        "FROM tbl_user_online_sync2 AS A JOIN tbl_user AS B ON A.useridx=B.useridx $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    $userlist = $db_main->gettotallist($sql);
    
    $db_main->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script>
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    } 
    
    function change_orderby(orderby)
    {
        window.location.href = "user_mng.php?orderby=" + orderby + "<?= $searchfield ?>";
    }
</script>
    <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 온라인 사용자 목록 <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
            </div>
            <!-- //title_warp -->
            
            <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
                <div class="detail_search_wrap">
                    <span class="search_lbl ml20">이름</span>
                    <input type="text" class="search_text" id="nickname" name="nickname" style="width:120px" value="<?= encode_html_attribute($search_nickname) ?>" onkeypress="search_press(event)" />
                              
                    <span class="search_lbl ml20">페이스북 아이디</span>
                    <input type="text" class="search_text" id="userid" name="userid" style="width:120px" value="<?= encode_html_attribute($search_userid) ?>" onkeypress="search_press(event)" />
                    
                    <span class="search_lbl ml20">서버</span>
                    <select name="serveridx" id="serveridx">
                        <option value="">서버 선택</option>
                        <option value="1" <?= ($search_serveridx == '1') ? "selected" : "" ?>>Common1</option>
                        <option value="2" <?= ($search_serveridx == '2') ? "selected" : "" ?>>Common2</option>
                        <option value="3" <?= ($search_serveridx == '3') ? "selected" : "" ?>>Common3</option>
                    </select>   
                           
                    <div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
                </div>
            </form>
            
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">               
            </colgroup>
            <thead>
            <tr>
                <th>번호</th>
                <th>이름</th>
                <th>페이스북 아이디</th>
                <th>Coin</th>
                <th>Honor Level</th>
                <th>Honor Point</th>
                <th>Total Spin</th>
                <th>Total Amount</th>
                <th>Heartbeat</th>
            </tr>
            </thead>
            <tbody>
<?
    for($i=0; $i<sizeof($userlist); $i++)
    {
        $useridx = $userlist[$i]["useridx"];
        $nickname = $userlist[$i]["nickname"];
        $userid = $userlist[$i]["userid"];
        $coin = $userlist[$i]["coin"];
        $honor_level = $userlist[$i]["honor_level"];
        $honor_point = $userlist[$i]["honor_point"];
        $totalspin = $userlist[$i]["totalspin"];
        $photourl = get_fb_pictureURL($userid,$client_accesstoken);
        $totalamount = $userlist[$i]["totalamount"];
        $heartbeatdate = $userlist[$i]["heartbeatdate"];
?>
            <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''" onclick="view_user_dtl(<?= $useridx ?>, 'online')">
                <td class="tdc"><?=  $totalcount - (($page-1) * $listcount) - $i ?></td>
                <td class="point_title"><span style="float:left;"><img src="<?= $photourl ?>" height="25" width="25" align="absmiddle" hspace="3"></span> <?= $nickname ?></td>
                <td class="tdc point"><?= $userid ?></td>
                <td class="tdc point"><?= make_price_format($coin) ?></td>
                <td class="tdc point"><?= make_price_format($honor_level) ?></td>
                <td class="tdc point"><?= make_price_format($honor_point) ?></td>
                <td class="tdc point"><?= make_price_format($totalspin) ?></td>
                <td class="tdc point"><?= make_price_format($totalamount) ?></td>                
                <td class="tdc"><?= (substr($heartbeatdate, 0, 10) != "0000-00-00") ? $heartbeatdate : "" ?></td>
            </tr>
<?
    } 
?>
            </tbody>
            </table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
        </div>
        <!--  //CONTENTS WRAP -->
        
        <div class="clear"></div>
    </div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>