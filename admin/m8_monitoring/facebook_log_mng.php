<?
    $top_menu = "monitoring";
    $sub_menu = "facebook_log";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    
    $search_ipaddress = trim($_GET["ipaddress"]);
    $search_cmd = trim($_GET["cmd"]);
    $search_errcode = trim($_GET["errcode"]);
    $search_start_writedate = $_GET["start_writedate"];
    $search_end_writedate = $_GET["end_writedate"];
    
    check_number($search_cmd);  
    check_xss($search_ipaddress);  
    check_xss($search_contents);  
    
    $listcount = "10";
    $pagename = "facebook_log_mng.php";
    $pagefield = "ipaddress=$search_ipaddress&cmd=$search_cmd&errcode=$search_errcode&start_writedate=$search_start_writedate&end_writedate=$search_end_writedate";
    
    $tail = " WHERE 1=1 AND errcode <> ''";
    
    if ($search_ipaddress != "")
        $tail .= " AND ipaddress LIKE '%$search_ipaddress%' ";

    if ($search_cmd != "")
    	$tail .= " AND cmd = $search_cmd ";
    
    if ($search_errcode != "")
        $tail .= " AND errcode LIKE '%$search_errcode%' ";
    
    if ($search_start_writedate != "")
        $tail .= " AND writedate >= '$search_start_writedate 00:00:00' ";
    
    if ($search_end_writedate != "")
        $tail .= " AND writedate <= '$search_end_writedate 23:59:59' ";
   
    $db_analysis = new CDatabase_Analysis();
    
    $totalcount = $db_analysis->getvalue("SELECT COUNT(*) FROM facebook_error_log $tail");
    
    $sql = "SELECT logidx, ipaddress, cmd, errcode, writedate FROM facebook_error_log $tail ORDER BY writedate DESC LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    $loglist = $db_analysis->gettotallist($sql); 
    
    $sql = "SELECT cmd FROM facebook_error_log GROUP BY cmd";
    
    $cmdlist = $db_analysis->gettotallist($sql);
    
    $db_analysis->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript">
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	
	    search_form.submit();
	}
	
	$(function() {
	    $("#start_writedate").datepicker({ });
	});
	
	$(function() {
	    $("#end_writedate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Facebook 로그 <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
	</div>
	<!-- //title_warp -->
            
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="facebook_log_mng.php">
		<div class="detail_search_wrap">
			<span class="search_lbl">IP 주소</span>
			<input type="input" class="search_text" id="ipaddress" name="ipaddress" style="width:120px" value="<?= encode_html_attribute($search_ipaddress) ?>" onkeypress="search_press(event)" />
                    
			<span class="search_lbl ml20">cmd 종류</span>
			<select name="cmd" id="cmd">
				<option value="">선택</option>
<?
	for($i=0; $i<sizeof($cmdlist); $i++)
	{
		$command = $cmdlist[$i]["cmd"];
?>                        
				<option value="<?= $command ?>" <?= ($search_cmd == $command) ? "selected" : "" ?>><?= $command ?></option>                        
<? 
	}
?>
			</select>
                    
			<span class="search_lbl ml20">오류 메시지</span>
			<input type="input" class="search_text" id="errcode" name="errcode" style="width:200px" value="<?= encode_html_attribute($search_errcode) ?>" onkeypress="search_press(event)" />
                    
			<span class="search_lbl ml20">기록일시</span>
			<input type="input" class="search_text" id="start_writedate" name="start_writedate" style="width:65px" value="<?= $search_start_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
			<input type="input" class="search_text" id="end_writedate" name="end_writedate" style="width:65px" value="<?= $search_end_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
                    
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>	            
	            
	</form>
	
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="0%">
			<col width="5%">
			<col width="20%">
			<col width="10%">
			<col width="50%">
			<col width="15%">
		</colgroup>
		 <thead>
            <tr>
                <th style="padding-left:5px"></th>
                <th>번호</th>
                <th>IP 주소</th>
                <th>cmd 종류</th>
                <th>오류 메시지</th>
                <th>기록일시</th>
            </tr>
		</thead>
		<tbody>
<?
    for($i=0; $i<sizeof($loglist); $i++)
    {
        $logidx = $loglist[$i]["logidx"];
        $ipaddress = $loglist[$i]["ipaddress"];
        $cmd = $loglist[$i]["cmd"];
        $errcode = $loglist[$i]["errcode"];
        $writedate = $loglist[$i]["writedate"];        
        
?>
            <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="">
                <td class="tdc" style="padding-left:5px"></td>
                <td class="tdc" <?= $style ?>><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
                <td class="tdc point" <?= $style ?>><?= $ipaddress ?></td>
                <td class="tdc point" <?= $style ?>><?= $cmd ?></td>
                <td class="tdㅊ" <?= $style ?>><?= encode_html_attribute($errcode) ?></td>
                <td class="tdc" <?= $style ?>><?= $writedate ?></td>
            </tr>
<?
    } 
?>
		</tbody>
	</table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
</div>
<!--  //CONTENTS WRAP -->

<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>