<?
    $top_menu = "monitoring";
	$sub_menu = "unknowncoin_user_new";
	
	include ($_SERVER ["DOCUMENT_ROOT"] . "/m_common/top_frame.inc.php");
	
	$logidx = $_GET ["logidx"];
	
	check_number($logidx);
	
	$db_other = new CDatabase_Other();
	
	if($logidx != "")
	{
		$sql = "SELECT logidx FROM tbl_unknowncoin_daily_new WHERE logidx = $logidx";
		$logidx = $db_other->getvalue($sql);
		
		if($logidx == "")
			error_back("존재하지 않는 데이터입니다.");
		
		$sql = "SELECT * FROM tbl_unknowncoin_daily_new WHERE logidx = $logidx";
		$unknowncoin_user_info = $db_other->getarray($sql);
		
		$logidx = $unknowncoin_user_info["logidx"];
		$useridx = $unknowncoin_user_info["useridx"];
		$coin = $unknowncoin_user_info["coin"];
		$os = $unknowncoin_user_info["os"];
		$unknown_coin_total = $unknowncoin_user_info["unknown_coin_total"];
		$writedate = $unknowncoin_user_info["writedate"];
		$status = $unknowncoin_user_info["status"];
		$comment = $unknowncoin_user_info["comment"];
	}
	
	$db_other->end();
?>

<script>
	function save_unknowncoin_user() 
	{
		var unknowncoin_user_form = document.unknowncoin_user_form;

		var param = {};

		param.logidx = unknowncoin_user_form.logidx.value;
		param.status = get_radio("status");
		param.comment = unknowncoin_user_form.comment.value;
		param.isnew = 1;

		WG_ajax_execute("monitoring/update_unknowncoin_user_new", param, save_unknowncoin_user_callback);
	}
	
	function save_unknowncoin_user_callback(result, reason)
	{
		if(!result)
		{
			alert("오류 발생 - " + reason);
		}
		else
		{
			window.location.href = "javascript:history.back()";
		}
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Unknown코인 급증(최근 일주일 미결제 & 최근 일주일 Unknown코인 절대값이 큰 사용자) 설정 </div>
	</div>
	<!-- //title_warp -->
	
	<form name="unknowncoin_user_form" id="unknowncoin_user_form">
		<input type="hidden" name="logidx" id="logidx" value="<?= $logidx ?>">
		<table class="tbl_view_basic">
			<colgroup>
				<col width="150">
				<col width="">
			</colgroup>
			
			<tbody>
				<tr>
					<th>useridx</th>
					<td><?= $useridx ?></td>
				</tr>
				
				<tr>
					<th>보유코인</th>
					<td><?= number_format($coin) ?></td>
				</tr>
				
				<tr>
					<th>Unknown코인(Total)</th>
					<td><?= number_format($unknown_coin_total) ?></td>
				</tr>
				
				<tr>
					<th>등록일</th>
					<td><?= $writedate ?></td>
				</tr>
				
				<tr>
					<th>상태</th>
					<td>
						<input type="radio" class="radio" name="status" id="status" value="0" <?= ($status == "0") ? "checked" : "" ?>> 미확인
						<input type="radio" class="radio" name="status" id="status" value="1" <?= ($status == "1") ? "checked" : "" ?>> 확인
					</td>
				</tr>
				
				<tr>
					<th>comment</th>
					<td><textarea id="comment" style="width:400px;height:100px;"><?= ($comment == "") ? "현재 코인 " : encode_html_attribute($comment) ?></textarea></td>
				</tr>
			</tbody>
		</table>
	</form>
	
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="저장" onclick="save_unknowncoin_user()">
		<input type="button" class="btn_setting_02" value="취소" onclick="history.back()">
	</div>
</div>

<!--  //CONTENTS WRAP -->

<div class="clear"></div>

<?
	include ($_SERVER ["DOCUMENT_ROOT"] . "/m_common/bottom_frame.inc.php");
?>