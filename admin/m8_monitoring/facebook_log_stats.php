<?
    $top_menu = "monitoring";
    $sub_menu = "facebook_log_stats";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $term = ($_GET["term"] == "") ? "1" : $_GET["term"];
    $lastdate = $_GET["lastdate"];
    $startdate = ($_GET["startdate"] == "") ? date("Y-m-d"): $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
    
    check_number($term);
    
    if ($term != "1" && $term != "5" && $term != "30" && $term != "60" && $term != "180" && $term != "360" && $term != "720" && $term != "1440")
        error_back("잘못된 접근입니다.");
    
    if ($lastdate == "")
    {
        $end_date = date("Y-m-d", time());
        $end_time = date("H:i:s", time());    
    }
    else 
    {
        $end_date = $lastdate;
        $end_time = "23:59:59";    
    }
    
    if ($term == "1") // 최근 6시간
    {
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time))-6,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time))-6,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));      
    }
    else if ($term == "5") // 최근 12시간
    {
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time))-12,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time))-12,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
    }
    else if ($term == "30") // 최근 1일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-1),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-1),date("Y",strtotime($end_date))));
    }
    else if ($term == "60") // 최근 2일 
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-2),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-2),date("Y",strtotime($end_date))));
    }
    else if ($term == "180") // 최근 5일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-5),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-5),date("Y",strtotime($end_date))));
    }
    else if ($term == "360") // 최근 7일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-7),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-7),date("Y",strtotime($end_date))));
    }
    else if ($term == "720") // 최근 10일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-10),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-10),date("Y",strtotime($end_date))));
    }
    else if ($term == "1440") // 최근 30일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-30),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-30),date("Y",strtotime($end_date))));
    }
    else if ($term == "10080") // 최근 60일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-60),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-60),date("Y",strtotime($end_date))));
    }
    else if ($term == "43200") // 최근 12개월
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),(date("m", strtotime($end_date))-12),(date("d", strtotime($end_date))),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),(date("m", strtotime($end_date))-12),(date("d", strtotime($end_date))),date("Y",strtotime($end_date))));
    }
        
    $period_tail = "";
    
    $db_analysis = new CDatabase_Analysis(); 
    
    $sql = "SELECT COUNT(*) AS latecount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM facebook_error_log ".
    		"WHERE errcode <> '' AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $err_list = $db_analysis->gettotallist($sql);
    
    if ($startdate != "")
        $period_tail .= " AND writedate >= '$startdate 00:00:00'";
    
    if ($enddate != "")
        $period_tail .= " AND writedate <= '$enddate 23:59:59'";
    
    $sql = "SELECT COUNT(logidx) AS total_error, cmd ".
            "FROM facebook_error_log AS t1 WHERE 1=1 $period_tail GROUP BY cmd ORDER BY total_error DESC";
    
    $total_list = $db_analysis->gettotallist($sql);   
    
    $db_analysis->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	
	function drawChart() 
	{
	    var datatable = new google.visualization.DataTable();
	    
	    datatable.addColumn('string', '시간');
	    datatable.addColumn('number', '에러건수');
	    datatable.addRows([
<?
	$start = floor(mktime(date("H",strtotime($start_time)), date("i",strtotime($start_time)), date("s", strtotime($start_time)), date("m",strtotime($start_date)), date("d", strtotime($start_date)), date("Y", strtotime($start_date)))/($term*60));
	$end = floor(mktime(date("H",strtotime($end_time)), date("i",strtotime($end_time)), date("s", strtotime($end_time)), date("m",strtotime($end_date)), date("d", strtotime($end_date)), date("Y", strtotime($end_date)))/($term*60)) + 1;
	
	for ($i=$start; $i<$end; $i=$i+1)
	{
	    $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
	    $temp_minute = $temp_time[0]*60 + $temp_time[1];
	  
	    $list_date = date("Y-m-d", $i*($term*60));
	    
	    if ($term > 10080)
	        echo("['".substr($list_date, 0, 7)."'");  
	    else if ($term > 720)
	        echo("['$list_date'");        
	    else
	        echo("['".make_time_format($temp_minute)."'");
	            
	    $print = false;
	    for ($j=0; $j<sizeof($err_list); $j++)
	    {
	        if ($err_list[$j]["minute"] == $i)
	        {
	    		echo(",{v:".$err_list[$j]["latecount"].",f:'".make_price_format($err_list[$j]["latecount"])."'}");
	    		$print = true;
	    		break;
	        }
	    }
	    
	    if (!$print)
	    echo(",0");
	    
	    if ($i+1 >= $end)
	        echo("]");  
	    else
	        echo("],");               
	}   
?>          
	]);
	
	    var options = {
	        title:'',            
	        axisTitlesPosition:'in',
	        curveType:'none',
	        focusTarget:'category',
	        interpolateNulls:'true',
	        legend:'top',
	        fontSize:12,
	        chartArea:{left:80,top:40,width:1000,height:400}
	    };
	
	    var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
	    chart.draw(datatable, options);
	}
	
	google.setOnLoadCallback(drawChart);
	
	function change_term(term)
	{
	    var search_form = document.search_form;        
	    var minute = document.getElementById("term_minute");
	    var minute5 = document.getElementById("term_5minute");
	    var minute30 = document.getElementById("term_30minute");
	    var minute60 = document.getElementById("term_60minute");
	    var minute180 = document.getElementById("term_180minute");
	    var minute360 = document.getElementById("term_360minute");
	    var minute720 = document.getElementById("term_720minute");
	    var minute1440 = document.getElementById("term_1440minute");
	    
	    search_form.term.value = term;
	    
	    if (term == "1")
	    {
	        minute.className="btn_schedule_select";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule";
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";  
	    }
	    else if (term == "5")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule_select";
	        minute30.className="btn_schedule";   
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";      
	    }
	    else if (term == "30")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule_select";  
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule"; 
	    }
	    else if (term == "60")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule"; 
	        minute60.className="btn_schedule_select";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";
	    }
	    else if (term == "180")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule"; 
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule_select";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";   
	    }
	    else if (term == "360")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule"; 
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule_select";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";   
	    }
	    else if (term == "720")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule"; 
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule_select";
	        minute1440.className="btn_schedule"; 
	    }
	    else if (term == "1440")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule";
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule_select";
	    }
	    
	    search_form.submit();
	}
	
	function search()
	{
	    var search_form = document.search_form;
	    search_form.submit();
	}
	
	$(function() {
	    $("#lastdate").datepicker({ });
	});
	
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="facebook_log_stats.php">
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; Facebook 로그 통계</div>
			<input type="hidden" name="term" id="term" value="<?= $term ?>" />
			<div class="search_box">
				<input type="button" class="<?= ($term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="1분" id="term_minute" onclick="change_term(1)" />
				<input type="button" class="<?= ($term == "5") ? "btn_schedule_select" : "btn_schedule" ?>" value="5분"  id="term_5minute" onclick="change_term(5)" />
				<input type="button" class="<?= ($term == "30") ? "btn_schedule_select" : "btn_schedule" ?>" value="30분" id="term_30minute" onclick="change_term(30)" />
				<input type="button" class="<?= ($term == "60") ? "btn_schedule_select" : "btn_schedule" ?>" value="1시간" id="term_60minute" onclick="change_term(60)" />
				<input type="button" class="<?= ($term == "180") ? "btn_schedule_select" : "btn_schedule" ?>" value="3시간" id="term_180minute" onclick="change_term(180)" />
				<input type="button" class="<?= ($term == "360") ? "btn_schedule_select" : "btn_schedule" ?>" value="6시간" id="term_360minute" onclick="change_term(360)" />
				<input type="button" class="<?= ($term == "720") ? "btn_schedule_select" : "btn_schedule" ?>" value="12시간" id="term_720minute" onclick="change_term(720)" />
				<input type="button" class="<?= ($term == "1440") ? "btn_schedule_select" : "btn_schedule" ?>" value="1일" id="term_1440minute" onclick="change_term(1440)" />                    
				<input type="input" class="search_text" id="lastdate" name="lastdate" style="width:65px" value="<?= $lastdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</div>
		<!-- //title_warp -->
		
		<div style="font-weight:bold;">[Facebook 로그 통계]</div>
		<div id="chart_div" style="height:500px; min-width:500px;"></div>
            
		<div style="height:30px;padding-top:40px;">
			<div style="font-weight:bold;float:left;">[Facebook 로그 통계 요약]</div>
			<div style="padding-right:15px;float:right;">
				<input type="input" class="search_text" id="startdate" name="startdate" style="width:65px" value="<?= $startdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
				<input type="input" class="search_text" id="enddate" name="enddate" style="width:65px" value="<?= $enddate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</div>
	</form>
            ​
	<div style="float:left;padding-top:10px;margin-right:15px;width:40%;">                    
		<table class="tbl_list_basic1"> 
			<thead>         
				<tr>
					<th>cmd</th>
					<th>총 에러건수</th>
				</tr>
			</thead>
			<tbody>
<?
    for ($i=0; $i<sizeof($total_list); $i++)
    {
        $cmd = $total_list[$i]["cmd"];
        $total_error = $total_list[$i]["total_error"];
        $sum_total_error = $sum_total_error + $total_error;
?>
				<tr>
					<td class="tdc point"><?= $cmd ?></td>
					<td class="tdc"><?= make_price_format($total_error)?></td>
				</tr>
<?
    }    					
?>   
				<tr>
					<td class="tdc point">합계</td>
					<td class="tdc"><?= make_price_format($sum_total_error)?></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<!--  //CONTENTS WRAP -->
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>