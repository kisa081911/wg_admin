<?
	$top_menu = "monitoring";
	$sub_menu = "unknowncoin_user_new";
	$table = "tbl_unknowncoin_daily_new";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$page = ($_GET["page"] == "") ? "1" : $_GET["page"];
	
	$search_platform = $_GET["platform"];
	$search_useridx = $_GET["useridx"];
	$search_create_sd = $_GET["search_create_sd"];
	$search_create_ed = $_GET["search_create_ed"];
	$search_write_sd = $_GET["search_write_sd"];
	$search_write_ed = $_GET["search_write_ed"];
	$search_status = $_GET["search_status"];
	
	$listcount = "10";
	$pagename = "unknowncoin_user_new.php";
	$pagefield = "check=$check&platform=$search_platform&useridx=$search_useridx&search_create_sd=$search_create_sd&search_create_ed=$search_create_ed&search_write_sd=$search_write_sd&search_write_ed=$search_write_ed&search_status=$search_status";
	
	$tail = "WHERE 1=1 ";
	
	$order_by = " ORDER BY DATE_FORMAT(writedate, '%Y-%m-%d') DESC, platform ASC, useridx DESC ";
	
	if($search_platform != "")
		$tail .= "AND platform = $search_platform ";
	
	if ($search_useridx != "")
		$tail .= " AND useridx = $search_useridx ";
	
	if ($search_create_sd != "")
		$tail .= " AND createdate >= '$search_create_sd 00:00:00' ";
	if ($search_create_ed != "")
		$tail .= " AND createdate <= '$search_create_ed 23:59:59' ";
		
	if ($search_write_sd != "")
		$tail .= " AND writedate >= '$search_write_sd 00:00:00' ";
	if ($search_write_ed != "")
		$tail .= " AND writedate <= '$search_write_ed 23:59:59' ";
	
	if($search_status != "")
		$tail .= "AND status = $search_status";
		
	$db_main2 = new CDatabase_Main2();
	$db_otherdb = new CDatabase_Other();
	$db_mobile = new CDatabase_Mobile();
	
	$sql = "SELECT COUNT(*) FROM $table $tail";
	$totalcount = $db_otherdb->getvalue($sql);
	
	$sql = "SELECT * FROM $table $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;
	$unknowncoin_list = $db_otherdb->gettotallist($sql);
	
	if ($totalcount < ($page-1) * $listcount && $page != 1)
		$page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	
	    search_form.submit();
	}
	
	$(function() {
		$("#search_create_sd").datepicker({ });
	    $("#search_write_sd").datepicker({ });
	});
	
	$(function() {
		$("#search_create_ed").datepicker({ });
	    $("#search_write_ed").datepicker({ });
	});

	function update_abuse_user_debug(useridx, status, platform) 
	{
		var msg = "";
		var comment = "";
		if(status == 0)
			msg = "디버그 등록 하시겠습니까?";
		else
			msg = "디버그 해제 하시겠습니까?";
		
		if(confirm(msg) == 0)
			return;

		if(status == 0)
			comment = prompt("Comment를 입력하세요.");


		if(comment == null)
			return;
		
		var param = {};
		param.useridx = useridx;
		param.status = status;
		param.platform = platform;
		
		if(status == 0)
			param.comment = comment;
			
		WG_ajax_execute("monitoring/update_abuse_user_debug", param, update_abuse_user_debug_callback);
	}

	function update_abuse_user_debug_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else        
        {
            window.location.href = window.location.href;
        }
    }
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Unknown코인 급증(최근 일주일 미결제 & 최근 일주일 Unknown코인 절대값이 큰 사용자) <span class="totalcount">(<?= number_format($totalcount) ?>)</span></div>
	</div>
	<!-- //title_warp -->
    
    <form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
    	<div class="detail_search_wrap">
    		<input type="hidden" name="check" id="check" value=<?= $check ?> />
    		<input type="hidden" name="reserve_check" id="reserve_check" value="0" />
    		<input type="hidden" name="confirm_check" id="confirm_check" value="0" />
    		<span class="search_lbl">유입경로</span>
			<select name="platform" id="platform">
				<option value="" <?= ($search_platform == "") ? "selected" : "" ?>>전체</option>
				<option value="0" <?= ($search_platform == "0") ? "selected" : "" ?>>Web</option>
				<option value="1" <?= ($search_platform == "1") ? "selected" : "" ?>>iOS</option>
				<option value="2" <?= ($search_platform == "2") ? "selected" : "" ?>>Android</option>
				<option value="3" <?= ($search_platform == "3") ? "selected" : "" ?>>Amazon</option>
			</select>
			
			&nbsp;
			
			<span class="search_lbl">상태</span>
			<select name="search_status" id="search_status">
				<option value="" <?= ($search_status == "") ? "selected" : "" ?>>전체</option>
				<option value="0" <?= ($search_status == "0") ? "selected" : "" ?>>미확인</option>
				<option value="1" <?= ($search_status == "1") ? "selected" : "" ?>>확인</option>
			</select>
			
			&nbsp;
			
			<span class="search_lbl">useridx</span>
            <input type="input" class="search_text" id="useridx" name="useridx" style="width:65px" value="<?= $search_useridx?>" onkeypress="search_press(event)" />&nbsp;
            <span class="search_lbl">가입일</span>
			<input type="text" class="search_text" id="search_create_sd" name="search_create_sd" style="width:65px" value="<?= $search_create_sd ?>" 
				onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
            <input type="text" class="search_text" id="search_create_ed" name="search_create_ed" style="width:65px" value="<?= $search_create_ed ?>" 
            	onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />&nbsp;
			<span class="search_lbl">등록일</span>
			<input type="text" class="search_text" id="search_write_sd" name="search_write_sd" value="<?= $search_write_sd ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
			<input type="text" class="search_text" id="search_write_ed" name="search_write_ed" value="<?= $search_write_ed ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
			
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>  
	
	<form id="table_form">
		<table class="tbl_list_basic1">
	        <colgroup>
	            <col width="65">
	            <col width="">
	            <col width="">
	            <col width="">
	            <col width="">
	            <col width="70">
	            <col width="70">
	            <col width="45">
	            <col width="">
	            <col width="70">
	            <col width="60">
	            <col width="60">
	        </colgroup>
	        <thead>
	      	    <tr>
					<th rowspan="2">유입경로</th>
	                <th rowspan="2">useridx</th>
	                <th rowspan="2">facebookid</th>
	                <th rowspan="2">이름</th>
	                <th rowspan="2">보유코인</th>
	                <th rowspan="2">가입일</th>
	                <th rowspan="2">등록일</th>
	                <th rowspan="2">상태</th>
	                <th rowspan="2">comment</th>
	                <th rowspan="2">작성일</th>
	                <th rowspan="2">상세</th>
	                <th colspan="2">디버그</th>
	           	</tr>
	           	<tr>
	           		<th>웹</th>
	           		<th>모바일</th>
           		</tr>
			</thead> 
			<tbody>
<?
    for($i=0; $i<sizeof($unknowncoin_list); $i++)
    {
    	$logidx = $unknowncoin_list[$i]["logidx"];
    	$useridx = $unknowncoin_list[$i]["useridx"];
    	$userid = $unknowncoin_list[$i]["userid"];
    	$nickname = $unknowncoin_list[$i]["nickname"];
    	$photourl = $unknowncoin_list[$i]["photourl"];
     	$coin = $unknowncoin_list[$i]["coin"];
     	$platform = $unknowncoin_list[$i]["platform"];
     	$createdate = $unknowncoin_list[$i]["createdate"];
     	$writedate = $unknowncoin_list[$i]["writedate"];
     	$status = $unknowncoin_list[$i]["status"];
     	$comment = $unknowncoin_list[$i]["comment"];
     	$commentdate = $unknowncoin_list[$i]["commentdate"];
     	
     	$web_debug = 0;
     	$mobile_debug = 0;
     	
     	$sql = "SELECT * FROM tbl_user_debug WHERE useridx = $useridx";
     	
     	if($db_main2->getvalue($sql) != "")
	    	$web_debug = 1;
	    
	    //if($db_mobile->getvalue($sql) != "")
      			$mobile_debug = 0;
     	
		if ($platform == "0")
			$platform = "Web";
		else if ($platform == "1")
			$platform = "iOS";
		else if ($platform == "2")
			$platform = "Android";
		else if ($platform == "3")
			$platform = "Amazon";
?>
				<tr>
					<td class="tdc"><?= $platform ?></td>
	            	<td class="tdc"><?= $useridx ?></td>
	            	<td class="tdc"><?= $userid ?></td>
					<td class="point_title"><a href="javascript:view_unknowncoin_user_new_dtl(<?= $logidx ?>)"><span style="float:left;"><img src="<?= $photourl ?>" height="25" width="25" align="absmiddle" hspace="3"></span><?= $nickname ?>&nbsp;&nbsp;</a><img src="/images/icon/facebook.png" height="20" width="20" align="absmiddle" style="cursor:pointer" onclick="event.cancelBubble=true;window.open('http://www.facebook.com/profile.php?id=<?= $userid ?>')" /></td>
		            <td class="tdr"><?= number_format($coin) ?></td>
		            <td class="tdc"><?= ($createdate == "0000-00-00 00:00:00") ? "-" : date("Y-m-d", strtotime($createdate)) ?></td>
		            <td class="tdc"><?= date("Y-m-d", strtotime($writedate)) ?></td>
		            <td class="tdc"><?= ($status == "0") ? "미확인" : (($status == "1") ? "확인" : "보류") ?></td>
		            <td class="tdc"><?= $comment ?></td>
		            <td class="tdc"><?= ($commentdate == "0000-00-00 00:00:00") ? "-" : date("Y-m-d", strtotime($commentdate)) ?></td>
		            <td class="tdc"><input type="button" class="btn_03" value="상세" onclick="view_user_dtl_new(<?= $useridx ?>,'')"></td>
<?
		if($web_debug == 0)
		{
?>
	            <td class="tdc"><input type="button" class="btn_03" value="등록" onclick="update_abuse_user_debug(<?= $useridx ?>, 0, 0)"></td>
<?
		}
		else
		{
?>
				<td class="tdc"><input type="button" class="btn_03" value="해제" onclick="update_abuse_user_debug(<?= $useridx ?>, 1, 0)"></td>
<?
		}
?>
		        </tr>
<?
    }
?>
			</tbody>
		</table>
	</form>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
	$db_main2->end();
	$db_otherdb->end();
	$db_mobile->end();
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
