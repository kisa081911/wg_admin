<?
	$top_menu = "monitoring";
	$sub_menu = "betrange";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

	$tab = ($_GET["tab"] == "") ? "1" : $_GET["tab"];

	$pagename = "betrange.php";

	if ($tab != "1" && $tab != "2")
		error_back("잘못된 접근입니다.");

	$db_main2 = new CDatabase_Main2();
	$db_game = new CDatabase_Game();
	$db_analysis = new CDatabase_Analysis();

	$sql = "SELECT slottype, slotname FROM tbl_slot_list";
	$slotname_list = $db_main2->gettotallist($sql);

	$slotnames = array();

	foreach ($slotname_list as $each_slot)
	{
		$slotnames[$each_slot["slottype"]] = $each_slot["slotname"];
	}

	$morecolumns = "";
	$betcount = 10;

	if ($tab == "1")
	{
		$tablename = "tbl_normal_bet_info";
		$morecolumns = ", bet6, bet7, bet8, bet9, bet10";
		$tablename_rank = "tbl_slot_ranking_normal_info";
	}
	else if ($tab == "2")
	{
		$tablename = "tbl_highroller_bet_info";
		$betcount = 5;
		$tablename_rank = "tbl_slot_ranking_highroller_info";
	}

	$sql = "SELECT slotidx, alignidx, linecount, bet1, bet2, bet3, bet4, bet5 $morecolumns ".
		"FROM $tablename ".
		"ORDER BY slotidx";
	$slot_list = $db_game->gettotallist($sql);

	$sql = "SELECT slottype, ranking_num ".
		"FROM $tablename_rank ".
		"ORDER BY ranking_num";

	$rank_list = $db_main2->gettotallist($sql);

	$db_main2->end();
	$db_game->end();
	$db_analysis->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.button.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.position.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.dialog.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.mouse.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.sortable.js"></script>
<script type="text/javascript">
/* 문자열 포멧 함수 추가 */
function formatWithParamInternal(str, prefix, obj) {
	for ( var _name in obj) {
		var _prop = obj[_name];
		var _propType = typeof _prop;
		var _propName = prefix && prefix.length > 0 ? prefix + '\\.' + _name : _name;
		if (_propType === 'object') {
			str = formatWithParamInternal(str, _propName, _prop);
		} else if (_propType === 'string' || _propType === 'number') {
			str = str.replace(new RegExp('\\#' + _propName + '\\#', 'gm'), _prop);
		}
	}
	return str;
}

String.prototype.formatWithParam = function() {
	var s = this, p = arguments[0] || {};
	s = formatWithParamInternal(s, null, p);
	return s;
};

String.prototype.format = String.prototype.f = function() {
	var s = this, i = arguments.length;

	while (i--) {
		s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
	}
	return s;
};
/* end of 문자열 포멧 함수 추가 */


	var slotInfo = [];
<?
foreach ($slot_list as $slot) {
	$slotrank = 2.5;
	foreach ($rank_list as $rank) {
		if ($rank["slottype"] == $slot["slotidx"])
			$slotrank = $rank["ranking_num"];
	}
?>
	slotInfo.push({slotidx:"<?= $slot["slotidx"] ?>", slotname:"<?= $slotnames[$slot["slotidx"]] ?>", totalbet:<?= $slot["bet1"] * $slot["linecount"] ?>, alignidx: <?= $slot["alignidx"] ?>, rank: <?= $slotrank ?>});
<? } ?>

	slotInfo.sort(function(a, b) {
		return a.rank > b.rank ? 1 : -1;
	});

	function view_slot_dtl(slotidx)
	{
		window.location.href = "/m8_monitoring/betrange_write.php?slotidx=" + slotidx + "&roomtype=<?= $tab ?>";
	}

	function tab_change(tab) {
		window.location.href = "<?= $pagename ?>?tab=" + tab;
	}

	function save_betrange_ratio_callback(result, reason)
	{
		if (!result) {
			alert("오류 발생 - " + reason);
		} else {
			window.location.href = "betrange.php?tab=<?= $tab ?>";
		}
	}

	/* jquery ui scripts */
	var $dialog = null;
	var $queryDialog = null;
	var $slotposition = null;
	var $ratioDialog = null;

	$(function() {
		$dialog = $('#dialog-form').dialog({
			autoOpen: false,
			height: 500,
			width: 500,
			modal: true,
			buttons: {
				'쿼리보기': function() {
					var $slotTitles = $slotposition.find('li:not(.template) .slot-title');
					var _query = '';
					for (var i = 0; i < $slotTitles.length; i++) {
						var _slotidx = $slotTitles.eq(i).data('slotidx');
						var _slotname = $slotTitles.eq(i).data('slotname');
						_query += 'update {0} set alignidx = {1} where slotidx = {2}; /* {3} */\r\n'.format('<?= $tab == 1 ? "tbl_normal_bet_info" : "tbl_highroller_bet_info" ?>', i + 1, _slotidx, _slotname);
					}
					$queryDialog.find('textarea').val(_query);
					$queryDialog.dialog("open");
				},
				'닫기': function() {
					$dialog.dialog('close');
				}
			}
		});

		$queryDialog = $('#query-dialog').dialog({
			autoOpen: false,
			height: 300,
			width: 800,
			modal: true
		});

		$ratioDialog = $('#ratio-dialog').dialog({
			autoOpen: false,
			height: 220,
			width: 420,
			modal: true,
			buttons: {
				'비율조정': function() {

					var param = {};
					param.betratio2 = $('#ratio-dialog').find('#bet2').val();
					param.betratio3 = $('#ratio-dialog').find('#bet3').val();
					param.betratio4 = $('#ratio-dialog').find('#bet4').val();
					param.betratio5 = $('#ratio-dialog').find('#bet5').val();

					WG_ajax_execute("game/update_betrange_ratio", param, save_betrange_ratio_callback);
				},
				'닫기': function() {
					$ratioDialog.dialog('close');
				}
			}
		});

		$slotposition = $('#slotposition');
		$slotposition.sortable();
		$slotposition.disableSelection();

		$('#reassignSlotsBtn').on('click', function() {
			$dialog.dialog('open');

			$slotposition.find('li:not(.template)').remove();

			var _lastSlotType = parseInt($('#newslotidx').val());
			var _newSlotInfo = [];

			var _direction = 1;
			var _unshiftCount = 0;
			for (var i = 0; i < slotInfo.length; i++) {
				if (slotInfo[i].slotidx > _lastSlotType)
					continue;

				if (_direction == 1) {
					_newSlotInfo.unshift(slotInfo[i]);
					_unshiftCount++;
				}
				else
					_newSlotInfo.push(slotInfo[i]);

				_direction = (_direction + 1) % 2;
			}

			console.log(_newSlotInfo);
			var _moveCount = _unshiftCount - <?= $tab == 1 ? 4 : 2 ?>;
			var _moveItems = _newSlotInfo.splice(0, _moveCount);
			_newSlotInfo = _newSlotInfo.concat(_moveItems);
			console.log(_newSlotInfo);

			var _templateHtml = $slotposition.find('.template')[0].outerHTML;
			for (i = 0; i < _newSlotInfo.length; i++) {
				var _slotInfo = _newSlotInfo[i];
				var $newLi = $(_templateHtml);
				$newLi.removeClass('template').show();
				$newLi.find('.slot-title').text('[' + _slotInfo.totalbet + '/' + _slotInfo.rank + '] ' + _slotInfo.slotname + ' (' + _slotInfo.slotidx + ')').data('slotidx', _slotInfo.slotidx).data('slotname', _slotInfo.slotname);
				$slotposition.append($newLi);
			}
			$slotposition.find('.ui-icon-circle-close').click(function() {
				$(this).closest('li').remove();
			});
		});

		$('#changeBetRatioBtn').on('click', function() {
			$ratioDialog.dialog('open');
		});
	});
	/* // jquery ui scripts */
</script>
<style>
#slotposition { list-style-type: none; margin: 0; padding: 0; width: 100%; }
#slotposition li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
#slotposition li span { margin-left: -1.3em; float: left; font-size: 12px; }
#slotposition li .ui-icon.ui-icon-circle-close { cursor: pointer; }
#slotposition li .slot-title { margin-left: 0px; }
#slotposition li:not(.template):nth-child(<?= $tab == 1 ? 5 : 3 ?>) { font-weight: bold; border-width: 2px; }
#slotposition li:not(.template):nth-child(<?= $tab == 1 ? 5 : 3 ?>) .slot-title:before { content: "[CENTER] "; }
</style>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 베팅레인지관리</div>
		<div style="float:right; margin-left:10px;">
			신규슬롯
			<select id="newslotidx">
<? for ($i = 0; $i < sizeof($slot_list); $i++) { ?>
				<option<?= $i == sizeof($slot_list) - 1 ? " selected" : "" ?>><?= $slot_list[$i]["slotidx"] ?></option>
<? } ?>
			</select>
			<input type="button" id="reassignSlotsBtn" class="btn_search" value="슬롯배치 조정"/>
<? if ($tab == 2) { ?>
			<input type="button" id="changeBetRatioBtn" class="btn_search" value="베팅비율 조정"/>
<? } ?>
		</div>
	</div>
	<!-- //title_warp -->

	<ul class="tab">
		<li id="tab_1" class="<?= ($tab == "1") ? "select" : "" ?>" onclick="tab_change('1')">레귤러 <?= ($tab == "1") ? "(".sizeof($slot_list).")" : "" ?></li>
		<li id="tab_2" class="<?= ($tab == "2") ? "select" : "" ?>" onclick="tab_change('2')">플래티넘 <?= ($tab == "2") ? "(".sizeof($slot_list).")" : "" ?></li>
	</ul>
	<table class="tbl_list_basic1" style="margin-top:5px">
		<colgroup>
			<col width="80">
			<col width="">
			<col width="80">
			<col width="80">
<? for ($i = 1; $i <= $betcount; $i++) { ?>
			<col width="60">
<? } ?>
		</colgroup>
		<thead>
			<tr>
				<th class="tdc">슬롯타입</th>
				<th class="tdc">슬롯이름</th>
				<th class="tdc">정렬순서</th>
				<th class="tdc">라인카운트</th>
	<?
		for ($i = 1; $i <= $betcount; $i++) {
	?>
				<th class="tdc">bet<?= $i ?></th>
	<?
		}
	?>
			</tr>
		</thead>
		<tbody>
<?
	$style = "";
	foreach ($slot_list as $slot) {
		$slotidx = $slot["slotidx"];
?>
			<tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="view_slot_dtl(<?= $slotidx ?>)">
				<td class="tdc" <?= $style ?>><?= $slotidx ?></td>
				<td class="tdl point_title"><?= $slotnames[$slotidx] ?></td>
				<td class="tdc" <?= $style ?>><?= $slot["alignidx"] ?></td>
				<td class="tdc" <?= $style ?>><?= $slot["linecount"] ?></td>
	<?
		for ($i = 1; $i <= $betcount; $i++) {
	?>
				<td class="tdc" <?= $style ?>><?= make_price_format($slot["bet".$i]) ?></td>
	<?
		}
	?>
			</tr>
<?
	}
?>
		</tbody>
	</table>
</div>
<!--  //CONTENTS WRAP -->

<!-- jquery ui widget -->
<div id="dialog-form" title="슬롯배치 조정">
	<ul id="slotposition">
		<li class="ui-state-default template" style="display:none;"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><span class="slot-title"></span><span class="ui-icon ui-icon-circle-close" style="float:right;"></span></li>
	</ul>
</div>
<div id="query-dialog" title="쿼리 보기">
	<textarea style="width:98%; height:98%;"></textarea>
</div>
<? if ($tab == 2) { ?>
<div id="ratio-dialog" title="플래티넘 베팅비율 조정">
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="50">
<? for ($i = 1; $i <= $betcount; $i++) { ?>
			<col width="">
<? } ?>
		</colgroup>
		<thead>
			<tr>
				<th>&nbsp;</th>
<? for ($i = 1; $i <= $betcount; $i++) { ?>
				<th class="tdc">bet<?= $i ?></th>
<? } ?>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>bet1</td>
<? for ($i = 1; $i <= $betcount; $i++) { ?>
				<td>x <input type="text" class="view_tbl_text" name="bet<?= $i ?>" id="bet<?= $i ?>" style="width:30px;" value="<?= $slot_list[0]["bet".$i] / $slot_list[0]["bet1"] ?>" onkeypress="return checkonlynum()"<?= $i == 1 ? " readonly" : "" ?> /></td>
<? } ?>
			</tr>
		</tbody>
	</table>
</div>
<? } ?>
<!-- //jquery ui widget -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
