<?
    $top_menu = "monitoring";
    $sub_menu = "slowquery_log_stats";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $term = ($_GET["term"] == "") ? "1" : $_GET["term"];
    $lastdate = $_GET["lastdate"];
    $startdate = ($_GET["startdate"] == "") ? date("Y-m-d"): $_GET["startdate"];
    $enddate = ($_GET["enddate"] == "") ? date("Y-m-d") : $_GET["enddate"];
    $latency_orderby = $_GET["latency"];
    $latencycount_orderby = $_GET["latencycount"];
    
    check_number($term);
    
    if ($term != "1" && $term != "5" && $term != "30" && $term != "60" && $term != "180" && $term != "360" && $term != "720" && $term != "1440")
        error_back("잘못된 접근입니다.");
    
    if ($lastdate == "")
    {
        $end_date = date("Y-m-d", time());
        $end_time = date("H:i:s", time());    
    }
    else 
    {
        $end_date = $lastdate;
        $end_time = "23:59:59";    
    }
    
    if ($term == "1") // 최근 6시간
    {
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time))-6,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time))-6,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));      
    }
    else if ($term == "5") // 최근 12시간
    {
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time))-12,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time))-12,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
    }
    else if ($term == "30") // 최근 1일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-1),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-1),date("Y",strtotime($end_date))));
    }
    else if ($term == "60") // 최근 2일 
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-2),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-2),date("Y",strtotime($end_date))));
    }
    else if ($term == "180") // 최근 5일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-5),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-5),date("Y",strtotime($end_date))));
    }
    else if ($term == "360") // 최근 7일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-7),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-7),date("Y",strtotime($end_date))));
    }
    else if ($term == "720") // 최근 10일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-10),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-10),date("Y",strtotime($end_date))));
    }
    else if ($term == "1440") // 최근 30일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-30),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-30),date("Y",strtotime($end_date))));
    }
    else if ($term == "10080") // 최근 60일
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-60),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-60),date("Y",strtotime($end_date))));
    }
    else if ($term == "43200") // 최근 12개월
    {        
        $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),(date("m", strtotime($end_date))-12),(date("d", strtotime($end_date))),date("Y",strtotime($end_date))));
        $start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),(date("m", strtotime($end_date))-12),(date("d", strtotime($end_date))),date("Y",strtotime($end_date))));
    }
    
    $late_where = " WHERE latency > 0  AND errcode = 0";
    $error_where = " WHERE errcode != '0' ";
    $period_tail = "";
    
    $db_analysis = new CDatabase_Analysis(); 
    
    $sql = "SELECT COUNT(*) AS latecount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
            "FROM query_slow_log ".
            "WHERE writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
            "GROUP BY minute ";

    $list1 = $db_analysis->gettotallist($sql);
    
    $orderby = "";
    
    if ($latency_orderby != "")
        $orderby = str_replace(":", " ", $latency_orderby);
    
    if ($latencycount_orderby != "")
    {
        if ($orderby == "")
            $orderby = str_replace(":", " ", $latencycount_orderby);
        else
            $orderby = ",".str_replace(":", " ", $latencycount_orderby);
    }
    
    if ($orderby == "")
        $orderby = "querycount desc, querytime desc";
    
    if ($startdate != "")
        $period_tail .= " AND writedate >= '$startdate 00:00:00'";
    
    if ($enddate != "")
        $period_tail .= " AND writedate <= '$enddate 23:59:59'";
    
    $sql = "SELECT COUNT(*) AS querycount, AVG(querytime) AS querytime, querytype FROM query_slow_log ".
            "WHERE 1=1 $period_tail GROUP BY querytype ORDER BY $orderby";
            
    $list2 = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) AS querycount, AVG(querytime) AS querytime, servername FROM query_slow_log ".
            "WHERE 1=1 $period_tail GROUP BY servername ORDER BY $orderby";
          
    $list3 = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) AS querycount, AVG(querytime) AS querytime, tablename ".
            "FROM query_slow_log AS A JOIN query_slow_table AS B ON A.queryidx=B.queryidx ".
            "WHERE 1=1 $period_tail GROUP BY tablename ORDER BY $orderby";
          
    $list4 = $db_analysis->gettotallist($sql);
    
    if (strstr($latency_orderby,":asc") != "")
        $latency_icon = "/images/icon/btn_up.png";
    else
        $latency_icon = "/images/icon/btn_down.png";
    
    if (strstr($latencycount_orderby,":asc") != "")
        $latencycount_icon = "/images/icon/btn_up.png";
    else
        $latencycount_icon = "/images/icon/btn_down.png";
    
    $db_analysis->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	
	function drawChart() 
	{
	    var datatable = new google.visualization.DataTable();
	    
	    datatable.addColumn('string', '시간');
	    datatable.addColumn('number', '쿼리건수');
	    datatable.addRows([
<?
	$start = floor(mktime(date("H",strtotime($start_time)), date("i",strtotime($start_time)), date("s", strtotime($start_time)), date("m",strtotime($start_date)), date("d", strtotime($start_date)), date("Y", strtotime($start_date)))/($term*60));
	$end = floor(mktime(date("H",strtotime($end_time)), date("i",strtotime($end_time)), date("s", strtotime($end_time)), date("m",strtotime($end_date)), date("d", strtotime($end_date)), date("Y", strtotime($end_date)))/($term*60)) + 1;
	
	for ($i=$start; $i<$end; $i=$i+1)
	{
	    $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
	    $temp_minute = $temp_time[0]*60 + $temp_time[1];
	  
	    $list_date = date("Y-m-d", $i*($term*60));
	    
	    if ($term > 10080)
	        echo("['".substr($list_date, 0, 7)."'");  
	    else if ($term > 720)
	        echo("['$list_date'");        
	    else
	        echo("['".make_time_format($temp_minute)."'");
	    
	    $print = false;
	    for ($j=0; $j<sizeof($list1); $j++)
	    {
	        if ($list1[$j]["minute"] == $i)
	        {
	            echo(",{v:".$list1[$j]["latecount"].",f:'".make_price_format($list1[$j]["latecount"])."'}");
	            $print = true;   
	            break;
	        }
	    }
	    
	    if (!$print)
	       echo(",0");
	    
	    if ($i+1 >= $end)
	        echo("]");  
	    else
	        echo("],");               
	}   
?>          
	]);
	
	    var options = {
	        title:'',            
	        axisTitlesPosition:'in',
	        curveType:'none',
	        focusTarget:'category',
	        interpolateNulls:'true',
	        legend:'top',
	        fontSize:12,
	        chartArea:{left:80,top:40,width:1000,height:400}
	    };
	
	    var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
	    chart.draw(datatable, options);
	}
	
	google.setOnLoadCallback(drawChart);
	
	function change_term(term)
	{
	    var search_form = document.search_form;        
	    var minute = document.getElementById("term_minute");
	    var minute5 = document.getElementById("term_5minute");
	    var minute30 = document.getElementById("term_30minute");
	    var minute60 = document.getElementById("term_60minute");
	    var minute180 = document.getElementById("term_180minute");
	    var minute360 = document.getElementById("term_360minute");
	    var minute720 = document.getElementById("term_720minute");
	    var minute1440 = document.getElementById("term_1440minute");
	    
	    search_form.term.value = term;
	    
	    if (term == "1")
	    {
	        minute.className="btn_schedule_select";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule";
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";  
	    }
	    else if (term == "5")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule_select";
	        minute30.className="btn_schedule";   
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";      
	    }
	    else if (term == "30")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule_select";  
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule"; 
	    }
	    else if (term == "60")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule"; 
	        minute60.className="btn_schedule_select";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";
	    }
	    else if (term == "180")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule"; 
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule_select";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";   
	    }
	    else if (term == "360")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule"; 
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule_select";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";   
	    }
	    else if (term == "720")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule"; 
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule_select";
	        minute1440.className="btn_schedule"; 
	    }
	    else if (term == "1440")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule";
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule_select";
	    }
	    
	    search_form.submit();
	}
	
	function search()
	{
	    var search_form = document.search_form;
	    search_form.submit();
	}   
	
	function change_orderby(id)
	{       
	    var img = document.getElementById(id).src;
	    var th = document.getElementById(id).parentNode.parentNode;
	    
	    if (img.indexOf("btn_down") != -1)
	    {
	        document.getElementById(id).src = "/images/icon/btn_up.png";
	        window.location.href = "client_log_stats.php?" + id + "=" + id + ":asc";
	    }
	    else
	    {            
	        document.getElementById(id).src = "/images/icon/btn_down.png";
	        window.location.href = "client_log_stats.php?" + id + "=" + id + ":desc";      
	    }
	}
	
	function check_sleeptime()
	{
	    setTimeout("window.location.reload(false)",60000);
	}
	
	$(function() {
	    $("#lastdate").datepicker({ });
	});
	
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="slowquery_log_stats.php">
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; Slow Query 통계</div>
			<input type="hidden" name="term" id="term" value="<?= $term ?>" />
			<div class="search_box">
				<input type="button" class="<?= ($term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="1분" id="term_minute" onclick="change_term(1)" />
				<input type="button" class="<?= ($term == "5") ? "btn_schedule_select" : "btn_schedule" ?>" value="5분"  id="term_5minute" onclick="change_term(5)" />
				<input type="button" class="<?= ($term == "30") ? "btn_schedule_select" : "btn_schedule" ?>" value="30분" id="term_30minute" onclick="change_term(30)" />
				<input type="button" class="<?= ($term == "60") ? "btn_schedule_select" : "btn_schedule" ?>" value="1시간" id="term_60minute" onclick="change_term(60)" />
				<input type="button" class="<?= ($term == "180") ? "btn_schedule_select" : "btn_schedule" ?>" value="3시간" id="term_180minute" onclick="change_term(180)" />
				<input type="button" class="<?= ($term == "360") ? "btn_schedule_select" : "btn_schedule" ?>" value="6시간" id="term_360minute" onclick="change_term(360)" />
				<input type="button" class="<?= ($term == "720") ? "btn_schedule_select" : "btn_schedule" ?>" value="12시간" id="term_720minute" onclick="change_term(720)" />
				<input type="button" class="<?= ($term == "1440") ? "btn_schedule_select" : "btn_schedule" ?>" value="1일" id="term_1440minute" onclick="change_term(1440)" />                    
				<input type="input" class="search_text" id="lastdate" name="lastdate" style="width:65px" value="<?= $lastdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</div>
		<!-- //title_warp -->
		
		<div style="font-weight:bold;">[Slow Query 통계 추이]</div>
		<div id="chart_div" style="height:500px; min-width:500px;"></div>
            
		<div style="height:30px;padding-top:40px;">
			<div style="font-weight:bold;float:left;">[Slow Query 통계 요약]</div>
			<div style="padding-right:15px;float:right;">
				<input type="input" class="search_text" id="startdate" name="startdate" style="width:65px" value="<?= $startdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
				<input type="input" class="search_text" id="enddate" name="enddate" style="width:65px" value="<?= $enddate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</div>
	</form>
            ​
	<div style="float:left;padding-top:10px;margin-right:15px;width:32%;">                    
		<table class="tbl_list_basic1"> 
			<thead>         
				<tr>
					<th>querytype</th>
					<th>총 쿼리건수</th>
					<th>평균 응답시간</th>
				</tr>
			</thead>
			<tbody>
<?
    for ($i=0; $i<sizeof($list2); $i++)
    {
        $querytype = $list2[$i]["querytype"];
        $querycount = $list2[$i]["querycount"];
        $querytime = $list2[$i]["querytime"];
?>
				<tr>
					<td class="tdc point"><?= $querytype ?></td>
					<td class="tdc"><?= $querycount ?></td>
					<td class="tdc"><?= $querytime ?></td>
				</tr>
<?
    }
?>
			</tbody>
		</table>
	</div>​
	<div style="float:left;padding-top:10px;margin-right:15px;width:32%;">                    
		<table class="tbl_list_basic1">    
			<thead>      
				<tr>
					<th>server</th>
					<th>총 쿼리건수</th>
					<th>평균 응답시간</th>
				</tr>
			</thead>
			<tbody>
<?
    for ($i=0; $i<sizeof($list3); $i++)
    {
        $servername = $list3[$i]["servername"];
        $querycount = $list3[$i]["querycount"];
        $querytime = $list3[$i]["querytime"];
?>
				<tr>
					<td class="tdc point"><?= $servername ?></td>
					<td class="tdc"><?= $querycount ?></td>
					<td class="tdc"><?= $querytime ?></td>
				</tr>
<?
    }
?>
			</tbody>
		</table>
	</div>​
	<div style="float:left;padding-top:10px;width:32%;">                    
		<table class="tbl_list_basic1">    
			<thead>      
				<tr>
					<th>table</th>
					<th>총 쿼리건수</th>
					<th>평균 응답시간</th>
				</tr>
			</thead>
			<tbody>
<?
    for ($i=0; $i<sizeof($list4); $i++)
    {
        $tablename = $list4[$i]["tablename"];
        $querycount = $list4[$i]["querycount"];
        $querytime = $list4[$i]["querytime"];
?>
				<tr>
					<td class="tdc point"><?= $tablename ?></td>
					<td class="tdc"><?= $querycount ?></td>
					<td class="tdc"><?= $querytime ?></td>
				</tr>
<?
    }
?>
			</tbody>
		</table>
	</div>
</div>
<!--  //CONTENTS WRAP -->

<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>