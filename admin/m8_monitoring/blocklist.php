<?
	$top_menu = "monitoring";
	$sub_menu = "blocklist";

	 include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$page = ($_GET["page"] == "") ? "1" : $_GET["page"];
	
	$listcount = "10";
	
	$db_main = new CDatabase_Main();
	
	$sql = "SELECT count(*) FROM tbl_user_block";
	
	$totalcount = $db_main->getvalue($sql);
	
	$sql = "SELECT * FROM tbl_user_block ". 
			"ORDER BY blockdate DESC ".
			"LIMIT ".(($page-1) * $listcount).", ".$listcount;

	$block_list = $db_main->gettotallist($sql);
		
	if ($totalcount < ($page-1) * $listcount && page != 1)
		$page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 차단 사용자 목록<span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
	</div>
	<!-- //title_warp -->
            
	<table class="tbl_list_basic1">
        <colgroup>
            <col width="100">
            <col width="">
            <col width="100">
            <col width="180">
            <col width="100">
            <col width="150">
            <col width="150">
        </colgroup>
        <thead>
      	    <tr>
                <th>번호</th>
                <th>이름</th>
                <th>블락카운트</th>
                <th>사유</th>
                <th>현재코인</th>
                <th>가입일</th>
                <th>등록일</th>
           	</tr>
		</thead> 
		<tbody>
<?
    for($i=0; $i<sizeof($block_list); $i++)
    {
    	$useridx = $block_list[$i]["useridx"];
    	$blockcount = $block_list[$i]["blockcount"];
    	$reason = $block_list[$i]["reason"];
    	$blockdate = $block_list[$i]["blockdate"];

    	$sql = "SELECT userid,nickname,coin,createdate,(SELECT COUNT(*) FROM tbl_user_online_sync2 WHERE useridx=tbl_user.useridx) AS online_status FROM tbl_user WHERE useridx='$useridx'";
    	$userinfo = $db_main->getarray($sql);
    	
    	$photourl = get_fb_pictureURL($userinfo["userid"],$client_accesstoken);
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="tdc point"><?= ($page - 1) * $listcount + $i + 1?></td>
            <td class="point_title" onclick="view_user_dtl(<?= $useridx ?>,'')"><span style="float:left;padding-top:8px;padding-right:2px;"><img src="/images/icon/<?= ($userinfo["online_status"] == "1") ? "status_1.png" : "status_3.png" ?>" align="absmiddle" /></span>  <span style="float:left;"><img src="<?= $photourl ?>" height="25" width="25" align="absmiddle" hspace="3"></span> <?= $userinfo["nickname"] ?>&nbsp;&nbsp;<img src="/images/icon/facebook.png" height="20" width="20" align="absmiddle" style="cursor:pointer" onclick="event.cancelBubble=true;window.open('http://www.facebook.com/<?= $userinfo["userid"] ?>')" /></td>
            <td class="tdc"><?= $blockcount ?></td>
            <td class="tdc"><?= $reason ?></td>
            <td class="tdc"><?= number_format($userinfo["coin"]) ?></td>            
            <td class="tdc"><?= $userinfo["createdate"] ?></td>
            <td class="tdc"><?= $blockdate ?></td>
        </tr>
<?
    }

    $db_main->end();    
?>

		</tbody>
	</table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>