<?
    $top_menu = "monitoring";
    $sub_menu = "online_log_stats";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $term = ($_GET["term"] == "") ? "1" : $_GET["term"];
    $lastdate = $_GET["lastdate"];
    $search_serveridx = $_GET["serveridx"];
    $search_objectidx = $_GET["objectidx"];
    
    check_number($term);
    
    if ($term != "1" && $term != "5" && $term != "30" && $term != "60" && $term != "180" && $term != "360" && $term != "720" && $term != "1440" && $term != "10080" && $term != "43200")
    	error_back("잘못된 접근입니다.");
    
    if ($lastdate == "")
    {
    	$end_date = date("Y-m-d", time());
    	$end_time = date("H:i:s", time());
    }
    else
    {
    	$end_date = $lastdate;
    	$end_time = "23:59:59";
    }
    
    if ($term == "1") // 최근 6시간
    {
    	$start_date = date("Y-m-d", mktime(date("H", strtotime($end_time))-6,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
    	$start_time = date("H:i:s", mktime(date("H", strtotime($end_time))-6,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
    }
    else if ($term == "5") // 최근 12시간
    {
    	$start_date = date("Y-m-d", mktime(date("H", strtotime($end_time))-12,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
    	$start_time = date("H:i:s", mktime(date("H", strtotime($end_time))-12,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
    }
    else if ($term == "30") // 최근 1일
    {
    	$start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-1),date("Y",strtotime($end_date))));
    	$start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-1),date("Y",strtotime($end_date))));
    }
    else if ($term == "60") // 최근 2일
    {
    	$start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-2),date("Y",strtotime($end_date))));
    	$start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-2),date("Y",strtotime($end_date))));
    }
    else if ($term == "180") // 최근 5일
    {
    	$start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-5),date("Y",strtotime($end_date))));
    	$start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-5),date("Y",strtotime($end_date))));
    }
    else if ($term == "360") // 최근 7일
    {
    	$start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-7),date("Y",strtotime($end_date))));
    	$start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-7),date("Y",strtotime($end_date))));
    }
    else if ($term == "720") // 최근 10일
    {
    	$start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-10),date("Y",strtotime($end_date))));
    	$start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-10),date("Y",strtotime($end_date))));
    }
    else if ($term == "1440") // 최근 30일
    {
    	$start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-30),date("Y",strtotime($end_date))));
    	$start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-30),date("Y",strtotime($end_date))));
    }
    else if ($term == "10080") // 최근 60일
    {
    	$start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-60),date("Y",strtotime($end_date))));
    	$start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),(date("d", strtotime($end_date))-60),date("Y",strtotime($end_date))));
    }
    else if ($term == "43200") // 최근 12개월
    {
    	$start_date = date("Y-m-d", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),(date("m", strtotime($end_date))-12),(date("d", strtotime($end_date))),date("Y",strtotime($end_date))));
    	$start_time = date("H:i:s", mktime(date("H", strtotime($end_time)),date("i", strtotime($end_time)),date("s", strtotime($end_time)),(date("m", strtotime($end_date))-12),(date("d", strtotime($end_date))),date("Y",strtotime($end_date))));
    }
    
    $user_where = " WHERE isnpc=0 ";
    $npc_where = " WHERE isnpc=1 ";
    $tail = "";
    
    if ($search_serveridx != "")
    	$tail .= " AND serveridx=$search_serveridx";
    
    if ($search_cityidx != "")
    	$tail .= " AND objectidx=$search_objectidx";
    
    $db_analysis = new CDatabase_Analysis();
    
    $sql = "SELECT SUM(totalcount)/COUNT(DISTINCT logkey) AS webusercount, ".
    		"FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM user_online_log ".
    		"$user_where $tail AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $userlist = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT SUM(totalcount)/COUNT(DISTINCT logkey) AS npccount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM user_online_log ".
    		"$npc_where $tail AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $npclist = $db_analysis->gettotallist($sql);
    
    $db_analysis->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	
	function drawChart() 
	{
	    var datatable1 = new google.visualization.DataTable();
	    
	    datatable1.addColumn('string', '시간');
	    datatable1.addColumn('number', '전체');
	    datatable1.addRows([
<?
	$start = round(mktime(date("H",strtotime($start_time)), date("i",strtotime($start_time)), date("s", strtotime($start_time)), date("m",strtotime($start_date)), date("d", strtotime($start_date)), date("Y", strtotime($start_date)))/($term*60));
	$end = round(mktime(date("H",strtotime($end_time)), date("i",strtotime($end_time)), date("s", strtotime($end_time)), date("m",strtotime($end_date)), date("d", strtotime($end_date)), date("Y", strtotime($end_date)))/($term*60)) + 1;
	
	for ($i=$start; $i<$end; $i=$i+1)
	{      
	    $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
	    $temp_minute = $temp_time[0]*60 + $temp_time[1];
	  
	    $list_date = date("Y-m-d", $i*($term*60));
	    
	    if ($term > 10080)
	        echo("['".substr($list_date, 0, 7)."'");  
	    else if ($term > 720)
	        echo("['$list_date'");        
	    else
	        echo("['".make_time_format($temp_minute)."'");
	    
	    $print = false;
	    for ($j=0; $j<sizeof($userlist); $j++)
	    {
	        if ($userlist[$j]["minute"] == $i)
	        {
	            echo(",{v:".round($userlist[$j]["webusercount"]).",f:'".make_price_format(round($userlist[$j]["webusercount"]))."'}");
	            $print = true;      
	            break;
	        }
	    }
	    
	    if (!$print)
	       echo(",0");
	    
	    if ($i+1 >= $end)
	        echo("]");  
	    else
	        echo("],");               
	}    
?>          
	    ]);
	    
	    var datatable2 = new google.visualization.DataTable();
	    
	    datatable2.addColumn('string', '시간');
	    datatable2.addColumn('number', 'NPC');
	    datatable2.addRows([
<?
	for ($i=$start; $i<$end; $i=$i+1)
	{      
	    $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
	    $temp_minute = $temp_time[0]*60 + $temp_time[1];
	  
	    $list_date = date("Y-m-d", $i*($term*60));
	    
	    if ($term > 10080)
	        echo("['".substr($list_date, 0, 7)."'");  
	    else if ($term > 720)
	        echo("['$list_date'");        
	    else
	        echo("['".make_time_format($temp_minute)."'");
	    
	    $print = false;
	    for ($j=0; $j<sizeof($npclist); $j++)
	    {
	        if ($npclist[$j]["minute"] == $i)
	        {
	            echo(",{v:".round($npclist[$j]["npccount"]).",f:'".make_price_format(round($npclist[$j]["npccount"]))."'}");
	            $print = true;      
	            break;
	        }
	    }
	    
	    if (!$print)
	       echo(",0");
	    
	    if ($i+1 >= $end)
	        echo("]");  
	    else
	        echo("],");  
	                 
	}    
?>          
	    ]);
	
	    var options1 = {
	        axisTitlesPosition:'in',  
	        curveType:'none',
	        focusTarget:'category',
	        interpolateNulls:'true',
	        legend:'top',
	        fontSize:12,
	        colors: ['#0000CD', '#008000', '#D2691E', '#C0C0C0', '#FFD700'],
	        'chartArea':{left:70,top:40,width:1070,height:130}
	    };
	
	    var options2 = {
	        axisTitlesPosition:'in',  
	        curveType:'none',
	        focusTarget:'category',
	        interpolateNulls:'true',
	        legend:'top',
	        fontSize:12,
	        'chartArea':{left:70,top:40,width:1070,height:130}
	    };
	
	    var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
	    chart.draw(datatable1, options1);
	    
	    chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
	    chart.draw(datatable2, options2);
	}
	
	google.setOnLoadCallback(drawChart);
	
	function change_term(term)
	{
	    var search_form = document.search_form;        
	    var minute = document.getElementById("term_minute");
	    var minute5 = document.getElementById("term_5minute");
	    var minute30 = document.getElementById("term_30minute");
	    var minute60 = document.getElementById("term_60minute");
	    var minute180 = document.getElementById("term_180minute");
	    var minute360 = document.getElementById("term_360minute");
	    var minute720 = document.getElementById("term_720minute");
	    var minute1440 = document.getElementById("term_1440minute");
	    var minute10080 = document.getElementById("term_10080minute");
	    var minute43200 = document.getElementById("term_43200minute");
	    
	    search_form.term.value = term;
	    
	    if (term == "1")
	    {
	        minute.className="btn_schedule_select";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule";
	        minute60.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";
	        minute10080.className="btn_schedule"; 
	        minute43200.className="btn_schedule";     
	    }
	    else if (term == "5")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule_select";
	        minute30.className="btn_schedule";   
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";
	        minute10080.className="btn_schedule"; 
	        minute43200.className="btn_schedule";           
	    }
	    else if (term == "30")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule_select";  
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";
	        minute10080.className="btn_schedule"; 
	        minute43200.className="btn_schedule";    
	    }
	    else if (term == "60")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule"; 
	        minute60.className="btn_schedule_select";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";
	        minute10080.className="btn_schedule"; 
	        minute43200.className="btn_schedule";      
	    }
	    else if (term == "180")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule"; 
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule_select";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";
	        minute10080.className="btn_schedule";  
	        minute43200.className="btn_schedule";      
	    }
	    else if (term == "360")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule"; 
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule_select";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";
	        minute10080.className="btn_schedule";  
	        minute43200.className="btn_schedule";      
	    }
	    else if (term == "720")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule"; 
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule_select";
	        minute1440.className="btn_schedule";
	        minute10080.className="btn_schedule";  
	        minute43200.className="btn_schedule";     
	    }
	    else if (term == "1440")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule";
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule_select";
	        minute10080.className="btn_schedule";  
	        minute43200.className="btn_schedule";     
	    }
	    else if (term == "10080")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule";
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";
	        minute10080.className="btn_schedule_select"; 
	        minute43200.className="btn_schedule";  
	    }
	    else if (term == "43200")
	    {
	        minute.className="btn_schedule";
	        minute5.className="btn_schedule";
	        minute30.className="btn_schedule";
	        minute60.className="btn_schedule";
	        minute180.className="btn_schedule";
	        minute360.className="btn_schedule";
	        minute720.className="btn_schedule";
	        minute1440.className="btn_schedule";
	        minute10080.className="btn_schedule";  
	        minute43200.className="btn_schedule_select";     
	    }
	    
	    search_form.submit();
	}

	function change_orderby(id)
	{       
	    var img = document.getElementById(id).src;
	    
	    if (img.indexOf("btn_down") != -1)
	    {
	        document.getElementById(id).src = "/images/icon/btn_up.png";
	        window.location.href = "client_log_stats.php?" + id + "=" + id + ":asc";
	    }
	    else
	    {            
	        document.getElementById(id).src = "/images/icon/btn_down.png";
	        window.location.href = "client_log_stats.php?" + id + "=" + id + ":desc";      
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	    search_form.submit();
	}
	
	function check_sleeptime()
	{
		setTimeout("window.location.reload(false)",60000);
	}

	
	$(function() {
	    $("#lastdate").datepicker({ });
	});
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="online_log_stats.php">
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 현재사용자 접속상태</div>
			<input type="hidden" name="term" id="term" value="<?= $term ?>" />
		</div>
		<!-- //title_warp -->
		
		<div class="search_box" style="float:none;text-align:right;">
			<select name="serveridx" id="serveridx" onchange="search()">
				<option value="">서버 선택</option>
				<option value="1" <?= ($search_serveridx == '1') ? "selected" : "" ?>>Common1</option>
				<option value="2" <?= ($search_serveridx == '2') ? "selected" : "" ?>>Common2</option>
				<option value="3" <?= ($search_serveridx == '3') ? "selected" : "" ?>>Common3</option>
			</select>
			
			<input type="button" class="<?= ($term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="1분" id="term_minute" onclick="change_term(1)" />
			<input type="button" class="<?= ($term == "5") ? "btn_schedule_select" : "btn_schedule" ?>" value="5분"  id="term_5minute" onclick="change_term(5)" />
			<input type="button" class="<?= ($term == "30") ? "btn_schedule_select" : "btn_schedule" ?>" value="30분" id="term_30minute" onclick="change_term(30)" />
			<input type="button" class="<?= ($term == "60") ? "btn_schedule_select" : "btn_schedule" ?>" value="1시간" id="term_60minute" onclick="change_term(60)" />
			<input type="button" class="<?= ($term == "180") ? "btn_schedule_select" : "btn_schedule" ?>" value="3시간" id="term_180minute" onclick="change_term(180)" />
			<input type="button" class="<?= ($term == "360") ? "btn_schedule_select" : "btn_schedule" ?>" value="6시간" id="term_360minute" onclick="change_term(360)" />
			<input type="button" class="<?= ($term == "720") ? "btn_schedule_select" : "btn_schedule" ?>" value="12시간" id="term_720minute" onclick="change_term(720)" />
			<input type="button" class="<?= ($term == "1440") ? "btn_schedule_select" : "btn_schedule" ?>" value="1일" id="term_1440minute" onclick="change_term(1440)" />
			<input type="button" class="<?= ($term == "10080") ? "btn_schedule_select" : "btn_schedule" ?>" value="7일" id="term_10080minute" onclick="change_term(10080)" />
			<input type="button" class="<?= ($term == "43200") ? "btn_schedule_select" : "btn_schedule" ?>" value="30일" id="term_43200minute" onclick="change_term(43200)" />                    
			<input type="input" class="search_text" id="lastdate" name="lastdate" style="width:65px" value="<?= $lastdate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />                    
			<input type="button" class="btn_search" value="검색" onclick="search()" />
		</div>
	</form>
	
	<!-- //title_warp -->
	<div id="chart_div1" style="height:230px; min-width: 500px"></div>
	<div id="chart_div2" style="height:230px; min-width: 500px"></div>
</div>
<!--  //CONTENTS WRAP -->

<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>