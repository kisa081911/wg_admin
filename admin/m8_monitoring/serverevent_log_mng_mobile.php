<?
    $top_menu = "monitoring";
    $sub_menu = "serverevent_log_mobile";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_order_by = ($_GET["orderby"] == "") ? "writedate desc" : $_GET["orderby"];
    
    $search_ipaddress = trim($_GET["ipaddress"]);
    $search_category = trim($_GET["category"]);
    $search_contents = trim($_GET["contents"]);
    $search_start_writedate = $_GET["start_writedate"];
    $search_end_writedate = $_GET["end_writedate"];
    $search_before_minute = $_GET["before_minute"];
    
    check_number($search_category);  
    check_xss($search_ipaddress);  
    check_xss($search_contents);  
    
    $listcount = "10";
    $pagename = "serverevent_log_mng_mobile.php";
    $pagefield = "orderby=$search_order_by&ipaddress=$search_ipaddress&category=$search_category&contents=$search_contents&start_writedate=$search_start_writedate&end_writedate=$search_end_writedate&before_minute=$search_before_minute";
    
    $tail = " WHERE contents NOT LIKE '%error packet (1, 39)%' "; 
    $order_by = "ORDER BY ".str_replace(":", " ", $search_order_by);
    
    if ($search_ipaddress != "")
        $tail .= " AND ipaddress LIKE '%$search_ipaddress%' ";
    
    if ($search_category != "")
        $tail .= " AND category = '$search_category' ";
    
    if ($search_contents != "")
        $tail .= " AND contents LIKE '%$search_contents%' ";
        
    if ($search_before_minute != "")
        $tail .= " AND writedate <= DATE_SUB(NOW(), INTERVAL $search_before_minute MINUTE) ";
    
    if ($search_start_writedate != "")
        $tail .= " AND writedate >= '$search_start_writedate 00:00:00' ";
    
    if ($search_end_writedate != "")
        $tail .= " AND writedate <= '$search_end_writedate 23:59:59' ";
   
    $db_analysis = new CDatabase_Analysis();
    
    $totalcount = $db_analysis->getvalue("SELECT COUNT(*) FROM server_log_mobile $tail");
    
    $sql = "SELECT logidx,category,ipaddress,contents,writedate FROM server_log_mobile $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    $loglist = $db_analysis->gettotallist($sql); 
    
    $db_analysis->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
        $page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }
    
    function search()
    {
        var search_form = document.search_form;

        search_form.submit();
    }

    $(function() {
        $("#start_writedate").datepicker({ });
    });
    
    $(function() {
        $("#end_writedate").datepicker({ });
    });
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 서버 이벤트 로그(Mobile) <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
	</div>
	<!-- //title_warp -->
            
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="serverevent_log_mng_mobile.php">
		<div class="detail_search_wrap">
			<span class="search_lbl">IP 주소</span>
			<input type="input" class="search_text" id="ipaddress" name="ipaddress" style="width:120px" value="<?= encode_html_attribute($search_ipaddress) ?>" onkeypress="search_press(event)" />
                    
			<span class="search_lbl ml20">이벤트 종류</span>
			<select name="category" id="category">
				<option value="">선택</option>
				<option value="1" <?= ($search_category == "1") ? "selected" : "" ?>>Info</option>
				<option value="2" <?= ($search_category == "2") ? "selected" : "" ?>>Warning</option>
				<option value="3" <?= ($search_category == "3") ? "selected" : "" ?>>Error</option> 
			</select>
                    
			<span class="search_lbl ml20">오류 메시지</span>
			<input type="input" class="search_text" id="contents" name="contents" style="width:200px" value="<?= encode_html_attribute($search_contents) ?>" onkeypress="search_press(event)" />
                    
			<span class="search_lbl ml20">기록일시</span>
			<input type="input" class="search_text" id="start_writedate" name="start_writedate" style="width:65px" value="<?= $search_start_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
			<input type="input" class="search_text" id="end_writedate" name="end_writedate" style="width:65px" value="<?= $search_end_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
                    
			<div class="clear"  style="padding-top:10px"></div>
                    
			<span class="search_lbl">시간검색</span>
			<input type="input" class="search_text" id="before_minute" name="before_minute" style="width:40px" value="<?= $search_before_minute ?>" onkeypress="search_press(event); return checkonlynum();" />분 전
                    
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	            
		<div style="float:right;padding-bottom:5px;">
			<select name="orderby" id="orderby" onchange="search()">
				<option value="" <?= ($search_order_by == "") ? "selected" : "" ?>>정렬선택</option>
				<option value="ipaddress:asc" <?= ($search_order_by == "ipaddress:asc") ? "selected" : "" ?>>ip주소 오름차순</option>
				<option value="ipaddress:desc" <?= ($search_order_by == "ipaddress:desc") ? "selected" : "" ?>>ip주소 내림차순</option>
				<option value="writedate:asc" <?= ($search_order_by == "writedate:asc") ? "selected" : "" ?>>기록일시 오름차순</option>
				<option value="writedate:desc" <?= ($search_order_by == "writedate:desc") ? "selected" : "" ?>>기록일시 내림차순</option>
			</select>
		</div>
	</form>
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="5">
			<col width="50">
			<col width="100">
			<col width="150">
			<col width="">
			<col width="150">
		</colgroup>
		<thead>
            <tr>
                <th style="padding-left:5px"></th>
                <th>번호</th>
                <th>IP 주소</th>
                <th>이벤트 종류</th>
                <th>오류 메시지</th>
                <th>기록일시</th>
            </tr>
		</thead>
		<tbody>
<?
    for($i=0; $i<sizeof($loglist); $i++)
    {
        $logidx = $loglist[$i]["logidx"];
        $ipaddress = $loglist[$i]["ipaddress"];
        $category = $loglist[$i]["category"];
        $contents = $loglist[$i]["contents"];
        $writedate = $loglist[$i]["writedate"];
        
        if ($category == "1")
        {
            $category_string = "Info";
            $style = "";
            $img = "/images/icon/info.png";
        }
        else if ($category == "2")
        {
            $category_string = "Warning";
            $style = "";
            $img = "/images/icon/warning.png";
        }
        else if ($category == "3")
        {
            $category_string = "Error";
            $style = "style='color:#fb7878;'";
            $img = "/images/icon/error.png";
        }
?>
            <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="">
                <td class="tdc" style="padding-left:5px"><img src="<?= $img ?>" align="absmiddle" /></td>
                <td class="tdc" <?= $style ?>><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
                <td class="tdc point" <?= $style ?>><?= $ipaddress ?></td>
                <td class="tdc point" <?= $style ?>><?= $category_string ?></td>
                <td class="tdl point" <?= $style ?>><?= encode_html_attribute($contents) ?></td>
                <td class="tdc" <?= $style ?>><?= $writedate ?></td>
            </tr>
<?
    } 
?>
		</tbody>
	</table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
