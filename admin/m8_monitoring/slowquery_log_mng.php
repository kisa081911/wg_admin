<?
    $top_menu = "monitoring";
    $sub_menu = "slowquery_log";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_order_by = ($_GET["orderby"] == "") ? "writedate desc" : $_GET["orderby"];
    
    $search_servername = $_GET["servername"];
    $search_tablename = $_GET["tablename"];
    $search_querytype = $_GET["querytype"];
    $search_content = trim($_GET["content"]);
    $search_start_querytime = $_GET["start_querytime"];
    $search_end_querytime = $_GET["end_querytime"];
    $search_start_writedate = $_GET["start_writedate"];
    $search_end_writedate = $_GET["end_writedate"];
    $search_before_minute = $_GET["before_minute"];
    
    check_number($search_start_querytime.$search_end_querytime);
    
    $listcount = "10";
    $pagename = "slowquery_log_mng.php";
    $pagefield = "orderby=$search_order_by&servername=$search_servername&tablename=$search_tablename&querytype=$search_querytype&content=$search_content&start_querytime=$search_start_querytime&end_querytime=$search_end_querytime&start_writedate=$search_start_writedate&end_writedate=$search_end_writedate&before_minute=$search_before_minute";
    
    $tail = " WHERE 1=1";
    $order_by = "ORDER BY ".str_replace(":", " ", $search_order_by);
    
    if ($search_servername != "")
    	$tail .= " AND servername='$search_servername' ";
    
    if ($search_tablename != "")
    	$tail .= " AND tablename='$search_tablename' ";
    
    if ($search_querytype != "")
    	$tail .= " AND querytype='$search_querytype' ";
    
    if ($search_content != "")
    	$tail .= " AND content LIKE '%$search_content%' ";
    
    if ($search_before_minute != "")
    	$tail .= " AND writedate <= DATE_SUB(NOW(), INTERVAL $search_before_minute MINUTE) ";
    
    if ($search_start_querytime != "")
    	$tail .= " AND querytime >= '$search_start_querytime' ";
    
    if ($search_end_querytime != "")
    	$tail .= " AND querytime <= '$search_end_querytime' ";
    
    if ($search_start_writedate != "")
    	$tail .= " AND writedate >= '$search_start_writedate 00:00:00' ";
    
    if ($search_end_writedate != "")
    	$tail .= " AND writedate <= '$search_end_writedate 23:59:59' ";
     
    $db_analysis = new CDatabase_Analysis();
    
    $totalcount = $db_analysis->getvalue("SELECT COUNT(*) FROM query_slow_log AS A JOIN query_slow_table AS B ON A.queryidx=B.queryidx $tail");
    
    $sql = "SELECT A.queryidx,ipaddress,servername,querytype,querytime,content,tablename,writedate ".
    		"FROM query_slow_log AS A JOIN query_slow_table AS B ON A.queryidx=B.queryidx $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    $loglist = $db_analysis->gettotallist($sql);
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
    	$page = floor(($totalcount + $listcount - 1) / $listcount);
    
    $server_list = $db_analysis->gettotallist("SELECT servername FROM query_slow_log AS A JOIN query_slow_table B ON A.queryidx=B.queryidx GROUP BY servername");
    $table_list = $db_analysis->gettotallist("SELECT tablename FROM query_slow_log AS A JOIN query_slow_table B ON A.queryidx=B.queryidx GROUP BY tablename");
    $querytype_list = $db_analysis->gettotallist("SELECT querytype FROM query_slow_log AS A JOIN query_slow_table B ON A.queryidx=B.queryidx GROUP BY querytype");
    
    $db_analysis->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script> 
<script type="text/javascript">
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	    search_form.submit();
	}
	
	$(function() {
	    $("#start_writedate").datepicker({ });
	});
	
	$(function() {
	    $("#end_writedate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Slow Query 로그 <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
	</div>
	<!-- //title_warp -->
	
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
		<div class="detail_search_wrap">
			<span class="search_lbl">server</span>
			<select name="servername" id="servername">                                    
				<option value="">선택</option>
<?
    		for ($i=0; $i<sizeof($server_list); $i++)
    		{
?>
				<option value="<?= $server_list[$i]["servername"] ?>" <?= ($search_servername == $server_list[$i]["servername"]) ? "selected" : "" ?>><?= $server_list[$i]["servername"] ?></option>

<?
    		}
?>
			</select>
                    
			<span class="search_lbl ml20">table</span>
			<select name="tablename" id="tablename">                                    
				<option value="">선택</option>
<?
    		for ($i=0; $i<sizeof($table_list); $i++)
    		{
?>
				<option value="<?= $table_list[$i]["tablename"] ?>" <?= ($search_tablename == $table_list[$i]["tablename"]) ? "selected" : "" ?>><?= $table_list[$i]["tablename"] ?></option>

<?
    		}
?>
			</select>
                    
			<span class="search_lbl ml20">querytype</span>
			<select name="querytype" id="querytype">                                    
				<option value="">선택</option>
<?
    		for ($i=0; $i<sizeof($querytype_list); $i++)
    		{
?>
				<option value="<?= $querytype_list[$i]["querytype"] ?>" <?= ($search_querytype == $querytype_list[$i]["querytype"]) ? "selected" : "" ?>><?= $querytype_list[$i]["querytype"] ?></option>

<?
    		}
?>
			</select>
                    
			<span class="search_lbl ml20">응답시간</span>
			<input type="input" class="search_text" id="start_querytime" name="start_querytime" style="width:120px" value="<?= $search_start_querytime ?>" onkeypress="search_press(event); return checkonlynum();" /> -
			<input type="input" class="search_text" id="end_querytime" name="end_querytime" style="width:120px" value="<?= $search_end_querytime ?>" onkeypress="search_press(event); return checkonlynum();"/>
                    
			<div class="clear"  style="padding-top:10px"></div>
                    
			<span class="search_lbl">기록일시</span>
			<input type="input" class="search_text" id="start_writedate" name="start_writedate" style="width:65px" value="<?= $search_start_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
			<input type="input" class="search_text" id="end_writedate" name="end_writedate" style="width:65px" value="<?= $search_end_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
                    
			<span class="search_lbl ml20">내용</span>
			<input type="input" class="search_text" id="content" name="content" style="width:150px" value="<?= encode_html_attribute($search_content) ?>" onkeypress="search_press(event)" />
                    
			<span class="search_lbl ml20">시간검색</span>
			<input type="input" class="search_text" id="before_minute" name="before_minute" style="width:40px" value="<?= $search_before_minute ?>" onkeypress="search_press(event); return checkonlynum();" />분 전
                    
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
                
		<div style="float:right;padding-bottom:5px;">
			<select name="orderby" id="orderby" onchange="search()">
				<option value="" <?= ($search_order_by == "") ? "selected" : "" ?>>정렬선택</option>
				<option value="writedate:asc" <?= ($search_order_by == "writedate:asc") ? "selected" : "" ?>>기록일시 오름차순</option>
				<option value="writedate:desc" <?= ($search_order_by == "writedate:desc") ? "selected" : "" ?>>기록일시 내림차순</option>
			</select>
		</div>
	</form>
	
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="50">
			<col width="120">
			<col width="120">
			<col width="80">
			<col width="80">
			<col width="">
			<col width="120">
		</colgroup>
		<thead>
            <tr>
                <th>번호</th>
                <th>server</th>
                <th>table</th>
                <th>querytype</th>
                <th>응답시간</th>
                <th>내용</th>
                <th>기록일시</th>
            </tr>
		</thead> 
		<tbody>
<?
    for($i=0; $i<sizeof($loglist); $i++)
    {
        $queryidx = $loglist[$i]["queryidx"];
        $ipaddress = $loglist[$i]["ipaddress"];
        $servername = $loglist[$i]["servername"];
        $tablename = $loglist[$i]["tablename"];
        $querytype = $loglist[$i]["querytype"];
        $querytime = $loglist[$i]["querytime"];
        $content = $loglist[$i]["content"];
        $writedate = $loglist[$i]["writedate"];
?>
            <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="">
                <td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
                <td class="tdc point"><?= $servername ?></td>
                <td class="tdc point"><?= $tablename ?></td>
                <td class="tdc point"><?= $querytype ?></td>
                <td class="tdc point"><?= $querytime ?></td>
                <td class="tdl point"><?= encode_html_attribute($content) ?></td>
                <td class="tdc"><?= $writedate ?></td>
            </tr>
<?
    } 
?>
		</tbody>
	</table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
</div>
<!--  //CONTENTS WRAP -->

<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>