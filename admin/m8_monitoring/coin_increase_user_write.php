<?
    $top_menu = "monitoring";
	$sub_menu = "coin_increase_user";
    
    include ($_SERVER ["DOCUMENT_ROOT"] . "/m_common/top_frame.inc.php");
    
    $logidx = $_GET ["logidx"];
    
    check_number($logidx);
    
    $db_analysis = new CDatabase_Analysis();
    
    if($logidx != "") 
    {
    	$sql = "SELECT logidx FROM tbl_coin_increase_user_list WHERE logidx = $logidx";
    	$logidx = $db_analysis->getvalue($sql);
    	
    	if($logidx == "")
    		error_back("존재하지 않는 데이터입니다.");
    	
    	$sql = "SELECT * FROM tbl_coin_increase_user_list WHERE logidx = $logidx";
    	$coin_increase_user_info = $db_analysis->getarray($sql);
    	
    	$logidx = $coin_increase_user_info["logidx"];
    	$useridx = $coin_increase_user_info["useridx"];
    	$coin = $coin_increase_user_info["coin"];
    	$adflag = $coin_increase_user_info["adflag"];
    	$createdate = $coin_increase_user_info["createdate"];
    	$writedate = $coin_increase_user_info["writedate"];
    	$status = $coin_increase_user_info["status"];
    	$comment = $coin_increase_user_info["comment"];
    }
    
    $db_analysis->end();    
?>

<script>
	function save_coin_increase_user() 
	{
		var coin_increase_user_form = document.coin_increase_user_form;

		var param = {};
		param.logidx = coin_increase_user_form.logidx.value;
		param.status = get_radio("status");
		param.comment = coin_increase_user_form.comment.value;

		WG_ajax_execute("monitoring/update_coin_increase_user", param, save_coin_increase_user_callback);
	}

	function save_coin_increase_user_callback(result, reason)
	{
		if(!result)
		{
			alert("오류 발생 - " + reason);
		}
		else
		{
			window.location.href = "javascript:history.back()";
		}
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 비결제 코인 급증 사용자(가입 1주내 미결제 5억이상) 설정 </div>
	</div>
	<!-- //title_warp -->
	
	<form name="coin_increase_user_form" id="coin_increase_user_form">
		<input type="hidden" name="logidx" id="logidx" value="<?= $logidx ?>">
		<table class="tbl_view_basic">
			<colgroup>
				<col width="150">
				<col width="">
			</colgroup>
			
			<tbody>
				<tr>
					<th>useridx</th>
					<td><?= $useridx ?></td>
				</tr>
				
				<tr>
					<th>보유 코인</th>
					<td><?= number_format($coin) ?></td>
				</tr>
				
				<tr>
					<th>가입일</th>
					<td><?= $createdate ?></td>
				</tr>
				
				<tr>
					<th>등록일</th>
					<td><?= $writedate ?></td>
				</tr>
				<tr>
					<th>상태</th>
					<td>
						<input type="radio" class="radio" name="status" value="0" <?= ($status == "0") ? "checked" : "" ?>> 미확인
						<input type="radio" class="radio" name="status" value="1" <?= ($status == "1") ? "checked" : "" ?>> 확인
						<input type="radio" class="radio" name="status" value="2" <?= ($status == "2" || $status == "3") ? "checked" : "" ?>> 보류 
					</td>
				</tr>
				
				<tr>
					<th>comment</th>
					<td><textarea id="comment" style="width:400px;height:100px;"><?= ($comment == "") ? "현재 코인 " : $comment ?></textarea></td>
				</tr>
			</tbody>
		</table>
	</form>
	
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="저장" onclick="save_coin_increase_user()">
		<input type="button" class="btn_setting_02" value="취소" onclick="history.back()">
	</div>
</div>

<!--  //CONTENTS WRAP -->

<div class="clear"></div>

<?
	include ($_SERVER ["DOCUMENT_ROOT"] . "/m_common/bottom_frame.inc.php");
?>