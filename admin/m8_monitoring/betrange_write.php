<?
	$top_menu = "monitoring";
	$sub_menu = "betrange";

	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

	$slotidx = ($_GET["slotidx"] == "") ? "" : $_GET["slotidx"];
	$roomtype = ($_GET["roomtype"] == "") ? "1" : $_GET["roomtype"];

	$pagename = "betrange_write.php";

	if ($slotidx == "" || ($roomtype != "1" && $roomtype != "2"))
		error_back("잘못된 접근입니다.");

	$db_main2 = new CDatabase_Main2();
	$db_game = new CDatabase_Game();

	$sql = "SELECT slotname FROM tbl_slot_list WHERE slottype = $slotidx";
	$slotname = $db_main2->getarray($sql);

	$morecolumns = "";
	$betcount = 10;
	$betlevelmultiple = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];
	if ($roomtype == "1") {
		$tablename = "tbl_normal_bet_info";
		$morecolumns = ", bet6, bet7, bet8, bet9, bet10";
	} else if ($roomtype == "2") {
		$tablename = "tbl_highroller_bet_info";
		$betcount = 5;
		$betlevelmultiple = [ 0, 1, 2, 6, 20, 40 ];
	}
	$sql = "SELECT slotidx, alignidx, linecount, bet1, bet2, bet3, bet4, bet5 $morecolumns ".
		"FROM $tablename ".
		"WHERE slotidx = $slotidx";
	$slot_info = $db_game->getarray($sql);

	$totalbet = $slot_info["bet1"] * $slot_info["linecount"];

	$db_main2->end();
	$db_game->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function update_linebet() {
	var $totalBet = $('.contents_wrap #totalbet');
	var _lineCount = <?= $slot_info["linecount"] ?>;
	var _totalBet = $totalBet.val();

	$('.contents_wrap td[data-multiple]').each(function() {
		var $eachTd = $(this);
		var _multiple = $eachTd.data('multiple');
		$eachTd.text(numberWithCommas(_totalBet / _lineCount * _multiple));
	});
}

function save_betrange()
{
	var input_form = document.input_form;

	if (input_form.totalbet.value == "" && input_form.totalbet.value < 9000) {
		alert("토탈벳 금액은 최소 9,000 입니다.");
		input_form.totalbet.focus();
		return;
	}

	var param = {};
	param.slotidx = "<?= $slotidx ?>";
	param.roomtype = "<?= $roomtype ?>";
	param.linecount = input_form.linecount.value;
	param.totalbet = input_form.totalbet.value;

	WG_ajax_execute("game/update_betrange", param, save_betrange_callback);
}

function save_betrange_callback(result, reason)
{
	if (!result) {
		alert("오류 발생 - " + reason);
	} else {
		window.location.href = "betrange.php?tab=<?= $roomtype ?>";
	}
}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; <?= $roomtype == "1" ? "레귤러" : "플래티넘" ?> 베팅레인지관리</div>
	</div>
	<!-- //title_warp -->
	<form name="input_form" id="input_form" onsubmit="return false">
		<div class="h2_title">[슬롯 정보 - <?= $slotname["slotname"] ?>]</div>
		<table class="tbl_view_basic">
			<colgroup>
				<col width="150">
				<col width="">
			</colgroup>
			<tbody>
				<tr>
					<th>슬롯 타입 (slotidx)</th>
					<td><input type="text" class="view_tbl_text" name="slotidx" id="slotidx" style="width:200px;" value="<?= $slot_info["slotidx"] ?>" readonly /></td>
				</tr>
				<tr>
					<th>슬롯 이름</th>
					<td><input type="text" class="view_tbl_text" name="slotname" id="slotname" style="width:200px;" value="<?= $slotname["slotname"] ?>" readonly /></td>
				</tr>
				<tr>
					<th>라인 수</th>
					<td><input type="text" class="view_tbl_text" name="linecount" id="linecount" style="width:200px;" value="<?= $slot_info["linecount"] ?>" readonly /></td>
				</tr>
				<tr>
					<th>토탈벳 (최소)</th>
					<td><input type="text" class="view_tbl_text" name="totalbet" id="totalbet" style="width:200px;background:white;" maxlength="20" value="<?= $totalbet ?>" onblur="update_linebet()" onkeypress="return checkonlynum()" /></td>
				</tr>
				<tr>
					<th>라인벳 <span title="토탈벳 변경 시 자동 계산됩니다.">ⓘ</span></th>
					<td>
						<table class="tbl_view_basic">
							<colgroup>
								<col width="100">
<? for ($i = 1; $i <= $betcount; $i++) { ?>
								<col width="60">
<? } ?>
							</colgroup>
							<thead>
								<tr>
									<th class="tdl">구분</th>
<? for ($i = 1; $i <= $betcount; $i++) { ?>
									<th class="tdc">bet<?= $i ?> (x<?= $betlevelmultiple[$i] ?>)</th>
<? } ?>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="tdl point">linebet</td>
<? for ($i = 1; $i <= $betcount; $i++) { ?>
									<td class="tdr" data-multiple="<?= $betlevelmultiple[$i] ?>"><?= make_price_format($slot_info["bet1"] * $betlevelmultiple[$i]) ?></td>
<? } ?>
								</tr>
								<tr>
									<td class="tdl point">totalbet</td>
<? for ($i = 1; $i <= $betcount; $i++) { ?>
									<td class="tdr" data-multiple="<?= $betlevelmultiple[$i] * $slot_info["linecount"] ?>"><?= make_price_format($slot_info["bet1"] * $betlevelmultiple[$i] * $slot_info["linecount"]) ?></td>
<? } ?>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</form>

	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="저장" onclick="save_betrange()">
		<input type="button" class="btn_setting_02" value="취소" onclick="go_page('betrange.php?tab=<?= $roomtype ?>')">
	</div>
</div>
<!--  //CONTENTS WRAP -->

<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
