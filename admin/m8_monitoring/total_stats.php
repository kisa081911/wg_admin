<?
    $top_menu = "monitoring";
    $sub_menu = "total_stats";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $term = "3";
    
    $isrefresh = ($_GET["isrefresh"] == "") ? "0" : $_GET["isrefresh"];
    
    // 최근 6시간 3분 term
    $end_date = date("Y-m-d", time());
    $end_time = date("H:i:s", time());
    
    $start_date = date("Y-m-d", mktime(date("H", strtotime($end_time))-6,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
    $start_time = date("H:i:s", mktime(date("H", strtotime($end_time))-6,date("i", strtotime($end_time)),date("s", strtotime($end_time)),date("m", strtotime($end_date)),date("d", strtotime($end_date)),date("Y", strtotime($end_date))));
    
    $user_where = " WHERE isnpc=0 ";
    $late_where = " WHERE latency > 0  AND errcode = 0";
    $error_where = " WHERE errcode != '0' ";
    $warningevent_where = " WHERE category=2 ";
    $errorevent_where = " WHERE category=3 ";
    
    $db_main2 = new CDatabase_Main2();
    $db_analysis = new CDatabase_Analysis();
    
    // 일반 유저    
    $sql = "SELECT SUM(webusercount) AS webusercount, SUM(iosusercount) AS iosusercount, SUM(androidusercount) AS androidusercount, SUM(amazonusercount) AS amazonusercount, SUM(group1) AS group1, SUM(group2) AS group2, SUM(group3) AS group3, minute ".
    		"FROM ( ".
    		"	SELECT SUM(totalcount)/COUNT(DISTINCT logkey) AS webusercount, 0 AS iosusercount, 0 AS androidusercount, 0 AS amazonusercount, SUM(group1)/COUNT(DISTINCT logkey) AS group1, SUM(group2)/COUNT(DISTINCT logkey) AS group2, SUM(group3)/COUNT(DISTINCT logkey) AS group3, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"	FROM user_online_log ".
    		"	$user_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"	GROUP BY minute ".
    		"	UNION ALL ".
    		"	SELECT 0 AS webusercount, SUM(totalcount)/COUNT(DISTINCT logkey) AS iosusercount, 0 AS androidusercount, 0 AS amazonusercount, SUM(group1)/COUNT(DISTINCT logkey) AS group1, SUM(group2)/COUNT(DISTINCT logkey) AS group2, SUM(group3)/COUNT(DISTINCT logkey) AS group3, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"	FROM user_online_mobile_log ".
    		"	$user_where AND os_type=1 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		" 	GROUP BY minute ".
    		"	UNION ALL ".
    		"	SELECT 0 AS webusercount, 0 AS iosusercount, SUM(totalcount)/COUNT(DISTINCT logkey) AS androidusercount, 0 AS amazonusercount, SUM(group1)/COUNT(DISTINCT logkey) AS group1, SUM(group2)/COUNT(DISTINCT logkey) AS group2, SUM(group3)/COUNT(DISTINCT logkey) AS group3, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"	FROM user_online_mobile_log ".
    		"	$user_where AND os_type=2 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		" 	GROUP BY minute ".
    		"	UNION ALL ".
    		"	SELECT 0 AS webusercount, 0 AS iosusercount, 0 AS androidusercount, SUM(totalcount)/COUNT(DISTINCT logkey) AS amazonusercount, SUM(group1)/COUNT(DISTINCT logkey) AS group1, SUM(group2)/COUNT(DISTINCT logkey) AS group2, SUM(group3)/COUNT(DISTINCT logkey) AS group3, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"	FROM user_online_mobile_log ".
    		"	$user_where AND os_type=3 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		" 	GROUP BY minute ".
    		") t1 ".
    		"GROUP BY minute ";    
    $userlist = $db_analysis->gettotallist($sql);
    
    // web일반 유저
    $sql = "SELECT SUM(webusercount_v1) AS webusercount_v1, SUM(webusercount_v2) AS webusercount_v2, minute ".
    		"FROM ( ".
    		"	SELECT SUM(totalcount)/COUNT(DISTINCT logkey) AS webusercount_v1, 0 AS webusercount_v2, SUM(group1)/COUNT(DISTINCT logkey) AS group1, SUM(group2)/COUNT(DISTINCT logkey) AS group2, SUM(group3)/COUNT(DISTINCT logkey) AS group3, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"	FROM user_online_log ".
    		"	$user_where AND is_v2 = 0 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"	GROUP BY minute ".
    		"	UNION ALL ".
    		"	SELECT 0 AS webusercount, SUM(totalcount)/COUNT(DISTINCT logkey) AS webusercount_v2, SUM(group1)/COUNT(DISTINCT logkey) AS group1, SUM(group2)/COUNT(DISTINCT logkey) AS group2, SUM(group3)/COUNT(DISTINCT logkey) AS group3, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"	FROM user_online_log ".
    		"	$user_where AND is_v2 = 1 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"	GROUP BY minute ".
    		") t1 ".
    		"GROUP BY minute ";
    $web_userlist = $db_analysis->gettotallist($sql);
    
    // 클라이언트 통신 로그
    // 늦은 응답 건수
    $sql = "SELECT COUNT(*) AS latecount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM client_communication_log $late_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $latelist = $db_analysis->gettotallist($sql);
    
    // 에러 건수
    $sql = "SELECT COUNT(*) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM client_communication_log $error_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
     
    $errorlist = $db_analysis->gettotallist($sql);
    
    //클라이언트 통신 로그(Mobile)
    $sql = "SELECT COUNT(*) AS latecount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "FROM (
                SELECT * FROM client_communication_log_ios
                UNION ALL
                SELECT * FROM client_communication_log_android
                UNION ALL
                SELECT * FROM client_communication_log_amazon
               ) t1 ".
        "$late_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        "GROUP BY minute ";
    
    $mobile_latelist = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "FROM (
                SELECT * FROM client_communication_log_ios
                UNION ALL
                SELECT * FROM client_communication_log_android
                UNION ALL
                SELECT * FROM client_communication_log_amazon
               ) t1 ".
        "$error_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        "GROUP BY minute ";
    
    $mobile_errorlist = $db_analysis->gettotallist($sql);
    
    // 서버 통신 로그 v_1.0
    
    // 늦은 응답 건수
    $sql = "SELECT COUNT(*) AS latecount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_communication_log $late_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $server_latelist = $db_analysis->gettotallist($sql);
    
    // 에러 건수
    $sql = "SELECT COUNT(*) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_communication_log $error_where AND errcode!=44 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
     
    $server_errorlist = $db_analysis->gettotallist($sql);
    
    // 에러 건수 (44번 응답)
    $sql = "SELECT COUNT(*) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_communication_log $error_where AND errcode=44 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
     
    $server_error44list = $db_analysis->gettotallist($sql);
    
    // 서버 통신 로그 v_2.0
    
    // 늦은 응답 건수
    $sql = "SELECT ostype, COUNT(*) AS latecount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_communication_log_v2 $late_where AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute, ostype ";
    
    $server_v2_latelist = $db_analysis->gettotallist($sql);
    
    // 에러 건수
    $sql = "SELECT ostype, COUNT(*) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_communication_log_v2 $error_where AND errcode!=44 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute, ostype ";
     
    $server_v2_errorlist = $db_analysis->gettotallist($sql);
    
    // 에러 건수 (44번 응답)
    $sql = "SELECT ostype, COUNT(*) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_communication_log_v2 $error_where AND errcode=44 AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute, ostype ";
     
    $server_v2_error44list = $db_analysis->gettotallist($sql);
    
    // 서버 이벤트 로그 V_1.0
    
    // 서버 이벤트 warning로그
    $sql = "SELECT COUNT(*) AS warningcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_log $warningevent_where $tail AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $warningevent_list = $db_analysis->gettotallist($sql);
     
    // 서버 이벤트 error로그
    $sql = "SELECT COUNT(*) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_log $errorevent_where $tail AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute ";
    
    $errorevent_list = $db_analysis->gettotallist($sql);

    // 서버 이벤트 로그 V_2.0
    
    // 서버 이벤트 warning로그
    $sql = "SELECT ostype, COUNT(*) AS warningcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_log_v2 $warningevent_where $tail AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute, ostype ";
    
    $warningevent_v2_list = $db_analysis->gettotallist($sql);
     
    // 서버 이벤트 error로그
    $sql = "SELECT ostype, COUNT(*) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
    		"FROM server_log_v2 $errorevent_where $tail AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
    		"GROUP BY minute, ostype";
    
    $errorevent_v2_list = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT servername,serverip,cpu,diskuse,memtotal,memfree,vmem,established,timewait,closewait,sync_recv,UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(writedate) AS seconds FROM tbl_server_status ORDER BY servername";
    $server_list = $db_main2->gettotallist($sql);
    
    $sql = "SELECT MAX(cpu) as maxcpu, serverip ".
    		"FROM tbl_server_status_log ".
    		"WHERE '". $start_date ." 00:00:00' <= writedate AND writedate <= '". $end_date ." 23:59:59' ".
    		"GROUP BY serverip";
    $maxcpu_list = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT SUM(IF(os_type = 0,cnt,0)) AS webusercount
            	,  SUM(IF(os_type = 1,cnt,0)) AS iosusercount
            	,  SUM(IF(os_type = 2,cnt,0))  AS androidusercount
            	,  SUM(IF(os_type = 3,cnt,0))  AS amazonusercount
            	, FLOOR(UNIX_TIMESTAMP(writedate)/(5*60)) AS MINUTE 
            FROM tbl_action_log_stat  WHERE  writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' GROUP BY MINUTE";
    $actionlist = $db_analysis->gettotallist($sql);
    
    // 모바일 서버 이벤트 로그
    $sql = "SELECT COUNT(*) AS warningcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "FROM server_log_mobile ".
        "$warningevent_where $tail AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        "GROUP BY minute ";
    $mobile_warningevent_list = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT COUNT(*) AS errorcount, FLOOR(UNIX_TIMESTAMP(writedate)/($term*60)) AS minute ".
        "FROM server_log_mobile ".
        "$errorevent_where $tail AND writedate BETWEEN '$start_date $start_time' AND '$end_date $end_time' ".
        "GROUP BY minute ";
    
    $mobile_errorevent_list = $db_analysis->gettotallist($sql);
    
    $db_main2->end();
    $db_analysis->end();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});

	function drawChart() 
    {
        var datatable1 = new google.visualization.DataTable();
        
        datatable1.addColumn('string', '시간');
        datatable1.addColumn('number', '전체');
        datatable1.addColumn('number', 'web');
        datatable1.addColumn('number', 'ios');
        datatable1.addColumn('number', 'android');
        datatable1.addColumn('number', 'amazon');
        datatable1.addColumn('number', 'VIP 1~3');
        datatable1.addColumn('number', 'VIP 4~6');
        datatable1.addColumn('number', 'VIP 7이상');
        datatable1.addRows([
<?
    $start = round(mktime(date("H",strtotime($start_time)), date("i",strtotime($start_time)), date("s", strtotime($start_time)), date("m",strtotime($start_date)), date("d", strtotime($start_date)), date("Y", strtotime($start_date)))/($term*60));
    $end = round(mktime(date("H",strtotime($end_time)), date("i",strtotime($end_time)), date("s", strtotime($end_time)), date("m",strtotime($end_date)), date("d", strtotime($end_date)), date("Y", strtotime($end_date)))/($term*60)) + 1;
    
    for ($i=$start; $i<$end; $i=$i+1)
    {
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        if ($term > 10080)
            echo("['".substr($list_date, 0, 7)."'");  
        else if ($term > 720)
            echo("['$list_date'");
        else
            echo("['".make_time_format($temp_minute)."'");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
            if ($userlist[$j]["minute"] == $i)
            {
                echo(",{v:".round($userlist[$j]["webusercount"] + $userlist[$j]["iosusercount"] + $userlist[$j]["androidusercount"] + $userlist[$j]["amazonusercount"]).",f:'".make_price_format(round($userlist[$j]["webusercount"] + $userlist[$j]["iosusercount"] + $userlist[$j]["androidusercount"] + $userlist[$j]["amazonusercount"]))."'}");
                $print = true;      
                break;
            }
        }

        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
	        if ($userlist[$j]["minute"] == $i)
	        {
	        	echo(",{v:".round($userlist[$j]["webusercount"]).",f:'".number_format(round($userlist[$j]["webusercount"]))."'}");
	        	$print = true;
	        	break;
	        }
        }
        
        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
	        if ($userlist[$j]["minute"] == $i)
	        {
	        	echo(",{v:".round($userlist[$j]["iosusercount"]).",f:'".number_format(round($userlist[$j]["iosusercount"]))."'}");
        		$print = true;
        		break;
	        }
        }
        
        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
        	if ($userlist[$j]["minute"] == $i)
        	{
        		echo(",{v:".round($userlist[$j]["androidusercount"]).",f:'".number_format(round($userlist[$j]["androidusercount"]))."'}");
        		$print = true;
        		break;
        	}
        }
        
        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
        	if ($userlist[$j]["minute"] == $i)
        	{
        		echo(",{v:".round($userlist[$j]["amazonusercount"]).",f:'".number_format(round($userlist[$j]["amazonusercount"]))."'}");
        		$print = true;
        		break;
        	}
        }        
        
        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
        	if ($userlist[$j]["minute"] == $i)
        	{
        		echo(",{v:".round($userlist[$j]["group1"]).",f:'".make_price_format(round($userlist[$j]["group1"]))."'}");
        		$print = true;
        		break;
        	}
        }
        
        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
        	if ($userlist[$j]["minute"] == $i)
        	{
        		echo(",{v:".round($userlist[$j]["group2"]).",f:'".make_price_format(round($userlist[$j]["group2"]))."'}");
        		$print = true;
        		break;
        	}
        }
        
        if (!$print)
        	echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($userlist); $j++)
        {
        	if ($userlist[$j]["minute"] == $i)
        	{
        		echo(",{v:".round($userlist[$j]["group3"]).",f:'".make_price_format(round($userlist[$j]["group3"]))."'}");
        		$print = true;
        		break;
        	}
        }
        
        if (!$print)
           echo(",0");
        
		           
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");                    
    }    
?>          
        ]);

        var datatable1_web = new google.visualization.DataTable();
            
            datatable1_web.addColumn('string', '시간');
            datatable1_web.addColumn('number', '전체');
            datatable1_web.addColumn('number', 'v1');
            datatable1_web.addColumn('number', 'v2');
            datatable1_web.addRows([
    <?
        $start = round(mktime(date("H",strtotime($start_time)), date("i",strtotime($start_time)), date("s", strtotime($start_time)), date("m",strtotime($start_date)), date("d", strtotime($start_date)), date("Y", strtotime($start_date)))/($term*60));
        $end = round(mktime(date("H",strtotime($end_time)), date("i",strtotime($end_time)), date("s", strtotime($end_time)), date("m",strtotime($end_date)), date("d", strtotime($end_date)), date("Y", strtotime($end_date)))/($term*60)) + 1;
        
        for ($i=$start; $i<$end; $i=$i+1)
        {
            $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
            $temp_minute = $temp_time[0]*60 + $temp_time[1];
          
            $list_date = date("Y-m-d", $i*($term*60));
            
            if ($term > 10080)
                echo("['".substr($list_date, 0, 7)."'");  
            else if ($term > 720)
                echo("['$list_date'");
            else
                echo("['".make_time_format($temp_minute)."'");
            
            $print = false;
            for ($j=0; $j<sizeof($web_userlist); $j++)
            {
                if ($web_userlist[$j]["minute"] == $i)
                {
                    echo(",{v:".round($web_userlist[$j]["webusercount_v1"] + $web_userlist[$j]["webusercount_v2"]).",f:'".make_price_format(round($web_userlist[$j]["webusercount_v1"] + $web_userlist[$j]["webusercount_v2"]))."'}");
                    $print = true;      
                    break;
                }
            }

            if (!$print)
            	echo(",0");
            
            $print = false;
            for ($j=0; $j<sizeof($web_userlist); $j++)
            {
    	        if ($web_userlist[$j]["minute"] == $i)
    	        {
    	        	echo(",{v:".round($web_userlist[$j]["webusercount_v1"]).",f:'".number_format(round($web_userlist[$j]["webusercount_v1"]))."'}");
    	        	$print = true;
    	        	break;
    	        }
            }
            
            if (!$print)
            	echo(",0");
            
            $print = false;
            for ($j=0; $j<sizeof($web_userlist); $j++)
            {
    	        if ($web_userlist[$j]["minute"] == $i)
    	        {
    	        	echo(",{v:".round($web_userlist[$j]["webusercount_v2"]).",f:'".number_format(round($web_userlist[$j]["webusercount_v2"]))."'}");
            		$print = true;
            		break;
    	        }
            }
            
           if (!$print)
            	echo(",0");                       
    		           
            if ($i+1 >= $end)
                echo("]");  
            else
                echo("],");                    
        }    
    ?>          
        ]);
        
        var datatable2 = new google.visualization.DataTable();
        
        datatable2.addColumn('string', '시간');
        datatable2.addColumn('number', '늦은응답건수(Web)');
        datatable2.addColumn('number', '에러건수(Web)');
        datatable2.addColumn('number', '늦은응답건수(Mobile)');
        datatable2.addColumn('number', '에러건수(Mobile)');
        datatable2.addRows([
<?
    for ($i=$start; $i<$end; $i=$i+1)
    {      
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        if ($term > 10080)
            echo("['".substr($list_date, 0, 7)."'");  
        else if ($term > 720)
            echo("['$list_date'");        
        else
            echo("['".make_time_format($temp_minute)."'");
        
        $print = false;
        for ($j=0; $j<sizeof($latelist); $j++)
        {
            if ($latelist[$j]["minute"] == $i)
            {
                echo(",{v:".$latelist[$j]["latecount"].",f:'".make_price_format($latelist[$j]["latecount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($errorlist); $j++)
        {
            if ($errorlist[$j]["minute"] == $i)
            {
                echo(",{v:".$errorlist[$j]["errorcount"].",f:'".make_price_format($errorlist[$j]["errorcount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($mobile_latelist); $j++)
        {
            if ($mobile_latelist[$j]["minute"] == $i)
            {
                echo(",{v:".$mobile_latelist[$j]["latecount"].",f:'".make_price_format($mobile_latelist[$j]["latecount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($mobile_errorlist); $j++)
        {
            if ($errorlist[$j]["minute"] == $i)
            {
                echo(",{v:".$mobile_errorlist[$j]["errorcount"].",f:'".make_price_format($mobile_errorlist[$j]["errorcount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
        
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");               
    }   
?>          
        ]);
        
        var datatable3 = new google.visualization.DataTable();
        
        datatable3.addColumn('string', '시간');
        datatable3.addColumn('number', '늦은응답건수');
        datatable3.addColumn('number', '에러건수(44번 응답 제외)');
        datatable3.addColumn('number', '에러건수(44번 응답)');
        datatable3.addRows([
<?
    for ($i=$start; $i<$end; $i=$i+1)
    {      
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        if ($term > 10080)
            echo("['".substr($list_date, 0, 7)."'");  
        else if ($term > 720)
            echo("['$list_date'");        
        else
            echo("['".make_time_format($temp_minute)."'");
        
        $print = false;
        for ($j=0; $j<sizeof($server_latelist); $j++)
        {
            if ($server_latelist[$j]["minute"] == $i)
            {
                echo(",{v:".$server_latelist[$j]["latecount"].",f:'".make_price_format($server_latelist[$j]["latecount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
           
        $print = false;
        for ($j=0; $j<sizeof($server_errorlist); $j++)
        {
            if ($server_errorlist[$j]["minute"] == $i)
            {
                echo(",{v:".$server_errorlist[$j]["errorcount"].",f:'".make_price_format($server_errorlist[$j]["errorcount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
           
        $print = false;
        for ($j=0; $j<sizeof($server_error44list); $j++)
        {
            if ($server_error44list[$j]["minute"] == $i)
            {
                echo(",{v:".$server_error44list[$j]["errorcount"].",f:'".make_price_format($server_error44list[$j]["errorcount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
        
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");                
    }   
?>          
        ]);

        var datatable3_v2 = new google.visualization.DataTable();
        
        datatable3_v2.addColumn('string', '시간');
        datatable3_v2.addColumn('number', '늦은응답건수');
        datatable3_v2.addColumn('number', '에러건수(44번 응답 제외)');
        datatable3_v2.addColumn('number', '에러건수(44번 응답)');
        datatable3_v2.addRows([
<?
    for ($i=$start; $i<$end; $i=$i+1)
    {      
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        if ($term > 10080)
            echo("['".substr($list_date, 0, 7)."'");  
        else if ($term > 720)
            echo("['$list_date'");        
        else
            echo("['".make_time_format($temp_minute)."'");
        
        $print = false;
        for ($j=0; $j<sizeof($server_v2_latelist); $j++)
        {
            if ($server_latelist[$j]["minute"] == $i)
            {
                echo(",{v:".$server_v2_latelist[$j]["latecount"].",f:'".make_price_format($server_v2_latelist[$j]["latecount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
           
        $print = false;
        for ($j=0; $j<sizeof($server_v2_errorlist); $j++)
        {
            if ($server_v2_errorlist[$j]["minute"] == $i)
            {
                echo(",{v:".$server_v2_errorlist[$j]["errorcount"].",f:'".make_price_format($server_v2_errorlist[$j]["errorcount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
           
        $print = false;
        for ($j=0; $j<sizeof($server_v2_error44list); $j++)
        {
            if ($server_v2_error44list[$j]["minute"] == $i)
            {
                echo(",{v:".$server_v2_error44list[$j]["errorcount"].",f:'".make_price_format($server_v2_error44list[$j]["errorcount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
        
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");                
    }   
?>          
        ]);
        
        var datatable4 = new google.visualization.DataTable();
        
        datatable4.addColumn('string', '시간');
        datatable4.addColumn('number', 'Waring 건수(Web)');
        datatable4.addColumn('number', 'Error 건수(Web)');
        datatable4.addColumn('number', 'Waring 건수(Mobile)');
        datatable4.addColumn('number', 'Error 건수(Mobile)');
        datatable4.addRows([
<?
    for ($i=$start; $i<$end; $i=$i+1)
    {      
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        if ($term > 10080)
            echo("['".substr($list_date, 0, 7)."'");  
        else if ($term > 720)
            echo("['$list_date'");        
        else
            echo("['".make_time_format($temp_minute)."'");
        
        $print = false;
        for ($j=0; $j<sizeof($warningevent_list); $j++)
        {
            if ($warningevent_list[$j]["minute"] == $i)
            {
                echo(",{v:".$warningevent_list[$j]["warningcount"].",f:'".make_price_format($warningevent_list[$j]["warningcount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($errorevent_list); $j++)
        {
            if ($errorevent_list[$j]["minute"] == $i)
            {
                echo(",{v:".$errorevent_list[$j]["errorcount"].",f:'".make_price_format($errorevent_list[$j]["errorcount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($mobile_warningevent_list); $j++)
        {
            if ($mobile_warningevent_list[$j]["minute"] == $i)
            {
                echo(",{v:".$mobile_warningevent_list[$j]["warningcount"].",f:'".make_price_format($mobile_warningevent_list[$j]["warningcount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($mobile_errorevent_list); $j++)
        {
            if ($errorevent_list[$j]["minute"] == $i)
            {
                echo(",{v:".$mobile_errorevent_list[$j]["errorcount"].",f:'".make_price_format($mobile_errorevent_list[$j]["errorcount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
        
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");         
    }     
?>          
        ]);

       var datatable4_v2 = new google.visualization.DataTable();
        
       datatable4_v2.addColumn('string', '시간');
       datatable4_v2.addColumn('number', 'Waring 건수');
       datatable4_v2.addColumn('number', 'Error 건수');
       datatable4_v2.addRows([
<?
    for ($i=$start; $i<$end; $i=$i+1)
    {      
        $temp_time = explode(":",date("H:i:s", $i*($term*60)));        
        $temp_minute = $temp_time[0]*60 + $temp_time[1];
      
        $list_date = date("Y-m-d", $i*($term*60));
        
        if ($term > 10080)
            echo("['".substr($list_date, 0, 7)."'");  
        else if ($term > 720)
            echo("['$list_date'");        
        else
            echo("['".make_time_format($temp_minute)."'");
        
        $print = false;
        for ($j=0; $j<sizeof($warningevent_v2_list); $j++)
        {
            if ($warningevent_v2_list[$j]["minute"] == $i)
            {
                echo(",{v:".$warningevent_v2_list[$j]["warningcount"].",f:'".make_price_format($warningevent_v2_list[$j]["warningcount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
        
        $print = false;
        for ($j=0; $j<sizeof($errorevent_v2_list); $j++)
        {
            if ($errorevent_v2_list[$j]["minute"] == $i)
            {
                echo(",{v:".$errorevent_v2_list[$j]["errorcount"].",f:'".make_price_format($errorevent_v2_list[$j]["errorcount"])."'}");
                $print = true;      
                break;
            }
        }
        
        if (!$print)
           echo(",0");
        
        if ($i+1 >= $end)
            echo("]");  
        else
            echo("],");         
    }     
?>          
        ]);


        
        var options1 = {          
            axisTitlesPosition:'in',  
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize:12,
            colors: ['#0000CD', '#008000', '#D2691E', '#C0C0C0', '#FFD700'],
            chartArea:{left:85,top:30,width:435,height:200}
        };
    
        var options2 = {          
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize:12,
            chartArea:{left:85,top:30,width:435,height:200}
        };
    
        var options3 = {      
            axisTitlesPosition:'in',
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize:12,
            chartArea:{left:85,top:30,width:435,height:200}
        };
    
        var options4 = {       
            axisTitlesPosition:'in',  
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize:12,
            chartArea:{left:85,top:30,width:435,height:200}
        };
	
	 var options5 = {          
            axisTitlesPosition:'in',  
            curveType:'none',
            focusTarget:'category',
            interpolateNulls:'true',
            legend:'top',
            fontSize:12,
            chartArea:{left:85,top:30,width:435,height:200}
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
        chart.draw(datatable1, options1);

        chart = new google.visualization.LineChart(document.getElementById('chart_div1_web'));
        chart.draw(datatable1_web, options1);        
        
        chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
        chart.draw(datatable2, options2);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_div3'));
        chart.draw(datatable3, options3);

        chart = new google.visualization.LineChart(document.getElementById('chart_div3_v2'));
        chart.draw(datatable3_v2, options3);
        
        chart = new google.visualization.LineChart(document.getElementById('chart_div4'));
        chart.draw(datatable4, options4);

        chart = new google.visualization.LineChart(document.getElementById('chart_div4_v2'));
        chart.draw(datatable4_v2, options4);
	
        chart = new google.visualization.LineChart(document.getElementById('chart_div5'));
        chart.draw(datatable5, options5);
    }
  
    google.setOnLoadCallback(drawChart);
	
	function fnPageRefresh()
	{
		if(document.getElementById("isrefresh").checked)
		{
			fnReload();
		}
		else
		{
			setTimeout("fnPageRefresh()",180000);
		}
	}

	function fnReload()
	{
		window.location.href="http://" + window.location.host + window.location.pathname + '?isrefresh=1';
	}

	window.onload = function()
	{
		setTimeout("fnPageRefresh()",180000);
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 모니터링 총괄 </div><div style="float:right;"><div style="float:right; margin-left:10px;"><input type="button" class="btn_search" onclick="window.open('https://developers.facebook.com/status')" value="페이스북 플랫폼 현황"/></div><div style="float:right; margin-top:3px;"><input type="checkbox" name="isrefresh" id="isrefresh" style="vertical-align:middle;" <?= ($isrefresh == "1") ? "checked" : "" ?> /> 3분마다 새로고침</div></div>
	</div>
	<!-- //title_warp -->
	
	<div style="min-width:490px;float:left;margin-left:40px;margin-right:20px;"><a href="online_log_stats.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">현재 사용자 접속 상태</a></div>
	<div style="min-width:490px;float:left;margin-left:40px;"><a href="online_log_stats.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">현재 사용자 접속 상태(Web)</a></div>	
	<div id="chart_div1" style="height:300px; min-width:530px;float:left;margin-right:20px;"></div>
	<div id="chart_div1_web" style="height:300px; min-width:530px;float:left;"></div>
	
	
	<div style="min-width:490px;float:left;margin-left:40px;margin-right:20px;"><a href="client_log_stats.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">클라이언트 통신 로그</a></div>	
	<div id="chart_div2" style="height:300px; min-width:530px;float:left;margin-right:140px;"></div>
	
	<div style="min-width:490px;float:left;margin-left:40px;margin-right:20px;"><a href="server_log_stats.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">서버 통신 로그_v1</a></div>
	<div style="min-width:490px;float:left;margin-left:40px;"><a href="serverevent_log_stats.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">서버 이벤트 로그_v1</a></div>
	<div id="chart_div3" style="height:300px; min-width:530px;float:left;margin-right:20px;"></div>
	<div id="chart_div4" style="height:300px; min-width:530px;float:left;"></div>
	
	<div style="min-width:490px;float:left;margin-left:40px;margin-right:20px;"><a href="server_log_stats_v2.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">서버 통신 로그_v2</a></div>
	<div style="min-width:490px;float:left;margin-left:40px;"><a href="serverevent_log_stats_v2.php" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">서버 이벤트 로그_v2</a></div>
	<div id="chart_div3_v2" style="height:300px; min-width:530px;float:left;margin-right:20px;"></div>
	<div id="chart_div4_v2" style="height:300px; min-width:530px;float:left;"></div>
            
	<div style="min-width:490px;float:left;font:12px;color:#000;font-weight:bold;cursor:ponter;">서버 상태 정보</div>
	<div>
		<table class="tbl_list_basic1">
			<colgroup>
				<col width="">   
				<col width="">  
				<col width="">  
				<col width="">  
				<col width="">  
				<col width=""> 
				<col width="">   
				<col width="">  
				<col width="">  
				<col width="">   
				<col width="">            
				<col width="">            
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">IP(서버명)</th>
					<th class="tdc">CPU</th>
					<th class="tdc">디스크</th>
					<th class="tdc">memtotal</th>
					<th class="tdc">memfree</th>
					<th class="tdc">메모리가용량</th>
					<th class="tdc">vmem</th>
					<th class="tdc">Connection</th>
					<th class="tdc">TIME_WAIT</th>
					<th class="tdc">CLOSE_WAIT</th>
					<th class="tdc">SYNC_RECV</th>
					<th class="tdc">UpdateDate</th>
				</tr>
			</thead>
			<tbody>
<?
    for ($i=0; $i<sizeof($server_list); $i++)
    {
        $servername = $server_list[$i]["servername"];
        $serverip = $server_list[$i]["serverip"];
        $cpu = $server_list[$i]["cpu"];
        
        $maxcpu = "";
        
        for($j=0; $j<sizeof($maxcpu_list); $j++)
        {
        	if($serverip == $maxcpu_list[$j]["serverip"])
        		$maxcpu = $maxcpu_list[$j]["maxcpu"];
		}
		
		
        $diskuse = $server_list[$i]["diskuse"];
        $memtotal = $server_list[$i]["memtotal"];
        $memfree = $server_list[$i]["memfree"];
        $vmem = $server_list[$i]["vmem"];
        $established = $server_list[$i]["established"];
        $timewait = $server_list[$i]["timewait"];
        $closewait = $server_list[$i]["closewait"];
        $sync_recv = $server_list[$i]["sync_recv"];
        $mem_rate = round($memfree / $memtotal * 10000) / 100;
        $minutes = round($server_list[$i]["seconds"]/60);
        
        if ($diskuse >= "90" || $mem_rate < "10" || $cpu > "90" || $minutes > 5)
            $style = "style='color:#fb7878;font-weight:bold;'";
        else
            $style = "";
?>
				<tr>
					<td class="tdc" <?= $style ?>><?= $serverip."($servername)" ?></td>
					<td class="tdc" <?= $style ?>><?= $cpu ?>%(<?= ($maxcpu=="")? "-" : $maxcpu."%" ?>)</td>
					<td class="tdc" <?= $style ?>><?= $diskuse ?>%</td>
					<td class="tdc" <?= $style ?>><?= make_price_format($memtotal) ?>MB</td>
					<td class="tdc" <?= $style ?>><?= make_price_format($memfree) ?>MB</td>
					<td class="tdc" <?= $style ?>><?= $mem_rate ?>%</td>
					<td class="tdc" <?= $style ?>><?= make_price_format($vmem) ?>MB</td>
					<td class="tdc" <?= $style ?>><?= make_price_format($established) ?></td>
					<td class="tdc" <?= $style ?>><?= make_price_format($timewait) ?></td>
					<td class="tdc" <?= $style ?>><?= make_price_format($closewait) ?></td>
					<td class="tdc" <?= $style ?>><?= make_price_format($sync_recv) ?></td>
					<td class="tdc" <?= $style ?>><?= $minutes ?>분전</td>
				</tr>
<?
    }
?>
			</tbody>
		</table>
	</div>
</div>
<!--  //CONTENTS WRAP -->

<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>