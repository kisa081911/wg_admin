<?
    $top_menu = "monitoring";
	$sub_menu = "slot_gain_high_coin_user_list";
	
	include ($_SERVER ["DOCUMENT_ROOT"] . "/m_common/top_frame.inc.php");
	
	$today = $_GET["today"];
	$useridx = $_GET["useridx"];
	$slottype = $_GET["slottype"];
	
	check_number($useridx);
	
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	
	if($today != "" && $useridx != "")
	{
		$sql = "SELECT useridx FROM tbl_slot_gain_high_coin_list WHERE useridx = $useridx AND today = '$today' AND slottype = $slottype";
		$useridx = $db_analysis->getvalue($sql);
		
		if($useridx == "")
			error_back("존재하지 않는 데이터입니다.");
		
		$sql = "SELECT * FROM tbl_slot_gain_high_coin_list WHERE useridx = $useridx AND today = '$today' AND slottype = $slottype";
		$user_list = $db_analysis->getarray($sql);
				
		$today = $user_list["today"];
    	$useridx = $user_list["useridx"];
    	$slottype = $user_list["slottype"];
    	$sum_moneyin = $user_list["sum_moneyin"];
    	$sum_moneyout = $user_list["sum_moneyout"];
    	$sub_money = $user_list["sub_money"];
    	$winrate = $user_list["winrate"] * 100;
    	$user_flag = $user_list["user_flag"];
    	$comment = $user_list["comment"];
    	
    	$sql = "SELECT slotname FROM tbl_slot_list WHERE slottype = $slottype";
    	$slot_name = $db_main2->getvalue($sql);
	}
	
	$db_main2->end();
	$db_analysis->end();	
?>

<script>
	function save_gain_high_coin_user() 
	{
		var gain_high_coin_user_form = document.gain_high_coin_user_form;

		var param = {};
		param.today = gain_high_coin_user_form.today.value;
		param.useridx = gain_high_coin_user_form.useridx.value;
		param.user_flag = get_radio("status");
		param.comment = gain_high_coin_user_form.comment.value;
		param.slottype = gain_high_coin_user_form.slottype.value;  

		WG_ajax_execute("monitoring/update_gain_high_coin_user", param, save_gain_high_coin_user_callback);
	}
	
	function save_gain_high_coin_user_callback(result, reason)
	{
		if(!result)
		{
			alert("오류 발생 - " + reason);
		}
		else
		{
			window.location.href = "javascript:history.back()";
		}
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 일간 슬롯 별 최대 코인 획득 유저 설정 </div>
	</div>
	<!-- //title_warp -->
	
	<form name="gain_high_coin_user_form" id="gain_high_coin_user_form">
		<input type="hidden" name="today" id="today" value="<?= $today ?>">
		<input type="hidden" name="useridx" id="useridx" value="<?= $useridx ?>">
		<input type="hidden" name="slottype" id="slottype" value="<?= $slottype ?>">
		<table class="tbl_view_basic">
			<colgroup>
				<col width="150">
				<col width="">
			</colgroup>
			
			<tbody>
				<tr>
					<th>날짜</th>
					<td><?= $today ?></td>
				</tr>
				
				<tr>
					<th>useridx</th>
					<td><?= $useridx ?></td>
				</tr>
				
				<tr>
					<th>슬롯</th>
					<td><?= $slot_name ?></td>
				</tr>
				
				<tr>
					<th>총 머니인</th>
					<td><?= number_format($sum_moneyin) ?></td>
				</tr>
				
				<tr>
					<th>총 머니아웃</th>
					<td><?= number_format($sum_moneyout) ?></td>
				</tr>
				
				<tr>
					<th>총 획득 코인</th>
					<td><?= number_format($sub_money) ?></td>
				</tr>
				
				<tr>
					<th>winrate</th>
					<td><?= $winrate ?>%</td>
				</tr>
				
				<tr>
					<th>상태</th>
					<td>
						<input type="radio" class="radio" name="status" value="1" <?= ($user_flag == "1") ? "checked" : "" ?>> 미확인
						<input type="radio" class="radio" name="status" value="2" <?= ($user_flag == "2") ? "checked" : "" ?>> 보류 
						<input type="radio" class="radio" name="status" value="3" <?= ($user_flag == "3") ? "checked" : "" ?>> 확인
					</td>
				</tr>
				
				<tr>
					<th>comment</th>
					<td><textarea id="comment" style="width:400px;height:100px;"><?= ($comment == "") ? "Default" : encode_html_attribute($comment) ?></textarea></td>
				</tr>
			</tbody>
		</table>
	</form>
	
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="저장" onclick="save_gain_high_coin_user()">
		<input type="button" class="btn_setting_02" value="취소" onclick="history.back()">
	</div>
</div>

<!--  //CONTENTS WRAP -->

<div class="clear"></div>

<?
	include ($_SERVER ["DOCUMENT_ROOT"] . "/m_common/bottom_frame.inc.php");
?>