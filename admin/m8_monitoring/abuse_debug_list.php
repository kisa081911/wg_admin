<?
	$platform = ($_GET["platform"] == "") ? "0" : $_GET["platform"];
	
    $top_menu = "monitoring";
    $sub_menu = ($platform == 0) ? "abuse_debug_list_web" : "abuse_debug_list_mobile";
    
   	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
   	
   	$page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_useridx = $_GET["search_useridx"];
    $search_user_type = ($_GET["type"] == "") ? "" : $_GET["type"];
    $search_order_by = ($_GET["order_by"] == "") ? "1" : $_GET["order_by"];
    
    $listcount = "10";
    $pagename = "abuse_debug_list.php";    
    $pagefield = "useridx=$useridx&type=$search_user_type&order_by=$search_order_by";
    
    $db_main2 = new CDatabase_Main2();
    $db_main = new CDatabase_Main();
    
    $tail = "WHERE 1=1 ";

    if($search_user_type != "")
    	$tail .= " AND user_type = $search_user_type ";
    			
    if($search_useridx != "")
    	$tail .= " AND useridx = $search_useridx ";
    
    $order_by_type = "";
    
    if($search_order_by == 1)
    	$order_by_type = " ORDER BY useridx DESC ";
    else if($search_order_by == 2)
    	$order_by_type = " ORDER BY writedate DESC ";
    
    
    $sql = "SELECT * FROM tbl_user_debug $tail $order_by_type LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    if($platform == 0)
    {
	    $user_debug_list = $db_main2->gettotallist($sql);
	    $totalcount = $db_main2->getvalue("SELECT COUNT(*) FROM tbl_user_debug $tail");
    }
        
    if ($totalcount < ($page-1) * $listcount && $page != 1)
    	$page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<script>
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	    search_form.submit();
	}

	function update_abuse_user_debug(useridx, status, platform) 
	{
		if(useridx == "")
		{
			useridx = prompt("등록할 useridx를 입력하세요.");

			if(useridx == null)
				return;
		}
		
		var msg = "";
		var comment = "";
		
		if(status == 0)
		{
			comment = prompt("Comment를 입력하세요.");

			if(comment == null)
				return;
			
			msg = "디버그 등록 하시겠습니까?";
		}
		else
			msg = "디버그 해제 하시겠습니까?";
		
		if(confirm(msg) == 0)
			return;

		var param = {};
		param.useridx = useridx;
		param.status = status;
		
		if(status == 0)
			param.comment = comment;			

		param.platform = platform;			
		
		WG_ajax_execute("monitoring/update_abuse_user_debug", param, update_abuse_user_debug_callback, true);
	}

	function update_abuse_user_debug_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else        
        {
            alert("수정 완료 했습니다.");
            window.location.reload(false);           
        }
    }
</script>

		<!-- CONTENTS WRAP -->
        <div class="contents_wrap">
	        <!-- title_warp -->
            <div class="title_wrap">
                <div class="title"><?= $top_menu_txt ?> &gt; 디버그 사용자목록(Mobile) <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
            </div>
	        <!-- //title_warp -->
		        
	        <form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
	        	<div class="detail_search_wrap">
	        		<input type="hidden" id="platform" name="platform" value="<?= $platform ?>"/>
	        		<span class="search_lbl">useridx</span>
		        	<input type="text" class="search_text" id="search_useridx" name="search_useridx" style="width:80px" value="<?= $search_useridx ?>" onkeypress="search_press(event); return checknum();" />
		        	<span class="search_lbl ml20">TYPE</span>
					<select name="type" id="type">
						<option value="" <?= ($search_user_type == "") ? "selected" : "" ?>>전체</option> 
						<option value="1" <?= ($search_user_type == "1") ? "selected" : "" ?>>직접등록</option> 		
						<option value="2" <?= ($search_user_type == "2") ? "selected" : "" ?>>slot team</option>									
					</select>
					<span class="search_lbl ml20">정렬</span>
					<select name="order_by" id="order_by">						
						<option value="1" <?= ($search_order_by == "1") ? "selected" : "" ?>>useridx</option> 		
						<option value="2" <?= ($search_order_by == "2") ? "selected" : "" ?>>등록일</option>									
					</select>
		        	<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
	        	</div>
	        </form>
	        
	        <table class="tbl_list_basic1">
	        <colgroup>
	        	<col width="80">
                <col width="150">
                <col width="">
                <col width="150">
                <col width="150">
                <col width="100">
	        </colgroup>
	        <thead>
	        	<th>번호</th>
                <th>useridx</th>
                <th>comment</th>
                <th>등록일</th>
                <th>최근 로그인</th>
                <th>디버그 해제</th>
            </thead>
            <tbody>
<?
	for($i=0;$i<sizeof($user_debug_list);$i++)
	{
		$useridx = $user_debug_list[$i]["useridx"];
		$comment = $user_debug_list[$i]["comment"];
		$writedate = $user_debug_list[$i]["writedate"];
		$logindate = $db_main -> getvalue("SELECT logindate FROM tbl_user WHERE useridx = $useridx");
?>
		<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
			<td class="tdc"><?=  $totalcount - (($page-1) * $listcount) - $i ?></td>
			<td class="tdc"><?= $useridx ?></td>
			<td class="tdl"><?= $comment ?></td>
			<td class="tdc"><?= $writedate ?></td>
			<td class="tdc"><?= $logindate ?></td>
			<td class="tdc"><input type="button" class="btn_03" value="해제" onclick="update_abuse_user_debug(<?= $useridx ?>, 1, <?= $platform ?>)"></td>
		</tr>
<?
	}
?>
            </tbody>
	        </table>
	        
	        <div class="button_warp tdr">
				<input type="button" class="btn_setting_01" value="디버그 유저 등록" onclick="update_abuse_user_debug('', 0, <?= $platform ?>)">
			</div>
<?
		include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");	
?>
        </div>
	    <!--  //CONTENTS WRAP -->
	    
	    <div class="clear"></div>
<?
	$db_main2->end();
	$db_main->end();
	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>