<?
    $top_menu = "monitoring";
    $sub_menu = "redshift_data_dashboard";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");

	
	$tab = ($_GET["tab"] == "") ? "0" :$_GET["tab"];
    
	$os_term = "ALL";
    
	$db_analysis = new CDatabase_Analysis();
    
	$db_analysis->execute("SET wait_timeout=300");
    
    $sdate = date("Y-m-d", time() - 24 * 60 * 60 * 7);
    $edate = date("Y-m-d");
    
    $today = date("Y-m-d");
    $yesterday = date('Y-m-d',mktime(0,0,0,date("m", time()),date("d", time())-1,date("Y", time())));   
    
    ini_set("memory_limit", "-1");

?>
	<script type="text/javascript">

	    
    function refresh()
    {
        window.location.reload(false);
    }
	
	function tab_change(tab)
    {
        var search_form = document.search_form;
        search_form.tab.value = tab;
        search_form.submit();
    }

    /*function change_os_term(term)
	{
		if (term == "ALL")
			window.location.href = 'dashboard.php';
		else if (term == "0")
			window.location.href = 'dashboard_web.php';
		else if (term == "1")
			window.location.href = 'dashboard_ios.php';
		else if (term == "2")
			window.location.href = 'dashboard_android.php';
		else if (term == "3")
			window.location.href = 'dashboard_amazon.php';
		else if (term == "4")
			window.location.href = 'dashboard_offcanvas.php';
	}*/
	 
    
	</script>	
	<!-- CONTENTS WRAP -->
	<div class="contents_wrap">
	<!--form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">
		<input type="hidden" name="tab" id="tab" value="<?= $tab ?>" /-->
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><a href="/m8_monitoring/redshift_data_dashboard.php">Redshift Dash Board</a> (M: mysql / R:redshift)</div>
			
		</div>
		
		<!-- //title_warp -->

<?
	$sql = "SELECT description, mysql_value, redshift_value ".
	   	   "FROM tbl_redshift_dashboard_stat ".
	 	   "WHERE platform = 100 ".
	   	   "and type = 4 ".
	   	   "ORDER BY subtype ASC ";
	$user_list = $db_analysis->gettotallist($sql); 
	
?>
		<div style="width:400px;float:left;margin-left:10px;">
			<a style="font:12px;color:#000;font-weight:bold;cursor:ponter;">User Data</a>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="40">
					<col width="45">
					<col width="45">
					<col width="50">                 
				</colgroup>
				<thead>
					<tr>
						<th class="tdc">기간</th>
						<th>건수(M)</th>
						<th>건수(R)</th>
						<th class="tdc">check</th>
					</tr>
				</thead>
				<tbody>
<?
    for($i=0;$i<sizeof($user_list);$i++)
    {
        $description = $user_list[$i]["description"];
        $mysql_value = $user_list[$i]["mysql_value"];
        $redshift_value = $user_list[$i]["redshift_value"];
        
        if(($mysql_value - $redshift_value) == 0)
        {
            $check_image = "/images/icon/status_1.png";
        }
        else if(($mysql_value - $redshift_value) != 0)
        {
            $check_image = "/images/icon/error.png";
        }
          
?>
				<tr>
					<td class="tdc point"><?= $description ?></td>
					<td class="tdc"><?= number_format($mysql_value) ?></td>
					<td class="tdc"><?= number_format($redshift_value) ?></td>
					<td class="tdc"><img src="<?=$check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>						
				</tr>
<?
    }
?>
				</tbody>
			</table>
		</div>
<?
    $sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 3) AS mysql_28order_cnt, ".
            " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 3) AS redshift_28order_cnt, ".
            " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 1) AS mysql_28order, ".
            " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 1) AS redshift_28order, ".
            " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 2) AS mysql_28order_cnl, ".
            " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 2) AS redshift_28order_cnl ";
    $day28_list = $db_analysis->getarray($sql); 
    
    if($day28_list["mysql_28order_cnt"] == $day28_list["redshift_28order_cnt"])
    {
        if($day28_list["mysql_28order"] == $day28_list["redshift_28order"])
        {
            if($day28_list["mysql_28order_cnl"] == $day28_list["redshift_28order_cnl"])
            {
                $order28_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $order28_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $order28_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $order28_check_image = "/images/icon/error.png";
    }
    
    $sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 6) AS mysql_7order_cnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 6) AS redshift_7order_cnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 4) AS mysql_7order, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 4) AS redshift_7order, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 5) AS mysql_7order_cnl, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 5) AS redshift_7order_cnl ";
    $day7_list = $db_analysis->getarray($sql);
    
    if($day7_list["mysql_7order_cnt"] == $day7_list["redshift_7order_cnt"])
    {
        if($day7_list["mysql_7order"] == $day7_list["redshift_7order"])
        {
            if($day7_list["mysql_7order_cnl"] == $day7_list["redshift_7order_cnl"])
            {
                $order7_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $order7_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $order7_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $order7_check_image = "/images/icon/error.png";
    }
    
    $sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 9) AS mysql_yesterdayorder_cnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 9) AS redshift_yesterdayorder_cnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 7) AS mysql_yesterdayorder, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 7) AS redshift_yesterdayorder, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 8) AS mysql_yesterdayorder_cnl, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 100 AND type = 1 AND subtype = 8) AS redshift_yesterdayorder_cnl ";
    $yesteday_list = $db_analysis->getarray($sql);
    
    if($yesteday_list["mysql_yesterdayorder_cnt"] == $yesteday_list["redshift_yesterdayorder_cnt"])
    {
        if($yesteday_list["mysql_yesterdayorder"] == $yesteday_list["redshift_yesterdayorder"])
        {
            if($yesteday_list["mysql_yesterdayorder_cnl"] == $yesteday_list["redshift_yesterdayorder_cnl"])
            {
                $yesterdayorder_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $yesterdayorder_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $yesterdayorder_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $yesterdayorder_check_image = "/images/icon/error.png";
    }
?>
		<div style="width:665px;float:left;margin-left:10px;">
			<a style="font:12px;color:#000;font-weight:bold;cursor:ponter;">결제내역</a>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="100">
					<col width="60">
					<col width="60"> 
					<col width="100"> 
					<col width="100"> 
					<col width="80"> 
					<col width="80">	
					<col width="70">					
				</colgroup>
				<thead>
					<tr>
						<th class="tdc">기간</th>
						<th>건수(M)</th>
						<th>건수(R)</th>
						<th class="tdr">구매금액(M)</th>
						<th class="tdr">구매금액(R)</th>
						<th class="tdr">취소금액(M)</th>
						<th class="tdr">취소금액(R)</th>
						<th class="tdc">Check</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="tdc point">최근28일</td>
						<td class="tdc"><?= number_format($day28_list["mysql_28order_cnt"]) ?></td>
						<td class="tdc"><?= number_format($day28_list["redshift_28order_cnt"]) ?></td>
						<td class="tdr">$<?= number_format($day28_list["mysql_28order"]) ?></td>
						<td class="tdr">$<?= number_format($day28_list["redshift_28order"]) ?></td>
						<td class="tdr">$<?= number_format($day28_list["mysql_28order_cnl"]) ?></td>
						<td class="tdr">$<?= number_format($day28_list["redshift_28order_cnl"]) ?></td>
						<td class="tdc"><img src="<?=$order28_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						<td class="tdc point">최근7일</td>
						<td class="tdc"><?= number_format($day7_list["mysql_7order_cnt"]) ?></td>
						<td class="tdc"><?= number_format($day7_list["redshift_7order_cnt"]) ?></td>
						<td class="tdr">$<?= number_format($day7_list["mysql_7order"]) ?></td>
						<td class="tdr">$<?= number_format($day7_list["redshift_7order"]) ?></td>
						<td class="tdr">$<?= number_format($day7_list["mysql_7order_cnl"]) ?></td>
						<td class="tdr">$<?= number_format($day7_list["redshift_7order_cnl"]) ?></td>
						<td class="tdc"><img src="<?=$order7_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						<td class="tdc point">어제</td>
						<td class="tdc"><?= number_format($yesteday_list["mysql_yesterdayorder_cnt"]) ?></td>
						<td class="tdc"><?= number_format($yesteday_list["redshift_yesterdayorder_cnt"]) ?></td>
						<td class="tdr">$<?= number_format($yesteday_list["mysql_yesterdayorder"]) ?></td>
						<td class="tdr">$<?= number_format($yesteday_list["redshift_yesterdayorder"]) ?></td>
						<td class="tdr">$<?= number_format($yesteday_list["mysql_yesterdayorder_cnl"]) ?></td>
						<td class="tdr">$<?= number_format($yesteday_list["redshift_yesterdayorder_cnl"]) ?></td>
						<td class="tdc"><img src="<?=$yesterdayorder_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
				</tbody>
			</table>
		</div>
			
		<div class="clear"></div>
		
<?
	$sql = "SELECT description, mysql_value, redshift_value ".
	   	   "FROM tbl_redshift_dashboard_stat ".
	 	   "WHERE platform = 100 ".
	   	   "and type = 3 ".
	   	   "ORDER BY subtype ASC ";
	$freechip_list = $db_analysis->gettotallist($sql); 
	
?>
		<div style="width:400px;float:left;margin-top: 20px;margin-left:10px;">
			<a style="font:12px;color:#000;font-weight:bold;cursor:ponter;">Freecoin Data</a>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="40">
					<col width="45">
					<col width="45">
					<col width="50">                 
				</colgroup>
				<thead>
					<tr>
						<th class="tdc">기간</th>
						<th>건수(M)</th>
						<th>건수(R)</th>
						<th class="tdc">check</th>
					</tr>
				</thead>
				<tbody>
<?
    for($i=0;$i<sizeof($freechip_list);$i++)
    {
        $description = $freechip_list[$i]["description"];
        $mysql_value = $freechip_list[$i]["mysql_value"];
        $redshift_value = $freechip_list[$i]["redshift_value"];
        
        if(($mysql_value - $redshift_value) == 0)
        {
            $check_image = "/images/icon/status_1.png";
        }
        else if(($mysql_value - $redshift_value) != 0)
        {
            $check_image = "/images/icon/error.png";
        }
          
?>
				<tr>
					<td class="tdc point"><?= $description ?></td>
					<td class="tdc"><?= number_format($mysql_value) ?></td>
					<td class="tdc"><?= number_format($redshift_value) ?></td>
					<td class="tdc"><img src="<?=$check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>						
				</tr>
<?
    }
?>
				</tbody>
			</table>
		</div>
		
<?
	$sql = "SELECT description, mysql_value, redshift_value ".
	   	   "FROM tbl_redshift_dashboard_stat ".
	 	   "WHERE platform = 0 ".
	   	   "and type = 5 ".
	   	   "ORDER BY subtype ASC ";
	$retention_list = $db_analysis->gettotallist($sql); 
	
	$sql = "SELECT description, mysql_value, redshift_value ".
	   	   "FROM tbl_redshift_dashboard_stat ".
	 	   "WHERE platform = 1 ".
	   	   "and type = 5 ".
	   	   "ORDER BY subtype ASC ";
	$retention_mobile_list = $db_analysis->gettotallist($sql);
	
?>
		<div style="width:665px;float:left;margin-top: 20px;margin-left:10px;">
			<a style="font:12px;color:#000;font-weight:bold;cursor:ponter;">Retention Data</a>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="40">
					<col width="40">
					<col width="45">
					<col width="45">
					<col width="50">                 
				</colgroup>
				<thead>
					<tr>
						<th class="tdc">platform</th>
						<th class="tdc">기간</th>
						<th>건수(M)</th>
						<th>건수(R)</th>
						<th class="tdc">check</th>
					</tr>
				</thead>
				<tbody>
<?
    for($i=0;$i<sizeof($retention_list);$i++)
    {
        $description = $retention_list[$i]["description"];
        $mysql_value = $retention_list[$i]["mysql_value"];
        $redshift_value = $retention_list[$i]["redshift_value"];
        
        if(($mysql_value - $redshift_value) == 0)
        {
            $check_image = "/images/icon/status_1.png";
        }
        else if(($mysql_value - $redshift_value) != 0)
        {
            $check_image = "/images/icon/error.png";
        }
          
?>
				<tr>
<?
				if($i==0)
				{
?>
					<td class="tdc point" rowspan=3>WEB</td>
<?
				}
?>
					<td class="tdc point"><?= $description ?></td>
					<td class="tdc"><?= number_format($mysql_value) ?></td>
					<td class="tdc"><?= number_format($redshift_value) ?></td>
					<td class="tdc"><img src="<?=$check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>						
				</tr>
<?
    }
?>
<?
    for($i=0;$i<sizeof($retention_mobile_list);$i++)
    {
        $description = $retention_mobile_list[$i]["description"];
        $mysql_value = $retention_mobile_list[$i]["mysql_value"];
        $redshift_value = $retention_mobile_list[$i]["redshift_value"];
        
        if(($mysql_value - $redshift_value) == 0)
        {
            $check_image = "/images/icon/status_1.png";
        }
        else if(($mysql_value - $redshift_value) != 0)
        {
            $check_image = "/images/icon/error.png";
        }
          
?>
				<tr>
<?
				if($i==0)
				{
?>
					<td class="tdc point" rowspan=3>Mobile</td>
<?
				}
?>
					<td class="tdc point"><?= $description ?></td>
					<td class="tdc"><?= number_format($mysql_value) ?></td>
					<td class="tdc"><?= number_format($redshift_value) ?></td>
					<td class="tdc"><img src="<?=$check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>						
				</tr>
<?
    }
?>
				</tbody>
			</table>
		</div>
		
		<div class="clear"></div>
		
<?
	$sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 2 AND subtype = 1) AS mysql_7row_cnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 2 AND subtype = 1) AS redshift_7row_cnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 2 AND subtype = 2) AS mysql_7moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 2 AND subtype = 2) AS redshift_7moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 2 AND subtype = 3) AS mysql_7moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 2 AND subtype = 3) AS redshift_7moneyout ";
    $web_day7_list = $db_analysis->getarray($sql);
    
    if($web_day7_list["mysql_7row_cnt"] == $web_day7_list["redshift_7row_cnt"])
    {
        if($web_day7_list["mysql_7moneyin"] == $web_day7_list["redshift_7moneyin"])
        {
            if($web_day7_list["mysql_7moneyout"] == $web_day7_list["redshift_7moneyout"])
            {
                $web_gamelog_7_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $web_gamelog_7_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $web_gamelog_7_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $web_gamelog_7_check_image = "/images/icon/error.png";
    }
    
    $sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 2 AND subtype = 4) AS mysql_yesterday_rowcnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 2 AND subtype = 4) AS redshift_yesterday_rowcnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 2 AND subtype = 5) AS mysql_yesterday_moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 2 AND subtype = 5) AS redshift_yesterday_moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 2 AND subtype = 6) AS mysql_yesterday_moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 2 AND subtype = 6) AS redshift_yesterday_moneyout ";
    $web_yesteday_list = $db_analysis->getarray($sql);
    
    if($web_yesteday_list["mysql_yesterday_rowcnt"] == $web_yesteday_list["redshift_yesterday_rowcnt"])
    {
        if($web_yesteday_list["mysql_yesterday_moneyin"] == $web_yesteday_list["redshift_yesterday_moneyin"])
        {
            if($web_yesteday_list["mysql_yesterday_moneyout"] == $web_yesteday_list["redshift_yesterday_moneyout"])
            {
                $web_yesterday_gamelog_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $web_yesterday_gamelog_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $web_yesterday_gamelog_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $web_yesterday_gamelog_check_image = "/images/icon/error.png";
    }
	
	//IOS
	$sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 2 AND subtype = 1) AS mysql_7row_cnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 2 AND subtype = 1) AS redshift_7row_cnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 2 AND subtype = 2) AS mysql_7moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 2 AND subtype = 2) AS redshift_7moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 2 AND subtype = 3) AS mysql_7moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 2 AND subtype = 3) AS redshift_7moneyout ";
    $ios_day7_list = $db_analysis->getarray($sql);
    
    if($ios_day7_list["mysql_7row_cnt"] == $ios_day7_list["redshift_7row_cnt"])
    {
        if($ios_day7_list["mysql_7moneyin"] == $ios_day7_list["redshift_7moneyin"])
        {
            if($ios_day7_list["mysql_7moneyout"] == $ios_day7_list["redshift_7moneyout"])
            {
                $ios_gamelog_7_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $ios_gamelog_7_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $ios_gamelog_7_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $ios_gamelog_7_check_image = "/images/icon/error.png";
    }
    
    $sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 2 AND subtype = 4) AS mysql_yesterday_rowcnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 2 AND subtype = 4) AS redshift_yesterday_rowcnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 2 AND subtype = 5) AS mysql_yesterday_moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 2 AND subtype = 5) AS redshift_yesterday_moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 2 AND subtype = 6) AS mysql_yesterday_moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 2 AND subtype = 6) AS redshift_yesterday_moneyout ";
    $ios_yesteday_list = $db_analysis->getarray($sql);
    
    if($ios_yesteday_list["mysql_yesterday_rowcnt"] == $ios_yesteday_list["redshift_yesterday_rowcnt"])
    {
        if($ios_yesteday_list["mysql_yesterday_moneyin"] == $ios_yesteday_list["redshift_yesterday_moneyin"])
        {
            if($ios_yesteday_list["mysql_yesterday_moneyout"] == $ios_yesteday_list["redshift_yesterday_moneyout"])
            {
                $ios_yesterday_gamelog_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $ios_yesterday_gamelog_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $ios_yesterday_gamelog_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $ios_yesterday_gamelog_check_image = "/images/icon/error.png";
    }
	
	//Android
	$sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 2 AND subtype = 1) AS mysql_7row_cnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 2 AND subtype = 1) AS redshift_7row_cnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 2 AND subtype = 2) AS mysql_7moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 2 AND subtype = 2) AS redshift_7moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 2 AND subtype = 3) AS mysql_7moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 2 AND subtype = 3) AS redshift_7moneyout ";
    $and_day7_list = $db_analysis->getarray($sql);
    
    if($and_day7_list["mysql_7row_cnt"] == $and_day7_list["redshift_7row_cnt"])
    {
        if($and_day7_list["mysql_7moneyin"] == $and_day7_list["redshift_7moneyin"])
        {
            if($and_day7_list["mysql_7moneyout"] == $and_day7_list["redshift_7moneyout"])
            {
                $and_gamelog_7_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $and_gamelog_7_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $and_gamelog_7_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $and_gamelog_7_check_image = "/images/icon/error.png";
    }
    
    $sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 2 AND subtype = 4) AS mysql_yesterday_rowcnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 2 AND subtype = 4) AS redshift_yesterday_rowcnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 2 AND subtype = 5) AS mysql_yesterday_moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 2 AND subtype = 5) AS redshift_yesterday_moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 2 AND subtype = 6) AS mysql_yesterday_moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 2 AND subtype = 6) AS redshift_yesterday_moneyout ";
    $and_yesteday_list = $db_analysis->getarray($sql);
    
    if($and_yesteday_list["mysql_yesterday_rowcnt"] == $and_yesteday_list["redshift_yesterday_rowcnt"])
    {
        if($and_yesteday_list["mysql_yesterday_moneyin"] == $and_yesteday_list["redshift_yesterday_moneyin"])
        {
            if($and_yesteday_list["mysql_yesterday_moneyout"] == $and_yesteday_list["redshift_yesterday_moneyout"])
            {
                $and_yesterday_gamelog_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $and_yesterday_gamelog_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $and_yesterday_gamelog_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $and_yesterday_gamelog_check_image = "/images/icon/error.png";
    }
	
	//Amazon
	$sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 2 AND subtype = 1) AS mysql_7row_cnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 2 AND subtype = 1) AS redshift_7row_cnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 2 AND subtype = 2) AS mysql_7moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 2 AND subtype = 2) AS redshift_7moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 2 AND subtype = 3) AS mysql_7moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 2 AND subtype = 3) AS redshift_7moneyout ";
    $ama_day7_list = $db_analysis->getarray($sql);
    
    if($ama_day7_list["mysql_7row_cnt"] == $ama_day7_list["redshift_7row_cnt"])
    {
        if($ama_day7_list["mysql_7moneyin"] == $ama_day7_list["redshift_7moneyin"])
        {
            if($ama_day7_list["mysql_7moneyout"] == $ama_day7_list["redshift_7moneyout"])
            {
                $ama_gamelog_7_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $ama_gamelog_7_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $ama_gamelog_7_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $ama_gamelog_7_check_image = "/images/icon/error.png";
    }
    
    $sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 2 AND subtype = 4) AS mysql_yesterday_rowcnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 2 AND subtype = 4) AS redshift_yesterday_rowcnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 2 AND subtype = 5) AS mysql_yesterday_moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 2 AND subtype = 5) AS redshift_yesterday_moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 2 AND subtype = 6) AS mysql_yesterday_moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 2 AND subtype = 6) AS redshift_yesterday_moneyout ";
    $ama_yesteday_list = $db_analysis->getarray($sql);
    
    if($ama_yesteday_list["mysql_yesterday_rowcnt"] == $ama_yesteday_list["redshift_yesterday_rowcnt"])
    {
        if($ama_yesteday_list["mysql_yesterday_moneyin"] == $ama_yesteday_list["redshift_yesterday_moneyin"])
        {
            if($ama_yesteday_list["mysql_yesterday_moneyout"] == $ama_yesteday_list["redshift_yesterday_moneyout"])
            {
                $ama_yesterday_gamelog_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $ama_yesterday_gamelog_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $ama_yesterday_gamelog_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $ama_yesterday_gamelog_check_image = "/images/icon/error.png";
    }

	
?>
		
		<div style="width:1077px;float:left;margin-top: 20px;margin-left:10px;margin-bottom:10px;">
			<a style="font:12px;color:#000;font-weight:bold;cursor:ponter;">Gamelog Data</a>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="100">
					<col width="50">
					<col width="60">
					<col width="60"> 
					<col width="100"> 
					<col width="100"> 
					<col width="80"> 
					<col width="80">	
					<col width="70">					
				</colgroup>
				<thead>
					<tr>
						<th class="tdc">Platform</th>
						<th class="tdc">기간</th>
						<th>건수(M)</th>
						<th>건수(R)</th>
						<th class="tdr">Moneyin(M)</th>
						<th class="tdr">Moneyin(R)</th>
						<th class="tdr">Moneyout(M)</th>
						<th class="tdr">Moneyout(R)</th>
						<th class="tdc">Check</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="tdc point" rowspan=2>Facebook</td>
						<td class="tdc point">최근7일</td>
						<td class="tdc"><?= number_format($web_day7_list["mysql_7row_cnt"]) ?></td>
						<td class="tdc"><?= number_format($web_day7_list["redshift_7row_cnt"]) ?></td>
						<td class="tdr">$<?= number_format($web_day7_list["mysql_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($web_day7_list["redshift_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($web_day7_list["mysql_7moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($web_day7_list["redshift_7moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$web_gamelog_7_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						
						<td class="tdc point">어제</td>
						<td class="tdc"><?= number_format($web_yesteday_list["mysql_yesterday_rowcnt"]) ?></td>
						<td class="tdc"><?= number_format($web_yesteday_list["redshift_yesterday_rowcnt"]) ?></td>
						<td class="tdr">$<?= number_format($web_yesteday_list["mysql_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($web_yesteday_list["redshift_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($web_yesteday_list["mysql_yesterday_moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($web_yesteday_list["redshift_yesterday_moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$web_yesterday_gamelog_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						<td class="tdc point" rowspan=2>IOS</td>
						<td class="tdc point">최근7일</td>
						<td class="tdc"><?= number_format($ios_day7_list["mysql_7row_cnt"]) ?></td>
						<td class="tdc"><?= number_format($ios_day7_list["redshift_7row_cnt"]) ?></td>
						<td class="tdr">$<?= number_format($ios_day7_list["mysql_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ios_day7_list["redshift_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ios_day7_list["mysql_7moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($ios_day7_list["redshift_7moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$ios_gamelog_7_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						
						<td class="tdc point">어제</td>
						<td class="tdc"><?= number_format($ios_yesteday_list["mysql_yesterday_rowcnt"]) ?></td>
						<td class="tdc"><?= number_format($ios_yesteday_list["redshift_yesterday_rowcnt"]) ?></td>
						<td class="tdr">$<?= number_format($ios_yesteday_list["mysql_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ios_yesteday_list["redshift_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ios_yesteday_list["mysql_yesterday_moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($ios_yesteday_list["redshift_yesterday_moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$ios_yesterday_gamelog_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						<td class="tdc point" rowspan=2>Android</td>
						<td class="tdc point">최근7일</td>
						<td class="tdc"><?= number_format($and_day7_list["mysql_7row_cnt"]) ?></td>
						<td class="tdc"><?= number_format($and_day7_list["redshift_7row_cnt"]) ?></td>
						<td class="tdr">$<?= number_format($and_day7_list["mysql_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($and_day7_list["redshift_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($and_day7_list["mysql_7moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($and_day7_list["redshift_7moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$and_gamelog_7_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						
						<td class="tdc point">어제</td>
						<td class="tdc"><?= number_format($and_yesteday_list["mysql_yesterday_rowcnt"]) ?></td>
						<td class="tdc"><?= number_format($and_yesteday_list["redshift_yesterday_rowcnt"]) ?></td>
						<td class="tdr">$<?= number_format($and_yesteday_list["mysql_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($and_yesteday_list["redshift_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($and_yesteday_list["mysql_yesterday_moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($and_yesteday_list["redshift_yesterday_moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$and_yesterday_gamelog_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						<td class="tdc point" rowspan=2>Amazon</td>
						<td class="tdc point">최근7일</td>
						<td class="tdc"><?= number_format($ama_day7_list["mysql_7row_cnt"]) ?></td>
						<td class="tdc"><?= number_format($ama_day7_list["redshift_7row_cnt"]) ?></td>
						<td class="tdr">$<?= number_format($ama_day7_list["mysql_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ama_day7_list["redshift_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ama_day7_list["mysql_7moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($ama_day7_list["redshift_7moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$ama_gamelog_7_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						
						<td class="tdc point">어제</td>
						<td class="tdc"><?= number_format($ama_yesteday_list["mysql_yesterday_rowcnt"]) ?></td>
						<td class="tdc"><?= number_format($ama_yesteday_list["redshift_yesterday_rowcnt"]) ?></td>
						<td class="tdr">$<?= number_format($ama_yesteday_list["mysql_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ama_yesteday_list["redshift_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ama_yesteday_list["mysql_yesterday_moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($ama_yesteday_list["redshift_yesterday_moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$ama_yesterday_gamelog_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div class="clear"></div>
		
<?
	$sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 6 AND subtype = 1) AS mysql_7row_cnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 6 AND subtype = 1) AS redshift_7row_cnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 6 AND subtype = 2) AS mysql_7moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 6 AND subtype = 2) AS redshift_7moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 6 AND subtype = 3) AS mysql_7moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 6 AND subtype = 3) AS redshift_7moneyout ";
    $web_day7_3h_list = $db_analysis->getarray($sql);
    
    if($web_day7_3h_list["mysql_7row_cnt"] == $web_day7_3h_list["redshift_7row_cnt"])
    {
        if($web_day7_3h_list["mysql_7moneyin"] == $web_day7_3h_list["redshift_7moneyin"])
        {
            if($web_day7_3h_list["mysql_7moneyout"] == $web_day7_3h_list["redshift_7moneyout"])
            {
                $web_gamelog_3h_7_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $web_gamelog_3h_7_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $web_gamelog_3h_7_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $web_gamelog_3h_7_check_image = "/images/icon/error.png";
    }
    
    $sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 6 AND subtype = 4) AS mysql_yesterday_rowcnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 6 AND subtype = 4) AS redshift_yesterday_rowcnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 6 AND subtype = 5) AS mysql_yesterday_moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 6 AND subtype = 5) AS redshift_yesterday_moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 6 AND subtype = 6) AS mysql_yesterday_moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 0 AND type = 6 AND subtype = 6) AS redshift_yesterday_moneyout ";
    $web_yesteday_3h_list = $db_analysis->getarray($sql);
    
    if($web_yesteday_3h_list["mysql_yesterday_rowcnt"] == $web_yesteday_3h_list["redshift_yesterday_rowcnt"])
    {
        if($web_yesteday_3h_list["mysql_yesterday_moneyin"] == $web_yesteday_3h_list["redshift_yesterday_moneyin"])
        {
            if($web_yesteday_3h_list["mysql_yesterday_moneyout"] == $web_yesteday_3h_list["redshift_yesterday_moneyout"])
            {
                $web_yesterday_gamelog_3h_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $web_yesterday_gamelog_3h_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $web_yesterday_gamelog_3h_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $web_yesterday_gamelog_3h_check_image = "/images/icon/error.png";
    }
	
	//IOS
	$sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 6 AND subtype = 1) AS mysql_7row_cnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 6 AND subtype = 1) AS redshift_7row_cnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 6 AND subtype = 2) AS mysql_7moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 6 AND subtype = 2) AS redshift_7moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 6 AND subtype = 3) AS mysql_7moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 6 AND subtype = 3) AS redshift_7moneyout ";
    $ios_day7_3h_list = $db_analysis->getarray($sql);
    
    if($ios_day7_3h_list["mysql_7row_cnt"] == $ios_day7_3h_list["redshift_7row_cnt"])
    {
        if($ios_day7_3h_list["mysql_7moneyin"] == $ios_day7_3h_list["redshift_7moneyin"])
        {
            if($ios_day7_3h_list["mysql_7moneyout"] == $ios_day7_3h_list["redshift_7moneyout"])
            {
                $ios_gamelog_3h_7_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $ios_gamelog_3h_7_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $ios_gamelog_3h_7_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $ios_gamelog_3h_7_check_image = "/images/icon/error.png";
    }
    
    $sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 6 AND subtype = 4) AS mysql_yesterday_rowcnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 6 AND subtype = 4) AS redshift_yesterday_rowcnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 6 AND subtype = 5) AS mysql_yesterday_moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 6 AND subtype = 5) AS redshift_yesterday_moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 6 AND subtype = 6) AS mysql_yesterday_moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 1 AND type = 6 AND subtype = 6) AS redshift_yesterday_moneyout ";
    $ios_yesteday_3h_list = $db_analysis->getarray($sql);
    
    if($ios_yesteday_3h_list["mysql_yesterday_rowcnt"] == $ios_yesteday_3h_list["redshift_yesterday_rowcnt"])
    {
        if($ios_yesteday_3h_list["mysql_yesterday_moneyin"] == $ios_yesteday_3h_list["redshift_yesterday_moneyin"])
        {
            if($ios_yesteday_3h_list["mysql_yesterday_moneyout"] == $ios_yesteday_3h_list["redshift_yesterday_moneyout"])
            {
                $ios_yesterday_gamelog_3h_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $ios_yesterday_gamelog_3h_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $ios_yesterday_gamelog_3h_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $ios_yesterday_gamelog_3h_check_image = "/images/icon/error.png";
    }
	
	//Android
	$sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 6 AND subtype = 1) AS mysql_7row_cnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 6 AND subtype = 1) AS redshift_7row_cnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 6 AND subtype = 2) AS mysql_7moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 6 AND subtype = 2) AS redshift_7moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 6 AND subtype = 3) AS mysql_7moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 6 AND subtype = 3) AS redshift_7moneyout ";
    $and_day7_3h_list = $db_analysis->getarray($sql);
    
    if($and_day7_3h_list["mysql_7row_cnt"] == $and_day7_3h_list["redshift_7row_cnt"])
    {
        if($and_day7_3h_list["mysql_7moneyin"] == $and_day7_3h_list["redshift_7moneyin"])
        {
            if($and_day7_3h_list["mysql_7moneyout"] == $and_day7_3h_list["redshift_7moneyout"])
            {
                $and_gamelog_3h_7_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $and_gamelog_3h_7_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $and_gamelog_3h_7_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $and_gamelog_3h_7_check_image = "/images/icon/error.png";
    }
    
    $sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 6 AND subtype = 4) AS mysql_yesterday_rowcnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 6 AND subtype = 4) AS redshift_yesterday_rowcnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 6 AND subtype = 5) AS mysql_yesterday_moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 6 AND subtype = 5) AS redshift_yesterday_moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 6 AND subtype = 6) AS mysql_yesterday_moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 2 AND type = 6 AND subtype = 6) AS redshift_yesterday_moneyout ";
    $and_yesteday_3h_list = $db_analysis->getarray($sql);
    
    if($and_yesteday_3h_list["mysql_yesterday_rowcnt"] == $and_yesteday_3h_list["redshift_yesterday_rowcnt"])
    {
        if($and_yesteday_3h_list["mysql_yesterday_moneyin"] == $and_yesteday_3h_list["redshift_yesterday_moneyin"])
        {
            if($and_yesteday_3h_list["mysql_yesterday_moneyout"] == $and_yesteday_3h_list["redshift_yesterday_moneyout"])
            {
                $and_yesterday_gamelog_3h_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $and_yesterday_gamelog_3h_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $and_yesterday_gamelog_3h_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $and_yesterday_gamelog_3h_check_image = "/images/icon/error.png";
    }
	
	//Amazon
	$sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 6 AND subtype = 1) AS mysql_7row_cnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 6 AND subtype = 1) AS redshift_7row_cnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 6 AND subtype = 2) AS mysql_7moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 6 AND subtype = 2) AS redshift_7moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 6 AND subtype = 3) AS mysql_7moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 6 AND subtype = 3) AS redshift_7moneyout ";
    $ama_day7_3h_list = $db_analysis->getarray($sql);
    
    if($ama_day7_3h_list["mysql_7row_cnt"] == $ama_day7_3h_list["redshift_7row_cnt"])
    {
        if($ama_day7_3h_list["mysql_7moneyin"] == $ama_day7_3h_list["redshift_7moneyin"])
        {
            if($ama_day7_3h_list["mysql_7moneyout"] == $ama_day7_3h_list["redshift_7moneyout"])
            {
                $ama_gamelog_3h_7_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $ama_gamelog_3h_7_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $ama_gamelog_3h_7_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $ama_gamelog_3h_7_check_image = "/images/icon/error.png";
    }
    
    $sql = " SELECT (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 6 AND subtype = 4) AS mysql_yesterday_rowcnt, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 6 AND subtype = 4) AS redshift_yesterday_rowcnt, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 6 AND subtype = 5) AS mysql_yesterday_moneyin, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 6 AND subtype = 5) AS redshift_yesterday_moneyin, ".
        " (SELECT mysql_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 6 AND subtype = 6) AS mysql_yesterday_moneyout, ".
        " (SELECT redshift_value FROM tbl_redshift_dashboard_stat WHERE platform = 3 AND type = 6 AND subtype = 6) AS redshift_yesterday_moneyout ";
    $ama_yesteday_3h_list = $db_analysis->getarray($sql);
    
    if($ama_yesteday_3h_list["mysql_yesterday_rowcnt"] == $ama_yesteday_3h_list["redshift_yesterday_rowcnt"])
    {
        if($ama_yesteday_3h_list["mysql_yesterday_moneyin"] == $ama_yesteday_3h_list["redshift_yesterday_moneyin"])
        {
            if($ama_yesteday_3h_list["mysql_yesterday_moneyout"] == $ama_yesteday_3h_list["redshift_yesterday_moneyout"])
            {
                $ama_yesterday_gamelog_3h_check_image = "/images/icon/status_1.png";
            }
            else
            {
                $ama_yesterday_gamelog_3h_check_image = "/images/icon/error.png";
            }
        }
        else
        {
            $ama_yesterday_gamelog_3h_check_image = "/images/icon/error.png";
        }
    }
    else
    {
        $ama_yesterday_gamelog_3h_check_image = "/images/icon/error.png";
    }

	
?>
		
		<div style="width:1077px;float:left;margin-top: 20px;margin-left:10px;margin-bottom:10px;">
			<a style="font:12px;color:#000;font-weight:bold;cursor:ponter;">Gamelog Data(3h)</a>
			<table class="tbl_list_basic1" style="margin-top:5px">
				<colgroup>
					<col width="100">
					<col width="50">
					<col width="60">
					<col width="60"> 
					<col width="100"> 
					<col width="100"> 
					<col width="80"> 
					<col width="80">	
					<col width="70">					
				</colgroup>
				<thead>
					<tr>
						<th class="tdc">Platform</th>
						<th class="tdc">기간</th>
						<th>건수(M)</th>
						<th>건수(R)</th>
						<th class="tdr">Moneyin(M)</th>
						<th class="tdr">Moneyin(R)</th>
						<th class="tdr">Moneyout(M)</th>
						<th class="tdr">Moneyout(R)</th>
						<th class="tdc">Check</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="tdc point" rowspan=2>Facebook</td>
						<td class="tdc point">최근7일</td>
						<td class="tdc"><?= number_format($web_day7_3h_list["mysql_7row_cnt"]) ?></td>
						<td class="tdc"><?= number_format($web_day7_3h_list["redshift_7row_cnt"]) ?></td>
						<td class="tdr">$<?= number_format($web_day7_3h_list["mysql_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($web_day7_3h_list["redshift_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($web_day7_3h_list["mysql_7moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($web_day7_3h_list["redshift_7moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$web_gamelog_3h_7_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						
						<td class="tdc point">어제</td>
						<td class="tdc"><?= number_format($web_yesteday_3h_list["mysql_yesterday_rowcnt"]) ?></td>
						<td class="tdc"><?= number_format($web_yesteday_3h_list["redshift_yesterday_rowcnt"]) ?></td>
						<td class="tdr">$<?= number_format($web_yesteday_3h_list["mysql_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($web_yesteday_3h_list["redshift_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($web_yesteday_3h_list["mysql_yesterday_moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($web_yesteday_3h_list["redshift_yesterday_moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$web_yesterday_gamelog_3h_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						<td class="tdc point" rowspan=2>IOS</td>
						<td class="tdc point">최근7일</td>
						<td class="tdc"><?= number_format($ios_day7_3h_list["mysql_7row_cnt"]) ?></td>
						<td class="tdc"><?= number_format($ios_day7_3h_list["redshift_7row_cnt"]) ?></td>
						<td class="tdr">$<?= number_format($ios_day7_3h_list["mysql_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ios_day7_3h_list["redshift_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ios_day7_3h_list["mysql_7moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($ios_day7_3h_list["redshift_7moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$ios_gamelog_3h_7_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						
						<td class="tdc point">어제</td>
						<td class="tdc"><?= number_format($ios_yesteday_3h_list["mysql_yesterday_rowcnt"]) ?></td>
						<td class="tdc"><?= number_format($ios_yesteday_3h_list["redshift_yesterday_rowcnt"]) ?></td>
						<td class="tdr">$<?= number_format($ios_yesteday_3h_list["mysql_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ios_yesteday_3h_list["redshift_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ios_yesteday_3h_list["mysql_yesterday_moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($ios_yesteday_3h_list["redshift_yesterday_moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$ios_yesterday_gamelog_3h_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						<td class="tdc point" rowspan=2>Android</td>
						<td class="tdc point">최근7일</td>
						<td class="tdc"><?= number_format($and_day7_3h_list["mysql_7row_cnt"]) ?></td>
						<td class="tdc"><?= number_format($and_day7_3h_list["redshift_7row_cnt"]) ?></td>
						<td class="tdr">$<?= number_format($and_day7_3h_list["mysql_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($and_day7_3h_list["redshift_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($and_day7_3h_list["mysql_7moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($and_day7_3h_list["redshift_7moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$and_gamelog_3h_7_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						
						<td class="tdc point">어제</td>
						<td class="tdc"><?= number_format($and_yesteday_3h_list["mysql_yesterday_rowcnt"]) ?></td>
						<td class="tdc"><?= number_format($and_yesteday_3h_list["redshift_yesterday_rowcnt"]) ?></td>
						<td class="tdr">$<?= number_format($and_yesteday_3h_list["mysql_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($and_yesteday_3h_list["redshift_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($and_yesteday_3h_list["mysql_yesterday_moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($and_yesteday_3h_list["redshift_yesterday_moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$and_yesterday_gamelog_3h_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						<td class="tdc point" rowspan=2>Amazon</td>
						<td class="tdc point">최근7일</td>
						<td class="tdc"><?= number_format($ama_day7_3h_list["mysql_7row_cnt"]) ?></td>
						<td class="tdc"><?= number_format($ama_day7_3h_list["redshift_7row_cnt"]) ?></td>
						<td class="tdr">$<?= number_format($ama_day7_3h_list["mysql_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ama_day7_3h_list["redshift_7moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ama_day7_3h_list["mysql_7moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($ama_day7_3h_list["redshift_7moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$ama_gamelog_3h_7_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
					<tr>
						
						<td class="tdc point">어제</td>
						<td class="tdc"><?= number_format($ama_yesteday_3h_list["mysql_yesterday_rowcnt"]) ?></td>
						<td class="tdc"><?= number_format($ama_yesteday_3h_list["redshift_yesterday_rowcnt"]) ?></td>
						<td class="tdr">$<?= number_format($ama_yesteday_3h_list["mysql_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ama_yesteday_3h_list["redshift_yesterday_moneyin"]) ?></td>
						<td class="tdr">$<?= number_format($ama_yesteday_3h_list["mysql_yesterday_moneyout"]) ?></td>
						<td class="tdr">$<?= number_format($ama_yesteday_3h_list["redshift_yesterday_moneyout"]) ?></td>
						<td class="tdc"><img src="<?=$ama_yesterday_gamelog_3h_check_image?>" align="absmiddle" style="cursor:pointer" align="absmiddle" width="15px" height="15px"></td>
					</tr>
				</tbody>
			</table>
		</div>
		 
		<div class="clear"></div>
		
	<!--/form-->
	</div>
	<div class="clear"></div>
	<div class="clear"></div>
	<!-- //CONTENTS WRAP -->
<? 
    $db_analysis->end();
	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>