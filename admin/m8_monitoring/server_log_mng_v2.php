<?
    $top_menu = "monitoring";
    $sub_menu = "server_log_v2";
     
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $page = ($_GET["page"] == "") ? "1" : $_GET["page"];
    $search_order_by = ($_GET["orderby"] == "") ? "writedate desc" : $_GET["orderby"];
    
    $search_ipaddress = trim($_GET["ipaddress"]);
    $search_cmd = trim($_GET["cmd"]);
    $search_errcode = trim($_GET["errcode"]);
    $search_start_latency = $_GET["start_latency"];
    $search_end_latency = $_GET["end_latency"];
    $search_start_writedate = $_GET["start_writedate"];
    $search_end_writedate = $_GET["end_writedate"];
    $search_category = $_GET["category"];
    $search_before_minute = $_GET["before_minute"];
     
    check_xss($search_ipaddress.$search_cmd.$search_errcode.$search_start_latency.$search_end_latency);
    
    $listcount = "10";
    $pagename = "server_log_mng_v2.php";
    $pagefield = "orderby=$search_order_by&ipaddress=$search_ipaddress&facebookid=$search_facebookid&cmd=$search_cmd&errcode=$search_errcode&start_latency=$search_start_latency&end_latency=$search_end_latency&start_writedate=$search_start_writedate&end_writedate=$search_end_writedate&category=$search_category&before_minute=$search_before_minute";
    
    $tail = " WHERE 1=1";
    $order_by = "ORDER BY ".str_replace(":", " ", $search_order_by);
    
    if ($search_category == "1")
    	$tail .= " AND errcode='0'";
    else if ($search_category == "2")
    	$tail .= " AND errcode!='0'";
    
    if ($search_ipaddress != "")
    	$tail .= " AND ipaddress LIKE '%$search_ipaddress%' ";
    
    if ($search_cmd != "")
    	$tail .= " AND cmd LIKE '%$search_cmd%' ";
    
    if ($search_errcode != "")
    	$tail .= " AND errcode LIKE '%$search_errcode%' ";
    
    if ($search_before_minute != "")
    	$tail .= " AND writedate <= DATE_SUB(NOW(), INTERVAL $search_before_minute MINUTE) ";
    
    if ($search_start_latency != "")
    	$tail .= " AND latency >= $search_start_latency ";
    
    if ($search_end_latency != "")
    	$tail .= " AND latency <= $search_end_latency ";
    
    if ($search_start_writedate != "")
    	$tail .= " AND writedate >= '$search_start_writedate 00:00:00' ";
    
    if ($search_end_writedate != "")
    	$tail .= " AND writedate <= '$search_end_writedate 23:59:59' ";
     
    $db_analysis = new CDatabase_Analysis();
    
    $totalcount = $db_analysis->getvalue("SELECT COUNT(*) FROM server_communication_log_v2 $tail");
    
    $sql = "SELECT logidx,ipaddress,cmd,errcode,latency,writedate FROM server_communication_log_v2 $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;
    
    $loglist = $db_analysis->gettotallist($sql);
    
    $db_analysis->end();
    
    if ($totalcount < ($page-1) * $listcount && page != 1)
    	$page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	    search_form.submit();
	}
	
	$(function() {
	    $("#start_writedate").datepicker({ });
	});
	
	$(function() {
	    $("#end_writedate").datepicker({ });
	});
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 서버 통신 로그 (Web)<span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
	</div>
	<!-- //title_warp -->
	
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?= $pagename ?>">
		<div class="detail_search_wrap">
			<span class="search_lbl">IP 주소</span>
			<input type="input" class="search_text" id="ipaddress" name="ipaddress" style="width:120px" value="<?= encode_html_attribute($search_ipaddress) ?>" onkeypress="search_press(event)" />
                    
			<span class="search_lbl ml20">cmd</span>
			<input type="input" class="search_text" id="cmd" name="cmd" style="width:120px" value="<?= $search_cmd ?>" onkeypress="search_press(event); return checkonlynum();" />
                    
			<span class="search_lbl ml20">에러코드</span>
			<input type="input" class="search_text" id="errcode" name="errcode" style="width:120px" value="<?= encode_html_attribute($search_errcode) ?>" onkeypress="search_press(event)" />
                           
			<span class="search_lbl ml20">이벤트 종류</span>
			<select name="category" id="category">
				<option value="">선택</option>
				<option value="1" <?= ($search_category == "1") ? "selected" : "" ?>>Warning</option>
				<option value="2" <?= ($search_category == "2") ? "selected" : "" ?>>Error</option> 
			</select>
                    
			<div class="clear"  style="padding-top:10px"></div>
                    
			<span class="search_lbl">반응시간</span>
			<input type="input" class="search_text" id="start_latency" name="start_latency" style="width:120px" value="<?= $search_start_latency ?>" onkeypress="search_press(event); return checkonlynum();" /> -
			<input type="input" class="search_text" id="end_latency" name="end_latency" style="width:120px" value="<?= $search_end_latency ?>" onkeypress="search_press(event); return checkonlynum();" />
                    
			<span class="search_lbl ml20">접속일시</span>
			<input type="input" class="search_text" id="start_writedate" name="start_writedate" style="width:65px" value="<?= $search_start_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
			<input type="input" class="search_text" id="end_writedate" name="end_writedate" style="width:65px" value="<?= $search_end_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
                                 
			<span class="search_lbl ml20">시간검색</span>
			<input type="input" class="search_text" id="before_minute" name="before_minute" style="width:40px" value="<?= $search_before_minute ?>" onkeypress="search_press(event); return checkonlynum();" />분 전
                    
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
            
		<div style="float:right;padding-bottom:5px;">
			<select name="orderby" id="orderby" onchange="search()">
				<option value="" <?= ($search_order_by == "") ? "selected" : "" ?>>정렬선택</option>
				<option value="ipaddress:asc" <?= ($search_order_by == "ipaddress:asc") ? "selected" : "" ?>>ip주소 오름차순</option>
				<option value="ipaddress:desc" <?= ($search_order_by == "ipaddress:desc") ? "selected" : "" ?>>ip주소 내림차순</option>
				<option value="cmd:asc" <?= ($search_order_by == "cmd:asc") ? "selected" : "" ?>>cmd 오름차순</option>
				<option value="cmd:desc" <?= ($search_order_by == "cmd:desc") ? "selected" : "" ?>>cmd 내림차순</option>
				<option value="errcode:asc" <?= ($search_order_by == "errcode:asc") ? "selected" : "" ?>>에러코드 오름차순</option>
				<option value="errcode:desc" <?= ($search_order_by == "errcode:desc") ? "selected" : "" ?>>에러코드 내림차순</option>
				<option value="latency:asc" <?= ($search_order_by == "latency:asc") ? "selected" : "" ?>>반응시간 오름차순</option>
				<option value="latency:desc" <?= ($search_order_by == "latency:desc") ? "selected" : "" ?>>반응시간 내림차순</option>
				<option value="writedate:asc" <?= ($search_order_by == "writedate:asc") ? "selected" : "" ?>>접속시간 오름차순</option>
				<option value="writedate:desc" <?= ($search_order_by == "writedate:desc") ? "selected" : "" ?>>접속시간 내림차순</option>
			</select>
		</div>
	</form>
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="5">
			<col width="50">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="150">
		</colgroup>
		<thead>
            <tr>
                <th style="padding-left:5px"></th>
                <th class="tdl">번호</th>
                <th>IP 주소</th>
                <th>cmd</th>
                <th>에러코드</th>
                <th>반응시간</th>
                <th>접속일시</th>
            </tr>
		</thead>
		<tbody>
<?
    for($i=0; $i<sizeof($loglist); $i++)
    {
        $logidx = $loglist[$i]["logidx"];
        $ipaddress = $loglist[$i]["ipaddress"];
        $cmd = $loglist[$i]["cmd"];
        $errcode = $loglist[$i]["errcode"];
        $latency = $loglist[$i]["latency"];
        $writedate = $loglist[$i]["writedate"];
        
        if ($errcode == "0")
        {
            $category_string = "Warning";
            $style = "";
            $img = "/images/icon/warning.png";
        }
        else
        {
            $category_string = "Error";
            $style = "style='color:#fb7878;'";
            $img = "/images/icon/error.png";
        }
?>
            <tr onmouseover="className='tr_over'" onmouseout="className=''" onclick="">
                <td class="tdc" style="padding-left:5px"><img src="<?= $img ?>" align="absmiddle" /></td>
                <td class="tdl" <?= $style ?>><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
                <td class="tdc point" <?= $style ?>><?= $ipaddress ?></td>
                <td class="tdc point" <?= $style ?>><?= $cmd ?></td>
                <td class="tdc point" <?= $style ?>><?= encode_html_attribute($errcode) ?></td>
                <td class="tdc point" <?= $style ?>><?= $latency ?></td>
                <td class="tdc" <?= $style ?>><?= $writedate ?></td>
            </tr>
<?
    } 
?>
		</tbody>
	</table>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>