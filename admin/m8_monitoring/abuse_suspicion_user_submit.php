<?
    $top_menu = "monitoring";
	$sub_menu = "abuse_suspicion_user";
	
	include ($_SERVER ["DOCUMENT_ROOT"] . "/m_common/top_frame.inc.php");
?>

<script>
	function submit_abuse_suspicion_user() 
	{
		var abuse_suspicion_user_form = document.abuse_suspicion_user_form;

		var param = {};
		param.useridx = abuse_suspicion_user_form.useridx.value;
		param.status = get_radio("status");
		param.comment = abuse_suspicion_user_form.comment.value; 

		WG_ajax_execute("monitoring/insert_abuse_suspicion_user", param, submit_abuse_suspicion_user_callback);
	}
	
	function submit_abuse_suspicion_user_callback(result, reason) 
	{
		if(!result)
		{
			alert("오류 발생 - " + reason);
		}
		else
		{
			alert("해당 사용자 등록하였습니다.");
			window.location.href = "javascript:history.back()";			
		}
	}
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 어뷰즈 의심 사용자 설정 </div>
	</div>
	<!-- //title_warp -->
	
	<form name="abuse_suspicion_user_form" id="abuse_suspicion_user_form">
		<table class="tbl_view_basic">
			<colgroup>
				<col width="150">
				<col width="">
			</colgroup>
			
			<tbody>
				<tr>
					<th>useridx</th>
					<td><input type="text" class="view_tbl_text" name="useridx" id="useridx" onkeydown="return event.keyCode != 13;" /></td>
				</tr>
				
				<tr>
					<th>상태</th>
					<td>
						<input type="radio" class="radio" name="status"  value="0" checked /> 미확인
						<input type="radio" class="radio" name="status"  value="1" /> 확인
						<input type="radio" class="radio" name="status"  value="2" /> 보류 
					</td>
				</tr>
				
				<tr>
					<th>comment</th>
					<td><textarea name="comment" id="comment" style="width:400px;height:100px;"><?= ($comment == "") ? "현재 코인 " : encode_html_attribute($comment) ?></textarea></td>
				</tr>
			</tbody>
		</table>
	</form>
	
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="저장" onclick="submit_abuse_suspicion_user()">
		<input type="button" class="btn_setting_02" value="취소" onclick="history.back()">
	</div>
</div>

<!--  //CONTENTS WRAP -->

<div class="clear"></div>

<?
	include ($_SERVER ["DOCUMENT_ROOT"] . "/m_common/bottom_frame.inc.php");
?>