<?
    $top_menu = "monitoring";
    $sub_menu = "scheduler_stats";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
    
    $today = date("Y-m-d",strtotime("-7 days"));
	$tomorrow = date("Y-m-d",strtotime("+1 days"));
    
    $db_analysis = new CDatabase_Analysis();
    
    $sql = "SELECT a.today, ifnull(oneminokcnt, 0) as oneminokcnt, lastdate ".
    		"FROM ( ".
    		"		SELECT today ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' AND type = 101 AND status = 1 ".
    		"		GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
    		") a LEFT JOIN ( ".
    		"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, COUNT( logidx ) AS oneminokcnt, MAX(writedate) AS lastdate ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE type = 101 AND status = 1 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' ".
    		"		GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
    		") b on a.today = b.today ".
    		"ORDER BY today DESC";
        
    $oneminok_list = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT a.today, ifnull(oneminfailcnt, 0) as oneminfailcnt ".
    		"FROM ( ".
    		"		SELECT today ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' AND type = 101 AND status = 0 ".
    		"		GROUP BY today ".
    		") a LEFT JOIN ( ".
    		"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, COUNT( logidx ) AS oneminfailcnt ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE type = 101 AND status = 0 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' ".
    		"		GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
    		") b on a.today = b.today ".
    		"ORDER BY today DESC";
    
    $oneminfail_list = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT a.today, ifnull(onemineventokcnt, 0) as onemineventokcnt, lastdate ".
    		"FROM ( ".
    		"		SELECT today ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' AND type = 102 AND status = 1 ".
    		"		GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
    		") a LEFT JOIN ( ".
    		"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, COUNT( logidx ) AS onemineventokcnt, MAX(writedate) AS lastdate ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE type = 102 AND status = 1 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' ".
    		"		GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
    		") b on a.today = b.today ".
    		"ORDER BY today DESC";
    
    $onemineventok_list = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT a.today, ifnull(onemineventfailcnt, 0) as oneminfailcnt ".
    		"FROM ( ".
    		"		SELECT today ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' AND type = 102 AND status = 0 ".
    		"		GROUP BY today ".
    		") a LEFT JOIN ( ".
    		"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, COUNT( logidx ) AS onemineventfailcnt ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE type = 102 AND status = 0 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' ".
    		"		GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
    		") b on a.today = b.today ".
    		"ORDER BY today DESC";
    
    $onemineventfail_list = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT a.today, ifnull(fiveminokcnt, 0) as fiveminokcnt, lastdate ".
    		"FROM ( ".
    		"		SELECT today ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' AND type = 201 AND status = 1 ".
    		"		GROUP BY today ".
    		") a LEFT JOIN ( ".
    		"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, COUNT( logidx ) AS fiveminokcnt, MAX(writedate) AS lastdate ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE type = 201 AND status = 1 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' ".
    		"		GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
    		") b on a.today = b.today ".
    		"ORDER BY today DESC";
    
    $fiveminok_list = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT a.today, ifnull(fiveminfailcnt, 0) as fiveminfailcnt ".
    		"FROM ( ".
    		"		SELECT today ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' AND type = 201 AND status = 0 ".
    		"		GROUP BY today ".
    		") a LEFT JOIN ( ".
    		"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, COUNT( logidx ) AS fiveminfailcnt ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE type = 201 AND status = 0 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' ".
    		"		GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
    		") b on a.today = b.today ".
    		"ORDER BY today DESC";
    
    $fiveminfail_list = $db_analysis->gettotallist($sql);    
    
    $sql = "SELECT a.today, ifnull(onehourokcnt, 0) as onehourokcnt, lastdate ".
    		"FROM ( ".
    		"		SELECT today ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' AND type = 301 AND status = 1 ".
    		"		GROUP BY today ".
    		") a LEFT JOIN ( ".
    		"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, COUNT( logidx ) AS onehourokcnt, MAX(writedate) AS lastdate ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE type = 301 AND status = 1 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' ".
    		"		GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
    		") b on a.today = b.today ".
    		"ORDER BY today DESC";
    
    $onehourok_list = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT a.today, ifnull(onehourfailcnt, 0) as onehourfailcnt ".
    		"FROM ( ".
    		"		SELECT today ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' AND type = 301 AND status = 0 ".
    		"		GROUP BY today ".
    		") a LEFT JOIN ( ".
    		"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, COUNT( logidx ) AS onehourfailcnt ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE type = 301 AND status = 0 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' ".
    		"		GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
    		") b on a.today = b.today ".
    		"ORDER BY today DESC";
    
    $onehourfail_list = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT a.today, ifnull(onedayokcnt, 0) as onedayokcnt, lastdate ".
    		"FROM ( ".
    		"		SELECT today ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' AND type = 401 AND status = 1 ".
    		"		GROUP BY today ".
    		") a LEFT JOIN ( ".
    		"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, COUNT( logidx ) AS onedayokcnt, MAX(writedate) AS lastdate ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE type = 401 AND status = 1 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' ".
    		"		GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
    		") b on a.today = b.today ".
    		"ORDER BY today DESC";
    
    $onedayok_list = $db_analysis->gettotallist($sql);
    
    $sql = "SELECT a.today, ifnull(onedayfailcnt, 0) as onedayfailcnt ".
    		"FROM ( ".
    		"		SELECT today ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' AND type = 401 AND status = 0 ".
    		"		GROUP BY today ".
    		") a LEFT JOIN ( ".
    		"		SELECT DATE_FORMAT(writedate, '%Y-%m-%d') AS today, COUNT( logidx ) AS onedayfailcnt ".
    		"		FROM tbl_scheduler_log ".
    		"		WHERE type = 401 AND status = 0 AND '$today 00:00:00' <= writedate AND writedate < '$tomorrow 00:00:00' ".
    		"		GROUP BY DATE_FORMAT(writedate, '%Y-%m-%d') ".
    		") b on a.today = b.today ".
    		"ORDER BY today DESC";
    
    $onedayfail_list = $db_analysis->gettotallist($sql);
    
    $db_analysis->end();
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
        
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 스케줄러 모니터링 </div>
	</div>
	
	<table class="tbl_list_basic1">
		<colgroup>
            <col width="10%">
            <col width="15%">
            <col width="15%">
            <col width="15%">
            <col width="15%">
            <col width="15%">           
		</colgroup>
        <thead>
        	<tr>
        		<th>일자</th>
            	<th>1분</th>
            	<th>1분(이벤트)</th>
             	<th>5분</th>             	
               	<th>1시간</th>
               	<th>1일</th>
         	</tr>
        </thead>
        <tbody>
<?
	$row_count = sizeof($oneminok_list);

    for($i=0; $i<$row_count; $i++)
    {
    	$today = $oneminok_list[$i]["today"];
    	$onemin = "OK";
    	$oneminokcnt = $oneminok_list[$i]["oneminokcnt"];
    	$oneminlastdate = $oneminok_list[$i]["lastdate"];
    	$oneminfailcnt = $oneminfail_list[$i]["oneminfailcnt"];
    	$oneminevent = "OK";
    	$onemineventokcnt = $onemineventok_list[$i]["onemineventokcnt"];
    	$onemineventlastdate = $onemineventok_list[$i]["lastdate"];
    	$onemineventfailcnt = $onemineventfail_list[$i]["onemineventfailcnt"];
    	$fivemin = "OK";
    	$fiveminokcnt = $fiveminok_list[$i]["fiveminokcnt"];
    	$fiveminlastdate = $fiveminok_list[$i]["lastdate"];
    	$fiveminfailcnt = $fiveminfail_list[$i]["fiveminfailcnt"];    	
    	$onehour = "OK";
    	$onehourokcnt = $onehourok_list[$i]["onehourokcnt"];
    	$onehourlastdate = $onehourok_list[$i]["lastdate"];
    	$onehourfailcnt = $onehourfail_list[$i]["onehourfailcnt"];
    	$oneday = "OK";
    	$onedayokcnt = $onedayok_list[$i]["onedayokcnt"];
    	$onedaylastdate = $onedayok_list[$i]["lastdate"];
    	$onedayfailcnt = $onedayfail_list[$i]["onedayfailcnt"];
    	
    	$oneminstatus = "blue";
    	$onemineventstatus = "blue";
    	$fiveminstatus = "blue";
    	$onehourstatus = "blue";
    	$onedaystatus = "blue";
    	
    	if($today == date("Y-m-d"))
    	{
    		if($oneminfailcnt == 1)
    		{
    			$oneminstatus = "orange";
    			$onemin = "Progressing";
    		}
    		else if($oneminfailcnt > 1)
    		{
    			$oneminstatus = "red";
    			$onemin = "Fail";
    		}
    		
    		if($onemineventfailcnt == 1)
    		{
    			$onemineventstatus = "orange";
    			$oneminevent = "Progressing";
    		}
    		else if($onemineventfailcnt > 1)
    		{
    			$onemineventstatus = "red";
    			$oneminevent = "Fail";
    		}
    		
    		if($fiveminfailcnt == 1)
    		{
    			$fiveminstatus = "orange";
    			$fivemin = "Progressing";
    		}
    		else if($fiveminfailcnt > 1)
    		{
    			$fiveminstatus = "red";
    			$fivemin = "Fail";
    		}
    		    		
    		
    		if($onehourfailcnt == 1)
    		{
    			$onehourstatus = "orange";
    			$onehour = "Progressing";
    		}
    		else if($onehourfailcnt > 1)
    		{
    			$onehourstatus = "red";
    			$onehour = "Fail";
    		}

    		if($onedayfailcnt == 1 && date("Y-m-d H:i:s") < date("Y-m-d")." 12:00:00")
    		{
    			$onedaystatus = "orange";
    			$oneday = "Progressing";
    		}
    		else if($onedayfailcnt == 1 && date("Y-m-d H:i:s") > date("Y-m-d")." 12:00:00")
    		{
    			$onedaystatus = "red";
    			$oneday = "Fail";
    		}
    		
    	}
    	else
    	{
    		if($oneminfailcnt > 0)
    		{
    			$oneminstatus = "red";
    			$onemin = "Fail";
    		}
    		
    		if($onemineventfailcnt > 0)
    		{
    			$onemineventstatus = "red";
    			$oneminevent = "Fail";
    		}
    		
    		if($fiveminfailcnt > 0)
    		{
    			$fiveminstatus = "red";
    			$fivemin = "Fail";
    		}    		
    		
    		if($onehourfailcnt > 0)
    		{
    			$onehourstatus = "red";
    			$onehour = "Fail";
    		}
    		
    		if($onedayfailcnt > 0)
    		{
    			$onedaystatus = "red";
    			$oneday = "Fail";
    		}
    	}
?>
		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
            <td class="tdc point"><?= $today ?></td>
            <td class="tdc"><span style="color:<?= $oneminstatus?>;font-weight:bold;"><?= $onemin ?></span><br/><?= $oneminokcnt ?>/1440<br/><?= $oneminlastdate?></td>
            <td class="tdc"><span style="color:<?= $onemineventstatus?>;font-weight:bold;"><?= $oneminevent ?></span><br/><?= $onemineventokcnt ?>/1440<br/><?= $onemineventlastdate?></td>
            <td class="tdc"><span style="color:<?= $fiveminstatus?>;font-weight:bold;"><?= $fivemin ?></span><br/><?= $fiveminokcnt ?>/288<br/><?= $fiveminlastdate?></td>
            <td class="tdc"><span style="color:<?= $onehourstatus?>;font-weight:bold;"><?= $onehour ?></span><br/><?= $onehourokcnt ?>/24<br/><?= $onehourlastdate?></td>
            <td class="tdc"><span style="color:<?= $onedaystatus?>;font-weight:bold;"><?= $oneday ?></span><br/><?= $onedayokcnt ?>/1<br/><?= $onedaylastdate?></td>            
        </tr>
<?
		$rowcnt++;
    }
    
    if($row_count == 0)
    {
?>
   		<tr class="" onmouseover="className='tr_over'" onmouseout="className=''">
   			<td class="tdc" colspan="7">검색 결과가 없습니다.</td>
   		</tr>
<?
   	}
?>
        </tbody>
	</table>
		
	<div class="clear"></div>
</div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>