<?
	$top_menu = "monitoring";
	$sub_menu = "graph_download_faillog_stat";
	
	include ($_SERVER ["DOCUMENT_ROOT"] . "/m_common/top_frame.inc.php");
	
	$startdate = $_GET ["startdate"];
	$enddate = $_GET ["enddate"];
	$pagename = "graph_download_faillog_stat.php";
	
	// 오늘 날짜 정보
	$today = date ( "Y-m-d" );
	$before_day = get_past_date ( $today, 7, "d" );
	$startdate = ($startdate == "") ? $before_day : $startdate;
	$enddate = ($enddate == "") ? $today : $enddate;

	$db_analysis = new CDatabase_Analysis ();
	
	// iOS
	$sql = "SELECT *
			FROM tbl_download_fail_stat
			WHERE today BETWEEN '$startdate' AND '$enddate' AND platform = 1
			ORDER BY today DESC";
	$ios_stat_list = $db_analysis->gettotallist($sql);
	
	$ios_date_list = array();
	$ios_totalcount_list = array();
	$ios_usercount_list = array();
	
	for($i=0; $i<sizeof($ios_stat_list); $i++)
	{
		$ios_date_list[$i] = $ios_stat_list[$i]["today"];
		$ios_totalcount_list[$i] = $ios_stat_list[$i]["total_count"];
		$ios_usercount_list[$i] = $ios_stat_list[$i]["user_count"];
	}
	
	// Android
	$sql = "SELECT *
			FROM tbl_download_fail_stat
			WHERE today BETWEEN '$startdate' AND '$enddate' AND platform = 2
			ORDER BY today DESC";
	$android_stat_list = $db_analysis->gettotallist($sql);
	
	$android_date_list = array();
	$android_totalcount_list = array();
	$android_usercount_list = array();
	
	for($i=0; $i<sizeof($android_stat_list); $i++)
	{
		$android_date_list[$i] = $android_stat_list[$i]["today"];
		$android_totalcount_list[$i] = $android_stat_list[$i]["total_count"];
		$android_usercount_list[$i] = $android_stat_list[$i]["user_count"];
	}
	
	// Amazon
	$sql = "SELECT *
			FROM tbl_download_fail_stat
			WHERE today BETWEEN '$startdate' AND '$enddate' AND platform = 3
			ORDER BY today DESC";
	$amazon_stat_list = $db_analysis->gettotallist($sql);
	
	$amazon_date_list = array();
	$amazon_totalcount_list = array();
	$amazon_usercount_list = array();
	
	for($i=0; $i<sizeof($amazon_stat_list); $i++)
	{
		$amazon_date_list[$i] = $amazon_stat_list[$i]["today"];
		$amazon_totalcount_list[$i] = $amazon_stat_list[$i]["total_count"];
		$amazon_usercount_list[$i] = $amazon_stat_list[$i]["user_count"];
	}

	$db_analysis->end ();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});

    function drawChart() 
    {
     	// iOS
		var data2 = new google.visualization.DataTable();
        
        data2.addColumn('string', '날짜');
        data2.addColumn('number', 'totalcount');
        data2.addColumn('number', 'usercount');
        data2.addRows([
<?
for($i = sizeof ( $ios_date_list ); $i > 0; $i --) {
	$date = $ios_date_list [$i - 1];
	$totalcount = $ios_totalcount_list [$i - 1];
	$usercount = $ios_usercount_list [$i - 1];
	
	echo ("['" . $date . "'");
	
	if ($totalcount != "")
		echo (",{v:" . $totalcount . ",f:'" . make_price_format ( $totalcount ) . "'}");
	else
		echo (",0");
	
	if ($usercount != "")
		echo (",{v:" . $usercount . ",f:'" . make_price_format ( $usercount ) . "'}]");
	else
		echo (",0]");
	
	if ($i > 1)
		echo (",");
}
?>     
        ]);

     	// Android
		var data3 = new google.visualization.DataTable();
        
        data3.addColumn('string', '날짜');
        data3.addColumn('number', 'totalcount');
        data3.addColumn('number', 'usercount');
        data3.addRows([
<?
for($i = sizeof ( $android_date_list ); $i > 0; $i --) {
	$date = $android_date_list [$i - 1];
	$totalcount = $android_totalcount_list [$i - 1];
	$usercount = $android_usercount_list [$i - 1];
	
	echo ("['" . $date . "'");
	
	if ($totalcount != "")
		echo (",{v:" . $totalcount . ",f:'" . make_price_format ( $totalcount ) . "'}");
	else
		echo (",0");
	
	if ($usercount != "")
		echo (",{v:" . $usercount . ",f:'" . make_price_format ( $usercount ) . "'}]");
	else
		echo (",0]");
	
	if ($i > 1)
		echo (",");
}
?>     
        ]);

     // Amazon
		var data4 = new google.visualization.DataTable();
        
        data4.addColumn('string', '날짜');
        data4.addColumn('number', 'totalcount');
        data4.addColumn('number', 'usercount');
        data4.addRows([
<?
for($i = sizeof ( $amazon_date_list ); $i > 0; $i --) {
	$date = $amazon_date_list [$i - 1];
	$totalcount = $amazon_totalcount_list [$i - 1];
	$usercount = $amazon_usercount_list [$i - 1];
	
	echo ("['" . $date . "'");
	
	if ($totalcount != "")
		echo (",{v:" . $totalcount . ",f:'" . make_price_format ( $totalcount ) . "'}");
	else
		echo (",0");
	
	if ($usercount != "")
		echo (",{v:" . $usercount . ",f:'" . make_price_format ( $usercount ) . "'}]");
	else
		echo (",0]");
	
	if ($i > 1)
		echo (",");
}
?>     
        ]);
    
        var options = {
                title:'',                                                      
                width:1050,                         
                height:200,
                axisTitlesPosition:'in',
                curveType:'none',
                focusTarget:'category',
                interpolateNulls:'true',
                legend:'top',
                fontSize : 12,
                chartArea:{left:80,top:40,width:1020,height:130}
        };
        
        var chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
        chart.draw(data2, options);

        var chart = new google.visualization.LineChart(document.getElementById('chart_div3'));
        chart.draw(data3, options);

        var chart = new google.visualization.LineChart(document.getElementById('chart_div4'));
        chart.draw(data4, options);
    }
  
    google.setOnLoadCallback(drawChart);
    
    function search_press(e)
    {
        if (((e.which) ? e.which : e.keyCode) == 13)
        {
            search();
        }
    }

    function search()
    {
        var search_form = document.search_form;

        search_form.submit();
    }
    
    $(function() {
        $("#startdate").datepicker({ });
    });
    
    $(function() {
        $("#enddate").datepicker({ });
    });
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">

	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; Mobile Download Fail 통계</div>
		<form name="search_form" id="search_form" method="get"
			action="<?= $pagename ?>">
			<div class="search_box">
				<input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10"	style="width: 65px" onkeypress="search_press(event)" /> ~ 
				<input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width: 65px" maxlength="10" onkeypress="search_press(event)" /> 
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</form>
	</div>
	<!-- //title_warp -->

	<div class="search_result">
		<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다<span
	</div>
	
	<div class="h2_title">[iOS]</div>
	<div id="chart_div2" style="height: 230px; min-width: 500px"></div>
	
	<div class="h2_title">[Android]</div>
	<div id="chart_div3" style="height: 230px; min-width: 500px"></div>
	
	<div class="h2_title">[Amazon]</div>
	<div id="chart_div4" style="height: 230px; min-width: 500px"></div>
</div>

<!--  //CONTENTS WRAP -->

<div class="clear"></div>
</div>
<!-- //MAIN WRAP -->
<?
	include ($_SERVER ["DOCUMENT_ROOT"] . "/m_common/bottom_frame.inc.php");
?>