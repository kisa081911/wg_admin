<?
	$top_menu = "monitoring";
	$sub_menu = "download_faillog_stat_daily";
	 
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$os_term = ($_GET["os_term"]=="")? "all": $_GET["os_term"];
	$more = $_GET["more"];
	$limit = $_GET["limit"];
	$os_tail = "";
	
	if($more == "")
		$more = 0;
	
	if($limit == "1")
		$more = 0;
	
		
	if($os_term != "all")
		$os_tail = " AND platform = $os_term";
	
	$limit_tail = "LIMIT 20";	
		
	if($more == "1")
		$limit_tail = "";
	
	$page_name = "download_faillog_stat_daily.php";
	
	$startdate = ($_GET["startdate"] == "") ? date("Y-m-d", mktime(0,0,0,date("m"),date("d")-7,date("Y"))) : $_GET["startdate"];
	$enddate = ($_GET["enddate"] == "") ? date("Y-m-d", mktime(0,0,0,date("m"),date("d")-1,date("Y"))) : $_GET["enddate"];
	
	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT category_info, SUM(total_count) AS total_count, SUM(user_count) AS user_count 
			FROM tbl_download_fail_stat_detail 
			WHERE today BETWEEN '$startdate' AND '$enddate' $os_tail 
			GROUP BY category_info ORDER BY total_count DESC $limit_tail";
	$faillog_stat_list = $db_analysis->gettotallist($sql);
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#startdate").datepicker({ });
	});
	
	$(function() {
	    $("#enddate").datepicker({ });
	});
	
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search(limit)
	{
	    var search_form = document.search_form;

	    document.search_form.more.value = limit;
	    
	    search_form.submit();
	}

	function change_os_term(term)
	{
		var search_form = document.search_form;
 
		var ios = document.getElementById("term_ios");
		var android = document.getElementById("term_android");
		var amazon = document.getElementById("term_amazon");
		var term_all = document.getElementById("term_all");

		document.search_form.os_term.value = term;

		if (term == "all")
		{
			term_all.className="btn_schedule_select";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "1")
		{
			term_all.className="btn_schedule";
			ios.className="btn_schedule_select";
			android.className="btn_schedule";
			amazon.className="btn_schedule";
		}
		else if (term == "2")
		{
			term_all.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule_select";
			amazon.className="btn_schedule";
		}
		else if (term == "3")
		{
			term_all.className="btn_schedule";
			ios.className="btn_schedule";
			android.className="btn_schedule";
			amazon.className="btn_schedule_select";
		}		

		search_form.submit();
	}
</script>
<!-- CONTENTS WRAP -->
    <div class="contents_wrap">
        <!-- title_warp -->
        <div class="title_wrap">
        	<div class="title"><?= $top_menu_txt ?> &gt; Mobile Download Faillog 현황</div>
        	<form name="search_form" id="search_form"  method="get" action="<?= $page_name ?>">
            <div class="search_box"> 
            	<input type="button" class="<?= ($os_term == "all") ? "btn_schedule_select" : "btn_schedule" ?>" value="전체보기" id="term_all" onclick="change_os_term('all')" />           	
				<input type="button" class="<?= ($os_term == "1") ? "btn_schedule_select" : "btn_schedule" ?>" value="iOS" id="term_ios" onclick="change_os_term('1')" />
				<input type="button" class="<?= ($os_term == "2") ? "btn_schedule_select" : "btn_schedule" ?>" value="Android" id="term_android" onclick="change_os_term('2')"    />
				<input type="button" class="<?= ($os_term == "3") ? "btn_schedule_select" : "btn_schedule" ?>" value="Amazon" id="term_amazon" onclick="change_os_term('3')"    />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="hidden" name="os_term" id="os_term" value="<?= $os_term ?>" />
				<input type="hidden" name="more" id="more" value="<?= ($more == "1") ? "1" : "0" ?>" />
				<input type="hidden" name="limit" id="limit" value="<?= ($more == "1") ? "1" : "0" ?>" />
                <input type="text" class="search_text" id="startdate" name="startdate" value="<?= $startdate ?>" maxlength="10" style="width:65px" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> ~
                <input type="text" class="search_text" id="enddate" name="enddate" value="<?= $enddate ?>" style="width:65px" maxlength="10" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
                <input type="button" class="btn_search" value="검색" onclick="search()" />
            </div>
            </form>
        </div>
        <!-- //title_warp -->
        
        <div class="search_result">
        	<span><?= $startdate ?></span> ~ <span><?= $enddate ?></span> 통계입니다
        </div>
        
		<table class="tbl_list_basic1">
        	<colgroup>
            	<col width="">
            	<col width="">
            	<col width="">
            	<col width="">
            </colgroup>
            <thead>
            	<tr>
	            	<th class="tdc">Category Info</th>
	            	<th class="tdr">Total Count</th>
	            	<th class="tdr">User Count</th>
            	</tr>
            </thead>
			<tbody>
<?
	for($i=0; $i<sizeof($faillog_stat_list); $i++)
	{
		$category_info = $faillog_stat_list[$i]["category_info"];
		$total_count = $faillog_stat_list[$i]["total_count"];
		$user_count = $faillog_stat_list[$i]["user_count"];
		
?>
				<tr>
					<td class="tdc"><?= $category_info ?></td>
					<td class="tdr"><?= number_format($total_count) ?></td>
					<td class="tdr"><?= number_format($user_count) ?></td>
				</tr>
<?
	}
?>			
			</tbody>
		</table>
	</div>
<!--  //CONTENTS WRAP -->

<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>