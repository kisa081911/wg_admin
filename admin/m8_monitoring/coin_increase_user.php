<?
	$top_menu = "monitoring";
	$check = ($_GET["check"] == "") ? "0" : $_GET["check"];
	
	if($check == "0")
	{
		$sub_menu = "coin_increase_user";
		$table = "tbl_coin_increase_user_list";
	}
	else if($check == "1")
	{
		$sub_menu = "coin_increase_user_check";
		$table = "tbl_coin_increase_user_check_list";
	}
	else if($check == "2")
	{
		$sub_menu = "coin_increase_user_reserve";
		$table = "tbl_coin_increase_user_list";
	}
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$page = ($_GET["page"] == "") ? "1" : $_GET["page"];
	
	$search_adflag = $_GET["adflag"];
	$search_useridx = $_GET["useridx"];
	$search_create_sd = $_GET["search_create_sd"];
	$search_create_ed = $_GET["search_create_ed"];
	$search_write_sd = $_GET["search_write_sd"];
	$search_write_ed = $_GET["search_write_ed"];
	$search_status = $_GET["status"];
	
	$listcount = "10";
	$pagename = "coin_increase_user.php";
	$pagefield = "check=$check&adflag=$search_adflag&useridx=$search_useridx&search_create_sd=$search_create_sd&search_create_ed=$search_create_ed&search_write_sd=$search_write_sd&search_write_ed=$search_write_ed&status=$search_status";
	
	$tail = "WHERE 1=1 ";
	
	if($check == "0")
		$tail .= "AND status NOT IN('3') ";
	else if($check == "2")
		$tail .= "AND status = 3 ";
	
	if($search_status == "-1" || $search_status == "")
		$tail .= " AND 1=1 ";
	else if($search_status == 0)
		$tail .= " AND status = 0 ";
	else if($search_status == 1)
		$tail .= " AND status = 1 ";
	else if($search_status == 2)
		$tail .= " AND status = 2 ";
	
	$order_by = " ORDER BY useridx DESC, writedate DESC ";
	
	if($search_adflag == "1" || $search_adflag == "")
		$tail .= "AND adflag NOT IN ('mobile', 'mobile_ad', 'mobile_kindle') ";
	else if($search_adflag == "2")
		$tail .= "AND adflag = 'mobile' ";
	else if($search_adflag == "3")
		$tail .= "AND adflag = 'mobile_ad' ";
	else if($search_adflag == "4")
		$tail .= "AND adflag = 'mobile_kindle' ";
	
	if($search_useridx != "")
		$tail .= "AND useridx = $search_useridx ";
		
	if($search_create_sd != "")
		$tail .= "AND createdate >= '$search_create_sd 00:00:00' ";
	if($search_create_ed != "")
		$tail .= "AND createdate <= '$search_create_ed 23:59:59' ";
	
	if($search_write_sd != "")
		$tail .= "AND writedate >= '$search_write_sd 00:00:00' ";
	if($search_write_ed != "")
		$tail .= "AND writedate <= '$search_write_ed 23:59:59' ";
		
	$db_analysis = new CDatabase_Analysis();
	
	$sql = "SELECT COUNT(*) FROM $table $tail";
	$totalcount = $db_analysis->getvalue($sql);
	
	$sql = "SELECT * FROM $table $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;
	$unusualuser_list = $db_analysis->gettotallist($sql);
	
	if ($totalcount < ($page-1) * $listcount && $page != 1)
		$page = floor(($totalcount + $listcount - 1) / $listcount);
		
	$db_analysis->end();
?>

<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script>
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}

	function search()
	{
	    var search_form = document.search_form;
	
	    search_form.submit();
	}

	$(function() {
	    $("#search_create_sd").datepicker({ });
	    $("#search_write_sd").datepicker({ });
	});
	
	$(function() {
	    $("#search_create_ed").datepicker({ });
	    $("#search_write_ed").datepicker({ });
	});

	function selectAll(source) 
	{
		var holder = document.getElementById( "table_form" );
		var checkboxes = holder.getElementsByClassName( "chkbox" );
		
		for(var i=0; i<checkboxes.length; i++)
		{
			checkboxes[i].checked = source.checked;
		}	
	}

	var bindCheckboxes = ( 
			function() {
				var _last_selected = null;
				
				return function() {
					var holder = document.getElementById( "table_form" );

					if ( holder === null ) 
						return;
					
					var checkboxes = holder.getElementsByClassName( "chkbox" );
					var index = 0;
					
					for ( var i = 0; i < checkboxes.length; ++i ) 
					{
						if ( "checkbox" !== checkboxes[ i ].getAttribute( "type" ) ) 
							continue;
						
						( function( ix ) {
							checkboxes[ ix ].addEventListener( "click", 
									function( event ) {
										var checked = this.checked;
										
										if ( event.shiftKey && ix != _last_selected ) 
										{
											var start = Math.min( ix, _last_selected ), stop = Math.max( ix, _last_selected );
											
											for ( var j = start; j <= stop; ++j ) 
											{
												checkboxes[ j ].checked = checked;
											}

											_last_selected = null;
										} 
										else 
										{
											_last_selected = ix;
										}
									}, false );
		      			})( index );
		      			
		      			index++;
		    		}
					
		  		}
			}
	)();
	document.addEventListener( "DOMContentLoaded", function() { bindCheckboxes( "table_form" );}, false );

	function update_coin_increase_user_status() 
	{
		var holder = document.getElementById( "table_form" );
		var checkboxes = holder.getElementsByClassName( "chkbox" );
		var logidx_list = "";
		
		for(var i=0; i<checkboxes.length; i++)
		{
			if(checkboxes[i].checked == true)
			{
				if(logidx_list == "")
					logidx_list = checkboxes[i].value;
				else
					logidx_list += "," + checkboxes[i].value;
			}
		}

		var param = {};
		param.logidx_list = logidx_list;        
        
		WG_ajax_execute("monitoring/update_coin_increase_user_status", param, update_coin_increase_user_status_callback);
	}
	
	function update_coin_increase_user_status_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else        
        {
            alert("상태를 변경 했습니다.");
            window.location.href = window.location.href;
        }
    }
</script>

		
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 비결제 코인 급증 사용자(가입 1주내 미결제 5억이상) <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
	</div>
	<!-- //title_warp -->
	
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?=$pagename ?>">
		<div class="detail_search_wrap">
			<input type="hidden" name="check" id="check" value=<?= $check ?> />
			<span class="search_lbl">유입경로</span>
			<select name="adflag" id="adflag">
				<option value="0" <?= ($search_adflag == "0") ? "selected" : "" ?>>전체</option>
				<option value="1" <?= ($search_adflag == "1" || $search_adflag == "") ? "selected" : "" ?>>Web</option>
				<!-- <option value="2" <?= ($search_adflag == "2") ? "selected" : "" ?>>iOS</option>
				<option value="3" <?= ($search_adflag == "3") ? "selected" : "" ?>>Android</option>
				<option value="4" <?= ($search_adflag == "4") ? "selected" : "" ?>>Amazon</option>  -->
			</select>
			
			&nbsp;
			
			<span class="search_lbl">useridx</span>
			<input type="text" class="search_text" id="useridx" name="useridx" style="width:120px" value="<?= encode_html_attribute($search_useridx) ?>" onkeypress="search_press(event)" />&nbsp;
			<span class="search_lbl">가입일</span>
			<input type="text" class="search_text" id="search_create_sd" name="search_create_sd" style="width:65px" value="<?= $search_create_sd ?>" 
				onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
            <input type="text" class="search_text" id="search_create_ed" name="search_create_ed" style="width:65px" value="<?= $search_create_ed ?>" 
            	onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />&nbsp;
			<span class="search_lbl">등록일</span>
			<input type="text" class="search_text" id="search_write_sd" name="search_write_sd" style="width:65px" value="<?= $search_write_sd ?>" 
				onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
            <input type="text" class="search_text" id="search_write_ed" name="search_write_ed" style="width:65px" value="<?= $search_write_ed ?>" 
            	onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
            	
		&nbsp;
		
			 <select name="status" id="status">
				<option value="-1" <?= ($search_status == ""  || $search_status == "-1") ? "selected" : "" ?>>전체</option>
				<option value="0" <?= ($search_status == "0") ? "selected" : "" ?>>미확인</option>
				<option value="1" <?= ($search_status == "1") ? "selected" : "" ?>>확인</option>
				<option value="2" <?= ($search_status == "2") ? "selected" : "" ?>>보류</option>				
			</select>           	
			
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>
    
    <form id="table_form">
		<table name="tbl" class="tbl_list_basic1">
	        <colgroup>
<?
			if($check == "0")
			{
?>
	            <col width="30">
<?
			}
?>
	            <col width="65">
	            <col width="">
	            <col width="">
	            <col width="">
	            <col width="">
	            <col width="70">
	            <col width="70">
	            <col width="45">
	            <col width="">
	            <col width="70">
	            <col width="60">
	        </colgroup>
	        
	        <thead>
	      	    <tr>
<?
			if($check == "0")
			{
?>
	                <th><input type="checkbox" class="selectAll" onClick="selectAll(this)"></th>
<?
			}
?>
	                <th>유입경로</th>
	                <th>useridx</th>
	                <th>facebookid</th>
	                <th>이름</th>
	                <th>보유코인</th>
	                <th>가입일</th>
	                <th>등록일</th>
	                <th>상태</th>
	                <th>comment</th>
	                <th>기입일</th>
	                <th>상세</th>
	           	</tr>
			</thead> 
			
			<tbody>
<?
		$db_main = new CDatabase_Main();
	
    	for($i=0; $i<sizeof($unusualuser_list); $i++)
    	{
	     	$logidx = $unusualuser_list[$i]["logidx"];
	     	$useridx = $unusualuser_list[$i]["useridx"];
	      	$coin = $unusualuser_list[$i]["coin"];
	      	$adflag = $unusualuser_list[$i]["adflag"];
	      	$createdate = $unusualuser_list[$i]["createdate"];
	      	$writedate = $unusualuser_list[$i]["writedate"];
	      	$status = $unusualuser_list[$i]["status"];
	      	$comment = $unusualuser_list[$i]["comment"];
	      	$commentdate = $unusualuser_list[$i]["commentdate"];
	      	
	      	if($adflag == "mobile")
	      		$adflag = "iOS";
	      	else if($adflag == "mobile_ad")
	      		$adflag = "Android";
	      	else if($adflag == "mobile_kindle")
	      		$adflag = "Amazon";
			else
				$adflag = "Web";
	      	
	    	$sql = "SELECT userid,nickname FROM tbl_user WHERE useridx='$useridx'";
	    	$userinfo = $db_main->getarray($sql);
	    	
	    	$photourl = get_fb_pictureURL($userinfo["userid"],$client_accesstoken);
?>
				<tr>
<?
			if($check == "0" && $status == "2")
			{
?>
	            	<td class="tdc"><div><input type="checkbox" value="<?= $logidx ?>" class="chkbox"></div></td>
<?
			}
			else if($check == "0" && $status != "2")
			{
?>
					<td></td>
<?
			}
?>
		            <td class="tdc"><?= $adflag ?></td>
		            <td class="tdc"><?= $useridx ?></td>
		            <td class="tdc"><?= $userinfo["userid"] ?></td>
<?
			if($check == "0" || $check == "2")
			{
?>
	            	<td class="point_title"><a href="javascript:view_coin_increase_user_dtl(<?= $logidx ?>)"><span style="float:left;"><img src="<?= $photourl ?>" height="25" width="25" align="absmiddle" hspace="3"></span><?= $userinfo["nickname"] ?>&nbsp;&nbsp;</a></td>
<?
			}
			else if($check == "1")
			{
?>
					<td class="point_title"><a href="javascript:view_user_dtl(<?= $useridx ?>,'')"><span style="float:left;"><img src="<?= $photourl ?>" height="25" width="25" align="absmiddle" hspace="3"></span><?= $userinfo["nickname"] ?>&nbsp;&nbsp;</a><img src="/images/icon/facebook.png" height="20" width="20" align="absmiddle" style="cursor:pointer" onclick="event.cancelBubble=true;window.open('http://www.facebook.com/profile.php?id=<?= $userinfo["userid"] ?>')" /></td>
<?
			}
?>
		            <td class="tdc"><?= number_format($coin) ?></td>
		            <td class="tdc"><?= date("Y-m-d", strtotime($createdate)) ?></td>
		            <td class="tdc"><?= date("Y-m-d", strtotime($writedate)) ?></td>
		            <td class="tdc"><?= ($status == "0") ? "미확인" : (($status == "1") ? "확인" : "보류") ?></td>
		            <td class="tdc"><?= $comment ?></td>
		            <td class="tdc"><?= ($commentdate == "0000-00-00 00:00:00") ? "-" : date("Y-m-d", strtotime($commentdate)) ?></td>
		            <td class="tdc"><input type="button" class="btn_03" value="상세" onclick="view_user_dtl_new(<?= $useridx ?>,'')"></td>
		        </tr>
<?
    	}
    
    	$db_main->end();
?>
			</tbody>
		</table>
	</form>
	
	<br>
<?
	if($check == "0")
	{
?>
	<input type="button" class="btn_03" value="보류 이동" onclick="update_coin_increase_user_status()">
<?
	}
	
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
