<?
	$top_menu = "monitoring";
	
	$check = ($_GET["check"] == "") ? "1" : $_GET["check"];
	
	if($check == "1")
	{
		$sub_menu = "slot_gain_high_coin_user_list";
	}
	else if($check == "4")
	{
		$sub_menu = "slot_gain_high_coin_user_list_reserve";
	}
	else if($check == "3")
	{
		$sub_menu = "slot_gain_high_coin_user_list_check";
	}
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$page = ($_GET["page"] == "") ? "1" : $_GET["page"];
	$search_useridx = $_GET["useridx"];
	$search_start_writedate = $_GET["start_writedate"];
	$search_end_writedate = $_GET["end_writedate"];
	$search_order = $_GET["order"];
	$search_user_flag = $_GET["status"];
	$isearch = $_GET["issearch"];	
	
	if ($isearch == "")
	{
		$search_start_writedate  = date("Y-m-d", time() - 60 * 60 * 24 * 3);
		$search_end_writedate  = date("Y-m-d", time() - 60 * 60 * 24 * 1);		
	}
	
	$tail = " WHERE today BETWEEN '$search_start_writedate' AND '$search_end_writedate' ";
	
	if($search_useridx != "")
		$tail .= "AND useridx = $search_useridx ";
	
	$order_by = "";
	
	if($search_order == "" || $search_order == "1")
		$order_by = " ORDER BY today DESC ";
	else if($search_order == "2")
		$order_by = " ORDER BY useridx ASC ";
	else if($search_order == "3")
		$order_by = " ORDER BY sub_money DESC ";
	
	if($check == "1")
	{
		if($search_user_flag == "" || $search_user_flag == "0")
			$tail .= "AND  user_flag NOT IN (3, 4) ";
		else if($search_user_flag == "1")
			$tail .= " AND user_flag = 1 ";
		else if($search_user_flag == "2")
			$tail .= " AND user_flag = 2 ";
	}
	else if($check == "3")
		$tail .= "AND user_flag = 3 ";
	else if($check == "4")
		$tail .= "AND user_flag = 4 ";
	
	$listcount = "10";
	$pagename = "slot_gain_high_coin_user_list.php";
	$pagefield = "useridx=$search_useridx&check=$check&start_writedate=$search_start_writedate&end_writedate=$search_end_writedate&order=$search_order&status=$search_user_flag&issearch=$isearch";
	
	$db_main2 = new CDatabase_Main2();
	$db_analysis = new CDatabase_Analysis();
	
	$totalcount = $db_analysis->getvalue("SELECT COUNT(*) FROM tbl_slot_gain_high_coin_list $tail");
	
	$sql = "SELECT * FROM tbl_slot_gain_high_coin_list $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;
	
	$user_list = $db_analysis->gettotallist($sql);
	
	//Slot 정보
	$sql = "SELECT slottype, slotname FROM tbl_slot_list";
	$slottype_list = $db_main2->gettotallist($sql);
	
	$db_main2->end();
	$db_analysis->end();
	
	if ($totalcount < ($page-1) * $listcount && page != 1)
		$page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	    search_form.submit();
	}
	
	$(function() {
	    $("#start_writedate").datepicker({ });
	});
	
	$(function() {
	    $("#end_writedate").datepicker({ });
	});

	function selectAll(source) 
	{
		var holder = document.getElementById( "table_form" );
		var checkboxes = holder.getElementsByClassName( "chkbox" );
		
		for(var i=0; i<checkboxes.length; i++)
		{
			checkboxes[i].checked = source.checked;
		}	
	}

	var bindCheckboxes = ( 
			function() {
				var _last_selected = null;
				
				return function() {
					var holder = document.getElementById( "table_form" );

					if ( holder === null ) 
						return;
					
					var checkboxes = holder.getElementsByClassName( "chkbox" );
					var index = 0;
					
					for ( var i = 0; i < checkboxes.length; ++i ) 
					{
						if ( "checkbox" !== checkboxes[ i ].getAttribute( "type" ) ) 
							continue;
						
						( function( ix ) {
							checkboxes[ ix ].addEventListener( "click", 
									function( event ) {
										var checked = this.checked;
										
										if ( event.shiftKey && ix != _last_selected ) 
										{
											var start = Math.min( ix, _last_selected ), stop = Math.max( ix, _last_selected );
											
											for ( var j = start; j <= stop; ++j ) 
											{
												checkboxes[ j ].checked = checked;
											}

											_last_selected = null;
										} 
										else 
										{
											_last_selected = ix;
										}
									}, false );
		      			})( index );
		      			
		      			index++;
		    		}
					
		  		}
			}
	)();
	document.addEventListener( "DOMContentLoaded", function() { bindCheckboxes( "table_form" );}, false );

	function update_coin_increase_user_status() 
	{
		var holder = document.getElementById( "table_form" );
		var checkboxes = holder.getElementsByClassName( "chkbox" );
		var logidx_list = "";
		
		for(var i=0; i<checkboxes.length; i++)
		{
			if(checkboxes[i].checked == true)
			{
				if(logidx_list == "")
					logidx_list = checkboxes[i].value;
				else
					logidx_list += "," + checkboxes[i].value;
			}
		}

		var param = {};
		param.logidx_list = logidx_list;        
        
		WG_ajax_execute("monitoring/update_slot_high_coin_user_status", param, update_coin_increase_user_status_callback);
	}
	
	function update_coin_increase_user_status_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else        
        {
            alert("상태를 변경 했습니다.");
            window.location.href = window.location.href;
        }
    }
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 일간 슬롯 별 최대 코인 획득 유저 <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
	</div>
	<!-- //title_warp -->
	
	<form name="search_form" id="search_form" method="get" onsubmit="return false" enctype="application/x-www-form-urlencoded" action="<?=$pagename ?>">
		<input type=hidden name=issearch value="1" />
		<div class="detail_search_wrap">	
			<input type="hidden" name="check" id="check" value=<?= $check ?> />
    		<input type="hidden" name="reserve_check" id="reserve_check" value="0" />
    		<input type="hidden" name="confirm_check" id="confirm_check" value="0" />		
			<span class="search_lbl">useridx</span>
			<input type="text" class="search_text" id="useridx" name="useridx" style="width:120px" value="<?= encode_html_attribute($search_useridx) ?>" onkeypress="search_press(event)" />&nbsp;
			<span class="search_lbl ml20">조회일</span>
			<input type="input" class="search_text" id="start_writedate" name="start_writedate" style="width:65px" value="<?= $search_start_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" /> -
			<input type="input" class="search_text" id="end_writedate" name="end_writedate" style="width:65px" value="<?= $search_end_writedate ?>" onfocus="manual_date_focus(this)" onblur="manual_date_blur(this)" onkeypress="search_press(event)" />
			<span class="search_lbl ml20">정렬 방식</span>
			<select name="order" id="order">	
				<option value="1" <?= ($search_order == "1") ? "selected" : "" ?>>날짜</option> 			
				<option value="2" <?= ($search_order == "2") ? "selected" : "" ?>>useridx</option>
				<option value="3" <?= ($search_order == "3") ? "selected" : "" ?>>총 획득 코인</option>				
			</select>
<?
	if($check == "1")
	{
?>

			<span class="search_lbl ml20">상태</span>
			<select name="status" id="status">
				<option value="0" <?= ($search_user_flag == "0") ? "selected" : "" ?>>전체</option> 		
				<option value="1" <?= ($search_user_flag == "1") ? "selected" : "" ?>>미확인</option> 			
				<option value="2" <?= ($search_user_flag == "2") ? "selected" : "" ?>>보류</option>				
			</select>
<?
	}
?>
			<div class="floatr"><input type="button" class="btn_search" value="검색" onclick="search()" /></div>
		</div>
	</form>
	
	<form id="table_form">
	<table class="tbl_list_basic1" style="width:1300px;">
		<colgroup>
<?
			if($check == "1")
			{
?>
	            	<col width="30">
<?
			}
?>
			<col width="70">
			<col width="50">
			<col width="">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="50">
			<col width="50">
			<col width="280">
			<col width="60">
		</colgroup>
		<thead>
            <tr>
<?
			if($check == "1")
			{
?>
      	    		<th><input type="checkbox" class="selectAll" onClick="selectAll(this)"></th>
<?
			}
?>
            	<th>날짜</th>
            	<th>useridx</th>
                <th>이름</th>
                <th>슬롯</th>
                <th>총 머니인</th>
                <th>총 머니 아웃</th>
                <th>총 획득 코인</th>
                <th>winrate</th>
                <th>상태</th>
                <th>Comment</th>
                <th>변경</th>
            </tr>
		</thead>
<?
		$db_main = new CDatabase_Main();
	
    	for($i=0; $i<sizeof($user_list); $i++)
    	{
    		$logidx = $user_list[$i]["logidx"];
    		$today = $user_list[$i]["today"];
    		$useridx = $user_list[$i]["useridx"];
    		$slottype = $user_list[$i]["slottype"];
    		$sum_moneyin = $user_list[$i]["sum_moneyin"];
    		$sum_moneyout = $user_list[$i]["sum_moneyout"];
    		$sub_money = $user_list[$i]["sub_money"];
    		$winrate = $user_list[$i]["winrate"] * 100;
    		$user_flag = $user_list[$i]["user_flag"];
    		$comment = $user_list[$i]["comment"];
    		
    		$sql = "SELECT userid,nickname FROM tbl_user WHERE useridx='$useridx'";
    		$userinfo = $db_main->getarray($sql);
    		
    		$photourl = get_fb_pictureURL($userinfo["userid"],$client_accesstoken);
    		
    		for($j=0; $j<sizeof($slottype_list); $j++)
    		{
	    		if($slottype_list[$j]["slottype"] == $slottype)
	    		{
	    			$slot_name = $slottype_list[$j]["slotname"];
						break;
	    		}
	    		else
	    		{
	    			$slot_name = "Unkown";
	    		}
    		}
    		
    		if($user_flag == 1)
    			$user_flag_comment = "미확인";
    		else if($user_flag == 2 || $user_flag == 4)
    			$user_flag_comment = "보류";
    		else if($user_flag == 3)
    			$user_flag_comment = "확인";
?>
			<tr>
<?
			if($check == "1" && $user_flag == "2")
			{
?>
            		<td class="tdc"><div><input type="checkbox" value="<?= $logidx ?>" class="chkbox"></div></td>
<?
			}
			else if($check == "1" && $user_flag != "2")
			{
?>
					<td></td>
<?
			}
?>
				<td class="tdc"><?= $today ?></td>
				<td class="tdc"><?= $useridx ?></td>
			    <td class="point_title"><a href="javascript:view_user_dtl(<?= $useridx ?>,'')"><span style="float:left;"><img src="<?= $photourl ?>" height="25" width="25" align="absmiddle" hspace="3"></span><?= $userinfo["nickname"] ?>&nbsp;&nbsp;</a><img src="/images/icon/facebook.png" height="20" width="20" align="absmiddle" style="cursor:pointer" onclick="event.cancelBubble=true;window.open('http://www.facebook.com/profile.php?id=<?= $userinfo["userid"] ?>')" /></td>
			    <td class="tdc"><?= $slot_name ?></td>
			    <td class="tdc"><?= number_format($sum_moneyin) ?></td>
			    <td class="tdc"><?= number_format($sum_moneyout) ?></td>
			    <td class="tdc"><?= number_format($sub_money) ?></td>
			    <td class="tdc"><?= $winrate ?>%</td>
			    <td class="tdc"><?= $user_flag_comment ?></td>
			    <td class="tdc"><?= $comment ?></td>
			    <td class="tdc"><input type="button" class="btn_03" value="변경" onclick="view_gain_high_coin_user_dtl('<?= $today ?>',<?= $useridx ?>,<?=$slottype?>)"></td>
		    </tr>

<?
    	}
    	
    	$db_main->end();
?>
			</tbody>
		</table>
	</form>
	
	<br>
	
<?
	if($check == "1")
	{
?>
		<input type="button" class="btn_03" value="보류 이동" onclick="update_coin_increase_user_status()">
<?
	}
	
    include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>     	
        	
        	
        	