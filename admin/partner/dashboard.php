<?
    $top_menu = "dashboard";
    $sub_menu = "detail";
    
    include($_SERVER["DOCUMENT_ROOT"]."/partner/common/top_frame.inc.php");
    
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    $search_payday = $_GET["payday"];
	$isearch = $_GET["issearch"];
		
	if ($isearch == "")
	{
		$search_end_createdate  = date("Y-m-d", time() - 60 * 60);
		$search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 6);
	}
	
	function get_stat($summarylist, $adflag, $today, $property)
	{
		$stat = 0;
		
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if($partner_userid == "maudau")
			{
				if ($summarylist[$i]["day"] == $today && ($summarylist[$i]["adflag"] == $adflag || $adflag == "maudau" && $summarylist[$i]["adflag"] != "maudau500" && $summarylist[$i]["adflag"] != "maudau_event" && substr($summarylist[$i]["adflag"], 0, 6) == "maudau"))
				{
					$stat += $summarylist[$i][$property];
				}
			}
			else
			{
				if ($summarylist[$i]["day"] == $today && ($summarylist[$i]["adflag"] == $adflag))
				{
					$stat += $summarylist[$i][$property];
				}
			}
		}	
		
		return $stat;
	}
?>
<link type="text/css" href="/js/themes/base/jquery.ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
$(function() {
    $("#start_createdate").datepicker({ });
});

$(function() {
    $("#end_createdate").datepicker({ });
});
</script>

        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
        <form name="search_form" id="search_form"  method="get" action="dashboard.php">
            <input type=hidden name=issearch value="1">
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title">Partner Dashboard &gt; Traffic Summary</div>
                <div class="search_box">
                    Date <input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
                    <input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
                    &nbsp;&nbsp;&nbsp;&nbsp;Purchased credits : within <input type="text" class="search_text" id="payday" name="payday" style="width:60px" value="<?= $search_payday ?>" onkeypress="search_press(event); return checknum();" /> days after installation
                     <input type="button" class="btn_search" value="Search" onclick="document.search_form.submit()" />
                </div>
            </div>
            <!-- //title_warp -->
            
            <div class="search_result">
                Date : <span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span>
            </div>
            
            <div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
                <col width="">
                <col width=""> 
                <col width=""> 
            </colgroup>
            <thead>
            <tr>
                <th>Date</th>
                <th class="tdr">Channel</th>
                <th class="tdr">Installations</th>
                <th class="tdr">Bounced users</th>
                <th class="tdr">Returned<br>inactive users<br>for 2 weeks</th>
                <th class="tdr">Total consumers<br>(incl. FB Earn)</th>
                <th class="tdr">Package consumers</th>
                <th class="tdr">Total credits<br>purchased<br>(per installation)</th>
                <th class="tdr">Total package credits<br>purchased<br>(per installation)</th>
            </tr>
            </thead>
            <tbody>
<?
    $db_main = new CDatabase_Main();
    $db_main2 = new CDatabase_Main2();

    $now = time();
    
    if ($search_end_createdate != "")
    {
    	$now = strtotime($search_end_createdate);
    }
    else
    {
    	$search_end_createdate = date('Y-m-d');
    }
    
    if ($search_start_createdate == "")
    {
    	$search_start_createdate = '2012-09-03';
    }
    
	$sql = "SELECT adflag,DATE_FORMAT(createdate,'%Y-%m-%d') AS day,COUNT(*) AS totalcount,".		
			"IFNULL(SUM(CASE WHEN NOT EXISTS (SELECT * FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) THEN 1 ELSE 0 END) ,0) AS unplaycount, ";
		
	if ($search_payday != "")
	{
		$sql .= "IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0) AS paycount,".
			"IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0) AS packagecount,".
			"IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) AS totalcredit,".
			"IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) AS packagecredit ";
	}
	else
	{
		$sql .= "IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0) AS paycount,".
			"IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0) AS packagecount,".
			"IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) AS totalcredit,".
			"IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) AS packagecredit ";
	}
		
	if($partner_userid == "maudau")
	{
		$sql .= " FROM tbl_user_ext WHERE createdate>='$search_start_createdate' AND createdate<='$search_end_createdate 23:59:59' AND adflag LIKE 'maudau%') GROUP BY adflag,DATE_FORMAT(createdate,'%Y-%m-%d')";
	}
	else
	{
		$sql .= " FROM tbl_user_ext WHERE createdate>='$search_start_createdate' AND createdate<='$search_end_createdate 23:59:59' AND adflag = '$partner_userid' GROUP BY adflag,DATE_FORMAT(createdate,'%Y-%m-%d')";
	}
	
	write_log($sql);
	
	$summarylist = $db_main->gettotallist($sql);
	
	$sum_totalcount = array();
	$sum_unplaycount = array();
	$sum_returncount = array();
	$sum_paycount = array();
	$sum_packagecount = array();
	$sum_totalcredit = array();
	$sum_packagecredit = array();
	
    while (true)
    {
    	$today = date("Y-m-d", $now);
    	$tomorrow = date("Y-m-d", $now + 24 * 60 * 60);
    	
    	if ($tomorrow < $search_start_createdate)
    		break;

    	$adflag = $partner_userid;
    	
		if ($today < $search_start_createdate)
		{
			$totalcount = $sum_totalcount[$adflag];
			$unplaycount = $sum_unplaycount[$adflag];
			$returncount = $sum_returncount[$adflag];
			$paycount = $sum_paycount[$adflag];
			$packagecount = $sum_packagecount[$adflag];
			$totalcredit = $sum_totalcredit[$adflag];
			$packagecredit = $sum_packagecredit[$adflag];
		}
		else
		{
			if ($adflag == "maudau")
				$returncount = $db_main2->getvalue("SELECT IFNULL(SUM(usercount),0) FROM tbl_user_retention WHERE adflag LIKE 'maudau%' AND adflag<>'maudau_event' AND adflag<>'maudau500' AND retentiondate='$today'");
			else
				$returncount = $db_main2->getvalue("SELECT IFNULL(SUM(usercount),0) FROM tbl_user_retention WHERE adflag = '$adflag' AND retentiondate='$today'");
				
			$totalcount = get_stat($summarylist, $adflag, $today, "totalcount");
			$unplaycount = get_stat($summarylist, $adflag, $today, "unplaycount");
			$paycount = get_stat($summarylist, $adflag, $today, "paycount");
			$packagecount = get_stat($summarylist, $adflag, $today, "packagecount");
			$totalcredit = get_stat($summarylist, $adflag, $today, "totalcredit");
			$packagecredit = get_stat($summarylist, $adflag, $today, "packagecredit");
	    			
			if ($sum_totalcount[$adflag] == "")
				$sum_totalcount[$adflag] = 0;
	    			
			if ($sum_unplaycount[$adflag] == "")
				$sum_unplaycount[$adflag] = 0;
	    			
			if ($sum_returncount[$adflag] == "")
				$sum_returncount[$adflag] = 0;
	    			
			if ($sum_paycount[$adflag] == "")
				$sum_paycount[$adflag] = 0;
	    			
			if ($sum_packagecount[$adflag] == "")
				$sum_packagecount[$adflag] = 0;
	    			
			if ($sum_totalcredit[$adflag] == "")
				$sum_totalcredit[$adflag] = 0;
						    			
			if ($sum_packagecredit[$adflag] == "")
				$sum_packagecredit[$adflag] = 0;
									
	    	$sum_totalcount[$adflag] += $totalcount;
	    	$sum_unplaycount[$adflag] += $unplaycount;
	    	$sum_returncount[$adflag] += $returncount;
			$sum_paycount[$adflag] += $paycount;
			$sum_packagecount[$adflag] += $packagecount;
			$sum_totalcredit[$adflag] += $totalcredit;
			$sum_packagecredit[$adflag] += $packagecredit;
		}
       		
    	if ($totalcount == 0)
    	{
    		$unplayratio = 0;
    		$payratio = 0;
    		$packageratio = 0;
    		$averagecredit = 0;
			$averagepackagecredit = 0;
    	}
    	else
    	{
    		$unplayratio = round($unplaycount * 10000 / $totalcount) / 100;
    		$payratio = round($paycount * 10000 / $totalcount) / 100;
    		$packageratio = round($packagecount * 10000 / $totalcount) / 100;
    		$averagecredit = round($totalcredit * 1000 / $totalcount) / 1000;
    		$averagepackagecredit = round($packagecredit * 1000 / $totalcount) / 1000;
    	}
?>
			<tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
		if ($i == 0)
		{
?>                        
				<td class="tdc point_title" valign="center"><?= ($today < $search_start_createdate) ? "<b>Total</b>" : $today ?></td>
<?
		}
?>                            
				<td class="tdr point"><?= str_replace("%", "", $adflag) ?></td>
				<td class="tdr point"><?= number_format($totalcount) ?></td>
				<td class="tdr point"><?= number_format($unplaycount) ?> (<?= $unplayratio ?> %)</td>
				<td class="tdr point"><?= number_format($returncount) ?></td>
				<td class="tdr point"><?= number_format($paycount) ?> (<?= $payratio ?> %)</td>
				<td class="tdr point"><?= number_format($packagecount) ?> (<?= $packageratio ?> %)</td>
				<td class="tdr point"><?= number_format($totalcredit) ?> (<?= $averagecredit ?>)</td>
				<td class="tdr point"><?= number_format($packagecredit) ?> (<?= $averagepackagecredit ?>)</td>
			</tr>
<?
    	$now -= 60 * 60 * 24;
    }

	$db_main->end();
	$db_main2->end();
?>    
			</tbody>
		</table>
	</div>
	
<?
	if ($partner_userid == "adotomi")
	{
?>
        <div style="width:100%;float:left;margin:20px 0 0 20px;text-align: right;">
            <a href="#" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">ifcontext details</a> <input type="button" class="btn_search" value="Excel Download" onclick="window.location.href='adotomi/ifcontext_excel.php?sdate=<?=$search_start_createdate?>&edate=<?=$search_end_createdate?>'" />
            <br><br>
		</div>
<?
	}
	else if ($partner_userid == "socialclicks")
	{
?>        
        <div style="width:100%;float:left;margin:20px 0 0 20px;text-align: right;">
            <a href="#" style="font:12px;color:#000;font-weight:bold;cursor:ponter;">subid details</a> <input type="button" class="btn_search" value="Excel Download" onclick="window.location.href='socialclicks/ifcontext_excel.php?sdate=<?=$search_start_createdate?>&edate=<?=$search_end_createdate?>'" />
            <br><br>
		</div>    
<?
	}
?>
        </form>
</div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/partner/common/bottom_frame.inc.php");
?>