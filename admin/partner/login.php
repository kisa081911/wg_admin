<?
	$saved_userid = $_COOKIE["saved_userid"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="height:100%">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=yes, initial-scale=1.0, minimum-scale=1.0,maximum-scale=1.0, width=device-width" />
<title>DOUBLE U TAKE5 PARTNER</title>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/common_util.js"></script>
<script>
	function show_pwd()
	{
		document.getElementById("password_cover").style.display = "none";
		document.getElementById("password").style.display = "";	
		document.getElementById("password").focus();
	}

	function login_press(e)
	{
		if (((e.which) ? e.which : e.keyCode) == 13)
		{
			form_submit();
			return false;
		}
	}

	function form_submit()
	{
		var login_form = document.login_form;

		if (login_form.adminid.value == "" || login_form.adminid.value == "ID")
		{
			alert("Please enter your id.");
			login_form.adminid.focus();
			return;
		}

		if (login_form.password.value == "")
		{
			alert("Please enter your password.");

			if (login_form.password.style.display != "none")
				login_form.password.focus();
			else
				login_form.password_cover.focus();
			
			return;
		}

		login_form.submit();
	}
</script>
</head>
<body class="login_body" style="height:100%;overflow-y:hidden;">
<form id="login_form" name="login_form" method="post" action="login_check.php" onsubmit="return false" style="height:100%">
	<table class="login_table_1">
		<tr>
			<td align="center" style="height:100%">
				<table class="login_table_2">
					<tr>
						<td align="center" style="padding-bottom:100px;">
							<div class="login_wrap">
								<table>
							        <colgroup>
							        	<col width="" />
							        	<col width="" />
							        </colgroup>
							        <tbody>
								        <tr>
								        	<td colspan="2" style="text-align:center"><img src="../images/login_logo.png" /></td>
								        </tr>
								        <tr>
								        	<td colspan="2">
								        		<div style="padding:30px 0 5px 0; text-align:center;">
								        			<input class="txt_loginid" tabindex="1" type="text" name="adminid" maxlength="50" onkeypress="login_press(event)" value="<?= ($saved_userid != "") ? $saved_userid : "ID" ?>" onfocus="<?= ($saved_userid != "") ? "" : "if (this.value == 'ID') { this.value=''; }" ?>" />
							        			</div>
							        		</td>						        	
								        </tr>
								        <tr>
								        	<td colspan="2">
								        		<div style="padding:5px 0 5px 0; text-align:center;">
								        			<input class="txt_loginpw" tabindex=2 type="text" name="password_cover" id="password_cover" value="Password" onfocus="show_pwd()" />
								        			<input class="txt_loginpw" type="password" name="password" id="password" maxlength="100" onkeypress="login_press(event)" style="display:none;"/>
								        		</div>
							        		</td>	        	
								        </tr>
								        <tr>
								            <td style="padding-top:10px;">
								                <div style="padding:0 0 0 98px"><input type="checkbox" name="saveid" id="saveid" value="1" style="border:0px;padding-top:2px;" <?= ($saved_userid != "") ? "checked" : "" ?> /> <span style="font-size:12px;">Save ID</span></div>
								            </td>
								            <td style="text-align:right;padding:10px 98px 0 0 ">
								            	<input type="button" class="btn_login" value="Login" onclick="form_submit()" />
								            </td>
								        </tr>
							        </tbody>
						        </table>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
</body>
</html>
