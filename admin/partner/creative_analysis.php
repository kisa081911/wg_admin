<?
    $top_menu = "dashboard";
    $sub_menu = "creative";
    
    include($_SERVER["DOCUMENT_ROOT"]."/partner/common/top_frame.inc.php");
    
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    $search_payday = $_GET["payday"];
    $isearch = $_GET["issearch"];
    
    if ($isearch == "")
    {
    	$search_end_createdate  = date("Y-m-d", time() - 60 * 60);
    	$search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 6);
    }
    
    function get_stat($summarylist, $fbsource, $today, $property)
    {
    	$stat = 0;
    
    	for ($i=0; $i<sizeof($summarylist); $i++)
    	{
    		if ($summarylist[$i]["day"] == $today && $summarylist[$i]["fbsource"] == $fbsource)
    		{
    			$stat += $summarylist[$i][$property];
    		}
    	}
    
    	return $stat;
    }
    
    $now = time();
    
    if ($search_end_createdate != "")
    {
    	$now = strtotime($search_end_createdate);
    }
    else
    {
    	$search_end_createdate = date('Y-m-d');
    }
    
    $db = new CDatabase();
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="/js/ui/ui.core.js"></script>
<script type="text/javascript" src="/js/ui/ui.datepicker.js"></script>
<script type="text/javascript">
	$(function() {
	    $("#start_createdate").datepicker({ });
	});
	
	$(function() {
	    $("#end_createdate").datepicker({ });
	});
	
	function search_press(e)
	{
	    if (((e.which) ? e.which : e.keyCode) == 13)
	    {
	        search();
	    }
	}
	
	function search()
	{
	    var search_form = document.search_form;
	
	    search_form.submit();
	}
</script>
	<div class="contents_wrap">
		<form name="search_form" id="search_form"  method="get" action="analysis.php">
            <input type=hidden name=issearch value="1">
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title">Partner Dashboard &gt; Creative Analysis</div>
                <div class="search_box">
                    Date of creation <input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
					<input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
                    &nbsp;&nbsp;&nbsp;&nbsp;Purchased credits : within <input type="text" class="search_text" id="payday" name="payday" style="width:60px" value="<?= $search_payday ?>" onkeypress="search_press(event); return checknum();" /> days after installation
					<input type="button" class="btn_search" value="검색" onclick="document.search_form.submit()" />
				</div>
			</div>
		</form>
		<!-- //title_warp -->

		<div class="search_result">
			Date : <span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span>
		</div>
	
		<div id="tab_content_1">
			<table class="tbl_list_basic1">
				<colgroup>
					<col width="">
					<col width="">
	                <col width="">
	                <col width=""> 
	                <col width="">
	            </colgroup>
	            <thead>
	            	<tr>
	               		<th>Creative</th>
						<th>Installations</th>
						<th>Bounced users</th>
						<th>Total consumers<br/>(incl. FB Earn)</th>
						<th>Total credits purchased<br/>(per installation)</th>
					</tr>
				</thead>
				<tbody>
	<?
				$sql = "SELECT * FROM (SELECT fbsource,COUNT(*) AS totalcount,".
						"IFNULL(SUM(CASE WHEN (SELECT IFNULL(SUM(money_in),0) FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx)=0 THEN 1 ELSE 0 END),0) AS unplaycount,";
	
				if ($search_payday != "")
				{
					$sql .= "IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0) AS paycount,".
							"IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) AS totalcredit";
				}
				else
				{
					$sql .= "IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0) AS paycount,".
							"IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) AS totalcredit";
				}
	
				$sql .= " FROM tbl_user_ext WHERE createdate>='$search_start_createdate' AND createdate<='$search_end_createdate 23:59:59' AND adflag='$partner_userid' GROUP BY fbsource) AS TEMP ORDER BY totalcount DESC";
				$summarylist = $db->gettotallist($sql);
				
				for ($i=0; $i<sizeof($summarylist); $i++)
				{
					$fbsource = $summarylist[$i]["fbsource"];
		  			$totalcount = $summarylist[$i]["totalcount"];
					$unplaycount = $summarylist[$i]["unplaycount"];
					$paycount = $summarylist[$i]["paycount"];
					$totalcredit = $summarylist[$i]["totalcredit"];
				
					if ($totalcount == 0)
					{
						$unplayratio = 0;
						$payratio = 0;
					}
					else
					{
						$unplayratio = round($unplaycount * 10000 / $totalcount) / 100;
						$payratio = round($paycount * 10000 / $totalcount) / 100;
					}
	?>
					<tr onmouseover="className='tr_over'" onmouseout="className=''">
						<td class="point"><?= $fbsource ?></td>
						<td class="tdr point"><?= number_format($totalcount) ?></td>
						<td class="tdr point"><?= number_format($unplaycount) ?> (<?= $unplayratio ?> %)</td>
						<td class="tdr point"><?= number_format($paycount) ?> (<?= $payratio ?> %)</td>
						<td class="tdr point"><?= number_format($totalcredit) ?></td>
					</tr>
	<?
				}
	?>
				</tbody>
			</table>
		</div>
	</div>
<?
	$db->end();

    include($_SERVER["DOCUMENT_ROOT"]."/partner/common/bottom_frame.inc.php");
?>