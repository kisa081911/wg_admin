<?
	include($_SERVER['DOCUMENT_ROOT']."/partner/common/common_include.inc.php");
	
	check_login();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>DOUBLE U TAKE5 PARTNER</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=9, IE=8, IE=7" />
<meta name="viewport" content="user-scalable=yes, initial-scale=1.0, minimum-scale=0.25,maximum-scale=1.0, width=device-width" />
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script>
<script type="text/javascript" >
</script>
</head>

<body class="bg_body">
<!-- FULL WRAP -->
<div class="full_wrap" style="position:relative;">
<div id="file_progress_info" style="position:absolute;z-index:500;left:350px;top:300px;" ></div>
	<!-- TOP -->
	<div class="top_area">
	
		<!--LOGO-->
		<div class="logo">
			<a href="javascript:window.location.href='/partner/dashboard.php'"><img src="/images/logo.png" /></a>
		</div>	
		<!-- // LOGO-->
	
		<!-- RIGHT LINK -->
		<div class="top_right_link_area">
			<div class="user_info">
				<?= $partner_partner ?> . <a href="/partner/logout.php" title="LOGOUT">LOGOUT</a>
			</div>
			<div class="user_info">
			</div>
	  	</div>
		<!-- //RIGHT LINK -->
  	
  	</div>
  	<!-- //TOP -->
	  
    <!-- GNB -->
    <div class="gnb_wrap clear">
    	<ul class="gnb">
        	<li class='select'><a href="dashboard.php" hidefocus><span>Dash Board</span></a></li>
        </ul>
    </div>
    <!-- //GNB -->

	<!-- CONTENTS WRAP -->
	<div class="gnb_title_wrap">
	    <div class="gnb_title">
			Partner Dashboard
	   </div>
	</div>
	
    <div class="main_wrap">
    	<div class="lnb_wrap">
    		<div class="lnb<?= ($sub_menu == "detail") ? "_select" : "" ?>" onclick="go_page('/partner/dashboard.php')">Traffic Summary</div>
<?
	if ($partner_partner == "adotomi")
	{
?>
    		<div class="lnb<?= ($sub_menu == "performance" && $_GET["landing"] != "1") ? "_select" : "" ?>" onclick="go_page('/partner/performance.php?landing=0')"> - No landing</div>
    		<div class="lnb<?= ($sub_menu == "performance" && $_GET["landing"] == "1") ? "_select" : "" ?>" onclick="go_page('/partner/performance.php?landing=1')"> - Using landing</div>
<?		
	}
?>
    		<div class="lnb<?= ($sub_menu == "analysis") ? "_select" : "" ?>" onclick="go_page('/partner/analysis.php')">Traffic Analysis</div>
    		<div class="lnb<?= ($sub_menu == "creative") ? "_select" : "" ?>" onclick="go_page('/partner/creative_analysis.php')">Creative Analysis</div>
    	</div>
