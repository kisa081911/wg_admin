<?
	include_once($_SERVER['DOCUMENT_ROOT']."/common/config.inc.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/common/common_util.inc.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/common/dbconnect/db_util_main.inc.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/common/dbconnect/db_util_main2.inc.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/common/dbconnect/db_util_analysis.inc.php");
	
	set_time_limit(60 * 60);
	
	session_start();
	
	$partner_userid = $_SESSION["partner_userid"];
	$partner_partner = $_SESSION["partner_partner"];
		
	function check_login()
	{
		global $partner_userid, $partner_partner;
		
		if ($partner_userid == "" || $partner_partner == "")
		{
			error_go("Login is required.", "/partner/login.php");			
		}
	}  
    
    function check_login_layer()
    {
        global $partner_userid, $partner_partner;
        
        if ($partner_userid == "" || $partner_partner == "")
        {
            error_close_layer("Login is required.", "/partner/login.php");         
        }
    }
?>