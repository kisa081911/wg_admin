<?
    $top_menu = "dashboard";
    $sub_menu = "analysis";
    
    include($_SERVER["DOCUMENT_ROOT"]."/partner/common/top_frame.inc.php");
    
    $search_adflag = $partner_userid;
    $search_start_createdate = $_GET["start_createdate"];
    $search_end_createdate = $_GET["end_createdate"];
    $search_payday = $_GET["payday"];
	$isearch = $_GET["issearch"];
		
	if ($isearch == "")
	{
		$search_end_createdate  = date("Y-m-d", time() - 60 * 60);
		$search_start_createdate  = date("Y-m-d", time() - 60 * 60 * 24 * 6);
	}
	
	function get_stat($summarylist, $sex, $country, $today, $property)
	{
		$stat = 0;
		
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if ($summarylist[$i]["day"] == $today || $today == "")
			{
				if ($summarylist[$i]["sex"] == $sex || $sex == "")
				{
					if ($summarylist[$i]["email"] == $country || $country == "" || $country == "unknown" && $summarylist[$i]["email"] == "")
					{
						$stat += $summarylist[$i][$property];
					}
				}
			}
		}	
		
		return $stat;
	}
	
	function get_stat2($summarylist, $sex, $startage, $endage, $property)
	{
		$stat = 0;
		
		for ($i=0; $i<sizeof($summarylist); $i++)
		{
			if ($summarylist[$i]["sex"] == $sex || $sex == "")
			{
				if ((int)$summarylist[$i]["birthyear"] <= date('Y') - $startage && (int)$summarylist[$i]["birthyear"] >= date('Y') - $endage)
				{
					$stat += $summarylist[$i][$property];
				}
			}
		}	
		
		return $stat;
	}	
?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<link type="text/css" href="/js/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="/js/ui/ui.core.js"></script>
<script type="text/javascript" src="/js/ui/ui.datepicker.js"></script>
<script type="text/javascript">
$(function() {
    $("#start_createdate").datepicker({ });
});

$(function() {
    $("#end_createdate").datepicker({ });
});
</script>

        <!-- CONTENTS WRAP -->
        <div class="contents_wrap">
        
        <form name="search_form" id="search_form"  method="get" action="analysis.php">
            <input type=hidden name=issearch value="1">
            <!-- title_warp -->
            <div class="title_wrap">
                <div class="title">Partner Dashboard &gt; Traffic Analysis</div>
                <div class="search_box">
                    Date of creation <input type="text" class="search_text" id="start_createdate" name="start_createdate" value="<?= $search_start_createdate ?>" maxlength="10" style="width:65px"  onkeypress="search_press(event)" /> ~
                    <input type="text" class="search_text" id="end_createdate" name="end_createdate" value="<?= $search_end_createdate ?>" style="width:65px" maxlength="10"  onkeypress="search_press(event)" />
                    &nbsp;&nbsp;&nbsp;&nbsp;Purchased credits : within <input type="text" class="search_text" id="payday" name="payday" style="width:60px" value="<?= $search_payday ?>" onkeypress="search_press(event); return checknum();" /> days after installation
                     <input type="button" class="btn_search" value="Search" onclick="document.search_form.submit()" />
                </div>
            </div>
            <!-- //title_warp -->
            
<?
	if ($search_adflag != "")
	{
?>		
            <div class="search_result">
                Date : <span><?= $search_start_createdate ?></span> ~ <span><?= $search_end_createdate ?></span>
            </div>
			<div class="search_result">
				1. Traffic analysis by date
			</div>
            <div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
                <col width="">
                <col width=""> 
                <col width=""> 
            </colgroup>
            <thead>
            <tr>
                <th>Date</th>
                <th class="tdr">Installations</th>
                <th class="tdr">Bounced users</th>
                <th class="tdr">Returned<br>inactive users<br>for 2 weeks</th>
                <th class="tdr">Total consumers<br>(incl. FB Earn)</th>
                <th class="tdr">Package consumers</th>
                <th class="tdr">Total credits<br>purchased<br>(per installation)</th>
                <th class="tdr">Total package credits<br>purchased<br>(per installation)</th>
            </tr>
            </thead>
            <tbody>
<?
    $db = new CDatabase();
    $db_analysis = new CDatabase_Analysis();
    $db4 = new CDatabase4();

    $now = time();
    
    if ($search_end_createdate != "")
    {
    	$now = strtotime($search_end_createdate);
    }
    else
    {
    	$search_end_createdate = date('Y-m-d');
    }
    
    if ($search_start_createdate == "")
    {
    	$search_start_createdate = '2012-09-03';
    }
    
	if ($search_adflag == "viral")
		$adflag = "";
	else if ($search_adflag == "maudau")
		$adflag = "maudau%' AND adflag<>'maudau_event' AND adflag<>'maudau500";
	else if ($search_adflag == "all")
		$adflag = "%";
	else
		$adflag = $search_adflag;
		
	$sql = "SELECT sex,email,DATE_FORMAT(tbl_user_ext.createdate,'%Y-%m-%d') AS day,COUNT(*) AS totalcount,".
		"IFNULL(SUM(CASE WHEN (SELECT IFNULL(SUM(money_in),0) FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx)=0 THEN 1 ELSE 0 END),0) AS unplaycount,";
		
	if ($search_payday != "")
	{
		$sql .= "IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0) AS paycount,".
			"IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND paycategory='facebook' AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0) AS packagecount,".
			"IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) AS totalcredit,".
			"IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND paycategory='facebook' AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) AS packagecredit ";
	}
	else
	{
		$sql .= "IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0) AS paycount,".
			"IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND paycategory='facebook') THEN 1 ELSE 0 END),0) AS packagecount,".
			"IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) AS totalcredit,".
			"IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND paycategory='facebook')),0) AS packagecredit ";
	}
		
	$sql .= " FROM tbl_user_ext JOIN tbl_user ON tbl_user_ext.useridx=tbl_user.useridx WHERE tbl_user_ext.createdate>='$search_start_createdate' AND tbl_user_ext.createdate<='$search_end_createdate 23:59:59' AND adflag LIKE '$adflag' GROUP BY DATE_FORMAT(tbl_user_ext.createdate,'%Y-%m-%d'),sex,email";
	
	$summarylist = $db->gettotallist($sql);
	
    while (true)
    {
    	$today = date("Y-m-d", $now);
    	$tomorrow = date("Y-m-d", $now + 24 * 60 * 60);
    	
    	if ($today < $search_start_createdate)
    		break;
    		
		$returncount = $db4->getvalue("SELECT IFNULL(SUM(usercount),0) FROM tbl_user_retention WHERE adflag LIKE '$adflag' AND retentiondate='$today'");
		$totalcount = get_stat($summarylist, "", "", $today, "totalcount");
		$unplaycount = get_stat($summarylist, "", "", $today, "unplaycount");
		$paycount = get_stat($summarylist, "", "", $today, "paycount");
		$packagecount = get_stat($summarylist, "", "", $today, "packagecount");
		$totalcredit = get_stat($summarylist, "", "", $today, "totalcredit");
		$packagecredit = get_stat($summarylist, "", "", $today, "packagecredit");
   		
		if ($totalcount == 0)
		{
			$unplayratio = 0;
			$payratio = 0;
			$packageratio = 0;
			$averagecredit = 0;
			$averagepackagecredit = 0;
		}
		else
		{
			$unplayratio = round($unplaycount * 10000 / $totalcount) / 100;
			$payratio = round($paycount * 10000 / $totalcount) / 100;
			$packageratio = round($packagecount * 10000 / $totalcount) / 100;
			$averagecredit = round($totalcredit * 1000 / $totalcount) / 1000;
			$averagepackagecredit = round($packagecredit * 1000 / $totalcount) / 1000;
		}
?>
                <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
                    <td class="tdc point_title" valign="center"><?= $today ?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>
                    <td class="tdr point"><?= number_format($unplaycount) ?> (<?= $unplayratio ?> %)</td>
                    <td class="tdr point"><?= number_format($returncount) ?></td>
                    <td class="tdr point"><?= number_format($paycount) ?> (<?= $payratio ?> %)</td>
                    <td class="tdr point"><?= number_format($packagecount) ?> (<?= $packageratio ?> %)</td>
                    <td class="tdr point"><?= number_format($totalcredit) ?> (<?= $averagecredit ?>)</td>
                    <td class="tdr point"><?= number_format($packagecredit) ?> (<?= $averagepackagecredit ?>)</td>
                </tr>
<?
    	$now -= 60 * 60 * 24;
    }
?>    
            </tbody>
            </table>
        </div>
        <br><br>
			<div class="search_result">
				2. Traffic analysis by country and sex
			</div>
            <div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
                <col width=""> 
                <col width=""> 
            </colgroup>
            <thead>
            <tr>
                <th>Country</th>
                <th>Sex</th>
                <th class="tdr">Installations</th>
                <th class="tdr">Bounced users</th>
                <th class="tdr">Total consumers<br>(incl. FB Earn)</th>
                <th class="tdr">Package consumers</th>
                <th class="tdr">Total credits<br>purchased<br>(per installation)</th>
                <th class="tdr">Total package credits<br>purchased<br>(per installation)</th>
            </tr>
            </thead>
            <tbody>
<?
	$list = $db->gettotallist("SELECT email FROM tbl_user_ext WHERE adflag LIKE '$adflag' AND createdate>='$search_start_createdate' AND createdate<='$search_end_createdate 23:59:59' GROUP BY email ORDER BY COUNT(*) DESC LIMIT 100");
	
    for ($i=0; $i<sizeof($list); $i++)
    {
    	$country = $list[$i]["email"];
		
		$fullname = $db_analysis->getvalue("SELECT fullname FROM country_code WHERE codename='$country'");
		
		if ($fullname == "")
			$fullname = $country;
		
		if ($country == "")
			$country = "unknown";
				
		for ($j=0; $j<2; $j++)
		{
			$totalcount = get_stat($summarylist, $j+1, $country, "", "totalcount");
			$unplaycount = get_stat($summarylist, $j+1, $country, "", "unplaycount");
			$paycount = get_stat($summarylist, $j+1, $country, "", "paycount");
			$packagecount = get_stat($summarylist, $j+1, $country, "", "packagecount");
			$totalcredit = get_stat($summarylist, $j+1, $country, "", "totalcredit");
			$packagecredit = get_stat($summarylist, $j+1, $country, "", "packagecredit");
			
			if ($totalcount == 0)
			{
				$unplayratio = 0;
				$payratio = 0;
				$packageratio = 0;
				$averagecredit = 0;
				$averagepackagecredit = 0;
			}
			else
			{
				$unplayratio = round($unplaycount * 10000 / $totalcount) / 100;
				$payratio = round($paycount * 10000 / $totalcount) / 100;
				$packageratio = round($packagecount * 10000 / $totalcount) / 100;
				$averagecredit = round($totalcredit * 1000 / $totalcount) / 1000;
				$averagepackagecredit = round($packagecredit * 1000 / $totalcount) / 1000;
			}
?>
                <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
			if ($j == 0)
			{
?>
                    <td class="tdc point_title" valign="center" rowspan=2><?= ($fullname == "") ? "unknown" : $fullname ?></td>
<?				
			}
?>
                    <td class="tdc point_title" valign="center"><?= ($j == 0) ? "Male" : "Female" ?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>
                    <td class="tdr point"><?= number_format($unplaycount) ?> (<?= $unplayratio ?> %)</td>
                    <td class="tdr point"><?= number_format($paycount) ?> (<?= $payratio ?> %)</td>
                    <td class="tdr point"><?= number_format($packagecount) ?> (<?= $packageratio ?> %)</td>
                    <td class="tdr point"><?= number_format($totalcredit) ?> (<?= $averagecredit ?>)</td>
                    <td class="tdr point"><?= number_format($packagecredit) ?> (<?= $averagepackagecredit ?>)</td>
                </tr>
<?
		}
    }
?>    
            </tbody>
            </table>
        </div>   
        <br><br>
			<div class="search_result">
				3. Traffic analysis by sex
			</div>
            <div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
                <col width=""> 
                <col width=""> 
            </colgroup>
            <thead>
            <tr>
                <th>Sex</th>
                <th class="tdr">Installations</th>
                <th class="tdr">Bounced users</th>
                <th class="tdr">Total consumers<br>(incl. FB Earn)</th>
                <th class="tdr">Package consumers</th>
                <th class="tdr">Total credits<br>purchased<br>(per installation)</th>
                <th class="tdr">Total package credits<br>purchased<br>(per installation)</th>
            </tr>
            </thead>
            <tbody>
<?
    for ($i=0; $i<2; $i++)
    {
    	$sex = $i+1;
		
		$totalcount = get_stat($summarylist, $sex, "", "", "totalcount");
		$unplaycount = get_stat($summarylist, $sex, "", "", "unplaycount");
		$paycount = get_stat($summarylist, $sex, "", "", "paycount");
		$packagecount = get_stat($summarylist, $sex, "", "", "packagecount");
		$totalcredit = get_stat($summarylist, $sex, "", "", "totalcredit");
		$packagecredit = get_stat($summarylist, $sex, "", "", "packagecredit");
			
		if ($totalcount == 0)
		{
			$unplayratio = 0;
			$payratio = 0;
			$packageratio = 0;
			$averagecredit = 0;
			$averagepackagecredit = 0;
		}
		else
		{
			$unplayratio = round($unplaycount * 10000 / $totalcount) / 100;
			$payratio = round($paycount * 10000 / $totalcount) / 100;
			$packageratio = round($packagecount * 10000 / $totalcount) / 100;
			$averagecredit = round($totalcredit * 1000 / $totalcount) / 1000;
			$averagepackagecredit = round($packagecredit * 1000 / $totalcount) / 1000;
		}
?>
                <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
                    <td class="tdc point_title" valign="center"><?= ($sex == 1) ? "Male" : "Female" ?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>
                    <td class="tdr point"><?= number_format($unplaycount) ?> (<?= $unplayratio ?> %)</td>
                    <td class="tdr point"><?= number_format($paycount) ?> (<?= $payratio ?> %)</td>
                    <td class="tdr point"><?= number_format($packagecount) ?> (<?= $packageratio ?> %)</td>
                    <td class="tdr point"><?= number_format($totalcredit) ?> (<?= $averagecredit ?>)</td>
                    <td class="tdr point"><?= number_format($packagecredit) ?> (<?= $averagepackagecredit ?>)</td>
                </tr>
<?
    }
?>    
            </tbody>
            </table>
        </div> 
        <br><br>
			<div class="search_result">
				4. Traffic analysis by age and sex (partial result)
			</div>
            <div id="tab_content_1">
            <table class="tbl_list_basic1">
            <colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width=""> 
                <col width=""> 
                <col width=""> 
            </colgroup>
            <thead>
            <tr>
                <th>Age</th>
                <th>Sex</th>
                <th class="tdr">Installations</th>
                <th class="tdr">Bounced users</th>
                <th class="tdr">Total consumers<br>(incl. FB Earn)</th>
                <th class="tdr">Package consumers</th>
                <th class="tdr">Total credits<br>purchased<br>(per installation)</th>
                <th class="tdr">Total package credits<br>purchased<br>(per installation)</th>
            </tr>
            </thead>
            <tbody>
<?
	$sql = "SELECT birthyear,sex,COUNT(*) AS totalcount,".
		"IFNULL(SUM(CASE WHEN (SELECT IFNULL(SUM(money_in),0) FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx)=0 THEN 1 ELSE 0 END),0) AS unplaycount,";
		
	if ($search_payday != "")
	{
		$sql .= "IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0) AS paycount,".
			"IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND paycategory='facebook' AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY)) THEN 1 ELSE 0 END),0) AS packagecount,".
			"IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) AS totalcredit,".
			"IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND paycategory='facebook' AND writedate<=DATE_ADD(tbl_user_ext.createdate, INTERVAL $search_payday DAY))),0) AS packagecredit ";
	}
	else
	{
		$sql .= "IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1) THEN 1 ELSE 0 END),0) AS paycount,".
			"IFNULL(SUM(CASE WHEN EXISTS (SELECT * FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND paycategory='facebook') THEN 1 ELSE 0 END),0) AS packagecount,".
			"IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1)),0) AS totalcredit,".
			"IFNULL(SUM((SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE useridx=tbl_user_ext.useridx AND status=1 AND paycategory='facebook')),0) AS packagecredit ";
	}
		
	$sql .= " FROM tbl_user_ext JOIN tbl_user ON tbl_user_ext.useridx=tbl_user.useridx WHERE tbl_user_ext.createdate>='$search_start_createdate' AND tbl_user_ext.createdate<='$search_end_createdate 23:59:59' AND adflag LIKE '$adflag' AND birthyear>0 GROUP BY birthyear,sex";
	
	$summarylist = $db->gettotallist($sql);
	
    for ($i=0; $i<30; $i++)
    {
    	$startage = 10 + $i * 5;
    	$endage = 10 + $i * 5 + 4;

    	if (get_stat2($summarylist, "", $startage, $endage, "totalcount") == 0)
    		continue;
    		
		for ($j=0; $j<2; $j++)
		{
			$totalcount = get_stat2($summarylist, $j+1, $startage, $endage, "totalcount");
			$unplaycount = get_stat2($summarylist, $j+1, $startage, $endage, "unplaycount");
			$paycount = get_stat2($summarylist, $j+1, $startage, $endage, "paycount");
			$packagecount = get_stat2($summarylist, $j+1, $startage, $endage, "packagecount");
			$totalcredit = get_stat2($summarylist, $j+1, $startage, $endage, "totalcredit");
			$packagecredit = get_stat2($summarylist, $j+1, $startage, $endage, "packagecredit");
			
			if ($totalcount == 0)
			{
				$unplayratio = 0;
				$payratio = 0;
				$packageratio = 0;
				$averagecredit = 0;
				$averagepackagecredit = 0;
			}
			else
			{
				$unplayratio = round($unplaycount * 10000 / $totalcount) / 100;
				$payratio = round($paycount * 10000 / $totalcount) / 100;
				$packageratio = round($packagecount * 10000 / $totalcount) / 100;
				$averagecredit = round($totalcredit * 1000 / $totalcount) / 1000;
				$averagepackagecredit = round($packagecredit * 1000 / $totalcount) / 1000;
			}
?>
                <tr  class="" onmouseover="className='tr_over'" onmouseout="className=''">
<?
			if ($j == 0)
			{
?>
                    <td class="tdc point_title" valign="center" rowspan=2><?= $startage ?> ~ <?= $endage ?></td>
<?				
			}
?>
                    <td class="tdc point_title" valign="center"><?= ($j == 0) ? "Male" : "Female" ?></td>
                    <td class="tdr point"><?= number_format($totalcount) ?></td>
                    <td class="tdr point"><?= number_format($unplaycount) ?> (<?= $unplayratio ?> %)</td>
                    <td class="tdr point"><?= number_format($paycount) ?> (<?= $payratio ?> %)</td>
                    <td class="tdr point"><?= number_format($packagecount) ?> (<?= $packageratio ?> %)</td>
                    <td class="tdr point"><?= number_format($totalcredit) ?> (<?= $averagecredit ?>)</td>
                    <td class="tdr point"><?= number_format($packagecredit) ?> (<?= $averagepackagecredit ?>)</td>
                </tr>
<?
		}
    }
?>    
            </tbody>
            </table>
        </div>
<?
		$db->end();
		$db_analysis->end();
		$db4->end();
	}
	else
	{
?>   
            <div class="search_result">
            </div>
<?
	}
?>     
        </form>
</div>
    <!-- //MAIN WRAP -->
<?
    include($_SERVER["DOCUMENT_ROOT"]."/partner/common/bottom_frame.inc.php");
?>