<?
    include($_SERVER['DOCUMENT_ROOT']."/partner/common/common_include.inc.php");
    
	ini_set("memory_limit", "-1");
	
	$search_start_createdate = $_GET["sdate"];
	$search_end_createdate = $_GET["edate"];
	
    $filename = "ifcontext_details.xls";
    
    $db = new CDatabase();
    
    $sql = "SELECT ifcontext,fbsource,logincount,missionclear,giftcount,giftreceivecount,advertisecount,createdate,".
    	"(SELECT userid FROM tbl_user WHERE useridx=tbl_user_ext.useridx) AS userid,".
    	"(SELECT IFNULL(SUM(money_in),0) FROM tbl_user_stat WHERE useridx=tbl_user_ext.useridx) AS money_in,".
    	"(SELECT IFNULL(SUM(facebookcredit),0) FROM tbl_product_order WHERE status=1 AND useridx=tbl_user_ext.useridx) AS facebookcredit ".
    	" FROM tbl_user_ext WHERE adflag='adotomi' AND createdate>='$search_start_createdate 00:00:00' AND createdate<='$search_end_createdate 23:59:59' ORDER BY useridx DESC LIMIT 50000";

    $list = $db->gettotallist($sql);
    
    $excel_contents = "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=UTF-8'><table border=1>".
	    "<tr>".         
    	"<td style='font-weight:bold;'>facebookid (order_id)</td>".
    	"<td style='font-weight:bold;'>landing</td>".
    	"<td style='font-weight:bold;'>ifcontext</td>".
    	"<td style='font-weight:bold;'>purchased facebook credit</td>".
    	"<td style='font-weight:bold;'>virtual money in (unplayed if 0)</td>".
    	"<td style='font-weight:bold;'>login count</td>".
    	"<td style='font-weight:bold;'>gifts sent</td>".
        "<td style='font-weight:bold;'>gifts received</td>".
    	"<td style='font-weight:bold;'>missions completed</td>".
        "<td style='font-weight:bold;'>new users recruited</td>".
    	"<td style='font-weight:bold;'>date of creation</td>".
    	"</tr>";
    
    for ($i=0; $i <sizeof($list); $i++)
    {
    	$data = $list[$i];
    	
        $excel_contents .= "<tr>".
            "<td style='mso-number-format:\@'>".$data['userid']."</td>".  
            "<td>".(($data['fbsource'] == "landing") ? "Y" : "N")."</td>".  
            "<td>".$data['ifcontext']."</td>".  
            "<td>".number_format($data['facebookcredit'])."</td>".  
            "<td>".number_format($data['money_in'])."</td>".  
            "<td>".$data['logincount']."</td>".  
            "<td>".$data['giftcount']."</td>".  
            "<td>".$data['giftreceivecount']."</td>".  
            "<td>".$data['missionclear']."</td>".  
            "<td>".$data['advertisecount']."</td>".  
            "<td>".$data['createdate']."</td>".  
            "</tr>";
    }
    
    $excel_contents .= "</table>";
    
    $db->end();
    
    Header("Content-type: application/x-msdownload");
    Header("Content-type: application/x-msexcel");
    Header("Content-Disposition: attachment; filename=$filename");
    
    echo($excel_contents);
?>