<?
	include_once("../common/common_include.inc.php");

	$cmd = $_GET["cmd"];
	
	if ($cmd == "")
	{
		error_close("Sorry, an error is occurred. Try it later.");
	}
	
	$infos = explode("/", $cmd);
	
	$cmd = $infos[0];
	$subcmd = $infos[1];
	
	check_xss(implode(",", $_POST));

	set_time_limit(60 * 10);

	if ($cmd == "product")
	{
		$db_main = new CDatabase_Main();
		$db_main2 = new CDatabase_Main2();

		include_once("../class/product_class.inc.php");
		
		if ($subcmd == "get_product_info")
		{
			$result = product_get_product_info();
		}
                		
		$db_main->end();
		$db_main2->end();
	}
	else if ($cmd == "game")
	{
		$db_main2 = new CDatabase_Main2();
		
		include_once("../class/game_class.inc.php");
		
		if ($subcmd == "get_event_info")
		{
			$result = game_get_event_info();
		}
		else if ($subcmd == "get_select_slot")
		{
			$result = game_get_select_slot();
		}
		
		$db_main2->end();
	}
	if ($cmd == "user")
	{
	    $totalcount1 = 0;
	    $db_main = new CDatabase_Main();
	    $db_main2 = new CDatabase_Main2();
	    $db_analysis = new CDatabase_Analysis();
	    $db_friend = new CDatabase_Friend();
	    $db_other = new CDatabase_Other();
	    $db_mobile = new CDatabase_Mobile();
	    $db_livestats = new CDatabase_Livestats();
	    
	    include_once("../class/user_class.inc.php");
	    
	    if ($subcmd == "get_actionlog_totalcount")
	    {
	        $result = user_get_actionlog_totalcount();
	    }
	   
	    $db_main->end();
	    $db_main2->end();
	    $db_analysis->end();
	    $db_friend->end();
	    $db_other->end();
	    $db_mobile->end();
	    $db_livestats->end();
	}
		
	if (is_array($result))
	{
		$str_data = "";
		
		foreach($result as $key=>$value)
		{
			if(!is_number($key))
			{
				$value = encode_json_value($value);
				
				if($str_data == "")
					$str_data = "\"$key\":\"".$value."\"";
				else 
					$str_data .= ",\"$key\":\"$value\"";
			}
		}
		$json = "{\"result\":\"1\", \"data\":{".$str_data."}}";
	}
	else if ($result == "")
		$json = "{\"result\":\"1\", \"reason\":\"".encode_html_attribute($result)."\"}";
	else
		$json = "{\"result\":\"0\", \"reason\":\"".encode_html_attribute($result)."\"}";

	header("Content-Type: application/json; charset=utf-8");

	echo($json);
?>