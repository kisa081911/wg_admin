<?
	include_once("../common/common_include.inc.php");

	$cmd = $_GET["cmd"];
	
	if ($cmd == "")
	{
		error_close("Sorry, an error is occurred. Try it later.");
	}
	
	$infos = explode("/", $cmd);
	
	$cmd = $infos[0];
	$subcmd = $infos[1];
	
	check_xss(implode(",", $_POST));
	
	set_time_limit(60 * 10);

	if ($cmd == "user")
	{	
		$totalcount1 = 0;
		$db_main = new CDatabase_Main();
		$db_main2 = new CDatabase_Main2();		
		$db_analysis = new CDatabase_Analysis();
		$db_friend = new CDatabase_Friend();
		$db_other = new CDatabase_Other();
		$db_mobile = new CDatabase_Mobile();
		$db_livestats = new CDatabase_Livestats();
		
		include_once("../class/user_class.inc.php");
		
		if ($subcmd == "get_freecoinlog_list")
		{
			$result = user_get_freecoinlog_list();
		}
		else if ($subcmd == "get_actionlog_list")
		{
			$result = user_get_actionlog_list();
		}
		else if ($subcmd == "get_friend_list")
		{
			$result = user_get_friend_list();
		}
		else if ($subcmd == "get_user_event_list")
		{
			$result = user_get_user_event_list();
		}
		else if ($subcmd == "get_jackpot_list")
		{
			$result = user_get_jackpot_list();
		}
		else if ($subcmd == "get_bigwin_list")
		{
			$result = user_get_bigwin_list();
		}
		else if ($subcmd == "get_user_unkown_coin_list")
		{
			$result = user_get_user_unkown_coin_list();
		}
		else if ($subcmd == "get_free_resource_list")
		{
			$result = user_get_free_resource_list();
		}
		else if ($subcmd == "get_slotdetail_list")
		{
			$result = user_get_slotdetail_list();
		}
		else if ($subcmd == "get_uuid_list")
		{
			$result = user_get_uuid_list();
		}
		else if ($subcmd == "get_agency_spend")
		{
			$result = user_get_agency_spend();
		}
		else if ($subcmd == "get_inbox_list")
		{
			$result = user_get_inbox_list();
		}
		else if ($subcmd == "get_actionlog_blackbox")
		{
			$result = user_get_actionlog_blackbox();
		}
		else if ($subcmd == "get_blackbox_action_list")
		{
			$result = user_get_blackbox_action_list();
		}
		else if ($subcmd == "get_blackbox_game_list")
		{
			$result = user_get_blackbox_game_list();
		}
		else if ($subcmd == "get_blackbox_client_list")
		{
			$result = user_get_blackbox_client_list();
		}
		else if ($subcmd == "get_share_list")
		{
		    $result = user_get_share_list();
		}
		$db_main->end();
		$db_main2->end();
		$db_analysis->end();
		$db_friend->end();
		$db_other->end();
		$db_mobile->end();
		$db_livestats->end();
	}
	else if($cmd == "game")
	{
		$db_main2 = new CDatabase_Main2();
		$db_analysis = new CDatabase_Analysis();
		
		include_once("../class/game_class.inc.php");
		
		if ($subcmd == "get_ratio_history")
		{
			$result = game_get_ratio_history();
		}
		else if ($subcmd == "get_fb_adset_info")
		{
			$result = game_get_fb_adset_info();
		}	
		
		$db_main2->end();
		$db_analysis->end();
	}
	else if ($cmd == "customer")
	{
		$db_analysis = new CDatabase_Analysis();
		
		include_once("../class/customer_class.inc.php");
		 
		if ($subcmd == "get_bug_issue_title")
		{
			$result = customer_get_bug_issue_title();
		}
		
		$db_analysis->end();
	}
		
	if (is_array($result))
	{
		$str_data = "";
		
		for ($i=0; $i<sizeof($result); $i++)
		{
			if($i == 0)
				$str_data .= "{";
			else
				$str_data .= ",{";
			$field_cnt = 0;
			
			foreach($result[$i] as $key=>$value)
			{
				if(!is_number($key))
				{
					$value = encode_json_value($value);
					$value = str_replace("\\n", " ", $value);
					
					if($field_cnt == 0)
						$str_data .= "\"$key\":".json_encode($value, JSON_UNESCAPED_SLASHES);
					else
						$str_data .= ",\"$key\":".json_encode($value, JSON_UNESCAPED_SLASHES);
					
					$field_cnt++;
				}
			}
			
			$str_data .= "}";
		}

		$json = "{\"result\":\"1\", \"data\":[".$str_data."]}";
	}
	else if ($result == "")
		$json = "{\"result\":\"1\", \"reason\":\"".encode_html_attribute($result)."\"}";
	else
		$json = "{\"result\":\"0\", \"reason\":\"".encode_html_attribute($result)."\"}";

	header("Content-Type: application/json; charset=utf-8");
	
	echo($json);
?>