<?
	include_once("../common/common_include.inc.php");

	$cmd = $_GET["cmd"];
	
	if ($cmd == "")
	{
		error_close("Sorry, an error is occurred. Try it later.");
	}
	
	$infos = explode("/", $cmd);
	
	$cmd = $infos[0];
	$subcmd = $infos[1];

	set_time_limit(60 * 10);
	
	if ($cmd == "product")
	{
		$db_main = new CDatabase_Main();
		$db_main2 = new CDatabase_Main2();

		include_once("../class/product_class.inc.php");
		
		if ($subcmd == "save_product")
		{
			$result = product_save_product();
		}
		else if ($subcmd == "update_product")
		{
			$result = product_update_product();
		}
		else if ($subcmd == "delete_product")
		{
			$result = product_delete_product();
		}
		else if ($subcmd == "dispute_settle")
		{
			$result = product_dispute_settle();
		}
		else if ($subcmd == "dispute_refund")
		{
			$result = product_dispute_refund();
		}
		else if ($subcmd == "save_product_mobile")
		{
			$result = product_save_product_mobile();
		}
		else if ($subcmd == "update_product_mobile")
		{
			$result = product_update_product_mobile();
		}
		else if ($subcmd == "delete_product_mobile")
		{
			$result = product_delete_product_mobile();
		}		
		 		
		$db_main->end();
		$db_main2->end();
	}
	else if($cmd == "user")
	{
		$db_main = new CDatabase_Main();
		$db_main2 = new CDatabase_Main2();
		$db_analysis = new CDatabase_Analysis();
		$db_inbox = new CDatabase_Inbox();
		$db_friend = new CDatabase_Friend();
		$db_other = new CDatabase_Other();
		$db_mobile = new CDatabase_Mobile();
		
		$facebook = new Facebook(array(
		    'appId'  => FACEBOOK_APP_ID,
		    'secret' => FACEBOOK_SECRET_KEY,
		    'cookie' => true,
		));
		
		
		include_once("../class/user_class.inc.php");
		
		if ($subcmd == "save_free_resource")
		{
			$result = user_save_free_resource();
		}
		else if ($subcmd == "update_friend")
		{
			$result = user_update_friend();
		}
		else if ($subcmd == "update_user_block")
		{
			$result = user_update_user_block();
		}
		else if ($subcmd == "update_daily_inflowrout_cost")
		{
			$result = user_update_daily_inflowrout_cost();
		}
		else if ($subcmd == "update_agency_spend")
		{
			$result = user_update_agency_spend();
		}
		else if ($subcmd == "insert_actionlog_blackbox")
        {
        	$result = user_insert_actionlog_blackbox();
        }
	    else if ($subcmd == "delete_blackbox")
        {
        	$result = user_delete_blackbox();
	    }
	    else if ($subcmd == "update_user_uuid_block")
	    {
	    	$result = user_update_user_uuid_block();
	    }
	    else if ($subcmd == "save_block_resource")
	    {
	    	$result = user_save_block_resource();
	    }
	    else if ($subcmd == "send_a2u")
	    {
	        $result = user_send_a2u();
	    }
	    else if ($subcmd == "send_push")
	    {
	        $result = user_send_push();
	    }
		
		$db_main->end();
		$db_main2->end();
		$db_analysis->end();
		$db_inbox->end();
		$db_friend->end();
		$db_other->end();		
		$db_mobile->end();		
	}
	else if ($cmd == "game")
	{
		$db_main = new CDatabase_Main();
		$db_main2 = new CDatabase_Main2();
		$db_analysis = new CDatabase_Analysis();
		$db_inbox = new CDatabase_Inbox();
		$db_game = new CDatabase_Game();
		$db_other = new CDatabase_Other();
		$db_mobile = new CDatabase_Mobile();

		include_once("../class/game_class.inc.php");
		
		if ($subcmd == "insert_event")
		{
			$result = game_insert_event();
		}
		else if ($subcmd == "update_event")
		{
			$result = game_update_event();
		}
		else if ($subcmd == "delete_event")
		{
			$result = game_delete_event();
		}
		else if ($subcmd == "update_shorturl")
		{
			$result = game_update_shorturl();
		}
		else if ($subcmd == "send_comment_gift")
		{
			$result = game_send_comment_gift();
		}
		else if ($subcmd == "change_ratio")
		{
			$result = game_change_ratio();
		}
		else if ($subcmd == "update_betrange")
		{
			$result = game_update_betrange();
		}
		else if($subcmd == "insert_and_push")
		{
			$result = game_insert_and_push();
		}
		else if($subcmd == "update_and_push")
		{
			$result = game_update_and_push();
		}
		else if($subcmd == "delete_and_push")
		{
			$result = game_delete_and_push();
		}
		else if($subcmd == "insert_mobile_push_event")
		{
			$result = game_insert_mobile_push_event();
		}
		else if($subcmd == "update_mobile_push_event")
		{
			$result = game_update_mobile_push_event();
		}
		else if($subcmd == "delete_mobile_push_event")
		{
			$result = game_delete_mobile_push_event();
		}
		else if ($subcmd == "insert_cross_promotion")
		{
			$result = game_insert_cross_promotion();
		}		
		else if ($subcmd == "update_cross_promotion")
		{
			$result = game_update_cross_promotion();
		}		
		else if ($subcmd == "delete_cross_promotion")
		{
			$result = game_delete_cross_promotion();
		}
		else if ($subcmd == "fiesta_change_ratio")
		{
			$result = game_fiesta_change_ratio();
		}
		else if ($subcmd == "insert_notice_popup")
		{
		    $result = game_insert_notice_popup();
		}
		else if ($subcmd == "update_notice_popup")
		{
		    $result = game_update_notice_popup();
		}
		else if ($subcmd == "delete_notice_popup")
		{
		    $result = game_delete_notice_popup();
		}
		else if ($subcmd == "update_chat_block")
		{
		    $result = game_update_chat_block();
		}
		else if($subcmd == "insert_regular_push")
		{
		    $result = game_insert_regular_push();
		}
		else if($subcmd == "update_regular_push")
		{
		    $result = game_update_regular_push();
		}
		else if($subcmd == "delete_regular_push")
		{
		    $result = game_delete_regular_push();
		}
		else if ($subcmd == "insert_notice_lobby")
		{
		    $result = game_insert_notice_lobby();
		}
		else if ($subcmd == "update_notice_lobby")
		{
		    $result = game_update_notice_lobby();
		}
		else if ($subcmd == "delete_notice_lobby")
		{
		    $result = game_delete_notice_lobby();
		}

		$db_main->end();
		$db_main2->end();
		$db_analysis->end();
		$db_inbox->end();
		$db_game->end();
		$db_other->end();
		$db_mobile->end();
	}
	else if($cmd == "admin")
	{
		$db_main2 = new CDatabase_Main2();
		$db_analysis = new CDatabase_Analysis();
		
		include_once("../class/admin_class.inc.php");
		
		if ($subcmd == "save_admin")
		{
			$result = admin_save_admin();
		}
		else if ($subcmd == "update_admin")
		{
			$result = admin_update_admin();
		}
		else if ($subcmd == "delete_admin")
		{
			$result = admin_delete_admin();
		}
		else if($subcmd == "update_resource")
		{
			$result = admin_update_resource();
		}
		else if ($subcmd == "tmpdeploy_resource")
		{
			$result = admin_tmpdeploy_resource();
		}
		else if ($subcmd == "file_check")
		{
			$result = admin_file_check();
		}
		else if ($subcmd == "file_md5check")
		{
			$result = admin_file_md5check();
		}
		else if ($subcmd == "beta_deploy_resource")
		{
			$result = admin_beta_deploy_resource();
		}
		else if ($subcmd == "deploy_resource")
		{
			$result = admin_deploy_resource();
		}
		else if ($subcmd == "maintenance_save")
		{
			$result = admin_maintenance_save();
		}
		else if ($subcmd == "maintenance_tmpdeploy_resource")
		{
			$result = admin_maintenance_tmpdeploy_resource();
		}
		else if ($subcmd == "maintenance_deploy_resource")
		{
			$result = admin_maintenance_deploy_resource();
		}
		else if ($subcmd == "update_resource_data")
		{
		    $result = admin_update_resource_data();
		}
		else if ($subcmd == "update_config_setting")
		{
		    $result = admin_update_config_setting();
		}
		else if ($subcmd == "copy_resource_pord_data")
		{
		    $result = admin_copy_resource_pord_data();
		}
		
		$db_main2->end();
		$db_analysis->end();
	}
	else if($cmd == "customer")
	{
		$db_main = new CDatabase_Main();
		$db_analysis = new CDatabase_Analysis();
		$db_inbox = new CDatabase_Inbox();
		$db_mobile = new CDatabase_Mobile();
		
		include_once("../class/customer_class.inc.php");
		
		if($subcmd == "save_qa")
		{
			$result = customer_save_qa();
		}
		else if($subcmd == "delete_qa")
		{
			$result = customer_delete_qa();
		}
		else if($subcmd == "answer_qa")
		{
			$result = customer_answer_qa();
		}
		else if($subcmd == "update_answer")
		{
			$result = customer_update_answer();
		}
		else if($subcmd == "delete_answer")
		{
			$result = customer_delete_answer();
		}
		else if($subcmd == "save_cr")
		{
			$result = customer_save_cr();
		}
		else if($subcmd == "save_template")
		{
			$result = customer_save_template();
		}
		else if($subcmd == "update_template")
		{
			$result = customer_update_template();
		}
		else if($subcmd == "delete_template")
		{
			$result = customer_delete_template();
		}
		else if($subcmd == "save_faq")
		{
			$result = customer_save_faq();
		}
		else if($subcmd == "update_faq")
		{
			$result = customer_update_faq();
		}
		else if($subcmd == "delete_faq")
		{
			$result = customer_delete_faq();
		}
		else if($subcmd == "move_faq")
		{
			$result = customer_move_faq();
		}
		else if ($subcmd == "save_qa_ios")
		{
			$result = customer_save_qa_ios();
		}
		else if ($subcmd == "save_qa_android")
		{
			$result = customer_save_qa_android();
		}
		else if ($subcmd == "save_qa_amazon")
		{
			$result = customer_save_qa_amazon();
		}
		else if ($subcmd == "delete_qa_ios")
		{
			$result = customer_delete_qa_ios();
		}
		else if ($subcmd == "delete_qa_android")
		{
			$result = customer_delete_qa_android();
		}
		else if ($subcmd == "delete_qa_amazon")
		{
			$result = customer_delete_qa_amazon();
		}
		else if ($subcmd == "answer_qa_ios")
		{
			$result = customer_answer_qa_ios();
		}
		else if ($subcmd == "answer_qa_android")
		{
			$result = customer_answer_qa_android();
		}
		else if ($subcmd == "answer_qa_amazon")
		{
			$result = customer_answer_qa_amazon();
		}
		else if ($subcmd == "delete_answer_ios")
		{
			$result = customer_delete_answer_ios();
		}
		else if ($subcmd == "delete_answer_android")
		{
			$result = customer_delete_answer_android();
		}
		else if ($subcmd == "delete_answer_amazon")
		{
			$result = customer_delete_answer_amazon();
		}
		else if ($subcmd == "update_answer_ios")
		{
			$result = customer_update_answer_ios();
		}
		else if ($subcmd == "update_answer_android")
		{
			$result = customer_update_answer_android();
		}
		else if ($subcmd == "update_answer_amazon")
		{
			$result = customer_update_answer_amazon();
		}
		else if ($subcmd == "save_mobile_ios_template")
		{
			$result = customer_save_mobile_ios_template();
		}
		else if ($subcmd == "save_mobile_android_template")
		{
			$result = customer_save_mobile_android_template();
		}
		else if ($subcmd == "save_mobile_amazon_template")
		{
			$result = customer_save_mobile_amazon_template();
		}
		else if ($subcmd == "update_mobile_ios_template")
		{
			$result = customer_update_mobile_ios_template();
		}
		else if ($subcmd == "update_mobile_android_template")
		{
			$result = customer_update_mobile_android_template();
		}
		else if ($subcmd == "update_mobile_amazon_template")
		{
			$result = customer_update_mobile_amazon_template();
		}
		else if ($subcmd == "delete_mobile_ios_template")
		{
			$result = customer_delete_mobile_ios_template();
		}
		else if ($subcmd == "delete_mobile_android_template")
		{
			$result = customer_delete_mobile_android_template();
		}
		else if ($subcmd == "delete_mobile_amazon_template")
		{
			$result = customer_delete_mobile_amazon_template();
		}
		else if ($subcmd == "move_mobile_ios_category")
		{
			$result = customer_move_mobile_ios_category();
		}
		else if ($subcmd == "move_mobile_android_category")
		{
			$result = customer_move_mobile_android_category();
		}
		else if ($subcmd == "move_mobile_amazon_category")
		{
			$result = customer_move_mobile_amazon_category();
		}
		else if ($subcmd == "save_mobile_ios_category")
		{
			$result = customer_save_mobile_ios_category();
		}
		else if ($subcmd == "save_mobile_android_category")
		{
			$result = customer_save_mobile_android_category();
		}
		else if ($subcmd == "save_mobile_amazon_category")
		{
			$result = customer_save_mobile_amazon_category();
		}
		else if ($subcmd == "update_mobile_ios_category")
		{
			$result = customer_update_mobile_ios_category();
		}
		else if ($subcmd == "update_mobile_android_category")
		{
			$result = customer_update_mobile_android_category();
		}
		else if ($subcmd == "update_mobile_amazon_category")
		{
			$result = customer_update_mobile_amazon_category();
		}
		else if ($subcmd == "delete_mobile_ios_category")
		{
			$result = customer_delete_mobile_ios_category();
		}
		else if ($subcmd == "delete_mobile_android_category")
		{
			$result = customer_delete_mobile_android_category();
		}
		else if ($subcmd == "delete_mobile_amazon_category")
		{
			$result = customer_delete_mobile_amazon_category();
		}
		else if ($subcmd == "update_mobileqa")
		{
			$result = customer_update_mobileqa();
		}
		else if ($subcmd == "insert_bug_issue_title")
		{
			$result = customer_insert_bug_issue_title();
		}
		else if ($subcmd == "insert_bug_issue_report")
		{
			$result = customer_insert_bug_issue_report();
		}
		else if ($subcmd == "delete_bug_issue_report")
		{
			$result = customer_delete_bug_issue_report();
		}		
		
		$db_main->end();
		$db_analysis->end();
		$db_inbox->end();
		$db_mobile->end();
	}
	else if ($cmd == "monitoring")
	{
		$db_main = new CDatabase_Main();
		$db_main2 = new CDatabase_Main2();
		$db_analysis = new CDatabase_Analysis();
		$db_other = new CDatabase_Other();

		include_once("../class/monitoring_class.inc.php");
		
		if($subcmd == "update_coin_increase_user_status")
		{
			$result = monitoring_update_coin_increase_user_status();
		}
		else if($subcmd == "update_coin_increase_user")
		{
			$result = monitoring_update_coin_increase_user();
		}
		else if($subcmd == "update_unknowncoin_user_status")
		{
			$result = monitoring_update_unknowncoin_user_status();
		}
		else if($subcmd == "update_unknowncoin_user")
		{
			$result = monitoring_update_unknowncoin_user();
		}
		else if($subcmd == "update_abuse_suspicion_user_status")
		{
			$result = monitoring_update_abuse_suspicion_user_status(); 
		}
		else if($subcmd == "insert_abuse_suspicion_user")
		{
			$result = monitoring_insert_abuse_suspicion_user();
		}
		else if($subcmd == "update_abuse_suspicion_user")
		{
			$result = monitoring_update_abuse_suspicion_user();
		}
		else if($subcmd == "update_abuse_user_debug")
		{
			$result = monitoring_update_abuse_user_debug();
		}
		else if($subcmd == "update_gain_high_coin_user")
		{
			$result = monitoring_update_gain_high_coin_user();
		}
		else if($subcmd == "update_slot_high_coin_user_status")
		{
			$result = monitoring_update_slot_high_coin_user_status();
		}
		else if($subcmd == "update_unknowncoin_user_new")
		{
			$result = monitoring_update_unknowncoin_user_new();
		}
		else if($subcmd == "update_coin_increase_user_new")
		{
			$result = monitoring_update_coin_increase_user_new();
		}
		else if($subcmd == "update_abuse_slot_user_new")
		{
			$result = monitoring_update_abuse_slot_user_new();
		}
				
		$db_main->end();
		$db_main2->end();
		$db_analysis->end();
		$db_other->end();
	}
		
	if ($result != "")
		$json = "{\"result\":\"0\", \"reason\":\"".encode_html_attribute($result)."\"}";
	else
		$json = "{\"result\":\"1\", \"reason\":\"".encode_html_attribute($result)."\"}";
	
	header("Content-Type: application/json; charset=utf-8");
	echo($json);
?>