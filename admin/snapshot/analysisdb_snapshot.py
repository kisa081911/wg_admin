#!/usr/bin/python
# -*- coding:utf-8 -*-
import os
import time
import sys
import MySQLdb
import traceback

def daemonize():
	pid = os.fork()
	if pid != 0:
		os._exit(0)

	os.setsid()
	maxfd = os.sysconf("SC_OPEN_MAX")
	for fd in range(0, maxfd):
		try:
			os.close(fd)
		except OSError:
			pass

def DeleteSnapshotDB():
	try:
		db = MySQLdb.connect(host='107.21.2.182', user='wadmin', passwd='!djemals0321', db='wcasino_analysis', port=30061)

		cursor = db.cursor()
		
		sql = "SELECT COUNT(snapidx) FROM tbl_aws_snapshot WHERE type = 40301"
		cursor.execute(sql)

		snapcnt = cursor.fetchone()[0]

		if snapcnt > 5:
			sql = "SELECT snapid FROM tbl_aws_snapshot WHERE type=40301 ORDER BY snapidx ASC LIMIT 1"

			cursor.execute(sql)

			oldsnapid = cursor.fetchone()[0]
		
			cmd = '/opt/aws/bin/ec2-delete-snapshot %s -O AKIAIJF4EK5FD6Z5SVYQ -W 7BjyMv0qwqvVAMyZlqJRCHntHVdVlwRfO+UGRJIA'%(oldsnapid)

			sql = "DELETE FROM tbl_aws_snapshot WHERE snapid='%s'"%(oldsnapid)

			cursor.execute(sql)

			m = os.popen(cmd)
			m.close();

			db.commit()

		cursor.close()

		db.close()

	except Exception,e:
		print(str(e))
		print(e)
		print(traceback.format_tb(sys.exc_info()[2]))

def CreateSnapshotDB():
	try:
		t = time.localtime()

		year = t.tm_year
		if t.tm_mon < 10:
			month = "0" + str(t.tm_mon)
		else:
			month = t.tm_mon
		if t.tm_mday < 10:
			day = "0" + str(t.tm_mday)
		else:
			day = t.tm_mday
		if t.tm_hour < 10:
			hour = "0" + str(t.tm_hour)
		else:
			hour = t.tm_hour

		cmd = '/opt/aws/bin/ec2-create-snapshot vol-36f655fe -O AKIAIJF4EK5FD6Z5SVYQ -W 7BjyMv0qwqvVAMyZlqJRCHntHVdVlwRfO+UGRJIA -d %s%s%s-%s_Take5_AnalysisDB'%(year, month, day, hour)

		m = os.popen(cmd)
		for line in m.read().splitlines():
			items = line.split()
		m.close()

		snapid = items[1]

		db = MySQLdb.connect(host='107.21.2.182', user='wadmin', passwd='!djemals0321', db='wcasino_analysis', port=30061)

		cursor = db.cursor()

		sql = "INSERT INTO tbl_aws_snapshot(type, snapid, writedate) VALUES(40301, '%s', now()) "%(snapid)
		cursor.execute(sql)
		db.commit()

		cursor.close()

		db.close()

	except Exception,e:
		print(str(e))
		print(e)
		print(traceback.format_tb(sys.exc_info()[2]))

daemonize()

def main_loop():
	CreateSnapshotDB()
	
	DeleteSnapshotDB()

if __name__ == "__main__":

	main_loop()

	sys.exit()

