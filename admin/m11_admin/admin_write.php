<?
	$top_menu = "admin";
	$sub_menu = "admin";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$adminidx = $_GET["adminidx"];
	
	$db_analysis = new CDatabase_Analysis();
	
	if ($adminidx != "")
	{
		check_number($adminidx);
		
		$sql = "SELECT adminidx,adminname,adminid,password,menu,status,dbaccesstype,iscustomerservice FROM tbl_admin WHERE adminidx=$adminidx";
		$admin = $db_analysis->getarray($sql);

		$adminidx = $admin["adminidx"];
		$adminname = $admin["adminname"];
		$adminid = $admin["adminid"];
		$password = $admin["password"];
		$password = decrypt($password);
        $menu = $admin["menu"];
		$status = $admin["status"];
		$dbaccesstype = $admin["dbaccesstype"];
		$iscustomerservice = $admin["iscustomerservice"];
		
		if ($adminidx == "")
			error_back("삭제되었거나 존재하지 않는 관리자입니다.");
	}
    
    $sql = "SELECT menuidx,menu_name,menu_name_kr FROM system_menu ORDER BY menu_order";
    $menu_list = $db_analysis->gettotallist($sql);
    
    $db_analysis->end();
?>
<script type="text/javascript">
	function save_admin()
	{
		var admin_form = document.admin_form;

		if (admin_form.adminname.value == "")
		{
			alert("이름을 입력하세요.");
			admin_form.adminname.focus();
			return;
		}

		if (admin_form.adminid.value == "")
		{
			alert("아이디를 입력하세요.");
			admin_form.adminid.focus();
			return;
		}

		if (admin_form.password.value == "")
		{
			alert("비밀번호를 입력하세요.");
			admin_form.password.focus();
			return;
		}
		
		var menulist = document.getElementsByName("menu");
		var list = "";
		for (var i=0; i<menulist.length; i++)
		{
		    if (menulist[i].checked)
		      list += "[" + menulist[i].value + "]";
		}

		var param = {};
    	param.adminidx = admin_form.adminidx.value;
    	param.adminname = admin_form.adminname.value;
    	param.adminid = admin_form.adminid.value;
    	param.password = admin_form.password.value;
    	param.menu = list;
    	param.status = get_radio("status");
    	param.dbaccesstype = get_radio("dbaccesstype");
    	param.iscustomerservice = (admin_form.iscustomerservice.checked ? "1" : "0");

		if (admin_form.adminidx.value == "")
			WG_ajax_execute("admin/save_admin", param, save_admin_callback, false);
		else
			WG_ajax_execute("admin/update_admin", param, save_admin_callback, false);
	}

	function save_admin_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
           	window.location.href = "admin_mng.php";
        }
    }
    
    function check_all(checked)
    {
        var list = document.getElementsByName("menu");
        
        for (var i=0; i<list.length; i++)
        {
            list[i].checked = checked;
        }
    }
    
    function delete_admin()
    {
        var admin_form = document.admin_form;

        if (!confirm("상품을 삭제 하시겠습니까?"))
            return;

        var param = {};
    	param.adminidx = admin_form.adminidx.value;

    	WG_ajax_execute("admin/delete_admin", param, delete_admin_callback, false);
    }

    function delete_admin_callback(result, reason)
    {
        if (!result)
        {
            alert("오류 발생 - " + reason);
        } 
        else
        {
            window.location.href = "admin_mng.php";
        }
    }
</script>

<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 관리자상세</div>
<?
    if ($adminidx != "")
    { 
?>
		<div class="title_button"><input type="button" class="btn_05" value="삭제" onclick="delete_admin()"></div>
<?
    } 
?>
	</div>
	<!-- //title_warp -->

	<form name="admin_form" id="admin_form">
		<input type="hidden" name="adminidx" id="adminidx" value="<?= $adminidx ?>" />
		<table class="tbl_view_basic">
			<colgroup>
				<col width="140">
				<col width="">
			</colgroup>
			<tbody>
				<tr>
					<th><span>*</span> 이름 </th>
					<td><input type="text" class="view_tbl_text" name="adminname" id="adminname" value="<?= $adminname ?>" style="width:300px" maxlength="50"></td>
				</tr>
				<tr>
					<th><span>*</span> 아이디</th>
					<td><input type="text" class="view_tbl_text" name="adminid" id="adminid" value="<?= $adminid ?>" style="width:300px" maxlength="50"></td>
				</tr>
				<tr>
					<th><span>*</span> 비밀번호</th>
					<td><input type="password" class="view_tbl_text" name="password" id="password" value="<?= $password ?>" style="width:300px" maxlength="50"></td>
				</tr>
				<tr>
			    	<th>
			        	접근권한
			        	<input type="checkbox" name="all_check" onclick="check_all(this.checked)" />
		        	</th>
			    	<td>
<?
    for ($i=0; $i<sizeof($menu_list); $i++)
    {
?>
                    	<input type="checkbox" name="menu" value="<?= $menu_list[$i]["menu_name"] ?>" <?= (strpos($menu, "[".$menu_list[$i]["menu_name"]."]") === false) ? "" : "checked" ?> /><?= $menu_list[$i]["menu_name_kr"] ?>&nbsp;
<?
    }
?>
			    	</td>
				</tr>
				<tr>
					<th>고객서비스인력</th>
					<td><input type="checkbox" class="radio" name="iscustomerservice" value="1" <?= ($iscustomerservice == "1") ? "checked" : "" ?>> 고객 서비스 인력 여부</td>
				</tr>
				<tr>
					<th><span>*</span> 상태</th>
					<td><input type="radio" class="radio" name="status" value="1" <?= ($status == "1" || $status == "") ? "checked" : "" ?>> 사용 <input type="radio" class="radio ml20" name="status" value="2" <?= ($status == "2") ? "checked" : "" ?>> 미사용</td>
				</tr>
				<tr>
					<th><span>*</span> DB권한</th>
					<td><input type="radio" class="radio" name="dbaccesstype" value="0" <?= ($dbaccesstype == "0" || $dbaccesstype == "") ? "checked" : "" ?> > Selector <input type="radio" class="radio ml20" name="dbaccesstype" value="1" <?= ($dbaccesstype == "1") ? "checked" : "" ?> > Admin <input type="radio" class="radio ml20" name="dbaccesstype" value="2" <?= ($dbaccesstype == "2") ? "checked" : "" ?> > Support <input type="radio" class="radio ml20" name="dbaccesstype" value="4" <?= ($dbaccesstype == "4") ? "checked" : "" ?> > QA <input type="radio" class="radio ml20" name="dbaccesstype" value="5" <?= ($dbaccesstype == "5") ? "checked" : "" ?> > SLOT</td>
				</tr>
			</tbody>
		</table>
	</form>
    		
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="저장" onclick="save_admin()">
		<input type="button" class="btn_setting_02" value="취소" onclick="go_page('admin_mng.php')">    		
	</div>
</div>
<!--  //CONTENTS WRAP -->
    	
<div class="clear"></div>

<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>