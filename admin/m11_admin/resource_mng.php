<?
	$top_menu = "deploy";
	$sub_menu = "resource_mng";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	if (strpos($login_adminmenu, "[deploy]") === false)
		error_back("접근 권한이 없습니다.");
	
	$db_main2 = new CDatabase_Main2();
	
	$sql = "SELECT configidx, configname, beforeversion, currentversion, writedate FROM tbl_config_version_info ORDER BY configidx";
	
	$resourcelist = $db_main2->gettotallist($sql);
	
	$db_main2->end();
?>
<script type="text/javascript">
	function update_resource(configidx, variance)
	{
		var param = {};

		param.configidx = configidx;
		param.variance = variance;

		WG_ajax_execute("admin/update_resource", param, update_resource_callback, true)
	}
	
	function update_resource_callback(result, reason)
	{
	    if (!result)
	    {
	        alert("오류 발생 - " + reason);
	    }
	    else
	    {
	       	window.location.href = "resource_mng.php";
	    }
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 리소스 관리</div>
	</div>
	<!-- //title_warp -->
	
	<table class="tbl_list_basic1">
    	<colgroup>
    		<col width="">
    		<col width="100">
    		<col width="100">
    		<col width="200">
    		<col width="100">
    	</colgroup>
    	<thead>
	    	<tr>
	    		<th class="tdl" style="padding-left:20px">리소스</th>
	    		<th>이전버전</th>
	    		<th>현재버전</th>
	    		<th>수정일</th>
	    		<th>버전수정</th>
	    	</tr>
    	</thead>
    	<tbody>
<?
	for($i=0; $i<sizeof($resourcelist); $i++)
	{ 
		$objectidx = $resourcelist[$i]["configidx"];
		$objectname = $resourcelist[$i]["configname"];
		$objectbeforeversion = $resourcelist[$i]["beforeversion"];
		$objectcurrentversion = $resourcelist[$i]["currentversion"];
		$objectdate = $resourcelist[$i]["writedate"];
?>
	    	<tr>
	    		<td class="point_title" style="padding-left:20px"><?= $objectname ?></td>
	    		<td class="tdc point"><?= $objectbeforeversion ?></td>
	    		<td class="tdc point"><?= $objectcurrentversion ?></td>
	    		<td class="tdc point"><?= $objectdate ?></td>
	    		<td class="tdc"><input type="button" class="btn_03" value="-" onclick="update_resource('<?= $objectidx ?>', -1);"> <input type="button" class="btn_03" value="+" onclick="update_resource('<?= $objectidx ?>', 1);"></td>
	    	</tr>
<?
	} 
?>
    	</tbody>
    </table>
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="config 배포" onclick="window.location.href='resource_deploy.php'">    		
	</div>
</div>
<!--  //CONTENTS WRAP -->

<div class="clear"></div>
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>