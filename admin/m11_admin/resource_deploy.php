<?
	$top_menu = "deploy";
	$sub_menu = "resource_mng";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$webservercnt = 2;
	
	if (strpos($login_adminmenu, "[deploy]") === false)
		error_back("접근 권한이 없습니다.");
?>
<script type="text/javascript">
	function tmpdeploy_resource()
	{
	    var param = {};

		param.webservercnt = <?= $webservercnt?>;

	    WG_ajax_execute("admin/tmpdeploy_resource", param, tmpdeploy_resource_callback, true)
	}

	function tmpdeploy_resource_callback(result, reason)
	{
	    if (!result)
	    {
			var issuccess = true;
		    
		    var retarray = reason.split('|');

	    	for(i=1;i<=retarray.length;i++)
	    	{
	    		$("#temp" + i.toString()).html(retarray[i-1]);

		    	if(retarray[i-1] != "OK")
		    		issuccess = false;
	    	}

	    	if(issuccess)
	    		document.getElementById("hdn_tempcheck").value = "true";
	    	else
	    		document.getElementById("hdn_tempcheck").value = "false";
	    }
	}

	function file_check()
	{
		var param = {};

		param.webservercnt = <?= $webservercnt?>;

	    WG_ajax_execute("admin/file_check", param, file_check_callback, true)
	}

	function file_check_callback(result, reason)
	{
	    if (!result)
	    {
	    	var issuccess = true;

		    var retarray = reason.split('|');

	    	for(i=1;i<=retarray.length;i++)
	    	{
				var msg = retarray[i-1];

				msg = ReplaceText(msg, "&lt;", "<");
				msg = ReplaceText(msg, "&gt;", ">");

				$("#check" + i.toString()).html(msg);

		    	if(retarray[i-1] != "OK")
		    		issuccess = false;
	    	}

	    	if(issuccess)
	    		document.getElementById("hdn_filecheck").value = "true";
	    	else
	    		document.getElementById("hdn_filecheck").value = "false";
	    }
	}

	function file_md5check()
	{
		var param = {};

		param.webservercnt = <?= $webservercnt?>;

	    WG_ajax_execute("admin/file_md5check", param, file_md5check_callback, true)
	}

	function file_md5check_callback(result, reason)
	{
	    if (!result)
	    {
		    var retarray = reason.split('|');

	    	for(i=1;i<=retarray.length;i++)
	    	{
	    		var msg = retarray[i-1];

	    		msg = ReplaceText(msg, "&lt;", "<");
				msg = ReplaceText(msg, "&gt;", ">");
				
				$("#md5" + i.toString()).html(msg);
	    	}
	    }
	}

	function beta_deploy_resource()
	{
		if(document.getElementById("hdn_tempcheck").value == "false")
		{
			alert("Temp 배포가 완료되지 않았습니다.");
		}
		else if(document.getElementById("hdn_filecheck").value == "false")
		{
			alert("파일체크가 완료되지 않았습니다.");
		}
		else
		{
			if(confirm("정말 배포하시겠습니까?"))
			{
				var param = {};

				param.webservercnt = <?= $webservercnt?>;

				WG_ajax_execute("admin/beta_deploy_resource", param, beta_deploy_resource_callback, true)
			}
		}
	}

	function beta_deploy_resource_callback(result, reason)
	{
	    if (!result)
	    {
		    var retarray = reason.split('|');

	    	for(i=1;i<=retarray.length;i++)
	    	{
		    	document.getElementById("beta_deploy" + i.toString()).innerHTML = retarray[i-1];
	    	}
	    }
	}
	
	function deploy_resource()
	{
		if(document.getElementById("hdn_tempcheck").value == "false")
		{
			alert("Temp 배포가 완료되지 않았습니다.");
		}
		else if(document.getElementById("hdn_filecheck").value == "false")
		{
			alert("파일체크가 완료되지 않았습니다.");
		}
		else
		{
			if(confirm("정말 배포하시겠습니까?"))
			{
			    var param = {};

				param.webservercnt = <?= $webservercnt?>;

			    WG_ajax_execute("admin/deploy_resource", param, deploy_resource_callback, true)
			}
		}
	}

	function deploy_resource_callback(result, reason)
	{
	    if (!result)
	    {
		    var retarray = reason.split('|');

	    	for(i=1;i<=retarray.length;i++)
	    	{
		    	document.getElementById("deploy" + i.toString()).innerHTML = retarray[i-1];
	    	}
	    }
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
    <div class="title_wrap">
    	<div class="title"><?= $top_menu_txt ?> &gt; config 배포</div>
       </div>
    <!-- //title_warp -->
    
    <table class="tbl_list_basic1">
    	<colgroup>
    		<col width="*">
    		<col width="15%">
    		<col width="25%">
    		<col width="10%">
    		<col width="15%">
    		<col width="15%">
    	</colgroup>
    	<thead>
	    	<tr>
	    		<th>Web Server</th>
	    		<th>Temp 배포</th>
	    		<th>파일 체크</th>
	    		<th>md5 체크섬</th>
	    		<th>베타 배포</th>
	    		<th>배포</th>
	    	</tr>
    	</thead>
    	<tbody>
<?
	for($i=1; $i<=$webservercnt; $i++)
	{ 
		$servername = "Web".str_pad($i, 2,'0',STR_PAD_LEFT);
?>
	    	<tr>
	    		<td class="point_title" style="text-align:center;"><?= $servername ?></td>
	    		<td class="tdc point" id="temp<?=$i?>"></td>
	    		<td class="tdc point" id="check<?=$i?>"></td>
	    		<td class="tdc point" id="md5<?=$i?>"></td>
	    		<td class="tdc point" id="beta_deploy<?=$i?>"></td>
	    		<td class="tdc point" id="deploy<?=$i?>"></td>
	    	</tr>
<?
	} 
?>
	    	
    	</tbody>
    </table>
    <div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="Temp 배포" onclick="tmpdeploy_resource();">
		<input type="button" class="btn_setting_01" value="파일 체크" onclick="file_check();">
		<input type="button" class="btn_setting_01" value="md5 체크섬" onclick="file_md5check();">
		<input type="button" class="btn_setting_01" value="베타 배포" onclick="beta_deploy_resource();">
		<input type="button" class="btn_setting_01" value="배포" onclick="deploy_resource();">    		
    </div>

	<input type="hidden" id="hdn_tempcheck" value="false" />
    <input type="hidden" id="hdn_filecheck" value="false" />
</div>
<!--  //CONTENTS WRAP -->

<div class="clear"></div>
<?
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>