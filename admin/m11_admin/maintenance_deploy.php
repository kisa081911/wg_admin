<?
	$top_menu = "admin";
	$sub_menu = "maintenance_deploy";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$webservercnt = 2;
	
	if (strpos($login_adminmenu, "[deploy]") === false)
		error_back("접근 권한이 없습니다.");

	$db_main2 = new CDatabase_Main2();
?>
<script type="text/javascript">

	function maintenance_save()
	{
		var param = {};		
		param.webservercnt = <?= $webservercnt?>;
		
		<?
			for($i=1; $i<=$webservercnt; $i++)
			{
				$servername = "WebServer".str_pad($i, 2,'0',STR_PAD_LEFT);
		?>
				param.<?=$servername?>_web_maintenance = get_radio("<?=$servername?>_web_flag");
				param.<?=$servername?>_mobile_maintenance = get_radio("<?=$servername?>_mobile_flag");				
		<?
			}
		?>

		WG_ajax_execute("admin/maintenance_save", param, maintenance_save_callback, true)
	}

	function maintenance_save_callback(result, reason)
	{
		if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            window.location.href = "maintenance_deploy.php";
        }
	}
	
	function maintenance_tmpdeploy_resource()
	{
		var param = {};

		param.webservercnt = <?= $webservercnt?>;

	    WG_ajax_execute("admin/maintenance_tmpdeploy_resource", param, maintenance_tmpdeploy_resource_callback, true)
	}

	function maintenance_tmpdeploy_resource_callback(result, reason)
	{
	    if (!result)
	    {
			var issuccess = true;
		    
		    var retarray = reason.split('|');

	    	for(i=1;i<=retarray.length;i++)
	    	{
	    		document.getElementById("temp" + i.toString()).innerHTML = retarray[i-1];

		    	if(retarray[i-1] != "OK")
		    		issuccess = false;
	    	}

	    	if(issuccess)
	    		document.getElementById("hdn_tempcheck").value = "true";
	    	else
	    		document.getElementById("hdn_tempcheck").value = "false";
	    }
	}
	
	function maintenance_deploy_resource()
	{
		if(document.getElementById("hdn_tempcheck").value == "false")
		{
			alert("Temp 배포가 완료되지 않았습니다.");
		}
		else
		{
			if(confirm("정말 배포하시겠습니까?"))
			{
				var param = {};
				param.webservercnt = <?= $webservercnt?>;

			    WG_ajax_execute("admin/maintenance_deploy_resource", param, maintenance_deploy_resource_callback, true)
			}
		}
	}

	function maintenance_deploy_resource_callback(result, reason)
	{
	    if (!result)
	    {
		    var retarray = reason.split('|');

	    	for(i=1;i<=retarray.length;i++)
	    	{
		    	document.getElementById("deploy" + i.toString()).innerHTML = retarray[i-1];
	    	}
	    }
	}
</script>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
    <div class="title_wrap">
    	<div class="title"><?= $top_menu_txt ?> &gt;  Maintenance 배포 배포</div>
       </div>
    <!-- //title_warp -->
    <form name="input_form" id="input_form" onsubmit="return false">
    	<table class="tbl_list_basic1">
    		<colgroup>
    			<col width="*">
    			<col width="20%">
    			<col width="20%">
    			<col width="20%">    		
    		</colgroup>
    		<thead>
	    		<tr>
	    			<th>Web Server</th>
	    			<th>Web Maintenance</th>
	    			<th>Mobile Maintenance</th>
	    			<th>Temp 배포</th>
	    			<th>배포</th>
	    		</tr>
    		</thead>
    		<tbody>
<?
	for($i=1; $i<=$webservercnt; $i++)
	{ 
			$servername = "WebServer".str_pad($i, 2,'0',STR_PAD_LEFT);
		
			$sql = "SELECT xml_web_maintenance, xml_mobile_maintenance FROM tbl_maintenance_xml_setting WHERE web_server = '$servername'";
			$maintenance_flag = $db_main2->getarray($sql);
			
			$web_maintenance_flag = $maintenance_flag["xml_web_maintenance"];
			$mobile_maintenance_flag = $maintenance_flag["xml_mobile_maintenance"];

?>
	    		<tr>
	    			<td class="point_title" style="text-align:center;"><?= $servername ?></td>
					<td class="tdc point" id="<?=$servername?>_web_maintenance">
						<input type="radio" id="<?=$servername?>_web_flag_0" name="<?=$servername?>_web_flag" value="0" <?= ($web_maintenance_flag=="0") ? "checked=\"true\"" : "" ?>>게임 접속 가능&nbsp;&nbsp;&nbsp;</input>
						<br>
						<input type="radio" id="<?=$servername?>_web_flag_1" name="<?=$servername?>_web_flag" value="1" <?= ($web_maintenance_flag=="1") ? "checked=\"true\"" : "" ?>>게임 접속 불가능</input>
						<br>
						<input type="radio" id="<?=$servername?>_web_flag_2" name="<?=$servername?>_web_flag" value="2" <?= ($web_maintenance_flag=="2") ? "checked=\"true\"" : "" ?>>내부만 접속 가능</input> 
					</td>
					<td class="tdc point" id="<?=$servername?>_mobile_maintenance">
						<input type="radio" id="<?=$servername?>_mobile_flag_0" name="<?=$servername?>_mobile_flag" value="0" <?= ($mobile_maintenance_flag=="0") ? "checked=\"true\"" : "" ?>>게임 접속 가능&nbsp;&nbsp;&nbsp;</input>
						<br>
						<input type="radio" id="<?=$servername?>_mobile_flag_1" name="<?=$servername?>_mobile_flag" value="1" <?= ($mobile_maintenance_flag=="1") ? "checked=\"true\"" : "" ?>>게임 접속 불가능</input>
						<br>
						<input type="radio" id="<?=$servername?>_mobile_flag_2" name="<?=$servername?>_mobile_flag" value="2" <?= ($mobile_maintenance_flag=="2") ? "checked=\"true\"" : "" ?>>내부만 접속 가능</input> 
					</td>
	    			<td class="tdc point" id="temp<?=$i?>"></td>	    		
	    			<td class="tdc point" id="deploy<?=$i?>"></td>
	    		</tr>
<?
	} 
?>
	    	
    		</tbody>
    	</table>
    	<div class="button_warp tdr">    		
    		<input type="button" class="btn_setting_01" value="메인 터넌스 저장" onclick="maintenance_save()" />
			<input type="button" class="btn_setting_01" value="Temp 배포" onclick="maintenance_tmpdeploy_resource();">
			<input type="button" class="btn_setting_01" value="배포" onclick="maintenance_deploy_resource();">    		
    	</div>
    </form>

	<input type="hidden" id="hdn_tempcheck" value="false" />
    <input type="hidden" id="hdn_filecheck" value="false" />
</div>
<!--  //CONTENTS WRAP -->

<div class="clear"></div>
<?
	$db_main2 -> end();
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>