<? 
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
	
	check_login_layer(); 
	
	$resource_name = $_POST["resource_name"];
	$type = $_POST["type"];
	$page_title = "";
	
	$db_main2 = new CDatabase_Main2();
	
	$sql = "SELECT t1.configname, t1.currentversion as prod, t2.currentversion as prod_cd ";
	$sql .= "FROM tbl_resource_version_info t1 INNER JOIN ";
	$sql .= "     tbl_resource_version_info t2 ON t1.`type` = 1 AND t2.`type` = 2 AND t1.configname = t2.configname AND t1.currentversion != t2.currentversion ";
	$sql .= "UNION ALL ";
	$sql .= "SELECT t2.configname, 'nothing' as prod, t2.currentversion as prod ";
	$sql .= "FROM tbl_resource_version_info t2 ";
	$sql .= "WHERE t2.`type` = 2 AND NOT EXISTS (SELECT * FROM tbl_resource_version_info WHERE configname = t2.configname AND `type` = 1)";
	
	$slot_resource_list = $db_main2->gettotallist($sql);
	
	$page_title = "Resource Data 복사";
	
	$db_main2->end();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $page_title ?></title>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/jquery.bpopup.min.js"></script>
<script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script>
<script type="text/javascript">
function update_resource_data()
{
	var param = {};

	WG_ajax_execute("admin/copy_resource_pord_data", param, copy_resource_data_callback, "", true);
}

function copy_resource_data_callback(result, reason)
{
	if (!result)
	{
		alert("오류 발생 - " + reason);
	}
	else
	{
		alert("변경 완료되었습니다.");
		window.parent.location.reload();
	}
}
</script>
</head>
<body class="layer_body" onload="window.focus()" onkeyup="if (getEventKeycode(event) == 27) { layer_close(); }">
<div id="layer_wrap">   
    <div class="layer_header" >
        <div class="layer_title">
        	<?= $page_title?>
        </div>
        <img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>        
    <div class="layer_contents_wrap" style="width:600px; background-color: white;">
         <!--  레이어 내용  -->
         
         <div class="layer_user_fix_height">
            <table class="tbl_list_basic1" style="margin-top:5px;width:550px;">
                <colgroup>
                    <col width="">
                    <col width="">
                    <col width="">
                </colgroup>
                <thead>
                    <tr>
                        <th class="tdc">리소스</th>
                        <th class="tdc">PROD_CD</th>
                        <th class="tdc">PROD</th>
                    </tr>
                </thead>
                <tbody>
<?
		if (sizeof($slot_resource_list) == 0)
		{
?>
                    <tr>
                         <td colspan="3" class="tdc" <?= $style ?>>복사할 대상이 존재하지 않습니다.</td>
                    </tr>
<?
		}
		else
		{
			for($i=0; $i<sizeof($slot_resource_list); $i++)
			{
				$configname = $slot_resource_list[$i]["configname"];
				$prod_cd = $slot_resource_list[$i]["prod_cd"];
				$prod = $slot_resource_list[$i]["prod"];
?>
                    <tr>
                         <td class="tdl" <?= $style ?>><?= $configname?></td>
                         <td class="tdl" <?= $style ?>><?= $prod_cd?></td>
                         <td class="tdc" <?= $style ?>><?= $prod?></td>
                    </tr>
<?
			}
		}
?>
                 </tbody>
             </table>
         </div>
         
         <!-- 확인 버튼 -->
         <div class="layer_button_wrap" style="width:550px;margin-top:10px;text-align:center;">
            <input type="button" class="btn_02" value="복사" onclick="update_resource_data()" />
            <input type="button" class="btn_02" value="닫기" onclick="fnPopupClose()" />
         </div>
         <!-- 확인 버튼 -->
    </div>
</div>
</body>
</html>

