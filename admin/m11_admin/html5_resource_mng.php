<?
    $top_menu = "deploy";
    $sub_menu = "html5_resource_mng";
    $top_menu_txt = "배포 관리";
    
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$type = ($_GET["type"] == "") ? "0" :$_GET["type"];
    
	$db_main2 = new CDatabase_Main2();
	
   	$pagename = "html5_resource_mng.php";

    $today = date("Y-m-d");
    $yesterday = date("Y-m-d",strtotime("-1 days"));
    $tomorrow = date("Y-m-d",strtotime("+1 days"));
?>
<style>
table.tbl_list_basic1 tbody tr:hover { background: #f3f3f3; }
</style>
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/jquery.bpopup.min.js"></script>
<script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
<script type="text/javascript">
	function search()
    {
        var search_form = document.search_form;
        search_form.submit();
    }
	
	function pop_update_resource_data(resource_name, type)
	{		
		try
		{
			
			var request = $.ajax({
	    		url: "pop_change_resource.php",
				type: "POST",
				data: {
						resource_name: resource_name, 
						type: type
				},
				cache: false,
				dataType: "html"
			});
	    	request.done(function( data ) {
	    		data = data.replace("&lt;", "<").replace("&gt;", ">"); 

	    		$('#to_pop_up').html(data);
	    		$('#to_pop_up').bPopup({
					easing: 'easeOutBack',
					speed: 850,
					opacity: 0.3,
					transition: 'slideDown',
					modalClose: false
				});
			});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
	    {
	    }
	}
	function copy_to_prod() 
	{
		try
		{
			var request = $.ajax({
				url: "pop_copy_to_prod_resource.php",
				type: "POST",
				cache: false,
				dataType: "html"
			});
			request.done(function( data ) {
				data = data.replace("&lt;", "<").replace("&gt;", ">"); 

				$('#to_pop_up').html(data);
				$('#to_pop_up').bPopup({
					height: 800,
					opacity: 0.3,
					modalClose: false
				});
			});
			request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e)
		{
			console.log(e);
		}
	}
</script>
<style>
	.tbl_list_basic1 tr td:nth-child(1) { max-width: 250px; text-overflow: ellipsis; overflow: hidden; white-space: nowrap; }
	.tbl_list_basic1 tr td:nth-child(2) { max-width: 600px; text-overflow: ellipsis; overflow: hidden; white-space: nowrap; }
</style>
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">	
	        
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 2.0  리소스 관리</div>
			<div class="search_box"> Resource Type : 
				<select name="type" id="type" style="width:130px">
					<option value="0" <?= ($type=="0") ? "selected" : "" ?>>DEV</option>
					<option value="1" <?= ($type=="1") ? "selected" : "" ?>>PROD</option>
					<option value="2" <?= ($type=="2") ? "selected" : "" ?>>PROD_CANDIDATE</option>					
				</select>&nbsp;&nbsp;&nbsp;
				<input type="button" class="btn_search" value="검색" onclick="search()" />
<? if ($type == "2") { ?>
				<input type="button" class="btn_search" value="PROD 복사" onclick="copy_to_prod()" />
<? } ?>
			</div>
		</div>
		<!-- //title_warp -->	
				
		<table class="tbl_list_basic1" style="margin-top:5px">
			<colgroup>
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">       
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">리소스</th>
					<th class="tdc">파일이름</th>
					<th class="tdc">타입</th>
					<th class="tdc">현재 버전</th>
					<th class="tdc">수정</th>
				</tr>
			</thead>
			<tbody>
<?
		$sql = "SELECT configname, filename, type, currentversion FROM tbl_resource_version_info WHERE type = $type ORDER BY configname ASC";
				
		$slot_resource_list = $db_main2->gettotallist($sql);
	
	    for($i=0; $i<sizeof($slot_resource_list); $i++)
	    {
	    	$configname = $slot_resource_list[$i]["configname"];
	    	$resource_name = $slot_resource_list[$i]["filename"];
	    	$type = $slot_resource_list[$i]["type"];
	    	$version = $slot_resource_list[$i]["currentversion"];
	    	
	    	/*if($type == "0")
	    		$type = "DEV";
	    	else if($type == "1")
	    		$type = "PROD";
    		else if($type == "2")
    			$type = "PROD_CANDIDATE";*/
	    	
?>
				<tr>
					<td class="tdl" title="<?= $configname ?>" <?= $style ?>><?= $configname?></td>
					<td class="tdl" title="<?= $resource_name?>" <?= $style ?>><?= $resource_name?></td>
					<td class="tdc" <?= $style ?>><?= $type?></td>
					<td class="tdc" <?= $style ?>><?= $version ?></td>
					<td class="tdc" <?= $style ?>>
						<input type="button" class="btn_03" value="수정" onclick = "pop_update_resource_data('<?= $resource_name?>',<?= $type?>)"/> 
					</td>
				</tr>
<?
	    }
?>
			</tbody>
		</table>
		<div class="button_warp tdr">
			<input type="button" class="btn_setting_01" value="리소스 추가" onclick="pop_update_resource_data()">    		
		</div>
	</form>
</div>        
<!--  //CONTENTS WRAP -->

<?	
    $db_main2->end();	
	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
