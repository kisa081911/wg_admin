<?
	$top_menu = "deploy";
	$sub_menu = "common_setting";
	$top_menu_txt = "배포관리";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$type = ($_GET["type"] == "") ? "0" :$_GET["type"];
	
	$db_main2 = new CDatabase_Main2();
	
	$pagename = "common_setting.php";

	$today = date("Y-m-d");
	$yesterday = date("Y-m-d",strtotime("-1 days"));
	$tomorrow = date("Y-m-d",strtotime("+1 days"));
?>
<link href="https://dev20.take5slots.com/3rdparty/jsoneditor/jsoneditor.min.css" rel="stylesheet" type="text/css">
<script src="https://dev20.take5slots.com/3rdparty/jsoneditor/jsoneditor.min.js"></script>
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/jquery.bpopup.min.js"></script>
<script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script>
<script type="text/javascript">
	function init(json)
	{
		// create the editor
		var container = document.getElementById("jsoneditor");
		var options = {
			mode: 'code'
		};
		window._editor = new JSONEditor(container, options);
		window._editor.set(json);
		window._previousSettingValue = json;
		/*
		var search_form = document.search_form;
		var settingString = search_form.setting.value;
		try {
			window._previousSettingValue = JSON.parse(settingString);
			console.log(window._previousSettingValue);
		} catch (e) {
			console.error(e);
		}
		*/
	}
	
	function search()
	{
		var search_form = document.search_form;
		search_form.submit();
	}
	
	function update_setting(type)
	{
		var search_form = document.search_form;
		var settingValue = window._editor.getText();
		
		if(settingValue == "")
		{
			alert("Setting 값을 입력하세요");
			window._editor.focus();
			return;
		}
		
		var currentSettingValue = null;
		try {
			currentSettingValue = JSON.parse(settingValue);
		} catch (e) {
			alert('Setting JSON 이 문법에 맞지 않습니다.');
			window._editor.focus();
			return;
		}
		
		var changes = '';
		for (var p in currentSettingValue) {
			if (currentSettingValue[p] != window._previousSettingValue[p]) {
				if (changes.length > 0) {
					changes += '\n';
				}
				var type = window._previousSettingValue[p] === undefined ? '신규' : '수정';
				changes += '[' + type + '][' + p + '] ' + window._previousSettingValue[p] + ' => ' + currentSettingValue[p];
			}
		}
		for (var p in window._previousSettingValue) {
			if (currentSettingValue[p] === undefined) {
				if (changes.length > 0) {
					changes += '\n';
				}
				changes += '[제거][' + p + '] ' + window._previousSettingValue[p] + ' => ' + currentSettingValue[p];
			}
		}
		
		if (changes.length == 0) {
			alert("변경된 사항이 없습니다.");
			window._editor.focus();
			return;
		}
		
		if (confirm(changes + '\n\n변경하시겠습니까?')) {

			var param = {};
			param.type = "<?= $type?>";
			param.settings = settingValue;
			WG_ajax_execute("admin/update_config_setting", param, update_config_setting_callback, "", true);
		}
	}

	function update_config_setting_callback(result, reason)
	{
		if(!result)
		{
			alert("오류 발생 -" + reason);
		}
		else
		{
			window.location.href = "common_setting.php?type=<?= $type?>";
		}
	}
</script>
<div class="contents_wrap">
	<form name="search_form" id="search_form"  method="get" action="<?= $pagename ?>">	
	        
		<!-- title_warp -->
		<div class="title_wrap">
			<div class="title"><?= $top_menu_txt ?> &gt; 2.0 Config 관리</div>
			<div class="search_box"> Resource Type : 
				<select name="type" id="type" style="width:60px">
					<option value="0" <?= ($type=="0") ? "selected" : "" ?>>DEV</option>
					<option value="1" <?= ($type=="1") ? "selected" : "" ?>>LIVE</option>
					<option value="2" <?= ($type=="2") ? "selected" : "" ?>>BETA</option>
				</select>&nbsp;&nbsp;&nbsp;
				<input type="button" class="btn_search" value="검색" onclick="search()" />
			</div>
		</div>
		<!-- //title_warp -->
				
		<table class="tbl_list_basic1" style="margin-top:5px">
			<colgroup>
				<col width="">
				<col width="">
			</colgroup>
			<thead>
				<tr>
					<th class="tdc">Settings</th>
					<th class="tdc">수정</th>
				</tr>
			</thead>
			<tbody>
<?
		$sql = "SELECT settings FROM tbl_common_settings_info WHERE type = $type";
		$config_value = $db_main2->getvalue($sql);
?>
				<tr>
					<td class="tdl">
						<div id="jsoneditor" style="width: 950px; height: 500px;"></div>
					</td>
					<td class="tdc">
						<input type="button" class="btn_03" value="적용" onclick = "update_setting(<?= $type?>)"/> 
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</div>
<script type="text/javascript">
	init(<?= $config_value ?>);
</script>
<!--  //CONTENTS WRAP -->

<?	
$db_main2->end();	
	
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>
