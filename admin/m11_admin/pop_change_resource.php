<? 
	include($_SERVER['DOCUMENT_ROOT']."/common/common_include.inc.php");
    
    check_login_layer(); 
    
    $resource_name = $_POST["resource_name"];
    $type = $_POST["type"];
    $page_title = "";
    
    $db_main2 = new CDatabase_Main2();
    
    if($resource_name != "" && $type != "")
    {
    	$sql = "SELECT configname, filename, `type`, `currentversion` FROM tbl_resource_version_info WHERE filename = '$resource_name' AND `type` = $type ";
    	$resource_data = $db_main2->getarray($sql);
    	
    	$page_title = "Resource Data 수정";
    }
    else 
   {
   		$page_title = "Resource Data 추가";
   }
    
   $db_main2->end();
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $page_title ?></title>
<link type="text/css" rel="stylesheet" href="/css/style.css" />
<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/js/jquery.bpopup.min.js"></script>
<script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/js/common_util.js"></script>
<script type="text/javascript" src="/js/ajax_helper.js"></script>
<script type="text/javascript" src="/js/common_biz.js"></script>
<script type="text/javascript">
$(document).ready(function() {
<?
	if($resource_name != "" && $type != "")
	{
?>
	$('#resource_version').focus();
<?
	}
	else
	{
?>
	$('#config_name').focus();
<?
	}
?>
});
	function update_resource_data(update_type)
    {
        var input_form = document.input_form;

        if (input_form.config_name.value == "")
        {			         
            alert("버전 키를 입력해주세요.");
            input_form.config_name.focus();
            return;
        }

        if (input_form.resource_name.value == "")
        {			         
            alert("리소스 경로를 입력해주세요.");
            input_form.resource_name.focus();
            return;
        }
        
        if (input_form.resource_type.value == "")
        {
            alert("리소스 타입을 입력해주세요.");
            input_form.resource_type.focus();
            return;
        }
		
		if (input_form.resource_version.value == "")
        {
            alert("리소스 버전을 입력해주세요.");
            input_form.resource_version.focus();
            return;
        }

		var param = {};
	    param.config_name=input_form.config_name.value;
	    param.resource_name=input_form.resource_name.value;
	    param.resource_type=input_form.resource_type.value;
	    param.resource_version=input_form.resource_version.value;
	    param.update_type=update_type;
		
	    WG_ajax_execute("admin/update_resource_data", param, update_resource_data_callback, "", true);
    }
    
    function update_resource_data_callback(result, reason)
    {
        var input_form = document.input_form; 
        
        if (!result)
        {
            alert("오류 발생 - " + reason);
        }
        else
        {
            alert("변경 완료되었습니다.");
			window.parent.location.reload();
        }
    }

    function verify_resource()
    {
		try
		{
			//var url = "https://dev20 .take5slots.com/cdn";
			var url = "https://cdn-html5.take5slots.com";
			//var current_version = "<?= $resource_data['currentversion']?>";
			var current_version = document.getElementById("resource_version").value;
			var filename = "<?= $resource_data['filename']?>";
				filename = ReplaceText(filename, "{version}", current_version);
			
				
			var request = $.ajax({
	    		url: "remote_file_exist.php",
				type: "POST",
				data: { 
					host_url : url + filename
				},
				cache: false,
				dataType: "html",
			});
			
	    	request.done(function( data ) {
		    	if(data == 200)
		    	{
					alert("OK");
					document.getElementById("btn_confirm").style.display = "";
		    	}
		    	else
		    	{
					alert("Fail");
					document.getElementById("btn_confirm").style.display = "none";
		    	}
	    	});
	    	request.fail(function( jqXHR, textStatus ) {
			});
		}
		catch (e){}
    }
</script>
</head>
<body class="layer_body" onload="window.focus()" onkeyup="if (getEventKeycode(event) == 27) { layer_close(); }">
<div id="layer_wrap">   
    <div class="layer_header" >
        <div class="layer_title">
        	<?= $page_title?>
        </div>
        <img id="close_button" class="close_button" src="/images/icon/close.png" alt="닫기" style="cursor:pointer"  onclick="fnPopupClose()" />
    </div>        
    <div class="layer_contents_wrap" style="width:600px; background-color: white;">
         <!--  레이어 내용  -->
         
         <div class="layer_user_fix_height">
            <form name="input_form" id="input_form" onsubmit="return false">
            <table class="tbl_view_basic" style="width:550px">
                <colgroup>
                    <col width="">
                    <col width="">
                </colgroup>
                    <tbody>
                         <tr>
                             <th>버전 키</th>
                             <td>
            					<input type="text" class="view_tbl_text" style="width:300px" name="config_name" id="config_name" value="<?= $resource_data['configname'] ?>"  />
            				</td>
                         </tr>
                         
                         <tr>
                             <th>타입</th>
                             <td>
            					<input type="text" class="view_tbl_text" style="width:300px" name="resource_type" id="resource_type" value="<?= $resource_data['type'] ?>"  />
            				</td>
                         </tr>
                         
                         <tr>
                             <th>리소스 경로</th>
                             <td><input type="text" class="view_tbl_text" style="width:400px" name="resource_name" id="resource_name" value="<?= $resource_data['filename']?>" /></td>
                         </tr>
                         
                         <tr>
                             <th>리소스 버전</th>
                             <td>
                             	<input type="text" class="view_tbl_text" style="width:300px" name="resource_version" id="resource_version" value="<?= $resource_data['currentversion']?>"  />
                             </td>
                         </tr>
                     </tbody>
             </table>  
             </form>  
         </div>
         
         <!-- 확인 버튼 -->
         <div class="layer_button_wrap" style="width:300px;text-align:right;">
         		<input type="button" class="btn_02" value="검증" onclick="verify_resource()" />
<?
			if($resource_name != "" && $type != "")
			{
?>
				<input type="button" class="btn_02" id="btn_confirm" style="display:none;" value="변경" onclick="update_resource_data(1)" />
<?
			}
			else
			{
?>
				<input type="button" class="btn_02" value="등록" onclick="update_resource_data(2)" />
<?
			}
?>
            <input type="button" class="btn_02" value="닫기" onclick="fnPopupClose()" />
         </div>
         <!-- 확인 버튼 -->
    </div>
</div>
</body>
</html>

