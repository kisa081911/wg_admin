<?
	$top_menu = "admin";
	$sub_menu = "admin";
	
	include($_SERVER["DOCUMENT_ROOT"]."/m_common/top_frame.inc.php");
	
	$page = ($_GET["page"] == "") ? "1" : $_GET["page"];
	
	$listcount = "10";
	$pagename = "admin_mng.php";
	$pagefield = "";
	
	$tail = " WHERE 1=1"; 
	$order_by = "ORDER BY adminname ASC";
	
	$db_analysis = new CDatabase_Analysis();
	
	$totalcount = $db_analysis->getvalue("SELECT COUNT(*) FROM tbl_admin $tail");
	
	$sql = "SELECT adminidx,adminname,adminid,status,dbaccesstype,writedate FROM tbl_admin $tail $order_by LIMIT ".(($page-1) * $listcount).", ".$listcount;
	
	$adminlist = $db_analysis->gettotallist($sql); 
	
	$db_analysis->end();

	if ($totalcount < ($page-1) * $listcount && page != 1)
		$page = floor(($totalcount + $listcount - 1) / $listcount);
?>
<!-- CONTENTS WRAP -->
<div class="contents_wrap">
	<!-- title_warp -->
	<div class="title_wrap">
		<div class="title"><?= $top_menu_txt ?> &gt; 관리자목록 <span class="totalcount">(<?= make_price_format($totalcount) ?>)</span></div>
	</div>
	<!-- //title_warp -->
	
	<table class="tbl_list_basic1">
		<colgroup>
			<col width="50">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
			<col width="100">
		</colgroup>
		<thead>
    		<tr>
    			<th>번호</th>
    			<th>이름</th>
    			<th>아이디</th>
    			<th>상태</th>
    			<th>DB권한</th>
    			<th>작성일</th>
			</tr>
		</thead>
		<tbody>
<?
	for($i=0; $i<sizeof($adminlist); $i++)
	{
		$adminidx = $adminlist[$i]["adminidx"];
		$adminname = $adminlist[$i]["adminname"];
		$adminid = $adminlist[$i]["adminid"];
		$status = $adminlist[$i]["status"];
		$dbaccesstype = $adminlist[$i]["dbaccesstype"];
		$writedate = $adminlist[$i]["writedate"];
?>
			<tr  class="<?= ($status=="2") ? "tr_disabled" : "" ?>" onmouseover="className='tr_over'" onmouseout="className='<?= ($status=="2") ? "tr_disabled" : "" ?>'" onclick="view_admin_dtl(<?= $adminidx ?>)">
				<td class="tdc"><?= $totalcount - (($page-1) * $listcount) - $i ?></td>
				<td class="tdc point"><?= $adminname ?></td>
				<td class="tdc point"><?= $adminid ?></td>
				<td class="tdc point"><?= ($status == "1") ? "사용" : "미사용" ?></td>
				<td class="tdc point">
<?
				if($dbaccesstype == "0")
					echo "Selector";
				else if($dbaccesstype == "1")
					echo "Admin";
				else if($dbaccesstype == "2")
					echo "Support";
				else if($dbaccesstype == "4")
					echo "QA";
				else if($dbaccesstype == "5")
					echo "SLOT";
?>
				</td>
				<td class="tdc"><?= $writedate ?></td>
			</tr>
<?
	} 
?>
		</tbody>
	</table>
	
<?
	include($_SERVER["DOCUMENT_ROOT"]."/common/pagenation.inc.php");
?>
	
	<div class="button_warp tdr">
		<input type="button" class="btn_setting_01" value="관리자 추가" onclick="window.location.href='admin_write.php'">    		
	</div>
</div>
<!--  //CONTENTS WRAP -->
        
<div class="clear"></div>
<?
    include($_SERVER["DOCUMENT_ROOT"]."/m_common/bottom_frame.inc.php");
?>